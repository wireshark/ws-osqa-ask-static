+++
type = "question"
title = "Capture filter"
description = '''Hello. How can i make capture filter for single protocol like Jabber fo example? I dont have certain ip or port, just protocol.'''
date = "2012-08-07T00:41:00Z"
lastmod = "2012-08-07T02:33:00Z"
weight = 13417
keywords = [ "capture-filter" ]
aliases = [ "/questions/13417" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture filter](/questions/13417/capture-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13417-score" class="post-score" title="current number of votes">0</div><span id="post-13417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.</p><p>How can i make <em>capture filter</em> for single protocol like Jabber fo example? I dont have certain ip or port, just protocol.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '12, 00:41</strong></p><img src="https://secure.gravatar.com/avatar/4ca6028e5ad56e72443e277ccf0d2fe7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pnk&#39;s gravatar image" /><p><span>Pnk</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pnk has no accepted answers">0%</span></p></div></div><div id="comments-container-13417" class="comments-container"></div><div id="comment-tools-13417" class="comment-tools"></div><div class="clear"></div><div id="comment-13417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13419"></span>

<div id="answer-container-13419" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13419-score" class="post-score" title="current number of votes">0</div><span id="post-13419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't really. The capture filter engine is powerful at the lower level protocols (datalink up to transport layer) but not above. The application protocols you refer to are beyond its reach.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Aug '12, 02:33</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13419" class="comments-container"></div><div id="comment-tools-13419" class="comment-tools"></div><div class="clear"></div><div id="comment-13419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

