+++
type = "question"
title = "WIRESHARK SERVICE"
description = '''Hi, Is it possible to use Wireshark and save traffic when Windows session is not opened. Thanks.'''
date = "2016-04-05T04:21:00Z"
lastmod = "2016-04-05T04:21:00Z"
weight = 51413
keywords = [ "blind", "service" ]
aliases = [ "/questions/51413" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WIRESHARK SERVICE](/questions/51413/wireshark-service)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51413-score" class="post-score" title="current number of votes">0</div><span id="post-51413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Is it possible to use Wireshark and save traffic when Windows session is not opened. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-blind" rel="tag" title="see questions tagged &#39;blind&#39;">blind</span> <span class="post-tag tag-link-service" rel="tag" title="see questions tagged &#39;service&#39;">service</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '16, 04:21</strong></p><img src="https://secure.gravatar.com/avatar/39cf9ba229af536502739ea1f541378c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chesnut&#39;s gravatar image" /><p><span>Chesnut</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chesnut has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Apr '16, 12:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span></p></div></div><div id="comments-container-51413" class="comments-container"></div><div id="comment-tools-51413" class="comment-tools"></div><div class="clear"></div><div id="comment-51413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

