+++
type = "question"
title = "Window Size = 0 in TCP SYN"
description = '''With reference to RFC793, does the Window Size in the SYN packet servers any purpose? If the sender send a SYN packet with Window Size 0, is this still valid?'''
date = "2017-05-25T23:06:00Z"
lastmod = "2017-05-25T23:06:00Z"
weight = 61632
keywords = [ "zero-window", "tcp", "tcpwindowsize" ]
aliases = [ "/questions/61632" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Window Size = 0 in TCP SYN](/questions/61632/window-size-0-in-tcp-syn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61632-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61632-score" class="post-score" title="current number of votes">0</div><span id="post-61632-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>With reference to RFC793, does the Window Size in the SYN packet servers any purpose? If the sender send a SYN packet with Window Size 0, is this still valid?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zero-window" rel="tag" title="see questions tagged &#39;zero-window&#39;">zero-window</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-tcpwindowsize" rel="tag" title="see questions tagged &#39;tcpwindowsize&#39;">tcpwindowsize</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '17, 23:06</strong></p><img src="https://secure.gravatar.com/avatar/a44bee2f89feec3db8b150c84bf32244?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jjplaw&#39;s gravatar image" /><p><span>jjplaw</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jjplaw has no accepted answers">0%</span></p></div></div><div id="comments-container-61632" class="comments-container"></div><div id="comment-tools-61632" class="comment-tools"></div><div class="clear"></div><div id="comment-61632-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

