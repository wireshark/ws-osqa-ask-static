+++
type = "question"
title = "Can wireshark distinguish Hol(head of line) blocking section?"
description = '''I&#x27;m studying Wireshark as my degree&#x27;s project lab. I&#x27;d like to distinguish HOL(head of line) Blocking section among captured packets by coloring those packets. I&#x27;m wondering if there exists a function like this. If so, I&#x27;d appreciate if you tell me how to apply the function. If there isn&#x27;t one like ...'''
date = "2017-05-12T01:53:00Z"
lastmod = "2017-05-13T03:36:00Z"
weight = 61362
keywords = [ "dissector", "custom" ]
aliases = [ "/questions/61362" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can wireshark distinguish Hol(head of line) blocking section?](/questions/61362/can-wireshark-distinguish-holhead-of-line-blocking-section)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61362-score" class="post-score" title="current number of votes">0</div><span id="post-61362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm studying Wireshark as my degree's project lab. I'd like to distinguish HOL(head of line) Blocking section among captured packets by coloring those packets. I'm wondering if there exists a function like this. If so, I'd appreciate if you tell me how to apply the function. If there isn't one like this, I'd like to create the function and know which source code and where to add new codes. It would be the best if i can do this by putting some lines of code but if not, I'm willing to create a kind of new dissector. I'm hoping someone could help me with this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-custom" rel="tag" title="see questions tagged &#39;custom&#39;">custom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '17, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/3a702eaa9f4d90c81f74480545063c71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ngn505&#39;s gravatar image" /><p><span>ngn505</span><br />
<span class="score" title="6 reputation points">6</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ngn505 has no accepted answers">0%</span></p></div></div><div id="comments-container-61362" class="comments-container"><span id="61364"></span><div id="comment-61364" class="comment"><div id="post-61364-score" class="comment-score"></div><div class="comment-text"><p>Head of line blocking of what? Ethernet frames? TCP packets? HTTP?</p></div><div id="comment-61364-info" class="comment-info"><span class="comment-age">(12 May '17, 02:10)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61380"></span><div id="comment-61380" class="comment"><div id="post-61380-score" class="comment-score"></div><div class="comment-text"><p>Oh I forgot to mention that. It's of TCP packets.</p></div><div id="comment-61380-info" class="comment-info"><span class="comment-age">(13 May '17, 03:36)</span> <span class="comment-user userinfo">ngn505</span></div></div></div><div id="comment-tools-61362" class="comment-tools"></div><div class="clear"></div><div id="comment-61362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

