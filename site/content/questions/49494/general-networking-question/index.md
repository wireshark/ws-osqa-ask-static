+++
type = "question"
title = "[closed] General Networking Question"
description = '''If I had a network with 50 users that had an average hourly bandwidth usage of 11.05GB, what bandwidth would it require to the internet?'''
date = "2016-01-24T16:59:00Z"
lastmod = "2016-01-25T02:41:00Z"
weight = 49494
keywords = [ "general" ]
aliases = [ "/questions/49494" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] General Networking Question](/questions/49494/general-networking-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49494-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49494-score" class="post-score" title="current number of votes">0</div><span id="post-49494-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If I had a network with 50 users that had an average hourly bandwidth usage of 11.05GB, what bandwidth would it require to the internet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-general" rel="tag" title="see questions tagged &#39;general&#39;">general</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '16, 16:59</strong></p><img src="https://secure.gravatar.com/avatar/18e5e60158b9cc6f0397b7f257942488?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Balter%20Wenjamin&#39;s gravatar image" /><p><span>Balter Wenjamin</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Balter Wenjamin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>25 Jan '16, 07:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-49494" class="comments-container"><span id="49499"></span><div id="comment-49499" class="comment"><div id="post-49499-score" class="comment-score"></div><div class="comment-text"><p>Please try to collect more details (which may finally lead you to answering your question to yourself).</p><ul><li><p>do you use GB as GigaBytes (which is the usual meaning of the abbreviation when talking about data volumes) or as Gigabits?</p></li><li><p>do these 11.05 GB represent traffic to/from the internet or total traffic of the machines, i.e. including traffic flows inside the network?</p></li><li><p>is it 11.05 GB per hour per user or 11.05 GB per hour for the whole network? 11.05 GBytes per hour = 88.4 Gbits/hr = 24,5 kbits/s, so unless the questions below impose additional requirements, any connection rate above 64 kbit/s would do if those 11.5 Gbytes/hour represent a traffic of the whole network, or any connection rate above 1.5 Mbit/s would do if it represents a traffic of a single user machine.</p></li><li><p>do the individual users access internet totally randomly or are there some identifiable traffic peaks?</p></li><li><p>if peaks exist, how time critical is the application, i.e. how much harm a delayed answer from the internet would cause to the user?</p></li><li><p>if your internet connection is going to be non-symmetrical (like xDSL, mobile connections), what is the expected amount of traffic in upload and download directions separately?</p></li></ul></div><div id="comment-49499-info" class="comment-info"><span class="comment-age">(25 Jan '16, 01:08)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="49501"></span><div id="comment-49501" class="comment"><div id="post-49501-score" class="comment-score"></div><div class="comment-text"><p>Arguably not an Ask Wireshark question.</p></div><div id="comment-49501-info" class="comment-info"><span class="comment-age">(25 Jan '16, 02:41)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-49494" class="comment-tools"></div><div class="clear"></div><div id="comment-49494-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 25 Jan '16, 07:49

</div>

</div>

</div>

