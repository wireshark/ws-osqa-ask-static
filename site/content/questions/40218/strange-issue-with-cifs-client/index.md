+++
type = "question"
title = "strange issue with cifs client"
description = '''Hello All, I have currently windows 7 client that playing music title from Netapp storage via cifs. The problem is we have suddenly Failed to read from file: &#92;&#92;192.168.2.134&#92;dalet&#92;storage&#92;SPLIT_Audio_2&#92;00007c59.mp2 (The specified network name is no longer available. I did a capture and i not quit su...'''
date = "2015-03-03T07:21:00Z"
lastmod = "2015-03-03T07:21:00Z"
weight = 40218
keywords = [ "cifs", "smb" ]
aliases = [ "/questions/40218" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [strange issue with cifs client](/questions/40218/strange-issue-with-cifs-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40218-score" class="post-score" title="current number of votes">0</div><span id="post-40218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>I have currently windows 7 client that playing music title from Netapp storage via cifs. The problem is we have suddenly <code>Failed to read from file: \\192.168.2.134\dalet\storage\SPLIT_Audio_2\00007c59.mp2 (The specified network name is no longer available.</code> I did a capture and i not quit sure what i where to pin point the issue its looks from client side for me but i am not sure. I have many Ack's not seen from client side but cant really uderstand why and high RTT to Ac'k , if someone could assist me to understand where is the issue exist , below attached link to trace in cloudshark. <a href="https://www.cloudshark.org/captures/fc6dd41cfd51">https://www.cloudshark.org/captures/fc6dd41cfd51</a></p><p>Please advice Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cifs" rel="tag" title="see questions tagged &#39;cifs&#39;">cifs</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '15, 07:21</strong></p><img src="https://secure.gravatar.com/avatar/491b248bc5431fa4cfed4498e4633f51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tbaror&#39;s gravatar image" /><p><span>tbaror</span><br />
<span class="score" title="10 reputation points">10</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tbaror has no accepted answers">0%</span></p></div></div><div id="comments-container-40218" class="comments-container"></div><div id="comment-tools-40218" class="comment-tools"></div><div class="clear"></div><div id="comment-40218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

