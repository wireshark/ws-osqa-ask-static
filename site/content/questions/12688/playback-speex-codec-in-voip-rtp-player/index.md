+++
type = "question"
title = "playback speex codec in VOIP rtp player?"
description = '''I have captured a very important VOIP conversation on my magicjack line but the codec that was used was speex. Is there anyway to get the wireshark voip rtp player to play this call properly instead of just silence? I am open to using another program if that is the only option but I really have no c...'''
date = "2012-07-12T22:11:00Z"
lastmod = "2012-07-13T06:16:00Z"
weight = 12688
keywords = [ "speex", "player", "codec", "rtp", "voip" ]
aliases = [ "/questions/12688" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [playback speex codec in VOIP rtp player?](/questions/12688/playback-speex-codec-in-voip-rtp-player)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12688-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12688-score" class="post-score" title="current number of votes">0</div><span id="post-12688-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured a very important VOIP conversation on my magicjack line but the codec that was used was speex. Is there anyway to get the wireshark voip rtp player to play this call properly instead of just silence? I am open to using another program if that is the only option but I really have no clue where to start if that is even an option. Any help will be appreciated thank you for your time. By the way in case it matters I am running windows 7 64bit.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-speex" rel="tag" title="see questions tagged &#39;speex&#39;">speex</span> <span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-codec" rel="tag" title="see questions tagged &#39;codec&#39;">codec</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jul '12, 22:11</strong></p><img src="https://secure.gravatar.com/avatar/4d2a5854030fc1b208f06e1b4d00ecf1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brom66an6&#39;s gravatar image" /><p><span>brom66an6</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brom66an6 has no accepted answers">0%</span></p></div></div><div id="comments-container-12688" class="comments-container"></div><div id="comment-tools-12688" class="comment-tools"></div><div class="clear"></div><div id="comment-12688-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12705"></span>

<div id="answer-container-12705" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12705-score" class="post-score" title="current number of votes">1</div><span id="post-12705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://wiki.wireshark.org/RTP_statistics">RTP Statistics</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jul '12, 06:16</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-12705" class="comments-container"></div><div id="comment-tools-12705" class="comment-tools"></div><div class="clear"></div><div id="comment-12705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

