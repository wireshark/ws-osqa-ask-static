+++
type = "question"
title = "Can we generate customized packets using Wireshark?"
description = '''We need to test CIFS protocol with negative test scenarios to check the system behavior from Server to Host. Would it be possible to generate the packets with wrong Data using Wireshark?'''
date = "2014-01-17T02:06:00Z"
lastmod = "2014-01-17T02:28:00Z"
weight = 28987
keywords = [ "packet-modification" ]
aliases = [ "/questions/28987" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can we generate customized packets using Wireshark?](/questions/28987/can-we-generate-customized-packets-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28987-score" class="post-score" title="current number of votes">0</div><span id="post-28987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We need to test CIFS protocol with negative test scenarios to check the system behavior from Server to Host. Would it be possible to generate the packets with wrong Data using Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-modification" rel="tag" title="see questions tagged &#39;packet-modification&#39;">packet-modification</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jan '14, 02:06</strong></p><img src="https://secure.gravatar.com/avatar/ee0d75749c0b2db3e41e7f27efe1fbf9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hari%20Kapparapu&#39;s gravatar image" /><p><span>Hari Kapparapu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hari Kapparapu has no accepted answers">0%</span></p></div></div><div id="comments-container-28987" class="comments-container"></div><div id="comment-tools-28987" class="comment-tools"></div><div class="clear"></div><div id="comment-28987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28988"></span>

<div id="answer-container-28988" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28988-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28988-score" class="post-score" title="current number of votes">2</div><span id="post-28988-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No. Wireshark is not a packet generator. Take a look at scapy instead to craft packets, or Ostinato to replay traces you already have.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jan '14, 02:28</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28988" class="comments-container"></div><div id="comment-tools-28988" class="comment-tools"></div><div class="clear"></div><div id="comment-28988-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

