+++
type = "question"
title = "Ethernet(802.3) traffic from unknown ip?"
description = '''Hi all,  I&#x27;m brand new to the world of Packet Capturing, well...I should say networking in general, so I apologize if my question comes accross as me looking for a spoonfed answer... I&#x27;ve been pentesting(still learning) my windows machine lately, and sometimes I leave my AP open, and where I live it...'''
date = "2014-12-13T21:55:00Z"
lastmod = "2014-12-13T21:55:00Z"
weight = 38551
keywords = [ "wlan" ]
aliases = [ "/questions/38551" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ethernet(802.3) traffic from unknown ip?](/questions/38551/ethernet8023-traffic-from-unknown-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38551-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38551-score" class="post-score" title="current number of votes">0</div><span id="post-38551-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I'm brand new to the world of Packet Capturing, well...I should say networking in general, so I apologize if my question comes accross as me looking for a spoonfed answer...</p><p>I've been pentesting(still learning) my windows machine lately, and sometimes I leave my AP open, and where I live it doesn't take long before someone connects and I start getting traffic from that mac. Recently a new mac address has popped up and is now generating traffic from what appears to be an ethernet connection! This cant be possible because I have the only physical access to my router. During this capture I however was not connected to the wired interface, but to my wifi, which started to worry me. I've read some posts here and saw that it could be an error in my configuration.</p><p>this is the frame summary</p><pre><code>5820    664.995713000   Netgear_xx xx xx    GemtekTe_xx xx xx   LLC 1432    I, N(R)=16, N(S)=0; DSAP NULL LSAP Individual, SSAP ISO Network Layer (OSLAN 2) Command    IEEE 802.3 Ethernet</code></pre><p>I want to know why this is showing up as a wired connection, and if possible, interperet the data being sent back and forth. I am the admin of this router, so I should have all the sources to do this, I just dont know how. Can anyone point me in the right direction? Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '14, 21:55</strong></p><img src="https://secure.gravatar.com/avatar/dcaf5ca624da613a85c98a64a2dc8033?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bonkers&#39;s gravatar image" /><p><span>Bonkers</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bonkers has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Dec '14, 03:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-38551" class="comments-container"></div><div id="comment-tools-38551" class="comment-tools"></div><div class="clear"></div><div id="comment-38551-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

