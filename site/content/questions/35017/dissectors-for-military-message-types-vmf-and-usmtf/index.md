+++
type = "question"
title = "Dissectors for Military Message Types: VMF and USMTF"
description = '''Is anyone aware of a Wireshark dissector that decodes military Variable Message Format (VMF) messages per MIL-STD-6017 &amp;amp; MIL-STD-6017A? The other type is called United States Message Text Format (USMTF) per MIL-STD-6040. How would I go about addressing the issue of requesting a dissector for the...'''
date = "2014-07-30T15:01:00Z"
lastmod = "2014-07-31T08:13:00Z"
weight = 35017
keywords = [ "vmf-usmtf-msg-types" ]
aliases = [ "/questions/35017" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Dissectors for Military Message Types: VMF and USMTF](/questions/35017/dissectors-for-military-message-types-vmf-and-usmtf)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35017-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35017-score" class="post-score" title="current number of votes">0</div><span id="post-35017-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is anyone aware of a Wireshark dissector that decodes military Variable Message Format (VMF) messages per MIL-STD-6017 &amp; MIL-STD-6017A?</p><p>The other type is called United States Message Text Format (USMTF) per MIL-STD-6040.</p><p>How would I go about addressing the issue of requesting a dissector for these and other special military message types?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vmf-usmtf-msg-types" rel="tag" title="see questions tagged &#39;vmf-usmtf-msg-types&#39;">vmf-usmtf-msg-types</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '14, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/28c97773d9680ab3f1a3838b1816a2a6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="VicOne&#39;s gravatar image" /><p><span>VicOne</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="VicOne has no accepted answers">0%</span></p></div></div><div id="comments-container-35017" class="comments-container"></div><div id="comment-tools-35017" class="comment-tools"></div><div class="clear"></div><div id="comment-35017-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35031"></span>

<div id="answer-container-35031" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35031-score" class="post-score" title="current number of votes">0</div><span id="post-35031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Normally one opens an <a href="https://bugs.wireshark.org">enhancement request</a> being sure to include:</p><ol><li>One or more sample captures</li><li>Links to the specifications (if they are freely available)</li></ol><p>If the specs are not freely available then, honestly, it's not worth opening the enhancement request because the chance of someone being able to implement a dissector for the protocol in question is very low. You'd have to find someone with access to the spec who is able and willing (and has the time) to implement the dissector.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jul '14, 08:13</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-35031" class="comments-container"></div><div id="comment-tools-35031" class="comment-tools"></div><div class="clear"></div><div id="comment-35031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

