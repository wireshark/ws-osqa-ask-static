+++
type = "question"
title = "How to get IP [SWF]"
description = '''Hello. I&#x27;m owner of a game server(I have admin access, but not on SSH , only on the main website, i need to find the &quot;IP&#x27;s of the players who play with &quot;cheats,etc&quot; for ban them. It&#x27;s a Adobe Game extension it&#x27;s &quot;.swf&quot; ,it&#x27;s a website, i want for get all the IP&#x27;s, but not works i get the &quot;Cloudflare...'''
date = "2017-06-24T14:25:00Z"
lastmod = "2017-06-24T14:25:00Z"
weight = 62285
keywords = [ "me", "help" ]
aliases = [ "/questions/62285" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get IP \[SWF\]](/questions/62285/how-to-get-ip-swf)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62285-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62285-score" class="post-score" title="current number of votes">0</div><span id="post-62285-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.</p><p>I'm owner of a game server(I have admin access, but not on SSH , only on the main website, i need to find the "IP's of the players who play with "cheats,etc" for ban them. It's a Adobe Game extension it's ".swf" ,it's a website, i want for get all the IP's, but not works i get the "Cloudflare IP".</p><p>I try with HTTP(on filter), but not works.. i get the CF ip + images/photos of players.</p><p>I use Ubuntu 16.04</p><p>Thanks you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-me" rel="tag" title="see questions tagged &#39;me&#39;">me</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '17, 14:25</strong></p><img src="https://secure.gravatar.com/avatar/675d7a13bd2e801364517722ea1e347e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john40&#39;s gravatar image" /><p><span>john40</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john40 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Jun '17, 14:32</strong> </span></p></div></div><div id="comments-container-62285" class="comments-container"></div><div id="comment-tools-62285" class="comment-tools"></div><div class="clear"></div><div id="comment-62285-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

