+++
type = "question"
title = "No default interface in v.2.2.5-0 on Windows"
description = '''I recently updated Wireshark on a notebook computer &amp;amp; Windows 10. One thing I&#x27;ve noticed is the default interface setting no longer works. Previously, when I started Wireshark, it was ready to go on the Ethernet port, which I had set as default, but I now have to select it each time I start Wire...'''
date = "2017-04-07T07:24:00Z"
lastmod = "2017-04-07T07:24:00Z"
weight = 60653
keywords = [ "default", "interface" ]
aliases = [ "/questions/60653" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No default interface in v.2.2.5-0 on Windows](/questions/60653/no-default-interface-in-v225-0-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60653-score" class="post-score" title="current number of votes">0</div><span id="post-60653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently updated Wireshark on a notebook computer &amp; Windows 10. One thing I've noticed is the default interface setting no longer works. Previously, when I started Wireshark, it was ready to go on the Ethernet port, which I had set as default, but I now have to select it each time I start Wireshark. Has anyone else noticed this?</p><p>BTW, it works as expected on Linux.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-default" rel="tag" title="see questions tagged &#39;default&#39;">default</span> <span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '17, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/ba86f283d614d2cd9b6116140eaddded?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JamesK&#39;s gravatar image" /><p><span>JamesK</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JamesK has no accepted answers">0%</span></p></div></div><div id="comments-container-60653" class="comments-container"></div><div id="comment-tools-60653" class="comment-tools"></div><div class="clear"></div><div id="comment-60653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

