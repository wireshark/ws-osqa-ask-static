+++
type = "question"
title = "MAP Operations Dissecting Problem In Wireshark"
description = '''When I invoke the Supplementary Services like Register SS or Erase SS or Active SS or Deactive SS or Interrogate SS operation from VLR to HLR for version 1 then wireshark show two GSM components while it should be only one component. Another question is that why wireshark not shows the application c...'''
date = "2012-11-26T06:10:00Z"
lastmod = "2012-11-27T00:16:00Z"
weight = 16313
keywords = [ "wireshark" ]
aliases = [ "/questions/16313" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MAP Operations Dissecting Problem In Wireshark](/questions/16313/map-operations-dissecting-problem-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16313-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16313-score" class="post-score" title="current number of votes">0</div><span id="post-16313-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I invoke the Supplementary Services like Register SS or Erase SS or Active SS or Deactive SS or Interrogate SS operation from VLR to HLR for version 1 then wireshark show two GSM components while it should be only one component.</p><p>Another question is that why wireshark not shows the application context for V1 in TCAP section of the wireshark.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '12, 06:10</strong></p><img src="https://secure.gravatar.com/avatar/010ffa1f01aa73d3d3d58b3305299d83?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sammi&#39;s gravatar image" /><p><span>sammi</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sammi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> wikified <strong>27 Nov '12, 01:19</strong> </span></p></div></div><div id="comments-container-16313" class="comments-container"></div><div id="comment-tools-16313" class="comment-tools"></div><div class="clear"></div><div id="comment-16313-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16346"></span>

<div id="answer-container-16346" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16346-score" class="post-score" title="current number of votes">0</div><span id="post-16346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think we'd need to se the trace to answer. Open a bug report and attach the trace.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '12, 00:16</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-16346" class="comments-container"></div><div id="comment-tools-16346" class="comment-tools"></div><div class="clear"></div><div id="comment-16346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

