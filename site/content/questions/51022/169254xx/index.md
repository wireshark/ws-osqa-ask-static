+++
type = "question"
title = "169.254.x.x"
description = '''Please help!  When I set my laptop to Obtain an IP address automatically IPconfig shows different IP address than Wireshark. Why is that? Thank you Regards Mehran'''
date = "2016-03-18T04:07:00Z"
lastmod = "2016-03-18T09:39:00Z"
weight = 51022
keywords = [ "ask.wireshark.org" ]
aliases = [ "/questions/51022" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [169.254.x.x](/questions/51022/169254xx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51022-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51022-score" class="post-score" title="current number of votes">0</div><span id="post-51022-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please help! When I set my laptop to Obtain an IP address automatically IPconfig shows different IP address than Wireshark. Why is that?</p><p>Thank you</p><p>Regards Mehran</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '16, 04:07</strong></p><img src="https://secure.gravatar.com/avatar/4518ebe112bd749fa7a1deb5b50db9b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mehran&#39;s gravatar image" /><p><span>Mehran</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mehran has no accepted answers">0%</span></p></div></div><div id="comments-container-51022" class="comments-container"></div><div id="comment-tools-51022" class="comment-tools"></div><div class="clear"></div><div id="comment-51022-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51037"></span>

<div id="answer-container-51037" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51037-score" class="post-score" title="current number of votes">0</div><span id="post-51037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is an automatic configured IPv4 address, used as fallback when no DHCP lease can be obtained and no fixed IP address is configured. Wireshark does not show any IP address of your device it does not have.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Mar '16, 09:23</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span> </br></p></div></div><div id="comments-container-51037" class="comments-container"><span id="51038"></span><div id="comment-51038" class="comment"><div id="post-51038-score" class="comment-score"></div><div class="comment-text"><p>which leads to the question what exactly does <span>@Mehran</span> mean by "IP address shown by Wireshark". Mehran, can you be more precise?</p></div><div id="comment-51038-info" class="comment-info"><span class="comment-age">(18 Mar '16, 09:39)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-51037" class="comment-tools"></div><div class="clear"></div><div id="comment-51037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

