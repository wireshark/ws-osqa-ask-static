+++
type = "question"
title = "gmail password sniffing"
description = '''how to sniff gmail password???'''
date = "2014-01-01T06:31:00Z"
lastmod = "2014-04-21T09:32:00Z"
weight = 28508
keywords = [ "password", "sniff", "gmail" ]
aliases = [ "/questions/28508" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [gmail password sniffing](/questions/28508/gmail-password-sniffing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28508-score" class="post-score" title="current number of votes">0</div><span id="post-28508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to sniff gmail password???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span> <span class="post-tag tag-link-sniff" rel="tag" title="see questions tagged &#39;sniff&#39;">sniff</span> <span class="post-tag tag-link-gmail" rel="tag" title="see questions tagged &#39;gmail&#39;">gmail</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jan '14, 06:31</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Jan '14, 09:11</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-28508" class="comments-container"></div><div id="comment-tools-28508" class="comment-tools"></div><div class="clear"></div><div id="comment-28508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="28509"></span>

<div id="answer-container-28509" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28509-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28509-score" class="post-score" title="current number of votes">0</div><span id="post-28509-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Your own, or someone else's?</p><p>Not easily as it will be encrypted using https, e.g. see the bottom half of this web page: <a href="http://samsclass.info/120/proj/p3-wireshark.htm.">http://samsclass.info/120/proj/p3-wireshark.htm.</a></p><p>If you can manage a <a href="http://en.wikipedia.org/wiki/Man-in-the-middle_attack">mitm</a> attack then you should be able to capture it, or use a proxy, e.g <a href="http://fiddler2.com/get-fiddler">Fiddler</a>, as is explained <a href="http://stackoverflow.com/questions/2888884/can-https-connections-be-hijacked-with-a-man-in-the-middle-attack">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jan '14, 09:25</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-28509" class="comments-container"><span id="28637"></span><div id="comment-28637" class="comment"><div id="post-28637-score" class="comment-score"></div><div class="comment-text"><p>i want to try on local machine not someone else.</p></div><div id="comment-28637-info" class="comment-info"><span class="comment-age">(07 Jan '14, 08:07)</span> <span class="comment-user userinfo">john6</span></div></div><span id="28639"></span><div id="comment-28639" class="comment"><div id="post-28639-score" class="comment-score"></div><div class="comment-text"><p>Well give Fiddler a try then.</p></div><div id="comment-28639-info" class="comment-info"><span class="comment-age">(07 Jan '14, 08:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="28671"></span><div id="comment-28671" class="comment"><div id="post-28671-score" class="comment-score"></div><div class="comment-text"><p>see my answer to a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/28647/sniff-a-facebook-password">http://ask.wireshark.org/questions/28647/sniff-a-facebook-password</a></p></blockquote><p>same problem, just a different web-site. So, on your own system, it ends up with using tools like Fiddler.</p><p>Regards<br />
Kurt</p></div><div id="comment-28671-info" class="comment-info"><span class="comment-age">(08 Jan '14, 08:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28509" class="comment-tools"></div><div class="clear"></div><div id="comment-28509-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="31962"></span>

<div id="answer-container-31962" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31962-score" class="post-score" title="current number of votes">0</div><span id="post-31962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>i know from facebook (which uses https to ) that it can be managed by sniffing the cookies</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '14, 02:46</strong></p><img src="https://secure.gravatar.com/avatar/cec615fb40a2b1cb61a1cdb3e1cf11d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="astrionn&#39;s gravatar image" /><p><span>astrionn</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="astrionn has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-31962" class="comments-container"></div><div id="comment-tools-31962" class="comment-tools"></div><div class="clear"></div><div id="comment-31962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="32030"></span>

<div id="answer-container-32030" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32030-score" class="post-score" title="current number of votes">0</div><span id="post-32030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To capture the password of an SSL encrypted page requires you to do at least one of two things:</p><ol><li>Have a copy of the SSL private key to decrypt the traffic between you and the server (not going to happen since Google owns this key -- might be valuable if you hosted your own mail or https page)</li><li>Use something like an SSL Proxy to perform a Man-In-The-Middle attack. This process allows the SSL Proxy platform to fake-out your system by forcing all outbound HTTPS connections to connect to the proxy and use it's SSL Cert and Private key. The proxy will then go out and connect to the remote machine to perform the action you're trying to do.</li></ol><p>I've used Charles Proxy (<a href="http://www.charlesproxy.com">http://www.charlesproxy.com</a>) in the past with great success for viewing SSL encrypted content.</p><p>Now... here's the kicker. A smart coder will create some form of a non-reversible, encrypted password before it even leaves your machine to avoid even this type of attack -- granted even this will have it's own pitfalls and could be used to fake-out the login with the correct style of attack, but it would prevent the plain-text password from showing.</p><p>Also, if you're trying to view something over IMAPS, your best bet might just be to make sure you don't connect over SSL, then using Wireshark to view this traffic is pretty simple.</p><p>I'm not familiar with performing any type of Cookie based attack or any other attack for that matter -- My job is simply to perform protocol analysis, which sometimes requires me to defeat SSL based traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Apr '14, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/5b11899f6ef8d3994b8bcc4e5c27609f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mire3212&#39;s gravatar image" /><p><span>mire3212</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mire3212 has no accepted answers">0%</span></p></div></div><div id="comments-container-32030" class="comments-container"></div><div id="comment-tools-32030" class="comment-tools"></div><div class="clear"></div><div id="comment-32030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

