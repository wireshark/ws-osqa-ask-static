+++
type = "question"
title = "VoIP: No SIP, H.323, IAX packets. Dissected RTP inside STUN. Anyway to find the peer negotiation ?"
description = '''Tried in couple of publicly available VoIP mobile applications. Their data traffic shows TCP, SSL and TLS and STUN packets. STUN packets are dissected to RTP and RTCP fine, but cannot view any SIP, H.323 or other negotiation protocols. My believe is those negotiation packets should be encrypted. But...'''
date = "2015-05-29T00:12:00Z"
lastmod = "2015-05-29T00:12:00Z"
weight = 42739
keywords = [ "sip", "encryption", "srtp", "stun", "rtp" ]
aliases = [ "/questions/42739" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP: No SIP, H.323, IAX packets. Dissected RTP inside STUN. Anyway to find the peer negotiation ?](/questions/42739/voip-no-sip-h323-iax-packets-dissected-rtp-inside-stun-anyway-to-find-the-peer-negotiation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42739-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42739-score" class="post-score" title="current number of votes">0</div><span id="post-42739-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Tried in couple of publicly available VoIP mobile applications. Their data traffic shows TCP, SSL and TLS and STUN packets. STUN packets are dissected to RTP and RTCP fine, but cannot view any SIP, H.323 or other negotiation protocols. My believe is those negotiation packets should be encrypted. But is there any way to see whether those RTP packets (dissected from STUN) are SRTP or just RTP ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-srtp" rel="tag" title="see questions tagged &#39;srtp&#39;">srtp</span> <span class="post-tag tag-link-stun" rel="tag" title="see questions tagged &#39;stun&#39;">stun</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '15, 00:12</strong></p><img src="https://secure.gravatar.com/avatar/4ec917e3556fb6d9c03cc0e39ec7732a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shas&#39;s gravatar image" /><p><span>Shas</span><br />
<span class="score" title="1 reputation points">1</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shas has no accepted answers">0%</span></p></div></div><div id="comments-container-42739" class="comments-container"></div><div id="comment-tools-42739" class="comment-tools"></div><div class="clear"></div><div id="comment-42739-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

