+++
type = "question"
title = "how to decode the version in different struct in QUIC"
description = '''For Wireshake, how to get the QUIC version, and CID length the different version in QUIC has different struct/length, but when we dont know the version, how to get the struct, when we dont know struct, how to get version. it&#x27;s a loop! '''
date = "2017-03-03T11:05:00Z"
lastmod = "2017-03-03T11:05:00Z"
weight = 59833
keywords = [ "quic" ]
aliases = [ "/questions/59833" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to decode the version in different struct in QUIC](/questions/59833/how-to-decode-the-version-in-different-struct-in-quic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59833-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59833-score" class="post-score" title="current number of votes">0</div><span id="post-59833-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For Wireshake, how to get the QUIC version, and CID length the different version in QUIC has different struct/length, but when we dont know the version, how to get the struct, when we dont know struct, how to get version. it's a loop! <img src="https://osqa-ask.wireshark.org/upfiles/version.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-quic" rel="tag" title="see questions tagged &#39;quic&#39;">quic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '17, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/853d7093103a60a3b0083b42b705b99e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neil_hao&#39;s gravatar image" /><p><span>neil_hao</span><br />
<span class="score" title="26 reputation points">26</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neil_hao has no accepted answers">0%</span></p></img></div></div><div id="comments-container-59833" class="comments-container"></div><div id="comment-tools-59833" class="comment-tools"></div><div class="clear"></div><div id="comment-59833-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

