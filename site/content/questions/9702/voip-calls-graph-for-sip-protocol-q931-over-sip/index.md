+++
type = "question"
title = "Voip calls graph for sip protocol (Q931 over SIP)"
description = '''Hello team. In the newest Cisco application device can carry QSIG (Q931) protocol over SIP. If I use the latest wireshark version to capture this kind of traffic I&#x27;m realizing that I cannot see the voip call graph flow. I remember that in the past it was possible but it seems that in this scenario t...'''
date = "2012-03-22T10:13:00Z"
lastmod = "2012-03-22T10:13:00Z"
weight = 9702
keywords = [ "q931", "sip" ]
aliases = [ "/questions/9702" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Voip calls graph for sip protocol (Q931 over SIP)](/questions/9702/voip-calls-graph-for-sip-protocol-q931-over-sip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9702-score" class="post-score" title="current number of votes">0</div><span id="post-9702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello team. In the newest Cisco application device can carry QSIG (Q931) protocol over SIP. If I use the latest wireshark version to capture this kind of traffic I'm realizing that I cannot see the voip call graph flow. I remember that in the past it was possible but it seems that in this scenario this feature doesn't work. Have you any clue about this ? Thanks Marco</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-q931" rel="tag" title="see questions tagged &#39;q931&#39;">q931</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '12, 10:13</strong></p><img src="https://secure.gravatar.com/avatar/513dee27da58d3cdff7d38b51be970c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marco%20Menozzi&#39;s gravatar image" /><p><span>Marco Menozzi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marco Menozzi has no accepted answers">0%</span></p></div></div><div id="comments-container-9702" class="comments-container"></div><div id="comment-tools-9702" class="comment-tools"></div><div class="clear"></div><div id="comment-9702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

