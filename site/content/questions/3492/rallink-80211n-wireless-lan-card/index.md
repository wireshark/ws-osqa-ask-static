+++
type = "question"
title = "Rallink 802.11n wireless LAN card"
description = '''my wireless is support for wireshark or not??.. help me... wireshark cannot detect my wireless driver...'''
date = "2011-04-13T19:23:00Z"
lastmod = "2011-06-08T13:21:00Z"
weight = 3492
keywords = [ "wireless", "interfaces", "802.11", "compatibility", "802.11n" ]
aliases = [ "/questions/3492" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Rallink 802.11n wireless LAN card](/questions/3492/rallink-80211n-wireless-lan-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3492-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3492-score" class="post-score" title="current number of votes">1</div><span id="post-3492-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>my wireless is support for wireshark or not??.. help me... wireshark cannot detect my wireless driver...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-compatibility" rel="tag" title="see questions tagged &#39;compatibility&#39;">compatibility</span> <span class="post-tag tag-link-802.11n" rel="tag" title="see questions tagged &#39;802.11n&#39;">802.11n</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '11, 19:23</strong></p><img src="https://secure.gravatar.com/avatar/735065ac32404c514df364b483605f97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ogatatsumi&#39;s gravatar image" /><p><span>ogatatsumi</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ogatatsumi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>08 Jun '11, 19:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-3492" class="comments-container"></div><div id="comment-tools-3492" class="comment-tools"></div><div class="clear"></div><div id="comment-3492-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4457"></span>

<div id="answer-container-4457" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4457-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4457-score" class="post-score" title="current number of votes">-1</div><span id="post-4457-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>YES,HELP ME DOWNLOAD IT</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jun '11, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/1a69fdb3f746ce2ce13fcd0b78d7159a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="denny&#39;s gravatar image" /><p><span>denny</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="denny has no accepted answers">0%</span></p></div></div><div id="comments-container-4457" class="comments-container"></div><div id="comment-tools-4457" class="comment-tools"></div><div class="clear"></div><div id="comment-4457-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

