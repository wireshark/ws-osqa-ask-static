+++
type = "question"
title = "How to run Wireshark from console"
description = '''When i run Wireshark from console I get this error.: error: XDG_RUNTIME_DIR not set in the environment. (wireshark:5679): Gtk-WARNING **: cannot open display: How to fix it?'''
date = "2017-01-07T18:50:00Z"
lastmod = "2017-01-09T00:53:00Z"
weight = 58585
keywords = [ "gtk", "console", "wireshark" ]
aliases = [ "/questions/58585" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to run Wireshark from console](/questions/58585/how-to-run-wireshark-from-console)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58585-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58585-score" class="post-score" title="current number of votes">0</div><span id="post-58585-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When i run Wireshark from console I get this error.:</p><pre><code>error: XDG_RUNTIME_DIR not set in the environment.
(wireshark:5679): Gtk-WARNING **: cannot open display:</code></pre><p>How to fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span> <span class="post-tag tag-link-console" rel="tag" title="see questions tagged &#39;console&#39;">console</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '17, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/ebef6356dd73fc6e18fa2c3340d7fd31?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Saldor&#39;s gravatar image" /><p><span>Saldor</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Saldor has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Jan '17, 04:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-58585" class="comments-container"><span id="58587"></span><div id="comment-58587" class="comment"><div id="post-58587-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "from console"? Do you mean, for example, running Wireshark from the command line on the Linux console?</p></div><div id="comment-58587-info" class="comment-info"><span class="comment-age">(07 Jan '17, 20:40)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="58604"></span><div id="comment-58604" class="comment"><div id="post-58604-score" class="comment-score"></div><div class="comment-text"><p>yes, from command line.</p></div><div id="comment-58604-info" class="comment-info"><span class="comment-age">(08 Jan '17, 22:15)</span> <span class="comment-user userinfo">Saldor</span></div></div><span id="58606"></span><div id="comment-58606" class="comment"><div id="post-58606-score" class="comment-score"></div><div class="comment-text"><p>From the command line <em>on the <a href="https://en.wikipedia.org/wiki/Linux_console">Linux console</a></em>, or from the command line <em>in a terminal window such as xterm or rxvt or KDE Konsole or gnome-terminal or...</em>"? The first of those won't work and would be very difficult to make work; the second of those should work, unless you're trying to run Wireshark as root (which you <strong><em>SHOULD NOT DO</em></strong>).</p></div><div id="comment-58606-info" class="comment-info"><span class="comment-age">(09 Jan '17, 00:53)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-58585" class="comment-tools"></div><div class="clear"></div><div id="comment-58585-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

