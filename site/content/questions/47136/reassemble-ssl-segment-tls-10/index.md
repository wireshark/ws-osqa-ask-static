+++
type = "question"
title = "Reassemble SSL Segment (TLS 1.0)"
description = '''Hi Well i am looking at network traffic through wireshark for https request I have configured my ssl decryption and it is working fine. But I am crurious about the structure of reassembled SSL Segment. on the transaction i am looking at the https request from d client has 2 TLSV1 record layers. so w...'''
date = "2015-11-02T00:03:00Z"
lastmod = "2015-11-02T04:04:00Z"
weight = 47136
keywords = [ "tlsv1" ]
aliases = [ "/questions/47136" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Reassemble SSL Segment (TLS 1.0)](/questions/47136/reassemble-ssl-segment-tls-10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47136-score" class="post-score" title="current number of votes">0</div><span id="post-47136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Well i am looking at network traffic through wireshark for https request I have configured my ssl decryption and it is working fine. But I am crurious about the structure of reassembled SSL Segment. on the transaction i am looking at the https request from d client has 2 TLSV1 record layers. so wireshark reassembles this layer segments and then decripts. both layers have the Encrypted application data layers. but how does it reasseble the segment is it like "Encrypted application data layer 1 +(concat) Encrypted application data layer 2" then decrypt or then that = to the Reassembled SSL segment or the is filtered data when reassembling the segment or additional data.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tlsv1" rel="tag" title="see questions tagged &#39;tlsv1&#39;">tlsv1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '15, 00:03</strong></p><img src="https://secure.gravatar.com/avatar/b7652a30c50df02fddb612a4f6613f0a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xslammer&#39;s gravatar image" /><p><span>xslammer</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xslammer has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-47136" class="comments-container"><span id="47146"></span><div id="comment-47146" class="comment"><div id="post-47146-score" class="comment-score"></div><div class="comment-text"><p>I'm sorry, but it's unclear what you are asking for. Please rephrase your question.</p></div><div id="comment-47146-info" class="comment-info"><span class="comment-age">(02 Nov '15, 04:04)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-47136" class="comment-tools"></div><div class="clear"></div><div id="comment-47136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

