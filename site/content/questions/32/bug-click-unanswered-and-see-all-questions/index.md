+++
type = "question"
title = "Bug? Click &quot;unanswered&quot; and see all questions..."
description = '''Shouldn&#x27;t this just show unanswered questions?  L'''
date = "2010-09-11T21:37:00Z"
lastmod = "2010-09-18T17:03:00Z"
weight = 32
keywords = [ "meta" ]
aliases = [ "/questions/32" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Bug? Click "unanswered" and see all questions...](/questions/32/bug-click-unanswered-and-see-all-questions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32-score" class="post-score" title="current number of votes">1</div><span id="post-32-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Shouldn't this just show unanswered questions?</p><p>L</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '10, 21:37</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>18 Sep '10, 03:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span></p></div></div><div id="comments-container-32" class="comments-container"></div><div id="comment-tools-32" class="comment-tools"></div><div class="clear"></div><div id="comment-32-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35"></span>

<div id="answer-container-35" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35-score" class="post-score" title="current number of votes">0</div><span id="post-35-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="lchappell has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The "unanswered" tab is actually the "questions-without-any-accepted-answers" tab. A question may have many answers but the original user must accept one of them (by clicking the check mark icon) before the question is officially answered. The wording is kind of funny but it follows the convention set by Stack Exchange.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Sep '10, 22:37</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-35" class="comments-container"><span id="40"></span><div id="comment-40" class="comment"><div id="post-40-score" class="comment-score"></div><div class="comment-text"><p>That makes sense, although it's not very intuitive that people have to click the check mark. Will this be explained in the notification mails? It would be great to have something like the following in the mail:</p><p>"Please review the response given to your question. If the response does indeed answer your (original) question, please click on the check mark on the left of the response to accept the answer."</p></div><div id="comment-40-info" class="comment-info"><span class="comment-age">(12 Sep '10, 23:01)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="215"></span><div id="comment-215" class="comment"><div id="post-215-score" class="comment-score"></div><div class="comment-text"><p>It looks like this is a known bug: http://jira.osqa.net/browse/OSQA-29 A fix is slated for OSQA 0.7. Too bad 0.6 hasn't been released yet.</p></div><div id="comment-215-info" class="comment-info"><span class="comment-age">(18 Sep '10, 17:03)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-35" class="comment-tools"></div><div class="clear"></div><div id="comment-35-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

