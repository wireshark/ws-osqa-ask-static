+++
type = "question"
title = "Save as -&gt; don&#x27;t have &quot;Packet Range frame&quot;"
description = '''Hello, I downloaded last version - 1.8.1. And there are no &quot;Packet Range&quot; frame in window &quot;save as&quot;. It&#x27;s some configuration option or some other reason for it?'''
date = "2012-08-13T10:26:00Z"
lastmod = "2012-08-13T10:47:00Z"
weight = 13591
keywords = [ "save", "displayed" ]
aliases = [ "/questions/13591" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Save as -&gt; don't have "Packet Range frame"](/questions/13591/save-as-dont-have-packet-range-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13591-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13591-score" class="post-score" title="current number of votes">0</div><span id="post-13591-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I downloaded last version - 1.8.1. And there are no "Packet Range" frame in window "save as". It's some configuration option or some other reason for it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-displayed" rel="tag" title="see questions tagged &#39;displayed&#39;">displayed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '12, 10:26</strong></p><img src="https://secure.gravatar.com/avatar/f020a9287afd11f44e57bf4820f7e7e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Viktor&#39;s gravatar image" /><p><span>Viktor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Viktor has no accepted answers">0%</span></p></div></div><div id="comments-container-13591" class="comments-container"></div><div id="comment-tools-13591" class="comment-tools"></div><div class="clear"></div><div id="comment-13591-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13592"></span>

<div id="answer-container-13592" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13592-score" class="post-score" title="current number of votes">2</div><span id="post-13592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This feature has been moved to the File-&gt;Export Specified Packets menu item.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Aug '12, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-13592" class="comments-container"></div><div id="comment-tools-13592" class="comment-tools"></div><div class="clear"></div><div id="comment-13592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

