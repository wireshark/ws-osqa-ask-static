+++
type = "question"
title = "TCP Flag reset on wirless connection not wired when telnet to port 4211 on PC"
description = '''When telneting from a server to a client that is connected via wireless the connection is refused whereas on the same pc when connected via CAT 5 the connection it is accepted. The packet decode shows that TCP flag is getting reset on the wireless connection but the client send a SYN packet on when ...'''
date = "2011-03-15T19:17:00Z"
lastmod = "2011-03-16T00:33:00Z"
weight = 2858
keywords = [ "telnet" ]
aliases = [ "/questions/2858" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TCP Flag reset on wirless connection not wired when telnet to port 4211 on PC](/questions/2858/tcp-flag-reset-on-wirless-connection-not-wired-when-telnet-to-port-4211-on-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2858-score" class="post-score" title="current number of votes">0</div><span id="post-2858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When telneting from a server to a client that is connected via wireless the connection is refused whereas on the same pc when connected via CAT 5 the connection it is accepted. The packet decode shows that TCP flag is getting reset on the wireless connection but the client send a SYN packet on when the PC is wired. Any ideas as to what can cause this? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telnet" rel="tag" title="see questions tagged &#39;telnet&#39;">telnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '11, 19:17</strong></p><img src="https://secure.gravatar.com/avatar/ca5436200dfa0bbf46a43696aa207f72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dchstech&#39;s gravatar image" /><p><span>dchstech</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dchstech has no accepted answers">0%</span></p></div></div><div id="comments-container-2858" class="comments-container"></div><div id="comment-tools-2858" class="comment-tools"></div><div class="clear"></div><div id="comment-2858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2861"></span>

<div id="answer-container-2861" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2861-score" class="post-score" title="current number of votes">0</div><span id="post-2861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That sounds like a firewall blocking incoming connections on your wireless link.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '11, 00:33</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2861" class="comments-container"></div><div id="comment-tools-2861" class="comment-tools"></div><div class="clear"></div><div id="comment-2861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

