+++
type = "question"
title = "Extracting only the RTMP data and excluding the TCP and Ipv4 data"
description = '''I have a captured stream of RTMP .When I open it in wireshark I get tcp as well as ipv4 packets along with RTMP packets.Now I want to dump all the RTMP packet data(not tcp or ipv4 data) in a file.Is it possible using Wireshak or any of its utility ??'''
date = "2016-10-06T05:31:00Z"
lastmod = "2016-10-06T05:31:00Z"
weight = 56186
keywords = [ "rtmp", "wireshark" ]
aliases = [ "/questions/56186" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extracting only the RTMP data and excluding the TCP and Ipv4 data](/questions/56186/extracting-only-the-rtmp-data-and-excluding-the-tcp-and-ipv4-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56186-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56186-score" class="post-score" title="current number of votes">0</div><span id="post-56186-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a captured stream of RTMP .When I open it in wireshark I get tcp as well as ipv4 packets along with RTMP packets.Now I want to dump all the RTMP packet data(not tcp or ipv4 data) in a file.Is it possible using Wireshak or any of its utility ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtmp" rel="tag" title="see questions tagged &#39;rtmp&#39;">rtmp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '16, 05:31</strong></p><img src="https://secure.gravatar.com/avatar/c46f8e6bef16f207d8c9aa367f697b6c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abhinay&#39;s gravatar image" /><p><span>abhinay</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abhinay has no accepted answers">0%</span></p></div></div><div id="comments-container-56186" class="comments-container"></div><div id="comment-tools-56186" class="comment-tools"></div><div class="clear"></div><div id="comment-56186-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

