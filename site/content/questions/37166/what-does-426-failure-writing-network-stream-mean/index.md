+++
type = "question"
title = "What does &quot;426 failure writing network stream&quot; mean?"
description = '''Hello, I&#x27;m checking a log of FTP client, and I find an error message as &quot;426 failure writing network stream&quot;. After several error messages like this, the FTP client retried to log-in FTP server and the FTP data connection was resumed. Is this error caused by network side or FTP server side? Thank yo...'''
date = "2014-10-19T20:50:00Z"
lastmod = "2014-10-19T22:12:00Z"
weight = 37166
keywords = [ "426" ]
aliases = [ "/questions/37166" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What does "426 failure writing network stream" mean?](/questions/37166/what-does-426-failure-writing-network-stream-mean)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37166-score" class="post-score" title="current number of votes">0</div><span id="post-37166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm checking a log of FTP client, and I find an error message as "426 failure writing network stream". After several error messages like this, the FTP client retried to log-in FTP server and the FTP data connection was resumed. Is this error caused by network side or FTP server side?</p><p>Thank you! Duncan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-426" rel="tag" title="see questions tagged &#39;426&#39;">426</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '14, 20:50</strong></p><img src="https://secure.gravatar.com/avatar/f189f6f8917511d5d84ba38b2ae7059b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Duncan&#39;s gravatar image" /><p><span>Duncan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Duncan has no accepted answers">0%</span></p></div></div><div id="comments-container-37166" class="comments-container"></div><div id="comment-tools-37166" class="comment-tools"></div><div class="clear"></div><div id="comment-37166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37167"></span>

<div id="answer-container-37167" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37167-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37167-score" class="post-score" title="current number of votes">0</div><span id="post-37167-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can't really say without a capture of when it happened... sounds like a server side problem though, but without proof I could be wrong of course.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '14, 22:12</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-37167" class="comments-container"></div><div id="comment-tools-37167" class="comment-tools"></div><div class="clear"></div><div id="comment-37167-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

