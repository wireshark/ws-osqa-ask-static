+++
type = "question"
title = "Wireshark and OpenBTS configuration"
description = '''Hi. I am using a Range Networks Dev. Kit (pretty similar to USRP from Ettus Research) which is running OpenBTS suite (OpenBTS itself, Sipauthserve, Smqueue, etc), and I need to configure Wireshark for packet sniffing tests. More precisely I need to view MOSMS in plain text (messages coming from prov...'''
date = "2013-11-18T14:27:00Z"
lastmod = "2013-11-18T14:27:00Z"
weight = 27078
keywords = [ "openbts", "wireshark" ]
aliases = [ "/questions/27078" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and OpenBTS configuration](/questions/27078/wireshark-and-openbts-configuration)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27078-score" class="post-score" title="current number of votes">0</div><span id="post-27078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. I am using a Range Networks Dev. Kit (pretty similar to USRP from Ettus Research) which is running OpenBTS suite (OpenBTS itself, Sipauthserve, Smqueue, etc), and I need to configure Wireshark for packet sniffing tests. More precisely I need to view MOSMS in plain text (messages coming from provisioned cell phones, sent to the "outside world"). Please note that OpenBTS is running on Dev Kit (Ubuntu), I'm using Putty from my PC in order to control OpenBTS via Ethernet port (RJ45), and Wireshark is installed and running on my PC.</p><p>So, what settings I need on Wireshark (and OpenBTS?), in order to analyze/capture plain text SMS?</p><p>Thanks a lot, Doru.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-openbts" rel="tag" title="see questions tagged &#39;openbts&#39;">openbts</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '13, 14:27</strong></p><img src="https://secure.gravatar.com/avatar/f4ee807505073ce43eb375fc51a40279?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Doru&#39;s gravatar image" /><p><span>Doru</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Doru has no accepted answers">0%</span></p></div></div><div id="comments-container-27078" class="comments-container"></div><div id="comment-tools-27078" class="comment-tools"></div><div class="clear"></div><div id="comment-27078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

