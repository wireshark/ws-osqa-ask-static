+++
type = "question"
title = "BER Error: This field lies beyond the end of the known sequence definition."
description = '''Dear Team, I am trying to decode IDP query trace from GSM world, i am getting the above error after the field iPSSPCapabilities: 00 in Expert Info: BER ERROR UNKNOWN FIELD IN SEQUENCE is the message, kindly let me know how to get this error cleared.'''
date = "2012-05-07T06:47:00Z"
lastmod = "2012-05-08T03:30:00Z"
weight = 10729
keywords = [ "gsm" ]
aliases = [ "/questions/10729" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [BER Error: This field lies beyond the end of the known sequence definition.](/questions/10729/ber-error-this-field-lies-beyond-the-end-of-the-known-sequence-definition)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10729-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10729-score" class="post-score" title="current number of votes">0</div><span id="post-10729-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Team, I am trying to decode IDP query trace from GSM world, i am getting the above error after the field iPSSPCapabilities: 00 in Expert Info: BER ERROR UNKNOWN FIELD IN SEQUENCE is the message, kindly let me know how to get this error cleared.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 May '12, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/e041aeb3658c4cb5405305409b1878b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="praveen_kr&#39;s gravatar image" /><p><span>praveen_kr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="praveen_kr has no accepted answers">0%</span></p></div></div><div id="comments-container-10729" class="comments-container"></div><div id="comment-tools-10729" class="comment-tools"></div><div class="clear"></div><div id="comment-10729-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10748"></span>

<div id="answer-container-10748" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10748-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10748-score" class="post-score" title="current number of votes">0</div><span id="post-10748-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Searching the sources iPSSPCapabilities looks to be Camel, the folowing possiblities comes to mind. - It's a Wireshark bug - open a bug report including a binary trace file with the packet. - It's a proprietarry extension - nothing we can do without the specification.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 May '12, 13:40</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-10748" class="comments-container"><span id="10774"></span><div id="comment-10774" class="comment"><div id="post-10774-score" class="comment-score"></div><div class="comment-text"><p>how to open a bug report please let me know</p></div><div id="comment-10774-info" class="comment-info"><span class="comment-age">(08 May '12, 03:27)</span> <span class="comment-user userinfo">praveen_kr</span></div></div><span id="10775"></span><div id="comment-10775" class="comment"><div id="post-10775-score" class="comment-score"></div><div class="comment-text"><p>go to <a href="https://bugs.wireshark.org/,">https://bugs.wireshark.org/,</a> create an account and 'file a bug report' (button on the page). Choose 'Wireshark' as product. The rest will be explained on the bugs page.</p></div><div id="comment-10775-info" class="comment-info"><span class="comment-age">(08 May '12, 03:30)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-10748" class="comment-tools"></div><div class="clear"></div><div id="comment-10748-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

