+++
type = "question"
title = "analyzing 802.11 capture"
description = '''I have captured a ping request/response via AirPcap interface. The ICMP messages are displayed as QoS Data versus ICMP. How do I display these frames as ICMP protocol ?'''
date = "2012-07-19T16:33:00Z"
lastmod = "2012-07-20T00:12:00Z"
weight = 12869
keywords = [ "semperfi" ]
aliases = [ "/questions/12869" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [analyzing 802.11 capture](/questions/12869/analyzing-80211-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12869-score" class="post-score" title="current number of votes">0</div><span id="post-12869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured a ping request/response via AirPcap interface. The ICMP messages are displayed as QoS Data versus ICMP. How do I display these frames as ICMP protocol ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-semperfi" rel="tag" title="see questions tagged &#39;semperfi&#39;">semperfi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '12, 16:33</strong></p><img src="https://secure.gravatar.com/avatar/7bad3b482a4233f7532de28f1a8cca64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jeff&#39;s gravatar image" /><p><span>Jeff</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jeff has no accepted answers">0%</span></p></div></div><div id="comments-container-12869" class="comments-container"></div><div id="comment-tools-12869" class="comment-tools"></div><div class="clear"></div><div id="comment-12869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12877"></span>

<div id="answer-container-12877" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12877-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12877-score" class="post-score" title="current number of votes">0</div><span id="post-12877-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That means wireshark is not able to decrypt the packets. Have you entered your wireless key/password correctly?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '12, 00:12</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '12, 00:13</strong> </span></p></div></div><div id="comments-container-12877" class="comments-container"></div><div id="comment-tools-12877" class="comment-tools"></div><div class="clear"></div><div id="comment-12877-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

