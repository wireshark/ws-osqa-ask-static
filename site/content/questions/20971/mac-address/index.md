+++
type = "question"
title = "mac address"
description = '''why does destination mac address 00:00:00:00:00:00 is reported for different types of ip add&#x27;s even though it is not corresponding any of the ip&#x27;s i am using??? what does this mac address means ?? is it like failing securely ???? someone help me out please '''
date = "2013-05-05T17:27:00Z"
lastmod = "2013-05-09T02:44:00Z"
weight = 20971
keywords = [ "mac-address" ]
aliases = [ "/questions/20971" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [mac address](/questions/20971/mac-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20971-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20971-score" class="post-score" title="current number of votes">0</div><span id="post-20971-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>why does destination mac address 00:00:00:00:00:00 is reported for different types of ip add's even though it is not corresponding any of the ip's i am using??? what does this mac address means ?? is it like failing securely ???? someone help me out please</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '13, 17:27</strong></p><img src="https://secure.gravatar.com/avatar/0f19aa9efeeb7a1409b85d75ad0ca07c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ark&#39;s gravatar image" /><p><span>ark</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ark has no accepted answers">0%</span></p></div></div><div id="comments-container-20971" class="comments-container"></div><div id="comment-tools-20971" class="comment-tools"></div><div class="clear"></div><div id="comment-20971-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21052"></span>

<div id="answer-container-21052" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21052-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21052-score" class="post-score" title="current number of votes">0</div><span id="post-21052-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Depending on what capture it is you are looking at, it is not necessarily the real MAC addresses that you see. Some trace tools create their own 'fake' MAC-addresses.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '13, 02:44</strong></p><img src="https://secure.gravatar.com/avatar/d6607c3aca20db751d019d8bbd2da893?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde2&#39;s gravatar image" /><p><span>mrEEde2</span><br />
<span class="score" title="336 reputation points">336</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde2 has 5 accepted answers">20%</span></p></div></div><div id="comments-container-21052" class="comments-container"></div><div id="comment-tools-21052" class="comment-tools"></div><div class="clear"></div><div id="comment-21052-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

