+++
type = "question"
title = "JSON Rest API"
description = '''Hello, I would like to create a filter that detects TWO kinds of JSON messages. Precisely, I would like to pick the following JSON messages and only them : ==&amp;gt; those two fileds BLABLABLA and TICTICTIC have these structures :  FILTER ONE : BLABLA fields are under : Hypertext transfer protocol/Java...'''
date = "2016-09-14T07:51:00Z"
lastmod = "2016-09-14T07:51:00Z"
weight = 55557
keywords = [ "funnix" ]
aliases = [ "/questions/55557" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [JSON Rest API](/questions/55557/json-rest-api)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55557-score" class="post-score" title="current number of votes">0</div><span id="post-55557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I would like to create a filter that detects TWO kinds of JSON messages.</p><p>Precisely, I would like to pick the following JSON messages and only them :</p><p>==&gt; those two fileds BLABLABLA and TICTICTIC have these structures :</p><p>FILTER ONE : BLABLA fields are under : Hypertext transfer protocol/JavaScript Object Notation(appmlication JSON)/Array/Object/Member Key : BLABLABLA</p><p>FILTER TWO : TICTICTIC fields under : Hypertext transfer protocol/JavaScript Object Notation(appmlication JSON)/Array/Object/Member Key : BLABLABLA/StringValue :TICTICTIC</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-funnix" rel="tag" title="see questions tagged &#39;funnix&#39;">funnix</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '16, 07:51</strong></p><img src="https://secure.gravatar.com/avatar/4205e2874fcf648ead8f5578241a01c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Funnix&#39;s gravatar image" /><p><span>Funnix</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Funnix has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Sep '16, 07:53</strong> </span></p></div></div><div id="comments-container-55557" class="comments-container"></div><div id="comment-tools-55557" class="comment-tools"></div><div class="clear"></div><div id="comment-55557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

