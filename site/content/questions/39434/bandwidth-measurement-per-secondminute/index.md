+++
type = "question"
title = "Bandwidth measurement per second&#92;minute"
description = '''Hi All, I am looking to capture bandwidth or bytes size per minute&#92;second timeline with wireshark. Protocol heirarchy stats seems to give the total bandwidth, also summary gives an average byte per second.  Is there any way of mapping&#92;graphing the amount of exact bytes&#92;bandwidth used per sec&#92;minute ...'''
date = "2015-01-27T03:36:00Z"
lastmod = "2015-01-27T05:14:00Z"
weight = 39434
keywords = [ "bandwidth" ]
aliases = [ "/questions/39434" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bandwidth measurement per second\\minute](/questions/39434/bandwidth-measurement-per-secondminute)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39434-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39434-score" class="post-score" title="current number of votes">0</div><span id="post-39434-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, I am looking to capture bandwidth or bytes size per minute\second timeline with wireshark. Protocol heirarchy stats seems to give the total bandwidth, also summary gives an average byte per second. Is there any way of mapping\graphing the amount of exact bytes\bandwidth used per sec\minute as opposed to the average.</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '15, 03:36</strong></p><img src="https://secure.gravatar.com/avatar/d5a6b67eba12bdb8f299419b3ac210a6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tomthumb&#39;s gravatar image" /><p><span>tomthumb</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tomthumb has no accepted answers">0%</span></p></div></div><div id="comments-container-39434" class="comments-container"></div><div id="comment-tools-39434" class="comment-tools"></div><div class="clear"></div><div id="comment-39434-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39438"></span>

<div id="answer-container-39438" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39438-score" class="post-score" title="current number of votes">1</div><span id="post-39438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could use Statistics -&gt; IO Graph.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '15, 05:14</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-39438" class="comments-container"></div><div id="comment-tools-39438" class="comment-tools"></div><div class="clear"></div><div id="comment-39438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

