+++
type = "question"
title = "Is there any way to get certificate in standard format using wireshark automation i.e. either Tshark or Lua?"
description = '''Hello,  using Tshark I able to get certificate packet details but I want only &quot;handshake Protocol: certificate&quot; data not packet details. and using Lua I able to get only ssl in Hex format but I want handshake Protocol: certificate&quot; data in hex Thank You.'''
date = "2015-08-28T04:00:00Z"
lastmod = "2015-08-28T04:42:00Z"
weight = 45446
keywords = [ "lua", "tshark", "wireshark" ]
aliases = [ "/questions/45446" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there any way to get certificate in standard format using wireshark automation i.e. either Tshark or Lua?](/questions/45446/is-there-any-way-to-get-certificate-in-standard-format-using-wireshark-automation-ie-either-tshark-or-lua)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45446-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45446-score" class="post-score" title="current number of votes">0</div><span id="post-45446-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, using Tshark I able to get certificate packet details but I want only "handshake Protocol: certificate" data not packet details. and using Lua I able to get only ssl in Hex format but I want handshake Protocol: certificate" data in hex</p><p>Thank You.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '15, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/a758a2fd6fc5d489ffed08e73335d70e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Varsharani%20Hawanna&#39;s gravatar image" /><p><span>Varsharani H...</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Varsharani Hawanna has no accepted answers">0%</span></p></div></div><div id="comments-container-45446" class="comments-container"><span id="45448"></span><div id="comment-45448" class="comment"><div id="post-45448-score" class="comment-score"></div><div class="comment-text"><p><strong>why do you post 5 (FIVE!) questions for the exact same issue?</strong></p><p>See my answer to a similar question, which you could have found by searching this site!</p><blockquote><p><a href="https://ask.wireshark.org/questions/44527/extracting-ssl-certificate-with-tshark">https://ask.wireshark.org/questions/44527/extracting-ssl-certificate-with-tshark</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div id="comment-45448-info" class="comment-info"><span class="comment-age">(28 Aug '15, 04:42)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-45446" class="comment-tools"></div><div class="clear"></div><div id="comment-45446-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

