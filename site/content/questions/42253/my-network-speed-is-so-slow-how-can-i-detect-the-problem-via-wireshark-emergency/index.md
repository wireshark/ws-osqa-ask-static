+++
type = "question"
title = "My Network Speed is So slow. How can I detect the problem via Wireshark? Emergency."
description = '''Hello Expert. I use Windows server 2012 R2 and When I copy a file from a server to another Server the copying Speed is very very slow. How can I find the problem? Please show me some guidance. Thank you.'''
date = "2015-05-09T07:18:00Z"
lastmod = "2015-05-10T03:16:00Z"
weight = 42253
keywords = [ "slow", "copying" ]
aliases = [ "/questions/42253" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [My Network Speed is So slow. How can I detect the problem via Wireshark? Emergency.](/questions/42253/my-network-speed-is-so-slow-how-can-i-detect-the-problem-via-wireshark-emergency)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42253-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42253-score" class="post-score" title="current number of votes">0</div><span id="post-42253-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Expert. I use Windows server 2012 R2 and When I copy a file from a server to another Server the copying Speed is very very slow. How can I find the problem? Please show me some guidance.</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span> <span class="post-tag tag-link-copying" rel="tag" title="see questions tagged &#39;copying&#39;">copying</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '15, 07:18</strong></p><img src="https://secure.gravatar.com/avatar/1f1d393403ea997213960ee852d8f897?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hack3rcon&#39;s gravatar image" /><p><span>hack3rcon</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hack3rcon has no accepted answers">0%</span></p></div></div><div id="comments-container-42253" class="comments-container"><span id="42265"></span><div id="comment-42265" class="comment"><div id="post-42265-score" class="comment-score"></div><div class="comment-text"><p>If you can provide a capture, we can help you better assess the issue.</p></div><div id="comment-42265-info" class="comment-info"><span class="comment-age">(09 May '15, 17:57)</span> <span class="comment-user userinfo">Beldum</span></div></div></div><div id="comment-tools-42253" class="comment-tools"></div><div class="clear"></div><div id="comment-42253-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42260"></span>

<div id="answer-container-42260" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42260-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42260-score" class="post-score" title="current number of votes">0</div><span id="post-42260-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Make a capture at one of these servers of the copying of a file, post the capture to cloudshark and add the link here. Maybee someone with insight in SMB (I assume) might come up with further suggestions then.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '15, 10:16</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-42260" class="comments-container"><span id="42270"></span><div id="comment-42270" class="comment"><div id="post-42270-score" class="comment-score"></div><div class="comment-text"><p>Hello. I attached Two files.</p><p><a href="https://osqa-ask.wireshark.org/upfiles/Wireshark-1_FcC6NMR.png">link text</a> <a href="https://osqa-ask.wireshark.org/upfiles/Wireshark-2_HcxMb0U.png">link text</a></p></div><div id="comment-42270-info" class="comment-info"><span class="comment-age">(09 May '15, 21:55)</span> <span class="comment-user userinfo">hack3rcon</span></div></div><span id="42275"></span><div id="comment-42275" class="comment"><div id="post-42275-score" class="comment-score"></div><div class="comment-text"><p>No, we said "post capture files". What you've posted are graphics files of two packets dissected, that is not what one can work with when analyzing the interactions of a protocol. Can you take two pictures from a basketball game and determine why the tactics of team is not working? That's what you're asking us to do right now. Show us the video of the game, so to speak. And tell us what the intentions are, which team is playing at what side, what are they trying to accomplish.</p></div><div id="comment-42275-info" class="comment-info"><span class="comment-age">(10 May '15, 03:16)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-42260" class="comment-tools"></div><div class="clear"></div><div id="comment-42260-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

