+++
type = "question"
title = "Wireshark Capture Interface not resizable under Ubuntu Linux"
description = '''The capture interface is not resizable, so if there are many interfaces, it will be not convenient to view all of them.'''
date = "2015-01-22T01:54:00Z"
lastmod = "2015-01-22T05:51:00Z"
weight = 39349
keywords = [ "capture-interface", "wireshark" ]
aliases = [ "/questions/39349" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Capture Interface not resizable under Ubuntu Linux](/questions/39349/wireshark-capture-interface-not-resizable-under-ubuntu-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39349-score" class="post-score" title="current number of votes">0</div><span id="post-39349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The capture interface is not resizable, so if there are many interfaces, it will be not convenient to view all of them.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-interface" rel="tag" title="see questions tagged &#39;capture-interface&#39;">capture-interface</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '15, 01:54</strong></p><img src="https://secure.gravatar.com/avatar/f8ff7cac8a632869fec673fd6c9ec22f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HuangY&#39;s gravatar image" /><p><span>HuangY</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HuangY has no accepted answers">0%</span></p></div></div><div id="comments-container-39349" class="comments-container"></div><div id="comment-tools-39349" class="comment-tools"></div><div class="clear"></div><div id="comment-39349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39350"></span>

<div id="answer-container-39350" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39350-score" class="post-score" title="current number of votes">1</div><span id="post-39350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not a question so you should have taken it to the bug tracker instead; please open a feature request/bug report at <a href="http://bugs.wireshark.org"></a><a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '15, 02:00</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-39350" class="comments-container"><span id="39352"></span><div id="comment-39352" class="comment"><div id="post-39352-score" class="comment-score"></div><div class="comment-text"><p>submitted, link as below: <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10880">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10880</a></p></div><div id="comment-39352-info" class="comment-info"><span class="comment-age">(22 Jan '15, 02:14)</span> <span class="comment-user userinfo">HuangY</span></div></div><span id="39355"></span><div id="comment-39355" class="comment"><div id="post-39355-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-39355-info" class="comment-info"><span class="comment-age">(22 Jan '15, 05:51)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-39350" class="comment-tools"></div><div class="clear"></div><div id="comment-39350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

