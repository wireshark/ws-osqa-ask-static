+++
type = "question"
title = "Wireless Adaptors"
description = '''Are there any specific wireless adaptors that do NOT work with WireShark? I googled and also looked on here and haven&#x27;t found any lists. Can anyone assist?'''
date = "2010-09-19T15:32:00Z"
lastmod = "2010-09-19T16:06:00Z"
weight = 221
keywords = [ "adaptors" ]
aliases = [ "/questions/221" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless Adaptors](/questions/221/wireless-adaptors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-221-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-221-score" class="post-score" title="current number of votes">0</div><span id="post-221-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are there any specific wireless adaptors that do NOT work with WireShark? I googled and also looked on here and haven't found any lists. Can anyone assist?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adaptors" rel="tag" title="see questions tagged &#39;adaptors&#39;">adaptors</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '10, 15:32</strong></p><img src="https://secure.gravatar.com/avatar/b8dd02a98602cd645c26f9e4b02e487a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Grafixx01&#39;s gravatar image" /><p><span>Grafixx01</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Grafixx01 has no accepted answers">0%</span></p></div></div><div id="comments-container-221" class="comments-container"><span id="223"></span><div id="comment-223" class="comment"><div id="post-223-score" class="comment-score"></div><div class="comment-text"><p>What OS are you running?</p></div><div id="comment-223-info" class="comment-info"><span class="comment-age">(19 Sep '10, 16:06)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-221" class="comment-tools"></div><div class="clear"></div><div id="comment-221-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

