+++
type = "question"
title = "[closed] ipconfig address"
description = '''can someone explain why ipconfig is the same as ipv4 on command prompt, I have to explain this on a test and I dont know the answer..thanks'''
date = "2016-01-24T05:49:00Z"
lastmod = "2016-01-24T07:22:00Z"
weight = 49491
keywords = [ "ipconfig" ]
aliases = [ "/questions/49491" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] ipconfig address](/questions/49491/ipconfig-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49491-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49491-score" class="post-score" title="current number of votes">0</div><span id="post-49491-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can someone explain why ipconfig is the same as ipv4 on command prompt, I have to explain this on a test and I dont know the answer..thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipconfig" rel="tag" title="see questions tagged &#39;ipconfig&#39;">ipconfig</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '16, 05:49</strong></p><img src="https://secure.gravatar.com/avatar/795b3cd54d24a090e60638d0bcedc22d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ttrlind&#39;s gravatar image" /><p><span>ttrlind</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ttrlind has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Jan '16, 07:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-49491" class="comments-container"><span id="49492"></span><div id="comment-49492" class="comment"><div id="post-49492-score" class="comment-score"></div><div class="comment-text"><p>Not really a question for Ask wireshark, better ask on the appropriate forum for your OS.</p><p>You should also note that teachers can also use the internet to look for those asking for answers to assignments.</p></div><div id="comment-49492-info" class="comment-info"><span class="comment-age">(24 Jan '16, 07:22)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-49491" class="comment-tools"></div><div class="clear"></div><div id="comment-49491-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 24 Jan '16, 07:22

</div>

</div>

</div>

