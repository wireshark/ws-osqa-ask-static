+++
type = "question"
title = "Need Router advertisement packet with Route information options"
description = '''I am looking for a Router advertisement sample packet with route information options(RFC 4191).  Can someone pls help me by posting the packet capture output?'''
date = "2014-12-23T03:24:00Z"
lastmod = "2014-12-30T03:57:00Z"
weight = 38680
keywords = [ "ipv6" ]
aliases = [ "/questions/38680" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Need Router advertisement packet with Route information options](/questions/38680/need-router-advertisement-packet-with-route-information-options)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38680-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38680-score" class="post-score" title="current number of votes">0</div><span id="post-38680-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a Router advertisement sample packet with route information options(RFC 4191). Can someone pls help me by posting the packet capture output?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Dec '14, 03:24</strong></p><img src="https://secure.gravatar.com/avatar/5b8fec0a9a70ae4eb5f75c30f98ab240?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vij&#39;s gravatar image" /><p><span>Vij</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vij has no accepted answers">0%</span></p></div></div><div id="comments-container-38680" class="comments-container"></div><div id="comment-tools-38680" class="comment-tools"></div><div class="clear"></div><div id="comment-38680-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38783"></span>

<div id="answer-container-38783" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38783-score" class="post-score" title="current number of votes">0</div><span id="post-38783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you looked through <a href="http://wiki.wireshark.org/SampleCaptures">the sample capture collections</a> out there?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Dec '14, 03:57</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-38783" class="comments-container"></div><div id="comment-tools-38783" class="comment-tools"></div><div class="clear"></div><div id="comment-38783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

