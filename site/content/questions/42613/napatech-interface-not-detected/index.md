+++
type = "question"
title = "Napatech Interface Not Detected"
description = '''Hello, I have a server running WireShark 1.12.5 w/ WinPcap 4.1.3 and it has a Napatech NT4E Adapter. When I Look at my interfaces, the only ones available are the Broadcom NIC&#x27;s and not the Napatech. Is there a setting that needs to be enabled or an add-on I need to install? Any help would be greatl...'''
date = "2015-05-22T07:43:00Z"
lastmod = "2015-05-22T09:17:00Z"
weight = 42613
keywords = [ "no-interface", "napatech" ]
aliases = [ "/questions/42613" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Napatech Interface Not Detected](/questions/42613/napatech-interface-not-detected)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42613-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42613-score" class="post-score" title="current number of votes">0</div><span id="post-42613-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have a server running WireShark 1.12.5 w/ WinPcap 4.1.3 and it has a Napatech NT4E Adapter. When I Look at my interfaces, the only ones available are the Broadcom NIC's and not the Napatech. Is there a setting that needs to be enabled or an add-on I need to install? Any help would be greatly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-no-interface" rel="tag" title="see questions tagged &#39;no-interface&#39;">no-interface</span> <span class="post-tag tag-link-napatech" rel="tag" title="see questions tagged &#39;napatech&#39;">napatech</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '15, 07:43</strong></p><img src="https://secure.gravatar.com/avatar/c7ea84d05d4d37233056307350c6c344?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="asofo&#39;s gravatar image" /><p><span>asofo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="asofo has no accepted answers">0%</span></p></div></div><div id="comments-container-42613" class="comments-container"></div><div id="comment-tools-42613" class="comment-tools"></div><div class="clear"></div><div id="comment-42613-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42615"></span>

<div id="answer-container-42615" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42615-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42615-score" class="post-score" title="current number of votes">1</div><span id="post-42615-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>AFAIK the Napatech cards are useless without the driver package, firmware updates and a special version of the libpcap library that can work with the card. Unfortunately, Napatech does not provide any of those to anyone except licensed capture system builders/vendors.</p><p>So if you got a single Napatech card from somewhere (e.g. Ebay) you got yourself a useless devices unless you can find someone who can give you the drivers and the libraries (who probably signed an NDA with Napatech to exactly prevent that kind of thing).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '15, 09:17</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-42615" class="comments-container"></div><div id="comment-tools-42615" class="comment-tools"></div><div class="clear"></div><div id="comment-42615-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

