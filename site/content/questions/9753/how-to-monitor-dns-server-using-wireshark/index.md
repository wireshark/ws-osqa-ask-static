+++
type = "question"
title = "How to monitor DNS server using Wireshark"
description = '''Dear Gurus, How can i monitor DNS ot some other system packets which are consuming BW of my network'''
date = "2012-03-25T23:23:00Z"
lastmod = "2012-03-26T04:08:00Z"
weight = 9753
keywords = [ "packetflow" ]
aliases = [ "/questions/9753" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to monitor DNS server using Wireshark](/questions/9753/how-to-monitor-dns-server-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9753-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9753-score" class="post-score" title="current number of votes">0</div><span id="post-9753-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Gurus, How can i monitor DNS ot some other system packets which are consuming BW of my network</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packetflow" rel="tag" title="see questions tagged &#39;packetflow&#39;">packetflow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '12, 23:23</strong></p><img src="https://secure.gravatar.com/avatar/cd40fae7fb8a8db50e134552610afa6d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ARsa&#39;s gravatar image" /><p><span>ARsa</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ARsa has no accepted answers">0%</span></p></div></div><div id="comments-container-9753" class="comments-container"></div><div id="comment-tools-9753" class="comment-tools"></div><div class="clear"></div><div id="comment-9753-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9762"></span>

<div id="answer-container-9762" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9762-score" class="post-score" title="current number of votes">0</div><span id="post-9762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could start a capture and filter on DNS?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '12, 04:08</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-9762" class="comments-container"></div><div id="comment-tools-9762" class="comment-tools"></div><div class="clear"></div><div id="comment-9762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

