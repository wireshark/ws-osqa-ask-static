+++
type = "question"
title = "Capture HTTP traffic during router configuration and replay traffic on other routers for quick configuration."
description = '''I use a router which allows configuration through a browser using HTTP. Could I capture the HTTP traffic during configuration of one router and then replay it on other routers to generate the same configuration on those routers? I&#x27;m aware that you can capture traffic from Wireshark and replay the re...'''
date = "2016-02-23T17:40:00Z"
lastmod = "2016-02-23T17:40:00Z"
weight = 50453
keywords = [ "http", "tcpdump", "wireshark" ]
aliases = [ "/questions/50453" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture HTTP traffic during router configuration and replay traffic on other routers for quick configuration.](/questions/50453/capture-http-traffic-during-router-configuration-and-replay-traffic-on-other-routers-for-quick-configuration)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50453-score" class="post-score" title="current number of votes">0</div><span id="post-50453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use a router which allows configuration through a browser using HTTP.</p><p>Could I capture the HTTP traffic during configuration of one router and then replay it on other routers to generate the same configuration on those routers?</p><p>I'm aware that you can capture traffic from Wireshark and replay the resulting file using another program. I just want to know if this would or wouldn't work, and why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '16, 17:40</strong></p><img src="https://secure.gravatar.com/avatar/380005e2f07231f0982bb11003dc23a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cjgarett15635&#39;s gravatar image" /><p><span>cjgarett15635</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cjgarett15635 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Feb '16, 17:41</strong> </span></p></div></div><div id="comments-container-50453" class="comments-container"></div><div id="comment-tools-50453" class="comment-tools"></div><div class="clear"></div><div id="comment-50453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

