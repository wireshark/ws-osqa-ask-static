+++
type = "question"
title = "Outlook Freezing"
description = '''Hi, Our outlook has been freezing quite intermittently. And wire shark dumps show us  &quot;No bind info for interface context ID 0&quot; Can someone help me check what the message actually means?'''
date = "2015-04-01T09:27:00Z"
lastmod = "2015-04-02T04:16:00Z"
weight = 41098
keywords = [ "outlook" ]
aliases = [ "/questions/41098" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Outlook Freezing](/questions/41098/outlook-freezing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41098-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41098-score" class="post-score" title="current number of votes">0</div><span id="post-41098-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Our outlook has been freezing quite intermittently. And wire shark dumps show us</p><p>"No bind info for interface context ID 0"</p><p>Can someone help me check what the message actually means?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outlook" rel="tag" title="see questions tagged &#39;outlook&#39;">outlook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/44bf2a6fa3308dcac119f06f185f540d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JaysonP&#39;s gravatar image" /><p><span>JaysonP</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JaysonP has no accepted answers">0%</span></p></div></div><div id="comments-container-41098" class="comments-container"></div><div id="comment-tools-41098" class="comment-tools"></div><div class="clear"></div><div id="comment-41098-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41139"></span>

<div id="answer-container-41139" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41139-score" class="post-score" title="current number of votes">0</div><span id="post-41139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's more a question for a Microsoft networking or Outlook support forum.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '15, 04:16</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-41139" class="comments-container"></div><div id="comment-tools-41139" class="comment-tools"></div><div class="clear"></div><div id="comment-41139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

