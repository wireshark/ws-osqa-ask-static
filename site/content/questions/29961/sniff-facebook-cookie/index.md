+++
type = "question"
title = "Sniff facebook  cookie"
description = '''Hi  i&quot;ve tried the sniffing on facebook and getting the cookie the thing is i dont get any packets that contain it i use the filter http.cookie and i see nothing maybe its not working anymore since facebook is on https no matter what settings you use? everything else is captured ok i see all the pac...'''
date = "2014-02-18T01:50:00Z"
lastmod = "2014-02-18T01:50:00Z"
weight = 29961
keywords = [ "facebook" ]
aliases = [ "/questions/29961" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sniff facebook cookie](/questions/29961/sniff-facebook-cookie)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29961-score" class="post-score" title="current number of votes">0</div><span id="post-29961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i"ve tried the sniffing on facebook and getting the cookie the thing is i dont get any packets that contain it i use the filter http.cookie and i see nothing maybe its not working anymore since facebook is on https no matter what settings you use? everything else is captured ok i see all the packets just not the cookie any idea?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-facebook" rel="tag" title="see questions tagged &#39;facebook&#39;">facebook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '14, 01:50</strong></p><img src="https://secure.gravatar.com/avatar/8763b54b3b4f03a15a26c4440a7d405a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dot&#39;s gravatar image" /><p><span>Dot</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dot has no accepted answers">0%</span></p></div></div><div id="comments-container-29961" class="comments-container"></div><div id="comment-tools-29961" class="comment-tools"></div><div class="clear"></div><div id="comment-29961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

