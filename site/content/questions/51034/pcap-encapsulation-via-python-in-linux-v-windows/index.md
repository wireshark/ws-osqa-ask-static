+++
type = "question"
title = "[closed] pcap encapsulation via python in linux V windows"
description = '''Hi All,   I have a python script which I successfully used to encapsulate a tcp session for the last number of years in the linux environment, it&#x27;s encapsulating by adding the hex equivalent (in linux). When I try to run the same script in windows it fails and appears to be adding the additional enc...'''
date = "2016-03-18T08:32:00Z"
lastmod = "2016-03-18T09:09:00Z"
weight = 51034
keywords = [ "encapsulation", "pytohn" ]
aliases = [ "/questions/51034" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] pcap encapsulation via python in linux V windows](/questions/51034/pcap-encapsulation-via-python-in-linux-v-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51034-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51034-score" class="post-score" title="current number of votes">0</div><span id="post-51034-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, I have a python script which I successfully used to encapsulate a tcp session for the last number of years in the linux environment, it's encapsulating by adding the hex equivalent (in linux). When I try to run the same script in windows it fails and appears to be adding the additional encapsulation by means of an ascii interpretation of the hex , any suggestions</p><p>kind regards</p><p>Seamus</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encapsulation" rel="tag" title="see questions tagged &#39;encapsulation&#39;">encapsulation</span> <span class="post-tag tag-link-pytohn" rel="tag" title="see questions tagged &#39;pytohn&#39;">pytohn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '16, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/e7eda9bac99d31f98d25acb579f2cd57?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mandraker1000&#39;s gravatar image" /><p><span>Mandraker1000</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mandraker1000 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>18 Mar '16, 09:09</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51034" class="comments-container"><span id="51035"></span><div id="comment-51035" class="comment"><div id="post-51035-score" class="comment-score"></div><div class="comment-text"><p>Sorry, this isn't a topic for this Q&amp;A site.</p></div><div id="comment-51035-info" class="comment-info"><span class="comment-age">(18 Mar '16, 09:09)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-51034" class="comment-tools"></div><div class="clear"></div><div id="comment-51034-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 18 Mar '16, 09:09

</div>

</div>

</div>

