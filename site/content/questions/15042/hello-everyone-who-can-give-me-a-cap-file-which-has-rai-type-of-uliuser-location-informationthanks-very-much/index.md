+++
type = "question"
title = "hello everyone ,who can give me a cap file which has RAI type of ULI(USER LOCATION INFORMATION),thanks very much"
description = '''1,hello everyone ,who can give me a cap file which has RAI type of ULI(USER LOCATION INFORMATION) 2,i compose a rai type UTL,but the wireshark can not decode the rac ,who can tell me what&#x27;s wrong with it. the cap file as follow: '''
date = "2012-10-17T00:21:00Z"
lastmod = "2012-10-17T00:21:00Z"
weight = 15042
keywords = [ "compose", "rai" ]
aliases = [ "/questions/15042" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [hello everyone ,who can give me a cap file which has RAI type of ULI(USER LOCATION INFORMATION),thanks very much](/questions/15042/hello-everyone-who-can-give-me-a-cap-file-which-has-rai-type-of-uliuser-location-informationthanks-very-much)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15042-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15042-score" class="post-score" title="current number of votes">0</div><span id="post-15042-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>1,hello everyone ,who can give me a cap file which has RAI type of ULI(USER LOCATION INFORMATION) 2,i compose a rai type UTL,but the wireshark can not decode the rac ,who can tell me what's wrong with it. the cap file as follow: <img src="https://osqa-ask.wireshark.org/upfiles/cap_2.bmp" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compose" rel="tag" title="see questions tagged &#39;compose&#39;">compose</span> <span class="post-tag tag-link-rai" rel="tag" title="see questions tagged &#39;rai&#39;">rai</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '12, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/a5d5176683d8b6b136833b352a34d4bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="optimism1981&#39;s gravatar image" /><p><span>optimism1981</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="optimism1981 has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Oct '12, 00:34</strong> </span></p></div></div><div id="comments-container-15042" class="comments-container"></div><div id="comment-tools-15042" class="comment-tools"></div><div class="clear"></div><div id="comment-15042-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

