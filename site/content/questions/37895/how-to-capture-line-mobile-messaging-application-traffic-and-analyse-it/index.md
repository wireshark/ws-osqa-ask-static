+++
type = "question"
title = "How to capture Line mobile Messaging Application Traffic and analyse it"
description = '''How can I sniff and read my chat messages on Line Messaging application Thanx'''
date = "2014-11-17T01:50:00Z"
lastmod = "2014-11-17T01:50:00Z"
weight = 37895
keywords = [ "capture", "traffic-analysis", "extract", "line", "instant-messaging" ]
aliases = [ "/questions/37895" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture Line mobile Messaging Application Traffic and analyse it](/questions/37895/how-to-capture-line-mobile-messaging-application-traffic-and-analyse-it)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37895-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37895-score" class="post-score" title="current number of votes">0</div><span id="post-37895-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I sniff and read my chat messages on Line Messaging application Thanx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-extract" rel="tag" title="see questions tagged &#39;extract&#39;">extract</span> <span class="post-tag tag-link-line" rel="tag" title="see questions tagged &#39;line&#39;">line</span> <span class="post-tag tag-link-instant-messaging" rel="tag" title="see questions tagged &#39;instant-messaging&#39;">instant-messaging</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '14, 01:50</strong></p><img src="https://secure.gravatar.com/avatar/725aa92cfc7bf87e921e124f6bae69be?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="captin&#39;s gravatar image" /><p><span>captin</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="captin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Nov '14, 11:03</strong> </span></p></div></div><div id="comments-container-37895" class="comments-container"></div><div id="comment-tools-37895" class="comment-tools"></div><div class="clear"></div><div id="comment-37895-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

