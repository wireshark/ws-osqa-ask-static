+++
type = "question"
title = "How to find the latency of RTPS (Real Time Publish-Subscribe) in Wireshark"
description = '''I have two machines that are communicating using RTPS (Real Time Publish-Subscribe) protocol and I want to find latency using Wireshark.But I&#x27;m unable to find latency with the given methods that are available on the internet as in case of TCP protocol. Perhaps the packet architecture is different. I...'''
date = "2016-10-27T01:48:00Z"
lastmod = "2016-10-27T01:48:00Z"
weight = 56730
keywords = [ "delay", "latency", "network", "tcp", "wireshark" ]
aliases = [ "/questions/56730" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to find the latency of RTPS (Real Time Publish-Subscribe) in Wireshark](/questions/56730/how-to-find-the-latency-of-rtps-real-time-publish-subscribe-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56730-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56730-score" class="post-score" title="current number of votes">0</div><span id="post-56730-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have two machines that are communicating using RTPS (Real Time Publish-Subscribe) protocol and I want to find latency using Wireshark.But I'm unable to find latency with the given methods that are available on the internet as in case of TCP protocol. Perhaps the packet architecture is different. I'm getting time in time column as shown in image below but I don't know how to get latency using time column.</p><p><img src="https://scontent.xx.fbcdn.net/v/t34.0-0/p280x280/14886275_1300441723322802_1901861835_n.jpg?_nc_ad=z-m&amp;oh=1657c5fda47650a474d6ecb08ea87f5a&amp;oe=58144BAF" alt="Wireshark Implementation of RTPS" /></p><p>Please help me to figure out latency of RTPS using Wireshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '16, 01:48</strong></p><img src="https://secure.gravatar.com/avatar/1800c7d1b17e7f28fe5c882015e50428?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Faisal%20Sajjad&#39;s gravatar image" /><p><span>Faisal Sajjad</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Faisal Sajjad has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '16, 01:51</strong> </span></p></div></div><div id="comments-container-56730" class="comments-container"></div><div id="comment-tools-56730" class="comment-tools"></div><div class="clear"></div><div id="comment-56730-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

