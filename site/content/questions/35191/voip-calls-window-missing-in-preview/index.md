+++
type = "question"
title = "VoIP Calls window missing in Preview?"
description = '''Hello, I can not find the VoIP Calls window under Statistics as in an earlier release. Where is that located now or do I need to activate some feature maybe? Many thanks, Tobias'''
date = "2014-08-05T02:55:00Z"
lastmod = "2014-08-05T03:20:00Z"
weight = 35191
keywords = [ "voipcalls", "sip" ]
aliases = [ "/questions/35191" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [VoIP Calls window missing in Preview?](/questions/35191/voip-calls-window-missing-in-preview)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35191-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35191-score" class="post-score" title="current number of votes">0</div><span id="post-35191-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I can not find the VoIP Calls window under Statistics as in an earlier release. Where is that located now or do I need to activate some feature maybe?</p><p>Many thanks, Tobias</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '14, 02:55</strong></p><img src="https://secure.gravatar.com/avatar/001b253c18cfe02d57480be7d162ae2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tobias%20Ehlert&#39;s gravatar image" /><p><span>Tobias Ehlert</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tobias Ehlert has no accepted answers">0%</span></p></div></div><div id="comments-container-35191" class="comments-container"></div><div id="comment-tools-35191" class="comment-tools"></div><div class="clear"></div><div id="comment-35191-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35192"></span>

<div id="answer-container-35192" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35192-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35192-score" class="post-score" title="current number of votes">1</div><span id="post-35192-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Tobias Ehlert has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're using the new QT based version - it does not yet have all the features that the GTK version has, so if you're missing anything check the GTK version. The developers will port features one by one, so it may take a while until everything is in the QT version.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '14, 02:57</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-35192" class="comments-container"><span id="35193"></span><div id="comment-35193" class="comment"><div id="post-35193-score" class="comment-score"></div><div class="comment-text"><p>Hi Jasper,</p><p>Thanks for the fast answer! I will then stick to the older version (Qt) since I have all the tools I need in that..</p><p>Many thanks, Tobias</p></div><div id="comment-35193-info" class="comment-info"><span class="comment-age">(05 Aug '14, 03:02)</span> <span class="comment-user userinfo">Tobias Ehlert</span></div></div><span id="35196"></span><div id="comment-35196" class="comment"><div id="post-35196-score" class="comment-score"></div><div class="comment-text"><p>Er, the "older" version is GTK.</p></div><div id="comment-35196-info" class="comment-info"><span class="comment-age">(05 Aug '14, 03:16)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="35197"></span><div id="comment-35197" class="comment"><div id="post-35197-score" class="comment-score"></div><div class="comment-text"><p><span>@grahamb</span>: ...unless you guys pull features out of QT that were already in, which I doubt (but which is still possible if they turned out too problematic I guess) ;-)</p></div><div id="comment-35197-info" class="comment-info"><span class="comment-age">(05 Aug '14, 03:18)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="35198"></span><div id="comment-35198" class="comment"><div id="post-35198-score" class="comment-score"></div><div class="comment-text"><p>I suppose to be truly accurate they are both the same age, they just have a different UI, with one (GTK) currently having more features than the other.</p></div><div id="comment-35198-info" class="comment-info"><span class="comment-age">(05 Aug '14, 03:20)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-35192" class="comment-tools"></div><div class="clear"></div><div id="comment-35192-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

