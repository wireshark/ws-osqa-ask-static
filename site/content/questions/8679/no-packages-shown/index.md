+++
type = "question"
title = "No packages shown"
description = '''Hello. I&#x27;ve tried to use wireshark to capture packages sending and recieving from google chrome. Still I can&#x27;t see anything(I did set capture filter on TCP or UDP and HTTP - both for port 80). What am I doing wrong? Can you help? '''
date = "2012-01-29T06:07:00Z"
lastmod = "2012-01-29T07:03:00Z"
weight = 8679
keywords = [ "chrome", "80", "packages" ]
aliases = [ "/questions/8679" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No packages shown](/questions/8679/no-packages-shown)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8679-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8679-score" class="post-score" title="current number of votes">0</div><span id="post-8679-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. I've tried to use wireshark to capture packages sending and recieving from google chrome. Still I can't see anything(I did set capture filter on TCP or UDP and HTTP - both for port 80).</p><p>What am I doing wrong? Can you help?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-chrome" rel="tag" title="see questions tagged &#39;chrome&#39;">chrome</span> <span class="post-tag tag-link-80" rel="tag" title="see questions tagged &#39;80&#39;">80</span> <span class="post-tag tag-link-packages" rel="tag" title="see questions tagged &#39;packages&#39;">packages</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jan '12, 06:07</strong></p><img src="https://secure.gravatar.com/avatar/3a705773dd431be0aaf32b2a6624c50a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samuel_spring&#39;s gravatar image" /><p><span>samuel_spring</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samuel_spring has no accepted answers">0%</span></p></div></div><div id="comments-container-8679" class="comments-container"><span id="8680"></span><div id="comment-8680" class="comment"><div id="post-8680-score" class="comment-score"></div><div class="comment-text"><p>You're probably capturing on the wrong interface - can you try to capture without any filters, just to see if you get anything at all?</p></div><div id="comment-8680-info" class="comment-info"><span class="comment-age">(29 Jan '12, 06:52)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="8681"></span><div id="comment-8681" class="comment"><div id="post-8681-score" class="comment-score"></div><div class="comment-text"><p>how many adapters do you see when you go to Capture -&gt; Options? And if there is more than one do any of the packets counters increment while you ping or surf?</p><p>(converted to a comment since there is no answer included)</p></div><div id="comment-8681-info" class="comment-info"><span class="comment-age">(29 Jan '12, 07:03)</span> <span class="comment-user userinfo">thetechfirm</span></div></div></div><div id="comment-tools-8679" class="comment-tools"></div><div class="clear"></div><div id="comment-8679-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

