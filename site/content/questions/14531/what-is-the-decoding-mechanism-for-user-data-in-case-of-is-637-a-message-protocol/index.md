+++
type = "question"
title = "what is the decoding mechanism for User Data in case of IS-637-A message protocol"
description = '''Hi, My UE is sending message in form of IS-637-A protocol. Its a SIP request:Message. Wireshark is able to decode the user data. I want to know the decoding machanism  so that I will be able to decode that mesasge without help of wireshark. I am devloping an application which will decode the message...'''
date = "2012-09-25T22:57:00Z"
lastmod = "2012-09-26T05:24:00Z"
weight = 14531
keywords = [ "decoding" ]
aliases = [ "/questions/14531" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what is the decoding mechanism for User Data in case of IS-637-A message protocol](/questions/14531/what-is-the-decoding-mechanism-for-user-data-in-case-of-is-637-a-message-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14531-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14531-score" class="post-score" title="current number of votes">0</div><span id="post-14531-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>My UE is sending message in form of IS-637-A protocol. Its a SIP request:Message. Wireshark is able to decode the user data. I want to know the decoding machanism so that I will be able to decode that mesasge without help of wireshark. I am devloping an application which will decode the message and come out with user data.</p><p>Need your help. Thanks in advance in any suggestion</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decoding" rel="tag" title="see questions tagged &#39;decoding&#39;">decoding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '12, 22:57</strong></p><img src="https://secure.gravatar.com/avatar/e99db177bb153fcca69bfb6efb2b2f98?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="raman&#39;s gravatar image" /><p><span>raman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="raman has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Sep '12, 05:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-14531" class="comments-container"></div><div id="comment-tools-14531" class="comment-tools"></div><div class="clear"></div><div id="comment-14531-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14539"></span>

<div id="answer-container-14539" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14539-score" class="post-score" title="current number of votes">0</div><span id="post-14539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look in packet-ansi_637.c it registers for application/vnd.3gpp2.sms which I assume this is and the content from the SIP message gets translated in dissect_ansi_637_trans_app() which calls dissect_ansi_637_trans().</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Sep '12, 05:04</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-14539" class="comments-container"><span id="14540"></span><div id="comment-14540" class="comment"><div id="post-14540-score" class="comment-score"></div><div class="comment-text"><p>A link to the file (from the development version of Wireshark) is <a href="http://anonsvn.wireshark.org/viewvc/trunk/epan/dissectors/packet-ansi_637.c?revision=45017&amp;view=markup">here</a>. Note that the code is GPL licensed so your own application must use a compatible license.</p></div><div id="comment-14540-info" class="comment-info"><span class="comment-age">(26 Sep '12, 05:24)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-14539" class="comment-tools"></div><div class="clear"></div><div id="comment-14539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

