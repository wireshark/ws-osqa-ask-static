+++
type = "question"
title = "SA editor UI disappeared at preference of the ESP protocol"
description = '''I just downloaded the latest stable wireshark source code, compiled and installed, when I try to decode IPSec ESP packets, I found there&#x27;s no UI to configure the SA under the ESP protocol preferences, anybody noticed that? or is it just a problem of my build? PS: the version I downloaded is 1.12.3'''
date = "2015-01-27T04:19:00Z"
lastmod = "2015-01-27T04:51:00Z"
weight = 39436
keywords = [ "sa", "esp" ]
aliases = [ "/questions/39436" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SA editor UI disappeared at preference of the ESP protocol](/questions/39436/sa-editor-ui-disappeared-at-preference-of-the-esp-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39436-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39436-score" class="post-score" title="current number of votes">0</div><span id="post-39436-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just downloaded the latest stable wireshark source code, compiled and installed, when I try to decode IPSec ESP packets, I found there's no UI to configure the SA under the ESP protocol preferences, anybody noticed that? or is it just a problem of my build?</p><p>PS: the version I downloaded is 1.12.3</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sa" rel="tag" title="see questions tagged &#39;sa&#39;">sa</span> <span class="post-tag tag-link-esp" rel="tag" title="see questions tagged &#39;esp&#39;">esp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '15, 04:19</strong></p><img src="https://secure.gravatar.com/avatar/6535877de270eac71c394ebbe2c5dae5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Crs&#39;s gravatar image" /><p><span>Crs</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Crs has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jan '15, 04:27</strong> </span></p></div></div><div id="comments-container-39436" class="comments-container"></div><div id="comment-tools-39436" class="comment-tools"></div><div class="clear"></div><div id="comment-39436-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39437"></span>

<div id="answer-container-39437" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39437-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39437-score" class="post-score" title="current number of votes">0</div><span id="post-39437-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Found my desktop didn't have libgcrypt-dev installed, automatically disabled ESP decryption during build.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '15, 04:51</strong></p><img src="https://secure.gravatar.com/avatar/6535877de270eac71c394ebbe2c5dae5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Crs&#39;s gravatar image" /><p><span>Crs</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Crs has no accepted answers">0%</span></p></div></div><div id="comments-container-39437" class="comments-container"></div><div id="comment-tools-39437" class="comment-tools"></div><div class="clear"></div><div id="comment-39437-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

