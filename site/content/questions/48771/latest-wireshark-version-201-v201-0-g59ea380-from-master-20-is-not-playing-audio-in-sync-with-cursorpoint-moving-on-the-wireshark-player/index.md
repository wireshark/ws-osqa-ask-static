+++
type = "question"
title = "latest wireshark Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0) is not playing audio in sync with cursor/point moving on the wireshark player?"
description = '''The latest version is not sync audio, audio graph and the pointer/cursor while playing it. it makes difficult the conclude if the issue is happening in audio.'''
date = "2015-12-31T08:32:00Z"
lastmod = "2015-12-31T08:32:00Z"
weight = 48771
keywords = [ "sync" ]
aliases = [ "/questions/48771" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [latest wireshark Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0) is not playing audio in sync with cursor/point moving on the wireshark player?](/questions/48771/latest-wireshark-version-201-v201-0-g59ea380-from-master-20-is-not-playing-audio-in-sync-with-cursorpoint-moving-on-the-wireshark-player)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48771-score" class="post-score" title="current number of votes">0</div><span id="post-48771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The latest version is not sync audio, audio graph and the pointer/cursor while playing it. it makes difficult the conclude if the issue is happening in audio.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sync" rel="tag" title="see questions tagged &#39;sync&#39;">sync</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '15, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/e620b9e53eb581620df0c58e476ee584?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Padmanathan%20Sagayaraj&#39;s gravatar image" /><p><span>Padmanathan ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Padmanathan Sagayaraj has no accepted answers">0%</span></p></div></div><div id="comments-container-48771" class="comments-container"></div><div id="comment-tools-48771" class="comment-tools"></div><div class="clear"></div><div id="comment-48771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

