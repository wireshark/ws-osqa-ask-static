+++
type = "question"
title = "Wireshark to stay on screen."
description = '''Good morning, My set up is a Macbook pro with Maverick with the main screen and one monitor through DVI connection. How can I get the Wireshark app to stay on the aux monitor without having to use hot corners? Thanks in advance.'''
date = "2014-05-26T06:44:00Z"
lastmod = "2014-05-26T08:36:00Z"
weight = 33080
keywords = [ "monitor.", "with", "alternate", "problems", "wireshark" ]
aliases = [ "/questions/33080" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark to stay on screen.](/questions/33080/wireshark-to-stay-on-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33080-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33080-score" class="post-score" title="current number of votes">0</div><span id="post-33080-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good morning, My set up is a Macbook pro with Maverick with the main screen and one monitor through DVI connection. How can I get the Wireshark app to stay on the aux monitor without having to use hot corners? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitor." rel="tag" title="see questions tagged &#39;monitor.&#39;">monitor.</span> <span class="post-tag tag-link-with" rel="tag" title="see questions tagged &#39;with&#39;">with</span> <span class="post-tag tag-link-alternate" rel="tag" title="see questions tagged &#39;alternate&#39;">alternate</span> <span class="post-tag tag-link-problems" rel="tag" title="see questions tagged &#39;problems&#39;">problems</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 May '14, 06:44</strong></p><img src="https://secure.gravatar.com/avatar/242e7e7d7d037bfadcaae4964a4ff775?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="entrophy&#39;s gravatar image" /><p><span>entrophy</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="entrophy has no accepted answers">0%</span></p></div></div><div id="comments-container-33080" class="comments-container"></div><div id="comment-tools-33080" class="comment-tools"></div><div class="clear"></div><div id="comment-33080-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33081"></span>

<div id="answer-container-33081" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33081-score" class="post-score" title="current number of votes">0</div><span id="post-33081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you checked if in your Preferences the "Save window position" checkmark is active? Go to Edit -&gt; Preferences -&gt; User Interface to find it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 May '14, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-33081" class="comments-container"><span id="33082"></span><div id="comment-33082" class="comment"><div id="post-33082-score" class="comment-score"></div><div class="comment-text"><p>Do not see that option available in the Mac addition that I am using.</p></div><div id="comment-33082-info" class="comment-info"><span class="comment-age">(26 May '14, 06:58)</span> <span class="comment-user userinfo">entrophy</span></div></div><span id="33090"></span><div id="comment-33090" class="comment"><div id="post-33090-score" class="comment-score"></div><div class="comment-text"><p>sorry, I do not have a Mac so I can't check ;-)</p></div><div id="comment-33090-info" class="comment-info"><span class="comment-age">(26 May '14, 08:36)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-33081" class="comment-tools"></div><div class="clear"></div><div id="comment-33081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

