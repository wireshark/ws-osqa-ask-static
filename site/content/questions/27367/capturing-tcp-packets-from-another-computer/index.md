+++
type = "question"
title = "Capturing TCP Packets from another computer"
description = '''I&#x27;m having a lot of trouble capturing TCP packets from another computer. I can see my TCP packets but not the packets from the other computers on my network. I have promiscuous mode on and feel like everything is up. Please don&#x27;t send me a link to the page that shows the different switches. I&#x27;ve got...'''
date = "2013-11-25T15:55:00Z"
lastmod = "2013-11-26T01:53:00Z"
weight = 27367
keywords = [ "switch", "packets", "tcp" ]
aliases = [ "/questions/27367" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing TCP Packets from another computer](/questions/27367/capturing-tcp-packets-from-another-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27367-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27367-score" class="post-score" title="current number of votes">0</div><span id="post-27367-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm having a lot of trouble capturing TCP packets from another computer. I can see my TCP packets but not the packets from the other computers on my network. I have promiscuous mode on and feel like everything is up. Please don't send me a link to the page that shows the different switches. I've gotten that so many times but I don't quite understand it. People send it to me but I don't understand it. If someone could help me with this problem I would appreciate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-switch" rel="tag" title="see questions tagged &#39;switch&#39;">switch</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '13, 15:55</strong></p><img src="https://secure.gravatar.com/avatar/e9f275152d7fc32f8e59c4421b5e32f8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Brad6547884&#39;s gravatar image" /><p><span>Brad6547884</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Brad6547884 has no accepted answers">0%</span></p></div></div><div id="comments-container-27367" class="comments-container"></div><div id="comment-tools-27367" class="comment-tools"></div><div class="clear"></div><div id="comment-27367-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27370"></span>

<div id="answer-container-27370" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27370-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27370-score" class="post-score" title="current number of votes">1</div><span id="post-27370-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For the packets you're looking for, are they going from those other computers toward your own?</p><p>Keep in mind that if you're capturing packets from your computer, you are only going to see packets that are being sent to or from your own network card. With Ethernet switches (the switches you're referring to?), traffic can go from one computer to another directly without all other computers on the network seeing the packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '13, 19:11</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-27370" class="comments-container"><span id="27383"></span><div id="comment-27383" class="comment"><div id="post-27383-score" class="comment-score"></div><div class="comment-text"><p>Also have a look at the <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Ethernet Capture Setup</a> page on the wiki.</p></div><div id="comment-27383-info" class="comment-info"><span class="comment-age">(26 Nov '13, 01:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-27370" class="comment-tools"></div><div class="clear"></div><div id="comment-27370-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

