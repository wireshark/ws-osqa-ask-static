+++
type = "question"
title = "wi-fi direct measurement"
description = '''Is it possible to measure wi-fi direct packet using the wireshark. If so, how to measure and analyze ??'''
date = "2012-03-30T03:15:00Z"
lastmod = "2012-03-30T03:28:00Z"
weight = 9858
keywords = [ "wi-fi", "direct" ]
aliases = [ "/questions/9858" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wi-fi direct measurement](/questions/9858/wi-fi-direct-measurement)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9858-score" class="post-score" title="current number of votes">0</div><span id="post-9858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to measure wi-fi direct packet using the wireshark. If so, how to measure and analyze ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wi-fi" rel="tag" title="see questions tagged &#39;wi-fi&#39;">wi-fi</span> <span class="post-tag tag-link-direct" rel="tag" title="see questions tagged &#39;direct&#39;">direct</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Mar '12, 03:15</strong></p><img src="https://secure.gravatar.com/avatar/295eb08094302aed1a4ee88ac60fa89f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="novice10&#39;s gravatar image" /><p><span>novice10</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="novice10 has no accepted answers">0%</span></p></div></div><div id="comments-container-9858" class="comments-container"></div><div id="comment-tools-9858" class="comment-tools"></div><div class="clear"></div><div id="comment-9858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9859"></span>

<div id="answer-container-9859" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9859-score" class="post-score" title="current number of votes">0</div><span id="post-9859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on capturing 802.11 traffic <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">HERE</a>. Note that you'll likely have difficulties doing this on Windows.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '12, 03:28</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-9859" class="comments-container"></div><div id="comment-tools-9859" class="comment-tools"></div><div class="clear"></div><div id="comment-9859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

