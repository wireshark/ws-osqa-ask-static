+++
type = "question"
title = "How do I convert an &quot;answer&quot; to a comment"
description = '''Folks sometimes post &quot;answers&quot; to their own questions when they should really be comments to either the original question or an actual answer. How can posters of the incorrect &quot;answer&quot; or editors turn the &quot;answer&quot; into a comment.'''
date = "2011-10-17T23:17:00Z"
lastmod = "2014-05-29T09:45:00Z"
weight = 6941
keywords = [ "answer", "comment", "meta", "editor", "ask.wireshark.org" ]
aliases = [ "/questions/6941" ]
osqa_answers = 3
osqa_accepted = true
+++

<div class="headNormal">

# [How do I convert an "answer" to a comment](/questions/6941/how-do-i-convert-an-answer-to-a-comment)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6941-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6941-score" class="post-score" title="current number of votes">1</div><span id="post-6941-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Folks sometimes post "answers" to their own questions when they should really be comments to either the original question or an actual answer.</p><p>How can posters of the incorrect "answer" or editors turn the "answer" into a comment.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-answer" rel="tag" title="see questions tagged &#39;answer&#39;">answer</span> <span class="post-tag tag-link-comment" rel="tag" title="see questions tagged &#39;comment&#39;">comment</span> <span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span> <span class="post-tag tag-link-editor" rel="tag" title="see questions tagged &#39;editor&#39;">editor</span> <span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '11, 23:17</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Oct '11, 07:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-6941" class="comments-container"></div><div id="comment-tools-6941" class="comment-tools"></div><div class="clear"></div><div id="comment-6941-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="6942"></span>

<div id="answer-container-6942" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6942-score" class="post-score" title="current number of votes">1</div><span id="post-6942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="grahamb has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Moderators can do that, after being appointed by Gerald.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '11, 23:43</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-6942" class="comments-container"><span id="6998"></span><div id="comment-6998" class="comment"><div id="post-6998-score" class="comment-score"></div><div class="comment-text"><p>You can also do this if you have &gt;= 2000 reputation points.</p></div><div id="comment-6998-info" class="comment-info"><span class="comment-age">(19 Oct '11, 15:20)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-6942" class="comment-tools"></div><div class="clear"></div><div id="comment-6942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="33164"></span>

<div id="answer-container-33164" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33164-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33164-score" class="post-score" title="current number of votes">0</div><span id="post-33164-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes finally figured it out, nothing on FAQ to explain that but thanks anyway.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '14, 05:34</strong></p><img src="https://secure.gravatar.com/avatar/7d265b804c113ade8c794311b7272681?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="itteche&#39;s gravatar image" /><p><span>itteche</span><br />
<span class="score" title="20 reputation points">20</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="itteche has no accepted answers">0%</span></p></div></div><div id="comments-container-33164" class="comments-container"><span id="33165"></span><div id="comment-33165" class="comment"><div id="post-33165-score" class="comment-score"></div><div class="comment-text"><p>If I follow correctly, the FAQ entry I was pointing you to was the fact that this is a Q&amp;A forum, not a time descending web based forum, and you should not be posting comments as "answers".</p><p>The moderators aim to help by converting answers to comments, sometimes it's difficult to determine which answer the comment is referring to. After multiple "violations" a helpful note is added as a comment :-)</p></div><div id="comment-33165-info" class="comment-info"><span class="comment-age">(29 May '14, 06:14)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="33177"></span><div id="comment-33177" class="comment"><div id="post-33177-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately, the option listed below for adding a comment does not work. Everytime I post something it states, Sorry, but akismet thinks your comment is spam". So if I need to discuss something with the person suggesting something, how should I proceed in getting a comment to them?</p></div><div id="comment-33177-info" class="comment-info"><span class="comment-age">(29 May '14, 09:45)</span> <span class="comment-user userinfo">itteche</span></div></div></div><div id="comment-tools-33164" class="comment-tools"></div><div class="clear"></div><div id="comment-33164-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="6984"></span>

<div id="answer-container-6984" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6984-score" class="post-score" title="current number of votes">-1</div><span id="post-6984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>people with &lt;&gt; symbol beside their names can do that !!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '11, 00:30</strong></p><img src="https://secure.gravatar.com/avatar/264adc05b644c1ab2d670b4773a12392?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flashkicker&#39;s gravatar image" /><p><span>flashkicker</span><br />
<span class="score" title="109 reputation points">109</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="19 badges"><span class="silver">●</span><span class="badgecount">19</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flashkicker has 5 accepted answers">41%</span></p></div></div><div id="comments-container-6984" class="comments-container"></div><div id="comment-tools-6984" class="comment-tools"></div><div class="clear"></div><div id="comment-6984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

