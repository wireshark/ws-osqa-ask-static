+++
type = "question"
title = "Bluetooth Packet tweaking for negative testing"
description = '''Is there any option available in Wireshark to alter a specific Bluetooth RFCOMM packet and to send this altered packet to another Bluetooth Device? mainly for Bluetooth RFCOMM negative testing.'''
date = "2013-09-30T05:10:00Z"
lastmod = "2013-09-30T07:58:00Z"
weight = 25372
keywords = [ "bluetooth" ]
aliases = [ "/questions/25372" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bluetooth Packet tweaking for negative testing](/questions/25372/bluetooth-packet-tweaking-for-negative-testing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25372-score" class="post-score" title="current number of votes">0</div><span id="post-25372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any option available in Wireshark to alter a specific Bluetooth RFCOMM packet and to send this altered packet to another Bluetooth Device?</p><p>mainly for Bluetooth RFCOMM negative testing.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '13, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/b59a87b44be99d3fc3ee22be2d7f520e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ash&#39;s gravatar image" /><p><span>Ash</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ash has no accepted answers">0%</span></p></div></div><div id="comments-container-25372" class="comments-container"><span id="25387"></span><div id="comment-25387" class="comment"><div id="post-25387-score" class="comment-score"></div><div class="comment-text"><p>what you need is Ubertooth</p><blockquote><p><a href="http://www.sharebrained.com/ubertooth/">http://www.sharebrained.com/ubertooth/</a></p></blockquote></div><div id="comment-25387-info" class="comment-info"><span class="comment-age">(30 Sep '13, 07:58)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-25372" class="comment-tools"></div><div class="clear"></div><div id="comment-25372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25384"></span>

<div id="answer-container-25384" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25384-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25384-score" class="post-score" title="current number of votes">1</div><span id="post-25384-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In brief, no.<br />
Wireshark is a packet <em>analyzer</em>, not a packet injection tool. You might try <a href="http://fiddler2.com/features/web-debugging" title="fiddler">fiddler</a>, straight up netcat, or some other tool, since Wireshark has no capability to send packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Sep '13, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span> </br></p></div></div><div id="comments-container-25384" class="comments-container"></div><div id="comment-tools-25384" class="comment-tools"></div><div class="clear"></div><div id="comment-25384-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

