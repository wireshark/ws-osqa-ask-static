+++
type = "question"
title = "How can I use Wireshark to find Line App ports?"
description = '''Hello Folks. How are you? I want to find which ports used by Line App, How can I use Wireshark to find it? Cheers.'''
date = "2014-11-26T11:14:00Z"
lastmod = "2014-11-30T08:31:00Z"
weight = 38180
keywords = [ "and", "line", "app", "wireshark" ]
aliases = [ "/questions/38180" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I use Wireshark to find Line App ports?](/questions/38180/how-can-i-use-wireshark-to-find-line-app-ports)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38180-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38180-score" class="post-score" title="current number of votes">0</div><span id="post-38180-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Folks. How are you? I want to find which ports used by Line App, How can I use Wireshark to find it?</p><p>Cheers.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-and" rel="tag" title="see questions tagged &#39;and&#39;">and</span> <span class="post-tag tag-link-line" rel="tag" title="see questions tagged &#39;line&#39;">line</span> <span class="post-tag tag-link-app" rel="tag" title="see questions tagged &#39;app&#39;">app</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '14, 11:14</strong></p><img src="https://secure.gravatar.com/avatar/1f1d393403ea997213960ee852d8f897?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hack3rcon&#39;s gravatar image" /><p><span>hack3rcon</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hack3rcon has no accepted answers">0%</span></p></div></div><div id="comments-container-38180" class="comments-container"><span id="38235"></span><div id="comment-38235" class="comment"><div id="post-38235-score" class="comment-score"></div><div class="comment-text"><p>is it possible?</p></div><div id="comment-38235-info" class="comment-info"><span class="comment-age">(29 Nov '14, 07:35)</span> <span class="comment-user userinfo">hack3rcon</span></div></div><span id="38236"></span><div id="comment-38236" class="comment"><div id="post-38236-score" class="comment-score"></div><div class="comment-text"><p>Is it possible to provide a link to whatever "Line App" is so folks might be able to better understand your question?</p></div><div id="comment-38236-info" class="comment-info"><span class="comment-age">(29 Nov '14, 09:13)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="38242"></span><div id="comment-38242" class="comment"><div id="post-38242-score" class="comment-score"></div><div class="comment-text"><p>Thank you. I mean is "http://line.me/en/"</p></div><div id="comment-38242-info" class="comment-info"><span class="comment-age">(30 Nov '14, 08:31)</span> <span class="comment-user userinfo">hack3rcon</span></div></div></div><div id="comment-tools-38180" class="comment-tools"></div><div class="clear"></div><div id="comment-38180-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

