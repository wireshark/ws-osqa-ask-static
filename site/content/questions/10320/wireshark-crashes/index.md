+++
type = "question"
title = "Wireshark crashes"
description = '''After V1.62 if I install the software, All OK but when I go to capture options and delete the NIC card manually to enter &quot;rpcap://IP address/eth2&quot; the program crashes (I cannot enter the rpcap it crashes straight after I try and delete the NIC card). If I reinstall V1.62 all works OK but as I said a...'''
date = "2012-04-19T21:57:00Z"
lastmod = "2012-04-27T07:38:00Z"
weight = 10320
keywords = [ "crashes" ]
aliases = [ "/questions/10320" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark crashes](/questions/10320/wireshark-crashes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10320-score" class="post-score" title="current number of votes">0</div><span id="post-10320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After V1.62 if I install the software, All OK but when I go to capture options and delete the NIC card manually to enter "rpcap://IP address/eth2" the program crashes (I cannot enter the rpcap it crashes straight after I try and delete the NIC card). If I reinstall V1.62 all works OK but as I said after that version they all crash if I try and delete the NIC card manually (to enter my own settings). My PC is running windows 7 32 bit.Any suggestions as to why and how this can be fixed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-crashes" rel="tag" title="see questions tagged &#39;crashes&#39;">crashes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '12, 21:57</strong></p><img src="https://secure.gravatar.com/avatar/a5502cb1af5ccb9f9a3a0374684bca72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MartinGSQ&#39;s gravatar image" /><p><span>MartinGSQ</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MartinGSQ has no accepted answers">0%</span></p></div></div><div id="comments-container-10320" class="comments-container"></div><div id="comment-tools-10320" class="comment-tools"></div><div class="clear"></div><div id="comment-10320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10484"></span>

<div id="answer-container-10484" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10484-score" class="post-score" title="current number of votes">0</div><span id="post-10484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might want to try version 1.6.7--there was an rpcap-related bug fix in that version. If that doesn't help, try the development version (1.7.1 would be good). If that doesn't help, please file a bug report on Wireshark's <a href="https://bugs.wireshark.org">bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Apr '12, 07:38</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-10484" class="comments-container"></div><div id="comment-tools-10484" class="comment-tools"></div><div class="clear"></div><div id="comment-10484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

