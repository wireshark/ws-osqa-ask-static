+++
type = "question"
title = "retransmission and out of order problem"
description = '''Hi Could you help me where could be the problem with my connection? link to the screenshot Thank you szkot'''
date = "2014-09-22T03:01:00Z"
lastmod = "2014-09-25T06:17:00Z"
weight = 36502
keywords = [ "retransmission", "out-of-order" ]
aliases = [ "/questions/36502" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [retransmission and out of order problem](/questions/36502/retransmission-and-out-of-order-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36502-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36502-score" class="post-score" title="current number of votes">0</div><span id="post-36502-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>Could you help me where could be the problem with my connection?</p><p><a href="http://postimg.org/image/n2nguh4u1/">link to the screenshot</a></p><p>Thank you szkot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-retransmission" rel="tag" title="see questions tagged &#39;retransmission&#39;">retransmission</span> <span class="post-tag tag-link-out-of-order" rel="tag" title="see questions tagged &#39;out-of-order&#39;">out-of-order</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '14, 03:01</strong></p><img src="https://secure.gravatar.com/avatar/7e762111142e86f232cf340b15dea7a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="szkotkr&#39;s gravatar image" /><p><span>szkotkr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="szkotkr has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Sep '14, 06:24</strong> </span></p></div></div><div id="comments-container-36502" class="comments-container"></div><div id="comment-tools-36502" class="comment-tools"></div><div class="clear"></div><div id="comment-36502-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36597"></span>

<div id="answer-container-36597" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36597-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36597-score" class="post-score" title="current number of votes">0</div><span id="post-36597-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Given our limited visibility of your infrastructure, not really. It looks like the trace was taken on (or closer to) .17, and all that I can conclude from the trace is there's is packet loss somewhere upstream of .17. You'll need to move the trace somewhere upstream, closer to .217 to isolate where the problem is being introduced.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '14, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/32272e9efae0156b7a71e9b634428d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smp&#39;s gravatar image" /><p><span>smp</span><br />
<span class="score" title="39 reputation points">39</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smp has no accepted answers">0%</span></p></div></div><div id="comments-container-36597" class="comments-container"></div><div id="comment-tools-36597" class="comment-tools"></div><div class="clear"></div><div id="comment-36597-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

