+++
type = "question"
title = "Packet List scrolling issue when packet is highlighted"
description = '''I have noticed in the latest couple of builds in 2.2.x on OXS (not sure about windows version) that when I click/select on a packet in the Packet List pane I can not scroll up or down beyond the highlighted or selected packet. When I do the Packet List pane just refreshes back to that selected packe...'''
date = "2017-03-08T02:12:00Z"
lastmod = "2017-03-08T02:48:00Z"
weight = 59910
keywords = [ "with", "selected", "scrolling", "packet" ]
aliases = [ "/questions/59910" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet List scrolling issue when packet is highlighted](/questions/59910/packet-list-scrolling-issue-when-packet-is-highlighted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59910-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59910-score" class="post-score" title="current number of votes">0</div><span id="post-59910-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have noticed in the latest couple of builds in 2.2.x on OXS (not sure about windows version) that when I click/select on a packet in the Packet List pane I can not scroll up or down beyond the highlighted or selected packet. When I do the Packet List pane just refreshes back to that selected packets so I can not scroll up or down to look for other information. I know I use to be able to do this without issue. This is not in a live packet capture.</p><p>Is there some setting for this?</p><p>Thanks John.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-with" rel="tag" title="see questions tagged &#39;with&#39;">with</span> <span class="post-tag tag-link-selected" rel="tag" title="see questions tagged &#39;selected&#39;">selected</span> <span class="post-tag tag-link-scrolling" rel="tag" title="see questions tagged &#39;scrolling&#39;">scrolling</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '17, 02:12</strong></p><img src="https://secure.gravatar.com/avatar/ac47106c2bc89d743fff1da90d1e1237?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mycrew143&#39;s gravatar image" /><p><span>mycrew143</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mycrew143 has no accepted answers">0%</span></p></div></div><div id="comments-container-59910" class="comments-container"></div><div id="comment-tools-59910" class="comment-tools"></div><div class="clear"></div><div id="comment-59910-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59911"></span>

<div id="answer-container-59911" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59911-score" class="post-score" title="current number of votes">0</div><span id="post-59911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there some setting for this?</p></blockquote><p>It's the "please fix the bug" setting. :-)</p><p>I.e., it's not supposed to <em>ever</em> work the way you say it's working; this is a bug, so there's no setting to control it. Please report it on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Mar '17, 02:48</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-59911" class="comments-container"></div><div id="comment-tools-59911" class="comment-tools"></div><div class="clear"></div><div id="comment-59911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

