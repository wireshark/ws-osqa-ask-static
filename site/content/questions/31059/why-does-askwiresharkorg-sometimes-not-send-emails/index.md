+++
type = "question"
title = "Why does ask.wireshark.org sometimes not send emails?"
description = '''I have a couple issues with getting email notifications from ask.wireshark.org, and I&#x27;m wondering if it&#x27;s expected behavior or a bug:  I usually get emails for tags I&#x27;ve subscribed to (ie, am &quot;interested in&quot;). But if I re-tag someone&#x27;s question to an interesting tag, it claims to automatically subsc...'''
date = "2014-03-21T10:04:00Z"
lastmod = "2014-03-21T10:04:00Z"
weight = 31059
keywords = [ "ask.wireshark.org" ]
aliases = [ "/questions/31059" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why does ask.wireshark.org sometimes not send emails?](/questions/31059/why-does-askwiresharkorg-sometimes-not-send-emails)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31059-score" class="post-score" title="current number of votes">0</div><span id="post-31059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a couple issues with getting email notifications from ask.wireshark.org, and I'm wondering if it's expected behavior or a bug:</p><ol><li>I usually get emails for tags I've subscribed to (ie, am "interested in"). But if I re-tag someone's question to an interesting tag, it claims to automatically subscribe me but no email notifications are sent when there are updates to the question/comments until I provide an answer.</li><li>Possible related to (1) above, but if I comment on a question (as opposed to answering it), and then the other user comments as well, I don't get a notification of that either. I.e., either it does not auto-subscribe me for only posting a comment to a question, or else auto-subscribing isn't working for that case either.</li></ol><p>(and no, the emails aren't in my spam folder)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '14, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div></div><div id="comments-container-31059" class="comments-container"></div><div id="comment-tools-31059" class="comment-tools"></div><div class="clear"></div><div id="comment-31059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

