+++
type = "question"
title = "Mac OSX Install Problems"
description = '''I&#x27;m getting an error message when I reboot after having installed wireshark on my snow leopard system and followed all the installation directions including the sudo commands listed in previous question - http://ask.wireshark.org/questions/2405/mac-osx-installation. I&#x27;m also not seeing any available...'''
date = "2011-03-17T17:28:00Z"
lastmod = "2011-08-21T22:49:00Z"
weight = 2912
keywords = [ "osx", "mac", "chmodbpf", "install", "error" ]
aliases = [ "/questions/2912" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Mac OSX Install Problems](/questions/2912/mac-osx-install-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2912-score" class="post-score" title="current number of votes">0</div><span id="post-2912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm getting an error message when I reboot after having installed wireshark on my snow leopard system and followed all the installation directions including the sudo commands listed in previous question - http://ask.wireshark.org/questions/2405/mac-osx-installation. I'm also not seeing any available capture interfaces when I start wireshark.</p><p>The error message is Insecure Startup Item Disabled "/Library/StartupItems/ChmodBPF" has not been started because it does not have the proper security settings.</p><p>I'm an admin user and believe I've followed all the instructions to a "T". I'm new to OSX so perhaps I've missed something.<br />
</p><p>Thanks,</p><p>Mark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-chmodbpf" rel="tag" title="see questions tagged &#39;chmodbpf&#39;">chmodbpf</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Mar '11, 17:28</strong></p><img src="https://secure.gravatar.com/avatar/86981bb609f55c22f0627c2b1251ed5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="markdmc&#39;s gravatar image" /><p><span>markdmc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="markdmc has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-2912" class="comments-container"></div><div id="comment-tools-2912" class="comment-tools"></div><div class="clear"></div><div id="comment-2912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2914"></span>

<div id="answer-container-2914" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2914-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2914-score" class="post-score" title="current number of votes">0</div><span id="post-2914-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>After further digging in the Q&amp;A I found <a href="http://ask.wireshark.org/questions/2829/capturing-with-wireshark-on-mac-os-1066">another thread on the same subject</a> that solved my issue.</p><p>Thanks to GeonJay and Guy Harris.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Mar '11, 17:45</strong></p><img src="https://secure.gravatar.com/avatar/86981bb609f55c22f0627c2b1251ed5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="markdmc&#39;s gravatar image" /><p><span>markdmc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="markdmc has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Mar '11, 17:50</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-2914" class="comments-container"></div><div id="comment-tools-2914" class="comment-tools"></div><div class="clear"></div><div id="comment-2914-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="5794"></span>

<div id="answer-container-5794" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5794-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5794-score" class="post-score" title="current number of votes">0</div><span id="post-5794-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>how do i see the wireshark packets on lion ?????? i completely install it but unable to see the SIP packet in Wireshark of Mac OSX Lion</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Aug '11, 22:49</strong></p><img src="https://secure.gravatar.com/avatar/235d700fb7ab9cca5975e5f7eb55bff2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="amit&#39;s gravatar image" /><p><span>amit</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="amit has no accepted answers">0%</span></p></div></div><div id="comments-container-5794" class="comments-container"></div><div id="comment-tools-5794" class="comment-tools"></div><div class="clear"></div><div id="comment-5794-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

