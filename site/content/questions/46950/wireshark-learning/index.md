+++
type = "question"
title = "Wireshark Learning"
description = '''Looking to see what book is recommended for learning reading packet traces within wireshark. I am most interested in VoIP related info. Looking at the Wireshark 101 or troubleshooting with wreshark.... whats recommended?'''
date = "2015-10-26T12:06:00Z"
lastmod = "2015-10-26T13:39:00Z"
weight = 46950
keywords = [ "books" ]
aliases = [ "/questions/46950" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Learning](/questions/46950/wireshark-learning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46950-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46950-score" class="post-score" title="current number of votes">0</div><span id="post-46950-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Looking to see what book is recommended for learning reading packet traces within wireshark. I am most interested in VoIP related info. Looking at the Wireshark 101 or troubleshooting with wreshark.... whats recommended?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-books" rel="tag" title="see questions tagged &#39;books&#39;">books</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '15, 12:06</strong></p><img src="https://secure.gravatar.com/avatar/37994555214ffc73e84b17888f3e5327?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TonySmith1986&#39;s gravatar image" /><p><span>TonySmith1986</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TonySmith1986 has no accepted answers">0%</span></p></div></div><div id="comments-container-46950" class="comments-container"></div><div id="comment-tools-46950" class="comment-tools"></div><div class="clear"></div><div id="comment-46950-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46957"></span>

<div id="answer-container-46957" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46957-score" class="post-score" title="current number of votes">0</div><span id="post-46957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you see the following questions?</p><blockquote><p><a href="https://ask.wireshark.org/questions/11370/a-good-way-to-learn-how-to-use-wireshark">https://ask.wireshark.org/questions/11370/a-good-way-to-learn-how-to-use-wireshark</a><br />
<a href="https://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark">https://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark</a><br />
<a href="https://ask.wireshark.org/questions/20848/need-the-most-experienced-person-here-to-recommend-on-which-is-the-best-source-for-learning-how-to-read-packets">https://ask.wireshark.org/questions/20848/need-the-most-experienced-person-here-to-recommend-on-which-is-the-best-source-for-learning-how-to-read-packets</a><br />
<a href="https://ask.wireshark.org/questions/10166/online-tutorial-for-reading-packet-capture-files">https://ask.wireshark.org/questions/10166/online-tutorial-for-reading-packet-capture-files</a><br />
</p></blockquote><p>and the following online resources?</p><blockquote><p><a href="https://supportforums.cisco.com/video/11927911/how-use-wireshark-voip-troubleshooting">https://supportforums.cisco.com/video/11927911/how-use-wireshark-voip-troubleshooting</a><br />
<a href="http://www.enterprisenetworkingplanet.com/unified_communications/troubleshooting-common-sip-problems-with-wireshark.html">http://www.enterprisenetworkingplanet.com/unified_communications/troubleshooting-common-sip-problems-with-wireshark.html</a><br />
<a href="http://www.linuxjournal.com/article/9398">http://www.linuxjournal.com/article/9398</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Oct '15, 13:39</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Oct '15, 14:31</strong> </span></p></div></div><div id="comments-container-46957" class="comments-container"></div><div id="comment-tools-46957" class="comment-tools"></div><div class="clear"></div><div id="comment-46957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

