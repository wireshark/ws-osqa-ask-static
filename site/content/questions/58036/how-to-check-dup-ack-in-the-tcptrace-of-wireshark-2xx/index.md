+++
type = "question"
title = "how to check DUP ACK in the tcptrace of wireshark 2.x.x"
description = '''Why in 2.x.x, it can&#x27;t be seen the TCP DUP ACK in the ACK line via time sequence-tcptrace as before version 1.x.x? （there is a &#x27;T&#x27; charact in the ACK line before）'''
date = "2016-12-13T03:25:00Z"
lastmod = "2016-12-13T05:30:00Z"
weight = 58036
keywords = [ "dup", "ack", "tcptrace" ]
aliases = [ "/questions/58036" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [how to check DUP ACK in the tcptrace of wireshark 2.x.x](/questions/58036/how-to-check-dup-ack-in-the-tcptrace-of-wireshark-2xx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58036-score" class="post-score" title="current number of votes">0</div><span id="post-58036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why in 2.x.x, it can't be seen the TCP DUP ACK in the ACK line via time sequence-tcptrace as before version 1.x.x? （there is a 'T' charact in the ACK line before）</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dup" rel="tag" title="see questions tagged &#39;dup&#39;">dup</span> <span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-tcptrace" rel="tag" title="see questions tagged &#39;tcptrace&#39;">tcptrace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '16, 03:25</strong></p><img src="https://secure.gravatar.com/avatar/54b3b359535c4363d67c1cc7b96c0799?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shoppingye&#39;s gravatar image" /><p><span>shoppingye</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shoppingye has no accepted answers">0%</span></p></div></div><div id="comments-container-58036" class="comments-container"></div><div id="comment-tools-58036" class="comment-tools"></div><div class="clear"></div><div id="comment-58036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58037"></span>

<div id="answer-container-58037" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58037-score" class="post-score" title="current number of votes">0</div><span id="post-58037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="shoppingye has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is feature request for that in Wireshark Bug Tracker:</p><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12009">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12009</a></p><p>I'm waiting for that feature too.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Dec '16, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/1e22670f8d643ca08d658b80a6782932?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Packet_vlad&#39;s gravatar image" /><p><span>Packet_vlad</span><br />
<span class="score" title="436 reputation points">436</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Packet_vlad has 5 accepted answers">20%</span></p></div></div><div id="comments-container-58037" class="comments-container"><span id="58041"></span><div id="comment-58041" class="comment"><div id="post-58041-score" class="comment-score"></div><div class="comment-text"><p>Thank you!</p></div><div id="comment-58041-info" class="comment-info"><span class="comment-age">(13 Dec '16, 05:30)</span> <span class="comment-user userinfo">shoppingye</span></div></div></div><div id="comment-tools-58037" class="comment-tools"></div><div class="clear"></div><div id="comment-58037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

