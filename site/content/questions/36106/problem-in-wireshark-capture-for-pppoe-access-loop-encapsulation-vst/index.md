+++
type = "question"
title = "problem in wireshark capture for PPPoE Access Loop Encapsulation VST"
description = '''wireshark is decoding Access Loop Encapsulation tag in the PPPoE vendor specific tag information as expected except while displaying. There are 3 bytes in that tag. 1st byte for datalink info, 2nd byte for encaps1 and 3rd is for encaps2. But while displaying wireshark showing both 2nd and 3rd byte a...'''
date = "2014-09-09T04:58:00Z"
lastmod = "2014-09-09T04:58:00Z"
weight = 36106
keywords = [ "access", "specific", "tag", "vendor", "pppoe" ]
aliases = [ "/questions/36106" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [problem in wireshark capture for PPPoE Access Loop Encapsulation VST](/questions/36106/problem-in-wireshark-capture-for-pppoe-access-loop-encapsulation-vst)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36106-score" class="post-score" title="current number of votes">0</div><span id="post-36106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>wireshark is decoding Access Loop Encapsulation tag in the PPPoE vendor specific tag information as expected except while displaying. There are 3 bytes in that tag. 1st byte for datalink info, 2nd byte for encaps1 and 3rd is for encaps2. But while displaying wireshark showing both 2nd and 3rd byte as l2-encaps1.</p><p>For more information please refer DSL forum TR-101 and section 3.9.5.</p><p>please confirm whether the problem is in wireshark display.</p><p>I am using version 1.12.0</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-access" rel="tag" title="see questions tagged &#39;access&#39;">access</span> <span class="post-tag tag-link-specific" rel="tag" title="see questions tagged &#39;specific&#39;">specific</span> <span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span> <span class="post-tag tag-link-vendor" rel="tag" title="see questions tagged &#39;vendor&#39;">vendor</span> <span class="post-tag tag-link-pppoe" rel="tag" title="see questions tagged &#39;pppoe&#39;">pppoe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '14, 04:58</strong></p><img src="https://secure.gravatar.com/avatar/fa4418644c86313f21c0e50a97e34d96?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Balamurugan%20M&#39;s gravatar image" /><p><span>Balamurugan M</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Balamurugan M has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Sep '14, 05:04</strong> </span></p></div></div><div id="comments-container-36106" class="comments-container"></div><div id="comment-tools-36106" class="comment-tools"></div><div class="clear"></div><div id="comment-36106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

