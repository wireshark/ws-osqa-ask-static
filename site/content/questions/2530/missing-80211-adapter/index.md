+++
type = "question"
title = "Missing 802.11 adapter"
description = '''Greetings, I just installed Wireshark 1.4.3 on Windows 7 64 bit and my Intel Centrino wireless adapter is not showing up in my interface list. Anything special that I have to do to get the 802.11 side of things up and running? The wired GigE interface that is built into the laptop is working just fi...'''
date = "2011-02-23T12:04:00Z"
lastmod = "2011-02-28T06:25:00Z"
weight = 2530
keywords = [ "adapters", "wifi", "802.11" ]
aliases = [ "/questions/2530" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Missing 802.11 adapter](/questions/2530/missing-80211-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2530-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2530-score" class="post-score" title="current number of votes">0</div><span id="post-2530-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings,</p><p>I just installed Wireshark 1.4.3 on Windows 7 64 bit and my Intel Centrino wireless adapter is not showing up in my interface list.</p><p>Anything special that I have to do to get the 802.11 side of things up and running?</p><p>The wired GigE interface that is built into the laptop is working just fine, but not the wireless interface.</p><p>Thanks, Dan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adapters" rel="tag" title="see questions tagged &#39;adapters&#39;">adapters</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '11, 12:04</strong></p><img src="https://secure.gravatar.com/avatar/09287d52ba1b717ead90e5097496383a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dlamay&#39;s gravatar image" /><p><span>dlamay</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dlamay has no accepted answers">0%</span></p></div></div><div id="comments-container-2530" class="comments-container"></div><div id="comment-tools-2530" class="comment-tools"></div><div class="clear"></div><div id="comment-2530-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2539"></span>

<div id="answer-container-2539" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2539-score" class="post-score" title="current number of votes">0</div><span id="post-2539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Capturing wireless traffic on Windows can be problematical,</p><p>See http://wiki.wireshark.org/CaptureSetup/WLAN (especially the Windows paragraph) for some information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '11, 13:30</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-2539" class="comments-container"></div><div id="comment-tools-2539" class="comment-tools"></div><div class="clear"></div><div id="comment-2539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="2581"></span>

<div id="answer-container-2581" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2581-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2581-score" class="post-score" title="current number of votes">0</div><span id="post-2581-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Under Windows 7 wireless NICs in many cases show up as "Microsoft" Interface Name... Try finding that one - apart from that you might want to take a look at the many other question regarding wireless capturing here at ask.wireshark before doing whatever you wanted to do ;)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '11, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div></div><div id="comments-container-2581" class="comments-container"></div><div id="comment-tools-2581" class="comment-tools"></div><div class="clear"></div><div id="comment-2581-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

