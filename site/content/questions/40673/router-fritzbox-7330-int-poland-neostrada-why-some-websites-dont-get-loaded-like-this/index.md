+++
type = "question"
title = "router fritzbox 7330 int @ poland / neostrada: why some websites dont get loaded (like this)?"
description = '''Hi Folks, with the new Router some websites dont get loaded in the browser. What could be the reason? How can I proceed to figure out? Here is an example (only 5,7 kB / 34 pakets!): [executable download removed] Thanks in advance!'''
date = "2015-03-18T13:38:00Z"
lastmod = "2015-03-19T02:22:00Z"
weight = 40673
keywords = [ "website", "poland", "neostrada", "fritzbox", "websites" ]
aliases = [ "/questions/40673" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [router fritzbox 7330 int @ poland / neostrada: why some websites dont get loaded (like this)?](/questions/40673/router-fritzbox-7330-int-poland-neostrada-why-some-websites-dont-get-loaded-like-this)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40673-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40673-score" class="post-score" title="current number of votes">0</div><span id="post-40673-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Folks,</p><p>with the new Router some websites dont get loaded in the browser.</p><p>What could be the reason? How can I proceed to figure out?</p><p>Here is an example (only 5,7 kB / 34 pakets!): <strong>[executable download removed]</strong></p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website" rel="tag" title="see questions tagged &#39;website&#39;">website</span> <span class="post-tag tag-link-poland" rel="tag" title="see questions tagged &#39;poland&#39;">poland</span> <span class="post-tag tag-link-neostrada" rel="tag" title="see questions tagged &#39;neostrada&#39;">neostrada</span> <span class="post-tag tag-link-fritzbox" rel="tag" title="see questions tagged &#39;fritzbox&#39;">fritzbox</span> <span class="post-tag tag-link-websites" rel="tag" title="see questions tagged &#39;websites&#39;">websites</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '15, 13:38</strong></p><img src="https://secure.gravatar.com/avatar/bced0dff4042b22e438a09048feadbf3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="voiplover&#39;s gravatar image" /><p><span>voiplover</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="voiplover has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Mar '15, 02:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-40673" class="comments-container"><span id="40678"></span><div id="comment-40678" class="comment"><div id="post-40678-score" class="comment-score"></div><div class="comment-text"><p>Can you please upload an unfiltered pcap.</p></div><div id="comment-40678-info" class="comment-info"><span class="comment-age">(19 Mar '15, 01:27)</span> <span class="comment-user userinfo">Roland</span></div></div><span id="40680"></span><div id="comment-40680" class="comment"><div id="post-40680-score" class="comment-score"></div><div class="comment-text"><p>If you want to share capture files please use services like <a href="http://cloudshark.org">CloudShark</a>, i.s.o. services which try to push you installers and other kinds of crap.</p></div><div id="comment-40680-info" class="comment-info"><span class="comment-age">(19 Mar '15, 02:22)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-40673" class="comment-tools"></div><div class="clear"></div><div id="comment-40673-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

