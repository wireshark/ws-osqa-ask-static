+++
type = "question"
title = "analyze https without private key"
description = '''Hi,  I have considered a rather curious problem. It is well known that sometimes you can go slow web browsing various reasons beyond the contracted ISP or network devices that we have, such as bad configuration, pipeline, keepalive, the server does micropauses, etc. With http web traffic (unencrypte...'''
date = "2014-07-26T03:05:00Z"
lastmod = "2014-07-27T14:01:00Z"
weight = 34924
keywords = [ "private-key", "time-request", "https" ]
aliases = [ "/questions/34924" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [analyze https without private key](/questions/34924/analyze-https-without-private-key)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34924-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34924-score" class="post-score" title="current number of votes">0</div><span id="post-34924-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have considered a rather curious problem. It is well known that sometimes you can go slow web browsing various reasons beyond the contracted ISP or network devices that we have, such as bad configuration, pipeline, keepalive, the server does micropauses, etc. With http web traffic (unencrypted) is very easy to analyze the connections with wireshark and other tools allow us to analyze the behavior of the server and client. My question maybe some know, or know some tool to estimate certain characteristics of a secure http (https) connection, such as the time taken between the request-response, duration of response, etc.. Does anyone know of any tools? thank you very much</p><ul><li>Cansado293</li></ul><p>PD: without the private key to decrypt the connection</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-private-key" rel="tag" title="see questions tagged &#39;private-key&#39;">private-key</span> <span class="post-tag tag-link-time-request" rel="tag" title="see questions tagged &#39;time-request&#39;">time-request</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '14, 03:05</strong></p><img src="https://secure.gravatar.com/avatar/1755f4baee4c2c555287ee3527c7bc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cansado2930&#39;s gravatar image" /><p><span>Cansado2930</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cansado2930 has no accepted answers">0%</span></p></div></div><div id="comments-container-34924" class="comments-container"></div><div id="comment-tools-34924" class="comment-tools"></div><div class="clear"></div><div id="comment-34924-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34932"></span>

<div id="answer-container-34932" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34932-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34932-score" class="post-score" title="current number of votes">0</div><span id="post-34932-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please read the following questions/answers.</p><p><a href="http://www.root9.net/2012/11/ssl-decryption-with-wireshark-private.html">http://www.root9.net/2012/11/ssl-decryption-with-wireshark-private.html</a><br />
<a href="http://ask.wireshark.org/questions/10730/how-to-config-master-key-and-session-id-in-wireshark">http://ask.wireshark.org/questions/10730/how-to-config-master-key-and-session-id-in-wireshark</a><br />
<a href="http://ask.wireshark.org/questions/4229/follow-ssl-stream-using-master-key-and-session-id">http://ask.wireshark.org/questions/4229/follow-ssl-stream-using-master-key-and-session-id</a><br />
</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jul '14, 14:01</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-34932" class="comments-container"></div><div id="comment-tools-34932" class="comment-tools"></div><div class="clear"></div><div id="comment-34932-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

