+++
type = "question"
title = "HTTP connect method"
description = '''I am working to implement web proxy for HTTP tunnel. I am looking for sample HTTP connect method header sent by web proxy to the server. And the 200 OK that the web proxy will validate before allowing SSL hello client packet to the server. Please let me know the above details and if possible provide...'''
date = "2011-10-08T09:10:00Z"
lastmod = "2011-10-08T09:10:00Z"
weight = 6807
keywords = [ "tunnel", "http" ]
aliases = [ "/questions/6807" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HTTP connect method](/questions/6807/http-connect-method)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6807-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6807-score" class="post-score" title="current number of votes">1</div><span id="post-6807-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working to implement web proxy for HTTP tunnel. I am looking for sample HTTP connect method header sent by web proxy to the server. And the 200 OK that the web proxy will validate before allowing SSL hello client packet to the server.</p><p>Please let me know the above details and if possible provide me a sample wireshark capture to see HTTP connect and corresponding 200 ok of it. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tunnel" rel="tag" title="see questions tagged &#39;tunnel&#39;">tunnel</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '11, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/e0c92308149e4c4378fc24d5948f1c76?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TheNomad&#39;s gravatar image" /><p><span>TheNomad</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TheNomad has no accepted answers">0%</span></p></div></div><div id="comments-container-6807" class="comments-container"></div><div id="comment-tools-6807" class="comment-tools"></div><div class="clear"></div><div id="comment-6807-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

