+++
type = "question"
title = "Extract files using tshark to save locally"
description = '''Is there a way to use TSHARK to extract files so they can be stored in a directory? I know how to do it with the GUI.... Thanks'''
date = "2013-10-21T18:02:00Z"
lastmod = "2013-10-22T21:32:00Z"
weight = 26266
keywords = [ "files", "tshark" ]
aliases = [ "/questions/26266" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Extract files using tshark to save locally](/questions/26266/extract-files-using-tshark-to-save-locally)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26266-score" class="post-score" title="current number of votes">0</div><span id="post-26266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to use TSHARK to extract files so they can be stored in a directory? I know how to do it with the GUI....</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '13, 18:02</strong></p><img src="https://secure.gravatar.com/avatar/155c2f7434fc5482a9ccc80583f601fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mlow1223&#39;s gravatar image" /><p><span>mlow1223</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mlow1223 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Oct '13, 18:03</strong> </span></p></div></div><div id="comments-container-26266" class="comments-container"></div><div id="comment-tools-26266" class="comment-tools"></div><div class="clear"></div><div id="comment-26266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26309"></span>

<div id="answer-container-26309" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26309-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26309-score" class="post-score" title="current number of votes">1</div><span id="post-26309-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't believe this is current something that can be done through tshark, though it would be great if it could! Your best bet might be filing a feature request over at bugs.wireshark.org. The developers look there for these kinds of requests. If i'm wrong, someone please correct me as this would be a really nice feature.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '13, 21:32</strong></p><img src="https://secure.gravatar.com/avatar/365cfc3c62b61b2ed219b5d146e8ad3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zachad&#39;s gravatar image" /><p><span>zachad</span><br />
<span class="score" title="331 reputation points">331</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zachad has 3 accepted answers">21%</span></p></div></div><div id="comments-container-26309" class="comments-container"></div><div id="comment-tools-26309" class="comment-tools"></div><div class="clear"></div><div id="comment-26309-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

