+++
type = "question"
title = "RTP player not able to play"
description = '''How can i play rtp packet with payload type dynamic 96? I googled the feature but first time only i can play the packet. After second time there were some bug console window and it shows &quot;WARN Dissector bug, Protoclo H264 in packet 226: packet-h264.c:521: failed assertion &quot;DISECTOR_ASSERT_NOT_REACHE...'''
date = "2012-06-29T09:18:00Z"
lastmod = "2012-07-02T11:08:00Z"
weight = 12325
keywords = [ "rtp" ]
aliases = [ "/questions/12325" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTP player not able to play](/questions/12325/rtp-player-not-able-to-play)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12325-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12325-score" class="post-score" title="current number of votes">0</div><span id="post-12325-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i play rtp packet with payload type dynamic 96? I googled the feature but first time only i can play the packet. After second time there were some bug console window and it shows "WARN Dissector bug, Protoclo H264 in packet 226: packet-h264.c:521: failed assertion "DISECTOR_ASSERT_NOT_REACHED""</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '12, 09:18</strong></p><img src="https://secure.gravatar.com/avatar/68076da459f877605522ab7b487ef461?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mamun&#39;s gravatar image" /><p><span>Mamun</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mamun has no accepted answers">0%</span></p></div></div><div id="comments-container-12325" class="comments-container"></div><div id="comment-tools-12325" class="comment-tools"></div><div class="clear"></div><div id="comment-12325-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12378"></span>

<div id="answer-container-12378" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12378-score" class="post-score" title="current number of votes">0</div><span id="post-12378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Either your capture file is damaged, or there is a bug in the dissector. Please file a bug report at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a> and add enough sample data (capture file) to recreate the error message.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jul '12, 11:08</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-12378" class="comments-container"></div><div id="comment-tools-12378" class="comment-tools"></div><div class="clear"></div><div id="comment-12378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

