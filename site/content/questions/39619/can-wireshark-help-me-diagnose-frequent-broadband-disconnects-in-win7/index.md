+++
type = "question"
title = "Can Wireshark help me diagnose frequent broadband disconnects in Win7?"
description = '''This is a newbie question. I run Win7 Ultimate and for months a reboot takes up to 5 mins + to connect to the net. It also disconnects randomly throughout the day and eventually reconnects on its own. If not Wireshark, what do you recommend I use to find this mind-numbing sucker? I use a Netgear Mod...'''
date = "2015-02-03T16:03:00Z"
lastmod = "2015-02-03T16:03:00Z"
weight = 39619
keywords = [ "windows7", "netgear", "disconnects" ]
aliases = [ "/questions/39619" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark help me diagnose frequent broadband disconnects in Win7?](/questions/39619/can-wireshark-help-me-diagnose-frequent-broadband-disconnects-in-win7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39619-score" class="post-score" title="current number of votes">0</div><span id="post-39619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This is a newbie question. I run Win7 Ultimate and for months a reboot takes up to 5 mins + to connect to the net. It also disconnects randomly throughout the day and eventually reconnects on its own. If not Wireshark, what do you recommend I use to find this mind-numbing sucker? I use a Netgear Modem/Router and have been through everything I can think of.</p><p>Many thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-netgear" rel="tag" title="see questions tagged &#39;netgear&#39;">netgear</span> <span class="post-tag tag-link-disconnects" rel="tag" title="see questions tagged &#39;disconnects&#39;">disconnects</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '15, 16:03</strong></p><img src="https://secure.gravatar.com/avatar/22c90c7717dd9bb9d0938a170fcaa1be?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gigasquid&#39;s gravatar image" /><p><span>gigasquid</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gigasquid has no accepted answers">0%</span></p></div></div><div id="comments-container-39619" class="comments-container"></div><div id="comment-tools-39619" class="comment-tools"></div><div class="clear"></div><div id="comment-39619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

