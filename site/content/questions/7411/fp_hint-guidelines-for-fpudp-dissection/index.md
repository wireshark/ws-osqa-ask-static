+++
type = "question"
title = "FP_Hint guidelines for FP/UDP dissection"
description = '''Can anyone share example of &quot;fp_hint&quot; modified pcap file? I&#x27;m desperately trying to apply all available FP/MAC/RLC dissecting machinery, which seems to depend on info appended by fp_hint. A short example will hopefully put me on-track.'''
date = "2011-11-14T03:46:00Z"
lastmod = "2011-11-16T07:33:00Z"
weight = 7411
keywords = [ "fp", "umts", "mac", "rlc", "heuristics" ]
aliases = [ "/questions/7411" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [FP\_Hint guidelines for FP/UDP dissection](/questions/7411/fp_hint-guidelines-for-fpudp-dissection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7411-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7411-score" class="post-score" title="current number of votes">0</div><span id="post-7411-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone share example of "fp_hint" modified pcap file? I'm desperately trying to apply all available FP/MAC/RLC dissecting machinery, which seems to depend on info appended by fp_hint. A short example will hopefully put me on-track.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span> <span class="post-tag tag-link-umts" rel="tag" title="see questions tagged &#39;umts&#39;">umts</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-rlc" rel="tag" title="see questions tagged &#39;rlc&#39;">rlc</span> <span class="post-tag tag-link-heuristics" rel="tag" title="see questions tagged &#39;heuristics&#39;">heuristics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '11, 03:46</strong></p><img src="https://secure.gravatar.com/avatar/b5a1393e4a31be8ec0a7e6d94d12282b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="exbungee&#39;s gravatar image" /><p><span>exbungee</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="exbungee has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Nov '11, 23:03</strong> </span></p></div></div><div id="comments-container-7411" class="comments-container"><span id="7473"></span><div id="comment-7473" class="comment"><div id="post-7473-score" class="comment-score"></div><div class="comment-text"><p>can someone provide an example of structure that needs to prepended to UDP packet carrying FP payload? Please specify the location in Eth frame to place fhis hint.</p></div><div id="comment-7473-info" class="comment-info"><span class="comment-age">(16 Nov '11, 07:33)</span> <span class="comment-user userinfo">exbungee</span></div></div></div><div id="comment-tools-7411" class="comment-tools"></div><div class="clear"></div><div id="comment-7411-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

