+++
type = "question"
title = "Capture Android WLAN traffic"
description = '''Hello! I need to capture WLAN traffic on an Android tablet. I&#x27;m running Wireshark on a Windows 7 laptop. Can I capture this traffic using an AirPcap adapter, or is there a better solution? '''
date = "2013-10-29T07:41:00Z"
lastmod = "2013-11-10T22:56:00Z"
weight = 26512
keywords = [ "android" ]
aliases = [ "/questions/26512" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Android WLAN traffic](/questions/26512/capture-android-wlan-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26512-score" class="post-score" title="current number of votes">0</div><span id="post-26512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! I need to capture WLAN traffic on an Android tablet. I'm running Wireshark on a Windows 7 laptop. Can I capture this traffic using an AirPcap adapter, or is there a better solution?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '13, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/85d9a7c9886a0f145341e9b7882b21b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bluskies58&#39;s gravatar image" /><p><span>bluskies58</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bluskies58 has no accepted answers">0%</span></p></div></div><div id="comments-container-26512" class="comments-container"></div><div id="comment-tools-26512" class="comment-tools"></div><div class="clear"></div><div id="comment-26512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="26636"></span>

<div id="answer-container-26636" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26636-score" class="post-score" title="current number of votes">0</div><span id="post-26636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can indeed capture this traffic when using an AirPcap adapter. You can also boot your laptop into a linux live distribution (like <a href="http://www.kali.org/">Kali linux</a>) to capture wlan traffic in monitor mode.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '13, 03:30</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-26636" class="comments-container"></div><div id="comment-tools-26636" class="comment-tools"></div><div class="clear"></div><div id="comment-26636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="26832"></span>

<div id="answer-container-26832" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26832-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26832-score" class="post-score" title="current number of votes">0</div><span id="post-26832-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>try using Tpacketcapture available free of Android play store,this does not need rooting. Although i am no sure if it can capture the WLAN management traffic</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '13, 22:56</strong></p><img src="https://secure.gravatar.com/avatar/3e5e9d76a54debaa630d909e37048da8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deepacket&#39;s gravatar image" /><p><span>deepacket</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deepacket has no accepted answers">0%</span></p></div></div><div id="comments-container-26832" class="comments-container"></div><div id="comment-tools-26832" class="comment-tools"></div><div class="clear"></div><div id="comment-26832-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

