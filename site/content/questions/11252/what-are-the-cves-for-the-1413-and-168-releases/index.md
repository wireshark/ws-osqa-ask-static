+++
type = "question"
title = "What are the CVEs for the 1.4.13 and 1.6.8 releases?"
description = '''What are the CVEs for the 1.4.13 and 1.6.8 releases? In particular for the wnpa-sec-2012-08, wnpa-sec-2012-09 and wnpa-sec-2012-10 advisories for security fixes?'''
date = "2012-05-23T05:30:00Z"
lastmod = "2012-05-23T12:15:00Z"
weight = 11252
keywords = [ "security", "cve" ]
aliases = [ "/questions/11252" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [What are the CVEs for the 1.4.13 and 1.6.8 releases?](/questions/11252/what-are-the-cves-for-the-1413-and-168-releases)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11252-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11252-score" class="post-score" title="current number of votes">0</div><span id="post-11252-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What are the CVEs for the <a href="http://www.wireshark.org/docs/relnotes/wireshark-1.4.13.html">1.4.13</a> and <a href="http://www.wireshark.org/docs/relnotes/wireshark-1.6.8.html">1.6.8</a> releases? In particular for the <a href="http://www.wireshark.org/security/wnpa-sec-2012-08.html">wnpa-sec-2012-08</a>, <a href="http://www.wireshark.org/security/wnpa-sec-2012-09.html">wnpa-sec-2012-09</a> and <a href="http://www.wireshark.org/security/wnpa-sec-2012-10.html">wnpa-sec-2012-10</a> advisories for security fixes?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-cve" rel="tag" title="see questions tagged &#39;cve&#39;">cve</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '12, 05:30</strong></p><img src="https://secure.gravatar.com/avatar/802ba86ab03110fb142eef689aa16533?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andreasstieger&#39;s gravatar image" /><p><span>andreasstieger</span><br />
<span class="score" title="2 reputation points">2</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andreasstieger has no accepted answers">0%</span></p></div></div><div id="comments-container-11252" class="comments-container"></div><div id="comment-tools-11252" class="comment-tools"></div><div class="clear"></div><div id="comment-11252-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="11263"></span>

<div id="answer-container-11263" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11263-score" class="post-score" title="current number of votes">0</div><span id="post-11263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="andreasstieger has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://seclists.org/oss-sec/2012/q2/387">Be patient</a>, young padawan.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 May '12, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-11263" class="comments-container"><span id="11264"></span><div id="comment-11264" class="comment"><div id="post-11264-score" class="comment-score"></div><div class="comment-text"><p>Thanks, just need the info, hold the rest.</p></div><div id="comment-11264-info" class="comment-info"><span class="comment-age">(23 May '12, 09:01)</span> <span class="comment-user userinfo">andreasstieger</span></div></div></div><div id="comment-tools-11263" class="comment-tools"></div><div class="clear"></div><div id="comment-11263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="11290"></span>

<div id="answer-container-11290" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11290-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11290-score" class="post-score" title="current number of votes">0</div><span id="post-11290-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>wnpa-sec-2012-08 / CVE-2012-2392 wnpa-sec-2012-09 / CVE-2012-2393 wnpa-sec-2012-10 / CVE-2012-2394</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 May '12, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/802ba86ab03110fb142eef689aa16533?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andreasstieger&#39;s gravatar image" /><p><span>andreasstieger</span><br />
<span class="score" title="2 reputation points">2</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andreasstieger has no accepted answers">0%</span></p></div></div><div id="comments-container-11290" class="comments-container"></div><div id="comment-tools-11290" class="comment-tools"></div><div class="clear"></div><div id="comment-11290-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

