+++
type = "question"
title = "wireshark legacy"
description = '''how can I download wireshark legacy?!  I downloaded twice but it wasn&#x27;t wireshark legacy, it was different! please anyone help me, thank you'''
date = "2017-10-15T06:15:00Z"
lastmod = "2017-10-15T08:51:00Z"
weight = 63908
keywords = [ "download" ]
aliases = [ "/questions/63908" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [wireshark legacy](/questions/63908/wireshark-legacy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63908-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63908-score" class="post-score" title="current number of votes">0</div><span id="post-63908-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can I download wireshark legacy?! I downloaded twice but it wasn't wireshark legacy, it was different! please anyone help me, thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '17, 06:15</strong></p><img src="https://secure.gravatar.com/avatar/ac2968faaf33a0c3d7274096dfd540ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Asma&#39;s gravatar image" /><p><span>Asma</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Asma has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '17, 06:19</strong> </span></p></div></div><div id="comments-container-63908" class="comments-container"></div><div id="comment-tools-63908" class="comment-tools"></div><div class="clear"></div><div id="comment-63908-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63909"></span>

<div id="answer-container-63909" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63909-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63909-score" class="post-score" title="current number of votes">0</div><span id="post-63909-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Asma has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need to check mark the "Wireshark 1" box in the installer for Wireshark to install the "Legacy" version (assuming you're talking about the Wireshark 2.4.2 Installer for Windows)</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Installer.png" alt="Wireshark Installer" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '17, 07:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '17, 07:07</strong> </span></p></div></div><div id="comments-container-63909" class="comments-container"><span id="63910"></span><div id="comment-63910" class="comment"><div id="post-63910-score" class="comment-score"></div><div class="comment-text"><p>THANK YOU SO MUCH Jasper</p></div><div id="comment-63910-info" class="comment-info"><span class="comment-age">(15 Oct '17, 08:51)</span> <span class="comment-user userinfo">Asma</span></div></div></div><div id="comment-tools-63909" class="comment-tools"></div><div class="clear"></div><div id="comment-63909-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

