+++
type = "question"
title = "Set Headers Placed in Invite Packet?"
description = '''I formed the sip servlet request, for this request i created setHeader, in wireshark Invite packet I verified the set header but there was no content named as header.  how to verify the formed setHeader in Invite Packet?'''
date = "2012-11-20T21:52:00Z"
lastmod = "2012-11-21T04:25:00Z"
weight = 16138
keywords = [ "headers", "set" ]
aliases = [ "/questions/16138" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Set Headers Placed in Invite Packet?](/questions/16138/set-headers-placed-in-invite-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16138-score" class="post-score" title="current number of votes">0</div><span id="post-16138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I formed the sip servlet request, for this request i created setHeader, in wireshark Invite packet I verified the set header but there was no content named as header.</p><p>how to verify the formed setHeader in Invite Packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-headers" rel="tag" title="see questions tagged &#39;headers&#39;">headers</span> <span class="post-tag tag-link-set" rel="tag" title="see questions tagged &#39;set&#39;">set</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Nov '12, 21:52</strong></p><img src="https://secure.gravatar.com/avatar/2fe684987f884c48e0d8da71d75a8662?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20babu&#39;s gravatar image" /><p><span>Dinesh babu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh babu has no accepted answers">0%</span></p></div></div><div id="comments-container-16138" class="comments-container"><span id="16152"></span><div id="comment-16152" class="comment"><div id="post-16152-score" class="comment-score"></div><div class="comment-text"><p>Is this a Wireshark question or a question regarding the SIP library you use?</p></div><div id="comment-16152-info" class="comment-info"><span class="comment-age">(21 Nov '12, 04:20)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="16153"></span><div id="comment-16153" class="comment"><div id="post-16153-score" class="comment-score"></div><div class="comment-text"><p>We use the SIP with in the sip request we form the setheader. from wireshark how to identify the setheader tag?</p><p>Is this wireshark question - Jaap</p></div><div id="comment-16153-info" class="comment-info"><span class="comment-age">(21 Nov '12, 04:25)</span> <span class="comment-user userinfo">Dinesh babu</span></div></div></div><div id="comment-tools-16138" class="comment-tools"></div><div class="clear"></div><div id="comment-16138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

