+++
type = "question"
title = "Endpoint Traffic"
description = '''Hi,  can a TCP/UDP traffic route from one endpoint as indicated in wireshark (statistics, endpoints) to another endpoint/s? thank you. newbie'''
date = "2015-03-25T18:43:00Z"
lastmod = "2015-04-06T17:56:00Z"
weight = 40871
keywords = [ "endpoint" ]
aliases = [ "/questions/40871" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Endpoint Traffic](/questions/40871/endpoint-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40871-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40871-score" class="post-score" title="current number of votes">0</div><span id="post-40871-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>can a TCP/UDP traffic route from one endpoint as indicated in wireshark (statistics, endpoints) to another endpoint/s?</p><p>thank you. newbie</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-endpoint" rel="tag" title="see questions tagged &#39;endpoint&#39;">endpoint</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '15, 18:43</strong></p><img src="https://secure.gravatar.com/avatar/2412d237bf0b26b4123ffdb7b258da75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="throatyssin&#39;s gravatar image" /><p><span>throatyssin</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="throatyssin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Mar '15, 02:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-40871" class="comments-container"><span id="40878"></span><div id="comment-40878" class="comment"><div id="post-40878-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-40878-info" class="comment-info"><span class="comment-age">(26 Mar '15, 02:48)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="40879"></span><div id="comment-40879" class="comment"><div id="post-40879-score" class="comment-score"></div><div class="comment-text"><p>and please add more details to the body of the question, as I don't understand what you are asking for?</p></div><div id="comment-40879-info" class="comment-info"><span class="comment-age">(26 Mar '15, 03:40)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="41238"></span><div id="comment-41238" class="comment"><div id="post-41238-score" class="comment-score"></div><div class="comment-text"><p>Hi, from the Wireshark, I was able to determine that some Tcp and Udp traffic is routed to a endpoint server, A. Is it possible for the traffic to route to another endpoint server , B, from endpoint A? Or does the traffic really terminates at A itself?</p><p>Thank you.</p></div><div id="comment-41238-info" class="comment-info"><span class="comment-age">(06 Apr '15, 17:56)</span> <span class="comment-user userinfo">许敬佳</span></div></div></div><div id="comment-tools-40871" class="comment-tools"></div><div class="clear"></div><div id="comment-40871-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

