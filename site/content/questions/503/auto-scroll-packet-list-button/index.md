+++
type = "question"
title = "Auto Scroll Packet List button"
description = '''in 1.41.1, when I either Clear or Apply a display filter during a live capture, the Auto Scroll Packet List button toggles off automatically. Is there a way to prevent this?'''
date = "2010-10-13T08:21:00Z"
lastmod = "2011-03-28T12:17:00Z"
weight = 503
keywords = [ "troubleshooting" ]
aliases = [ "/questions/503" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Auto Scroll Packet List button](/questions/503/auto-scroll-packet-list-button)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-503-score" class="post-score" title="current number of votes">0</div><span id="post-503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>in 1.41.1, when I either Clear or Apply a display filter during a live capture, the Auto Scroll Packet List button toggles off automatically. Is there a way to prevent this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '10, 08:21</strong></p><img src="https://secure.gravatar.com/avatar/bcf711a40000b9ef4623f12fad6c56c5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ziplock&#39;s gravatar image" /><p><span>ziplock</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ziplock has no accepted answers">0%</span></p></div></div><div id="comments-container-503" class="comments-container"></div><div id="comment-tools-503" class="comment-tools"></div><div class="clear"></div><div id="comment-503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3180"></span>

<div id="answer-container-3180" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3180-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3180-score" class="post-score" title="current number of votes">0</div><span id="post-3180-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This was a <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=4891">bug</a> that has since been resolved. To correct this problem, I would recommend that you upgrade Wireshark to at least the latest stable release, currently 1.4.4, if you have not done so already.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '11, 12:17</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3180" class="comments-container"></div><div id="comment-tools-3180" class="comment-tools"></div><div class="clear"></div><div id="comment-3180-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

