+++
type = "question"
title = "&quot;follow stream&quot; button is gray ,can not be used."
description = '''Before 2.0.2 Version, this feature is available.After this version, this feature can not be used,I suspect is BUG. When you enter the &quot;Statistics&quot; → &quot;Conversation&quot;, select a TCP dialogue, &quot;follow stream&quot; button is gray ,can not be used.'''
date = "2016-09-08T23:27:00Z"
lastmod = "2016-09-09T02:44:00Z"
weight = 55412
keywords = [ "follow", "button", "stream" ]
aliases = [ "/questions/55412" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["follow stream" button is gray ,can not be used.](/questions/55412/follow-stream-button-is-gray-can-not-be-used)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55412-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55412-score" class="post-score" title="current number of votes">0</div><span id="post-55412-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Before 2.0.2 Version, this feature is available.After this version, this feature can not be used,I suspect is BUG. When you enter the "Statistics" → "Conversation", select a TCP dialogue, "follow stream" button is gray ,can not be used.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-follow" rel="tag" title="see questions tagged &#39;follow&#39;">follow</span> <span class="post-tag tag-link-button" rel="tag" title="see questions tagged &#39;button&#39;">button</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '16, 23:27</strong></p><img src="https://secure.gravatar.com/avatar/7642f48ab511cde0520e6a97f27eb109?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lkjimmyxx&#39;s gravatar image" /><p><span>lkjimmyxx</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lkjimmyxx has no accepted answers">0%</span></p></div></div><div id="comments-container-55412" class="comments-container"></div><div id="comment-tools-55412" class="comment-tools"></div><div class="clear"></div><div id="comment-55412-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55418"></span>

<div id="answer-container-55418" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55418-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55418-score" class="post-score" title="current number of votes">0</div><span id="post-55418-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looks like a bug, please raise an entry on the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a> posting a link to the entry back here for the benefit of other users.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '16, 02:09</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-55418" class="comments-container"><span id="55422"></span><div id="comment-55422" class="comment"><div id="post-55422-score" class="comment-score"></div><div class="comment-text"><p>Grahamb,thank you very much. My English is not good, cannot be competent for complex operations, can you help me to operate it? best wishs.</p></div><div id="comment-55422-info" class="comment-info"><span class="comment-age">(09 Sep '16, 02:44)</span> <span class="comment-user userinfo">lkjimmyxx</span></div></div></div><div id="comment-tools-55418" class="comment-tools"></div><div class="clear"></div><div id="comment-55418-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

