+++
type = "question"
title = "There are missing menu items on my gentoo installation. What is wrong?"
description = '''Hello, after fresh installation on my gentoo only that menu items are presented: File, Edit, View, Go, Capture, Analyze, Help. The others items.. ..Tools, Statistic, etc... are missing. my use flags: Installed versions: 1.10.6(16:50:22 19.4.2014)(caps crypt filecaps geoip ipv6 lua netlink pcap qt4 s...'''
date = "2014-04-24T12:44:00Z"
lastmod = "2014-04-24T14:13:00Z"
weight = 32156
keywords = [ "gentoo" ]
aliases = [ "/questions/32156" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [There are missing menu items on my gentoo installation. What is wrong?](/questions/32156/there-are-missing-menu-items-on-my-gentoo-installation-what-is-wrong)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32156-score" class="post-score" title="current number of votes">0</div><span id="post-32156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, after fresh installation on my gentoo only that menu items are presented: File, Edit, View, Go, Capture, Analyze, Help.</p><p>The others items.. ..Tools, Statistic, etc... are missing.</p><p>my use flags: Installed versions: 1.10.6(16:50:22 19.4.2014)(caps crypt filecaps geoip ipv6 lua netlink pcap qt4 ssl zlib -adns -doc -doc-pdf -gtk2 -gtk3 -kerberos -libadns -portaudio -selinux -smi)</p><p>What to do please?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gentoo" rel="tag" title="see questions tagged &#39;gentoo&#39;">gentoo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '14, 12:44</strong></p><img src="https://secure.gravatar.com/avatar/f6e2f0e0e4f6ded3e8ece1038d136330?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="karpi&#39;s gravatar image" /><p><span>karpi</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="karpi has no accepted answers">0%</span></p></div></div><div id="comments-container-32156" class="comments-container"></div><div id="comment-tools-32156" class="comment-tools"></div><div class="clear"></div><div id="comment-32156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32157"></span>

<div id="answer-container-32157" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32157-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32157-score" class="post-score" title="current number of votes">1</div><span id="post-32157-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is most likely the new QT based version, which is still being ported from the GTK version and does not yet have all the features. You'll have to install (or run, if already installed) the GTK version to get the "old" interface.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Apr '14, 13:01</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32157" class="comments-container"><span id="32160"></span><div id="comment-32160" class="comment"><div id="post-32160-score" class="comment-score"></div><div class="comment-text"><p>Oh, really! GTK+ 3 frontend works well. (USE flags: -gtk2 gtk3 -qt4 ) I've just filled a bug on gentoo.</p><p>Also maybe just small notice here: <a href="http://www.wireshark.org/docs/wsug_html/#idp386418628">http://www.wireshark.org/docs/wsug_html/#idp386418628</a> should be great.</p><p>Thanks a much.</p></div><div id="comment-32160-info" class="comment-info"><span class="comment-age">(24 Apr '14, 14:13)</span> <span class="comment-user userinfo">karpi</span></div></div></div><div id="comment-tools-32157" class="comment-tools"></div><div class="clear"></div><div id="comment-32157-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

