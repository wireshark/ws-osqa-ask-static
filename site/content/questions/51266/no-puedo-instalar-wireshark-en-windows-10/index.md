+++
type = "question"
title = "No puedo instalar Wireshark en windows 10"
description = '''no puedo instalar wireshark en windows 10 al final de la instalacion me salta un error 0xc000012f alguien sabe como solucionarlo?'''
date = "2016-03-29T12:07:00Z"
lastmod = "2016-03-29T12:28:00Z"
weight = 51266
keywords = [ "wiresharkwindows10" ]
aliases = [ "/questions/51266" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No puedo instalar Wireshark en windows 10](/questions/51266/no-puedo-instalar-wireshark-en-windows-10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51266-score" class="post-score" title="current number of votes">0</div><span id="post-51266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>no puedo instalar wireshark en windows 10 al final de la instalacion me salta un error 0xc000012f alguien sabe como solucionarlo?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wiresharkwindows10" rel="tag" title="see questions tagged &#39;wiresharkwindows10&#39;">wiresharkwindows10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '16, 12:07</strong></p><img src="https://secure.gravatar.com/avatar/5de6d0291d38e41df89dc708520922ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fexion&#39;s gravatar image" /><p><span>fexion</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fexion has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Mar '16, 14:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span></p></div></div><div id="comments-container-51266" class="comments-container"><span id="51267"></span><div id="comment-51267" class="comment"><div id="post-51267-score" class="comment-score"></div><div class="comment-text"><p>Translation (Or what I have understood) I can't install Wireshark at Windows 10. The installation always ends with the error 0x000012f. Does anybody know a solution for that?</p></div><div id="comment-51267-info" class="comment-info"><span class="comment-age">(29 Mar '16, 12:28)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-51266" class="comment-tools"></div><div class="clear"></div><div id="comment-51266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

