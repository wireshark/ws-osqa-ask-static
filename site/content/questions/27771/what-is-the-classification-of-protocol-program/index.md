+++
type = "question"
title = "What is the classification of protocol program?"
description = '''My question is &quot;what is the classification of protocol program?&quot; Can u please tell me the correct answer this question.'''
date = "2013-12-04T10:52:00Z"
lastmod = "2013-12-04T13:28:00Z"
weight = 27771
keywords = [ "protocol" ]
aliases = [ "/questions/27771" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What is the classification of protocol program?](/questions/27771/what-is-the-classification-of-protocol-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27771-score" class="post-score" title="current number of votes">0</div><span id="post-27771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My question is "what is the classification of protocol program?" Can u please tell me the correct answer this question.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '13, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/6c362ab764eb0e27d52da83d7a130090?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sweety&#39;s gravatar image" /><p><span>Sweety</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sweety has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>04 Dec '13, 13:28</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-27771" class="comments-container"><span id="27774"></span><div id="comment-27774" class="comment"><div id="post-27774-score" class="comment-score"></div><div class="comment-text"><p>I don't know what the question is asking, so I don't know what the answer would be. What do you mean by "classification" and "protocol program"?</p></div><div id="comment-27774-info" class="comment-info"><span class="comment-age">(04 Dec '13, 13:28)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-27771" class="comment-tools"></div><div class="clear"></div><div id="comment-27771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

