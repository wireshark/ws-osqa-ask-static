+++
type = "question"
title = "QinQ frame size showed is 1522 and is supposed to  be 1526...."
description = '''Hi: I&#x27;m capturing some QinQ frames and I&#x27;m seeing the frame size showed is 1522 and it was supposed to be 1526. BTW I can see both tags (inner and outer). Any ideas?'''
date = "2013-10-11T14:06:00Z"
lastmod = "2013-10-12T13:57:00Z"
weight = 25921
keywords = [ "frame", "qinq", "size" ]
aliases = [ "/questions/25921" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [QinQ frame size showed is 1522 and is supposed to be 1526....](/questions/25921/qinq-frame-size-showed-is-1522-and-is-supposed-to-be-1526)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25921-score" class="post-score" title="current number of votes">0</div><span id="post-25921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi: I'm capturing some QinQ frames and I'm seeing the frame size showed is 1522 and it was supposed to be 1526. BTW I can see both tags (inner and outer). Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-qinq" rel="tag" title="see questions tagged &#39;qinq&#39;">qinq</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '13, 14:06</strong></p><img src="https://secure.gravatar.com/avatar/e63c9db597fe23673882aedf6e5c1beb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JG73&#39;s gravatar image" /><p><span>JG73</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JG73 has no accepted answers">0%</span></p></div></div><div id="comments-container-25921" class="comments-container"><span id="25942"></span><div id="comment-25942" class="comment"><div id="post-25942-score" class="comment-score"></div><div class="comment-text"><p>If by "frame size" you mean the "Frame Length" field in the packet dissection, note that the captured packet might not include the FCS, in which case it would be 4 bytes shorter than you'd expect for a frame length that includes the FCS.</p></div><div id="comment-25942-info" class="comment-info"><span class="comment-age">(12 Oct '13, 13:57)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-25921" class="comment-tools"></div><div class="clear"></div><div id="comment-25921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

