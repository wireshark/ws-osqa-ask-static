+++
type = "question"
title = "Problem with BSSAP+ protocol."
description = '''I capture both GSM and GPRS but wireshark not able to decode the traffic when the protocol is BSSAP+ (GPRS). all the packets marked as &quot;Message Type: Unassigned: treated as an unknown Message type. (0)&quot;. does anyone no about a problem with this protocol on version 1.10.2?'''
date = "2013-10-25T01:20:00Z"
lastmod = "2013-10-25T01:20:00Z"
weight = 26397
keywords = [ "shimi" ]
aliases = [ "/questions/26397" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problem with BSSAP+ protocol.](/questions/26397/problem-with-bssap-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26397-score" class="post-score" title="current number of votes">0</div><span id="post-26397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I capture both GSM and GPRS but wireshark not able to decode the traffic when the protocol is BSSAP+ (GPRS). all the packets marked as "Message Type: Unassigned: treated as an unknown Message type. (0)". does anyone no about a problem with this protocol on version 1.10.2?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-shimi" rel="tag" title="see questions tagged &#39;shimi&#39;">shimi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '13, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/7d102e9a6aec2347eecf87c795208e62?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shim&#39;s gravatar image" /><p><span>shim</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shim has no accepted answers">0%</span></p></div></div><div id="comments-container-26397" class="comments-container"></div><div id="comment-tools-26397" class="comment-tools"></div><div class="clear"></div><div id="comment-26397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

