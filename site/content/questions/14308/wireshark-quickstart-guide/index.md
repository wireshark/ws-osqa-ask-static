+++
type = "question"
title = "Wireshark QuickStart Guide"
description = '''Are there answers available to the exercises in the Wireshark QuickStart Guide?'''
date = "2012-09-16T19:21:00Z"
lastmod = "2012-09-17T00:47:00Z"
weight = 14308
keywords = [ "quickstart", "answers" ]
aliases = [ "/questions/14308" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark QuickStart Guide](/questions/14308/wireshark-quickstart-guide)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14308-score" class="post-score" title="current number of votes">0</div><span id="post-14308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are there answers available to the exercises in the Wireshark QuickStart Guide?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-quickstart" rel="tag" title="see questions tagged &#39;quickstart&#39;">quickstart</span> <span class="post-tag tag-link-answers" rel="tag" title="see questions tagged &#39;answers&#39;">answers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '12, 19:21</strong></p><img src="https://secure.gravatar.com/avatar/8e0d87509b5f1ce846b4b0c753dc29b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dude101&#39;s gravatar image" /><p><span>dude101</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dude101 has no accepted answers">0%</span></p></div></div><div id="comments-container-14308" class="comments-container"><span id="14316"></span><div id="comment-14316" class="comment"><div id="post-14316-score" class="comment-score"></div><div class="comment-text"><p>And by "Wireshark Quickstart Guide" you are referring to which guide exactly (please provide the URL)?</p></div><div id="comment-14316-info" class="comment-info"><span class="comment-age">(17 Sep '12, 00:47)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-14308" class="comment-tools"></div><div class="clear"></div><div id="comment-14308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

