+++
type = "question"
title = "Torrent download capture"
description = '''Hi, my network is 100pc&#x27;s big and would like to know how to capture traffic from clients downloading through torrents such as bittorrent and utorrent. My ISP has given me a warning on copyright violation because someone is downloading movies on our business internet. And not all pc&#x27;s can be proxied....'''
date = "2011-09-20T09:18:00Z"
lastmod = "2011-09-20T09:41:00Z"
weight = 6466
keywords = [ "download", "capture", "torrent" ]
aliases = [ "/questions/6466" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Torrent download capture](/questions/6466/torrent-download-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6466-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6466-score" class="post-score" title="current number of votes">0</div><span id="post-6466-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, my network is 100pc's big and would like to know how to capture traffic from clients downloading through torrents such as bittorrent and utorrent. My ISP has given me a warning on copyright violation because someone is downloading movies on our business internet. And not all pc's can be proxied.</p><p>Thx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-torrent" rel="tag" title="see questions tagged &#39;torrent&#39;">torrent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '11, 09:18</strong></p><img src="https://secure.gravatar.com/avatar/749c0c883a29b02efeede7d3500634f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Frederick%20Botha&#39;s gravatar image" /><p><span>Frederick Botha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Frederick Botha has no accepted answers">0%</span></p></div></div><div id="comments-container-6466" class="comments-container"></div><div id="comment-tools-6466" class="comment-tools"></div><div class="clear"></div><div id="comment-6466-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6467"></span>

<div id="answer-container-6467" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6467-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6467-score" class="post-score" title="current number of votes">0</div><span id="post-6467-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might want to capture your internet uplink and then use the protocol and endpoint statistics to find out who is doing suspicious protocols or amounts of traffic. Movies especially are quite large (usually above and beyond 500MB), so if a node is having that much traffic you could zero in on it.</p><p>Torrent traffic can run on a lot of different ports, often even randomized on each start of the torrent program, but you should be able to spot the traffic anyway. You might want to take a look at the conversation statistics to see if there are any nodes that have tons of external IPs as communication partners, which would be typical for torrent downloads - each seeder they are connected to would appear in the list.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Sep '11, 09:41</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-6467" class="comments-container"></div><div id="comment-tools-6467" class="comment-tools"></div><div class="clear"></div><div id="comment-6467-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

