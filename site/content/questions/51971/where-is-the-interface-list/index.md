+++
type = "question"
title = "Where is the interface list??"
description = '''Sorry, I&#x27;m new at this and I can&#x27;t seem to find the interface list...'''
date = "2016-04-26T14:13:00Z"
lastmod = "2016-04-26T23:52:00Z"
weight = 51971
keywords = [ "help" ]
aliases = [ "/questions/51971" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where is the interface list??](/questions/51971/where-is-the-interface-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51971-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51971-score" class="post-score" title="current number of votes">0</div><span id="post-51971-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sorry, I'm new at this and I can't seem to find the interface list...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '16, 14:13</strong></p><img src="https://secure.gravatar.com/avatar/15b318b7f54d8731b02306b376dc9188?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Serenity%20Adams&#39;s gravatar image" /><p><span>Serenity Adams</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Serenity Adams has no accepted answers">0%</span></p></div></div><div id="comments-container-51971" class="comments-container"></div><div id="comment-tools-51971" class="comment-tools"></div><div class="clear"></div><div id="comment-51971-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51972"></span>

<div id="answer-container-51972" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51972-score" class="post-score" title="current number of votes">0</div><span id="post-51972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try Capture -&gt; Options, or CTRL-K.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '16, 14:18</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-51972" class="comments-container"><span id="51976"></span><div id="comment-51976" class="comment"><div id="post-51976-score" class="comment-score"></div><div class="comment-text"><p>It's not the same, I'm trying to find someones IP address online, I just need to find the interface list, I know how to do everything else.</p></div><div id="comment-51976-info" class="comment-info"><span class="comment-age">(26 Apr '16, 15:51)</span> <span class="comment-user userinfo">Serenity Adams</span></div></div><span id="51981"></span><div id="comment-51981" class="comment"><div id="post-51981-score" class="comment-score"></div><div class="comment-text"><p>hm, do you mean Statistics -&gt; Endpoints?</p></div><div id="comment-51981-info" class="comment-info"><span class="comment-age">(26 Apr '16, 23:25)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="51983"></span><div id="comment-51983" class="comment"><div id="post-51983-score" class="comment-score"></div><div class="comment-text"><p>Or do you mean "list of interfaces on which a capture stored in a pcapng file has been taken"?</p><p>And do you mean "interface" as an IP address or "interface" as a hardware object?</p></div><div id="comment-51983-info" class="comment-info"><span class="comment-age">(26 Apr '16, 23:52)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-51972" class="comment-tools"></div><div class="clear"></div><div id="comment-51972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

