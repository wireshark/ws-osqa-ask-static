+++
type = "question"
title = "What is the device about wi-fiALL : p2p ,in pcap file"
description = '''I receive one packets with vendor specific tag: please see image file at : http://imgur.com/7NdgESL i need more info about this device and listen channel : Operating Class 81'''
date = "2015-08-01T00:22:00Z"
lastmod = "2015-08-02T17:29:00Z"
weight = 44718
keywords = [ "wireshark" ]
aliases = [ "/questions/44718" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What is the device about wi-fiALL : p2p ,in pcap file](/questions/44718/what-is-the-device-about-wi-fiall-p2p-in-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44718-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44718-score" class="post-score" title="current number of votes">0</div><span id="post-44718-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I receive one packets with vendor specific tag: please see image file at : <a href="http://imgur.com/7NdgESL">http://imgur.com/7NdgESL</a></p><p>i need more info about this device and listen channel : Operating Class 81</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '15, 00:22</strong></p><img src="https://secure.gravatar.com/avatar/9b7c129577c67f2c99ed567977a59b22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NewMill&#39;s gravatar image" /><p><span>NewMill</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NewMill has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Aug '15, 00:24</strong> </span></p></div></div><div id="comments-container-44718" class="comments-container"></div><div id="comment-tools-44718" class="comment-tools"></div><div class="clear"></div><div id="comment-44718-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44719"></span>

<div id="answer-container-44719" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44719-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44719-score" class="post-score" title="current number of votes">0</div><span id="post-44719-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>"Wi-FiAll" is short for "<a href="http://www.wi-fi.org">Wi-Fi Alliance</a>", and "P2P" refers to The Specification Formerly Known As Wi-Fi P2P (peer-to-peer) and now known as <a href="https://en.wikipedia.org/wiki/Wi-Fi_Direct">Wi-Fi Direct</a>.</p><p>Presumably the device that sent that packet is announcing the details of its support for Wi-Fi Direct.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '15, 00:55</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-44719" class="comments-container"><span id="44721"></span><div id="comment-44721" class="comment"><div id="post-44721-score" class="comment-score"></div><div class="comment-text"><p>Thanks. Mostly on what situations people use this wifi direct?</p></div><div id="comment-44721-info" class="comment-info"><span class="comment-age">(01 Aug '15, 01:29)</span> <span class="comment-user userinfo">NewMill</span></div></div><span id="44749"></span><div id="comment-44749" class="comment"><div id="post-44749-score" class="comment-score"></div><div class="comment-text"><p>One of the best known services that use P2P is Miracast (aka WiFi Display).<br />
</p><p>Miracast = Devices make use of a WiFi connection to deliver audio and video content from one device to another, without cables or a connection to an existing Wi-Fi network.</p><p>With Miracast, you can share your phone's screen to a TV screen without the 2 being on the same WiFi network. The phone and TV create their own WiFi network using P2P and then use Miracast to share the audio/video content between the phone to the TV. However, your phone and TV must both support Miracast. If your TV does not support Miracast and has a USB adapter, then you can buy a Miracast adapter to interface your TV.</p></div><div id="comment-44749-info" class="comment-info"><span class="comment-age">(02 Aug '15, 17:29)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-44719" class="comment-tools"></div><div class="clear"></div><div id="comment-44719-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

