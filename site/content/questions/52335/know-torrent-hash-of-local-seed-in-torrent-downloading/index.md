+++
type = "question"
title = "[closed] Know torrent hash of local seed in torrent downloading"
description = '''I use internet from my ISP with cable connections. Whenever i download any torrent with more seed/peers, it get downloaded within seconds due to local seed. Is their any method through which i can know torrents or torrent hash that are being used or downloaded in my network connections. So that I co...'''
date = "2016-05-09T02:34:00Z"
lastmod = "2016-05-09T04:20:00Z"
weight = 52335
keywords = [ "seed", "torrent", "p2p" ]
aliases = [ "/questions/52335" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Know torrent hash of local seed in torrent downloading](/questions/52335/know-torrent-hash-of-local-seed-in-torrent-downloading)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52335-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52335-score" class="post-score" title="current number of votes">0</div><span id="post-52335-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use internet from my ISP with cable connections. Whenever i download any torrent with more seed/peers, it get downloaded within seconds due to local seed. Is their any method through which i can know torrents or torrent hash that are being used or downloaded in my network connections. So that I could save time by just downloading only that particular torrents.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-seed" rel="tag" title="see questions tagged &#39;seed&#39;">seed</span> <span class="post-tag tag-link-torrent" rel="tag" title="see questions tagged &#39;torrent&#39;">torrent</span> <span class="post-tag tag-link-p2p" rel="tag" title="see questions tagged &#39;p2p&#39;">p2p</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '16, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/e0c449b01c48ded913c315802c4c8ebb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="monsterdevil&#39;s gravatar image" /><p><span>monsterdevil</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="monsterdevil has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>09 May '16, 04:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-52335" class="comments-container"><span id="52337"></span><div id="comment-52337" class="comment"><div id="post-52337-score" class="comment-score"></div><div class="comment-text"><p>Not with Wireshark, as that will only show traffic after you have started the torrent download.</p><p>Not really a topic for this site.</p></div><div id="comment-52337-info" class="comment-info"><span class="comment-age">(09 May '16, 04:01)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="52338"></span><div id="comment-52338" class="comment"><div id="post-52338-score" class="comment-score"></div><div class="comment-text"><p>But i have seen various topics of torrent detection in LAN network with wireshark, but i couldn't get it, hence this question</p></div><div id="comment-52338-info" class="comment-info"><span class="comment-age">(09 May '16, 04:20)</span> <span class="comment-user userinfo">monsterdevil</span></div></div></div><div id="comment-tools-52335" class="comment-tools"></div><div class="clear"></div><div id="comment-52335-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 09 May '16, 04:01

</div>

</div>

</div>

