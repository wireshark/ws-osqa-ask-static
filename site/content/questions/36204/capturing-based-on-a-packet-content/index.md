+++
type = "question"
title = "Capturing based on a packet content"
description = '''Hi, I&#x27;d like to logs all packets that containg a specific pattern (a string in my case). I want to log only thoses packets 24h/7. Right now, I can log all packets that comes to a specified port and apply a filter, but this generate tons of data on the disk, generating too much data to be usefull. So...'''
date = "2014-09-11T07:55:00Z"
lastmod = "2014-09-13T09:27:00Z"
weight = 36204
keywords = [ "capture", "capture-filter" ]
aliases = [ "/questions/36204" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing based on a packet content](/questions/36204/capturing-based-on-a-packet-content)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36204-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36204-score" class="post-score" title="current number of votes">0</div><span id="post-36204-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'd like to logs all packets that containg a specific pattern (a string in my case). I want to log only thoses packets 24h/7.</p><p>Right now, I can log all packets that comes to a specified port and apply a filter, but this generate tons of data on the disk, generating too much data to be usefull.</p><p>So I'd like to be able to log using a filter (not logging everything that comes in and then applying a filter).</p><p>Is that possible with wireshark?</p><p>Thx!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '14, 07:55</strong></p><img src="https://secure.gravatar.com/avatar/8a1cb95e24888fc69b54da09d66fba75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Snalibe&#39;s gravatar image" /><p><span>Snalibe</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Snalibe has no accepted answers">0%</span></p></div></div><div id="comments-container-36204" class="comments-container"></div><div id="comment-tools-36204" class="comment-tools"></div><div class="clear"></div><div id="comment-36204-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36295"></span>

<div id="answer-container-36295" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36295-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36295-score" class="post-score" title="current number of votes">1</div><span id="post-36295-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please use this String-Matching Capture Filter Generator:</p><p><a href="https://www.wireshark.org/tools/string-cf.html">https://www.wireshark.org/tools/string-cf.html</a></p><p>As an alternative you could use <a href="http://ngrep.sourceforge.net/">ngrep</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '14, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-36295" class="comments-container"></div><div id="comment-tools-36295" class="comment-tools"></div><div class="clear"></div><div id="comment-36295-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

