+++
type = "question"
title = "getting the contents"
description = '''i have captured a packet.. i want to get the actual content using wireshark.. thank you'''
date = "2014-12-10T20:55:00Z"
lastmod = "2014-12-11T03:36:00Z"
weight = 38522
keywords = [ "content" ]
aliases = [ "/questions/38522" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [getting the contents](/questions/38522/getting-the-contents)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38522-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38522-score" class="post-score" title="current number of votes">0</div><span id="post-38522-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have captured a packet.. i want to get the actual content using wireshark.. thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-content" rel="tag" title="see questions tagged &#39;content&#39;">content</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '14, 20:55</strong></p><img src="https://secure.gravatar.com/avatar/eec6a24bc7b9b853596ac15d89f9ee2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eager&#39;s gravatar image" /><p><span>eager</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eager has no accepted answers">0%</span></p></div></div><div id="comments-container-38522" class="comments-container"><span id="38526"></span><div id="comment-38526" class="comment"><div id="post-38526-score" class="comment-score"></div><div class="comment-text"><p>Content of what? Wireshark will display all the contents of the packet that it knows about. You'll need to be much more explicit about what you have and what you want.</p></div><div id="comment-38526-info" class="comment-info"><span class="comment-age">(11 Dec '14, 03:36)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-38522" class="comment-tools"></div><div class="clear"></div><div id="comment-38522-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

