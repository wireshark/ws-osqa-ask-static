+++
type = "question"
title = "Does Wireshark have an expert analysis function?"
description = '''Does Wireshark have an expert analysis function?'''
date = "2014-04-26T15:27:00Z"
lastmod = "2014-04-28T18:03:00Z"
weight = 32204
keywords = [ "analylsis", "expert" ]
aliases = [ "/questions/32204" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Does Wireshark have an expert analysis function?](/questions/32204/does-wireshark-have-an-expert-analysis-function)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32204-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32204-score" class="post-score" title="current number of votes">0</div><span id="post-32204-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does Wireshark have an expert analysis function?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analylsis" rel="tag" title="see questions tagged &#39;analylsis&#39;">analylsis</span> <span class="post-tag tag-link-expert" rel="tag" title="see questions tagged &#39;expert&#39;">expert</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '14, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/885666c057a323159826c414b83eae37?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fred&#39;s gravatar image" /><p><span>fred</span><br />
<span class="score" title="26 reputation points">26</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fred has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Apr '14, 13:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-32204" class="comments-container"><span id="32208"></span><div id="comment-32208" class="comment"><div id="post-32208-score" class="comment-score"></div><div class="comment-text"><p>What kind of expert knowledge do you need?</p></div><div id="comment-32208-info" class="comment-info"><span class="comment-age">(26 Apr '14, 16:33)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="32225"></span><div id="comment-32225" class="comment"><div id="post-32225-score" class="comment-score"></div><div class="comment-text"><p>i care some key indicators, for example, i plan to do statistics on a web server, and need to get average response time,average network time,access number, distribution of return state etc.</p></div><div id="comment-32225-info" class="comment-info"><span class="comment-age">(27 Apr '14, 18:43)</span> <span class="comment-user userinfo">fred</span></div></div></div><div id="comment-tools-32204" class="comment-tools"></div><div class="clear"></div><div id="comment-32204-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="32205"></span>

<div id="answer-container-32205" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32205-score" class="post-score" title="current number of votes">2</div><span id="post-32205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure. Main Menu -&gt; Analyze -&gt; Expert Info. Don't expect it to point out exact problems though, but in defense of Wireshark I have to say that I have never seen a network analyzer that had a good expert.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '14, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32205" class="comments-container"><span id="32206"></span><div id="comment-32206" class="comment"><div id="post-32206-score" class="comment-score"></div><div class="comment-text"><p>Could you give me some advice about how to more efficiently use expert function?</p></div><div id="comment-32206-info" class="comment-info"><span class="comment-age">(26 Apr '14, 15:42)</span> <span class="comment-user userinfo">fred</span></div></div><span id="32207"></span><div id="comment-32207" class="comment"><div id="post-32207-score" class="comment-score"></div><div class="comment-text"><p>Well, the only thing you can do is go through the expert messages and check if they point to a problem you're looking for.</p></div><div id="comment-32207-info" class="comment-info"><span class="comment-age">(26 Apr '14, 16:32)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="32226"></span><div id="comment-32226" class="comment"><div id="post-32226-score" class="comment-score"></div><div class="comment-text"><p>can you give a real example?</p></div><div id="comment-32226-info" class="comment-info"><span class="comment-age">(27 Apr '14, 18:44)</span> <span class="comment-user userinfo">fred</span></div></div><span id="32249"></span><div id="comment-32249" class="comment"><div id="post-32249-score" class="comment-score"></div><div class="comment-text"><p>Example: the expert says: "TCP Zero Window segment", which is a pretty good warning sign that the performance of the receiving node is not good enough to handle the load of incoming packets.</p><p>Now check if that node recovers from that Zero Window state within a few milliseconds or even microseconds by looking for "Window Updates" - if so, the zero window is still not good, but may not be the problem you're looking for, because the delay it caused is so small that it doesn't matter.</p></div><div id="comment-32249-info" class="comment-info"><span class="comment-age">(28 Apr '14, 05:09)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="32274"></span><div id="comment-32274" class="comment"><div id="post-32274-score" class="comment-score"></div><div class="comment-text"><p>can you give more examples about expert info? i have no idea how to use those expert info.</p></div><div id="comment-32274-info" class="comment-info"><span class="comment-age">(28 Apr '14, 18:01)</span> <span class="comment-user userinfo">fred</span></div></div></div><div id="comment-tools-32205" class="comment-tools"></div><div class="clear"></div><div id="comment-32205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="32258"></span>

<div id="answer-container-32258" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32258-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32258-score" class="post-score" title="current number of votes">1</div><span id="post-32258-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>i plan to do statistics on a web server, and need to get average response time,average network time,access number, distribution of return state etc.</p></blockquote><p>O.K. you won't get that from Wireshark out of the box, as the HTTP stats don't work at that level of detail. However, you can create all those stats by using tshark (CLI tool) and some scripting.</p><p>If that is an option for you, meaning you know how to script something, please report back and I'll post more details.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '14, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-32258" class="comments-container"><span id="32275"></span><div id="comment-32275" class="comment"><div id="post-32275-score" class="comment-score"></div><div class="comment-text"><p>hi,Kurt could you give some script examples to get those key indicators by using tshark?</p></div><div id="comment-32275-info" class="comment-info"><span class="comment-age">(28 Apr '14, 18:03)</span> <span class="comment-user userinfo">fred</span></div></div></div><div id="comment-tools-32258" class="comment-tools"></div><div class="clear"></div><div id="comment-32258-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

