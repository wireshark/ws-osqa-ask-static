+++
type = "question"
title = "Wireless Cisco CAPWAP header size"
description = '''I did a capture with wireshark on the port where our Cisco 3500 AP is connected and I have a ton of errors for the CAPWAP header. The &quot;Expert Info&quot; Message is (Warn/Malformed): Wrong Calculate length (11) =! header lenght (15) ! (May be try to use Cisco Wireless Controller Support Preference ?) I di...'''
date = "2013-09-25T18:45:00Z"
lastmod = "2013-12-19T14:37:00Z"
weight = 25254
keywords = [ "wireless", "capwap", "cisco" ]
aliases = [ "/questions/25254" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless Cisco CAPWAP header size](/questions/25254/wireless-cisco-capwap-header-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25254-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25254-score" class="post-score" title="current number of votes">0</div><span id="post-25254-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I did a capture with wireshark on the port where our Cisco 3500 AP is connected and I have a ton of errors for the CAPWAP header.</p><p>The "Expert Info" Message is (Warn/Malformed): Wrong Calculate length (11) =! header lenght (15) ! (May be try to use Cisco Wireless Controller Support Preference ?)</p><p>I did a capture at the end of the day when I was the only one in the office (PC, iPhone, a couple of iPads) and I did it during the middle of the day with a good number of clients connected a lot more Apple products. It seems to have a lot more of those errors with apple devices. In fact, that is how I fist noticed it. I was trying to trouble shoot an AP that was dropping apple products.</p><p>Is this message stating that WireShark is calculating that the header to be a certain length and it's actual length is different thus giving the warning?</p><p>Has anyone here encountered this problem and if so do you have a fix?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-capwap" rel="tag" title="see questions tagged &#39;capwap&#39;">capwap</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '13, 18:45</strong></p><img src="https://secure.gravatar.com/avatar/54aa785bba7b4c18ded0398f386a1df6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="djroge1&#39;s gravatar image" /><p><span>djroge1</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="djroge1 has no accepted answers">0%</span></p></div></div><div id="comments-container-25254" class="comments-container"><span id="28299"></span><div id="comment-28299" class="comment"><div id="post-28299-score" class="comment-score"></div><div class="comment-text"><p>I have same errors (from time to time). Have no idea why it is like this... at least now.</p></div><div id="comment-28299-info" class="comment-info"><span class="comment-age">(19 Dec '13, 14:37)</span> <span class="comment-user userinfo">Yakuza</span></div></div></div><div id="comment-tools-25254" class="comment-tools"></div><div class="clear"></div><div id="comment-25254-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

