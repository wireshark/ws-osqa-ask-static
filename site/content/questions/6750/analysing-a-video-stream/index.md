+++
type = "question"
title = "analysing a video stream"
description = '''Hello, I connected a Set-Top-Box (IPTV-Box) with my Computer via ethernet. The IP-Box gets its signal from an antenna-cable. On my Computer I installed a software(including a videoplayer) of the box that allows me to watch the video stream. So when I am watching this stream i start a wireshark analy...'''
date = "2011-10-06T02:07:00Z"
lastmod = "2011-10-06T02:07:00Z"
weight = 6750
keywords = [ "video", "analysis", "stream" ]
aliases = [ "/questions/6750" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [analysing a video stream](/questions/6750/analysing-a-video-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6750-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6750-score" class="post-score" title="current number of votes">0</div><span id="post-6750-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I connected a Set-Top-Box (IPTV-Box) with my Computer via ethernet. The IP-Box gets its signal from an antenna-cable. On my Computer I installed a software(including a videoplayer) of the box that allows me to watch the video stream. So when I am watching this stream i start a wireshark analysis. Now I want to know what kind of video stream is sent to my computer like mpeg1,2,3,4 or others. Is it possible to see this in the wireshark analysis?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '11, 02:07</strong></p><img src="https://secure.gravatar.com/avatar/4d25ed7a2f43380fb04855e1c38a0e1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wolfi87&#39;s gravatar image" /><p><span>wolfi87</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wolfi87 has no accepted answers">0%</span></p></div></div><div id="comments-container-6750" class="comments-container"></div><div id="comment-tools-6750" class="comment-tools"></div><div class="clear"></div><div id="comment-6750-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

