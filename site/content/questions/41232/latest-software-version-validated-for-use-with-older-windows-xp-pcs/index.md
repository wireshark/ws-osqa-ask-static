+++
type = "question"
title = "Latest Software Version Validated for Use with older Windows XP PC&#x27;s"
description = '''Morning, I&#x27;ve kept an older Windows PC with XP on hand to support periodic connectivity with legacy devices and older apps not necessarily fully compatible with newer operating systems and their backward &quot;compatibility&quot; modes. It will be handy for me to add Wireshark to that PC. Request advise which...'''
date = "2015-04-06T13:40:00Z"
lastmod = "2015-04-06T13:58:00Z"
weight = 41232
keywords = [ "poser" ]
aliases = [ "/questions/41232" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Latest Software Version Validated for Use with older Windows XP PC's](/questions/41232/latest-software-version-validated-for-use-with-older-windows-xp-pcs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41232-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41232-score" class="post-score" title="current number of votes">0</div><span id="post-41232-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Morning,</p><p>I've kept an older Windows PC with XP on hand to support periodic connectivity with legacy devices and older apps not necessarily fully compatible with newer operating systems and their backward "compatibility" modes. It will be handy for me to add Wireshark to that PC. Request advise which version of Wireshark was last validated to work on the older 32-bit Windows XP platform since the OS no longer resides within the supportability world?</p><p>Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-poser" rel="tag" title="see questions tagged &#39;poser&#39;">poser</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '15, 13:40</strong></p><img src="https://secure.gravatar.com/avatar/f83cb9ac051a3c32872f4b3ff9c6e22c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jimmy%20Rackley&#39;s gravatar image" /><p><span>Jimmy Rackley</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jimmy Rackley has no accepted answers">0%</span></p></div></div><div id="comments-container-41232" class="comments-container"></div><div id="comment-tools-41232" class="comment-tools"></div><div class="clear"></div><div id="comment-41232-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41233"></span>

<div id="answer-container-41233" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41233-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41233-score" class="post-score" title="current number of votes">0</div><span id="post-41233-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The last release branch that officially supports XP is 1.10: <a href="https://wiki.wireshark.org/Development/LifeCycle">https://wiki.wireshark.org/Development/LifeCycle</a> . As noted on that page, 1.10 reaches EOL on June 5, 2015.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '15, 13:58</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-41233" class="comments-container"></div><div id="comment-tools-41233" class="comment-tools"></div><div class="clear"></div><div id="comment-41233-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

