+++
type = "question"
title = "how to unhide a interface"
description = '''I am running wireshark 1.8.4 on windows pro I am attempting to do remote capture on a Linux 5 box. I was able to get the remote interface and I hide it. Now if I go to manage interfaces it does not show up but it shows up in the interface list. I need to delete it but I can&#x27;t because it doesn&#x27;t show...'''
date = "2012-12-21T12:30:00Z"
lastmod = "2012-12-21T12:30:00Z"
weight = 17144
keywords = [ "interfaces" ]
aliases = [ "/questions/17144" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to unhide a interface](/questions/17144/how-to-unhide-a-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17144-score" class="post-score" title="current number of votes">0</div><span id="post-17144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am running wireshark 1.8.4 on windows pro I am attempting to do remote capture on a Linux 5 box. I was able to get the remote interface and I hide it. Now if I go to manage interfaces it does not show up but it shows up in the interface list. I need to delete it but I can't because it doesn't show up it the remote interface list.</p><p>So how do I unhide it and delete it from the interface list?</p><p>Thanks much</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Dec '12, 12:30</strong></p><img src="https://secure.gravatar.com/avatar/3b3950d6f8b741c6c5f44bdf32f0e4ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aberryjr&#39;s gravatar image" /><p><span>aberryjr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aberryjr has no accepted answers">0%</span></p></div></div><div id="comments-container-17144" class="comments-container"></div><div id="comment-tools-17144" class="comment-tools"></div><div class="clear"></div><div id="comment-17144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

