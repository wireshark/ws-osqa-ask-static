+++
type = "question"
title = "about measuring TCP performance in p2p network"
description = '''Hi, I&#x27;m doing research about P2P and captured download log from multiple source. How I can get throughput and delay from that ? please help, I&#x27;m a beginner in wireshark Thanks'''
date = "2017-01-12T00:43:00Z"
lastmod = "2017-01-12T00:43:00Z"
weight = 58687
keywords = [ "p2p", "tcp" ]
aliases = [ "/questions/58687" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [about measuring TCP performance in p2p network](/questions/58687/about-measuring-tcp-performance-in-p2p-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58687-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58687-score" class="post-score" title="current number of votes">0</div><span id="post-58687-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm doing research about P2P and captured download log from multiple source. How I can get throughput and delay from that ?</p><p>please help, I'm a beginner in wireshark Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-p2p" rel="tag" title="see questions tagged &#39;p2p&#39;">p2p</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '17, 00:43</strong></p><img src="https://secure.gravatar.com/avatar/7181180bc94075ed1bb4aaa46e391b9a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="YogaF&#39;s gravatar image" /><p><span>YogaF</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="YogaF has no accepted answers">0%</span></p></div></div><div id="comments-container-58687" class="comments-container"></div><div id="comment-tools-58687" class="comment-tools"></div><div class="clear"></div><div id="comment-58687-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

