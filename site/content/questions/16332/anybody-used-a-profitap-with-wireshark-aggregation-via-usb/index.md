+++
type = "question"
title = "Anybody used a ProfiTAP with Wireshark? (aggregation via USB)"
description = '''Hi guys New to Wireshark and new to the forum. I&#x27;m looking at getting a general purpose, low(ish) cost full-duplex TAP. I&#x27;d like to avoid needing two NICs on my laptop and this looks like a good option: http://www.moesarc.co.uk/network_taps_profitap1.html Does anyone have any good or bad experiences...'''
date = "2012-11-26T11:16:00Z"
lastmod = "2013-03-01T05:31:00Z"
weight = 16332
keywords = [ "profitap", "tap" ]
aliases = [ "/questions/16332" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Anybody used a ProfiTAP with Wireshark? (aggregation via USB)](/questions/16332/anybody-used-a-profitap-with-wireshark-aggregation-via-usb)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16332-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16332-score" class="post-score" title="current number of votes">0</div><span id="post-16332-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys</p><p>New to Wireshark and new to the forum.</p><p>I'm looking at getting a general purpose, low(ish) cost full-duplex TAP. I'd like to avoid needing two NICs on my laptop and this looks like a good option:</p><p><a href="http://www.moesarc.co.uk/network_taps_profitap1.html">http://www.moesarc.co.uk/network_taps_profitap1.html</a></p><p>Does anyone have any good or bad experiences of using this with Wireshark?</p><p>Many thanks.</p><p>Stu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-profitap" rel="tag" title="see questions tagged &#39;profitap&#39;">profitap</span> <span class="post-tag tag-link-tap" rel="tag" title="see questions tagged &#39;tap&#39;">tap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '12, 11:16</strong></p><img src="https://secure.gravatar.com/avatar/1ec502db2fef9362cd8f7a3a938ec3cf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="london-stu&#39;s gravatar image" /><p><span>london-stu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="london-stu has no accepted answers">0%</span></p></div></div><div id="comments-container-16332" class="comments-container"><span id="19027"></span><div id="comment-19027" class="comment"><div id="post-19027-score" class="comment-score"></div><div class="comment-text"><p><span>@london-stu</span>: This is a Question and Answer site, not so much a forum. You post a question and people provide an answer, of which you are supposed to select the correct, applicable one. So if the answer from <span>@Laurent</span> below is fine, please click the tick. There's also a user mailing list at <span class="__cf_email__" data-cfemail="087f617a6d7b60697a63257d7b6d7a7b487f617a6d7b60697a6326677a6f">[email protected]</span> / <a href="https://www.wireshark.org/mailman/listinfo/wireshark-users.">https://www.wireshark.org/mailman/listinfo/wireshark-users.</a></p></div><div id="comment-19027-info" class="comment-info"><span class="comment-age">(01 Mar '13, 05:31)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-16332" class="comment-tools"></div><div class="clear"></div><div id="comment-16332-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19025"></span>

<div id="answer-container-19025" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19025-score" class="post-score" title="current number of votes">0</div><span id="post-19025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi Stu,</p><p>The Profitap TAP works perfect with Wireshark.</p><p>Laurent</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Mar '13, 04:42</strong></p><img src="https://secure.gravatar.com/avatar/111722c7fa85dbc71859d048b3c1fdd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Laurent&#39;s gravatar image" /><p><span>Laurent</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Laurent has no accepted answers">0%</span></p></div></div><div id="comments-container-19025" class="comments-container"></div><div id="comment-tools-19025" class="comment-tools"></div><div class="clear"></div><div id="comment-19025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

