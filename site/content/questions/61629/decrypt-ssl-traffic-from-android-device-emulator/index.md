+++
type = "question"
title = "Decrypt SSL traffic from Android Device (Emulator)"
description = '''I&#x27;m using an Android Emulator and logging into some apps (while running WireShark), and I now trying to figure out how to decrypt the SSL traffic. I understand that I need to find some sort of key to throw into WireShark, but I am unable to figure out how to find that on the Android OS, or if it&#x27;s e...'''
date = "2017-05-25T09:55:00Z"
lastmod = "2017-05-25T09:55:00Z"
weight = 61629
keywords = [ "ssl", "android", "wireshark", "ssl_decrypt" ]
aliases = [ "/questions/61629" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt SSL traffic from Android Device (Emulator)](/questions/61629/decrypt-ssl-traffic-from-android-device-emulator)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61629-score" class="post-score" title="current number of votes">0</div><span id="post-61629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using an Android Emulator and logging into some apps (while running WireShark), and I now trying to figure out how to decrypt the SSL traffic.</p><p>I understand that I need to find some sort of key to throw into WireShark, but I am unable to figure out how to find that on the Android OS, or if it's even possible.</p><p>Any ideas?</p><p>I have tried some other options, like some sort of app that uses the man in the middle technique, which connects through a VPN. However, all the apps on my phone no longer seem to have internet connection when doing that.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-ssl_decrypt" rel="tag" title="see questions tagged &#39;ssl_decrypt&#39;">ssl_decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '17, 09:55</strong></p><img src="https://secure.gravatar.com/avatar/cf999cecf32dbdd3ccc142b4858ca921?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samiejg&#39;s gravatar image" /><p><span>samiejg</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samiejg has no accepted answers">0%</span></p></div></div><div id="comments-container-61629" class="comments-container"></div><div id="comment-tools-61629" class="comment-tools"></div><div class="clear"></div><div id="comment-61629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

