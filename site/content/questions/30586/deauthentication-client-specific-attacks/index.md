+++
type = "question"
title = "Deauthentication - Client specific attacks"
description = '''I noticed that with deauthentication display filter on (wlan.fc.type_subtype eq 12), wireshark can only see broadcast deauthentication (but not client specific deauthentication). This is confirmed as we able to see the client specific deauthentication message with a different software. Can you confi...'''
date = "2014-03-07T15:53:00Z"
lastmod = "2014-03-09T03:28:00Z"
weight = 30586
keywords = [ "display_filter", "display" ]
aliases = [ "/questions/30586" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Deauthentication - Client specific attacks](/questions/30586/deauthentication-client-specific-attacks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30586-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30586-score" class="post-score" title="current number of votes">0</div><span id="post-30586-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I noticed that with deauthentication display filter on (wlan.fc.type_subtype eq 12), wireshark can only see broadcast deauthentication (but not client specific deauthentication). This is confirmed as we able to see the client specific deauthentication message with a different software. Can you confirm whether it is a limitation with wireshark and whether it will be fixed in the near future?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display_filter" rel="tag" title="see questions tagged &#39;display_filter&#39;">display_filter</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Mar '14, 15:53</strong></p><img src="https://secure.gravatar.com/avatar/9ce5848c46132f2ad5b01ad9fb47248c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mezzokatso&#39;s gravatar image" /><p><span>mezzokatso</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mezzokatso has no accepted answers">0%</span></p></div></div><div id="comments-container-30586" class="comments-container"><span id="30610"></span><div id="comment-30610" class="comment"><div id="post-30610-score" class="comment-score"></div><div class="comment-text"><p>can you provide a sample capture file with two frames in the same file (on google drive, dropbox, cloudshark.org)?</p><ul><li>a frame with broadcast deauthentication</li><li>a frame with client deauthentication</li></ul></div><div id="comment-30610-info" class="comment-info"><span class="comment-age">(09 Mar '14, 03:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30586" class="comment-tools"></div><div class="clear"></div><div id="comment-30586-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

