+++
type = "question"
title = "Time format for Delta and TCP Delta Columns"
description = '''Hello,  I couldn&#x27;t find any help for this on the Internet and here so here I go ! Some of my profiles have the Delta and TCP Delta columns. They both have a display format of nanoseconds, which is unneccessary to me. It&#x27;s not much, but it would be cleaner for me if it stopped at milliseconds.  My re...'''
date = "2017-05-03T12:09:00Z"
lastmod = "2017-05-05T05:24:00Z"
weight = 61215
keywords = [ "delta", "time", "milliseconds", "format" ]
aliases = [ "/questions/61215" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Time format for Delta and TCP Delta Columns](/questions/61215/time-format-for-delta-and-tcp-delta-columns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61215-score" class="post-score" title="current number of votes">0</div><span id="post-61215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I couldn't find any help for this on the Internet and here so here I go !</p><p>Some of my profiles have the Delta and TCP Delta columns. They both have a display format of nanoseconds, which is unneccessary to me. It's not much, but it would be cleaner for me if it stopped at milliseconds.</p><p>My regular time colums can be easily changed via 'View - Time display format' , but I'm really looking for time format for these 2 specific columns.</p><p>Is there a way I can do this ?</p><p>Thanks for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delta" rel="tag" title="see questions tagged &#39;delta&#39;">delta</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span> <span class="post-tag tag-link-milliseconds" rel="tag" title="see questions tagged &#39;milliseconds&#39;">milliseconds</span> <span class="post-tag tag-link-format" rel="tag" title="see questions tagged &#39;format&#39;">format</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '17, 12:09</strong></p><img src="https://secure.gravatar.com/avatar/21cfa2071214d5dacbb6d0cd9769a6d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jerioux&#39;s gravatar image" /><p><span>jerioux</span><br />
<span class="score" title="25 reputation points">25</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jerioux has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 May '17, 12:10</strong> </span></p></div></div><div id="comments-container-61215" class="comments-container"></div><div id="comment-tools-61215" class="comment-tools"></div><div class="clear"></div><div id="comment-61215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="61240"></span>

<div id="answer-container-61240" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61240-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61240-score" class="post-score" title="current number of votes">2</div><span id="post-61240-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="jerioux has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can add an additional "Delta time" column. To do so go to 'Preferences' -&gt; 'Appearance' -&gt; 'Columns'. Click the '+' and in 'Type' drop-down list choose 'Delta time displayed' or 'Delta time'.</p><p>These two types respect the configured time display format.</p><p>For 'TCP Delta' I'm not sure which field you mean.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 May '17, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-61240" class="comments-container"><span id="61243"></span><div id="comment-61243" class="comment"><div id="post-61243-score" class="comment-score"></div><div class="comment-text"><p>You solved my issue for the Delta!</p><p>I found something really interesting following that:</p><p>I used to have a column that was custom type with field name: frame.time_delta. This does EXACTLY the same job as the type 'Delta Time like you suggested but the latest is working with the timestamps we use. The one I used was stucked on nanoseconds. I corrected my column and it's now showing in milliseconds.</p><p>For the TCP Delta, I meant the filter: tcp.time_delta I search for that column in the type ''Delta time(conversation)'' but nothing is displayed. I found out on the internet this is a known bug in Wireshark. So I'll just have to work with it at a nano level.</p><p>It seems like whenever we are using ''custom'' type with a specific filter related to time, the timestamps are always at nano.</p></div><div id="comment-61243-info" class="comment-info"><span class="comment-age">(04 May '17, 11:48)</span> <span class="comment-user userinfo">jerioux</span></div></div></div><div id="comment-tools-61240" class="comment-tools"></div><div class="clear"></div><div id="comment-61240-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="61247"></span>

<div id="answer-container-61247" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61247-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61247-score" class="post-score" title="current number of votes">1</div><span id="post-61247-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For TCP Delta time you need to go to Packet Detail in the main Wireshark window. Right click on Transmission Control Protocol--&gt; Protocol Preferences --&gt; Calculate Conversation Time Stamp</p><p><strong>When you enable the above option, you will see an additional field "Timestamp" in the Detail section</strong> <img src="https://osqa-ask.wireshark.org/upfiles/TCP_delta.png" alt="alt text" /></p><p>Select the second option Time Since Previous Frame in this TCP stream--&gt; Right click --&gt; Apply As column</p><p>Keep the Time Column, Time Delta and TCP Delta column Adjacent to each other for ease of use and viewing</p><p>Hope that helps</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 May '17, 20:42</strong></p><img src="https://secure.gravatar.com/avatar/3e5e9d76a54debaa630d909e37048da8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deepacket&#39;s gravatar image" /><p><span>deepacket</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deepacket has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61247" class="comments-container"><span id="61252"></span><div id="comment-61252" class="comment"><div id="post-61252-score" class="comment-score"></div><div class="comment-text"><p>Hello Deepacket,</p><p>I already have this column, the same way you explained :) However the problem I reported for this specific column is that it goes up to nano seconds, and it's impossible to put it in milliseconds like the other Delta columns, which follows the time reference.</p><p>Have a nice day,</p></div><div id="comment-61252-info" class="comment-info"><span class="comment-age">(05 May '17, 05:24)</span> <span class="comment-user userinfo">jerioux</span></div></div></div><div id="comment-tools-61247" class="comment-tools"></div><div class="clear"></div><div id="comment-61247-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

