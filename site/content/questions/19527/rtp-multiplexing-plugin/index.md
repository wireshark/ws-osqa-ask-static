+++
type = "question"
title = "[closed] RTP Multiplexing Plugin"
description = '''Hi Expert, I am not a very experienced user of wireshark yet.I need to decode RTP Multiplex streams using Wireshark. As i understand wireshark not able to decode RTP Multiplexing packets,To decode RTP Multiplexing packet we need to load some dissector. So can you please let me know step by step how ...'''
date = "2013-03-15T00:28:00Z"
lastmod = "2013-03-15T01:02:00Z"
weight = 19527
keywords = [ "multiplexing", "ip", "rtp", "header" ]
aliases = [ "/questions/19527" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] RTP Multiplexing Plugin](/questions/19527/rtp-multiplexing-plugin)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19527-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19527-score" class="post-score" title="current number of votes">0</div><span id="post-19527-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Expert,</p><p>I am not a very experienced user of wireshark yet.I need to decode RTP Multiplex streams using Wireshark. As i understand wireshark not able to decode RTP Multiplexing packets,To decode RTP Multiplexing packet we need to load some dissector. So can you please let me know step by step how to load new plugin in wireshark for RTP multiplexing as i m not a coding software engineer.</p><p>I would appreciate if you could help me</p><p>Thanks// Vikas</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiplexing" rel="tag" title="see questions tagged &#39;multiplexing&#39;">multiplexing</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '13, 00:28</strong></p><img src="https://secure.gravatar.com/avatar/5a33fae2fd3e576257f0660435437267?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Viki&#39;s gravatar image" /><p><span>Viki</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Viki has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>15 Mar '13, 01:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-19527" class="comments-container"><span id="19528"></span><div id="comment-19528" class="comment"><div id="post-19528-score" class="comment-score"></div><div class="comment-text"><p>The answers still the same as <span>@anders</span> said in your very similar question :<a href="http://ask.wireshark.org/questions/19435/rtp-multiplex-dissector,">http://ask.wireshark.org/questions/19435/rtp-multiplex-dissector,</a> someone will have to write the code to do the dissection as it doesn't exist yet.</p></div><div id="comment-19528-info" class="comment-info"><span class="comment-age">(15 Mar '13, 01:02)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-19527" class="comment-tools"></div><div class="clear"></div><div id="comment-19527-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Jaap 15 Mar '13, 01:57

</div>

</div>

</div>

