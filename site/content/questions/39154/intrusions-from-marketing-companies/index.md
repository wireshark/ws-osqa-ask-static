+++
type = "question"
title = "Intrusions from marketing companies"
description = '''Please forgive me for asking a very basic question. Periodically, the Wireshark log on one of my PCs shows a flood of entries in white on a red background (I haven&#x27;t altered the default colour settings). They are all from external IP addresses using TCP, all of length 60, and the info column for eac...'''
date = "2015-01-15T04:25:00Z"
lastmod = "2015-01-15T06:11:00Z"
weight = 39154
keywords = [ "marketing", "intrusion" ]
aliases = [ "/questions/39154" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Intrusions from marketing companies](/questions/39154/intrusions-from-marketing-companies)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39154-score" class="post-score" title="current number of votes">0</div><span id="post-39154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please forgive me for asking a very basic question.</p><p>Periodically, the Wireshark log on one of my PCs shows a flood of entries in white on a red background (I haven't altered the default colour settings). They are all from external IP addresses using TCP, all of length 60, and the info column for each begins '80 {right-arrow} 49xxx [RST}'.</p><p>Following up some of these IP addresses reveals that a fair number come from marketing companies such as turn.com and Yahoo Ad Manager.</p><p>Should I be worried? And how can I stop this happening?</p><p>Many thanks in anticipation.</p><p>Jonathan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-marketing" rel="tag" title="see questions tagged &#39;marketing&#39;">marketing</span> <span class="post-tag tag-link-intrusion" rel="tag" title="see questions tagged &#39;intrusion&#39;">intrusion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '15, 04:25</strong></p><img src="https://secure.gravatar.com/avatar/10801cd50cbecda0f09eedafd2947fa3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jonathanc&#39;s gravatar image" /><p><span>jonathanc</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jonathanc has no accepted answers">0%</span></p></div></div><div id="comments-container-39154" class="comments-container"></div><div id="comment-tools-39154" class="comment-tools"></div><div class="clear"></div><div id="comment-39154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39157"></span>

<div id="answer-container-39157" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39157-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39157-score" class="post-score" title="current number of votes">0</div><span id="post-39157-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Should I be worried?</p></blockquote><p>Most certainly no.</p><blockquote><p>And how can I stop this happening?</p></blockquote><p>If you installed a lot of free tools, chances are pretty high that you've installed those background AD tools anlongside with the free tools. So, how can you stop that AD traffic? Be carefull what you install on your system!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '15, 06:05</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-39157" class="comments-container"><span id="39158"></span><div id="comment-39158" class="comment"><div id="post-39158-score" class="comment-score"></div><div class="comment-text"><p>Thanks, Kurt - I'm reassured. I thought I'd been very careful what I installed, but obviously not careful enough - I'll be more so in future!</p><p>Thanks again</p><p>Jonathan</p></div><div id="comment-39158-info" class="comment-info"><span class="comment-age">(15 Jan '15, 06:11)</span> <span class="comment-user userinfo">jonathanc</span></div></div></div><div id="comment-tools-39157" class="comment-tools"></div><div class="clear"></div><div id="comment-39157-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

