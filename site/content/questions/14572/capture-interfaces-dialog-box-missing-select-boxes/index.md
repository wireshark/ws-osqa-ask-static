+++
type = "question"
title = "&quot;Capture Interfaces&quot; dialog box missing select boxes"
description = '''Version 1.6.2 on Linux the &quot;Capture Interfaces&quot; dialog box (Menu Capture::Interfaces...) does not include selection boxes to select more than one interface. This is different than Figure 4.2 in the documentation, which shows a checkbox to the left of the interface icon and device identifier. How can...'''
date = "2012-09-27T07:48:00Z"
lastmod = "2012-09-28T08:51:00Z"
weight = 14572
keywords = [ "interfaces", "1.6", "linux" ]
aliases = [ "/questions/14572" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["Capture Interfaces" dialog box missing select boxes](/questions/14572/capture-interfaces-dialog-box-missing-select-boxes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14572-score" class="post-score" title="current number of votes">0</div><span id="post-14572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Version 1.6.2 on Linux the "Capture Interfaces" dialog box (Menu Capture::Interfaces...) does not include selection boxes to select more than one interface. This is different than Figure 4.2 in the documentation, which shows a checkbox to the left of the interface icon and device identifier.</p><p>How can I select more than one but less than <strong>all</strong> of the interfaces for a capture?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span> <span class="post-tag tag-link-1.6" rel="tag" title="see questions tagged &#39;1.6&#39;">1.6</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Sep '12, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/5c845e3932533743a7c4eed3ce8b0479?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sholton&#39;s gravatar image" /><p><span>sholton</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sholton has no accepted answers">0%</span></p></div></div><div id="comments-container-14572" class="comments-container"></div><div id="comment-tools-14572" class="comment-tools"></div><div class="clear"></div><div id="comment-14572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14573"></span>

<div id="answer-container-14573" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14573-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14573-score" class="post-score" title="current number of votes">2</div><span id="post-14573-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Install version 1.8.0 or later (1.8.2 is the current release). The ability to capture on multiple interfaces was introduced in 1.8.x.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Sep '12, 07:57</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-14573" class="comments-container"><span id="14589"></span><div id="comment-14589" class="comment"><div id="post-14589-score" class="comment-score"></div><div class="comment-text"><p>... and the web documentation is always for the latest release.</p></div><div id="comment-14589-info" class="comment-info"><span class="comment-age">(28 Sep '12, 06:22)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="14603"></span><div id="comment-14603" class="comment"><div id="post-14603-score" class="comment-score"></div><div class="comment-text"><p>Therefore we should allow for differentiation, though the use of the major-minor version number in the URL.</p></div><div id="comment-14603-info" class="comment-info"><span class="comment-age">(28 Sep '12, 08:51)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-14573" class="comment-tools"></div><div class="clear"></div><div id="comment-14573-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

