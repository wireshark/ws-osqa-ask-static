+++
type = "question"
title = "Gigamon Wireshark Integration"
description = '''Hi, Is there any integration between Gigamon to Wireshark except the Gigamon trailer? Thanks '''
date = "2017-03-19T00:14:00Z"
lastmod = "2017-03-19T04:07:00Z"
weight = 60167
keywords = [ "integration" ]
aliases = [ "/questions/60167" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Gigamon Wireshark Integration](/questions/60167/gigamon-wireshark-integration)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60167-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60167-score" class="post-score" title="current number of votes">0</div><span id="post-60167-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is there any integration between Gigamon to Wireshark except the Gigamon trailer?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-integration" rel="tag" title="see questions tagged &#39;integration&#39;">integration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '17, 00:14</strong></p><img src="https://secure.gravatar.com/avatar/f0d049fff33eee7fbeff10d3c08275d7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yakovd&#39;s gravatar image" /><p><span>yakovd</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yakovd has no accepted answers">0%</span></p></div></div><div id="comments-container-60167" class="comments-container"><span id="60169"></span><div id="comment-60169" class="comment"><div id="post-60169-score" class="comment-score"></div><div class="comment-text"><p>I'm not sure what you are asking. Wireshark dissects the gigamon trailer yes, that code was probably committed by someone at gigamon with access to the description of the packet content.</p></div><div id="comment-60169-info" class="comment-info"><span class="comment-age">(19 Mar '17, 00:38)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="60170"></span><div id="comment-60170" class="comment"><div id="post-60170-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the reply, I meant is there any other integration beside the trailer.</p></div><div id="comment-60170-info" class="comment-info"><span class="comment-age">(19 Mar '17, 02:21)</span> <span class="comment-user userinfo">yakovd</span></div></div><span id="60172"></span><div id="comment-60172" class="comment"><div id="post-60172-score" class="comment-score"></div><div class="comment-text"><p>I have no idea if gigamon "includes/integrates" wireshark in any if their products/offerings. Wireshark is a stand alone packet analyser. The project is mot "integrating" with gigamon.</p></div><div id="comment-60172-info" class="comment-info"><span class="comment-age">(19 Mar '17, 04:07)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-60167" class="comment-tools"></div><div class="clear"></div><div id="comment-60167-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

