+++
type = "question"
title = "Asterix CAT-240 Plugin for Wireshark"
description = '''Hi everyone; I&#x27;m looking for a plugin to observe asterix category 240 data in wireshark. I seached the internet for a while but I couldn&#x27;t find one. Is there any link that you can adress me?  Thank you...'''
date = "2016-07-13T06:30:00Z"
lastmod = "2016-07-13T06:30:00Z"
weight = 54034
keywords = [ "asterix", "plugin", "wireshark" ]
aliases = [ "/questions/54034" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Asterix CAT-240 Plugin for Wireshark](/questions/54034/asterix-cat-240-plugin-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54034-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54034-score" class="post-score" title="current number of votes">0</div><span id="post-54034-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone; I'm looking for a plugin to observe asterix category 240 data in wireshark. I seached the internet for a while but I couldn't find one. Is there any link that you can adress me? Thank you...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asterix" rel="tag" title="see questions tagged &#39;asterix&#39;">asterix</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '16, 06:30</strong></p><img src="https://secure.gravatar.com/avatar/fb483e28cde6e87252e8f52756592620?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ASamilG&#39;s gravatar image" /><p><span>ASamilG</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ASamilG has no accepted answers">0%</span></p></div></div><div id="comments-container-54034" class="comments-container"></div><div id="comment-tools-54034" class="comment-tools"></div><div class="clear"></div><div id="comment-54034-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

