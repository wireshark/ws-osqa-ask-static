+++
type = "question"
title = "Why Wireshark has stopped working when I open a 104MByte pcap file"
description = '''Why Wireshark has stopped working when I open a 104 MByte pcap file. But no problem to open a smaller file e.g. 5 MByte.'''
date = "2014-01-08T08:53:00Z"
lastmod = "2014-01-08T11:44:00Z"
weight = 28672
keywords = [ "large", "stop", "file", "working" ]
aliases = [ "/questions/28672" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why Wireshark has stopped working when I open a 104MByte pcap file](/questions/28672/why-wireshark-has-stopped-working-when-i-open-a-104mbyte-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28672-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28672-score" class="post-score" title="current number of votes">0</div><span id="post-28672-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why Wireshark has stopped working when I open a 104 MByte pcap file. But no problem to open a smaller file e.g. 5 MByte.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-large" rel="tag" title="see questions tagged &#39;large&#39;">large</span> <span class="post-tag tag-link-stop" rel="tag" title="see questions tagged &#39;stop&#39;">stop</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-working" rel="tag" title="see questions tagged &#39;working&#39;">working</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '14, 08:53</strong></p><img src="https://secure.gravatar.com/avatar/9d52e88dc9de22a0c815571bc8f671cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob1668&#39;s gravatar image" /><p><span>Bob1668</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob1668 has no accepted answers">0%</span></p></div></div><div id="comments-container-28672" class="comments-container"></div><div id="comment-tools-28672" class="comment-tools"></div><div class="clear"></div><div id="comment-28672-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28685"></span>

<div id="answer-container-28685" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28685-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28685-score" class="post-score" title="current number of votes">1</div><span id="post-28685-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You may run out of memory - see this wiki for more information: <a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory">Out of Memory</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '14, 11:44</strong></p><img src="https://secure.gravatar.com/avatar/94630d1ea1108afeafb344e884044d15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Boaz%20Galil&#39;s gravatar image" /><p><span>Boaz Galil</span><br />
<span class="score" title="56 reputation points">56</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Boaz Galil has no accepted answers">0%</span></p></div></div><div id="comments-container-28685" class="comments-container"></div><div id="comment-tools-28685" class="comment-tools"></div><div class="clear"></div><div id="comment-28685-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

