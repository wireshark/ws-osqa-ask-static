+++
type = "question"
title = "Finding Telephone Numbers with WIreShark"
description = '''I have some blackberries roaming around on my wireless network. Is there a way to use wireshark to find the telephone numbers for these blackberries? '''
date = "2011-08-30T04:13:00Z"
lastmod = "2011-08-30T13:18:00Z"
weight = 5951
keywords = [ "telephony" ]
aliases = [ "/questions/5951" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Finding Telephone Numbers with WIreShark](/questions/5951/finding-telephone-numbers-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5951-score" class="post-score" title="current number of votes">0</div><span id="post-5951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have some blackberries roaming around on my wireless network. Is there a way to use wireshark to find the telephone numbers for these blackberries?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telephony" rel="tag" title="see questions tagged &#39;telephony&#39;">telephony</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Aug '11, 04:13</strong></p><img src="https://secure.gravatar.com/avatar/6eb3558a0e039749235d743eda29461d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Belnando&#39;s gravatar image" /><p><span>Belnando</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Belnando has no accepted answers">0%</span></p></div></div><div id="comments-container-5951" class="comments-container"></div><div id="comment-tools-5951" class="comment-tools"></div><div class="clear"></div><div id="comment-5951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5970"></span>

<div id="answer-container-5970" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5970-score" class="post-score" title="current number of votes">2</div><span id="post-5970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Probably not. A Blackberry, like other mobile computers, has multiple network interfaces, and if it's using one network interface (Wi-Fi), that doesn't say anything about its other network interfaces (mobile phone interface) - you can get the Wi-Fi MAC address for the Blackberry from a network capture, but, unless some network traffic from the Blackberry happens to announce its phone number (and I'm not sure what traffic that would be - the user could, for example, be e-mailing their phone number to somebody, but they could be e-mailing a home or office landline number, or somebody else's phone number, or...).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '11, 13:18</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-5970" class="comments-container"></div><div id="comment-tools-5970" class="comment-tools"></div><div class="clear"></div><div id="comment-5970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

