+++
type = "question"
title = "Only showing USB Interfaces"
description = '''I&#x27;m new to Wireshark and I&#x27;m trying to learn the basics, but when I open up Wireshark it only shows my USB ports as interfaces. For some reason it&#x27;s not finding my NIC. Any suggestions?  '''
date = "2017-02-27T17:41:00Z"
lastmod = "2017-03-01T16:53:00Z"
weight = 59721
keywords = [ "nic", "usb" ]
aliases = [ "/questions/59721" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Only showing USB Interfaces](/questions/59721/only-showing-usb-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59721-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59721-score" class="post-score" title="current number of votes">0</div><span id="post-59721-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm new to Wireshark and I'm trying to learn the basics, but when I open up Wireshark it only shows my USB ports as interfaces. For some reason it's not finding my NIC. Any suggestions?</p><p><img src="https://i.imgur.com/0nrFujV.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nic" rel="tag" title="see questions tagged &#39;nic&#39;">nic</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '17, 17:41</strong></p><img src="https://secure.gravatar.com/avatar/7da046c52244d1a86b44c7afa8cd4111?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="danrobcampbell&#39;s gravatar image" /><p><span>danrobcampbell</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="danrobcampbell has no accepted answers">0%</span></p></img></div></div><div id="comments-container-59721" class="comments-container"><span id="59724"></span><div id="comment-59724" class="comment"><div id="post-59724-score" class="comment-score"></div><div class="comment-text"><p>Can you paste the contents of Help -&gt; About Wireshark dialog (the Wireshark tab)? We need all the text beginning with "Version ...", highlight it with the mouse and copy with Ctrl + C.</p></div><div id="comment-59724-info" class="comment-info"><span class="comment-age">(28 Feb '17, 03:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="59795"></span><div id="comment-59795" class="comment"><div id="post-59795-score" class="comment-score"></div><div class="comment-text"><p>My Wireshark is now just crashing on startup. I've removed and installed it a few times now and it's still just crashing. I'm going to try and troubleshoot more but I think this is actually an issue with my machine rather than the software. Thanks for your time.</p></div><div id="comment-59795-info" class="comment-info"><span class="comment-age">(01 Mar '17, 16:53)</span> <span class="comment-user userinfo">danrobcampbell</span></div></div></div><div id="comment-tools-59721" class="comment-tools"></div><div class="clear"></div><div id="comment-59721-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

