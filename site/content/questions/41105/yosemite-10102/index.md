+++
type = "question"
title = "Yosemite 10.10.2"
description = '''I recently upgraded my MacBook Pro to Yosemite 10.10.2 and encrypted the drive. I have successfully used Wireshark prior to the upgrade. When I try to launch wireshark now, the icon appears on the dock for about 45 seconds then disappears. I tried to uninstall and remove all components as suggested ...'''
date = "2015-04-01T11:18:00Z"
lastmod = "2015-04-01T15:47:00Z"
weight = 41105
keywords = [ "mac", "os", "yosemite" ]
aliases = [ "/questions/41105" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Yosemite 10.10.2](/questions/41105/yosemite-10102)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41105-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41105-score" class="post-score" title="current number of votes">0</div><span id="post-41105-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently upgraded my MacBook Pro to Yosemite 10.10.2 and encrypted the drive. I have successfully used Wireshark prior to the upgrade. When I try to launch wireshark now, the icon appears on the dock for about 45 seconds then disappears. I tried to uninstall and remove all components as suggested on the installation page, then reinstalled, without success. Any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-os" rel="tag" title="see questions tagged &#39;os&#39;">os</span> <span class="post-tag tag-link-yosemite" rel="tag" title="see questions tagged &#39;yosemite&#39;">yosemite</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 11:18</strong></p><img src="https://secure.gravatar.com/avatar/c117f6eb1333e38527e48f4147bc11ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Susan%20James&#39;s gravatar image" /><p><span>Susan James</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Susan James has no accepted answers">0%</span></p></div></div><div id="comments-container-41105" class="comments-container"><span id="41112"></span><div id="comment-41112" class="comment"><div id="post-41112-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark is this?</p></div><div id="comment-41112-info" class="comment-info"><span class="comment-age">(01 Apr '15, 15:47)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-41105" class="comment-tools"></div><div class="clear"></div><div id="comment-41105-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

