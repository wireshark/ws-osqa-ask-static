+++
type = "question"
title = "Can&#x27;t open recent file: Permission denied"
description = '''Hello, I am running Wireshark Version 1.12.10 (v1.12.10-0-g7f56a20 from master-1.12); When I apply a profile I created I get this msg from Wireshark; Can&#x27;t open recent file &quot;C:&#92;Users&#92;xxxxxx&#92;AppData&#92;Roaming&#92;Wireshark&#92;recent&quot;: Permission denied. I have admon right on my laptop, I have checked my permi...'''
date = "2016-03-17T07:59:00Z"
lastmod = "2016-03-21T10:25:00Z"
weight = 51006
keywords = [ "denied", "permission" ]
aliases = [ "/questions/51006" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't open recent file: Permission denied](/questions/51006/cant-open-recent-file-permission-denied)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51006-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51006-score" class="post-score" title="current number of votes">0</div><span id="post-51006-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am running Wireshark Version 1.12.10 (v1.12.10-0-g7f56a20 from master-1.12);</p><p>When I apply a profile I created I get this msg from Wireshark;</p><p>Can't open recent file "C:\Users\xxxxxx\AppData\Roaming\Wireshark\recent": Permission denied.</p><p>I have admon right on my laptop, I have checked my permissions to the recent file, tis looks good as well.</p><p>Any ideas?</p><p>GXM</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-denied" rel="tag" title="see questions tagged &#39;denied&#39;">denied</span> <span class="post-tag tag-link-permission" rel="tag" title="see questions tagged &#39;permission&#39;">permission</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Mar '16, 07:59</strong></p><img src="https://secure.gravatar.com/avatar/1e3f59f140fd59623e2ec0f96d55a766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GXM&#39;s gravatar image" /><p><span>GXM</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GXM has no accepted answers">0%</span></p></div></div><div id="comments-container-51006" class="comments-container"></div><div id="comment-tools-51006" class="comment-tools"></div><div class="clear"></div><div id="comment-51006-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51074"></span>

<div id="answer-container-51074" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51074-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51074-score" class="post-score" title="current number of votes">0</div><span id="post-51074-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is probably an issue with windows roam profile file sharing. Check the links below to see if this helps at all. It is from MSN and it is recommended security settings for roaming profiles</p><p><a href="https://msdn.microsoft.com/en-us/library/cc757013(v=ws.10).aspx">link text</a></p><p><a href="http://technet.microsoft.com/en-us/library/cc737633(WS.10).aspx">link text</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '16, 10:25</strong></p><img src="https://secure.gravatar.com/avatar/7dc1fee5b4e29c4e6cc3d5059312aac7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="msmorten&#39;s gravatar image" /><p><span>msmorten</span><br />
<span class="score" title="4 reputation points">4</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="msmorten has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Mar '16, 10:29</strong> </span></p></div></div><div id="comments-container-51074" class="comments-container"></div><div id="comment-tools-51074" class="comment-tools"></div><div class="clear"></div><div id="comment-51074-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

