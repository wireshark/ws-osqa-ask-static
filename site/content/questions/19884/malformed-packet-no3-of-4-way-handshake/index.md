+++
type = "question"
title = "Malformed Packet (no3 of 4-way-handshake)"
description = '''Hello,  Since last week I’ve been trying to capture the 4-way-handshake of my own router without success. I tried on different PCs, on different distances to the router, even on different routers, and I even ordered an additional wifi-card that was recommended for use with backtrack. The result is a...'''
date = "2013-03-27T12:54:00Z"
lastmod = "2013-04-03T13:25:00Z"
weight = 19884
keywords = [ "packet", "malformed" ]
aliases = [ "/questions/19884" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed Packet (no3 of 4-way-handshake)](/questions/19884/malformed-packet-no3-of-4-way-handshake)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19884-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19884-score" class="post-score" title="current number of votes">0</div><span id="post-19884-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Since last week I’ve been trying to capture the 4-way-handshake of my own router without success. I tried on different PCs, on different distances to the router, even on different routers, and I even ordered an additional wifi-card that was recommended for use with backtrack. The result is always the same. In Wireshark:</p><p>In the top section it always shows that exactly 1 or 4, 2 of 4 and 4 of 4 are malformed and always exactly on 3 of 4 it does not show this message. But when I click on 3 of 4 in the top section, on the bottom of the middle section it also ends up telling, that the packet is malformed. This is always the case, whatever I do:</p><p>2385, 19.882257, D-Link_18:af:68, AskeyCom_f4:ee:36, EAPOL, 131, Key (Message 1 of 4)[Malformed Packet] 2387, 19.883838, AskeyCom_f4:ee:36, D-Link_18:af:68, EAPOL, 155, Key (Message 2 of 4)[Malformed Packet] 7033, 34.142877, D-Link_18:af:68, AsustekC_2f:93:df, EAPOL, 235, Key (Message 3 of 4)* 2771, 21.117310, AskeyCom_f4:ee:36, D-Link_18:af:68, EAPOL, 133, Key (Message 4 of 4)[Malformed Packet]</p><ul><li>Frame 7033: 235 bytes on wire (1880 bits), 235 bytes captured (1880 bits) IEEE 802.11 Data, Flags: ......F.. Logical-Link Control 802.1X Authentication [Malformed Packet: EAPOL]</li></ul><p>I hope anybody can help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '13, 12:54</strong></p><img src="https://secure.gravatar.com/avatar/a983910487ed7deb204f4769e43d169d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Markey&#39;s gravatar image" /><p><span>Markey</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Markey has no accepted answers">0%</span></p></div></div><div id="comments-container-19884" class="comments-container"><span id="20068"></span><div id="comment-20068" class="comment"><div id="post-20068-score" class="comment-score"></div><div class="comment-text"><p>Noone has a clue?</p></div><div id="comment-20068-info" class="comment-info"><span class="comment-age">(03 Apr '13, 13:25)</span> <span class="comment-user userinfo">Markey</span></div></div></div><div id="comment-tools-19884" class="comment-tools"></div><div class="clear"></div><div id="comment-19884-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

