+++
type = "question"
title = "address of pcap file"
description = '''hello experts, i wanted to know what part of source code contains the file pointer which contains path to pcap file when u try to open a pcap file in wireshark . like for example: when i click file/open file_dlg_open_cb is called  then you browse the menu for pcap file now i wanted to know what part...'''
date = "2012-03-06T23:21:00Z"
lastmod = "2012-03-06T23:21:00Z"
weight = 9414
keywords = [ "file" ]
aliases = [ "/questions/9414" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [address of pcap file](/questions/9414/address-of-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9414-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9414-score" class="post-score" title="current number of votes">0</div><span id="post-9414-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello experts,</p><p>i wanted to know what part of source code contains the file pointer which contains path to pcap file when u try to open a pcap file in wireshark . like for example: when i click file/open file_dlg_open_cb is called then you browse the menu for pcap file now i wanted to know what part of code stores this address or path to pcap file.</p><p>regards, sangamesh patil</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '12, 23:21</strong></p><img src="https://secure.gravatar.com/avatar/6cb6685f12bd537f0f2e1e86a591e940?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sangmeshp&#39;s gravatar image" /><p><span>sangmeshp</span><br />
<span class="score" title="36 reputation points">36</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sangmeshp has no accepted answers">0%</span></p></div></div><div id="comments-container-9414" class="comments-container"></div><div id="comment-tools-9414" class="comment-tools"></div><div class="clear"></div><div id="comment-9414-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

