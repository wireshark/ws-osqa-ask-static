+++
type = "question"
title = "Packet Length description"
description = '''Hi What do &quot;Packet Length&quot; columns mean? Such as &quot;Count, Average, Min val, Max val, Rate (ms), Percent, Burst Rate, and Burst start&quot;. There is no description for these columns in the user guide. Thank you'''
date = "2015-07-18T01:55:00Z"
lastmod = "2015-07-18T04:44:00Z"
weight = 44279
keywords = [ "statistics" ]
aliases = [ "/questions/44279" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Packet Length description](/questions/44279/packet-length-description)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44279-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44279-score" class="post-score" title="current number of votes">1</div><span id="post-44279-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi What do "Packet Length" columns mean? Such as "Count, Average, Min val, Max val, Rate (ms), Percent, Burst Rate, and Burst start". There is no description for these columns in the user guide. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '15, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/e741db8f02fe6965c0f885aebd047cd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mohammed&#39;s gravatar image" /><p><span>Mohammed</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mohammed has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jul '15, 02:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/285b1f0f4caadc088a38c40aea22feba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lekensteyn&#39;s gravatar image" /><p><span>Lekensteyn</span><br />
<span class="score" title="2213 reputation points"><span>2.2k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="24 badges"><span class="bronze">●</span><span class="badgecount">24</span></span></p></div></div><div id="comments-container-44279" class="comments-container"></div><div id="comment-tools-44279" class="comment-tools"></div><div class="clear"></div><div id="comment-44279-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44282"></span>

<div id="answer-container-44282" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44282-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44282-score" class="post-score" title="current number of votes">1</div><span id="post-44282-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Mohammed has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>An explanation of the burst related columns can be found here. <a href="https://ask.wireshark.org/questions/42545/what-data-does-burst-rate-and-burst-start-provide-in-statistics-packet-lengths">https://ask.wireshark.org/questions/42545/what-data-does-burst-rate-and-burst-start-provide-in-statistics-packet-lengths</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '15, 04:44</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-44282" class="comments-container"></div><div id="comment-tools-44282" class="comment-tools"></div><div class="clear"></div><div id="comment-44282-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

