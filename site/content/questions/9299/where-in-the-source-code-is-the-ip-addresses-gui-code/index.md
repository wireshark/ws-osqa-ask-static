+++
type = "question"
title = "Where in the source code is the IP Addresses GUI code"
description = '''I am looking for the GUI code for the Statistics -&amp;gt; IP Addresses window. Is there a naming convention for windows to GTK source code that I can use to find that window or other files?'''
date = "2012-03-01T19:46:00Z"
lastmod = "2012-03-02T15:29:00Z"
weight = 9299
keywords = [ "ip", "gui", "gtk", "addresses" ]
aliases = [ "/questions/9299" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Where in the source code is the IP Addresses GUI code](/questions/9299/where-in-the-source-code-is-the-ip-addresses-gui-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9299-score" class="post-score" title="current number of votes">0</div><span id="post-9299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for the GUI code for the Statistics -&gt; IP Addresses window. Is there a naming convention for windows to GTK source code that I can use to find that window or other files?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span> <span class="post-tag tag-link-addresses" rel="tag" title="see questions tagged &#39;addresses&#39;">addresses</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '12, 19:46</strong></p><img src="https://secure.gravatar.com/avatar/6b740d377988bc6b3351922b57a0459a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zewrestler&#39;s gravatar image" /><p><span>zewrestler</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zewrestler has no accepted answers">0%</span></p></div></div><div id="comments-container-9299" class="comments-container"><span id="9306"></span><div id="comment-9306" class="comment"><div id="post-9306-score" class="comment-score"></div><div class="comment-text"><p>Well, at first it would be good to tell us which branch you're working with, either one of the 1.x branches or trunk.</p></div><div id="comment-9306-info" class="comment-info"><span class="comment-age">(02 Mar '12, 04:04)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="9319"></span><div id="comment-9319" class="comment"><div id="post-9319-score" class="comment-score"></div><div class="comment-text"><p>I'm running the developmental 1.7.1</p></div><div id="comment-9319-info" class="comment-info"><span class="comment-age">(02 Mar '12, 15:29)</span> <span class="comment-user userinfo">zewrestler</span></div></div></div><div id="comment-tools-9299" class="comment-tools"></div><div class="clear"></div><div id="comment-9299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

