+++
type = "question"
title = "open ws trace file on another pc"
description = '''Hello, I cannot open a ws trace on another pc? I try yet to save or export but when I open the file on another pc I don&#x27;t get what I observe on the source PC?!? Can you help me? Many tanks for your help!!! Phil.'''
date = "2011-08-23T02:52:00Z"
lastmod = "2011-08-23T03:34:00Z"
weight = 5819
keywords = [ "readanotherpc" ]
aliases = [ "/questions/5819" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [open ws trace file on another pc](/questions/5819/open-ws-trace-file-on-another-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5819-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5819-score" class="post-score" title="current number of votes">0</div><span id="post-5819-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I cannot open a ws trace on another pc? I try yet to save or export but when I open the file on another pc I don't get what I observe on the source PC?!? Can you help me? Many tanks for your help!!! Phil.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-readanotherpc" rel="tag" title="see questions tagged &#39;readanotherpc&#39;">readanotherpc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '11, 02:52</strong></p><img src="https://secure.gravatar.com/avatar/6119f0765942e0483c36878befd317dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PhilLu&#39;s gravatar image" /><p><span>PhilLu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PhilLu has no accepted answers">0%</span></p></div></div><div id="comments-container-5819" class="comments-container"><span id="5820"></span><div id="comment-5820" class="comment"><div id="post-5820-score" class="comment-score"></div><div class="comment-text"><p>You'll need to provide more info in your question.</p><p>Does the second machine successfully open the file? If not what error is reported.</p><p>If the second machine can open the file, what is different about it?</p></div><div id="comment-5820-info" class="comment-info"><span class="comment-age">(23 Aug '11, 03:34)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-5819" class="comment-tools"></div><div class="clear"></div><div id="comment-5819-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

