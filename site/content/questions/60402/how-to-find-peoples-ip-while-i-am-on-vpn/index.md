+++
type = "question"
title = "How to find Peoples IP while i am on VPN?"
description = '''So is there a way to find peoples IP address while i am on a VPN? When my vpn is active i am never able to find it, but as soon as i deactivate it i am able to find IP´s. Thank you!'''
date = "2017-03-29T04:59:00Z"
lastmod = "2017-03-29T04:59:00Z"
weight = 60402
keywords = [ "ip", "vpn" ]
aliases = [ "/questions/60402" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to find Peoples IP while i am on VPN?](/questions/60402/how-to-find-peoples-ip-while-i-am-on-vpn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60402-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60402-score" class="post-score" title="current number of votes">0</div><span id="post-60402-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So is there a way to find peoples IP address while i am on a VPN? When my vpn is active i am never able to find it, but as soon as i deactivate it i am able to find IP´s.</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '17, 04:59</strong></p><img src="https://secure.gravatar.com/avatar/75f51c3780317f19d228cfbd35d6e3d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HDinis09&#39;s gravatar image" /><p><span>HDinis09</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HDinis09 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Mar '17, 05:00</strong> </span></p></div></div><div id="comments-container-60402" class="comments-container"></div><div id="comment-tools-60402" class="comment-tools"></div><div class="clear"></div><div id="comment-60402-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

