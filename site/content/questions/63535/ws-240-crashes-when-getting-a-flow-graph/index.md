+++
type = "question"
title = "WS 2.4.0 crashes when getting a Flow Graph"
description = '''Hi, I have just upgraded to WS 2.4.0 on windows 2 servers and it crashes on both when trying to get a Flow Graph in the Statistics menu. Servers are only used for WS and they are X5560 dual core CPU @ 2.8Ghz with 48GB RAM an 64 bit OS: Windows Server 2008 R2 Enterprise SP1 servers. Former release di...'''
date = "2017-08-28T07:34:00Z"
lastmod = "2017-08-28T07:49:00Z"
weight = 63535
keywords = [ "wireshark_crashed" ]
aliases = [ "/questions/63535" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WS 2.4.0 crashes when getting a Flow Graph](/questions/63535/ws-240-crashes-when-getting-a-flow-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63535-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63535-score" class="post-score" title="current number of votes">0</div><span id="post-63535-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have just upgraded to WS 2.4.0 on windows 2 servers and it crashes on both when trying to get a Flow Graph in the Statistics menu. Servers are only used for WS and they are X5560 dual core CPU @ 2.8Ghz with 48GB RAM an 64 bit OS: Windows Server 2008 R2 Enterprise SP1 servers. Former release didn't have this problem.</p><p>Any idea if this is a bug in this WS release or something on server/OS level?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark_crashed" rel="tag" title="see questions tagged &#39;wireshark_crashed&#39;">wireshark_crashed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '17, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/4fc43c83d14e6cb53bf36dd8013dbcf1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="profke&#39;s gravatar image" /><p><span>profke</span><br />
<span class="score" title="10 reputation points">10</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="profke has no accepted answers">0%</span></p></div></div><div id="comments-container-63535" class="comments-container"></div><div id="comment-tools-63535" class="comment-tools"></div><div class="clear"></div><div id="comment-63535-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63536"></span>

<div id="answer-container-63536" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63536-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63536-score" class="post-score" title="current number of votes">0</div><span id="post-63536-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like a bug. You'll need to report it at the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a> (if not already reported), adding a capture will improve the chances of a speedy fix.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Aug '17, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63536" class="comments-container"></div><div id="comment-tools-63536" class="comment-tools"></div><div class="clear"></div><div id="comment-63536-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

