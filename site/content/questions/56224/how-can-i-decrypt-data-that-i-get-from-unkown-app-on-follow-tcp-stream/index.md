+++
type = "question"
title = "How can I decrypt data that i get from unkown app on Follow Tcp Stream"
description = '''How can I decrypt data that i get from unkown app on Follow Tcp Stream this data contain user and password  This is the code that i get in Follow TCP STREAM by ASCII .(.@~.z.?~.Q.G...dA&amp;gt;..G..!..........w[AN...(....=JB........P.jb.|........E.)....iC...=...z.?~.Yb gA........f..tP.t-?....%r.$.._%o&amp;a...'''
date = "2016-10-07T10:28:00Z"
lastmod = "2016-10-07T10:28:00Z"
weight = 56224
keywords = [ "decryption" ]
aliases = [ "/questions/56224" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I decrypt data that i get from unkown app on Follow Tcp Stream](/questions/56224/how-can-i-decrypt-data-that-i-get-from-unkown-app-on-follow-tcp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56224-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56224-score" class="post-score" title="current number of votes">0</div><span id="post-56224-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I decrypt data that i get from unkown app on Follow Tcp Stream this data contain user and password</p><p>This is the code that i get in Follow TCP STREAM by ASCII</p><pre><code>.([email protected]~.z.?~.Q.G...dA&gt;..G..!..........w[AN...(....=JB........P.jb.|........E.)....iC...=...z.?~.Yb  gA........f..tP.t-?....%r.$.._%o&amp;.......-=q..Z.Q.4bo
45Y.&quot;V.o.7p..m...fH....)9......L....^?./.[#.........n.h4...V.5.
.-.3?..j......Y.T...J.c..L...H..W.t..)&amp;K..8...]..P&lt;..:i.............8..~..Jg..Op..2.O+.K.&amp;...y.;...n... ..Z.H|9....T...Y $9Z..JT......i?..
.%.............&gt;..SE.J...........&quot;[email protected]?6z.sH.rf..5 ..&amp;............C.~.na..}...&lt;....-
.,&lt;4U..UN...B.X.h.).X.f..D......}..F3.d..$. ...R.o5.....).[|..2.^u[=....}......#..;,.2
.y..Y..8...?..W..g
Q.....A...{.c!.2..Wa3.......Q..z.?~.v.v...]&quot;..d...s..e..Q.&gt;.......d.L.J.g+e.....[.t...........~..zD.c....:.....o........;G.w.%yC.6..r.m.].....N..L.f......Y+...
oL..:..\Kpb0.9..
.4{./.....oqI]...%.0..&amp;[......G.y#....h......%[email protected]{8vR...}.S+H......fv.X..q..#...%%p&gt;..N.;[email protected]]Y..i..paR.....&amp;.&lt;....Wd....... d&quot;.......h..%.......F.n...
...h......%..h.M.......Lt...E.,:/.....c&lt; ...vAG{8vR...}.S+H......fv.X..q..#...%%p&gt;..N.;[email protected]]Y..i..paR.....&amp;.&lt;....Wd....... d&quot;.......h..%.......F.n...
...h......%..h.M......j.t...q....t.....m..;.l..D..x.5.G9......Cv.a.{.&lt;..5.........&quot;... ..h....eQv_.x.B.S.WM...
./2..=..}..y.ob..aK...U.i....S...d..$:l....o.P-,....Q...O..0~.....r..O...W.f.....bt......l.&#39;.i..........S........#D.W..u..........Q~q...I..ZwA../B....[...~.%....y.ygx|...M.....@n...l&gt;%_:..0ee%.U......V......K...z.+.E... ..4w+78.    L$.F.....tm/e.9..a..8Nn.. ...
k:&lt;..e5.1..3r.z.t9... ..^..].u..*2F...0.+...e{[email protected]^&lt;.... ;B&lt;lT..i&quot;.....h#d..t.....O..!...a.....o.4n..9....2..cE..#.L..ur*T...F......%T.lqL.
V
L....X..p#.!........&lt;o.6...g.5....4X....L.&#39;S..................../D.....
..5.7.....KD.LKDSr#...Ur    .rr 7..........#.y.....T&quot;.{ :o.8....Q.. .6..........C....|fb..W&lt;...Y.\@...).....&gt;...R...    iR=..,..5w{@...&gt;BZ.n..  ....v=QK4e........y.d.)]...O..~J8^|.,..8P...su...{w.+G.f4.f...
...D%..]. F...L......?..R~...7E.G.2_52|.#..V..R.0..m..O).),4.Z.......!....)..Q.Of./.|....&gt;.U..(d..  .?W[H8..)#&quot;.......~C.\.....!].....~l....l.-...wG1*&amp;g.f....4.{...Q.=K.-J.}8.0.1.&quot;w......,..e..Q.+(X..).u&gt;&gt;.9.(*..B</code></pre><p><img src="https://osqa-ask.wireshark.org/upfiles/tcp_3Fc9jHn.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '16, 10:28</strong></p><img src="https://secure.gravatar.com/avatar/a8a264623937b8de93449ec606d3912b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mebaze&#39;s gravatar image" /><p><span>mebaze</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mebaze has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Oct '16, 12:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-56224" class="comments-container"></div><div id="comment-tools-56224" class="comment-tools"></div><div class="clear"></div><div id="comment-56224-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

