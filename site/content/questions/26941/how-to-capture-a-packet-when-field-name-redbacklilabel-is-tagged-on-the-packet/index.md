+++
type = "question"
title = "How to capture a packet when field name redbackli.label is tagged on the packet"
description = '''How to capture a packet when field name redbackli.label is tagged on the packet. through tshark command, i knew how to find out in wireshark, pls let me know using tshark comamnd with example.'''
date = "2013-11-13T06:19:00Z"
lastmod = "2013-11-13T07:20:00Z"
weight = 26941
keywords = [ "capture-filter", "lawfulintercept" ]
aliases = [ "/questions/26941" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture a packet when field name redbackli.label is tagged on the packet](/questions/26941/how-to-capture-a-packet-when-field-name-redbacklilabel-is-tagged-on-the-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26941-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26941-score" class="post-score" title="current number of votes">0</div><span id="post-26941-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to capture a packet when field name redbackli.label is tagged on the packet. through tshark command, i knew how to find out in wireshark, pls let me know using tshark comamnd with example.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-lawfulintercept" rel="tag" title="see questions tagged &#39;lawfulintercept&#39;">lawfulintercept</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '13, 06:19</strong></p><img src="https://secure.gravatar.com/avatar/87a569b3f0938ff4d79f6dfb3ccff316?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Velpandian&#39;s gravatar image" /><p><span>Velpandian</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Velpandian has no accepted answers">0%</span></p></div></div><div id="comments-container-26941" class="comments-container"></div><div id="comment-tools-26941" class="comment-tools"></div><div class="clear"></div><div id="comment-26941-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26948"></span>

<div id="answer-container-26948" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26948-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26948-score" class="post-score" title="current number of votes">1</div><span id="post-26948-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Capture filters are limited in their ability to decode protocols, so can't capture on most protocol fields unlike display filters.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '13, 07:20</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Nov '13, 07:21</strong> </span></p></div></div><div id="comments-container-26948" class="comments-container"></div><div id="comment-tools-26948" class="comment-tools"></div><div class="clear"></div><div id="comment-26948-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

