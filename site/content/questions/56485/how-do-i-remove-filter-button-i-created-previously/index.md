+++
type = "question"
title = "How do I remove filter button I created previously"
description = '''Hello I am new to Wireshark, so please be gentle with me :)  I added a filter button to the right of the filter expression box. Whilst it was easy to add the button currently I cannot see a way to remove it so it no longer appears as a button. can someone please tell me how to remove the button I cr...'''
date = "2016-10-18T00:59:00Z"
lastmod = "2016-10-18T11:49:00Z"
weight = 56485
keywords = [ "filter", "button" ]
aliases = [ "/questions/56485" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I remove filter button I created previously](/questions/56485/how-do-i-remove-filter-button-i-created-previously)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56485-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56485-score" class="post-score" title="current number of votes">0</div><span id="post-56485-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I am new to Wireshark, so please be gentle with me :)</p><p>I added a filter button to the right of the filter expression box. Whilst it was easy to add the button currently I cannot see a way to remove it so it no longer appears as a button.</p><p>can someone please tell me how to remove the button I created.</p><p>Thanks E Brant</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-button" rel="tag" title="see questions tagged &#39;button&#39;">button</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '16, 00:59</strong></p><img src="https://secure.gravatar.com/avatar/ff39c11ae2cb05528da757366e76d84b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EBrant&#39;s gravatar image" /><p><span>EBrant</span><br />
<span class="score" title="1 reputation points">1</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EBrant has no accepted answers">0%</span></p></div></div><div id="comments-container-56485" class="comments-container"></div><div id="comment-tools-56485" class="comment-tools"></div><div class="clear"></div><div id="comment-56485-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56486"></span>

<div id="answer-container-56486" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56486-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56486-score" class="post-score" title="current number of votes">2</div><span id="post-56486-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to Edit -&gt; Preferences -&gt; Filter Expressions</p><p>There should be a list of your buttons you can edit or remove.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Oct '16, 01:36</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-56486" class="comments-container"><span id="56496"></span><div id="comment-56496" class="comment"><div id="post-56496-score" class="comment-score"></div><div class="comment-text"><p>Thanks very much Jasper for taking the time to answer my question :) EBrant</p></div><div id="comment-56496-info" class="comment-info"><span class="comment-age">(18 Oct '16, 10:33)</span> <span class="comment-user userinfo">EBrant</span></div></div><span id="56497"></span><div id="comment-56497" class="comment"><div id="post-56497-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-56497-info" class="comment-info"><span class="comment-age">(18 Oct '16, 11:49)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-56486" class="comment-tools"></div><div class="clear"></div><div id="comment-56486-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

