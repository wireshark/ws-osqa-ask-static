+++
type = "question"
title = "A few Questions"
description = '''Configuration information. Server: X86 Cisco UCS OS: Windows 2012 R2 Std Use NMS solution of Watch Tech WireShark program is installed but occasionally uses. In the same network where the server is installed, the IPSCAN security equipment. IPSCAN Role: Probe MAC false advertising and unauthorized de...'''
date = "2016-09-05T05:41:00Z"
lastmod = "2016-09-05T05:41:00Z"
weight = 55333
keywords = [ "wireshark" ]
aliases = [ "/questions/55333" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [A few Questions](/questions/55333/a-few-questions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55333-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55333-score" class="post-score" title="current number of votes">0</div><span id="post-55333-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Configuration information.</p><p>Server: X86 Cisco UCS</p><p>OS: Windows 2012 R2 Std</p><p>Use NMS solution of Watch Tech</p><p>WireShark program is installed but occasionally uses.</p><p>In the same network where the server is installed, the IPSCAN security equipment.</p><p>IPSCAN Role: Probe MAC false advertising and unauthorized devices intended for continuous as the GW MAC (Unicast).</p><p>Issue detais IPSCAN security equipment should be updated to Probe MAC (false) for false advertising off target device to capture the ARP MAC (GW direction) of the server to the Windows OS.</p><p>ARP Reply is sent to the device as Unicast, it was the situation should not change the ARP cache communication occurs with the destination address and see at any other appropriate server.</p><p>Questions</p><p>Capture the ARP Reply Packet going to different way, by the NPF Driver is installed WireShark WinPcap Library or the case or to update the ARP inquiry if possible.</p><p>Symptoms that occur in state WireShark is not working</p><p>Fragmented tests, symptoms that disappear after the deletion is confirmed that WireShark. (When deleting options: Delete, including WinPcap).</p><p>if you have solution, we need that.</p><p>thanks you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '16, 05:41</strong></p><img src="https://secure.gravatar.com/avatar/a2cde5ce3e8b416c90385b9a088dbde9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="minwoo%20lee&#39;s gravatar image" /><p><span>minwoo lee</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="minwoo lee has no accepted answers">0%</span></p></div></div><div id="comments-container-55333" class="comments-container"></div><div id="comment-tools-55333" class="comment-tools"></div><div class="clear"></div><div id="comment-55333-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

