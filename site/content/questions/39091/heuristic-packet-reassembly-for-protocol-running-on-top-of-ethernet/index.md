+++
type = "question"
title = "Heuristic packet reassembly for protocol running on top of Ethernet"
description = '''I have a Heuristic eth dissector what needs to reassemble multiple packets. I know this is possible with UDP and TCP, but is there any way to do it if I don&#x27;t have either of those packet types? Is there an example of this? Thanks'''
date = "2015-01-13T06:49:00Z"
lastmod = "2015-01-13T06:49:00Z"
weight = 39091
keywords = [ "heuristic", "heuristics", "reassemble", "reassembly" ]
aliases = [ "/questions/39091" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Heuristic packet reassembly for protocol running on top of Ethernet](/questions/39091/heuristic-packet-reassembly-for-protocol-running-on-top-of-ethernet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39091-score" class="post-score" title="current number of votes">1</div><span id="post-39091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a Heuristic eth dissector what needs to reassemble multiple packets. I know this is possible with UDP and TCP, but is there any way to do it if I don't have either of those packet types? Is there an example of this?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-heuristic" rel="tag" title="see questions tagged &#39;heuristic&#39;">heuristic</span> <span class="post-tag tag-link-heuristics" rel="tag" title="see questions tagged &#39;heuristics&#39;">heuristics</span> <span class="post-tag tag-link-reassemble" rel="tag" title="see questions tagged &#39;reassemble&#39;">reassemble</span> <span class="post-tag tag-link-reassembly" rel="tag" title="see questions tagged &#39;reassembly&#39;">reassembly</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '15, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/b99b95035ae2498a1cc7a09ff324f78d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="synportack24&#39;s gravatar image" /><p><span>synportack24</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="synportack24 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Jan '15, 15:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-39091" class="comments-container"></div><div id="comment-tools-39091" class="comment-tools"></div><div class="clear"></div><div id="comment-39091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

