+++
type = "question"
title = "Windows 10 blocked traffic"
description = '''Hello All, I&#x27;ve been pulling my hair for 2 days and am unable to figure out why this is happening. I truly hope someone can help me out. I have the following devices connected on a small network: MY SETUP Computer A - Windows 10 Pro Computer B - Windows 10 Home WiFi Access Point All Windows firewall...'''
date = "2015-08-04T19:39:00Z"
lastmod = "2015-08-04T19:39:00Z"
weight = 44848
keywords = [ "windows", "ports", "xbmc" ]
aliases = [ "/questions/44848" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 10 blocked traffic](/questions/44848/windows-10-blocked-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44848-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44848-score" class="post-score" title="current number of votes">0</div><span id="post-44848-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>I've been pulling my hair for 2 days and am unable to figure out why this is happening. I truly hope someone can help me out.</p><p>I have the following devices connected on a small network:</p><p><strong>MY SETUP</strong> Computer A - Windows 10 Pro Computer B - Windows 10 Home WiFi Access Point</p><p>All Windows firewalls are turned off AVG AV running on both machines</p><p>The devices are connected to 8-port un-managed gigabit switch. The switch is connected to ZyXell USG20 firewall/router</p><p>Galaxy S5 mobile phone connected to the WAP</p><p>All devices are getting (reserved) IP addresses from the ZyXell firewall/router so they all on the same subnet.</p><p>I have an application running on both of the computers. It's the same exact app (XBMC if you are curious). The app has internal web server listening to port 8088 (I can configure any port I need).</p><p>On the Galaxy S5 mobile phone I have a small remote control app that connects to the app on the PC and provide control functionality. All I configure is the IP address and target port.</p><p><strong>My ISSUE</strong> I am able to connect the mobile app to Computer A but not to Computer B</p><p><strong>I TRIED THE FOLLOWING:</strong> Connect from computer A to computer B (browser) - Success Connect from mobile to Computer B (Browser) - Failed Ping Computer B from the mobile device - success Ping the mobile device from Computer B - Success</p><p>In WireShark running on Computer B I can see the incoming traffic from the mobile device when I attempt to connect the app.</p><p>I'm pretty lost....</p><p>Thanks in advance</p><p>-Eyal</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-ports" rel="tag" title="see questions tagged &#39;ports&#39;">ports</span> <span class="post-tag tag-link-xbmc" rel="tag" title="see questions tagged &#39;xbmc&#39;">xbmc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Aug '15, 19:39</strong></p><img src="https://secure.gravatar.com/avatar/00b0f0a8a0e775ea4eb0588629fc4ae8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bluemanta&#39;s gravatar image" /><p><span>bluemanta</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bluemanta has no accepted answers">0%</span></p></div></div><div id="comments-container-44848" class="comments-container"></div><div id="comment-tools-44848" class="comment-tools"></div><div class="clear"></div><div id="comment-44848-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

