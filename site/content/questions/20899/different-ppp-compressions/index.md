+++
type = "question"
title = "Different PPP compressions"
description = '''Hi In PPP CCP there is an option to enable Deflate(zlib), BSD and Magnalink compressions. What happens if they negotiate and agree on all three? How those packets would look like, and which compression algorithm would they use? I&#x27;m curious, the MPPC&#x27;s rfc was clear, but the rfc-s about this, isn&#x27;t.'''
date = "2013-05-02T03:55:00Z"
lastmod = "2013-05-02T03:55:00Z"
weight = 20899
keywords = [ "ppp", "compression" ]
aliases = [ "/questions/20899" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Different PPP compressions](/questions/20899/different-ppp-compressions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20899-score" class="post-score" title="current number of votes">0</div><span id="post-20899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi In PPP CCP there is an option to enable Deflate(zlib), BSD and Magnalink compressions. What happens if they negotiate and agree on all three? How those packets would look like, and which compression algorithm would they use? I'm curious, the MPPC's rfc was clear, but the rfc-s about this, isn't.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ppp" rel="tag" title="see questions tagged &#39;ppp&#39;">ppp</span> <span class="post-tag tag-link-compression" rel="tag" title="see questions tagged &#39;compression&#39;">compression</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '13, 03:55</strong></p><img src="https://secure.gravatar.com/avatar/7619001c77a659e8a458285f50e89efb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KisKer&#39;s gravatar image" /><p><span>KisKer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KisKer has no accepted answers">0%</span></p></div></div><div id="comments-container-20899" class="comments-container"></div><div id="comment-tools-20899" class="comment-tools"></div><div class="clear"></div><div id="comment-20899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

