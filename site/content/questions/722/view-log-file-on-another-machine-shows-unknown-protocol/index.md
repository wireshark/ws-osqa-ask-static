+++
type = "question"
title = "View log file on another machine - shows unknown protocol"
description = '''Good morning I did a capture at a remote site using my laptop. When I view the capture on my desktop machine running the same version of Wireshark, only raw data is displayed with UNKNOWN in the protocol column. Is there another file that I need to move from the laptop to desktop to decode the captu...'''
date = "2010-10-28T06:26:00Z"
lastmod = "2010-10-28T08:07:00Z"
weight = 722
keywords = [ "capture-file" ]
aliases = [ "/questions/722" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [View log file on another machine - shows unknown protocol](/questions/722/view-log-file-on-another-machine-shows-unknown-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-722-score" class="post-score" title="current number of votes">0</div><span id="post-722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good morning</p><p>I did a capture at a remote site using my laptop. When I view the capture on my desktop machine running the same version of Wireshark, only raw data is displayed with UNKNOWN in the protocol column. Is there another file that I need to move from the laptop to desktop to decode the capture file?</p><p>Thanks</p><p>Steve / Canada</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-file" rel="tag" title="see questions tagged &#39;capture-file&#39;">capture-file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '10, 06:26</strong></p><img src="https://secure.gravatar.com/avatar/2073dd04f758285a0d3e2d5146b283d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scostello&#39;s gravatar image" /><p><span>scostello</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scostello has no accepted answers">0%</span></p></div></div><div id="comments-container-722" class="comments-container"></div><div id="comment-tools-722" class="comment-tools"></div><div class="clear"></div><div id="comment-722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="726"></span>

<div id="answer-container-726" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-726-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-726-score" class="post-score" title="current number of votes">1</div><span id="post-726-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's something wrong with your capture file, or you're the victim of a <a href="http://wiki.wireshark.org/PracticalJokes">PracticalJoke</a>. The commandline tool capinfo may provide some sanity checking for you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Oct '10, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-726" class="comments-container"></div><div id="comment-tools-726" class="comment-tools"></div><div class="clear"></div><div id="comment-726-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

