+++
type = "question"
title = "Language used from Browser to Web Server"
description = '''How can I find in http header which language is being used from Browser to Webserver? How can I filter, which command I have to use?'''
date = "2012-10-10T06:41:00Z"
lastmod = "2012-10-10T06:44:00Z"
weight = 14890
keywords = [ "languages" ]
aliases = [ "/questions/14890" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Language used from Browser to Web Server](/questions/14890/language-used-from-browser-to-web-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14890-score" class="post-score" title="current number of votes">0</div><span id="post-14890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I find in http header which language is being used from Browser to Webserver? How can I filter, which command I have to use?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-languages" rel="tag" title="see questions tagged &#39;languages&#39;">languages</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '12, 06:41</strong></p><img src="https://secure.gravatar.com/avatar/138aa869c0284e18802057d83c031754?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="marsal&#39;s gravatar image" /><p><span>marsal</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="marsal has no accepted answers">0%</span></p></div></div><div id="comments-container-14890" class="comments-container"></div><div id="comment-tools-14890" class="comment-tools"></div><div class="clear"></div><div id="comment-14890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14892"></span>

<div id="answer-container-14892" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14892-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14892-score" class="post-score" title="current number of votes">1</div><span id="post-14892-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's the HTTP request header <strong>Accept-Language</strong>.</p><p>Display Filter: http.accept_language contains "en-en" (or any other language, like "de-de", "it-it", etc.)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '12, 06:44</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Oct '12, 07:22</strong> </span></p></div></div><div id="comments-container-14892" class="comments-container"></div><div id="comment-tools-14892" class="comment-tools"></div><div class="clear"></div><div id="comment-14892-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

