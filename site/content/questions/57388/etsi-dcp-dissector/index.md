+++
type = "question"
title = "ETSI DCP dissector"
description = '''Hi, I am trying to decode some DAB EDI which uses the ETSI DCP encapsulation protocol and are receiving malformed packet warnings. My data appears to be valid but the dissector appears to be having trouble re-combining the fragments. Is anyone able to add anything here? Cheers'''
date = "2016-11-15T04:11:00Z"
lastmod = "2016-11-15T05:39:00Z"
weight = 57388
keywords = [ "etsi", "edi", "drm", "dcp", "dab" ]
aliases = [ "/questions/57388" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ETSI DCP dissector](/questions/57388/etsi-dcp-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57388-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57388-score" class="post-score" title="current number of votes">0</div><span id="post-57388-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to decode some DAB EDI which uses the ETSI DCP encapsulation protocol and are receiving malformed packet warnings. My data appears to be valid but the dissector appears to be having trouble re-combining the fragments. Is anyone able to add anything here? Cheers</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-etsi" rel="tag" title="see questions tagged &#39;etsi&#39;">etsi</span> <span class="post-tag tag-link-edi" rel="tag" title="see questions tagged &#39;edi&#39;">edi</span> <span class="post-tag tag-link-drm" rel="tag" title="see questions tagged &#39;drm&#39;">drm</span> <span class="post-tag tag-link-dcp" rel="tag" title="see questions tagged &#39;dcp&#39;">dcp</span> <span class="post-tag tag-link-dab" rel="tag" title="see questions tagged &#39;dab&#39;">dab</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '16, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/ce2197565e4dd61b04043be06b6d365a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HiZ&#39;s gravatar image" /><p><span>HiZ</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HiZ has no accepted answers">0%</span></p></div></div><div id="comments-container-57388" class="comments-container"><span id="57390"></span><div id="comment-57390" class="comment"><div id="post-57390-score" class="comment-score"></div><div class="comment-text"><p>Sharing your capture would be a good start. Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-57390-info" class="comment-info"><span class="comment-age">(15 Nov '16, 05:39)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-57388" class="comment-tools"></div><div class="clear"></div><div id="comment-57388-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

