+++
type = "question"
title = "KINK (Kerberized Internet Negotiation of Keys) protocol"
description = '''Hi, does the Wireshark support a KINK protocol ?  Thanks for replies'''
date = "2015-04-28T11:13:00Z"
lastmod = "2015-04-29T02:01:00Z"
weight = 41926
keywords = [ "protocol" ]
aliases = [ "/questions/41926" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [KINK (Kerberized Internet Negotiation of Keys) protocol](/questions/41926/kink-kerberized-internet-negotiation-of-keys-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41926-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41926-score" class="post-score" title="current number of votes">0</div><span id="post-41926-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, does the Wireshark support a KINK protocol ?</p><p>Thanks for replies</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '15, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/522847ff667775705eeecdc735b7eabe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="peter%20david&#39;s gravatar image" /><p><span>peter david</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="peter david has no accepted answers">0%</span></p></div></div><div id="comments-container-41926" class="comments-container"></div><div id="comment-tools-41926" class="comment-tools"></div><div class="clear"></div><div id="comment-41926-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41937"></span>

<div id="answer-container-41937" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41937-score" class="post-score" title="current number of votes">2</div><span id="post-41937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Check out the <a href="https://www.wireshark.org/docs/dfref/">display filter page</a> for reference.</p><blockquote><p>kink: Kerberized Internet Negotiation of Key (1.0.0 to 1.12.3, 19 fields)</p></blockquote><p>(I guess this list needs updating for 1.12.4)</p><p>And these are the <a href="https://www.wireshark.org/docs/dfref/k/kink.html">protocol fields</a>.</p><p>(This confirms 1.12.4 support)</p><p>So, short answer: Yes.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Apr '15, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-41937" class="comments-container"></div><div id="comment-tools-41937" class="comment-tools"></div><div class="clear"></div><div id="comment-41937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

