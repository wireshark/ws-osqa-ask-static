+++
type = "question"
title = "Capture all data to and from an IP source"
description = '''I need to be able to inspect all network data coming in and being sent to a device on my LAN. My issue is that I can only see the packets from the source device that are addressed to the PC that is running Wireshark. How can I set Wireshark to show ALL traffic to and from this device, including inte...'''
date = "2014-05-02T12:33:00Z"
lastmod = "2014-05-02T13:44:00Z"
weight = 32438
keywords = [ "capture", "lan", "filters" ]
aliases = [ "/questions/32438" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture all data to and from an IP source](/questions/32438/capture-all-data-to-and-from-an-ip-source)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32438-score" class="post-score" title="current number of votes">0</div><span id="post-32438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to be able to inspect all network data coming in and being sent to a device on my LAN. My issue is that I can only see the packets from the source device that are addressed to the PC that is running Wireshark. How can I set Wireshark to show ALL traffic to and from this device, including internet traffic?</p><p>Thanks for the assistance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '14, 12:33</strong></p><img src="https://secure.gravatar.com/avatar/fd507924983d4fc8b15fe333311a657b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DMachado&#39;s gravatar image" /><p><span>DMachado</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DMachado has no accepted answers">0%</span></p></div></div><div id="comments-container-32438" class="comments-container"></div><div id="comment-tools-32438" class="comment-tools"></div><div class="clear"></div><div id="comment-32438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32439"></span>

<div id="answer-container-32439" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32439-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32439-score" class="post-score" title="current number of votes">1</div><span id="post-32439-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think this is what you need to take a look at: <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 May '14, 13:44</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32439" class="comments-container"></div><div id="comment-tools-32439" class="comment-tools"></div><div class="clear"></div><div id="comment-32439-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

