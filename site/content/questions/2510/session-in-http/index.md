+++
type = "question"
title = "session in http"
description = '''when I open a web page X and it has many links to other servers in it. How can I distinguish different request packet which goes to X from the one that goes to links?Can I track it in Wireshark?'''
date = "2011-02-22T15:30:00Z"
lastmod = "2011-02-23T13:24:00Z"
weight = 2510
keywords = [ "session" ]
aliases = [ "/questions/2510" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [session in http](/questions/2510/session-in-http)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2510-score" class="post-score" title="current number of votes">0</div><span id="post-2510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when I open a web page X and it has many links to other servers in it. How can I distinguish different request packet which goes to X from the one that goes to links?Can I track it in Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-session" rel="tag" title="see questions tagged &#39;session&#39;">session</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '11, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/0d1f835bfa8cc91838057ef65fc4d1c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="A%20B&#39;s gravatar image" /><p><span>A B</span><br />
<span class="score" title="1 reputation points">1</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="A B has no accepted answers">0%</span></p></div></div><div id="comments-container-2510" class="comments-container"></div><div id="comment-tools-2510" class="comment-tools"></div><div class="clear"></div><div id="comment-2510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2512"></span>

<div id="answer-container-2512" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2512-score" class="post-score" title="current number of votes">0</div><span id="post-2512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can look at the Host header (display filter field: http.host).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '11, 22:09</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2512" class="comments-container"><span id="2523"></span><div id="comment-2523" class="comment"><div id="post-2523-score" class="comment-score"></div><div class="comment-text"><p>Can you explain more please?If in 2 tabs I open 2 websites and run Wireshark how can I distinguish different request packets between 2 web sites?</p></div><div id="comment-2523-info" class="comment-info"><span class="comment-age">(23 Feb '11, 08:48)</span> <span class="comment-user userinfo">A B</span></div></div><span id="2538"></span><div id="comment-2538" class="comment"><div id="post-2538-score" class="comment-score"></div><div class="comment-text"><p>"Answer" converted to a comment in keeping with the philosophy of ask.wireshark.org. Please see the FAQ</p></div><div id="comment-2538-info" class="comment-info"><span class="comment-age">(23 Feb '11, 13:24)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-2512" class="comment-tools"></div><div class="clear"></div><div id="comment-2512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

