+++
type = "question"
title = "tshark decrypt esp packets with command line arguments"
description = '''Hello, I work with wireshark a lot and I need to decode a LOT of traces that have ESP. It takes a long time to manually enter in all the information necessary in the GUI to decode each different trace, so I am trying to figure out a way to pass the ESP decryption parameters as command line arguments...'''
date = "2013-07-11T14:41:00Z"
lastmod = "2013-07-19T12:01:00Z"
weight = 22874
keywords = [ "encrytion", "decrypt", "tshark", "command-line", "esp" ]
aliases = [ "/questions/22874" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [tshark decrypt esp packets with command line arguments](/questions/22874/tshark-decrypt-esp-packets-with-command-line-arguments)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22874-score" class="post-score" title="current number of votes">0</div><span id="post-22874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I work with wireshark a lot and I need to decode a LOT of traces that have ESP. It takes a long time to manually enter in all the information necessary in the GUI to decode each different trace, so I am trying to figure out a way to pass the ESP decryption parameters as command line arguments to tshark or wireshark. Or even be able to edit a file like esp_sa where I can just append the necessary keys with a script then open with wireshark. This hasn't worked however, so I am hoping to find someone who knows how to do this.</p><p>Any help is appreciated!</p><p>Thanks, Jon</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encrytion" rel="tag" title="see questions tagged &#39;encrytion&#39;">encrytion</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span> <span class="post-tag tag-link-esp" rel="tag" title="see questions tagged &#39;esp&#39;">esp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '13, 14:41</strong></p><img src="https://secure.gravatar.com/avatar/e96b0196e8e968b1a2d8f6ddfda87ab1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lemurshark&#39;s gravatar image" /><p><span>Lemurshark</span><br />
<span class="score" title="26 reputation points">26</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lemurshark has no accepted answers">0%</span></p></div></div><div id="comments-container-22874" class="comments-container"></div><div id="comment-tools-22874" class="comment-tools"></div><div class="clear"></div><div id="comment-22874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22876"></span>

<div id="answer-container-22876" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22876-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22876-score" class="post-score" title="current number of votes">0</div><span id="post-22876-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Lemurshark has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just did a quick test, and the following works on my system.</p><p><strong>Step #1:</strong> Create a file <strong>esp_sa</strong> in the <strong>application data</strong> directory of the User. Simply create SA entries in the GUI and then use the created file as a template. <strong>Edit -&gt; Preferences -&gt; Protocols -&gt; ESP -&gt; ESP SAs</strong>.</p><p>Sample File (for the capture file I posted in <a href="http://ask.wireshark.org/questions/12019/how-can-i-decrypt-ikev1-packets">this question</a> - scroll down to the ESP part):</p><pre><code># This file is automatically generated, DO NOT MODIFY.
&quot;IPv4&quot;,&quot;192.168.140.205&quot;,&quot;192.168.140.200&quot;,&quot;0x1c0d7b38&quot;,&quot;TripleDES-CBC [RFC2451]&quot;,&quot;0x39e87c9ca500616b36f2f0d3c7fb688621d7bbf31414abbd&quot;,&quot;HMAC-SHA-1-96 [RFC2404]&quot;,&quot;0xc364660133b04a4f20e52000dbe4a6ba154c09c1&quot;</code></pre><p><strong>Step #2:</strong> run tshark with the following option (additionally to your other options): -o esp.enable_encryption_decode:TRUE. This is not necessary, if you set the same option in the GUI (will be written into the preferences file).</p><blockquote><p>Or even be able to edit a file like esp_sa where I can just append the necessary keys with a script then open with wireshark. This hasn't worked however,</p></blockquote><p>did you get an error message? If no, what exactly did not work?</p><p>This is what I get (frame #12/#13 are the decrypted ESP frames).</p><pre><code>tshark -nr ipsec.cap

  1   0.000000 192.168.140.205 -&gt; 192.168.140.200 ISAKMP 294 0x0000 (0) Identity Protection (Main Mode)
  2   0.014556 192.168.140.200 -&gt; 192.168.140.205 ISAKMP 214 0x0000 (0) Identity Protection (Main Mode)
  3   0.042441 192.168.140.205 -&gt; 192.168.140.200 ISAKMP 82 0x0000 (0) Informational
  4  10.054177 192.168.140.200 -&gt; 192.168.140.205 ISAKMP 214 0x0000 (0) Identity Protection (Main Mode)
  5  10.073018 192.168.140.205 -&gt; 192.168.140.200 ISAKMP 270 0x0000 (0) Identity Protection (Main Mode)
  6  10.090702 192.168.140.200 -&gt; 192.168.140.205 ISAKMP 270 0x0000 (0) Identity Protection (Main Mode)
  7  10.104128 192.168.140.205 -&gt; 192.168.140.200 ISAKMP 110 0x0000 (0) Identity Protection (Main Mode)
  8  10.105329 192.168.140.200 -&gt; 192.168.140.205 ISAKMP 110 0x0000 (0) Identity Protection (Main Mode)
  9  10.108102 192.168.140.205 -&gt; 192.168.140.200 ISAKMP 198 0x0000 (0) Quick Mode
 10  10.109646 192.168.140.200 -&gt; 192.168.140.205 ISAKMP 198 0x0000 (0) Quick Mode
 11  10.308616 192.168.140.205 -&gt; 192.168.140.200 ISAKMP 94 0x0000 (0) Quick Mode

  12  13.860464 172.16.205.2 -&gt; 172.16.200.2 ICMP 126 0x38e7 (14567),0x57db (22491) Echo (ping) request  id=0x0300, seq=16640/65, ttl=127

  13  13.861386 172.16.200.2 -&gt; 172.16.205.2 ICMP 126 0xe4d4 (58580),0x3ff7 (16375) Echo (ping) reply    id=0x0300, seq=16640/65, ttl=127 (r
equest in 12)
</code></pre><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '13, 16:41</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jul '13, 16:45</strong> </span></p></div></div><div id="comments-container-22876" class="comments-container"><span id="23173"></span><div id="comment-23173" class="comment"><div id="post-23173-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt. I tried appending the lines to esp_sa again and it worked. I don't know what was wrong the first time that it didn't decode the ESP, but it's working now! This really helps as I've just written a script to decrypt the traces, and it's much faster than entering them by hand.</p></div><div id="comment-23173-info" class="comment-info"><span class="comment-age">(19 Jul '13, 12:01)</span> <span class="comment-user userinfo">Lemurshark</span></div></div></div><div id="comment-tools-22876" class="comment-tools"></div><div class="clear"></div><div id="comment-22876-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

