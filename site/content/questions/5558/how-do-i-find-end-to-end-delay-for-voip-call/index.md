+++
type = "question"
title = "How do i find end to end delay for VoIP call?"
description = '''I have got trace from both ends. I have used sipgate.co.uk as sip provider and Xlite as soft phone. I need to do analysis of jitter, bandwidth, packet loss and roundtrip delay. I could successfully manage to get jitter but i am stuck in delay.. Please help asap. Thanking you'''
date = "2011-08-07T11:05:00Z"
lastmod = "2011-08-08T02:44:00Z"
weight = 5558
keywords = [ "delay", "sip", "rtp", "voip" ]
aliases = [ "/questions/5558" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do i find end to end delay for VoIP call?](/questions/5558/how-do-i-find-end-to-end-delay-for-voip-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5558-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5558-score" class="post-score" title="current number of votes">0</div><span id="post-5558-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have got trace from both ends. I have used sipgate.co.uk as sip provider and Xlite as soft phone. I need to do analysis of jitter, bandwidth, packet loss and roundtrip delay. I could successfully manage to get jitter but i am stuck in delay.. Please help asap. Thanking you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '11, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/e9a71f5b3e4829f26d1f5642cbb6fbd5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sujay&#39;s gravatar image" /><p><span>sujay</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sujay has no accepted answers">0%</span></p></div></div><div id="comments-container-5558" class="comments-container"></div><div id="comment-tools-5558" class="comment-tools"></div><div class="clear"></div><div id="comment-5558-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5569"></span>

<div id="answer-container-5569" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5569-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5569-score" class="post-score" title="current number of votes">0</div><span id="post-5569-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://tools.ietf.org/html/rfc3611#section-4.7.3">RFC3611 section 4.7.3</a> for that. If you want to extract it from the captures themselves you'll need to know if and how the timebases of the two captures were synchronized.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Aug '11, 02:44</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-5569" class="comments-container"></div><div id="comment-tools-5569" class="comment-tools"></div><div class="clear"></div><div id="comment-5569-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

