+++
type = "question"
title = "Old Version Starts Up"
description = '''I have upgraded wireshark using &quot;apt-get install wireshark&quot;, After downloading and installing successfully, it starts up the old version after I type &quot;gksudo wireshark&quot;. How should I make sure my kali linux starts the latest version ? Thanks.'''
date = "2016-11-27T23:51:00Z"
lastmod = "2017-01-04T10:51:00Z"
weight = 57672
keywords = [ "upgrade", "startup", "install" ]
aliases = [ "/questions/57672" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Old Version Starts Up](/questions/57672/old-version-starts-up)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57672-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57672-score" class="post-score" title="current number of votes">0</div><span id="post-57672-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have upgraded wireshark using "apt-get install wireshark", After downloading and installing successfully, it starts up the old version after I type "gksudo wireshark". How should I make sure my kali linux starts the latest version ? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-upgrade" rel="tag" title="see questions tagged &#39;upgrade&#39;">upgrade</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '16, 23:51</strong></p><img src="https://secure.gravatar.com/avatar/29e590389c6683df8c8181e1be62ddae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="darkblood&#39;s gravatar image" /><p><span>darkblood</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="darkblood has no accepted answers">0%</span></p></div></div><div id="comments-container-57672" class="comments-container"></div><div id="comment-tools-57672" class="comment-tools"></div><div class="clear"></div><div id="comment-57672-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58510"></span>

<div id="answer-container-58510" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58510-score" class="post-score" title="current number of votes">0</div><span id="post-58510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is probably a question better asked of a Kali Linux site. We don't provide packages for Kali Linux.</p><p>One possibility is that you now have 2 versions of Wireshark installed. You could check this by doing, at a shell prompt, <code>type -a wireshark</code> to list the locations of the two versions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '17, 10:51</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-58510" class="comments-container"></div><div id="comment-tools-58510" class="comment-tools"></div><div class="clear"></div><div id="comment-58510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

