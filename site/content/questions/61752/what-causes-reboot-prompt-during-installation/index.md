+++
type = "question"
title = "What causes reboot prompt during installation?"
description = '''Greetings. I&#x27;ve written an automated installer to install WireShark during our server builds. A couple of people have reported that there is sometimes a reboot prompt at the end of the installation. I&#x27;m trying to trap the server conditions that might trigger this. Can someone tell me what causes tha...'''
date = "2017-06-02T12:57:00Z"
lastmod = "2017-06-02T12:57:00Z"
weight = 61752
keywords = [ "reboot" ]
aliases = [ "/questions/61752" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What causes reboot prompt during installation?](/questions/61752/what-causes-reboot-prompt-during-installation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61752-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61752-score" class="post-score" title="current number of votes">0</div><span id="post-61752-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings. I've written an automated installer to install WireShark during our server builds. A couple of people have reported that there is sometimes a reboot prompt at the end of the installation. I'm trying to trap the server conditions that might trigger this. Can someone tell me what causes that prompt? I've been unable to duplicate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reboot" rel="tag" title="see questions tagged &#39;reboot&#39;">reboot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '17, 12:57</strong></p><img src="https://secure.gravatar.com/avatar/e213e5e245b60273b11ca558e5cb8315?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pat%20Richard&#39;s gravatar image" /><p><span>Pat Richard</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pat Richard has no accepted answers">0%</span></p></div></div><div id="comments-container-61752" class="comments-container"></div><div id="comment-tools-61752" class="comment-tools"></div><div class="clear"></div><div id="comment-61752-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

