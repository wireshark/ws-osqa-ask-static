+++
type = "question"
title = "Best WS version for mid 2010 Mac Mini with 10.7.2"
description = '''Having seen questions about, and experienced, several anomolies with installing 1.6.2 and 1.6.3, I&#x27;m wondering what&#x27;s seen as the best version to use on the 2.4mhz core 2 duo Mac mini Mid 2010 with 10.7.2 and 10.7 server? The 1.6.3 64bit for 10.6? My experience with 1.6.3/64 is that it requires x11 ...'''
date = "2011-11-06T11:43:00Z"
lastmod = "2011-11-08T21:31:00Z"
weight = 7256
keywords = [ "macmini", "10.7" ]
aliases = [ "/questions/7256" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Best WS version for mid 2010 Mac Mini with 10.7.2](/questions/7256/best-ws-version-for-mid-2010-mac-mini-with-1072)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7256-score" class="post-score" title="current number of votes">0</div><span id="post-7256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Having seen questions about, and experienced, several anomolies with installing 1.6.2 and 1.6.3, I'm wondering what's seen as the best version to use on the 2.4mhz core 2 duo Mac mini Mid 2010 with 10.7.2 and 10.7 server? The 1.6.3 64bit for 10.6? My experience with 1.6.3/64 is that it requires x11 loaded by hand, first, to open. Then it still, as with 10.6.8 on Mac mini, takes about 2 minutes to open.</p><p>FWIW, my experiences have varied since 10.5 on various mac minis, and I haven't seen any definitive pattern or solution. Operation on my Macbook 2008 and Macbook Pro 2010 have been consistent and normal - go figure. Same with Win XPpro sp3 both native, and under VB, and VMware - works great, less filling. Thanks in advance for your thoughts.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macmini" rel="tag" title="see questions tagged &#39;macmini&#39;">macmini</span> <span class="post-tag tag-link-10.7" rel="tag" title="see questions tagged &#39;10.7&#39;">10.7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '11, 11:43</strong></p><img src="https://secure.gravatar.com/avatar/52ff5d6b59bd5798a667a6f346a52421?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packetlevel&#39;s gravatar image" /><p><span>packetlevel</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packetlevel has no accepted answers">0%</span></p></div></div><div id="comments-container-7256" class="comments-container"></div><div id="comment-tools-7256" class="comment-tools"></div><div class="clear"></div><div id="comment-7256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7315"></span>

<div id="answer-container-7315" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7315-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7315-score" class="post-score" title="current number of votes">0</div><span id="post-7315-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>disabling unused protocol helps loading faster. also, try to check system logs, may be there are some specific application problems with newest 64 bit version that are silently slowing you down in Mac's favourite fashion.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 21:31</strong></p><img src="https://secure.gravatar.com/avatar/35d96b8e73e6deb4e332d076fd3269b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShomeaX&#39;s gravatar image" /><p><span>ShomeaX</span><br />
<span class="score" title="73 reputation points">73</span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShomeaX has no accepted answers">0%</span></p></div></div><div id="comments-container-7315" class="comments-container"></div><div id="comment-tools-7315" class="comment-tools"></div><div class="clear"></div><div id="comment-7315-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

