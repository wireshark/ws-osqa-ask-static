+++
type = "question"
title = "Intel Pro Wireless 3945ABG"
description = '''After installing Wireshark on Windows Vista (unfortunatelly) :) I have a big problem. I use Intel Wireless Card. But it does not show in a list of interfaces for capturing data. I thought it is a problem of putting card into promiscous mode. Tried to change drivers etc. Googled a lot and some people...'''
date = "2012-03-05T01:22:00Z"
lastmod = "2012-03-05T02:31:00Z"
weight = 9349
keywords = [ "wireless" ]
aliases = [ "/questions/9349" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Intel Pro Wireless 3945ABG](/questions/9349/intel-pro-wireless-3945abg)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9349-score" class="post-score" title="current number of votes">0</div><span id="post-9349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installing Wireshark on Windows Vista (unfortunatelly) :) I have a big problem. I use Intel Wireless Card. But it does not show in a list of interfaces for capturing data. I thought it is a problem of putting card into promiscous mode. Tried to change drivers etc. Googled a lot and some people have problems with putting it in the mode some do not. No matter what I do card is not on the list. Any ideas?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '12, 01:22</strong></p><img src="https://secure.gravatar.com/avatar/bcf9c2c65fb7d53b6f0ed51ad592bcaa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tomo&#39;s gravatar image" /><p><span>tomo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tomo has no accepted answers">0%</span></p></div></div><div id="comments-container-9349" class="comments-container"><span id="9351"></span><div id="comment-9351" class="comment"><div id="post-9351-score" class="comment-score"></div><div class="comment-text"><p>Do you have an interface labelled "Microsoft"?</p></div><div id="comment-9351-info" class="comment-info"><span class="comment-age">(05 Mar '12, 01:29)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-9349" class="comment-tools"></div><div class="clear"></div><div id="comment-9349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9355"></span>

<div id="answer-container-9355" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9355-score" class="post-score" title="current number of votes">0</div><span id="post-9355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ok. I just came through few of the questions and realized one of the "Microsoft" interfaces is the card. Strange thing is it is shown as Ethernet card not wireless. Normally with wifi cards a lot of traffic comes as Protocol IEEE 802.11 and this is not the case here. What do I do wrong ? :)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '12, 02:27</strong></p><img src="https://secure.gravatar.com/avatar/bcf9c2c65fb7d53b6f0ed51ad592bcaa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tomo&#39;s gravatar image" /><p><span>tomo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tomo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Mar '12, 02:27</strong> </span></p></div></div><div id="comments-container-9355" class="comments-container"><span id="9356"></span><div id="comment-9356" class="comment"><div id="post-9356-score" class="comment-score"></div><div class="comment-text"><p>read the other questions again - search for "wireless".</p><p>short version: your windows driver can't capture in "promiscuous" mode and can't see real 802.11 frames</p></div><div id="comment-9356-info" class="comment-info"><span class="comment-age">(05 Mar '12, 02:31)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-9355" class="comment-tools"></div><div class="clear"></div><div id="comment-9355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

