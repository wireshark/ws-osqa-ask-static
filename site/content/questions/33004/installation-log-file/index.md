+++
type = "question"
title = "Installation log file"
description = '''Hi,  I would like to know if there is a switch to create a log file in c:&#92;temp&#92;wireshark.log also need it for winpcap-nmap-4.13.exe. I am doing a silent install and need a trace.  Thks'''
date = "2014-05-22T09:15:00Z"
lastmod = "2014-05-26T17:10:00Z"
weight = 33004
keywords = [ "logfile" ]
aliases = [ "/questions/33004" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Installation log file](/questions/33004/installation-log-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33004-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33004-score" class="post-score" title="current number of votes">0</div><span id="post-33004-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to know if there is a switch to create a log file in c:\temp\wireshark.log also need it for winpcap-nmap-4.13.exe. I am doing a silent install and need a trace. Thks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-logfile" rel="tag" title="see questions tagged &#39;logfile&#39;">logfile</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '14, 09:15</strong></p><img src="https://secure.gravatar.com/avatar/9b1e83f48303c48183b49f121b7f4b43?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sylsimp&#39;s gravatar image" /><p><span>sylsimp</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sylsimp has no accepted answers">0%</span></p></div></div><div id="comments-container-33004" class="comments-container"></div><div id="comment-tools-33004" class="comment-tools"></div><div class="clear"></div><div id="comment-33004-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33098"></span>

<div id="answer-container-33098" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33098-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33098-score" class="post-score" title="current number of votes">0</div><span id="post-33098-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Hi, I would like to know if there is a switch to create a log file</p></blockquote><p>no, because the NSIS package created for Wireshark does not write a log during the installation.</p><p>You can try to monitor the installation process with <a href="http://technet.microsoft.com/de-de/sysinternals/bb896645.aspx">Process Monitor</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 May '14, 17:10</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-33098" class="comments-container"></div><div id="comment-tools-33098" class="comment-tools"></div><div class="clear"></div><div id="comment-33098-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

