+++
type = "question"
title = "telnet authentication"
description = '''how can i determine telnet username from captured traffic between user and server'''
date = "2013-06-11T16:13:00Z"
lastmod = "2013-06-11T16:19:00Z"
weight = 21937
keywords = [ "all" ]
aliases = [ "/questions/21937" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [telnet authentication](/questions/21937/telnet-authentication)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21937-score" class="post-score" title="current number of votes">0</div><span id="post-21937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i determine telnet username from captured traffic between user and server</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-all" rel="tag" title="see questions tagged &#39;all&#39;">all</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jun '13, 16:13</strong></p><img src="https://secure.gravatar.com/avatar/eda7a86a64d21f347aaa6056df87eea8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="black%20zone&#39;s gravatar image" /><p><span>black zone</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="black zone has no accepted answers">0%</span></p></div></div><div id="comments-container-21937" class="comments-container"></div><div id="comment-tools-21937" class="comment-tools"></div><div class="clear"></div><div id="comment-21937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21938"></span>

<div id="answer-container-21938" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21938-score" class="post-score" title="current number of votes">2</div><span id="post-21938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Telnet is clear text, so if you can find a packet of the telnet communication (which should be easy enough) you can either read the packet contents until you find the typed password, or use the "Follow TCP Stream" popup menu option. As long as the username was entered <strong>after</strong> the capture was started you should be able to see it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jun '13, 16:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-21938" class="comments-container"></div><div id="comment-tools-21938" class="comment-tools"></div><div class="clear"></div><div id="comment-21938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

