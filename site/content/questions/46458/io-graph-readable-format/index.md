+++
type = "question"
title = "IO Graph readable format"
description = '''I would like more readable format in IO graph. I&#x27;d like to see e.g. dotted bits/tick like 100.000.000 not seeing just 100000000 because it&#x27;s hard to count all the zeros.'''
date = "2015-10-11T09:48:00Z"
lastmod = "2015-10-12T12:09:00Z"
weight = 46458
keywords = [ "graph", "io", "iograph" ]
aliases = [ "/questions/46458" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IO Graph readable format](/questions/46458/io-graph-readable-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46458-score" class="post-score" title="current number of votes">1</div><span id="post-46458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like more readable format in IO graph. I'd like to see e.g. dotted bits/tick like 100.000.000 not seeing just 100000000 because it's hard to count all the zeros.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-io" rel="tag" title="see questions tagged &#39;io&#39;">io</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '15, 09:48</strong></p><img src="https://secure.gravatar.com/avatar/d3cc635d365ad5f34876f2c0862322f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kuchenmann&#39;s gravatar image" /><p><span>kuchenmann</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kuchenmann has no accepted answers">0%</span></p></div></div><div id="comments-container-46458" class="comments-container"><span id="46470"></span><div id="comment-46470" class="comment"><div id="post-46470-score" class="comment-score"></div><div class="comment-text"><p>Is this in the GTK+ (1.x) or Qt (2.x) interface?</p></div><div id="comment-46470-info" class="comment-info"><span class="comment-age">(12 Oct '15, 12:09)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-46458" class="comment-tools"></div><div class="clear"></div><div id="comment-46458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46459"></span>

<div id="answer-container-46459" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46459-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46459-score" class="post-score" title="current number of votes">0</div><span id="post-46459-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That would be an enhancement request, which can be filled through <a href="https://bugs.wireshark.org/bugzilla/">bugzilla</a>. Oh, and patches to source code are welcome through the <a href="https://code.wireshark.org">code site</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Oct '15, 12:14</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-46459" class="comments-container"></div><div id="comment-tools-46459" class="comment-tools"></div><div class="clear"></div><div id="comment-46459-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

