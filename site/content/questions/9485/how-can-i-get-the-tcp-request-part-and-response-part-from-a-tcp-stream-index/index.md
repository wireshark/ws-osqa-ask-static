+++
type = "question"
title = "How can I get the Tcp Request part and response part from a TCP Stream Index?"
description = '''HI ,   I have an entire payload of a particular tcp stream say tcp.stream eq 2 using Wireshark lua script.  But in this , the request and response are not differentiated. I want to differentiate the request part and response part. How can i do it.  Thanks in advance for you rhelp'''
date = "2012-03-12T03:38:00Z"
lastmod = "2012-03-12T03:38:00Z"
weight = 9485
keywords = [ "lua", "tcp-options", "tcp" ]
aliases = [ "/questions/9485" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I get the Tcp Request part and response part from a TCP Stream Index?](/questions/9485/how-can-i-get-the-tcp-request-part-and-response-part-from-a-tcp-stream-index)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9485-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9485-score" class="post-score" title="current number of votes">0</div><span id="post-9485-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI , I have an entire payload of a particular tcp stream say tcp.stream eq 2 using Wireshark lua script. But in this , the request and response are not differentiated. I want to differentiate the request part and response part. How can i do it.</p><p>Thanks in advance for you rhelp</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-tcp-options" rel="tag" title="see questions tagged &#39;tcp-options&#39;">tcp-options</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '12, 03:38</strong></p><img src="https://secure.gravatar.com/avatar/eaba5d948ba0b95759c596eb2c6e7ecb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rijith&#39;s gravatar image" /><p><span>Rijith</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rijith has no accepted answers">0%</span></p></div></div><div id="comments-container-9485" class="comments-container"></div><div id="comment-tools-9485" class="comment-tools"></div><div class="clear"></div><div id="comment-9485-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

