+++
type = "question"
title = "Unable to see SIP packets in Wireshark for Mac Lion"
description = '''How do i see SIP packets on Wireshark for operating system Mac OSX Lion 10.7? '''
date = "2011-08-21T23:23:00Z"
lastmod = "2011-08-22T05:52:00Z"
weight = 5798
keywords = [ "sip" ]
aliases = [ "/questions/5798" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to see SIP packets in Wireshark for Mac Lion](/questions/5798/unable-to-see-sip-packets-in-wireshark-for-mac-lion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5798-score" class="post-score" title="current number of votes">0</div><span id="post-5798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do i see SIP packets on Wireshark for operating system Mac OSX Lion 10.7?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '11, 23:23</strong></p><img src="https://secure.gravatar.com/avatar/235d700fb7ab9cca5975e5f7eb55bff2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="amit&#39;s gravatar image" /><p><span>amit</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="amit has no accepted answers">0%</span></p></div></div><div id="comments-container-5798" class="comments-container"><span id="5802"></span><div id="comment-5802" class="comment"><div id="post-5802-score" class="comment-score">1</div><div class="comment-text"><p>By opening capture files with SIP packets in them... but I doubt that is really your question.</p></div><div id="comment-5802-info" class="comment-info"><span class="comment-age">(22 Aug '11, 05:52)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-5798" class="comment-tools"></div><div class="clear"></div><div id="comment-5798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

