+++
type = "question"
title = "Newbie help improving network"
description = '''I&#x27;d sure appreciate help understanding Wireshark&#x27;s statistics and any advice that can come from that on improving my network. If there&#x27;s something on these captures that stands out as burdensome or unnecessary or excessive on the network, please let me know. These stats were captured overnight with ...'''
date = "2014-10-24T02:56:00Z"
lastmod = "2014-10-24T02:56:00Z"
weight = 37332
keywords = [ "analyze", "bottleneck", "statistics", "newbie" ]
aliases = [ "/questions/37332" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Newbie help improving network](/questions/37332/newbie-help-improving-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37332-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37332-score" class="post-score" title="current number of votes">0</div><span id="post-37332-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'd sure appreciate help understanding Wireshark's statistics and any advice that can come from that on improving my network. If there's something on these captures that stands out as burdensome or unnecessary or excessive on the network, please let me know.</p><p>These stats were captured overnight with no (or extremely little) user activity.</p><p>Thank you!</p><p>Since I'm new here, I can't upload the photos to this message, so there are linked here:</p><p><a href="http://postimg.org/gallery/8xhmennw/">capture screen shots</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analyze" rel="tag" title="see questions tagged &#39;analyze&#39;">analyze</span> <span class="post-tag tag-link-bottleneck" rel="tag" title="see questions tagged &#39;bottleneck&#39;">bottleneck</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '14, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/98fcf3be5caacb27b7370d73f60ce557?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="josephny&#39;s gravatar image" /><p><span>josephny</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="josephny has no accepted answers">0%</span></p></div></div><div id="comments-container-37332" class="comments-container"></div><div id="comment-tools-37332" class="comment-tools"></div><div class="clear"></div><div id="comment-37332-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

