+++
type = "question"
title = "Web Server Performance Issue"
description = '''I have a complex webserver slow loading issue and would like to provide a diagram and captures for someone to take a look and help me with their take.'''
date = "2015-04-09T11:30:00Z"
lastmod = "2015-04-10T03:24:00Z"
weight = 41330
keywords = [ "loads", "slow", "complex", "wepage" ]
aliases = [ "/questions/41330" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Web Server Performance Issue](/questions/41330/web-server-performance-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41330-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41330-score" class="post-score" title="current number of votes">0</div><span id="post-41330-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a complex webserver slow loading issue and would like to provide a diagram and captures for someone to take a look and help me with their take.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-loads" rel="tag" title="see questions tagged &#39;loads&#39;">loads</span> <span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span> <span class="post-tag tag-link-complex" rel="tag" title="see questions tagged &#39;complex&#39;">complex</span> <span class="post-tag tag-link-wepage" rel="tag" title="see questions tagged &#39;wepage&#39;">wepage</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Apr '15, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/cd7a383fd17f7bbde6414e7910d28fb9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BokisETAS&#39;s gravatar image" /><p><span>BokisETAS</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BokisETAS has no accepted answers">0%</span></p></div></div><div id="comments-container-41330" class="comments-container"><span id="41332"></span><div id="comment-41332" class="comment"><div id="post-41332-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-41332-info" class="comment-info"><span class="comment-age">(09 Apr '15, 12:47)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="41346"></span><div id="comment-41346" class="comment"><div id="post-41346-score" class="comment-score"></div><div class="comment-text"><p>Please upload the files somewhere (google drive, dropbox, cloudshark.org) and post the link here.</p></div><div id="comment-41346-info" class="comment-info"><span class="comment-age">(10 Apr '15, 03:24)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-41330" class="comment-tools"></div><div class="clear"></div><div id="comment-41330-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

