+++
type = "question"
title = "NOT Spewing Emails."
description = '''My ISP is suggesting that my computer is infected and is &quot;spewing email&quot; when it isn&#x27;t infected nor spewing anything. All I see in the traffic logs are normal traffic and I use a lot of software both free and paid to keep the computer clean. Plus as it turns out there is another person on the other ...'''
date = "2011-01-25T03:06:00Z"
lastmod = "2011-01-27T12:12:00Z"
weight = 1914
keywords = [ "trojan", "email" ]
aliases = [ "/questions/1914" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [NOT Spewing Emails.](/questions/1914/not-spewing-emails)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1914-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1914-score" class="post-score" title="current number of votes">0</div><span id="post-1914-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My ISP is suggesting that my computer is infected and is "spewing email" when it isn't infected nor spewing anything. All I see in the traffic logs are normal traffic and I use a lot of software both free and paid to keep the computer clean. Plus as it turns out there is another person on the other side of the state that just happens to be cruising the internet while using my static IP address.</p><p>What gives?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trojan" rel="tag" title="see questions tagged &#39;trojan&#39;">trojan</span> <span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '11, 03:06</strong></p><img src="https://secure.gravatar.com/avatar/d760a1de8d38fa5a667527c53585b7cf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mysticide&#39;s gravatar image" /><p><span>mysticide</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mysticide has no accepted answers">0%</span></p></div></div><div id="comments-container-1914" class="comments-container"></div><div id="comment-tools-1914" class="comment-tools"></div><div class="clear"></div><div id="comment-1914-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1972"></span>

<div id="answer-container-1972" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1972-score" class="post-score" title="current number of votes">1</div><span id="post-1972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>An IP-address, like a postal address, points to the ISP router port that your Internet access is connected to. I can plug your address in my computer or Internet gateway, I can send IP-packets with that source address, but if any host would try to answer me, the Internet routers would deliver their answer to your doorstep, not mine. Since posting in a forum or sending email requires two-way communication, there is no way to do that from anywhere but your access line, either at your end or at the ISP's location. If you are using a cable modem, there is a theoretical chance that somebody in your neighborhood, on the same CATV distribution cable, could use your address. If you use an in-house WiFi router, chances are somebody taps into your home network be breaking the access code. Might be a good idea to change your WPA password, preferably to something long, like 20 characters or more. If you use WEP or no security, break-in is too easy. MAC-security too is easy to break.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '11, 08:13</strong></p><img src="https://secure.gravatar.com/avatar/71bf494ed8ed44215796e5f5b367ef32?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ernst%20Lopes%20Cardozo&#39;s gravatar image" /><p><span>Ernst Lopes ...</span><br />
<span class="score" title="30 reputation points">30</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ernst Lopes Cardozo has no accepted answers">0%</span></p></div></div><div id="comments-container-1972" class="comments-container"></div><div id="comment-tools-1972" class="comment-tools"></div><div class="clear"></div><div id="comment-1972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1916"></span>

<div id="answer-container-1916" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1916-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1916-score" class="post-score" title="current number of votes">0</div><span id="post-1916-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess my question is how do you know that? If you have a good answer to this question, talk to your ISP. The computer on the other side of the state cannot be using your IP address while you are and actually cruising the internet or sending email. This type of traffic is largely TCP based and requires a 3 way handshake. So one of the two of you would be having major issues using this IP address.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jan '11, 03:36</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span> </br></p></div></div><div id="comments-container-1916" class="comments-container"><span id="1918"></span><div id="comment-1918" class="comment"><div id="post-1918-score" class="comment-score"></div><div class="comment-text"><p>Here's where it gets good. I frequent a website that has a forum and have been using that site/forum for many years so they know me there. I received an message from the administrator asking if someone else in my home was using a computer to go to that site and log in because they committed bannable offenses but they can't ban them or else I would get locked out too. Currently they are as good as muted until this double use of an IP address is resolved.</p><p>The administrator can't give me any technical details because of privacy issues with that other persons account and my ISP refuses to cooperate by answering my qustions about why or how is someone else using my IP address. My IP address has never changed in the two or so years that I have used their service.</p><p>How could someone else end up getting assigned my IP address by their ISP, the same IP address that is assigned to me by my ISP?</p><p>The odds of two people using the same IP address on two different sides of the state ending up using the same forum are astronomical from what I am told.</p><p>I don't know what I could do to help resolve these matters.</p></div><div id="comment-1918-info" class="comment-info"><span class="comment-age">(25 Jan '11, 04:00)</span> <span class="comment-user userinfo">mysticide</span></div></div><span id="1980"></span><div id="comment-1980" class="comment"><div id="post-1980-score" class="comment-score"></div><div class="comment-text"><p>The first answer from Ernst answers this question - someone else on the other side of the state can't be using your IP. Now, being a person who likes to argue extremes I'll say that it's POSSIBLE if the method to which you connect to your ISP is based on a broadcast medium. If you're on an older DOCSIS cable modem system then someone COULD clone your modems MAC and pretend to be you. If you have wireless access to your provider then the same thing is possible. It's WAY more likely that you have a wireless network at home that has been compromised.</p></div><div id="comment-1980-info" class="comment-info"><span class="comment-age">(27 Jan '11, 12:12)</span> <span class="comment-user userinfo">GeonJay</span></div></div></div><div id="comment-tools-1916" class="comment-tools"></div><div class="clear"></div><div id="comment-1916-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

