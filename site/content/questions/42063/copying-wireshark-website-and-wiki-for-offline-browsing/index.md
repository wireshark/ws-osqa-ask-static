+++
type = "question"
title = "Copying Wireshark website and wiki for offline browsing"
description = '''how can i copy wireshark and the wiki for offline browsing using a windows platform? '''
date = "2015-05-04T13:23:00Z"
lastmod = "2015-05-06T16:38:00Z"
weight = 42063
keywords = [ "wiki", "mirror" ]
aliases = [ "/questions/42063" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Copying Wireshark website and wiki for offline browsing](/questions/42063/copying-wireshark-website-and-wiki-for-offline-browsing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42063-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42063-score" class="post-score" title="current number of votes">0</div><span id="post-42063-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i copy wireshark and the wiki for offline browsing using a windows platform?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wiki" rel="tag" title="see questions tagged &#39;wiki&#39;">wiki</span> <span class="post-tag tag-link-mirror" rel="tag" title="see questions tagged &#39;mirror&#39;">mirror</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '15, 13:23</strong></p><img src="https://secure.gravatar.com/avatar/2e19e9edee628ae8df6868a3fc2e410e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="avm31982&#39;s gravatar image" /><p><span>avm31982</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="avm31982 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 May '15, 15:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-42063" class="comments-container"><span id="42102"></span><div id="comment-42102" class="comment"><div id="post-42102-score" class="comment-score"></div><div class="comment-text"><p>have you tried a "wget" utility for windows like "swiss file knife"?</p></div><div id="comment-42102-info" class="comment-info"><span class="comment-age">(05 May '15, 12:16)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="42113"></span><div id="comment-42113" class="comment"><div id="post-42113-score" class="comment-score"></div><div class="comment-text"><p>I downloaded rsync, which I was going to use through Cygwin but it was too large for my hard drive which had no space. I just downloaded gnu wget, and used the setup utility to install it but it seems only to have created manuals, an no binary or path for command line execution. perhaps you known a working binary?</p></div><div id="comment-42113-info" class="comment-info"><span class="comment-age">(05 May '15, 21:54)</span> <span class="comment-user userinfo">avm31982</span></div></div></div><div id="comment-tools-42063" class="comment-tools"></div><div class="clear"></div><div id="comment-42063-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42114"></span>

<div id="answer-container-42114" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42114-score" class="post-score" title="current number of votes">0</div><span id="post-42114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As I told a good and small Tool is "Swiss file knife" you can get it here <a href="http://stahlworks.com/dev/swiss-file-knife.html.">http://stahlworks.com/dev/swiss-file-knife.html.</a> It includes a wget. I like this tool. Before that tool, I used a compiled wget.exe by just downloading it and make it available within the PATH variable.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '15, 22:16</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 May '15, 22:17</strong> </span></p></div></div><div id="comments-container-42114" class="comments-container"><span id="42120"></span><div id="comment-42120" class="comment"><div id="post-42120-score" class="comment-score"></div><div class="comment-text"><p>I created a path for sfk and it just keeps returning error 1 cannot read file with the wget command issued. thanks for replying, I didn't get the whole site, but I got a good portion of it with httrack</p></div><div id="comment-42120-info" class="comment-info"><span class="comment-age">(06 May '15, 02:56)</span> <span class="comment-user userinfo">avm31982</span></div></div><span id="42161"></span><div id="comment-42161" class="comment"><div id="post-42161-score" class="comment-score"></div><div class="comment-text"><p>Here is al ink to a compiled wget: <a href="http://gnuwin32.sourceforge.net/packages/wget.htm">http://gnuwin32.sourceforge.net/packages/wget.htm</a></p><p>The wget.exe is inside the "bin" Folder. The easiest way of use is to copy the File in the system32 Folder.</p></div><div id="comment-42161-info" class="comment-info"><span class="comment-age">(06 May '15, 16:38)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-42114" class="comment-tools"></div><div class="clear"></div><div id="comment-42114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

