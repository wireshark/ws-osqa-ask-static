+++
type = "question"
title = "Is there anything in this packet trace that would cause a modem/router to drop the Internet connection?"
description = '''Hi, could anyone here please check through this Wireshark log for me and see if they can spot anything which can cause a modem/router to temporarily &quot;drop&quot; the Internet connection? The behavior during the connection drop is similar to that of when the modem/router is manually reset, except with this...'''
date = "2014-04-03T11:04:00Z"
lastmod = "2014-04-04T08:07:00Z"
weight = 31498
keywords = [ "connection", "dropped", "internet" ]
aliases = [ "/questions/31498" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there anything in this packet trace that would cause a modem/router to drop the Internet connection?](/questions/31498/is-there-anything-in-this-packet-trace-that-would-cause-a-modemrouter-to-drop-the-internet-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31498-score" class="post-score" title="current number of votes">0</div><span id="post-31498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>could anyone here please check through this Wireshark log for me and see if they can spot anything which can cause a modem/router to temporarily "drop" the Internet connection? The behavior during the connection drop is similar to that of when the modem/router is manually reset, except with this issue, the link is re-established much faster but connectivity is still lost altogether for a period of roughly 30-45 seconds.</p><p>The lights on my router remain the same as they are during normal operation, so this issue appears to be causing some kind of "internal overload"(?) which causes a temporary drop but not a full-on reset.</p><p>I am having this issue with an Xbox 360 game (Battlefield 4) and it has become extremely frustrating. Some days I can play without problems, and on others I am constantly being kicked offline because of this issue. I've tried numerous "fixes" but nothing has worked.</p><p>If you can suggest a remedy for something like this, please do. If you can also skim through this log and see if you find anything which may be the culprit, that'd be greatly appreciated as well.</p><p>Log download here: <a href="http://www.sendspace.com/file/gp7fmx">http://www.sendspace.com/file/gp7fmx</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection" rel="tag" title="see questions tagged &#39;connection&#39;">connection</span> <span class="post-tag tag-link-dropped" rel="tag" title="see questions tagged &#39;dropped&#39;">dropped</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Apr '14, 11:04</strong></p><img src="https://secure.gravatar.com/avatar/4f2dd488f081d625273910b221f549bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Watch7ower&#39;s gravatar image" /><p><span>Watch7ower</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Watch7ower has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Apr '14, 18:58</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-31498" class="comments-container"><span id="31508"></span><div id="comment-31508" class="comment"><div id="post-31508-score" class="comment-score"></div><div class="comment-text"><p>Anyone...?</p></div><div id="comment-31508-info" class="comment-info"><span class="comment-age">(04 Apr '14, 05:50)</span> <span class="comment-user userinfo">Watch7ower</span></div></div></div><div id="comment-tools-31498" class="comment-tools"></div><div class="clear"></div><div id="comment-31498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31510"></span>

<div id="answer-container-31510" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31510-score" class="post-score" title="current number of votes">0</div><span id="post-31510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer in your other question</p><blockquote><p><a href="http://ask.wireshark.org/questions/31253/internet-connection-drops-entirely-while-trying-to-play-battlefield-4">http://ask.wireshark.org/questions/31253/internet-connection-drops-entirely-while-trying-to-play-battlefield-4</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '14, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-31510" class="comments-container"></div><div id="comment-tools-31510" class="comment-tools"></div><div class="clear"></div><div id="comment-31510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

