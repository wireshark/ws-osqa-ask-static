+++
type = "question"
title = "Language/framework to analyze or categorize capture results?"
description = '''I am attempting to analyze capture results obtained from running a single application once using HTTP Ajax polling (comet) and once using Websockets. I would like to determine the difference in overhead between the two (so things like HTTP headers or resending requests, handshakes, etc.). What would...'''
date = "2013-03-15T21:54:00Z"
lastmod = "2013-03-16T05:02:00Z"
weight = 19551
keywords = [ "framework", "scapy", "http", "websockets", "libpcap" ]
aliases = [ "/questions/19551" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Language/framework to analyze or categorize capture results?](/questions/19551/languageframework-to-analyze-or-categorize-capture-results)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19551-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19551-score" class="post-score" title="current number of votes">0</div><span id="post-19551-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am attempting to analyze capture results obtained from running a single application once using HTTP Ajax polling (comet) and once using Websockets. I would like to determine the difference in overhead between the two (so things like HTTP headers or resending requests, handshakes, etc.).</p><p>What would be the best way to approach this? I was thinking that using a scripting language with a framework that understands the libpcap format (such as Python/<a href="http://www.secdev.org/projects/scapy/">scapy</a>). I would perfer something a little hacky rather than something with a huge learning curve.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-framework" rel="tag" title="see questions tagged &#39;framework&#39;">framework</span> <span class="post-tag tag-link-scapy" rel="tag" title="see questions tagged &#39;scapy&#39;">scapy</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-websockets" rel="tag" title="see questions tagged &#39;websockets&#39;">websockets</span> <span class="post-tag tag-link-libpcap" rel="tag" title="see questions tagged &#39;libpcap&#39;">libpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '13, 21:54</strong></p><img src="https://secure.gravatar.com/avatar/3cb0a62645df892ca8c8ea7dd49c855a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mxrider&#39;s gravatar image" /><p><span>mxrider</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mxrider has no accepted answers">0%</span></p></div></div><div id="comments-container-19551" class="comments-container"></div><div id="comment-tools-19551" class="comment-tools"></div><div class="clear"></div><div id="comment-19551-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19556"></span>

<div id="answer-container-19556" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19556-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19556-score" class="post-score" title="current number of votes">0</div><span id="post-19556-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://wiki.wireshark.org/Mate">Meta Analysis and Tracing Engine</a> may work or you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '13, 05:02</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-19556" class="comments-container"></div><div id="comment-tools-19556" class="comment-tools"></div><div class="clear"></div><div id="comment-19556-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

