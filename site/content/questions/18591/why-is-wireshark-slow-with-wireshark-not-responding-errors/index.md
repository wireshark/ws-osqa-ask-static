+++
type = "question"
title = "Why is wireshark slow with wireshark not responding errors"
description = '''Hi, I&#x27;ve installed wireshark on 2 windows 7 computers. Both do the same thing when I run it. They will capture packets and then the screen freezes with a &quot;wireshark not responding error&quot;. If I wait long enough it will respond again, but if I click on a packet, or scroll the screen down, it will happ...'''
date = "2013-02-13T08:33:00Z"
lastmod = "2014-05-03T10:21:00Z"
weight = 18591
keywords = [ "not", "responding" ]
aliases = [ "/questions/18591" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Why is wireshark slow with wireshark not responding errors](/questions/18591/why-is-wireshark-slow-with-wireshark-not-responding-errors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18591-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18591-score" class="post-score" title="current number of votes">0</div><span id="post-18591-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I've installed wireshark on 2 windows 7 computers. Both do the same thing when I run it. They will capture packets and then the screen freezes with a "wireshark not responding error". If I wait long enough it will respond again, but if I click on a packet, or scroll the screen down, it will happen again.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-responding" rel="tag" title="see questions tagged &#39;responding&#39;">responding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '13, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/51982b10798892f606893044728e2ccc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="giantsfan&#39;s gravatar image" /><p><span>giantsfan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="giantsfan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Feb '13, 08:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-18591" class="comments-container"><span id="18594"></span><div id="comment-18594" class="comment"><div id="post-18594-score" class="comment-score"></div><div class="comment-text"><p>sounds like a problem related to the graphics driver.</p><ul><li>Are those two computers equipped with the same hardware?</li><li>Do you access the computers via RDP or any other remote desktop software?</li></ul></div><div id="comment-18594-info" class="comment-info"><span class="comment-age">(13 Feb '13, 08:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-18591" class="comment-tools"></div><div class="clear"></div><div id="comment-18591-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="18595"></span>

<div id="answer-container-18595" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18595-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18595-score" class="post-score" title="current number of votes">2</div><span id="post-18595-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you have name resolution switched on? try switching it off.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '13, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-18595" class="comments-container"><span id="18619"></span><div id="comment-18619" class="comment"><div id="post-18619-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I turned off all the name resolution options in preferences and everything is working great!! Thanks for responding to my question.</p></div><div id="comment-18619-info" class="comment-info"><span class="comment-age">(13 Feb '13, 16:43)</span> <span class="comment-user userinfo">giantsfan</span></div></div></div><div id="comment-tools-18595" class="comment-tools"></div><div class="clear"></div><div id="comment-18595-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="32453"></span>

<div id="answer-container-32453" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32453-score" class="post-score" title="current number of votes">0</div><span id="post-32453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try enable the concurrent network name resolver: <img src="https://dl.dropboxusercontent.com/u/674259/2014-05-04%2001.19.08.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '14, 10:21</strong></p><img src="https://secure.gravatar.com/avatar/87d3bd2e656afe0add3af5a96f688ef2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flz&#39;s gravatar image" /><p><span>flz</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flz has no accepted answers">0%</span></p></img></div></div><div id="comments-container-32453" class="comments-container"></div><div id="comment-tools-32453" class="comment-tools"></div><div class="clear"></div><div id="comment-32453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

