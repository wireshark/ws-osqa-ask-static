+++
type = "question"
title = "[closed] Is ExecDos.dll a valid part of your install ?"
description = '''I downloaded and attempted to install WireShart / WinpCap. I am using Malwarebytes and it picks up one of your install files as being Malware - ExecDos.dll, Hmmm - Is this program part of your normal install (and it is safe to install) or did some malware get into your build /install ??'''
date = "2010-09-29T12:30:00Z"
lastmod = "2010-09-29T13:09:00Z"
weight = 357
keywords = [ "malware", "execdos" ]
aliases = [ "/questions/357" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Is ExecDos.dll a valid part of your install ?](/questions/357/is-execdosdll-a-valid-part-of-your-install)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-357-score" class="post-score" title="current number of votes">0</div><span id="post-357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded and attempted to install WireShart / WinpCap.</p><p>I am using Malwarebytes and it picks up one of your install files as being Malware - ExecDos.dll,</p><p>Hmmm - Is this program part of your normal install (and it is safe to install) or did some malware get into your build /install ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malware" rel="tag" title="see questions tagged &#39;malware&#39;">malware</span> <span class="post-tag tag-link-execdos" rel="tag" title="see questions tagged &#39;execdos&#39;">execdos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '10, 12:30</strong></p><img src="https://secure.gravatar.com/avatar/4b425ee0c06bf0978a91b38fd390d9c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gordzilla&#39;s gravatar image" /><p><span>Gordzilla</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gordzilla has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>29 Sep '10, 13:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-357" class="comments-container"><span id="360"></span><div id="comment-360" class="comment"><div id="post-360-score" class="comment-score"></div><div class="comment-text"><p>What's WireShart?</p></div><div id="comment-360-info" class="comment-info"><span class="comment-age">(29 Sep '10, 13:07)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="362"></span><div id="comment-362" class="comment"><div id="post-362-score" class="comment-score"></div><div class="comment-text"><p>I think it's actually WiresHart maybe... something electrical?</p></div><div id="comment-362-info" class="comment-info"><span class="comment-age">(29 Sep '10, 13:09)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-357" class="comment-tools"></div><div class="clear"></div><div id="comment-357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Gerald Combs 29 Sep '10, 13:14

</div>

</div>

</div>

