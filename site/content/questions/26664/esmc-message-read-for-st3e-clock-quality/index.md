+++
type = "question"
title = "ESMC message read for ST3E clock quality"
description = '''When I use Wireshark to decode an ESMC packet (Ethernet Sync Messaging Channel), it sees the quality level of stratum 3E as an invalid SSM message (1101 = SSM Code: QL-INV13) instead of QL-ST3E (1101 = SSM Code: QL-ST3E)'''
date = "2013-11-04T10:06:00Z"
lastmod = "2013-11-04T13:14:00Z"
weight = 26664
keywords = [ "esmc" ]
aliases = [ "/questions/26664" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ESMC message read for ST3E clock quality](/questions/26664/esmc-message-read-for-st3e-clock-quality)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26664-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26664-score" class="post-score" title="current number of votes">0</div><span id="post-26664-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I use Wireshark to decode an ESMC packet (Ethernet Sync Messaging Channel), it sees the quality level of stratum 3E as an invalid SSM message (1101 = SSM Code: QL-INV13) instead of QL-ST3E (1101 = SSM Code: QL-ST3E)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esmc" rel="tag" title="see questions tagged &#39;esmc&#39;">esmc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '13, 10:06</strong></p><img src="https://secure.gravatar.com/avatar/60a5042051afc8fa6efb36a9cff34ed5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marc%20Lefebvre&#39;s gravatar image" /><p><span>Marc Lefebvre</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marc Lefebvre has no accepted answers">0%</span></p></div></div><div id="comments-container-26664" class="comments-container"><span id="26665"></span><div id="comment-26665" class="comment"><div id="post-26665-score" class="comment-score">2</div><div class="comment-text"><p>If you think it's a bug you should file it at <a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a>. This site is for Q&amp;A, not for bug reporting ;-)</p></div><div id="comment-26665-info" class="comment-info"><span class="comment-age">(04 Nov '13, 13:14)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-26664" class="comment-tools"></div><div class="clear"></div><div id="comment-26664-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

