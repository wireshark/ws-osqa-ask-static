+++
type = "question"
title = "error building wireshark rpm for cent os 6.2"
description = '''Hi, I am trying to build a rpm for my modified wireshark source code version 1.8.0rc2. is there any steps on website where I can loop up inorder to generate rpm package?  Regards, Kerol '''
date = "2012-08-16T14:56:00Z"
lastmod = "2012-08-17T11:38:00Z"
weight = 13693
keywords = [ "rpm-package", "wireshark" ]
aliases = [ "/questions/13693" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [error building wireshark rpm for cent os 6.2](/questions/13693/error-building-wireshark-rpm-for-cent-os-62)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13693-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13693-score" class="post-score" title="current number of votes">0</div><span id="post-13693-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to build a rpm for my modified wireshark source code version 1.8.0rc2. is there any steps on website where I can loop up inorder to generate rpm package?</p><p>Regards, Kerol</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rpm-package" rel="tag" title="see questions tagged &#39;rpm-package&#39;">rpm-package</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '12, 14:56</strong></p><img src="https://secure.gravatar.com/avatar/4539bd8a993e934fff1bce2e159f7e15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kerolkarper&#39;s gravatar image" /><p><span>kerolkarper</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kerolkarper has no accepted answers">0%</span></p></div></div><div id="comments-container-13693" class="comments-container"></div><div id="comment-tools-13693" class="comment-tools"></div><div class="clear"></div><div id="comment-13693-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13698"></span>

<div id="answer-container-13698" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13698-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13698-score" class="post-score" title="current number of votes">0</div><span id="post-13698-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently I'm aware of <a href="http://www.wireshark.org/docs/wsdg_html_chunked/ChSrcBinary.html#ChSrcRpm">The Developers Guide</a> as the only source of information on it. Another source is the <a href="http://pkgs.fedoraproject.org/cgit/wireshark.git/tree/?id=f16">Fedora Core build files</a>. I guess CentOS has something similar, I just never looked it up.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Aug '12, 02:25</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13698" class="comments-container"></div><div id="comment-tools-13698" class="comment-tools"></div><div class="clear"></div><div id="comment-13698-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="13704"></span>

<div id="answer-container-13704" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13704-score" class="post-score" title="current number of votes">0</div><span id="post-13704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, first you need to get a wireshark.spec file. When I went through this exercise recently I think I started with the CentOS 6 spec file (which you can get out of their SRPM--source rpm) and modified it until it could build a usable RPM with Wireshark (at the time) 1.8.0.</p><p>The actual building of the RPM is done by:</p><ol><li>Put Wireshark source code (and maybe some other stuff) in ~/rpmbuild/SOURCES</li><li>Put wireshark.spec in ~/rpmbuild/SPECS</li><li><code>cd ~/rpmbuild/SPECS</code></li><li><code>rpmbuild -ba wireshark.spec</code></li></ol><p>Yes, it's on my list to update Wireshark's .spec file (in packaging/rpm/SPECS/) so it's easier to use or more up to date or something.</p><p>(I'd give you my .spec file but it's got a lot of my private stuff in there which would probably cause more confusion than help. If I do get it cleaned up, I'll put it in Wireshark's repository.)</p><p>BTW, I'd suggest not using 1.8.0rc2: that's a Release Candidate. The current version is 1.8.2.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Aug '12, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-13704" class="comments-container"><span id="13709"></span><div id="comment-13709" class="comment"><div id="post-13709-score" class="comment-score"></div><div class="comment-text"><p>Thanks mate. I am doing for homework purpose so whichever release it is , doesnt matter that much to me. Here is what I have done for Cent OS 6.2</p><p>I found the spec file in wireshark-root-dir/packaging/rpm/SPECS/wireshark.spec</p><p>modified it and</p><p>installed libqt4-dev libtool openssl-devel<br />
</p><p>then from wireshark-root-dir ran</p><p>./configure</p><p>make rpm-package</p><p>and it will build new rpm based on spec file. :)</p></div><div id="comment-13709-info" class="comment-info"><span class="comment-age">(17 Aug '12, 11:38)</span> <span class="comment-user userinfo">kerolkarper</span></div></div></div><div id="comment-tools-13704" class="comment-tools"></div><div class="clear"></div><div id="comment-13704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

