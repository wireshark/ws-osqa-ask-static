+++
type = "question"
title = "How to carture One pc on the network"
description = '''How to carture One pc on the network  my network ranage is 192.168.10.0/24 I install wireshark in 192.168.10.6 I have another pc 192.168.10.25  Now I want using 192.168.10.6 pc wireshark app scan 192.168.10.25  How to do this? please help me'''
date = "2014-02-06T19:33:00Z"
lastmod = "2014-02-06T22:44:00Z"
weight = 29505
keywords = [ "ip", "address" ]
aliases = [ "/questions/29505" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to carture One pc on the network](/questions/29505/how-to-carture-one-pc-on-the-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29505-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29505-score" class="post-score" title="current number of votes">0</div><span id="post-29505-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to carture One pc on the network</p><p>my network ranage is 192.168.10.0/24</p><p>I install wireshark in 192.168.10.6 I have another pc 192.168.10.25</p><p>Now I want using 192.168.10.6 pc wireshark app scan 192.168.10.25 How to do this? please help me</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '14, 19:33</strong></p><img src="https://secure.gravatar.com/avatar/7dd7fec0df894d099f35918291478c49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nature&#39;s gravatar image" /><p><span>nature</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nature has no accepted answers">0%</span></p></div></div><div id="comments-container-29505" class="comments-container"></div><div id="comment-tools-29505" class="comment-tools"></div><div class="clear"></div><div id="comment-29505-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29510"></span>

<div id="answer-container-29510" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29510-score" class="post-score" title="current number of votes">0</div><span id="post-29510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Capture Setup/Ethernet</a> for lots of information. (I'm assuming that you're running on an ethernet network).</p><p>The bottom line: if you're on an ethernet switched network and you want to see all the traffic sent/received by another pc on the network (and not just the traffic from your pac to that pc), things get difficult.</p><p><a href="http://wiki.wireshark.org/CaptureSetup">Capture Setup</a> has many links with information about capturing on different types of network.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '14, 22:44</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-29510" class="comments-container"><span id="29511"></span><div id="comment-29511" class="comment"><div id="post-29511-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-29511-info" class="comment-info"><span class="comment-age">(06 Feb '14, 22:44)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-29510" class="comment-tools"></div><div class="clear"></div><div id="comment-29510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

