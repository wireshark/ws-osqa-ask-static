+++
type = "question"
title = "How to configure WS on wireless home network"
description = '''Hi. I&#x27;m total newbie with WS. I have my laptop connected to modem through wireless connection. Many other devices (smartphones) are also connected to the same modem via wireless connection. How should I configure WS so that I could watch traffic, which goes in smartphones, on my laptop? This is my h...'''
date = "2017-10-17T14:03:00Z"
lastmod = "2017-10-17T19:32:00Z"
weight = 63981
keywords = [ "configuration" ]
aliases = [ "/questions/63981" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to configure WS on wireless home network](/questions/63981/how-to-configure-ws-on-wireless-home-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63981-score" class="post-score" title="current number of votes">0</div><span id="post-63981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. I'm total newbie with WS. I have my laptop connected to modem through wireless connection. Many other devices (smartphones) are also connected to the same modem via wireless connection. How should I configure WS so that I could watch traffic, which goes in smartphones, on my laptop? This is my home network.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '17, 14:03</strong></p><img src="https://secure.gravatar.com/avatar/b2c956957ac8dcd2ac4b554002f0f4ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="waldux&#39;s gravatar image" /><p><span>waldux</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="waldux has no accepted answers">0%</span></p></div></div><div id="comments-container-63981" class="comments-container"></div><div id="comment-tools-63981" class="comment-tools"></div><div class="clear"></div><div id="comment-63981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63987"></span>

<div id="answer-container-63987" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63987-score" class="post-score" title="current number of votes">0</div><span id="post-63987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '17, 19:32</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-63987" class="comments-container"></div><div id="comment-tools-63987" class="comment-tools"></div><div class="clear"></div><div id="comment-63987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

