+++
type = "question"
title = "How to find some informations in protocol TCP and UDP. Please read."
description = '''Hi everyone! :D So my teacher about Network subject (unfortunatelly it&#x27;s not my best) gave me 2 (easy for you).tcpd files:file 1, file 2. And I have to catch some information from this packets:  what kind of services was used ? any mistakes ? what was sent ? used adresses and protocols (take care ab...'''
date = "2013-05-23T13:34:00Z"
lastmod = "2013-05-24T01:03:00Z"
weight = 21422
keywords = [ "udp", "analyze", "packets", "tcp", "tcpd" ]
aliases = [ "/questions/21422" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to find some informations in protocol TCP and UDP. Please read.](/questions/21422/how-to-find-some-informations-in-protocol-tcp-and-udp-please-read)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21422-score" class="post-score" title="current number of votes">0</div><span id="post-21422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone! :D</p><p>So my teacher about Network subject (unfortunatelly it's not my best) gave me 2 (easy for you).tcpd files:<a href="http://marcing.faculty.wmi.amu.edu.pl/DSIKLI0/3.tcpd">file 1</a>, <a href="http://marcing.faculty.wmi.amu.edu.pl/DSIKLI0/6.tcpd">file 2</a>. And I have to catch some information from this packets:</p><ol><li>what kind of services was used ? any mistakes ? what was sent ?</li><li>used adresses and protocols (take care about class of adresses and translation of adresses (NAT) and transport protocols</li><li>which systems was installed on the computers (for example: client - Windwos, server: Linux)</li><li>how distance (number of routers) was between devices</li><li>where was started scanner (server, client or for example 3 routers far from server)</li><li>time of made capture</li><li>configuration of local network (for example default router, adressed MAC)</li><li>how its possible to repeat captured comunication (give program or make a printscreen)</li><li>(additional) catch packets received and sent through the program tracepath or traceroute</li><li>(additional) catch question sent to DNS server</li></ol><p>I already did point 1,2 (except NAT),6,7(only MAC). Its not that I wanna that somebody make this instead of me, but please let me give some help. Its best for me if you just write me where in this packets I can find information for example about configuration of local network.</p><p>Regards !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-analyze" rel="tag" title="see questions tagged &#39;analyze&#39;">analyze</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-tcpd" rel="tag" title="see questions tagged &#39;tcpd&#39;">tcpd</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '13, 13:34</strong></p><img src="https://secure.gravatar.com/avatar/dec35211bf14f0a03912b9f327798acf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Toni77&#39;s gravatar image" /><p><span>Toni77</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Toni77 has no accepted answers">0%</span></p></div></div><div id="comments-container-21422" class="comments-container"><span id="21435"></span><div id="comment-21435" class="comment"><div id="post-21435-score" class="comment-score"></div><div class="comment-text"><p>The tracefiles that you refer to don't seem to match the questions. Maybe this is why you have trouble answering the questions yourself?</p></div><div id="comment-21435-info" class="comment-info"><span class="comment-age">(24 May '13, 01:03)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-21422" class="comment-tools"></div><div class="clear"></div><div id="comment-21422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

