+++
type = "question"
title = "How to monitor internet bandwidth consumption"
description = '''HI. I&#x27;ve this issue. From some weeks I&#x27;ve noted a general slowdown on my ADSL Internet Connection. In my network there are 6 servers and e few number of clients. The six server are: a VoIP server, a DC, a FreeNAS server, etc... The question is: may I use WireShark to monitor and identify how much AD...'''
date = "2014-09-30T00:47:00Z"
lastmod = "2014-09-30T18:56:00Z"
weight = 36716
keywords = [ "bandwidthutilization" ]
aliases = [ "/questions/36716" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to monitor internet bandwidth consumption](/questions/36716/how-to-monitor-internet-bandwidth-consumption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36716-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36716-score" class="post-score" title="current number of votes">0</div><span id="post-36716-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI. I've this issue. From some weeks I've noted a general slowdown on my ADSL Internet Connection. In my network there are 6 servers and e few number of clients. The six server are: a VoIP server, a DC, a FreeNAS server, etc... The question is: may I use WireShark to monitor and identify how much ADLS bandwidth each server is consuming in real time? In other words: may I know (and monitor) during the day (and night) which type of upload and download connection every server is performing so may I isolate the source of the issue (general internet slowdown)? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidthutilization" rel="tag" title="see questions tagged &#39;bandwidthutilization&#39;">bandwidthutilization</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '14, 00:47</strong></p><img src="https://secure.gravatar.com/avatar/d51704b8a42befd4e65f1b85743233d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m4biz&#39;s gravatar image" /><p><span>m4biz</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m4biz has no accepted answers">0%</span></p></div></div><div id="comments-container-36716" class="comments-container"></div><div id="comment-tools-36716" class="comment-tools"></div><div class="clear"></div><div id="comment-36716-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36740"></span>

<div id="answer-container-36740" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36740-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36740-score" class="post-score" title="current number of votes">0</div><span id="post-36740-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I saw this really cool video on YouTube, I don't know if it will be much help to you.</p><p><a href="http://www.youtube.com/watch?v=HYs1azeDnZ4">http://www.youtube.com/watch?v=HYs1azeDnZ4</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Sep '14, 18:56</strong></p><img src="https://secure.gravatar.com/avatar/4784c5fb1a0142030d51a339706a456c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beldum&#39;s gravatar image" /><p><span>Beldum</span><br />
<span class="score" title="49 reputation points">49</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beldum has no accepted answers">0%</span></p></div></div><div id="comments-container-36740" class="comments-container"></div><div id="comment-tools-36740" class="comment-tools"></div><div class="clear"></div><div id="comment-36740-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

