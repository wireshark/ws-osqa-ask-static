+++
type = "question"
title = "802.11 Protocol"
description = '''Hi, I have a 802.11 wireless sniffing captured file. How to start with ? How to extract the data out of the pcap ?'''
date = "2013-09-03T13:50:00Z"
lastmod = "2013-09-03T22:06:00Z"
weight = 24315
keywords = [ "802.11" ]
aliases = [ "/questions/24315" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [802.11 Protocol](/questions/24315/80211-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24315-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24315-score" class="post-score" title="current number of votes">0</div><span id="post-24315-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have a 802.11 wireless sniffing captured file. How to start with ? How to extract the data out of the pcap ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '13, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/e3b0323745a98f44e1e5de8a081bf52c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vj%20kumar&#39;s gravatar image" /><p><span>vj kumar</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vj kumar has no accepted answers">0%</span></p></div></div><div id="comments-container-24315" class="comments-container"></div><div id="comment-tools-24315" class="comment-tools"></div><div class="clear"></div><div id="comment-24315-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24325"></span>

<div id="answer-container-24325" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24325-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24325-score" class="post-score" title="current number of votes">0</div><span id="post-24325-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If the traffic is not encrypted (not on a "protected" networking using WEP or WPA/WPA2), Wireshark should do that for you.</p><p>If the traffic <em>is</em> encrypted, you will have to <a href="http://wiki.wireshark.org/HowToDecrypt802.11">configure Wireshark to decrypt it</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Sep '13, 22:06</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-24325" class="comments-container"></div><div id="comment-tools-24325" class="comment-tools"></div><div class="clear"></div><div id="comment-24325-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

