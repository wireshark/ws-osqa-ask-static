+++
type = "question"
title = "Wireshark on PPPoE?"
description = '''Is use of Wireshark in a windows OS different on PPPoE then Other ISP uses? Just for starters, malformed packets are the majority of what I&#x27;m finding. Checksums never match what they are suppose to be.'''
date = "2010-12-05T18:40:00Z"
lastmod = "2010-12-08T16:48:00Z"
weight = 1251
keywords = [ "pppoe" ]
aliases = [ "/questions/1251" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on PPPoE?](/questions/1251/wireshark-on-pppoe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1251-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1251-score" class="post-score" title="current number of votes">0</div><span id="post-1251-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is use of Wireshark in a windows OS different on PPPoE then Other ISP uses? Just for starters, malformed packets are the majority of what I'm finding. Checksums never match what they are suppose to be.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pppoe" rel="tag" title="see questions tagged &#39;pppoe&#39;">pppoe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '10, 18:40</strong></p><img src="https://secure.gravatar.com/avatar/5b11deb852a57a459c4fb39592d2fb02?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Algonquian_Cougar&#39;s gravatar image" /><p><span>Algonquian_C...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Algonquian_Cougar has no accepted answers">0%</span></p></div></div><div id="comments-container-1251" class="comments-container"></div><div id="comment-tools-1251" class="comment-tools"></div><div class="clear"></div><div id="comment-1251-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1258"></span>

<div id="answer-container-1258" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1258-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1258-score" class="post-score" title="current number of votes">0</div><span id="post-1258-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here are a couple of links to checksum errors:</p><p>http://ask.wireshark.org/questions/830/tcp-checksum-errors</p><p>and</p><p>http://www.wireshark.org/docs/wsug_html_chunked/ChAdvChecksums.html</p><p>Eric</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Dec '10, 09:16</strong></p><img src="https://secure.gravatar.com/avatar/d5aa09edfeeb0600f74a72e63806f227?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="erics&#39;s gravatar image" /><p><span>erics</span><br />
<span class="score" title="46 reputation points">46</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="erics has no accepted answers">0%</span></p></div></div><div id="comments-container-1258" class="comments-container"><span id="1292"></span><div id="comment-1292" class="comment"><div id="post-1292-score" class="comment-score"></div><div class="comment-text"><p>Eric, thank you on your link on your HeaderChecksum. Mine are going out fine. It's what's coming in. Plus all Packets are Malformed and Errored or Warned. But here again the outgoing is fine. Plus many other server problems at my ISP. From their DNS, DHCP, and their Exchange Server. I don't think it could be possible their Routered relays are the problem. Took them 9 months just to get my contracted service speed (bandwidth) set right. Which literally amounted to turning a switch on. I can't imagine what getting this corrected will take, in an amount of time!</p></div><div id="comment-1292-info" class="comment-info"><span class="comment-age">(08 Dec '10, 16:48)</span> <span class="comment-user userinfo">Algonquian_C...</span></div></div></div><div id="comment-tools-1258" class="comment-tools"></div><div class="clear"></div><div id="comment-1258-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

