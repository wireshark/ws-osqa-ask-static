+++
type = "question"
title = "When do you support Lua5.3?"
description = '''Lua5.3 has been producted more than two years, and I&#x27;d like to know when to support it, or never?'''
date = "2017-07-19T22:12:00Z"
lastmod = "2017-07-20T05:02:00Z"
weight = 62903
keywords = [ "lua" ]
aliases = [ "/questions/62903" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [When do you support Lua5.3?](/questions/62903/when-do-you-support-lua53)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62903-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62903-score" class="post-score" title="current number of votes">0</div><span id="post-62903-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Lua5.3 has been producted more than two years, and I'd like to know when to support it, or never?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '17, 22:12</strong></p><img src="https://secure.gravatar.com/avatar/f55622080a70f3b5f1e4835f091b773c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pig4210&#39;s gravatar image" /><p><span>pig4210</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pig4210 has no accepted answers">0%</span></p></div></div><div id="comments-container-62903" class="comments-container"><span id="62907"></span><div id="comment-62907" class="comment"><div id="post-62907-score" class="comment-score"></div><div class="comment-text"><p>There's a lengthy discussion here listing pros and cons <a href="https://www.wireshark.org/lists/wireshark-dev/201608/msg00155.html">https://www.wireshark.org/lists/wireshark-dev/201608/msg00155.html</a></p><p>I don't quite remeber the final verdict but I think something did not compile on one of our platforms with LUA 5.3</p></div><div id="comment-62907-info" class="comment-info"><span class="comment-age">(20 Jul '17, 00:21)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="62908"></span><div id="comment-62908" class="comment"><div id="post-62908-score" class="comment-score"></div><div class="comment-text"><p>More stuff here <a href="https://ask.wireshark.org/questions/44219/running-wireshark-with-lua-devel-53">https://ask.wireshark.org/questions/44219/running-wireshark-with-lua-devel-53</a></p></div><div id="comment-62908-info" class="comment-info"><span class="comment-age">(20 Jul '17, 00:23)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-62903" class="comment-tools"></div><div class="clear"></div><div id="comment-62903-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62906"></span>

<div id="answer-container-62906" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62906-score" class="post-score" title="current number of votes">0</div><span id="post-62906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There has been a <a href="https://www.wireshark.org/lists/wireshark-dev/201608/msg00082.html">longer thread at the developer mailing list</a> some time ago to support Lua 5.3.</p><p>As I've understood it: no currently active plans. Devs: Correct me when I'm wrong.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '17, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-62906" class="comments-container"><span id="62925"></span><div id="comment-62925" class="comment"><div id="post-62925-score" class="comment-score">1</div><div class="comment-text"><p>You are correct.</p></div><div id="comment-62925-info" class="comment-info"><span class="comment-age">(20 Jul '17, 05:02)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-62906" class="comment-tools"></div><div class="clear"></div><div id="comment-62906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

