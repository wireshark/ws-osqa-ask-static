+++
type = "question"
title = "Batch export to csv of .pcap files"
description = '''Hi all,  I have been attempting to use TShark in a batch method to process a significant number of pcap files to .csv files.  I am aware that this can be done using the &quot;-T fields&quot; approach, or just as &quot;-T text&quot;. However, the output I want is just as it would be presented in wireshark, i.e. an overa...'''
date = "2012-03-12T02:05:00Z"
lastmod = "2012-03-14T10:54:00Z"
weight = 9484
keywords = [ "csv", "export", "tshark", "batch" ]
aliases = [ "/questions/9484" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Batch export to csv of .pcap files](/questions/9484/batch-export-to-csv-of-pcap-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9484-score" class="post-score" title="current number of votes">0</div><span id="post-9484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I have been attempting to use TShark in a batch method to process a significant number of pcap files to .csv files.</p><p>I am aware that this can be done using the "-T fields" approach, or just as "-T text". However, the output I want is just as it would be presented in wireshark, i.e. an overall source, not just an IP or Ethernet address, etc. I'd also like to include the information field, and I would also like to not lose the granularity of the protocol, and have this displayed in text, as opposed to an index.</p><p>Does anyone have any suggestions about how to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-csv" rel="tag" title="see questions tagged &#39;csv&#39;">csv</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-batch" rel="tag" title="see questions tagged &#39;batch&#39;">batch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '12, 02:05</strong></p><img src="https://secure.gravatar.com/avatar/661039db20010e63da14f1153417aea9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TimeLord86&#39;s gravatar image" /><p><span>TimeLord86</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TimeLord86 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Mar '12, 02:05</strong> </span></p></div></div><div id="comments-container-9484" class="comments-container"><span id="9541"></span><div id="comment-9541" class="comment"><div id="post-9541-score" class="comment-score"></div><div class="comment-text"><p>It's not entirely clear which fields you want, i.e. what do you mean by "overall source, not just IP or Ethernet address"? The "granularity of the protocol" isn't clear either.</p><p>If you explain your requirements more clearly someone may be able to help.</p></div><div id="comment-9541-info" class="comment-info"><span class="comment-age">(14 Mar '12, 10:54)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-9484" class="comment-tools"></div><div class="clear"></div><div id="comment-9484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

