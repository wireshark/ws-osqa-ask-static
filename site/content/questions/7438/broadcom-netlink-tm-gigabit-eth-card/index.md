+++
type = "question"
title = "Broadcom NetLink (TM) Gigabit Eth card"
description = '''Hello  The Broadcom NetLink (TM) Gigabit Eth card supports capturing vlan tag packet with wireshark? Thanks !'''
date = "2011-11-15T06:13:00Z"
lastmod = "2011-12-15T03:26:00Z"
weight = 7438
keywords = [ "broadcom", "netlink", "vlan", "tag" ]
aliases = [ "/questions/7438" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Broadcom NetLink (TM) Gigabit Eth card](/questions/7438/broadcom-netlink-tm-gigabit-eth-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7438-score" class="post-score" title="current number of votes">0</div><span id="post-7438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello The Broadcom NetLink (TM) Gigabit Eth card supports capturing vlan tag packet with wireshark?</p><p>Thanks !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadcom" rel="tag" title="see questions tagged &#39;broadcom&#39;">broadcom</span> <span class="post-tag tag-link-netlink" rel="tag" title="see questions tagged &#39;netlink&#39;">netlink</span> <span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '11, 06:13</strong></p><img src="https://secure.gravatar.com/avatar/a38e15befb6d704b57aa8513c40c2ef4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akalavri&#39;s gravatar image" /><p><span>akalavri</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akalavri has no accepted answers">0%</span></p></div></div><div id="comments-container-7438" class="comments-container"></div><div id="comment-tools-7438" class="comment-tools"></div><div class="clear"></div><div id="comment-7438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="7521"></span>

<div id="answer-container-7521" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7521-score" class="post-score" title="current number of votes">0</div><span id="post-7521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think in the advanced settings of the broadcom NIC you have to turn on 802.1q tagging. I can't remember what the default is. Also, depending on what kind of switches you're using you have to enable them to pass on the 802.1q vlan tags. I've seen it as an option in some Cisco switches.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '11, 17:41</strong></p><img src="https://secure.gravatar.com/avatar/1f3966b6e9de3a63326e2d3fd51c8c04?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John_Modlin&#39;s gravatar image" /><p><span>John_Modlin</span><br />
<span class="score" title="120 reputation points">120</span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John_Modlin has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-7521" class="comments-container"></div><div id="comment-tools-7521" class="comment-tools"></div><div class="clear"></div><div id="comment-7521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="7985"></span>

<div id="answer-container-7985" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7985-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7985-score" class="post-score" title="current number of votes">0</div><span id="post-7985-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>After installing the Broadcom Advanced control suite 3 I be able to set up vlan tags in my network card.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Dec '11, 03:26</strong></p><img src="https://secure.gravatar.com/avatar/a38e15befb6d704b57aa8513c40c2ef4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akalavri&#39;s gravatar image" /><p><span>akalavri</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akalavri has no accepted answers">0%</span></p></div></div><div id="comments-container-7985" class="comments-container"></div><div id="comment-tools-7985" class="comment-tools"></div><div class="clear"></div><div id="comment-7985-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

