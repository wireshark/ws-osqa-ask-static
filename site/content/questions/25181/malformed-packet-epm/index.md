+++
type = "question"
title = "Malformed packet: EPM"
description = '''Hi, Is there anyone had experienced to worked on trobleshooting the malformed packet:epm alerts from the expert infos? While i&#x27;m working on my trace file I notice these alerts in several frame (about 15 times). I working on the outlook slowdown issue. '''
date = "2013-09-24T17:29:00Z"
lastmod = "2013-09-24T19:57:00Z"
weight = 25181
keywords = [ "packet", "malformed" ]
aliases = [ "/questions/25181" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed packet: EPM](/questions/25181/malformed-packet-epm)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25181-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25181-score" class="post-score" title="current number of votes">0</div><span id="post-25181-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Is there anyone had experienced to worked on trobleshooting the malformed packet:epm alerts from the expert infos? While i'm working on my trace file I notice these alerts in several frame (about 15 times). I working on the outlook slowdown issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '13, 17:29</strong></p><img src="https://secure.gravatar.com/avatar/9e98ee16269051111f8ed9855ef24c8f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lma&#39;s gravatar image" /><p><span>lma</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lma has no accepted answers">0%</span></p></div></div><div id="comments-container-25181" class="comments-container"><span id="25185"></span><div id="comment-25185" class="comment"><div id="post-25185-score" class="comment-score"></div><div class="comment-text"><p>If possible, please upload a capture file to <a href="http://cloudshark.org/">cloudshark</a> and provide the link to it so someone can perform some analysis on it.</p></div><div id="comment-25185-info" class="comment-info"><span class="comment-age">(24 Sep '13, 19:57)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-25181" class="comment-tools"></div><div class="clear"></div><div id="comment-25181-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

