+++
type = "question"
title = "Visual studio solution file"
description = '''Hello, has somebody created a .sln file to use Visual Studio IDE for Wireshark ? At present I use simple text editor + make, but it is not really convenient. Thank you a lot in advance, Yuriy'''
date = "2013-05-29T08:31:00Z"
lastmod = "2013-05-29T08:42:00Z"
weight = 21567
keywords = [ "visual-studio" ]
aliases = [ "/questions/21567" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Visual studio solution file](/questions/21567/visual-studio-solution-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21567-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21567-score" class="post-score" title="current number of votes">0</div><span id="post-21567-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>has somebody created a .sln file to use Visual Studio IDE for Wireshark ? At present I use simple text editor + make, but it is not really convenient.</p><p>Thank you a lot in advance, Yuriy</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-visual-studio" rel="tag" title="see questions tagged &#39;visual-studio&#39;">visual-studio</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '13, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/3799a296db769e662c5a90d85c6999c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yuriy&#39;s gravatar image" /><p><span>Yuriy</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yuriy has no accepted answers">0%</span></p></div></div><div id="comments-container-21567" class="comments-container"></div><div id="comment-tools-21567" class="comment-tools"></div><div class="clear"></div><div id="comment-21567-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21568"></span>

<div id="answer-container-21568" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21568-score" class="post-score" title="current number of votes">1</div><span id="post-21568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not really, there is one in the source tree but it's long out of date. I've used a text editor and nmake for the last 14 years or so of my work with Wireshark dev and haven't found too many issues.</p><p>IIRC the major problem is that VS solutions are very picky about stuff outside of the direct source tree, e.g. the win32 libs and require absolute paths and so are quite inflexible. I haven't really looked at this since the latest VS solution changes in VS2010 so it may be better now.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '13, 08:42</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-21568" class="comments-container"></div><div id="comment-tools-21568" class="comment-tools"></div><div class="clear"></div><div id="comment-21568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

