+++
type = "question"
title = "~/.wireshark/ethers not working, wireshark v2.2.6"
description = '''/etc/ethers works, but not in the home directory'''
date = "2017-10-07T12:51:00Z"
lastmod = "2017-10-07T14:57:00Z"
weight = 63729
keywords = [ "ethers" ]
aliases = [ "/questions/63729" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [~/.wireshark/ethers not working, wireshark v2.2.6](/questions/63729/wiresharkethers-not-working-wireshark-v226)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63729-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63729-score" class="post-score" title="current number of votes">0</div><span id="post-63729-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>/etc/ethers works, but not in the home directory</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethers" rel="tag" title="see questions tagged &#39;ethers&#39;">ethers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '17, 12:51</strong></p><img src="https://secure.gravatar.com/avatar/d4659f38a392fe3d2b0f19ac1863e7a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="naisanza&#39;s gravatar image" /><p><span>naisanza</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="naisanza has no accepted answers">0%</span></p></div></div><div id="comments-container-63729" class="comments-container"><span id="63730"></span><div id="comment-63730" class="comment"><div id="post-63730-score" class="comment-score"></div><div class="comment-text"><p>so I assume you're <strong>not</strong> running Wireshark as root, and put the ethers file in your user home directory?</p></div><div id="comment-63730-info" class="comment-info"><span class="comment-age">(07 Oct '17, 14:13)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="63732"></span><div id="comment-63732" class="comment"><div id="post-63732-score" class="comment-score"></div><div class="comment-text"><p><a href="https://ask.wireshark.org/users/145/jasper">@Jasper</a> no you were right, it was running as root. I went to look up how to run it as a user</p></div><div id="comment-63732-info" class="comment-info"><span class="comment-age">(07 Oct '17, 14:57)</span> <span class="comment-user userinfo">naisanza</span></div></div></div><div id="comment-tools-63729" class="comment-tools"></div><div class="clear"></div><div id="comment-63729-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

