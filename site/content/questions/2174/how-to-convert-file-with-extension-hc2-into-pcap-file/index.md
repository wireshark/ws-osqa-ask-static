+++
type = "question"
title = "how to convert file with extension .hc2 into pcap file?"
description = '''I have a file with extension .hc2.I want to convert this file into pcap format.I have changed the extension by using command window but after opening this file with wireshark it shows error &quot;isnt a capture file understand by wirehsark &quot; . :-( Can Anyone help me to solve this problem.  Thanks. sachet'''
date = "2011-02-05T21:51:00Z"
lastmod = "2011-02-13T13:37:00Z"
weight = 2174
keywords = [ "fomat" ]
aliases = [ "/questions/2174" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to convert file with extension .hc2 into pcap file?](/questions/2174/how-to-convert-file-with-extension-hc2-into-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2174-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2174-score" class="post-score" title="current number of votes">0</div><span id="post-2174-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a file with extension .hc2.I want to convert this file into pcap format.I have changed the extension by using command window but after opening this file with wireshark it shows error "isnt a capture file understand by wirehsark " . :-(</p><p>Can Anyone help me to solve this problem.</p><p>Thanks. sachet</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fomat" rel="tag" title="see questions tagged &#39;fomat&#39;">fomat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '11, 21:51</strong></p><img src="https://secure.gravatar.com/avatar/b5215898b3dc18424f88afd7031ff303?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SACHET&#39;s gravatar image" /><p><span>SACHET</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SACHET has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Feb '11, 21:53</strong> </span></p></div></div><div id="comments-container-2174" class="comments-container"></div><div id="comment-tools-2174" class="comment-tools"></div><div class="clear"></div><div id="comment-2174-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2175"></span>

<div id="answer-container-2175" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2175-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2175-score" class="post-score" title="current number of votes">0</div><span id="post-2175-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Which program created the file?</p><p>As wireshark does not recognize the file you might want to check whether that program is able to save the file in libpcap format or you need an external program to convert this filetype to a libpcap based file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '11, 01:12</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-2175" class="comments-container"><span id="2311"></span><div id="comment-2311" class="comment"><div id="post-2311-score" class="comment-score"></div><div class="comment-text"><p>...or find documentation for the .hc2 file format, so that code could perhaps be written for Wireshark to read those files.</p></div><div id="comment-2311-info" class="comment-info"><span class="comment-age">(13 Feb '11, 13:37)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-2175" class="comment-tools"></div><div class="clear"></div><div id="comment-2175-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

