+++
type = "question"
title = "about protocol recognizing"
description = '''iec-104 runs on 2407 port and recogizes as &quot;orion&quot; protocol. it is bad because of wireshark cant dicipher data. how can i adjust this program to recognize correctly this protocol on port 2407? Sorry for bad-bad english))'''
date = "2013-04-24T04:49:00Z"
lastmod = "2013-04-24T05:55:00Z"
weight = 20761
keywords = [ "iec-104" ]
aliases = [ "/questions/20761" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [about protocol recognizing](/questions/20761/about-protocol-recognizing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20761-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20761-score" class="post-score" title="current number of votes">0</div><span id="post-20761-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>iec-104 runs on 2407 port and recogizes as "orion" protocol. it is bad because of wireshark cant dicipher data. how can i adjust this program to recognize correctly this protocol on port 2407?</p><p>Sorry for bad-bad english))</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iec-104" rel="tag" title="see questions tagged &#39;iec-104&#39;">iec-104</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '13, 04:49</strong></p><img src="https://secure.gravatar.com/avatar/61b7b49e8e7552dd688a9498b1679bcd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Curecool&#39;s gravatar image" /><p><span>Curecool</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Curecool has no accepted answers">0%</span></p></div></div><div id="comments-container-20761" class="comments-container"></div><div id="comment-tools-20761" class="comment-tools"></div><div class="clear"></div><div id="comment-20761-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20762"></span>

<div id="answer-container-20762" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20762-score" class="post-score" title="current number of votes">2</div><span id="post-20762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Curecool has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Right click on any of the packets and select: 'Decode As'</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Apr '13, 05:18</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-20762" class="comments-container"><span id="20763"></span><div id="comment-20763" class="comment"><div id="post-20763-score" class="comment-score"></div><div class="comment-text"><p>Thank you wery much!!!</p></div><div id="comment-20763-info" class="comment-info"><span class="comment-age">(24 Apr '13, 05:55)</span> <span class="comment-user userinfo">Curecool</span></div></div></div><div id="comment-tools-20762" class="comment-tools"></div><div class="clear"></div><div id="comment-20762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

