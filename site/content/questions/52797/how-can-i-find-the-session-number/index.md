+++
type = "question"
title = "How can I find the session number ?"
description = '''Hey guys. Does anyone know how to capture a session number ? I have searched everywhere and no one has asked this question yet. I have a person who always use VPN and I know him bcs he is the only person who use it. I want to get his session number so I can confront him with it.'''
date = "2016-05-19T16:50:00Z"
lastmod = "2016-05-20T02:27:00Z"
weight = 52797
keywords = [ "cookies", "session", "number", "wireshark" ]
aliases = [ "/questions/52797" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I find the session number ?](/questions/52797/how-can-i-find-the-session-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52797-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52797-score" class="post-score" title="current number of votes">0</div><span id="post-52797-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey guys.</p><p>Does anyone know how to capture a session number ? I have searched everywhere and no one has asked this question yet.</p><p>I have a person who always use VPN and I know him bcs he is the only person who use it. I want to get his session number so I can confront him with it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cookies" rel="tag" title="see questions tagged &#39;cookies&#39;">cookies</span> <span class="post-tag tag-link-session" rel="tag" title="see questions tagged &#39;session&#39;">session</span> <span class="post-tag tag-link-number" rel="tag" title="see questions tagged &#39;number&#39;">number</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 May '16, 16:50</strong></p><img src="https://secure.gravatar.com/avatar/9c524a76dfdec4dce33b9809889555e4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="azoz158&#39;s gravatar image" /><p><span>azoz158</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="azoz158 has no accepted answers">0%</span></p></div></div><div id="comments-container-52797" class="comments-container"><span id="52802"></span><div id="comment-52802" class="comment"><div id="post-52802-score" class="comment-score"></div><div class="comment-text"><p>What does session number mean? Can you elaborate?</p></div><div id="comment-52802-info" class="comment-info"><span class="comment-age">(20 May '16, 02:27)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-52797" class="comment-tools"></div><div class="clear"></div><div id="comment-52797-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

