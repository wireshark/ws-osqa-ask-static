+++
type = "question"
title = "Examples of PPP"
description = '''Hallo,Can anybody give me examples of PPP traffic , with bsd-compression, with deflate-compression and with some others types of compression?'''
date = "2013-02-18T09:40:00Z"
lastmod = "2013-02-18T14:50:00Z"
weight = 18709
keywords = [ "ppp" ]
aliases = [ "/questions/18709" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Examples of PPP](/questions/18709/examples-of-ppp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18709-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18709-score" class="post-score" title="current number of votes">0</div><span id="post-18709-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hallo,Can anybody give me examples of PPP traffic , with bsd-compression, with deflate-compression and with some others types of compression?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ppp" rel="tag" title="see questions tagged &#39;ppp&#39;">ppp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '13, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/48a6d6935069784c27df9a24a5aba9dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kokolbin&#39;s gravatar image" /><p><span>Kokolbin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kokolbin has no accepted answers">0%</span></p></div></div><div id="comments-container-18709" class="comments-container"></div><div id="comment-tools-18709" class="comment-tools"></div><div class="clear"></div><div id="comment-18709-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18720"></span>

<div id="answer-container-18720" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18720-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18720-score" class="post-score" title="current number of votes">0</div><span id="post-18720-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is a pcap file on packetlife.net with VJ compression (PPP_TCP_compression.cap).</p><blockquote><p><code>http://packetlife.net/captures/protocol/ppp/</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Feb '13, 14:50</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-18720" class="comments-container"></div><div id="comment-tools-18720" class="comment-tools"></div><div class="clear"></div><div id="comment-18720-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

