+++
type = "question"
title = "home network"
description = '''Hi, I&#x27;m using a desktop and accessing the router using a cable. When I start to capturing the packets, it only capture packets that are only from my desktop IP. I would like to know if is possible that I can capture other machine IP address that are connected to the router too?'''
date = "2011-03-31T08:30:00Z"
lastmod = "2011-03-31T09:46:00Z"
weight = 3246
keywords = [ "home", "network" ]
aliases = [ "/questions/3246" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [home network](/questions/3246/home-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3246-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3246-score" class="post-score" title="current number of votes">0</div><span id="post-3246-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm using a desktop and accessing the router using a cable. When I start to capturing the packets, it only capture packets that are only from my desktop IP. I would like to know if is possible that I can capture other machine IP address that are connected to the router too?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-home" rel="tag" title="see questions tagged &#39;home&#39;">home</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '11, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/76e8108d97b4c6317e1fc6ea552bc111?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MaxGenesis%20Tee&#39;s gravatar image" /><p><span>MaxGenesis Tee</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MaxGenesis Tee has no accepted answers">0%</span></p></div></div><div id="comments-container-3246" class="comments-container"></div><div id="comment-tools-3246" class="comment-tools"></div><div class="clear"></div><div id="comment-3246-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3247"></span>

<div id="answer-container-3247" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3247-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3247-score" class="post-score" title="current number of votes">1</div><span id="post-3247-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess your router has a couple of switch ports that other devices are connected to. If the router can mirror packets coming and going to other ports to your port you might be able to capture other machines as well, but usually home network devices don't have that kind of feature. In that case the only other option is to go a little black hat and use arp cache poisoning to see the traffic of others, but I would not recommend that for any kind of serious network analysis capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Mar '11, 09:46</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-3247" class="comments-container"></div><div id="comment-tools-3247" class="comment-tools"></div><div class="clear"></div><div id="comment-3247-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

