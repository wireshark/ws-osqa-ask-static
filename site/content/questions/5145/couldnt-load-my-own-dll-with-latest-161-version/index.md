+++
type = "question"
title = "couldn&#x27;t load my own dll with latest 1.6.1 version"
description = '''couldn&#x27;t load my own dll with latest 1.6.1 version, got message like:  The specified procedure could not be found  But my own dlls were working fine with the old versions.'''
date = "2011-07-20T08:55:00Z"
lastmod = "2011-07-20T09:48:00Z"
weight = 5145
keywords = [ "dissector", "compatibility", "dll" ]
aliases = [ "/questions/5145" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [couldn't load my own dll with latest 1.6.1 version](/questions/5145/couldnt-load-my-own-dll-with-latest-161-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5145-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5145-score" class="post-score" title="current number of votes">0</div><span id="post-5145-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>couldn't load my own dll with latest 1.6.1 version, got message like:</p><blockquote><p>The specified procedure could not be found</p></blockquote><p>But my own dlls were working fine with the old versions.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-compatibility" rel="tag" title="see questions tagged &#39;compatibility&#39;">compatibility</span> <span class="post-tag tag-link-dll" rel="tag" title="see questions tagged &#39;dll&#39;">dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '11, 08:55</strong></p><img src="https://secure.gravatar.com/avatar/5b9ff28cdcc424cf0d1e087c86249334?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lulu&#39;s gravatar image" /><p><span>lulu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lulu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jul '11, 15:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5145" class="comments-container"><span id="5148"></span><div id="comment-5148" class="comment"><div id="post-5148-score" class="comment-score">2</div><div class="comment-text"><p>Have you recompiled your plugin against the new version? Compatibility of plugins with different versions is not guaranteed (or suggested).</p></div><div id="comment-5148-info" class="comment-info"><span class="comment-age">(20 Jul '11, 09:48)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-5145" class="comment-tools"></div><div class="clear"></div><div id="comment-5145-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

