+++
type = "question"
title = "Contacts for Support"
description = '''Is there a phone number to call to speak to a Tech? I&#x27;m am trying to isolate particular devices to monitor. I have set up Wireshark and have been monitoring for about a week. The time stamp is not something I can translate and I had captured all traffic on the network. If there is a way to filter af...'''
date = "2013-03-25T09:43:00Z"
lastmod = "2013-03-25T09:53:00Z"
weight = 19816
keywords = [ "supp" ]
aliases = [ "/questions/19816" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Contacts for Support](/questions/19816/contacts-for-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19816-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19816-score" class="post-score" title="current number of votes">0</div><span id="post-19816-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a phone number to call to speak to a Tech? I'm am trying to isolate particular devices to monitor. I have set up Wireshark and have been monitoring for about a week. The time stamp is not something I can translate and I had captured all traffic on the network. If there is a way to filter after the capture then I need to learn how.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-supp" rel="tag" title="see questions tagged &#39;supp&#39;">supp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '13, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/4de8fffaf56a3cb2107f3049d51c47ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GenoIL&#39;s gravatar image" /><p><span>GenoIL</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GenoIL has no accepted answers">0%</span></p></div></div><div id="comments-container-19816" class="comments-container"></div><div id="comment-tools-19816" class="comment-tools"></div><div class="clear"></div><div id="comment-19816-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19817"></span>

<div id="answer-container-19817" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19817-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19817-score" class="post-score" title="current number of votes">1</div><span id="post-19817-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think that there is a number to speak to a tech, but maybe someone at this Q&amp;A site can help - which would require you to ask specific questions. Or take a look at the help ressources here: <a href="http://www.wireshark.org/docs/">http://www.wireshark.org/docs/</a></p><p>To get you started: if you try to isolate devices try filtering on their IP addresses, like "ip.addr==a.b.c.d". Or open up the Endpoint or Conversation Statistics. Protocol Hierarchy is a good thing, too. You can also change what the time column displays in the View menu -&gt; Time Display Format.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Mar '13, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Mar '13, 09:56</strong> </span></p></div></div><div id="comments-container-19817" class="comments-container"></div><div id="comment-tools-19817" class="comment-tools"></div><div class="clear"></div><div id="comment-19817-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

