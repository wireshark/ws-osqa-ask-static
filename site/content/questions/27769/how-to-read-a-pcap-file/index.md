+++
type = "question"
title = "How to read a pcap file"
description = '''My major is business and ICT course is mandatory. I did not understand much from the whole course. I have a TMA that involve reading a pcap file and answering questions. I opened the file with wireshark but i do not know where to look for in order to answer the question. Can someone help me? Email m...'''
date = "2013-12-04T07:32:00Z"
lastmod = "2013-12-04T07:47:00Z"
weight = 27769
keywords = [ "pcap" ]
aliases = [ "/questions/27769" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to read a pcap file](/questions/27769/how-to-read-a-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27769-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27769-score" class="post-score" title="current number of votes">0</div><span id="post-27769-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My major is business and ICT course is mandatory. I did not understand much from the whole course. I have a TMA that involve reading a pcap file and answering questions. I opened the file with wireshark but i do not know where to look for in order to answer the question. Can someone help me? Email me at <span class="__cf_email__" data-cfemail="4b263f3e2f24382e7a0b322a232424652824653e20">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '13, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/3401b2a35b3adea30c0776b865d47e8b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="maximas&#39;s gravatar image" /><p><span>maximas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="maximas has no accepted answers">0%</span></p></div></div><div id="comments-container-27769" class="comments-container"></div><div id="comment-tools-27769" class="comment-tools"></div><div class="clear"></div><div id="comment-27769-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27770"></span>

<div id="answer-container-27770" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27770-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27770-score" class="post-score" title="current number of votes">2</div><span id="post-27770-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Why email when you can ask questions here? It may help if you post the pcap file somewhere (like on <a href="http://www.cloudshark.org"></a><a href="http://www.cloudshark.org">http://www.cloudshark.org</a>) so that it can be examined. If you need help, just ask.</p><p><em>A word of warning though</em>: this is not the <strong>PAHERT</strong> ("Packet Analysis Homework Emergency Response Team"). If you have an assignment you should try to solve it yourself. Take a look at the packet list, and check the most important options in the Statistics Menu (Protocol Hiearchy, Endpoints, Conversations, I/O Graph). If, and only if, there are questions left afterwards you can ask them here.</p><p>Do not expect us to do your job - we love to help when it comes to reading packets, but we have other things to do than homework assignments. BTW, some tutors/professors check these boards, too ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Dec '13, 07:47</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27770" class="comments-container"></div><div id="comment-tools-27770" class="comment-tools"></div><div class="clear"></div><div id="comment-27770-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

