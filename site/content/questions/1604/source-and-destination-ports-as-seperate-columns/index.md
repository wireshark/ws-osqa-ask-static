+++
type = "question"
title = "source and destination ports as seperate columns"
description = '''Hello, I have not been able to find a way to display source and destintation ports as seperately displayed columns such as: source ip, port, dest ip, port. Is there a way to do this?'''
date = "2011-01-03T13:13:00Z"
lastmod = "2011-04-10T08:10:00Z"
weight = 1604
keywords = [ "port", "display-filter" ]
aliases = [ "/questions/1604" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [source and destination ports as seperate columns](/questions/1604/source-and-destination-ports-as-seperate-columns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1604-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1604-score" class="post-score" title="current number of votes">0</div><span id="post-1604-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I have not been able to find a way to display source and destintation ports as seperately displayed columns such as: source ip, port, dest ip, port. Is there a way to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jan '11, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/7df3f9a4b16eae9f77feb6eabe92919e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eelarry&#39;s gravatar image" /><p><span>eelarry</span><br />
<span class="score" title="36 reputation points">36</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eelarry has no accepted answers">0%</span></p></div></div><div id="comments-container-1604" class="comments-container"></div><div id="comment-tools-1604" class="comment-tools"></div><div class="clear"></div><div id="comment-1604-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1605"></span>

<div id="answer-container-1605" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1605-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1605-score" class="post-score" title="current number of votes">1</div><span id="post-1605-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="eelarry has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you can go to "Edit -&gt; Preferences -&gt; Columns" and add the columns you need.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jan '11, 13:27</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-1605" class="comments-container"><span id="1607"></span><div id="comment-1607" class="comment"><div id="post-1607-score" class="comment-score"></div><div class="comment-text"><p>Thanks much. I knew it had to be there.</p></div><div id="comment-1607-info" class="comment-info"><span class="comment-age">(03 Jan '11, 13:44)</span> <span class="comment-user userinfo">eelarry</span></div></div><span id="1608"></span><div id="comment-1608" class="comment"><div id="post-1608-score" class="comment-score"></div><div class="comment-text"><p>(converted your answer to a comment, which is more appropriate on this Q&amp;A site)</p></div><div id="comment-1608-info" class="comment-info"><span class="comment-age">(03 Jan '11, 14:03)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-1605" class="comment-tools"></div><div class="clear"></div><div id="comment-1605-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1610"></span>

<div id="answer-container-1610" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1610-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1610-score" class="post-score" title="current number of votes">1</div><span id="post-1610-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Also, in newer (1.4.x or better) releases, you can right click on a field (source or dest port for example) and choose "apply as column"</p><p>Brilliant feature, I might add. Kudos to the developers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jan '11, 16:54</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span></p></div></div><div id="comments-container-1610" class="comments-container"><span id="3420"></span><div id="comment-3420" class="comment"><div id="post-3420-score" class="comment-score"></div><div class="comment-text"><p>Thank you. Sorry for being so late in noticing your reply.</p></div><div id="comment-3420-info" class="comment-info"><span class="comment-age">(10 Apr '11, 08:10)</span> <span class="comment-user userinfo">eelarry</span></div></div></div><div id="comment-tools-1610" class="comment-tools"></div><div class="clear"></div><div id="comment-1610-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

