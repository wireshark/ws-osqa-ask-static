+++
type = "question"
title = "call flow diagram program"
description = '''Hi. Beside Wireshark, is there any program that can open .pcap or .txt file and is able to draw a call flow diagram? I found SIPWorkbench, but it seems that displays only SIP traffic while I need to see also RTP and DIAMETER (from my .pcap and .txt trace). Thanks for any good advice. :-) Bg, Thomas.'''
date = "2011-04-27T03:41:00Z"
lastmod = "2014-04-12T20:12:00Z"
weight = 3740
keywords = [ "flow", "program", "call", "display", "trace" ]
aliases = [ "/questions/3740" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [call flow diagram program](/questions/3740/call-flow-diagram-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3740-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3740-score" class="post-score" title="current number of votes">0</div><span id="post-3740-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi.</p><p>Beside Wireshark, is there any program that can open .pcap or .txt file and is able to draw a call flow diagram? I found SIPWorkbench, but it seems that displays only SIP traffic while I need to see also RTP and DIAMETER (from my .pcap and .txt trace). Thanks for any good advice. :-)</p><p>Bg,</p><p>Thomas.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-program" rel="tag" title="see questions tagged &#39;program&#39;">program</span> <span class="post-tag tag-link-call" rel="tag" title="see questions tagged &#39;call&#39;">call</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-trace" rel="tag" title="see questions tagged &#39;trace&#39;">trace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '11, 03:41</strong></p><img src="https://secure.gravatar.com/avatar/13231e33ab17a93476f7b98c9d5b272a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wired&#39;s gravatar image" /><p><span>wired</span><br />
<span class="score" title="44 reputation points">44</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wired has one accepted answer">9%</span></p></div></div><div id="comments-container-3740" class="comments-container"></div><div id="comment-tools-3740" class="comment-tools"></div><div class="clear"></div><div id="comment-3740-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="3741"></span>

<div id="answer-container-3741" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3741-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3741-score" class="post-score" title="current number of votes">0</div><span id="post-3741-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not sure if you strictly look for open source tools or if you can afford to buy one, but I'd take a look at <a href="http://www.flukenetworks.com/enterprise-network/network-monitoring/ClearSight-Analyzer">Clearsight</a>. Maybe it is what you're looking for, but I have to admit I haven't played with Clearsight for a few years now, so my knowledge about its current features is quite limited.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Apr '11, 03:53</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-3741" class="comments-container"><span id="3742"></span><div id="comment-3742" class="comment"><div id="post-3742-score" class="comment-score"></div><div class="comment-text"><p>Tried with, but no luck.</p></div><div id="comment-3742-info" class="comment-info"><span class="comment-age">(27 Apr '11, 05:08)</span> <span class="comment-user userinfo">wired</span></div></div></div><div id="comment-tools-3741" class="comment-tools"></div><div class="clear"></div><div id="comment-3741-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="31767"></span>

<div id="answer-container-31767" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31767-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31767-score" class="post-score" title="current number of votes">0</div><span id="post-31767-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can try VisualEther. With VisualEther you can generate call flows/signaling flows from any PCAP file.</p><p><a href="http://www.eventhelix.com/VisualEther/">http://www.eventhelix.com/VisualEther/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Apr '14, 20:12</strong></p><img src="https://secure.gravatar.com/avatar/bfebcf0a91fc0e34658d374c3489dd66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eventhelix&#39;s gravatar image" /><p><span>eventhelix</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eventhelix has no accepted answers">0%</span></p></div></div><div id="comments-container-31767" class="comments-container"></div><div id="comment-tools-31767" class="comment-tools"></div><div class="clear"></div><div id="comment-31767-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

