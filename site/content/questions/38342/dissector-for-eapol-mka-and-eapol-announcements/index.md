+++
type = "question"
title = "Dissector for EAPOL-MKA and EAPOL-Announcements"
description = '''I am trying to find out if anyone has created the dissector for the packet types 888E03xx, where  888E = PAE Ethernet Type  03 = Protocol Version  and xx is any of the following: 05 = EAPOL-MKA 06 = EAPOL-Announcement (Generic) 07 = EAPOL-Announcement (Specific) 08 = EAPOL-Announcement-Req'''
date = "2014-12-04T18:37:00Z"
lastmod = "2014-12-04T18:37:00Z"
weight = 38342
keywords = [ "dissector" ]
aliases = [ "/questions/38342" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Dissector for EAPOL-MKA and EAPOL-Announcements](/questions/38342/dissector-for-eapol-mka-and-eapol-announcements)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38342-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38342-score" class="post-score" title="current number of votes">0</div><span id="post-38342-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to find out if anyone has created the dissector for the packet types 888E03xx, where</p><p>888E = PAE Ethernet Type</p><p>03 = Protocol Version</p><p>and xx is any of the following:</p><p>05 = EAPOL-MKA 06 = EAPOL-Announcement (Generic) 07 = EAPOL-Announcement (Specific) 08 = EAPOL-Announcement-Req</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '14, 18:37</strong></p><img src="https://secure.gravatar.com/avatar/8a0951a5460bd2499eec666477d47222?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mwebb333&#39;s gravatar image" /><p><span>mwebb333</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mwebb333 has no accepted answers">0%</span></p></div></div><div id="comments-container-38342" class="comments-container"></div><div id="comment-tools-38342" class="comment-tools"></div><div class="clear"></div><div id="comment-38342-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

