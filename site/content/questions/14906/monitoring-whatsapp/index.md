+++
type = "question"
title = "Monitoring WhatsApp"
description = '''hi, how can i monitore whatsapp on my network?? thank you '''
date = "2012-10-10T15:30:00Z"
lastmod = "2016-04-01T06:25:00Z"
weight = 14906
keywords = [ "whatsapp" ]
aliases = [ "/questions/14906" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring WhatsApp](/questions/14906/monitoring-whatsapp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14906-score" class="post-score" title="current number of votes">0</div><span id="post-14906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, how can i monitore whatsapp on my network?? thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '12, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/5bb12605fb3798fbb1053c91e3e965f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lebtrack&#39;s gravatar image" /><p><span>lebtrack</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lebtrack has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Mar '16, 23:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-14906" class="comments-container"><span id="14952"></span><div id="comment-14952" class="comment"><div id="post-14952-score" class="comment-score"></div><div class="comment-text"><p>You mean WhatsApp Messenger? The mobile messaging app?</p></div><div id="comment-14952-info" class="comment-info"><span class="comment-age">(12 Oct '12, 01:06)</span> <span class="comment-user userinfo">rakki</span></div></div><span id="50736"></span><div id="comment-50736" class="comment"><div id="post-50736-score" class="comment-score"></div><div class="comment-text"><p>you can capture the whatsapp packets traffic travelling through gateway or modem through wireshark,After that you can dissect and monitor.</p></div><div id="comment-50736-info" class="comment-info"><span class="comment-age">(06 Mar '16, 23:13)</span> <span class="comment-user userinfo">rathnaTech</span></div></div></div><div id="comment-tools-14906" class="comment-tools"></div><div class="clear"></div><div id="comment-14906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51345"></span>

<div id="answer-container-51345" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51345-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51345-score" class="post-score" title="current number of votes">0</div><span id="post-51345-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>you can use a plug in called whatsapp.so or put in the filter field various filter, as ip.addr tcp.port and so on...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '16, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/1f422a72eab029cae4d8742650674201?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cicciovo&#39;s gravatar image" /><p><span>cicciovo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cicciovo has no accepted answers">0%</span></p></div></div><div id="comments-container-51345" class="comments-container"></div><div id="comment-tools-51345" class="comment-tools"></div><div class="clear"></div><div id="comment-51345-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

