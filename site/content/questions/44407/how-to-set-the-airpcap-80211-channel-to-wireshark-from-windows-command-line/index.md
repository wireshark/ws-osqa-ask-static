+++
type = "question"
title = "How to set the AirPcap 802.11 channel to wireshark from windows command line."
description = '''can anybody knows how to set specified 802.11 channel from windows command line. from wireshak gui - Capture-&amp;gt;Options-&amp;gt; AirPcapUSB wireless capture -&amp;gt; Edit Interface Settings-&amp;gt; wirelessSettings-&amp;gt; Advanced Wireless settings -&amp;gt; from Basic Parameters we will select the channel. how to...'''
date = "2015-07-23T00:28:00Z"
lastmod = "2015-07-23T00:28:00Z"
weight = 44407
keywords = [ "airpcap", "802.11", "command-line", "channel" ]
aliases = [ "/questions/44407" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to set the AirPcap 802.11 channel to wireshark from windows command line.](/questions/44407/how-to-set-the-airpcap-80211-channel-to-wireshark-from-windows-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44407-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44407-score" class="post-score" title="current number of votes">0</div><span id="post-44407-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can anybody knows how to set specified 802.11 channel from windows command line. from wireshak gui - Capture-&gt;Options-&gt; AirPcapUSB wireless capture -&gt; Edit Interface Settings-&gt; wirelessSettings-&gt; Advanced Wireless settings -&gt; from Basic Parameters we will select the channel.</p><p>how to do this from command line. pls help to me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span> <span class="post-tag tag-link-channel" rel="tag" title="see questions tagged &#39;channel&#39;">channel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '15, 00:28</strong></p><img src="https://secure.gravatar.com/avatar/53f38baf181e75f44024dc66f6c893f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TCSR&#39;s gravatar image" /><p><span>TCSR</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TCSR has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Jul '15, 15:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-44407" class="comments-container"></div><div id="comment-tools-44407" class="comment-tools"></div><div class="clear"></div><div id="comment-44407-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

