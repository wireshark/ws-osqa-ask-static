+++
type = "question"
title = "NACK-Oriented Reliable Multicast NORM"
description = '''This is the new approved multicast form put forward by Quest Software (formerly Dell Software). I have been able to find an old RFC, but don&#x27;t know who is using it. Supposedly it requires no modification of switches. Does your experience differ? It is supposed to use TCP/IP packets. Does anyo'''
date = "2017-10-27T14:01:00Z"
lastmod = "2017-10-27T14:01:00Z"
weight = 64308
keywords = [ "multicast", "norm" ]
aliases = [ "/questions/64308" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [NACK-Oriented Reliable Multicast NORM](/questions/64308/nack-oriented-reliable-multicast-norm)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64308-score" class="post-score" title="current number of votes">0</div><span id="post-64308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This is the new approved multicast form put forward by Quest Software (formerly Dell Software). I have been able to find an old RFC, but don't know who is using it. Supposedly it requires no modification of switches. Does your experience differ? It is supposed to use TCP/IP packets. Does anyo</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multicast" rel="tag" title="see questions tagged &#39;multicast&#39;">multicast</span> <span class="post-tag tag-link-norm" rel="tag" title="see questions tagged &#39;norm&#39;">norm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '17, 14:01</strong></p><img src="https://secure.gravatar.com/avatar/0d88d217c68f5a9529fe59df6682b617?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pbrawner&#39;s gravatar image" /><p><span>pbrawner</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pbrawner has no accepted answers">0%</span></p></div></div><div id="comments-container-64308" class="comments-container"></div><div id="comment-tools-64308" class="comment-tools"></div><div class="clear"></div><div id="comment-64308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

