+++
type = "question"
title = "Installed Wireshark but CAN&#x27;T FIND IT (total rookie question)"
description = '''Hello! Quick question: I use Ubuntu/Linux and I recently installed Wireshark using Ubuntu Software Center. It says that I successfully installed it, but now I don&#x27;t know where to find the Wireshark folder or program! Which folder is it in and how do I access it? Thanks to anyone who helps me! And th...'''
date = "2012-06-02T18:51:00Z"
lastmod = "2012-06-08T16:20:00Z"
weight = 11576
keywords = [ "location" ]
aliases = [ "/questions/11576" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Installed Wireshark but CAN'T FIND IT (total rookie question)](/questions/11576/installed-wireshark-but-cant-find-it-total-rookie-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11576-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11576-score" class="post-score" title="current number of votes">0</div><span id="post-11576-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! Quick question: I use Ubuntu/Linux and I recently installed Wireshark using Ubuntu Software Center. It says that I successfully installed it, but now I don't know where to find the Wireshark folder or program! Which folder is it in and how do I access it?</p><p>Thanks to anyone who helps me! And thank you for not judging my embarrassingly low level of computer literacy. :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-location" rel="tag" title="see questions tagged &#39;location&#39;">location</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '12, 18:51</strong></p><img src="https://secure.gravatar.com/avatar/6011bcba466b04babd8caaae97b9efff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ally&#39;s gravatar image" /><p><span>Ally</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ally has no accepted answers">0%</span></p></div></div><div id="comments-container-11576" class="comments-container"><span id="11779"></span><div id="comment-11779" class="comment"><div id="post-11779-score" class="comment-score"></div><div class="comment-text"><p>Thanks everyone! I was able to access it. I really appreciate the help. :) Isn't it nice that on the internet people help each other? (and it's not just trolls on YouTube) :)</p></div><div id="comment-11779-info" class="comment-info"><span class="comment-age">(08 Jun '12, 16:20)</span> <span class="comment-user userinfo">Ally</span></div></div></div><div id="comment-tools-11576" class="comment-tools"></div><div class="clear"></div><div id="comment-11576-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="11578"></span>

<div id="answer-container-11578" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11578-score" class="post-score" title="current number of votes">0</div><span id="post-11578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><strong>Search applications in Unity</strong>:</p><p>Please use Unity Dash to search for applications:</p><blockquote><p><code>https://help.ubuntu.com/11.04/ubuntu-help/unity-dash-intro.html</code><br />
</p></blockquote><p><strong>Search applications at the CLI</strong>:</p><p>Start a Terminal (Ctrl-Alt-T) or again Dash (then type 'Terminal') and run this commands:</p><blockquote><p><code>which wireshark</code><br />
<code>which tshark</code><br />
</p></blockquote><p>At the CLI there is no need to know the application path, just type <code>wireshark</code> or <code>tshark</code> in the terminal window and the program will be started.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '12, 02:58</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-11578" class="comments-container"></div><div id="comment-tools-11578" class="comment-tools"></div><div class="clear"></div><div id="comment-11578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="11587"></span>

<div id="answer-container-11587" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11587-score" class="post-score" title="current number of votes">0</div><span id="post-11587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try sudo wireshark from terminal, that way you can find the interfaces with root privs.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '12, 13:09</strong></p><img src="https://secure.gravatar.com/avatar/2b1d4ebc68ff2908f81bbfd9a43aaf78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pluribus&#39;s gravatar image" /><p><span>pluribus</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pluribus has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-11587" class="comments-container"><span id="11591"></span><div id="comment-11591" class="comment"><div id="post-11591-score" class="comment-score"></div><div class="comment-text"><p>Wireshark should <strong>not</strong> be run as root. There are many lines of code in Wireshark dissectors that could be security vulnerabilities.</p></div><div id="comment-11591-info" class="comment-info"><span class="comment-age">(03 Jun '12, 13:48)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="11603"></span><div id="comment-11603" class="comment"><div id="post-11603-score" class="comment-score"></div><div class="comment-text"><p>I had no idea wireshark can be remotely exploited when running as root. Thanks for telling me.</p><p>How can I get wireshark to find my interfaces without using "sudo?" I just found this is the only way it would actually get into promiscuous mode. For me, just running wireshark fails to find my wlan0 interface.</p></div><div id="comment-11603-info" class="comment-info"><span class="comment-age">(03 Jun '12, 14:43)</span> <span class="comment-user userinfo">pluribus</span></div></div><span id="11604"></span><div id="comment-11604" class="comment"><div id="post-11604-score" class="comment-score"></div><div class="comment-text"><p>See the <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">CapturePrivileges</a> wiki page.</p></div><div id="comment-11604-info" class="comment-info"><span class="comment-age">(03 Jun '12, 23:27)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="11607"></span><div id="comment-11607" class="comment"><div id="post-11607-score" class="comment-score">1</div><div class="comment-text"><blockquote><p>I had no idea wireshark can be remotely exploited when running as root.</p></blockquote><p>saying that wireshark can be remotely exploited (generally), overshoots the mark. It's the myriad of lines of code in the dissectors, that <strong>pose a risk</strong> if you run wireshark <strong>with root privileges</strong>. Nobody knows if that code possibly contains security bugs. So, it's better not to run wireshark with root privileges. If you hit such a bug, while dissecting traffic, the impact will be of much less significance if wireshark runs within the context of an unprivileged user.</p></div><div id="comment-11607-info" class="comment-info"><span class="comment-age">(04 Jun '12, 02:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-11587" class="comment-tools"></div><div class="clear"></div><div id="comment-11587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

