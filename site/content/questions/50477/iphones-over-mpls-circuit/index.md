+++
type = "question"
title = "IPhones over mpls circuit"
description = '''can i capture and end to end iphone call, when the is a mpls circuit in between'''
date = "2016-02-24T10:22:00Z"
lastmod = "2016-02-24T14:05:00Z"
weight = 50477
keywords = [ "capture", "call", "iphone" ]
aliases = [ "/questions/50477" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IPhones over mpls circuit](/questions/50477/iphones-over-mpls-circuit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50477-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50477-score" class="post-score" title="current number of votes">0</div><span id="post-50477-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can i capture and end to end iphone call, when the is a mpls circuit in between</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-call" rel="tag" title="see questions tagged &#39;call&#39;">call</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '16, 10:22</strong></p><img src="https://secure.gravatar.com/avatar/2dbdfac72a5c2cbcf4686cc565c99624?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kyoung3633&#39;s gravatar image" /><p><span>kyoung3633</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kyoung3633 has no accepted answers">0%</span></p></div></div><div id="comments-container-50477" class="comments-container"><span id="50488"></span><div id="comment-50488" class="comment"><div id="post-50488-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "end to end" for this question? It's definitely possible to capture traffic from iPhones regardless of whether there's MPLS encapsulation where you are capturing, if that alone is the question.</p><p>What specifically are you trying to accomplish and "see" in the packet capture?</p></div><div id="comment-50488-info" class="comment-info"><span class="comment-age">(24 Feb '16, 14:05)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-50477" class="comment-tools"></div><div class="clear"></div><div id="comment-50477-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50479"></span>

<div id="answer-container-50479" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50479-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50479-score" class="post-score" title="current number of votes">0</div><span id="post-50479-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you mean "IP phone" or "Apple iPhone"?</p><p>If you really have in mind a VoIP call (which seems to be the case as you!ve mentioned MPLS), you can <em>capture</em> it, but that does not necessarily mean you can <em>extract the audio</em>. Whether you can depends on whether the media stream of the call itself and/or the network to network connection over MPLS is encrypted and if it is, whether you have access to the decryption keys. Also some voice codecs are easier to decode than others.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '16, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-50479" class="comments-container"></div><div id="comment-tools-50479" class="comment-tools"></div><div class="clear"></div><div id="comment-50479-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

