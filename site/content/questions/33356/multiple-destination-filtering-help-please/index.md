+++
type = "question"
title = "Multiple destination filtering help please"
description = '''I capture packets fine, but is there an easy way to filter the destination ips to only one entry per ip?'''
date = "2014-06-03T13:15:00Z"
lastmod = "2014-06-03T14:29:00Z"
weight = 33356
keywords = [ "filter", "ip", "destination" ]
aliases = [ "/questions/33356" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Multiple destination filtering help please](/questions/33356/multiple-destination-filtering-help-please)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33356-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33356-score" class="post-score" title="current number of votes">0</div><span id="post-33356-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I capture packets fine, but is there an easy way to filter the destination ips to only one entry per ip?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-destination" rel="tag" title="see questions tagged &#39;destination&#39;">destination</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '14, 13:15</strong></p><img src="https://secure.gravatar.com/avatar/96205dbfedc129b5f45f1774a7dcdd26?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TheLMGSlayer&#39;s gravatar image" /><p><span>TheLMGSlayer</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TheLMGSlayer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jun '14, 13:45</strong> </span></p></div></div><div id="comments-container-33356" class="comments-container"><span id="33358"></span><div id="comment-33358" class="comment"><div id="post-33358-score" class="comment-score"></div><div class="comment-text"><p>It's a bit hard to understand what you want to achieve.</p><p>So, can you please either rephrase your question or add a sample of how the desired result should look like?</p></div><div id="comment-33358-info" class="comment-info"><span class="comment-age">(03 Jun '14, 14:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33356" class="comment-tools"></div><div class="clear"></div><div id="comment-33356-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

