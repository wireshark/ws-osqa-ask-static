+++
type = "question"
title = "how to combine multiple pcap into a single file"
description = '''i have pcap files aodv-node-1-0.pcap upto aodv-node-30-0.pcap.How do i combine them to a single pcap file? kindly guide thanking in advance'''
date = "2016-01-18T17:30:00Z"
lastmod = "2016-01-18T19:15:00Z"
weight = 49341
keywords = [ "pcap" ]
aliases = [ "/questions/49341" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to combine multiple pcap into a single file](/questions/49341/how-to-combine-multiple-pcap-into-a-single-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49341-score" class="post-score" title="current number of votes">0</div><span id="post-49341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have pcap files aodv-node-1-0.pcap upto aodv-node-30-0.pcap.How do i combine them to a single pcap file?</p><p>kindly guide</p><p>thanking in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '16, 17:30</strong></p><img src="https://secure.gravatar.com/avatar/a05b64949a99eaf287de249727eee9e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wsuser16&#39;s gravatar image" /><p><span>wsuser16</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wsuser16 has no accepted answers">0%</span></p></div></div><div id="comments-container-49341" class="comments-container"></div><div id="comment-tools-49341" class="comment-tools"></div><div class="clear"></div><div id="comment-49341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49346"></span>

<div id="answer-container-49346" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49346-score" class="post-score" title="current number of votes">1</div><span id="post-49346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Use mergecap. See link:</p><p><a href="https://www.wireshark.org/docs/man-pages/mergecap.html">https://www.wireshark.org/docs/man-pages/mergecap.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '16, 19:15</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-49346" class="comments-container"></div><div id="comment-tools-49346" class="comment-tools"></div><div class="clear"></div><div id="comment-49346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

