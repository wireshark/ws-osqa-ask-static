+++
type = "question"
title = "Email communication"
description = '''New to Wireshark but we have an issue with a domain that they can send email to us but when we respond we get a generic failed message (Timed Out). I want to know how I can use this software to find out where it is failing but I am not sure what filter I would need or how to just do it for this spec...'''
date = "2017-07-31T11:53:00Z"
lastmod = "2017-07-31T12:03:00Z"
weight = 63263
keywords = [ "email" ]
aliases = [ "/questions/63263" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Email communication](/questions/63263/email-communication)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63263-score" class="post-score" title="current number of votes">0</div><span id="post-63263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>New to Wireshark but we have an issue with a domain that they can send email to us but when we respond we get a generic failed message (Timed Out). I want to know how I can use this software to find out where it is failing but I am not sure what filter I would need or how to just do it for this specific domain. Any help would be great! Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '17, 11:53</strong></p><img src="https://secure.gravatar.com/avatar/828492b0ecfaacc94f701caf36443e49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hughe01205&#39;s gravatar image" /><p><span>hughe01205</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hughe01205 has no accepted answers">0%</span></p></div></div><div id="comments-container-63263" class="comments-container"><span id="63264"></span><div id="comment-63264" class="comment"><div id="post-63264-score" class="comment-score"></div><div class="comment-text"><p>Wireshark can capture the traffic between email clients and servers, but it's likely to be encrypted making analysis difficult.</p><p>Your question also needs some more explanation about your email system such as do you use a local email server or a "cloud" service such as GMail?</p></div><div id="comment-63264-info" class="comment-info"><span class="comment-age">(31 Jul '17, 12:03)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-63263" class="comment-tools"></div><div class="clear"></div><div id="comment-63263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

