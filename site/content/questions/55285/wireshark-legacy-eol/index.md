+++
type = "question"
title = "Wireshark Legacy EOL"
description = '''Hello: I&#x27;m just wondering if WireShark Legacy will ever have an End Of Life? Thank you paul'''
date = "2016-09-02T05:23:00Z"
lastmod = "2016-09-02T09:10:00Z"
weight = 55285
keywords = [ "legacy" ]
aliases = [ "/questions/55285" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Legacy EOL](/questions/55285/wireshark-legacy-eol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55285-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55285-score" class="post-score" title="current number of votes">0</div><span id="post-55285-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello:</p><p>I'm just wondering if WireShark Legacy will ever have an End Of Life?</p><p>Thank you</p><p>paul</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-legacy" rel="tag" title="see questions tagged &#39;legacy&#39;">legacy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Sep '16, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/fbe6825b890bc4c637a5160e6fb707a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pauli&#39;s gravatar image" /><p><span>Pauli</span><br />
<span class="score" title="0 reputation points">0</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pauli has no accepted answers">0%</span></p></div></div><div id="comments-container-55285" class="comments-container"></div><div id="comment-tools-55285" class="comment-tools"></div><div class="clear"></div><div id="comment-55285-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55295"></span>

<div id="answer-container-55295" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55295-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55295-score" class="post-score" title="current number of votes">0</div><span id="post-55295-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It <a href="https://wiki.wireshark.org/Development/LifeCycle">already happened</a>, if you're referring to Wireshark 1.x. If you are referring to the GTK User Interface, that's another matter. So far it's still there, but may fall behind the Qt interface more and more. Eventually it will be dropped, maybe after Wireshark 2.2.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Sep '16, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-55295" class="comments-container"><span id="55296"></span><div id="comment-55296" class="comment"><div id="post-55296-score" class="comment-score"></div><div class="comment-text"><p>Yeah, I saw that.</p><p>I'm just wondering if it will ever be removed from being included in the download of WireShark 2.x?</p><p>Thank you</p></div><div id="comment-55296-info" class="comment-info"><span class="comment-age">(02 Sep '16, 07:32)</span> <span class="comment-user userinfo">Pauli</span></div></div><span id="55299"></span><div id="comment-55299" class="comment"><div id="post-55299-score" class="comment-score"></div><div class="comment-text"><p>Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p></div><div id="comment-55299-info" class="comment-info"><span class="comment-age">(02 Sep '16, 09:08)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="55300"></span><div id="comment-55300" class="comment"><div id="post-55300-score" class="comment-score"></div><div class="comment-text"><p><code>if it will ever be removed from being included in the download of WireShark 2.x?</code></p><p>Eventually yes, it will be dropped. The issue is with maintenance of the features and packaging of related libraries, which is out of our control.</p></div><div id="comment-55300-info" class="comment-info"><span class="comment-age">(02 Sep '16, 09:10)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-55295" class="comment-tools"></div><div class="clear"></div><div id="comment-55295-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

