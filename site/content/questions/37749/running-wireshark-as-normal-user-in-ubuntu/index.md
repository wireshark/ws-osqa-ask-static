+++
type = "question"
title = "Running WireShark as normal user in Ubuntu"
description = '''Ive just installed WireShark and the only one visible interface is bluetooth. When run as root - all physical interfaces are visible. Why is that? And how I make Wireshark to see all interfaces when running as normal user?'''
date = "2014-11-11T02:56:00Z"
lastmod = "2014-11-11T03:40:00Z"
weight = 37749
keywords = [ "root", "ubuntu" ]
aliases = [ "/questions/37749" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Running WireShark as normal user in Ubuntu](/questions/37749/running-wireshark-as-normal-user-in-ubuntu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37749-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37749-score" class="post-score" title="current number of votes">0</div><span id="post-37749-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Ive just installed WireShark and the only one visible interface is bluetooth. When run as root - all physical interfaces are visible. Why is that? And how I make Wireshark to see all interfaces when running as normal user?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-root" rel="tag" title="see questions tagged &#39;root&#39;">root</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '14, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/d8834afe823be77e70cac638b6cc9e19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="developer11&#39;s gravatar image" /><p><span>developer11</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="developer11 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Nov '14, 02:57</strong> </span></p></div></div><div id="comments-container-37749" class="comments-container"></div><div id="comment-tools-37749" class="comment-tools"></div><div class="clear"></div><div id="comment-37749-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37752"></span>

<div id="answer-container-37752" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37752-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37752-score" class="post-score" title="current number of votes">1</div><span id="post-37752-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Look at the <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">Capture Privileges</a> page on the Wiki, especially the section "Debian, Ubuntu and other Debian derivatives". Also there are a number of similar questions on this site with the answer, e.g. <a href="https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed">https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '14, 03:40</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-37752" class="comments-container"></div><div id="comment-tools-37752" class="comment-tools"></div><div class="clear"></div><div id="comment-37752-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

