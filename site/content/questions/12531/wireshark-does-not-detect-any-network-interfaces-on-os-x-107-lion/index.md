+++
type = "question"
title = "Wireshark does not detect any network interfaces on OS X 10.7 Lion?"
description = '''How do I make Wireshark to work on OS X 10.7 running on a 2011 MBP? The application started but it complains that it cannot detect any network interfaces and I do want to monitor WiFi in promiscuous mode.'''
date = "2012-07-09T09:26:00Z"
lastmod = "2012-07-12T13:53:00Z"
weight = 12531
keywords = [ "osx", "lion", "mac" ]
aliases = [ "/questions/12531" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark does not detect any network interfaces on OS X 10.7 Lion?](/questions/12531/wireshark-does-not-detect-any-network-interfaces-on-os-x-107-lion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12531-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12531-score" class="post-score" title="current number of votes">0</div><span id="post-12531-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I make Wireshark to work on OS X 10.7 running on a 2011 MBP?</p><p>The application started but it complains that it cannot detect any network interfaces and I do want to monitor WiFi in promiscuous mode.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-lion" rel="tag" title="see questions tagged &#39;lion&#39;">lion</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '12, 09:26</strong></p><img src="https://secure.gravatar.com/avatar/63c50004c4f6eaf3235b9ea836f4b6cf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sorin&#39;s gravatar image" /><p><span>sorin</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sorin has no accepted answers">0%</span></p></div></div><div id="comments-container-12531" class="comments-container"><span id="12665"></span><div id="comment-12665" class="comment"><div id="post-12665-score" class="comment-score"></div><div class="comment-text"><p>Recommend you try the "lion-native wireshark" with installer from <a href="http://www.Christian-Hornung.de">www.Christian-Hornung.de</a></p><p>I've been using this with Lion since the OS was released and it runs like a champ.</p></div><div id="comment-12665-info" class="comment-info"><span class="comment-age">(12 Jul '12, 12:45)</span> <span class="comment-user userinfo">viapple</span></div></div></div><div id="comment-tools-12531" class="comment-tools"></div><div class="clear"></div><div id="comment-12531-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="12533"></span>

<div id="answer-container-12533" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12533-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12533-score" class="post-score" title="current number of votes">1</div><span id="post-12533-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>did you check: <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges/">This</a>, <a href="http://ask.wireshark.org/questions/578/mac-os-cant-detect-any-interface">this</a> and <a href="http://ask.wireshark.org/questions/6420/no-interfaces-found-on-mac-os-x">this</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jul '12, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-12533" class="comments-container"></div><div id="comment-tools-12533" class="comment-tools"></div><div class="clear"></div><div id="comment-12533-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="12666"></span>

<div id="answer-container-12666" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12666-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12666-score" class="post-score" title="current number of votes">0</div><span id="post-12666-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark (Lion-native) by <a href="http://www.Christian-Hornung.de">www.Christian-Hornung.de</a></p><p>It just works. Lemme know how you get on.</p><p>Regards,</p><p>viapple</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jul '12, 12:46</strong></p><img src="https://secure.gravatar.com/avatar/2ab6f79247b1fa2599b14dc999260113?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="viapple&#39;s gravatar image" /><p><span>viapple</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="viapple has no accepted answers">0%</span></p></div></div><div id="comments-container-12666" class="comments-container"><span id="12670"></span><div id="comment-12670" class="comment"><div id="post-12670-score" class="comment-score"></div><div class="comment-text"><p>Seems to be an older version (1.6.1) though.</p></div><div id="comment-12670-info" class="comment-info"><span class="comment-age">(12 Jul '12, 13:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-12666" class="comment-tools"></div><div class="clear"></div><div id="comment-12666-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

