+++
type = "question"
title = "headless automate export object when capturing packeting"
description = '''Hello, Is there possible to run wireshark and exporting http objects (file -&amp;gt; export -&amp;gt; objects -&amp;gt; http feature), and automate saving these objects to files under the structed path in GET header while wireshark is caputing packet and running in headless mode. How? Thanks,'''
date = "2012-11-06T00:09:00Z"
lastmod = "2016-12-13T15:31:00Z"
weight = 15560
keywords = [ "headless", "objects", "export", "http", "automation" ]
aliases = [ "/questions/15560" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [headless automate export object when capturing packeting](/questions/15560/headless-automate-export-object-when-capturing-packeting)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15560-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15560-score" class="post-score" title="current number of votes">0</div><span id="post-15560-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Is there possible to run wireshark and exporting http objects (file -&gt; export -&gt; objects -&gt; http feature), and automate saving these objects to files under the structed path in GET header while wireshark is caputing packet and running in headless mode.</p><p>How?</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-headless" rel="tag" title="see questions tagged &#39;headless&#39;">headless</span> <span class="post-tag tag-link-objects" rel="tag" title="see questions tagged &#39;objects&#39;">objects</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-automation" rel="tag" title="see questions tagged &#39;automation&#39;">automation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '12, 00:09</strong></p><img src="https://secure.gravatar.com/avatar/0bc79452dd4cd8d360c95b9b2e44af3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiresharknewbite_1&#39;s gravatar image" /><p><span>wiresharknew...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiresharknewbite_1 has no accepted answers">0%</span></p></div></div><div id="comments-container-15560" class="comments-container"></div><div id="comment-tools-15560" class="comment-tools"></div><div class="clear"></div><div id="comment-15560-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="15562"></span>

<div id="answer-container-15562" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15562-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15562-score" class="post-score" title="current number of votes">0</div><span id="post-15562-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think this is possible at the moment; exporting objects is "manual mode only" as far as I can tell.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Nov '12, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-15562" class="comments-container"></div><div id="comment-tools-15562" class="comment-tools"></div><div class="clear"></div><div id="comment-15562-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15564"></span>

<div id="answer-container-15564" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15564-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15564-score" class="post-score" title="current number of votes">0</div><span id="post-15564-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately this is not possible in GUI mode, at least not in an automated ("headless") way. You could try to use an external GUI automation tool like <a href="http://www.autoitscript.com/site/autoit/">AutoIt</a> (or similar), but I'm not sure it that will work with GTK.</p><p>Can you please tell us more about the goal you want to achieve?</p><p>Maybe there is another way, like one of these tools</p><ul><li><a href="http://pypi.python.org/pypi/tcpextract/1.1">tcpextract</a></li><li><a href="http://www.cockos.com/assniffer/">assniffer</a></li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Nov '12, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Nov '12, 01:10</strong> </span></p></div></div><div id="comments-container-15564" class="comments-container"><span id="15568"></span><div id="comment-15568" class="comment"><div id="post-15568-score" class="comment-score"></div><div class="comment-text"><p>actually I need to monitor live network traffic and give live report on the keyworks in html transmated file. and the url of that file.</p></div><div id="comment-15568-info" class="comment-info"><span class="comment-age">(06 Nov '12, 01:26)</span> <span class="comment-user userinfo">wiresharknew...</span></div></div><span id="15572"></span><div id="comment-15572" class="comment"><div id="post-15572-score" class="comment-score"></div><div class="comment-text"><p>well, I don't think that wireshark is the right tool for your job.</p><p>Wireshark is a network analyzer and people use it to troubleshoot network problems. I suggest you take a look at these tools.</p><ul><li>Wireshark Wiki - Tools: <a href="http://wiki.wireshark.org/Tools">http://wiki.wireshark.org/Tools</a></li><li>WinPcap - Tools: <a href="http://www.winpcap.org/misc/links.htm">http://www.winpcap.org/misc/links.htm</a></li></ul><p>BTW: <a href="http://www.cockos.com/assniffer/">assniffer</a>, together with a script could possibly do the job.</p><p>Maybe it's even easier to use a HTTP proxy and extract the required information there.</p><ul><li>squid: <a href="http://www.squid-cache.org/">http://www.squid-cache.org/</a></li><li>varnish: <a href="https://www.varnish-cache.org/">https://www.varnish-cache.org/</a></li></ul><p>Regards<br />
Kurt</p></div><div id="comment-15572-info" class="comment-info"><span class="comment-age">(06 Nov '12, 01:49)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15564" class="comment-tools"></div><div class="clear"></div><div id="comment-15564-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="58057"></span>

<div id="answer-container-58057" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58057-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58057-score" class="post-score" title="current number of votes">0</div><span id="post-58057-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't save the full GET path, but as of Wireshark 2.3.0, you can export HTTP objects with tshark.</p><p>Wireshark 2.3.0 hasn't been released yet, so you can grab a daily build from <a href="https://www.wireshark.org/download/automated/win32/">here</a>. To extract HTTP objects from the command-line, run the following command:</p><pre><code>tshark -r mypcap.pcap --export-objects &quot;http,destdir&quot;</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Dec '16, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/8df259c952186aa93179732461b8d1e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moshe&#39;s gravatar image" /><p><span>moshe</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moshe has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Dec '16, 15:32</strong> </span></p></div></div><div id="comments-container-58057" class="comments-container"></div><div id="comment-tools-58057" class="comment-tools"></div><div class="clear"></div><div id="comment-58057-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

