+++
type = "question"
title = "vlan tag can&#x27;t display in the detail panel"
description = '''vlan tag can&#x27;t display in the detail panel in this path,there is not have vlan :edit---preference---protocol so the vlan tag is contained in ethernet II i want the vlan tag can dispaly singlely in the detail panel like the old version. thanks!!'''
date = "2012-07-26T20:38:00Z"
lastmod = "2012-07-27T06:01:00Z"
weight = 13046
keywords = [ "vlan", "tag" ]
aliases = [ "/questions/13046" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [vlan tag can't display in the detail panel](/questions/13046/vlan-tag-cant-display-in-the-detail-panel)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13046-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13046-score" class="post-score" title="current number of votes">0</div><span id="post-13046-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>vlan tag can't display in the detail panel in this path,there is not have vlan :edit---preference---protocol so the vlan tag is contained in ethernet II i want the vlan tag can dispaly singlely in the detail panel like the old version. thanks!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '12, 20:38</strong></p><img src="https://secure.gravatar.com/avatar/66c65493d8391204fb5433d8d14476d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tufei&#39;s gravatar image" /><p><span>tufei</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tufei has no accepted answers">0%</span></p></div></div><div id="comments-container-13046" class="comments-container"></div><div id="comment-tools-13046" class="comment-tools"></div><div class="clear"></div><div id="comment-13046-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13067"></span>

<div id="answer-container-13067" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13067-score" class="post-score" title="current number of votes">1</div><span id="post-13067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This was a 'feature' introduced in Wireshark 1.6.0 and removed in Wireshark 1.6.3. Up/downgrade to an applicable version.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jul '12, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13067" class="comments-container"><span id="13079"></span><div id="comment-13079" class="comment"><div id="post-13079-score" class="comment-score"></div><div class="comment-text"><p>See <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=2254">bug 2254</a> for more details.</p></div><div id="comment-13079-info" class="comment-info"><span class="comment-age">(27 Jul '12, 06:01)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-13067" class="comment-tools"></div><div class="clear"></div><div id="comment-13067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

