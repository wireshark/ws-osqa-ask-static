+++
type = "question"
title = "tshark export statistics in csv format"
description = '''Hello. Is there a way to get a CSV file with the output of tshark&#x27;s -z conv,ip similar to the one I get in Wireshark-&amp;gt;Statistics-&amp;gt;Conversations-&amp;gt;IP-&amp;gt;Copy? I was going through the help, but I could only find how to do it for exporting certain fields of pcap files, and not for statistics. ...'''
date = "2013-03-03T11:06:00Z"
lastmod = "2013-03-05T10:58:00Z"
weight = 19103
keywords = [ "statistics", "tshark", "csv" ]
aliases = [ "/questions/19103" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tshark export statistics in csv format](/questions/19103/tshark-export-statistics-in-csv-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19103-score" class="post-score" title="current number of votes">0</div><span id="post-19103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. Is there a way to get a CSV file with the output of tshark's -z conv,ip similar to the one I get in Wireshark-&gt;Statistics-&gt;Conversations-&gt;IP-&gt;Copy? I was going through the help, but I could only find how to do it for exporting certain fields of pcap files, and not for statistics. Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-csv" rel="tag" title="see questions tagged &#39;csv&#39;">csv</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '13, 11:06</strong></p><img src="https://secure.gravatar.com/avatar/93c3b1d3f74af64ff716efa87a04e5cc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hugosp&#39;s gravatar image" /><p><span>hugosp</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hugosp has no accepted answers">0%</span></p></div></div><div id="comments-container-19103" class="comments-container"></div><div id="comment-tools-19103" class="comment-tools"></div><div class="clear"></div><div id="comment-19103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19163"></span>

<div id="answer-container-19163" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19163-score" class="post-score" title="current number of votes">1</div><span id="post-19163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately the stats of tshark (-z conv,ip) are different from the stats in Wireshark. To get the same output you would have to extend the code of tshark.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '13, 10:58</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-19163" class="comments-container"></div><div id="comment-tools-19163" class="comment-tools"></div><div class="clear"></div><div id="comment-19163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

