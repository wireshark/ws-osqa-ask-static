+++
type = "question"
title = "Forensic book on WireShark"
description = '''Are there any good books out there on Wireshark that deal with Network Forensics?'''
date = "2017-05-01T09:46:00Z"
lastmod = "2017-05-01T09:46:00Z"
weight = 61138
keywords = [ "forensics" ]
aliases = [ "/questions/61138" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Forensic book on WireShark](/questions/61138/forensic-book-on-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61138-score" class="post-score" title="current number of votes">0</div><span id="post-61138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are there any good books out there on Wireshark that deal with Network Forensics?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-forensics" rel="tag" title="see questions tagged &#39;forensics&#39;">forensics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '17, 09:46</strong></p><img src="https://secure.gravatar.com/avatar/6528b7a1b93429c225c495afe7659433?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="elliep&#39;s gravatar image" /><p><span>elliep</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="elliep has no accepted answers">0%</span></p></div></div><div id="comments-container-61138" class="comments-container"></div><div id="comment-tools-61138" class="comment-tools"></div><div class="clear"></div><div id="comment-61138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

