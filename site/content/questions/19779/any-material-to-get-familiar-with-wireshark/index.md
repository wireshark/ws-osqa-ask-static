+++
type = "question"
title = "any material to get familiar with wireshark?"
description = '''if anyone knew how to get familiar with wireshark expressions (material or tutorial videos) pls let me knew'''
date = "2013-03-23T15:25:00Z"
lastmod = "2013-03-24T05:37:00Z"
weight = 19779
keywords = [ "startup" ]
aliases = [ "/questions/19779" ]
osqa_answers = 4
osqa_accepted = false
+++

<div class="headNormal">

# [any material to get familiar with wireshark?](/questions/19779/any-material-to-get-familiar-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19779-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19779-score" class="post-score" title="current number of votes">1</div><span id="post-19779-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>if anyone knew how to get familiar with wireshark expressions (material or tutorial videos) pls let me knew</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '13, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/0f19aa9efeeb7a1409b85d75ad0ca07c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ark&#39;s gravatar image" /><p><span>ark</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ark has no accepted answers">0%</span></p></div></div><div id="comments-container-19779" class="comments-container"></div><div id="comment-tools-19779" class="comment-tools"></div><div class="clear"></div><div id="comment-19779-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

4 Answers:

</div>

</div>

<span id="19784"></span>

<div id="answer-container-19784" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19784-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19784-score" class="post-score" title="current number of votes">4</div><span id="post-19784-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><strong>Wireshark Resources</strong></p><p><a href="http://sharkfest.wireshark.org/">http://sharkfest.wireshark.org/</a> Wireshark developers and users conference... This year @ UC-Bekely</p><p><a href="http://www.wireshark.org/">http://www.wireshark.org/</a> Regarding latest developments in wireshark</p><p><a href="http://ask.wireshark.org/">http://ask.wireshark.org/</a> Wireshark Q&amp;A community</p><p><a href="https://blog.wireshark.org/">https://blog.wireshark.org/</a> wireshark blog</p><p><strong>wireshark reference books:</strong></p><p>Practical Packet analysis by Chris Sanders</p><p>Computer Networking:Internet protocols in action by Jeanna Matthews</p><p>Wireshark Network Analysis:The official Guide for Wireshark certified network Analyst by Laura Chappell(2nd edition)</p><p>Wireshark 101:Essential skills for Network Analysis</p><p>Wireshark certified network analyst prep guide.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '13, 17:35</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-19784" class="comments-container"></div><div id="comment-tools-19784" class="comment-tools"></div><div class="clear"></div><div id="comment-19784-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19781"></span>

<div id="answer-container-19781" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19781-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19781-score" class="post-score" title="current number of votes">2</div><span id="post-19781-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try Wireshark University, or look for Wireshark books at Amazon. Also, you could take a look at the presentations held at <a href="http://sharkfest.wireshark.org">Sharkfest</a> to see if anything helps you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '13, 16:01</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-19781" class="comments-container"></div><div id="comment-tools-19781" class="comment-tools"></div><div class="clear"></div><div id="comment-19781-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19782"></span>

<div id="answer-container-19782" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19782-score" class="post-score" title="current number of votes">2</div><span id="post-19782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's a boatload of info on <a href="http://www.wireshark.org/docs/">http://www.wireshark.org/docs/</a>, and on the <a href="http://wiki.wireshark.org">Wiki</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '13, 16:07</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-19782" class="comments-container"></div><div id="comment-tools-19782" class="comment-tools"></div><div class="clear"></div><div id="comment-19782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19786"></span>

<div id="answer-container-19786" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19786-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19786-score" class="post-score" title="current number of votes">2</div><span id="post-19786-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can also take a look at YouTube:<br />
- <a href="http://www.youtube.com/results?search_query=wireshark+&amp;oq=wireshark+&amp;gs_l=youtube.3..0l10.160334.160334.0.160706.1.1.0.0.0.0.129.129.0j1.1.0...0.0...1ac.1.XDU8wOfgga8">Wireshark</a><br />
- <a href="http://www.youtube.com/user/packetanalyzer?feature=watch">Mike Pennacchi</a>.<br />
Or take a look at <a href="http://www.slideshare.net/LoveMyTool/tag/wireshark">Slideshare</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Mar '13, 05:37</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-19786" class="comments-container"></div><div id="comment-tools-19786" class="comment-tools"></div><div class="clear"></div><div id="comment-19786-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

