+++
type = "question"
title = "Uninstall Wireshark - Stop and get rid of DumpCap to reinstall everything"
description = '''I do not get Wireshark running, it hangs on loading configuration. From another question I learn that reinstall helps. THis brings the next issue:   uninstall fails because some assoicated program is running   So I guess it is DumpCap. right?   3.I cannot stop DumpCap however, since &quot;access is denie...'''
date = "2014-03-12T06:38:00Z"
lastmod = "2014-03-12T07:23:00Z"
weight = 30729
keywords = [ "dumpcap", "install", "uninstall" ]
aliases = [ "/questions/30729" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Uninstall Wireshark - Stop and get rid of DumpCap to reinstall everything](/questions/30729/uninstall-wireshark-stop-and-get-rid-of-dumpcap-to-reinstall-everything)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30729-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30729-score" class="post-score" title="current number of votes">0</div><span id="post-30729-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I do not get Wireshark running, it hangs on loading configuration. From another question I learn that reinstall helps. THis brings the next issue:</p><ol><li><p>uninstall fails because some assoicated program is running</p></li><li><p>So I guess it is DumpCap. right?</p></li></ol><p>3.I cannot stop DumpCap however, since "access is denied" even when I use Process Explorer as local Administrator.</p><p>So next question is: How can I uninstall Wireshark ? How can I stop DumpCap?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '14, 06:38</strong></p><img src="https://secure.gravatar.com/avatar/763e51406d48132ced03848e9e7b0fc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="citykid&#39;s gravatar image" /><p><span>citykid</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="citykid has no accepted answers">0%</span></p></div></div><div id="comments-container-30729" class="comments-container"></div><div id="comment-tools-30729" class="comment-tools"></div><div class="clear"></div><div id="comment-30729-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30730"></span>

<div id="answer-container-30730" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30730-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30730-score" class="post-score" title="current number of votes">0</div><span id="post-30730-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Solved it by rebooting.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '14, 07:23</strong></p><img src="https://secure.gravatar.com/avatar/763e51406d48132ced03848e9e7b0fc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="citykid&#39;s gravatar image" /><p><span>citykid</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="citykid has no accepted answers">0%</span></p></div></div><div id="comments-container-30730" class="comments-container"></div><div id="comment-tools-30730" class="comment-tools"></div><div class="clear"></div><div id="comment-30730-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

