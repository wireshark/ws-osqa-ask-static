+++
type = "question"
title = "Capture from modem 4G"
description = '''Hi! I&#x27;m need capture traffic from my 4g modem. But I don&#x27;t see it in interface list of Wireshark. What Am I need to do?'''
date = "2014-08-30T09:54:00Z"
lastmod = "2014-08-30T10:14:00Z"
weight = 35886
keywords = [ "capture", "modem" ]
aliases = [ "/questions/35886" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture from modem 4G](/questions/35886/capture-from-modem-4g)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35886-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35886-score" class="post-score" title="current number of votes">0</div><span id="post-35886-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>I'm need capture traffic from my 4g modem. But I don't see it in interface list of Wireshark.</p><p>What Am I need to do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-modem" rel="tag" title="see questions tagged &#39;modem&#39;">modem</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Aug '14, 09:54</strong></p><img src="https://secure.gravatar.com/avatar/c124485dcadedbfece8eba0efdeef85c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rizvan_ipd&#39;s gravatar image" /><p><span>Rizvan_ipd</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rizvan_ipd has no accepted answers">0%</span></p></div></div><div id="comments-container-35886" class="comments-container"></div><div id="comment-tools-35886" class="comment-tools"></div><div class="clear"></div><div id="comment-35886-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35887"></span>

<div id="answer-container-35887" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35887-score" class="post-score" title="current number of votes">0</div><span id="post-35887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Assuming you are on a Windows host, WinPCAP does not support this kind of interfaces. What you can do is either use Microsoft Network Monitor (or its replacement Microsoft Message Analyzer) to do the capture, or if it is plugged on a USB port do the capture at USB level thanks to <a href="http://desowin.org/usbpcap">USBPcap</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '14, 10:14</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-35887" class="comments-container"></div><div id="comment-tools-35887" class="comment-tools"></div><div class="clear"></div><div id="comment-35887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

