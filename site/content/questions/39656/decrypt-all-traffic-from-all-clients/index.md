+++
type = "question"
title = "Decrypt All traffic from all clients"
description = '''Ok I can decrypt a specified client connected to wifi by deauthing it and getting the handshake using Wireshark. After Wireshark captures the EAPOL messages it starts to decrypt the traffic. But I want to decrypt the traffic from ALL clients connected to the AP. I deauthenticate them and get all han...'''
date = "2015-02-05T00:03:00Z"
lastmod = "2015-02-16T23:43:00Z"
weight = 39656
keywords = [ "decryption" ]
aliases = [ "/questions/39656" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt All traffic from all clients](/questions/39656/decrypt-all-traffic-from-all-clients)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39656-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39656-score" class="post-score" title="current number of votes">0</div><span id="post-39656-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Ok I can decrypt a specified client connected to wifi by deauthing it and getting the handshake using Wireshark. After Wireshark captures the EAPOL messages it starts to decrypt the traffic.</p><p>But I want to decrypt the traffic from ALL clients connected to the AP. I deauthenticate them and get all handshakes but not all their traffic gets decrypted. I don't know which one is decrypted but I'm sure not ALL of them.</p><p>Any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '15, 00:03</strong></p><img src="https://secure.gravatar.com/avatar/4485813e7500770cc2dd74393d69b392?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srsh&#39;s gravatar image" /><p><span>srsh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srsh has no accepted answers">0%</span></p></div></div><div id="comments-container-39656" class="comments-container"><span id="39754"></span><div id="comment-39754" class="comment"><div id="post-39754-score" class="comment-score"></div><div class="comment-text"><blockquote><p>but I'm sure not ALL of them.</p></blockquote><p>how do you derive that conclusion?</p></div><div id="comment-39754-info" class="comment-info"><span class="comment-age">(10 Feb '15, 03:05)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="39902"></span><div id="comment-39902" class="comment"><div id="post-39902-score" class="comment-score"></div><div class="comment-text"><p>Simply because the data from/to some clients are missing. In fact I can't find them by searching.</p></div><div id="comment-39902-info" class="comment-info"><span class="comment-age">(16 Feb '15, 23:43)</span> <span class="comment-user userinfo">srsh</span></div></div></div><div id="comment-tools-39656" class="comment-tools"></div><div class="clear"></div><div id="comment-39656-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

