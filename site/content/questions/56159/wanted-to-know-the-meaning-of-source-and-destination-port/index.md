+++
type = "question"
title = "Wanted to know the meaning of source and destination port."
description = '''On my package 1, under the info it says (source)54841 &amp;gt; 80(destination).  May I know what are these terms mean and why these particular ports are being used. i know that for tcp, they uses 80 for destination port. but is there a particular reason? Thank you'''
date = "2016-10-05T04:47:00Z"
lastmod = "2016-10-05T04:53:00Z"
weight = 56159
keywords = [ "basic", "wireshark" ]
aliases = [ "/questions/56159" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wanted to know the meaning of source and destination port.](/questions/56159/wanted-to-know-the-meaning-of-source-and-destination-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56159-score" class="post-score" title="current number of votes">0</div><span id="post-56159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On my package 1, under the info it says (source)54841 &gt; 80(destination). May I know what are these terms mean and why these particular ports are being used. i know that for tcp, they uses 80 for destination port. but is there a particular reason? Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-basic" rel="tag" title="see questions tagged &#39;basic&#39;">basic</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '16, 04:47</strong></p><img src="https://secure.gravatar.com/avatar/24ef87d3554361031b6dceafb51d7d2a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tomandscooby&#39;s gravatar image" /><p><span>tomandscooby</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tomandscooby has no accepted answers">0%</span></p></div></div><div id="comments-container-56159" class="comments-container"></div><div id="comment-tools-56159" class="comment-tools"></div><div class="clear"></div><div id="comment-56159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56160"></span>

<div id="answer-container-56160" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56160-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56160-score" class="post-score" title="current number of votes">1</div><span id="post-56160-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wikipedia entry on <a href="https://en.wikipedia.org/wiki/Port_(computer_networking)">Ports</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '16, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-56160" class="comments-container"></div><div id="comment-tools-56160" class="comment-tools"></div><div class="clear"></div><div id="comment-56160-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

