+++
type = "question"
title = "[closed] Pcap help needed"
description = '''Have a pcap file I need help with attempting to figure out the following: The IP address of the router? The model of the router if possible? The firmware of the router? The release number of the router is using? And last the ip address of the user who logged into the router admin panel? link text]1'''
date = "2017-07-18T14:18:00Z"
lastmod = "2017-07-18T14:20:00Z"
weight = 62852
keywords = [ "pcap" ]
aliases = [ "/questions/62852" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Pcap help needed](/questions/62852/pcap-help-needed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62852-score" class="post-score" title="current number of votes">0</div><span id="post-62852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Have a pcap file I need help with attempting to figure out the following: The IP address of the router? The model of the router if possible? The firmware of the router? The release number of the router is using? And last the ip address of the user who logged into the router admin panel?</p><p>link text]<a href="https://www.dropbox.com/s/4thsx9xa52gmalq/NCL-2015-PCAP3.cap?dl=0">1</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '17, 14:18</strong></p><img src="https://secure.gravatar.com/avatar/aee8489136c023abf69ed04b97ec6c26?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pyrotaz&#39;s gravatar image" /><p><span>pyrotaz</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pyrotaz has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>18 Jul '17, 14:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-62852" class="comments-container"><span id="62853"></span><div id="comment-62853" class="comment"><div id="post-62853-score" class="comment-score"></div><div class="comment-text"><p>Please add your link to your original question instead of creating a new one. Thanks!</p></div><div id="comment-62853-info" class="comment-info"><span class="comment-age">(18 Jul '17, 14:20)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-62852" class="comment-tools"></div><div class="clear"></div><div id="comment-62852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Jasper 18 Jul '17, 14:19

</div>

</div>

</div>

