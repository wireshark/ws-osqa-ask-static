+++
type = "question"
title = "Using Wireshark to monitor specific locations on a map that have IP&#x27;s within the area searched"
description = '''I hate hackers and voyeurs who think that they have the right for whatever reason their twisted little minds tell them the reason is for their hacking and watching a persons online activity so that that the information can be used for political or religious reasons. I like my privacy and am entitled...'''
date = "2012-05-07T08:59:00Z"
lastmod = "2012-05-07T11:24:00Z"
weight = 10738
keywords = [ "hacking", "wifi" ]
aliases = [ "/questions/10738" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Using Wireshark to monitor specific locations on a map that have IP's within the area searched](/questions/10738/using-wireshark-to-monitor-specific-locations-on-a-map-that-have-ips-within-the-area-searched)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10738-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10738-score" class="post-score" title="current number of votes">-2</div><span id="post-10738-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I hate hackers and voyeurs who think that they have the right for whatever reason their twisted little minds tell them the reason is for their hacking and watching a persons online activity so that that the information can be used for political or religious reasons.</p><p>I like my privacy and am entitled to my privacy.</p><p>Is Wireshark able to have a monitoring feature where lets say that you connect to <a href="http://MSNBC.com">MSNBC.com</a> and if someone in the area that you have established as the area to watch for connections connects to <a href="http://MSNBC.com">MSNBC.com</a> as well you would receive an alert based upon the time and IP of the computer that accessed <a href="http://MSNBC.com">MSNBC.com</a>'s portal?</p><p>Also does Wireshark have a process where it can monitor all out going WiFi traffic from a source PC such as my laptop that will tell me where the leach is coming from as well as which processes such as email, websites visited etc. that the leacher is gathering information about a person using WiFi?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hacking" rel="tag" title="see questions tagged &#39;hacking&#39;">hacking</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 May '12, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/f661de45733e702d850cdf97d07025e4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="screenburner&#39;s gravatar image" /><p><span>screenburner</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="screenburner has no accepted answers">0%</span></p></div></div><div id="comments-container-10738" class="comments-container"></div><div id="comment-tools-10738" class="comment-tools"></div><div class="clear"></div><div id="comment-10738-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10743"></span>

<div id="answer-container-10743" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10743-score" class="post-score" title="current number of votes">2</div><span id="post-10743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><h1 id="no.">No.</h1><p>Wireshark cannot snoop on other Internet users from your home. Monitoring your <em>own</em> home network traffic is possible, and from this you could identify what particular <em>device</em> is using your network, but not necessarily what it is doing or even <em>where</em> it is, and certainly not what particular applications that device might be using. <strong>Always use encryption when possible to protect your private data.</strong> Once the information leaves your home network (whether through your <em>wired</em> modem or <em>wireless</em> connection), anyone who can observe it can know its entire content --this could be anyone between you and your Internet destination for Internet traffic, or anyone with a wireless-enabled device if you have not properly secured your wireless network (and, perhaps, some more dedicated parties if you have gone to the trouble of securing your wireless).</p><p>A tool such as you describe does not exist.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 May '12, 11:24</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span></p></div></div><div id="comments-container-10743" class="comments-container"></div><div id="comment-tools-10743" class="comment-tools"></div><div class="clear"></div><div id="comment-10743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

