+++
type = "question"
title = "P1 MTA bind X.224 COTP packet - where does [Destination reference: 0x2c3d0000] come from"
description = '''P1 MTA bind X.224 COTP packet - where does [Destination reference: 0x2c3d0000] come from? I have working X.400 p1 connection from a 2010 code base and we are upgrading it to our current code base. The remote side responds with a bind-busy now. In my X.224 COTP packet on the old working connection &quot;[...'''
date = "2017-03-31T13:25:00Z"
lastmod = "2017-03-31T13:25:00Z"
weight = 60498
keywords = [ "p1", "dst_ref", "cotp", "x.224" ]
aliases = [ "/questions/60498" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [P1 MTA bind X.224 COTP packet - where does \[Destination reference: 0x2c3d0000\] come from](/questions/60498/p1-mta-bind-x224-cotp-packet-where-does-destination-reference-0x2c3d0000-come-from)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60498-score" class="post-score" title="current number of votes">0</div><span id="post-60498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>P1 MTA bind X.224 COTP packet - where does [Destination reference: 0x2c3d0000] come from?</p><p>I have working X.400 p1 connection from a 2010 code base and we are upgrading it to our current code base. The remote side responds with a bind-busy now.</p><p>In my X.224 COTP packet on the old working connection "[Destination reference: 0x00000] is indicated. On the non-working connection, a non-zero Destination reference is indicated.</p><p>Where does the wireshark get this information from? - it's not part of the COTP packet. It is also enclosed in square brackets.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/graeme_01.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-p1" rel="tag" title="see questions tagged &#39;p1&#39;">p1</span> <span class="post-tag tag-link-dst_ref" rel="tag" title="see questions tagged &#39;dst_ref&#39;">dst_ref</span> <span class="post-tag tag-link-cotp" rel="tag" title="see questions tagged &#39;cotp&#39;">cotp</span> <span class="post-tag tag-link-x.224" rel="tag" title="see questions tagged &#39;x.224&#39;">x.224</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '17, 13:25</strong></p><img src="https://secure.gravatar.com/avatar/01183070f2f2be89f4bc8ab6d60a82cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wernerreiche&#39;s gravatar image" /><p><span>wernerreiche</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wernerreiche has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Mar '17, 13:26</strong> </span></p></div></div><div id="comments-container-60498" class="comments-container"></div><div id="comment-tools-60498" class="comment-tools"></div><div class="clear"></div><div id="comment-60498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

