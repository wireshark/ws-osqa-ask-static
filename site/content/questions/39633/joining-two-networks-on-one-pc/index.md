+++
type = "question"
title = "Joining two networks on one PC"
description = '''Hi, I was wondering if anyone can help me. I have 2 NICs on one pc, IP of the first router is 192.168.1.1 and its a wired only network with internet access. The second is a dual band wireless router IP 192.168.0.1 I was wondering how I would allow the wireless router to use the internet connection o...'''
date = "2015-02-04T04:34:00Z"
lastmod = "2015-02-04T12:23:00Z"
weight = 39633
keywords = [ "wireless", "wan", "wired", "networks", "dns" ]
aliases = [ "/questions/39633" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Joining two networks on one PC](/questions/39633/joining-two-networks-on-one-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39633-score" class="post-score" title="current number of votes">0</div><span id="post-39633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I was wondering if anyone can help me. I have 2 NICs on one pc, IP of the first router is 192.168.1.1 and its a wired only network with internet access. The second is a dual band wireless router IP 192.168.0.1</p><p>I was wondering how I would allow the wireless router to use the internet connection on the first router so the traffic passes through the pc.</p><p>I tried bridging the connections which didn't seem to work at first but I managed to get one device connected to the wireless connection with internet access but any other devices I connected were unable to reach the internet.<br />
</p><p>I also tried setting the WAN ip of the wireless router to 192.168.1.15 using the IP of the first router for the WAN gateway and DNS but I couldn't connect at all. I was just wondering what route I should be going for.</p><p>Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wan" rel="tag" title="see questions tagged &#39;wan&#39;">wan</span> <span class="post-tag tag-link-wired" rel="tag" title="see questions tagged &#39;wired&#39;">wired</span> <span class="post-tag tag-link-networks" rel="tag" title="see questions tagged &#39;networks&#39;">networks</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Feb '15, 04:34</strong></p><img src="https://secure.gravatar.com/avatar/ad45e1036a84acfcb172a36581ea10fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Musket&#39;s gravatar image" /><p><span>Musket</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Musket has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-39633" class="comments-container"></div><div id="comment-tools-39633" class="comment-tools"></div><div class="clear"></div><div id="comment-39633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39634"></span>

<div id="answer-container-39634" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39634-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39634-score" class="post-score" title="current number of votes">1</div><span id="post-39634-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Note really a Wireshark question. You'll likely get better support at the appropriate forums for your OS.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Feb '15, 05:26</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-39634" class="comments-container"><span id="39637"></span><div id="comment-39637" class="comment"><div id="post-39637-score" class="comment-score"></div><div class="comment-text"><p>Hi, thanks for your comment. I just thought I'd ask here as as I am trying to use wireshark to monitor wifi traffic going through a router. Is there a better was to do this than by the methods I have mentioned using windows?</p><p>Thanks</p></div><div id="comment-39637-info" class="comment-info"><span class="comment-age">(04 Feb '15, 06:26)</span> <span class="comment-user userinfo">Musket</span></div></div><span id="39640"></span><div id="comment-39640" class="comment"><div id="post-39640-score" class="comment-score"></div><div class="comment-text"><p>Buy a cheap switch that can do port mirroring and put it between the Wifi AP and the internet router, mirror the traffic to another port and connect your monitoring system to that port.</p><p>See the wiki page for <a href="http://wiki.wireshark.org/SwitchReference">switches</a> for more info.</p></div><div id="comment-39640-info" class="comment-info"><span class="comment-age">(04 Feb '15, 06:59)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="39647"></span><div id="comment-39647" class="comment"><div id="post-39647-score" class="comment-score"></div><div class="comment-text"><p>Ok great, thanks for the help. I'll have a look into that.</p></div><div id="comment-39647-info" class="comment-info"><span class="comment-age">(04 Feb '15, 12:23)</span> <span class="comment-user userinfo">Musket</span></div></div></div><div id="comment-tools-39634" class="comment-tools"></div><div class="clear"></div><div id="comment-39634-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

