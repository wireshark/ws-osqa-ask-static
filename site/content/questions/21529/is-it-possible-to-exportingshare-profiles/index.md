+++
type = "question"
title = "Is it possible to exporting/share profiles?"
description = '''The profile definitions is an extremely versatile feature, and I have begun creating a variety of them depending on what view of the collected data I am interested in. What I have been unable to do is to share a &quot;crafted profile&quot; with co-workers or my work station at home. Is there a method to repli...'''
date = "2013-05-28T11:02:00Z"
lastmod = "2013-05-29T10:31:00Z"
weight = 21529
keywords = [ "save", "export", "profiles" ]
aliases = [ "/questions/21529" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to exporting/share profiles?](/questions/21529/is-it-possible-to-exportingshare-profiles)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21529-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21529-score" class="post-score" title="current number of votes">0</div><span id="post-21529-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The profile definitions is an extremely versatile feature, and I have begun creating a variety of them depending on what view of the collected data I am interested in. What I have been unable to do is to share a "crafted profile" with co-workers or my work station at home. Is there a method to replicate a profile definition from one host to others?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-profiles" rel="tag" title="see questions tagged &#39;profiles&#39;">profiles</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 May '13, 11:02</strong></p><img src="https://secure.gravatar.com/avatar/dd7ccbff7bb78e38074d113186debea3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EngAtWork&#39;s gravatar image" /><p><span>EngAtWork</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EngAtWork has no accepted answers">0%</span></p></div></div><div id="comments-container-21529" class="comments-container"></div><div id="comment-tools-21529" class="comment-tools"></div><div class="clear"></div><div id="comment-21529-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="21530"></span>

<div id="answer-container-21530" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21530-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21530-score" class="post-score" title="current number of votes">0</div><span id="post-21530-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to Help&gt;About wireshark &gt;Folders&gt;Personal configuration in Wireshark GUI.</p><p>when you click personal configuration link it will open the location of your profiles.Under Profiles folder you can see all your crafted profiles just select and share profile of your interest with your co-workers by copying it from your source location to destination.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 May '13, 11:07</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 May '13, 11:17</strong> </span></p></div></div><div id="comments-container-21530" class="comments-container"></div><div id="comment-tools-21530" class="comment-tools"></div><div class="clear"></div><div id="comment-21530-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21531"></span>

<div id="answer-container-21531" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21531-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21531-score" class="post-score" title="current number of votes">0</div><span id="post-21531-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, you can find the files that are part of a profile in the Wireshark configuration directory which is located in your user home directory path. For Windows Vista and up its at c:\users\USERNAME\AppData\Roaming\Wireshark, while for Linux its in the Wireshark subfolder of the normal user home directory.</p><p>In there are some files depending on if you've already changed settings (if they're default you'll not see a file until you change a setting) for capture filteres, preferences, displayfilters, colorfilters etc. These are the default settings.</p><p>Each additional profile there is a subdirectory, again with a set of files that make up the profile. Just copy the files to the according directory on the target machine and you've migrated the settings.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 May '13, 11:07</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-21531" class="comments-container"><span id="21533"></span><div id="comment-21533" class="comment"><div id="post-21533-score" class="comment-score"></div><div class="comment-text"><p>Thank you - I admit I'd tried to tweak the display order of columns by editing (despite the warnings against doing so), which invalidated the definition.</p></div><div id="comment-21533-info" class="comment-info"><span class="comment-age">(28 May '13, 11:18)</span> <span class="comment-user userinfo">EngAtWork</span></div></div><span id="21535"></span><div id="comment-21535" class="comment"><div id="post-21535-score" class="comment-score">1</div><div class="comment-text"><p>why edit the file? Use drag and drop on the colum headers to rearrange them, or by dragging rows up and down in the preferences setting ;-)</p></div><div id="comment-21535-info" class="comment-info"><span class="comment-age">(28 May '13, 11:20)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-21531" class="comment-tools"></div><div class="clear"></div><div id="comment-21531-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21572"></span>

<div id="answer-container-21572" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21572-score" class="post-score" title="current number of votes">0</div><span id="post-21572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have my profiles directory linked to Dropbox, that way I have all my profiles available on all three laptops. You could make a shared folder on Dropbox to even share the profiles with other users.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '13, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-21572" class="comments-container"><span id="21579"></span><div id="comment-21579" class="comment"><div id="post-21579-score" class="comment-score"></div><div class="comment-text"><p>I tried that, too, but went back to local profiles because the preferences file would be problematic in regard to the capture interfaces mentioned in there IIRC. On Windows all interfaces have different GUIDs for the NICs so Wireshark gets confused to see a preference mentioning NICs that aren't found in the system.</p></div><div id="comment-21579-info" class="comment-info"><span class="comment-age">(29 May '13, 10:31)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-21572" class="comment-tools"></div><div class="clear"></div><div id="comment-21572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

