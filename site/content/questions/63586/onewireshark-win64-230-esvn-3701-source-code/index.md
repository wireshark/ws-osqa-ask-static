+++
type = "question"
title = "&quot;OneWireshark-win64-2.3.0-ESVN-3701&quot; source code"
description = '''Hello everyone, I&#x27;m using wireshark to decode a captured file of Ericsson OCS for Telco which used Diameter protocol. I tried &quot;Decode As ...&quot; but some command codes still Unknown. I found that OneWireshark-win64-2.3.0-ESVN-3701 is able to understand these command codes but I cannot find out the sour...'''
date = "2017-09-12T02:04:00Z"
lastmod = "2017-09-13T18:47:00Z"
weight = 63586
keywords = [ "ericsson", "onewireshark" ]
aliases = [ "/questions/63586" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["OneWireshark-win64-2.3.0-ESVN-3701" source code](/questions/63586/onewireshark-win64-230-esvn-3701-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63586-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63586-score" class="post-score" title="current number of votes">0</div><span id="post-63586-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone, I'm using wireshark to decode a captured file of Ericsson OCS for Telco which used Diameter protocol. I tried "Decode As ..." but some command codes still Unknown. I found that OneWireshark-win64-2.3.0-ESVN-3701 is able to understand these command codes but I cannot find out the source code of this version from the official website of Wireshark. Could you please tell me how I can download the source code of "OneWireshark-win64-2.3.0-ESVN-3701" ?</p><p>Thank you very much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ericsson" rel="tag" title="see questions tagged &#39;ericsson&#39;">ericsson</span> <span class="post-tag tag-link-onewireshark" rel="tag" title="see questions tagged &#39;onewireshark&#39;">onewireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '17, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/824a7342f59ff90e6040505b38626416?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hoangsonk49&#39;s gravatar image" /><p><span>hoangsonk49</span><br />
<span class="score" title="81 reputation points">81</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="29 badges"><span class="silver">●</span><span class="badgecount">29</span></span><span title="33 badges"><span class="bronze">●</span><span class="badgecount">33</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hoangsonk49 has 2 accepted answers">28%</span></p></div></div><div id="comments-container-63586" class="comments-container"></div><div id="comment-tools-63586" class="comment-tools"></div><div class="clear"></div><div id="comment-63586-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63587"></span>

<div id="answer-container-63587" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63587-score" class="post-score" title="current number of votes">1</div><span id="post-63587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you look under help/about you will find the following notice:</p><blockquote><p>This software contains Ericsson proprietary information and is intended solely for internal Ericsson use. Under no circumstances should this software be resold, leased or otherwise allowed to go outside Ericsson to other companies or individuals alike Proprietary information Copyright Ericsson AB 2003-2017. All rights reserved.</p></blockquote></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Sep '17, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-63587" class="comments-container"><span id="63595"></span><div id="comment-63595" class="comment"><div id="post-63595-score" class="comment-score"></div><div class="comment-text"><p>Thank Anders for your information :)</p></div><div id="comment-63595-info" class="comment-info"><span class="comment-age">(13 Sep '17, 18:47)</span> <span class="comment-user userinfo">hoangsonk49</span></div></div></div><div id="comment-tools-63587" class="comment-tools"></div><div class="clear"></div><div id="comment-63587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

