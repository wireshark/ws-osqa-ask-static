+++
type = "question"
title = "Which wifi stick should I buy?"
description = '''Hello, I would like to buy a wifi stick and it would be very kind if someone could prepose me some supported wifi sticks :)  I need a wifi stick which supports the a/B/n standard,the5 ghz frequenz spectrum, packet injection, monitor and promiscous mode The stick should not be very expensive about 30...'''
date = "2015-08-13T03:33:00Z"
lastmod = "2015-08-13T06:17:00Z"
weight = 45050
keywords = [ "mode", "wifi", "stick", "monitor" ]
aliases = [ "/questions/45050" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Which wifi stick should I buy?](/questions/45050/which-wifi-stick-should-i-buy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45050-score" class="post-score" title="current number of votes">0</div><span id="post-45050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I would like to buy a wifi stick and it would be very kind if someone could prepose me some supported wifi sticks :)</p><p>I need a wifi stick which supports the a/B/n standard,the5 ghz frequenz spectrum, packet injection, monitor and promiscous mode</p><p>The stick should not be very expensive about 30 euros or cheaper..5ghz is no must have..</p><p>Thank you for your answers and suggestions!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-stick" rel="tag" title="see questions tagged &#39;stick&#39;">stick</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '15, 03:33</strong></p><img src="https://secure.gravatar.com/avatar/259ee2a78cb465805eea49d8874ba845?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eizi&#39;s gravatar image" /><p><span>eizi</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eizi has no accepted answers">0%</span></p></div></div><div id="comments-container-45050" class="comments-container"></div><div id="comment-tools-45050" class="comment-tools"></div><div class="clear"></div><div id="comment-45050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45057"></span>

<div id="answer-container-45057" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45057-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45057-score" class="post-score" title="current number of votes">0</div><span id="post-45057-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I posted an answer to a similar question. Here is a link to my answer:</p><p><a href="https://ask.wireshark.org/questions/45014/5ghz-monitor-mode">https://ask.wireshark.org/questions/45014/5ghz-monitor-mode</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Aug '15, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-45057" class="comments-container"></div><div id="comment-tools-45057" class="comment-tools"></div><div class="clear"></div><div id="comment-45057-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

