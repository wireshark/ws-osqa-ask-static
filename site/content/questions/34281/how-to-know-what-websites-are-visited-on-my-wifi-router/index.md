+++
type = "question"
title = "How to know what websites are visited on my wifi router ?"
description = '''hello , I have a wifi router and many of my friends and neighbors have connected to it what I would like to know what websites they are visiting in a simple way &amp;gt;&amp;gt;&amp;gt; and how to send them a message  thanks in advance . '''
date = "2014-06-29T20:50:00Z"
lastmod = "2014-06-29T20:50:00Z"
weight = 34281
keywords = [ "security", "wifi", "tap", "hub" ]
aliases = [ "/questions/34281" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to know what websites are visited on my wifi router ?](/questions/34281/how-to-know-what-websites-are-visited-on-my-wifi-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34281-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34281-score" class="post-score" title="current number of votes">0</div><span id="post-34281-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello , I have a wifi router and many of my friends and neighbors have connected to it what I would like to know what websites they are visiting in a simple way &gt;&gt;&gt; and how to send them a message thanks in advance .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-tap" rel="tag" title="see questions tagged &#39;tap&#39;">tap</span> <span class="post-tag tag-link-hub" rel="tag" title="see questions tagged &#39;hub&#39;">hub</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '14, 20:50</strong></p><img src="https://secure.gravatar.com/avatar/6e767a60043377a2201c460e4949b01b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fabian%20Lightangel&#39;s gravatar image" /><p><span>Fabian Light...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fabian Lightangel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jun '14, 20:55</strong> </span></p></div></div><div id="comments-container-34281" class="comments-container"></div><div id="comment-tools-34281" class="comment-tools"></div><div class="clear"></div><div id="comment-34281-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

