+++
type = "question"
title = "[closed] DHCP slow traffic"
description = '''Hello,i need to test DHCP traffic when is slow.I have problem ,because my traffic is not slow,how can i slow my dhcp traffic and test it?Tnx'''
date = "2015-01-05T08:05:00Z"
lastmod = "2015-01-05T08:05:00Z"
weight = 38891
keywords = [ "wireless", "dhcp", "wireshark" ]
aliases = [ "/questions/38891" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] DHCP slow traffic](/questions/38891/dhcp-slow-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38891-score" class="post-score" title="current number of votes">0</div><span id="post-38891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,i need to test DHCP traffic when is slow.I have problem ,because my traffic is not slow,how can i slow my dhcp traffic and test it?Tnx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-dhcp" rel="tag" title="see questions tagged &#39;dhcp&#39;">dhcp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '15, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/2c16e7186efbc82c4d412cafa7a47c4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Blue%20Master&#39;s gravatar image" /><p><span>Blue Master</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Blue Master has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>05 Jan '15, 08:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-38891" class="comments-container"></div><div id="comment-tools-38891" class="comment-tools"></div><div class="clear"></div><div id="comment-38891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 05 Jan '15, 08:24

</div>

</div>

</div>

