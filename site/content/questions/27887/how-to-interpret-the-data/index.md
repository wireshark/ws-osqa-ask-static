+++
type = "question"
title = "How to interpret the data ?"
description = '''Hi, this may be a very naive question, but I&#x27;ve just installed wireshark, and I&#x27;m not sure at all about how to interpret the data. Is there a forum on the net dedicated for wireshark (or other sniffer) data interpretation ?'''
date = "2013-12-07T06:22:00Z"
lastmod = "2013-12-07T18:23:00Z"
weight = 27887
keywords = [ "interpretation" ]
aliases = [ "/questions/27887" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to interpret the data ?](/questions/27887/how-to-interpret-the-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27887-score" class="post-score" title="current number of votes">0</div><span id="post-27887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, this may be a very naive question, but I've just installed wireshark, and I'm not sure at all about how to interpret the data. Is there a forum on the net dedicated for wireshark (or other sniffer) data interpretation ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interpretation" rel="tag" title="see questions tagged &#39;interpretation&#39;">interpretation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '13, 06:22</strong></p><img src="https://secure.gravatar.com/avatar/4a96f605faaabc3e30924dcd64fb5d8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ling%20talfi&#39;s gravatar image" /><p><span>Ling talfi</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ling talfi has no accepted answers">0%</span></p></div></div><div id="comments-container-27887" class="comments-container"></div><div id="comment-tools-27887" class="comment-tools"></div><div class="clear"></div><div id="comment-27887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27906"></span>

<div id="answer-container-27906" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27906-score" class="post-score" title="current number of votes">1</div><span id="post-27906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ling talfi has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are a few good resources out there for learning how to use the program in general. Some examples:</p><p>User manual: <a href="http://www.wireshark.org/download/docs/user-guide-a4.pdf">http://www.wireshark.org/download/docs/user-guide-a4.pdf</a></p><p>Video training (at cost): <a href="http://www.cbtnuggets.com/it-training-videos/course/cbtn_wireshark">http://www.cbtnuggets.com/it-training-videos/course/cbtn_wireshark</a></p><p>Books include: <a href="http://www.amazon.com/Wireshark-Network-Analysis-Second-Edition/dp/1893939944">http://www.amazon.com/Wireshark-Network-Analysis-Second-Edition/dp/1893939944</a></p><p>There are free video tutorials on YouTube as well. I've actually got a few on my channel though nothing really introductory and it's just kind of getting off the ground.</p><p>Also, this is the place to ask questions that might come up as you press onward. It's Q&amp;A rather than a forum but it works for spot questions if you need it. Also look through the history as most common questions have been asked and answered here before also.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '13, 18:23</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-27906" class="comments-container"></div><div id="comment-tools-27906" class="comment-tools"></div><div class="clear"></div><div id="comment-27906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

