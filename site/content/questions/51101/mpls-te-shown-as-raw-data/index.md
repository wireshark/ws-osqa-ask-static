+++
type = "question"
title = "[closed] MPLS TE shown as raw data"
description = '''Hello, I previously asked Jaap about this, but after 6 months I have not received any answer. So posting it again. I have the latest wireshark version and have this issue. I run MPLS TE in our network but the MPLS data is shown as &quot;raw data&quot;, meanwhile I can see the sample MPLS capture on the wiresh...'''
date = "2016-03-22T10:39:00Z"
lastmod = "2016-03-22T11:35:00Z"
weight = 51101
keywords = [ "raw", "engineering", "te", "traffic", "data" ]
aliases = [ "/questions/51101" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] MPLS TE shown as raw data](/questions/51101/mpls-te-shown-as-raw-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51101-score" class="post-score" title="current number of votes">0</div><span id="post-51101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I previously asked Jaap about this, but after 6 months I have not received any answer. So posting it again.</p><p>I have the latest wireshark version and have this issue. I run MPLS TE in our network but the MPLS data is shown as "raw data", meanwhile I can see the sample MPLS capture on the wireshark website and it is displayed correctly. I tried all the options in the protocols&gt;MPLS sections and it is still the same.</p><p>Is there a possibility to fix this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-raw" rel="tag" title="see questions tagged &#39;raw&#39;">raw</span> <span class="post-tag tag-link-engineering" rel="tag" title="see questions tagged &#39;engineering&#39;">engineering</span> <span class="post-tag tag-link-te" rel="tag" title="see questions tagged &#39;te&#39;">te</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '16, 10:39</strong></p><img src="https://secure.gravatar.com/avatar/0f3d9bacc469017f1ad93512ee870f2b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vadym%20Bel&#39;s gravatar image" /><p><span>Vadym Bel</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vadym Bel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>22 Mar '16, 11:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51101" class="comments-container"><span id="51104"></span><div id="comment-51104" class="comment"><div id="post-51104-score" class="comment-score"></div><div class="comment-text"><p>Raising a duplicate question isn't helpful. Hopefully someone will be able to answer your <a href="https://ask.wireshark.org/questions/47068/mpls-te-shown-as-raw-data">original</a> question, but note the link to the capture doesn't seem to work for me, so an update my be required.</p></div><div id="comment-51104-info" class="comment-info"><span class="comment-age">(22 Mar '16, 11:35)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-51101" class="comment-tools"></div><div class="clear"></div><div id="comment-51101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 22 Mar '16, 11:35

</div>

</div>

</div>

