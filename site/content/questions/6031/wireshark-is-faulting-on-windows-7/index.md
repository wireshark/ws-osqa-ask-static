+++
type = "question"
title = "Wireshark is faulting on Windows 7"
description = '''Hi - I installed version 1.6.1, enabled WinPCAP to start automatically and restarted windows 7. When I run Wireshark as administrator (or any user) it crashes. The following error is in the event log. The only thing about my environment of note is that I have VMware installed which adds some network...'''
date = "2011-08-31T15:27:00Z"
lastmod = "2011-08-31T15:43:00Z"
weight = 6031
keywords = [ "windows7", "crash" ]
aliases = [ "/questions/6031" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark is faulting on Windows 7](/questions/6031/wireshark-is-faulting-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6031-score" class="post-score" title="current number of votes">0</div><span id="post-6031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi - I installed version 1.6.1, enabled WinPCAP to start automatically and restarted windows 7. When I run Wireshark as administrator (or any user) it crashes. The following error is in the event log. The only thing about my environment of note is that I have VMware installed which adds some network drivers. Any suggestions?</p><p>Faulting application name: wireshark.exe, version: 1.6.1.38096, time stamp: 0x4e2498fd Faulting module name: libglib-2.0-0.dll, version: 2.26.1.0, time stamp: 0x4d176f2c Exception code: 0x40000015 Fault offset: 0x000559a5 Faulting process id: 0x1e40 Faulting application start time: 0x01cc682c5d48573a Faulting application path: C:Program FilesUtilitiesWiresharkwireshark.exe Faulting module path: C:Program FilesUtilitiesWiresharklibglib-2.0-0.dll Report Id: 9ee9202a-d41f-11e0-bc65-005056c00008</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '11, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/48e6e242880ab4cf8bd81928d9e695d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="David06902&#39;s gravatar image" /><p><span>David06902</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="David06902 has no accepted answers">0%</span></p></div></div><div id="comments-container-6031" class="comments-container"></div><div id="comment-tools-6031" class="comment-tools"></div><div class="clear"></div><div id="comment-6031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6032"></span>

<div id="answer-container-6032" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6032-score" class="post-score" title="current number of votes">0</div><span id="post-6032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Uninstalling and re-installing wireshark (keeping WinPCAP) resolved the problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '11, 15:43</strong></p><img src="https://secure.gravatar.com/avatar/48e6e242880ab4cf8bd81928d9e695d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="David06902&#39;s gravatar image" /><p><span>David06902</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="David06902 has no accepted answers">0%</span></p></div></div><div id="comments-container-6032" class="comments-container"></div><div id="comment-tools-6032" class="comment-tools"></div><div class="clear"></div><div id="comment-6032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

