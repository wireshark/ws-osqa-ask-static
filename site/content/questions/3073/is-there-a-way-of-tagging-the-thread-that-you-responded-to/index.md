+++
type = "question"
title = "Is there a way of tagging the thread that you responded to?"
description = '''Sometimes, I forget which thread I responded to when viewing the questions. I know you can look at the &quot;recent activity&quot; under the username, but is there a way of tagging it in the &quot;QUESTIONS&quot; tab? thank you!'''
date = "2011-03-23T16:57:00Z"
lastmod = "2011-03-24T16:18:00Z"
weight = 3073
keywords = [ "question", "forum" ]
aliases = [ "/questions/3073" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Is there a way of tagging the thread that you responded to?](/questions/3073/is-there-a-way-of-tagging-the-thread-that-you-responded-to)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3073-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3073-score" class="post-score" title="current number of votes">2</div><span id="post-3073-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sometimes, I forget which thread I responded to when viewing the questions. I know you can look at the "recent activity" under the username, but is there a way of tagging it in the "QUESTIONS" tab? thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-question" rel="tag" title="see questions tagged &#39;question&#39;">question</span> <span class="post-tag tag-link-forum" rel="tag" title="see questions tagged &#39;forum&#39;">forum</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '11, 16:57</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span></p></div></div><div id="comments-container-3073" class="comments-container"></div><div id="comment-tools-3073" class="comment-tools"></div><div class="clear"></div><div id="comment-3073-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3078"></span>

<div id="answer-container-3078" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3078-score" class="post-score" title="current number of votes">4</div><span id="post-3078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't know about tagging, per se, but you can subscribe to any question you want. On the right-hand side of the page of any question is a "Follow this question" section where you can "<em><code>subscribe me</code></em>".</p><p>Additionally, you can modify your subscription preferences to have yourself automatically subscribed to certain questions based on different criteria. Under your account settings, there should be a "<em><code>User tools</code></em>" drop-down. If you choose the "<em><code>email notification settings</code></em>", you should be able to change your preferences to hopefully meet your needs.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Mar '11, 07:13</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3078" class="comments-container"><span id="3093"></span><div id="comment-3093" class="comment"><div id="post-3093-score" class="comment-score"></div><div class="comment-text"><p>Ah, that'll work! Thanks for the hint.</p></div><div id="comment-3093-info" class="comment-info"><span class="comment-age">(24 Mar '11, 16:18)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-3078" class="comment-tools"></div><div class="clear"></div><div id="comment-3078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

