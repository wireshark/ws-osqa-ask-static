+++
type = "question"
title = "Wireshark filtering out ONLY telnet?"
description = '''Okay, so I have an oddball problem that I&#x27;m at a loss for. I have a Cisco switch on my desk and I&#x27;ve set up a monitor port such that my laptop can see the sessions going in and out of my desktop. (Laptop is obviously running Wireshark.) When I ping an external device from my desktop, I see all the p...'''
date = "2014-07-18T12:11:00Z"
lastmod = "2014-07-18T12:11:00Z"
weight = 34767
keywords = [ "telnet" ]
aliases = [ "/questions/34767" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark filtering out ONLY telnet?](/questions/34767/wireshark-filtering-out-only-telnet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34767-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34767-score" class="post-score" title="current number of votes">0</div><span id="post-34767-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Okay, so I have an oddball problem that I'm at a loss for.</p><p>I have a Cisco switch on my desk and I've set up a monitor port such that my laptop can see the sessions going in and out of my desktop. (Laptop is obviously running Wireshark.)</p><p>When I ping an external device from my desktop, I see all the packets associated with the ICMP. When I point my browser to the external entity, I see all those packets. However, when I telnet to the external device, I never see the telnet packets. Telnet works and I get to the host just fine. I just don't see them in Wireshark.</p><p>I have no capture or display filters in place... At all. I've done a complete uninstall of Wireshark (including settings) and reinstalled. For some reason my system is either dropping these or filtering them somewhere I can't find. Coworker's laptop with Wireshark sees the packets just fine. Thoughts? What the heck is going on with my system / install?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telnet" rel="tag" title="see questions tagged &#39;telnet&#39;">telnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '14, 12:11</strong></p><img src="https://secure.gravatar.com/avatar/92a30007136793701e4b9e809bbcf74d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AndyB&#39;s gravatar image" /><p><span>AndyB</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AndyB has no accepted answers">0%</span></p></div></div><div id="comments-container-34767" class="comments-container"></div><div id="comment-tools-34767" class="comment-tools"></div><div class="clear"></div><div id="comment-34767-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

