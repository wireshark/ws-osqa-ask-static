+++
type = "question"
title = "port mirroring: Cannot capture all traffic from my virtual machines."
description = '''Hello! I&#x27;m working with 5 PC&#x27;s: one for port mirroring and I want to capture traffic between the rest. In all the PC&#x27;s I have virtual machines running (ubuntu, virtualbox). In PC 1, 2 and 3 I have just one VM, and in the PC-4 I have 3 VM running. My problem is that I cannot capture all the traffic f...'''
date = "2015-05-06T08:57:00Z"
lastmod = "2015-05-06T08:57:00Z"
weight = 42141
keywords = [ "capture", "virtualbox", "wireshark" ]
aliases = [ "/questions/42141" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [port mirroring: Cannot capture all traffic from my virtual machines.](/questions/42141/port-mirroring-cannot-capture-all-traffic-from-my-virtual-machines)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42141-score" class="post-score" title="current number of votes">0</div><span id="post-42141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! I'm working with 5 PC's: one for port mirroring and I want to capture traffic between the rest. In all the PC's I have virtual machines running (ubuntu, virtualbox). In PC 1, 2 and 3 I have just one VM, and in the PC-4 I have 3 VM running.</p><p>My problem is that I cannot capture all the traffic from the PC-4, I get only the traffic from one virtual machine and for the 2 other VM wireshark cannot see their traffic.</p><p>How can I solve this problem? Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '15, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/9a91c4e67339dccebf1f569a8b53b96e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="houssem&#39;s gravatar image" /><p><span>houssem</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="houssem has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 May '15, 08:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-42141" class="comments-container"></div><div id="comment-tools-42141" class="comment-tools"></div><div class="clear"></div><div id="comment-42141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

