+++
type = "question"
title = "io graph wireshark - skype call"
description = '''anyone knows how to see in this graph the time gap where the connection was established the time gap of the conversarion and the time gap of the post-conversation. graph below: https://gyazo.com/2c5ab01b37003b8c862bbe33e469ef50'''
date = "2016-05-18T13:17:00Z"
lastmod = "2016-05-18T13:17:00Z"
weight = 52745
keywords = [ "urgent-help", "io", "iograph" ]
aliases = [ "/questions/52745" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [io graph wireshark - skype call](/questions/52745/io-graph-wireshark-skype-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52745-score" class="post-score" title="current number of votes">0</div><span id="post-52745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>anyone knows how to see in this graph the time gap where the connection was established the time gap of the conversarion and the time gap of the post-conversation. graph below:</p><p><a href="https://gyazo.com/2c5ab01b37003b8c862bbe33e469ef50">https://gyazo.com/2c5ab01b37003b8c862bbe33e469ef50</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-urgent-help" rel="tag" title="see questions tagged &#39;urgent-help&#39;">urgent-help</span> <span class="post-tag tag-link-io" rel="tag" title="see questions tagged &#39;io&#39;">io</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 May '16, 13:17</strong></p><img src="https://secure.gravatar.com/avatar/f3d077b5e5650e3bf3da0063fe334e26?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wireshark07&#39;s gravatar image" /><p><span>wireshark07</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wireshark07 has no accepted answers">0%</span></p></div></div><div id="comments-container-52745" class="comments-container"></div><div id="comment-tools-52745" class="comment-tools"></div><div class="clear"></div><div id="comment-52745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

