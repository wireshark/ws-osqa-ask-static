+++
type = "question"
title = "CMP message with ECC public key"
description = '''could you please provide wireshark capture for CMP certificate request message with ECC public key secp384r1 curve. '''
date = "2014-09-26T01:29:00Z"
lastmod = "2014-09-26T01:29:00Z"
weight = 36625
keywords = [ "capture", "wireshark" ]
aliases = [ "/questions/36625" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [CMP message with ECC public key](/questions/36625/cmp-message-with-ecc-public-key)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36625-score" class="post-score" title="current number of votes">0</div><span id="post-36625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>could you please provide wireshark capture for CMP certificate request message with ECC public key secp384r1 curve.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '14, 01:29</strong></p><img src="https://secure.gravatar.com/avatar/3bc9ad1edbd05b3e4bac07c4a1362732?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nag&#39;s gravatar image" /><p><span>nag</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nag has no accepted answers">0%</span></p></div></div><div id="comments-container-36625" class="comments-container"></div><div id="comment-tools-36625" class="comment-tools"></div><div class="clear"></div><div id="comment-36625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

