+++
type = "question"
title = "wireshark installation on windows 2000 server"
description = '''Hi All, please advise me, i am trying to install wire shark on windows server 2000 but i am getting error and from error message it looks like OS not supported. I even tried to download the small application version of wireshark and tried to install on USB drive but again win pcap ( ver 4.1.0) is no...'''
date = "2011-06-02T19:20:00Z"
lastmod = "2014-05-29T08:06:00Z"
weight = 4355
keywords = [ "installation", "windows2000", "server" ]
aliases = [ "/questions/4355" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark installation on windows 2000 server](/questions/4355/wireshark-installation-on-windows-2000-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4355-score" class="post-score" title="current number of votes">0</div><span id="post-4355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>please advise me, i am trying to install wire shark on windows server 2000 but i am getting error and from error message it looks like OS not supported.</p><p>I even tried to download the small application version of wireshark and tried to install on USB drive but again win pcap ( ver 4.1.0) is not installed on server.</p><p>Tried installing the win pcap 3.1 ver and tried to run the wire shark from usb drive but no luck.</p><p>please let me know which version works on windows server 2000. or any version that supports usb drive installation i can try that as i do not want to install permenantly on windows server.</p><p>I believe win pcap 3.1 supports windows 2000. so if any older version i can download and try. please send me the link of any older version that i can run for windows 2000 server.</p><p>Thanks, Bharat</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span> <span class="post-tag tag-link-windows2000" rel="tag" title="see questions tagged &#39;windows2000&#39;">windows2000</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '11, 19:20</strong></p><img src="https://secure.gravatar.com/avatar/72f763da37a74d5fe4d10620f2c66aa0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bharatp80&#39;s gravatar image" /><p><span>bharatp80</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bharatp80 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Feb '12, 18:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4355" class="comments-container"></div><div id="comment-tools-4355" class="comment-tools"></div><div class="clear"></div><div id="comment-4355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4356"></span>

<div id="answer-container-4356" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4356-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4356-score" class="post-score" title="current number of votes">0</div><span id="post-4356-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sorry i found another older version 1.2.7 which is working fine and i am able to install it.</p><p>sorry for confusion.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '11, 20:03</strong></p><img src="https://secure.gravatar.com/avatar/72f763da37a74d5fe4d10620f2c66aa0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bharatp80&#39;s gravatar image" /><p><span>bharatp80</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bharatp80 has no accepted answers">0%</span></p></div></div><div id="comments-container-4356" class="comments-container"><span id="4357"></span><div id="comment-4357" class="comment"><div id="post-4357-score" class="comment-score"></div><div class="comment-text"><p>In case you still want download older versions. Older versions of WinPcap are still available in the <a href="http://www.winpcap.org/archive/">archive</a>.<br />
Other download links:<br />
• all versions of <a href="ftp://ftp.uni-kl.de/pub/wireshark/win32/all-versions/">Wireshark(win32)</a><br />
• all versions of <a href="ftp://ftp.uni-kl.de/pub/wireshark/win64/all-versions/">Wireshark(win64)</a><br />
• all available <a href="http://www.wireshark.org/docs/relnotes/">release notes</a><br />
• latest stable <a href="http://www.winpcap.org/install/default.htm">WinPcap version 4.1.2</a><br />
</p></div><div id="comment-4357-info" class="comment-info"><span class="comment-age">(02 Jun '11, 23:55)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-4356" class="comment-tools"></div><div class="clear"></div><div id="comment-4356-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="33170"></span>

<div id="answer-container-33170" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33170-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33170-score" class="post-score" title="current number of votes">0</div><span id="post-33170-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The latest version that works on Windows Server 2000 is 1.2.18 which is available here:</p><p><a href="http://www.wireshark.org/download/win32/all-versions/wireshark-win32-1.2.18.exe">http://www.wireshark.org/download/win32/all-versions/wireshark-win32-1.2.18.exe</a></p><p>That's 32bit; if your OS is 64bit I would guess it's the IA64 architecture (Itanium) and I don't think there is any official Wireshark package for it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '14, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/8d772d2960dd73fc14d5be0183e977dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dermoth&#39;s gravatar image" /><p><span>dermoth</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dermoth has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-33170" class="comments-container"></div><div id="comment-tools-33170" class="comment-tools"></div><div class="clear"></div><div id="comment-33170-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

