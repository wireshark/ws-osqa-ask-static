+++
type = "question"
title = "How do I find my UDID on android?"
description = '''I am not a phone or tech junkie and don&#x27;t understand what it means to download parosproxy and run it, not do I understand how to use wireshark to find it? Any help in layman&#x27;s terms would be appreciated. I have the original Android and need help!!'''
date = "2011-05-11T16:31:00Z"
lastmod = "2011-05-11T22:30:00Z"
weight = 4049
keywords = [ "udid", "android" ]
aliases = [ "/questions/4049" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I find my UDID on android?](/questions/4049/how-do-i-find-my-udid-on-android)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4049-score" class="post-score" title="current number of votes">0</div><span id="post-4049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am not a phone or tech junkie and don't understand what it means to download parosproxy and run it, not do I understand how to use wireshark to find it? Any help in layman's terms would be appreciated. I have the original Android and need help!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udid" rel="tag" title="see questions tagged &#39;udid&#39;">udid</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 May '11, 16:31</strong></p><img src="https://secure.gravatar.com/avatar/d28d6f0e8c15df4716071c1bead02f72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kimhtaylor&#39;s gravatar image" /><p><span>kimhtaylor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kimhtaylor has no accepted answers">0%</span></p></div></div><div id="comments-container-4049" class="comments-container"><span id="4050"></span><div id="comment-4050" class="comment"><div id="post-4050-score" class="comment-score"></div><div class="comment-text"><p>Start googling to find useful information:<br />
<a href="http://stackoverflow.com/questions/3226135/android-udid-like-iphone">http://stackoverflow.com/questions/3226135/android-udid-like-iphone</a><br />
<a href="http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id">http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id</a></p></div><div id="comment-4050-info" class="comment-info"><span class="comment-age">(11 May '11, 22:30)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-4049" class="comment-tools"></div><div class="clear"></div><div id="comment-4049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

