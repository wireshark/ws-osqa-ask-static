+++
type = "question"
title = "Only Able to Capture One Side of RTP"
description = '''When I capture RTP Packets from my IP phone I only seem to get one side of the conversation. The source and destination stay the same all the way through the conversation. I never see the switch during the call. So when I go to play back the call I only get one side. The source always seems to be th...'''
date = "2012-06-14T17:18:00Z"
lastmod = "2012-06-14T17:18:00Z"
weight = 11910
keywords = [ "rtp" ]
aliases = [ "/questions/11910" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Only Able to Capture One Side of RTP](/questions/11910/only-able-to-capture-one-side-of-rtp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11910-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11910-score" class="post-score" title="current number of votes">0</div><span id="post-11910-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I capture RTP Packets from my IP phone I only seem to get one side of the conversation. The source and destination stay the same all the way through the conversation. I never see the switch during the call. So when I go to play back the call I only get one side. The source always seems to be the person who initiated the call.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '12, 17:18</strong></p><img src="https://secure.gravatar.com/avatar/d943ea305fd8f3e90687f25cbccb1151?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JBroeking&#39;s gravatar image" /><p><span>JBroeking</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JBroeking has no accepted answers">0%</span></p></div></div><div id="comments-container-11910" class="comments-container"></div><div id="comment-tools-11910" class="comment-tools"></div><div class="clear"></div><div id="comment-11910-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

