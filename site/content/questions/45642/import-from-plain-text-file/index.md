+++
type = "question"
title = "Import from plain text file"
description = '''I know Wireshark has an option to export packets into a plain text file. How can I achieve the opposite? I know that Wireshark cannot read text files directly, but can someone point me in the direction of how to modify the text file such that Wireshark can read and import it?'''
date = "2015-09-05T10:58:00Z"
lastmod = "2015-09-05T11:48:00Z"
weight = 45642
keywords = [ "import", "export" ]
aliases = [ "/questions/45642" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Import from plain text file](/questions/45642/import-from-plain-text-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45642-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45642-score" class="post-score" title="current number of votes">0</div><span id="post-45642-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I know Wireshark has an option to export packets into a plain text file. How can I achieve the opposite? I know that Wireshark cannot read text files directly, but can someone point me in the direction of how to modify the text file such that Wireshark can read and import it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-import" rel="tag" title="see questions tagged &#39;import&#39;">import</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '15, 10:58</strong></p><img src="https://secure.gravatar.com/avatar/a9d61cdbe90d3bdd8bde1cb50fdf05f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xeek&#39;s gravatar image" /><p><span>xeek</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xeek has no accepted answers">0%</span></p></div></div><div id="comments-container-45642" class="comments-container"></div><div id="comment-tools-45642" class="comment-tools"></div><div class="clear"></div><div id="comment-45642-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45643"></span>

<div id="answer-container-45643" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45643-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45643-score" class="post-score" title="current number of votes">0</div><span id="post-45643-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark has menu items (under "Export Packet DIssections") to export the summary lines, detailed packet dissections, or raw hex dump of packets to plain text files.</p><p>The only text files Wireshark could import would be text files containing hex dumps of raw packet data; there is no capability to read detailed packet dissections and attempt to figure out what the raw packet data would be for them.</p><p>Wireshark's capability to import text files is similar to what its text2pcap program provides; the text2pcap man page says:</p><pre><code>   Text2pcap understands a hexdump of the form generated by od −Ax −tx1
   −v.  In other words, each byte is individually displayed, with spaces
   separating the bytes from each other.  Each line begins with an offset
   describing the position in the file, with a space separating it from
   the following bytes.  The offset is a hex number (can also be octal or
   decimal − see −o), of more than two hex digits. Here is a sample dump
   that text2pcap can recognize:

   000000 00 0e b6 00 00 02 00 0e b6 00 00 01 08 00 45 00
   000010 00 28 00 00 00 00 ff 01 37 d1 c0 00 02 01 c0 00
   000020 02 02 08 00 a6 2f 00 01 00 01 48 65 6c 6c 6f 20
   000030 57 6f 72 6c 64 21
   000036

   There is no limit on the width or number of bytes per line. Also the
   text dump at the end of the line is ignored. Bytes/hex numbers can be
   uppercase or lowercase. Any text before the offset is ignored,
   including email forwarding characters ’&gt;’. Any lines of text between
   the bytestring lines is ignored. The offsets are used to track the
   bytes, so offsets must be correct. Any line which has only bytes
   without a leading offset is ignored. An offset is recognized as being a
   hex number longer than two characters. Any text after the bytes is
   ignored (e.g. the character dump). Any hex numbers in this text are
   also ignored. An offset of zero is indicative of starting a new packet,
   so a single text file with a series of hexdumps can be converted into a
   packet capture with multiple packets. Packets may be preceded by a
   timestamp. These are interpreted according to the format given on the
   command line (see −t). If not, the first packet is timestamped with the
   current time the conversion takes place. Multiple packets are written
   with timestamps differing by one microsecond each.  In general, short
   of these restrictions, text2pcap is pretty liberal about reading in
   hexdumps and has been tested with a variety of mangled outputs
   (including being forwarded through email multiple times, with limited
   line wrap etc.)

   There are a couple of other special features to note. Any line where
   the first non‐whitespace character is ’#’ will be ignored as a comment.
   Any line beginning with #TEXT2PCAP is a directive and options can be
   inserted after this command to be processed by text2pcap. Currently
   there are no directives implemented; in the future, these may be used
   to give more fine grained control on the dump and the way it should be
   processed e.g. timestamps, encapsulation type etc.

   Text2pcap also allows the user to read in dumps of application‐level
   data, by inserting dummy L2, L3 and L4 headers before each packet. The
   user can elect to insert Ethernet headers, Ethernet and IP, or
   Ethernet, IP and UDP/TCP/SCTP headers before each packet. This allows
   Wireshark or any other full‐packet decoder to handle these dumps.</code></pre><p>The time stamp format and "dumps of application-level data" stuff is, in Wireshark, controlled by fields in the "Import from Hex Dump..." dialog.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Sep '15, 11:48</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-45643" class="comments-container"></div><div id="comment-tools-45643" class="comment-tools"></div><div class="clear"></div><div id="comment-45643-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

