+++
type = "question"
title = "Bandwidth calculation formula"
description = '''Help I need to calculate bandwidth requirement for site with 200 workstations running 15 different applications, got all the applications bandwidth per workstation. How do I calculate the bandwidth to get the right size line?'''
date = "2012-02-15T08:25:00Z"
lastmod = "2012-02-15T08:25:00Z"
weight = 9026
keywords = [ "calculation", "formula", "bandwidth" ]
aliases = [ "/questions/9026" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Bandwidth calculation formula](/questions/9026/bandwidth-calculation-formula)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9026-score" class="post-score" title="current number of votes">0</div><span id="post-9026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Help I need to calculate bandwidth requirement for site with 200 workstations running 15 different applications, got all the applications bandwidth per workstation. How do I calculate the bandwidth to get the right size line?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-calculation" rel="tag" title="see questions tagged &#39;calculation&#39;">calculation</span> <span class="post-tag tag-link-formula" rel="tag" title="see questions tagged &#39;formula&#39;">formula</span> <span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '12, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/2f304b34d4dc34830ca551f86c1aab18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hennie&#39;s gravatar image" /><p><span>Hennie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hennie has no accepted answers">0%</span></p></div></div><div id="comments-container-9026" class="comments-container"></div><div id="comment-tools-9026" class="comment-tools"></div><div class="clear"></div><div id="comment-9026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

