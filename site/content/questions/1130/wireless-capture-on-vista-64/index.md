+++
type = "question"
title = "Wireless Capture on Vista 64"
description = '''Using windows Vista 64 bit have a dell wireless wlan card 1510 i have winacap and airpcap but still my wireless button is disabled.  NOW WHAT?? Kal'''
date = "2010-11-26T08:06:00Z"
lastmod = "2010-11-27T14:27:00Z"
weight = 1130
keywords = [ "wlan" ]
aliases = [ "/questions/1130" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless Capture on Vista 64](/questions/1130/wireless-capture-on-vista-64)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1130-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1130-score" class="post-score" title="current number of votes">0</div><span id="post-1130-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using windows Vista 64 bit</p><p>have a dell wireless wlan card 1510</p><p>i have winacap and airpcap but still my wireless button is disabled.</p><p>NOW WHAT??</p><p>Kal</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '10, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/863d1722bbf5ca6be22ec24596c69ea8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kal&#39;s gravatar image" /><p><span>Kal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kal has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Nov '10, 14:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span></p></div></div><div id="comments-container-1130" class="comments-container"><span id="1135"></span><div id="comment-1135" class="comment"><div id="post-1135-score" class="comment-score"></div><div class="comment-text"><p>Kal - I edited your title for clarity -</p><p>What "wireless button" are you referring to? Do you mean the Wireless toolbar? What version of Wireshark are you running? On my Vista systems I use the 32-bit version of Wireshark (as there are some limitations on the 64-bit version of Wireshark).</p><p>Did you mean winpcap? Do you actually have an AirPcap adapter?</p></div><div id="comment-1135-info" class="comment-info"><span class="comment-age">(27 Nov '10, 14:27)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-1130" class="comment-tools"></div><div class="clear"></div><div id="comment-1130-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

