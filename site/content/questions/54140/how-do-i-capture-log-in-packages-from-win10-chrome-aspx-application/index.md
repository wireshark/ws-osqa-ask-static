+++
type = "question"
title = "how do i capture log in packages from win10, chrome, .ASPX application"
description = '''I have a raspberry pi 3 with kali linux installed and wireshark connected to the same network with a windows 10 laptop running a web page that requires a user name - password login. I am running the web page with google chrome. i don&#x27;t seem to get any kind of http packets when every log in to the we...'''
date = "2016-07-18T13:01:00Z"
lastmod = "2016-07-18T14:41:00Z"
weight = 54140
keywords = [ "asp.net", "password", "windows10", "chrome-browser" ]
aliases = [ "/questions/54140" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how do i capture log in packages from win10, chrome, .ASPX application](/questions/54140/how-do-i-capture-log-in-packages-from-win10-chrome-aspx-application)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54140-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54140-score" class="post-score" title="current number of votes">0</div><span id="post-54140-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a raspberry pi 3 with kali linux installed and wireshark connected to the same network with a windows 10 laptop running a web page that requires a user name - password login. I am running the web page with google chrome. i don't seem to get any kind of http packets when every log in to the web page. both of the computers are connected to the same router with cables.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asp.net" rel="tag" title="see questions tagged &#39;asp.net&#39;">asp.net</span> <span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span> <span class="post-tag tag-link-chrome-browser" rel="tag" title="see questions tagged &#39;chrome-browser&#39;">chrome-browser</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '16, 13:01</strong></p><img src="https://secure.gravatar.com/avatar/dc82959124c00aee3b58ee4c721e2aac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thanasis%20Valto%20Kourpetis&#39;s gravatar image" /><p><span>Thanasis Val...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thanasis Valto Kourpetis has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jul '16, 16:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-54140" class="comments-container"></div><div id="comment-tools-54140" class="comment-tools"></div><div class="clear"></div><div id="comment-54140-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54147"></span>

<div id="answer-container-54147" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54147-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54147-score" class="post-score" title="current number of votes">0</div><span id="post-54147-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>both of the computers are connected to the same router with cables.</p></blockquote><p>Unless they are connected to the same <em>hub</em> with cables, you cannot capture one computer's traffic using another computer (except the traffic they exchange with each other, which is not your case).</p><p>See <a href="https://wiki.wireshark.org/CaptureSetup">Wireshark Wiki on capture setup</a> for more detailed information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '16, 14:41</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-54147" class="comments-container"></div><div id="comment-tools-54147" class="comment-tools"></div><div class="clear"></div><div id="comment-54147-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

