+++
type = "question"
title = "Waiting for Capture Input Data..."
description = '''Wireshark 1.6.0 Windows 7 Pro HP Mini Netbook 1GB. The above message displays when capture starts. No packets are displayed and data is captured. Data can not be displayed except with View | Show data in New Window. I can not find a way to display the data while capturing or after capture stops.'''
date = "2011-07-19T08:07:00Z"
lastmod = "2011-07-19T08:07:00Z"
weight = 5126
keywords = [ "capture", "display" ]
aliases = [ "/questions/5126" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Waiting for Capture Input Data...](/questions/5126/waiting-for-capture-input-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5126-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5126-score" class="post-score" title="current number of votes">0</div><span id="post-5126-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark 1.6.0 Windows 7 Pro HP Mini Netbook 1GB. The above message displays when capture starts. No packets are displayed and data is captured. Data can not be displayed except with View | Show data in New Window. I can not find a way to display the data while capturing or after capture stops.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '11, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/71882bbfce23eb593cf5fe74791ca113?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ryoung&#39;s gravatar image" /><p><span>ryoung</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ryoung has no accepted answers">0%</span></p></div></div><div id="comments-container-5126" class="comments-container"></div><div id="comment-tools-5126" class="comment-tools"></div><div class="clear"></div><div id="comment-5126-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

