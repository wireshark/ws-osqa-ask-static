+++
type = "question"
title = "74.124.69.108 TLSV1"
description = '''Does anyone know what this will be for? There are allot of TCP and TLSV1 connections using port 993?'''
date = "2014-10-04T10:48:00Z"
lastmod = "2014-10-04T12:50:00Z"
weight = 36840
keywords = [ "958" ]
aliases = [ "/questions/36840" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [74.124.69.108 TLSV1](/questions/36840/7412469108-tlsv1)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36840-score" class="post-score" title="current number of votes">0</div><span id="post-36840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know what this will be for? There are allot of TCP and TLSV1 connections using port 993?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-958" rel="tag" title="see questions tagged &#39;958&#39;">958</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '14, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/6528b7a1b93429c225c495afe7659433?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="elliep&#39;s gravatar image" /><p><span>elliep</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="elliep has no accepted answers">0%</span></p></div></div><div id="comments-container-36840" class="comments-container"></div><div id="comment-tools-36840" class="comment-tools"></div><div class="clear"></div><div id="comment-36840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36841"></span>

<div id="answer-container-36841" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36841-score" class="post-score" title="current number of votes">0</div><span id="post-36841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>IMAP over SSL (IMAPS) uses 993 typically.</p><p>Edit: As for the IP address, it's registered under an Alaskan ISP with no public details beyond that. Going by DNS, it looks like this is from an IP pool for mobile devices in a CDMA network (name resolves to "74-124-69-108.dynamic.cdma.acsalaska.net).</p><p>Best guess, the packets are from a mobile handset trying to reach a secure IMAP server over the Internet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Oct '14, 12:50</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Oct '14, 15:21</strong> </span></p></div></div><div id="comments-container-36841" class="comments-container"></div><div id="comment-tools-36841" class="comment-tools"></div><div class="clear"></div><div id="comment-36841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

