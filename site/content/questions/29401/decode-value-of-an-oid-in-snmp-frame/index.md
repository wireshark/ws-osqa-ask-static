+++
type = "question"
title = "Decode value of an OID in snmp frame"
description = '''The OID values ​​are not decoded, is always shown in decimal. IF-MIB :: ifOperStatus.14 (1.3.6.1.2.1.2.2.1.8.14): Object Name: 1.3.6.1.2.1.2.2.1.8.14 (IF-MIB :: ifOperStatus.14) Value (Integer32): 2 &amp;lt;- should interpret and say DOWN IF-MIB :: ifDescr.14 (1.3.6.1.2.1.2.2.1.2.14) 4e4f4e4500 &amp;lt;- sh...'''
date = "2014-02-03T09:42:00Z"
lastmod = "2014-02-03T09:42:00Z"
weight = 29401
keywords = [ "valueoid" ]
aliases = [ "/questions/29401" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decode value of an OID in snmp frame](/questions/29401/decode-value-of-an-oid-in-snmp-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29401-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29401-score" class="post-score" title="current number of votes">0</div><span id="post-29401-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The OID values ​​are not decoded, is always shown in decimal.</p><p>IF-MIB :: ifOperStatus.14 (1.3.6.1.2.1.2.2.1.8.14): Object Name: 1.3.6.1.2.1.2.2.1.8.14 (IF-MIB :: ifOperStatus.14) Value (Integer32): 2 <strong>&lt;- should interpret and say DOWN</strong></p><p>IF-MIB :: ifDescr.14 (1.3.6.1.2.1.2.2.1.2.14) 4e4f4e4500 <strong>&lt;- should interpret and say NONE</strong> Object Name: 1.3.6.1.2.1.2.2.1.2.14 (IF-MIB :: ifDescr.14) Value (OctetString): 4e4f4e4500 <strong>&lt;- should interpret and say NONE</strong> Please if you can tell me as I do to decode the values ​​of the OID</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-valueoid" rel="tag" title="see questions tagged &#39;valueoid&#39;">valueoid</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '14, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/4309b4e45619b8612a63e5b203a48d12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="crivera&#39;s gravatar image" /><p><span>crivera</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="crivera has no accepted answers">0%</span></p></div></div><div id="comments-container-29401" class="comments-container"></div><div id="comment-tools-29401" class="comment-tools"></div><div class="clear"></div><div id="comment-29401-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

