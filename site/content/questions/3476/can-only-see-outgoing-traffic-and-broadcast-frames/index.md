+++
type = "question"
title = "Can only see outgoing traffic and broadcast frames"
description = '''Hello All, I am trying to smiff my network using a hub on my Router&#x27;s LAN port. However, I can only see Local LAn frames, Frames originated from the LAN, broadcasts, but no frames comming from other hosts outside the LAN. (When the outside traffic is on my PC I can capture it) Note that I used anoth...'''
date = "2011-04-13T02:39:00Z"
lastmod = "2011-04-13T02:39:00Z"
weight = 3476
keywords = [ "outgoingunicast" ]
aliases = [ "/questions/3476" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can only see outgoing traffic and broadcast frames](/questions/3476/can-only-see-outgoing-traffic-and-broadcast-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3476-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3476-score" class="post-score" title="current number of votes">0</div><span id="post-3476-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All, I am trying to smiff my network using a hub on my Router's LAN port. However, I can only see Local LAn frames, Frames originated from the LAN, broadcasts, but no frames comming from other hosts outside the LAN. (When the outside traffic is on my PC I can capture it)</p><p>Note that I used another PC on the same HUB and it captures all the traffic successfully.</p><p>So it must be something on my PC.</p><p>I had Wireshark 1.4.1 and upgrated to 1.4.4 and still have the same problem.<br />
</p><p>Can you help me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outgoingunicast" rel="tag" title="see questions tagged &#39;outgoingunicast&#39;">outgoingunicast</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '11, 02:39</strong></p><img src="https://secure.gravatar.com/avatar/5ddf08722ae72e2fc5cce472975b6ab1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="charisc&#39;s gravatar image" /><p><span>charisc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="charisc has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Apr '11, 02:42</strong> </span></p></div></div><div id="comments-container-3476" class="comments-container"></div><div id="comment-tools-3476" class="comment-tools"></div><div class="clear"></div><div id="comment-3476-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

