+++
type = "question"
title = "Decoding Pilot packet"
description = '''Hi I am using Wireshark &quot;Version 1.8.6 (SVN Rev 48142 from /trunk-1.8)&quot; on Mac OS 10.7.5 . I want decode a &quot;Pilot&quot; packet but not seeing any option to in &quot;Decode As&quot; list.  Is there any provision I can decode a packet as Pilot ? I Is that any Plugin I would need to install for the same ?'''
date = "2013-04-25T21:07:00Z"
lastmod = "2013-04-26T01:23:00Z"
weight = 20814
keywords = [ "decode_as", "pilot" ]
aliases = [ "/questions/20814" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding Pilot packet](/questions/20814/decoding-pilot-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20814-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20814-score" class="post-score" title="current number of votes">0</div><span id="post-20814-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am using Wireshark "Version 1.8.6 (SVN Rev 48142 from /trunk-1.8)" on Mac OS 10.7.5 . I want decode a "Pilot" packet but not seeing any option to in "Decode As" list. Is there any provision I can decode a packet as Pilot ? I Is that any Plugin I would need to install for the same ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode_as" rel="tag" title="see questions tagged &#39;decode_as&#39;">decode_as</span> <span class="post-tag tag-link-pilot" rel="tag" title="see questions tagged &#39;pilot&#39;">pilot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '13, 21:07</strong></p><img src="https://secure.gravatar.com/avatar/5f01675e1c293ef9b7b1375afd75bf17?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yogoos&#39;s gravatar image" /><p><span>yogoos</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yogoos has no accepted answers">0%</span></p></div></div><div id="comments-container-20814" class="comments-container"><span id="20815"></span><div id="comment-20815" class="comment"><div id="post-20815-score" class="comment-score"></div><div class="comment-text"><p>please add some information about 'Pilot'. What is that? A protocol? A piece of software?</p></div><div id="comment-20815-info" class="comment-info"><span class="comment-age">(26 Apr '13, 01:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20814" class="comment-tools"></div><div class="clear"></div><div id="comment-20814-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

