+++
type = "question"
title = "MMS protocol tree item number"
description = '''Hi, I have transmission that contains mms reports with &quot;listOfVariable&quot; (with over 1000 items)... It would be greatly easier if &quot;listOfVariable&quot; and &quot;listOfAccessResult&quot; subitems could contains numbers / indexes. Is it possible to enable indexes for tree items or that kind of feature has to be added...'''
date = "2014-11-03T05:23:00Z"
lastmod = "2014-11-03T05:23:00Z"
weight = 37555
keywords = [ "subitems", "indexes", "tree", "mms", "view" ]
aliases = [ "/questions/37555" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [MMS protocol tree item number](/questions/37555/mms-protocol-tree-item-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37555-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37555-score" class="post-score" title="current number of votes">0</div><span id="post-37555-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have transmission that contains mms reports with "listOfVariable" (with over 1000 items)... It would be greatly easier if "listOfVariable" and "listOfAccessResult" subitems could contains numbers / indexes.</p><p>Is it possible to enable indexes for tree items or that kind of feature has to be added?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-subitems" rel="tag" title="see questions tagged &#39;subitems&#39;">subitems</span> <span class="post-tag tag-link-indexes" rel="tag" title="see questions tagged &#39;indexes&#39;">indexes</span> <span class="post-tag tag-link-tree" rel="tag" title="see questions tagged &#39;tree&#39;">tree</span> <span class="post-tag tag-link-mms" rel="tag" title="see questions tagged &#39;mms&#39;">mms</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '14, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/885fe173f0b22741a3e712e501eb6056?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mezdej&#39;s gravatar image" /><p><span>mezdej</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mezdej has no accepted answers">0%</span></p></div></div><div id="comments-container-37555" class="comments-container"></div><div id="comment-tools-37555" class="comment-tools"></div><div class="clear"></div><div id="comment-37555-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

