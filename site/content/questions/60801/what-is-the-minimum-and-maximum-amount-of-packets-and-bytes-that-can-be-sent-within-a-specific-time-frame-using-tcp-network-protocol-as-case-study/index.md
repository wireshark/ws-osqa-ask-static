+++
type = "question"
title = "What is the minimum and maximum amount of packets and bytes that can be sent within  a specific time frame using TCP network protocol as case study"
description = '''Please i am a research student, i am working on the data collected from Lawrence Berkeley National Laboratory (LBNL) ( ftp://ftp.bro-ids.org/enterprise-traces/hdr-traces05/ ) and i am using Wireshark network analyzer for the analysis, but for i not being conversant with the network protocols, i am h...'''
date = "2017-04-13T05:23:00Z"
lastmod = "2017-04-13T05:23:00Z"
weight = 60801
keywords = [ "ethernet", "udp", "networking", "tcpdump", "wireshark" ]
aliases = [ "/questions/60801" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What is the minimum and maximum amount of packets and bytes that can be sent within a specific time frame using TCP network protocol as case study](/questions/60801/what-is-the-minimum-and-maximum-amount-of-packets-and-bytes-that-can-be-sent-within-a-specific-time-frame-using-tcp-network-protocol-as-case-study)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60801-score" class="post-score" title="current number of votes">0</div><span id="post-60801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please i am a research student, i am working on the data collected from Lawrence Berkeley National Laboratory (LBNL) ( <a href="ftp://ftp.bro-ids.org/enterprise-traces/hdr-traces05/">ftp://ftp.bro-ids.org/enterprise-traces/hdr-traces05/</a> ) and i am using Wireshark network analyzer for the analysis, but for i not being conversant with the network protocols, i am having challenges in understanding the normal behaviour of the network protocols. This lead me into asking the the following question in order to help me continue in my research work. By looking at Conversation Statistics in Wireshark,</p><ol><li>What is the minimum and maximum amount of packets and bytes that can be sent within a specific time frame?</li><li>And finally what is the number of addresses from the same port (A or B) that can be permitted to engage in communication at the same time frame ?</li></ol><p>Please i am a beginner and so my question might not seem professional, but i will be glad if i receive an answer to the above questions. Thanks and God bless you all</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-networking" rel="tag" title="see questions tagged &#39;networking&#39;">networking</span> <span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '17, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/2684ca6915e0a949c2442e7ca10cad91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moronto&#39;s gravatar image" /><p><span>moronto</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moronto has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Apr '17, 05:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-60801" class="comments-container"></div><div id="comment-tools-60801" class="comment-tools"></div><div class="clear"></div><div id="comment-60801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

