+++
type = "question"
title = "cell phone capture when connected to wifi"
description = '''if i connect my phone to my own wifi i cant seem to get wireshark to capture any data. am i missing something'''
date = "2015-03-07T16:36:00Z"
lastmod = "2015-03-09T13:03:00Z"
weight = 40358
keywords = [ "cell", "phone", "capture" ]
aliases = [ "/questions/40358" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [cell phone capture when connected to wifi](/questions/40358/cell-phone-capture-when-connected-to-wifi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40358-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40358-score" class="post-score" title="current number of votes">0</div><span id="post-40358-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>if i connect my phone to my own wifi i cant seem to get wireshark to capture any data. am i missing something</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cell" rel="tag" title="see questions tagged &#39;cell&#39;">cell</span> <span class="post-tag tag-link-phone" rel="tag" title="see questions tagged &#39;phone&#39;">phone</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Mar '15, 16:36</strong></p><img src="https://secure.gravatar.com/avatar/17f16daadc51318fdfc3ec490593c43a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anonymous1&#39;s gravatar image" /><p><span>anonymous1</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anonymous1 has no accepted answers">0%</span></p></div></div><div id="comments-container-40358" class="comments-container"><span id="40380"></span><div id="comment-40380" class="comment"><div id="post-40380-score" class="comment-score"></div><div class="comment-text"><p>Hi, It would be helpful to know what OS you are running.</p><p>May I also suggest reading this</p><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">Wifi capture from wireshark wiki</a></p><pre><code>If you&#39;re trying to capture network traffic that&#39;s not being sent to or from the machine running Wireshark or TShark, i.e. traffic between two or more other machines on an Ethernet segment, or are interested in 802.11 management or control packets, or are interested in radio-layer information about packets, you will probably have to capture in &quot;monitor mode&quot;. This is discussed below.</code></pre><p>regards</p></div><div id="comment-40380-info" class="comment-info"><span class="comment-age">(09 Mar '15, 04:43)</span> <span class="comment-user userinfo">izopizo</span></div></div><span id="40398"></span><div id="comment-40398" class="comment"><div id="post-40398-score" class="comment-score"></div><div class="comment-text"><p>i am running windows 7</p></div><div id="comment-40398-info" class="comment-info"><span class="comment-age">(09 Mar '15, 12:57)</span> <span class="comment-user userinfo">anonymous1</span></div></div><span id="40399"></span><div id="comment-40399" class="comment"><div id="post-40399-score" class="comment-score"></div><div class="comment-text"><p>is your WiFi access point / switch capable of port mirroring?</p></div><div id="comment-40399-info" class="comment-info"><span class="comment-age">(09 Mar '15, 13:03)</span> <span class="comment-user userinfo">net_tech</span></div></div></div><div id="comment-tools-40358" class="comment-tools"></div><div class="clear"></div><div id="comment-40358-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

