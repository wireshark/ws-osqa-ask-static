+++
type = "question"
title = "interface names for rpcap"
description = '''Hello  I am using wireshark version 1.12. I want to use wireshark with remote interfaces. I have one remote server with 8 Ethernet Interfaces and i have started on my remote server the rpcapd with the options rpcapd -n -p 2001. On my local machine i can add the remote interface, but it is hard to fi...'''
date = "2015-05-20T03:51:00Z"
lastmod = "2015-05-20T03:51:00Z"
weight = 42574
keywords = [ "device", "rpcapd", "name", "in" ]
aliases = [ "/questions/42574" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [interface names for rpcap](/questions/42574/interface-names-for-rpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42574-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42574-score" class="post-score" title="current number of votes">0</div><span id="post-42574-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I am using wireshark version 1.12. I want to use wireshark with remote interfaces. I have one remote server with 8 Ethernet Interfaces and i have started on my remote server the rpcapd with the options rpcapd -n -p 2001. On my local machine i can add the remote interface, but it is hard to find out the right interface from the interface list. It shows only a very long entry of network adapter. like \Dev\NPF_{............] Is there a possibilities to see here more user friendly names (.e.g. the interface name of the network adapter).</p><p>Best regards Harald</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-device" rel="tag" title="see questions tagged &#39;device&#39;">device</span> <span class="post-tag tag-link-rpcapd" rel="tag" title="see questions tagged &#39;rpcapd&#39;">rpcapd</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span> <span class="post-tag tag-link-in" rel="tag" title="see questions tagged &#39;in&#39;">in</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '15, 03:51</strong></p><img src="https://secure.gravatar.com/avatar/2242ab4877ad71ce4e131f6beb6af0e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="klopfstock&#39;s gravatar image" /><p><span>klopfstock</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="klopfstock has no accepted answers">0%</span></p></div></div><div id="comments-container-42574" class="comments-container"></div><div id="comment-tools-42574" class="comment-tools"></div><div class="clear"></div><div id="comment-42574-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

