+++
type = "question"
title = "portableapps_package.vcxproj missing to build Portable version of wireshark"
description = '''Hi, I would like to build a portable version of my wireshark. But I don&#x27;t find portableapps_package.vcxproj in the directory. What do I have to do? Can I get it somewhere? Thanks'''
date = "2016-10-19T05:04:00Z"
lastmod = "2016-10-19T05:43:00Z"
weight = 56505
keywords = [ "portable" ]
aliases = [ "/questions/56505" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [portableapps\_package.vcxproj missing to build Portable version of wireshark](/questions/56505/portableapps_packagevcxproj-missing-to-build-portable-version-of-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56505-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56505-score" class="post-score" title="current number of votes">0</div><span id="post-56505-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I would like to build a portable version of my wireshark. But I don't find portableapps_package.vcxproj in the directory. What do I have to do? Can I get it somewhere?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-portable" rel="tag" title="see questions tagged &#39;portable&#39;">portable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '16, 05:04</strong></p><img src="https://secure.gravatar.com/avatar/adff8d731ffe044e74b218776bff2c64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lumi&#39;s gravatar image" /><p><span>Lumi</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lumi has no accepted answers">0%</span></p></div></div><div id="comments-container-56505" class="comments-container"><span id="56506"></span><div id="comment-56506" class="comment"><div id="post-56506-score" class="comment-score"></div><div class="comment-text"><p>Have you run the <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html#ChWin32Generate">CMake generation step</a> from the Developers Guide along with all the preceding steps? That generates the Visual Studio project files.</p></div><div id="comment-56506-info" class="comment-info"><span class="comment-age">(19 Oct '16, 05:43)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-56505" class="comment-tools"></div><div class="clear"></div><div id="comment-56505-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

