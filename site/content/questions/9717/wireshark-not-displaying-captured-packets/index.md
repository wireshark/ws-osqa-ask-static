+++
type = "question"
title = "Wireshark not displaying captured packets"
description = '''Hi All, I am unable to view the packets on my wireshark installed on Win Xp machine. It only shows one frame, however I can see that packets are being captured just fine and below I see the number of packets captured and displayed. If I open the pcap on another machine, it works just fine. I cant re...'''
date = "2012-03-23T02:40:00Z"
lastmod = "2015-10-13T14:43:00Z"
weight = 9717
keywords = [ "1.6.5", "wireshark" ]
aliases = [ "/questions/9717" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not displaying captured packets](/questions/9717/wireshark-not-displaying-captured-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9717-score" class="post-score" title="current number of votes">0</div><span id="post-9717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I am unable to view the packets on my wireshark installed on Win Xp machine. It only shows one frame, however I can see that packets are being captured just fine and below I see the number of packets captured and displayed. If I open the pcap on another machine, it works just fine. I cant remember having changed any setting on wireshark, it was working for me two days ago. I have reinstalled Wireshark, but that hasnt helped. Not sure if I am missing something obvious here. Has anyone come across this before?</p><p>Thanks, Soumya</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1.6.5" rel="tag" title="see questions tagged &#39;1.6.5&#39;">1.6.5</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '12, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/49b6871f06971d5e8e5dffe551e8e2e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Soumya0800&#39;s gravatar image" /><p><span>Soumya0800</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Soumya0800 has no accepted answers">0%</span></p></div></div><div id="comments-container-9717" class="comments-container"></div><div id="comment-tools-9717" class="comment-tools"></div><div class="clear"></div><div id="comment-9717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9722"></span>

<div id="answer-container-9722" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9722-score" class="post-score" title="current number of votes">2</div><span id="post-9722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It could be that someone has played a <a href="http://wiki.wireshark.org/PracticalJokes">practical joke</a> on you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '12, 14:25</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-9722" class="comments-container"><span id="46507"></span><div id="comment-46507" class="comment"><div id="post-46507-score" class="comment-score"></div><div class="comment-text"><p>Bad answer, cmaynard. How about a constructive one.</p></div><div id="comment-46507-info" class="comment-info"><span class="comment-age">(13 Oct '15, 12:31)</span> <span class="comment-user userinfo">mattmc61</span></div></div><span id="46524"></span><div id="comment-46524" class="comment"><div id="post-46524-score" class="comment-score">1</div><div class="comment-text"><p><em>Bad answer</em></p><p>How so? While I do not claim that my answer is the definitive solution to <a href="https://ask.wireshark.org/users/2409/soumya0800">Soumya0800</a>'s problem, the symptoms, as described, reminded me of the practical joke, thus I provided this answer in case it was indeed the source of the problem. If it's not, then there's no harm and we've at least narrowed down the problem a little bit by ruling out this particular situation. I suppose in your view any answer that does not definitively solve a problem is not constructive?</p><p>And how do you know that it's not the source of the problem? Has Soumya0800 provided some feedback to you, because no feedback has been provided here? If it's not the problem, and if Soumya0800 expects continued assistance, then it's up to Soumya0800 to provide feedback and additional details so that someone can help, or at least attempt to, although that's apparently not good enough for you.</p><p>Well, if you still think it's such a bad answer, then you are more than welcome to try to provide a better one.</p></div><div id="comment-46524-info" class="comment-info"><span class="comment-age">(13 Oct '15, 14:43)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-9722" class="comment-tools"></div><div class="clear"></div><div id="comment-9722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

