+++
type = "question"
title = "capturing packets on Windows 7 when a data card is being used"
description = '''hello I am a starter for wireshark.I am trying to capture packets using a reliance data card and my os is windows 7.In the interfaces list the ip of this datacard is not being shown .Can u help me Yamini'''
date = "2013-04-04T23:37:00Z"
lastmod = "2013-04-06T10:45:00Z"
weight = 20099
keywords = [ "capture", "windows7", "packets" ]
aliases = [ "/questions/20099" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [capturing packets on Windows 7 when a data card is being used](/questions/20099/capturing-packets-on-windows-7-when-a-data-card-is-being-used)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20099-score" class="post-score" title="current number of votes">0</div><span id="post-20099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello I am a starter for wireshark.I am trying to capture packets using a reliance data card and my os is windows 7.In the interfaces list the ip of this datacard is not being shown .Can u help me Yamini</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '13, 23:37</strong></p><img src="https://secure.gravatar.com/avatar/81fd26a61e9abcf15ba5c3223aa01dd2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yamini&#39;s gravatar image" /><p><span>yamini</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yamini has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Apr '13, 10:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-20099" class="comments-container"></div><div id="comment-tools-20099" class="comment-tools"></div><div class="clear"></div><div id="comment-20099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20105"></span>

<div id="answer-container-20105" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20105-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20105-score" class="post-score" title="current number of votes">2</div><span id="post-20105-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark uses WinPcap to list interfaces and to capture from the interfaces. If WinPcap is not able to list the reliance data card interface, you can contact the WinPcap team at <a href="http://www.winpcap.org/contact.htm">http://www.winpcap.org/contact.htm</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Apr '13, 00:33</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-20105" class="comments-container"><span id="20130"></span><div id="comment-20130" class="comment"><div id="post-20130-score" class="comment-score"></div><div class="comment-text"><p>Now I got the interface detected after i have used windowsXP. <a href="http://wiki.wireshark.org/SampleCaptures#head-5d1cb7d95d26641c61a5ba82ab7c0c76c08133e7">http://wiki.wireshark.org/SampleCaptures#head-5d1cb7d95d26641c61a5ba82ab7c0c76c08133e7</a> Exactly what I need is how to get this results which is the sample result given under PPP ... Can you provide any help Thanku in advance</p></div><div id="comment-20130-info" class="comment-info"><span class="comment-age">(06 Apr '13, 07:31)</span> <span class="comment-user userinfo">yamini</span></div></div><span id="20131"></span><div id="comment-20131" class="comment"><div id="post-20131-score" class="comment-score"></div><div class="comment-text"><p>PPP protocol samples which are given under PPP in sample captures</p></div><div id="comment-20131-info" class="comment-info"><span class="comment-age">(06 Apr '13, 07:33)</span> <span class="comment-user userinfo">yamini</span></div></div></div><div id="comment-tools-20105" class="comment-tools"></div><div class="clear"></div><div id="comment-20105-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20137"></span>

<div id="answer-container-20137" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20137-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20137-score" class="post-score" title="current number of votes">1</div><span id="post-20137-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A "data card" (i.e., an add-on network interface for mobile phone data) is a PPP interface on Windows. WinPcap is what Wireshark uses to capture traffic on Windows; as <a href="http://www.winpcap.org/misc/faq.htm#Q-5">this item in the WinPcap FAQ</a> notes, WinPcap supports PPP interfaces on 32-bit Windows XP, but does not support it on Windows Vista or later (or on 64-bit Windows XP).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '13, 10:45</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-20137" class="comments-container"></div><div id="comment-tools-20137" class="comment-tools"></div><div class="clear"></div><div id="comment-20137-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

