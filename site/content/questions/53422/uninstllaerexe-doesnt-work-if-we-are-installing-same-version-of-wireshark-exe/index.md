+++
type = "question"
title = "Uninstllaer.exe doesn’t work if we are installing Same Version of Wireshark ExE"
description = '''Hi If we are installing same version of Wireshark Uninstaller.exe doesn&#x27;t call on and it over rides the previous files and update newly. But in my requirement i have a plug-in added which needs to uninstalled completely and come up with new plug-in from new exe. Because Plugin name is differs. Regar...'''
date = "2016-06-13T20:17:00Z"
lastmod = "2016-06-14T15:02:00Z"
weight = 53422
keywords = [ "windows", "wireshark", "uninstall" ]
aliases = [ "/questions/53422" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Uninstllaer.exe doesn’t work if we are installing Same Version of Wireshark ExE](/questions/53422/uninstllaerexe-doesnt-work-if-we-are-installing-same-version-of-wireshark-exe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53422-score" class="post-score" title="current number of votes">0</div><span id="post-53422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>If we are installing same version of Wireshark Uninstaller.exe doesn't call on and it over rides the previous files and update newly.</p><p>But in my requirement i have a plug-in added which needs to uninstalled completely and come up with new plug-in from new exe. Because Plugin name is differs.</p><p>Regards Dinesh Sadu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '16, 20:17</strong></p><img src="https://secure.gravatar.com/avatar/04334c27cb629065a13d61a61b611038?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20Babu%20Sadu&#39;s gravatar image" /><p><span>Dinesh Babu ...</span><br />
<span class="score" title="16 reputation points">16</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh Babu Sadu has no accepted answers">0%</span></p></div></div><div id="comments-container-53422" class="comments-container"><span id="53443"></span><div id="comment-53443" class="comment"><div id="post-53443-score" class="comment-score"></div><div class="comment-text"><p>I cannot reproduce your issue, both with Wireshark 2.0.4 and current development tree. When I'm being asked whether I want to uninstall previous version (which is the exact same one as the one I'm currently installing) and I press yes, all files are removed.</p><p>Which Wireshark version are you building?</p></div><div id="comment-53443-info" class="comment-info"><span class="comment-age">(14 Jun '16, 15:02)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-53422" class="comment-tools"></div><div class="clear"></div><div id="comment-53422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

