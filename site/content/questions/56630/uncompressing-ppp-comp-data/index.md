+++
type = "question"
title = "Uncompressing PPP Comp Data"
description = '''I&#x27;m writing an application which suppose to integrate a new system, with an old command base system, this command base system provide an api, and very simple sample about how to send and receive data, but nothing more. till now i send data, received result, every thing goods... well it wasn&#x27;t that m...'''
date = "2016-10-24T23:22:00Z"
lastmod = "2016-10-24T23:22:00Z"
weight = 56630
keywords = [ "ppp", "uncompress" ]
aliases = [ "/questions/56630" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Uncompressing PPP Comp Data](/questions/56630/uncompressing-ppp-comp-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56630-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56630-score" class="post-score" title="current number of votes">0</div><span id="post-56630-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm writing an application which suppose to integrate a new system, with an old command base system, this command base system provide an api, and very simple sample about how to send and receive data, but nothing more. till now i send data, received result, every thing goods... well it wasn't that much good, lot of trouble, etc... BTW, till now it was just fine, but now i came across two command which they are acting differently... I send a command, receive two masked page instead of result, and i cannot just send another command simply, i have to move over the mask, fill the form, and confirm it in the end. which i do not know how this system transfer these data.</p><p>Now to find out which data and where they are sended by this APP, i run wireshark and capture some of it's data over an isolated VPN connection. but the data were compressed.</p><p>I <a href="https://drive.google.com/file/d/0B9FrLcW1mGYeQzJqVkxkTFZZM2M/view?usp=sharing">attach</a> the file i saved from this action, shall you may help me. The VPN IP address itself is: ipres4.iranair.com [78.38.203.224] but i'm not sure which server does this program communicate with.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ppp" rel="tag" title="see questions tagged &#39;ppp&#39;">ppp</span> <span class="post-tag tag-link-uncompress" rel="tag" title="see questions tagged &#39;uncompress&#39;">uncompress</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '16, 23:22</strong></p><img src="https://secure.gravatar.com/avatar/c9b57c9f547f934358f893181c1f1eba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deadManN&#39;s gravatar image" /><p><span>deadManN</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deadManN has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Oct '16, 23:23</strong> </span></p></div></div><div id="comments-container-56630" class="comments-container"></div><div id="comment-tools-56630" class="comment-tools"></div><div class="clear"></div><div id="comment-56630-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

