+++
type = "question"
title = "nmake /f Makefile.nmake all error"
description = '''after executing nmake /f Makefile.nmake setup succesfully. When i am executing nmake /f Makefile.nmake all,  i am getting an error, &#x27;c:&#92;Program&#x27; is not recognized as an internal or external command, operable program or batch file. NMAKE : fatal error U1077: &#x27;C:&#92;WINDOWS&#92;system32&#92;cmd.exe&#x27; : return cod...'''
date = "2015-02-26T01:39:00Z"
lastmod = "2015-02-26T02:12:00Z"
weight = 40082
keywords = [ "build", "wireshark" ]
aliases = [ "/questions/40082" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [nmake /f Makefile.nmake all error](/questions/40082/nmake-f-makefilenmake-all-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40082-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40082-score" class="post-score" title="current number of votes">0</div><span id="post-40082-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>after executing nmake /f Makefile.nmake setup succesfully.</p><p>When i am executing nmake /f Makefile.nmake all, i am getting an error,</p><p>'c:\Program' is not recognized as an internal or external command, operable program or batch file. NMAKE : fatal error U1077: 'C:\WINDOWS\system32\cmd.exe' : return code '0x1'</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-build" rel="tag" title="see questions tagged &#39;build&#39;">build</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '15, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/302a32bb6a6f237c61731914f3657167?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dhruv%20Gupta&#39;s gravatar image" /><p><span>Dhruv Gupta</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dhruv Gupta has no accepted answers">0%</span></p></div></div><div id="comments-container-40082" class="comments-container"><span id="40086"></span><div id="comment-40086" class="comment"><div id="post-40086-score" class="comment-score"></div><div class="comment-text"><p>You'll need to show the preceding output to give some context to have a chance of help.</p><p>Use <code>nmake -f makefile.nmake &gt; output.txt</code> and edit your question with the contents of output.txt. If it's really big, then a sensible snip of preceding info that appears to be correct would be OK.</p></div><div id="comment-40086-info" class="comment-info"><span class="comment-age">(26 Feb '15, 02:12)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-40082" class="comment-tools"></div><div class="clear"></div><div id="comment-40082-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

