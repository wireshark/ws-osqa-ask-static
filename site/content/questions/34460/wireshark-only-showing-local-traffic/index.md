+++
type = "question"
title = "[closed] Wireshark only showing local traffic"
description = '''I just installed Wireshark on my Windows. I run the capture on my wireless network. I tried to visit some websites through my phone (my phone connected to the same wireless network), but I didn&#x27;t see the website that I just visited in wireshark in the list. Any idea what do I need to setup?'''
date = "2014-07-08T01:29:00Z"
lastmod = "2014-07-08T01:29:00Z"
weight = 34460
keywords = [ "local" ]
aliases = [ "/questions/34460" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Wireshark only showing local traffic](/questions/34460/wireshark-only-showing-local-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34460-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34460-score" class="post-score" title="current number of votes">0</div><span id="post-34460-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed Wireshark on my Windows. I run the capture on my wireless network.</p><p>I tried to visit some websites through my phone (my phone connected to the same wireless network), but I didn't see the website that I just visited in wireshark in the list.</p><p>Any idea what do I need to setup?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-local" rel="tag" title="see questions tagged &#39;local&#39;">local</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jul '14, 01:29</strong></p><img src="https://secure.gravatar.com/avatar/8d1c0beac5f847d004651ed715cba014?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sandy007&#39;s gravatar image" /><p><span>Sandy007</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sandy007 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '14, 22:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-34460" class="comments-container"></div><div id="comment-tools-34460" class="comment-tools"></div><div class="clear"></div><div id="comment-34460-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Jasper 08 Jul '14, 01:35

</div>

</div>

</div>

