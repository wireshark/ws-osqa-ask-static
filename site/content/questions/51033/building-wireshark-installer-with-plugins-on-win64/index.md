+++
type = "question"
title = "Building Wireshark installer with plugins on Win64"
description = '''Hello, I am trying to build a wireshark installer using nsis. It&#x27;s a branched 2.0 version with some custom tweaks including plugins. The wireshark is building and working correctly when build from sources on Win64. When I run msbuild /m /p:Configuration=RelWithDebInfo nsis_package_prep.vcxproj every...'''
date = "2016-03-18T08:25:00Z"
lastmod = "2016-04-08T02:51:00Z"
weight = 51033
keywords = [ "packaging", "nsis", "win64", "plugin", "wireshark" ]
aliases = [ "/questions/51033" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Building Wireshark installer with plugins on Win64](/questions/51033/building-wireshark-installer-with-plugins-on-win64)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51033-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51033-score" class="post-score" title="current number of votes">0</div><span id="post-51033-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am trying to build a wireshark installer using nsis. It's a branched 2.0 version with some custom tweaks including plugins. The wireshark is building and working correctly when build from sources on Win64.</p><p>When I run msbuild /m /p:Configuration=RelWithDebInfo nsis_package_prep.vcxproj everything fnishes neatly with no errors. When I run msbuild /m /p:Configuration=RelWithDebInfo nsis_package.vcxproj I get error right after it fails to detect plugin dll.</p><p>Log:</p><pre><code>SetOutPath: &quot;$INSTDIR\plugins\2.0.2&quot;
File: &quot;C:\Development\bmakan\wsbuild64\run\RelWithDebInfo\plugins\my_plugin.dll&quot; -&gt; no files found.

The error I get is as follows:
    Error in script &quot;wireshark.nsi&quot; on line 960 -- aborting creation process
1&gt;C:\Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0\V120\Microsoft.CppCommon.targets(170,5): error MSB6006: &quot;cmd.exe&quot; exited with code 1. [C:\Development\bmakan\wsbuild64\nsis_package.vcxproj]</code></pre><p>I have vcredist_x64.exe in Wireshark-win64-libs-2.0 folder. I believe it's a correct one since I took it from installation folder. Also tried 2 others for VS 2010 and 2005 (I have 2013).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packaging" rel="tag" title="see questions tagged &#39;packaging&#39;">packaging</span> <span class="post-tag tag-link-nsis" rel="tag" title="see questions tagged &#39;nsis&#39;">nsis</span> <span class="post-tag tag-link-win64" rel="tag" title="see questions tagged &#39;win64&#39;">win64</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '16, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/a03fa5b340afab78d2e44b63e8dcf3d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aliniel&#39;s gravatar image" /><p><span>Aliniel</span><br />
<span class="score" title="30 reputation points">30</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aliniel has 2 accepted answers">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '16, 09:11</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51033" class="comments-container"><span id="51036"></span><div id="comment-51036" class="comment"><div id="post-51036-score" class="comment-score"></div><div class="comment-text"><p>So do you have the file "C:\Development\bmakan\wsbuild64\run\RelWithDebInfo\plugins\my_plugin.dll"?</p></div><div id="comment-51036-info" class="comment-info"><span class="comment-age">(18 Mar '16, 09:11)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="51335"></span><div id="comment-51335" class="comment"><div id="post-51335-score" class="comment-score"></div><div class="comment-text"><p>Apologies for a long inactivity. You are right, the file is indeed missing. Must have been checking somewhere else for it. Is this file supposed to be generated by running "msbuild /m /p:Configuration=RelWithDebInfo nsis_package_prep.vcxproj"?</p></div><div id="comment-51335-info" class="comment-info"><span class="comment-age">(01 Apr '16, 01:24)</span> <span class="comment-user userinfo">Aliniel</span></div></div><span id="51339"></span><div id="comment-51339" class="comment"><div id="post-51339-score" class="comment-score"></div><div class="comment-text"><p>No it should be generated during the "msbuild /m /p:Configuration=RelWithDebInfo Wireshark.sln" step, assuming that you added your my_plugin folder in a CMakeListsCustom.txt file in your root Wireshark source tree (see CMakeListsCustom.txt.example)</p></div><div id="comment-51339-info" class="comment-info"><span class="comment-age">(01 Apr '16, 02:18)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div><span id="51407"></span><div id="comment-51407" class="comment"><div id="post-51407-score" class="comment-score"></div><div class="comment-text"><p>I've found some bugs with my plugins. Until they're fixed, I'm putting this on hold. I'll let you know after that.</p></div><div id="comment-51407-info" class="comment-info"><span class="comment-age">(05 Apr '16, 00:22)</span> <span class="comment-user userinfo">Aliniel</span></div></div></div><div id="comment-tools-51033" class="comment-tools"></div><div class="clear"></div><div id="comment-51033-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51503"></span>

<div id="answer-container-51503" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51503-score" class="post-score" title="current number of votes">-1</div><span id="post-51503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Aliniel has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There was a problem with one of my plugins which was not working correctly. After fixing the issue the instaler build worked correctly when following the provided tutorial.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '16, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/a03fa5b340afab78d2e44b63e8dcf3d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aliniel&#39;s gravatar image" /><p><span>Aliniel</span><br />
<span class="score" title="30 reputation points">30</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aliniel has 2 accepted answers">100%</span></p></div></div><div id="comments-container-51503" class="comments-container"></div><div id="comment-tools-51503" class="comment-tools"></div><div class="clear"></div><div id="comment-51503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

