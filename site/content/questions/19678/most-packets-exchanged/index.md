+++
type = "question"
title = "Most Packets exchanged"
description = '''Which two computers exchanged most of the total packets between each other?  can anyone give me expression for these question? thanks in advance.'''
date = "2013-03-20T06:57:00Z"
lastmod = "2013-03-20T07:43:00Z"
weight = 19678
keywords = [ "wireshark1.2.5" ]
aliases = [ "/questions/19678" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Most Packets exchanged](/questions/19678/most-packets-exchanged)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19678-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19678-score" class="post-score" title="current number of votes">0</div><span id="post-19678-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Which two computers exchanged most of the total packets between each other? can anyone give me expression for these question? thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark1.2.5" rel="tag" title="see questions tagged &#39;wireshark1.2.5&#39;">wireshark1.2.5</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '13, 06:57</strong></p><img src="https://secure.gravatar.com/avatar/99550f54b17509f8f45059a141b7711a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sunith%20Kumar&#39;s gravatar image" /><p><span>Sunith Kumar</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sunith Kumar has no accepted answers">0%</span></p></div></div><div id="comments-container-19678" class="comments-container"></div><div id="comment-tools-19678" class="comment-tools"></div><div class="clear"></div><div id="comment-19678-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19679"></span>

<div id="answer-container-19679" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19679-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19679-score" class="post-score" title="current number of votes">1</div><span id="post-19679-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Sunith Kumar has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark -&gt; Statistics Menu -&gt; Conversations.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '13, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-19679" class="comments-container"><span id="19680"></span><div id="comment-19680" class="comment"><div id="post-19680-score" class="comment-score"></div><div class="comment-text"><p>thnks a lot</p></div><div id="comment-19680-info" class="comment-info"><span class="comment-age">(20 Mar '13, 07:03)</span> <span class="comment-user userinfo">Sunith Kumar</span></div></div><span id="19682"></span><div id="comment-19682" class="comment"><div id="post-19682-score" class="comment-score"></div><div class="comment-text"><p>How to check which computer system transmitted the host bytes?</p></div><div id="comment-19682-info" class="comment-info"><span class="comment-age">(20 Mar '13, 07:19)</span> <span class="comment-user userinfo">Sunith Kumar</span></div></div><span id="19683"></span><div id="comment-19683" class="comment"><div id="post-19683-score" class="comment-score"></div><div class="comment-text"><p>Statistics Menu -&gt; Endpoints -&gt; Sort by TX bytes</p></div><div id="comment-19683-info" class="comment-info"><span class="comment-age">(20 Mar '13, 07:43)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-19679" class="comment-tools"></div><div class="clear"></div><div id="comment-19679-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

