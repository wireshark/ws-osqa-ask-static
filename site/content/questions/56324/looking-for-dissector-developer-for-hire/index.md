+++
type = "question"
title = "Looking for Dissector developer for hire"
description = '''I work for a financial firm. We are looking for an experienced wireshark developer in the NY/NJ area to create dissectors for our suite of custom protocols. '''
date = "2016-10-12T10:08:00Z"
lastmod = "2016-10-13T12:27:00Z"
weight = 56324
keywords = [ "dissector" ]
aliases = [ "/questions/56324" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Looking for Dissector developer for hire](/questions/56324/looking-for-dissector-developer-for-hire)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56324-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56324-score" class="post-score" title="current number of votes">0</div><span id="post-56324-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I work for a financial firm. We are looking for an experienced wireshark developer in the NY/NJ area to create dissectors for our suite of custom protocols.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '16, 10:08</strong></p><img src="https://secure.gravatar.com/avatar/575922bd3f5d72b465eac18a045acfd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="steve_merc&#39;s gravatar image" /><p><span>steve_merc</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="steve_merc has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-56324" class="comments-container"><span id="56331"></span><div id="comment-56331" class="comment"><div id="post-56331-score" class="comment-score"></div><div class="comment-text"><p>You should post to the <a href="https://www.wireshark.org/mailman/listinfo/wireshark-dev">developer mailing list</a> for maximum exposure.</p></div><div id="comment-56331-info" class="comment-info"><span class="comment-age">(12 Oct '16, 22:10)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="56343"></span><div id="comment-56343" class="comment"><div id="post-56343-score" class="comment-score"></div><div class="comment-text"><p>Presumably these are C based dissectors for speed?</p></div><div id="comment-56343-info" class="comment-info"><span class="comment-age">(13 Oct '16, 10:45)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="56344"></span><div id="comment-56344" class="comment"><div id="post-56344-score" class="comment-score"></div><div class="comment-text"><p>Graham, Yes we would like to create C based holistic dissectors. Using our trading app structures to define the fields. This way as they are modified, we can make a change to keep the dissector in sync with our ever changing protocol spec.</p><p>One of the challenges I see is that our firm uses IP addresses not ports to define a protocol. Same ports are used on multiple applications. The servers IP that differentiates the protocol.</p><p>thanks Steve</p></div><div id="comment-56344-info" class="comment-info"><span class="comment-age">(13 Oct '16, 11:17)</span> <span class="comment-user userinfo">steve_merc</span></div></div><span id="56345"></span><div id="comment-56345" class="comment"><div id="post-56345-score" class="comment-score"></div><div class="comment-text"><p>Where/How are the protocols trading app structures defined...c/c++ headers, xml, pdf? How often do the protocols change? Are they internal protocols or possibly derivatives of exchange protocols?</p></div><div id="comment-56345-info" class="comment-info"><span class="comment-age">(13 Oct '16, 11:50)</span> <span class="comment-user userinfo">william</span></div></div><span id="56348"></span><div id="comment-56348" class="comment"><div id="post-56348-score" class="comment-score"></div><div class="comment-text"><p>William I prefer to answer those questions privately, not in the public group.</p></div><div id="comment-56348-info" class="comment-info"><span class="comment-age">(13 Oct '16, 12:27)</span> <span class="comment-user userinfo">steve_merc</span></div></div></div><div id="comment-tools-56324" class="comment-tools"></div><div class="clear"></div><div id="comment-56324-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

