+++
type = "question"
title = "Calculate RTT with Tshark"
description = '''Hi, I used different commands for measuring RTT value for a PCAP file with Tshark ... I still think that, it could be better and need some more switches... Any idea ?  Thanks.'''
date = "2015-07-20T01:33:00Z"
lastmod = "2015-07-20T01:53:00Z"
weight = 44307
keywords = [ "rtt", "pcap", "tshark" ]
aliases = [ "/questions/44307" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Calculate RTT with Tshark](/questions/44307/calculate-rtt-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44307-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44307-score" class="post-score" title="current number of votes">0</div><span id="post-44307-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I used different commands for measuring RTT value for a PCAP file with Tshark ... I still think that, it could be better and need some more switches... Any idea ? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtt" rel="tag" title="see questions tagged &#39;rtt&#39;">rtt</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '15, 01:33</strong></p><img src="https://secure.gravatar.com/avatar/0df72a5f5db5a2c33c8f966dd7262b66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alessio77&#39;s gravatar image" /><p><span>alessio77</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alessio77 has no accepted answers">0%</span></p></div></div><div id="comments-container-44307" class="comments-container"><span id="44308"></span><div id="comment-44308" class="comment"><div id="post-44308-score" class="comment-score"></div><div class="comment-text"><p>what is your question?</p></div><div id="comment-44308-info" class="comment-info"><span class="comment-age">(20 Jul '15, 01:53)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-44307" class="comment-tools"></div><div class="clear"></div><div id="comment-44307-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

