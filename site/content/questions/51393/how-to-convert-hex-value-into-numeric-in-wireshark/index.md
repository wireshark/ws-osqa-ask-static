+++
type = "question"
title = "How to convert hex value into numeric in Wireshark?"
description = '''Hi friends i can capture Mpls packet in wirshark its showing generic labels as Hex value instead of numeric value . how to convert hexa value into numeric value thanks in advance  check out below '''
date = "2016-04-04T06:35:00Z"
lastmod = "2016-04-04T06:35:00Z"
weight = 51393
keywords = [ "mpls" ]
aliases = [ "/questions/51393" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to convert hex value into numeric in Wireshark?](/questions/51393/how-to-convert-hex-value-into-numeric-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51393-score" class="post-score" title="current number of votes">0</div><span id="post-51393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi friends</p><p>i can capture Mpls packet in wirshark its showing generic labels as Hex value instead of numeric value . how to convert hexa value into numeric value</p><p>thanks in advance check out below</p><p><img src="https://osqa-ask.wireshark.org/upfiles/r2to_r3.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mpls" rel="tag" title="see questions tagged &#39;mpls&#39;">mpls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '16, 06:35</strong></p><img src="https://secure.gravatar.com/avatar/5e339028458443d78ab665d6da43d0e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Networkprofessional&#39;s gravatar image" /><p><span>Networkprofe...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Networkprofessional has no accepted answers">0%</span></p></img></div></div><div id="comments-container-51393" class="comments-container"></div><div id="comment-tools-51393" class="comment-tools"></div><div class="clear"></div><div id="comment-51393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

