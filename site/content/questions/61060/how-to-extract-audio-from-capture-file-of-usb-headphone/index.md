+++
type = "question"
title = "How to extract audio from capture file of USB Headphone"
description = '''Hi, I have a capture file of USBPCap from an USB Headphone. I&#x27;d like to know how can I extract the audio from the bytes in the file.'''
date = "2017-04-26T10:00:00Z"
lastmod = "2017-04-26T10:00:00Z"
weight = 61060
keywords = [ "extract", "audio", "pcap", "usb", "usbpcap" ]
aliases = [ "/questions/61060" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to extract audio from capture file of USB Headphone](/questions/61060/how-to-extract-audio-from-capture-file-of-usb-headphone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61060-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61060-score" class="post-score" title="current number of votes">0</div><span id="post-61060-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have a capture file of USBPCap from an USB Headphone. I'd like to know how can I extract the audio from the bytes in the file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-extract" rel="tag" title="see questions tagged &#39;extract&#39;">extract</span> <span class="post-tag tag-link-audio" rel="tag" title="see questions tagged &#39;audio&#39;">audio</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-usbpcap" rel="tag" title="see questions tagged &#39;usbpcap&#39;">usbpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '17, 10:00</strong></p><img src="https://secure.gravatar.com/avatar/302edfc8a7b65e8911eef34df33b3d85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lincecum&#39;s gravatar image" /><p><span>lincecum</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lincecum has no accepted answers">0%</span></p></div></div><div id="comments-container-61060" class="comments-container"></div><div id="comment-tools-61060" class="comment-tools"></div><div class="clear"></div><div id="comment-61060-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

