+++
type = "question"
title = "[closed] Best Wireless card for use with Wireshark and other similar programs?"
description = '''Hi I&#x27;m using the latest version of Ubuntu 14.04 and want to learn how to use Wireshark and other software that uses the wireless card, zenmap etc... What would you suggest for a wireless card? I&#x27;m on a 2 year old HP laptop. Thanks. Other specs: hp 2000 3.8 gb memory intel pentium cpu 2020m @ 2.40GHz...'''
date = "2015-01-23T15:23:00Z"
lastmod = "2015-01-24T04:10:00Z"
weight = 39374
keywords = [ "wireless", "card" ]
aliases = [ "/questions/39374" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Best Wireless card for use with Wireshark and other similar programs?](/questions/39374/best-wireless-card-for-use-with-wireshark-and-other-similar-programs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39374-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39374-score" class="post-score" title="current number of votes">0</div><span id="post-39374-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I'm using the latest version of Ubuntu 14.04 and want to learn how to use Wireshark and other software that uses the wireless card, zenmap etc... What would you suggest for a wireless card? I'm on a 2 year old HP laptop. Thanks.</p><p>Other specs: hp 2000 3.8 gb memory intel pentium cpu 2020m @ 2.40GHz x2 graphics intet ivybridge mobile os type 64 bit free disk space 48Gb</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-card" rel="tag" title="see questions tagged &#39;card&#39;">card</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '15, 15:23</strong></p><img src="https://secure.gravatar.com/avatar/09dae98a95c755f08fbc46360c3d34c3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="johnbaptiste&#39;s gravatar image" /><p><span>johnbaptiste</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="johnbaptiste has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Jan '15, 04:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-39374" class="comments-container"><span id="39379"></span><div id="comment-39379" class="comment"><div id="post-39379-score" class="comment-score"></div><div class="comment-text"><p>Exact duplicate of <a href="https://ask.wireshark.org/questions/39336/best-wireless-card-for-use-with-wireshark-and-other-similar-programs.">https://ask.wireshark.org/questions/39336/best-wireless-card-for-use-with-wireshark-and-other-similar-programs.</a></p></div><div id="comment-39379-info" class="comment-info"><span class="comment-age">(24 Jan '15, 04:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-39374" class="comment-tools"></div><div class="clear"></div><div id="comment-39374-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 24 Jan '15, 04:10

</div>

</div>

</div>

