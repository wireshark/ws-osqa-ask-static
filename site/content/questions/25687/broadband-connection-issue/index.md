+++
type = "question"
title = "Broadband Connection Issue?"
description = '''Frequently my broadband connection is getting disconnected. I have raised a fault ticket number several times, but from ISP end they are not giving any proper response. They blindly say it may be due to faulty modem. But the modem / router (NETGEAR DGN1000) which i use is a brand new one. And i dont...'''
date = "2013-10-06T18:09:00Z"
lastmod = "2013-10-07T07:52:00Z"
weight = 25687
keywords = [ "broadband" ]
aliases = [ "/questions/25687" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Broadband Connection Issue?](/questions/25687/broadband-connection-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25687-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25687-score" class="post-score" title="current number of votes">0</div><span id="post-25687-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Frequently my broadband connection is getting disconnected. I have raised a fault ticket number several times, but from ISP end they are not giving any proper response. They blindly say it may be due to faulty modem. But the modem / router (NETGEAR DGN1000) which i use is a brand new one. And i dont think that might be the problem. Any way to findout where the packet drops and the issue is in which end? either the issue is in my end or it is from ISP. Any help would be very helpful. Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadband" rel="tag" title="see questions tagged &#39;broadband&#39;">broadband</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '13, 18:09</strong></p><img src="https://secure.gravatar.com/avatar/b41802fe7f333c0b2b2b68be7da4f757?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Karthick&#39;s gravatar image" /><p><span>Karthick</span><br />
<span class="score" title="21 reputation points">21</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Karthick has no accepted answers">0%</span></p></div></div><div id="comments-container-25687" class="comments-container"></div><div id="comment-tools-25687" class="comment-tools"></div><div class="clear"></div><div id="comment-25687-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25709"></span>

<div id="answer-container-25709" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25709-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25709-score" class="post-score" title="current number of votes">1</div><span id="post-25709-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Karthick has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Frequently my broadband connection is getting disconnected.</p></blockquote><p>Well, if the connection itself is disconnected, then the problem is likely to be your ISP or your modem. In either case you need to <strong>capture the DSL communication</strong> with your provider. Unfortunately <strong>you cannot do that without special hardware</strong> (DSL sniffer), so the only option you have is to check the logs of the router or get a router with an open source OS (dd-wrt or openwrt) that gives more options for troubleshooting.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '13, 07:52</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25709" class="comments-container"></div><div id="comment-tools-25709" class="comment-tools"></div><div class="clear"></div><div id="comment-25709-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

