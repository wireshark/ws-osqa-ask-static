+++
type = "question"
title = "How to dissect CoAP packets?"
description = '''I am not sure how to load &quot;coap&quot; from dissector table and add my protocol to it. Could someone please help me?'''
date = "2014-07-29T07:49:00Z"
lastmod = "2014-07-29T07:49:00Z"
weight = 34972
keywords = [ "coap" ]
aliases = [ "/questions/34972" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to dissect CoAP packets?](/questions/34972/how-to-dissect-coap-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34972-score" class="post-score" title="current number of votes">0</div><span id="post-34972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am not sure how to load "coap" from dissector table and add my protocol to it. Could someone please help me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-coap" rel="tag" title="see questions tagged &#39;coap&#39;">coap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '14, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/cdba9ca71a2cc31a8961c51fb80edb6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abhilash&#39;s gravatar image" /><p><span>abhilash</span><br />
<span class="score" title="10 reputation points">10</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abhilash has no accepted answers">0%</span></p></div></div><div id="comments-container-34972" class="comments-container"></div><div id="comment-tools-34972" class="comment-tools"></div><div class="clear"></div><div id="comment-34972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

