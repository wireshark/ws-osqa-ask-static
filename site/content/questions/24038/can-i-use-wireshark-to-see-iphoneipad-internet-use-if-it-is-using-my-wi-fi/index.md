+++
type = "question"
title = "Can I use Wireshark to see iPhone/iPad internet use if it is using my wi-fi?"
description = '''I want to be able to see if my wife has another Facebook account and/or email accounts. I don&#x27;t necessarily need to see the passwords, just the activity and what is going on. However, she always uses her iPhone 4S or my daughter&#x27;s iPad. Please help as I recently determined she was using her primary ...'''
date = "2013-08-25T12:20:00Z"
lastmod = "2013-08-25T12:48:00Z"
weight = 24038
keywords = [ "wifi", "iphone" ]
aliases = [ "/questions/24038" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I use Wireshark to see iPhone/iPad internet use if it is using my wi-fi?](/questions/24038/can-i-use-wireshark-to-see-iphoneipad-internet-use-if-it-is-using-my-wi-fi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24038-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24038-score" class="post-score" title="current number of votes">0</div><span id="post-24038-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to be able to see if my wife has another Facebook account and/or email accounts. I don't necessarily need to see the passwords, just the activity and what is going on. However, she always uses her iPhone 4S or my daughter's iPad. Please help as I recently determined she was using her primary FB acct to rekindle an old flame and my subsequent distrust is ripping our chance at staying married apart. I just need to know that she is being honest with me. However, every application out there requires a jailbroken iPhone and hers is not. If Wireshark isn't the one to use, can you recommend another that does the task discreetly.</p><p>We have a: Windows 7 laptop Macbook Air iPhone 4S iPad mini Utilizing a 2 TB Apple Time Capsule as the wifi router</p><p>Many thanks.</p><p>Rugger</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '13, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/8d12961215b566416b049c277c6b8e64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rugger869&#39;s gravatar image" /><p><span>Rugger869</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rugger869 has no accepted answers">0%</span></p></div></div><div id="comments-container-24038" class="comments-container"></div><div id="comment-tools-24038" class="comment-tools"></div><div class="clear"></div><div id="comment-24038-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24039"></span>

<div id="answer-container-24039" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24039-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24039-score" class="post-score" title="current number of votes">0</div><span id="post-24039-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't. Facebook traffic is protected by SSL (at least the last time I tried), so you can't see login, password, or any activity. All you'll see is encrypted packets. You'd have to pull major stunts like man-in-the-middle attacks via (transparent) proxy to be able to see what is going on, and I cannot recommend to try this. It ain't legal in most countries (and "but, the NSA does it, too!" doesn't work in most cases) ;-)</p><p>The best way to go (even if may be the hardest) is probably still a good personal talk with your wife.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Aug '13, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-24039" class="comments-container"></div><div id="comment-tools-24039" class="comment-tools"></div><div class="clear"></div><div id="comment-24039-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

