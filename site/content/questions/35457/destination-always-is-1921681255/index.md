+++
type = "question"
title = "Destination always is 192.168.1.255"
description = '''On my mac, when connected to my wireless, I was able to see the source ip/host and the destination ip, or host. the outgoing sites were always reported via ip or host. All was well. Now recently, i am seeing all the source ips on my network showing destination as 192.168.1.255. When I test it by hav...'''
date = "2014-08-13T05:46:00Z"
lastmod = "2014-08-13T05:46:00Z"
weight = 35457
keywords = [ "192.168.1.255" ]
aliases = [ "/questions/35457" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Destination always is 192.168.1.255](/questions/35457/destination-always-is-1921681255)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35457-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35457-score" class="post-score" title="current number of votes">0</div><span id="post-35457-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On my mac, when connected to my wireless, I was able to see the source ip/host and the destination ip, or host. the outgoing sites were always reported via ip or host. All was well.</p><p>Now recently, i am seeing all the source ips on my network showing destination as 192.168.1.255. When I test it by having one wireless client go to Facebook for example, it will show the 192.168.1.255 instead of Facebook host etc.</p><p>what did I do? maybe playing with settings in wireshark, how to reset to default?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-192.168.1.255" rel="tag" title="see questions tagged &#39;192.168.1.255&#39;">192.168.1.255</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '14, 05:46</strong></p><img src="https://secure.gravatar.com/avatar/14e8ff606c6f737f476604034a184baa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="catcurio&#39;s gravatar image" /><p><span>catcurio</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="catcurio has no accepted answers">0%</span></p></div></div><div id="comments-container-35457" class="comments-container"></div><div id="comment-tools-35457" class="comment-tools"></div><div class="clear"></div><div id="comment-35457-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

