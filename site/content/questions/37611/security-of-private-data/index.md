+++
type = "question"
title = "Security of private data"
description = '''hello I just collect traces on my laptop while taking wireshark trace, I sent emails with confidential content, can i have access to these confidential data on the tracks? I spent calls through a softphone, can i have access to voice data (listenning my call) on the tracks? In summary, how to have t...'''
date = "2014-11-06T02:42:00Z"
lastmod = "2014-11-07T22:56:00Z"
weight = 37611
keywords = [ "secu" ]
aliases = [ "/questions/37611" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Security of private data](/questions/37611/security-of-private-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37611-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37611-score" class="post-score" title="current number of votes">0</div><span id="post-37611-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello</p><p>I just collect traces on my laptop while taking wireshark trace, I sent emails with confidential content, can i have access to these confidential data on the tracks?</p><p>I spent calls through a softphone, can i have access to voice data (listenning my call) on the tracks?</p><p>In summary, how to have the data on track wireshark?</p><p>I use version 1.12.1</p><p>thank you</p><p>kind regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-secu" rel="tag" title="see questions tagged &#39;secu&#39;">secu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '14, 02:42</strong></p><img src="https://secure.gravatar.com/avatar/52fd67dc8b633771937d1720443d49c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DEEE&#39;s gravatar image" /><p><span>DEEE</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DEEE has no accepted answers">0%</span></p></div></div><div id="comments-container-37611" class="comments-container"></div><div id="comment-tools-37611" class="comment-tools"></div><div class="clear"></div><div id="comment-37611-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37685"></span>

<div id="answer-container-37685" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37685-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37685-score" class="post-score" title="current number of votes">0</div><span id="post-37685-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The majority of traffic today is encrypted using Transport Layer Security <a href="http://en.wikipedia.org/wiki/Transport_Layer_Security">TLS</a><br />
The whole purpose of this is that the private data cannot be read by anyone sniffing your traffic. So without access to the <a href="http://en.wikipedia.org/wiki/Public-key_cryptography">private key</a> you cannot <a href="https://ask.wireshark.org/tags/decrypt/">decrypt</a> the data in a secured connection.</p><p>What do you mean with 'How to have the data on track wireshark?' ?</p><p>Regards Matthias</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '14, 22:56</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span> </br></p></div></div><div id="comments-container-37685" class="comments-container"></div><div id="comment-tools-37685" class="comment-tools"></div><div class="clear"></div><div id="comment-37685-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

