+++
type = "question"
title = "WireShark for VXWorks"
description = '''May I know is there any WireShark for VXWorks version, I&#x27;m using VXWorks 5.3.1 here. Thanks in advanced!'''
date = "2016-11-24T18:34:00Z"
lastmod = "2016-11-25T11:27:00Z"
weight = 57622
keywords = [ "vxworks", "wireshark" ]
aliases = [ "/questions/57622" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark for VXWorks](/questions/57622/wireshark-for-vxworks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57622-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57622-score" class="post-score" title="current number of votes">0</div><span id="post-57622-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>May I know is there any WireShark for VXWorks version, I'm using VXWorks 5.3.1 here. Thanks in advanced!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vxworks" rel="tag" title="see questions tagged &#39;vxworks&#39;">vxworks</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '16, 18:34</strong></p><img src="https://secure.gravatar.com/avatar/590a2d0586df28020c39e158ed08df40?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="1sun3&#39;s gravatar image" /><p><span>1sun3</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="1sun3 has no accepted answers">0%</span></p></div></div><div id="comments-container-57622" class="comments-container"></div><div id="comment-tools-57622" class="comment-tools"></div><div class="clear"></div><div id="comment-57622-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57639"></span>

<div id="answer-container-57639" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57639-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57639-score" class="post-score" title="current number of votes">0</div><span id="post-57639-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, there is not. It only runs on various forms of UN*X and on Windows.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '16, 11:27</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-57639" class="comments-container"></div><div id="comment-tools-57639" class="comment-tools"></div><div class="clear"></div><div id="comment-57639-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

