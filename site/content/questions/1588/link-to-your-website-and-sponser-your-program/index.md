+++
type = "question"
title = "Link to your website and sponser your program"
description = '''This is not a tech question about your amazing program called Wireshark. I was wondering if I could post a reference link to your site? I really love your program and I have started using a tutorial information on maintaining a server and this program has helped with a lot of issues I have been runn...'''
date = "2011-01-02T13:55:00Z"
lastmod = "2011-01-02T18:18:00Z"
weight = 1588
keywords = [ "website", "to", "link" ]
aliases = [ "/questions/1588" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Link to your website and sponser your program](/questions/1588/link-to-your-website-and-sponser-your-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1588-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1588-score" class="post-score" title="current number of votes">0</div><span id="post-1588-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This is not a tech question about your amazing program called Wireshark.</p><p>I was wondering if I could post a reference link to your site? I really love your program and I have started using a tutorial information on maintaining a server and this program has helped with a lot of issues I have been running into as a diagnostic tool.</p><p>Just wanted to get permission before I posted a link.</p><p>Oh, the website is http://www.magetech.net</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website" rel="tag" title="see questions tagged &#39;website&#39;">website</span> <span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-link" rel="tag" title="see questions tagged &#39;link&#39;">link</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jan '11, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/43876d434cc0647e4fc1a04456c18b9a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tbay007&#39;s gravatar image" /><p><span>tbay007</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tbay007 has no accepted answers">0%</span></p></div></div><div id="comments-container-1588" class="comments-container"></div><div id="comment-tools-1588" class="comment-tools"></div><div class="clear"></div><div id="comment-1588-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1589"></span>

<div id="answer-container-1589" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1589-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1589-score" class="post-score" title="current number of votes">0</div><span id="post-1589-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Your website seems to be a great way for you to journal your learnings. I'm sure almost no one asks permission to link to a website, and I am sure that none is required. Most of us here are keen to see more people use and understand the usefulness of Wireshark. (And of course I can only speak for myself!)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jan '11, 15:16</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-1589" class="comments-container"></div><div id="comment-tools-1589" class="comment-tools"></div><div class="clear"></div><div id="comment-1589-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1592"></span>

<div id="answer-container-1592" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1592-score" class="post-score" title="current number of votes">0</div><span id="post-1592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I am of the same mindset as martyvis. If a web site is public, I see no reason you cannot link to it. There is really no downside to that, at least in my opinion. I have a personal blog and occasionally post links to other related sites of interest. I don't worry too much about getting the site owner's permissions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jan '11, 18:18</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span></p></div></div><div id="comments-container-1592" class="comments-container"></div><div id="comment-tools-1592" class="comment-tools"></div><div class="clear"></div><div id="comment-1592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

