+++
type = "question"
title = "NPF Driver not working; says service name is invalid when I try to start in cmd!"
description = '''&quot;NPF Driver not working&quot; is what I get every time I start up wireshark and I went to the forums and found what worked for most people; typing in commands to start it up, but when I do it says &quot;service name is invalid&quot; and I&#x27;m thinking something didn&#x27;t install right or something... Help?'''
date = "2012-08-22T11:28:00Z"
lastmod = "2012-08-28T21:01:00Z"
weight = 13823
keywords = [ "npf", "driver" ]
aliases = [ "/questions/13823" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [NPF Driver not working; says service name is invalid when I try to start in cmd!](/questions/13823/npf-driver-not-working-says-service-name-is-invalid-when-i-try-to-start-in-cmd)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13823-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13823-score" class="post-score" title="current number of votes">0</div><span id="post-13823-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>"NPF Driver not working" is what I get every time I start up wireshark and I went to the forums and found what worked for most people; typing in commands to start it up, but when I do it says "service name is invalid" and I'm thinking something didn't install right or something... Help?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-npf" rel="tag" title="see questions tagged &#39;npf&#39;">npf</span> <span class="post-tag tag-link-driver" rel="tag" title="see questions tagged &#39;driver&#39;">driver</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '12, 11:28</strong></p><img src="https://secure.gravatar.com/avatar/2ba05cf571bfc96034883bbec31983fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Desgax&#39;s gravatar image" /><p><span>Desgax</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Desgax has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Aug '12, 21:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-13823" class="comments-container"></div><div id="comment-tools-13823" class="comment-tools"></div><div class="clear"></div><div id="comment-13823-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13948"></span>

<div id="answer-container-13948" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13948-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13948-score" class="post-score" title="current number of votes">0</div><span id="post-13948-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You don't mention what version of Windows you're running, but maybe the answers provided to <a href="http://ask.wireshark.org/questions/1281/npf-driver-problem-in-windows-7">question 1281</a> will help you, as it seems to be related. If not, try searching for answers to similar questions tagged with <a href="http://ask.wireshark.org/tags/npf/">npf</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Aug '12, 21:01</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-13948" class="comments-container"></div><div id="comment-tools-13948" class="comment-tools"></div><div class="clear"></div><div id="comment-13948-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

