+++
type = "question"
title = "Malformed SSH Packets"
description = '''I&#x27;m getting malformed SSH packets in my pcap files. These happen at what appears to be the Diffie-Hellman key exchange. I&#x27;m using SCP to transfer data from a hp-ux 11.23 server to a Red Hat server. 98% of the time I have no problem, but the other 2% of the time I have no idea. Anyone can give me som...'''
date = "2015-02-17T08:26:00Z"
lastmod = "2015-02-18T02:39:00Z"
weight = 39916
keywords = [ "ssh", "malformed", "diffie-hellman" ]
aliases = [ "/questions/39916" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed SSH Packets](/questions/39916/malformed-ssh-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39916-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39916-score" class="post-score" title="current number of votes">0</div><span id="post-39916-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm getting malformed SSH packets in my pcap files. These happen at what appears to be the Diffie-Hellman key exchange. I'm using SCP to transfer data from a hp-ux 11.23 server to a Red Hat server. 98% of the time I have no problem, but the other 2% of the time I have no idea. Anyone can give me some input on this it would be appreciated. Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssh" rel="tag" title="see questions tagged &#39;ssh&#39;">ssh</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span> <span class="post-tag tag-link-diffie-hellman" rel="tag" title="see questions tagged &#39;diffie-hellman&#39;">diffie-hellman</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '15, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/82fd301e64f9722812b4a793f6c0e192?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mike%20Walterman&#39;s gravatar image" /><p><span>Mike Walterman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mike Walterman has no accepted answers">0%</span></p></div></div><div id="comments-container-39916" class="comments-container"><span id="39924"></span><div id="comment-39924" class="comment"><div id="post-39924-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>? What is your OS and which version and what is your Wireshark version?</p></div><div id="comment-39924-info" class="comment-info"><span class="comment-age">(18 Feb '15, 02:39)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-39916" class="comment-tools"></div><div class="clear"></div><div id="comment-39916-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

