+++
type = "question"
title = "Unable to receive multicast datagram sent from Arduino"
description = '''hi, I have the same problem as this question but the difference is; the mac address of the sending data is not null but I&#x27;m not sure it is true. Here is the screenshot of package; https://dyp.im/delete/MUfY6HOPyFjXufXi Does the problem occur , though full of mac address? Osman'''
date = "2014-11-19T01:59:00Z"
lastmod = "2014-11-20T13:10:00Z"
weight = 37961
keywords = [ "udp", "multicast" ]
aliases = [ "/questions/37961" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to receive multicast datagram sent from Arduino](/questions/37961/unable-to-receive-multicast-datagram-sent-from-arduino)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37961-score" class="post-score" title="current number of votes">0</div><span id="post-37961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, I have the same problem as <a href="https://ask.wireshark.org/questions/26732/incoming-udp-only-works-while-capturing-packages-with-wireshark">this</a> question but the difference is; the mac address of the sending data is not null but I'm not sure it is true. Here is the screenshot of package; <a href="https://dyp.im/delete/MUfY6HOPyFjXufXi">https://dyp.im/delete/MUfY6HOPyFjXufXi</a></p><p>Does the problem occur , though full of mac address?</p><p>Osman</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-multicast" rel="tag" title="see questions tagged &#39;multicast&#39;">multicast</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '14, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/d603b86aa23bb164b61aca6e1e707563?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="otto33&#39;s gravatar image" /><p><span>otto33</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="otto33 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>19 Nov '14, 02:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-37961" class="comments-container"></div><div id="comment-tools-37961" class="comment-tools"></div><div class="clear"></div><div id="comment-37961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37962"></span>

<div id="answer-container-37962" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37962-score" class="post-score" title="current number of votes">0</div><span id="post-37962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The destination mac address is an <a href="http://en.wikipedia.org/wiki/IP_multicast">IPv4 multicast</a> one. The receiving host has to be a member of the multicast group by joining it to actually receive the datagrams. The group is usually handled by a switch.<br />
</p><p>Putting the receiving host into promiscuous mode by starting a Wireshark capture allows it to receive the packet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Nov '14, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span> </br></p></div></div><div id="comments-container-37962" class="comments-container"><span id="38030"></span><div id="comment-38030" class="comment"><div id="post-38030-score" class="comment-score"></div><div class="comment-text"><p>thanks for reply, i dont want the software i use for receivng data should not depend on wireshark and i know that my encoder couldnt fill the mac adress correctly. i just want to know that the receiving OS drops the frame or not?</p></div><div id="comment-38030-info" class="comment-info"><span class="comment-age">(20 Nov '14, 13:10)</span> <span class="comment-user userinfo">otto33</span></div></div></div><div id="comment-tools-37962" class="comment-tools"></div><div class="clear"></div><div id="comment-37962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

