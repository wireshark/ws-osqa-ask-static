+++
type = "question"
title = "Can wireshark decode sy protocol ?"
description = '''Can wireshark decode sy protocol ? Especially Ericsson Sy version.'''
date = "2013-03-05T12:50:00Z"
lastmod = "2013-03-05T14:31:00Z"
weight = 19180
keywords = [ "sy", "protocol" ]
aliases = [ "/questions/19180" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can wireshark decode sy protocol ?](/questions/19180/can-wireshark-decode-sy-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19180-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19180-score" class="post-score" title="current number of votes">0</div><span id="post-19180-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can wireshark decode sy protocol ? Especially Ericsson Sy version.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sy" rel="tag" title="see questions tagged &#39;sy&#39;">sy</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '13, 12:50</strong></p><img src="https://secure.gravatar.com/avatar/b42e8f49bcb0b4da5dc1950f1dec38af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Stl&#39;s gravatar image" /><p><span>Stl</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Stl has no accepted answers">0%</span></p></div></div><div id="comments-container-19180" class="comments-container"></div><div id="comment-tools-19180" class="comment-tools"></div><div class="clear"></div><div id="comment-19180-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19188"></span>

<div id="answer-container-19188" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19188-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19188-score" class="post-score" title="current number of votes">0</div><span id="post-19188-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark understands at least some of both Sy and Ericsson Sy (I'm assuming you're talking about the Diameter interface Sy here). If you find the support lacking, please file bugs with sample captures so the support can be improved.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '13, 14:31</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-19188" class="comments-container"></div><div id="comment-tools-19188" class="comment-tools"></div><div class="clear"></div><div id="comment-19188-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

