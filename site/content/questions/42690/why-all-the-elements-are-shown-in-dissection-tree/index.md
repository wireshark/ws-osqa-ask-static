+++
type = "question"
title = "Why all the elements are shown in dissection tree?"
description = '''I have developed my own plugin in wireshark using lua. In dissection tree when we click at first time it is showing all the elements of the tree (till in the depth root item, child, grand child). I want that at first click it should see only level 1 (root level) information only. I have seen below q...'''
date = "2015-05-26T22:25:00Z"
lastmod = "2015-06-27T19:22:00Z"
weight = 42690
keywords = [ "lua", "dissection", "tree", "wireshark" ]
aliases = [ "/questions/42690" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why all the elements are shown in dissection tree?](/questions/42690/why-all-the-elements-are-shown-in-dissection-tree)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42690-score" class="post-score" title="current number of votes">0</div><span id="post-42690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have developed my own plugin in wireshark using lua. In dissection tree when we click at first time it is showing all the elements of the tree (till in the depth root item, child, grand child). I want that at first click it should see only level 1 (root level) information only.</p><p>I have seen below questions and referred.</p><ol><li><a href="https://ask.wireshark.org/questions/42404/lua-dissector-tree-collapse">https://ask.wireshark.org/questions/42404/lua-dissector-tree-collapse</a></li><li><a href="https://ask.wireshark.org/questions/31356/how-to-get-all-tree-items-collapsed-as-default-in-gtk-version">https://ask.wireshark.org/questions/31356/how-to-get-all-tree-items-collapsed-as-default-in-gtk-version</a></li></ol><p>I tried the solution which is mentioned by <span>@Hadriel</span> in second question (related to ett value) but it isnot applied. can anyone look into this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissection" rel="tag" title="see questions tagged &#39;dissection&#39;">dissection</span> <span class="post-tag tag-link-tree" rel="tag" title="see questions tagged &#39;tree&#39;">tree</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 May '15, 22:25</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 May '15, 22:26</strong> </span></p></div></div><div id="comments-container-42690" class="comments-container"></div><div id="comment-tools-42690" class="comment-tools"></div><div class="clear"></div><div id="comment-42690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43619"></span>

<div id="answer-container-43619" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43619-score" class="post-score" title="current number of votes">1</div><span id="post-43619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I answered this <a href="https://ask.wireshark.org/questions/42404/lua-dissector-tree-collapse">in another post</a>, assuming your script is similar.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '15, 19:22</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div></div><div id="comments-container-43619" class="comments-container"></div><div id="comment-tools-43619" class="comment-tools"></div><div class="clear"></div><div id="comment-43619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

