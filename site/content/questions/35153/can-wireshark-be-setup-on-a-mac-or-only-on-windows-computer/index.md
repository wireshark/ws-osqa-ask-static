+++
type = "question"
title = "Can Wireshark be setup on a Mac or only on Windows computer?"
description = '''We have a Mac environment 10 Macs with only 1 PC for payroll operations. Can my Meraki MX60 send captured packets to Wireshark running on a Mac OS X Snow Leopard / Lion or must it only be a Windows computer?'''
date = "2014-08-04T05:43:00Z"
lastmod = "2014-08-04T06:55:00Z"
weight = 35153
keywords = [ "mac", "server" ]
aliases = [ "/questions/35153" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark be setup on a Mac or only on Windows computer?](/questions/35153/can-wireshark-be-setup-on-a-mac-or-only-on-windows-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35153-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35153-score" class="post-score" title="current number of votes">0</div><span id="post-35153-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have a Mac environment 10 Macs with only 1 PC for payroll operations. Can my Meraki MX60 send captured packets to Wireshark running on a Mac OS X Snow Leopard / Lion or must it only be a Windows computer?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Aug '14, 05:43</strong></p><img src="https://secure.gravatar.com/avatar/fbf7dc105b6e87328d773f6c577054e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="txtornados&#39;s gravatar image" /><p><span>txtornados</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="txtornados has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Aug '14, 05:43</strong> </span></p></div></div><div id="comments-container-35153" class="comments-container"></div><div id="comment-tools-35153" class="comment-tools"></div><div class="clear"></div><div id="comment-35153-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35159"></span>

<div id="answer-container-35159" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35159-score" class="post-score" title="current number of votes">0</div><span id="post-35159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark works on a lot of systems, including:</p><ul><li>Windows</li><li><strong>Apple / Mac OS X</strong></li><li>Linux(es)</li><li>*BSD</li><li>several other Unix like OSes</li></ul><p>See here: <a href="https://www.wireshark.org/download.html">https://www.wireshark.org/download.html</a></p><p>So, yes you can run it on Mac OS X.</p><blockquote><p>Can my Meraki MX60 send captured packets to Wireshark</p></blockquote><p>I can't answer that question, as I don't know how the MX60 would do that. Please ask your local Meraki guru.</p><p>If the MX60 is able to forward traffic in a format that Wireshark knows, it should work.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Aug '14, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Aug '14, 07:13</strong> </span></p></div></div><div id="comments-container-35159" class="comments-container"></div><div id="comment-tools-35159" class="comment-tools"></div><div class="clear"></div><div id="comment-35159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

