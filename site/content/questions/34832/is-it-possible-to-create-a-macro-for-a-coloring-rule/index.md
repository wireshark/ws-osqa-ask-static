+++
type = "question"
title = "Is it possible to create a macro for a coloring rule?"
description = '''I regularly apply a color rule for each A &amp;gt; B conversation, so i can see which host is the source for each packet. This is really useful when dealing with two party communication ( A &amp;lt;&amp;gt; B ) but recently i found it pretty much indispensable while dealing with a server in the middle of a thre...'''
date = "2014-07-22T09:10:00Z"
lastmod = "2014-07-22T09:10:00Z"
weight = 34832
keywords = [ "macro", "coloring" ]
aliases = [ "/questions/34832" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to create a macro for a coloring rule?](/questions/34832/is-it-possible-to-create-a-macro-for-a-coloring-rule)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34832-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34832-score" class="post-score" title="current number of votes">0</div><span id="post-34832-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I regularly apply a color rule for each A &gt; B conversation, so i can see which host is the source for each packet. This is really useful when dealing with two party communication ( A &lt;&gt; B ) but recently i found it pretty much indispensable while dealing with a server in the middle of a three-way conversation ( A &lt;&gt; B &lt;&gt; C ).</p><p>It would make my life a lot easier if there was a way of creating a macro for assigning coloring rules like this, I cant imagine you can create a macro of actions taken in the conversation list so maybe it could work instead by selecting a host, tagging its ip.src as a rule and applying a color.</p><p>This may not even be possible, but its something i do so regularly it would be awesome if i could speed the process up.</p><p>Is this possible within wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macro" rel="tag" title="see questions tagged &#39;macro&#39;">macro</span> <span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '14, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/a78a6780cf81ac79e2e91d3d4e860709?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AlexH&#39;s gravatar image" /><p><span>AlexH</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AlexH has no accepted answers">0%</span></p></div></div><div id="comments-container-34832" class="comments-container"></div><div id="comment-tools-34832" class="comment-tools"></div><div class="clear"></div><div id="comment-34832-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

