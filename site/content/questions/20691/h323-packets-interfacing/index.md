+++
type = "question"
title = "H.323 packets interfacing"
description = '''Dear, Kindly guide me, I want to capture video streaming packets (H.323). I have installed Wireshark 1.8.6. We connected videocon hardware to a LAN switch and computer with wireshark installed to same LAN switch. The remote hardware is connected to the same LAN switch through WLAN. We are unable to ...'''
date = "2013-04-21T23:54:00Z"
lastmod = "2013-04-22T06:38:00Z"
weight = 20691
keywords = [ "rtp", "h225", "h323", "h245" ]
aliases = [ "/questions/20691" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [H.323 packets interfacing](/questions/20691/h323-packets-interfacing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20691-score" class="post-score" title="current number of votes">0</div><span id="post-20691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear, Kindly guide me, I want to capture video streaming packets (H.323). I have installed Wireshark 1.8.6. We connected videocon hardware to a LAN switch and computer with wireshark installed to same LAN switch. The remote hardware is connected to the same LAN switch through WLAN. We are unable to get packets H.323, H225 or H245 and even RTP. Pl guide so that we can can these, pl.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-h225" rel="tag" title="see questions tagged &#39;h225&#39;">h225</span> <span class="post-tag tag-link-h323" rel="tag" title="see questions tagged &#39;h323&#39;">h323</span> <span class="post-tag tag-link-h245" rel="tag" title="see questions tagged &#39;h245&#39;">h245</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Apr '13, 23:54</strong></p><img src="https://secure.gravatar.com/avatar/e2392542a3665126fe34508194015943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yousaf%20Khan&#39;s gravatar image" /><p><span>Yousaf Khan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yousaf Khan has no accepted answers">0%</span></p></div></div><div id="comments-container-20691" class="comments-container"><span id="20702"></span><div id="comment-20702" class="comment"><div id="post-20702-score" class="comment-score"></div><div class="comment-text"><blockquote><p>connected to the <strong>same LAN</strong> switch through <strong>WLAN</strong>.</p></blockquote><p>how does that work?</p><p>Is your setup like this?</p><pre><code>videcon -- LAN switch -- Wlan AP -- ::: thin air ::: -- remote client
                |
                |
              Wireshark</code></pre></div><div id="comment-20702-info" class="comment-info"><span class="comment-age">(22 Apr '13, 06:38)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20691" class="comment-tools"></div><div class="clear"></div><div id="comment-20691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

