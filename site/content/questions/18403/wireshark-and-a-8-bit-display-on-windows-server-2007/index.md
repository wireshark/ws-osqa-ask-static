+++
type = "question"
title = "Wireshark and a 8-bit display on Windows server 2007?"
description = '''I have installed Wireshark on a windows server 2007 SP2 but the Wireshark UI doesnt display correctly. The display settings are 8-bit with no option to change. Is wireshark not compatible with 8-bit display settings and/or is there a fix for this problem? Thanks in advance -Karl'''
date = "2013-02-07T07:03:00Z"
lastmod = "2013-02-07T07:34:00Z"
weight = 18403
keywords = [ "color", "colour", "8-bit", "display", "monitor" ]
aliases = [ "/questions/18403" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and a 8-bit display on Windows server 2007?](/questions/18403/wireshark-and-a-8-bit-display-on-windows-server-2007)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18403-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18403-score" class="post-score" title="current number of votes">0</div><span id="post-18403-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have installed Wireshark on a windows server 2007 SP2 but the Wireshark UI doesnt display correctly. The display settings are 8-bit with no option to change. Is wireshark not compatible with 8-bit display settings and/or is there a fix for this problem?</p><p>Thanks in advance</p><p>-Karl</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span> <span class="post-tag tag-link-colour" rel="tag" title="see questions tagged &#39;colour&#39;">colour</span> <span class="post-tag tag-link-8-bit" rel="tag" title="see questions tagged &#39;8-bit&#39;">8-bit</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '13, 07:03</strong></p><img src="https://secure.gravatar.com/avatar/c2d645474134710e189dc348676a7730?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Karhoo&#39;s gravatar image" /><p><span>Karhoo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Karhoo has no accepted answers">0%</span></p></div></div><div id="comments-container-18403" class="comments-container"></div><div id="comment-tools-18403" class="comment-tools"></div><div class="clear"></div><div id="comment-18403-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18405"></span>

<div id="answer-container-18405" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18405-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18405-score" class="post-score" title="current number of votes">1</div><span id="post-18405-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChIntroPlatforms.html">System Requirements</a>: 800x600 (1280x1024 or higher recommended) resolution with at least 65536 (16bit) colors (256 colors should work if Wireshark is installed with the "legacy GTK1" selection of the Wireshark 1.0.x releases).</p><p>So if you must use a 256 color display you'll have to use a very old copy of Wireshark and use the GTK1 install option.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '13, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-18405" class="comments-container"></div><div id="comment-tools-18405" class="comment-tools"></div><div class="clear"></div><div id="comment-18405-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

