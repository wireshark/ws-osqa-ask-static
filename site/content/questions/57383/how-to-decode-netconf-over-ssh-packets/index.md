+++
type = "question"
title = "How to decode netconf over ssh packets"
description = '''Hi, I am running Netconf server and client applications over SSH. Could you please explain the steps to capture and decode the messages exchanged between client and server. Regards Shri Prakash'''
date = "2016-11-14T17:44:00Z"
lastmod = "2016-11-15T02:24:00Z"
weight = 57383
keywords = [ "netconf" ]
aliases = [ "/questions/57383" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode netconf over ssh packets](/questions/57383/how-to-decode-netconf-over-ssh-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57383-score" class="post-score" title="current number of votes">0</div><span id="post-57383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am running Netconf server and client applications over SSH. Could you please explain the steps to capture and decode the messages exchanged between client and server.</p><p>Regards Shri Prakash</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-netconf" rel="tag" title="see questions tagged &#39;netconf&#39;">netconf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '16, 17:44</strong></p><img src="https://secure.gravatar.com/avatar/f35364374c526726c9d5acc7a4bd24e2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shri&#39;s gravatar image" /><p><span>shri</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shri has no accepted answers">0%</span></p></div></div><div id="comments-container-57383" class="comments-container"></div><div id="comment-tools-57383" class="comment-tools"></div><div class="clear"></div><div id="comment-57383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57387"></span>

<div id="answer-container-57387" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57387-score" class="post-score" title="current number of votes">0</div><span id="post-57387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Since Wireshark isn't capable to look inside SSH sessions it can't dissect the netconf session either.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Nov '16, 02:24</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-57387" class="comments-container"></div><div id="comment-tools-57387" class="comment-tools"></div><div class="clear"></div><div id="comment-57387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

