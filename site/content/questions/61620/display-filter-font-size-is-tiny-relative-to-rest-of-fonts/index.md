+++
type = "question"
title = "Display filter font size is tiny relative to rest of fonts"
description = '''Also some of the dialog fonts are less than half the size of the other fonts. I&#x27;m on Windows 10, latest version, but this has happened with previous versions of Wireshark and Windows. I&#x27;ve tried adjusting the font preferences, but it doesn&#x27;t affect the display filter - I can barely read it. I think ...'''
date = "2017-05-25T06:12:00Z"
lastmod = "2017-05-25T11:15:00Z"
weight = 61620
keywords = [ "gui" ]
aliases = [ "/questions/61620" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Display filter font size is tiny relative to rest of fonts](/questions/61620/display-filter-font-size-is-tiny-relative-to-rest-of-fonts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61620-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61620-score" class="post-score" title="current number of votes">0</div><span id="post-61620-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Also some of the dialog fonts are less than half the size of the other fonts. I'm on Windows 10, latest version, but this has happened with previous versions of Wireshark and Windows. I've tried adjusting the font preferences, but it doesn't affect the display filter - I can barely read it. I think it happens because I RDP my box from my laptop which has higher resolution - when I log back in locally my screen readjusts, but Wireshark stays funky. Please let me know of a way to adjust or reset this - Wireshark is extremely hard to use like this. Thanks!</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_t7olnqJ.PNG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '17, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/8232c06208d3d97adcbb6916c9217051?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RickX&#39;s gravatar image" /><p><span>RickX</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RickX has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61620" class="comments-container"><span id="61621"></span><div id="comment-61621" class="comment"><div id="post-61621-score" class="comment-score"></div><div class="comment-text"><p>What font do you have selected? Can you show what is in the Edit -&gt; Preferences -&gt; Advanced dialog when searching for "font"?</p></div><div id="comment-61621-info" class="comment-info"><span class="comment-age">(25 May '17, 06:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="61630"></span><div id="comment-61630" class="comment"><div id="post-61630-score" class="comment-score"></div><div class="comment-text"><p>I think the default was consolas, but I tried a number of fixed and variable fonts. All would update things like the packet info, but nothing seemed to touch the display filter bar.</p></div><div id="comment-61630-info" class="comment-info"><span class="comment-age">(25 May '17, 11:15)</span> <span class="comment-user userinfo">RickX</span></div></div></div><div id="comment-tools-61620" class="comment-tools"></div><div class="clear"></div><div id="comment-61620-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

