+++
type = "question"
title = "Wireshark not capturing traffic on 64 bit linux (not capturing RLS and OHMPI Requets)"
description = '''Wireshark is capturing the traffic well on a 32 bit linux machine. Oracle service bus is installed on 32 bit linux machine and its capturing all the traffic well. But not in 64 bit.'''
date = "2012-08-01T04:06:00Z"
lastmod = "2012-08-01T08:11:00Z"
weight = 13229
keywords = [ "12345" ]
aliases = [ "/questions/13229" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not capturing traffic on 64 bit linux (not capturing RLS and OHMPI Requets)](/questions/13229/wireshark-not-capturing-traffic-on-64-bit-linux-not-capturing-rls-and-ohmpi-requets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13229-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13229-score" class="post-score" title="current number of votes">0</div><span id="post-13229-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark is capturing the traffic well on a 32 bit linux machine. Oracle service bus is installed on 32 bit linux machine and its capturing all the traffic well. But not in 64 bit.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-12345" rel="tag" title="see questions tagged &#39;12345&#39;">12345</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '12, 04:06</strong></p><img src="https://secure.gravatar.com/avatar/d2b61556dda30d02213bedb910b92e4b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prasobh&#39;s gravatar image" /><p><span>prasobh</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prasobh has no accepted answers">0%</span></p></div></div><div id="comments-container-13229" class="comments-container"><span id="13253"></span><div id="comment-13253" class="comment"><div id="post-13253-score" class="comment-score"></div><div class="comment-text"><p>what is your</p><ul><li>OS / OS version of the system running Wireshark</li><li>Wireshark version (wireshark -v)</li></ul><p>Are there</p><ul><li>any error messages</li><li>any packets at all (broadcast, etc.) when you capture on the 64Bit OS</li></ul><p>Regards<br />
Kurt</p></div><div id="comment-13253-info" class="comment-info"><span class="comment-age">(01 Aug '12, 08:11)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-13229" class="comment-tools"></div><div class="clear"></div><div id="comment-13229-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

