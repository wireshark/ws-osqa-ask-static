+++
type = "question"
title = "Where is data held/stored?"
description = '''Please confirm if this tool stores data that it has access to when providing services? If so, where is it held and can a user ask for it to be deleted. If data is deleted, can Wireshark provided confirmation of this? '''
date = "2017-02-09T08:54:00Z"
lastmod = "2017-02-09T08:58:00Z"
weight = 59297
keywords = [ "data" ]
aliases = [ "/questions/59297" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where is data held/stored?](/questions/59297/where-is-data-heldstored)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59297-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59297-score" class="post-score" title="current number of votes">0</div><span id="post-59297-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please confirm if this tool stores data that it has access to when providing services? If so, where is it held and can a user ask for it to be deleted. If data is deleted, can Wireshark provided confirmation of this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '17, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/9e93662fea64d8416dd20383767a6b01?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HS7&#39;s gravatar image" /><p><span>HS7</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HS7 has no accepted answers">0%</span></p></div></div><div id="comments-container-59297" class="comments-container"></div><div id="comment-tools-59297" class="comment-tools"></div><div class="clear"></div><div id="comment-59297-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59298"></span>

<div id="answer-container-59298" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59298-score" class="post-score" title="current number of votes">0</div><span id="post-59298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark stores captured network packets in files. There are temporary files in the system/user temp folder, and in at any location the user saves the final files to. So as long as the user has access to the file system she/he can delete them of course.</p><p>If files are deleted on the file system Wireshark won't notice (unless tried to open again).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '17, 08:58</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-59298" class="comments-container"></div><div id="comment-tools-59298" class="comment-tools"></div><div class="clear"></div><div id="comment-59298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

