+++
type = "question"
title = "[closed] Src And dst address is Same"
description = '''Please help me what is this  src and dst address is all zero&#x27;s why wireshark showing this?'''
date = "2017-02-12T02:59:00Z"
lastmod = "2017-02-12T09:48:00Z"
weight = 59351
keywords = [ "all_zeros" ]
aliases = [ "/questions/59351" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Src And dst address is Same](/questions/59351/src-and-dst-address-is-same)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59351-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59351-score" class="post-score" title="current number of votes">0</div><span id="post-59351-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please help me what is this</p><p>src and dst address is all zero's why wireshark showing this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-all_zeros" rel="tag" title="see questions tagged &#39;all_zeros&#39;">all_zeros</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '17, 02:59</strong></p><img src="https://secure.gravatar.com/avatar/1b7c6a2341e9b966d3ed788d5737dc96?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Najam&#39;s gravatar image" /><p><span>Najam</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Najam has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>12 Feb '17, 09:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span></p></div></div><div id="comments-container-59351" class="comments-container"><span id="59352"></span><div id="comment-59352" class="comment"><div id="post-59352-score" class="comment-score"></div><div class="comment-text"><p>Help us out here, we not omni-cognisant. At least show a text dump or screenshot of the packet, much better to actually post a link to the capture somewhere.</p><p>It's likely the packet is some form of discovery, e.g. ARP in which case IP's aren't necessarily known (guessing that the src and dst addresses you mention are IP's)</p></div><div id="comment-59352-info" class="comment-info"><span class="comment-age">(12 Feb '17, 03:42)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="59355"></span><div id="comment-59355" class="comment"><div id="post-59355-score" class="comment-score"></div><div class="comment-text"><p>Question closed. The question "Suspicious_Packets" shows full details.</p></div><div id="comment-59355-info" class="comment-info"><span class="comment-age">(12 Feb '17, 09:48)</span> <span class="comment-user userinfo">packethunter</span></div></div></div><div id="comment-tools-59351" class="comment-tools"></div><div class="clear"></div><div id="comment-59351-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "The question "Suspicious\_Packets" shows full details." by packethunter 12 Feb '17, 09:49

</div>

</div>

</div>

