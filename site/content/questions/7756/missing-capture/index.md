+++
type = "question"
title = "missing capture"
description = '''I just installed wireshark and have no capture, or display filters active. Wireshark is missing a lot of messages that do show up on Microsoft Network Monitor. e.g. Some that are missing are marked as DNS and others as TCP in the Microsoft application. I presume I&#x27;ve got something set up wrong, any ...'''
date = "2011-12-03T15:45:00Z"
lastmod = "2011-12-04T02:03:00Z"
weight = 7756
keywords = [ "setup", "dns", "filters", "missing" ]
aliases = [ "/questions/7756" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [missing capture](/questions/7756/missing-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7756-score" class="post-score" title="current number of votes">0</div><span id="post-7756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed wireshark and have no capture, or display filters active.</p><p>Wireshark is missing a lot of messages that do show up on Microsoft Network Monitor. e.g. Some that are missing are marked as DNS and others as TCP in the Microsoft application.</p><p>I presume I've got something set up wrong, any suggestions how to get the complete capture in wireshark too would be much appreciated :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-setup" rel="tag" title="see questions tagged &#39;setup&#39;">setup</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span> <span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span> <span class="post-tag tag-link-missing" rel="tag" title="see questions tagged &#39;missing&#39;">missing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Dec '11, 15:45</strong></p><img src="https://secure.gravatar.com/avatar/226b77f1bf91879473aacd5d071dba19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Covert%20Coven&#39;s gravatar image" /><p><span>Covert Coven</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Covert Coven has no accepted answers">0%</span></p></div></div><div id="comments-container-7756" class="comments-container"><span id="7759"></span><div id="comment-7759" class="comment"><div id="post-7759-score" class="comment-score"></div><div class="comment-text"><p>What interfaces are you trying to capture on?</p></div><div id="comment-7759-info" class="comment-info"><span class="comment-age">(04 Dec '11, 01:09)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-7756" class="comment-tools"></div><div class="clear"></div><div id="comment-7756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7760"></span>

<div id="answer-container-7760" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7760-score" class="post-score" title="current number of votes">0</div><span id="post-7760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hey thanks for wanting to help :) I worked it out myself just now, because I exported the files captured in each and then imported them in the other: D'oh... I didn't have the DNS protocol enabled on wireshark. It's the first time I used it so it's all new to me ;)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Dec '11, 02:03</strong></p><img src="https://secure.gravatar.com/avatar/226b77f1bf91879473aacd5d071dba19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Covert%20Coven&#39;s gravatar image" /><p><span>Covert Coven</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Covert Coven has no accepted answers">0%</span></p></div></div><div id="comments-container-7760" class="comments-container"></div><div id="comment-tools-7760" class="comment-tools"></div><div class="clear"></div><div id="comment-7760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

