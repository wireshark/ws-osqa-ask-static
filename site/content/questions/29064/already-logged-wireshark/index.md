+++
type = "question"
title = "Already logged wireshark"
description = '''It is possible to find out the password and username when entering a page already logged in?'''
date = "2014-01-21T10:10:00Z"
lastmod = "2014-01-22T01:14:00Z"
weight = 29064
keywords = [ "logged", "wireshark" ]
aliases = [ "/questions/29064" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Already logged wireshark](/questions/29064/already-logged-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29064-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29064-score" class="post-score" title="current number of votes">0</div><span id="post-29064-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>It is possible to find out the password and username when entering a page already logged in?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-logged" rel="tag" title="see questions tagged &#39;logged&#39;">logged</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '14, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/4fa5378861fc50e40c53f37db49ce496?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="odo&#39;s gravatar image" /><p><span>odo</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="odo has no accepted answers">0%</span></p></div></div><div id="comments-container-29064" class="comments-container"></div><div id="comment-tools-29064" class="comment-tools"></div><div class="clear"></div><div id="comment-29064-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29068"></span>

<div id="answer-container-29068" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29068-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29068-score" class="post-score" title="current number of votes">2</div><span id="post-29068-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In general no, as the credentials will (should) be transmitted only once. The browser might store them somewhere, but that's not a problem you can analyze with Wireshark.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jan '14, 11:20</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Jan '14, 12:06</strong> </span></p></div></div><div id="comments-container-29068" class="comments-container"><span id="29082"></span><div id="comment-29082" class="comment"><div id="post-29082-score" class="comment-score"></div><div class="comment-text"><p>I understand, thanks!</p></div><div id="comment-29082-info" class="comment-info"><span class="comment-age">(22 Jan '14, 01:14)</span> <span class="comment-user userinfo">odo</span></div></div></div><div id="comment-tools-29068" class="comment-tools"></div><div class="clear"></div><div id="comment-29068-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

