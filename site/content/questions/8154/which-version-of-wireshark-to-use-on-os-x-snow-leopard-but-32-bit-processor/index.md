+++
type = "question"
title = "which version of wireshark to use on OS X Snow Leopard but 32-bit processor?"
description = '''Have a MacBook Pro Intel Core Duo 32-bit, but run Snow Leopard... should I install Leopard 32-bit? any differences in the OS that will affect Wireshark? Thanks'''
date = "2011-12-28T11:56:00Z"
lastmod = "2011-12-28T18:43:00Z"
weight = 8154
keywords = [ "macbook", "versions" ]
aliases = [ "/questions/8154" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [which version of wireshark to use on OS X Snow Leopard but 32-bit processor?](/questions/8154/which-version-of-wireshark-to-use-on-os-x-snow-leopard-but-32-bit-processor)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8154-score" class="post-score" title="current number of votes">0</div><span id="post-8154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Have a MacBook Pro Intel Core Duo 32-bit, but run Snow Leopard... should I install Leopard 32-bit? any differences in the OS that will affect Wireshark?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macbook" rel="tag" title="see questions tagged &#39;macbook&#39;">macbook</span> <span class="post-tag tag-link-versions" rel="tag" title="see questions tagged &#39;versions&#39;">versions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '11, 11:56</strong></p><img src="https://secure.gravatar.com/avatar/67a2d2fc229079b65fe2a65f125a581b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="canon273&#39;s gravatar image" /><p><span>canon273</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="canon273 has no accepted answers">0%</span></p></div></div><div id="comments-container-8154" class="comments-container"></div><div id="comment-tools-8154" class="comment-tools"></div><div class="clear"></div><div id="comment-8154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8158"></span>

<div id="answer-container-8158" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8158-score" class="post-score" title="current number of votes">0</div><span id="post-8158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The only 32-bit x86 binary version we have is the Leopard version; it should work, although the "monitor mode" checkbox might not be available for AirPort adapters (you have to, instead, select one of the 802.11 headers to capture in monitor mode).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Dec '11, 18:43</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-8158" class="comments-container"></div><div id="comment-tools-8158" class="comment-tools"></div><div class="clear"></div><div id="comment-8158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

