+++
type = "question"
title = "How would I create a filter (Capture &amp; Display) that performs the following:"
description = '''Logs only packets that have either your IP address OR your Hardware Address as either destination or source? Logs only packets that have either PING Request or PING Replies in them (but no other types of ICMP traffic). Logs only packets that have either ARP Request or ARP Responses in them. Logs onl...'''
date = "2016-02-06T16:12:00Z"
lastmod = "2016-02-06T16:12:00Z"
weight = 49934
keywords = [ "capture-filter", "homework" ]
aliases = [ "/questions/49934" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How would I create a filter (Capture & Display) that performs the following:](/questions/49934/how-would-i-create-a-filter-capture-display-that-performs-the-following)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49934-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49934-score" class="post-score" title="current number of votes">0</div><span id="post-49934-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Logs only packets that have either your IP address OR your Hardware Address as either destination or source?</p><p>Logs only packets that have either PING Request or PING Replies in them (but no other types of ICMP traffic).</p><p>Logs only packets that have either ARP Request or ARP Responses in them.</p><p>Logs only packets that are NOT Ethernet Broadcasts.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '16, 16:12</strong></p><img src="https://secure.gravatar.com/avatar/c5f89aa2c9e5660f45f68ebc9b3ea64a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sunwers&#39;s gravatar image" /><p><span>sunwers</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sunwers has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Feb '16, 17:02</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-49934" class="comments-container"></div><div id="comment-tools-49934" class="comment-tools"></div><div class="clear"></div><div id="comment-49934-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

