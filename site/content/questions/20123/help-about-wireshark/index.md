+++
type = "question"
title = "Help about wireshark"
description = '''Hi, I am a student in Croatia at the Faculty of traffic and transport science and I am doing a degree work about wireshark. I already have some books, but could You please send me some materials or books about wireshark on my e-mail or tell we wich is the best book or materials to study with so I co...'''
date = "2013-04-06T03:36:00Z"
lastmod = "2013-04-06T04:17:00Z"
weight = 20123
keywords = [ "materials" ]
aliases = [ "/questions/20123" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Help about wireshark](/questions/20123/help-about-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20123-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20123-score" class="post-score" title="current number of votes">0</div><span id="post-20123-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am a student in Croatia at the Faculty of traffic and transport science and I am doing a degree work about wireshark. I already have some books, but could You please send me some materials or books about wireshark on my e-mail or tell we wich is the best book or materials to study with so I could best present wireshark and it's features?</p><p>best regards, Dalibor Golubić, <span class="__cf_email__" data-cfemail="593d3835303b362b3e36352c3b303a192038313636773a3634">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-materials" rel="tag" title="see questions tagged &#39;materials&#39;">materials</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '13, 03:36</strong></p><img src="https://secure.gravatar.com/avatar/889a5b46a69a37a4f42df980fe2778d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dado&#39;s gravatar image" /><p><span>Dado</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dado has no accepted answers">0%</span></p></div></div><div id="comments-container-20123" class="comments-container"></div><div id="comment-tools-20123" class="comment-tools"></div><div class="clear"></div><div id="comment-20123-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20124"></span>

<div id="answer-container-20124" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20124-score" class="post-score" title="current number of votes">1</div><span id="post-20124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look at the answers to this similar question: <a href="http://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark">http://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '13, 04:17</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-20124" class="comments-container"></div><div id="comment-tools-20124" class="comment-tools"></div><div class="clear"></div><div id="comment-20124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

