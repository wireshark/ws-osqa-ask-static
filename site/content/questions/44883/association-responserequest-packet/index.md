+++
type = "question"
title = "[closed] Association response/request packet?"
description = '''Association Request packet shows which power mode I have enabled in my Intel 6300 client laptop. SM Power save mode disabled (0x0003) for High performance mode. Dynamic SM Power save mode (0x0001) for Power Save mode.  But in Association response packet which is obtained from the Access point, it sh...'''
date = "2015-08-05T12:31:00Z"
lastmod = "2015-08-05T12:45:00Z"
weight = 44883
keywords = [ "capture", "request", "response", "packet", "wireshark" ]
aliases = [ "/questions/44883" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Association response/request packet?](/questions/44883/association-responserequest-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44883-score" class="post-score" title="current number of votes">0</div><span id="post-44883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><strong>Association Request</strong> packet shows which power mode I have enabled in my Intel 6300 client laptop.</p><p>SM Power save mode <em>disabled</em> (0x0003) for <strong>High performance mode.</strong></p><p>Dynamic <em>SM Power save mode</em> (0x0001) for <strong>Power Save mode.</strong></p><hr /><p>But in <strong>Association response packet</strong> which is obtained from the Access point, it shows:</p><p>SM Power save mode <em>disabled</em> (0x0003) for <strong>High performance mode.</strong> <strong>&lt;--</strong> shows here disabled</p><p>SM Power save mode <em>disabled</em> (0x0003) for <strong>Power Save mode.</strong> <strong>&lt;--</strong> shows here disabled</p><hr /><p>Refer IEEE 802.11 wireless LAN management frame -&gt; Tagged parameters -&gt; Tag: HT Capabilities -&gt; HT Capabilities Info -&gt; HT SM Power save.</p><p>Is this normal? please explain why its getting disabled even though i enabled it..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '15, 12:31</strong></p><img src="https://secure.gravatar.com/avatar/6c2eccb0ac05aad1c712a709d07d00d0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wireshark_geek&#39;s gravatar image" /><p><span>wireshark_geek</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wireshark_geek has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>05 Aug '15, 12:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-44883" class="comments-container"><span id="44884"></span><div id="comment-44884" class="comment"><div id="post-44884-score" class="comment-score"></div><div class="comment-text"><p>This appears to be a duplicate of <a href="https://ask.wireshark.org/questions/44854/association-request-and-response-packet">https://ask.wireshark.org/questions/44854/association-request-and-response-packet</a></p></div><div id="comment-44884-info" class="comment-info"><span class="comment-age">(05 Aug '15, 12:45)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-44883" class="comment-tools"></div><div class="clear"></div><div id="comment-44883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 05 Aug '15, 12:45

</div>

</div>

</div>

