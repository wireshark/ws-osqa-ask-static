+++
type = "question"
title = "Can you create a GEOIP database of your own companies RFC1918 IP&#x27;s?"
description = '''I am wondering is there a way to generate your own GeoIP database for your companies own RFC1918 IP&#x27;s? What I am trying to do is take our office locations city, Office IP subent/s, and Lat/Lon and make my own database so that I can see the interconnectivity of my own companies commuications. This wa...'''
date = "2013-01-23T14:38:00Z"
lastmod = "2013-01-23T14:55:00Z"
weight = 17911
keywords = [ "geoip", "rfc1918", "private", "database" ]
aliases = [ "/questions/17911" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can you create a GEOIP database of your own companies RFC1918 IP's?](/questions/17911/can-you-create-a-geoip-database-of-your-own-companies-rfc1918-ips)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17911-score" class="post-score" title="current number of votes">0</div><span id="post-17911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am wondering is there a way to generate your own GeoIP database for your companies own RFC1918 IP's? What I am trying to do is take our office locations city, Office IP subent/s, and Lat/Lon and make my own database so that I can see the interconnectivity of my own companies commuications.</p><p>This way I can go to the Endpoints Map and see the relation quicker.</p><p>Thank you in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span> <span class="post-tag tag-link-rfc1918" rel="tag" title="see questions tagged &#39;rfc1918&#39;">rfc1918</span> <span class="post-tag tag-link-private" rel="tag" title="see questions tagged &#39;private&#39;">private</span> <span class="post-tag tag-link-database" rel="tag" title="see questions tagged &#39;database&#39;">database</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '13, 14:38</strong></p><img src="https://secure.gravatar.com/avatar/514206ae0fcaca7de6ef613a3a9cd492?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dugcool&#39;s gravatar image" /><p><span>dugcool</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dugcool has no accepted answers">0%</span></p></div></div><div id="comments-container-17911" class="comments-container"></div><div id="comment-tools-17911" class="comment-tools"></div><div class="clear"></div><div id="comment-17911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17912"></span>

<div id="answer-container-17912" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17912-score" class="post-score" title="current number of votes">1</div><span id="post-17912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://dev.maxmind.com/geoip/csv">Does this help</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '13, 14:55</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-17912" class="comments-container"></div><div id="comment-tools-17912" class="comment-tools"></div><div class="clear"></div><div id="comment-17912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

