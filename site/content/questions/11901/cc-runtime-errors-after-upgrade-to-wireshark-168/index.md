+++
type = "question"
title = "++CC Runtime Errors after upgrade to Wireshark 1.68"
description = '''Run time errors coming up randomly while doing a Capture or looking at statistics?? We recently upgraded from Version 1.6.2 due to multiple performance of tool not responding, loosing graphic display and also abends due to low memory errors. This was corrected the performance of the software improve...'''
date = "2012-06-14T10:12:00Z"
lastmod = "2012-06-14T19:26:00Z"
weight = 11901
keywords = [ "runtime", "errorslosed", "henstrad" ]
aliases = [ "/questions/11901" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [++CC Runtime Errors after upgrade to Wireshark 1.68](/questions/11901/cc-runtime-errors-after-upgrade-to-wireshark-168)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11901-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11901-score" class="post-score" title="current number of votes">0</div><span id="post-11901-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Run time errors coming up randomly while doing a Capture or looking at statistics?? We recently upgraded from Version 1.6.2 due to multiple performance of tool not responding, loosing graphic display and also abends due to low memory errors. This was corrected the performance of the software improved. I can navigate without a probem the different pannels of the tool, but I am unable to stop now the CC ++ Runtime errors please advice on how I can correct this problem? which is causing the tool to abend each time the errors come up??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span> <span class="post-tag tag-link-errorslosed" rel="tag" title="see questions tagged &#39;errorslosed&#39;">errorslosed</span> <span class="post-tag tag-link-henstrad" rel="tag" title="see questions tagged &#39;henstrad&#39;">henstrad</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '12, 10:12</strong></p><img src="https://secure.gravatar.com/avatar/4fe945c1d2b3f1792a82a336f0520070?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AMS121212&#39;s gravatar image" /><p><span>AMS121212</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AMS121212 has no accepted answers">0%</span></p></div></div><div id="comments-container-11901" class="comments-container"></div><div id="comment-tools-11901" class="comment-tools"></div><div class="clear"></div><div id="comment-11901-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11906"></span>

<div id="answer-container-11906" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11906-score" class="post-score" title="current number of votes">0</div><span id="post-11906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you post the exact error message you get? It's not clear from your question what the error is.</p><p>If you're running out of memory then it's not likely that the upgrade will have helped much. See the <a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory">Out Of Memory</a> page on the wiki for more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jun '12, 12:30</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-11906" class="comments-container"><span id="11912"></span><div id="comment-11912" class="comment"><div id="post-11912-score" class="comment-score"></div><div class="comment-text"><p>The error is: Application has requested Runtime error to terminate the application. This is when Wireshark abends?</p></div><div id="comment-11912-info" class="comment-info"><span class="comment-age">(14 Jun '12, 19:26)</span> <span class="comment-user userinfo">AMS121212</span></div></div></div><div id="comment-tools-11906" class="comment-tools"></div><div class="clear"></div><div id="comment-11906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

