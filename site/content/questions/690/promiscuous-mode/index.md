+++
type = "question"
title = "promiscuous mode"
description = '''When I start wireshark I go to capture on the tool bar, then interfaces. Under descriptions is Broadcom NetXtreme Gigabit Ethernet Driver followed by the MAC address. I click on Options and make sure promiscuous mode is checked and a dialog box opens up wi this in it. Please check that &quot;DeviceNPF_{4...'''
date = "2010-10-26T18:26:00Z"
lastmod = "2010-10-27T21:18:00Z"
weight = 690
keywords = [ "promiscuous", "mode" ]
aliases = [ "/questions/690" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [promiscuous mode](/questions/690/promiscuous-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-690-score" class="post-score" title="current number of votes">0</div><span id="post-690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I start wireshark I go to capture on the tool bar, then interfaces. Under descriptions is Broadcom NetXtreme Gigabit Ethernet Driver followed by the MAC address. I click on Options and make sure promiscuous mode is checked and a dialog box opens up wi this in it. Please check that "DeviceNPF_{4A65B691-9F55-4127-9C92-727DB3ACB245}" is the proper interface. If I go back and uncheck promiscuous mode I can then captur packets on the wire. Lost.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '10, 18:26</strong></p><img src="https://secure.gravatar.com/avatar/7c5575e1cfebee5b57c33cf1a125de71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saturine&#39;s gravatar image" /><p><span>saturine</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saturine has no accepted answers">0%</span></p></div></div><div id="comments-container-690" class="comments-container"><span id="719"></span><div id="comment-719" class="comment"><div id="post-719-score" class="comment-score"></div><div class="comment-text"><p>I have an HP laptop with the Broadcom NetXtreme Gigabit Ethernet card - driver date 11/9/2005 version 8.48.0.0 and it does not have a problem.</p><p>What version of Wireshark are you using as mine does not show a MAC address - it shows an IP address column.</p></div><div id="comment-719-info" class="comment-info"><span class="comment-age">(27 Oct '10, 21:18)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-690" class="comment-tools"></div><div class="clear"></div><div id="comment-690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

