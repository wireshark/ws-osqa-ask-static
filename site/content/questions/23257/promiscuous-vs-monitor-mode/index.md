+++
type = "question"
title = "Promiscuous vs monitor mode"
description = '''Hi, It looks like my Wireshark is not running in monitor mode. This is in short. Now the extended version :-).  I&#x27;m, running Wireshark 1.6.7 on Ubuntu 12.04. I activated monitor mode on mon0 interface through airmon. I set Wireshark to listen on mon0. When I open mon0 settings I can see that the &quot;Ca...'''
date = "2013-07-22T14:50:00Z"
lastmod = "2013-07-22T14:50:00Z"
weight = 23257
keywords = [ "promiscuous-mode", "monitor-mode" ]
aliases = [ "/questions/23257" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Promiscuous vs monitor mode](/questions/23257/promiscuous-vs-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23257-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23257-score" class="post-score" title="current number of votes">0</div><span id="post-23257-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, It looks like my Wireshark is not running in monitor mode. This is in short. Now the extended version :-). I'm, running Wireshark 1.6.7 on Ubuntu 12.04. I activated monitor mode on mon0 interface through airmon. I set Wireshark to listen on mon0. When I open mon0 settings I can see that the "Capture packets in promiscuous mode" is enabled. "Capture packets in monitor mode" is not checked, and when I try to activate it I get error "The capabilities of the capture device "mon0" could not be obtained (That device doesn't support monitor mode)... Why is that so when airmon states that "monitor mode enabled on mon0"? When I'm sniffing on that interface i'm not getting any traffic from other machines within my wireless network. This indicates that nor the monitor mode nor the promiscuous mode is active on that interface (?). What am I doing wrong? This is my WLAN so I know the password (I set WPA2 Personal encryption on it). Thanks on any help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-promiscuous-mode" rel="tag" title="see questions tagged &#39;promiscuous-mode&#39;">promiscuous-mode</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '13, 14:50</strong></p><img src="https://secure.gravatar.com/avatar/8c1d2e18fd109856aeeb39970ea92e87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mate%20Strgacic&#39;s gravatar image" /><p><span>Mate Strgacic</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mate Strgacic has no accepted answers">0%</span></p></div></div><div id="comments-container-23257" class="comments-container"></div><div id="comment-tools-23257" class="comment-tools"></div><div class="clear"></div><div id="comment-23257-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

