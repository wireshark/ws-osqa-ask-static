+++
type = "question"
title = "Wireshark PCAPNG log file to KDD99 dataset format conversion"
description = '''I am compiling a list of relevant and computable features from Wireshark log file data and need help. I am comparing the log file data to KDD Cup 1999 Intrusion Detection Dataset format. Almost all the standard ML papers used this dataset. This dataset has 41 features and the list of features is giv...'''
date = "2017-07-13T07:36:00Z"
lastmod = "2017-07-13T07:36:00Z"
weight = 62747
keywords = [ "extraction", "pcapng", "kdd99", "feature", "wireshark" ]
aliases = [ "/questions/62747" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark PCAPNG log file to KDD99 dataset format conversion](/questions/62747/wireshark-pcapng-log-file-to-kdd99-dataset-format-conversion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62747-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62747-score" class="post-score" title="current number of votes">0</div><span id="post-62747-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am compiling a list of relevant and computable features from Wireshark log file data and need help.</p><p>I am comparing the log file data to KDD Cup 1999 Intrusion Detection Dataset format. Almost all the standard ML papers used this dataset. This dataset has 41 features and the list of features is given here: <a href="http://kdd.ics.uci.edu/databases/kddcup99/kddcup.names">http://kdd.ics.uci.edu/databases/kddcup99/kddcup.names</a></p><p>My goal is to find which features (from the above list) are computable from the log file and also find a way to extract those features.</p><p>Thanks and regards,</p><p>P.S.: Here is the link to the dataset: <a href="http://kdd.ics.uci.edu/databases/kddcup99/kddcup99.html">http://kdd.ics.uci.edu/databases/kddcup99/kddcup99.html</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-extraction" rel="tag" title="see questions tagged &#39;extraction&#39;">extraction</span> <span class="post-tag tag-link-pcapng" rel="tag" title="see questions tagged &#39;pcapng&#39;">pcapng</span> <span class="post-tag tag-link-kdd99" rel="tag" title="see questions tagged &#39;kdd99&#39;">kdd99</span> <span class="post-tag tag-link-feature" rel="tag" title="see questions tagged &#39;feature&#39;">feature</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '17, 07:36</strong></p><img src="https://secure.gravatar.com/avatar/e670a3d770953b3656aa9de45d702ba7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PTDS&#39;s gravatar image" /><p><span>PTDS</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PTDS has no accepted answers">0%</span></p></div></div><div id="comments-container-62747" class="comments-container"></div><div id="comment-tools-62747" class="comment-tools"></div><div class="clear"></div><div id="comment-62747-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

