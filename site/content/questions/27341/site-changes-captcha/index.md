+++
type = "question"
title = "Site changes: CAPTCHA ?"
description = '''I just had to fill in a CAPTCHA to be allowed to edit one of my answers (I was logged in)? Is this intentional? It kind of disturbs the workflow. UPDATE: Strange. It only happens for the answer in one question !?!?  http://ask.wireshark.org/questions/27328/shutr-protocol-suppressed-headers-for-uplin...'''
date = "2013-11-25T05:38:00Z"
lastmod = "2013-11-25T06:11:00Z"
weight = 27341
keywords = [ "captcha", "site" ]
aliases = [ "/questions/27341" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Site changes: CAPTCHA ?](/questions/27341/site-changes-captcha)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27341-score" class="post-score" title="current number of votes">1</div><span id="post-27341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just had to fill in a CAPTCHA to be allowed to edit one of my answers (I was logged in)? Is this intentional? It kind of disturbs the workflow.</p><p><strong>UPDATE</strong>: Strange. It only happens for the answer in one question !?!?</p><blockquote><p><a href="http://ask.wireshark.org/questions/27328/shutr-protocol-suppressed-headers-for-uplink-traffic-reduction">http://ask.wireshark.org/questions/27328/shutr-protocol-suppressed-headers-for-uplink-traffic-reduction</a></p></blockquote><p><img src="https://osqa-ask.wireshark.org/upfiles/Captcha_small.png" alt="alt text" /></p><p><strong>UPDATE</strong></p><p>The CAPTCHA accepts even parts of the numbers !?! Looks like a bug in the CAPTCHA checking routine...</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Captcha_2_small.PNG" alt="alt text" /></p><p>Anyway, if someone of the Admins could please check why there is a CAPTCHA only for that single answer of the following question, I would be thankful:</p><blockquote><p><a href="http://ask.wireshark.org/questions/27328/shutr-protocol-suppressed-headers-for-uplink-traffic-reduction">http://ask.wireshark.org/questions/27328/shutr-protocol-suppressed-headers-for-uplink-traffic-reduction</a></p></blockquote><p>Regards<br />
Kurt</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-captcha" rel="tag" title="see questions tagged &#39;captcha&#39;">captcha</span> <span class="post-tag tag-link-site" rel="tag" title="see questions tagged &#39;site&#39;">site</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '13, 05:38</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Nov '13, 05:55</strong> </span></p></div></div><div id="comments-container-27341" class="comments-container"><span id="27343"></span><div id="comment-27343" class="comment"><div id="post-27343-score" class="comment-score"></div><div class="comment-text"><p>I just made some minor edits to your answer to that question, just to see if I also got CAPTCHA, and I did, so it's not just you. I don't know why the CAPTCHA though.</p></div><div id="comment-27343-info" class="comment-info"><span class="comment-age">(25 Nov '13, 06:07)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="27345"></span><div id="comment-27345" class="comment"><div id="post-27345-score" class="comment-score">1</div><div class="comment-text"><p>strange indeed, because it's (so far) just that single answer.</p><p>Maybe it's a problem with the site hoster (cloudflare), like some stupid 'security' or 'intellectual property protection' module that triggers on keywords. I thought it's the link to the google patents. But the CAPTCHA appears even if I remove that link and anything related to patents.</p></div><div id="comment-27345-info" class="comment-info"><span class="comment-age">(25 Nov '13, 06:11)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27341" class="comment-tools"></div><div class="clear"></div><div id="comment-27341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

