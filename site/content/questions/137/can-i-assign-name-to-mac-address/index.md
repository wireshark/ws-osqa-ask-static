+++
type = "question"
title = "Can I assign name to mac-address?"
description = '''Can I assign name to mac-address, so that when I see the capture it&#x27;ll be very easy to understand? -ram.'''
date = "2010-09-15T22:52:00Z"
lastmod = "2010-09-18T00:24:00Z"
weight = 137
keywords = [ "name-resolving", "mac-address", "ethers" ]
aliases = [ "/questions/137" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Can I assign name to mac-address?](/questions/137/can-i-assign-name-to-mac-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-137-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-137-score" class="post-score" title="current number of votes">0</div><span id="post-137-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can I assign name to mac-address, so that when I see the capture it'll be very easy to understand?</p><p>-ram.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-name-resolving" rel="tag" title="see questions tagged &#39;name-resolving&#39;">name-resolving</span> <span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span> <span class="post-tag tag-link-ethers" rel="tag" title="see questions tagged &#39;ethers&#39;">ethers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '10, 22:52</strong></p><img src="https://secure.gravatar.com/avatar/5c59321a66976ba615e1a50b46a4d209?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ramprasad&#39;s gravatar image" /><p><span>Ramprasad</span><br />
<span class="score" title="20 reputation points">20</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ramprasad has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>17 Sep '10, 11:20</strong> </span></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span></p></div></div><div id="comments-container-137" class="comments-container"></div><div id="comment-tools-137" class="comment-tools"></div><div class="clear"></div><div id="comment-137-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="139"></span>

<div id="answer-container-139" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-139-score" class="post-score" title="current number of votes">3</div><span id="post-139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ramprasad has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's what the <em>ethers</em> file can do for you, see <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChAppFilesConfigurationSection.html">Users Guide</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 23:15</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-139" class="comments-container"><span id="149"></span><div id="comment-149" class="comment"><div id="post-149-score" class="comment-score"></div><div class="comment-text"><p>Hi,</p><p>Thats what I'm looking for. But I have 1 more question. The ethers file I edit will make the names appear in "packet details", but I want these name to appear in "packet list" (in run time) so that I can see the name in live, instead of selecting &amp; then seeing the name.</p></div><div id="comment-149-info" class="comment-info"><span class="comment-age">(16 Sep '10, 07:09)</span> <span class="comment-user userinfo">Ramprasad</span></div></div></div><div id="comment-tools-139" class="comment-tools"></div><div class="clear"></div><div id="comment-139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="152"></span>

<div id="answer-container-152" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-152-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-152-score" class="post-score" title="current number of votes">3</div><span id="post-152-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could add an eth.dst and eth.src column to see the MAC header addressing in the Packet List pane.</p><p>Steps to add Ethernet Source/Destination Addresses to Packet List Pane (see note below about Wireshark v1.4.0 issue):</p><ul><li>Select Edit &gt; Preferences &gt; Columns (on left)</li><li>Select Add ("New Column/Number" appears at end of column list)</li><li>Click the arrow in the Field type window and there is a big list - choose Hw dest addr (resolved)</li><li>Click on New Column in the window and rename your column.</li></ul><p>Click and drag your column to where you want it now and click OK. Do the same to add a Hw src addr column.</p><p>Wireshark 1.4.0 Note - One of the <strong>greatest</strong> features of Wireshark 1.4.0 is that you can right click on a field (such as the Ethernet Source Address field) and select Apply As Column. Unfortunately, using this method to create an eth.src or eth. dst column doesn't appear to work. Creating an eth.addr column does not provide name resolution. Hmmm... will have to play with it further to see if it's just me - on first cup of coffee this morning.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '10, 08:20</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-152" class="comments-container"><span id="210"></span><div id="comment-210" class="comment"><div id="post-210-score" class="comment-score"></div><div class="comment-text"><p>Laura, could you use "More -&gt; Convert to comment" to include this response to the answer of Jaap?</p></div><div id="comment-210-info" class="comment-info"><span class="comment-age">(18 Sep '10, 00:24)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-152" class="comment-tools"></div><div class="clear"></div><div id="comment-152-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

