+++
type = "question"
title = "identify spyware routes"
description = '''I am a newbie to this networking, forced under dirress by a nosy ex wife trying to retrieve my information via email and cloud storage. My work and home laptops are not networked for any use that I have for them except sharing a printer at work with 4 other computers via ethernet. However, I have se...'''
date = "2014-11-20T21:33:00Z"
lastmod = "2014-11-20T21:33:00Z"
weight = 38032
keywords = [ "keylogging", "enterprising", "hacking", "spyware", "help" ]
aliases = [ "/questions/38032" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [identify spyware routes](/questions/38032/identify-spyware-routes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38032-score" class="post-score" title="current number of votes">0</div><span id="post-38032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a newbie to this networking, forced under dirress by a nosy ex wife trying to retrieve my information via email and cloud storage. My work and home laptops are not networked for any use that I have for them except sharing a printer at work with 4 other computers via ethernet. However, I have several different signs of networking programs/applications active, workgroups set up that I can not change settings all of the sudden, 87,000 instances on my built in camera I have never used, etc. She has very computer savy friends and I dont, but I have Wireshark, LOL. How can I find info showing my data going into the wrong hands with this program please. Is there a service that evaluates the data streams for a fee. I have captured several days and need to get this figured out before we start getting into court proceedings to protect my self, and I guess apprehend her little mole in my office that I feel is backdoor networking me when I plug my ethernet cord into my laptop at work. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-keylogging" rel="tag" title="see questions tagged &#39;keylogging&#39;">keylogging</span> <span class="post-tag tag-link-enterprising" rel="tag" title="see questions tagged &#39;enterprising&#39;">enterprising</span> <span class="post-tag tag-link-hacking" rel="tag" title="see questions tagged &#39;hacking&#39;">hacking</span> <span class="post-tag tag-link-spyware" rel="tag" title="see questions tagged &#39;spyware&#39;">spyware</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Nov '14, 21:33</strong></p><img src="https://secure.gravatar.com/avatar/e48aa84e99ae2933cf2c3bb37a432aa9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lost%20in%20networks&#39;s gravatar image" /><p><span>Lost in netw...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lost in networks has no accepted answers">0%</span></p></div></div><div id="comments-container-38032" class="comments-container"></div><div id="comment-tools-38032" class="comment-tools"></div><div class="clear"></div><div id="comment-38032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

