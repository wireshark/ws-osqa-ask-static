+++
type = "question"
title = "[closed] accolate ANIC drivers"
description = '''Im triing to configure wireshark with accolate ANIC-4000 card can`t find drivers anywhere on net  send couple mails to accolade support without answer  if someone have drivers for linux or windows and will to share thank you in advance Nik'''
date = "2016-06-26T23:55:00Z"
lastmod = "2016-06-26T23:55:00Z"
weight = 53664
keywords = [ "accolate", "driver", "anic" ]
aliases = [ "/questions/53664" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] accolate ANIC drivers](/questions/53664/accolate-anic-drivers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53664-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53664-score" class="post-score" title="current number of votes">0</div><span id="post-53664-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im triing to configure wireshark with accolate ANIC-4000 card</p><p>can`t find drivers anywhere on net</p><p>send couple mails to accolade support without answer</p><p>if someone have drivers for linux or windows</p><p>and will to share</p><p>thank you in advance</p><p>Nik</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-accolate" rel="tag" title="see questions tagged &#39;accolate&#39;">accolate</span> <span class="post-tag tag-link-driver" rel="tag" title="see questions tagged &#39;driver&#39;">driver</span> <span class="post-tag tag-link-anic" rel="tag" title="see questions tagged &#39;anic&#39;">anic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '16, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/0453159704389e8578460537f16de4c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cozmo&#39;s gravatar image" /><p><span>cozmo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cozmo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>27 Jun '16, 05:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-53664" class="comments-container"></div><div id="comment-tools-53664" class="comment-tools"></div><div class="clear"></div><div id="comment-53664-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by JeffMorriss 27 Jun '16, 05:59

</div>

</div>

</div>

