+++
type = "question"
title = "Can somebody help my with finding the streaming URL for live streams from BBC iPlayer"
description = '''During the last 2 days I read a lot of how to find out streaming URLs from different websites. Unfortunately I was not able to get the streaming URL for live streams from the BBC iPlayer. The website I am talking about is: http://www.bbc.co.uk/iplayer/live/bbcone After sniffing the website with Wire...'''
date = "2015-08-20T04:43:00Z"
lastmod = "2015-08-20T04:43:00Z"
weight = 45266
keywords = [ "url", "streaming", "bbc", "stream" ]
aliases = [ "/questions/45266" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can somebody help my with finding the streaming URL for live streams from BBC iPlayer](/questions/45266/can-somebody-help-my-with-finding-the-streaming-url-for-live-streams-from-bbc-iplayer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45266-score" class="post-score" title="current number of votes">0</div><span id="post-45266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>During the last 2 days I read a lot of how to find out streaming URLs from different websites. Unfortunately I was not able to get the streaming URL for live streams from the BBC iPlayer.</p><p>The website I am talking about is: <a href="http://www.bbc.co.uk/iplayer/live/bbcone">http://www.bbc.co.uk/iplayer/live/bbcone</a></p><p>After sniffing the website with Wireshark I got the following result: <img src="https://osqa-ask.wireshark.org/upfiles/Bildschirmfoto_2015-08-20_um_13.35.19.png" alt="alt text" /></p><p>I thought that I have to put the Host address and the GET line together like this:</p><p><strong><a href="http://vs-hds-uk-live.bbcfmt.vo.llnwd.net/pool_5/live/bbc_one_london/bbc_one_london.isml/bbc_one_london-audio_3%3d64000-video%3d281000.f4m">http://vs-hds-uk-live.bbcfmt.vo.llnwd.net/pool_5/live/bbc_one_london/bbc_one_london.isml/bbc_one_london-audio_3%3d64000-video%3d281000.f4m</a></strong></p><p>But it does not work. I tried it via VLC and Safari.</p><p>Does anyone has an idea or am I doing something wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-streaming" rel="tag" title="see questions tagged &#39;streaming&#39;">streaming</span> <span class="post-tag tag-link-bbc" rel="tag" title="see questions tagged &#39;bbc&#39;">bbc</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '15, 04:43</strong></p><img src="https://secure.gravatar.com/avatar/48ce5d5dad6e4ceb0c3209f1ca4e6379?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dimobln&#39;s gravatar image" /><p><span>dimobln</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dimobln has no accepted answers">0%</span></p></img></div></div><div id="comments-container-45266" class="comments-container"></div><div id="comment-tools-45266" class="comment-tools"></div><div class="clear"></div><div id="comment-45266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

