+++
type = "question"
title = "unable to intercept whatsapp"
description = '''hi all, i m new to wireshark just wanted to know can we read the Whatsapp message with Wireshark,please suggest '''
date = "2017-03-21T04:59:00Z"
lastmod = "2017-03-21T08:10:00Z"
weight = 60221
keywords = [ "whatsapp" ]
aliases = [ "/questions/60221" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [unable to intercept whatsapp](/questions/60221/unable-to-intercept-whatsapp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60221-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60221-score" class="post-score" title="current number of votes">0</div><span id="post-60221-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi all,</p><p>i m new to wireshark just wanted to know can we read the Whatsapp message with Wireshark,please suggest</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '17, 04:59</strong></p><img src="https://secure.gravatar.com/avatar/e25d6167dd33343b926cb765187b1d39?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="snig143&#39;s gravatar image" /><p><span>snig143</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="snig143 has no accepted answers">0%</span></p></div></div><div id="comments-container-60221" class="comments-container"></div><div id="comment-tools-60221" class="comment-tools"></div><div class="clear"></div><div id="comment-60221-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60227"></span>

<div id="answer-container-60227" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60227-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60227-score" class="post-score" title="current number of votes">1</div><span id="post-60227-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no WhatsApp message dissector in Wireshark, and since more and more communications are encrypted end-to-end this becomes less and less applicable.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '17, 08:10</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60227" class="comments-container"></div><div id="comment-tools-60227" class="comment-tools"></div><div class="clear"></div><div id="comment-60227-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

