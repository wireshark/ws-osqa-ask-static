+++
type = "question"
title = "Testing the broadband of an ad hoc network"
description = '''I&#x27;m trying to mesure the broadband between two computers linked through PLC (and ethernet). They&#x27;re not connected to the internet (I&#x27;m doing this for school and if i want the experiment to have a sense i must do it in a certain place where i can&#x27;t get internet). I managed (painfully) to set up the c...'''
date = "2016-12-21T11:06:00Z"
lastmod = "2016-12-22T07:03:00Z"
weight = 58274
keywords = [ "broadband", "ethernet", "network", "windows" ]
aliases = [ "/questions/58274" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Testing the broadband of an ad hoc network](/questions/58274/testing-the-broadband-of-an-ad-hoc-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58274-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58274-score" class="post-score" title="current number of votes">0</div><span id="post-58274-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to mesure the broadband between two computers linked through PLC (and ethernet). They're not connected to the internet (I'm doing this for school and if i want the experiment to have a sense i must do it in a certain place where i can't get internet). I managed (painfully) to set up the connection between them but since they don't share data i'm stuck with an almost empty result. Can you tell me if WireShark has something like a simulator of data trafic ? And how to do this. (I've searched across the net but all i find is scams and shady little programs) (Sorry for the mistackes i'm not english)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadband" rel="tag" title="see questions tagged &#39;broadband&#39;">broadband</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Dec '16, 11:06</strong></p><img src="https://secure.gravatar.com/avatar/7d85658ca052c07d91fd10b933550270?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Odon&#39;s gravatar image" /><p><span>Odon</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Odon has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Dec '16, 03:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-58274" class="comments-container"><span id="58275"></span><div id="comment-58275" class="comment"><div id="post-58275-score" class="comment-score">1</div><div class="comment-text"><p>Not exactly sure what you are looking for, but maybe iperf?</p><p><a href="https://iperf.fr/iperf-download.php">https://iperf.fr/iperf-download.php</a></p></div><div id="comment-58275-info" class="comment-info"><span class="comment-age">(21 Dec '16, 11:46)</span> <span class="comment-user userinfo">Bob Jones</span></div></div><span id="58298"></span><div id="comment-58298" class="comment"><div id="post-58298-score" class="comment-score"></div><div class="comment-text"><p>I had found it but it looked too complicated for me ,i tried again thanks to you ,and it seems to work, thanks a lot.</p></div><div id="comment-58298-info" class="comment-info"><span class="comment-age">(22 Dec '16, 06:31)</span> <span class="comment-user userinfo">Odon</span></div></div><span id="58299"></span><div id="comment-58299" class="comment"><div id="post-58299-score" class="comment-score"></div><div class="comment-text"><p>Then you can try jperf.</p></div><div id="comment-58299-info" class="comment-info"><span class="comment-age">(22 Dec '16, 07:03)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-58274" class="comment-tools"></div><div class="clear"></div><div id="comment-58274-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

