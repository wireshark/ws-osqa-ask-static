+++
type = "question"
title = "Tracking down an IPO address"
description = '''I had someone post a fake review about my company on Yelp and they posted it anonymously. I need someone who can trace the IPO address of where that review came from so my attorney can subpoena the internet provider and reveal who is slandering my business. I can be reached at 770-843-1211 or emaile...'''
date = "2016-07-11T09:22:00Z"
lastmod = "2016-07-11T10:41:00Z"
weight = 53987
keywords = [ "tracing", "ipo", "address" ]
aliases = [ "/questions/53987" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Tracking down an IPO address](/questions/53987/tracking-down-an-ipo-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53987-score" class="post-score" title="current number of votes">-1</div><span id="post-53987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had someone post a fake review about my company on Yelp and they posted it anonymously. I need someone who can trace the IPO address of where that review came from so my attorney can subpoena the internet provider and reveal who is slandering my business. I can be reached at 770-843-1211 or emailed at <span class="__cf_email__" data-cfemail="523b3c263d253c22333b3c263b3c3512353f333b3e7c313d3f7c">[email protected]</span> Thank you.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tracing" rel="tag" title="see questions tagged &#39;tracing&#39;">tracing</span> <span class="post-tag tag-link-ipo" rel="tag" title="see questions tagged &#39;ipo&#39;">ipo</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '16, 09:22</strong></p><img src="https://secure.gravatar.com/avatar/04c88d1beeb2fe11d00957326176f5c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quenton%20Rivers&#39;s gravatar image" /><p><span>Quenton Rivers</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quenton Rivers has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-53987" class="comments-container"></div><div id="comment-tools-53987" class="comment-tools"></div><div class="clear"></div><div id="comment-53987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53989"></span>

<div id="answer-container-53989" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53989-score" class="post-score" title="current number of votes">1</div><span id="post-53989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I need someone who can trace the IPO address of where that review came from</p></blockquote><p>The someone you want is called "Yelp, Inc". The posting has already been made, so the Internet packets involved in making the posting are no longer available on a network, and therefore there's nothing Wireshark can do to help.</p><p>You should contact Yelp and complain to them about the review. See, for example, the <a href="http://www.yelp-support.com/article/How-do-I-report-content-that-violates-Yelp-s-Content-Guidelines-or-Terms-of-Service?l=en_US">"How do I report content that violates Yelp's Content Guidelines or Terms of Service?"</a> item in the <a href="http://www.yelp-support.com/Legal_Questions?l=en_US">Legal Questions</a> section of the <a href="http://www.yelp-support.com/?l=en_US">Yelp support center</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '16, 10:41</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-53989" class="comments-container"></div><div id="comment-tools-53989" class="comment-tools"></div><div class="clear"></div><div id="comment-53989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

