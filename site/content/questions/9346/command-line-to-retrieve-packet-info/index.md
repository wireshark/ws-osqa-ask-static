+++
type = "question"
title = "Command line to retrieve packet info"
description = '''Hi, thanks for the reply, tried several commands from the manual but could not get what i wanted. Supose I am given a MAC address, a packet type of 8(a beacon frame) and an element ID of 0, then I want to display the SSID parameter set(a tagged parameter) information of a beacon frame from that MAC ...'''
date = "2012-03-04T21:00:00Z"
lastmod = "2012-03-04T21:00:00Z"
weight = 9346
keywords = [ "fields", "command-line" ]
aliases = [ "/questions/9346" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Command line to retrieve packet info](/questions/9346/command-line-to-retrieve-packet-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9346-score" class="post-score" title="current number of votes">0</div><span id="post-9346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, thanks for the reply, tried several commands from the manual but could not get what i wanted. Supose I am given a MAC address, a packet type of 8(a beacon frame) and an element ID of 0, then I want to display the SSID parameter set(a tagged parameter) information of a beacon frame from that MAC address. Can someone give any relevant examples.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fields" rel="tag" title="see questions tagged &#39;fields&#39;">fields</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '12, 21:00</strong></p><img src="https://secure.gravatar.com/avatar/f46c9e12b1c2186822b2edd8f11c9a2e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="automation&#39;s gravatar image" /><p><span>automation</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="automation has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>04 Mar '12, 22:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-9346" class="comments-container"></div><div id="comment-tools-9346" class="comment-tools"></div><div class="clear"></div><div id="comment-9346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

