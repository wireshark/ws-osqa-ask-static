+++
type = "question"
title = "wireshark 1.4.3"
description = '''We require wireshark 1.4.3 for PMU test lab. whether wireshark 1.4.3 is available for free? '''
date = "2012-09-25T02:47:00Z"
lastmod = "2012-09-25T06:15:00Z"
weight = 14499
keywords = [ "wireshark1.2.5" ]
aliases = [ "/questions/14499" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark 1.4.3](/questions/14499/wireshark-143)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14499-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14499-score" class="post-score" title="current number of votes">0</div><span id="post-14499-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We require wireshark 1.4.3 for PMU test lab. whether wireshark 1.4.3 is available for free?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark1.2.5" rel="tag" title="see questions tagged &#39;wireshark1.2.5&#39;">wireshark1.2.5</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '12, 02:47</strong></p><img src="https://secure.gravatar.com/avatar/bf49d866b119e2163aebae77578dab66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sci&#39;s gravatar image" /><p><span>sci</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sci has no accepted answers">0%</span></p></div></div><div id="comments-container-14499" class="comments-container"></div><div id="comment-tools-14499" class="comment-tools"></div><div class="clear"></div><div id="comment-14499-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="14500"></span>

<div id="answer-container-14500" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14500-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14500-score" class="post-score" title="current number of votes">0</div><span id="post-14500-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at the download area, e.g. for win32 see <a href="http://www.wireshark.org/download/win32/all-versions/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '12, 02:58</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-14500" class="comments-container"></div><div id="comment-tools-14500" class="comment-tools"></div><div class="clear"></div><div id="comment-14500-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14508"></span>

<div id="answer-container-14508" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14508-score" class="post-score" title="current number of votes">0</div><span id="post-14508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>... and several others <a href="http://www.wireshark.org/download/">here</a>. As well as your favorite Linux distribution may have it on file in their repositories. Or grab the relevant RPM from the web. This all depends on the platform you want to run it on.</p><p>All this can be used free of charge.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '12, 06:15</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-14508" class="comments-container"></div><div id="comment-tools-14508" class="comment-tools"></div><div class="clear"></div><div id="comment-14508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

