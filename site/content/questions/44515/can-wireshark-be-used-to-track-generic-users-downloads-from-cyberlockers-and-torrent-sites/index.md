+++
type = "question"
title = "Can WireShark be used to track generic users&#x27; downloads from cyberlockers and torrent sites."
description = '''Dear Sirs, I was wondering if WireShark can be used to track generic users&#x27; downloads from cyberlockers and torrent sites (not from my IP address alone but from all the IPs from which the sites are accessed). Thanks for your time in answering this.'''
date = "2015-07-27T02:17:00Z"
lastmod = "2015-07-27T03:18:00Z"
weight = 44515
keywords = [ "wireshark" ]
aliases = [ "/questions/44515" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can WireShark be used to track generic users' downloads from cyberlockers and torrent sites.](/questions/44515/can-wireshark-be-used-to-track-generic-users-downloads-from-cyberlockers-and-torrent-sites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44515-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44515-score" class="post-score" title="current number of votes">0</div><span id="post-44515-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sirs,</p><p>I was wondering if WireShark can be used to track generic users' downloads from cyberlockers and torrent sites (not from my IP address alone but from all the IPs from which the sites are accessed).</p><p>Thanks for your time in answering this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '15, 02:17</strong></p><img src="https://secure.gravatar.com/avatar/c8788dc67866a910e82d4901ae50bff4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="removed-by-request&#39;s gravatar image" /><p><span>removed-by-r...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="removed-by-request has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jul '19, 10:32</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-44515" class="comments-container"></div><div id="comment-tools-44515" class="comment-tools"></div><div class="clear"></div><div id="comment-44515-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44518"></span>

<div id="answer-container-44518" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44518-score" class="post-score" title="current number of votes">0</div><span id="post-44518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, if you can capture right at each of the sites you want to monitor, which you probably can't. You can't capture from a remote location. You can always only capture packets that cross your physical path.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jul '15, 03:18</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-44518" class="comments-container"></div><div id="comment-tools-44518" class="comment-tools"></div><div class="clear"></div><div id="comment-44518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

