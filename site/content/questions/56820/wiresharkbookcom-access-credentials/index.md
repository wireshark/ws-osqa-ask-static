+++
type = "question"
title = "wiresharkbook.com access credentials"
description = '''Hello guys, I&#x27;m not sure if this is the best place where to ask, but I&#x27;m sure some of you may know the answer. I&#x27;ve bought Wireshark101 by Laura Chappell and I must say it&#x27;s quite interesting my issue is that: www.wiresharkbook.com, where most of the supplementary content lies, is protected by a log...'''
date = "2016-10-29T11:25:00Z"
lastmod = "2016-11-03T10:59:00Z"
weight = 56820
keywords = [ "wcna", "wireshark101", "wireshark" ]
aliases = [ "/questions/56820" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [wiresharkbook.com access credentials](/questions/56820/wiresharkbookcom-access-credentials)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56820-score" class="post-score" title="current number of votes">0</div><span id="post-56820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello guys, I'm not sure if this is the best place where to ask, but I'm sure some of you may know the answer. I've bought Wireshark101 by Laura Chappell and I must say it's quite interesting my issue is that:</p><p>www.wiresharkbook.com, where most of the supplementary content lies, is protected by a login. I haven't found the right credentials anywhere so far. <strong>Do you know how to access that website?</strong></p><p>I've also sent an email to <span class="__cf_email__" data-cfemail="fb92959d94bb8c92899e88939a899099949490d5989496">[email protected]</span> without getting any answer.</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wcna" rel="tag" title="see questions tagged &#39;wcna&#39;">wcna</span> <span class="post-tag tag-link-wireshark101" rel="tag" title="see questions tagged &#39;wireshark101&#39;">wireshark101</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '16, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/d950267961ae3199078756d7a71ab25e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rob019&#39;s gravatar image" /><p><span>Rob019</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rob019 has no accepted answers">0%</span></p></div></div><div id="comments-container-56820" class="comments-container"></div><div id="comment-tools-56820" class="comment-tools"></div><div class="clear"></div><div id="comment-56820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56821"></span>

<div id="answer-container-56821" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56821-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56821-score" class="post-score" title="current number of votes">1</div><span id="post-56821-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Rob019 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I see the login prompt now, but www.wiresharkbook.com has never been password protected before, and it was not password-protected a few days ago. I think this might be a temporary problem with the web site. Wait a while, maybe even until after the weekend, and try again.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '16, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-56821" class="comments-container"><span id="56825"></span><div id="comment-56825" class="comment"><div id="post-56825-score" class="comment-score"></div><div class="comment-text"><p>www.wiresharkbook.com can now be accessed again without any login credentials.</p></div><div id="comment-56825-info" class="comment-info"><span class="comment-age">(29 Oct '16, 17:24)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div><span id="56903"></span><div id="comment-56903" class="comment"><div id="post-56903-score" class="comment-score"></div><div class="comment-text"><p>Indeed it was a temporary issue :)</p></div><div id="comment-56903-info" class="comment-info"><span class="comment-age">(01 Nov '16, 09:52)</span> <span class="comment-user userinfo">Rob019</span></div></div><span id="56940"></span><div id="comment-56940" class="comment"><div id="post-56940-score" class="comment-score"></div><div class="comment-text"><p>Well, despite the claims above, it is still asking for credentials ... Bit nasty since I have a wcna exam scheduled in 4 days. Any other source for the trace files ?</p></div><div id="comment-56940-info" class="comment-info"><span class="comment-age">(03 Nov '16, 09:20)</span> <span class="comment-user userinfo">easterman</span></div></div><span id="56943"></span><div id="comment-56943" class="comment"><div id="post-56943-score" class="comment-score"></div><div class="comment-text"><p>Not asking for credentials as of two minutes ago.</p></div><div id="comment-56943-info" class="comment-info"><span class="comment-age">(03 Nov '16, 10:30)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div><span id="56944"></span><div id="comment-56944" class="comment"><div id="post-56944-score" class="comment-score"></div><div class="comment-text"><p>my mistake, somehow I switched to wiresharkbook.org which is asking for credentials. Wiresharkbook.com was giving certificate errors last sunday. Thanks,working now</p></div><div id="comment-56944-info" class="comment-info"><span class="comment-age">(03 Nov '16, 10:59)</span> <span class="comment-user userinfo">easterman</span></div></div></div><div id="comment-tools-56821" class="comment-tools"></div><div class="clear"></div><div id="comment-56821-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

