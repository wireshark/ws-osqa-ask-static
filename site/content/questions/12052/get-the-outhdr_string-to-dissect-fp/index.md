+++
type = "question"
title = "[closed] get the outhdr_string  to dissect FP."
description = '''In FP/AAL2/ATM/DCT2000 mode,the outhdr_string imformation(use to dissect in higher level) is dissector in dct2000 level.but in FP/UDP/IP/ mode,the outhdr_string imformation can&#x27;t dissector from lower level. And with out the outhdr_string imformation ,I can&#x27;t dissector the fp packet. Hope the master ...'''
date = "2012-06-19T02:23:00Z"
lastmod = "2012-06-19T02:27:00Z"
weight = 12052
keywords = [ "fp", "dissect_fp", "outhdr_string" ]
aliases = [ "/questions/12052" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] get the outhdr\_string to dissect FP.](/questions/12052/get-the-outhdr_string-to-dissect-fp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12052-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12052-score" class="post-score" title="current number of votes">0</div><span id="post-12052-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In FP/AAL2/ATM/DCT2000 mode,the outhdr_string imformation(use to dissect in higher level) is dissector in dct2000 level.but in FP/UDP/IP/ mode,the outhdr_string imformation can't dissector from lower level. And with out the outhdr_string imformation ,I can't dissector the fp packet. Hope the master tell me how to resolve the problem. Best regards!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span> <span class="post-tag tag-link-dissect_fp" rel="tag" title="see questions tagged &#39;dissect_fp&#39;">dissect_fp</span> <span class="post-tag tag-link-outhdr_string" rel="tag" title="see questions tagged &#39;outhdr_string&#39;">outhdr_string</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '12, 02:23</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Sep '12, 08:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-12052" class="comments-container"><span id="12053"></span><div id="comment-12053" class="comment"><div id="post-12053-score" class="comment-score"></div><div class="comment-text"><p>Please stop asking the same question over and over again.</p></div><div id="comment-12053-info" class="comment-info"><span class="comment-age">(19 Jun '12, 02:27)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-12052" class="comment-tools"></div><div class="clear"></div><div id="comment-12052-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by helloworld 19 Jun '12, 02:26

</div>

</div>

</div>

