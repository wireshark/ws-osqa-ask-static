+++
type = "question"
title = "Static Analysis of Wireshark by PVS-Studio"
description = '''Hello, My name is Andrey and I work in OOO “Program Verification Systems”. We develop the PVS-Studio static code analyzer for C/C++/C++11. I am C/C++ developer and I know from my experience that it is very difficult to write code and check it. To make these processes easier static code analyzers wer...'''
date = "2015-06-05T04:35:00Z"
lastmod = "2015-06-05T14:13:00Z"
weight = 42919
keywords = [ "fixing", "bug", "analisis" ]
aliases = [ "/questions/42919" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Static Analysis of Wireshark by PVS-Studio](/questions/42919/static-analysis-of-wireshark-by-pvs-studio)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42919-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42919-score" class="post-score" title="current number of votes">0</div><span id="post-42919-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>My name is Andrey and I work in OOO “Program Verification Systems”. We develop the PVS-Studio static code analyzer for C/C++/C++11. I am C/C++ developer and I know from my experience that it is very difficult to write code and check it. To make these processes easier static code analyzers were developed, for example, PVS-Studio. In this article, <a href="http://www.viva64.com/en/b/0328/">Static Analysis of Wireshark by PVS-Studio</a>, I wrote about some easily fixing errors. I really hope if PVS-Studio can make Wireshark safer.</p><p>I want to thank Wireshark team of developers for the good product.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fixing" rel="tag" title="see questions tagged &#39;fixing&#39;">fixing</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span> <span class="post-tag tag-link-analisis" rel="tag" title="see questions tagged &#39;analisis&#39;">analisis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '15, 04:35</strong></p><img src="https://secure.gravatar.com/avatar/721e77e8c8083f18e77fa0a573371649?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AndreyKalashnikovViva64&#39;s gravatar image" /><p><span>AndreyKalash...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AndreyKalashnikovViva64 has no accepted answers">0%</span></p></div></div><div id="comments-container-42919" class="comments-container"><span id="42922"></span><div id="comment-42922" class="comment"><div id="post-42922-score" class="comment-score"></div><div class="comment-text"><p>If you could run it on top of trunk that would be even more useful for us.</p></div><div id="comment-42922-info" class="comment-info"><span class="comment-age">(05 Jun '15, 05:05)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="42924"></span><div id="comment-42924" class="comment"><div id="post-42924-score" class="comment-score"></div><div class="comment-text"><p>If you could <strong>donate a free license</strong> to the Wireshark developers, that would be even more useful ;-)</p></div><div id="comment-42924-info" class="comment-info"><span class="comment-age">(05 Jun '15, 05:42)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="42927"></span><div id="comment-42927" class="comment"><div id="post-42927-score" class="comment-score"></div><div class="comment-text"><p>Note that the master branch can be built using cmake to produce a Visual Studio solution in case that's easier to use for PVS-Studio.</p><p>+1 for a free licence to use on our buildbots.</p></div><div id="comment-42927-info" class="comment-info"><span class="comment-age">(05 Jun '15, 05:48)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="42932"></span><div id="comment-42932" class="comment"><div id="post-42932-score" class="comment-score"></div><div class="comment-text"><p>Or provide a service similar to what Coverity provides with <a href="https://scan.coverity.com">Coverity Scan</a>.</p></div><div id="comment-42932-info" class="comment-info"><span class="comment-age">(05 Jun '15, 14:13)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-42919" class="comment-tools"></div><div class="clear"></div><div id="comment-42919-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

