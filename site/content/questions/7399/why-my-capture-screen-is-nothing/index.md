+++
type = "question"
title = "Why my capture screen is nothing ???"
description = '''I use 3g to connect internet and i try to capture but nothing happen??  '''
date = "2011-11-12T20:29:00Z"
lastmod = "2011-11-13T15:01:00Z"
weight = 7399
keywords = [ "nothing" ]
aliases = [ "/questions/7399" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why my capture screen is nothing ???](/questions/7399/why-my-capture-screen-is-nothing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7399-score" class="post-score" title="current number of votes">0</div><span id="post-7399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use 3g to connect internet and i try to capture but nothing happen?? <img src="http://img202.imageshack.us/img202/5178/67717378.jpg" alt="alt text" /></p><p><img src="http://img207.imageshack.us/img207/8980/75881977.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nothing" rel="tag" title="see questions tagged &#39;nothing&#39;">nothing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '11, 20:29</strong></p><img src="https://secure.gravatar.com/avatar/e91285b07bea2128f0dc1f5b0b32a834?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mong_Toi&#39;s gravatar image" /><p><span>Mong_Toi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mong_Toi has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Nov '11, 20:30</strong> </span></p></div></div><div id="comments-container-7399" class="comments-container"></div><div id="comment-tools-7399" class="comment-tools"></div><div class="clear"></div><div id="comment-7399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7405"></span>

<div id="answer-container-7405" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7405-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7405-score" class="post-score" title="current number of votes">0</div><span id="post-7405-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The screen shots look like you're running on Windows, but which version? 32 or 64 bit? Which version of <a href="http://www.winpcap.org/misc/faq.htm#Q-14">WinPcap</a> do you have installed? Are you running Wireshark with <a href="http://www.winpcap.org/misc/faq.htm#Q-7">sufficient privileges</a> to capture? Have you read <a href="http://ask.wireshark.org/questions/4679/windows-7-3g-modem-wireshark-wont-work">Question 4679</a> and the answer provided there by Guy?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '11, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></img></div></div><div id="comments-container-7405" class="comments-container"></div><div id="comment-tools-7405" class="comment-tools"></div><div class="clear"></div><div id="comment-7405-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

