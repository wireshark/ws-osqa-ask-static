+++
type = "question"
title = "How to distinguish between IS-683 and IS-637 over Ansi sms-BearerData"
description = '''How to distinguish between IS-683 and IS-637 over Ansi sms-BearerData'''
date = "2012-10-28T02:22:00Z"
lastmod = "2012-10-28T02:22:00Z"
weight = 15319
keywords = [ "is-683" ]
aliases = [ "/questions/15319" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to distinguish between IS-683 and IS-637 over Ansi sms-BearerData](/questions/15319/how-to-distinguish-between-is-683-and-is-637-over-ansi-sms-bearerdata)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15319-score" class="post-score" title="current number of votes">0</div><span id="post-15319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to distinguish between IS-683 and IS-637 over Ansi sms-BearerData</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-is-683" rel="tag" title="see questions tagged &#39;is-683&#39;">is-683</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '12, 02:22</strong></p><img src="https://secure.gravatar.com/avatar/59ba8e597d8db82f4f4ca3686152f67a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Emi&#39;s gravatar image" /><p><span>Emi</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Emi has no accepted answers">0%</span></p></div></div><div id="comments-container-15319" class="comments-container"></div><div id="comment-tools-15319" class="comment-tools"></div><div class="clear"></div><div id="comment-15319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

