+++
type = "question"
title = "Capturing remote packets on my wifi network?"
description = '''Hi, I have others conected to my wifi network, I don&#x27;t have physical access to their machines but I want to be able to view all packets that&#x27;s going through my wifi; is this possible? In fact, I only want to view remote traffic going through my wifi, not my own.'''
date = "2015-01-19T07:51:00Z"
lastmod = "2015-01-20T15:35:00Z"
weight = 39276
keywords = [ "remote-capture", "wifi" ]
aliases = [ "/questions/39276" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing remote packets on my wifi network?](/questions/39276/capturing-remote-packets-on-my-wifi-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39276-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39276-score" class="post-score" title="current number of votes">0</div><span id="post-39276-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have others conected to my wifi network, I don't have physical access to their machines but I want to be able to view all packets that's going through my wifi; is this possible? In fact, I only want to view remote traffic going through my wifi, not my own.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '15, 07:51</strong></p><img src="https://secure.gravatar.com/avatar/0186d7f3ee1d3c7711bcac1cc9ac5621?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Triodysin&#39;s gravatar image" /><p><span>Triodysin</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Triodysin has no accepted answers">0%</span></p></div></div><div id="comments-container-39276" class="comments-container"></div><div id="comment-tools-39276" class="comment-tools"></div><div class="clear"></div><div id="comment-39276-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39324"></span>

<div id="answer-container-39324" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39324-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39324-score" class="post-score" title="current number of votes">0</div><span id="post-39324-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are a couple of options.</p><p>First, if your Wireless Access Point can span traffic to one of it's ports that could work. However, most Access Points can't do this. That is really the only option to get <strong>all</strong> traffic that goes through your Wireless network.</p><p>The second option is to get a wireless NIC that supports Monitor mode (Macs do), place it near the access point and run Wireshark. If you don't want any of the Mac's traffic to go out/get captured you can disabled your TCP stack. Here is how you do that on a Mac: System Preferences &gt; Network &gt; Wi-Fi &gt; Change "Configure IPV4" from "Using DHCP" to "Off"</p><p>Hope this helps.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '15, 15:35</strong></p><img src="https://secure.gravatar.com/avatar/4a4df10c701372e5dbbb8015a1d6b67b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="patrick_harrold&#39;s gravatar image" /><p><span>patrick_harrold</span><br />
<span class="score" title="36 reputation points">36</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="patrick_harrold has no accepted answers">0%</span></p></div></div><div id="comments-container-39324" class="comments-container"></div><div id="comment-tools-39324" class="comment-tools"></div><div class="clear"></div><div id="comment-39324-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

