+++
type = "question"
title = "Windows 10 scaling for 4K 15.6 inch screen"
description = '''Does wireshark have an option to scale the entire app to better fit a high density &quot;smaller display?&quot; I have a 15. inch 4k Laptop and it shows up really tiny when opened and I have to lean in to make it out, which isn&#x27;t the best experience..'''
date = "2016-01-19T21:01:00Z"
lastmod = "2016-07-06T22:39:00Z"
weight = 49395
keywords = [ "scaling", "windows10", "4k", "small_windows_size" ]
aliases = [ "/questions/49395" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 10 scaling for 4K 15.6 inch screen](/questions/49395/windows-10-scaling-for-4k-156-inch-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49395-score" class="post-score" title="current number of votes">0</div><span id="post-49395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does wireshark have an option to scale the entire app to better fit a high density "smaller display?" I have a 15. inch 4k Laptop and it shows up really tiny when opened and I have to lean in to make it out, which isn't the best experience..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-scaling" rel="tag" title="see questions tagged &#39;scaling&#39;">scaling</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span> <span class="post-tag tag-link-4k" rel="tag" title="see questions tagged &#39;4k&#39;">4k</span> <span class="post-tag tag-link-small_windows_size" rel="tag" title="see questions tagged &#39;small_windows_size&#39;">small_windows_size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '16, 21:01</strong></p><img src="https://secure.gravatar.com/avatar/c4743e2672d4d92d6e9bdcf271933b91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Noc&#39;s gravatar image" /><p><span>Noc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Noc has no accepted answers">0%</span></p></div></div><div id="comments-container-49395" class="comments-container"><span id="49398"></span><div id="comment-49398" class="comment"><div id="post-49398-score" class="comment-score">1</div><div class="comment-text"><p>Please state your version...</p></div><div id="comment-49398-info" class="comment-info"><span class="comment-age">(20 Jan '16, 00:22)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="49399"></span><div id="comment-49399" class="comment"><div id="post-49399-score" class="comment-score"></div><div class="comment-text"><p>I downloaded the latest stable version. Wireshark-win64-2.0.1</p></div><div id="comment-49399-info" class="comment-info"><span class="comment-age">(20 Jan '16, 00:24)</span> <span class="comment-user userinfo">Noc</span></div></div><span id="53873"></span><div id="comment-53873" class="comment"><div id="post-53873-score" class="comment-score"></div><div class="comment-text"><p>Any news ? I am using Wireshark-win64-2.0.4, the text probably looks ok, but the buttons are definitely too small...</p></div><div id="comment-53873-info" class="comment-info"><span class="comment-age">(06 Jul '16, 20:59)</span> <span class="comment-user userinfo">gold637</span></div></div><span id="53876"></span><div id="comment-53876" class="comment"><div id="post-53876-score" class="comment-score"></div><div class="comment-text"><p>Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p></div><div id="comment-53876-info" class="comment-info"><span class="comment-age">(06 Jul '16, 22:39)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-49395" class="comment-tools"></div><div class="clear"></div><div id="comment-49395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49613"></span>

<div id="answer-container-49613" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49613-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49613-score" class="post-score" title="current number of votes">1</div><span id="post-49613-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark 2.x (Qt version) is a <a href="https://msdn.microsoft.com/en-gb/library/windows/desktop/dn469266(v=vs.85).aspx#windows_8_1_control_panel_overview">per-monitor DPI aware</a> application so will adjust the display according to the scaling you have set in the Settings -&gt; Display.</p><p>Apart from that you can set the font size used for the packet list, packet details and hex bytes pane in the Preferences -&gt; Appearance -&gt; Font and Colors dialog by clicking the font description button.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jan '16, 16:04</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49613" class="comments-container"></div><div id="comment-tools-49613" class="comment-tools"></div><div class="clear"></div><div id="comment-49613-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

