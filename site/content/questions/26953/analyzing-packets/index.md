+++
type = "question"
title = "analyzing packets"
description = '''I just installed wireshark and need to know what to look for on the network that might be slowing it down - many broadcasts?, rogue dhcp server? etc? What should I look for specifically? Is there a quick simple guide for this? Thanks.'''
date = "2013-11-13T08:54:00Z"
lastmod = "2013-11-13T13:22:00Z"
weight = 26953
keywords = [ "network" ]
aliases = [ "/questions/26953" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [analyzing packets](/questions/26953/analyzing-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26953-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26953-score" class="post-score" title="current number of votes">0</div><span id="post-26953-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed wireshark and need to know what to look for on the network that might be slowing it down - many broadcasts?, rogue dhcp server? etc? What should I look for specifically? Is there a quick simple guide for this? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '13, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/2ecfaaf06b51f1ee754784019619043a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tolinrome&#39;s gravatar image" /><p><span>tolinrome</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tolinrome has no accepted answers">0%</span></p></div></div><div id="comments-container-26953" class="comments-container"></div><div id="comment-tools-26953" class="comment-tools"></div><div class="clear"></div><div id="comment-26953-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26966"></span>

<div id="answer-container-26966" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26966-score" class="post-score" title="current number of votes">0</div><span id="post-26966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't want to offend you but the answer to your question</p><ul><li>Is there a quick simple guide for this?</li></ul><p>is no. Wireshark is just a - admittedly great - tool to help you spot things faster. You still need to understand the protocols that are used in the communication. A good start is probably <a href="https://www.goodreads.com/book/show/505560.The_Protocols">TCP/IP Illustrated</a> by W. Richard Stevens. The wireshark books from Laura Chappell are cretainly also a good read: <a href="http://www.wiresharkbook.com/">http://www.wiresharkbook.com/</a></p><p>More resources:</p><ul><li><a href="http://www.youtube.com/results?search_query=sharkfest+wireshark&amp;sm=1">http://www.youtube.com/</a></li><li><a href="http://www.wiresharkbook.com/resources.html">http://www.wiresharkbook.com/resources.html</a></li></ul><p>NB.: Performance problems are among the most difficult problems to diagnose.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '13, 13:22</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Nov '13, 22:51</strong> </span></p></div></div><div id="comments-container-26966" class="comments-container"></div><div id="comment-tools-26966" class="comment-tools"></div><div class="clear"></div><div id="comment-26966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

