+++
type = "question"
title = "ENIP PCCC DATA Decodes"
description = '''I am developing a new control system cybersecurity course and I need some help developing data dissector for ENIP PCCC communication. PCCC shows up as ENIP DATA payloads within Wireshark. I have purchased a decoding software from FTE, but this is not realistic on a massive scale and it has many more...'''
date = "2011-07-25T10:49:00Z"
lastmod = "2011-07-25T10:49:00Z"
weight = 5224
keywords = [ "pccc", "enip", "dissector" ]
aliases = [ "/questions/5224" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ENIP PCCC DATA Decodes](/questions/5224/enip-pccc-data-decodes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5224-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5224-score" class="post-score" title="current number of votes">0</div><span id="post-5224-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am developing a new control system cybersecurity course and I need some help developing data dissector for ENIP PCCC communication. PCCC shows up as ENIP DATA payloads within Wireshark. I have purchased a decoding software from FTE, but this is not realistic on a massive scale and it has many more features than I need.<br />
</p><p>Matt Luallen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pccc" rel="tag" title="see questions tagged &#39;pccc&#39;">pccc</span> <span class="post-tag tag-link-enip" rel="tag" title="see questions tagged &#39;enip&#39;">enip</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jul '11, 10:49</strong></p><img src="https://secure.gravatar.com/avatar/41537bf1671f7b00bda1c890badad6ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="luallen&#39;s gravatar image" /><p><span>luallen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="luallen has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-5224" class="comments-container"></div><div id="comment-tools-5224" class="comment-tools"></div><div class="clear"></div><div id="comment-5224-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

