+++
type = "question"
title = "Categorizing service type in wireshark"
description = '''Is there a way to categorize the flows in Wireshark by their service type (E.G: Web Browsing, ssh, ftp...)? Statistics-&amp;gt;End point list (IPv$4) gives me some idea about the flow, but I was wondering if there&#x27;s a more accurate way of determining the service type.'''
date = "2016-04-24T20:01:00Z"
lastmod = "2016-04-28T02:45:00Z"
weight = 51918
keywords = [ "filter", "service" ]
aliases = [ "/questions/51918" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Categorizing service type in wireshark](/questions/51918/categorizing-service-type-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51918-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51918-score" class="post-score" title="current number of votes">0</div><span id="post-51918-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to categorize the flows in Wireshark by their service type (E.G: Web Browsing, ssh, ftp...)?</p><p>Statistics-&gt;End point list (IPv$4) gives me some idea about the flow, but I was wondering if there's a more accurate way of determining the service type.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-service" rel="tag" title="see questions tagged &#39;service&#39;">service</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '16, 20:01</strong></p><img src="https://secure.gravatar.com/avatar/dfb8180fcc1405e17753a9c540823390?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="capturebuddy&#39;s gravatar image" /><p><span>capturebuddy</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="capturebuddy has no accepted answers">0%</span></p></div></div><div id="comments-container-51918" class="comments-container"></div><div id="comment-tools-51918" class="comment-tools"></div><div class="clear"></div><div id="comment-51918-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52041"></span>

<div id="answer-container-52041" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52041-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52041-score" class="post-score" title="current number of votes">0</div><span id="post-52041-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi Capturebuddy, could "Statistics/Protocol Hierarchy" help in what you are looking for ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '16, 02:45</strong></p><img src="https://secure.gravatar.com/avatar/bba638c3a54975c52c98530defa199af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ValerioItaly&#39;s gravatar image" /><p><span>ValerioItaly</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ValerioItaly has no accepted answers">0%</span></p></div></div><div id="comments-container-52041" class="comments-container"></div><div id="comment-tools-52041" class="comment-tools"></div><div class="clear"></div><div id="comment-52041-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

