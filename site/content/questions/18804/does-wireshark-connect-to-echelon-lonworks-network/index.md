+++
type = "question"
title = "Does Wireshark connect to Echelon LonWorks network?"
description = '''How can I get Wireshark to connect to a Echelon LonWorks network? I have an Echelon TP/FT-10 USB to LonWorks interface. When I run Wireshark it does not see the Echelon TP/FT-10 interface. How do I &quot;add&quot; this interface to Wireshark?'''
date = "2013-02-21T11:13:00Z"
lastmod = "2013-09-20T18:06:00Z"
weight = 18804
keywords = [ "interface", "802.15.4", "lonworks" ]
aliases = [ "/questions/18804" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Does Wireshark connect to Echelon LonWorks network?](/questions/18804/does-wireshark-connect-to-echelon-lonworks-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18804-score" class="post-score" title="current number of votes">0</div><span id="post-18804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I get Wireshark to connect to a Echelon LonWorks network? I have an Echelon TP/FT-10 USB to LonWorks interface. When I run Wireshark it does not see the Echelon TP/FT-10 interface. How do I "add" this interface to Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-802.15.4" rel="tag" title="see questions tagged &#39;802.15.4&#39;">802.15.4</span> <span class="post-tag tag-link-lonworks" rel="tag" title="see questions tagged &#39;lonworks&#39;">lonworks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '13, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/b8121dff3d28206a9c635cef1a858276?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jerry&#39;s gravatar image" /><p><span>jerry</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jerry has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Sep '13, 13:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-18804" class="comments-container"><span id="25061"></span><div id="comment-25061" class="comment"><div id="post-25061-score" class="comment-score"></div><div class="comment-text"><p>What operating system are you using?</p></div><div id="comment-25061-info" class="comment-info"><span class="comment-age">(20 Sep '13, 18:06)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-18804" class="comment-tools"></div><div class="clear"></div><div id="comment-18804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

