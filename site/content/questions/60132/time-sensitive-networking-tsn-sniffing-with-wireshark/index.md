+++
type = "question"
title = "Time Sensitive Networking (TSN) sniffing with Wireshark"
description = '''As Wireshark has a lot of the available dissectors is it possible to also sniff the Time Sensitive Networking (TSN), which is used in industry? That would be a great feature.'''
date = "2017-03-16T17:10:00Z"
lastmod = "2017-03-16T17:10:00Z"
weight = 60132
keywords = [ "tsn", "industry", "wireshark" ]
aliases = [ "/questions/60132" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Time Sensitive Networking (TSN) sniffing with Wireshark](/questions/60132/time-sensitive-networking-tsn-sniffing-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60132-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60132-score" class="post-score" title="current number of votes">0</div><span id="post-60132-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>As Wireshark has a lot of the available dissectors is it possible to also sniff the Time Sensitive Networking (TSN), which is used in industry? That would be a great feature.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tsn" rel="tag" title="see questions tagged &#39;tsn&#39;">tsn</span> <span class="post-tag tag-link-industry" rel="tag" title="see questions tagged &#39;industry&#39;">industry</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '17, 17:10</strong></p><img src="https://secure.gravatar.com/avatar/0d5c20b01de61cf351ec0b51c904a023?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jendker&#39;s gravatar image" /><p><span>Jendker</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jendker has no accepted answers">0%</span></p></div></div><div id="comments-container-60132" class="comments-container"></div><div id="comment-tools-60132" class="comment-tools"></div><div class="clear"></div><div id="comment-60132-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

