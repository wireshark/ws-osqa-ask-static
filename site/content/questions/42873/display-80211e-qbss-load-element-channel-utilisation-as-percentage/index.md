+++
type = "question"
title = "Display 802.11e QBSS Load Element Channel Utilisation as Percentage"
description = '''Hello Experts, I am trying to work out a way of applying the 802.11e QBSS Load Element for channel utilisation as a column in percentage format. I am aware of the option of using &quot;wlan_mgt.qbss.cu&quot;, however this represents an integer based on the maximum load being 255. Can anybody suggest a tweak t...'''
date = "2015-06-04T01:41:00Z"
lastmod = "2015-06-04T01:41:00Z"
weight = 42873
keywords = [ "wlan", "custom_column" ]
aliases = [ "/questions/42873" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Display 802.11e QBSS Load Element Channel Utilisation as Percentage](/questions/42873/display-80211e-qbss-load-element-channel-utilisation-as-percentage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42873-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42873-score" class="post-score" title="current number of votes">0</div><span id="post-42873-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Experts,</p><p>I am trying to work out a way of applying the 802.11e QBSS Load Element for channel utilisation as a column in percentage format.</p><p>I am aware of the option of using "wlan_mgt.qbss.cu", however this represents an integer based on the maximum load being 255.</p><p>Can anybody suggest a tweak to represent this in a percentage format?</p><p>If there isn't an option already, would a .lua dissector be able to be used to accomplish this task?</p><p>Kind Regards,</p><p>Josh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span> <span class="post-tag tag-link-custom_column" rel="tag" title="see questions tagged &#39;custom_column&#39;">custom_column</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '15, 01:41</strong></p><img src="https://secure.gravatar.com/avatar/fc13abd7c82055bfbec02496e094d39a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krypton&#39;s gravatar image" /><p><span>krypton</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krypton has no accepted answers">0%</span></p></div></div><div id="comments-container-42873" class="comments-container"></div><div id="comment-tools-42873" class="comment-tools"></div><div class="clear"></div><div id="comment-42873-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

