+++
type = "question"
title = "Why can&#x27;t I save payload as .au file"
description = '''I use Wireshark to capture a call between two clients using telephony software. The codec used is G711 PCMU and comfort noise (PT=CN), as you can see in picture 1 there are 7 streams altogether, as you can see in picture 2. I want to save the payload as .au file, however, I can only save the first 5...'''
date = "2010-11-17T09:36:00Z"
lastmod = "2010-11-21T16:58:00Z"
weight = 988
keywords = [ ".au", "save", "payload", "audio" ]
aliases = [ "/questions/988" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Why can't I save payload as .au file](/questions/988/why-cant-i-save-payload-as-au-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-988-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-988-score" class="post-score" title="current number of votes">0</div><span id="post-988-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use Wireshark to capture a call between two clients using telephony software. The codec used is G711 PCMU and comfort noise (PT=CN), as you can see in picture 1 there are 7 streams altogether, as you can see in picture 2. I want to save the payload as .au file, however, I can only save the first 5 streams (which only contain some ringing sound or background noise) as .au files, the last 2 (which contain the speech content) can't be saved as .au file (but they can be saved as .raw format). The error message is “Can't save in a file: saving in au format supported only for alaw/ulaw streams”</p><p>However, I can play them using Telephony-&gt;VoIP Calls-&gt;Player</p><p>What is wrong with this?　Thank you!</p><p><img src="http://img3.laibafile.cn/getimgXXX/2/4/photo2/2010/11/18/middle/65401101_3131263_middle.jpg" alt="pic1" /> <img src="http://img3.laibafile.cn/getimgXXX/2/1/photo2/2010/11/18/middle/65401102_3131263_middle.jpg" alt="pic2" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-.au" rel="tag" title="see questions tagged &#39;.au&#39;">.au</span> <span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span> <span class="post-tag tag-link-audio" rel="tag" title="see questions tagged &#39;audio&#39;">audio</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '10, 09:36</strong></p><img src="https://secure.gravatar.com/avatar/2edef74a36aba8b70b94d9885864e7b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="esolve&#39;s gravatar image" /><p><span>esolve</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="esolve has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Nov '10, 03:44</strong> </span></p></div></div><div id="comments-container-988" class="comments-container"><span id="1001"></span><div id="comment-1001" class="comment"><div id="post-1001-score" class="comment-score"></div><div class="comment-text"><p>These pictures can't be viewed; "You don't have permission to access the requested object."</p></div><div id="comment-1001-info" class="comment-info"><span class="comment-age">(18 Nov '10, 01:15)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="1006"></span><div id="comment-1006" class="comment"><div id="post-1006-score" class="comment-score"></div><div class="comment-text"><p>No answer yet, but maybe people can interpret the pics:</p><p>Links to the pics for those who don't like page source ;)</p><p>http://img3.laibafile.cn/getimgXXX/2/4/photo2/2010/11/18/middle/65401101_3131263_middle.jpg http://img3.laibafile.cn/getimgXXX/2/1/photo2/2010/11/18/middle/65401102_3131263_middle.jpg</p><p>If you get an error message stating Access Denied, click the Link inside the error statement to the file again. Worked fine for me using IE8</p></div><div id="comment-1006-info" class="comment-info"><span class="comment-age">(18 Nov '10, 05:20)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-988" class="comment-tools"></div><div class="clear"></div><div id="comment-988-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1009"></span>

<div id="answer-container-1009" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1009-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1009-score" class="post-score" title="current number of votes">0</div><span id="post-1009-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As the error says: "saving in au format supported only for alaw/ulaw streams". If there's PT_CN packets in the stream it cannot properly save these in au format, because there's no conversion for this payload type provided.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '10, 06:59</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></img></div></div><div id="comments-container-1009" class="comments-container"><span id="1020"></span><div id="comment-1020" class="comment"><div id="post-1020-score" class="comment-score"></div><div class="comment-text"><p>But I can play them using the functionality within Wireshark. And previously, I have also captured some packets where there seems to be CN payload and I CAN also save them as .au file.</p></div><div id="comment-1020-info" class="comment-info"><span class="comment-age">(19 Nov '10, 05:34)</span> <span class="comment-user userinfo">esolve</span></div></div></div><div id="comment-tools-1009" class="comment-tools"></div><div class="clear"></div><div id="comment-1009-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1052"></span>

<div id="answer-container-1052" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1052-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1052-score" class="post-score" title="current number of votes">0</div><span id="post-1052-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>However, I can play them using Telephony-&gt;VoIP Calls-&gt;Player SO I think wireshark can convert them?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '10, 16:58</strong></p><img src="https://secure.gravatar.com/avatar/2edef74a36aba8b70b94d9885864e7b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="esolve&#39;s gravatar image" /><p><span>esolve</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="esolve has no accepted answers">0%</span></p></div></div><div id="comments-container-1052" class="comments-container"></div><div id="comment-tools-1052" class="comment-tools"></div><div class="clear"></div><div id="comment-1052-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

