+++
type = "question"
title = "Code licensing"
description = '''Some code in Wireshark are dual-licensed under BSD-like and GPL licenses. The question is - can I use parts of such dual-licensed code in a proprietary product under terms of BSD license?'''
date = "2011-02-17T05:10:00Z"
lastmod = "2011-02-17T12:42:00Z"
weight = 2399
keywords = [ "dual-license", "licensing" ]
aliases = [ "/questions/2399" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Code licensing](/questions/2399/code-licensing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2399-score" class="post-score" title="current number of votes">0</div><span id="post-2399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Some code in Wireshark are dual-licensed under BSD-like and GPL licenses. The question is - can I use parts of such dual-licensed code in a proprietary product under terms of BSD license?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dual-license" rel="tag" title="see questions tagged &#39;dual-license&#39;">dual-license</span> <span class="post-tag tag-link-licensing" rel="tag" title="see questions tagged &#39;licensing&#39;">licensing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '11, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/a5b0803100bf5244e815c7a97efcfa97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="readytouse&#39;s gravatar image" /><p><span>readytouse</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="readytouse has no accepted answers">0%</span></p></div></div><div id="comments-container-2399" class="comments-container"><span id="2401"></span><div id="comment-2401" class="comment"><div id="post-2401-score" class="comment-score"></div><div class="comment-text"><p>Is there a particular file (or set of files) you are referring to ?</p></div><div id="comment-2401-info" class="comment-info"><span class="comment-age">(17 Feb '11, 09:31)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="2404"></span><div id="comment-2404" class="comment"><div id="post-2404-score" class="comment-score"></div><div class="comment-text"><p>For example: epan\crypt\airpdcap_tkip.c</p></div><div id="comment-2404-info" class="comment-info"><span class="comment-age">(17 Feb '11, 09:43)</span> <span class="comment-user userinfo">readytouse</span></div></div></div><div id="comment-tools-2399" class="comment-tools"></div><div class="clear"></div><div id="comment-2399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2406"></span>

<div id="answer-container-2406" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2406-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2406-score" class="post-score" title="current number of votes">0</div><span id="post-2406-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="readytouse has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, that's possible. It's up to the copyright holder to determine how (under what license) the code is disseminated. In this case they decided that the BSD license applies (also), so it can be used in accordance with that license.</p><p>If you wanted to use it under the GPL, you would have to adhere to that. Just make sure to abide by the requirements stated in the applicable license in your situation and you're good.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '11, 12:42</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2406" class="comments-container"></div><div id="comment-tools-2406" class="comment-tools"></div><div class="clear"></div><div id="comment-2406-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

