+++
type = "question"
title = "No interface details pane"
description = '''I am running 1.4.2 on a MacBook with OSX 10.6.5. I do not have an Interface Details window or any way to open one. Any help will be appreciated.'''
date = "2011-01-22T13:48:00Z"
lastmod = "2011-01-23T06:04:00Z"
weight = 1881
keywords = [ "interface", "details" ]
aliases = [ "/questions/1881" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No interface details pane](/questions/1881/no-interface-details-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1881-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1881-score" class="post-score" title="current number of votes">0</div><span id="post-1881-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am running 1.4.2 on a MacBook with OSX 10.6.5. I do not have an Interface Details window or any way to open one. Any help will be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-details" rel="tag" title="see questions tagged &#39;details&#39;">details</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '11, 13:48</strong></p><img src="https://secure.gravatar.com/avatar/8b039da459e96a10e792ba7b7082d36d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShadowUser&#39;s gravatar image" /><p><span>ShadowUser</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShadowUser has no accepted answers">0%</span></p></div></div><div id="comments-container-1881" class="comments-container"></div><div id="comment-tools-1881" class="comment-tools"></div><div class="clear"></div><div id="comment-1881-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1890"></span>

<div id="answer-container-1890" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1890-score" class="post-score" title="current number of votes">1</div><span id="post-1890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a dialog available <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChCapInterfaceDetailsSection.html">on Windows only</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '11, 06:04</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1890" class="comments-container"></div><div id="comment-tools-1890" class="comment-tools"></div><div class="clear"></div><div id="comment-1890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

