+++
type = "question"
title = "Local network slow down investigation: how to determine what causes?"
description = '''Hi! I have a LAN with PCs and printers. Gigabit unmanaged switch, and and mixed uplink 100 and 1000 megabit. Something on the net pereodicly slowing down performance. How to discover what?'''
date = "2015-07-22T01:49:00Z"
lastmod = "2015-07-22T13:30:00Z"
weight = 44363
keywords = [ "slow", "investigation" ]
aliases = [ "/questions/44363" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Local network slow down investigation: how to determine what causes?](/questions/44363/local-network-slow-down-investigation-how-to-determine-what-causes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44363-score" class="post-score" title="current number of votes">0</div><span id="post-44363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! I have a LAN with PCs and printers. Gigabit unmanaged switch, and and mixed uplink 100 and 1000 megabit. Something on the net pereodicly slowing down performance. How to discover what?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span> <span class="post-tag tag-link-investigation" rel="tag" title="see questions tagged &#39;investigation&#39;">investigation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '15, 01:49</strong></p><img src="https://secure.gravatar.com/avatar/e21115058e2273ea0daf4895c8a41230?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Rain&#39;s gravatar image" /><p><span>Bob Rain</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Rain has no accepted answers">0%</span></p></div></div><div id="comments-container-44363" class="comments-container"><span id="44386"></span><div id="comment-44386" class="comment"><div id="post-44386-score" class="comment-score"></div><div class="comment-text"><p>A possible point to start is here: <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div id="comment-44386-info" class="comment-info"><span class="comment-age">(22 Jul '15, 13:30)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-44363" class="comment-tools"></div><div class="clear"></div><div id="comment-44363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

