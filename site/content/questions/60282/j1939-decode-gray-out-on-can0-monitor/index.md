+++
type = "question"
title = "J1939 decode gray out on can0 monitor"
description = '''I am monitoring traffic from a LInux socketcan can0 interface and want to decode as J1939 to filter on PGN values. However, that option is grayed out. I&#x27;m using wireshark 1.2.1 on Raspberry Pi 3 run as root. What am I missing?'''
date = "2017-03-23T08:48:00Z"
lastmod = "2017-05-03T06:21:00Z"
weight = 60282
keywords = [ "j1939" ]
aliases = [ "/questions/60282" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [J1939 decode gray out on can0 monitor](/questions/60282/j1939-decode-gray-out-on-can0-monitor)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60282-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60282-score" class="post-score" title="current number of votes">0</div><span id="post-60282-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am monitoring traffic from a LInux socketcan can0 interface and want to decode as J1939 to filter on PGN values. However, that option is grayed out. I'm using wireshark 1.2.1 on Raspberry Pi 3 run as root. What am I missing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-j1939" rel="tag" title="see questions tagged &#39;j1939&#39;">j1939</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '17, 08:48</strong></p><img src="https://secure.gravatar.com/avatar/4f2c3af78296f1a6678007cd5f24d770?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wssri&#39;s gravatar image" /><p><span>wssri</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wssri has no accepted answers">0%</span></p></div></div><div id="comments-container-60282" class="comments-container"><span id="60303"></span><div id="comment-60303" class="comment"><div id="post-60303-score" class="comment-score"></div><div class="comment-text"><p>Can you confirm your Wireshark version?</p><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>, Google Drive, DropBox etc.?</p></div><div id="comment-60303-info" class="comment-info"><span class="comment-age">(24 Mar '17, 02:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="61204"></span><div id="comment-61204" class="comment"><div id="post-61204-score" class="comment-score"></div><div class="comment-text"><p>Did you find a solution for this? And do you have a capture you can share?</p></div><div id="comment-61204-info" class="comment-info"><span class="comment-age">(03 May '17, 06:21)</span> <span class="comment-user userinfo">mfcss</span></div></div></div><div id="comment-tools-60282" class="comment-tools"></div><div class="clear"></div><div id="comment-60282-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

