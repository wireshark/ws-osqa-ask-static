+++
type = "question"
title = "Pcap Analysis Help"
description = '''Can anyone help me with the analysis of the below two Pcaps. Is there anything malicious? Thankyou so so much! https://drive.google.com/open?id=0B8ChIxrRbYZqdGhONkFmS1pIdTA https://drive.google.com/open?id=0B8ChIxrRbYZqUnRhLVZjLVR5cFU'''
date = "2017-10-25T11:36:00Z"
lastmod = "2017-10-25T11:36:00Z"
weight = 64201
keywords = [ "malicious", "pcap" ]
aliases = [ "/questions/64201" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Pcap Analysis Help](/questions/64201/pcap-analysis-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64201-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64201-score" class="post-score" title="current number of votes">0</div><span id="post-64201-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone help me with the analysis of the below two Pcaps. Is there anything malicious?</p><p>Thankyou so so much!</p><p><a href="https://drive.google.com/open?id=0B8ChIxrRbYZqdGhONkFmS1pIdTA">https://drive.google.com/open?id=0B8ChIxrRbYZqdGhONkFmS1pIdTA</a></p><p><a href="https://drive.google.com/open?id=0B8ChIxrRbYZqUnRhLVZjLVR5cFU">https://drive.google.com/open?id=0B8ChIxrRbYZqUnRhLVZjLVR5cFU</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malicious" rel="tag" title="see questions tagged &#39;malicious&#39;">malicious</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '17, 11:36</strong></p><img src="https://secure.gravatar.com/avatar/6de7689342ca298db449c0ee0d55df3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Max123123&#39;s gravatar image" /><p><span>Max123123</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Max123123 has no accepted answers">0%</span></p></div></div><div id="comments-container-64201" class="comments-container"></div><div id="comment-tools-64201" class="comment-tools"></div><div class="clear"></div><div id="comment-64201-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

