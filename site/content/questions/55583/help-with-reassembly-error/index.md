+++
type = "question"
title = "Help with reassembly error"
description = '''I&#x27;ve been having the a reassembly error protocol and spurious retransmission error and I think its causing some download to fail. can you please help me fix the issue. Thanks 10501 101.754348 192.168.29.242 52.84.194.163 TCP 55 [TCP Keep-Alive] 5906→443 [ACK] Seq=13103 Ack=23655 Win=64277 Len=1[Reas...'''
date = "2016-09-15T23:35:00Z"
lastmod = "2016-09-16T12:17:00Z"
weight = 55583
keywords = [ "reassembly", "tcp" ]
aliases = [ "/questions/55583" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help with reassembly error](/questions/55583/help-with-reassembly-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55583-score" class="post-score" title="current number of votes">0</div><span id="post-55583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've been having the a reassembly error protocol and spurious retransmission error and I think its causing some download to fail. can you please help me fix the issue. Thanks</p><pre><code>10501   101.754348  192.168.29.242  52.84.194.163   TCP 55  [TCP Keep-Alive] 5906→443 [ACK] Seq=13103 Ack=23655 Win=64277 Len=1[Reassembly error, protocol TCP: New fragment overlaps old data (retransmission?)]</code></pre><p>I have capture file can you please look at it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reassembly" rel="tag" title="see questions tagged &#39;reassembly&#39;">reassembly</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '16, 23:35</strong></p><img src="https://secure.gravatar.com/avatar/b4fd3410565b696b0eb8f1d7171fe489?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DJDumlao&#39;s gravatar image" /><p><span>DJDumlao</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DJDumlao has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>16 Sep '16, 02:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-55583" class="comments-container"><span id="55603"></span><div id="comment-55603" class="comment"><div id="post-55603-score" class="comment-score"></div><div class="comment-text"><p>Can you provide a link to the capture file? You can put it on cloudshark.org or some file-sharing site.</p></div><div id="comment-55603-info" class="comment-info"><span class="comment-age">(16 Sep '16, 12:17)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-55583" class="comment-tools"></div><div class="clear"></div><div id="comment-55583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

