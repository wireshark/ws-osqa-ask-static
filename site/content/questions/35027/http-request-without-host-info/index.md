+++
type = "question"
title = "http request without host info"
description = '''Hi I have an apache server, which has a php code. from that php code I am calling python script to turn on the led. When I request the address from the web browser, it works fine, but when I request it from my office panel, it doesnt work. I can not do any changes to my office panel. from wireshark ...'''
date = "2014-07-31T03:53:00Z"
lastmod = "2014-08-01T09:30:00Z"
weight = 35027
keywords = [ "http", "wireshark" ]
aliases = [ "/questions/35027" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [http request without host info](/questions/35027/http-request-without-host-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35027-score" class="post-score" title="current number of votes">0</div><span id="post-35027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have an apache server, which has a php code. from that php code I am calling python script to turn on the led. When I request the address from the web browser, it works fine, but when I request it from my office panel, it doesnt work. I can not do any changes to my office panel. from wireshark files I have observed that http request is different. When I request from web browser it request with host info,connection type, cache control. but when I request it from panel it doesnt have host info cache control and connection type. I am new to networking world. Please help me out</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '14, 03:53</strong></p><img src="https://secure.gravatar.com/avatar/d58ee36544aed66df4d4d145ba0d4c19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zain_mbed&#39;s gravatar image" /><p><span>zain_mbed</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zain_mbed has no accepted answers">0%</span></p></div></div><div id="comments-container-35027" class="comments-container"><span id="35028"></span><div id="comment-35028" class="comment"><div id="post-35028-score" class="comment-score"></div><div class="comment-text"><p>Post the capture files to cloudshark for us to have a look.</p></div><div id="comment-35028-info" class="comment-info"><span class="comment-age">(31 Jul '14, 04:56)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-35027" class="comment-tools"></div><div class="clear"></div><div id="comment-35027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35041"></span>

<div id="answer-container-35041" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35041-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35041-score" class="post-score" title="current number of votes">0</div><span id="post-35041-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>here are some thoughts.</p><blockquote><p>but when I request it from panel it doesnt have host info cache control and connection type.</p></blockquote><p>Apparently the request generated by your 'office panel' (what is that?), does not work, because there is either something missing (you mentioned the Host: header), or the request is broken in another way (wrong headers, wrong request, etc.).</p><p>If the <strong>Host:</strong> header is missing, and Apache won't know how to process the request (depends on the apache configuration), as the <strong>Host:</strong> header helps Apache to identify the virtual host configuration. So, that's most likely the problem. Please ask your local Apache guru how to configure Apache in a way to accept the request without <strong>Host:</strong> header and to process it within the virtual host config that hosts your PHP code. <strong>However:</strong> If your PHP code is hosted on a rented multi-domain server, that won't be possible! In that case, you'l have to fix your 'office panel'.</p><blockquote><p>I can not do any changes to my office panel.</p></blockquote><p>Well, then there is no solution for you, other than configuring the web server, as I said above.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jul '14, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-35041" class="comments-container"><span id="35049"></span><div id="comment-35049" class="comment"><div id="post-35049-score" class="comment-score"></div><div class="comment-text"><p>Thank you Kurt for such a detailed reply. I would like you to ask, how can I configure my apache server in a way that it entertain the requests without header info. Kindly help me out. It is my first task. I have completed it, I just want to integrate it with panel</p></div><div id="comment-35049-info" class="comment-info"><span class="comment-age">(01 Aug '14, 02:22)</span> <span class="comment-user userinfo">zain_mbed</span></div></div><span id="35056"></span><div id="comment-35056" class="comment"><div id="post-35056-score" class="comment-score"></div><div class="comment-text"><blockquote><p>how can I configure my apache server in a way that it entertain the requests without header info.</p></blockquote><p>well, that's 'kind of' out of scope of this site and it depends on your current config. Please ask your local Apache guru or read the docs.</p><blockquote><p><a href="http://httpd.apache.org/docs/current/vhosts/examples.html">http://httpd.apache.org/docs/current/vhosts/examples.html</a></p></blockquote></div><div id="comment-35056-info" class="comment-info"><span class="comment-age">(01 Aug '14, 05:13)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="35071"></span><div id="comment-35071" class="comment"><div id="post-35071-score" class="comment-score"></div><div class="comment-text"><p>I have set a virtual host, kindly can any one assist me that how can I set up my server so that it can entertain requests without header information :/</p></div><div id="comment-35071-info" class="comment-info"><span class="comment-age">(01 Aug '14, 09:03)</span> <span class="comment-user userinfo">zain_mbed</span></div></div><span id="35072"></span><div id="comment-35072" class="comment"><div id="post-35072-score" class="comment-score"></div><div class="comment-text"><blockquote><p>kindly can any one assist me that how can I set up my server so that it can entertain requests without header information</p></blockquote><p>That should not be your goal. The HTTP RFC requires a Host: header to exist in a valid HTTP version 1.1 request. What you are trying to do is force Apache to respond to an invalid HTTP request, and like I said, that should not be your goal. Your goal should be to figure out how to modify the client so that it sends a RFC-compliant HTTP request to your webserver. I doubt anyone in this type of forum will be inclined to help you configure your webserver in this manner. And as Kurt clearly stated, configuring Apache is sort of outside the scope of this forum. You might get better results posting something to an Apache forum.</p></div><div id="comment-35072-info" class="comment-info"><span class="comment-age">(01 Aug '14, 09:30)</span> <span class="comment-user userinfo">smp</span></div></div></div><div id="comment-tools-35041" class="comment-tools"></div><div class="clear"></div><div id="comment-35041-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

