+++
type = "question"
title = "Why lose conection, using citrix?"
description = '''basically lose network connections and I fail to find the problem. Iam using Citrix, VMware and Windows Server i have this capture: https://www.cloudshark.org/captures/4442063982f4 Best Regards'''
date = "2014-04-23T07:38:00Z"
lastmod = "2014-04-23T11:28:00Z"
weight = 32101
keywords = [ "network", "lost", "citrix" ]
aliases = [ "/questions/32101" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why lose conection, using citrix?](/questions/32101/why-lose-conection-using-citrix)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32101-score" class="post-score" title="current number of votes">0</div><span id="post-32101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>basically lose network connections and I fail to find the problem. Iam using Citrix, VMware and Windows Server i have this capture:</p><p><a href="https://www.cloudshark.org/captures/4442063982f4">https://www.cloudshark.org/captures/4442063982f4</a></p><p>Best Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-lost" rel="tag" title="see questions tagged &#39;lost&#39;">lost</span> <span class="post-tag tag-link-citrix" rel="tag" title="see questions tagged &#39;citrix&#39;">citrix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '14, 07:38</strong></p><img src="https://secure.gravatar.com/avatar/3af2fe72df63430434222a02b57019f7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Iacobuzio&#39;s gravatar image" /><p><span>Iacobuzio</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Iacobuzio has no accepted answers">0%</span></p></div></div><div id="comments-container-32101" class="comments-container"><span id="32110"></span><div id="comment-32110" class="comment"><div id="post-32110-score" class="comment-score"></div><div class="comment-text"><p>Two comments.</p><p><strong>First:</strong></p><p>If you want somebody to analyze a capture file for you, you should provide at least the following information</p><ul><li>client IP</li><li>server IP</li><li>protocol used (RDP, Citrix ICA, whatever)</li><li>a detailed description of the problem. "Basically lose network connections" isn't very helpful</li><li>the exact time (more or less) when you believe to have seen the problem (client disconnect or whatever).</li></ul><p><strong>Second:</strong></p><p>I don't see any signs for <strong>real</strong> network problems in the capture file. There are some FCS errors, but none of them resulted in lost network connectivity. So, either you provided the wrong capture file, or you captured something that is unrelated to you network problems. So, please provide the required information (see above) and a new capture file that contains a somehow interrupted connection.</p><p>Regards<br />
Kurt</p></div><div id="comment-32110-info" class="comment-info"><span class="comment-age">(23 Apr '14, 11:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-32101" class="comment-tools"></div><div class="clear"></div><div id="comment-32101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

