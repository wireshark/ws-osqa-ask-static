+++
type = "question"
title = "Capturing interface-local multicast"
description = '''I have recently upgraded to Wireshark from 1.8.11 to the 1.10.3, and Winpcap from 4.1.2 to 4.1.3, on Windows 7, 64 bit. I used to be able to see interface-local multicast packets that my applications use to transmit data to each other i.e. addresses ff01:... I no longer can. Can anyone suggest why n...'''
date = "2013-12-06T09:27:00Z"
lastmod = "2013-12-06T09:27:00Z"
weight = 27875
keywords = [ "multicast", "interface-local" ]
aliases = [ "/questions/27875" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing interface-local multicast](/questions/27875/capturing-interface-local-multicast)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27875-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27875-score" class="post-score" title="current number of votes">0</div><span id="post-27875-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have recently upgraded to Wireshark from 1.8.11 to the 1.10.3, and Winpcap from 4.1.2 to 4.1.3, on Windows 7, 64 bit. I used to be able to see interface-local multicast packets that my applications use to transmit data to each other i.e. addresses ff01:... I no longer can. Can anyone suggest why not or what is needed to do so? (Or is it a figment of my imagination that it used to work!) Many thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multicast" rel="tag" title="see questions tagged &#39;multicast&#39;">multicast</span> <span class="post-tag tag-link-interface-local" rel="tag" title="see questions tagged &#39;interface-local&#39;">interface-local</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '13, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/eb4fdb94b231c3fde6b93910fd5f3c35?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KPR&#39;s gravatar image" /><p><span>KPR</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KPR has no accepted answers">0%</span></p></div></div><div id="comments-container-27875" class="comments-container"></div><div id="comment-tools-27875" class="comment-tools"></div><div class="clear"></div><div id="comment-27875-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

