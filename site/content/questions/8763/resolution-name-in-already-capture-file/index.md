+++
type = "question"
title = "Resolution name in already capture file"
description = '''hi all I have a pcap file about 9000 file , not resolution name I want convert to csv format and I want a filed of ip.dst like this &#x27;www.google.com&#x27; who have any idea to convent file with resolution name  this my script tshark -r c:svr.pcap -T fields -e frame.number -e frame.time -e eth.src -e eth.d...'''
date = "2012-02-01T21:53:00Z"
lastmod = "2012-02-01T21:53:00Z"
weight = 8763
keywords = [ "dns" ]
aliases = [ "/questions/8763" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Resolution name in already capture file](/questions/8763/resolution-name-in-already-capture-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8763-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8763-score" class="post-score" title="current number of votes">0</div><span id="post-8763-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi all</p><p>I have a pcap file about 9000 file , not resolution name</p><p>I want convert to csv format and I want a filed of ip.dst like this 'www.google.com'</p><p>who have any idea to convent file with resolution name<br />
</p><p>this my script tshark -r c:svr.pcap -T fields -e frame.number -e frame.time -e eth.src -e eth.dst -e ip.src -e ip.dst -e ip.proto -e frame.len -e data.len -e tcp.srcport -e tcp.dstport -e http.user_aget -E header=y -E separator=, -E quote=d -E occurrence=f &gt; c:test.csv</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '12, 21:53</strong></p><img src="https://secure.gravatar.com/avatar/65689fc46ee57270e3d30d4b6c90271e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jakkapun%20Kanyasing&#39;s gravatar image" /><p><span>Jakkapun Kan...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jakkapun Kanyasing has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-8763" class="comments-container"></div><div id="comment-tools-8763" class="comment-tools"></div><div class="clear"></div><div id="comment-8763-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

