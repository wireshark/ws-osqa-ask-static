+++
type = "question"
title = "wireshark 2.0.2 tracing VoIP calls and stream, new howto ???"
description = '''Hi All I had to remove the update to 2.x because I could not find any up to date guide how the new interface works, a lot of simplicity has been removed and RTP stream are completely in accessible unless I spend hours typing because copy and past is not always an option. Its impossible to get the UD...'''
date = "2016-03-23T15:27:00Z"
lastmod = "2016-03-24T05:48:00Z"
weight = 51138
keywords = [ "voip" ]
aliases = [ "/questions/51138" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark 2.0.2 tracing VoIP calls and stream, new howto ???](/questions/51138/wireshark-202-tracing-voip-calls-and-stream-new-howto)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51138-score" class="post-score" title="current number of votes">0</div><span id="post-51138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All I had to remove the update to 2.x because I could not find any up to date guide how the new interface works, a lot of simplicity has been removed and RTP stream are completely in accessible unless I spend hours typing because copy and past is not always an option. Its impossible to get the UDP/RTP streams and flows for the whole conversation as it seems. Has anyone any updated docs to that problem.# thanks Torsten</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '16, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/5ec86ff672f1df1a09addf7a1accbb10?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="torstenker&#39;s gravatar image" /><p><span>torstenker</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="torstenker has no accepted answers">0%</span></p></div></div><div id="comments-container-51138" class="comments-container"></div><div id="comment-tools-51138" class="comment-tools"></div><div class="clear"></div><div id="comment-51138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51150"></span>

<div id="answer-container-51150" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51150-score" class="post-score" title="current number of votes">0</div><span id="post-51150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you do a searching on here? You could have found <a href="https://ask.wireshark.org/questions/48159">this</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Mar '16, 05:48</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-51150" class="comments-container"></div><div id="comment-tools-51150" class="comment-tools"></div><div class="clear"></div><div id="comment-51150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

