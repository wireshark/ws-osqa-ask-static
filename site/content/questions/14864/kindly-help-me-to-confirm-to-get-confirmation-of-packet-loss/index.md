+++
type = "question"
title = "kindly help me to confirm to get confirmation of packet loss"
description = '''Hi , Somebody can help there is file missing from client to server&amp;amp; observed below massage in the snoop logs.kindly help me to confirm is there packet drop during the data transmission. &quot;21618&quot;,&quot;5135.427497&quot;,&quot;172.29.0.52&quot;,&quot;10.31.96.9&quot;,&quot;TCP&quot;,&quot;[TCP segment of a reassembled PDU]&quot; &quot;21619&quot;,&quot;5135.4275...'''
date = "2012-10-10T02:10:00Z"
lastmod = "2012-10-10T11:56:00Z"
weight = 14864
keywords = [ "helpme", "kindly" ]
aliases = [ "/questions/14864" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [kindly help me to confirm to get confirmation of packet loss](/questions/14864/kindly-help-me-to-confirm-to-get-confirmation-of-packet-loss)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14864-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14864-score" class="post-score" title="current number of votes">0</div><span id="post-14864-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ,</p><p>Somebody can help there is file missing from client to server&amp; observed below massage in the snoop logs.kindly help me to confirm is there packet drop during the data transmission.</p><p>"21618","5135.427497","172.29.0.52","10.31.96.9","TCP","[TCP segment of a reassembled PDU]" "21619","5135.427510","172.29.0.52","10.31.96.9","TCP","[TCP Dup ACK 21618#1] 34560 &gt; ssh [ACK] Seq=302100 Ack=953008 Win=49640 Len=0 SLE=954468 SRE=955928" "21620","5135.517490","10.31.96.9","172.29.0.52","SSHv2","[TCP Previous segment lost] Encrypted response packet len=1460" "21621","5135.517525","172.29.0.52","10.31.96.9","TCP","[TCP Dup ACK 21618#2] 34560 &gt; ssh [ACK] Seq=302100 Ack=953008 Win=49640 Len=0 SLE=957388 SRE=958848 SLE=954468 SRE=955928"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-helpme" rel="tag" title="see questions tagged &#39;helpme&#39;">helpme</span> <span class="post-tag tag-link-kindly" rel="tag" title="see questions tagged &#39;kindly&#39;">kindly</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '12, 02:10</strong></p><img src="https://secure.gravatar.com/avatar/094215fa7f0a2a6a26df3ef08dd7cac7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="malli_mmk&#39;s gravatar image" /><p><span>malli_mmk</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="malli_mmk has no accepted answers">0%</span></p></div></div><div id="comments-container-14864" class="comments-container"></div><div id="comment-tools-14864" class="comment-tools"></div><div class="clear"></div><div id="comment-14864-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14903"></span>

<div id="answer-container-14903" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14903-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14903-score" class="post-score" title="current number of votes">0</div><span id="post-14903-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Based on the information provided, I can confirm packet loss. The error messages '[TCP Dup ACK]' and '[TCP Previous segment lost]' are a clear sign of packet loss.</p><p>Without further information it's impossible to tell why/where the loss takes place!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '12, 11:56</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-14903" class="comments-container"></div><div id="comment-tools-14903" class="comment-tools"></div><div class="clear"></div><div id="comment-14903-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

