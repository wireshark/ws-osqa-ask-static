+++
type = "question"
title = "There are no interfaces on which a capture can be done"
description = '''Hi expert, My laptop is running on Windows 7 SP1 (32 bit). I successfully installed the version 1.4.7. Unfortunately, the application can&#x27;t run due to no interface detected. I already did uninstall/install of the program but still problem exists. I also tried to install the old version but still the...'''
date = "2011-06-06T06:04:00Z"
lastmod = "2011-06-06T18:10:00Z"
weight = 4399
keywords = [ "interfaces" ]
aliases = [ "/questions/4399" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [There are no interfaces on which a capture can be done](/questions/4399/there-are-no-interfaces-on-which-a-capture-can-be-done)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4399-score" class="post-score" title="current number of votes">0</div><span id="post-4399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi expert,</p><p>My laptop is running on Windows 7 SP1 (32 bit). I successfully installed the version 1.4.7. Unfortunately, the application can't run due to no interface detected. I already did uninstall/install of the program but still problem exists. I also tried to install the old version but still the problem persists. For your assistance. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jun '11, 06:04</strong></p><img src="https://secure.gravatar.com/avatar/f2e865bd253fe49208d10ccd569a7ca7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mat&#39;s gravatar image" /><p><span>mat</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mat has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jun '11, 06:08</strong> </span></p></div></div><div id="comments-container-4399" class="comments-container"></div><div id="comment-tools-4399" class="comment-tools"></div><div class="clear"></div><div id="comment-4399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4415"></span>

<div id="answer-container-4415" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4415-score" class="post-score" title="current number of votes">0</div><span id="post-4415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mat has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When I installed the WinPcap before, I didn't select the automatic detect of interfaces. I installed it again with the option automatic detect. I run the Wireshark application, I can now capture a trace. Regards,</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jun '11, 18:10</strong></p><img src="https://secure.gravatar.com/avatar/f2e865bd253fe49208d10ccd569a7ca7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mat&#39;s gravatar image" /><p><span>mat</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mat has one accepted answer">100%</span></p></div></div><div id="comments-container-4415" class="comments-container"></div><div id="comment-tools-4415" class="comment-tools"></div><div class="clear"></div><div id="comment-4415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4400"></span>

<div id="answer-container-4400" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4400-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4400-score" class="post-score" title="current number of votes">1</div><span id="post-4400-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Make sure winpcap is installed. http://winpcap.org And try running it using administrator login. Windows 7 and Vista have more access controls, controlled through the UAC, built in to them than xp does.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jun '11, 07:03</strong></p><img src="https://secure.gravatar.com/avatar/1f3966b6e9de3a63326e2d3fd51c8c04?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John_Modlin&#39;s gravatar image" /><p><span>John_Modlin</span><br />
<span class="score" title="120 reputation points">120</span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John_Modlin has no accepted answers">0%</span></p></div></div><div id="comments-container-4400" class="comments-container"></div><div id="comment-tools-4400" class="comment-tools"></div><div class="clear"></div><div id="comment-4400-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

