+++
type = "question"
title = "PKTC 175 Unknown KMMID 0x4d"
description = '''Hello folks, I&#x27;m troubleshooting a network problem at a school and I&#x27;m having difficulties finding the problematic node. It seems as if there&#x27;s a node that&#x27;s causing the spanning tree protocol to restart every ten minutes, leaving the entire network completely dead for half a minute in the process. ...'''
date = "2012-02-08T05:17:00Z"
lastmod = "2012-02-14T14:29:00Z"
weight = 8898
keywords = [ "unknown" ]
aliases = [ "/questions/8898" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [PKTC 175 Unknown KMMID 0x4d](/questions/8898/pktc-175-unknown-kmmid-0x4d)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8898-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8898-score" class="post-score" title="current number of votes">0</div><span id="post-8898-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello folks,</p><p>I'm troubleshooting a network problem at a school and I'm having difficulties finding the problematic node. It seems as if there's a node that's causing the spanning tree protocol to restart every ten minutes, leaving the entire network completely dead for half a minute in the process. At least this is my gathered info from talking to several network techs and HP support.</p><p>Trying to analyse the traffic isn't easy in my book, and no matter how many times I've used Wireshark and tried to understand the logs, I don't have the education to fully understand what each line means. Hence I need your help.</p><p>There's particulary one line that I don't understand, and can't seem to find any info on. It's this line:</p><p>1700 90.108443 10.0.50.240 239.255.255.250 PKTC 175 Unknown KMMID 0x4d (Unknown DOI 0x2d)</p><p>Does any one know what this means?</p><p>Here's the log that captures the error: http://linford.se/temp/net_log.pcap</p><p>And here's more info about the problem and our troubleshooting: http://communities.vmware.com/message/1905462#1905462</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '12, 05:17</strong></p><img src="https://secure.gravatar.com/avatar/efe50a37e132db895e7bf6a119e26439?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Phatsta&#39;s gravatar image" /><p><span>Phatsta</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Phatsta has no accepted answers">0%</span></p></div></div><div id="comments-container-8898" class="comments-container"></div><div id="comment-tools-8898" class="comment-tools"></div><div class="clear"></div><div id="comment-8898-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9006"></span>

<div id="answer-container-9006" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9006-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9006-score" class="post-score" title="current number of votes">0</div><span id="post-9006-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Bump.</p><p>Anyone have any idea what so ever?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Feb '12, 14:29</strong></p><img src="https://secure.gravatar.com/avatar/efe50a37e132db895e7bf6a119e26439?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Phatsta&#39;s gravatar image" /><p><span>Phatsta</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Phatsta has no accepted answers">0%</span></p></div></div><div id="comments-container-9006" class="comments-container"></div><div id="comment-tools-9006" class="comment-tools"></div><div class="clear"></div><div id="comment-9006-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

