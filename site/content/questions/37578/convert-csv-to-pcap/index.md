+++
type = "question"
title = "convert csv to pcap"
description = '''I am working on database with CSV extension. i need to import this database to wireshark. how can i do it?'''
date = "2014-11-04T21:07:00Z"
lastmod = "2014-11-04T21:25:00Z"
weight = 37578
keywords = [ "csv" ]
aliases = [ "/questions/37578" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [convert csv to pcap](/questions/37578/convert-csv-to-pcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37578-score" class="post-score" title="current number of votes">0</div><span id="post-37578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working on database with CSV extension. i need to import this database to wireshark. how can i do it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-csv" rel="tag" title="see questions tagged &#39;csv&#39;">csv</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '14, 21:07</strong></p><img src="https://secure.gravatar.com/avatar/6bb9a7105c59980d5b8a4945ccec4b7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zohre&#39;s gravatar image" /><p><span>zohre</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zohre has no accepted answers">0%</span></p></div></div><div id="comments-container-37578" class="comments-container"></div><div id="comment-tools-37578" class="comment-tools"></div><div class="clear"></div><div id="comment-37578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37579"></span>

<div id="answer-container-37579" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37579-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37579-score" class="post-score" title="current number of votes">1</div><span id="post-37579-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Write a program that takes an csv file as input and writes out a pcap(ng) file.That would only work if the csv coded input contains every single bit of the packet to be created or if it could be deduced from the input.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Nov '14, 21:25</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-37579" class="comments-container"></div><div id="comment-tools-37579" class="comment-tools"></div><div class="clear"></div><div id="comment-37579-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

