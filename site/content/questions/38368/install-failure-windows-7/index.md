+++
type = "question"
title = "Install Failure Windows 7"
description = '''The error states: Wireshark installer for 64 bit has stopped working. A problem caused the program to stop working correctly. This occurs regardless of if I run as admin, or do the 32 bit or 64 bit version or if I use the default install directory or custom. The exact moment of failure is always the...'''
date = "2014-12-05T10:07:00Z"
lastmod = "2014-12-07T12:25:00Z"
weight = 38368
keywords = [ "fails", "install" ]
aliases = [ "/questions/38368" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Install Failure Windows 7](/questions/38368/install-failure-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38368-score" class="post-score" title="current number of votes">0</div><span id="post-38368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The error states: Wireshark installer for 64 bit has stopped working. A problem caused the program to stop working correctly.</p><p>This occurs regardless of if I run as admin, or do the 32 bit or 64 bit version or if I use the default install directory or custom.</p><p>The exact moment of failure is always the same: The last line in the window always says C:\Users\XXX</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fails" rel="tag" title="see questions tagged &#39;fails&#39;">fails</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '14, 10:07</strong></p><img src="https://secure.gravatar.com/avatar/35ac6c1ccd3c66ecc41ed70578663ab1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alanar&#39;s gravatar image" /><p><span>Alanar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alanar has no accepted answers">0%</span></p></div></div><div id="comments-container-38368" class="comments-container"></div><div id="comment-tools-38368" class="comment-tools"></div><div class="clear"></div><div id="comment-38368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38419"></span>

<div id="answer-container-38419" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38419-score" class="post-score" title="current number of votes">0</div><span id="post-38419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Run <strong>chkdsk /r</strong> and reboot.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '14, 12:05</strong></p><img src="https://secure.gravatar.com/avatar/1391fe339a48dabf8b1060889cec3d76?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kennybobs&#39;s gravatar image" /><p><span>Kennybobs</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kennybobs has no accepted answers">0%</span></p></div></div><div id="comments-container-38419" class="comments-container"><span id="38420"></span><div id="comment-38420" class="comment"><div id="post-38420-score" class="comment-score"></div><div class="comment-text"><p>Thanks. Turns out I had some Malware preventing the install and some corrupted System files. I am in Wireshark heaven now.</p></div><div id="comment-38420-info" class="comment-info"><span class="comment-age">(07 Dec '14, 12:25)</span> <span class="comment-user userinfo">Alanar</span></div></div></div><div id="comment-tools-38419" class="comment-tools"></div><div class="clear"></div><div id="comment-38419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

