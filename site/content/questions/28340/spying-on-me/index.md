+++
type = "question"
title = "[closed] Spying on me?"
description = '''Can anyone tell me, in simple terms, how I can tell if wifi monitoring software has been installed on my pc? I&#x27;ve read about Gargoyle and colasoft etc. I just want to know if what happens on my tablet is private, or if someone is snooping on my wifi usage. There is someone in the house that has acce...'''
date = "2013-12-23T07:32:00Z"
lastmod = "2013-12-23T09:22:00Z"
weight = 28340
keywords = [ "wifi", "monitoring", "capturing" ]
aliases = [ "/questions/28340" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Spying on me?](/questions/28340/spying-on-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28340-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28340-score" class="post-score" title="current number of votes">0</div><span id="post-28340-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone tell me, in simple terms, how I can tell if wifi monitoring software has been installed on my pc? I've read about Gargoyle and colasoft etc. I just want to know if what happens on my tablet is private, or if someone is snooping on my wifi usage. There is someone in the house that has access to the PC and is very computer savy.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span> <span class="post-tag tag-link-capturing" rel="tag" title="see questions tagged &#39;capturing&#39;">capturing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Dec '13, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/64d46bbeb522dbfb3d6d832409099ccf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joedecember&#39;s gravatar image" /><p><span>joedecember</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joedecember has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>23 Dec '13, 09:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28340" class="comments-container"><span id="28344"></span><div id="comment-28344" class="comment"><div id="post-28344-score" class="comment-score"></div><div class="comment-text"><p>This is not related to Wireshark.</p></div><div id="comment-28344-info" class="comment-info"><span class="comment-age">(23 Dec '13, 09:22)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28340" class="comment-tools"></div><div class="clear"></div><div id="comment-28340-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Kurt Knochner 23 Dec '13, 09:21

</div>

</div>

</div>

