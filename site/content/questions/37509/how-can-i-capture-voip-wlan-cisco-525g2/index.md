+++
type = "question"
title = "How can I capture VoIP WLAN Cisco 525G2?"
description = '''Have Cisco 525G2 VoIP Phone via WLAN either to Ubiquity AP or Linksys E1200 What is the setup to record packets and diagnose'''
date = "2014-10-31T12:25:00Z"
lastmod = "2014-10-31T12:25:00Z"
weight = 37509
keywords = [ "voip" ]
aliases = [ "/questions/37509" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I capture VoIP WLAN Cisco 525G2?](/questions/37509/how-can-i-capture-voip-wlan-cisco-525g2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37509-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37509-score" class="post-score" title="current number of votes">0</div><span id="post-37509-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Have Cisco 525G2 VoIP Phone via WLAN either to Ubiquity AP or Linksys E1200 What is the setup to record packets and diagnose</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '14, 12:25</strong></p><img src="https://secure.gravatar.com/avatar/43121681f9839256ec45a43b4a7e734b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ddkwireshark&#39;s gravatar image" /><p><span>ddkwireshark</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ddkwireshark has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Oct '14, 12:29</strong> </span></p></div></div><div id="comments-container-37509" class="comments-container"></div><div id="comment-tools-37509" class="comment-tools"></div><div class="clear"></div><div id="comment-37509-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

