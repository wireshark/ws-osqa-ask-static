+++
type = "question"
title = "Problem with the command line &quot;stop&quot;"
description = '''I have a problem, I can&#x27;t stop wireshark with a command line. I used the command line to start wireshark : &quot;wireshark -i 3 -k&quot; and after I would like to stop wireshark with a new command line. But when I use the command &quot;wireshark -a duration:1&quot;, I have another instance of wireshark which opens. Is ...'''
date = "2011-10-06T06:24:00Z"
lastmod = "2011-10-08T05:55:00Z"
weight = 6752
keywords = [ "stop", "start", "line", "command" ]
aliases = [ "/questions/6752" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Problem with the command line "stop"](/questions/6752/problem-with-the-command-line-stop)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6752-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6752-score" class="post-score" title="current number of votes">0</div><span id="post-6752-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a problem, I can't stop wireshark with a command line. I used the command line to start wireshark : <strong>"wireshark -i 3 -k"</strong> and after I would like to stop wireshark with a new command line. But when I use the command <strong>"wireshark -a duration:1"</strong>, I have another instance of wireshark which opens. Is it possible to start and stop wireshark with 2 commands line ?</p><p>Thanks for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-stop" rel="tag" title="see questions tagged &#39;stop&#39;">stop</span> <span class="post-tag tag-link-start" rel="tag" title="see questions tagged &#39;start&#39;">start</span> <span class="post-tag tag-link-line" rel="tag" title="see questions tagged &#39;line&#39;">line</span> <span class="post-tag tag-link-command" rel="tag" title="see questions tagged &#39;command&#39;">command</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '11, 06:24</strong></p><img src="https://secure.gravatar.com/avatar/43af7152ed326c592397b0433c5a2c5d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Patrick&#39;s gravatar image" /><p><span>Patrick</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Patrick has no accepted answers">0%</span></p></div></div><div id="comments-container-6752" class="comments-container"></div><div id="comment-tools-6752" class="comment-tools"></div><div class="clear"></div><div id="comment-6752-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="6796"></span>

<div id="answer-container-6796" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6796-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6796-score" class="post-score" title="current number of votes">1</div><span id="post-6796-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, it is not possible to start and stop Wireshark with two separate commands. Wireshark's initial options can be set on the command line, and a capture can be started from the command line, but the only way to stop a capture from the user interface is to click the "Stop" button.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '11, 13:28</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-6796" class="comments-container"></div><div id="comment-tools-6796" class="comment-tools"></div><div class="clear"></div><div id="comment-6796-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="6758"></span>

<div id="answer-container-6758" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6758-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6758-score" class="post-score" title="current number of votes">0</div><span id="post-6758-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Use just one command line:<br />
wireshark -i 3 -k -a duration:60</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Oct '11, 13:15</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></p></div></div><div id="comments-container-6758" class="comments-container"><span id="6770"></span><div id="comment-6770" class="comment"><div id="post-6770-score" class="comment-score"></div><div class="comment-text"><p>I want to use 2 commands because I start Wireshark and I wait an event with another software (Teststand). When I have this event, I stop wireshark. The duration between the start and the event is never the same and I want to stop the capture immediatly after the event.</p></div><div id="comment-6770-info" class="comment-info"><span class="comment-age">(07 Oct '11, 00:51)</span> <span class="comment-user userinfo">Patrick</span></div></div><span id="6803"></span><div id="comment-6803" class="comment"><div id="post-6803-score" class="comment-score"></div><div class="comment-text"><p>Is there any particular reason why you don't use tshark?<br />
You can run tshark and hit CTRL+C, after the event has happened:<br />
$ tshark -i 3 -w myfile.pcap</p></div><div id="comment-6803-info" class="comment-info"><span class="comment-age">(08 Oct '11, 05:25)</span> <span class="comment-user userinfo">joke</span></div></div><span id="6804"></span><div id="comment-6804" class="comment"><div id="post-6804-score" class="comment-score"></div><div class="comment-text"><p>If you're doing long term captures and don't need to see dissection as the capture takes place then using dumpcap is probably even better than using tshark.</p><p>dumpcap just writes the capture to a file which can then be analyzed using Wireshark.</p><p>See http://wiki.wireshark.org/KnownBugs/OutOfMemory (especially the "workarounds" section).</p></div><div id="comment-6804-info" class="comment-info"><span class="comment-age">(08 Oct '11, 05:55)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-6758" class="comment-tools"></div><div class="clear"></div><div id="comment-6758-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

