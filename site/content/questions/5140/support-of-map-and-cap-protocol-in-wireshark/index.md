+++
type = "question"
title = "Support of MAP and CAP Protocol in Wireshark"
description = '''I would be grateful, if somebody tell whether wireshark capture MAP and CAP Protocols packets. Thanks,'''
date = "2011-07-19T20:48:00Z"
lastmod = "2011-07-20T14:14:00Z"
weight = 5140
keywords = [ "map", "cap" ]
aliases = [ "/questions/5140" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Support of MAP and CAP Protocol in Wireshark](/questions/5140/support-of-map-and-cap-protocol-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5140-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5140-score" class="post-score" title="current number of votes">0</div><span id="post-5140-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would be grateful, if somebody tell whether wireshark capture MAP and CAP Protocols packets.</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-map" rel="tag" title="see questions tagged &#39;map&#39;">map</span> <span class="post-tag tag-link-cap" rel="tag" title="see questions tagged &#39;cap&#39;">cap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '11, 20:48</strong></p><img src="https://secure.gravatar.com/avatar/9883c4cccd8225710c066787a67605f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="arvind&#39;s gravatar image" /><p><span>arvind</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="arvind has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:07</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-5140" class="comments-container"><span id="5144"></span><div id="comment-5144" class="comment"><div id="post-5144-score" class="comment-score"></div><div class="comment-text"><p>Sorry, death by acronym here... can you elaborate on what protocols you mean by telling us a bit more?</p></div><div id="comment-5144-info" class="comment-info"><span class="comment-age">(20 Jul '11, 07:56)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-5140" class="comment-tools"></div><div class="clear"></div><div id="comment-5140-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5149"></span>

<div id="answer-container-5149" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5149-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5149-score" class="post-score" title="current number of votes">0</div><span id="post-5149-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark dissects GSM MAP and CAMEL Application Protocol(CAP).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '11, 14:14</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-5149" class="comments-container"></div><div id="comment-tools-5149" class="comment-tools"></div><div class="clear"></div><div id="comment-5149-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

