+++
type = "question"
title = "Company usage confirmation"
description = '''Dear Wireshark colleagues, I had to ask for your confirmation allowing Comcel S.A. company to use Wireshark software due to some local regulations. Please confirm us if we can use your software.  Thanks in advance! Comcel S.A.'''
date = "2012-06-01T14:43:00Z"
lastmod = "2012-06-01T16:23:00Z"
weight = 11553
keywords = [ "approval", "license" ]
aliases = [ "/questions/11553" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Company usage confirmation](/questions/11553/company-usage-confirmation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11553-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11553-score" class="post-score" title="current number of votes">0</div><span id="post-11553-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Wireshark colleagues, I had to ask for your confirmation allowing Comcel S.A. company to use Wireshark software due to some local regulations. Please confirm us if we can use your software.</p><p>Thanks in advance!</p><p>Comcel S.A.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-approval" rel="tag" title="see questions tagged &#39;approval&#39;">approval</span> <span class="post-tag tag-link-license" rel="tag" title="see questions tagged &#39;license&#39;">license</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '12, 14:43</strong></p><img src="https://secure.gravatar.com/avatar/def788e142071f5ef1595e6ebae10d60?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Telco%20Eng&#39;s gravatar image" /><p><span>Telco Eng</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Telco Eng has no accepted answers">0%</span></p></div></div><div id="comments-container-11553" class="comments-container"></div><div id="comment-tools-11553" class="comment-tools"></div><div class="clear"></div><div id="comment-11553-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="11554"></span>

<div id="answer-container-11554" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11554-score" class="post-score" title="current number of votes">0</div><span id="post-11554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Short answer: Yes, you can use it. Wireshark is free software.</p><p>Longer answer: See <a href="http://www.wireshark.org/faq.html#q1.6">Question 1.6 on theWireshark FAQ page</a> for a slightly longer explanation, including a link to the license under which Wireshark is released.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jun '12, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-11554" class="comments-container"></div><div id="comment-tools-11554" class="comment-tools"></div><div class="clear"></div><div id="comment-11554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="11556"></span>

<div id="answer-container-11556" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11556-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11556-score" class="post-score" title="current number of votes">0</div><span id="post-11556-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please also check the Export Regulations:</p><p><a href="http://www.wireshark.org/export.html">http://www.wireshark.org/export.html</a></p><blockquote><p>to use Wireshark software due to some local regulations. P</p></blockquote><p>If there are local regulations (in your country) that prohibit the use of wireshark (crypto, privacy, etc.), I suggest you better get a permission from the local authorities.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jun '12, 16:23</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-11556" class="comments-container"></div><div id="comment-tools-11556" class="comment-tools"></div><div class="clear"></div><div id="comment-11556-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

