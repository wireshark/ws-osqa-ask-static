+++
type = "question"
title = "Could not find dissector for: &#x27;HTTP&#x27;"
description = '''Under &quot;Protocols &amp;gt; SSL&quot; I am trying to add a new entry in the &quot;RSA Key list&quot;. I fill out the fields, click &quot;OK&quot;, and get this error -  &quot;error in column &#x27;Protocol&#x27;: Could not find dissector for: &#x27;HTTP&#x27;&quot; any ideas what is going on here&quot; TIA, Phillip'''
date = "2011-08-29T10:31:00Z"
lastmod = "2011-08-29T14:26:00Z"
weight = 5921
keywords = [ "dissector", "http" ]
aliases = [ "/questions/5921" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Could not find dissector for: 'HTTP'](/questions/5921/could-not-find-dissector-for-http)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5921-score" class="post-score" title="current number of votes">0</div><span id="post-5921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Under "Protocols &gt; SSL" I am trying to add a new entry in the "RSA Key list". I fill out the fields, click "OK", and get this error -</p><p>"error in column 'Protocol': Could not find dissector for: 'HTTP'"</p><p>any ideas what is going on here"</p><p>TIA,</p><p>Phillip</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '11, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/e19d5ed148848d6c50593d26e7f95a2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="audiphilth&#39;s gravatar image" /><p><span>audiphilth</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="audiphilth has no accepted answers">0%</span></p></div></div><div id="comments-container-5921" class="comments-container"></div><div id="comment-tools-5921" class="comment-tools"></div><div class="clear"></div><div id="comment-5921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5929"></span>

<div id="answer-container-5929" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5929-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5929-score" class="post-score" title="current number of votes">2</div><span id="post-5929-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>use <code>http</code>, not <code>HTTP</code></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Aug '11, 14:26</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-5929" class="comments-container"></div><div id="comment-tools-5929" class="comment-tools"></div><div class="clear"></div><div id="comment-5929-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

