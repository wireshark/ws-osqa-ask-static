+++
type = "question"
title = "Wireless intel client not seeing any traffic"
description = '''I have wireshark on my pc that is connected to wireless network, but not seeing any outgoing packets to websites. I can see everything fine on my mac client, but not on the intel laptop client. same network.  any ideas? '''
date = "2014-08-11T09:56:00Z"
lastmod = "2014-08-11T10:00:00Z"
weight = 35413
keywords = [ "intel", "forticlient" ]
aliases = [ "/questions/35413" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless intel client not seeing any traffic](/questions/35413/wireless-intel-client-not-seeing-any-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35413-score" class="post-score" title="current number of votes">0</div><span id="post-35413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have wireshark on my pc that is connected to wireless network, but not seeing any outgoing packets to websites. I can see everything fine on my mac client, but not on the intel laptop client. same network.<br />
</p><p>any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-intel" rel="tag" title="see questions tagged &#39;intel&#39;">intel</span> <span class="post-tag tag-link-forticlient" rel="tag" title="see questions tagged &#39;forticlient&#39;">forticlient</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Aug '14, 09:56</strong></p><img src="https://secure.gravatar.com/avatar/14e8ff606c6f737f476604034a184baa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="catcurio&#39;s gravatar image" /><p><span>catcurio</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="catcurio has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-35413" class="comments-container"><span id="35414"></span><div id="comment-35414" class="comment"><div id="post-35414-score" class="comment-score"></div><div class="comment-text"><p>Is this a duplicate of your earlier questions:</p><p><a href="https://ask.wireshark.org/questions/35401/wireshark-not-seeing-mobile-data">https://ask.wireshark.org/questions/35401/wireshark-not-seeing-mobile-data</a></p><p><a href="https://ask.wireshark.org/questions/35402/only-seeing-my-own-traffic-on-wireless-network">https://ask.wireshark.org/questions/35402/only-seeing-my-own-traffic-on-wireless-network</a></p></div><div id="comment-35414-info" class="comment-info"><span class="comment-age">(11 Aug '14, 10:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-35413" class="comment-tools"></div><div class="clear"></div><div id="comment-35413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

