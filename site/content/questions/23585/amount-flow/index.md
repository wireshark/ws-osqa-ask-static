+++
type = "question"
title = "Amount Flow"
description = '''Hello, I need to do a flow amount of collect. any way to accomplish this? I have a collection and need to reassemble flows.'''
date = "2013-08-06T04:59:00Z"
lastmod = "2013-08-08T02:46:00Z"
weight = 23585
keywords = [ "flow" ]
aliases = [ "/questions/23585" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Amount Flow](/questions/23585/amount-flow)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23585-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23585-score" class="post-score" title="current number of votes">-1</div><span id="post-23585-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I need to do a flow amount of collect. any way to accomplish this? I have a collection and need to reassemble flows.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Aug '13, 04:59</strong></p><img src="https://secure.gravatar.com/avatar/baed048f5c0c70f360b151dd07bb72c3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gustavo&#39;s gravatar image" /><p><span>Gustavo</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gustavo has no accepted answers">0%</span></p></div></div><div id="comments-container-23585" class="comments-container"><span id="23633"></span><div id="comment-23633" class="comment"><div id="post-23633-score" class="comment-score"></div><div class="comment-text"><p>I think we need more information.</p><blockquote><p>I need to do a flow amount of collect.</p></blockquote><p>What does that mean?</p><blockquote><p>I have a collection and need to reassemble flows.</p></blockquote><p>A <strong>collection</strong> of what? And what exactly is a <strong>flow</strong> for you?</p></div><div id="comment-23633-info" class="comment-info"><span class="comment-age">(08 Aug '13, 02:46)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23585" class="comment-tools"></div><div class="clear"></div><div id="comment-23585-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

