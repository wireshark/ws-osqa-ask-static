+++
type = "question"
title = "Trace pane shows incorrect info message"
description = '''Hello, I&#x27;ve recorded a trace, there are messages that are being showed as 180 Ringing (in the upper pane, th colored one), but when i see inside the message i see that this is actually 200 OK message this is not a one-time error, i also see BYE meesage in the trace, but this is actually ACK strange,...'''
date = "2011-02-23T05:42:00Z"
lastmod = "2011-02-24T04:35:00Z"
weight = 2517
keywords = [ "info", "sip" ]
aliases = [ "/questions/2517" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Trace pane shows incorrect info message](/questions/2517/trace-pane-shows-incorrect-info-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2517-score" class="post-score" title="current number of votes">0</div><span id="post-2517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I've recorded a trace, there are messages that are being showed as 180 Ringing (in the upper pane, th colored one), but when i see inside the message i see that this is actually 200 OK message</p><p>this is not a one-time error, i also see BYE meesage in the trace, but this is actually ACK strange, right? (How do i upload a screenshot?)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '11, 05:42</strong></p><img src="https://secure.gravatar.com/avatar/386f90e90ff5551b1d9abcb771e89b94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hk76&#39;s gravatar image" /><p><span>hk76</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hk76 has no accepted answers">0%</span></p></div></div><div id="comments-container-2517" class="comments-container"><span id="2520"></span><div id="comment-2520" class="comment"><div id="post-2520-score" class="comment-score"></div><div class="comment-text"><p>You need to put the image on a web server you have access to and then use the Image tag to point to its URL.</p></div><div id="comment-2520-info" class="comment-info"><span class="comment-age">(23 Feb '11, 07:32)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="2541"></span><div id="comment-2541" class="comment"><div id="post-2541-score" class="comment-score"></div><div class="comment-text"><p>Or: If you can share a (small) capture file you can attach it to an EMAil sent to <span class="__cf_email__" data-cfemail="2e59475c4b5d464f5c45035b5d4b5c5d6e59475c4b5d464f5c4500415c4900">[email protected]</span></p></div><div id="comment-2541-info" class="comment-info"><span class="comment-age">(23 Feb '11, 13:48)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-2517" class="comment-tools"></div><div class="clear"></div><div id="comment-2517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2547"></span>

<div id="answer-container-2547" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2547-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2547-score" class="post-score" title="current number of votes">0</div><span id="post-2547-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>thanks, how do i update this case only? just send and email with the capture file? i think i need to connect it to a question id.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '11, 04:35</strong></p><img src="https://secure.gravatar.com/avatar/386f90e90ff5551b1d9abcb771e89b94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hk76&#39;s gravatar image" /><p><span>hk76</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hk76 has no accepted answers">0%</span></p></div></div><div id="comments-container-2547" class="comments-container"></div><div id="comment-tools-2547" class="comment-tools"></div><div class="clear"></div><div id="comment-2547-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

