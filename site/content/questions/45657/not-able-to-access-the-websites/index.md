+++
type = "question"
title = "Not able to access the websites"
description = '''Hi Team, Sometimes I am not able to access the Internet (means not resolve the IP address of the website and whenever I entered the public IP address in the web browser I am able to access the website) so I have captured some traffic using wireshark and noted that getting some message for DNS ([Pack...'''
date = "2015-09-07T04:41:00Z"
lastmod = "2015-09-08T00:06:00Z"
weight = 45657
keywords = [ "limited", "packet", "snaplen", "size" ]
aliases = [ "/questions/45657" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Not able to access the websites](/questions/45657/not-able-to-access-the-websites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45657-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45657-score" class="post-score" title="current number of votes">0</div><span id="post-45657-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Team,</p><p>Sometimes I am not able to access the Internet (means not resolve the IP address of the website and whenever I entered the public IP address in the web browser I am able to access the website) so I have captured some traffic using wireshark and noted that getting some message for DNS ([Packet size limited during capture: DNS truncated] so Please let me know why I am getting this message.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-limited" rel="tag" title="see questions tagged &#39;limited&#39;">limited</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-snaplen" rel="tag" title="see questions tagged &#39;snaplen&#39;">snaplen</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '15, 04:41</strong></p><img src="https://secure.gravatar.com/avatar/21ad99b5132fa3aabdc1323c5567c41b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Smit19&#39;s gravatar image" /><p><span>Smit19</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Smit19 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Sep '15, 09:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-45657" class="comments-container"></div><div id="comment-tools-45657" class="comment-tools"></div><div class="clear"></div><div id="comment-45657-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="45659"></span>

<div id="answer-container-45659" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45659-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45659-score" class="post-score" title="current number of votes">0</div><span id="post-45659-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Your capturing setup is likely to be limiting the size of packets received, this is usually an option.</p><p>What version of Wireshark and what OS are you using?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Sep '15, 04:52</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-45659" class="comments-container"><span id="45661"></span><div id="comment-45661" class="comment"><div id="post-45661-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your Answer.</p><p>The Wireshark version is (v1.12.7-0-g7fc8978 from master-1.12).</p><p>OS:: Windows.</p><p>and I want to know that is there any problem in network or this is normal message.</p></div><div id="comment-45661-info" class="comment-info"><span class="comment-age">(07 Sep '15, 05:40)</span> <span class="comment-user userinfo">Smit19</span></div></div><span id="45662"></span><div id="comment-45662" class="comment"><div id="post-45662-score" class="comment-score"></div><div class="comment-text"><p>Your description of it seems normal to me, you have set a packet size limit during the capture so that's what Wireshark reports.</p><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>, Google Drive, Dropbox?</p></div><div id="comment-45662-info" class="comment-info"><span class="comment-age">(07 Sep '15, 06:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="45688"></span><div id="comment-45688" class="comment"><div id="post-45688-score" class="comment-score"></div><div class="comment-text"><p>Please refer below link for the pcap.</p><p><a href="https://drive.google.com/file/d/0B6P32GvfmmqwYkp5dUFsZG9IOVk/view?usp=sharing">https://drive.google.com/file/d/0B6P32GvfmmqwYkp5dUFsZG9IOVk/view?usp=sharing</a></p><p>I would like to inform you I have not set the packet limit size in wireshark so if possible please let me know why this message I am getting in wireshark.</p></div><div id="comment-45688-info" class="comment-info"><span class="comment-age">(07 Sep '15, 21:45)</span> <span class="comment-user userinfo">Smit19</span></div></div></div><div id="comment-tools-45659" class="comment-tools"></div><div class="clear"></div><div id="comment-45659-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="45690"></span>

<div id="answer-container-45690" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45690-score" class="post-score" title="current number of votes">0</div><span id="post-45690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You're getting that message because there WAS a packet size limit of 96 bytes when the packets were captured. Click on Statistics &gt; Summary and in the middle section, titled "Capture," you will see "96 bytes" under "Packet size limit."</p><p>Go to Edit &gt; Preferences &gt; Capture and then click "Edit" to the right of "Interfaces." Select the appropriate interface, and make sure the box to the right of "Limit each packet to" is unchecked.</p><p>I'd also go to the Capture Options page, double-click the interface you're capturing on and again make sure "Limit each packet to" is unchecked.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Sep '15, 22:39</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-45690" class="comments-container"><span id="45694"></span><div id="comment-45694" class="comment"><div id="post-45694-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much for your answer and let me check the same from my side.</p></div><div id="comment-45694-info" class="comment-info"><span class="comment-age">(08 Sep '15, 00:06)</span> <span class="comment-user userinfo">Smit19</span></div></div></div><div id="comment-tools-45690" class="comment-tools"></div><div class="clear"></div><div id="comment-45690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

