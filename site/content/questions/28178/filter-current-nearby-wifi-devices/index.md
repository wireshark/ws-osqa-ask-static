+++
type = "question"
title = "Filter current nearby wifi devices"
description = '''I&#x27;m trying to get a list of all the nearby devices with wifi turned on. I&#x27;m currently using the filter &#x27;wlan.fc.type_subtype == 0x04&#x27; but it&#x27;s filling up too quickly, causing loss of overview. Does Wireshark have the ability only show frames from the last 20 seconds? And is it possible to tag frames...'''
date = "2013-12-16T12:04:00Z"
lastmod = "2013-12-16T22:58:00Z"
weight = 28178
keywords = [ "filter", "wifi", "mac-address" ]
aliases = [ "/questions/28178" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter current nearby wifi devices](/questions/28178/filter-current-nearby-wifi-devices)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28178-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28178-score" class="post-score" title="current number of votes">0</div><span id="post-28178-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to get a list of all the nearby devices with wifi turned on. I'm currently using the filter 'wlan.fc.type_subtype == 0x04' but it's filling up too quickly, causing loss of overview. Does Wireshark have the ability only show frames from the last 20 seconds? And is it possible to tag frames from a certain mac-address so I can easily identify known phones?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '13, 12:04</strong></p><img src="https://secure.gravatar.com/avatar/bc003ef6dc91d44a30590e0e8ce20e77?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Timmmmmm&#39;s gravatar image" /><p><span>Timmmmmm</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Timmmmmm has no accepted answers">0%</span></p></div></div><div id="comments-container-28178" class="comments-container"></div><div id="comment-tools-28178" class="comment-tools"></div><div class="clear"></div><div id="comment-28178-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28190"></span>

<div id="answer-container-28190" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28190-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28190-score" class="post-score" title="current number of votes">0</div><span id="post-28190-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Would a filter on arp and other broadcast frames do what you need? <img src="https://osqa-ask.wireshark.org/upfiles/Selection_001.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Dec '13, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></img></div></div><div id="comments-container-28190" class="comments-container"></div><div id="comment-tools-28190" class="comment-tools"></div><div class="clear"></div><div id="comment-28190-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

