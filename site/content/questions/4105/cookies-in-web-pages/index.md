+++
type = "question"
title = "cookies in web pages"
description = '''What is the use of cookies in some web sites like cnn.com? I don&#x27;t log in but when I capture HTTP packets using Wireshark I see cookies. what is it for?'''
date = "2011-05-17T15:10:00Z"
lastmod = "2011-05-18T00:31:00Z"
weight = 4105
keywords = [ "cookies" ]
aliases = [ "/questions/4105" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [cookies in web pages](/questions/4105/cookies-in-web-pages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4105-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4105-score" class="post-score" title="current number of votes">0</div><span id="post-4105-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is the use of cookies in some web sites like cnn.com? I don't log in but when I capture HTTP packets using Wireshark I see cookies. what is it for?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cookies" rel="tag" title="see questions tagged &#39;cookies&#39;">cookies</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 May '11, 15:10</strong></p><img src="https://secure.gravatar.com/avatar/0d1f835bfa8cc91838057ef65fc4d1c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="A%20B&#39;s gravatar image" /><p><span>A B</span><br />
<span class="score" title="1 reputation points">1</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="A B has no accepted answers">0%</span></p></div></div><div id="comments-container-4105" class="comments-container"></div><div id="comment-tools-4105" class="comment-tools"></div><div class="clear"></div><div id="comment-4105-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4108"></span>

<div id="answer-container-4108" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4108-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4108-score" class="post-score" title="current number of votes">2</div><span id="post-4108-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Cookies are not just used to keep login information but it is basically a way to store information (name/value pairs) on the client and retrieve it automatically every time the client reconnects to the server. Common things to store in cookies are session informations or web site options, ad settings and other stuff.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 May '11, 15:26</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-4108" class="comments-container"><span id="4109"></span><div id="comment-4109" class="comment"><div id="post-4109-score" class="comment-score"></div><div class="comment-text"><p>you mean they show different ads for different users?can you describe a little more?thanks.</p></div><div id="comment-4109-info" class="comment-info"><span class="comment-age">(17 May '11, 15:28)</span> <span class="comment-user userinfo">A B</span></div></div><span id="4114"></span><div id="comment-4114" class="comment"><div id="post-4114-score" class="comment-score"></div><div class="comment-text"><p>It usually works like this: you visit a web page that shows ad banners and, together with loading the banner, you get a cookie from the ad company containing a unique ID. When you visit another web page that has banners from the same ad company they get their previous cookie back. That way they know what web sites you are surfing.</p><p>You can often defend yourself against techniques like that by blocking 3rd party cookies or using ad blockers.</p></div><div id="comment-4114-info" class="comment-info"><span class="comment-age">(18 May '11, 00:31)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-4108" class="comment-tools"></div><div class="clear"></div><div id="comment-4108-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

