+++
type = "question"
title = "hosts file location issue"
description = '''I used to be able to use a hosts file in: C:&#92;Users&#92;myUserID&#92;AppData&#92;Roaming&#92;Wireshark I just installed 1.10.0 in my Windows 7 32-bit Enterprise system and now Wireshark seems to have this preference:  hosts file in the current profile C:&#92;Users&#92;myUserID&#92;AppData&#92;Roaming&#92;Wireshark&#92;profiles&#92;Analysis (fo...'''
date = "2013-06-14T14:03:00Z"
lastmod = "2015-02-27T00:00:00Z"
weight = 22072
keywords = [ "hosts", "location", "1.10.0" ]
aliases = [ "/questions/22072" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [hosts file location issue](/questions/22072/hosts-file-location-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22072-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22072-score" class="post-score" title="current number of votes">0</div><span id="post-22072-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I used to be able to use a hosts file in: C:\Users\myUserID\AppData\Roaming\Wireshark</p><p>I just installed 1.10.0 in my Windows 7 32-bit Enterprise system and now Wireshark seems to have this preference:</p><ol><li>hosts file in the current profile C:\Users\myUserID\AppData\Roaming\Wireshark\profiles\Analysis (for example)</li><li>hosts file in the Wireshark program location</li></ol><p>I'm not seeing this change in the Release Notes for 1.10.0</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hosts" rel="tag" title="see questions tagged &#39;hosts&#39;">hosts</span> <span class="post-tag tag-link-location" rel="tag" title="see questions tagged &#39;location&#39;">location</span> <span class="post-tag tag-link-1.10.0" rel="tag" title="see questions tagged &#39;1.10.0&#39;">1.10.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 14:03</strong></p><img src="https://secure.gravatar.com/avatar/18b90188e619589890256bce8bec51b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GaryChaulklin&#39;s gravatar image" /><p><span>GaryChaulklin</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GaryChaulklin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jun '13, 10:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-22072" class="comments-container"><span id="22136"></span><div id="comment-22136" class="comment"><div id="post-22136-score" class="comment-score"></div><div class="comment-text"><p>Also listed as <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8817">bug 8817</a>.</p></div><div id="comment-22136-info" class="comment-info"><span class="comment-age">(18 Jun '13, 10:19)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="40114"></span><div id="comment-40114" class="comment"><div id="post-40114-score" class="comment-score"></div><div class="comment-text"><p>What profile are you using? See "Configuration Profiles..." under the "Edit" menu.</p></div><div id="comment-40114-info" class="comment-info"><span class="comment-age">(27 Feb '15, 00:00)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-22072" class="comment-tools"></div><div class="clear"></div><div id="comment-22072-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25804"></span>

<div id="answer-container-25804" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25804-score" class="post-score" title="current number of votes">0</div><span id="post-25804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi just remove the extension (.txt) of that hosts file .It will work fine. Only At -- 1. hosts file in the current profile C:\Users\myUserID\AppData\Roaming\Wireshark\profiles\Analysis (for example)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Oct '13, 05:57</strong></p><img src="https://secure.gravatar.com/avatar/33b6ff7cdefbc39deb47b7de4336d160?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="biswa&#39;s gravatar image" /><p><span>biswa</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="biswa has no accepted answers">0%</span></p></div></div><div id="comments-container-25804" class="comments-container"></div><div id="comment-tools-25804" class="comment-tools"></div><div class="clear"></div><div id="comment-25804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

