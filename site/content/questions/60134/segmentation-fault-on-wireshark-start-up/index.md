+++
type = "question"
title = "Segmentation fault on wireshark start up"
description = '''Hi I am trying to start wireshark on kali 2.0 but it keep giving me &quot;Segmentation fault&quot; as a result when i try to run it from terminal. I tried searching online but not works.'''
date = "2017-03-16T20:03:00Z"
lastmod = "2017-03-16T20:03:00Z"
weight = 60134
keywords = [ "segmentation" ]
aliases = [ "/questions/60134" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Segmentation fault on wireshark start up](/questions/60134/segmentation-fault-on-wireshark-start-up)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60134-score" class="post-score" title="current number of votes">0</div><span id="post-60134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am trying to start wireshark on kali 2.0 but it keep giving me "Segmentation fault" as a result when i try to run it from terminal.</p><p>I tried searching online but not works.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-segmentation" rel="tag" title="see questions tagged &#39;segmentation&#39;">segmentation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '17, 20:03</strong></p><img src="https://secure.gravatar.com/avatar/d421ce24fb10d6d81af5df2faa17d37a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="letmein02&#39;s gravatar image" /><p><span>letmein02</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="letmein02 has no accepted answers">0%</span></p></div></div><div id="comments-container-60134" class="comments-container"></div><div id="comment-tools-60134" class="comment-tools"></div><div class="clear"></div><div id="comment-60134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

