+++
type = "question"
title = "I want to test my dissector using the code coverage tool lcov"
description = '''What are the steps for testing dissector using lcov?'''
date = "2012-11-15T01:55:00Z"
lastmod = "2012-11-15T20:25:00Z"
weight = 15923
keywords = [ "test-question" ]
aliases = [ "/questions/15923" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [I want to test my dissector using the code coverage tool lcov](/questions/15923/i-want-to-test-my-dissector-using-the-code-coverage-tool-lcov)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15923-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15923-score" class="post-score" title="current number of votes">0</div><span id="post-15923-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What are the steps for testing dissector using lcov?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-test-question" rel="tag" title="see questions tagged &#39;test-question&#39;">test-question</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '12, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div></div><div id="comments-container-15923" class="comments-container"></div><div id="comment-tools-15923" class="comment-tools"></div><div class="clear"></div><div id="comment-15923-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15927"></span>

<div id="answer-container-15927" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15927-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15927-score" class="post-score" title="current number of votes">1</div><span id="post-15927-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Akhil has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>please read the following Wiki for a first idea.</p><blockquote><p><code>http://wiki.wireshark.org/Development/CodeCoverage</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Nov '12, 02:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-15927" class="comments-container"><span id="15951"></span><div id="comment-15951" class="comment"><div id="post-15951-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt</p></div><div id="comment-15951-info" class="comment-info"><span class="comment-age">(15 Nov '12, 20:25)</span> <span class="comment-user userinfo">Akhil</span></div></div></div><div id="comment-tools-15927" class="comment-tools"></div><div class="clear"></div><div id="comment-15927-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

