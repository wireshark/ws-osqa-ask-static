+++
type = "question"
title = "Capturing interfaces"
description = '''I can&#x27;t find my Wired** connection in the INTERFACELIST Please help me, Axel'''
date = "2015-07-06T10:26:00Z"
lastmod = "2015-07-06T10:34:00Z"
weight = 43896
keywords = [ "wired", "interfacelist" ]
aliases = [ "/questions/43896" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing interfaces](/questions/43896/capturing-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43896-score" class="post-score" title="current number of votes">0</div><span id="post-43896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't find my Wired** connection in the INTERFACELIST</p><p>Please help me, Axel</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wired" rel="tag" title="see questions tagged &#39;wired&#39;">wired</span> <span class="post-tag tag-link-interfacelist" rel="tag" title="see questions tagged &#39;interfacelist&#39;">interfacelist</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '15, 10:26</strong></p><img src="https://secure.gravatar.com/avatar/9acea77bd06bec461465c0bd02d19c5c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AxBite&#39;s gravatar image" /><p><span>AxBite</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AxBite has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jul '15, 10:58</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-43896" class="comments-container"><span id="43897"></span><div id="comment-43897" class="comment"><div id="post-43897-score" class="comment-score"></div><div class="comment-text"><p>What OS and what Wireshark version are you using?</p></div><div id="comment-43897-info" class="comment-info"><span class="comment-age">(06 Jul '15, 10:34)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-43896" class="comment-tools"></div><div class="clear"></div><div id="comment-43896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

