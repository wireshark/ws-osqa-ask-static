+++
type = "question"
title = "Wireshark is not showing TCP Packets"
description = '''Hi, I&#x27;m trying to capture the TCP traffic between 2 devices with wireshark. The 2 devices are connected via Ethernet with a Netgear ProSafe doing mirror of the port I want to view connections. My computer is Win7 x64 with a Realtek PCIe GBE. I want to see TCP packets with destination Ip of one of th...'''
date = "2015-07-02T01:18:00Z"
lastmod = "2015-07-02T01:18:00Z"
weight = 43806
keywords = [ "realtek", "tcp", "wireshark" ]
aliases = [ "/questions/43806" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark is not showing TCP Packets](/questions/43806/wireshark-is-not-showing-tcp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43806-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43806-score" class="post-score" title="current number of votes">0</div><span id="post-43806-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm trying to capture the TCP traffic between 2 devices with wireshark. The 2 devices are connected via Ethernet with a Netgear ProSafe doing mirror of the port I want to view connections.</p><p>My computer is Win7 x64 with a Realtek PCIe GBE. I want to see TCP packets with destination Ip of one of the devices.</p><p>I have also tried same sniffer situation with a computer with an Intel NIC and I can see all TCP packets ( 1 per second)</p><p>for both cases, wireshark was installed for first time.</p><p>I read something about chimney and I already disabled via CMD, but continue with same situation.</p><p>Any Idea?</p><p>Thanks, Guillermo</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-realtek" rel="tag" title="see questions tagged &#39;realtek&#39;">realtek</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jul '15, 01:18</strong></p><img src="https://secure.gravatar.com/avatar/28f1a0949cfcbaba37afbd6cf2c97a59?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="taquionbcn&#39;s gravatar image" /><p><span>taquionbcn</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="taquionbcn has no accepted answers">0%</span></p></div></div><div id="comments-container-43806" class="comments-container"></div><div id="comment-tools-43806" class="comment-tools"></div><div class="clear"></div><div id="comment-43806-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

