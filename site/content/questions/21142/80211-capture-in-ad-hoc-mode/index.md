+++
type = "question"
title = "802.11 Capture in Ad hoc mode."
description = '''We have set of AirPcap Nx. We interface the device with Wireshark to capture the 802.11n packets. In the infrastructure mode, we can get the values for the 802.11 QoS parameters ( AC parameters). But in the ad Hoc Mode we could not. It is empty. Could you advice please.'''
date = "2013-05-14T12:43:00Z"
lastmod = "2013-05-14T12:43:00Z"
weight = 21142
keywords = [ "qos", "ad", "parameters", "hoc" ]
aliases = [ "/questions/21142" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [802.11 Capture in Ad hoc mode.](/questions/21142/80211-capture-in-ad-hoc-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21142-score" class="post-score" title="current number of votes">0</div><span id="post-21142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have set of AirPcap Nx. We interface the device with Wireshark to capture the 802.11n packets.</p><p>In the infrastructure mode, we can get the values for the 802.11 QoS parameters ( AC parameters). But in the ad Hoc Mode we could not.</p><p>It is empty. Could you advice please.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-qos" rel="tag" title="see questions tagged &#39;qos&#39;">qos</span> <span class="post-tag tag-link-ad" rel="tag" title="see questions tagged &#39;ad&#39;">ad</span> <span class="post-tag tag-link-parameters" rel="tag" title="see questions tagged &#39;parameters&#39;">parameters</span> <span class="post-tag tag-link-hoc" rel="tag" title="see questions tagged &#39;hoc&#39;">hoc</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 May '13, 12:43</strong></p><img src="https://secure.gravatar.com/avatar/54cc926dedb726ac5af0fa4a07d9e989?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="azizghali&#39;s gravatar image" /><p><span>azizghali</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="azizghali has no accepted answers">0%</span></p></div></div><div id="comments-container-21142" class="comments-container"></div><div id="comment-tools-21142" class="comment-tools"></div><div class="clear"></div><div id="comment-21142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

