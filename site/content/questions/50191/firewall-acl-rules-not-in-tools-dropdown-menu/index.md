+++
type = "question"
title = "Firewall ACL Rules not in Tools dropdown menu"
description = '''I can only see Lua. I am using version 2.0.1 and running Windows 7.'''
date = "2016-02-14T18:16:00Z"
lastmod = "2016-03-11T02:14:00Z"
weight = 50191
keywords = [ "tools" ]
aliases = [ "/questions/50191" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Firewall ACL Rules not in Tools dropdown menu](/questions/50191/firewall-acl-rules-not-in-tools-dropdown-menu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50191-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50191-score" class="post-score" title="current number of votes">0</div><span id="post-50191-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can only see Lua. I am using version 2.0.1 and running Windows 7.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tools" rel="tag" title="see questions tagged &#39;tools&#39;">tools</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '16, 18:16</strong></p><img src="https://secure.gravatar.com/avatar/cd185082e4843b0b09d32771412d466e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="otariidae&#39;s gravatar image" /><p><span>otariidae</span><br />
<span class="score" title="10 reputation points">10</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="otariidae has no accepted answers">0%</span></p></div></div><div id="comments-container-50191" class="comments-container"></div><div id="comment-tools-50191" class="comment-tools"></div><div class="clear"></div><div id="comment-50191-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50207"></span>

<div id="answer-container-50207" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50207-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50207-score" class="post-score" title="current number of votes">3</div><span id="post-50207-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="otariidae has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently you'll have to use the legacy GTK UI for that, it hasn't been ported over to the new Qt UI yet.</p><p>Search for the application "Wireshark Legacy" in your start menu, or run wireshark-gtk.exe from the Wireshark installation directory.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '16, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50207" class="comments-container"><span id="50815"></span><div id="comment-50815" class="comment"><div id="post-50815-score" class="comment-score"></div><div class="comment-text"><p>In the legacy version of 2.0.2 on W7 64bit even the "Firewall ACL Rules" is greyed out .. ? <img src="https://osqa-ask.wireshark.org/upfiles/2016-03-11_105218.jpg" alt="alt text" /></p></div><div id="comment-50815-info" class="comment-info"><span class="comment-age">(11 Mar '16, 01:59)</span> <span class="comment-user userinfo">Marc</span></div></div><span id="50816"></span><div id="comment-50816" class="comment"><div id="post-50816-score" class="comment-score">1</div><div class="comment-text"><p>Load a capture file first.</p></div><div id="comment-50816-info" class="comment-info"><span class="comment-age">(11 Mar '16, 02:02)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="50817"></span><div id="comment-50817" class="comment"><div id="post-50817-score" class="comment-score"></div><div class="comment-text"><p>oops! yup , thanks :-)</p></div><div id="comment-50817-info" class="comment-info"><span class="comment-age">(11 Mar '16, 02:14)</span> <span class="comment-user userinfo">Marc</span></div></div></div><div id="comment-tools-50207" class="comment-tools"></div><div class="clear"></div><div id="comment-50207-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

