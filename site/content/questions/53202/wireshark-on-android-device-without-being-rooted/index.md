+++
type = "question"
title = "Wireshark on android device without being rooted"
description = '''Hello Sir, I wish to inquire the possibility for using wireshark on a android device without being rooted. Wireshark is a daily tool used at work and at home some times but as its being so useful I would love to install into my android latest device but I am not willing to root my device. So I am ra...'''
date = "2016-06-04T13:48:00Z"
lastmod = "2016-06-05T04:19:00Z"
weight = 53202
keywords = [ "vatu" ]
aliases = [ "/questions/53202" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on android device without being rooted](/questions/53202/wireshark-on-android-device-without-being-rooted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53202-score" class="post-score" title="current number of votes">0</div><span id="post-53202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Sir, I wish to inquire the possibility for using wireshark on a android device without being rooted.</p><p>Wireshark is a daily tool used at work and at home some times but as its being so useful I would love to install into my android latest device but I am not willing to root my device. So I am raising this query for possible ways of having installed into my android device without rooting my device?</p><p>I must say you guys have always amazed me with this Powerful tool of your. My scope of work as a Core Engineer has never being easy and fun without using wireshark. I salute your Technical Team and Administration team for many all impossible into possible.</p><p>Thank you and waiting upon your response to my query.</p><p>hosea dot tonyatgmaildot com</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vatu" rel="tag" title="see questions tagged &#39;vatu&#39;">vatu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '16, 13:48</strong></p><img src="https://secure.gravatar.com/avatar/3a6763b99e8ce53e4015eeb83c5c6e4c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wokeke%20Tony%20Hosea&#39;s gravatar image" /><p><span>Wokeke Tony ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wokeke Tony Hosea has no accepted answers">0%</span></p></div></div><div id="comments-container-53202" class="comments-container"></div><div id="comment-tools-53202" class="comment-tools"></div><div class="clear"></div><div id="comment-53202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53205"></span>

<div id="answer-container-53205" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53205-score" class="post-score" title="current number of votes">0</div><span id="post-53205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't run Wireshark on an android device directly, root or no root. The trick to run <a href="http://balintreczey.hu/blog/run-wireshark-on-android-using-lil-debi/">Wireshark on an android device</a> is by using Lil' Debi, which installs a Debian subsystem on the phone. So if you want Wireshark, you need root and Lil' Debi.</p><p>If you can live with packet capture and analyzing it in Wireshark after transfer, you might want to check out <a href="http://www.taosoftware.co.jp/en/android/packetcapture/">tPacketCapture</a>, which uses the VPNService of Android to get access to the data, so there's no root required.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jun '16, 04:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-53205" class="comments-container"></div><div id="comment-tools-53205" class="comment-tools"></div><div class="clear"></div><div id="comment-53205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

