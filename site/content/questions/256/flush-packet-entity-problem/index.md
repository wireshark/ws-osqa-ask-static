+++
type = "question"
title = "Flush Packet entity Problem"
description = '''respected sir i use a dial up connection over a lan.while playing counter strike i encounter a problem blinking at the right top corner of screen (Flush Packet entity Problem) than games runs slow and even internet runs slow. kindly let me know what i should do about it can wireshark software help m...'''
date = "2010-09-22T00:35:00Z"
lastmod = "2010-09-22T10:37:00Z"
weight = 256
keywords = [ "flush-packet-entity" ]
aliases = [ "/questions/256" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Flush Packet entity Problem](/questions/256/flush-packet-entity-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-256-score" class="post-score" title="current number of votes">0</div><span id="post-256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>respected sir i use a dial up connection over a lan.while playing counter strike i encounter a problem blinking at the right top corner of screen (Flush Packet entity Problem) than games runs slow and even internet runs slow. kindly let me know what i should do about it can wireshark software help me solve this problem???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flush-packet-entity" rel="tag" title="see questions tagged &#39;flush-packet-entity&#39;">flush-packet-entity</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '10, 00:35</strong></p><img src="https://secure.gravatar.com/avatar/559f13f7b1f427085bd845da038bef09?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShadowLord&#39;s gravatar image" /><p><span>ShadowLord</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShadowLord has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Sep '10, 01:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span></p></div></div><div id="comments-container-256" class="comments-container"></div><div id="comment-tools-256" class="comment-tools"></div><div class="clear"></div><div id="comment-256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="261"></span>

<div id="answer-container-261" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-261-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-261-score" class="post-score" title="current number of votes">1</div><span id="post-261-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark can be used to spot all types of network problems and many application problems. It sounds like you might want to take off some gaming time to research the Flush Packet Entity Problem (which occurs on other apps as well) and capture/analyze your traffic.</p><p>When you do capture your traffic, select Analyze &gt; Expert Info Composite to see if Wireshark screams about any particular problems. Good place to start. There are some basic free videos available out at www.wiresharkbook.com/coffee if you are unfamiliar with Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '10, 10:37</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-261" class="comments-container"></div><div id="comment-tools-261" class="comment-tools"></div><div class="clear"></div><div id="comment-261-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

