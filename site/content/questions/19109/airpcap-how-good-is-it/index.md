+++
type = "question"
title = "AirPcap how good is it?"
description = '''Is anyone using AirPcap? If so what version? How does it work, good, bad? What else can you tell me about it? Will it work with other programs besides wireshark? '''
date = "2013-03-03T17:13:00Z"
lastmod = "2013-03-05T23:00:00Z"
weight = 19109
keywords = [ "airpcap" ]
aliases = [ "/questions/19109" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [AirPcap how good is it?](/questions/19109/airpcap-how-good-is-it)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19109-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19109-score" class="post-score" title="current number of votes">0</div><span id="post-19109-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is anyone using AirPcap? If so what version? How does it work, good, bad? What else can you tell me about it? Will it work with other programs besides wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '13, 17:13</strong></p><img src="https://secure.gravatar.com/avatar/03448f5a71ad0563e7fcd9b666410b88?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jimjimalabim&#39;s gravatar image" /><p><span>jimjimalabim</span><br />
<span class="score" title="20 reputation points">20</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jimjimalabim has no accepted answers">0%</span></p></div></div><div id="comments-container-19109" class="comments-container"></div><div id="comment-tools-19109" class="comment-tools"></div><div class="clear"></div><div id="comment-19109-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19178"></span>

<div id="answer-container-19178" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19178-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19178-score" class="post-score" title="current number of votes">1</div><span id="post-19178-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="jimjimalabim has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is anyone using AirPcap?</p></blockquote><p>I'm sure many of the people on this site are using (or have used) AirPcap.</p><blockquote><p>If so what version?</p></blockquote><p>Mostly Nx.</p><blockquote><p>How does it work, good, bad?</p></blockquote><p>good/bad in terms of what? In general its rather good.</p><blockquote><p>What else can you tell me about it?</p></blockquote><p>well, it's best to retrieve whatever you are interested from the product page yourself: <a href="http://www.riverbed.com/de/products/cascade/wireshark_enhancements/airpcap.php">http://www.riverbed.com/de/products/cascade/wireshark_enhancements/airpcap.php</a><br />
</p><blockquote><p>Will it work with other programs besides wireshark?</p></blockquote><p>Yes. Some open source tools are know to work with it (BackTrack Linux, Aircrack-ng) as well as commercial tools (Riverbed Pilot, Metageek, etc.).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '13, 12:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-19178" class="comments-container"><span id="19192"></span><div id="comment-19192" class="comment"><div id="post-19192-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt!!!</p></div><div id="comment-19192-info" class="comment-info"><span class="comment-age">(05 Mar '13, 16:25)</span> <span class="comment-user userinfo">jimjimalabim</span></div></div><span id="19201"></span><div id="comment-19201" class="comment"><div id="post-19201-score" class="comment-score"></div><div class="comment-text"><p><span>@jimjimalabim</span>: If you like the answer, please follow etiquette and click the checkmark, indicating you accept the asnwer.</p></div><div id="comment-19201-info" class="comment-info"><span class="comment-age">(05 Mar '13, 23:00)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-19178" class="comment-tools"></div><div class="clear"></div><div id="comment-19178-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

