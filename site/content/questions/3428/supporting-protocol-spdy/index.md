+++
type = "question"
title = "supporting protocol SPDY"
description = '''When support SPDY (from Google)? Google Chrome support this protocol on pages Google ( (GMail, Calendar, Picasa, Maps, Hotpot, etc) Docs:  SPDY - The Chromium Projectshttp://www.chromium.org/spdy'''
date = "2011-04-10T13:47:00Z"
lastmod = "2011-04-10T14:04:00Z"
weight = 3428
keywords = [ "spdy", "chrome", "google", "speedy" ]
aliases = [ "/questions/3428" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [supporting protocol SPDY](/questions/3428/supporting-protocol-spdy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3428-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3428-score" class="post-score" title="current number of votes">0</div><span id="post-3428-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When support SPDY (from Google)? Google Chrome support this protocol on pages Google ( (GMail, Calendar, Picasa, Maps, Hotpot, etc)</p><p>Docs: <a href="http://www.chromium.org/spdy">SPDY - The Chromium Projects</a>http://www.chromium.org/spdy</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spdy" rel="tag" title="see questions tagged &#39;spdy&#39;">spdy</span> <span class="post-tag tag-link-chrome" rel="tag" title="see questions tagged &#39;chrome&#39;">chrome</span> <span class="post-tag tag-link-google" rel="tag" title="see questions tagged &#39;google&#39;">google</span> <span class="post-tag tag-link-speedy" rel="tag" title="see questions tagged &#39;speedy&#39;">speedy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '11, 13:47</strong></p><img src="https://secure.gravatar.com/avatar/e2ef0d683b02aaf21b77c52a5e6bdcf8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dzakus&#39;s gravatar image" /><p><span>Dzakus</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dzakus has no accepted answers">0%</span></p></div></div><div id="comments-container-3428" class="comments-container"></div><div id="comment-tools-3428" class="comment-tools"></div><div class="clear"></div><div id="comment-3428-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3429"></span>

<div id="answer-container-3429" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3429-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3429-score" class="post-score" title="current number of votes">1</div><span id="post-3429-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just checked the SPDY page and it says:</p><blockquote><p><strong>Wireshark SPDY extension</strong> The wireshark extension needs some love. You'll find it checked into the chromium source tree under net/tools.<br />
</p></blockquote><p>So I guess its up to someone having enough love for that kind of task I think :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '11, 14:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span> </br></p></div></div><div id="comments-container-3429" class="comments-container"></div><div id="comment-tools-3429" class="comment-tools"></div><div class="clear"></div><div id="comment-3429-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

