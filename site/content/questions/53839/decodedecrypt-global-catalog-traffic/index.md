+++
type = "question"
title = "Decode/decrypt global-catalog traffic..."
description = '''Hi all, I&#x27;m attempting to diagnose an Exchange issue and would like to view the contents of requests sent over port 3268 (global catalog/msft-gc). Do you know how I&#x27;d be able to decrypt/decode that data please? If I try to decode as LDAP, it still doesn&#x27;t appear in a human-readable form. Hope you ca...'''
date = "2016-07-05T08:37:00Z"
lastmod = "2016-07-05T08:37:00Z"
weight = 53839
keywords = [ "global-catalog", "ldap" ]
aliases = [ "/questions/53839" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decode/decrypt global-catalog traffic...](/questions/53839/decodedecrypt-global-catalog-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53839-score" class="post-score" title="current number of votes">0</div><span id="post-53839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I'm attempting to diagnose an Exchange issue and would like to view the contents of requests sent over port 3268 (global catalog/msft-gc).</p><p>Do you know how I'd be able to decrypt/decode that data please? If I try to decode as LDAP, it still doesn't appear in a human-readable form.</p><p>Hope you can help.</p><p>Matt</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-global-catalog" rel="tag" title="see questions tagged &#39;global-catalog&#39;">global-catalog</span> <span class="post-tag tag-link-ldap" rel="tag" title="see questions tagged &#39;ldap&#39;">ldap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jul '16, 08:37</strong></p><img src="https://secure.gravatar.com/avatar/a025ced0b1e6cca5504f2d0644b8adb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="leekie&#39;s gravatar image" /><p><span>leekie</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="leekie has no accepted answers">0%</span></p></div></div><div id="comments-container-53839" class="comments-container"></div><div id="comment-tools-53839" class="comment-tools"></div><div class="clear"></div><div id="comment-53839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

