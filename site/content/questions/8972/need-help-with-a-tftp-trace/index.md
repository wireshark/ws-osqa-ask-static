+++
type = "question"
title = "Need help with a tftp trace"
description = '''I am stuck at a problem where i am trying to telnet into a network device, copy file using telnet from this device to a window server. The file is copied till 2k data and then terminated. I took trace on the windows machine and need help with it. This is what I see in the trace with tftp traffic: 56...'''
date = "2012-02-12T23:45:00Z"
lastmod = "2012-02-12T23:45:00Z"
weight = 8972
keywords = [ "tftp", "wireshark" ]
aliases = [ "/questions/8972" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need help with a tftp trace](/questions/8972/need-help-with-a-tftp-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8972-score" class="post-score" title="current number of votes">0</div><span id="post-8972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am stuck at a problem where i am trying to telnet into a network device, copy file using telnet from this device to a window server. The file is copied till 2k data and then terminated. I took trace on the windows machine and need help with it. This is what I see in the trace with tftp traffic:</p><pre><code>569 18.778842   171.74.136.79   136.147.14.129  TFTP    46  Acknowledgement, Block: 4
2193    80.778631   171.74.136.79   136.147.14.129  TFTP    46  Acknowledgement, Block: 4</code></pre><p>The copy starts from the device to the windows machine and then does it 4 times and then stops. Any tips?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tftp" rel="tag" title="see questions tagged &#39;tftp&#39;">tftp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '12, 23:45</strong></p><img src="https://secure.gravatar.com/avatar/bc4ba25fdeb3b02452c06235ad413ae2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prabhathandoo&#39;s gravatar image" /><p><span>prabhathandoo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prabhathandoo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 20:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-8972" class="comments-container"></div><div id="comment-tools-8972" class="comment-tools"></div><div class="clear"></div><div id="comment-8972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

