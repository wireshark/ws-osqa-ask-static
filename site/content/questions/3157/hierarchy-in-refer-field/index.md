+++
type = "question"
title = "hierarchy in refer field"
description = '''I am so confused with refer field in http request packets.When I have an embedded object in one web page which is on another server and it is embedded on other page. I cant find an example in order to see do experiment. Can you describe it for me?'''
date = "2011-03-27T12:14:00Z"
lastmod = "2011-03-27T12:14:00Z"
weight = 3157
keywords = [ "http", "packet" ]
aliases = [ "/questions/3157" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [hierarchy in refer field](/questions/3157/hierarchy-in-refer-field)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3157-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3157-score" class="post-score" title="current number of votes">0</div><span id="post-3157-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am so confused with refer field in http request packets.When I have an embedded object in one web page which is on another server and it is embedded on other page. I cant find an example in order to see do experiment. Can you describe it for me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '11, 12:14</strong></p><img src="https://secure.gravatar.com/avatar/0d1f835bfa8cc91838057ef65fc4d1c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="A%20B&#39;s gravatar image" /><p><span>A B</span><br />
<span class="score" title="1 reputation points">1</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="A B has no accepted answers">0%</span></p></div></div><div id="comments-container-3157" class="comments-container"></div><div id="comment-tools-3157" class="comment-tools"></div><div class="clear"></div><div id="comment-3157-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

