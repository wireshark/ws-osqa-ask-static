+++
type = "question"
title = "Protocol breakdown"
description = '''What function in Wireshark provides you with a breakdown of the different protocol types on the LAN segment?'''
date = "2014-01-15T07:20:00Z"
lastmod = "2014-01-16T02:16:00Z"
weight = 28921
keywords = [ "protocol", "homework" ]
aliases = [ "/questions/28921" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol breakdown](/questions/28921/protocol-breakdown)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28921-score" class="post-score" title="current number of votes">0</div><span id="post-28921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What function in Wireshark provides you with a breakdown of the different protocol types on the LAN segment?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '14, 07:20</strong></p><img src="https://secure.gravatar.com/avatar/81dc7ef173dc45679c115b238b7eeadb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jw123&#39;s gravatar image" /><p><span>jw123</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jw123 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '14, 07:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28921" class="comments-container"></div><div id="comment-tools-28921" class="comment-tools"></div><div class="clear"></div><div id="comment-28921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28936"></span>

<div id="answer-container-28936" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28936-score" class="post-score" title="current number of votes">0</div><span id="post-28936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my other answer. It's the same.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '14, 13:33</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28936" class="comments-container"><span id="28942"></span><div id="comment-28942" class="comment"><div id="post-28942-score" class="comment-score"></div><div class="comment-text"><p>ur answer is yes I bothered looking at the site, however I am unsure if my answer is right. knowing how to interpret the information is totally different and no there isn't a book that I have for this online class!</p></div><div id="comment-28942-info" class="comment-info"><span class="comment-age">(15 Jan '14, 19:01)</span> <span class="comment-user userinfo">jw123</span></div></div><span id="28944"></span><div id="comment-28944" class="comment"><div id="post-28944-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I am unsure if my answer is right</p></blockquote><p>Then just give your answer in class. Either it'll be right or wrong and, if it's wrong, you'll find out. (You also might not pass the class, but that's not <em>our</em> problem, that's <em>your</em> problem.)</p></div><div id="comment-28944-info" class="comment-info"><span class="comment-age">(15 Jan '14, 19:15)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="28952"></span><div id="comment-28952" class="comment"><div id="post-28952-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I am unsure if my answer is right.</p></blockquote><p>and your answer would be what?</p><blockquote><p>I have for this online class!</p></blockquote><p>Where is this course (URL)?</p></div><div id="comment-28952-info" class="comment-info"><span class="comment-age">(16 Jan '14, 00:55)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28957"></span><div id="comment-28957" class="comment"><div id="post-28957-score" class="comment-score">1</div><div class="comment-text"><p>"Looking at the site" is not what I meant. Start Wireshark, load your trace, and take a look at the Wireshark "Statistics" menu. You should spot the menu option to solve this question within a couple of seconds, especially if you look at the options at the top of the menu. It's really not that difficult - asking questions here takes longer than actually solving this in Wireshark.</p></div><div id="comment-28957-info" class="comment-info"><span class="comment-age">(16 Jan '14, 02:16)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-28936" class="comment-tools"></div><div class="clear"></div><div id="comment-28936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

