+++
type = "question"
title = "wireshark in linux and ubuntu"
description = '''sir i want to capture traffic using wireshark and i dnt know how to install or start it in linux or ubuntu '''
date = "2013-11-30T09:16:00Z"
lastmod = "2013-11-30T10:29:00Z"
weight = 27586
keywords = [ "linux" ]
aliases = [ "/questions/27586" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark in linux and ubuntu](/questions/27586/wireshark-in-linux-and-ubuntu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27586-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27586-score" class="post-score" title="current number of votes">0</div><span id="post-27586-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>sir i want to capture traffic using wireshark and i dnt know how to install or start it in linux or ubuntu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '13, 09:16</strong></p><img src="https://secure.gravatar.com/avatar/d316fdeb8063b3195b7cc70909561df0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shaziya%20islam&#39;s gravatar image" /><p><span>shaziya islam</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shaziya islam has no accepted answers">0%</span></p></div></div><div id="comments-container-27586" class="comments-container"></div><div id="comment-tools-27586" class="comment-tools"></div><div class="clear"></div><div id="comment-27586-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27587"></span>

<div id="answer-container-27587" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27587-score" class="post-score" title="current number of votes">0</div><span id="post-27587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Installing Wireshark may be different on different Linux flavors, depending on their packet management. With Ubuntu, you could use the Software Management tool, or install as root from a command line by entering</p><p><code>apt-get install wireshark  apt-get install tshark</code></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '13, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27587" class="comments-container"></div><div id="comment-tools-27587" class="comment-tools"></div><div class="clear"></div><div id="comment-27587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

