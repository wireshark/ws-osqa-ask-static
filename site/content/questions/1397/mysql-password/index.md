+++
type = "question"
title = "Mysql Password"
description = '''Well since the little 24x7 bot didn&#x27;t work out a answer so I can just go a head and ask it here. I recently Wiresharked a keylogger that used MySQL, but it didn&#x27;t display the password only a encoded/encrypted password: &#92;xe1@&#92;xda&#92;xb3I&#92;xfa&#92;x8a&amp;gt;W&#92;x91&#92;xebF&#92;xbe&#92;x01r&#92;x0a&#92;xda4&#92;x9ao And I don&#x27;t really kn...'''
date = "2010-12-18T23:14:00Z"
lastmod = "2010-12-18T23:14:00Z"
weight = 1397
keywords = [ "encryption", "password", "mysql" ]
aliases = [ "/questions/1397" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mysql Password](/questions/1397/mysql-password)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1397-score" class="post-score" title="current number of votes">-1</div><span id="post-1397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Well since the little 24x7 bot didn't work out a answer so I can just go a head and ask it here. I recently Wiresharked a keylogger that used MySQL, but it didn't display the password only a encoded/encrypted password: <code>\[email protected]\xda\xb3I\xfa\x8a&gt;W\x91\xebF\xbe\x01r\x0a\xda4\x9ao</code><br />
And I don't really know what that is for encryption so can anybody help me please?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span> <span class="post-tag tag-link-mysql" rel="tag" title="see questions tagged &#39;mysql&#39;">mysql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '10, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/d3f1a4f032baa41c3b8d7c3c78abac9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ikin&#39;s gravatar image" /><p><span>Ikin</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ikin has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Dec '10, 23:16</strong> </span></p></div></div><div id="comments-container-1397" class="comments-container"></div><div id="comment-tools-1397" class="comment-tools"></div><div class="clear"></div><div id="comment-1397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

