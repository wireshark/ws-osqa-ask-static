+++
type = "question"
title = "CWIDS and Decryption"
description = '''Hi, Now that in the developments build there is a fix to get the eapol keys (and in fact the regular trafic) inside the CWIDS frames would it be possible to decrypt the 802.11 trafic inside these frames? For now I use a basic python script using scapy to rebuild another pcap file and then I use Wire...'''
date = "2016-07-01T00:26:00Z"
lastmod = "2016-07-01T00:26:00Z"
weight = 53759
keywords = [ "cwids", "decryption" ]
aliases = [ "/questions/53759" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [CWIDS and Decryption](/questions/53759/cwids-and-decryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53759-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53759-score" class="post-score" title="current number of votes">0</div><span id="post-53759-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Now that in the developments build there is a fix to get the eapol keys (and in fact the regular trafic) inside the CWIDS frames would it be possible to decrypt the 802.11 trafic inside these frames?</p><p>For now I use a basic python script using scapy to rebuild another pcap file and then I use Wireshark on this new file. This is both painful and long to process with huge captures.</p><p>Is anyone aware of a better/faster solution that would be directly integrated inside Wireshark? Or will this feature be deploy in a near future directly into the application?</p><p>Thanks in advance,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cwids" rel="tag" title="see questions tagged &#39;cwids&#39;">cwids</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jul '16, 00:26</strong></p><img src="https://secure.gravatar.com/avatar/d3817490e1e3a5109754697c5806f2a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yonibley&#39;s gravatar image" /><p><span>Yonibley</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yonibley has no accepted answers">0%</span></p></div></div><div id="comments-container-53759" class="comments-container"></div><div id="comment-tools-53759" class="comment-tools"></div><div class="clear"></div><div id="comment-53759-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

