+++
type = "question"
title = "HNBAP Procedures missing"
description = '''Hi! Does anyone know about which 3GPP standard version the HNBAP dissector is done (Display Filter Reference: UTRAN Iuh interface HNBAP signalling, Protocol field name: hnbap)? Because 3GPP TNL Update and HNB Configuration Transfer Procedure are considered like Unknown Message and no field name aren...'''
date = "2011-11-10T07:50:00Z"
lastmod = "2011-11-10T13:45:00Z"
weight = 7373
keywords = [ "3gpp", "dissector", "hnbap" ]
aliases = [ "/questions/7373" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [HNBAP Procedures missing](/questions/7373/hnbap-procedures-missing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7373-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7373-score" class="post-score" title="current number of votes">0</div><span id="post-7373-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>Does anyone know about which 3GPP standard version the HNBAP dissector is done (Display Filter Reference: UTRAN Iuh interface HNBAP signalling, Protocol field name: hnbap)?</p><p>Because 3GPP TNL Update and HNB Configuration Transfer Procedure are considered like Unknown Message and no field name aren't defined at <a href="http://www.wireshark.org/docs/dfref/h/hnbap.html">http://www.wireshark.org/docs/dfref/h/hnbap.html</a></p><p>Thanks a lot.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-hnbap" rel="tag" title="see questions tagged &#39;hnbap&#39;">hnbap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '11, 07:50</strong></p><img src="https://secure.gravatar.com/avatar/b45f93fdfa837f36334066e28cd7334c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sifu&#39;s gravatar image" /><p><span>sifu</span><br />
<span class="score" title="31 reputation points">31</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sifu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Nov '11, 01:36</strong> </span></p></div></div><div id="comments-container-7373" class="comments-container"></div><div id="comment-tools-7373" class="comment-tools"></div><div class="clear"></div><div id="comment-7373-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7374"></span>

<div id="answer-container-7374" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7374-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7374-score" class="post-score" title="current number of votes">0</div><span id="post-7374-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="sifu has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>3GPP TS 25.469 V9.1.0 (2010-03)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '11, 10:21</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-7374" class="comments-container"><span id="7380"></span><div id="comment-7380" class="comment"><div id="post-7380-score" class="comment-score"></div><div class="comment-text"><p>Updated to V10.1.0 (2011-06) in revision 39787.</p></div><div id="comment-7380-info" class="comment-info"><span class="comment-age">(10 Nov '11, 13:45)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-7374" class="comment-tools"></div><div class="clear"></div><div id="comment-7374-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

