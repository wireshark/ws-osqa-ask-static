+++
type = "question"
title = "server 2008 dumpcap error"
description = '''error reads couldn&#x27;t run dumpcap none in child process: %1 is not a valid win32 application'''
date = "2014-07-09T08:14:00Z"
lastmod = "2014-07-09T08:14:00Z"
weight = 34509
keywords = [ "dumpcap" ]
aliases = [ "/questions/34509" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [server 2008 dumpcap error](/questions/34509/server-2008-dumpcap-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34509-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34509-score" class="post-score" title="current number of votes">0</div><span id="post-34509-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>error reads couldn't run dumpcap none in child process: %1 is not a valid win32 application</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '14, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/999142410ec9972e0aef1cb2a133e251?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="no%20no&#39;s gravatar image" /><p><span>no no</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="no no has no accepted answers">0%</span></p></div></div><div id="comments-container-34509" class="comments-container"></div><div id="comment-tools-34509" class="comment-tools"></div><div class="clear"></div><div id="comment-34509-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

