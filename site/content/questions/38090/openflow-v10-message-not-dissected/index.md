+++
type = "question"
title = "OpenFlow v1.0 &quot; Message not dissected &quot;"
description = '''Hi, I have installed Wireshark 1.12.1 on Ubuntu 14.04.I have captured openflow v1.0 packets on wireshark.I am getting &quot; Message not Dissected &quot; when i open the error packet. Can someone please help me in extracting the contents of the packet ? or is there any plugin available to do the same? Thanks ...'''
date = "2014-11-23T23:23:00Z"
lastmod = "2014-11-30T19:12:00Z"
weight = 38090
keywords = [ "openflow", "v1.0", "plugin", "wireshark" ]
aliases = [ "/questions/38090" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [OpenFlow v1.0 " Message not dissected "](/questions/38090/openflow-v10-message-not-dissected)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38090-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38090-score" class="post-score" title="current number of votes">0</div><span id="post-38090-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have installed Wireshark 1.12.1 on Ubuntu 14.04.I have captured openflow v1.0 packets on wireshark.I am getting " Message not Dissected " when i open the error packet. Can someone please help me in extracting the contents of the packet ? or is there any plugin available to do the same?</p><p>Thanks Ajay</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-openflow" rel="tag" title="see questions tagged &#39;openflow&#39;">openflow</span> <span class="post-tag tag-link-v1.0" rel="tag" title="see questions tagged &#39;v1.0&#39;">v1.0</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '14, 23:23</strong></p><img src="https://secure.gravatar.com/avatar/8c954ad1dbde9053c484a7b92d336885?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ajayvb87&#39;s gravatar image" /><p><span>ajayvb87</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ajayvb87 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Nov '14, 23:26</strong> </span></p></div></div><div id="comments-container-38090" class="comments-container"></div><div id="comment-tools-38090" class="comment-tools"></div><div class="clear"></div><div id="comment-38090-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38099"></span>

<div id="answer-container-38099" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38099-score" class="post-score" title="current number of votes">0</div><span id="post-38099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wiresharks OpenFlow v 1.0 dissector is not complete as you have discovered v 1.4 and v 1.5 have better support I think. There is <a href="http://archive.openflow.org/wk/index.php/OpenFlow_Wireshark_Dissector">http://archive.openflow.org/wk/index.php/OpenFlow_Wireshark_Dissector</a> but I have no idea how well that works. You'd probably be better off working with a more recent version of OpenFlow...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Nov '14, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-38099" class="comments-container"><span id="38244"></span><div id="comment-38244" class="comment"><div id="post-38244-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much Anders..</p></div><div id="comment-38244-info" class="comment-info"><span class="comment-age">(30 Nov '14, 19:12)</span> <span class="comment-user userinfo">ajayvb87</span></div></div></div><div id="comment-tools-38099" class="comment-tools"></div><div class="clear"></div><div id="comment-38099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

