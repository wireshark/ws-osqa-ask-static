+++
type = "question"
title = "sample capture for EGP"
description = '''hello I thank you very much for your good site  Excuse me, I need a sample capture for EGP routing protocl, to make this porotocol to give to my college for a exercise project please if you can, give me a sample capture of this protocol to see it&#x27;s structure thank you a lot'''
date = "2012-11-16T11:13:00Z"
lastmod = "2012-11-16T13:31:00Z"
weight = 15975
keywords = [ "sample", "capture", "egp", "protocol" ]
aliases = [ "/questions/15975" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [sample capture for EGP](/questions/15975/sample-capture-for-egp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15975-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15975-score" class="post-score" title="current number of votes">0</div><span id="post-15975-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello I thank you very much for your good site Excuse me, I need a sample capture for EGP routing protocl, to make this porotocol to give to my college for a exercise project please if you can, give me a sample capture of this protocol to see it's structure thank you a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sample" rel="tag" title="see questions tagged &#39;sample&#39;">sample</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-egp" rel="tag" title="see questions tagged &#39;egp&#39;">egp</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '12, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/166be91de047abfd15bf84cbe521fbc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="simineh&#39;s gravatar image" /><p><span>simineh</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="simineh has no accepted answers">0%</span></p></div></div><div id="comments-container-15975" class="comments-container"><span id="15976"></span><div id="comment-15976" class="comment"><div id="post-15976-score" class="comment-score"></div><div class="comment-text"><p>The EGP ("Exterior Gateway Protocol") as a specific protocol is obsolete.</p><p>See: <a href="https://en.wikipedia.org/wiki/Exterior_Gateway_Protocol">EGP</a></p><p>Wireshark doesn't have a dissector for same (AFAIK) and also I'm not aware of any captures of same on the Wireshark site.</p><p>Do you mean "Border Gateway Protocol" ?</p></div><div id="comment-15976-info" class="comment-info"><span class="comment-age">(16 Nov '12, 11:47)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="15982"></span><div id="comment-15982" class="comment"><div id="post-15982-score" class="comment-score"></div><div class="comment-text"><p>hello really dont be tired i tell to you, that i need a sample capture for EGP routing protocol, and then you give me tihs site address: <a href="https://en.wikipedia.org/wiki/Exterior_Gateway_Protocol">https://en.wikipedia.org/wiki/Exterior_Gateway_Protocol</a> are this site address for sample capture? you can tell to me: "we dont have it" but you gibe me, i become very glad when see your answer &gt;--&gt;- ): but you disappoint me really dont be tired and thanks</p></div><div id="comment-15982-info" class="comment-info"><span class="comment-age">(16 Nov '12, 12:12)</span> <span class="comment-user userinfo">simineh</span></div></div><span id="15984"></span><div id="comment-15984" class="comment"><div id="post-15984-score" class="comment-score"></div><div class="comment-text"><p>Instead of being disrespectful to those who try to help you, you would do yourself a favor if you could be more precise with your question. What exactly you mean by EGP? The protocol defined in rfc904?</p></div><div id="comment-15984-info" class="comment-info"><span class="comment-age">(16 Nov '12, 13:31)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15975" class="comment-tools"></div><div class="clear"></div><div id="comment-15975-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

