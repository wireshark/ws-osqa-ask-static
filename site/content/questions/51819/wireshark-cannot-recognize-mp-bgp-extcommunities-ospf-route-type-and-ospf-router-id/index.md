+++
type = "question"
title = "Wireshark cannot recognize MP-BGP extcommunities &quot;OSPF Route-Type&quot; and &quot;OSPF Router ID&quot;?"
description = '''Basically I want a confirmation on if that&#x27;s the case or if I&#x27;m doing something wrong. These two communities get categorized as &quot;Transitive Experimental&quot;. This has been seen on GNS3 with some version of IOS if that&#x27;s important.'''
date = "2016-04-20T06:24:00Z"
lastmod = "2016-04-21T02:47:00Z"
weight = 51819
keywords = [ "communities", "ospf", "mp-bgp" ]
aliases = [ "/questions/51819" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark cannot recognize MP-BGP extcommunities "OSPF Route-Type" and "OSPF Router ID"?](/questions/51819/wireshark-cannot-recognize-mp-bgp-extcommunities-ospf-route-type-and-ospf-router-id)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51819-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51819-score" class="post-score" title="current number of votes">0</div><span id="post-51819-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Basically I want a confirmation on if that's the case or if I'm doing something wrong. These two communities get categorized as "Transitive Experimental".</p><p>This has been seen on GNS3 with some version of IOS if that's important.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-communities" rel="tag" title="see questions tagged &#39;communities&#39;">communities</span> <span class="post-tag tag-link-ospf" rel="tag" title="see questions tagged &#39;ospf&#39;">ospf</span> <span class="post-tag tag-link-mp-bgp" rel="tag" title="see questions tagged &#39;mp-bgp&#39;">mp-bgp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '16, 06:24</strong></p><img src="https://secure.gravatar.com/avatar/ee67ebfcc65ae577a3be9f297b7867e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DxD&#39;s gravatar image" /><p><span>DxD</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DxD has no accepted answers">0%</span></p></div></div><div id="comments-container-51819" class="comments-container"><span id="51831"></span><div id="comment-51831" class="comment"><div id="post-51831-score" class="comment-score"></div><div class="comment-text"><p>And what Wireshark version are you referring to here?</p></div><div id="comment-51831-info" class="comment-info"><span class="comment-age">(21 Apr '16, 00:55)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="51833"></span><div id="comment-51833" class="comment"><div id="post-51833-score" class="comment-score"></div><div class="comment-text"><p>Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0)</p></div><div id="comment-51833-info" class="comment-info"><span class="comment-age">(21 Apr '16, 02:47)</span> <span class="comment-user userinfo">DxD</span></div></div></div><div id="comment-tools-51819" class="comment-tools"></div><div class="clear"></div><div id="comment-51819-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

