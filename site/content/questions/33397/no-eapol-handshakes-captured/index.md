+++
type = "question"
title = "No eapol handshakes captured"
description = '''Hello, I set up my capture in monitor and promiscuous mode with radio tap headers, and of course added my WPA key. Everything I do with my own computer is sniffed correctly. However if I connect another device, I don&#x27;t see any EAPOL handshakes and therefore no encrypted traffic. Can you help me? I a...'''
date = "2014-06-04T10:40:00Z"
lastmod = "2014-06-05T17:41:00Z"
weight = 33397
keywords = [ "sniffing", "wpa2", "eapol", "wlan", "handshake" ]
aliases = [ "/questions/33397" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No eapol handshakes captured](/questions/33397/no-eapol-handshakes-captured)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33397-score" class="post-score" title="current number of votes">0</div><span id="post-33397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I set up my capture in monitor and promiscuous mode with radio tap headers, and of course added my WPA key. Everything I do with my own computer is sniffed correctly. However if I connect another device, I don't see any EAPOL handshakes and therefore no encrypted traffic.</p><p>Can you help me? I am working on a hackintosh (just to be more precisely).</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span> <span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span> <span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '14, 10:40</strong></p><img src="https://secure.gravatar.com/avatar/9271a3d05ddb400f58241f33a113c20c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Badudel&#39;s gravatar image" /><p><span>Badudel</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Badudel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jun '14, 10:41</strong> </span></p></div></div><div id="comments-container-33397" class="comments-container"><span id="33471"></span><div id="comment-33471" class="comment"><div id="post-33471-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I set up my capture in monitor and promiscuous</p></blockquote><p>How did you enable <strong>monitor mode</strong> on your hackintosh?<br />
What is the OS X release on that system?</p></div><div id="comment-33471-info" class="comment-info"><span class="comment-age">(05 Jun '14, 13:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="33490"></span><div id="comment-33490" class="comment"><div id="post-33490-score" class="comment-score"></div><div class="comment-text"><p>I enabled monitor mode with the checkbox from wireshark. In OSX the wifi symbol turns into an eye when entering monitor mode so I am sure it is turned on. I am using Mavericks.</p></div><div id="comment-33490-info" class="comment-info"><span class="comment-age">(05 Jun '14, 17:41)</span> <span class="comment-user userinfo">Badudel</span></div></div></div><div id="comment-tools-33397" class="comment-tools"></div><div class="clear"></div><div id="comment-33397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

