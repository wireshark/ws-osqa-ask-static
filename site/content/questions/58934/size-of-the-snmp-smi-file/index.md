+++
type = "question"
title = "Size of the snmp-smi file"
description = '''Hi! How many records may be in the snmp-smi file? After I added manually a ~2000 mib modules in the file Wireshark is not run. Thnx.'''
date = "2017-01-21T12:30:00Z"
lastmod = "2017-01-21T16:48:00Z"
weight = 58934
keywords = [ "snmp-smi" ]
aliases = [ "/questions/58934" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Size of the snmp-smi file](/questions/58934/size-of-the-snmp-smi-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58934-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58934-score" class="post-score" title="current number of votes">0</div><span id="post-58934-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! How many records may be in the snmp-smi file? After I added manually a ~2000 mib modules in the file Wireshark is not run. Thnx.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snmp-smi" rel="tag" title="see questions tagged &#39;snmp-smi&#39;">snmp-smi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '17, 12:30</strong></p><img src="https://secure.gravatar.com/avatar/9d4f2ecdca8bfd16374ac5736582af80?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Magister&#39;s gravatar image" /><p><span>Magister</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Magister has no accepted answers">0%</span></p></div></div><div id="comments-container-58934" class="comments-container"></div><div id="comment-tools-58934" class="comment-tools"></div><div class="clear"></div><div id="comment-58934-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58938"></span>

<div id="answer-container-58938" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58938-score" class="post-score" title="current number of votes">0</div><span id="post-58938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Libsmi is very sensitive about the min format. It is more likely the mibs have syntax errors. I don't remember if the order of loading matters too e.g a if a mib depends on another mib that mib must be loaded first.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jan '17, 16:48</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-58938" class="comments-container"></div><div id="comment-tools-58938" class="comment-tools"></div><div class="clear"></div><div id="comment-58938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

