+++
type = "question"
title = "Monitoring Gb over IP interface with ciphering on"
description = '''Hi, we would like to monitor Gb over IP interface with the help of Wireshark, but it is required to have ciphering on in the test network. I know that monitoring of Gb interface as such is possible with Wireshark, since BSSGB protocol is a supported Wireshark display filter. But the problem now is w...'''
date = "2011-05-23T09:12:00Z"
lastmod = "2011-05-23T09:12:00Z"
weight = 4189
keywords = [ "ip", "over", "ciphering", "gb" ]
aliases = [ "/questions/4189" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring Gb over IP interface with ciphering on](/questions/4189/monitoring-gb-over-ip-interface-with-ciphering-on)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4189-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4189-score" class="post-score" title="current number of votes">0</div><span id="post-4189-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>we would like to monitor Gb over IP interface with the help of Wireshark, but it is required to have ciphering on in the test network. I know that monitoring of Gb interface as such is possible with Wireshark, since BSSGB protocol is a supported Wireshark display filter. But the problem now is with Ciphering switched on in the network (which you normally have switched off in test networks in order to monitor signalling messages on any protocol analyser).</p><p>Does anybody have any hint how I can use Wireshark also in this special scenario? Is it possible to monitor Gr interface in parallel and somehow tell Wireshark to take the ciphering key from there to decipher the Gb interface? But anyway, Gr interface would use MAP protocol, and this I can't find in Wireshark display filter list.</p><p>Thanks a lot in advance and Best Regards,</p><p>Carsten</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-over" rel="tag" title="see questions tagged &#39;over&#39;">over</span> <span class="post-tag tag-link-ciphering" rel="tag" title="see questions tagged &#39;ciphering&#39;">ciphering</span> <span class="post-tag tag-link-gb" rel="tag" title="see questions tagged &#39;gb&#39;">gb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '11, 09:12</strong></p><img src="https://secure.gravatar.com/avatar/9f1ca38e1209a3d108ce3264b81fb798?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Carsten&#39;s gravatar image" /><p><span>Carsten</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Carsten has no accepted answers">0%</span></p></div></div><div id="comments-container-4189" class="comments-container"></div><div id="comment-tools-4189" class="comment-tools"></div><div class="clear"></div><div id="comment-4189-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

