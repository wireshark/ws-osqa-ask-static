+++
type = "question"
title = "the link type of interface X was not specified"
description = '''I just installed 1.11.2 (SVN Rev 53411 from /trunk) for Windows from the download page and when trying to set a capture filter I get a popup with this message. A bit of searching seems to suggest this is bug 9473, which is fixed in revision 53581. Has this bug fix been incorporated into the Windows ...'''
date = "2013-12-05T02:28:00Z"
lastmod = "2013-12-06T02:32:00Z"
weight = 27801
keywords = [ "windows" ]
aliases = [ "/questions/27801" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [the link type of interface X was not specified](/questions/27801/the-link-type-of-interface-x-was-not-specified)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27801-score" class="post-score" title="current number of votes">0</div><span id="post-27801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed 1.11.2 (SVN Rev 53411 from /trunk) for Windows from the download page and when trying to set a capture filter I get a popup with this message. A bit of searching seems to suggest this is bug 9473, which is fixed in revision 53581.</p><p>Has this bug fix been incorporated into the Windows version yet?</p><p>thanks, Greg Choules</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '13, 02:28</strong></p><img src="https://secure.gravatar.com/avatar/d043de6006e8bfbb1af73c5a8e9855c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gchoules&#39;s gravatar image" /><p><span>gchoules</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gchoules has no accepted answers">0%</span></p></div></div><div id="comments-container-27801" class="comments-container"></div><div id="comment-tools-27801" class="comment-tools"></div><div class="clear"></div><div id="comment-27801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27802"></span>

<div id="answer-container-27802" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27802-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27802-score" class="post-score" title="current number of votes">0</div><span id="post-27802-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll need to download one of the automated builds that were made after revision 53581, there seems to be plenty of them. Note that these are on the "bleeding edge", so may have other issues. Have a look <a href="http://www.wireshark.org/download/automated/">here</a> for the files.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Dec '13, 03:01</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-27802" class="comments-container"><span id="27854"></span><div id="comment-27854" class="comment"><div id="post-27854-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Note that these are on the "bleeding edge", so may have other issues.</p></blockquote><p>Of course, that also applies to 1.11.x releases; they might be a little more stable than an automated build, but they're still development releases.</p></div><div id="comment-27854-info" class="comment-info"><span class="comment-age">(06 Dec '13, 02:32)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-27802" class="comment-tools"></div><div class="clear"></div><div id="comment-27802-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

