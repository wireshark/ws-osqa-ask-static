+++
type = "question"
title = "see other packages HTTP"
description = '''Please tell me what to do (preferably detailed instructions) to see other packages HTTP in the same wi-fi, it shows me only my traffic.'''
date = "2013-11-12T01:53:00Z"
lastmod = "2013-11-12T02:19:00Z"
weight = 26869
keywords = [ "http" ]
aliases = [ "/questions/26869" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [see other packages HTTP](/questions/26869/see-other-packages-http)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26869-score" class="post-score" title="current number of votes">0</div><span id="post-26869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please tell me what to do (preferably detailed instructions) to see other packages HTTP in the same wi-fi, it shows me only my traffic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '13, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/348b58b10734f511c32ddaa3f6c15488?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sokolov%20%20Andrey&#39;s gravatar image" /><p><span>Sokolov Andrey</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sokolov  Andrey has no accepted answers">0%</span></p></div></div><div id="comments-container-26869" class="comments-container"></div><div id="comment-tools-26869" class="comment-tools"></div><div class="clear"></div><div id="comment-26869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26871"></span>

<div id="answer-container-26871" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26871-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26871-score" class="post-score" title="current number of votes">0</div><span id="post-26871-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll need to read and understand how to setup your captures. Start at the Wiki page on <a href="http://wiki.wireshark.org/CaptureSetup">Capture Setup</a>, and then look at the page for the media type used by your network, most likely <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">802.11</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Nov '13, 02:17</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-26871" class="comments-container"><span id="26872"></span><div id="comment-26872" class="comment"><div id="post-26872-score" class="comment-score"></div><div class="comment-text"><p>as a wireshark enter the network key and where can I get it?</p></div><div id="comment-26872-info" class="comment-info"><span class="comment-age">(12 Nov '13, 02:19)</span> <span class="comment-user userinfo">Sokolov Andrey</span></div></div></div><div id="comment-tools-26871" class="comment-tools"></div><div class="clear"></div><div id="comment-26871-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

