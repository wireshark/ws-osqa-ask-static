+++
type = "question"
title = "I/O Graph bits or bytes per second?"
description = '''In former versions of wireshark it was possible to see bits or bytes per second in I/O-graph. But now there are only packtes. No one needs packets, bits or bytes are much more usefull. And with version 2.4.0 now also wireshark-legacy is no longer existing.  This is no improvement, tool is getting wo...'''
date = "2017-08-16T13:50:00Z"
lastmod = "2017-09-29T14:19:00Z"
weight = 63477
keywords = [ "iograph" ]
aliases = [ "/questions/63477" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I/O Graph bits or bytes per second?](/questions/63477/io-graph-bits-or-bytes-per-second)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63477-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63477-score" class="post-score" title="current number of votes">0</div><span id="post-63477-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In former versions of wireshark it was possible to see bits or bytes per second in I/O-graph. But now there are only packtes. No one needs packets, bits or bytes are much more usefull. And with version 2.4.0 now also wireshark-legacy is no longer existing. This is no improvement, tool is getting worse with every new version.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '17, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/d3cc635d365ad5f34876f2c0862322f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kuchenmann&#39;s gravatar image" /><p><span>kuchenmann</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kuchenmann has no accepted answers">0%</span></p></div></div><div id="comments-container-63477" class="comments-container"></div><div id="comment-tools-63477" class="comment-tools"></div><div class="clear"></div><div id="comment-63477-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63478"></span>

<div id="answer-container-63478" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63478-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63478-score" class="post-score" title="current number of votes">1</div><span id="post-63478-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can set the Y scale units (packets, bytes, bits) independently for each track of the graph, along with the display filter used to choose packets for that track and its colour, style, and eventually field of the packet which is to be used as source of the values.</p><p>And these settings are stored in your profile so they survive restart of the application.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Aug '17, 14:09</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-63478" class="comments-container"><span id="63673"></span><div id="comment-63673" class="comment"><div id="post-63673-score" class="comment-score"></div><div class="comment-text"><p>But only in good old "Legacy" version of Wireshark, which is no longer available (since 2.4.0). And with newer version of Wireshark it is not possible to set Y scale units to bits, or bytes. It is fixed to packets.</p></div><div id="comment-63673-info" class="comment-info"><span class="comment-age">(29 Sep '17, 05:01)</span> <span class="comment-user userinfo">kuchenmann</span></div></div><span id="63674"></span><div id="comment-63674" class="comment"><div id="post-63674-score" class="comment-score"></div><div class="comment-text"><p>Have you tried double clicking the Y scale units? On my OS (Windows) that allows me to select the scale units from a drop list that includes bytes and bits.</p></div><div id="comment-63674-info" class="comment-info"><span class="comment-age">(29 Sep '17, 05:22)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="63679"></span><div id="comment-63679" class="comment"><div id="post-63679-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot. Now I got it. Did not recognize that I have to double-click the lines below the I/O graph.</p></div><div id="comment-63679-info" class="comment-info"><span class="comment-age">(29 Sep '17, 14:19)</span> <span class="comment-user userinfo">kuchenmann</span></div></div></div><div id="comment-tools-63478" class="comment-tools"></div><div class="clear"></div><div id="comment-63478-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

