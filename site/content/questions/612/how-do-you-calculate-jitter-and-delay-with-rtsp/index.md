+++
type = "question"
title = "How do you calculate Jitter and Delay With RTSP"
description = '''I captured RTSP but I can&#x27;t calculate jitter and delay  please help me '''
date = "2010-10-24T21:56:00Z"
lastmod = "2010-10-26T10:51:00Z"
weight = 612
keywords = [ "rtsp" ]
aliases = [ "/questions/612" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do you calculate Jitter and Delay With RTSP](/questions/612/how-do-you-calculate-jitter-and-delay-with-rtsp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-612-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-612-score" class="post-score" title="current number of votes">0</div><span id="post-612-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured RTSP but I can't calculate jitter and delay</p><p>please help me</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtsp" rel="tag" title="see questions tagged &#39;rtsp&#39;">rtsp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '10, 21:56</strong></p><img src="https://secure.gravatar.com/avatar/fab1fda7113d3537bea59ed56b01ff6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kwonmickeydere&#39;s gravatar image" /><p><span>kwonmickeydere</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kwonmickeydere has no accepted answers">0%</span></p></div></div><div id="comments-container-612" class="comments-container"></div><div id="comment-tools-612" class="comment-tools"></div><div class="clear"></div><div id="comment-612-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="678"></span>

<div id="answer-container-678" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-678-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-678-score" class="post-score" title="current number of votes">1</div><span id="post-678-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You want to capture the actual RTP streams, not just the RTSP control packets. Once you've done that, you can use Statistics-&gt;RTP to show the streams and/or pick streams for further analysis.</p><p>See <a href="http://wiki.wireshark.org/RTP_statistics">http://wiki.wireshark.org/RTP_statistics</a> for more details, and come back here if that doesn't answer your questions...good luck!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Oct '10, 10:51</strong></p><img src="https://secure.gravatar.com/avatar/11ea89c2fd5a5830c69d0574a51b8142?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wesmorgan1&#39;s gravatar image" /><p><span>wesmorgan1</span><br />
<span class="score" title="411 reputation points">411</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wesmorgan1 has 2 accepted answers">4%</span></p></div></div><div id="comments-container-678" class="comments-container"></div><div id="comment-tools-678" class="comment-tools"></div><div class="clear"></div><div id="comment-678-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

