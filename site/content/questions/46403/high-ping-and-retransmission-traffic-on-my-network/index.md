+++
type = "question"
title = "High ping and retransmission traffic on my network?"
description = '''https://gyazo.com/efe6633712673556010b6534db13d73c https://gyazo.com/a00e8b24da6266ebcbcc51dd4ae7b7cc There are several pages of both of these. I&#x27;m unsure what to think of this, as i&#x27;m relatively new to wireshark. Anything worrying about this?'''
date = "2015-10-07T09:36:00Z"
lastmod = "2015-10-07T11:24:00Z"
weight = 46403
keywords = [ "ping", "retransmissions", "wireshark" ]
aliases = [ "/questions/46403" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [High ping and retransmission traffic on my network?](/questions/46403/high-ping-and-retransmission-traffic-on-my-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46403-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46403-score" class="post-score" title="current number of votes">0</div><span id="post-46403-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="https://gyazo.com/efe6633712673556010b6534db13d73c">https://gyazo.com/efe6633712673556010b6534db13d73c</a></p><p><a href="https://gyazo.com/a00e8b24da6266ebcbcc51dd4ae7b7cc">https://gyazo.com/a00e8b24da6266ebcbcc51dd4ae7b7cc</a></p><p>There are several pages of both of these. I'm unsure what to think of this, as i'm relatively new to wireshark. Anything worrying about this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ping" rel="tag" title="see questions tagged &#39;ping&#39;">ping</span> <span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '15, 09:36</strong></p><img src="https://secure.gravatar.com/avatar/4700ed777ad116f80e7709c977903126?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cirra&#39;s gravatar image" /><p><span>Cirra</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cirra has no accepted answers">0%</span></p></div></div><div id="comments-container-46403" class="comments-container"></div><div id="comment-tools-46403" class="comment-tools"></div><div class="clear"></div><div id="comment-46403-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46408"></span>

<div id="answer-container-46408" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46408-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46408-score" class="post-score" title="current number of votes">0</div><span id="post-46408-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hard to tell without the whole trace. But it looks to me that the traffic is blocked by a firewall?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '15, 11:24</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-46408" class="comments-container"></div><div id="comment-tools-46408" class="comment-tools"></div><div class="clear"></div><div id="comment-46408-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

