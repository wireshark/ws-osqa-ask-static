+++
type = "question"
title = "Dynamic Capture Export using python"
description = '''Does anyone know where I should start looking (i.e. python or wireshark tutorials) if I wanted to write python code to dynamically extract capture data into a table for analyzing? Thanks!'''
date = "2016-04-11T09:55:00Z"
lastmod = "2016-04-19T13:47:00Z"
weight = 51557
keywords = [ "python", "extract" ]
aliases = [ "/questions/51557" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Dynamic Capture Export using python](/questions/51557/dynamic-capture-export-using-python)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51557-score" class="post-score" title="current number of votes">0</div><span id="post-51557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know where I should start looking (i.e. python or wireshark tutorials) if I wanted to write python code to dynamically extract capture data into a table for analyzing?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-python" rel="tag" title="see questions tagged &#39;python&#39;">python</span> <span class="post-tag tag-link-extract" rel="tag" title="see questions tagged &#39;extract&#39;">extract</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '16, 09:55</strong></p><img src="https://secure.gravatar.com/avatar/28dd213bb6b909199cd90abb2fda5e98?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bjmarty&#39;s gravatar image" /><p><span>bjmarty</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bjmarty has no accepted answers">0%</span></p></div></div><div id="comments-container-51557" class="comments-container"></div><div id="comment-tools-51557" class="comment-tools"></div><div class="clear"></div><div id="comment-51557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51801"></span>

<div id="answer-container-51801" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51801-score" class="post-score" title="current number of votes">0</div><span id="post-51801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please take a look at Sharktools or Pyshark</p><blockquote><p><a href="https://github.com/armenb/sharktools">https://github.com/armenb/sharktools</a><br />
<a href="http://kiminewt.github.io/pyshark/">http://kiminewt.github.io/pyshark/</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '16, 13:47</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-51801" class="comments-container"></div><div id="comment-tools-51801" class="comment-tools"></div><div class="clear"></div><div id="comment-51801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

