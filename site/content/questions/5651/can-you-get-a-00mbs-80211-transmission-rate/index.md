+++
type = "question"
title = "Can you get a 0.0Mb/s 802.11 transmission rate?"
description = '''Hi, My question is rather simple. Can you get a 0.0Mb/s data rate (per stream)? I have multiple pcap files (165K of packets on average) with 802.11 traffic on them and I always seem to get a few tens of 802.11 data(only) packets showing 0.0Mb/s data rate (in the radiotap header). As far as I am awar...'''
date = "2011-08-11T09:03:00Z"
lastmod = "2011-08-17T12:03:00Z"
weight = 5651
keywords = [ "data", "rate", "airpcap", "802.11", "wireshark" ]
aliases = [ "/questions/5651" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can you get a 0.0Mb/s 802.11 transmission rate?](/questions/5651/can-you-get-a-00mbs-80211-transmission-rate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5651-score" class="post-score" title="current number of votes">0</div><span id="post-5651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>My question is rather simple. Can you get a 0.0Mb/s data rate (per stream)?</p><p>I have multiple pcap files (165K of packets on average) with 802.11 traffic on them and I always seem to get a few tens of 802.11 data(only) packets showing 0.0Mb/s data rate (in the radiotap header).</p><p>As far as I am aware you can only get quantized data rate values (of 1, 2, 5.5, 6, 9, 11, 12, 18, 22, 24, 33, 36, 48, 54)?</p><p>The 802.11 traffic was captured with an AirPcap Tx USB adapter and Wireshark Version 1.6.1 (SVN Rev 38096 from /trunk-1.6).</p><p>Am I missing something major?</p><p>Thanks and regards Alex</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-rate" rel="tag" title="see questions tagged &#39;rate&#39;">rate</span> <span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Aug '11, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/18440914e73da9d5280d0404ad6cf9a1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="almost_linear&#39;s gravatar image" /><p><span>almost_linear</span><br />
<span class="score" title="31 reputation points">31</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="almost_linear has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Aug '11, 12:11</strong> </span></p></div></div><div id="comments-container-5651" class="comments-container"><span id="5725"></span><div id="comment-5725" class="comment"><div id="post-5725-score" class="comment-score"></div><div class="comment-text"><p>Hi,</p><p>As I am still perplexed about this issue I upload an image showing the 802.11 transmission rate vs data (in bits), of an AirPcap Tx/Wireshark measurement.</p><p>The figure was produce in Pilot PE, but Wireshark also shows valid data packets (FCS=correct), which are not retransmits, with 0.0Mbps transmission rate.</p><p>Any advice would be more than welcome.</p><p>Thanks Alex</p><p><a href="http://imgur.com/U3UQG">Figure showing 802.11 Transmission rate of 0.0Mbps</a></p></div><div id="comment-5725-info" class="comment-info"><span class="comment-age">(17 Aug '11, 12:03)</span> <span class="comment-user userinfo">almost_linear</span></div></div></div><div id="comment-tools-5651" class="comment-tools"></div><div class="clear"></div><div id="comment-5651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

