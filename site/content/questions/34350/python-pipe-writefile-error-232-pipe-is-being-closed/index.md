+++
type = "question"
title = "Python Pipe: WriteFile error 232: Pipe is being closed"
description = '''Hi, I am trying to implement a Python Pipe so that I can do a live capture from a pcap file. I have used example 3 found on this page, with no luck. I am able to correctly start the pipe, start a live capture, and read the current contents of the pcap file, but I encounter the error found in my titl...'''
date = "2014-07-02T08:28:00Z"
lastmod = "2014-07-02T08:28:00Z"
weight = 34350
keywords = [ "pipe", "python" ]
aliases = [ "/questions/34350" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Python Pipe: WriteFile error 232: Pipe is being closed](/questions/34350/python-pipe-writefile-error-232-pipe-is-being-closed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34350-score" class="post-score" title="current number of votes">0</div><span id="post-34350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to implement a Python Pipe so that I can do a live capture from a pcap file. I have used example 3 found on <a href="http://wiki.wireshark.org/CaptureSetup/Pipes">this page</a>, with no luck. I am able to correctly start the pipe, start a live capture, and read the current contents of the pcap file, but I encounter the error found in my title after the first write to the pipe, error 232 "pipe is being closed."</p><p>Does anybody else have any experience with this error, or using Python to create a pipe to Wireshark in Windows?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-python" rel="tag" title="see questions tagged &#39;python&#39;">python</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jul '14, 08:28</strong></p><img src="https://secure.gravatar.com/avatar/022af746265de149e9b2beef14179ed7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brk22&#39;s gravatar image" /><p><span>brk22</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brk22 has no accepted answers">0%</span></p></div></div><div id="comments-container-34350" class="comments-container"></div><div id="comment-tools-34350" class="comment-tools"></div><div class="clear"></div><div id="comment-34350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

