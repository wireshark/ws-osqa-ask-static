+++
type = "question"
title = "what is the root cause for slowness? (pcap attached)"
description = '''can anyone advise the root cause for slowness when accessing the site? From the pcap, it seems that the request from the client did not get acknowledged by the server. why did it happen? many thanks.'''
date = "2015-04-22T23:31:00Z"
lastmod = "2015-04-23T01:15:00Z"
weight = 41717
keywords = [ "slowness" ]
aliases = [ "/questions/41717" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [what is the root cause for slowness? (pcap attached)](/questions/41717/what-is-the-root-cause-for-slowness-pcap-attached)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41717-score" class="post-score" title="current number of votes">0</div><span id="post-41717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can anyone advise the root cause for slowness when accessing the site? From the pcap, it seems that the request from the client did not get acknowledged by the server. why did it happen? many thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-slowness" rel="tag" title="see questions tagged &#39;slowness&#39;">slowness</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '15, 23:31</strong></p><img src="https://secure.gravatar.com/avatar/1f54e2c0fc93ae5b3245b7ec019cd210?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="westone&#39;s gravatar image" /><p><span>westone</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="westone has no accepted answers">0%</span></p></div></div><div id="comments-container-41717" class="comments-container"><span id="41718"></span><div id="comment-41718" class="comment"><div id="post-41718-score" class="comment-score"></div><div class="comment-text"><p>sorry could not upload the pcap</p></div><div id="comment-41718-info" class="comment-info"><span class="comment-age">(22 Apr '15, 23:33)</span> <span class="comment-user userinfo">westone</span></div></div><span id="41720"></span><div id="comment-41720" class="comment"><div id="post-41720-score" class="comment-score"></div><div class="comment-text"><p>I converted your answer to a comment, as that's how this Q&amp;A site works. Please read the FAQ.</p><blockquote><p>sorry could not upload the pcap</p></blockquote><p>Please upload your pcap somewhere else (google drive, dropbox, cloudshark.org) and then post the link here.</p></div><div id="comment-41720-info" class="comment-info"><span class="comment-age">(23 Apr '15, 01:15)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-41717" class="comment-tools"></div><div class="clear"></div><div id="comment-41717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

