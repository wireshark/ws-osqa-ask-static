+++
type = "question"
title = ".trace from Anritsu MasterClaw can&#x27;t open"
description = '''I&#x27;m using Anritsu MacterClaw Call-trace for tracing sigtran and TDM SS7 msgs. By default, i can&#x27;t open saved files and I&#x27;m wondering is it possible at all. We need wireshark because of bether filtering messages and we are using version 1.2.7 Thanks in advance'''
date = "2010-12-01T23:58:00Z"
lastmod = "2010-12-02T00:11:00Z"
weight = 1200
keywords = [ "open", "file" ]
aliases = [ "/questions/1200" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [.trace from Anritsu MasterClaw can't open](/questions/1200/trace-from-anritsu-masterclaw-cant-open)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1200-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1200-score" class="post-score" title="current number of votes">0</div><span id="post-1200-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using Anritsu MacterClaw Call-trace for tracing sigtran and TDM SS7 msgs.</p><p>By default, i can't open saved files and I'm wondering is it possible at all.</p><p>We need wireshark because of bether filtering messages and we are using version 1.2.7</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-open" rel="tag" title="see questions tagged &#39;open&#39;">open</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '10, 23:58</strong></p><img src="https://secure.gravatar.com/avatar/0fbb84c329c49567daa1065ebda095b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ms979&#39;s gravatar image" /><p><span>ms979</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ms979 has no accepted answers">0%</span></p></div></div><div id="comments-container-1200" class="comments-container"><span id="1201"></span><div id="comment-1201" class="comment"><div id="post-1201-score" class="comment-score"></div><div class="comment-text"><p>Not familiar with Anritsu product, but update to Wireshark version 1.4.2 to be up to date with the latest radiotap library and dissectors.</p></div><div id="comment-1201-info" class="comment-info"><span class="comment-age">(02 Dec '10, 00:11)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-1200" class="comment-tools"></div><div class="clear"></div><div id="comment-1200-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

