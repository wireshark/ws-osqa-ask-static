+++
type = "question"
title = "Where is the 1.9.X codebase branches?"
description = '''With SVN browser, I see releases only up to 1.8.6, but it looks like the latest is 1.9.3 with a development release at 1.9.2.  Where would the 1.9.X be if we wanted to do work forked off of those?'''
date = "2013-04-04T15:34:00Z"
lastmod = "2013-04-04T23:43:00Z"
weight = 20093
keywords = [ "1.9", "repository", "trunk" ]
aliases = [ "/questions/20093" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Where is the 1.9.X codebase branches?](/questions/20093/where-is-the-19x-codebase-branches)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20093-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20093-score" class="post-score" title="current number of votes">0</div><span id="post-20093-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>With SVN browser, I see releases only up to 1.8.6, but it looks like the latest is 1.9.3 with a development release at 1.9.2.</p><p>Where would the 1.9.X be if we wanted to do work forked off of those?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1.9" rel="tag" title="see questions tagged &#39;1.9&#39;">1.9</span> <span class="post-tag tag-link-repository" rel="tag" title="see questions tagged &#39;repository&#39;">repository</span> <span class="post-tag tag-link-trunk" rel="tag" title="see questions tagged &#39;trunk&#39;">trunk</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '13, 15:34</strong></p><img src="https://secure.gravatar.com/avatar/aa113408c3b312aac94702bb9639bbea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joeEmbed&#39;s gravatar image" /><p><span>joeEmbed</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joeEmbed has no accepted answers">0%</span></p></div></div><div id="comments-container-20093" class="comments-container"></div><div id="comment-tools-20093" class="comment-tools"></div><div class="clear"></div><div id="comment-20093-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20095"></span>

<div id="answer-container-20095" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20095-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20095-score" class="post-score" title="current number of votes">0</div><span id="post-20095-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://anonsvn.wireshark.org/viewvc/trunk/">trunk</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '13, 16:15</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-20095" class="comments-container"><span id="20096"></span><div id="comment-20096" class="comment"><div id="post-20096-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I meant more specifically, how would one designate 1.9.1 vs. 1.9.2 off of trunk, for example?</p></div><div id="comment-20096-info" class="comment-info"><span class="comment-age">(04 Apr '13, 16:26)</span> <span class="comment-user userinfo">joeEmbed</span></div></div></div><div id="comment-tools-20095" class="comment-tools"></div><div class="clear"></div><div id="comment-20095-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20100"></span>

<div id="answer-container-20100" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20100-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20100-score" class="post-score" title="current number of votes">0</div><span id="post-20100-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The development releases are true snapshots of the trunk. There's no branch or tag assigned to them in the repository. That is deliberate as to give no importance to them, as in they represent nothing more than the bleeding edge of development of that moment in time.</p><p>What you can do is fork of any (recent) revision of trunk and have your own development snapshot so to speak.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '13, 23:43</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-20100" class="comments-container"></div><div id="comment-tools-20100" class="comment-tools"></div><div class="clear"></div><div id="comment-20100-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

