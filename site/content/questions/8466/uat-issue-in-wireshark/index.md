+++
type = "question"
title = "UAT issue in wireshark"
description = '''I m facing an issue using UAT in wireshark. Whatever value I m putting in the &quot;port&quot; box of UAT it takes the value used in UAT_FLD_RANGE(basename,field_name,title,max,desc). Suppose I enter a value of 123 in the box in UAT and then click &quot;OK&quot;, then it still stores &quot;max&quot; in the above mentioned functi...'''
date = "2012-01-19T05:31:00Z"
lastmod = "2012-06-20T19:54:00Z"
weight = 8466
keywords = [ "uat" ]
aliases = [ "/questions/8466" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [UAT issue in wireshark](/questions/8466/uat-issue-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8466-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8466-score" class="post-score" title="current number of votes">0</div><span id="post-8466-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I m facing an issue using UAT in wireshark. Whatever value I m putting in the "port" box of UAT it takes the value used in UAT_FLD_RANGE(basename,field_name,title,max,desc). Suppose I enter a value of 123 in the box in UAT and then click "OK", then it still stores "max" in the above mentioned function. I want to know when we click "OK" in the UAT how does it store the value we enter so that I can debug the issue. I think I'm clear?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-uat" rel="tag" title="see questions tagged &#39;uat&#39;">uat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '12, 05:31</strong></p><img src="https://secure.gravatar.com/avatar/b7bdcb1b20e2c4bba13948b04439d544?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vish&#39;s gravatar image" /><p><span>vish</span><br />
<span class="score" title="0 reputation points">0</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vish has no accepted answers">0%</span></p></div></div><div id="comments-container-8466" class="comments-container"><span id="12095"></span><div id="comment-12095" class="comment"><div id="post-12095-score" class="comment-score"></div><div class="comment-text"><p>how to use UAT in wireshark? thanks!</p></div><div id="comment-12095-info" class="comment-info"><span class="comment-age">(20 Jun '12, 19:11)</span> <span class="comment-user userinfo">smilezuzu</span></div></div><span id="12097"></span><div id="comment-12097" class="comment"><div id="post-12097-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark were you using? I just tried this with the SCCP dissector and it seems to work fine on Windows 7 64-bit SVN r43424. I believe there might have been some recent work done on UAT's; maybe you could try a newer version of Wireshark than what you were using before?</p></div><div id="comment-12097-info" class="comment-info"><span class="comment-age">(20 Jun '12, 19:54)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-8466" class="comment-tools"></div><div class="clear"></div><div id="comment-8466-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

