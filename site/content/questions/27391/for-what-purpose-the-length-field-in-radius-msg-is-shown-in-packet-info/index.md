+++
type = "question"
title = "For what purpose the length field in Radius msg is shown in packet info"
description = '''For Radius messages, the &quot;Info&quot; column shows the packet ID and length in packet list window, like:  So just curious: Why is the length field displayed here? Is it of any special purpose to be here?'''
date = "2013-11-26T02:18:00Z"
lastmod = "2013-11-29T02:27:00Z"
weight = 27391
keywords = [ "length", "radius", "display" ]
aliases = [ "/questions/27391" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [For what purpose the length field in Radius msg is shown in packet info](/questions/27391/for-what-purpose-the-length-field-in-radius-msg-is-shown-in-packet-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27391-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27391-score" class="post-score" title="current number of votes">0</div><span id="post-27391-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For Radius messages, the "Info" column shows the packet ID and length in packet list window, like: <img src="https://osqa-ask.wireshark.org/upfiles/radius_inWireshark_1.png" alt="alt text" /></p><p>So just curious: Why is the length field displayed here? Is it of any special purpose to be here?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-length" rel="tag" title="see questions tagged &#39;length&#39;">length</span> <span class="post-tag tag-link-radius" rel="tag" title="see questions tagged &#39;radius&#39;">radius</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '13, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/1076e6e05527f71ac739a860e91182e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Weller&#39;s gravatar image" /><p><span>Weller</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Weller has no accepted answers">0%</span></p></img></div></div><div id="comments-container-27391" class="comments-container"></div><div id="comment-tools-27391" class="comment-tools"></div><div class="clear"></div><div id="comment-27391-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27394"></span>

<div id="answer-container-27394" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27394-score" class="post-score" title="current number of votes">2</div><span id="post-27394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Weller has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No special reason, other than the author decided to do so. Maybe he had an issue with crippled frames. At least there is no comment in the source code that gives any better explanation.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Nov '13, 02:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Nov '13, 07:11</strong> </span></p></div></div><div id="comments-container-27394" class="comments-container"><span id="27552"></span><div id="comment-27552" class="comment"><div id="post-27552-score" class="comment-score"></div><div class="comment-text"><p>OK, I C. Thank you!</p></div><div id="comment-27552-info" class="comment-info"><span class="comment-age">(29 Nov '13, 02:27)</span> <span class="comment-user userinfo">Weller</span></div></div></div><div id="comment-tools-27394" class="comment-tools"></div><div class="clear"></div><div id="comment-27394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

