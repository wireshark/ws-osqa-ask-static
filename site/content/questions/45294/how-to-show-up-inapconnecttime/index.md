+++
type = "question"
title = "how to show up inap.connectTime"
description = '''Dear Sir, We have collected INAP@sigtran packets from ethernet interface and we found inap.connectTime seems like to be able to calculate the duration between INAP InitialDP and Connect. Could you help tell us how to show up inap.connectTime in filtered screen? Many Thanks &amp;amp; Best Regards, Richar...'''
date = "2015-08-21T04:36:00Z"
lastmod = "2015-08-21T04:36:00Z"
weight = 45294
keywords = [ "inap.connecttime", "inap" ]
aliases = [ "/questions/45294" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to show up inap.connectTime](/questions/45294/how-to-show-up-inapconnecttime)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45294-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45294-score" class="post-score" title="current number of votes">0</div><span id="post-45294-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sir,</p><p>We have collected <span class="__cf_email__" data-cfemail="327b7c736272415b554640535c">[email protected]</span> packets from ethernet interface and we found inap.connectTime seems like to be able to calculate the duration between INAP InitialDP and Connect. Could you help tell us how to show up inap.connectTime in filtered screen?</p><p>Many Thanks &amp; Best Regards, Richard</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-inap.connecttime" rel="tag" title="see questions tagged &#39;inap.connecttime&#39;">inap.connecttime</span> <span class="post-tag tag-link-inap" rel="tag" title="see questions tagged &#39;inap&#39;">inap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '15, 04:36</strong></p><img src="https://secure.gravatar.com/avatar/6dd3d2c5e26950e707ef6378f8a403f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chiang%20Richard&#39;s gravatar image" /><p><span>Chiang Richard</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chiang Richard has no accepted answers">0%</span></p></div></div><div id="comments-container-45294" class="comments-container"></div><div id="comment-tools-45294" class="comment-tools"></div><div class="clear"></div><div id="comment-45294-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

