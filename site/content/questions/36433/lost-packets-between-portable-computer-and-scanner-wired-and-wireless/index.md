+++
type = "question"
title = "Lost packets between portable computer and scanner, wired and wireless."
description = '''Can someone help me troubleshooting these logs? I have 2 logs, wired and wireless between a portable computer and a scanner. I looked at the lost packets, with wired there were 10 packets lost in 5 minutes and wireless there were like 159 lost packets in 5 minutes. I don&#x27;t know the reason what cause...'''
date = "2014-09-18T06:02:00Z"
lastmod = "2014-09-18T06:02:00Z"
weight = 36433
keywords = [ "wireless", "computer", "wired", "scanner", "portable" ]
aliases = [ "/questions/36433" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Lost packets between portable computer and scanner, wired and wireless.](/questions/36433/lost-packets-between-portable-computer-and-scanner-wired-and-wireless)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36433-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36433-score" class="post-score" title="current number of votes">0</div><span id="post-36433-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone help me troubleshooting these logs? I have 2 logs, wired and wireless between a portable computer and a scanner. I looked at the lost packets, with wired there were 10 packets lost in 5 minutes and wireless there were like 159 lost packets in 5 minutes. I don't know the reason what causes this and need help. There are also alot of retransmissions, I don't know what's good and what isn't. Can someone tell me what causes the lose of the packets and how I can get this to a minimum? Our customer wants to use the setup as wireless that's why I need t optimize it to work well with wireless.</p><p>Thanks in advance!</p><p><a href="https://onedrive.live.com/redir?resid=CE78C1CB2CAB3EE6!155&amp;authkey=!ACVWMy7ZdNbdBJM&amp;ithint=folder%2c">https://onedrive.live.com/redir?resid=CE78C1CB2CAB3EE6!155&amp;authkey=!ACVWMy7ZdNbdBJM&amp;ithint=folder%2c</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-computer" rel="tag" title="see questions tagged &#39;computer&#39;">computer</span> <span class="post-tag tag-link-wired" rel="tag" title="see questions tagged &#39;wired&#39;">wired</span> <span class="post-tag tag-link-scanner" rel="tag" title="see questions tagged &#39;scanner&#39;">scanner</span> <span class="post-tag tag-link-portable" rel="tag" title="see questions tagged &#39;portable&#39;">portable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '14, 06:02</strong></p><img src="https://secure.gravatar.com/avatar/79b5e3fff33e57e08ffb6426b75de367?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cihad%20Palta&#39;s gravatar image" /><p><span>Cihad Palta</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cihad Palta has no accepted answers">0%</span></p></div></div><div id="comments-container-36433" class="comments-container"></div><div id="comment-tools-36433" class="comment-tools"></div><div class="clear"></div><div id="comment-36433-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

