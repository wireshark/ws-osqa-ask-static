+++
type = "question"
title = "Cant get packets from other devices"
description = '''I&#x27;m new to this. I can&#x27;t get any http packets from other devices on my network. Promiscuous mode is enabled. I can only get packets from the computer which has Wireshark installed.  I&#x27;m running OSX 10.9.4  Any help is appreciated.  Add: Also to add, I have no monitor mode. There is not even a check ...'''
date = "2014-09-11T14:36:00Z"
lastmod = "2014-09-12T09:16:00Z"
weight = 36216
keywords = [ "http" ]
aliases = [ "/questions/36216" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cant get packets from other devices](/questions/36216/cant-get-packets-from-other-devices)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36216-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36216-score" class="post-score" title="current number of votes">0</div><span id="post-36216-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm new to this. I can't get any http packets from other devices on my network. Promiscuous mode is enabled. I can only get packets from the computer which has Wireshark installed.</p><p>I'm running OSX 10.9.4</p><p>Any help is appreciated.</p><p>Add: Also to add, I have no monitor mode. There is not even a check box to enable it in capture options.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '14, 14:36</strong></p><img src="https://secure.gravatar.com/avatar/1bbd4da42a0ff1b36031110feb9435b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dragon56&#39;s gravatar image" /><p><span>dragon56</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dragon56 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Sep '14, 16:31</strong> </span></p></div></div><div id="comments-container-36216" class="comments-container"><span id="36254"></span><div id="comment-36254" class="comment"><div id="post-36254-score" class="comment-score"></div><div class="comment-text"><p>How are your network devices connected, wired or wireless?</p></div><div id="comment-36254-info" class="comment-info"><span class="comment-age">(12 Sep '14, 00:46)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="36271"></span><div id="comment-36271" class="comment"><div id="post-36271-score" class="comment-score"></div><div class="comment-text"><p>All wireless.</p></div><div id="comment-36271-info" class="comment-info"><span class="comment-age">(12 Sep '14, 08:41)</span> <span class="comment-user userinfo">dragon56</span></div></div></div><div id="comment-tools-36216" class="comment-tools"></div><div class="clear"></div><div id="comment-36216-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36273"></span>

<div id="answer-container-36273" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36273-score" class="post-score" title="current number of votes">0</div><span id="post-36273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you looked at the advice on the Wireshark Wiki <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN Capture Setup</a> page, in particular the advice for <a href="http://wiki.wireshark.org/CaptureSetup/WLAN#Mac_OS_X">OSX</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Sep '14, 09:16</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36273" class="comments-container"></div><div id="comment-tools-36273" class="comment-tools"></div><div class="clear"></div><div id="comment-36273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

