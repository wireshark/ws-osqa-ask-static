+++
type = "question"
title = "How do I retrieve an http request payload as xml?"
description = '''I am unable to get the http request in xml format. The response it giving in both hexadecimal and xml format. I&#x27;m firing the request from one linux server to another.   0090 74 3a 20 74 65 78 74 2f 68 74 6d 6c 2c 20 69 6d t: text/html, im 00a0 61 67 65 2f 67 69 66 2c 20 69 6d 61 67 65 2f 6a age/gif,...'''
date = "2012-07-30T03:57:00Z"
lastmod = "2012-09-03T00:13:00Z"
weight = 13111
keywords = [ "xml" ]
aliases = [ "/questions/13111" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I retrieve an http request payload as xml?](/questions/13111/how-do-i-retrieve-an-http-request-payload-as-xml)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13111-score" class="post-score" title="current number of votes">0</div><span id="post-13111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am unable to get the http request in xml format. The response it giving in both hexadecimal and xml format. I'm firing the request from one linux server to another.</p><pre><code>0090  74 3a 20 74 65 78 74 2f 68 74 6d 6c 2c 20 69 6d   t: text/html, im
00a0  61 67 65 2f 67 69 66 2c 20 69 6d 61 67 65 2f 6a   age/gif, image/j
00b0  70 65 67 2c 20 2a 2f 2a 3b 20 71 3d 2e 32 0d 0a   peg, */*; q=.2..
00c0  43 6f 6e 6e 65 63 74 69 6f 6e 3a 20 4b 65 65 70   Connection: Keep
00d0  2d 41 6c 69 76 65 0d 0a 0d 0a 30 63 30 36 0d 0a   -Alive....0c06..
00e0  3c 3f 78 6d 6c 20 76 65 72 73 69 6f 6e 3d 22 31   &lt;?xml version=&quot;1
00f0  2e 30 22 20 65 6e 63 6f 64 69 6e 67 3d 22 55 54   .0&quot; encoding=&quot;UT
0100  46 2d 38 22 3f 3e 0a 3c 65 6e 76 3a 45 6e 76 65   F-8&quot;?&gt;.&lt;env:Enve
0110  6c 6f 70 65 20 78 6d 6c 6e 73 3a 65 6e 76 3d 22   lope xmlns:env=&quot;
0120  68 74 74 70 3a 2f 2f 77 77 77 2e 77 33 2e 6f 72   http://www.w3.or
0130  67 2f 32 30 30 33 2f 30 35 2f 73 6f 61 70 2d 65   g/2003/05/soap-e
0140  6e 76 65 6c 6f 70 65 22 3e 0a 20 20 20 20 3c 65   nvelope&quot;&gt;.    &lt;e
0150  6e 76 3a 48 65 61 64 65 72 20 77 73 61 3a 6d 75   nv:Header wsa:mu
0160  73 74 55 6e 64 65 72 73 74 61 6e 64 3d 22 31 22   stUnderstand=&quot;1&quot;
0170  20 78 6d 6c 6e 73 3a 77 73 61 3d 22 68 74 74 70    xmlns:wsa=&quot;http
0180  3a 2f 2f 77 77 77 2e 77 33 2e 6f 72 67 2f 32 30   ://www.w3.org/20
0190  30 35 2f 30 38 2f 61 64 64 72 65 73 73 69 6e 67   05/08/addressing
01a0  22 3e 0a 20 20 20 20 20 20 20 20 3c 77 73 61 3a   &quot;&gt;.        &lt;wsa:
01b0  54 6f 3e 68 74 74 70 3a 2f 2f 6c 6f 63 61 6c 68   To&gt;http://localh
01c0  6f 73 74 3a 38 30 38 30 2f 50 49 58 4d 61 6e 61   ost:8080/PIXMana
01d0  67 65 72 5f 53 65 72 76 69 63 65 2f 50 49 58 4d   ger_Service/PIXM</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xml" rel="tag" title="see questions tagged &#39;xml&#39;">xml</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '12, 03:57</strong></p><img src="https://secure.gravatar.com/avatar/d2b61556dda30d02213bedb910b92e4b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prasobh&#39;s gravatar image" /><p><span>prasobh</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prasobh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jul '12, 08:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-13111" class="comments-container"><span id="13112"></span><div id="comment-13112" class="comment"><div id="post-13112-score" class="comment-score"></div><div class="comment-text"><p>im using the below command to get the output. tshark -i eth1 host 10.81.172.109 -V -R http &gt; Tshark_Test1.cap</p></div><div id="comment-13112-info" class="comment-info"><span class="comment-age">(30 Jul '12, 04:00)</span> <span class="comment-user userinfo">prasobh</span></div></div></div><div id="comment-tools-13111" class="comment-tools"></div><div class="clear"></div><div id="comment-13111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13129"></span>

<div id="answer-container-13129" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13129-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13129-score" class="post-score" title="current number of votes">0</div><span id="post-13129-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please try this:</p><ol><li>use this display filter: <strong><code>http.request</code></strong></li><li>pick a request at your choice (left click)</li><li>right click -&gt; "Follow TCP Stream"</li></ol><p>You will see all requests (and responses) within that TCP stream in the pop-up window. Copy-Paste whatever part of the request you need.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '12, 13:11</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jul '12, 13:12</strong> </span></p></div></div><div id="comments-container-13129" class="comments-container"><span id="14000"></span><div id="comment-14000" class="comment"><div id="post-14000-score" class="comment-score"></div><div class="comment-text"><p>Hi Kurt, im getting the xml format of request while checking wire shark in gui mode. But not in command mode when using tshark command.</p></div><div id="comment-14000-info" class="comment-info"><span class="comment-age">(03 Sep '12, 00:13)</span> <span class="comment-user userinfo">prasobh</span></div></div></div><div id="comment-tools-13129" class="comment-tools"></div><div class="clear"></div><div id="comment-13129-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

