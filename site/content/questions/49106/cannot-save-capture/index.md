+++
type = "question"
title = "Cannot save capture"
description = '''Hi everyone, I downloaded and installed the latest Wireshark 2.0.1 on Windows7/64 machine. It does the capture but as soon as I stop it and try saving the data using menu &quot;File/Save&quot; or &quot;File/Save As...&quot; the program immediately closes. Anything I am missing? Thank you!'''
date = "2016-01-11T17:04:00Z"
lastmod = "2016-01-11T17:04:00Z"
weight = 49106
keywords = [ "filesavesaveas" ]
aliases = [ "/questions/49106" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot save capture](/questions/49106/cannot-save-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49106-score" class="post-score" title="current number of votes">0</div><span id="post-49106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone, I downloaded and installed the latest Wireshark 2.0.1 on Windows7/64 machine. It does the capture but as soon as I stop it and try saving the data using menu "File/Save" or "File/Save As..." the program immediately closes. Anything I am missing? Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filesavesaveas" rel="tag" title="see questions tagged &#39;filesavesaveas&#39;">filesavesaveas</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '16, 17:04</strong></p><img src="https://secure.gravatar.com/avatar/9d78f35aef4f24a5136eca825f769048?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mikef&#39;s gravatar image" /><p><span>mikef</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mikef has no accepted answers">0%</span></p></div></div><div id="comments-container-49106" class="comments-container"></div><div id="comment-tools-49106" class="comment-tools"></div><div class="clear"></div><div id="comment-49106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

