+++
type = "question"
title = "Menu bar stats remain after uninstall"
description = '''I&#x27;ve followed the instructions to uninstall Wireshark, but the menu bar stats are still there in Mac 10.6.8. How do I get rid of those menu items on the top right of my Mac screen? Thanks.'''
date = "2013-01-25T17:06:00Z"
lastmod = "2013-01-25T17:59:00Z"
weight = 17957
keywords = [ "man", "uninstall" ]
aliases = [ "/questions/17957" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Menu bar stats remain after uninstall](/questions/17957/menu-bar-stats-remain-after-uninstall)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17957-score" class="post-score" title="current number of votes">0</div><span id="post-17957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've followed the instructions to uninstall Wireshark, but the menu bar stats are still there in Mac 10.6.8. How do I get rid of those menu items on the top right of my Mac screen? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-man" rel="tag" title="see questions tagged &#39;man&#39;">man</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '13, 17:06</strong></p><img src="https://secure.gravatar.com/avatar/87050a8400165e2a095b9a868040d207?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JakeF&#39;s gravatar image" /><p><span>JakeF</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JakeF has no accepted answers">0%</span></p></div></div><div id="comments-container-17957" class="comments-container"></div><div id="comment-tools-17957" class="comment-tools"></div><div class="clear"></div><div id="comment-17957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17958"></span>

<div id="answer-container-17958" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17958-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17958-score" class="post-score" title="current number of votes">0</div><span id="post-17958-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>By finding out what software put them there, and either telling it not to put them there or removing that software. Wireshark doesn't install any menu extras - it doesn't even use the OS X menu bar, as it's an X11 application in the form in which wireshark.org distributes it, and has its own menu bars on its application windows - so those stats apparently came from something else.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jan '13, 17:59</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-17958" class="comments-container"></div><div id="comment-tools-17958" class="comment-tools"></div><div class="clear"></div><div id="comment-17958-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

