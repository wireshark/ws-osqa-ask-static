+++
type = "question"
title = "DPNSS Decode in 4.5.0"
description = '''I downloaded 4.5.0 today and notice that DPNSS has disappeared as a decode any reason for this? Does anyone one know the latest version supporting it? Thanks'''
date = "2013-08-15T10:01:00Z"
lastmod = "2013-08-15T15:42:00Z"
weight = 23802
keywords = [ "dpnss" ]
aliases = [ "/questions/23802" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [DPNSS Decode in 4.5.0](/questions/23802/dpnss-decode-in-450)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23802-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23802-score" class="post-score" title="current number of votes">0</div><span id="post-23802-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded 4.5.0 today and notice that DPNSS has disappeared as a decode any reason for this? Does anyone one know the latest version supporting it? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dpnss" rel="tag" title="see questions tagged &#39;dpnss&#39;">dpnss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Aug '13, 10:01</strong></p><img src="https://secure.gravatar.com/avatar/340d9eb1347d5a093025e403068eb809?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="P%20Gooding&#39;s gravatar image" /><p><span>P Gooding</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="P Gooding has no accepted answers">0%</span></p></div></div><div id="comments-container-23802" class="comments-container"><span id="23807"></span><div id="comment-23807" class="comment"><div id="post-23807-score" class="comment-score"></div><div class="comment-text"><p>My Mistake I meant to say 1.10.1 DPNSS has disappeared from the list of supported protocols as far as I can see. DPNSS should run over LAPD from my memory.</p></div><div id="comment-23807-info" class="comment-info"><span class="comment-age">(15 Aug '13, 14:39)</span> <span class="comment-user userinfo">P Gooding</span></div></div><span id="23810"></span><div id="comment-23810" class="comment"><div id="post-23810-score" class="comment-score"></div><div class="comment-text"><blockquote><p>My Mistake I meant to say 1.10.1 DPNSS has disappeared from the list of supported protocols as far as I can see.</p></blockquote><p>In 1.10.1 (Windows XP) I can still filter for <strong>dpnss</strong> and I still see DPNSS under</p><blockquote><p>Analyze -&gt; Enabled Protocols</p></blockquote><p>So, what is your OS and how did you figure out that DPNSS is missing in your installation?</p><p>Regards<br />
Kurt</p></div><div id="comment-23810-info" class="comment-info"><span class="comment-age">(15 Aug '13, 15:42)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23802" class="comment-tools"></div><div class="clear"></div><div id="comment-23802-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23806"></span>

<div id="answer-container-23806" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23806-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23806-score" class="post-score" title="current number of votes">0</div><span id="post-23806-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>4.5.0? Do you mean Wireshark 1.4.5? The DPNSS dissector is part of current Wireshark so it should work, perhaps you need to set some preferences? What protocol carries DPNSS in your capture(s)? Any reason for not using Wireshark 1.10.1? Even 1.4.x is quite old.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Aug '13, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span> </br></p></div></div><div id="comments-container-23806" class="comments-container"></div><div id="comment-tools-23806" class="comment-tools"></div><div class="clear"></div><div id="comment-23806-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

