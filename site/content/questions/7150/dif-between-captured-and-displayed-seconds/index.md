+++
type = "question"
title = "dif between captured and displayed seconds"
description = '''Hi, Does anyone know the difference between  &quot;Seconds since previous captured packet&quot; &quot;Seconds since previous displayed packet&quot;  for the time setting view John'''
date = "2011-10-29T19:53:00Z"
lastmod = "2011-10-29T20:15:00Z"
weight = 7150
keywords = [ "seconds", "times" ]
aliases = [ "/questions/7150" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [dif between captured and displayed seconds](/questions/7150/dif-between-captured-and-displayed-seconds)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7150-score" class="post-score" title="current number of votes">0</div><span id="post-7150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Does anyone know the difference between "Seconds since previous captured packet" "Seconds since previous displayed packet" for the time setting view</p><p>John</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-seconds" rel="tag" title="see questions tagged &#39;seconds&#39;">seconds</span> <span class="post-tag tag-link-times" rel="tag" title="see questions tagged &#39;times&#39;">times</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '11, 19:53</strong></p><img src="https://secure.gravatar.com/avatar/2cd01b51309490ac1ee00ae615abd0f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="desert1940fox&#39;s gravatar image" /><p><span>desert1940fox</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="desert1940fox has no accepted answers">0%</span></p></div></div><div id="comments-container-7150" class="comments-container"></div><div id="comment-tools-7150" class="comment-tools"></div><div class="clear"></div><div id="comment-7150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7151"></span>

<div id="answer-container-7151" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7151-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7151-score" class="post-score" title="current number of votes">3</div><span id="post-7151-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you have a display filter, so that not all of the packets in the capture are displayed, "seconds since previous captured packet" is the difference between the packet's time stamp and the previous packet in the capture, regardless of whether that packet is displayed or not, and "seconds since previous displayed packet" is the difference between the packet's time stamp and the previous packet in the list of displayed packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '11, 20:15</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-7151" class="comments-container"></div><div id="comment-tools-7151" class="comment-tools"></div><div class="clear"></div><div id="comment-7151-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

