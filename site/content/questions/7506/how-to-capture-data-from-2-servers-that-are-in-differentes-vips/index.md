+++
type = "question"
title = "how to capture data from 2 servers that are in differentes VIPs"
description = '''Hi, let me explain.. i have 2 sets of tornado servers that are in differents sites, with different VIP. So, im wondering how can i capture data from both and compare them, i mean, for example the Tornado A that is in Saint Louis, and the Tornado B that is in New York, we are experiencing TCP retrans...'''
date = "2011-11-18T10:09:00Z"
lastmod = "2011-11-18T10:09:00Z"
weight = 7506
keywords = [ "retransmissions" ]
aliases = [ "/questions/7506" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to capture data from 2 servers that are in differentes VIPs](/questions/7506/how-to-capture-data-from-2-servers-that-are-in-differentes-vips)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7506-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7506-score" class="post-score" title="current number of votes">0</div><span id="post-7506-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, let me explain.. i have 2 sets of tornado servers that are in differents sites, with different VIP. So, im wondering how can i capture data from both and compare them, i mean, for example the Tornado A that is in Saint Louis, and the Tornado B that is in New York, we are experiencing TCP retransmissions, and so i want to compare where the data is being retransmitted.</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '11, 10:09</strong></p><img src="https://secure.gravatar.com/avatar/b5c18dc9692a6d27b9bd7647c9fe501a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="guidin360&#39;s gravatar image" /><p><span>guidin360</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="guidin360 has no accepted answers">0%</span></p></div></div><div id="comments-container-7506" class="comments-container"></div><div id="comment-tools-7506" class="comment-tools"></div><div class="clear"></div><div id="comment-7506-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

