+++
type = "question"
title = "capture windows named pipe traffic such as &#92;pipe&#92;lsarpc, &#92;pipe&#92;netlogon, &#92;pipe&#92;samr"
description = '''I have a server that has named pipe such as &#92;pipe&#92;lsarpc, &#92;pipe&#92;netlogon, &#92;pipe&#92;samr, I need to decommission the server and wanted to know if any clients are still connecting thru those named pipes and which clients are connecting via it and doing what. And also to see if any clients are still be au...'''
date = "2014-02-18T17:48:00Z"
lastmod = "2014-02-18T17:48:00Z"
weight = 29991
keywords = [ "windows", "capture", "named", "pipes" ]
aliases = [ "/questions/29991" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [capture windows named pipe traffic such as \\pipe\\lsarpc, \\pipe\\netlogon, \\pipe\\samr](/questions/29991/capture-windows-named-pipe-traffic-such-as-pipelsarpc-pipenetlogon-pipesamr)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29991-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29991-score" class="post-score" title="current number of votes">0</div><span id="post-29991-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a server that has named pipe such as \pipe\lsarpc, \pipe\netlogon, \pipe\samr,</p><p>I need to decommission the server and wanted to know if any clients are still connecting thru those named pipes and which clients are connecting via it and doing what.</p><p>And also to see if any clients are still be authenticated thru that server.</p><p>Appreciated very much for any assistance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-named" rel="tag" title="see questions tagged &#39;named&#39;">named</span> <span class="post-tag tag-link-pipes" rel="tag" title="see questions tagged &#39;pipes&#39;">pipes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '14, 17:48</strong></p><img src="https://secure.gravatar.com/avatar/5e1f1353f808bffd75b119779468f66b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rm2014&#39;s gravatar image" /><p><span>rm2014</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rm2014 has no accepted answers">0%</span></p></div></div><div id="comments-container-29991" class="comments-container"></div><div id="comment-tools-29991" class="comment-tools"></div><div class="clear"></div><div id="comment-29991-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

