+++
type = "question"
title = "Have Wireshark realy an Trojaner/KeyLogger ?!!"
description = '''Hello, i have maked an virus/maleware test ---&amp;gt; https://www.virustotal.com/de/file/72949ee020a9b21c7cff94b68920db883504ddbb442870496095471b20707feb/analysis/ An Virustotal have found an (Trojan.Keylogger.Win32.46401)  Have Wireshark realy an Trojaner/KeyLogger ?!!'''
date = "2015-12-25T16:09:00Z"
lastmod = "2015-12-28T05:50:00Z"
weight = 48722
keywords = [ "download", "virus", "software", "keylogger", "wireshark" ]
aliases = [ "/questions/48722" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Have Wireshark realy an Trojaner/KeyLogger ?!!](/questions/48722/have-wireshark-realy-an-trojanerkeylogger)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48722-score" class="post-score" title="current number of votes">0</div><span id="post-48722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, i have maked an virus/maleware test ---&gt; <a href="https://www.virustotal.com/de/file/72949ee020a9b21c7cff94b68920db883504ddbb442870496095471b20707feb/analysis/">https://www.virustotal.com/de/file/72949ee020a9b21c7cff94b68920db883504ddbb442870496095471b20707feb/analysis/</a></p><p>An Virustotal have found an (Trojan.Keylogger.Win32.46401) Have Wireshark realy an Trojaner/KeyLogger ?!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-virus" rel="tag" title="see questions tagged &#39;virus&#39;">virus</span> <span class="post-tag tag-link-software" rel="tag" title="see questions tagged &#39;software&#39;">software</span> <span class="post-tag tag-link-keylogger" rel="tag" title="see questions tagged &#39;keylogger&#39;">keylogger</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Dec '15, 16:09</strong></p><img src="https://secure.gravatar.com/avatar/3021308d673cefe6f7479a880e8b5f84?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EASYLAYER&#39;s gravatar image" /><p><span>EASYLAYER</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EASYLAYER has no accepted answers">0%</span></p></div></div><div id="comments-container-48722" class="comments-container"></div><div id="comment-tools-48722" class="comment-tools"></div><div class="clear"></div><div id="comment-48722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48723"></span>

<div id="answer-container-48723" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48723-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48723-score" class="post-score" title="current number of votes">2</div><span id="post-48723-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, only Zillya as one out of fifty-three scanners used by VirusTotal gives an indication of a malware. Did you consider this could be a false positive? Isn't this is precicely what you use VirusTotal for? To get a varied evaluation of the target to be able to avoid false positives/negatives? They even say it themselves: "Probably harmless! There are strong indicators suggesting that this file is safe to use." I expect their signature database will be updated eventually (it's at 20151215 now) and then you can try again to see what happens.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Dec '15, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-48723" class="comments-container"><span id="48732"></span><div id="comment-48732" class="comment"><div id="post-48732-score" class="comment-score"></div><div class="comment-text"><p>I agree with Jaap.<br />
</p></div><div id="comment-48732-info" class="comment-info"><span class="comment-age">(28 Dec '15, 04:42)</span> <span class="comment-user userinfo">thetechfirm</span></div></div><span id="48734"></span><div id="comment-48734" class="comment"><div id="post-48734-score" class="comment-score"></div><div class="comment-text"><p>Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p></div><div id="comment-48734-info" class="comment-info"><span class="comment-age">(28 Dec '15, 05:50)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-48723" class="comment-tools"></div><div class="clear"></div><div id="comment-48723-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

