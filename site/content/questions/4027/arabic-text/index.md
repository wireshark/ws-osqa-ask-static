+++
type = "question"
title = "arabic text"
description = '''Hello friends, I have configured capture on ASA firewall and i can see everything when downloaded to wireshark application. i tested MSN conversation and POP protocol traffic with arabic text but i didn&#x27;t see the arabic text when viewing it in wireshark. Does the problem with ASA itself when capturi...'''
date = "2011-05-10T23:13:00Z"
lastmod = "2011-05-11T00:24:00Z"
weight = 4027
keywords = [ "text", "arabic" ]
aliases = [ "/questions/4027" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [arabic text](/questions/4027/arabic-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4027-score" class="post-score" title="current number of votes">0</div><span id="post-4027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello friends, I have configured capture on ASA firewall and i can see everything when downloaded to wireshark application. i tested MSN conversation and POP protocol traffic with arabic text but i didn't see the arabic text when viewing it in wireshark. Does the problem with ASA itself when capturing the traffic or from the wireshark itself? and how can i see the sniffed arabic text?</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-arabic" rel="tag" title="see questions tagged &#39;arabic&#39;">arabic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '11, 23:13</strong></p><img src="https://secure.gravatar.com/avatar/78a1fd7e0db832efc378a0009d83cd6a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Abood&#39;s gravatar image" /><p><span>Abood</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Abood has no accepted answers">0%</span></p></div></div><div id="comments-container-4027" class="comments-container"></div><div id="comment-tools-4027" class="comment-tools"></div><div class="clear"></div><div id="comment-4027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4028"></span>

<div id="answer-container-4028" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4028-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4028-score" class="post-score" title="current number of votes">0</div><span id="post-4028-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The problem is with Wireshark itself. It's really only equipped to handle ASCII, and some occasional unicode.</p><p>Depending on the protocol it may be possible to save the payload and use an external application to show the contents properly.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '11, 23:49</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4028" class="comments-container"><span id="4029"></span><div id="comment-4029" class="comment"><div id="post-4029-score" class="comment-score"></div><div class="comment-text"><p>thanks for your reply. Protocols: MSNMS &amp; POP which application shall i use and how? Thanks in advance</p></div><div id="comment-4029-info" class="comment-info"><span class="comment-age">(11 May '11, 00:24)</span> <span class="comment-user userinfo">Abood</span></div></div></div><div id="comment-tools-4028" class="comment-tools"></div><div class="clear"></div><div id="comment-4028-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

