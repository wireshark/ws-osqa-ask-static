+++
type = "question"
title = "How to check for TIFF files in a capture"
description = '''I have a capture of a fax to email which is a bad transmission. The file that is created is a TIFF file that is emailed to a persons inbox. I need to see that file so I can see where the issue is. '''
date = "2013-07-03T12:05:00Z"
lastmod = "2013-07-03T12:18:00Z"
weight = 22618
keywords = [ "ralbo-micro" ]
aliases = [ "/questions/22618" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to check for TIFF files in a capture](/questions/22618/how-to-check-for-tiff-files-in-a-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22618-score" class="post-score" title="current number of votes">0</div><span id="post-22618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a capture of a fax to email which is a bad transmission. The file that is created is a TIFF file that is emailed to a persons inbox. I need to see that file so I can see where the issue is.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ralbo-micro" rel="tag" title="see questions tagged &#39;ralbo-micro&#39;">ralbo-micro</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '13, 12:05</strong></p><img src="https://secure.gravatar.com/avatar/09f8313311c4437aa4fe1c4f580f946a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ralbo-micor&#39;s gravatar image" /><p><span>ralbo-micor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ralbo-micor has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jul '13, 12:11</strong> </span></p></div></div><div id="comments-container-22618" class="comments-container"></div><div id="comment-tools-22618" class="comment-tools"></div><div class="clear"></div><div id="comment-22618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22619"></span>

<div id="answer-container-22619" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22619-score" class="post-score" title="current number of votes">0</div><span id="post-22619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>right click on a packet of the SMTP connection. Then select "Follow TCP Stream". In the pop-up window, choose the right conversation (client:* -&gt; server:25) and click on "Save as" (in RAW format). Give that file the extension .tiff. Now you should be able to open the file with an image viewer.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jul '13, 12:18</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jul '13, 12:20</strong> </span></p></div></div><div id="comments-container-22619" class="comments-container"></div><div id="comment-tools-22619" class="comment-tools"></div><div class="clear"></div><div id="comment-22619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

