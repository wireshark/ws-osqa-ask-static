+++
type = "question"
title = "sniff a specified remote ip"
description = '''how can i sniff a specified remote ip server with wireshark scanner?'''
date = "2016-12-17T23:04:00Z"
lastmod = "2016-12-18T06:56:00Z"
weight = 58205
keywords = [ "sniffng", "wireshark" ]
aliases = [ "/questions/58205" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [sniff a specified remote ip](/questions/58205/sniff-a-specified-remote-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58205-score" class="post-score" title="current number of votes">0</div><span id="post-58205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i sniff a specified remote ip server with wireshark scanner?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffng" rel="tag" title="see questions tagged &#39;sniffng&#39;">sniffng</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '16, 23:04</strong></p><img src="https://secure.gravatar.com/avatar/5bb19a01c4e56b8f9fbb907b717a5f6a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="acropo&#39;s gravatar image" /><p><span>acropo</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="acropo has no accepted answers">0%</span></p></div></div><div id="comments-container-58205" class="comments-container"></div><div id="comment-tools-58205" class="comment-tools"></div><div class="clear"></div><div id="comment-58205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58211"></span>

<div id="answer-container-58211" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58211-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58211-score" class="post-score" title="current number of votes">0</div><span id="post-58211-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You may want to begin studying <a href="https://wiki.wireshark.org/CaptureSetup">capture setup</a>, which goes into that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '16, 06:56</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-58211" class="comments-container"></div><div id="comment-tools-58211" class="comment-tools"></div><div class="clear"></div><div id="comment-58211-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

