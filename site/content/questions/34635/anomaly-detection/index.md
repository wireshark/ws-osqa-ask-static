+++
type = "question"
title = "anomaly detection"
description = '''Can wireshark be use to detect anomaly in capured packets?'''
date = "2014-07-14T14:18:00Z"
lastmod = "2014-07-15T02:16:00Z"
weight = 34635
keywords = [ "pcap#" ]
aliases = [ "/questions/34635" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [anomaly detection](/questions/34635/anomaly-detection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34635-score" class="post-score" title="current number of votes">0</div><span id="post-34635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can wireshark be use to detect anomaly in capured packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap#" rel="tag" title="see questions tagged &#39;pcap#&#39;">pcap#</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jul '14, 14:18</strong></p><img src="https://secure.gravatar.com/avatar/e3778ae8a621767b52cdf5a8052a93c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maigana&#39;s gravatar image" /><p><span>Maigana</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maigana has no accepted answers">0%</span></p></div></div><div id="comments-container-34635" class="comments-container"><span id="34637"></span><div id="comment-34637" class="comment"><div id="post-34637-score" class="comment-score"></div><div class="comment-text"><p>The question sounds suspiciously like a homework or test question.</p><p>If so, my inclination would be to say learn a bit about the capabilities of Wireshark (and/or communications protocol analyzers, in general). :)</p><p>There's lots of information available on the web.</p></div><div id="comment-34637-info" class="comment-info"><span class="comment-age">(14 Jul '14, 14:40)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-34635" class="comment-tools"></div><div class="clear"></div><div id="comment-34635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="34636"></span>

<div id="answer-container-34636" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34636-score" class="post-score" title="current number of votes">0</div><span id="post-34636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, but <em>you</em> have to decide what constitutes an anomaly, and if it's not one of the conditions that Wireshark already highlights (via a coloring rule or an Expert Info message), then you'll have to create a coloring rule or display filter to identify the anomalous packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jul '14, 14:36</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-34636" class="comments-container"></div><div id="comment-tools-34636" class="comment-tools"></div><div class="clear"></div><div id="comment-34636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="34645"></span>

<div id="answer-container-34645" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34645-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34645-score" class="post-score" title="current number of votes">0</div><span id="post-34645-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>see my answer to a very similar question:</p><blockquote><p><a href="http://ask.wireshark.org/questions/31235/wireshark-question">http://ask.wireshark.org/questions/31235/wireshark-question</a></p></blockquote><p>BTW: There is no standardized network, so everything we would say about an 'anomaly' in captured packets, could be totally normal for anybody else.</p><p>If you are looking for an 'expert system' that tells you that it found some 'standard' problems in your capture file, you could use the Expert info messages, as mentioned by <span>@Jim Aragon</span>. But don't expect too much from that. It's just an overview of some typical problems, that helps knowledgeable troubleshooters to nail down a problem.</p><p>I won't be like this: Your application <strong>Expencemaster</strong> used by user <strong>Marky Mark</strong> on the server-farm <strong>sf2763</strong> in the datacenter <strong>DC272</strong> is slow in response between 09:00 AM and 11:00 AM, because there is an overload of the DNS servers at that time.</p><p>That won't happen, at least not in the next couple of years.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '14, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-34645" class="comments-container"></div><div id="comment-tools-34645" class="comment-tools"></div><div class="clear"></div><div id="comment-34645-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

