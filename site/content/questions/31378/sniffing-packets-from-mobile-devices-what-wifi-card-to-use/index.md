+++
type = "question"
title = "Sniffing packets from mobile devices, what wifi card to use?"
description = '''Hello! We are 2 students assigned to build a PC that is able to &quot;sniff&quot; packets from our mobile devices (both iOS and Android). We have no experience with packet capture or Wireshark before and need some help with what Wifi card we should buy. We can adjust to different operative systems (prefer lin...'''
date = "2014-04-02T05:03:00Z"
lastmod = "2014-04-02T05:48:00Z"
weight = 31378
keywords = [ "wifi", "sniffing", "packet-capture", "pcap", "wireshark" ]
aliases = [ "/questions/31378" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Sniffing packets from mobile devices, what wifi card to use?](/questions/31378/sniffing-packets-from-mobile-devices-what-wifi-card-to-use)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31378-score" class="post-score" title="current number of votes">0</div><span id="post-31378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello!</p><p>We are 2 students assigned to build a PC that is able to "sniff" packets from our mobile devices (both iOS and Android). We have no experience with packet capture or Wireshark before and need some help with what Wifi card we should buy. We can adjust to different operative systems (prefer linux) but we need help to find a card that we know is capable of capturing packets from mobile devices. And if there is anything more we should be aware of before we buy a card you are welcome to tell us. We have attached a photo you can take a look at if the text is not clear to you.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/roughdraft.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '14, 05:03</strong></p><img src="https://secure.gravatar.com/avatar/588b8bb1f780f36e866309884979dd27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pervan&#39;s gravatar image" /><p><span>pervan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pervan has no accepted answers">0%</span></p></img></div></div><div id="comments-container-31378" class="comments-container"><span id="31380"></span><div id="comment-31380" class="comment"><div id="post-31380-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "WiFi", is it 802.11 a.k.a. WLAN, or are you talking about 3G/4G/UMTS/LTE?</p></div><div id="comment-31380-info" class="comment-info"><span class="comment-age">(02 Apr '14, 05:11)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="31381"></span><div id="comment-31381" class="comment"><div id="post-31381-score" class="comment-score"></div><div class="comment-text"><p>802.11 (WLAN). So the thing we need is a 802.11 card to use as an access point and capture the packets comming from our mobile devices, we just dont know what card to use.</p></div><div id="comment-31381-info" class="comment-info"><span class="comment-age">(02 Apr '14, 05:16)</span> <span class="comment-user userinfo">pervan</span></div></div></div><div id="comment-tools-31378" class="comment-tools"></div><div class="clear"></div><div id="comment-31378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="31379"></span>

<div id="answer-container-31379" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31379-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31379-score" class="post-score" title="current number of votes">3</div><span id="post-31379-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's a whole lot of info on the <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN capture</a> page on the wiki. Have a look at that and then come back with any further questions you have.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '14, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-31379" class="comments-container"><span id="31383"></span><div id="comment-31383" class="comment"><div id="post-31383-score" class="comment-score"></div><div class="comment-text"><p>Reading the wikipage right now and taking notes, so thanks for that! //The other student</p></div><div id="comment-31383-info" class="comment-info"><span class="comment-age">(02 Apr '14, 05:32)</span> <span class="comment-user userinfo">JET</span></div></div></div><div id="comment-tools-31379" class="comment-tools"></div><div class="clear"></div><div id="comment-31379-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="31382"></span>

<div id="answer-container-31382" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31382-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31382-score" class="post-score" title="current number of votes">2</div><span id="post-31382-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could also just set up a dedicated WLAN access point that you hook up to the university network and force the devices to use it. Then capture on the ethernet link, if that's easier.</p><p>For capture cards you could use AirPCAP adapters, or almost any device on linux as long as you manage to put them in monitor mode.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '14, 05:21</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-31382" class="comments-container"><span id="31384"></span><div id="comment-31384" class="comment"><div id="post-31384-score" class="comment-score"></div><div class="comment-text"><p>We've looked at the AirPCAP adapter now and we'll run it by our teacher/handler and see what he thinks, so thanks for the tip!</p><p>"You could also just set up a dedicated WLAN access point that you hook up to the university network and force the devices to use it. Then capture on the ethernet link, if that's easier."</p><p>Yeah this seems to be something more what we had in mind. Guess we could have been a bit clearer on that from the start! So we should be able to use, for an example,an AirPCAP adapter as an AP that we force our mobile devices to use?</p></div><div id="comment-31384-info" class="comment-info"><span class="comment-age">(02 Apr '14, 05:44)</span> <span class="comment-user userinfo">JET</span></div></div><span id="31385"></span><div id="comment-31385" class="comment"><div id="post-31385-score" class="comment-score"></div><div class="comment-text"><p>I doubt you can use the capturing adapter as a tethering device. Keep in mind that WiFi cards are half duplex, and when capturing you will only be able to read packets from the air, not send anything. AirPCAP does not behave like a normal WiFi card, it is record only - unless using the injection feature, which I haven't used so far.</p><p>My approach would be to use the dedicated AP, have the mobile devices connect to it, and then capture their traffic on the wired connection to the university network by SPAN/TAP.</p></div><div id="comment-31385-info" class="comment-info"><span class="comment-age">(02 Apr '14, 05:48)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-31382" class="comment-tools"></div><div class="clear"></div><div id="comment-31382-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

