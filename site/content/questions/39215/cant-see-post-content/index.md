+++
type = "question"
title = "Can&#x27;t see post content"
description = '''Hey, i applied this filter to my 1gb pcap file [yeah, i captured quite a lot] http.request.method == &quot;POST&quot;  and i can&#x27;t seem to be able to find the post data. I might not know where to look, given the fact that i am still a noob with this application. So, how do i see the post content?'''
date = "2015-01-16T10:29:00Z"
lastmod = "2015-01-16T10:36:00Z"
weight = 39215
keywords = [ "filter", "post", "pcap" ]
aliases = [ "/questions/39215" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see post content](/questions/39215/cant-see-post-content)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39215-score" class="post-score" title="current number of votes">0</div><span id="post-39215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey, i applied this filter to my 1gb pcap file [yeah, i captured quite a lot]</p><pre><code>http.request.method == &quot;POST&quot;</code></pre><p>and i can't seem to be able to find the post data. I might not know where to look, given the fact that i am still a noob with this application. So, how do i see the post content?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-post" rel="tag" title="see questions tagged &#39;post&#39;">post</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '15, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/57a346c51606f30cffeaf3ea7bf48656?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LGMan&#39;s gravatar image" /><p><span>LGMan</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LGMan has one accepted answer">100%</span></p></div></div><div id="comments-container-39215" class="comments-container"></div><div id="comment-tools-39215" class="comment-tools"></div><div class="clear"></div><div id="comment-39215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39216"></span>

<div id="answer-container-39216" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39216-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39216-score" class="post-score" title="current number of votes">0</div><span id="post-39216-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="LGMan has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Figured it out - Right click the packet you want to see and choose Follow TCP stream. A window will pop up. Red color means data send and blue data received. Search among sent data [red color] and you will find data sent.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jan '15, 10:36</strong></p><img src="https://secure.gravatar.com/avatar/57a346c51606f30cffeaf3ea7bf48656?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LGMan&#39;s gravatar image" /><p><span>LGMan</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LGMan has one accepted answer">100%</span></p></div></div><div id="comments-container-39216" class="comments-container"></div><div id="comment-tools-39216" class="comment-tools"></div><div class="clear"></div><div id="comment-39216-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

