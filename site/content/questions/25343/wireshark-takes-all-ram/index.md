+++
type = "question"
title = "Wireshark takes all RAM"
description = '''That&#x27;s my biggest complain. Sometimes I fire it up to capture sip||rtp and forget about it. When I come back next day my PC feels crippled. Sometimes I don&#x27;t notice that problem right away and keep working half a day on barely breathing pc. Isn&#x27;t there some kind of backup file for ongoing capture to...'''
date = "2013-09-29T17:29:00Z"
lastmod = "2013-09-29T17:36:00Z"
weight = 25343
keywords = [ "memory" ]
aliases = [ "/questions/25343" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark takes all RAM](/questions/25343/wireshark-takes-all-ram)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25343-score" class="post-score" title="current number of votes">0</div><span id="post-25343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>That's my biggest complain. Sometimes I fire it up to capture sip||rtp and forget about it. When I come back next day my PC feels crippled. Sometimes I don't notice that problem right away and keep working half a day on barely breathing pc.</p><p>Isn't there some kind of backup file for ongoing capture to dump excessively huge captures? Why is that wireshark.exe ends up eating 4-5GB of ram over a couple of days??</p><p>One suggestion is to save as much data to a file when wireshark is minimized (e.g. when it cannot be used by scrolling in realtime capture list etc). So that in this mode the beast doesn't kill my pc.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-memory" rel="tag" title="see questions tagged &#39;memory&#39;">memory</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '13, 17:29</strong></p><img src="https://secure.gravatar.com/avatar/85a49cd89084e6512dee0f140e86d5b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="psp80&#39;s gravatar image" /><p><span>psp80</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="psp80 has no accepted answers">0%</span></p></div></div><div id="comments-container-25343" class="comments-container"></div><div id="comment-tools-25343" class="comment-tools"></div><div class="clear"></div><div id="comment-25343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25344"></span>

<div id="answer-container-25344" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25344-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25344-score" class="post-score" title="current number of votes">3</div><span id="post-25344-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is also a lot of information in the Wiki.</p><blockquote><p><a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory">http://wiki.wireshark.org/KnownBugs/OutOfMemory</a><br />
<a href="http://wiki.wireshark.org/Performance">http://wiki.wireshark.org/Performance</a><br />
</p></blockquote><p>Please see also the numerous other questions about this issue.</p><blockquote><p><a href="http://ask.wireshark.org/tags/memory/">http://ask.wireshark.org/tags/memory/</a><br />
<a href="http://ask.wireshark.org/tags/out-of-memory/">http://ask.wireshark.org/tags/out-of-memory/</a><br />
<a href="http://ask.wireshark.org/questions/25091/wireshark-tshark-out-of-memory-problem">http://ask.wireshark.org/questions/25091/wireshark-tshark-out-of-memory-problem</a><br />
</p></blockquote><p>and in Blogs</p><blockquote><p><a href="http://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/">http://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/</a><br />
</p></blockquote><p>and ....</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Sep '13, 17:36</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Sep '13, 17:37</strong> </span></p></div></div><div id="comments-container-25344" class="comments-container"></div><div id="comment-tools-25344" class="comment-tools"></div><div class="clear"></div><div id="comment-25344-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

