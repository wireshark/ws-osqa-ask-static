+++
type = "question"
title = "Wireshark TCAP BER error"
description = '''Hi Community, I am trying to decode a GSM-MAP message and always see an error in the TCAP layer as &#x27;BER Error: This error lies beyond the end of the known sequence definition.&#x27; This error is only in the first MAP-PUSSR and MAP-USSR message, rest all messages are fine. I am using Wireshark version 1....'''
date = "2012-01-13T11:43:00Z"
lastmod = "2012-01-13T19:15:00Z"
weight = 8378
keywords = [ "tcap", "gsm", "ussd" ]
aliases = [ "/questions/8378" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark TCAP BER error](/questions/8378/wireshark-tcap-ber-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8378-score" class="post-score" title="current number of votes">0</div><span id="post-8378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Community,</p><p>I am trying to decode a GSM-MAP message and always see an error in the TCAP layer as 'BER Error: This error lies beyond the end of the known sequence definition.'</p><p>This error is only in the first MAP-PUSSR and MAP-USSR message, rest all messages are fine. I am using Wireshark version 1.6.1 to capture data. Kindly note MAP is running on SCTP.</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcap" rel="tag" title="see questions tagged &#39;tcap&#39;">tcap</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-ussd" rel="tag" title="see questions tagged &#39;ussd&#39;">ussd</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '12, 11:43</strong></p><img src="https://secure.gravatar.com/avatar/a529663639fd7f59faae2b1f2e82ec72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gsmguy&#39;s gravatar image" /><p><span>gsmguy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gsmguy has no accepted answers">0%</span></p></div></div><div id="comments-container-8378" class="comments-container"></div><div id="comment-tools-8378" class="comment-tools"></div><div class="clear"></div><div id="comment-8378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8381"></span>

<div id="answer-container-8381" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8381-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8381-score" class="post-score" title="current number of votes">0</div><span id="post-8381-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'd suggest filing a bug on <a href="http://bugs.wireshark.org/">the Wireshark bugzilla</a> for this; attach to that bug a capture that shows the problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jan '12, 19:15</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Jan '12, 19:16</strong> </span></p></div></div><div id="comments-container-8381" class="comments-container"></div><div id="comment-tools-8381" class="comment-tools"></div><div class="clear"></div><div id="comment-8381-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

