+++
type = "question"
title = "Can I get the exact peak/maximum rate value from IO Graphs"
description = '''I set Tick interval as 1 sec at X Axis and Unit as Bits/Tick at Y axis. Can I get the exact peak/maximum rate value from IO Graphs? Thank you!'''
date = "2012-09-09T18:42:00Z"
lastmod = "2012-09-10T00:07:00Z"
weight = 14151
keywords = [ "io", "graphs", "rate", "peak", "value" ]
aliases = [ "/questions/14151" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I get the exact peak/maximum rate value from IO Graphs](/questions/14151/can-i-get-the-exact-peakmaximum-rate-value-from-io-graphs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14151-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14151-score" class="post-score" title="current number of votes">0</div><span id="post-14151-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I set Tick interval as 1 sec at X Axis and Unit as Bits/Tick at Y axis.</p><p>Can I get the <strong>exact</strong> peak/maximum rate value from IO Graphs?</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-io" rel="tag" title="see questions tagged &#39;io&#39;">io</span> <span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span> <span class="post-tag tag-link-rate" rel="tag" title="see questions tagged &#39;rate&#39;">rate</span> <span class="post-tag tag-link-peak" rel="tag" title="see questions tagged &#39;peak&#39;">peak</span> <span class="post-tag tag-link-value" rel="tag" title="see questions tagged &#39;value&#39;">value</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '12, 18:42</strong></p><img src="https://secure.gravatar.com/avatar/54b0ef640d513b2b7a55c3f28a11c965?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mobilefone&#39;s gravatar image" /><p><span>mobilefone</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mobilefone has no accepted answers">0%</span></p></div></div><div id="comments-container-14151" class="comments-container"></div><div id="comment-tools-14151" class="comment-tools"></div><div class="clear"></div><div id="comment-14151-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14153"></span>

<div id="answer-container-14153" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14153-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14153-score" class="post-score" title="current number of votes">0</div><span id="post-14153-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't know how exact it has to be for you, but if you need the absolute number of bits/second down to the last digit you probably can't get that from the I/O graph. As far as I know there is no way to "ask" the I/O graph what the highest peak is in absolute numbers.</p><p>Question is, why isn't it enough to work with an aproximate value? The I/O graph is usually good enough to get a reading that tells you what you need to know; it is rarely important to get more detailed values that what you can already read from it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Sep '12, 00:07</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14153" class="comments-container"></div><div id="comment-tools-14153" class="comment-tools"></div><div class="clear"></div><div id="comment-14153-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

