+++
type = "question"
title = "[closed] How does TCP determines sequence number"
description = '''A 3000-km-long T1 trunk is used to transmit 64-byte frames using Go-Back-N protocol. If the propagation speed is 6 microseconds/km, how many bits should the sequence numbers be?'''
date = "2015-03-26T08:16:00Z"
lastmod = "2015-03-26T08:19:00Z"
weight = 40898
keywords = [ "gbn", "t1trunk" ]
aliases = [ "/questions/40898" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How does TCP determines sequence number](/questions/40898/how-does-tcp-determines-sequence-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40898-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40898-score" class="post-score" title="current number of votes">0</div><span id="post-40898-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A 3000-km-long T1 trunk is used to transmit 64-byte frames using Go-Back-N protocol. If the propagation speed is 6 microseconds/km, how many bits should the sequence numbers be?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gbn" rel="tag" title="see questions tagged &#39;gbn&#39;">gbn</span> <span class="post-tag tag-link-t1trunk" rel="tag" title="see questions tagged &#39;t1trunk&#39;">t1trunk</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '15, 08:16</strong></p><img src="https://secure.gravatar.com/avatar/400c28a9159fb180ae9f3e38d48250fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deegirl&#39;s gravatar image" /><p><span>deegirl</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deegirl has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>26 Mar '15, 08:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-40898" class="comments-container"><span id="40900"></span><div id="comment-40900" class="comment"><div id="post-40900-score" class="comment-score"></div><div class="comment-text"><p>Not a Wireshark question.</p></div><div id="comment-40900-info" class="comment-info"><span class="comment-age">(26 Mar '15, 08:19)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-40898" class="comment-tools"></div><div class="clear"></div><div id="comment-40898-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 26 Mar '15, 08:19

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40899"></span>

<div id="answer-container-40899" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40899-score" class="post-score" title="current number of votes">0</div><span id="post-40899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>TCP sequence numbers are <strong>always</strong> 32 bits long ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '15, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40899" class="comments-container"></div><div id="comment-tools-40899" class="comment-tools"></div><div class="clear"></div><div id="comment-40899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

