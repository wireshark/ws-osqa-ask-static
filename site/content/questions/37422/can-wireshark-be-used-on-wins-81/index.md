+++
type = "question"
title = "Can Wireshark be used on Wins 8.1?"
description = '''Hi,  Previously I am using Wireshark on wins 7 and it was working perfectly on my application. Recently tried on wins 8.1 and it gives me this error: system cannot capture network packets because there is confusion of which adapter to use What does this error actually mean? I have tried to switch of...'''
date = "2014-10-28T19:14:00Z"
lastmod = "2014-10-29T05:13:00Z"
weight = 37422
keywords = [ "windows8" ]
aliases = [ "/questions/37422" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark be used on Wins 8.1?](/questions/37422/can-wireshark-be-used-on-wins-81)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37422-score" class="post-score" title="current number of votes">0</div><span id="post-37422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Previously I am using Wireshark on wins 7 and it was working perfectly on my application. Recently tried on wins 8.1 and it gives me this error:</p><p>system cannot capture network packets because there is confusion of which adapter to use</p><p>What does this error actually mean? I have tried to switch off the WiFi but failed.</p><p>Please advise. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '14, 19:14</strong></p><img src="https://secure.gravatar.com/avatar/8682ec6319a0ebff7daad336cccab913?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tan&#39;s gravatar image" /><p><span>Tan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tan has no accepted answers">0%</span></p></div></div><div id="comments-container-37422" class="comments-container"><span id="37431"></span><div id="comment-37431" class="comment"><div id="post-37431-score" class="comment-score"></div><div class="comment-text"><p>what is you Wireshark version and where did you download it from?</p><p>Reason: I can't find that error message neither in the source code of Wireshark nor in the code of WinPcap !?!.</p><p>Can you please add a screenshot of that message?</p></div><div id="comment-37431-info" class="comment-info"><span class="comment-age">(29 Oct '14, 05:13)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37422" class="comment-tools"></div><div class="clear"></div><div id="comment-37422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

