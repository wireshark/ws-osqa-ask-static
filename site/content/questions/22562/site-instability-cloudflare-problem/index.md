+++
type = "question"
title = "Site instability - Cloudflare problem"
description = '''Hi, I just want to report recurrent problems with access to http://ask.wireshark.org during the last two weeks. I often see error messages from Cloudflare like &quot;no live server available, please try later&quot; or something like this: 502 Bad Gateway cloudflare-nginx   Are these problems location related ...'''
date = "2013-07-02T11:14:00Z"
lastmod = "2013-07-02T11:32:00Z"
weight = 22562
keywords = [ "cloudflare" ]
aliases = [ "/questions/22562" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Site instability - Cloudflare problem](/questions/22562/site-instability-cloudflare-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22562-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22562-score" class="post-score" title="current number of votes">0</div><span id="post-22562-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I just want to report recurrent problems with access to <a href="http://ask.wireshark.org">http://ask.wireshark.org</a> during the last two weeks. I often see error messages from Cloudflare like "no live server available, please try later" or something like this:</p><pre><code>502 Bad Gateway
cloudflare-nginx</code></pre><p>Are these problems location related (access from different regions of the world), or a general problem with the hosting of the site in the Cloudflare cloud?</p><p>Regards<br />
Kurt</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cloudflare" rel="tag" title="see questions tagged &#39;cloudflare&#39;">cloudflare</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jul '13, 11:14</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-22562" class="comments-container"></div><div id="comment-tools-22562" class="comment-tools"></div><div class="clear"></div><div id="comment-22562-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22564"></span>

<div id="answer-container-22564" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22564-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22564-score" class="post-score" title="current number of votes">0</div><span id="post-22564-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We're having backend issues that I won't be able to address fully until next week, unfortunately. My apologies for the downtime.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jul '13, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-22564" class="comments-container"><span id="22565"></span><div id="comment-22565" class="comment"><div id="post-22565-score" class="comment-score"></div><div class="comment-text"><p>O.K. thanks for the update.</p></div><div id="comment-22565-info" class="comment-info"><span class="comment-age">(02 Jul '13, 11:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-22564" class="comment-tools"></div><div class="clear"></div><div id="comment-22564-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

