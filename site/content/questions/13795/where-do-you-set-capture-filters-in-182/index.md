+++
type = "question"
title = "Where do you set capture filters in 1.8.2 ?"
description = '''The Capture / Options dialog has changed. Now it shows the list of adapters ... but the place where you select a capture filter (or type in an new one) has disappeared. The Help file still shows the old dialog, so it&#x27;s no help. Where do you set a capture filter before starting a capture in 1.8.2 ?'''
date = "2012-08-21T11:39:00Z"
lastmod = "2012-08-21T11:56:00Z"
weight = 13795
keywords = [ "filter", "capture", "options" ]
aliases = [ "/questions/13795" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where do you set capture filters in 1.8.2 ?](/questions/13795/where-do-you-set-capture-filters-in-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13795-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13795-score" class="post-score" title="current number of votes">0</div><span id="post-13795-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The Capture / Options dialog has changed. Now it shows the list of adapters ... but the place where you select a capture filter (or type in an new one) has disappeared. The Help file still shows the old dialog, so it's no help.</p><p>Where do you set a capture filter before starting a capture in 1.8.2 ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-options" rel="tag" title="see questions tagged &#39;options&#39;">options</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '12, 11:39</strong></p><img src="https://secure.gravatar.com/avatar/48c39c922c592af48f346ee005ee74c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rmhartman&#39;s gravatar image" /><p><span>rmhartman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rmhartman has no accepted answers">0%</span></p></div></div><div id="comments-container-13795" class="comments-container"></div><div id="comment-tools-13795" class="comment-tools"></div><div class="clear"></div><div id="comment-13795-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13797"></span>

<div id="answer-container-13797" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13797-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13797-score" class="post-score" title="current number of votes">1</div><span id="post-13797-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Refer to the answer provided by <a href="http://ask.wireshark.org/users/527/jim-aragon">Jim Aragon</a> to <a href="http://ask.wireshark.org/questions/12221/how-to-chose-a-capture-filter-in-version-180-as-previous-in-the-capture-options-menu-of-version-16">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Aug '12, 11:56</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-13797" class="comments-container"></div><div id="comment-tools-13797" class="comment-tools"></div><div class="clear"></div><div id="comment-13797-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

