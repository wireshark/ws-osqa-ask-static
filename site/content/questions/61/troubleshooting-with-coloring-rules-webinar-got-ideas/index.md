+++
type = "question"
title = "Troubleshooting with Coloring Rules webinar - got ideas?"
description = '''Hi folks -  This new Wireshark Jumpstart coming on October 19th - it&#x27;s free, as all the Jumpstarts, are.  Registration is open over at http://www.chappellseminars.com/s-wiresharkcolors.html.  If you have anything you would like me to cover/should be covered, let me know. '''
date = "2010-09-14T08:22:00Z"
lastmod = "2010-09-14T08:22:00Z"
weight = 61
keywords = [ "color-rules", "training", "webinar", "free" ]
aliases = [ "/questions/61" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Troubleshooting with Coloring Rules webinar - got ideas?](/questions/61/troubleshooting-with-coloring-rules-webinar-got-ideas)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61-score" class="post-score" title="current number of votes">0</div><span id="post-61-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi folks -</p><p>This new Wireshark Jumpstart coming on October 19th - it's free, as all the Jumpstarts, are.</p><p>Registration is open over at http://www.chappellseminars.com/s-wiresharkcolors.html.</p><p>If you have anything you would like me to cover/should be covered, let me know.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color-rules" rel="tag" title="see questions tagged &#39;color-rules&#39;">color-rules</span> <span class="post-tag tag-link-training" rel="tag" title="see questions tagged &#39;training&#39;">training</span> <span class="post-tag tag-link-webinar" rel="tag" title="see questions tagged &#39;webinar&#39;">webinar</span> <span class="post-tag tag-link-free" rel="tag" title="see questions tagged &#39;free&#39;">free</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '10, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-61" class="comments-container"></div><div id="comment-tools-61" class="comment-tools"></div><div class="clear"></div><div id="comment-61-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

