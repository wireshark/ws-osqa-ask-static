+++
type = "question"
title = "how can I capture RPL packets using wireshark 1.5?"
description = '''Hi! I need to analyse RPL packets exchanged between telosb motes. Even the interfaces that wiresshark give to me can&#x27;t capture RPL packets.  Please guide me how can I capture RPL packets using wireshark 1.5? Is there any configuration that I have to add? Great thanks!'''
date = "2011-05-05T03:19:00Z"
lastmod = "2011-05-05T03:19:00Z"
weight = 3934
keywords = [ "capture", "packets", "rpl" ]
aliases = [ "/questions/3934" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how can I capture RPL packets using wireshark 1.5?](/questions/3934/how-can-i-capture-rpl-packets-using-wireshark-15)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3934-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3934-score" class="post-score" title="current number of votes">0</div><span id="post-3934-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>I need to analyse RPL packets exchanged between telosb motes. Even the interfaces that wiresshark give to me can't capture RPL packets. Please guide me how can I capture RPL packets using wireshark 1.5? Is there any configuration that I have to add? Great thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-rpl" rel="tag" title="see questions tagged &#39;rpl&#39;">rpl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '11, 03:19</strong></p><img src="https://secure.gravatar.com/avatar/0335030e1ae576653b4d6a4c811f2276?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RCH&#39;s gravatar image" /><p><span>RCH</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RCH has no accepted answers">0%</span></p></div></div><div id="comments-container-3934" class="comments-container"></div><div id="comment-tools-3934" class="comment-tools"></div><div class="clear"></div><div id="comment-3934-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

