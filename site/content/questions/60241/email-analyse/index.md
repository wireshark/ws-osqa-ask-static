+++
type = "question"
title = "Email analyse"
description = '''Hi, I am trying to retive messages and emails on a wireshark capture, and have come across bay179.mail.live.com and not sure what this means, however if I filter tcp contains bay179 I get these packets.  When I then run the filter dns contains bay179 I get packet looking like this:  Is there anyway ...'''
date = "2017-03-21T14:35:00Z"
lastmod = "2017-03-22T11:19:00Z"
weight = 60241
keywords = [ "wireshark" ]
aliases = [ "/questions/60241" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Email analyse](/questions/60241/email-analyse)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60241-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60241-score" class="post-score" title="current number of votes">0</div><span id="post-60241-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to retive messages and emails on a wireshark capture, and have come across bay179.mail.live.com and not sure what this means, however if I filter tcp contains bay179 I get these packets. <img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2017-03-21_at_21.31.44.png" alt="alt text" /></p><p>When I then run the filter dns contains bay179 I get packet looking like this:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2017-03-21_at_21.34.05.png" alt="alt text" /></p><p>Is there anyway of getting any information off this?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '17, 14:35</strong></p><img src="https://secure.gravatar.com/avatar/9331fbc9e5ad1aec6c88229c257e2565?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emma123&#39;s gravatar image" /><p><span>emma123</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emma123 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-60241" class="comments-container"></div><div id="comment-tools-60241" class="comment-tools"></div><div class="clear"></div><div id="comment-60241-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60267"></span>

<div id="answer-container-60267" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60267-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60267-score" class="post-score" title="current number of votes">0</div><span id="post-60267-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What you see there is encrypted web traffic, so I guess you accessed your email with a web browser. There's not much to see here unless you have the encryption keys, in which case you could decrypt the conversation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Mar '17, 11:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></img></div></div><div id="comments-container-60267" class="comments-container"></div><div id="comment-tools-60267" class="comment-tools"></div><div class="clear"></div><div id="comment-60267-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

