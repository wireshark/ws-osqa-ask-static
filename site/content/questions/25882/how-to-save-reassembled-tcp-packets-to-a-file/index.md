+++
type = "question"
title = "How to save reassembled TCP packets to a file ?"
description = '''Right-clicking on the bottom pane gave me two choices in the contextual menu : hex view and bits view. I wish to save the packets to a file how can I do that ?  thanks :)'''
date = "2013-10-10T07:36:00Z"
lastmod = "2013-10-10T07:45:00Z"
weight = 25882
keywords = [ "reassembled", "save" ]
aliases = [ "/questions/25882" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to save reassembled TCP packets to a file ?](/questions/25882/how-to-save-reassembled-tcp-packets-to-a-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25882-score" class="post-score" title="current number of votes">0</div><span id="post-25882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Right-clicking on the bottom pane gave me two choices in the contextual menu : hex view and bits view. I wish to save the packets to a file how can I do that ?</p><p>thanks :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reassembled" rel="tag" title="see questions tagged &#39;reassembled&#39;">reassembled</span> <span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '13, 07:36</strong></p><img src="https://secure.gravatar.com/avatar/94eb051be96f49a1665b097330fd97bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ychaouche&#39;s gravatar image" /><p><span>ychaouche</span><br />
<span class="score" title="31 reputation points">31</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ychaouche has one accepted answer">100%</span></p></div></div><div id="comments-container-25882" class="comments-container"></div><div id="comment-tools-25882" class="comment-tools"></div><div class="clear"></div><div id="comment-25882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25883"></span>

<div id="answer-container-25883" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25883-score" class="post-score" title="current number of votes">0</div><span id="post-25883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ychaouche has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Oh, simple :) so just go on second pane, select 5th element on the stack (Reassemebled TCP Segments), right-click on that item then Export select Packet bytes.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '13, 07:45</strong></p><img src="https://secure.gravatar.com/avatar/94eb051be96f49a1665b097330fd97bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ychaouche&#39;s gravatar image" /><p><span>ychaouche</span><br />
<span class="score" title="31 reputation points">31</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ychaouche has one accepted answer">100%</span></p></div></div><div id="comments-container-25883" class="comments-container"></div><div id="comment-tools-25883" class="comment-tools"></div><div class="clear"></div><div id="comment-25883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

