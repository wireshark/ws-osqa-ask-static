+++
type = "question"
title = "Playing codec G722 in Wireshark for Mac."
description = '''Hello guys. I was wondering if there is a way to play a G722 RTP stream on Wireshark for Mac. I can play G722 RTP streams in Wireshark for Windows but I am unable to do it on MacOS. I am running the last version 2.4. Any help would be appreciated.'''
date = "2017-08-01T09:10:00Z"
lastmod = "2017-08-01T11:10:00Z"
weight = 63305
keywords = [ "g722", "macosx", "apple", "wireshark" ]
aliases = [ "/questions/63305" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Playing codec G722 in Wireshark for Mac.](/questions/63305/playing-codec-g722-in-wireshark-for-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63305-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63305-score" class="post-score" title="current number of votes">0</div><span id="post-63305-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello guys.</p><p>I was wondering if there is a way to play a G722 RTP stream on Wireshark for Mac. I can play G722 RTP streams in Wireshark for Windows but I am unable to do it on MacOS. I am running the last version 2.4.</p><p>Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-g722" rel="tag" title="see questions tagged &#39;g722&#39;">g722</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-apple" rel="tag" title="see questions tagged &#39;apple&#39;">apple</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '17, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/72f379e00e44e4769d43f612a8e2e310?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kryptonian89&#39;s gravatar image" /><p><span>Kryptonian89</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kryptonian89 has no accepted answers">0%</span></p></div></div><div id="comments-container-63305" class="comments-container"></div><div id="comment-tools-63305" class="comment-tools"></div><div class="clear"></div><div id="comment-63305-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63309"></span>

<div id="answer-container-63309" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63309-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63309-score" class="post-score" title="current number of votes">0</div><span id="post-63309-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Presumably the macOS version is compiled without SpanDSP support (the library used to perfrom G.722 decoding). Let's see if we can add it to the macOS builder for 2.4.1.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '17, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-63309" class="comments-container"></div><div id="comment-tools-63309" class="comment-tools"></div><div class="clear"></div><div id="comment-63309-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

