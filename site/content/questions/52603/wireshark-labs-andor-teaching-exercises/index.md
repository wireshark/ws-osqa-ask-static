+++
type = "question"
title = "Wireshark Labs and/or Teaching Exercises"
description = '''Hello all!  I am a High School computer science teacher, and I am currently teaching a &quot;Intro to Cyber Security&quot; course. It&#x27;s mainly a survey course, and forces on everything from passwords, to ethical hacking, to pen testing, to network analysis.  I want my students to have a beginner&#x27;s understandi...'''
date = "2016-05-15T21:55:00Z"
lastmod = "2016-05-17T01:20:00Z"
weight = 52603
keywords = [ "school", "class", "lab", "wireshark" ]
aliases = [ "/questions/52603" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark Labs and/or Teaching Exercises](/questions/52603/wireshark-labs-andor-teaching-exercises)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52603-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52603-score" class="post-score" title="current number of votes">0</div><span id="post-52603-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all!</p><p>I am a High School computer science teacher, and I am currently teaching a "Intro to Cyber Security" course. It's mainly a survey course, and forces on everything from passwords, to ethical hacking, to pen testing, to network analysis.</p><p>I want my students to have a beginner's understanding of Wireshark. There are plenty of Youtube tutorials that show how to use the software, but I want my students to move through some simulated labs, and those are hard to make. I have done my best looking up what's online, but most of the time I will find PDFs of college teacher's assignments referencing .pcap files that are elsewhere. Does anyone on here have any good Wireshark labs that they have ever worked through? Are there any good sites that have sort of "lab" exercises where you download a .pcap or .dmp file? Do you have any oldl lessons or labs from classes you have taken?</p><p>It would mean a ton to both me and my class!</p><p>Best, Tobin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-school" rel="tag" title="see questions tagged &#39;school&#39;">school</span> <span class="post-tag tag-link-class" rel="tag" title="see questions tagged &#39;class&#39;">class</span> <span class="post-tag tag-link-lab" rel="tag" title="see questions tagged &#39;lab&#39;">lab</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '16, 21:55</strong></p><img src="https://secure.gravatar.com/avatar/f6e24e79c98bd55beee6deccff0ca129?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TobinShields&#39;s gravatar image" /><p><span>TobinShields</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TobinShields has no accepted answers">0%</span></p></div></div><div id="comments-container-52603" class="comments-container"></div><div id="comment-tools-52603" class="comment-tools"></div><div class="clear"></div><div id="comment-52603-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="52608"></span>

<div id="answer-container-52608" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52608-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52608-score" class="post-score" title="current number of votes">3</div><span id="post-52608-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="TobinShields has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In can recommend the <a href="http://www.malware-traffic-analysis.net/">Malware-Traffic-Analysis</a> website. There are a lot of training exercises with step-through solutions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 May '16, 00:56</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-52608" class="comments-container"><span id="52622"></span><div id="comment-52622" class="comment"><div id="post-52622-score" class="comment-score"></div><div class="comment-text"><p>Wow--this is EXACTLY what I was hoping to find. In fact, this is an utter gold mine for both my own personal development, and something that I could run through with my student. I havn't run through these yet, but this is incredibly helpful. Bookmarking now...</p><p>Thanks a ton!</p></div><div id="comment-52622-info" class="comment-info"><span class="comment-age">(16 May '16, 07:35)</span> <span class="comment-user userinfo">TobinShields</span></div></div><span id="52659"></span><div id="comment-52659" class="comment"><div id="post-52659-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-52659-info" class="comment-info"><span class="comment-age">(17 May '16, 01:20)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-52608" class="comment-tools"></div><div class="clear"></div><div id="comment-52608-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="52605"></span>

<div id="answer-container-52605" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52605-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52605-score" class="post-score" title="current number of votes">0</div><span id="post-52605-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I know the book <a href="https://www.packtpub.com/networking-and-servers/wireshark-network-security">Wireshark Network Security</a> contains some documented examples, referencing PCAP hives. This could be a starting point to set something up. Full disclosure: I've been involved in reviewing this book.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '16, 22:55</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52605" class="comments-container"></div><div id="comment-tools-52605" class="comment-tools"></div><div class="clear"></div><div id="comment-52605-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

