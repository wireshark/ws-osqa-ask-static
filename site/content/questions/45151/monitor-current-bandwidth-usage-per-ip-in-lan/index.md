+++
type = "question"
title = "Monitor current bandwidth usage per IP in lan"
description = '''Hi i want to monitor current or live or throughput data usage per IP for my Lan so that i can monitor accurately for every device in network Is that possible =if possible can anyone assist me on that. Thanks'''
date = "2015-08-17T01:41:00Z"
lastmod = "2015-08-17T23:18:00Z"
weight = 45151
keywords = [ "traffic-analysis", "networking", "bandwidthutilization", "netflow" ]
aliases = [ "/questions/45151" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor current bandwidth usage per IP in lan](/questions/45151/monitor-current-bandwidth-usage-per-ip-in-lan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45151-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45151-score" class="post-score" title="current number of votes">0</div><span id="post-45151-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i want to monitor current or live or throughput data usage per IP for my Lan so that i can monitor accurately for every device in network</p><p>Is that possible =if possible can anyone assist me on that.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-networking" rel="tag" title="see questions tagged &#39;networking&#39;">networking</span> <span class="post-tag tag-link-bandwidthutilization" rel="tag" title="see questions tagged &#39;bandwidthutilization&#39;">bandwidthutilization</span> <span class="post-tag tag-link-netflow" rel="tag" title="see questions tagged &#39;netflow&#39;">netflow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Aug '15, 01:41</strong></p><img src="https://secure.gravatar.com/avatar/8a44916b9059276d12ebd31fedb38fe5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akbar&#39;s gravatar image" /><p><span>akbar</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akbar has no accepted answers">0%</span></p></div></div><div id="comments-container-45151" class="comments-container"></div><div id="comment-tools-45151" class="comment-tools"></div><div class="clear"></div><div id="comment-45151-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45154"></span>

<div id="answer-container-45154" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45154-score" class="post-score" title="current number of votes">2</div><span id="post-45154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Choose the right tool for the job. Use Wireshark to drill down into the details of a packet, use switch monitoring tools to get network statistics. <a href="http://www.ntop.org/">ntop-ng</a> could so the job as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Aug '15, 04:49</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-45154" class="comments-container"><span id="45166"></span><div id="comment-45166" class="comment"><div id="post-45166-score" class="comment-score"></div><div class="comment-text"><p>i don't have manageable switch working with unmanageable switch also have a router which support SNMP i,e RV082 cisco can you assist me to work with these things.if yes elaborate a bit more on this.</p><p>thanks</p></div><div id="comment-45166-info" class="comment-info"><span class="comment-age">(17 Aug '15, 12:17)</span> <span class="comment-user userinfo">akbar</span></div></div><span id="45167"></span><div id="comment-45167" class="comment"><div id="post-45167-score" class="comment-score"></div><div class="comment-text"><p>Sorry, that's not a Wireshark question.</p></div><div id="comment-45167-info" class="comment-info"><span class="comment-age">(17 Aug '15, 12:59)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="45185"></span><div id="comment-45185" class="comment"><div id="post-45185-score" class="comment-score"></div><div class="comment-text"><p>atleast can you mailed me <span class="__cf_email__" data-cfemail="6e0f050c0f1c1b0a0a0700575d5c5c2e09030f0702400d0103">[email protected]</span></p></div><div id="comment-45185-info" class="comment-info"><span class="comment-age">(17 Aug '15, 23:18)</span> <span class="comment-user userinfo">akbar</span></div></div></div><div id="comment-tools-45154" class="comment-tools"></div><div class="clear"></div><div id="comment-45154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

