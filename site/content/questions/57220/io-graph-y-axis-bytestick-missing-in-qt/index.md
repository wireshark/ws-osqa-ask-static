+++
type = "question"
title = "IO Graph Y Axis Bytes/Tick Missing in QT"
description = '''The IO Graph in v2.2.1 in the QT version has almost no options for tweaking. Where did the Y Units Dropbox go? (For selecting Bytes/Tick)'''
date = "2016-11-09T11:00:00Z"
lastmod = "2016-11-09T12:12:00Z"
weight = 57220
keywords = [ "graph", "qt" ]
aliases = [ "/questions/57220" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [IO Graph Y Axis Bytes/Tick Missing in QT](/questions/57220/io-graph-y-axis-bytestick-missing-in-qt)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57220-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57220-score" class="post-score" title="current number of votes">0</div><span id="post-57220-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The IO Graph in v2.2.1 in the QT version has almost no options for tweaking. Where did the Y Units Dropbox go? (For selecting Bytes/Tick)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-qt" rel="tag" title="see questions tagged &#39;qt&#39;">qt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Nov '16, 11:00</strong></p><img src="https://secure.gravatar.com/avatar/2b5155ac9a5c6f55ef9f18b22817417b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gregv&#39;s gravatar image" /><p><span>gregv</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gregv has no accepted answers">0%</span></p></div></div><div id="comments-container-57220" class="comments-container"></div><div id="comment-tools-57220" class="comment-tools"></div><div class="clear"></div><div id="comment-57220-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57221"></span>

<div id="answer-container-57221" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57221-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57221-score" class="post-score" title="current number of votes">2</div><span id="post-57221-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="gregv has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It is still there, and for me it feels more confortable to use. Because, you can combine whatever you want at the Y-Axis now:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/2016-11-09_20-50-02.png" alt="alt text" /></p><p>Further info can be found here: <a href="https://crnetpackets.com/2015/06/09/the-great-new-features-of-wireshark-1-99/">https://crnetpackets.com/2015/06/09/the-great-new-features-of-wireshark-1-99/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Nov '16, 11:52</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Nov '16, 11:56</strong> </span></p></div></div><div id="comments-container-57221" class="comments-container"><span id="57222"></span><div id="comment-57222" class="comment"><div id="post-57222-score" class="comment-score"></div><div class="comment-text"><p>awesome, thanks...it's a little hard to find since you have to double click to change it.</p></div><div id="comment-57222-info" class="comment-info"><span class="comment-age">(09 Nov '16, 12:12)</span> <span class="comment-user userinfo">gregv</span></div></div></div><div id="comment-tools-57221" class="comment-tools"></div><div class="clear"></div><div id="comment-57221-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

