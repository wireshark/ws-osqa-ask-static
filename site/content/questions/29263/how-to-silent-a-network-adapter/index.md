+++
type = "question"
title = "How to silent a network adapter?"
description = '''Hello all, I am using Wireshark on WIN7-64bit. The goal is to spy an Ethernet Powerlink network from my computer. I would then like my network adapter to spy silently. (Powerlink is time triggered and does not like packets transmitted asynchronously by my computer). I am not a Win7 expert and do not...'''
date = "2014-01-29T00:34:00Z"
lastmod = "2014-01-29T00:50:00Z"
weight = 29263
keywords = [ "nic", "silent", "powerlink", "win7" ]
aliases = [ "/questions/29263" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to silent a network adapter?](/questions/29263/how-to-silent-a-network-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29263-score" class="post-score" title="current number of votes">0</div><span id="post-29263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all,</p><p>I am using Wireshark on WIN7-64bit. The goal is to spy an Ethernet Powerlink network from my computer. I would then like <strong>my network adapter to spy silently</strong>. (Powerlink is time triggered and does not like packets transmitted asynchronously by my computer).</p><p>I am not a Win7 expert and do not know how to have my network adapter "listen only".</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nic" rel="tag" title="see questions tagged &#39;nic&#39;">nic</span> <span class="post-tag tag-link-silent" rel="tag" title="see questions tagged &#39;silent&#39;">silent</span> <span class="post-tag tag-link-powerlink" rel="tag" title="see questions tagged &#39;powerlink&#39;">powerlink</span> <span class="post-tag tag-link-win7" rel="tag" title="see questions tagged &#39;win7&#39;">win7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jan '14, 00:34</strong></p><img src="https://secure.gravatar.com/avatar/8656875424bd8993d19888c1e8bb0610?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aleroyer&#39;s gravatar image" /><p><span>aleroyer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aleroyer has no accepted answers">0%</span></p></div></div><div id="comments-container-29263" class="comments-container"></div><div id="comment-tools-29263" class="comment-tools"></div><div class="clear"></div><div id="comment-29263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29264"></span>

<div id="answer-container-29264" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29264-score" class="post-score" title="current number of votes">0</div><span id="post-29264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to the adapter settings in Windows and remove <strong>all</strong> checkboxes, especially those for IPv4 and IPv6.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jan '14, 00:50</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-29264" class="comments-container"></div><div id="comment-tools-29264" class="comment-tools"></div><div class="clear"></div><div id="comment-29264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

