+++
type = "question"
title = "Help diagnosing network problem? Seeing a lot of RST"
description = '''I seem to be having a lot of issues with the network today and I&#x27;m pretty new to this and to wireshark. Can anyone give me a little help? I am seeing a lot of connect resets (RST) and the network seems really slow and my connection to the firewall disconnects and reconnects like every min. I just ca...'''
date = "2015-12-07T10:52:00Z"
lastmod = "2015-12-07T12:20:00Z"
weight = 48333
keywords = [ "connections" ]
aliases = [ "/questions/48333" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help diagnosing network problem? Seeing a lot of RST](/questions/48333/help-diagnosing-network-problem-seeing-a-lot-of-rst)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48333-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48333-score" class="post-score" title="current number of votes">0</div><span id="post-48333-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I seem to be having a lot of issues with the network today and I'm pretty new to this and to wireshark. Can anyone give me a little help? I am seeing a lot of connect resets (RST) and the network seems really slow and my connection to the firewall disconnects and reconnects like every min. I just can't figure out whats going on. What exactly should i be looking for?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connections" rel="tag" title="see questions tagged &#39;connections&#39;">connections</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '15, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/91882b009050e665f425043c1f7f33ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ThompsonAdmin&#39;s gravatar image" /><p><span>ThompsonAdmin</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ThompsonAdmin has no accepted answers">0%</span></p></div></div><div id="comments-container-48333" class="comments-container"><span id="48335"></span><div id="comment-48335" class="comment"><div id="post-48335-score" class="comment-score"></div><div class="comment-text"><p>Could you shar e us a trace on a public accessible place like dropbox or <a href="http://www.cloudshark.org">http://www.cloudshark.org</a></p></div><div id="comment-48335-info" class="comment-info"><span class="comment-age">(07 Dec '15, 12:12)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="48336"></span><div id="comment-48336" class="comment"><div id="post-48336-score" class="comment-score"></div><div class="comment-text"><p><a href="https://drive.google.com/file/d/0B3aXFCKzXbMZdm5wVzNLVkQ1V0U/view?usp=sharing">https://drive.google.com/file/d/0B3aXFCKzXbMZdm5wVzNLVkQ1V0U/view?usp=sharing</a></p></div><div id="comment-48336-info" class="comment-info"><span class="comment-age">(07 Dec '15, 12:20)</span> <span class="comment-user userinfo">ThompsonAdmin</span></div></div></div><div id="comment-tools-48333" class="comment-tools"></div><div class="clear"></div><div id="comment-48333-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

