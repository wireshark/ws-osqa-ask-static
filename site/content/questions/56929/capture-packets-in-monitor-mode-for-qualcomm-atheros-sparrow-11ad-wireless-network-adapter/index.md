+++
type = "question"
title = "Capture Packets in Monitor mode for Qualcomm Atheros Sparrow 11ad Wireless Network Adapter"
description = '''I am trying to run an IEEE 802.11ad/WiGig based interface (Qualcomm Atheros Sparrow 11ad Wireless Network Adapter) in monitor mode in Ubuntu 16.04.1. The driver is wil6210 which uses firmware wil6210.fw and wil6210.brd. I am able to set the interface into monitor mode and verify that using iwconfig ...'''
date = "2016-11-02T10:23:00Z"
lastmod = "2016-11-02T10:23:00Z"
weight = 56929
keywords = [ "wifi", "driver", "monitor", "ubuntu" ]
aliases = [ "/questions/56929" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Packets in Monitor mode for Qualcomm Atheros Sparrow 11ad Wireless Network Adapter](/questions/56929/capture-packets-in-monitor-mode-for-qualcomm-atheros-sparrow-11ad-wireless-network-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56929-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56929-score" class="post-score" title="current number of votes">0</div><span id="post-56929-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to run an IEEE 802.11ad/WiGig based interface (Qualcomm Atheros Sparrow 11ad Wireless Network Adapter) in monitor mode in Ubuntu 16.04.1. The driver is wil6210 which uses firmware wil6210.fw and wil6210.brd. I am able to set the interface into monitor mode and verify that using iwconfig command. However, I am not able to capture any control or management packets. I would like to know if someone has experienced with this driver and managed to capture 802.11ad frames using Wireshark. I followed the same steps mentioned in <a href="https://wireless.wiki.kernel.org/en/users/drivers/wil6210">wil6210 driver Website</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-driver" rel="tag" title="see questions tagged &#39;driver&#39;">driver</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '16, 10:23</strong></p><img src="https://secure.gravatar.com/avatar/566cfe38b17a31f0dc825c86538cf3d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hany%20Assasa&#39;s gravatar image" /><p><span>Hany Assasa</span><br />
<span class="score" title="21 reputation points">21</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hany Assasa has no accepted answers">0%</span></p></div></div><div id="comments-container-56929" class="comments-container"></div><div id="comment-tools-56929" class="comment-tools"></div><div class="clear"></div><div id="comment-56929-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

