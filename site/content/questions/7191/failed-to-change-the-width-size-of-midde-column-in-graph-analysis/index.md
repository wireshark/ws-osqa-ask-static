+++
type = "question"
title = "Failed to change the width size of midde column in Graph Analysis."
description = '''Can someone tell me how I can fix it or which version will address this issue? I&#x27;m now using v1.6.2.'''
date = "2011-11-02T00:01:00Z"
lastmod = "2011-11-02T07:12:00Z"
weight = 7191
keywords = [ "graph", "analysis" ]
aliases = [ "/questions/7191" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Failed to change the width size of midde column in Graph Analysis.](/questions/7191/failed-to-change-the-width-size-of-midde-column-in-graph-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7191-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7191-score" class="post-score" title="current number of votes">0</div><span id="post-7191-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone tell me how I can fix it or which version will address this issue? I'm now using v1.6.2.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '11, 00:01</strong></p><img src="https://secure.gravatar.com/avatar/828a00f4bace3170c9a08ff292cb610b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ccie10953&#39;s gravatar image" /><p><span>ccie10953</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ccie10953 has no accepted answers">0%</span></p></div></div><div id="comments-container-7191" class="comments-container"></div><div id="comment-tools-7191" class="comment-tools"></div><div class="clear"></div><div id="comment-7191-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7198"></span>

<div id="answer-container-7198" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7198-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7198-score" class="post-score" title="current number of votes">1</div><span id="post-7198-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>By editing the code in graph_analysis.c to make it work in a nice manner :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Nov '11, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-7198" class="comments-container"></div><div id="comment-tools-7198" class="comment-tools"></div><div class="clear"></div><div id="comment-7198-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

