+++
type = "question"
title = "Interpret packets - Profinet dissector"
description = '''Hi, I would like to know where about I can start to find some documentation about how wireshark interprets and structures the telgrams, concretely, how can I use a dissector in my code. thanks '''
date = "2015-10-23T09:24:00Z"
lastmod = "2015-10-23T09:37:00Z"
weight = 46883
keywords = [ "cpp", "code", "dissector", "profinet" ]
aliases = [ "/questions/46883" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Interpret packets - Profinet dissector](/questions/46883/interpret-packets-profinet-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46883-score" class="post-score" title="current number of votes">0</div><span id="post-46883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to know where about I can start to find some documentation about how wireshark interprets and structures the telgrams, concretely, how can I use a dissector in my code.</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cpp" rel="tag" title="see questions tagged &#39;cpp&#39;">cpp</span> <span class="post-tag tag-link-code" rel="tag" title="see questions tagged &#39;code&#39;">code</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-profinet" rel="tag" title="see questions tagged &#39;profinet&#39;">profinet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '15, 09:24</strong></p><img src="https://secure.gravatar.com/avatar/8a0a354cdee7a422caea198b23d7111e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aramos&#39;s gravatar image" /><p><span>aramos</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aramos has no accepted answers">0%</span></p></div></div><div id="comments-container-46883" class="comments-container"><span id="46884"></span><div id="comment-46884" class="comment"><div id="post-46884-score" class="comment-score"></div><div class="comment-text"><blockquote><p>how can I use a dissector in my code.</p></blockquote><p>what do you want to achieve?</p></div><div id="comment-46884-info" class="comment-info"><span class="comment-age">(23 Oct '15, 09:37)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-46883" class="comment-tools"></div><div class="clear"></div><div id="comment-46883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

