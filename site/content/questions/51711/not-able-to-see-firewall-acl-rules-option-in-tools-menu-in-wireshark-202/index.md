+++
type = "question"
title = "[closed] Not able to see &quot;firewall ACL Rules&quot; option in tools menu in wireshark 2.0.2 ?"
description = '''Hello Wireshark team , I am not able to see &quot;Firewall ACL Rules&quot; option in wireshark version 2.0.2. (As mentioned in below image) Is it intended or missing ??!!! If I go to legacy one version which is in parallel installed with 2.0.2 installer then it showing this option.  '''
date = "2016-04-15T20:32:00Z"
lastmod = "2016-04-16T05:19:00Z"
weight = 51711
keywords = [ "firewall", "wireshark", "acl" ]
aliases = [ "/questions/51711" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Not able to see "firewall ACL Rules" option in tools menu in wireshark 2.0.2 ?](/questions/51711/not-able-to-see-firewall-acl-rules-option-in-tools-menu-in-wireshark-202)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51711-score" class="post-score" title="current number of votes">0</div><span id="post-51711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Wireshark team ,</p><p>I am not able to see "Firewall ACL Rules" option in wireshark version 2.0.2. (As mentioned in below image)</p><p>Is it intended or missing ??!!!</p><p>If I go to legacy one version which is in parallel installed with 2.0.2 installer then it showing this option.<br />
</p><p><img src="https://osqa-ask.wireshark.org/upfiles/wireshark2_xgDWmC3.PNG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-acl" rel="tag" title="see questions tagged &#39;acl&#39;">acl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '16, 20:32</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span> </br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>16 Apr '16, 05:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51711" class="comments-container"><span id="51717"></span><div id="comment-51717" class="comment"><div id="post-51717-score" class="comment-score"></div><div class="comment-text"><p>Asked quite a few times before, and answered notably yesterday.</p><p>See this <a href="https://ask.wireshark.org/questions/51701/windows-10-and-wireshark-202-firewall-rules-acl">question</a></p></div><div id="comment-51717-info" class="comment-info"><span class="comment-age">(16 Apr '16, 05:19)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-51711" class="comment-tools"></div><div class="clear"></div><div id="comment-51711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 16 Apr '16, 05:19

</div>

</div>

</div>

