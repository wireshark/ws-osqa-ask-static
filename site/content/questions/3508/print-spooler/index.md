+++
type = "question"
title = "Print Spooler"
description = '''I installed Wireshark (and I think it installed WinPCap too). And ever since I cant print.. Looking at my services the Print Spooler costantly stops - restarting it it stops in out 10 seconds. I run Windows 7 SP1 (32Bit) does anyone have any ideas. I am getting the following in the event viewer Faul...'''
date = "2011-04-15T04:31:00Z"
lastmod = "2011-04-16T08:33:00Z"
weight = 3508
keywords = [ "print", "spooler", "wireshark" ]
aliases = [ "/questions/3508" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Print Spooler](/questions/3508/print-spooler)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3508-score" class="post-score" title="current number of votes">0</div><span id="post-3508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I installed Wireshark (and I think it installed WinPCap too).</p><p>And ever since I cant print.. Looking at my services the Print Spooler costantly stops - restarting it it stops in out 10 seconds.</p><p>I run Windows 7 SP1 (32Bit) does anyone have any ideas.</p><p>I am getting the following in the event viewer</p><p>Faulting application name: spoolsv.exe, version: 6.1.7601.17514, time stamp: 0x4ce7aa85 Faulting module name: hpzpnp.dll, version: 61.63.461.42, time stamp: 0x45d1d0e4 Exception code: 0xc0000005 Fault offset: 0x00004f0a Faulting process id: 0x15d8 Faulting application start time: 0x01cbfb5fe2ecf6b6 Faulting application path: C:WindowsSystem32spoolsv.exe Faulting module path: \?C:WindowsSystem32spooldriversw32x863hpzpnp.dll Report Id: 21369c56-6753-11e0-a277-001f29422dc3</p><p>As a workaround I removed all HP printers as I according to logs hpzpnp.dll is what is making the system crash? but this hasnt made any odds?</p><p>Any advice would be great.</p><p>Cheers M</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-print" rel="tag" title="see questions tagged &#39;print&#39;">print</span> <span class="post-tag tag-link-spooler" rel="tag" title="see questions tagged &#39;spooler&#39;">spooler</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '11, 04:31</strong></p><img src="https://secure.gravatar.com/avatar/045139be8382462d704a08c654fdf385?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bakinboys&#39;s gravatar image" /><p><span>bakinboys</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bakinboys has no accepted answers">0%</span></p></div></div><div id="comments-container-3508" class="comments-container"></div><div id="comment-tools-3508" class="comment-tools"></div><div class="clear"></div><div id="comment-3508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3532"></span>

<div id="answer-container-3532" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3532-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3532-score" class="post-score" title="current number of votes">0</div><span id="post-3532-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A quick google search indicates that others have had similar problems with no mention of Wireshark as the cause. If you still suspect Wireshark and/or WinPcap though, try uninstalling them. If you still experience the problem after that, then it rules them out at least. Have you tried contacting HP for support?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '11, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3532" class="comments-container"></div><div id="comment-tools-3532" class="comment-tools"></div><div class="clear"></div><div id="comment-3532-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

