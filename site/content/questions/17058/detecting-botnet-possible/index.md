+++
type = "question"
title = "detecting botnet possible?"
description = '''Hi, some scriptkiddy is bragging about having my PC in his botnet. I don&#x27;t really know what to do or even if I&#x27;m really in his botnet, so I&#x27;d like to check that point first. Is it possible to detect a botnet via wireshark?  If yes, how would I know I&#x27;m infected? Thanks in advance fellows...'''
date = "2012-12-19T07:31:00Z"
lastmod = "2012-12-20T12:46:00Z"
weight = 17058
keywords = [ "detect", "infection", "botnet" ]
aliases = [ "/questions/17058" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [detecting botnet possible?](/questions/17058/detecting-botnet-possible)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17058-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17058-score" class="post-score" title="current number of votes">0</div><span id="post-17058-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, some scriptkiddy is bragging about having my PC in his botnet. I don't really know what to do or even if I'm really in his botnet, so I'd like to check that point first.</p><p>Is it possible to detect a botnet via wireshark? If yes, how would I know I'm infected?</p><p>Thanks in advance fellows...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-detect" rel="tag" title="see questions tagged &#39;detect&#39;">detect</span> <span class="post-tag tag-link-infection" rel="tag" title="see questions tagged &#39;infection&#39;">infection</span> <span class="post-tag tag-link-botnet" rel="tag" title="see questions tagged &#39;botnet&#39;">botnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '12, 07:31</strong></p><img src="https://secure.gravatar.com/avatar/74c7db18641b0e7a903fc2b622aaf5dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="QAI&#39;s gravatar image" /><p><span>QAI</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="QAI has no accepted answers">0%</span></p></div></div><div id="comments-container-17058" class="comments-container"></div><div id="comment-tools-17058" class="comment-tools"></div><div class="clear"></div><div id="comment-17058-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17061"></span>

<div id="answer-container-17061" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17061-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17061-score" class="post-score" title="current number of votes">2</div><span id="post-17061-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="QAI has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wouldn't it be better to detect a bot infection with a malware scanner?</p><p>I suggest to use one of the boot cd images mentioned in the following article</p><blockquote><p><code>http://www.techmixer.com/free-bootable-antivirus-rescue-cds-download-list/</code><br />
</p></blockquote><p>If none of those tools finds anything, then</p><ul><li>you have no infection</li><li>or your script kiddie is a genius (99,99% are not!) and developed something that is yet unknown by the AV vendors. However, how clever would it be to brag about that botnet in public ?? ;-)</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Dec '12, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-17061" class="comments-container"><span id="17064"></span><div id="comment-17064" class="comment"><div id="post-17064-score" class="comment-score"></div><div class="comment-text"><p>Thank you, I will try some of these discs now :)</p><p>Well, this kiddy was bragging via PM in a Forum, also stating the "honour" of beeing on of his few zombies. It's an annoying kid, but since I have no knowledge about networking, I get a bit nervous about such things^^</p></div><div id="comment-17064-info" class="comment-info"><span class="comment-age">(19 Dec '12, 08:24)</span> <span class="comment-user userinfo">QAI</span></div></div><span id="17065"></span><div id="comment-17065" class="comment"><div id="post-17065-score" class="comment-score"></div><div class="comment-text"><blockquote><p>also stating the "honour" of beeing on of his few zombies</p></blockquote><p>first scan your PC. If you don't find anything, demand a proof ;-) Just in case: BACKUP, BACKUP, BACKUP !!!</p></div><div id="comment-17065-info" class="comment-info"><span class="comment-age">(19 Dec '12, 08:26)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="17073"></span><div id="comment-17073" class="comment"><div id="post-17073-score" class="comment-score"></div><div class="comment-text"><p>No virus found, now waiting for response to my proof demanding :)</p><p>Thank you Kurt!</p></div><div id="comment-17073-info" class="comment-info"><span class="comment-age">(19 Dec '12, 12:05)</span> <span class="comment-user userinfo">QAI</span></div></div><span id="17075"></span><div id="comment-17075" class="comment"><div id="post-17075-score" class="comment-score">1</div><div class="comment-text"><p>Just in case: Run dumpcap with a ringbuffer to capture the whole traffic from your machine.</p><p>Get the interface number</p><blockquote><p><code>dumpcap -D -M</code><br />
</p></blockquote><p>Run dumpcap</p><blockquote><p><code>dumpcap -ni xxx -w bot_traffic.pcap -b filesize 200000 -b files 40</code><br />
</p></blockquote><p>Replace <strong>xxx</strong> with the interface number of step one. This will generate a max. of 8 Gbyte data. Make sure there is enough free space on disk.</p><p>As soon as you think there is something suspicious going on, stop dumpcap and note the exact time (incl. seconds). Then you can open the capture files with Wireshark and look for possible botnet traffic (around the time you stopped dumpcap). Unfortunately I can't tell you what to look for, as different bots use different communication methods.</p><p>BTW: To figure out if something suspicious is going on, you can run a network monitor tool that shows all connections from your system to the internet.</p><blockquote><p><code>http://technet.microsoft.com/en-us/sysinternals/bb897437.aspx</code><br />
<code>http://www.nirsoft.net/utils/cports.html</code><br />
</p></blockquote><p><strong>Hint:</strong> If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions.</p></div><div id="comment-17075-info" class="comment-info"><span class="comment-age">(19 Dec '12, 12:26)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="17104"></span><div id="comment-17104" class="comment"><div id="post-17104-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot again, the scriptkiddy admitted he just wanted some attention. Network also shows nothing out of the usual, if I don't do something myself everything idles completely :)</p></div><div id="comment-17104-info" class="comment-info"><span class="comment-age">(20 Dec '12, 11:17)</span> <span class="comment-user userinfo">QAI</span></div></div><span id="17112"></span><div id="comment-17112" class="comment not_top_scorer"><div id="post-17112-score" class="comment-score"></div><div class="comment-text"><p>Apparently one of those 99.99% ;-)</p></div><div id="comment-17112-info" class="comment-info"><span class="comment-age">(20 Dec '12, 12:46)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-17061" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-17061-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

