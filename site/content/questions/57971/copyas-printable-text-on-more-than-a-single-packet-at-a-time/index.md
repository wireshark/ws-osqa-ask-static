+++
type = "question"
title = "Copy....as Printable Text on more than a single packet at a time"
description = '''I am working with SNMP packets. I know you can Right-click on a packet, Copy, ...as Printable Text, and then paste the data.  This is great. However when doing this for hundreds of packets, very tedious and gives plenty of opportunity for fat-fingering. Is there a way to perform the same operation a...'''
date = "2016-12-08T14:29:00Z"
lastmod = "2016-12-09T18:13:00Z"
weight = 57971
keywords = [ "printable", "text", "as", "snmp" ]
aliases = [ "/questions/57971" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Copy....as Printable Text on more than a single packet at a time](/questions/57971/copyas-printable-text-on-more-than-a-single-packet-at-a-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57971-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57971-score" class="post-score" title="current number of votes">0</div><span id="post-57971-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working with SNMP packets.</p><p>I know you can Right-click on a packet, Copy, ...as Printable Text, and then paste the data. This is great. However when doing this for hundreds of packets, very tedious and gives plenty of opportunity for fat-fingering. <strong>Is there a way to perform the same operation as a single packet, but on a large number of SNMP packets...without having to select and print the plain text from each individual packet?</strong></p><p>Btw, using File &gt; Export Packet Dissections, Plain Text gives you the OID numeric tree...not the Plain Text that you get when you go Right-Click on packet, Copy, as Printable Text (which shows the human readable text of the SNMP alert). Thanks.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-printable" rel="tag" title="see questions tagged &#39;printable&#39;">printable</span> <span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-as" rel="tag" title="see questions tagged &#39;as&#39;">as</span> <span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '16, 14:29</strong></p><img src="https://secure.gravatar.com/avatar/ed1276dd1840b5936f0e463eb18ea659?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lars&#39;s gravatar image" /><p><span>Lars</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lars has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-57971" class="comments-container"></div><div id="comment-tools-57971" class="comment-tools"></div><div class="clear"></div><div id="comment-57971-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57983"></span>

<div id="answer-container-57983" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57983-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57983-score" class="post-score" title="current number of votes">0</div><span id="post-57983-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Also doing File &gt; Print, only prints the packets in the numeric OID long form, not in the Printable Text way that I need. What I am looking to do is to export all of the captured packets at one time, but do it in the way it does when you: - Select a single packet - Right-click that packet - Selecting Copy - Select "...as Printable Text". This would be a huge time saver. I have poked around in Wireshark and I have not found an easy way to do this yet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '16, 18:13</strong></p><img src="https://secure.gravatar.com/avatar/ed1276dd1840b5936f0e463eb18ea659?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lars&#39;s gravatar image" /><p><span>Lars</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lars has no accepted answers">0%</span></p></div></div><div id="comments-container-57983" class="comments-container"></div><div id="comment-tools-57983" class="comment-tools"></div><div class="clear"></div><div id="comment-57983-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

