+++
type = "question"
title = "extract message line with tcpflow"
description = '''hello i want to do process on packet payload. and i want to work with payload data like string.i want to access all line of the all application layer data. i want to know how could extract message lines from raw packets? i want to have a file that show every protocol message in a line.  how can i do...'''
date = "2015-04-23T03:04:00Z"
lastmod = "2015-04-23T03:04:00Z"
weight = 41724
keywords = [ "payload" ]
aliases = [ "/questions/41724" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [extract message line with tcpflow](/questions/41724/extract-message-line-with-tcpflow)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41724-score" class="post-score" title="current number of votes">0</div><span id="post-41724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello i want to do process on packet payload. and i want to work with payload data like string.i want to access all line of the all application layer data. i want to know how could extract message lines from raw packets? i want to have a file that show every protocol message in a line. how can i do it? please help me. i am waiting for your suggestions thanks a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '15, 03:04</strong></p><img src="https://secure.gravatar.com/avatar/ffdb11952a5028d43a89614d8cad5983?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fateme&#39;s gravatar image" /><p><span>Fateme</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fateme has no accepted answers">0%</span></p></div></div><div id="comments-container-41724" class="comments-container"></div><div id="comment-tools-41724" class="comment-tools"></div><div class="clear"></div><div id="comment-41724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

