+++
type = "question"
title = "how to start wireshark from CLI to capture and decode SSL/TLS communication"
description = '''Hello, I&#x27;d like to know how to start wireshark from CLI (command line in windows7) to be able to capture and decode SSL/TLS communication. I know how to do it from wireshark GUI but do not know how to do it from CLI. Thnaks a lot for your help'''
date = "2014-06-18T00:29:00Z"
lastmod = "2014-11-14T15:24:00Z"
weight = 33917
keywords = [ "ssl" ]
aliases = [ "/questions/33917" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to start wireshark from CLI to capture and decode SSL/TLS communication](/questions/33917/how-to-start-wireshark-from-cli-to-capture-and-decode-ssltls-communication)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33917-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33917-score" class="post-score" title="current number of votes">0</div><span id="post-33917-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I'd like to know how to start wireshark from CLI (command line in windows7) to be able to capture and decode SSL/TLS communication. I know how to do it from wireshark GUI but do not know how to do it from CLI. Thnaks a lot for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '14, 00:29</strong></p><img src="https://secure.gravatar.com/avatar/042b4b2b5d6b84b7489ad56c8ea09699?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pokoutnik&#39;s gravatar image" /><p><span>pokoutnik</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pokoutnik has no accepted answers">0%</span></p></div></div><div id="comments-container-33917" class="comments-container"><span id="34294"></span><div id="comment-34294" class="comment"><div id="post-34294-score" class="comment-score"></div><div class="comment-text"><p>How would you do it from the GUI? You need some keys or other secrets to decrypt SSL traffic.</p></div><div id="comment-34294-info" class="comment-info"><span class="comment-age">(30 Jun '14, 10:30)</span> <span class="comment-user userinfo">Lekensteyn</span></div></div><span id="37873"></span><div id="comment-37873" class="comment"><div id="post-37873-score" class="comment-score"></div><div class="comment-text"><p>I also want this. Poking about in the GUI to assign the mapping between IP and private key info is a real pain in the ass.</p></div><div id="comment-37873-info" class="comment-info"><span class="comment-age">(14 Nov '14, 15:24)</span> <span class="comment-user userinfo">amgems</span></div></div></div><div id="comment-tools-33917" class="comment-tools"></div><div class="clear"></div><div id="comment-33917-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

