+++
type = "question"
title = "Promiscuous mode requirement for reading captured packet file?"
description = '''I&#x27;ve been given a packet capture file that ran under promiscuous mode to monitor all traffic on a particular router. When I opened this file on my Windows machine, it only showed local capture data. But when I opened this file on other Linux machine, it shows all of the monitored traffic, including ...'''
date = "2016-04-20T23:00:00Z"
lastmod = "2016-04-20T23:36:00Z"
weight = 51828
keywords = [ "promiscuous", "capture-file" ]
aliases = [ "/questions/51828" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Promiscuous mode requirement for reading captured packet file?](/questions/51828/promiscuous-mode-requirement-for-reading-captured-packet-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51828-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51828-score" class="post-score" title="current number of votes">0</div><span id="post-51828-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've been given a packet capture file that ran under promiscuous mode to monitor all traffic on a particular router. When I opened this file on my Windows machine, it only showed local capture data. But when I opened this file on other Linux machine, it shows all of the monitored traffic, including from other devices.</p><p>Does the machine you're opening a pre-captured file have to be setup as promiscuous mode in order to look at all of the traffic? Or is there some option that simply toggles this feature on?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-capture-file" rel="tag" title="see questions tagged &#39;capture-file&#39;">capture-file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '16, 23:00</strong></p><img src="https://secure.gravatar.com/avatar/dfb8180fcc1405e17753a9c540823390?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="capturebuddy&#39;s gravatar image" /><p><span>capturebuddy</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="capturebuddy has no accepted answers">0%</span></p></div></div><div id="comments-container-51828" class="comments-container"></div><div id="comment-tools-51828" class="comment-tools"></div><div class="clear"></div><div id="comment-51828-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51830"></span>

<div id="answer-container-51830" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51830-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51830-score" class="post-score" title="current number of votes">0</div><span id="post-51830-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Does the machine you're opening a pre-captured file have to be setup as promiscuous mode in order to look at all of the traffic?</p></blockquote><p>No. Promiscuous mode is a <em>capture</em> mode; it has <em>no</em> effect on an existing capture. (Seriously. If it appears that it makes a difference, something else is happening.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '16, 23:36</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-51830" class="comments-container"></div><div id="comment-tools-51830" class="comment-tools"></div><div class="clear"></div><div id="comment-51830-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

