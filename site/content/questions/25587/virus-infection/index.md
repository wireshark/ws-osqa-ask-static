+++
type = "question"
title = "Virus infection"
description = '''Dear Sir/Madam, is it possible to get the URL or source IP from which virus is spreaded in individual machine that is happened 3 days before, please respond. if you have any doubt call 8925347074 or deepak.j.be@gmail.com'''
date = "2013-10-03T07:09:00Z"
lastmod = "2013-10-03T07:28:00Z"
weight = 25587
keywords = [ "virus", "port", "infection" ]
aliases = [ "/questions/25587" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Virus infection](/questions/25587/virus-infection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25587-score" class="post-score" title="current number of votes">0</div><span id="post-25587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sir/Madam,</p><p>is it possible to get the URL or source IP from which virus is spreaded in individual machine that is happened 3 days before, please respond. if you have any doubt call 8925347074 or <span class="__cf_email__" data-cfemail="8febeaeaffeee4a1e5a1edeacfe8e2eee6e3a1ece0e2">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-virus" rel="tag" title="see questions tagged &#39;virus&#39;">virus</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-infection" rel="tag" title="see questions tagged &#39;infection&#39;">infection</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '13, 07:09</strong></p><img src="https://secure.gravatar.com/avatar/6caffae1a6579e228f9c1f830b1d5efa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deepak_learner&#39;s gravatar image" /><p><span>Deepak_learner</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deepak_learner has no accepted answers">0%</span></p></div></div><div id="comments-container-25587" class="comments-container"></div><div id="comment-tools-25587" class="comment-tools"></div><div class="clear"></div><div id="comment-25587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25589"></span>

<div id="answer-container-25589" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25589-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25589-score" class="post-score" title="current number of votes">0</div><span id="post-25589-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>if you have captured the network traffic at that time you might be able to do that. If you do not have those captures you can only hope to find what you want by leveraging a forensical investigation against the infected PC.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Oct '13, 07:28</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Oct '13, 07:28</strong> </span></p></div></div><div id="comments-container-25589" class="comments-container"></div><div id="comment-tools-25589" class="comment-tools"></div><div class="clear"></div><div id="comment-25589-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

