+++
type = "question"
title = "Testing Gateway"
description = '''I have just finished configuring the IP address on my workstation. My gateway address is 172.16.255.254. What command would I enter to test that the gateway is reachable?'''
date = "2014-09-12T23:03:00Z"
lastmod = "2014-09-12T23:05:00Z"
weight = 36292
keywords = [ "gateway" ]
aliases = [ "/questions/36292" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Testing Gateway](/questions/36292/testing-gateway)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36292-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36292-score" class="post-score" title="current number of votes">0</div><span id="post-36292-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have just finished configuring the IP address on my workstation. My gateway address is 172.16.255.254. What command would I enter to test that the gateway is reachable?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gateway" rel="tag" title="see questions tagged &#39;gateway&#39;">gateway</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '14, 23:03</strong></p><img src="https://secure.gravatar.com/avatar/17cd477de8494aa3065f13ae54e5a737?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Barretstrife&#39;s gravatar image" /><p><span>Barretstrife</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Barretstrife has no accepted answers">0%</span></p></div></div><div id="comments-container-36292" class="comments-container"></div><div id="comment-tools-36292" class="comment-tools"></div><div class="clear"></div><div id="comment-36292-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36293"></span>

<div id="answer-container-36293" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36293-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36293-score" class="post-score" title="current number of votes">0</div><span id="post-36293-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On a command line, run "ping 172.16.255.254". You should see answers coming back when the gateway is up and reachable.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Sep '14, 23:05</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-36293" class="comments-container"></div><div id="comment-tools-36293" class="comment-tools"></div><div class="clear"></div><div id="comment-36293-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

