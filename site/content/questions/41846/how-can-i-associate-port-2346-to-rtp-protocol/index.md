+++
type = "question"
title = "How can I associate port 2346 to RTP protocol..?"
description = '''I know I can use &quot;Decode as&quot; for my packets, but I have lots of them to handle, thus I want to associate sending port 2346 to RTP protocol to make Wireshark to parse packets automatically. I checked all menus, but I cannot figure out how to make this configuration..., anybody knows how? Thanks.'''
date = "2015-04-25T22:07:00Z"
lastmod = "2015-04-25T23:23:00Z"
weight = 41846
keywords = [ "decode" ]
aliases = [ "/questions/41846" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How can I associate port 2346 to RTP protocol..?](/questions/41846/how-can-i-associate-port-2346-to-rtp-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41846-score" class="post-score" title="current number of votes">0</div><span id="post-41846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know I can use "Decode as" for my packets, but I have lots of them to handle, thus I want to associate sending port 2346 to RTP protocol to make Wireshark to parse packets automatically.</p><p>I checked all menus, but I cannot figure out how to make this configuration..., anybody knows how?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '15, 22:07</strong></p><img src="https://secure.gravatar.com/avatar/d10e76912ae0a0d745f3451d29395d86?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DiveDave&#39;s gravatar image" /><p><span>DiveDave</span><br />
<span class="score" title="21 reputation points">21</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DiveDave has one accepted answer">100%</span></p></div></div><div id="comments-container-41846" class="comments-container"></div><div id="comment-tools-41846" class="comment-tools"></div><div class="clear"></div><div id="comment-41846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41847"></span>

<div id="answer-container-41847" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41847-score" class="post-score" title="current number of votes">3</div><span id="post-41847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="DiveDave has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>After using "Decode as" to tell Wireshark to decode traffic over port 2346 as RTP, go to Analyze &gt; User Specified Decodes and click "Save."</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Apr '15, 22:43</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-41847" class="comments-container"><span id="41848"></span><div id="comment-41848" class="comment"><div id="post-41848-score" class="comment-score"></div><div class="comment-text"><p>COOOOOOOOOOOOOOOOOOOOOOOL~!!!!!</p><p>Thanks a lot~!!!</p></div><div id="comment-41848-info" class="comment-info"><span class="comment-age">(25 Apr '15, 23:23)</span> <span class="comment-user userinfo">DiveDave</span></div></div></div><div id="comment-tools-41847" class="comment-tools"></div><div class="clear"></div><div id="comment-41847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

