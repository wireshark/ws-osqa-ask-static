+++
type = "question"
title = "Need help getting started with Wireshark."
description = '''Is there anyone who can help me? I&#x27;m a software student in Iran and I want study the Wireshark source code. I really enjoy this software but I don&#x27;t know where to start? I need your guidance. Please help me get started. Thanks.'''
date = "2013-09-01T11:32:00Z"
lastmod = "2013-09-01T15:38:00Z"
weight = 24269
keywords = [ "newbie", "novice", "beginner", "helpme" ]
aliases = [ "/questions/24269" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Need help getting started with Wireshark.](/questions/24269/need-help-getting-started-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24269-score" class="post-score" title="current number of votes">0</div><span id="post-24269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there anyone who can help me? I'm a software student in Iran and I want study the Wireshark source code. I really enjoy this software but I don't know where to start? I need your guidance. Please help me get started. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span> <span class="post-tag tag-link-novice" rel="tag" title="see questions tagged &#39;novice&#39;">novice</span> <span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span> <span class="post-tag tag-link-helpme" rel="tag" title="see questions tagged &#39;helpme&#39;">helpme</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '13, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/58dc0730cb53fb744028a8c308870b51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shaghayegh&#39;s gravatar image" /><p><span>shaghayegh</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shaghayegh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Sep '13, 15:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-24269" class="comments-container"></div><div id="comment-tools-24269" class="comment-tools"></div><div class="clear"></div><div id="comment-24269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24270"></span>

<div id="answer-container-24270" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24270-score" class="post-score" title="current number of votes">1</div><span id="post-24270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See Graham Bloice's answer to <a href="http://ask.wireshark.org/questions/10166/online-tutorial-for-reading-packet-capture-files">this</a> question. Also, <a href="http://www.youtube.com">www.youtube.com</a> hosts plenty of <a href="http://www.youtube.com/results?search_query=wireshark">Wireshark-related videos</a>. Try reading the various README documents in the <a href="http://anonsvn.wireshark.org/viewvc/trunk/doc/">docs/</a> directory of the Wireshark source tree. Or just run a relevant search using your favorite search engine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Sep '13, 15:38</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-24270" class="comments-container"></div><div id="comment-tools-24270" class="comment-tools"></div><div class="clear"></div><div id="comment-24270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

