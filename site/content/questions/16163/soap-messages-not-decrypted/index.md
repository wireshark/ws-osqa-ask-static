+++
type = "question"
title = "Soap Messages Not Decrypted"
description = '''Hello, A little context, I&#x27;m viewing the packet capture of a web service call, and the web service uses SSL. Service request/response is standard SOAP message. Question is, why is it that when I add my key file to Wireshark(via Preferences --&amp;gt; SSL Protocol, which is a .p12 with both the private a...'''
date = "2012-11-21T08:44:00Z"
lastmod = "2012-11-21T08:44:00Z"
weight = 16163
keywords = [ "ssl" ]
aliases = [ "/questions/16163" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Soap Messages Not Decrypted](/questions/16163/soap-messages-not-decrypted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16163-score" class="post-score" title="current number of votes">0</div><span id="post-16163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>A little context, I'm viewing the packet capture of a web service call, and the web service uses SSL. Service request/response is standard SOAP message.</p><p>Question is, why is it that when I add my key file to Wireshark(via Preferences --&gt; SSL Protocol, which is a .p12 with both the private and public keys) that when I choose to "Follow SSL Stream" I can see the Soap request decrypted, but the response is still encrypted? I had thought that by adding the key file I'd be able to see both decrypted.</p><p>Thanks, John</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '12, 08:44</strong></p><img src="https://secure.gravatar.com/avatar/bd7336ee862013e85d5d474672b8777b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JohnA&#39;s gravatar image" /><p><span>JohnA</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JohnA has no accepted answers">0%</span></p></div></div><div id="comments-container-16163" class="comments-container"></div><div id="comment-tools-16163" class="comment-tools"></div><div class="clear"></div><div id="comment-16163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

