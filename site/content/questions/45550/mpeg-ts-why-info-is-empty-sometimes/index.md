+++
type = "question"
title = "mpeg ts - why &quot;info&quot; is empty sometimes"
description = '''Hello, I try to analyze mpeg-ts file. It is strange that the info column is sometimes empty.  Does anyone knows why ? This is the sample I&#x27;m trying to analyze, though I see the issue in every ts file. https://drive.google.com/file/d/0B22GsWueReZTUmJrVGd6WjRSTEU/view?usp=sharing Regards, Ran'''
date = "2015-08-31T11:20:00Z"
lastmod = "2015-08-31T11:20:00Z"
weight = 45550
keywords = [ "mpegts" ]
aliases = [ "/questions/45550" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [mpeg ts - why "info" is empty sometimes](/questions/45550/mpeg-ts-why-info-is-empty-sometimes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45550-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45550-score" class="post-score" title="current number of votes">0</div><span id="post-45550-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I try to analyze mpeg-ts file. It is strange that the info column is sometimes empty. Does anyone knows why ? This is the sample I'm trying to analyze, though I see the issue in every ts file. <a href="https://drive.google.com/file/d/0B22GsWueReZTUmJrVGd6WjRSTEU/view?usp=sharing">https://drive.google.com/file/d/0B22GsWueReZTUmJrVGd6WjRSTEU/view?usp=sharing</a></p><p>Regards, Ran</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mpegts" rel="tag" title="see questions tagged &#39;mpegts&#39;">mpegts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '15, 11:20</strong></p><img src="https://secure.gravatar.com/avatar/9418be66540d8f31d003168235b1b707?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ransh&#39;s gravatar image" /><p><span>ransh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ransh has no accepted answers">0%</span></p></div></div><div id="comments-container-45550" class="comments-container"></div><div id="comment-tools-45550" class="comment-tools"></div><div class="clear"></div><div id="comment-45550-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

