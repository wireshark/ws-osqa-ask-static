+++
type = "question"
title = "Can&#x27;t see Foundation Fieldbus traffic"
description = '''Hello. I can&#x27;t see Foundation Fieldbus traffic. What interface use to see Foundation Fieldbus traffic ? I have only local network connection in Interface List. When I try to apply any filter of Foundation Fieldbus protocol (f-l, ff.fms=true), there is no any result.'''
date = "2015-09-23T23:57:00Z"
lastmod = "2015-09-25T03:00:00Z"
weight = 46092
keywords = [ "interface", "foundation", "fieldbus" ]
aliases = [ "/questions/46092" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see Foundation Fieldbus traffic](/questions/46092/cant-see-foundation-fieldbus-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46092-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46092-score" class="post-score" title="current number of votes">0</div><span id="post-46092-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. I can't see Foundation Fieldbus traffic. What interface use to see Foundation Fieldbus traffic ? I have only local network connection in Interface List. When I try to apply any filter of Foundation Fieldbus protocol (f-l, ff.fms=true), there is no any result.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-foundation" rel="tag" title="see questions tagged &#39;foundation&#39;">foundation</span> <span class="post-tag tag-link-fieldbus" rel="tag" title="see questions tagged &#39;fieldbus&#39;">fieldbus</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '15, 23:57</strong></p><img src="https://secure.gravatar.com/avatar/8cfd332e195556047c64751222e0bda3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="botru&#39;s gravatar image" /><p><span>botru</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="botru has no accepted answers">0%</span></p></div></div><div id="comments-container-46092" class="comments-container"><span id="46113"></span><div id="comment-46113" class="comment"><div id="post-46113-score" class="comment-score"></div><div class="comment-text"><p>What type of FF are you using, H1 or HSE?</p></div><div id="comment-46113-info" class="comment-info"><span class="comment-age">(24 Sep '15, 09:32)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="46119"></span><div id="comment-46119" class="comment"><div id="post-46119-score" class="comment-score"></div><div class="comment-text"><p>I'm using HSE type of FF to connect to modem (gateway to FF device),which transfer from HSE to H1 network.</p></div><div id="comment-46119-info" class="comment-info"><span class="comment-age">(24 Sep '15, 11:27)</span> <span class="comment-user userinfo">botru</span></div></div><span id="46149"></span><div id="comment-46149" class="comment"><div id="post-46149-score" class="comment-score"></div><div class="comment-text"><p>What's the output of <code>dumpcap -D</code>. Depending on your OS and your configuration you may need to elevate that command either using sudo or run from an elevated command prompt.</p></div><div id="comment-46149-info" class="comment-info"><span class="comment-age">(25 Sep '15, 03:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-46092" class="comment-tools"></div><div class="clear"></div><div id="comment-46092-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

