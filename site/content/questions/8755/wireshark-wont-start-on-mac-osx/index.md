+++
type = "question"
title = "Wireshark won&#x27;t start on Mac OSX"
description = '''I installed Wireshark 1.6.5 (Intel 64) on Mac OSX 10.6.8, but when I try to open it (by clicking the application icon), nothing appears to happen. How can I get Wireshark to run properly?'''
date = "2012-02-01T12:22:00Z"
lastmod = "2012-02-07T07:52:00Z"
weight = 8755
keywords = [ "osx", "mac", "installation" ]
aliases = [ "/questions/8755" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark won't start on Mac OSX](/questions/8755/wireshark-wont-start-on-mac-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8755-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8755-score" class="post-score" title="current number of votes">0</div><span id="post-8755-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I installed Wireshark 1.6.5 (Intel 64) on Mac OSX 10.6.8, but when I try to open it (by clicking the application icon), nothing appears to happen. How can I get Wireshark to run properly?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '12, 12:22</strong></p><img src="https://secure.gravatar.com/avatar/a53ebf101bdcc8f9f6a6570513b477f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saurabh&#39;s gravatar image" /><p><span>saurabh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saurabh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Feb '12, 15:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-8755" class="comments-container"><span id="8793"></span><div id="comment-8793" class="comment"><div id="post-8793-score" class="comment-score"></div><div class="comment-text"><p>possible duplicate: <a href="http://ask.wireshark.org/questions/4528/wireshark-does-not-run">wireshark does not run</a></p></div><div id="comment-8793-info" class="comment-info"><span class="comment-age">(02 Feb '12, 15:23)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-8755" class="comment-tools"></div><div class="clear"></div><div id="comment-8755-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8876"></span>

<div id="answer-container-8876" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8876-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8876-score" class="post-score" title="current number of votes">1</div><span id="post-8876-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>did you install X11 ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '12, 07:52</strong></p><img src="https://secure.gravatar.com/avatar/00448c96be3c09429b5391c5566872b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rametep66&#39;s gravatar image" /><p><span>Rametep66</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rametep66 has no accepted answers">0%</span></p></div></div><div id="comments-container-8876" class="comments-container"></div><div id="comment-tools-8876" class="comment-tools"></div><div class="clear"></div><div id="comment-8876-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

