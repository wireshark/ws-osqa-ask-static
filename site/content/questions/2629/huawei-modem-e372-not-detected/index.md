+++
type = "question"
title = "Huawei Modem E372 not Detected"
description = '''Hi,  I am using a Huawei modem E372 to connect to internet . But this device is not listed in the available interfaces.  Any suggestion how to solve this. Thanks, Hany El-Akel'''
date = "2011-03-02T05:31:00Z"
lastmod = "2011-03-02T11:01:00Z"
weight = 2629
keywords = [ "huawei", "detected", "modem", "e372" ]
aliases = [ "/questions/2629" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Huawei Modem E372 not Detected](/questions/2629/huawei-modem-e372-not-detected)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2629-score" class="post-score" title="current number of votes">0</div><span id="post-2629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am using a Huawei modem E372 to connect to internet . But this device is not listed in the available interfaces.</p><p>Any suggestion how to solve this.</p><p>Thanks, Hany El-Akel</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-huawei" rel="tag" title="see questions tagged &#39;huawei&#39;">huawei</span> <span class="post-tag tag-link-detected" rel="tag" title="see questions tagged &#39;detected&#39;">detected</span> <span class="post-tag tag-link-modem" rel="tag" title="see questions tagged &#39;modem&#39;">modem</span> <span class="post-tag tag-link-e372" rel="tag" title="see questions tagged &#39;e372&#39;">e372</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Mar '11, 05:31</strong></p><img src="https://secure.gravatar.com/avatar/17f253ee8e762d6295553f9b6224707e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hany&#39;s gravatar image" /><p><span>Hany</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hany has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Mar '11, 05:33</strong> </span></p></div></div><div id="comments-container-2629" class="comments-container"></div><div id="comment-tools-2629" class="comment-tools"></div><div class="clear"></div><div id="comment-2629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2638"></span>

<div id="answer-container-2638" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2638-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2638-score" class="post-score" title="current number of votes">0</div><span id="post-2638-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Run an operating system other than Windows on your machine, or run Windows 2000 or the 32-bit Windows XP rather than 64-bit Windows XP, Windows Vista, or Windows 7. The Huawei modem in question is a cell-phone modem, which means it shows up as a PPP networking device, and WinPcap, which is what Wireshark uses to capture network traffic on Windows, <a href="http://www.winpcap.org/misc/faq.htm#Q-5">doesn't support PPP devices in Windows Vista or Windows 7</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Mar '11, 11:01</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-2638" class="comments-container"></div><div id="comment-tools-2638" class="comment-tools"></div><div class="clear"></div><div id="comment-2638-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

