+++
type = "question"
title = "Strange window scaling graph"
description = '''Hello, I&#x27;m doing some test on TCP performance and I&#x27;m trying to catch the evolution of the window size through Wireshark. Below you can find the graph and as you can see, there are parallel lines instead of a more linear output... Is this normal ? I&#x27;m more searching for a step-like evolution of the ...'''
date = "2017-02-21T16:44:00Z"
lastmod = "2017-02-24T08:45:00Z"
weight = 59593
keywords = [ "tcpwindowscaling", "tcpwindowsize" ]
aliases = [ "/questions/59593" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Strange window scaling graph](/questions/59593/strange-window-scaling-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59593-score" class="post-score" title="current number of votes">0</div><span id="post-59593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I'm doing some test on TCP performance and I'm trying to catch the evolution of the window size through Wireshark. Below you can find the graph and as you can see, there are parallel lines instead of a more linear output... Is this normal ? I'm more searching for a step-like evolution of the window.</p><p><a href="https://drive.google.com/open?id=0B7Gu57aIrt5wV3k2YlpIa3VNVGM">Graph</a> <img src="https://osqa-ask.wireshark.org/upfiles/6001.png" alt="alt text" /></p><p>Thanks for your suggestions</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcpwindowscaling" rel="tag" title="see questions tagged &#39;tcpwindowscaling&#39;">tcpwindowscaling</span> <span class="post-tag tag-link-tcpwindowsize" rel="tag" title="see questions tagged &#39;tcpwindowsize&#39;">tcpwindowsize</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '17, 16:44</strong></p><img src="https://secure.gravatar.com/avatar/1d1ada0bff46b3f280f132953846f501?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="astrid&#39;s gravatar image" /><p><span>astrid</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="astrid has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Feb '17, 08:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-59593" class="comments-container"><span id="59660"></span><div id="comment-59660" class="comment"><div id="post-59660-score" class="comment-score"></div><div class="comment-text"><p>Astrid,</p><p>could you add more information?</p><p>Like: * Have a display filter applied to the graph? * Is the reported window size from one TCP session and one direction?</p><p>Maybe sharing the pcap (s. <a href="https://blog.packet-foo.com/2016/11/the-wireshark-qa-trace-file-sharing-tutorial/)">https://blog.packet-foo.com/2016/11/the-wireshark-qa-trace-file-sharing-tutorial/)</a> can also help.</p></div><div id="comment-59660-info" class="comment-info"><span class="comment-age">(24 Feb '17, 07:15)</span> <span class="comment-user userinfo">Uli</span></div></div><span id="59666"></span><div id="comment-59666" class="comment"><div id="post-59666-score" class="comment-score"></div><div class="comment-text"><p>If you're going to provide an image, you should include it in the question instead of linking to it, as the image isn't necessarily accessible by everyone due to certain company internet policies.</p></div><div id="comment-59666-info" class="comment-info"><span class="comment-age">(24 Feb '17, 08:45)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-59593" class="comment-tools"></div><div class="clear"></div><div id="comment-59593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

