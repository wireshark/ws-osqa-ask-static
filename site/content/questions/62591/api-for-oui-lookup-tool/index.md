+++
type = "question"
title = "API for OUI Lookup Tool"
description = '''Is there an API for Wireshark OUI Lookup Tool?'''
date = "2017-07-06T13:51:00Z"
lastmod = "2017-07-14T09:17:00Z"
weight = 62591
keywords = [ "oui", "api" ]
aliases = [ "/questions/62591" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [API for OUI Lookup Tool](/questions/62591/api-for-oui-lookup-tool)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62591-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62591-score" class="post-score" title="current number of votes">0</div><span id="post-62591-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there an API for Wireshark OUI Lookup Tool?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-oui" rel="tag" title="see questions tagged &#39;oui&#39;">oui</span> <span class="post-tag tag-link-api" rel="tag" title="see questions tagged &#39;api&#39;">api</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '17, 13:51</strong></p><img src="https://secure.gravatar.com/avatar/6f5f740b6c618c8c957a18690d1ffacb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="simransk&#39;s gravatar image" /><p><span>simransk</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="simransk has no accepted answers">0%</span></p></div></div><div id="comments-container-62591" class="comments-container"></div><div id="comment-tools-62591" class="comment-tools"></div><div class="clear"></div><div id="comment-62591-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62783"></span>

<div id="answer-container-62783" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62783-score" class="post-score" title="current number of votes">0</div><span id="post-62783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The OUI lookup tool downloads a JSON-ified version of the <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf">manuf file</a> and does its work in your browser. It doesn't connect to any sort of backend API or database. The data itself is less than 2 MB.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jul '17, 09:17</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-62783" class="comments-container"></div><div id="comment-tools-62783" class="comment-tools"></div><div class="clear"></div><div id="comment-62783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

