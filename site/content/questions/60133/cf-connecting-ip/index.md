+++
type = "question"
title = "CF-Connecting-IP"
description = '''Hello, I would like to capture CloudFlare&#x27;s CF-Connecting-IP special header (client&#x27;s IP) due to an ongoing DDoS attack. Actually, i want to see the IPs that have the larger number of connections. But since we are behind cloud flare, we need to use the header CF-Connecting-IP. How can i do that usin...'''
date = "2017-03-16T18:09:00Z"
lastmod = "2017-03-16T18:09:00Z"
weight = 60133
keywords = [ "cloudflare" ]
aliases = [ "/questions/60133" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [CF-Connecting-IP](/questions/60133/cf-connecting-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60133-score" class="post-score" title="current number of votes">0</div><span id="post-60133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I would like to capture CloudFlare's CF-Connecting-IP special header (client's IP) due to an ongoing DDoS attack. Actually, i want to see the IPs that have the larger number of connections.</p><p>But since we are behind cloud flare, we need to use the header CF-Connecting-IP.</p><p>How can i do that using Wireshark?</p><p>Thanks S.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cloudflare" rel="tag" title="see questions tagged &#39;cloudflare&#39;">cloudflare</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '17, 18:09</strong></p><img src="https://secure.gravatar.com/avatar/6862e36afb0a54fdd281841b22aef96e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="planck&#39;s gravatar image" /><p><span>planck</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="planck has no accepted answers">0%</span></p></div></div><div id="comments-container-60133" class="comments-container"></div><div id="comment-tools-60133" class="comment-tools"></div><div class="clear"></div><div id="comment-60133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

