+++
type = "question"
title = "Spurious Retransmission"
description = '''Hi folks, Can someone please help me read the following capture: from our office space we cant seem to visite a certain website. This site is reachable via the net (from home etc). I have made a capture from the fw and it shows the following. There is nothing being blocked by the firewall. '''
date = "2015-10-28T01:56:00Z"
lastmod = "2015-10-28T14:23:00Z"
weight = 47007
keywords = [ "spurious", "retransmission" ]
aliases = [ "/questions/47007" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Spurious Retransmission](/questions/47007/spurious-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47007-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47007-score" class="post-score" title="current number of votes">0</div><span id="post-47007-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi folks,</p><p>Can someone please help me read the following capture: from our office space we cant seem to visite a certain website. This site is reachable via the net (from home etc). I have made a capture from the fw and it shows the following. There is nothing being blocked by the firewall.</p><p><img src="http://i64.tinypic.com/33mrdzd.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spurious" rel="tag" title="see questions tagged &#39;spurious&#39;">spurious</span> <span class="post-tag tag-link-retransmission" rel="tag" title="see questions tagged &#39;retransmission&#39;">retransmission</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '15, 01:56</strong></p><img src="https://secure.gravatar.com/avatar/b9f63a32cfd667b83137e277d27677c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alan%20Telo&#39;s gravatar image" /><p><span>Alan Telo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alan Telo has no accepted answers">0%</span></p></img></div></div><div id="comments-container-47007" class="comments-container"><span id="47036"></span><div id="comment-47036" class="comment"><div id="post-47036-score" class="comment-score"></div><div class="comment-text"><p>Could you provide us a tracefile? You can anonymize with the tool tracewrangler (<a href="https://www.tracewrangler.com">https://www.tracewrangler.com</a>)</p></div><div id="comment-47036-info" class="comment-info"><span class="comment-age">(28 Oct '15, 14:23)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-47007" class="comment-tools"></div><div class="clear"></div><div id="comment-47007-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

