+++
type = "question"
title = "Reading packet captures."
description = '''Is there a &quot;reading packet captures for dummies&quot; anywhere? I just do not seem to understand a few things. I would like to view images moving across the network as well as know how to decrypt and/or reassemble (for viewing) packet bytes. Some of the wiki pages and tutorials just seem like mumbo jumbo...'''
date = "2011-09-08T21:40:00Z"
lastmod = "2011-09-08T23:15:00Z"
weight = 6226
keywords = [ "reading", "packets" ]
aliases = [ "/questions/6226" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Reading packet captures.](/questions/6226/reading-packet-captures)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6226-score" class="post-score" title="current number of votes">0</div><span id="post-6226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a "reading packet captures for dummies" anywhere? I just do not seem to understand a few things.</p><p>I would like to view images moving across the network as well as know how to decrypt and/or reassemble (for viewing) packet bytes.</p><p>Some of the wiki pages and tutorials just seem like mumbo jumbo to me.</p><p>Appreciate any help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reading" rel="tag" title="see questions tagged &#39;reading&#39;">reading</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '11, 21:40</strong></p><img src="https://secure.gravatar.com/avatar/c4274755acb552feda4555da3406dff3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wshrkmkn&#39;s gravatar image" /><p><span>wshrkmkn</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wshrkmkn has no accepted answers">0%</span></p></div></div><div id="comments-container-6226" class="comments-container"></div><div id="comment-tools-6226" class="comment-tools"></div><div class="clear"></div><div id="comment-6226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6229"></span>

<div id="answer-container-6229" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6229-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6229-score" class="post-score" title="current number of votes">0</div><span id="post-6229-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could look at <a href="http://www.wiresharktraining.com/index.html">Wireshark University</a> or <a href="http://www.tcpipguide.com/index.htm">The TCP/IP Guide</a> for instance.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '11, 23:15</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6229" class="comments-container"></div><div id="comment-tools-6229" class="comment-tools"></div><div class="clear"></div><div id="comment-6229-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

