+++
type = "question"
title = "How to request a new dissector supporting a protocol?"
description = '''Hi guys, I&#x27;d like to ask that, if we have a mechanism for us to submit a request for supporting a certain protocol? I found that there are some bugs in the bugzilla for this purpose, but I don&#x27;t think this is a clear and easy way. Do you have any answer? Thanks.'''
date = "2013-08-27T20:35:00Z"
lastmod = "2013-08-28T01:11:00Z"
weight = 24115
keywords = [ "request" ]
aliases = [ "/questions/24115" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to request a new dissector supporting a protocol?](/questions/24115/how-to-request-a-new-dissector-supporting-a-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24115-score" class="post-score" title="current number of votes">0</div><span id="post-24115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys, I'd like to ask that, if we have a mechanism for us to submit a request for supporting a certain protocol? I found that there are some bugs in the bugzilla for this purpose, but I don't think this is a clear and easy way. Do you have any answer?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '13, 20:35</strong></p><img src="https://secure.gravatar.com/avatar/f93696f1f8cda9ab2c5efc13d5b35625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="polerfox&#39;s gravatar image" /><p><span>polerfox</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="polerfox has no accepted answers">0%</span></p></div></div><div id="comments-container-24115" class="comments-container"></div><div id="comment-tools-24115" class="comment-tools"></div><div class="clear"></div><div id="comment-24115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24116"></span>

<div id="answer-container-24116" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24116-score" class="post-score" title="current number of votes">1</div><span id="post-24116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The way to request a new dissector IS through bugzilla. BUT as people work on Wireshark on their spare time or as part of their daytime job on stuff interesting their employer. A dissector will only be developed if and when someone takes an interest in developing it. The bug report should contain a referent to the relevant protocol description(s) and prefrerably traces of the protocol in question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Aug '13, 21:52</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-24116" class="comments-container"><span id="24117"></span><div id="comment-24117" class="comment"><div id="post-24117-score" class="comment-score"></div><div class="comment-text"><p>Note that a bugzilla entry for a new dissector should be marked as an "Enhancement".</p></div><div id="comment-24117-info" class="comment-info"><span class="comment-age">(28 Aug '13, 01:11)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-24116" class="comment-tools"></div><div class="clear"></div><div id="comment-24116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

