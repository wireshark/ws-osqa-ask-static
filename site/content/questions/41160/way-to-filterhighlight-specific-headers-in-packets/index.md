+++
type = "question"
title = "Way to filter/highlight specific headers in packets?"
description = '''Is there a way to highlight all the &#x27;to&#x27; and &#x27;from&#x27;headers in captured sip packets? So that it will be easier to quickly identify the useful bits of the message? Is there any plugin or feature in WS that will do this? Thanks'''
date = "2015-04-02T11:26:00Z"
lastmod = "2015-04-06T03:49:00Z"
weight = 41160
keywords = [ "filter", "highlight", "sip", "multiple-headers" ]
aliases = [ "/questions/41160" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Way to filter/highlight specific headers in packets?](/questions/41160/way-to-filterhighlight-specific-headers-in-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41160-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41160-score" class="post-score" title="current number of votes">0</div><span id="post-41160-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to highlight all the 'to' and 'from'headers in captured sip packets?</p><p>So that it will be easier to quickly identify the useful bits of the message?</p><p>Is there any plugin or feature in WS that will do this?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-highlight" rel="tag" title="see questions tagged &#39;highlight&#39;">highlight</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-multiple-headers" rel="tag" title="see questions tagged &#39;multiple-headers&#39;">multiple-headers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '15, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/7aff65395956d148edff666327e1bd28?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HappyTimes&#39;s gravatar image" /><p><span>HappyTimes</span><br />
<span class="score" title="10 reputation points">10</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HappyTimes has no accepted answers">0%</span></p></div></div><div id="comments-container-41160" class="comments-container"></div><div id="comment-tools-41160" class="comment-tools"></div><div class="clear"></div><div id="comment-41160-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41212"></span>

<div id="answer-container-41212" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41212-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41212-score" class="post-score" title="current number of votes">1</div><span id="post-41212-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="HappyTimes has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no way to color fields in the details pane (I guess that's what you are asking for) and I'm also not aware of any third-party plugin that is able to do that.</p><p>So, if you need that functionality, please file an enhancement bug at <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> .</p><p>What you can do in the meantime: Add columns for those fields you want to see directly, like in the following example.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/sip_column.png" alt="alt text" /></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '15, 03:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></img></div></div><div id="comments-container-41212" class="comments-container"></div><div id="comment-tools-41212" class="comment-tools"></div><div class="clear"></div><div id="comment-41212-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

