+++
type = "question"
title = "can i export selected packet bytes from a stream using Tshark ?"
description = '''Hi, I am trying to extract a file from a capture. It can be done using wireshark by identifying a TCP stream and then &quot;Export selected packet bytes&quot; from the stream. Is it possible to reproduce it in Tshark? thanks!'''
date = "2011-02-02T06:32:00Z"
lastmod = "2011-02-02T06:32:00Z"
weight = 2103
keywords = [ "tshark", "stream" ]
aliases = [ "/questions/2103" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can i export selected packet bytes from a stream using Tshark ?](/questions/2103/can-i-export-selected-packet-bytes-from-a-stream-using-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2103-score" class="post-score" title="current number of votes">0</div><span id="post-2103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to extract a file from a capture. It can be done using wireshark by identifying a TCP stream and then "Export selected packet bytes" from the stream.</p><p>Is it possible to reproduce it in Tshark?</p><p>thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '11, 06:32</strong></p><img src="https://secure.gravatar.com/avatar/765b158b45bb08f96b9e48acafd33579?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thsark_user&#39;s gravatar image" /><p><span>thsark_user</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thsark_user has no accepted answers">0%</span></p></div></div><div id="comments-container-2103" class="comments-container"></div><div id="comment-tools-2103" class="comment-tools"></div><div class="clear"></div><div id="comment-2103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

