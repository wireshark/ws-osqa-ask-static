+++
type = "question"
title = "Wireshark-users Digest, Vol 65, Issue 3"
description = '''I received a message with the subject: Wireshark-users Digest, Vol 65, Issue 3 from &quot;wireshark-users-request&quot; It was 13.8 MB long. Not sure how it did it but it took my email client DOWN - twice - before I figured out that there was an issue with this email. The only thing peculiar about the message...'''
date = "2011-10-04T12:06:00Z"
lastmod = "2011-10-04T12:15:00Z"
weight = 6715
keywords = [ "email" ]
aliases = [ "/questions/6715" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark-users Digest, Vol 65, Issue 3](/questions/6715/wireshark-users-digest-vol-65-issue-3)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6715-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6715-score" class="post-score" title="current number of votes">0</div><span id="post-6715-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I received a message with the subject: Wireshark-users Digest, Vol 65, Issue 3 from "wireshark-users-request"</p><p>It was 13.8 MB long. Not sure how it did it but it took my email client DOWN - twice - before I figured out that there was an issue with this email.</p><p>The only thing peculiar about the message is the length - I don't recall any of these messages being even one MB.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '11, 12:06</strong></p><img src="https://secure.gravatar.com/avatar/2b54913de7bfd696b930bdc190d8ae90?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gary&#39;s gravatar image" /><p><span>Gary</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gary has no accepted answers">0%</span></p></div></div><div id="comments-container-6715" class="comments-container"><span id="6716"></span><div id="comment-6716" class="comment"><div id="post-6716-score" class="comment-score"></div><div class="comment-text"><p>Someone had attached a very large capture in text format (and someone else <a href="http://www.wireshark.org/lists/wireshark-users/201110/msg00008.html">complained</a> about it).</p></div><div id="comment-6716-info" class="comment-info"><span class="comment-age">(04 Oct '11, 12:15)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-6715" class="comment-tools"></div><div class="clear"></div><div id="comment-6715-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

