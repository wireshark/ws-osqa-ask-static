+++
type = "question"
title = "[closed] How to remove passphase from PEM file by code (c#)?"
description = '''Thanks. I am looking for an option to use this from c# code. I am able to execute this line (Process that starts CMD and calls it). But not able to send the &quot;passphase&quot; right after it. Is there an option to call that command with the passphase? Or maybe familiar with a method to do it from the c# co...'''
date = "2015-11-02T03:30:00Z"
lastmod = "2015-11-02T03:30:00Z"
weight = 47143
keywords = [ "pem", "wireshark" ]
aliases = [ "/questions/47143" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to remove passphase from PEM file by code (c\#)?](/questions/47143/how-to-remove-passphase-from-pem-file-by-code-c)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47143-score" class="post-score" title="current number of votes">0</div><span id="post-47143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Thanks. I am looking for an option to use this from c# code. I am able to execute this line (Process that starts CMD and calls it). But not able to send the "passphase" right after it. Is there an option to call that command with the passphase? Or maybe familiar with a method to do it from the c# code? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pem" rel="tag" title="see questions tagged &#39;pem&#39;">pem</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '15, 03:30</strong></p><img src="https://secure.gravatar.com/avatar/021f8f9e84e2ba27274d165e39fe4713?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yoava&#39;s gravatar image" /><p><span>yoava</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yoava has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>02 Nov '15, 04:02</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-47143" class="comments-container"></div><div id="comment-tools-47143" class="comment-tools"></div><div class="clear"></div><div id="comment-47143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "This is the Wireshark QA site, not a C\# programming forum. Please ask your question in a C\# forum or on stackoverflow.com" by Kurt Knochner 02 Nov '15, 04:02

</div>

</div>

</div>

