+++
type = "question"
title = "Runtime Error when save capture file before program quit without stopping capturing data from network card."
description = '''Runtime Error when save capture file before program quit without stopping capturing data from network card.'''
date = "2011-03-21T23:19:00Z"
lastmod = "2011-03-21T23:19:00Z"
weight = 3008
keywords = [ "before", "quit", "save", "error" ]
aliases = [ "/questions/3008" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Runtime Error when save capture file before program quit without stopping capturing data from network card.](/questions/3008/runtime-error-when-save-capture-file-before-program-quit-without-stopping-capturing-data-from-network-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3008-score" class="post-score" title="current number of votes">0</div><span id="post-3008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Runtime Error when save capture file before program quit without stopping capturing data from network card.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-before" rel="tag" title="see questions tagged &#39;before&#39;">before</span> <span class="post-tag tag-link-quit" rel="tag" title="see questions tagged &#39;quit&#39;">quit</span> <span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '11, 23:19</strong></p><img src="https://secure.gravatar.com/avatar/9f94dd6c84b70b9abb80a22546d09710?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brenthuang&#39;s gravatar image" /><p><span>brenthuang</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brenthuang has no accepted answers">0%</span></p></div></div><div id="comments-container-3008" class="comments-container"></div><div id="comment-tools-3008" class="comment-tools"></div><div class="clear"></div><div id="comment-3008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

