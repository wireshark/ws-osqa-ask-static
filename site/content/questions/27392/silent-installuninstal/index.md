+++
type = "question"
title = "Silent install/uninstal"
description = '''How to do silent install/uninstall in a production environment of this application?'''
date = "2013-11-26T02:33:00Z"
lastmod = "2013-11-27T06:17:00Z"
weight = 27392
keywords = [ "silent" ]
aliases = [ "/questions/27392" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Silent install/uninstal](/questions/27392/silent-installuninstal)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27392-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27392-score" class="post-score" title="current number of votes">0</div><span id="post-27392-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to do silent install/uninstall in a production environment of this application?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-silent" rel="tag" title="see questions tagged &#39;silent&#39;">silent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '13, 02:33</strong></p><img src="https://secure.gravatar.com/avatar/3b5f269302565f5ca1b4ba81e2aca5e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nitin&#39;s gravatar image" /><p><span>Nitin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nitin has no accepted answers">0%</span></p></div></div><div id="comments-container-27392" class="comments-container"></div><div id="comment-tools-27392" class="comment-tools"></div><div class="clear"></div><div id="comment-27392-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27397"></span>

<div id="answer-container-27397" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27397-score" class="post-score" title="current number of votes">0</div><span id="post-27397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the answer to a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/1902/unable-to-install-uninstall-wireshark-silently">http://ask.wireshark.org/questions/1902/unable-to-install-uninstall-wireshark-silently</a></p></blockquote><p>Solution: use the cli switch <code>/S</code></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Nov '13, 02:54</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Nov '13, 04:48</strong> </span></p></div></div><div id="comments-container-27397" class="comments-container"><span id="27480"></span><div id="comment-27480" class="comment"><div id="post-27480-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt can please let me know how to do silent install for the WinPcap 4.1.3 as silent installation of wireshark didn't install WinPcap.</p></div><div id="comment-27480-info" class="comment-info"><span class="comment-age">(27 Nov '13, 02:59)</span> <span class="comment-user userinfo">Nitin</span></div></div><span id="27487"></span><div id="comment-27487" class="comment"><div id="post-27487-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately there is <strong>no silent installer</strong> for WinPcap! There are however descriptions how to do it 'manually'</p><blockquote><p><a href="http://paperlined.org/apps/wireshark/winpcap_silent_install.html">http://paperlined.org/apps/wireshark/winpcap_silent_install.html</a><br />
</p></blockquote><p>or how to automate the installation with AutoIT.</p><blockquote><p><a href="http://briandesmond.com/blog/installing-winpcap-silently/">http://briandesmond.com/blog/installing-winpcap-silently/</a><br />
</p></blockquote><p>As an alternative: Build your own WinPcap installer with <a href="http://nsis.sourceforge.net/Main_Page">NSIS</a> (or your preferred windows installer tool) to allow a silent installation.</p><p>Sorry, currently no other option...</p></div><div id="comment-27487-info" class="comment-info"><span class="comment-age">(27 Nov '13, 06:17)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27397" class="comment-tools"></div><div class="clear"></div><div id="comment-27397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

