+++
type = "question"
title = "TShark - export TCP conversations while running"
description = '''Is there a way to export TCP conversations statistics while Tshark is running? I would like the output of &quot;tshark -i any -z conv,tcp -q -n&quot; to be shown continuously while the capture is still running (not only when it is finished). PS: I noticed that Wireshark shows these statistics while running, b...'''
date = "2015-07-22T05:38:00Z"
lastmod = "2015-07-22T05:38:00Z"
weight = 44371
keywords = [ "conversations", "statistics", "tshark", "tcp" ]
aliases = [ "/questions/44371" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TShark - export TCP conversations while running](/questions/44371/tshark-export-tcp-conversations-while-running)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44371-score" class="post-score" title="current number of votes">0</div><span id="post-44371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to export TCP conversations statistics while Tshark is running? I would like the output of "tshark -i any -z conv,tcp -q -n" to be shown continuously while the capture is still running (not only when it is finished).</p><p>PS: I noticed that Wireshark shows these statistics while running, but I want to do this with CLI only.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversations" rel="tag" title="see questions tagged &#39;conversations&#39;">conversations</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '15, 05:38</strong></p><img src="https://secure.gravatar.com/avatar/45fe407d9296515eb602b728fc0a8100?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EBG&#39;s gravatar image" /><p><span>EBG</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EBG has no accepted answers">0%</span></p></div></div><div id="comments-container-44371" class="comments-container"></div><div id="comment-tools-44371" class="comment-tools"></div><div class="clear"></div><div id="comment-44371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

