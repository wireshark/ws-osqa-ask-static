+++
type = "question"
title = "wire shark shows a lot  of LLC protocol from my mobile devices?"
description = '''i use ubuntu 14.04 and wireless connection  when i start capturing wire shark gives me llc protocol from my mobile devices  and the only data i can read is from my host(laptop) i am a wireshark noob .. so any help??'''
date = "2015-07-17T06:22:00Z"
lastmod = "2015-07-17T06:25:00Z"
weight = 44238
keywords = [ "llc", "14.04", "ubuntu", "wireshark" ]
aliases = [ "/questions/44238" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wire shark shows a lot of LLC protocol from my mobile devices?](/questions/44238/wire-shark-shows-a-lot-of-llc-protocol-from-my-mobile-devices)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44238-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44238-score" class="post-score" title="current number of votes">0</div><span id="post-44238-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i use ubuntu 14.04 and wireless connection<br />
when i start capturing wire shark gives me llc protocol from my mobile devices<br />
and the only data i can read is from my host(laptop)<br />
i am a wireshark noob .. so any help??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-llc" rel="tag" title="see questions tagged &#39;llc&#39;">llc</span> <span class="post-tag tag-link-14.04" rel="tag" title="see questions tagged &#39;14.04&#39;">14.04</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jul '15, 06:22</strong></p><img src="https://secure.gravatar.com/avatar/8d5d9781e0cb31a58321bdc0a42ebc87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ahmed%20Al-hadi&#39;s gravatar image" /><p><span>Ahmed Al-hadi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ahmed Al-hadi has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-44238" class="comments-container"></div><div id="comment-tools-44238" class="comment-tools"></div><div class="clear"></div><div id="comment-44238-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44239"></span>

<div id="answer-container-44239" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44239-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44239-score" class="post-score" title="current number of votes">0</div><span id="post-44239-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Likely that you have encryption enabled on the WLAN, see the Wiki page on <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">WLAN capturing</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jul '15, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span> </br></p></div></div><div id="comments-container-44239" class="comments-container"></div><div id="comment-tools-44239" class="comment-tools"></div><div class="clear"></div><div id="comment-44239-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

