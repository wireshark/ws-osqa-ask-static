+++
type = "question"
title = "wireshark crashes when exporting a PDML file"
description = '''Using wireshark version 1.10.1. Using DNP 3.0 Transport decoder when export as XML - &quot;PDML&quot; wireshark consistently crashes.'''
date = "2013-08-21T21:31:00Z"
lastmod = "2013-08-22T01:06:00Z"
weight = 23937
keywords = [ "pdml" ]
aliases = [ "/questions/23937" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark crashes when exporting a PDML file](/questions/23937/wireshark-crashes-when-exporting-a-pdml-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23937-score" class="post-score" title="current number of votes">0</div><span id="post-23937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using wireshark version 1.10.1. Using DNP 3.0 Transport decoder when export as XML - "PDML" wireshark consistently crashes.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pdml" rel="tag" title="see questions tagged &#39;pdml&#39;">pdml</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '13, 21:31</strong></p><img src="https://secure.gravatar.com/avatar/632fb54f72542ddb328fbf73c61f840b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smajor&#39;s gravatar image" /><p><span>smajor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smajor has no accepted answers">0%</span></p></div></div><div id="comments-container-23937" class="comments-container"></div><div id="comment-tools-23937" class="comment-tools"></div><div class="clear"></div><div id="comment-23937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23942"></span>

<div id="answer-container-23942" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23942-score" class="post-score" title="current number of votes">1</div><span id="post-23942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a bug, not a question. Please report the bug at the Wireshark <a href="https://bugs.wireshark.org/bugzilla/">Bugzilla</a> site, attaching a capture that illustrates the problem, and then post the bug number back here as a comment.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Aug '13, 01:06</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-23942" class="comments-container"></div><div id="comment-tools-23942" class="comment-tools"></div><div class="clear"></div><div id="comment-23942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

