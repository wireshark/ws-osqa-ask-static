+++
type = "question"
title = "Capturing multiple Exegin boxes on one host"
description = '''I think I have a unique problem. I would like to have multiple Exegin boxes sniffing RF traffic with static IPs (connected to 3G/4G modems) being captured simultaneously on multiple instances of Wireshark on a host Unix machine. I setup the host to have a VLAN for each instance of Wireshark to captu...'''
date = "2012-02-01T16:50:00Z"
lastmod = "2012-02-06T10:31:00Z"
weight = 8760
keywords = [ "remote", "vlan", "exegin", "ssh" ]
aliases = [ "/questions/8760" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing multiple Exegin boxes on one host](/questions/8760/capturing-multiple-exegin-boxes-on-one-host)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8760-score" class="post-score" title="current number of votes">0</div><span id="post-8760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I think I have a unique problem. I would like to have multiple Exegin boxes sniffing RF traffic with static IPs (connected to 3G/4G modems) being captured simultaneously on multiple instances of Wireshark on a host Unix machine. I setup the host to have a VLAN for each instance of Wireshark to capture each Exegin's traffic. (If there's a more elegant way to do this, please tell me!)</p><p>So far, I've learned that I need to SSH to remotely capture traffic. But since the Exegin isn't a computer where I can setup tshark or SSH, how do I make this work?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span> <span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-exegin" rel="tag" title="see questions tagged &#39;exegin&#39;">exegin</span> <span class="post-tag tag-link-ssh" rel="tag" title="see questions tagged &#39;ssh&#39;">ssh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '12, 16:50</strong></p><img src="https://secure.gravatar.com/avatar/4d316f7decaed1489f2a2876c74d711e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="paulsce&#39;s gravatar image" /><p><span>paulsce</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="paulsce has no accepted answers">0%</span></p></div></div><div id="comments-container-8760" class="comments-container"><span id="8761"></span><div id="comment-8761" class="comment"><div id="post-8761-score" class="comment-score"></div><div class="comment-text"><p>So are the "Exegin boxes" <a href="http://www.exegin.com/hardware/q51app.php">q51 PANalyzer</a> devices?</p></div><div id="comment-8761-info" class="comment-info"><span class="comment-age">(01 Feb '12, 19:09)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="8779"></span><div id="comment-8779" class="comment"><div id="post-8779-score" class="comment-score"></div><div class="comment-text"><p>Yes, yes they are.</p></div><div id="comment-8779-info" class="comment-info"><span class="comment-age">(02 Feb '12, 09:43)</span> <span class="comment-user userinfo">paulsce</span></div></div></div><div id="comment-tools-8760" class="comment-tools"></div><div class="clear"></div><div id="comment-8760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8857"></span>

<div id="answer-container-8857" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8857-score" class="post-score" title="current number of votes">0</div><span id="post-8857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you "need to SSH to remotely capture traffic" for internal security reasons?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '12, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/2daac0a41fead6cda1ad1c2fa19f9ee9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ffierling&#39;s gravatar image" /><p><span>ffierling</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ffierling has no accepted answers">0%</span></p></div></div><div id="comments-container-8857" class="comments-container"></div><div id="comment-tools-8857" class="comment-tools"></div><div class="clear"></div><div id="comment-8857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

