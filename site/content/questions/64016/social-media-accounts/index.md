+++
type = "question"
title = "Social media accounts"
description = '''Can wireshark be used to determine user names of facebook? I know my kids have accounts that I have access to, but I suspect they also have accounts that I am not seeing.'''
date = "2017-10-18T13:21:00Z"
lastmod = "2017-10-19T00:32:00Z"
weight = 64016
keywords = [ "socialmedia" ]
aliases = [ "/questions/64016" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Social media accounts](/questions/64016/social-media-accounts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64016-score" class="post-score" title="current number of votes">0</div><span id="post-64016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can wireshark be used to determine user names of facebook? I know my kids have accounts that I have access to, but I suspect they also have accounts that I am not seeing.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-socialmedia" rel="tag" title="see questions tagged &#39;socialmedia&#39;">socialmedia</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '17, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/149215dbf1f3916e7e97f53fd168c7fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="benny19130&#39;s gravatar image" /><p><span>benny19130</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="benny19130 has no accepted answers">0%</span></p></div></div><div id="comments-container-64016" class="comments-container"></div><div id="comment-tools-64016" class="comment-tools"></div><div class="clear"></div><div id="comment-64016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64024"></span>

<div id="answer-container-64024" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64024-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64024-score" class="post-score" title="current number of votes">0</div><span id="post-64024-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think so. Facebook is using HTTPS, meaning that everything they do is encrypted. This question goes in the same general direction as this post (meaning, the same rules apply):</p><p><a href="https://blog.packet-foo.com/2016/07/how-to-use-wireshark-to-steal-passwords/">https://blog.packet-foo.com/2016/07/how-to-use-wireshark-to-steal-passwords/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '17, 00:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-64024" class="comments-container"></div><div id="comment-tools-64024" class="comment-tools"></div><div class="clear"></div><div id="comment-64024-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

