+++
type = "question"
title = "Plugin for wireshark"
description = '''Hi, I need a plugin for wireshark of the protocol Siemens CorNet-IP?? I make Bachelor&#x27;s thesis for theme Analysis of Protocol Siemens. Thanks'''
date = "2011-02-01T08:54:00Z"
lastmod = "2011-02-02T08:00:00Z"
weight = 2071
keywords = [ "david" ]
aliases = [ "/questions/2071" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Plugin for wireshark](/questions/2071/plugin-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2071-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2071-score" class="post-score" title="current number of votes">0</div><span id="post-2071-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I need a plugin for wireshark of the protocol Siemens CorNet-IP?? I make Bachelor's thesis for theme Analysis of Protocol Siemens. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-david" rel="tag" title="see questions tagged &#39;david&#39;">david</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '11, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/ec1cd30b7181e42afe1697e508e2f4fb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="David%20B%C4%9Bl%C3%ADk&#39;s gravatar image" /><p><span>David Bělík</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="David Bělík has no accepted answers">0%</span></p></div></div><div id="comments-container-2071" class="comments-container"></div><div id="comment-tools-2071" class="comment-tools"></div><div class="clear"></div><div id="comment-2071-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2087"></span>

<div id="answer-container-2087" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2087-score" class="post-score" title="current number of votes">0</div><span id="post-2087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think Siemens has published the specifications of the HiPath Feature Access protocol. That would be the least that's needed to write a dissector, or it would have to be reverse engineered. Neither of which we've seen.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '11, 15:09</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2087" class="comments-container"><span id="2107"></span><div id="comment-2107" class="comment"><div id="post-2107-score" class="comment-score"></div><div class="comment-text"><p>It is a pity, 'cause I do not know how to describe the protocol (decode), though I know nothing about him. You do not have any advice please?</p></div><div id="comment-2107-info" class="comment-info"><span class="comment-age">(02 Feb '11, 08:00)</span> <span class="comment-user userinfo">David Bělík</span></div></div></div><div id="comment-tools-2087" class="comment-tools"></div><div class="clear"></div><div id="comment-2087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

