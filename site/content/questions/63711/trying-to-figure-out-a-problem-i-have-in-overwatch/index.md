+++
type = "question"
title = "Trying to figure out a problem I have in Overwatch"
description = '''Hey, I suspect that my ISP is restricting my internet connection in peak hours to decrease the load from their network. I am playing the game Overwatch from Blizzard and recently I have an issue where I can&#x27;t login to the game and it&#x27;s stuck on the login screen where it says &#x27;Entering game&#x27;... then ...'''
date = "2017-10-06T07:25:00Z"
lastmod = "2017-10-06T07:25:00Z"
weight = 63711
keywords = [ "overwatch" ]
aliases = [ "/questions/63711" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Trying to figure out a problem I have in Overwatch](/questions/63711/trying-to-figure-out-a-problem-i-have-in-overwatch)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63711-score" class="post-score" title="current number of votes">0</div><span id="post-63711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey,</p><p>I suspect that my ISP is restricting my internet connection in peak hours to decrease the load from their network. I am playing the game Overwatch from Blizzard and recently I have an issue where I can't login to the game and it's stuck on the login screen where it says 'Entering game'... then I receive an error 'Lost connection to the game server'. After a while when this is not a peak hour, I can login to the game normally. I contacted Blizzard and there are no issues from their side... also my friends are able to login without any issues during the peak hours. I want to mention that my internet connection except that works perfect, there are no high ping issues, download and upload speeds are fine, no disconnections etc... (except disconnections from Overwatch servers...) I started capturing with Wireshark from the moment I hit 'Play' on Blizzard App till the moment I get the error 'Lost connection to the game server' and saved this log for your analysis. I hope there is anyone professional who can tell me what's exactly the problem here, so I can go with it to my ISP and blame them for restricting my connection.</p><p>Here's the capture: <a href="https://www.cloudshark.org/captures/61982ddd41ed">https://www.cloudshark.org/captures/61982ddd41ed</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-overwatch" rel="tag" title="see questions tagged &#39;overwatch&#39;">overwatch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '17, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/86d050d9c447581082751ae390715677?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="duzap&#39;s gravatar image" /><p><span>duzap</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="duzap has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Oct '17, 07:25</strong> </span></p></div></div><div id="comments-container-63711" class="comments-container"></div><div id="comment-tools-63711" class="comment-tools"></div><div class="clear"></div><div id="comment-63711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

