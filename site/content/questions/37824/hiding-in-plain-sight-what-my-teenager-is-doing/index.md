+++
type = "question"
title = "Hiding in plain sight... what my teenager is doing?"
description = '''My teenager has discovered a way to circumvent my netgear wndr4500 router logging. Any ideas on how to shine a light on these activities?'''
date = "2014-11-13T07:24:00Z"
lastmod = "2014-11-13T13:51:00Z"
weight = 37824
keywords = [ "firewall", "security", "teenager" ]
aliases = [ "/questions/37824" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Hiding in plain sight... what my teenager is doing?](/questions/37824/hiding-in-plain-sight-what-my-teenager-is-doing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37824-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37824-score" class="post-score" title="current number of votes">0</div><span id="post-37824-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My teenager has discovered a way to circumvent my netgear wndr4500 router logging. Any ideas on how to shine a light on these activities?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-teenager" rel="tag" title="see questions tagged &#39;teenager&#39;">teenager</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '14, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/ccf1cc4efe5830fc523faa9d842077f5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TonyBromo&#39;s gravatar image" /><p><span>TonyBromo</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TonyBromo has no accepted answers">0%</span></p></div></div><div id="comments-container-37824" class="comments-container"><span id="37835"></span><div id="comment-37835" class="comment"><div id="post-37835-score" class="comment-score">1</div><div class="comment-text"><blockquote><p>Any ideas on how to shine a light on these activities?</p></blockquote><p>maybe ask your teenager for help? Apparently he/she is pretty smart ;-)</p></div><div id="comment-37835-info" class="comment-info"><span class="comment-age">(13 Nov '14, 12:51)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="37840"></span><div id="comment-37840" class="comment"><div id="post-37840-score" class="comment-score"></div><div class="comment-text"><p>Kurt: He is very smart... Which is why I am not taking anything for granted! :-)</p></div><div id="comment-37840-info" class="comment-info"><span class="comment-age">(13 Nov '14, 13:40)</span> <span class="comment-user userinfo">TonyBromo</span></div></div></div><div id="comment-tools-37824" class="comment-tools"></div><div class="clear"></div><div id="comment-37824-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="37831"></span>

<div id="answer-container-37831" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37831-score" class="post-score" title="current number of votes">0</div><span id="post-37831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What exactly do you mean when you say 'Circumvent'? Did (s)he just disable logging?</p><p>It sounds like you can log into your router and find your way around pretty well, so I'd say go take a look at logging and see if 1) it is enabled, 2) If there is a rule or something set up that would allow your kid's computer to get around it.</p><p>If neither of those looks to be the case, you could just wait until a time you know he is using the network and throw up wireshark and take a look at it. Filter on his MAC address.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '14, 10:37</strong></p><img src="https://secure.gravatar.com/avatar/866323e083aa7a657fe6f57636ead128?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jibbs9&#39;s gravatar image" /><p><span>Jibbs9</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jibbs9 has no accepted answers">0%</span></p></div></div><div id="comments-container-37831" class="comments-container"><span id="37839"></span><div id="comment-37839" class="comment"><div id="post-37839-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jibbs9, logging is enabled on the router, it's just his activity appears to be missing from the logs... He does not have the password to the router and it stays locked in my office. What seems weird is that his activity was there just a few days ago, plus I have not made any router changes. He and I have talked, he doesn't seem to be using a VPN client to get around the home firewall/logging. I use my work PC on my home network (which also uses a VPN client) and I don't see any activity for that unit in the logs - which led me to the hypothesis that he was possibly using a VPN (he uses to get around the school firewall). Tonight I am going to try to reset my router to its defaults and review this for a few days.</p></div><div id="comment-37839-info" class="comment-info"><span class="comment-age">(13 Nov '14, 13:38)</span> <span class="comment-user userinfo">TonyBromo</span></div></div><span id="37842"></span><div id="comment-37842" class="comment"><div id="post-37842-score" class="comment-score"></div><div class="comment-text"><p>Did you check the client connection history on your router? (i.e. the list of everything that has been connected to your network recently). Is he connected to it and you just arent seeing the traffic? If thats the case, then I'd re-ask him about the VPN. If not, do you live in close proximity to any neighbors? Could he be using someone else's wifi?</p></div><div id="comment-37842-info" class="comment-info"><span class="comment-age">(13 Nov '14, 13:51)</span> <span class="comment-user userinfo">Jibbs9</span></div></div></div><div id="comment-tools-37831" class="comment-tools"></div><div class="clear"></div><div id="comment-37831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="37841"></span>

<div id="answer-container-37841" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37841-score" class="post-score" title="current number of votes">0</div><span id="post-37841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Kurt: He is very smart... Which is why I am not taking anything for granted! :-)</p></blockquote><p>Maybe he is "recycling" (using) <strong>your</strong> IP address and/or mac address (you can change that easily while running Linux) and you are simply not recognizing his traffic in the logs ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '14, 13:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-37841" class="comments-container"></div><div id="comment-tools-37841" class="comment-tools"></div><div class="clear"></div><div id="comment-37841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

