+++
type = "question"
title = "Change Selected Text"
description = '''Hi all,  I am using Wireshark 1.12.5 for Mac and i have this weird issue i can not resolve. The selected protocol are light green with white text. I can&#x27;t seem to change that to a darker color so i can see. There was a question regarding this a couple of years back but theres not much else as of lat...'''
date = "2015-05-15T21:50:00Z"
lastmod = "2015-05-18T02:25:00Z"
weight = 42432
keywords = [ "color" ]
aliases = [ "/questions/42432" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Change Selected Text](/questions/42432/change-selected-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42432-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42432-score" class="post-score" title="current number of votes">0</div><span id="post-42432-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I am using Wireshark 1.12.5 for Mac and i have this weird issue i can not resolve. The selected protocol are light green with white text. I can't seem to change that to a darker color so i can see. There was a question regarding this a couple of years back but theres not much else as of late. Am i the only one that has ran across this "bug" perhaps. Googling yielded unsuccessful results. Please assist. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '15, 21:50</strong></p><img src="https://secure.gravatar.com/avatar/d16bb6919bece461a900013c5946be53?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hipusmc&#39;s gravatar image" /><p><span>hipusmc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hipusmc has no accepted answers">0%</span></p></div></div><div id="comments-container-42432" class="comments-container"><span id="42450"></span><div id="comment-42450" class="comment"><div id="post-42450-score" class="comment-score"></div><div class="comment-text"><p>Are you using the GTK version? If yes, you have to change the theme settings as mentioned in the other question. Search for gtkrc.</p></div><div id="comment-42450-info" class="comment-info"><span class="comment-age">(17 May '15, 02:50)</span> <span class="comment-user userinfo">Roland</span></div></div><span id="42454"></span><div id="comment-42454" class="comment"><div id="post-42454-score" class="comment-score"></div><div class="comment-text"><p>A screenshot might help....</p></div><div id="comment-42454-info" class="comment-info"><span class="comment-age">(17 May '15, 06:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="42488"></span><div id="comment-42488" class="comment"><div id="post-42488-score" class="comment-score"></div><div class="comment-text"><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-05-18_at_4.20.13_AM_small_WXc1LRL.png" alt="alt text" /></p></div><div id="comment-42488-info" class="comment-info"><span class="comment-age">(18 May '15, 02:22)</span> <span class="comment-user userinfo">hipusmc</span></div></div><span id="42489"></span><div id="comment-42489" class="comment"><div id="post-42489-score" class="comment-score"></div><div class="comment-text"><p>this makes reading anything i click on unbearably difficult as the combo light green background/ white text won't cut it.</p><p>Also, i looked into the Gtk version and am totally lost. I was wondering if there was a guide out there to assist me. I mean, all i want is just to change the color of the selected text. Wish they had that options. Thanks for your assistance.</p></div><div id="comment-42489-info" class="comment-info"><span class="comment-age">(18 May '15, 02:25)</span> <span class="comment-user userinfo">hipusmc</span></div></div></div><div id="comment-tools-42432" class="comment-tools"></div><div class="clear"></div><div id="comment-42432-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

