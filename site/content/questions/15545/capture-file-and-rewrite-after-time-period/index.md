+++
type = "question"
title = "Capture file and rewrite after time period."
description = '''Is there a setting to setup a cature file and log for a select period of time and rewrite file after time expires?'''
date = "2012-11-05T08:50:00Z"
lastmod = "2012-11-05T10:40:00Z"
weight = 15545
keywords = [ "capture", "period", "file", "time" ]
aliases = [ "/questions/15545" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture file and rewrite after time period.](/questions/15545/capture-file-and-rewrite-after-time-period)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15545-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15545-score" class="post-score" title="current number of votes">0</div><span id="post-15545-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a setting to setup a cature file and log for a select period of time and rewrite file after time expires?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-period" rel="tag" title="see questions tagged &#39;period&#39;">period</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '12, 08:50</strong></p><img src="https://secure.gravatar.com/avatar/0735507dc6b1d1d626aa80a02bf9167a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hijazisj&#39;s gravatar image" /><p><span>hijazisj</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hijazisj has no accepted answers">0%</span></p></div></div><div id="comments-container-15545" class="comments-container"></div><div id="comment-tools-15545" class="comment-tools"></div><div class="clear"></div><div id="comment-15545-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15547"></span>

<div id="answer-container-15547" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15547-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15547-score" class="post-score" title="current number of votes">0</div><span id="post-15547-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>yes, go to the Capture Options dialog in the GUI.</p><blockquote><p><code>Capture -&gt; Options</code><br />
</p></blockquote><p>Then choose the option <strong>use multiple files</strong>. Select whatever condition is best for you.</p><ul><li>next file every x megabytes</li><li>next file every x minutes</li><li>...</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Nov '12, 10:40</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-15547" class="comments-container"></div><div id="comment-tools-15547" class="comment-tools"></div><div class="clear"></div><div id="comment-15547-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

