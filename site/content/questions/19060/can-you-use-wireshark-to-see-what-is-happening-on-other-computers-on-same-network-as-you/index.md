+++
type = "question"
title = "Can you use wireshark to see what is happening on other computers on same network as you?"
description = '''Hi I am new to wireshark and i am doing a computer course at college. I am very interested in learning wireshark however i am just wondering if this software can be used to see what is happening on other computers on the same network as you? I have heard some rumours that you need to be on a wired n...'''
date = "2013-03-01T11:33:00Z"
lastmod = "2013-03-02T16:55:00Z"
weight = 19060
keywords = [ "wireshark" ]
aliases = [ "/questions/19060" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can you use wireshark to see what is happening on other computers on same network as you?](/questions/19060/can-you-use-wireshark-to-see-what-is-happening-on-other-computers-on-same-network-as-you)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19060-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19060-score" class="post-score" title="current number of votes">0</div><span id="post-19060-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am new to wireshark and i am doing a computer course at college. I am very interested in learning wireshark however i am just wondering if this software can be used to see what is happening on other computers on the same network as you? I have heard some rumours that you need to be on a wired netowrk to do so.</p><p>Also i have no interest in using this software to 'spy' on people i just want to expand my knowledge of computers and networks.</p><p>Thank You</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '13, 11:33</strong></p><img src="https://secure.gravatar.com/avatar/2ae56c2af0917a63fbc625bdedcbe21d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="danbyization&#39;s gravatar image" /><p><span>danbyization</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="danbyization has no accepted answers">0%</span></p></div></div><div id="comments-container-19060" class="comments-container"></div><div id="comment-tools-19060" class="comment-tools"></div><div class="clear"></div><div id="comment-19060-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19062"></span>

<div id="answer-container-19062" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19062-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19062-score" class="post-score" title="current number of votes">0</div><span id="post-19062-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>One way of doing it is with ARP Poisoning. Explore Cain and able tool from oxid.it to poison the network you are residing. With that you can act as a middle man for every packet traversing from a host in your network to your default gateway. Be careful in using this(you might use it at your home rather than at your college where privacy is non negotiable).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Mar '13, 11:46</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-19062" class="comments-container"><span id="19090"></span><div id="comment-19090" class="comment"><div id="post-19090-score" class="comment-score"></div><div class="comment-text"><p>Oh yeah i will definitely only be doing that kind of stuff at my home. So what can you do with wireshark then if you cant do that??</p></div><div id="comment-19090-info" class="comment-info"><span class="comment-age">(02 Mar '13, 04:40)</span> <span class="comment-user userinfo">danbyization</span></div></div><span id="19098"></span><div id="comment-19098" class="comment"><div id="post-19098-score" class="comment-score"></div><div class="comment-text"><p>Even though Cain and able tool poisons your network it is the wireshark that displays the intended packets. If you want to capture and sneak in to all those neighboring packets wireshark will help you to do that but it won't poison the network.</p></div><div id="comment-19098-info" class="comment-info"><span class="comment-age">(02 Mar '13, 16:55)</span> <span class="comment-user userinfo">krishnayeddula</span></div></div></div><div id="comment-tools-19062" class="comment-tools"></div><div class="clear"></div><div id="comment-19062-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

