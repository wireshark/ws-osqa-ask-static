+++
type = "question"
title = "No Interfaces Found"
description = '''All, I have installed Wireshark 2.2.6 with WinPcap 4_1_3 and show No Interfaces found. I have turned off my Firewall (Norton 360) and Run it in administrator mode. When Installing WinPcap I did get errors writing to C:&#92;WINDOWS&#92;system32&#92;drivers&#92;npf.sys and then wpcap.dll but fixed the permissions. I ...'''
date = "2017-05-22T14:27:00Z"
lastmod = "2017-05-22T16:32:00Z"
weight = 61560
keywords = [ "interface" ]
aliases = [ "/questions/61560" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No Interfaces Found](/questions/61560/no-interfaces-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61560-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61560-score" class="post-score" title="current number of votes">0</div><span id="post-61560-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>All,</p><p>I have installed Wireshark 2.2.6 with WinPcap 4_1_3 and show No Interfaces found. I have turned off my Firewall (Norton 360) and Run it in administrator mode. When Installing WinPcap I did get errors writing to C:\WINDOWS\system32\drivers\npf.sys and then wpcap.dll but fixed the permissions. I then restarted my computer and still No interfaces found. Help!</p><p>Ray</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '17, 14:27</strong></p><img src="https://secure.gravatar.com/avatar/c8f54b04676d2c00a285380dd4c9fb9b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marvell64&#39;s gravatar image" /><p><span>Marvell64</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marvell64 has no accepted answers">0%</span></p></div></div><div id="comments-container-61560" class="comments-container"></div><div id="comment-tools-61560" class="comment-tools"></div><div class="clear"></div><div id="comment-61560-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61562"></span>

<div id="answer-container-61562" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61562-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61562-score" class="post-score" title="current number of votes">0</div><span id="post-61562-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's possible Norton 360 is causing the issue, even if it is disabled. Have you tried removing it entirely? What OS are you running?</p><p>To fix this you can try the following:</p><ol><li>Download the separate <a href="https://www.winpcap.org/install/">WinPcap installer</a>.</li><li>Uninstall WinPcap (usually from Programs and Features).</li><li>Reboot.</li><li>Right click the installer from step 1, and run as Administrator.</li></ol><p>If this doesn't fix it, remove WinPcap again and try <a href="https://github.com/nmap/npcap/releases">npcap</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '17, 15:07</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61562" class="comments-container"><span id="61565"></span><div id="comment-61565" class="comment"><div id="post-61565-score" class="comment-score"></div><div class="comment-text"><p>I am running Windows 10 Home Edition.</p></div><div id="comment-61565-info" class="comment-info"><span class="comment-age">(22 May '17, 16:32)</span> <span class="comment-user userinfo">Marvell64</span></div></div></div><div id="comment-tools-61562" class="comment-tools"></div><div class="clear"></div><div id="comment-61562-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

