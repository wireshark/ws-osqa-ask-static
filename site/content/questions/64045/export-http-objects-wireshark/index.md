+++
type = "question"
title = "Export http objects wireshark"
description = '''Hi All, Quick question. When I do a file, export objects and HTTP and save I cant read it due to the encoding. Any way I can change this? Thanks.'''
date = "2017-10-20T02:49:00Z"
lastmod = "2017-10-20T08:23:00Z"
weight = 64045
keywords = [ "http" ]
aliases = [ "/questions/64045" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Export http objects wireshark](/questions/64045/export-http-objects-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64045-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64045-score" class="post-score" title="current number of votes">0</div><span id="post-64045-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>Quick question. When I do a file, export objects and HTTP and save I cant read it due to the encoding. Any way I can change this?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Oct '17, 02:49</strong></p><img src="https://secure.gravatar.com/avatar/36f04b4302b26d53b2beed437292e1fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LiamH&#39;s gravatar image" /><p><span>LiamH</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LiamH has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Oct '17, 07:02</strong> </span></p></div></div><div id="comments-container-64045" class="comments-container"><span id="64050"></span><div id="comment-64050" class="comment"><div id="post-64050-score" class="comment-score"></div><div class="comment-text"><p>Encoding? Encoding of what? Is it compressed maybe?</p></div><div id="comment-64050-info" class="comment-info"><span class="comment-age">(20 Oct '17, 08:23)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-64045" class="comment-tools"></div><div class="clear"></div><div id="comment-64045-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

