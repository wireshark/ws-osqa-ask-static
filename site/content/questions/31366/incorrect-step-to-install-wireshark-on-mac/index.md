+++
type = "question"
title = "incorrect step to install wireshark on MAC"
description = '''Here is my step install wireshark on MAC OS x 10.9.2  Install Wireshark, using the installer from http://wireshark.org. Open /Applications/Wireshark.app,  From the picker dialog, click Browse button, we need to choose XQuartz BUT I choose incorrect application. HOW CAN I DO? Please let me know. '''
date = "2014-04-02T00:18:00Z"
lastmod = "2014-04-02T14:07:00Z"
weight = 31366
keywords = [ "osx", "mac", "installation" ]
aliases = [ "/questions/31366" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [incorrect step to install wireshark on MAC](/questions/31366/incorrect-step-to-install-wireshark-on-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31366-score" class="post-score" title="current number of votes">0</div><span id="post-31366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Here is my step install wireshark on MAC OS x 10.9.2</p><ol><li>Install Wireshark, using the installer from <a href="http://wireshark.org">http://wireshark.org</a>.</li><li>Open /Applications/Wireshark.app,</li><li>From the picker dialog, click Browse button, we need to choose XQuartz BUT I choose incorrect application. HOW CAN I DO? Please let me know.</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '14, 00:18</strong></p><img src="https://secure.gravatar.com/avatar/84162459cb46654dd3767c6ca2a4da9f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deknoy&#39;s gravatar image" /><p><span>deknoy</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deknoy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Apr '14, 01:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-31366" class="comments-container"></div><div id="comment-tools-31366" class="comment-tools"></div><div class="clear"></div><div id="comment-31366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31437"></span>

<div id="answer-container-31437" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31437-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31437-score" class="post-score" title="current number of votes">0</div><span id="post-31437-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the answers to similar questions:</p><blockquote><p><a href="http://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx">http://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx</a> <a href="http://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion">http://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '14, 14:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Apr '14, 14:07</strong> </span></p></div></div><div id="comments-container-31437" class="comments-container"></div><div id="comment-tools-31437" class="comment-tools"></div><div class="clear"></div><div id="comment-31437-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

