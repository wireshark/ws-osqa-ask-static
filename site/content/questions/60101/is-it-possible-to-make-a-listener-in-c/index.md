+++
type = "question"
title = "Is it possible to make a listener in C++?"
description = '''Hello! Sorry if this is a dumb question, but is it possible to do things that can be done in a Lua Listener in C++? For example, I have a lua listener that grabs protofield data for each packet and saves them to a CSV file, then opens an external python application.  Is it possible to write a listen...'''
date = "2017-03-15T13:55:00Z"
lastmod = "2017-03-15T13:55:00Z"
weight = 60101
keywords = [ "listener", "lua", "dissector", "c++", "wireshark" ]
aliases = [ "/questions/60101" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to make a listener in C++?](/questions/60101/is-it-possible-to-make-a-listener-in-c)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60101-score" class="post-score" title="current number of votes">0</div><span id="post-60101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! Sorry if this is a dumb question, but is it possible to do things that can be done in a Lua Listener in C++?</p><p>For example, I have a lua listener that grabs protofield data for each packet and saves them to a CSV file, then opens an external python application.</p><p>Is it possible to write a listener in C++? I don't think I can do the above in a dissector since a dissector occurs once per frame.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-listener" rel="tag" title="see questions tagged &#39;listener&#39;">listener</span> <span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-c++" rel="tag" title="see questions tagged &#39;c++&#39;">c++</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '17, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/00cd850e8d2944c2c7dcdc13baf50a81?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Irfan%20Hossain&#39;s gravatar image" /><p><span>Irfan Hossain</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Irfan Hossain has no accepted answers">0%</span></p></div></div><div id="comments-container-60101" class="comments-container"></div><div id="comment-tools-60101" class="comment-tools"></div><div class="clear"></div><div id="comment-60101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

