+++
type = "question"
title = "Follow UDP Stream in Analyze"
description = '''I captured 700000 data packets and it took at least an hour for Follow UDP Stream to finish. Did the time look normal?'''
date = "2010-11-17T09:29:00Z"
lastmod = "2011-11-11T09:48:00Z"
weight = 987
keywords = [ "udp" ]
aliases = [ "/questions/987" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Follow UDP Stream in Analyze](/questions/987/follow-udp-stream-in-analyze)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-987-score" class="post-score" title="current number of votes">0</div><span id="post-987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured 700000 data packets and it took at least an hour for Follow UDP Stream to finish. Did the time look normal?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '10, 09:29</strong></p><img src="https://secure.gravatar.com/avatar/7d5eb862ae18cbe55648ea80f8cfed19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SMCL3&#39;s gravatar image" /><p><span>SMCL3</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SMCL3 has no accepted answers">0%</span></p></div></div><div id="comments-container-987" class="comments-container"><span id="7383"></span><div id="comment-7383" class="comment"><div id="post-7383-score" class="comment-score"></div><div class="comment-text"><p>I have a wireshark trace with some SIP messages in it. When I click on a certain message and analyze it using "Follow UDP Stream" I see all of the expected messages however I don't see all of them in actual Wireshark trace with time stamps. No active filters either. Any idea how I can expose messages in trace that are hiding ?</p></div><div id="comment-7383-info" class="comment-info"><span class="comment-age">(11 Nov '11, 09:48)</span> <span class="comment-user userinfo">Atif</span></div></div></div><div id="comment-tools-987" class="comment-tools"></div><div class="clear"></div><div id="comment-987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1033"></span>

<div id="answer-container-1033" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1033-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1033-score" class="post-score" title="current number of votes">0</div><span id="post-1033-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Whether an hour is normal all depends on the system that was used for the analysis. There are no statistics on what a particular system can or cannot do and how much time it will take.</p><p>However, 700000 packets is a lot to reassemble, so I would not be surprised by such a long time for Follow UDP Stream to finish.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '10, 03:21</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-1033" class="comments-container"></div><div id="comment-tools-1033" class="comment-tools"></div><div class="clear"></div><div id="comment-1033-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

