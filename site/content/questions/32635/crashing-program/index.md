+++
type = "question"
title = "Crashing program"
description = '''Hello  Please excuse my ignorance I&#x27;m pretty new to wireshark.. I have a piece of sortware that is crashing and I was wondering is it possible to do a packet capture to see why this may be happening? Thanks Daniel'''
date = "2014-05-08T01:59:00Z"
lastmod = "2014-05-08T06:55:00Z"
weight = 32635
keywords = [ "crashing", "software" ]
aliases = [ "/questions/32635" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Crashing program](/questions/32635/crashing-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32635-score" class="post-score" title="current number of votes">0</div><span id="post-32635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>Please excuse my ignorance I'm pretty new to wireshark..</p><p>I have a piece of sortware that is crashing and I was wondering is it possible to do a packet capture to see why this may be happening?</p><p>Thanks</p><p>Daniel</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-crashing" rel="tag" title="see questions tagged &#39;crashing&#39;">crashing</span> <span class="post-tag tag-link-software" rel="tag" title="see questions tagged &#39;software&#39;">software</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '14, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/4bd330770f829c93ef8b96d9cc3e7bb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Binman%20Dan&#39;s gravatar image" /><p><span>Binman Dan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Binman Dan has no accepted answers">0%</span></p></div></div><div id="comments-container-32635" class="comments-container"></div><div id="comment-tools-32635" class="comment-tools"></div><div class="clear"></div><div id="comment-32635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32640"></span>

<div id="answer-container-32640" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32640-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32640-score" class="post-score" title="current number of votes">0</div><span id="post-32640-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That could be a difficult task, and may not get you any results, but it is possible. You can capture on the PC the software is running and then run the application. If you know what ports it is using you can then check if you can see any packets to that port that would explain why the application crashes - if the application uses TCP, look for Reset packets and find out if they're a cause of the crash.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 May '14, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32640" class="comments-container"></div><div id="comment-tools-32640" class="comment-tools"></div><div class="clear"></div><div id="comment-32640-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

