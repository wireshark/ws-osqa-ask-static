+++
type = "question"
title = "is there a way to automate capturing of raw data from packets in wireshark"
description = '''Hi everyone, I am working on a project that requires me to create a plug-in for wireshark that extracts raw data from packets and analyzes them. Hence I wanted to know if there is any way to do that. I tried to look for wireshark api&#x27;s but am not able to get much from it. What I basically need to do...'''
date = "2016-05-25T08:11:00Z"
lastmod = "2016-05-25T10:05:00Z"
weight = 52917
keywords = [ "parse", "raw_packet_capture", "automate", "wiresharkapi", "automation" ]
aliases = [ "/questions/52917" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [is there a way to automate capturing of raw data from packets in wireshark](/questions/52917/is-there-a-way-to-automate-capturing-of-raw-data-from-packets-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52917-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52917-score" class="post-score" title="current number of votes">0</div><span id="post-52917-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone, I am working on a project that requires me to create a plug-in for wireshark that extracts raw data from packets and analyzes them. Hence I wanted to know if there is any way to do that. I tried to look for wireshark api's but am not able to get much from it. What I basically need to do is to scan through the raw data of each packet and find certain information to check whether the packet is of interest or not.</p><p>Thanks in advance,</p><p>Regards, Shobhit.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-parse" rel="tag" title="see questions tagged &#39;parse&#39;">parse</span> <span class="post-tag tag-link-raw_packet_capture" rel="tag" title="see questions tagged &#39;raw_packet_capture&#39;">raw_packet_capture</span> <span class="post-tag tag-link-automate" rel="tag" title="see questions tagged &#39;automate&#39;">automate</span> <span class="post-tag tag-link-wiresharkapi" rel="tag" title="see questions tagged &#39;wiresharkapi&#39;">wiresharkapi</span> <span class="post-tag tag-link-automation" rel="tag" title="see questions tagged &#39;automation&#39;">automation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '16, 08:11</strong></p><img src="https://secure.gravatar.com/avatar/6bc4481ab93643cb2e083fc8051868a1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shubhgarg123&#39;s gravatar image" /><p><span>shubhgarg123</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shubhgarg123 has no accepted answers">0%</span></p></div></div><div id="comments-container-52917" class="comments-container"></div><div id="comment-tools-52917" class="comment-tools"></div><div class="clear"></div><div id="comment-52917-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52928"></span>

<div id="answer-container-52928" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52928-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52928-score" class="post-score" title="current number of votes">0</div><span id="post-52928-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, feast your eyes on <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=doc/README.dissector;hb=HEAD">this</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 May '16, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52928" class="comments-container"></div><div id="comment-tools-52928" class="comment-tools"></div><div class="clear"></div><div id="comment-52928-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

