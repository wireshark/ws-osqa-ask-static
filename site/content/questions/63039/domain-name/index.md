+++
type = "question"
title = "Domain Name"
description = '''Hello i&#x27;m a new user in Wireshark. I have some problems about Domain name for destination, i can not see domain name i see only IP address how can i do with it ? Sorry for English writing'''
date = "2017-07-24T01:03:00Z"
lastmod = "2017-07-24T01:20:00Z"
weight = 63039
keywords = [ "ip", "domain", "address" ]
aliases = [ "/questions/63039" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Domain Name](/questions/63039/domain-name)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63039-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63039-score" class="post-score" title="current number of votes">0</div><span id="post-63039-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello i'm a new user in Wireshark. I have some problems about Domain name for destination, i can not see domain name i see only IP address how can i do with it ? Sorry for English writing</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-domain" rel="tag" title="see questions tagged &#39;domain&#39;">domain</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '17, 01:03</strong></p><img src="https://secure.gravatar.com/avatar/4f4ad0afe443c29cea2e036509acb2d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Samann&#39;s gravatar image" /><p><span>Samann</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Samann has no accepted answers">0%</span></p></div></div><div id="comments-container-63039" class="comments-container"></div><div id="comment-tools-63039" class="comment-tools"></div><div class="clear"></div><div id="comment-63039-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63040"></span>

<div id="answer-container-63040" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63040-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63040-score" class="post-score" title="current number of votes">0</div><span id="post-63040-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is called <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChAdvNameResolutionSection.html">IP name resolution</a>, a feature which has to be enabled in the preferences, under Name Resolution. It depends on the Domain Name System (DNS) to be able to provide you the names of the IP addresses, or reads the file /etc/hosts for any name provided there.</p><p>Many addresses on the internet will have names registered in the DNS, for local addresses (on the LAN that is) it depends on the local network setup. Eg. some home routers do provide this, some don't.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jul '17, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-63040" class="comments-container"></div><div id="comment-tools-63040" class="comment-tools"></div><div class="clear"></div><div id="comment-63040-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

