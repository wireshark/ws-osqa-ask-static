+++
type = "question"
title = "No interface available to capture"
description = '''Running Snow Leopard 10.6.7. Installed wireshark. When I try to select an interface from which to capture I get message that no interface is available. Obviously I have an interface or I couldn&#x27;t send this message. Any help appreciated. Thanks'''
date = "2011-04-16T19:19:00Z"
lastmod = "2011-04-17T07:37:00Z"
weight = 3536
keywords = [ "interface", "no" ]
aliases = [ "/questions/3536" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No interface available to capture](/questions/3536/no-interface-available-to-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3536-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3536-score" class="post-score" title="current number of votes">0</div><span id="post-3536-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Running Snow Leopard 10.6.7. Installed wireshark. When I try to select an interface from which to capture I get message that no interface is available.</p><p>Obviously I have an interface or I couldn't send this message.</p><p>Any help appreciated.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-no" rel="tag" title="see questions tagged &#39;no&#39;">no</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '11, 19:19</strong></p><img src="https://secure.gravatar.com/avatar/d591d567375cf48b10b59b80908a8fff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="snifferpro&#39;s gravatar image" /><p><span>snifferpro</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="snifferpro has no accepted answers">0%</span></p></div></div><div id="comments-container-3536" class="comments-container"></div><div id="comment-tools-3536" class="comment-tools"></div><div class="clear"></div><div id="comment-3536-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3537"></span>

<div id="answer-container-3537" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3537-score" class="post-score" title="current number of votes">1</div><span id="post-3537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here are some links to information that should hopefully help you out:</p><ul><li><a href="http://langui.sh/2010/01/31/no-interfaces-available-in-wireshark-mac-os-x/">http://langui.sh/2010/01/31/no-interfaces-available-in-wireshark-mac-os-x/</a></li><li><a href="http://ask.wireshark.org/questions/717/cant-run-the-script-chmodbpf">http://ask.wireshark.org/questions/717/cant-run-the-script-chmodbpf</a></li><li><a href="http://www.wireshark.org/faq.html#q9.1">http://www.wireshark.org/faq.html#q9.1</a></li><li><a href="http://article.gmane.org/gmane.network.wireshark.devel/20997">http://article.gmane.org/gmane.network.wireshark.devel/20997</a></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '11, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Apr '11, 08:05</strong> </span></p></div></div><div id="comments-container-3537" class="comments-container"></div><div id="comment-tools-3537" class="comment-tools"></div><div class="clear"></div><div id="comment-3537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

