+++
type = "question"
title = "I do not see the network card"
description = '''Hello I have a Mac with Snow Leopard 10.6.8 I installed version Version 1.8.5 (SVN Rev 47350 from /trunk-1.8) with Wireskark and X11 but when I check the network sceda I do not see ip mac has a network address fixed 192.168.1.20 someone knows help me before it worked because I already used now do no...'''
date = "2014-08-13T07:42:00Z"
lastmod = "2014-08-13T07:42:00Z"
weight = 35458
keywords = [ "1104" ]
aliases = [ "/questions/35458" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I do not see the network card](/questions/35458/i-do-not-see-the-network-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35458-score" class="post-score" title="current number of votes">0</div><span id="post-35458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I have a Mac with Snow Leopard 10.6.8 I installed version Version 1.8.5 (SVN Rev 47350 from /trunk-1.8) with Wireskark and X11 but when I check the network sceda I do not see ip mac has a network address fixed 192.168.1.20 someone knows help me before it worked because I already used now do not understand why I do not see the network card mac is connected via network cable to the router Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1104" rel="tag" title="see questions tagged &#39;1104&#39;">1104</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '14, 07:42</strong></p><img src="https://secure.gravatar.com/avatar/29a4aef7cb165a13b87fa04cbe8d32c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Massimo&#39;s gravatar image" /><p><span>Massimo</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Massimo has no accepted answers">0%</span></p></div></div><div id="comments-container-35458" class="comments-container"></div><div id="comment-tools-35458" class="comment-tools"></div><div class="clear"></div><div id="comment-35458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

