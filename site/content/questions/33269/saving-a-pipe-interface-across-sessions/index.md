+++
type = "question"
title = "Saving a pipe interface across sessions"
description = '''Is there a way to save a named pipe capture interface so that it is always in the interface list? I.E I want pipes added through Manage Interfaces -&amp;gt; New to be there the next time I start Wireshark.'''
date = "2014-06-02T04:51:00Z"
lastmod = "2014-06-02T13:39:00Z"
weight = 33269
keywords = [ "pipe", "interface", "configuration" ]
aliases = [ "/questions/33269" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Saving a pipe interface across sessions](/questions/33269/saving-a-pipe-interface-across-sessions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33269-score" class="post-score" title="current number of votes">0</div><span id="post-33269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to save a named pipe capture interface so that it is always in the interface list?</p><p>I.E I want pipes added through Manage Interfaces -&gt; New to be there the next time I start Wireshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '14, 04:51</strong></p><img src="https://secure.gravatar.com/avatar/d3a401b158e956a431d34c5e71109063?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oyv&#39;s gravatar image" /><p><span>oyv</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oyv has no accepted answers">0%</span></p></div></div><div id="comments-container-33269" class="comments-container"><span id="33317"></span><div id="comment-33317" class="comment"><div id="post-33317-score" class="comment-score"></div><div class="comment-text"><p>what is your</p><ul><li>OS and OS version</li><li>Wireshark version</li></ul></div><div id="comment-33317-info" class="comment-info"><span class="comment-age">(02 Jun '14, 13:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="33318"></span><div id="comment-33318" class="comment"><div id="post-33318-score" class="comment-score"></div><div class="comment-text"><p>I would like an answer for 1.10 and 1.11, all OSes.</p></div><div id="comment-33318-info" class="comment-info"><span class="comment-age">(02 Jun '14, 13:39)</span> <span class="comment-user userinfo">oyv</span></div></div></div><div id="comment-tools-33269" class="comment-tools"></div><div class="clear"></div><div id="comment-33269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

