+++
type = "question"
title = "&quot;print to file&quot; missing in the 2.0.0 for macosx"
description = '''Folks  In the latest 2.0.0 for macosx print to file is missing  is thee another way to print a record to a test file ? Alain '''
date = "2015-11-23T11:30:00Z"
lastmod = "2015-11-23T14:52:00Z"
weight = 47883
keywords = [ "print", "macosx", "2.0.0" ]
aliases = [ "/questions/47883" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["print to file" missing in the 2.0.0 for macosx](/questions/47883/print-to-file-missing-in-the-200-for-macosx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47883-score" class="post-score" title="current number of votes">0</div><span id="post-47883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Folks</p><p>In the latest 2.0.0 for macosx print to file is missing is thee another way to print a record to a test file ?</p><p>Alain</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-11-23_at_20.17.37.png" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-print" rel="tag" title="see questions tagged &#39;print&#39;">print</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-2.0.0" rel="tag" title="see questions tagged &#39;2.0.0&#39;">2.0.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '15, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/2ed49214838ea8e7f0fb4a28169b9039?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alanssie&#39;s gravatar image" /><p><span>alanssie</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alanssie has no accepted answers">0%</span></p></img></div></div><div id="comments-container-47883" class="comments-container"></div><div id="comment-tools-47883" class="comment-tools"></div><div class="clear"></div><div id="comment-47883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47884"></span>

<div id="answer-container-47884" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47884-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47884-score" class="post-score" title="current number of votes">0</div><span id="post-47884-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>okay found it, it's under the Export Packet Dissections menu item</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-11-23_at_20.46.25.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Nov '15, 11:48</strong></p><img src="https://secure.gravatar.com/avatar/2ed49214838ea8e7f0fb4a28169b9039?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alanssie&#39;s gravatar image" /><p><span>alanssie</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alanssie has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Nov '15, 14:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-47884" class="comments-container"><span id="47901"></span><div id="comment-47901" class="comment"><div id="post-47901-score" class="comment-score"></div><div class="comment-text"><p>It's been there under "Export Packet Dissections" since at least 1.12.0.</p></div><div id="comment-47901-info" class="comment-info"><span class="comment-age">(23 Nov '15, 14:52)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-47884" class="comment-tools"></div><div class="clear"></div><div id="comment-47884-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

