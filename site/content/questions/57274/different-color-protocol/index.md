+++
type = "question"
title = "Different Color Protocol"
description = '''I&#x27;m completing an assignment and am looking for some help.  the question is &quot;Are there any protocols that appear with more than one color? Why or why not? &quot; I obviously have different color protocols but am unsure what a protocol with more than one color would look like. '''
date = "2016-11-10T12:34:00Z"
lastmod = "2016-11-10T13:14:00Z"
weight = 57274
keywords = [ "color-rules" ]
aliases = [ "/questions/57274" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Different Color Protocol](/questions/57274/different-color-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57274-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57274-score" class="post-score" title="current number of votes">0</div><span id="post-57274-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm completing an assignment and am looking for some help. the question is "Are there any protocols that appear with more than one color? Why or why not? "</p><p>I obviously have different color protocols but am unsure what a protocol with more than one color would look like.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color-rules" rel="tag" title="see questions tagged &#39;color-rules&#39;">color-rules</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '16, 12:34</strong></p><img src="https://secure.gravatar.com/avatar/6b5ceb1f6a9938a7551e359fc01ab20f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="usbornbball&#39;s gravatar image" /><p><span>usbornbball</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="usbornbball has no accepted answers">0%</span></p></div></div><div id="comments-container-57274" class="comments-container"></div><div id="comment-tools-57274" class="comment-tools"></div><div class="clear"></div><div id="comment-57274-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57277"></span>

<div id="answer-container-57277" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57277-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57277-score" class="post-score" title="current number of votes">0</div><span id="post-57277-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, you can have colouring rules which match on different fields of the same protocol. So depending on those rules you can have multiple colours for the same protocol.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '16, 13:14</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-57277" class="comments-container"></div><div id="comment-tools-57277" class="comment-tools"></div><div class="clear"></div><div id="comment-57277-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

