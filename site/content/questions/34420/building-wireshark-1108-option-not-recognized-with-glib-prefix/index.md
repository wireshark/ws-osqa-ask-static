+++
type = "question"
title = "Building wireshark 1.10.8 - option not recognized --with-glib-prefix"
description = '''./configure complains that --with-glib-prefix=&amp;lt;path&amp;gt; and --with-glib=&amp;lt;path&amp;gt; are unrecognized options. How do I build with a glib in a non-standard location? I am not a root on the machine so I am building locally. Linux-2.6.18-164.el5-x86_64 Red Hat Enterprise Thanks.'''
date = "2014-07-04T12:30:00Z"
lastmod = "2014-07-04T12:30:00Z"
weight = 34420
keywords = [ "configure", "build" ]
aliases = [ "/questions/34420" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Building wireshark 1.10.8 - option not recognized --with-glib-prefix](/questions/34420/building-wireshark-1108-option-not-recognized-with-glib-prefix)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34420-score" class="post-score" title="current number of votes">0</div><span id="post-34420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>./configure complains that --with-glib-prefix=&lt;path&gt; and --with-glib=&lt;path&gt; are unrecognized options. How do I build with a glib in a non-standard location?</p><p>I am not a root on the machine so I am building locally.</p><p>Linux-2.6.18-164.el5-x86_64 Red Hat Enterprise</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-configure" rel="tag" title="see questions tagged &#39;configure&#39;">configure</span> <span class="post-tag tag-link-build" rel="tag" title="see questions tagged &#39;build&#39;">build</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jul '14, 12:30</strong></p><img src="https://secure.gravatar.com/avatar/72478c24289d27d3471f91b0c40f622b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="instalater&#39;s gravatar image" /><p><span>instalater</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="instalater has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jul '14, 12:34</strong> </span></p></div></div><div id="comments-container-34420" class="comments-container"></div><div id="comment-tools-34420" class="comment-tools"></div><div class="clear"></div><div id="comment-34420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

