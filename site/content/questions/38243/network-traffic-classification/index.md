+++
type = "question"
title = "Network Traffic classification"
description = '''Is there any tool or method to separate normal traffic and malicious traffic from pcap? For example : if malicious traffic detected with snort i need to store those packets .only if malicious traffic . if ($pcap==snort.signature(i mean it&#x27;s malicious) then store this packet.sorry for my bad english....'''
date = "2014-11-30T11:16:00Z"
lastmod = "2014-12-02T00:45:00Z"
weight = 38243
keywords = [ "security", "snort" ]
aliases = [ "/questions/38243" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Network Traffic classification](/questions/38243/network-traffic-classification)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38243-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38243-score" class="post-score" title="current number of votes">0</div><span id="post-38243-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any tool or method to separate normal traffic and malicious traffic from pcap? For example : if malicious traffic detected with snort i need to store those packets .only if malicious traffic . if ($pcap==snort.signature(i mean it's malicious) then store this packet.sorry for my bad english. .Thanks for respond.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-snort" rel="tag" title="see questions tagged &#39;snort&#39;">snort</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '14, 11:16</strong></p><img src="https://secure.gravatar.com/avatar/2e4816518cf100b1ed7daa77ddf12d6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chinguun&#39;s gravatar image" /><p><span>Chinguun</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chinguun has no accepted answers">0%</span></p></div></div><div id="comments-container-38243" class="comments-container"></div><div id="comment-tools-38243" class="comment-tools"></div><div class="clear"></div><div id="comment-38243-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38263"></span>

<div id="answer-container-38263" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38263-score" class="post-score" title="current number of votes">1</div><span id="post-38263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're trying to confirm if there's a way to use Snort filters in Wireshark when reading a packet capture file, sorting by one Snort rule or another, I haven't used it myself but there is a plugin to allow for this within Wireshark: <a href="http://www.honeynet.org/node/790">http://www.honeynet.org/node/790</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Dec '14, 16:03</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-38263" class="comments-container"><span id="38275"></span><div id="comment-38275" class="comment"><div id="post-38275-score" class="comment-score"></div><div class="comment-text"><p>thanks for respond.But git link doesnt work .how solve this ?</p></div><div id="comment-38275-info" class="comment-info"><span class="comment-age">(02 Dec '14, 00:45)</span> <span class="comment-user userinfo">Chinguun</span></div></div></div><div id="comment-tools-38263" class="comment-tools"></div><div class="clear"></div><div id="comment-38263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

