+++
type = "question"
title = "wireshark downloading"
description = '''I install wireshark and wincap and it work first couple of times, and now when i go to start it up it gets to configuration of files and never finishes.'''
date = "2014-06-01T18:50:00Z"
lastmod = "2014-06-01T23:21:00Z"
weight = 33252
keywords = [ "android" ]
aliases = [ "/questions/33252" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark downloading](/questions/33252/wireshark-downloading)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33252-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33252-score" class="post-score" title="current number of votes">0</div><span id="post-33252-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I install wireshark and wincap and it work first couple of times, and now when i go to start it up it gets to configuration of files and never finishes.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '14, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/c2559980b1466d0f9cb0c9988180f90c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="giantrobot&#39;s gravatar image" /><p><span>giantrobot</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="giantrobot has no accepted answers">0%</span></p></div></div><div id="comments-container-33252" class="comments-container"><span id="33256"></span><div id="comment-33256" class="comment"><div id="post-33256-score" class="comment-score"></div><div class="comment-text"><p>What is your</p><ul><li>OS and OS Version</li><li>Wireshark Version</li></ul><p>I bet it's Windows 8.1.</p></div><div id="comment-33256-info" class="comment-info"><span class="comment-age">(01 Jun '14, 23:21)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33252" class="comment-tools"></div><div class="clear"></div><div id="comment-33252-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

