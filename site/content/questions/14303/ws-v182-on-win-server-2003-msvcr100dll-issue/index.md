+++
type = "question"
title = "WS V1.8.2 on win server 2003, msvcr100.dll issue!"
description = '''Hi  I have installed Wireshark version 1.8.2 (SVN Rev 44520 from /trunk-1.8) on a new win server 2003. I can open a large (previously captured) packet capture file ~ 600MB, without any trouble. When I select &quot;Telephony&quot; - &quot;VoIP Calls&quot;, I get a progress screen. This stalls at about 60%, and 30 sec la...'''
date = "2012-09-16T06:15:00Z"
lastmod = "2012-09-16T06:15:00Z"
weight = 14303
keywords = [ "2003", "msvcr100.dll" ]
aliases = [ "/questions/14303" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WS V1.8.2 on win server 2003, msvcr100.dll issue!](/questions/14303/ws-v182-on-win-server-2003-msvcr100dll-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14303-score" class="post-score" title="current number of votes">0</div><span id="post-14303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I have installed Wireshark version 1.8.2 (SVN Rev 44520 from /trunk-1.8) on a new win server 2003.</p><p>I can open a large (previously captured) packet capture file ~ 600MB, without any trouble.</p><p>When I select "Telephony" - "VoIP Calls", I get a progress screen. This stalls at about 60%, and 30 sec later wireshark ends.</p><p>Event log shows: Faulting application wireshark.exe, version 1.8.2.44520, faulting module msvcr100.dll, version 10.0.40219.1, fault address 0x0008d6fd.</p><p>I have tried installing the "Visual C++ 2010 x86 Runtime Lib W/SP1" and replacing msvcr100.dll (in the wireshark dir), from version 10.0.40219.1 too 10.0.40219.325.</p><p>Any thoughts?</p><p>Many thanks,</p><p>Simon</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-2003" rel="tag" title="see questions tagged &#39;2003&#39;">2003</span> <span class="post-tag tag-link-msvcr100.dll" rel="tag" title="see questions tagged &#39;msvcr100.dll&#39;">msvcr100.dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '12, 06:15</strong></p><img src="https://secure.gravatar.com/avatar/74eba69d2bd1ffdc7bf37cb4d68a5545?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nolan&#39;s gravatar image" /><p><span>nolan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nolan has no accepted answers">0%</span></p></div></div><div id="comments-container-14303" class="comments-container"></div><div id="comment-tools-14303" class="comment-tools"></div><div class="clear"></div><div id="comment-14303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

