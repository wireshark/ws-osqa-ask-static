+++
type = "question"
title = "wireshark not capture  http POST request"
description = '''hello When I run wireshark on some public wireless (without password) , I can see GET requests from many users that surfing the web, and no POST requests. when I filter &quot;http &amp;amp;&amp;amp; tcp&quot; I can see all the websites that the users visiting and can read the html code of their visitis. (there are a ...'''
date = "2015-11-24T12:39:00Z"
lastmod = "2015-11-24T12:39:00Z"
weight = 47939
keywords = [ "kali", "post", "linux" ]
aliases = [ "/questions/47939" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark not capture http POST request](/questions/47939/wireshark-not-capture-http-post-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47939-score" class="post-score" title="current number of votes">0</div><span id="post-47939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello</p><p>When I run wireshark on some public wireless (without password) , I can see GET requests from many users that surfing the web, and no POST requests. when I filter "http &amp;&amp; tcp" I can see all the websites that the users visiting and can read the html code of their visitis. (there are a lot users in this wifi) .when I filter http.request.method == "POST", I get 1 - 4 lame packets that belong to some XML and not even use web. it doesnt make sense. like I said, a lot of users use this network. and there are a lot of packets when I filter http &amp;&amp; tcp , so the users are use the network and surfing.</p><p>When I run wireshark on another wifi (with password - also , lot of users are conntented to this wifi) , and I filter http &amp;&amp; tcp, I can see only my computer's traffic. but when wireshark shows all the packets, I can see that there are a lot of another devices uses the network with another protocols.</p><p>I use kali linux. I turned on monitor mode and start airmon-ng on the wireless interface.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kali" rel="tag" title="see questions tagged &#39;kali&#39;">kali</span> <span class="post-tag tag-link-post" rel="tag" title="see questions tagged &#39;post&#39;">post</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '15, 12:39</strong></p><img src="https://secure.gravatar.com/avatar/d7b04968867fe95899dd4c13ed1faf74?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Reyes2777&#39;s gravatar image" /><p><span>Reyes2777</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Reyes2777 has no accepted answers">0%</span></p></div></div><div id="comments-container-47939" class="comments-container"></div><div id="comment-tools-47939" class="comment-tools"></div><div class="clear"></div><div id="comment-47939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

