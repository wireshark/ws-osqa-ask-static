+++
type = "question"
title = "What is using the most bandwidth on my nic?"
description = '''Hello, I have noticed a network card on our file server is using a lot of bandwidth, I have captured the data for 2 minutes, but how can I tell what source IP is downloading uploading the most data? Many thanks'''
date = "2010-09-17T05:16:00Z"
lastmod = "2010-09-17T05:32:00Z"
weight = 172
keywords = [ "bandwidth" ]
aliases = [ "/questions/172" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [What is using the most bandwidth on my nic?](/questions/172/what-is-using-the-most-bandwidth-on-my-nic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-172-score" class="post-score" title="current number of votes">0</div><span id="post-172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have noticed a network card on our file server is using a lot of bandwidth, I have captured the data for 2 minutes, but how can I tell what source IP is downloading uploading the most data?</p><p>Many thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Sep '10, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/1e995183e5891465732f36982ead7799?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gonzouk&#39;s gravatar image" /><p><span>Gonzouk</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gonzouk has 2 accepted answers">100%</span></p></div></div><div id="comments-container-172" class="comments-container"></div><div id="comment-tools-172" class="comment-tools"></div><div class="clear"></div><div id="comment-172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="175"></span>

<div id="answer-container-175" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-175-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-175-score" class="post-score" title="current number of votes">-2</div><span id="post-175-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Gonzouk has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Just perfect, thanks again!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Sep '10, 05:27</strong></p><img src="https://secure.gravatar.com/avatar/1e995183e5891465732f36982ead7799?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gonzouk&#39;s gravatar image" /><p><span>Gonzouk</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gonzouk has 2 accepted answers">100%</span></p></div></div><div id="comments-container-175" class="comments-container"><span id="177"></span><div id="comment-177" class="comment"><div id="post-177-score" class="comment-score"></div><div class="comment-text"><p>The idea of the Q&amp;A site is to use the "accept" for the answer that actually does have the answer to the question. That way, people are able to find the answer quickly. You can use the "add new comment" to uhmm... add a comment like you put in your "answer" ;-)</p></div><div id="comment-177-info" class="comment-info"><span class="comment-age">(17 Sep '10, 05:32)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-175" class="comment-tools"></div><div class="clear"></div><div id="comment-175-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="174"></span>

<div id="answer-container-174" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-174-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-174-score" class="post-score" title="current number of votes">1</div><span id="post-174-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use the "Statistics -&gt; conversations" option and then click on the "IP" tab. Then click on the "Total Bytes" column to sort the list.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Sep '10, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-174" class="comments-container"></div><div id="comment-tools-174" class="comment-tools"></div><div class="clear"></div><div id="comment-174-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

