+++
type = "question"
title = "Good source of publicly-available packet captures of 3GPP call flows?"
description = '''I figured I&#x27;d make a shout-out request here since I haven&#x27;t been able to find a good source of packet capture files for 3GPP call flows, particularly for EPS. I want them as a supplement to a video series I&#x27;m putting together on the EPC, and since I plan to make the series public domain I&#x27;m restrict...'''
date = "2013-09-15T08:59:00Z"
lastmod = "2013-09-15T08:59:00Z"
weight = 24717
keywords = [ "3gpp", "pcap", "eps", "lte", "epc" ]
aliases = [ "/questions/24717" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Good source of publicly-available packet captures of 3GPP call flows?](/questions/24717/good-source-of-publicly-available-packet-captures-of-3gpp-call-flows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24717-score" class="post-score" title="current number of votes">0</div><span id="post-24717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I figured I'd make a shout-out request here since I haven't been able to find a good source of packet capture files for 3GPP call flows, particularly for EPS. I want them as a supplement to a video series I'm putting together on the EPC, and since I plan to make the series public domain I'm restricted from using all the NDA-protected captures I would normally have access to. Example call flows that would be great to have (which I really, really don't want to packet-craft myself) loosely in order of priority. I would appreciate any of these:</p><ul><li>Normal EPS Attach Procedure (not picky on any optional components of this call flow). Unciphered NAS if at all possible.</li><li>S11 or S5 interface capture, preferably showing at least one example of a non-128 response code.</li><li>An SGs interface trace of an incoming CSFB call. Doesn't need to include the S1AP/NAS component but would be great if it did. I'd also take MO since beggers can't be choosers. :)</li><li>Any Gx or Gy call flow captures, preferably one that illustrates use of MSCC's with a full RSU/GSU/USU flow. Any non-2xxx result code scenario would be good as well.</li><li>Any example of two legs of a Diameter-routed packet.</li><li>An SS7 transaction traced over a Sigtran link, preferably one that demonstrates GT translation (I have a few videos that cover historical voice). Could potentially use direct ISUP call with routing on just DPC as well. Not picky on Sigtran flavour.</li><li>An SMS sent or received over deciphered NAS.</li><li>Any EMM procedures (periodic update, page, etc.). A page triggered by PDN request visible in trace would be great as well.</li><li>Any handover procedure. Full X2 would be great, as would any inter-SGw or inter-MME flow.</li></ul><p>So, that's my wish list. Just keep in mind that they would need to be free to redistribute to the public domain, which is the deal-breaker for all my existing sources of these kinds of captures.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-eps" rel="tag" title="see questions tagged &#39;eps&#39;">eps</span> <span class="post-tag tag-link-lte" rel="tag" title="see questions tagged &#39;lte&#39;">lte</span> <span class="post-tag tag-link-epc" rel="tag" title="see questions tagged &#39;epc&#39;">epc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '13, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '13, 09:07</strong> </span></p></div></div><div id="comments-container-24717" class="comments-container"></div><div id="comment-tools-24717" class="comment-tools"></div><div class="clear"></div><div id="comment-24717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

