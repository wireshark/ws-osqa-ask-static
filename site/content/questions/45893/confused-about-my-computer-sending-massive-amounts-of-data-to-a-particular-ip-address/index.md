+++
type = "question"
title = "Confused about my computer sending massive amounts of data to a particular IP address"
description = '''Hey all im new to networking. After noticing a huge amount of lag from my browser, I decided to check into it. After checking my ping I noticed there is a huge delay. With my confusion building more I decided to install WireShark to see what was going on. Apparently my computer keeps spamming a cert...'''
date = "2015-09-16T15:31:00Z"
lastmod = "2015-09-17T02:19:00Z"
weight = 45893
keywords = [ "help" ]
aliases = [ "/questions/45893" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Confused about my computer sending massive amounts of data to a particular IP address](/questions/45893/confused-about-my-computer-sending-massive-amounts-of-data-to-a-particular-ip-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45893-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45893-score" class="post-score" title="current number of votes">0</div><span id="post-45893-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey all im new to networking. After noticing a huge amount of lag from my browser, I decided to check into it. After checking my ping I noticed there is a huge delay. With my confusion building more I decided to install WireShark to see what was going on. Apparently my computer keeps spamming a certain range of IPs with packets and Im not sure how to stop it.</p><p><a href="http://i.imgur.com/nmGTb2E.jpg">http://i.imgur.com/nmGTb2E.jpg</a></p><p>Is this some sort of DDoS attack? Or am I a part of a botnet DDoSing an IP? It wont stop, I even installed PeerBlock to block the IPs in question but it continues.</p><p>Sorry if something really simple is happening but I'm just not understanding it.</p><p>I installed TCPView to see if the packets would show up but I haven't found anything yet. It almost seems like its coming out of nowhere. Sorry I'm new so if theres a dumb answer please help me figure it out.</p><p>I checked my network and its definitely coming from my computer.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '15, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/5551730d644273c5e25409cc7455c0e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nick1212&#39;s gravatar image" /><p><span>nick1212</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nick1212 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Sep '15, 15:33</strong> </span></p></div></div><div id="comments-container-45893" class="comments-container"><span id="45895"></span><div id="comment-45895" class="comment"><div id="post-45895-score" class="comment-score"></div><div class="comment-text"><p>I restarted and it seemed to fix the problem shortly, but it's happening again. Any ideas? Its clogging up my entire internet and I can't figure out how to stop it.</p></div><div id="comment-45895-info" class="comment-info"><span class="comment-age">(16 Sep '15, 18:53)</span> <span class="comment-user userinfo">nick1212</span></div></div></div><div id="comment-tools-45893" class="comment-tools"></div><div class="clear"></div><div id="comment-45893-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45897"></span>

<div id="answer-container-45897" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45897-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45897-score" class="post-score" title="current number of votes">0</div><span id="post-45897-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You are downloading a large file from 190.93.250.70. Open a browser session to <a href="http://190.93.250.70">http://190.93.250.70</a> and you should find out what/who this web server is/does. Regards Matthias</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '15, 21:46</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div></div><div id="comments-container-45897" class="comments-container"><span id="45898"></span><div id="comment-45898" class="comment"><div id="post-45898-score" class="comment-score"></div><div class="comment-text"><p>Hey thanks for the answer, but when I open the IP in console it redirects me to CloudFlare stating that I can't directly access the website using Direct IP.</p></div><div id="comment-45898-info" class="comment-info"><span class="comment-age">(16 Sep '15, 21:52)</span> <span class="comment-user userinfo">nick1212</span></div></div><span id="45906"></span><div id="comment-45906" class="comment"><div id="post-45906-score" class="comment-score"></div><div class="comment-text"><p>You 'open the IP in console' - what do you mean by this ? What does a host 190.93.250.70 command reolve the address to ?</p></div><div id="comment-45906-info" class="comment-info"><span class="comment-age">(17 Sep '15, 02:19)</span> <span class="comment-user userinfo">mrEEde</span></div></div></div><div id="comment-tools-45897" class="comment-tools"></div><div class="clear"></div><div id="comment-45897-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

