+++
type = "question"
title = "Unknown invokedata blob"
description = '''For packet Unknown INAP (64) invoke command:Unknown(64) is coming. Unknown invokedata blob error. Can a solution be suggested for this'''
date = "2011-10-04T05:36:00Z"
lastmod = "2011-10-04T08:47:00Z"
weight = 6698
keywords = [ "ss7" ]
aliases = [ "/questions/6698" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unknown invokedata blob](/questions/6698/unknown-invokedata-blob)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6698-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6698-score" class="post-score" title="current number of votes">0</div><span id="post-6698-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For packet Unknown INAP (64) invoke command:Unknown(64) is coming. Unknown invokedata blob error. Can a solution be suggested for this</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ss7" rel="tag" title="see questions tagged &#39;ss7&#39;">ss7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '11, 05:36</strong></p><img src="https://secure.gravatar.com/avatar/11643eef8f339d4b22c9d7fe68afd094?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rissac&#39;s gravatar image" /><p><span>rissac</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rissac has no accepted answers">0%</span></p></div></div><div id="comments-container-6698" class="comments-container"></div><div id="comment-tools-6698" class="comment-tools"></div><div class="clear"></div><div id="comment-6698-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6702"></span>

<div id="answer-container-6702" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6702-score" class="post-score" title="current number of votes">0</div><span id="post-6702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>If that's an operation code of 64, it's either a non standard INAP version or a later spec than IN-operationcodes (Q.1248.1:07/2001) which is waht Wireshark supports. If this Opcode exists in a later spec, raise an enhancment bug report. Regards Anders</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Oct '11, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-6702" class="comments-container"><span id="6708"></span><div id="comment-6708" class="comment"><div id="post-6708-score" class="comment-score"></div><div class="comment-text"><p>afterthought: It might not be INAP at all depending on your setup the ssn is used to "distribute" the TCAP messages to the "right" dissector this might be wrong. Does other messages apere to be correctly dissected?</p></div><div id="comment-6708-info" class="comment-info"><span class="comment-age">(04 Oct '11, 08:47)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-6702" class="comment-tools"></div><div class="clear"></div><div id="comment-6702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

