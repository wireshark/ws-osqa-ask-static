+++
type = "question"
title = "offline use of wireshark...???"
description = '''Can anyone tell me the offline use of wireshark. I am using it for networking purpose but its online so need to know about the each n every case of offline where we can use wireshark. Thanx in advance...'''
date = "2013-08-21T03:39:00Z"
lastmod = "2013-08-22T05:52:00Z"
weight = 23903
keywords = [ "wireshark" ]
aliases = [ "/questions/23903" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [offline use of wireshark...???](/questions/23903/offline-use-of-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23903-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23903-score" class="post-score" title="current number of votes">0</div><span id="post-23903-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone tell me the offline use of wireshark. I am using it for networking purpose but its online so need to know about the each n every case of offline where we can use wireshark.</p><p>Thanx in advance...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '13, 03:39</strong></p><img src="https://secure.gravatar.com/avatar/7d1e25c38fcc91966b7b03659242db49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Eagle%20Eye&#39;s gravatar image" /><p><span>Eagle Eye</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Eagle Eye has no accepted answers">0%</span></p></div></div><div id="comments-container-23903" class="comments-container"></div><div id="comment-tools-23903" class="comment-tools"></div><div class="clear"></div><div id="comment-23903-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23906"></span>

<div id="answer-container-23906" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23906-score" class="post-score" title="current number of votes">1</div><span id="post-23906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What do you mean by Online/offline? You can use Wireshark to do live captures or to load capture files that has been taken previously on the capture files you can do more or less the same operations. If yo mean access to the internet that's not needed other than to reach help pages and update the SW.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Aug '13, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-23906" class="comments-container"><span id="23943"></span><div id="comment-23943" class="comment"><div id="post-23943-score" class="comment-score"></div><div class="comment-text"><p>By word 'online' I mean when you configure your wireshark for any particular node/protocol and you capture the output in heavy running traffic. this online has no relation with "INTERNET" :)</p></div><div id="comment-23943-info" class="comment-info"><span class="comment-age">(22 Aug '13, 02:22)</span> <span class="comment-user userinfo">Eagle Eye</span></div></div><span id="23955"></span><div id="comment-23955" class="comment"><div id="post-23955-score" class="comment-score"></div><div class="comment-text"><p>When doing live captures with Wireshark the GUI may have eproblems keeping up with heavy traffic especially if you have "Update list of packets in realtime" and "Automatic scrolling in live capture" preferences set. It may be better to stop the capture before starting to do analyse or use dumpcap or tshark for live captures and use Wireshark to analyse the captured files - offline in your nomenclature I suppose.</p></div><div id="comment-23955-info" class="comment-info"><span class="comment-age">(22 Aug '13, 05:52)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-23906" class="comment-tools"></div><div class="clear"></div><div id="comment-23906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

