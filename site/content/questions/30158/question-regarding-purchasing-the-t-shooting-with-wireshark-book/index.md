+++
type = "question"
title = "Question regarding purchasing the T-shooting with wireshark book.."
description = '''Hello, Today I had attended the WireShark Virtual World Tour, and I could had sworn that I heard Laura Chappell state that if you were to purchase the paperback version of the &#x27;Troubleshooting with Wireshark&#x27; from Amazon, it would include a code to purchase the Kindle version for $3 .... I was hopin...'''
date = "2014-02-24T14:38:00Z"
lastmod = "2014-02-24T14:42:00Z"
weight = 30158
keywords = [ "book" ]
aliases = [ "/questions/30158" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Question regarding purchasing the T-shooting with wireshark book..](/questions/30158/question-regarding-purchasing-the-t-shooting-with-wireshark-book)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30158-score" class="post-score" title="current number of votes">0</div><span id="post-30158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Today I had attended the WireShark Virtual World Tour, and I could had sworn that I heard Laura Chappell state that if you were to purchase the paperback version of the 'Troubleshooting with Wireshark' from Amazon, it would include a code to purchase the Kindle version for $3 .... I was hoping that someone would confirm this.</p><p>thanks, chris mayberry</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-book" rel="tag" title="see questions tagged &#39;book&#39;">book</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '14, 14:38</strong></p><img src="https://secure.gravatar.com/avatar/0e71daaf40cca12cb7bdf974e46eb3a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cltmayberry&#39;s gravatar image" /><p><span>cltmayberry</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cltmayberry has no accepted answers">0%</span></p></div></div><div id="comments-container-30158" class="comments-container"></div><div id="comment-tools-30158" class="comment-tools"></div><div class="clear"></div><div id="comment-30158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30159"></span>

<div id="answer-container-30159" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30159-score" class="post-score" title="current number of votes">0</div><span id="post-30159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>nevermind!... I was able to locate the answer to my own question... its called 'Kindle matchbook'</p><p>for those interested, here is the Amazon terms;</p><p>"If you buy a new print edition of this book (or purchased one in the past), you can buy the Kindle edition for only $2.99 (Save 91%). Print edition purchase must be sold by Amazon."</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '14, 14:42</strong></p><img src="https://secure.gravatar.com/avatar/0e71daaf40cca12cb7bdf974e46eb3a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cltmayberry&#39;s gravatar image" /><p><span>cltmayberry</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cltmayberry has no accepted answers">0%</span></p></div></div><div id="comments-container-30159" class="comments-container"></div><div id="comment-tools-30159" class="comment-tools"></div><div class="clear"></div><div id="comment-30159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

