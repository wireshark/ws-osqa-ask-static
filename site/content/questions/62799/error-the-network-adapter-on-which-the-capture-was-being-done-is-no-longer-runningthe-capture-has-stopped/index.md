+++
type = "question"
title = "Error: The network adapter on which the capture was being done is no longer running;the capture has stopped."
description = '''Error: The network adapter on which the capture was being done is no longer running;the capture has stopped. I was trying to record the wireshark logs while running an application. I got the above error. Can someone tell me why I get this error?'''
date = "2017-07-15T07:12:00Z"
lastmod = "2017-07-15T07:39:00Z"
weight = 62799
keywords = [ "adapter", "network" ]
aliases = [ "/questions/62799" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Error: The network adapter on which the capture was being done is no longer running;the capture has stopped.](/questions/62799/error-the-network-adapter-on-which-the-capture-was-being-done-is-no-longer-runningthe-capture-has-stopped)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62799-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62799-score" class="post-score" title="current number of votes">0</div><span id="post-62799-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Error: The network adapter on which the capture was being done is no longer running;the capture has stopped.</p><p>I was trying to record the wireshark logs while running an application. I got the above error. Can someone tell me why I get this error?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '17, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/4783b7fc55f4d0d9df2d4fd05eff9973?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mia&#39;s gravatar image" /><p><span>mia</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mia has no accepted answers">0%</span></p></div></div><div id="comments-container-62799" class="comments-container"></div><div id="comment-tools-62799" class="comment-tools"></div><div class="clear"></div><div id="comment-62799-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62801"></span>

<div id="answer-container-62801" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62801-score" class="post-score" title="current number of votes">0</div><span id="post-62801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You haven't provided details regarding the operating system and network adapter type involved.</p><p>Most often this happens if you disconnect the network cable to the interface on which you capture, but if it is not your case, the answer to <a href="https://ask.wireshark.org/questions/62466/wireshark-suddenly-stops">another similar question</a> may help you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '17, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-62801" class="comments-container"></div><div id="comment-tools-62801" class="comment-tools"></div><div class="clear"></div><div id="comment-62801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

