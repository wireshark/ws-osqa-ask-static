+++
type = "question"
title = "Intrusion attempts when using WireShark"
description = '''I recently tried WireShark for some days. Later, my antivirus suite (Symantec) reported three blocked intrusion attempts into my system (Windows 7) on exactly those three consecutive days I used the software. They were categorized as &quot;Web Attack: Exploit Kit Variant 2&quot; and all came from some St. Pet...'''
date = "2012-02-27T02:37:00Z"
lastmod = "2012-02-27T04:14:00Z"
weight = 9238
keywords = [ "security", "attack", "intrusion" ]
aliases = [ "/questions/9238" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Intrusion attempts when using WireShark](/questions/9238/intrusion-attempts-when-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9238-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9238-score" class="post-score" title="current number of votes">0</div><span id="post-9238-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently tried WireShark for some days. Later, my antivirus suite (Symantec) reported three blocked intrusion attempts into my system (Windows 7) on exactly those three consecutive days I used the software. They were categorized as "Web Attack: Exploit Kit Variant 2" and all came from some St. Petersburg IP (31.184.192.8). I didn't install any other new software during those days (apart wrom the WinPCap installed during WireShark setup).</p><p>Did anybody else notice something like that? Does WireShark maybe open some port which attracts attacks like that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-attack" rel="tag" title="see questions tagged &#39;attack&#39;">attack</span> <span class="post-tag tag-link-intrusion" rel="tag" title="see questions tagged &#39;intrusion&#39;">intrusion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '12, 02:37</strong></p><img src="https://secure.gravatar.com/avatar/d8670fede444011ddfddf51abfb782b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="volltonfarbe&#39;s gravatar image" /><p><span>volltonfarbe</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="volltonfarbe has no accepted answers">0%</span></p></div></div><div id="comments-container-9238" class="comments-container"></div><div id="comment-tools-9238" class="comment-tools"></div><div class="clear"></div><div id="comment-9238-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9239"></span>

<div id="answer-container-9239" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9239-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9239-score" class="post-score" title="current number of votes">0</div><span id="post-9239-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you use an installer downloaded from <a href="http://www.wireshark.org"></a><a href="http://www.wireshark.org">www.wireshark.org</a>? Then there should not have been any malware included.</p><p>But if you downloaded it from somewhere else, then yes, it could be that someone attached some malware to the installer. Gerald posted about it on the <a href="https://blog.wireshark.org/2011/12/gratuitous-used-car-analogy/">Wireshark Blog</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '12, 03:17</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-9239" class="comments-container"><span id="9241"></span><div id="comment-9241" class="comment"><div id="post-9241-score" class="comment-score"></div><div class="comment-text"><p>I downloaded the file from the the official webpage, which gave me the following download link: <a href="http://wiresharkdownloads.riverbed.com/wireshark/win64/wireshark-win64-1.6.5.exe">http://wiresharkdownloads.riverbed.com/wireshark/win64/wireshark-win64-1.6.5.exe</a> . I checked the files' checksums, and the checksums signature, it does verify.</p><p>I was just wondering if running the software does open some ports that are usually closed, or performs some other action that makes the computer more 'visible' for potential attackers. It might be mere coincidence, of course, but I found it strange that this happened exactly on those days that I used the software.</p></div><div id="comment-9241-info" class="comment-info"><span class="comment-age">(27 Feb '12, 04:14)</span> <span class="comment-user userinfo">volltonfarbe</span></div></div></div><div id="comment-tools-9239" class="comment-tools"></div><div class="clear"></div><div id="comment-9239-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

