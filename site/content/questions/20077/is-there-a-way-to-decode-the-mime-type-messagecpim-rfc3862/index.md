+++
type = "question"
title = "Is there a way to decode the MIME type message/cpim (rfc3862)?"
description = '''If not, how can a development of a wireshark extension be requested/promoted?'''
date = "2013-04-04T04:25:00Z"
lastmod = "2013-04-04T09:26:00Z"
weight = 20077
keywords = [ "cpim" ]
aliases = [ "/questions/20077" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a way to decode the MIME type message/cpim (rfc3862)?](/questions/20077/is-there-a-way-to-decode-the-mime-type-messagecpim-rfc3862)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20077-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20077-score" class="post-score" title="current number of votes">0</div><span id="post-20077-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If not, how can a development of a wireshark extension be requested/promoted?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cpim" rel="tag" title="see questions tagged &#39;cpim&#39;">cpim</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '13, 04:25</strong></p><img src="https://secure.gravatar.com/avatar/1c23b7726ea6f8290ba8c1b783193e1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AndroidAndy&#39;s gravatar image" /><p><span>AndroidAndy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AndroidAndy has no accepted answers">0%</span></p></div></div><div id="comments-container-20077" class="comments-container"></div><div id="comment-tools-20077" class="comment-tools"></div><div class="clear"></div><div id="comment-20077-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20084"></span>

<div id="answer-container-20084" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20084-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20084-score" class="post-score" title="current number of votes">0</div><span id="post-20084-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The best you can do is provide a developer with all the data he or she needs to make and test such a thing. Therefore you can <a href="https://bugs.wireshark.org/bugzilla/enter_bug.cgi?product=Wireshark">file an enhancement bug</a> with this request, references to relevant standards, and most importantly sample capture files. Good sample captures are a must-have in order to do development and tests.</p><p>Then it's a matter of waiting until someone has spare time left to make such a thing. Or maybe you may be able to contract someone to do it for you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '13, 09:26</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-20084" class="comments-container"></div><div id="comment-tools-20084" class="comment-tools"></div><div class="clear"></div><div id="comment-20084-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

