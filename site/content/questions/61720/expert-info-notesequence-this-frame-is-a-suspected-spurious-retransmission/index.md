+++
type = "question"
title = "Expert Info (Note/Sequence): This frame is a (suspected) spurious retransmission"
description = '''Hi all, we have some problem with web navigation, sometimes all Microsoft machines not surfing on some sites. (ever same sites when the problem occur). Can you help me? if you want can i share some logs. thank you very much Maicol'''
date = "2017-05-31T12:59:00Z"
lastmod = "2017-05-31T12:59:00Z"
weight = 61720
keywords = [ "web", "cisco", "navigation", "microsoft", "huawei" ]
aliases = [ "/questions/61720" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Expert Info (Note/Sequence): This frame is a (suspected) spurious retransmission](/questions/61720/expert-info-notesequence-this-frame-is-a-suspected-spurious-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61720-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61720-score" class="post-score" title="current number of votes">0</div><span id="post-61720-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>we have some problem with web navigation, sometimes all Microsoft machines not surfing on some sites. (ever same sites when the problem occur).</p><p>Can you help me? if you want can i share some logs.</p><p>thank you very much Maicol</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-web" rel="tag" title="see questions tagged &#39;web&#39;">web</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-navigation" rel="tag" title="see questions tagged &#39;navigation&#39;">navigation</span> <span class="post-tag tag-link-microsoft" rel="tag" title="see questions tagged &#39;microsoft&#39;">microsoft</span> <span class="post-tag tag-link-huawei" rel="tag" title="see questions tagged &#39;huawei&#39;">huawei</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 May '17, 12:59</strong></p><img src="https://secure.gravatar.com/avatar/ae584db88e33b52ff7f08ead6f40dff4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="beherenow&#39;s gravatar image" /><p><span>beherenow</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="beherenow has no accepted answers">0%</span></p></div></div><div id="comments-container-61720" class="comments-container"></div><div id="comment-tools-61720" class="comment-tools"></div><div class="clear"></div><div id="comment-61720-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

