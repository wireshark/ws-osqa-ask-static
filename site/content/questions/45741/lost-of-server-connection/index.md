+++
type = "question"
title = "Lost of server connection"
description = '''I am having trouble keeping a connection to a server. I can browse Windows folder for 2 minutes then it just stop loading. At the same time, I can play a youtube video without any problem. I have a wireshark capture, but I don&#x27;t know how to put it in this message. If someone could help me with that ...'''
date = "2015-09-09T14:53:00Z"
lastmod = "2015-09-10T06:52:00Z"
weight = 45741
keywords = [ "communication", "lost", "server" ]
aliases = [ "/questions/45741" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Lost of server connection](/questions/45741/lost-of-server-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45741-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45741-score" class="post-score" title="current number of votes">0</div><span id="post-45741-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am having trouble keeping a connection to a server.</p><p>I can browse Windows folder for 2 minutes then it just stop loading. At the same time, I can play a youtube video without any problem.</p><p>I have a wireshark capture, but I don't know how to put it in this message. If someone could help me with that also.</p><p>Thx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-communication" rel="tag" title="see questions tagged &#39;communication&#39;">communication</span> <span class="post-tag tag-link-lost" rel="tag" title="see questions tagged &#39;lost&#39;">lost</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '15, 14:53</strong></p><img src="https://secure.gravatar.com/avatar/e5dc14bc1e80af09d462a18a3fd9b106?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ren%C3%A9-Maxime&#39;s gravatar image" /><p><span>René-Maxime</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="René-Maxime has no accepted answers">0%</span></p></div></div><div id="comments-container-45741" class="comments-container"><span id="45754"></span><div id="comment-45754" class="comment"><div id="post-45754-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-45754-info" class="comment-info"><span class="comment-age">(10 Sep '15, 04:37)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="45759"></span><div id="comment-45759" class="comment"><div id="post-45759-score" class="comment-score"></div><div class="comment-text"><p><a href="https://www.cloudshark.org/captures/c757db786879">Here is the link on cloud shark</a></p><p>I have tried to use a Wireless card in the desktop PC last night, still the same issues</p></div><div id="comment-45759-info" class="comment-info"><span class="comment-age">(10 Sep '15, 06:52)</span> <span class="comment-user userinfo">René-Maxime</span></div></div></div><div id="comment-tools-45741" class="comment-tools"></div><div class="clear"></div><div id="comment-45741-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

