+++
type = "question"
title = "DCE/RPC with NTLMSSP decryption"
description = '''Hi, I have pcap containing DCE/RPC traffic whith authentication over NTLMSSP at the beginning. Is it possible with Wireshark to decrypt DCE/RPC communication provided I have NTLMSSP NT password?'''
date = "2017-10-09T04:36:00Z"
lastmod = "2017-10-09T05:41:00Z"
weight = 63757
keywords = [ "decryption", "rpc", "dce", "ntlmssp" ]
aliases = [ "/questions/63757" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [DCE/RPC with NTLMSSP decryption](/questions/63757/dcerpc-with-ntlmssp-decryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63757-score" class="post-score" title="current number of votes">0</div><span id="post-63757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have pcap containing DCE/RPC traffic whith authentication over NTLMSSP at the beginning. Is it possible with Wireshark to decrypt DCE/RPC communication provided I have NTLMSSP NT password?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-rpc" rel="tag" title="see questions tagged &#39;rpc&#39;">rpc</span> <span class="post-tag tag-link-dce" rel="tag" title="see questions tagged &#39;dce&#39;">dce</span> <span class="post-tag tag-link-ntlmssp" rel="tag" title="see questions tagged &#39;ntlmssp&#39;">ntlmssp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '17, 04:36</strong></p><img src="https://secure.gravatar.com/avatar/f0956732f479c7b90f8165de13e91d58?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="berma&#39;s gravatar image" /><p><span>berma</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="berma has no accepted answers">0%</span></p></div></div><div id="comments-container-63757" class="comments-container"><span id="63760"></span><div id="comment-63760" class="comment"><div id="post-63760-score" class="comment-score"></div><div class="comment-text"><p>EDIT: In NTLMSSP protocol preferences I entered the NT Password, but still in DCE/RPC packets I see "Ecrypted stub data" instead of decrypted content.</p></div><div id="comment-63760-info" class="comment-info"><span class="comment-age">(09 Oct '17, 05:41)</span> <span class="comment-user userinfo">berma</span></div></div></div><div id="comment-tools-63757" class="comment-tools"></div><div class="clear"></div><div id="comment-63757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

