+++
type = "question"
title = "how do i add a new conversation type"
description = '''Where do I need to add code to do this?'''
date = "2012-11-21T15:03:00Z"
lastmod = "2012-11-22T18:39:00Z"
weight = 16184
keywords = [ "conversation", "developer" ]
aliases = [ "/questions/16184" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how do i add a new conversation type](/questions/16184/how-do-i-add-a-new-conversation-type)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16184-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16184-score" class="post-score" title="current number of votes">0</div><span id="post-16184-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where do I need to add code to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversation" rel="tag" title="see questions tagged &#39;conversation&#39;">conversation</span> <span class="post-tag tag-link-developer" rel="tag" title="see questions tagged &#39;developer&#39;">developer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '12, 15:03</strong></p><img src="https://secure.gravatar.com/avatar/56993b38fa37140b783e7913ec139f45?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="protocolmagic&#39;s gravatar image" /><p><span>protocolmagic</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="protocolmagic has no accepted answers">0%</span></p></div></div><div id="comments-container-16184" class="comments-container"><span id="16214"></span><div id="comment-16214" class="comment"><div id="post-16214-score" class="comment-score"></div><div class="comment-text"><p>You need to be more specific about wht you are trying to do. What do you mean by new conversation type?</p></div><div id="comment-16214-info" class="comment-info"><span class="comment-age">(22 Nov '12, 07:17)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="16227"></span><div id="comment-16227" class="comment"><div id="post-16227-score" class="comment-score"></div><div class="comment-text"><p>I have made a patch to the 802.11 dissector to export the receiver and transmitter address for every frame. A conversation in 802.11 is between a pair of receiver and transmitter addresses. I'd like to add this to the list of conversations you can filter for.</p></div><div id="comment-16227-info" class="comment-info"><span class="comment-age">(22 Nov '12, 18:39)</span> <span class="comment-user userinfo">protocolmagic</span></div></div></div><div id="comment-tools-16184" class="comment-tools"></div><div class="clear"></div><div id="comment-16184-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

