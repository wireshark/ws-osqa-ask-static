+++
type = "question"
title = "How to measure duration time between two packets via command or API"
description = '''I use a notebook to browse web. How to measure duration from packet A tcp SYN to packet B tcp ACK. I can see it from wireshark GUI through filter. If I want to measure duration so many times, maybe 1000 times. Anyone know whether API or command can do this thing? Thanks Shown cws@nctu.edu.tw'''
date = "2013-12-10T22:06:00Z"
lastmod = "2013-12-10T22:06:00Z"
weight = 27989
keywords = [ "duration" ]
aliases = [ "/questions/27989" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to measure duration time between two packets via command or API](/questions/27989/how-to-measure-duration-time-between-two-packets-via-command-or-api)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27989-score" class="post-score" title="current number of votes">0</div><span id="post-27989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use a notebook to browse web. How to measure duration from packet A tcp SYN to packet B tcp ACK. I can see it from wireshark GUI through filter. If I want to measure duration so many times, maybe 1000 times. Anyone know whether API or command can do this thing?</p><p>Thanks Shown <span class="__cf_email__" data-cfemail="53302420133d3027267d3637267d2724">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-duration" rel="tag" title="see questions tagged &#39;duration&#39;">duration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '13, 22:06</strong></p><img src="https://secure.gravatar.com/avatar/ffb9ab00d1421d8e0567835017b26713?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shown&#39;s gravatar image" /><p><span>Shown</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shown has no accepted answers">0%</span></p></div></div><div id="comments-container-27989" class="comments-container"></div><div id="comment-tools-27989" class="comment-tools"></div><div class="clear"></div><div id="comment-27989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

