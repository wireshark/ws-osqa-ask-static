+++
type = "question"
title = "imac wireshark installation gets hung up, cannot install"
description = '''RE: iMac, Wireshark, install, hung I am trying to install the Wireshark 1.12.0 intel 64 version. I read somewhere that an XQuartz might need to be installed, but this article referred to a lion, and not Mavericks. I&#x27;ve tried three times installing, even authorizing apps downloaded from anywhere, but...'''
date = "2014-08-07T07:02:00Z"
lastmod = "2014-08-07T07:02:00Z"
weight = 35304
keywords = [ "hung", "install", "imac", "wireshark" ]
aliases = [ "/questions/35304" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [imac wireshark installation gets hung up, cannot install](/questions/35304/imac-wireshark-installation-gets-hung-up-cannot-install)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35304-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35304-score" class="post-score" title="current number of votes">0</div><span id="post-35304-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>RE: iMac, Wireshark, install, hung</p><p>I am trying to install the Wireshark 1.12.0 intel 64 version. I read somewhere that an XQuartz might need to be installed, but this article referred to a lion, and not Mavericks. I've tried three times installing, even authorizing apps downloaded from anywhere, but it gets hung up at "Preparing for installation" going on unreasonable timing of 15 minutes when I finally force quit. I would like to know if anyone has had this issue, and how were you able to correct it?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hung" rel="tag" title="see questions tagged &#39;hung&#39;">hung</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-imac" rel="tag" title="see questions tagged &#39;imac&#39;">imac</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '14, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/4bf069c688b4aa9c599b9ff76958b608?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="macaid&#39;s gravatar image" /><p><span>macaid</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="macaid has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-35304" class="comments-container"></div><div id="comment-tools-35304" class="comment-tools"></div><div class="clear"></div><div id="comment-35304-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

