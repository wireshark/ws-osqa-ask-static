+++
type = "question"
title = "code for decoding"
description = '''can anybody paste a sample c++ or c code to decode the data captured from wireshark ??'''
date = "2011-05-26T23:42:00Z"
lastmod = "2011-05-27T20:42:00Z"
weight = 4247
keywords = [ "dissector", "source-code" ]
aliases = [ "/questions/4247" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [code for decoding](/questions/4247/code-for-decoding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4247-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4247-score" class="post-score" title="current number of votes">0</div><span id="post-4247-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can anybody paste a sample c++ or c code to decode the data captured from wireshark ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-source-code" rel="tag" title="see questions tagged &#39;source-code&#39;">source-code</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 May '11, 23:42</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>27 May '11, 20:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-4247" class="comments-container"></div><div id="comment-tools-4247" class="comment-tools"></div><div class="clear"></div><div id="comment-4247-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="4248"></span>

<div id="answer-container-4248" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4248-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4248-score" class="post-score" title="current number of votes">1</div><span id="post-4248-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>the source-code for wireshark or tshark is freely available sample code that does exactly what you want.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '11, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 May '11, 00:21</strong> </span></p></div></div><div id="comments-container-4248" class="comments-container"></div><div id="comment-tools-4248" class="comment-tools"></div><div class="clear"></div><div id="comment-4248-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4249"></span>

<div id="answer-container-4249" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4249-score" class="post-score" title="current number of votes">0</div><span id="post-4249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is the tutorial I used to learn libpcap capture, parse. Save Wireshark data as pcap and parse it with this code.</p><p>http://yuba.stanford.edu/~casado/pcap/section1.html</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '11, 00:23</strong></p><img src="https://secure.gravatar.com/avatar/2bb36804afb054782c66c3ad2d8690f6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jakan&#39;s gravatar image" /><p><span>jakan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jakan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 May '11, 00:23</strong> </span></p></div></div><div id="comments-container-4249" class="comments-container"></div><div id="comment-tools-4249" class="comment-tools"></div><div class="clear"></div><div id="comment-4249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4264"></span>

<div id="answer-container-4264" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4264-score" class="post-score" title="current number of votes">0</div><span id="post-4264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are several resources that show you how to write your own Wireshark dissector. The difficulty level depends on the chosen language. I prefer Lua because it's easier to work with (other than debugging for which I rely on print-outs to the console), Lua doesn't require installing a bunch of packages to setup a build environment; Lua just needs a Wireshark installation.</p><p>Writing a dissector in C:</p><ul><li><a href="http://wiki.wireshark.org/Development">Development wiki</a> page describes how to setup your environment</li><li><a href="http://anonsvn.wireshark.org/wireshark/trunk/doc/README.developer">README.developer</a> (I don't know why that's only in a simple text file)</li><li><a href="http://www.wireshark.org/docs/wsdg_html/#ChDissectAdd">Adding a Basic Dissector</a></li><li><a href="http://anonsvn.wireshark.org/viewvc/trunk-1.4/plugins/">Wireshark source code for dissectors</a></li></ul><p>Writing a dissector in Lua:</p><ul><li><a href="https://www.wireshark.org/docs/wsug_html_chunked/wsluarm.html">Wireshark Lua API</a></li><li><a href="https://www.wireshark.org/docs/wsug_html_chunked/wslua_dissector_example.html">Example of dissector written in Lua</a></li><li><a href="http://wiki.wireshark.org/Lua/Dissectors">Lua/Dissector wiki</a></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '11, 20:42</strong></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworld has 28 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 May '11, 20:45</strong> </span></p></div></div><div id="comments-container-4264" class="comments-container"></div><div id="comment-tools-4264" class="comment-tools"></div><div class="clear"></div><div id="comment-4264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

