+++
type = "question"
title = "Wireshark 802.11 on monitor mode, not able to see http protocol with wpa2-psk"
description = '''Hello everyone, Lately Ive done dual-boot to my laptop with Windows 8 and Ubuntu 12.04, I have succefuly managed to use monitor mode to capture packets from my local wlan. To enter monitor mode I use: sudo ifconfig wlan0 down sudo iwconfig wlan0 mode managed sudo ifconfig wlan0 up When I capture pac...'''
date = "2014-04-16T18:04:00Z"
lastmod = "2014-04-16T18:04:00Z"
weight = 31907
keywords = [ "capture", "wpa2", "monitor", "packet", "802.11" ]
aliases = [ "/questions/31907" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 802.11 on monitor mode, not able to see http protocol with wpa2-psk](/questions/31907/wireshark-80211-on-monitor-mode-not-able-to-see-http-protocol-with-wpa2-psk)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31907-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31907-score" class="post-score" title="current number of votes">0</div><span id="post-31907-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone, Lately Ive done dual-boot to my laptop with Windows 8 and Ubuntu 12.04, I have succefuly managed to use monitor mode to capture packets from my local wlan. To enter monitor mode I use: sudo ifconfig wlan0 down sudo iwconfig wlan0 mode managed sudo ifconfig wlan0 up</p><p>When I capture packets(on wpa2-psk protected wlan) I can only see 802.11 protocol, I tried to use WPA PSK Generator, and tried to write: WPA-PWD:PWD:SSID in the options in wireshark on the 802.11 tab.</p><p>When my wlan was oprn(password-less), I saw protocols like HTTP,UDP,TCP... and I did manage to check and I have captured packets from another computer in my house.</p><p>I want to capture packets in my wap2-psk protected wlan, but I want to encrypt it with the password and see the protocls like: HTTP,TCP.. Is that possible? If so how? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '14, 18:04</strong></p><img src="https://secure.gravatar.com/avatar/b2981d638b43d76dc30ba55b319bb50f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="omer364&#39;s gravatar image" /><p><span>omer364</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="omer364 has no accepted answers">0%</span></p></div></div><div id="comments-container-31907" class="comments-container"></div><div id="comment-tools-31907" class="comment-tools"></div><div class="clear"></div><div id="comment-31907-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

