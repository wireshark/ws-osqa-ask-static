+++
type = "question"
title = "[closed] Feature Request - Output Format of Custom Columns"
description = '''Hi! I would like to add a custom column for ptp.v2.ClockIdentity, but when I do this, the value of this field is represented in the column (packet list view) as a number while it shows as a hex-byte sequency in the packet details. I guess this is because the data type of the field itself is 64bit un...'''
date = "2011-10-11T01:30:00Z"
lastmod = "2014-03-19T05:23:00Z"
weight = 6841
keywords = [ "packetlist", "feature-request", "columns", "custom" ]
aliases = [ "/questions/6841" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Feature Request - Output Format of Custom Columns](/questions/6841/feature-request-output-format-of-custom-columns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6841-score" class="post-score" title="current number of votes">1</div><span id="post-6841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>I would like to add a custom column for ptp.v2.ClockIdentity, but when I do this, the value of this field is represented in the column (packet list view) as a number while it shows as a hex-byte sequency in the packet details.</p><p>I guess this is because the data type of the field itself is 64bit unsigned int, so my question is: Is there a way to let custom columns use the same representation that is used in the packet details? If not, I would really like to see this feature being added in later versions.</p><p>This whole thing is one of the most powerful network analyzing tools (as we all know), but this thing would increase its usefulness even more for me (anybody else?).</p><p>Thanks, Heiko</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packetlist" rel="tag" title="see questions tagged &#39;packetlist&#39;">packetlist</span> <span class="post-tag tag-link-feature-request" rel="tag" title="see questions tagged &#39;feature-request&#39;">feature-request</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span> <span class="post-tag tag-link-custom" rel="tag" title="see questions tagged &#39;custom&#39;">custom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '11, 01:30</strong></p><img src="https://secure.gravatar.com/avatar/8bf835f7379662cd7a1d156f398fe072?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Heiko&#39;s gravatar image" /><p><span>Heiko</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Heiko has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Mar '14, 05:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-6841" class="comments-container"><span id="30947"></span><div id="comment-30947" class="comment"><div id="post-30947-score" class="comment-score"></div><div class="comment-text"><p>+1: many protocols feature fields with structured bitmasks that are very difficult to read in decimal (e.g. SMPP). the ability for Wireshark to utilize an arbitrary (user-supplied) format for its display columns would be highly useful. why stop at hex? binary, float, various date formats — just give us everything <code>printf(…)</code> has :]</p></div><div id="comment-30947-info" class="comment-info"><span class="comment-age">(19 Mar '14, 04:58)</span> <span class="comment-user userinfo">RubyTuesdayDONO</span></div></div><span id="30948"></span><div id="comment-30948" class="comment"><div id="post-30948-score" class="comment-score"></div><div class="comment-text"><p>Ask Wireshark isn't the place for Feature Requests, those should be added to the Wireshark <a href="https://bugs.wireshark.org">Bugzilla</a>, marking the item as an "enhancement".</p></div><div id="comment-30948-info" class="comment-info"><span class="comment-age">(19 Mar '14, 05:23)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-6841" class="comment-tools"></div><div class="clear"></div><div id="comment-6841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant. Use Wireshark Bugzilla for Feature Requests" by grahamb 19 Mar '14, 05:53

</div>

</div>

</div>

