+++
type = "question"
title = "Using Wireshark to help Create Firewall Rules - Best Filter"
description = '''Good Morning, First I apologize if this is like the most asked question and I failed to find it in my searching.  I&#x27;m currently doing a review of an environment to put more stringent firewall rules in place. Doing a normal Wireshark pull of traffic to a box and dumping out to text I get a lot more i...'''
date = "2016-12-02T08:37:00Z"
lastmod = "2016-12-02T08:37:00Z"
weight = 57790
keywords = [ "filter", "ip", "firewall", "creation", "rule" ]
aliases = [ "/questions/57790" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Using Wireshark to help Create Firewall Rules - Best Filter](/questions/57790/using-wireshark-to-help-create-firewall-rules-best-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57790-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57790-score" class="post-score" title="current number of votes">0</div><span id="post-57790-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good Morning,</p><p>First I apologize if this is like the most asked question and I failed to find it in my searching.</p><p>I'm currently doing a review of an environment to put more stringent firewall rules in place. Doing a normal Wireshark pull of traffic to a box and dumping out to text I get a lot more information then what I really am needing.</p><p>What I would like to do is just record Source Server, Destination Server and the port used for the communication.</p><p>What is the best way to complete that.</p><p>My assumption being I can then Dump to text via File &gt; Export &gt; File selecting CS as my save type. Pull that into excel, remove duplicates and have a pretty good communication list of UDP/TCP IP ports being used.</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span> <span class="post-tag tag-link-creation" rel="tag" title="see questions tagged &#39;creation&#39;">creation</span> <span class="post-tag tag-link-rule" rel="tag" title="see questions tagged &#39;rule&#39;">rule</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '16, 08:37</strong></p><img src="https://secure.gravatar.com/avatar/fff5ed84b6e08350225cd6a059f27200?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mike%20W&#39;s gravatar image" /><p><span>Mike W</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mike W has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Dec '16, 08:41</strong> </span></p></div></div><div id="comments-container-57790" class="comments-container"></div><div id="comment-tools-57790" class="comment-tools"></div><div class="clear"></div><div id="comment-57790-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

