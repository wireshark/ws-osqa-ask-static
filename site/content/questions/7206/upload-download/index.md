+++
type = "question"
title = "Upload &amp; download"
description = '''Hello I like to measure a download and upload in wireshark, but i&#x27;m a beginner in this software. I need it to a project of the web. I&#x27;l be grateful for help'''
date = "2011-11-02T12:23:00Z"
lastmod = "2011-11-08T23:18:00Z"
weight = 7206
keywords = [ "and", "download", "upload" ]
aliases = [ "/questions/7206" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Upload & download](/questions/7206/upload-download)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7206-score" class="post-score" title="current number of votes">0</div><span id="post-7206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I like to measure a download and upload in wireshark, but i'm a beginner in this software. I need it to a project of the web. I'l be grateful for help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-and" rel="tag" title="see questions tagged &#39;and&#39;">and</span> <span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-upload" rel="tag" title="see questions tagged &#39;upload&#39;">upload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '11, 12:23</strong></p><img src="https://secure.gravatar.com/avatar/7cf07b52a99d023c47feaa2949123970?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Blazeyos&#39;s gravatar image" /><p><span>Blazeyos</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Blazeyos has no accepted answers">0%</span></p></div></div><div id="comments-container-7206" class="comments-container"><span id="7207"></span><div id="comment-7207" class="comment"><div id="post-7207-score" class="comment-score">1</div><div class="comment-text"><p>I went to the <strong>statistcs then IO Graphs</strong>, evry thing would be perfect if I it summary Graphs in the end of 8 hours</p><p>Sory for me English</p></div><div id="comment-7207-info" class="comment-info"><span class="comment-age">(02 Nov '11, 12:43)</span> <span class="comment-user userinfo">Blazeyos</span></div></div><span id="7213"></span><div id="comment-7213" class="comment"><div id="post-7213-score" class="comment-score"></div><div class="comment-text"><p>What is that you want to measure exactly?</p></div><div id="comment-7213-info" class="comment-info"><span class="comment-age">(02 Nov '11, 18:01)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="7217"></span><div id="comment-7217" class="comment"><div id="post-7217-score" class="comment-score"></div><div class="comment-text"><p>I want measure Up&amp;Dow in me PC by wireshark, TCP protocol.</p></div><div id="comment-7217-info" class="comment-info"><span class="comment-age">(03 Nov '11, 03:53)</span> <span class="comment-user userinfo">Blazeyos</span></div></div></div><div id="comment-tools-7206" class="comment-tools"></div><div class="clear"></div><div id="comment-7206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7320"></span>

<div id="answer-container-7320" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7320-score" class="post-score" title="current number of votes">0</div><span id="post-7320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you are about measuring Upload/Download connection speeds, check out <a href="http://speedtest.net/">bandwidth meter online</a></p><p>However if it is about measuring web application performance you could check out <a href="http://www.ieinspector.com/httpanalyzer/">HTTP analyser</a>, wireshark is not very user-friendly for the computer-illiterate users.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 23:18</strong></p><img src="https://secure.gravatar.com/avatar/35d96b8e73e6deb4e332d076fd3269b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShomeaX&#39;s gravatar image" /><p><span>ShomeaX</span><br />
<span class="score" title="73 reputation points">73</span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShomeaX has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Nov '11, 23:18</strong> </span></p></div></div><div id="comments-container-7320" class="comments-container"></div><div id="comment-tools-7320" class="comment-tools"></div><div class="clear"></div><div id="comment-7320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

