+++
type = "question"
title = "Automatic extracting needed data from the ready pcap-files"
description = '''Hello! I have a server for storing the pcap-files (few hundreds) of sniffered interfaces. And I&#x27;d like to extract needed data from this pcap-files. Just for example: I have 200 pcap-files 50 MB each. It&#x27;s needed to search all ICMP-pings from IP1 to IP2 and answeres. Currently I learn the possibility...'''
date = "2014-05-05T02:56:00Z"
lastmod = "2014-05-05T05:37:00Z"
weight = 32527
keywords = [ "data", "automatic", "file", "extracting" ]
aliases = [ "/questions/32527" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Automatic extracting needed data from the ready pcap-files](/questions/32527/automatic-extracting-needed-data-from-the-ready-pcap-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32527-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32527-score" class="post-score" title="current number of votes">0</div><span id="post-32527-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! I have a server for storing the pcap-files (few hundreds) of sniffered interfaces. And I'd like to extract needed data from this pcap-files. Just for example: I have 200 pcap-files 50 MB each. It's needed to search all ICMP-pings from IP1 to IP2 and answeres. Currently I learn the possibility of using of script-languages and tshark-commands. May be other ways exists? What are the variants how I can do it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-automatic" rel="tag" title="see questions tagged &#39;automatic&#39;">automatic</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-extracting" rel="tag" title="see questions tagged &#39;extracting&#39;">extracting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '14, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/d7ebdfa64a88154cd163c7cc781e4315?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="factorial&#39;s gravatar image" /><p><span>factorial</span><br />
<span class="score" title="26 reputation points">26</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="factorial has no accepted answers">0%</span></p></div></div><div id="comments-container-32527" class="comments-container"></div><div id="comment-tools-32527" class="comment-tools"></div><div class="clear"></div><div id="comment-32527-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32532"></span>

<div id="answer-container-32532" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32532-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32532-score" class="post-score" title="current number of votes">1</div><span id="post-32532-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="factorial has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Currently I learn the possibility of <strong>using of script-languages and tshark</strong>-commands.</p></blockquote><p>that's basically the only option to do it in an automatic way, besides reading the pcap file directly with your own software. So, go ahead with that approach.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '14, 05:37</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-32532" class="comments-container"></div><div id="comment-tools-32532" class="comment-tools"></div><div class="clear"></div><div id="comment-32532-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

