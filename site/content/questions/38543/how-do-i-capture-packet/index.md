+++
type = "question"
title = "How do I capture packet?"
description = '''Hi I am trying to capture packet from my mobile device. I am using commandline tshark to achieve this. tshark -Y &quot;http.request&quot; and some multiple combinations filter. I am not able to capture hfull uri. I tried with the &quot;http.request.uri&quot; and &quot;http.request.full_uri&quot; . Can anybody help me out? also I...'''
date = "2014-12-12T08:30:00Z"
lastmod = "2014-12-12T08:30:00Z"
weight = 38543
keywords = [ "commandline", "tshark", "libpcap" ]
aliases = [ "/questions/38543" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I capture packet?](/questions/38543/how-do-i-capture-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38543-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38543-score" class="post-score" title="current number of votes">0</div><span id="post-38543-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am trying to capture packet from my mobile device. I am using commandline tshark to achieve this.</p><p>tshark -Y "http.request" and some multiple combinations filter. I am not able to capture hfull uri. I tried with the "http.request.uri" and "http.request.full_uri" .</p><p>Can anybody help me out? also I am not able to install libpcap on my mac.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-commandline" rel="tag" title="see questions tagged &#39;commandline&#39;">commandline</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-libpcap" rel="tag" title="see questions tagged &#39;libpcap&#39;">libpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Dec '14, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/603eb8454fe87415ef1ff981dfa0784b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shrikantKaramkar&#39;s gravatar image" /><p><span>shrikantKara...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shrikantKaramkar has no accepted answers">0%</span></p></div></div><div id="comments-container-38543" class="comments-container"></div><div id="comment-tools-38543" class="comment-tools"></div><div class="clear"></div><div id="comment-38543-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

