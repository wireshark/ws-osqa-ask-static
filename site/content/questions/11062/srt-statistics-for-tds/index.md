+++
type = "question"
title = "SRT statistics for TDS?"
description = '''Is there a way to perform SRT statistics for TDS protocol using the Service Response Time statistics tool like can be done for NFS, SMB, etc? Or maybe is there some other way anyone may know to get request/reply statistics other than going through and doing it manually?  Thank-you'''
date = "2012-05-16T12:59:00Z"
lastmod = "2012-05-17T11:50:00Z"
weight = 11062
keywords = [ "tds", "statistics", "srt" ]
aliases = [ "/questions/11062" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [SRT statistics for TDS?](/questions/11062/srt-statistics-for-tds)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11062-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11062-score" class="post-score" title="current number of votes">0</div><span id="post-11062-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to perform SRT statistics for TDS protocol using the Service Response Time statistics tool like can be done for NFS, SMB, etc? Or maybe is there some other way anyone may know to get request/reply statistics other than going through and doing it manually?</p><p>Thank-you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tds" rel="tag" title="see questions tagged &#39;tds&#39;">tds</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-srt" rel="tag" title="see questions tagged &#39;srt&#39;">srt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '12, 12:59</strong></p><img src="https://secure.gravatar.com/avatar/98ec75d031a962cf9b8cd542330f511d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="seyerekim&#39;s gravatar image" /><p><span>seyerekim</span><br />
<span class="score" title="36 reputation points">36</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="seyerekim has no accepted answers">0%</span></p></div></div><div id="comments-container-11062" class="comments-container"></div><div id="comment-tools-11062" class="comment-tools"></div><div class="clear"></div><div id="comment-11062-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11094"></span>

<div id="answer-container-11094" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11094-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11094-score" class="post-score" title="current number of votes">1</div><span id="post-11094-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="seyerekim has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No one has taken the time and interest to write code for SRT statistics for TDS so at this moment this is not possible in Wireshark.</p><p>Requests for new features can be filed on <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> (when marked as an "enhancement request")</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 May '12, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-11094" class="comments-container"><span id="11112"></span><div id="comment-11112" class="comment"><div id="post-11112-score" class="comment-score"></div><div class="comment-text"><p>Thanks Sake, I appreciate your confirming that for me.</p></div><div id="comment-11112-info" class="comment-info"><span class="comment-age">(17 May '12, 11:50)</span> <span class="comment-user userinfo">seyerekim</span></div></div></div><div id="comment-tools-11094" class="comment-tools"></div><div class="clear"></div><div id="comment-11094-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

