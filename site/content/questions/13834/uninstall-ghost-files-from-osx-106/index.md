+++
type = "question"
title = "Uninstall &quot;ghost&quot; files from OSX 10.6"
description = '''Hi, I&#x27;m trying to do the same thing. I have a macbook pro 10.6. The problem is I installed it and followed the directions. After installing it and restarting my computer a bunch of (I think they are called) &quot;ghost files&quot; appeared on my desktop and on my main harddrive and maybe other places. So want...'''
date = "2012-08-22T23:39:00Z"
lastmod = "2012-08-24T19:42:00Z"
weight = 13834
keywords = [ "osx", "mac", "uninstall" ]
aliases = [ "/questions/13834" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Uninstall "ghost" files from OSX 10.6](/questions/13834/uninstall-ghost-files-from-osx-106)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13834-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13834-score" class="post-score" title="current number of votes">0</div><span id="post-13834-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm trying to do the same thing. I have a macbook pro 10.6. The problem is I installed it and followed the directions. After installing it and restarting my computer a bunch of (I think they are called) "ghost files" appeared on my desktop and on my main harddrive and maybe other places. So want to uninstall Wireshark and everything that came with it, including the X11 app that came with it and ALL those "ghost files" that came with it. A detailed step-by-step answer would be much appreaciated. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '12, 23:39</strong></p><img src="https://secure.gravatar.com/avatar/2785941f50866c8c7763486e1cdebe0a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="carla888&#39;s gravatar image" /><p><span>carla888</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="carla888 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>23 Aug '12, 02:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-13834" class="comments-container"><span id="13888"></span><div id="comment-13888" class="comment"><div id="post-13888-score" class="comment-score"></div><div class="comment-text"><p>What are some examples of these "ghost files"?</p><p>The X11 app doesn't come with Wireshark, it comes with OS X. It might get installed on demand when you first try to run an X11 app, such as Wireshark.</p></div><div id="comment-13888-info" class="comment-info"><span class="comment-age">(24 Aug '12, 19:42)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-13834" class="comment-tools"></div><div class="clear"></div><div id="comment-13834-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

