+++
type = "question"
title = "porting from Linux to Windows - RC5 Decryption"
description = '''Hi, I am trying to port a dissector from Linux to Windows. The dissector requires rc5 encryption/decryption capability and the Linux code uses openssl rc5 header files. It seems wireshark doesnt use openssl instead uses GNUTLS. I am wondering if there is a way to use openssl for wireshark dissector ...'''
date = "2012-01-13T10:59:00Z"
lastmod = "2012-01-13T10:59:00Z"
weight = 8377
keywords = [ "rc5", "openssl", "wireshark" ]
aliases = [ "/questions/8377" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [porting from Linux to Windows - RC5 Decryption](/questions/8377/porting-from-linux-to-windows-rc5-decryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8377-score" class="post-score" title="current number of votes">0</div><span id="post-8377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to port a dissector from Linux to Windows. The dissector requires rc5 encryption/decryption capability and the Linux code uses openssl rc5 header files. It seems wireshark doesnt use openssl instead uses GNUTLS. I am wondering if there is a way to use openssl for wireshark dissector development ? if yes, what changes are needed in the source code ? also are there any pros and cons of trying to do that ?</p><p>Thanks,</p><p>Am.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rc5" rel="tag" title="see questions tagged &#39;rc5&#39;">rc5</span> <span class="post-tag tag-link-openssl" rel="tag" title="see questions tagged &#39;openssl&#39;">openssl</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '12, 10:59</strong></p><img src="https://secure.gravatar.com/avatar/fb2614cfcd393a0a53795d0dd188c494?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ashish&#39;s gravatar image" /><p><span>Ashish</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ashish has no accepted answers">0%</span></p></div></div><div id="comments-container-8377" class="comments-container"></div><div id="comment-tools-8377" class="comment-tools"></div><div class="clear"></div><div id="comment-8377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

