+++
type = "question"
title = "am I part of a Ddos"
description = '''I have non stop ICMP requests going from my pc to a server in china. Am I being used as a zombie. if so, any suggestions on stopping it?'''
date = "2014-10-30T23:30:00Z"
lastmod = "2014-10-31T01:30:00Z"
weight = 37485
keywords = [ "ddos", "icmp" ]
aliases = [ "/questions/37485" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [am I part of a Ddos](/questions/37485/am-i-part-of-a-ddos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37485-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37485-score" class="post-score" title="current number of votes">0</div><span id="post-37485-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have non stop ICMP requests going from my pc to a server in china. Am I being used as a zombie. if so, any suggestions on stopping it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span> <span class="post-tag tag-link-icmp" rel="tag" title="see questions tagged &#39;icmp&#39;">icmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '14, 23:30</strong></p><img src="https://secure.gravatar.com/avatar/de12c3e7355e195163f6bc513387e95a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kwwle&#39;s gravatar image" /><p><span>kwwle</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kwwle has no accepted answers">0%</span></p></div></div><div id="comments-container-37485" class="comments-container"><span id="37489"></span><div id="comment-37489" class="comment"><div id="post-37489-score" class="comment-score"></div><div class="comment-text"><p>What ICMP requests are those? can you paste an example?</p></div><div id="comment-37489-info" class="comment-info"><span class="comment-age">(31 Oct '14, 01:25)</span> <span class="comment-user userinfo">mrEEde</span></div></div></div><div id="comment-tools-37485" class="comment-tools"></div><div class="clear"></div><div id="comment-37485-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37490"></span>

<div id="answer-container-37490" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37490-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37490-score" class="post-score" title="current number of votes">0</div><span id="post-37490-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are several protocols to communicate with a botnet zombie. ICMP is one option.</p><blockquote><p><a href="http://blog.zeltser.com/post/10755639827/reverse-icmp-shell">http://blog.zeltser.com/post/10755639827/reverse-icmp-shell</a></p></blockquote><p>So, yes if there is constant ICMP traffic to a host in china and you have no business with that host, then you could have been taken over.</p><p>Please upload a capture file somewhere (google drive, dropbox, cloudshark.org) and post the link here.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Oct '14, 01:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-37490" class="comments-container"></div><div id="comment-tools-37490" class="comment-tools"></div><div class="clear"></div><div id="comment-37490-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

