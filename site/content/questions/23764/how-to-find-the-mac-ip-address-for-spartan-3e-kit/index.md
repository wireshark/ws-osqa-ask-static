+++
type = "question"
title = "how to find the mac &amp; ip address for spartan-3e kit???"
description = '''i want to send data to the spartan-3e kit using ethernet for that i have to assign ip address to the kit'''
date = "2013-08-13T22:41:00Z"
lastmod = "2013-08-14T02:31:00Z"
weight = 23764
keywords = [ "vhdl", "fpga" ]
aliases = [ "/questions/23764" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to find the mac & ip address for spartan-3e kit???](/questions/23764/how-to-find-the-mac-ip-address-for-spartan-3e-kit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23764-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23764-score" class="post-score" title="current number of votes">-1</div><span id="post-23764-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i want to send data to the spartan-3e kit using ethernet for that i have to assign ip address to the kit</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vhdl" rel="tag" title="see questions tagged &#39;vhdl&#39;">vhdl</span> <span class="post-tag tag-link-fpga" rel="tag" title="see questions tagged &#39;fpga&#39;">fpga</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '13, 22:41</strong></p><img src="https://secure.gravatar.com/avatar/ac361d59c69adff0099837999dcdfc4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hemanth&#39;s gravatar image" /><p><span>hemanth</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hemanth has no accepted answers">0%</span></p></div></div><div id="comments-container-23764" class="comments-container"></div><div id="comment-tools-23764" class="comment-tools"></div><div class="clear"></div><div id="comment-23764-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23768"></span>

<div id="answer-container-23768" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23768-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23768-score" class="post-score" title="current number of votes">2</div><span id="post-23768-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The question has absolutely nothing to do with Wireshark. You'll probably find the answer, unsurprisingly, in the Xilinx documentation for the Spartan-3e starter kit.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Aug '13, 02:31</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-23768" class="comments-container"></div><div id="comment-tools-23768" class="comment-tools"></div><div class="clear"></div><div id="comment-23768-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

