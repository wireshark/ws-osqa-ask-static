+++
type = "question"
title = "pcsync-https rst"
description = '''Hi Team,  I am getting a pcsync-https RST from server to server TCP connections. Do anybody know why this happens?'''
date = "2012-06-29T13:34:00Z"
lastmod = "2012-07-03T13:41:00Z"
weight = 12335
keywords = [ "rst", "pcsync-https" ]
aliases = [ "/questions/12335" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [pcsync-https rst](/questions/12335/pcsync-https-rst)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12335-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12335-score" class="post-score" title="current number of votes">0</div><span id="post-12335-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Team, I am getting a pcsync-https RST from server to server TCP connections. Do anybody know why this happens?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rst" rel="tag" title="see questions tagged &#39;rst&#39;">rst</span> <span class="post-tag tag-link-pcsync-https" rel="tag" title="see questions tagged &#39;pcsync-https&#39;">pcsync-https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '12, 13:34</strong></p><img src="https://secure.gravatar.com/avatar/323a069446770e30749e3b81e89e40a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sajan&#39;s gravatar image" /><p><span>Sajan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sajan has no accepted answers">0%</span></p></div></div><div id="comments-container-12335" class="comments-container"><span id="12422"></span><div id="comment-12422" class="comment"><div id="post-12422-score" class="comment-score"></div><div class="comment-text"><p>without a full capture file, it's impossible to give a meaningfull answer. Please post a sample capture at <a href="http://cloudshark.org">cloudshark.org</a>.</p><p>HINT: You cannot delete an anonymous upload at <a href="http://cloudshark.org">cloudshark.org</a>!</p></div><div id="comment-12422-info" class="comment-info"><span class="comment-age">(03 Jul '12, 13:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12335" class="comment-tools"></div><div class="clear"></div><div id="comment-12335-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

