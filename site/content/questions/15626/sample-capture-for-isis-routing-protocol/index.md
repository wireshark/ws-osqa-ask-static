+++
type = "question"
title = "sample capture for ISIS routing protocol"
description = '''hello I thank you for your good site I have a problem about ISIS routing protocol  I need a sample capture of ISIS routing protocol to see it thanks'''
date = "2012-11-07T03:04:00Z"
lastmod = "2012-11-07T03:10:00Z"
weight = 15626
keywords = [ "12345" ]
aliases = [ "/questions/15626" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [sample capture for ISIS routing protocol](/questions/15626/sample-capture-for-isis-routing-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15626-score" class="post-score" title="current number of votes">0</div><span id="post-15626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello I thank you for your good site I have a problem about ISIS routing protocol I need a sample capture of ISIS routing protocol to see it thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-12345" rel="tag" title="see questions tagged &#39;12345&#39;">12345</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '12, 03:04</strong></p><img src="https://secure.gravatar.com/avatar/166be91de047abfd15bf84cbe521fbc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="simineh&#39;s gravatar image" /><p><span>simineh</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="simineh has no accepted answers">0%</span></p></div></div><div id="comments-container-15626" class="comments-container"></div><div id="comment-tools-15626" class="comment-tools"></div><div class="clear"></div><div id="comment-15626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15627"></span>

<div id="answer-container-15627" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15627-score" class="post-score" title="current number of votes">1</div><span id="post-15627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Does this link help? <a href="http://packetlife.net/captures/protocol/isis/">http://packetlife.net/captures/protocol/isis/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '12, 03:10</strong></p><img src="https://secure.gravatar.com/avatar/46196bc495ce51058590c4e4ae334d22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SidR&#39;s gravatar image" /><p><span>SidR</span><br />
<span class="score" title="245 reputation points">245</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="22 badges"><span class="bronze">●</span><span class="badgecount">22</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SidR has 3 accepted answers">30%</span></p></div></div><div id="comments-container-15627" class="comments-container"></div><div id="comment-tools-15627" class="comment-tools"></div><div class="clear"></div><div id="comment-15627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

