+++
type = "question"
title = "OS X Mountain Lion won&#x27;t launch"
description = '''I have followed and tried all of the advise that I could find here still with no luck. I keep Getting the &quot;While Wireshark is open .......&quot; Dialog but then nada. In terminal I am seeing this on any attempt at a launch; .wireshark/.fccache-new: No such file or directory Any ideas?'''
date = "2013-05-16T10:03:00Z"
lastmod = "2013-05-16T14:00:00Z"
weight = 21187
keywords = [ "macosx" ]
aliases = [ "/questions/21187" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [OS X Mountain Lion won't launch](/questions/21187/os-x-mountain-lion-wont-launch)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21187-score" class="post-score" title="current number of votes">0</div><span id="post-21187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have followed and tried all of the advise that I could find here still with no luck. I keep Getting the "While Wireshark is open ......." Dialog but then nada.</p><p>In terminal I am seeing this on any attempt at a launch; .wireshark/.fccache-new: No such file or directory</p><p>Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '13, 10:03</strong></p><img src="https://secure.gravatar.com/avatar/9bb670aabe7f5fcab7da4e36fac71216?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cunning1&#39;s gravatar image" /><p><span>cunning1</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cunning1 has no accepted answers">0%</span></p></div></div><div id="comments-container-21187" class="comments-container"></div><div id="comment-tools-21187" class="comment-tools"></div><div class="clear"></div><div id="comment-21187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21195"></span>

<div id="answer-container-21195" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21195-score" class="post-score" title="current number of votes">0</div><span id="post-21195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you are attempting to install wireshark on mac os x mountain lion, i recommend using the following guide.</p><p><a href="http://blog.israeltorres.org/home/write-ups/installingwiresharkonmacosx108mountainlion">http://blog.israeltorres.org/home/write-ups/installingwiresharkonmacosx108mountainlion</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 May '13, 14:00</strong></p><img src="https://secure.gravatar.com/avatar/b9a4be286b5fb288b053e3bf1ad16710?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samiam&#39;s gravatar image" /><p><span>samiam</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samiam has no accepted answers">0%</span></p></div></div><div id="comments-container-21195" class="comments-container"></div><div id="comment-tools-21195" class="comment-tools"></div><div class="clear"></div><div id="comment-21195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

