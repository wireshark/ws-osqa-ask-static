+++
type = "question"
title = "Right click not working"
description = '''No contextual menus pop up when I right click in the detailed packet window. I am trying to complete some of the labs in the Wireshark 101 book, and this is preventing me from doing so. Is there a solution to fix the problem? Windows 7 Home Premium Wireshark 1.10.7'''
date = "2014-04-23T13:55:00Z"
lastmod = "2014-04-23T15:36:00Z"
weight = 32125
keywords = [ "right", "click" ]
aliases = [ "/questions/32125" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Right click not working](/questions/32125/right-click-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32125-score" class="post-score" title="current number of votes">0</div><span id="post-32125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>No contextual menus pop up when I right click in the detailed packet window. I am trying to complete some of the labs in the Wireshark 101 book, and this is preventing me from doing so. Is there a solution to fix the problem?</p><p>Windows 7 Home Premium Wireshark 1.10.7</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-right" rel="tag" title="see questions tagged &#39;right&#39;">right</span> <span class="post-tag tag-link-click" rel="tag" title="see questions tagged &#39;click&#39;">click</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '14, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/a80a31a3ffc9ce46647e3b5a12c0150d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Organics&#39;s gravatar image" /><p><span>Organics</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Organics has no accepted answers">0%</span></p></div></div><div id="comments-container-32125" class="comments-container"><span id="32127"></span><div id="comment-32127" class="comment"><div id="post-32127-score" class="comment-score"></div><div class="comment-text"><p>Which function do you need? What are you trying to do?</p></div><div id="comment-32127-info" class="comment-info"><span class="comment-age">(23 Apr '14, 15:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="32128"></span><div id="comment-32128" class="comment"><div id="post-32128-score" class="comment-score"></div><div class="comment-text"><p>For example i can not right click protocol and have the menu pop up so i can go to the wiki. Or I cant right click TTL or host (in a DNS query) and have it display in as one of the main columns for dissection.</p></div><div id="comment-32128-info" class="comment-info"><span class="comment-age">(23 Apr '14, 15:11)</span> <span class="comment-user userinfo">Organics</span></div></div><span id="32129"></span><div id="comment-32129" class="comment"><div id="post-32129-score" class="comment-score"></div><div class="comment-text"><p>can you right-click <strong>anywhere else</strong> in the GUI?</p></div><div id="comment-32129-info" class="comment-info"><span class="comment-age">(23 Apr '14, 15:17)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="32130"></span><div id="comment-32130" class="comment"><div id="post-32130-score" class="comment-score"></div><div class="comment-text"><p>Yes, on the main interface i can right click packets and get a submenu, but when i double click a packet and the detailed packet window comes up is when right click stops working</p></div><div id="comment-32130-info" class="comment-info"><span class="comment-age">(23 Apr '14, 15:18)</span> <span class="comment-user userinfo">Organics</span></div></div></div><div id="comment-tools-32125" class="comment-tools"></div><div class="clear"></div><div id="comment-32125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="32126"></span>

<div id="answer-container-32126" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32126-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32126-score" class="post-score" title="current number of votes">0</div><span id="post-32126-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Works fine for me. I'm guessing you mean the "middle pane" where the packet decode is shown. Can you try to create a new profile (or delete the existing ones) to see if the problem goes away?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Apr '14, 14:39</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32126" class="comments-container"></div><div id="comment-tools-32126" class="comment-tools"></div><div class="clear"></div><div id="comment-32126-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="32133"></span>

<div id="answer-container-32133" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32133-score" class="post-score" title="current number of votes">0</div><span id="post-32133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p><strong>but when i double click a packet</strong> and the detailed packet window comes up is when right click stops working</p></blockquote><p>O.K. you should have mentioned that! The right-click does not work in the pop-up after you double-clicked a frame (for whatever reason it's not implemented).</p><p>Please select a frame with a left-click in the '<a href="http://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketListPaneSection.html">Packet List Pane</a>' and <strong>then</strong> right-click in the '<a href="http://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketDetailsPaneSection.html">Packet Details Pane</a>'. That should work.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Apr '14, 15:34</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Apr '14, 15:42</strong> </span></p></div></div><div id="comments-container-32133" class="comments-container"><span id="32134"></span><div id="comment-32134" class="comment"><div id="post-32134-score" class="comment-score"></div><div class="comment-text"><p>Thanks ill give it a shot at work tomorrow. =)</p></div><div id="comment-32134-info" class="comment-info"><span class="comment-age">(23 Apr '14, 15:36)</span> <span class="comment-user userinfo">Organics</span></div></div></div><div id="comment-tools-32133" class="comment-tools"></div><div class="clear"></div><div id="comment-32133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

