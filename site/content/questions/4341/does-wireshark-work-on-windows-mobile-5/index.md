+++
type = "question"
title = "Does WireShark work on Windows Mobile 5"
description = '''Hello! Does WireShark work on Windows Mobile 5? Also, if purchased - do you support Windows Mobile 5? Thank you'''
date = "2011-06-02T10:30:00Z"
lastmod = "2011-06-02T16:47:00Z"
weight = 4341
keywords = [ "windows", "mobile", "5.0" ]
aliases = [ "/questions/4341" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does WireShark work on Windows Mobile 5](/questions/4341/does-wireshark-work-on-windows-mobile-5)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4341-score" class="post-score" title="current number of votes">0</div><span id="post-4341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! Does WireShark work on Windows Mobile 5? Also, if purchased - do you support Windows Mobile 5?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-5.0" rel="tag" title="see questions tagged &#39;5.0&#39;">5.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '11, 10:30</strong></p><img src="https://secure.gravatar.com/avatar/a5e0d226c9711e8bfc1821dc66229f25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mbarajas&#39;s gravatar image" /><p><span>mbarajas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mbarajas has no accepted answers">0%</span></p></div></div><div id="comments-container-4341" class="comments-container"></div><div id="comment-tools-4341" class="comment-tools"></div><div class="clear"></div><div id="comment-4341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4349"></span>

<div id="answer-container-4349" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4349-score" class="post-score" title="current number of votes">0</div><span id="post-4349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No it does not, because GTK (the graphical libraries used by Wireshark) have not been built for Windows Mobile 5 AFAIK.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '11, 13:15</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-4349" class="comments-container"><span id="4352"></span><div id="comment-4352" class="comment"><div id="post-4352-score" class="comment-score"></div><div class="comment-text"><p>...and WinPcap's support for Windows CE-based operating systems is limited.</p></div><div id="comment-4352-info" class="comment-info"><span class="comment-age">(02 Jun '11, 16:47)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4349" class="comment-tools"></div><div class="clear"></div><div id="comment-4349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

