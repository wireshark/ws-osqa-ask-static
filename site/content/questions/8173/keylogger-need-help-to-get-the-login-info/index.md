+++
type = "question"
title = "Keylogger Need help to get the login info."
description = '''Here is a picture of it. How does it connect to the server? I want to delete my logged files he have logged, please help me before I get hacked.  Thanks.'''
date = "2012-01-01T17:25:00Z"
lastmod = "2012-01-01T17:25:00Z"
weight = 8173
keywords = [ "keylogger", "port", "tcp" ]
aliases = [ "/questions/8173" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Keylogger Need help to get the login info.](/questions/8173/keylogger-need-help-to-get-the-login-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8173-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8173-score" class="post-score" title="current number of votes">0</div><span id="post-8173-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Here is a picture of it. How does it connect to the server? I want to delete my logged files he have logged, please help me before I get hacked.</p><p><img src="http://img42.imageshack.us/img42/5561/kylogger.jpg" alt="alt text" /></p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-keylogger" rel="tag" title="see questions tagged &#39;keylogger&#39;">keylogger</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jan '12, 17:25</strong></p><img src="https://secure.gravatar.com/avatar/d4868997c95f73ad8a05aa211b53dc63?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lelouch&#39;s gravatar image" /><p><span>Lelouch</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lelouch has no accepted answers">0%</span></p></img></div></div><div id="comments-container-8173" class="comments-container"></div><div id="comment-tools-8173" class="comment-tools"></div><div class="clear"></div><div id="comment-8173-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

