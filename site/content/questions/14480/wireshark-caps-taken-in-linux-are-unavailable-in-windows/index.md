+++
type = "question"
title = "Wireshark caps taken in Linux are unavailable in windows"
description = '''Hi, I have captures taken in a Linux (ubunto) virtual machine. I transferred them using ftp to a windows 7 virtual machine and I&#x27;m getting the following message: &quot;The capture file appears to be damaged or corrupt. (pcap: File has 3186583733-byte packet, bigger then maximum of 65535)&quot; I tried saving ...'''
date = "2012-09-24T08:06:00Z"
lastmod = "2012-09-24T08:07:00Z"
weight = 14480
keywords = [ "windows", "ftp", "linux" ]
aliases = [ "/questions/14480" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark caps taken in Linux are unavailable in windows](/questions/14480/wireshark-caps-taken-in-linux-are-unavailable-in-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14480-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14480-score" class="post-score" title="current number of votes">0</div><span id="post-14480-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have captures taken in a Linux (ubunto) virtual machine. I transferred them using ftp to a windows 7 virtual machine and I'm getting the following message: "The capture file appears to be damaged or corrupt. (pcap: File has 3186583733-byte packet, bigger then maximum of 65535)" I tried saving the captures in other formats but getting the same resault. How can I open captures taken in linux on windows?</p><p>Thanks, Vered.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '12, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/d623decdbf5d02b7cd8efa81c01d208c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="VeredT&#39;s gravatar image" /><p><span>VeredT</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="VeredT has no accepted answers">0%</span></p></div></div><div id="comments-container-14480" class="comments-container"></div><div id="comment-tools-14480" class="comment-tools"></div><div class="clear"></div><div id="comment-14480-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14481"></span>

<div id="answer-container-14481" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14481-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14481-score" class="post-score" title="current number of votes">2</div><span id="post-14481-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Transfer them using binary mode instead of ASCII mode.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Sep '12, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-14481" class="comments-container"></div><div id="comment-tools-14481" class="comment-tools"></div><div class="clear"></div><div id="comment-14481-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

