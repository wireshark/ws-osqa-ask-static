+++
type = "question"
title = "Round trip plot opening issue"
description = '''When i try to open Reound trip time plot directly, it doenst appear. But when open &#x27;stevensens&#x27; plot first then &#x27;Round trip time&#x27; plot, it can be opened. Is it some bug or something?'''
date = "2016-01-24T22:40:00Z"
lastmod = "2016-01-24T22:40:00Z"
weight = 49497
keywords = [ "graph" ]
aliases = [ "/questions/49497" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Round trip plot opening issue](/questions/49497/round-trip-plot-opening-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49497-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49497-score" class="post-score" title="current number of votes">0</div><span id="post-49497-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When i try to open Reound trip time plot directly, it doenst appear. But when open 'stevensens' plot first then 'Round trip time' plot, it can be opened. Is it some bug or something?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '16, 22:40</strong></p><img src="https://secure.gravatar.com/avatar/0d12b2919a26f63367dda9778fac6a87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alok%20Kumar&#39;s gravatar image" /><p><span>Alok Kumar</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alok Kumar has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jan '16, 01:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-49497" class="comments-container"></div><div id="comment-tools-49497" class="comment-tools"></div><div class="clear"></div><div id="comment-49497-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

