+++
type = "question"
title = "How to edit the resulting file dump?"
description = '''How to edit the resulting file dump. Trim the time until the right moment. I need to send it to technical support, but with this program I unfortunately did not work.'''
date = "2012-12-23T06:44:00Z"
lastmod = "2012-12-27T23:09:00Z"
weight = 17194
keywords = [ "edit", "editcap", "pcap" ]
aliases = [ "/questions/17194" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to edit the resulting file dump?](/questions/17194/how-to-edit-the-resulting-file-dump)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17194-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17194-score" class="post-score" title="current number of votes">0</div><span id="post-17194-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to edit the resulting file dump. Trim the time until the right moment. I need to send it to technical support, but with this program I unfortunately did not work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-edit" rel="tag" title="see questions tagged &#39;edit&#39;">edit</span> <span class="post-tag tag-link-editcap" rel="tag" title="see questions tagged &#39;editcap&#39;">editcap</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Dec '12, 06:44</strong></p><img src="https://secure.gravatar.com/avatar/432afac6bf57c7129dc35d8c35e3ec36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kallikanzarosa&#39;s gravatar image" /><p><span>Kallikanzarosa</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kallikanzarosa has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Dec '12, 07:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-17194" class="comments-container"></div><div id="comment-tools-17194" class="comment-tools"></div><div class="clear"></div><div id="comment-17194-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17195"></span>

<div id="answer-container-17195" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17195-score" class="post-score" title="current number of votes">2</div><span id="post-17195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at <a href="http://www.wireshark.org/docs/man-pages/editcap.html">editcap</a> which comes with Wireshark. The -A and -B options can specify a start and stop time respectively for the output packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Dec '12, 07:10</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-17195" class="comments-container"><span id="17198"></span><div id="comment-17198" class="comment"><div id="post-17198-score" class="comment-score"></div><div class="comment-text"><p>where insert this command?</p></div><div id="comment-17198-info" class="comment-info"><span class="comment-age">(23 Dec '12, 08:13)</span> <span class="comment-user userinfo">Kallikanzarosa</span></div></div><span id="17211"></span><div id="comment-17211" class="comment not_top_scorer"><div id="post-17211-score" class="comment-score"></div><div class="comment-text"><p>it is a command line tool, so you have to run it in a shell. Calling editcap without parameters will give you a help output.</p></div><div id="comment-17211-info" class="comment-info"><span class="comment-age">(23 Dec '12, 18:01)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="17266"></span><div id="comment-17266" class="comment not_top_scorer"><div id="post-17266-score" class="comment-score"></div><div class="comment-text"><p>command it's cmd or in wireshark? I beginner and can't find where insert "editcap"</p></div><div id="comment-17266-info" class="comment-info"><span class="comment-age">(27 Dec '12, 05:13)</span> <span class="comment-user userinfo">Kallikanzarosa</span></div></div><span id="17269"></span><div id="comment-17269" class="comment"><div id="post-17269-score" class="comment-score">1</div><div class="comment-text"><p>cmd, in the same directory where you find the wireshark executable.</p></div><div id="comment-17269-info" class="comment-info"><span class="comment-age">(27 Dec '12, 05:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="17270"></span><div id="comment-17270" class="comment not_top_scorer"><div id="post-17270-score" class="comment-score"></div><div class="comment-text"><p>step by step ?) I can't do it. I must use "START C:\Program Files\Wireshark\editcap.exe" or not?</p></div><div id="comment-17270-info" class="comment-info"><span class="comment-age">(27 Dec '12, 05:31)</span> <span class="comment-user userinfo">Kallikanzarosa</span></div></div><span id="17271"></span><div id="comment-17271" class="comment not_top_scorer"><div id="post-17271-score" class="comment-score"></div><div class="comment-text"><p>pls. I need sent it to zyxel support(</p></div><div id="comment-17271-info" class="comment-info"><span class="comment-age">(27 Dec '12, 06:06)</span> <span class="comment-user userinfo">Kallikanzarosa</span></div></div><span id="17273"></span><div id="comment-17273" class="comment"><div id="post-17273-score" class="comment-score">1</div><div class="comment-text"><blockquote><p>I must use "START C:\Program Files\Wireshark\editcap.exe" or not?</p></blockquote><p>well, you could have tested that yourself, right? But never mind. Here is a step-by-step guide.</p><ul><li>step 1: open a DOS box (read the windows help how to do that)</li><li>step 2: change to the directory where your capture file (input.pcap) is located</li></ul><p><code>cd c:\my_directory_where_the_cap_file_lives</code><br />
</p><ul><li>step 3: type the following in the DOS box<br />
</li></ul><p><code>%programfiles%\wireshark\editcap -A 2012-12-24 08:00 -B 2012-12-24 10:00 input.pcap output.pcap</code><br />
</p><p><strong>WARNING:</strong> The dates are just sample dates. Please use your own!!!<br />
</p><p><strong>HINT</strong> If you don't know what '-A' and '-B' will do for you, please read the man page of editcap!!</p><blockquote><p><code>http://www.wireshark.org/docs/man-pages/editcap.html</code><br />
</p></blockquote><ul><li>step 4: check the file <code>c:\my_directory_where_the_cap_file_lives\output.cap</code> with wireshark</li><li>step 5: if everything is O.K., open your mail client and attach the file <code>c:\my_directory_where_the_cap_file_lives\output.cap</code> into a mail to zyxel support</li><li>step 6: wait for a response</li><li>step 7: come back here if you or zyxel support have any further questions</li></ul><p>Regards<br />
Kurt</p></div><div id="comment-17273-info" class="comment-info"><span class="comment-age">(27 Dec '12, 06:51)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="17274"></span><div id="comment-17274" class="comment"><div id="post-17274-score" class="comment-score">1</div><div class="comment-text"><p>For detailed usage info of editcap see <a href="http://www.wireshark.org/docs/man-pages/editcap.html">here</a>.</p><p>But going back to the original question, it seems to me that you've got a capture file you can load in Wireshark. If that is so then you could note the frame number of the first and last frame of the time span you are interested in sending to zyxel. Then go to the menu File|Save As... and define, at the bottom of the dialog, the packet range using the numbers you noted before.</p></div><div id="comment-17274-info" class="comment-info"><span class="comment-age">(27 Dec '12, 06:54)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="17275"></span><div id="comment-17275" class="comment"><div id="post-17275-score" class="comment-score">1</div><div class="comment-text"><p>Open a command prompt, <code>cd</code> to the directory containing the capture file, and then enter the command <code>c:\program files\wireshark\editcap -A YYYY-MM-DD HH:MM:SS -B YYYY-MM-DD HH:MM:SS infile outfile</code> where infile is the name of your source capture, outfile is the name of the output file, the -A parameter specifies the starting date and time for output packets and -B the ending date and time.</p></div><div id="comment-17275-info" class="comment-info"><span class="comment-age">(27 Dec '12, 06:58)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="17294"></span><div id="comment-17294" class="comment not_top_scorer"><div id="post-17294-score" class="comment-score"></div><div class="comment-text"><p>Time date formet not true (don't work) but I chose packet (333-768) and do it! Thanks!)</p></div><div id="comment-17294-info" class="comment-info"><span class="comment-age">(27 Dec '12, 23:09)</span> <span class="comment-user userinfo">Kallikanzarosa</span></div></div></div><div id="comment-tools-17195" class="comment-tools"><span class="comments-showing"> showing 5 of 10 </span> <a href="#" class="show-all-comments-link">show 5 more comments</a></div><div class="clear"></div><div id="comment-17195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

