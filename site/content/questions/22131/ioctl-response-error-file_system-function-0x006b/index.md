+++
type = "question"
title = "Ioctl Response, Error: FILE_SYSTEM Function: 0x006b"
description = '''Does anyone have a definition for this error? It&#x27;s over protocol SMB2. Preceding it, only a few lines up, I get another error &quot;Ioctl Response, Error: STATUS_NOT_SUPPORTED&quot;'''
date = "2013-06-18T04:29:00Z"
lastmod = "2013-06-18T04:29:00Z"
weight = 22131
keywords = [ "smb2", "smb" ]
aliases = [ "/questions/22131" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ioctl Response, Error: FILE\_SYSTEM Function: 0x006b](/questions/22131/ioctl-response-error-file_system-function-0x006b)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22131-score" class="post-score" title="current number of votes">0</div><span id="post-22131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone have a definition for this error? It's over protocol SMB2. Preceding it, only a few lines up, I get another error "Ioctl Response, Error: STATUS_NOT_SUPPORTED"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smb2" rel="tag" title="see questions tagged &#39;smb2&#39;">smb2</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '13, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/4f2f419887ceff098a8dfe6e71ce70c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abigcoorsman&#39;s gravatar image" /><p><span>abigcoorsman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abigcoorsman has no accepted answers">0%</span></p></div></div><div id="comments-container-22131" class="comments-container"></div><div id="comment-tools-22131" class="comment-tools"></div><div class="clear"></div><div id="comment-22131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

