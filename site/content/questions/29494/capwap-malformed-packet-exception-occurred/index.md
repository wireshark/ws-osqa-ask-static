+++
type = "question"
title = "CAPWAP Malformed Packet (Exception occurred)"
description = '''I keep getting these packets and haven&#x27;t found a reason for it, or a solution. Does anyone have an advice? Thanks.'''
date = "2014-02-06T12:01:00Z"
lastmod = "2014-02-07T07:21:00Z"
weight = 29494
keywords = [ "capwap" ]
aliases = [ "/questions/29494" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [CAPWAP Malformed Packet (Exception occurred)](/questions/29494/capwap-malformed-packet-exception-occurred)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29494-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29494-score" class="post-score" title="current number of votes">0</div><span id="post-29494-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I keep getting these packets and haven't found a reason for it, or a solution. Does anyone have an advice? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capwap" rel="tag" title="see questions tagged &#39;capwap&#39;">capwap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '14, 12:01</strong></p><img src="https://secure.gravatar.com/avatar/ef4e67635f733079ce2fd64e39e835e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bt425&#39;s gravatar image" /><p><span>bt425</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bt425 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Feb '14, 12:02</strong> </span></p></div></div><div id="comments-container-29494" class="comments-container"><span id="29495"></span><div id="comment-29495" class="comment"><div id="post-29495-score" class="comment-score"></div><div class="comment-text"><p>Do you wonder that you see CAPWAP frames, or that they are malformed?</p></div><div id="comment-29495-info" class="comment-info"><span class="comment-age">(06 Feb '14, 12:05)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29496"></span><div id="comment-29496" class="comment"><div id="post-29496-score" class="comment-score"></div><div class="comment-text"><p>I am getting malformed capwap packets and needed to know how to resolve them.</p></div><div id="comment-29496-info" class="comment-info"><span class="comment-age">(06 Feb '14, 13:24)</span> <span class="comment-user userinfo">bt425</span></div></div><span id="29497"></span><div id="comment-29497" class="comment"><div id="post-29497-score" class="comment-score"></div><div class="comment-text"><p>It seems that under WTP Descriptor, subheading Descriptor Value shows &lt;missing&gt;.</p></div><div id="comment-29497-info" class="comment-info"><span class="comment-age">(06 Feb '14, 13:34)</span> <span class="comment-user userinfo">bt425</span></div></div></div><div id="comment-tools-29494" class="comment-tools"></div><div class="clear"></div><div id="comment-29494-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29534"></span>

<div id="answer-container-29534" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29534-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29534-score" class="post-score" title="current number of votes">0</div><span id="post-29534-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I figured it out. Edit::Preferences::Protocols::CAPWAP::Cisco Wireless Controller Support was unchecked. After checking it, I quit getting the errors.</p><p>I found it at this link:</p><p><a href="http://wifinigel.blogspot.com/2012/04/decoding-cisco-capwap-with-wireshark.html">http://wifinigel.blogspot.com/2012/04/decoding-cisco-capwap-with-wireshark.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '14, 07:21</strong></p><img src="https://secure.gravatar.com/avatar/ef4e67635f733079ce2fd64e39e835e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bt425&#39;s gravatar image" /><p><span>bt425</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bt425 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Feb '14, 13:30</strong> </span></p></div></div><div id="comments-container-29534" class="comments-container"></div><div id="comment-tools-29534" class="comment-tools"></div><div class="clear"></div><div id="comment-29534-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

