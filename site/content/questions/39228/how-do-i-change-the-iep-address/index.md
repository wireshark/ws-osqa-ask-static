+++
type = "question"
title = "How do I change the IEP address?"
description = '''I&#x27;m trying to do a capture, but the it has the wrong IEP address on it and I can&#x27;t figure out how to change the IEP address on it. Can you please help me?'''
date = "2015-01-16T17:03:00Z"
lastmod = "2015-01-16T17:03:00Z"
weight = 39228
keywords = [ "iep", "change", "address" ]
aliases = [ "/questions/39228" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I change the IEP address?](/questions/39228/how-do-i-change-the-iep-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39228-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39228-score" class="post-score" title="current number of votes">0</div><span id="post-39228-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to do a capture, but the it has the wrong IEP address on it and I can't figure out how to change the IEP address on it. Can you please help me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iep" rel="tag" title="see questions tagged &#39;iep&#39;">iep</span> <span class="post-tag tag-link-change" rel="tag" title="see questions tagged &#39;change&#39;">change</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '15, 17:03</strong></p><img src="https://secure.gravatar.com/avatar/c59a4ceba956b45ce325126d01d79c72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jackie%20Harkness&#39;s gravatar image" /><p><span>Jackie Harkness</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jackie Harkness has no accepted answers">0%</span></p></div></div><div id="comments-container-39228" class="comments-container"></div><div id="comment-tools-39228" class="comment-tools"></div><div class="clear"></div><div id="comment-39228-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

