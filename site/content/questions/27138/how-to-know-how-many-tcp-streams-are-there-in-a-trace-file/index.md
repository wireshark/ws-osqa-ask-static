+++
type = "question"
title = "how to know how many TCP streams are there in a trace file?"
description = '''I would like to know how many TCP streams are there in a trace file? could anyone help on this? thank you!'''
date = "2013-11-20T01:10:00Z"
lastmod = "2013-11-22T07:21:00Z"
weight = 27138
keywords = [ "tcp_stream" ]
aliases = [ "/questions/27138" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [how to know how many TCP streams are there in a trace file?](/questions/27138/how-to-know-how-many-tcp-streams-are-there-in-a-trace-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27138-score" class="post-score" title="current number of votes">0</div><span id="post-27138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know how many TCP streams are there in a trace file? could anyone help on this?</p><p>thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp_stream" rel="tag" title="see questions tagged &#39;tcp_stream&#39;">tcp_stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Nov '13, 01:10</strong></p><img src="https://secure.gravatar.com/avatar/2d1a8885858c8435654658b25f489bd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveZhou&#39;s gravatar image" /><p><span>SteveZhou</span><br />
<span class="score" title="191 reputation points">191</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveZhou has no accepted answers">0%</span></p></div></div><div id="comments-container-27138" class="comments-container"></div><div id="comment-tools-27138" class="comment-tools"></div><div class="clear"></div><div id="comment-27138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27141"></span>

<div id="answer-container-27141" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27141-score" class="post-score" title="current number of votes">0</div><span id="post-27141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SteveZhou has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you tried the Statistics menu -&gt; Conversations -&gt; TCP Tab? It lists all TCP connections.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '13, 02:08</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27141" class="comments-container"><span id="27152"></span><div id="comment-27152" class="comment"><div id="post-27152-score" class="comment-score"></div><div class="comment-text"><blockquote><p>It lists all TCP connections.</p></blockquote><p>just to complete your answer: <strong>and</strong> the number of the connections in the TCP tab 'title', like: <code>[TCP : 15]</code></p><p>Maybe the OP does not look at the tab title ;-))</p></div><div id="comment-27152-info" class="comment-info"><span class="comment-age">(20 Nov '13, 03:20)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="27280"></span><div id="comment-27280" class="comment"><div id="post-27280-score" class="comment-score"></div><div class="comment-text"><p>nice, thank you!</p></div><div id="comment-27280-info" class="comment-info"><span class="comment-age">(22 Nov '13, 07:21)</span> <span class="comment-user userinfo">SteveZhou</span></div></div></div><div id="comment-tools-27141" class="comment-tools"></div><div class="clear"></div><div id="comment-27141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

