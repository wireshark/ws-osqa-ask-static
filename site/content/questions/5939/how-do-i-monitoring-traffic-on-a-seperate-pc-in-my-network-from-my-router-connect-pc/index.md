+++
type = "question"
title = "How do I monitoring traffic on a seperate pc in my network from my router connect PC?"
description = '''There are three seperate pc&#x27;s in my home network that are connected wirelessly to my wireless router. How do I monitor websites visited from those pc? Thanks in advance, John'''
date = "2011-08-29T19:51:00Z"
lastmod = "2011-08-30T07:47:00Z"
weight = 5939
keywords = [ "wireless", "capture" ]
aliases = [ "/questions/5939" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I monitoring traffic on a seperate pc in my network from my router connect PC?](/questions/5939/how-do-i-monitoring-traffic-on-a-seperate-pc-in-my-network-from-my-router-connect-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5939-score" class="post-score" title="current number of votes">0</div><span id="post-5939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There are three seperate pc's in my home network that are connected wirelessly to my wireless router. How do I monitor websites visited from those pc?</p><p>Thanks in advance,</p><p>John</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '11, 19:51</strong></p><img src="https://secure.gravatar.com/avatar/c2968f209e95d8a488762673f4341578?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bigjohn2005&#39;s gravatar image" /><p><span>bigjohn2005</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bigjohn2005 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>30 Aug '11, 07:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-5939" class="comments-container"></div><div id="comment-tools-5939" class="comment-tools"></div><div class="clear"></div><div id="comment-5939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5957"></span>

<div id="answer-container-5957" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5957-score" class="post-score" title="current number of votes">2</div><span id="post-5957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The <a href="http://wiki.wireshark.org/" title="http://wiki.wireshark.org/">Wireshark Wiki</a> is a good resource to look at. Specifically, read the <a href="http://wiki.wireshark.org/CaptureSetup" title="CaptureSetup">Capture Setup</a> article. The specific setup for capturing traffic in your home will depend on the hardware you already have, but these are good starting points. Additionally, you may want to look at the <a href="http://www.wireshark.org/docs/wsug_html_chunked/" title="User&#39;s Guide">Wireshark User's Guide</a> for more information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '11, 07:47</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span></p></div></div><div id="comments-container-5957" class="comments-container"></div><div id="comment-tools-5957" class="comment-tools"></div><div class="clear"></div><div id="comment-5957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

