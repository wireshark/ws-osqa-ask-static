+++
type = "question"
title = "Capture Filters for 1.7"
description = '''Is it me or is there no capture filter field in 1.7?'''
date = "2011-11-15T16:41:00Z"
lastmod = "2011-11-15T18:55:00Z"
weight = 7453
keywords = [ "capture-filter" ]
aliases = [ "/questions/7453" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Capture Filters for 1.7](/questions/7453/capture-filters-for-17)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7453-score" class="post-score" title="current number of votes">0</div><span id="post-7453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it me or is there no capture filter field in 1.7?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '11, 16:41</strong></p><img src="https://secure.gravatar.com/avatar/f797bdc41d990dca073837114e048b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EricKnaus&#39;s gravatar image" /><p><span>EricKnaus</span><br />
<span class="score" title="46 reputation points">46</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EricKnaus has no accepted answers">0%</span></p></div></div><div id="comments-container-7453" class="comments-container"></div><div id="comment-tools-7453" class="comment-tools"></div><div class="clear"></div><div id="comment-7453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7455"></span>

<div id="answer-container-7455" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7455-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7455-score" class="post-score" title="current number of votes">1</div><span id="post-7455-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's you. ;) Wireshark 1.7 supports capturing from multiple interfaces, so you can apply a different capture filter to each interface if you want. Because of this, the capture filter field was moved to the per-interface settings dialog.</p><p>So, prior to 1.7, you would specify a capture filter using, "Capture -&gt; Options -&gt; Capture Filter", but starting with 1.7, you would use, "Capture -&gt; Options -&gt; [double-click on your interface of choice] -&gt; Capture Filter".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Nov '11, 18:19</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-7455" class="comments-container"><span id="7457"></span><div id="comment-7457" class="comment"><div id="post-7457-score" class="comment-score"></div><div class="comment-text"><p>It's me alright! Cleverly disguised under "Capture Filters".</p><p>Thanks</p></div><div id="comment-7457-info" class="comment-info"><span class="comment-age">(15 Nov '11, 18:55)</span> <span class="comment-user userinfo">EricKnaus</span></div></div></div><div id="comment-tools-7455" class="comment-tools"></div><div class="clear"></div><div id="comment-7455-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

