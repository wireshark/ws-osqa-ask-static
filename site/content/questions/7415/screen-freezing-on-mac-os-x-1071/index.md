+++
type = "question"
title = "Screen &quot;freezing&quot; on Mac os x 10.7.1"
description = '''HI there, I am using wireshark since several years now and this to my entire satisfaction on Windows. I am having troubles on Mac OS X, as when the capture is started, the program seems to freeze and you can hardly use menu options, without waiting endless time, or killing the program. In Windows, t...'''
date = "2011-11-14T07:46:00Z"
lastmod = "2011-11-15T06:57:00Z"
weight = 7415
keywords = [ "osx", "mac", "gui", "troubleshooting", "lion" ]
aliases = [ "/questions/7415" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Screen "freezing" on Mac os x 10.7.1](/questions/7415/screen-freezing-on-mac-os-x-1071)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7415-score" class="post-score" title="current number of votes">0</div><span id="post-7415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI there, I am using wireshark since several years now and this to my entire satisfaction on Windows. I am having troubles on Mac OS X, as when the capture is started, the program seems to freeze and you can hardly use menu options, without waiting endless time, or killing the program. In Windows, the display is behaving smoothly, without problem. I am running the latest stable version 1.6.3 on both Windows and Mac, and Mac os X 10.7.1. Any help or tips greatly appreciated. With kind regards,</p><p>Dominique</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span> <span class="post-tag tag-link-lion" rel="tag" title="see questions tagged &#39;lion&#39;">lion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '11, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/61e6815dfcbd4dcd7bff156f5068ef4f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dte0042&#39;s gravatar image" /><p><span>dte0042</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dte0042 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>15 Nov '11, 06:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/aa651167cb1d51fa9dca1212f1123bfa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bstn&#39;s gravatar image" /><p><span>bstn</span><br />
<span class="score" title="375 reputation points">375</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span></p></div></div><div id="comments-container-7415" class="comments-container"></div><div id="comment-tools-7415" class="comment-tools"></div><div class="clear"></div><div id="comment-7415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7440"></span>

<div id="answer-container-7440" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7440-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7440-score" class="post-score" title="current number of votes">0</div><span id="post-7440-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It sounds like your system is running Wireshark while low on memory. How much RAM do you have available before you start Wireshark? and after?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Nov '11, 06:46</strong></p><img src="https://secure.gravatar.com/avatar/aa651167cb1d51fa9dca1212f1123bfa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bstn&#39;s gravatar image" /><p><span>bstn</span><br />
<span class="score" title="375 reputation points">375</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bstn has 4 accepted answers">14%</span></p></div></div><div id="comments-container-7440" class="comments-container"><span id="7441"></span><div id="comment-7441" class="comment"><div id="post-7441-score" class="comment-score"></div><div class="comment-text"><p>HI, I'm running a macbook pro with 8 gigs of memory, where 3.7 are free. I don't think that it comes from the memory. And, to be complete, I don't think it's linked to 10.7.1, as I remember having this problem before. Unfortunately, some time ago , I had a version that ran fine, but I can't remember which version it was. Thanks and regards, Dominique</p></div><div id="comment-7441-info" class="comment-info"><span class="comment-age">(15 Nov '11, 06:57)</span> <span class="comment-user userinfo">dte0042</span></div></div></div><div id="comment-tools-7440" class="comment-tools"></div><div class="clear"></div><div id="comment-7440-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

