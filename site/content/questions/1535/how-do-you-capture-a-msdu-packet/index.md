+++
type = "question"
title = "how do you capture A-MSDU packet?"
description = '''can you help me capture A-MSDU packet by wwireshark? thamk you very much.'''
date = "2010-12-30T02:44:00Z"
lastmod = "2010-12-31T05:49:00Z"
weight = 1535
keywords = [ "capture" ]
aliases = [ "/questions/1535" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [how do you capture A-MSDU packet?](/questions/1535/how-do-you-capture-a-msdu-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1535-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1535-score" class="post-score" title="current number of votes">0</div><span id="post-1535-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can you help me capture A-MSDU packet by wwireshark? thamk you very much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Dec '10, 02:44</strong></p><img src="https://secure.gravatar.com/avatar/4f406866b1740fdc247ba628d2515395?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haquyen&#39;s gravatar image" /><p><span>haquyen</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haquyen has no accepted answers">0%</span></p></div></div><div id="comments-container-1535" class="comments-container"></div><div id="comment-tools-1535" class="comment-tools"></div><div class="clear"></div><div id="comment-1535-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1545"></span>

<div id="answer-container-1545" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1545-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1545-score" class="post-score" title="current number of votes">0</div><span id="post-1545-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll need the <a href="http://www.cacetech.com/products/airpcap_nx.html">AirPcap Nx</a> adapter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Dec '10, 23:16</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1545" class="comments-container"></div><div id="comment-tools-1545" class="comment-tools"></div><div class="clear"></div><div id="comment-1545-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1555"></span>

<div id="answer-container-1555" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1555-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1555-score" class="post-score" title="current number of votes">0</div><span id="post-1555-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>oh, thanks Jaap</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Dec '10, 05:49</strong></p><img src="https://secure.gravatar.com/avatar/4f406866b1740fdc247ba628d2515395?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haquyen&#39;s gravatar image" /><p><span>haquyen</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haquyen has no accepted answers">0%</span></p></div></div><div id="comments-container-1555" class="comments-container"></div><div id="comment-tools-1555" class="comment-tools"></div><div class="clear"></div><div id="comment-1555-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

