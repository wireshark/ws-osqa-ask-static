+++
type = "question"
title = "What kind of changes should I commit for review?"
description = '''Hi, I&#x27;m working on Wireshark development for my company. We&#x27;re trying to keep it as close to the official release as possible. So, we&#x27;re only adding commits from dev-release as needed to keep it 1.12-like. We&#x27;re also submitting some changes that we deem useful upstream for review into official relea...'''
date = "2015-11-11T07:49:00Z"
lastmod = "2015-11-11T08:55:00Z"
weight = 47512
keywords = [ "development", "dictionary" ]
aliases = [ "/questions/47512" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [What kind of changes should I commit for review?](/questions/47512/what-kind-of-changes-should-i-commit-for-review)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47512-score" class="post-score" title="current number of votes">0</div><span id="post-47512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm working on Wireshark development for my company. We're trying to keep it as close to the official release as possible. So, we're only adding commits from dev-release as needed to keep it 1.12-like. We're also submitting some changes that we deem useful upstream for review into official release. My question is, what kind of changes should be submitted review? As an example, should we keep our own dictionaries of Vendor Specific diameter AVP codes, or should we submit it to the official version instead?</p><p>Thanks up front!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-dictionary" rel="tag" title="see questions tagged &#39;dictionary&#39;">dictionary</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '15, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/a03fa5b340afab78d2e44b63e8dcf3d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aliniel&#39;s gravatar image" /><p><span>Aliniel</span><br />
<span class="score" title="30 reputation points">30</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aliniel has 2 accepted answers">100%</span></p></div></div><div id="comments-container-47512" class="comments-container"></div><div id="comment-tools-47512" class="comment-tools"></div><div class="clear"></div><div id="comment-47512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47516"></span>

<div id="answer-container-47516" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47516-score" class="post-score" title="current number of votes">1</div><span id="post-47516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Aliniel has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It should be ok to submit your vendor specific AVPs and any other changes you have made.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '15, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-47516" class="comments-container"><span id="47517"></span><div id="comment-47517" class="comment"><div id="post-47517-score" class="comment-score"></div><div class="comment-text"><p>Any contribution should be made towards master.</p></div><div id="comment-47517-info" class="comment-info"><span class="comment-age">(11 Nov '15, 08:55)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-47516" class="comment-tools"></div><div class="clear"></div><div id="comment-47516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

