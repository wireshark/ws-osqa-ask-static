+++
type = "question"
title = "802.11r, 802.11k, 802.11u Supported fully? or Partially?"
description = '''Hi all, I have searched through the forums and the help sections and seen reference to these three 802.11 amendments but I haven&#x27;t found definitive statements of support.  Does wireshark + AriPcap decode: 802.11u, 802.11r, 802.11k headers/fields ? Thanks Joey'''
date = "2012-03-09T13:18:00Z"
lastmod = "2012-03-12T06:33:00Z"
weight = 9462
keywords = [ "decode", "802.11u", "802.11k", "802.11r", "802.11" ]
aliases = [ "/questions/9462" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [802.11r, 802.11k, 802.11u Supported fully? or Partially?](/questions/9462/80211r-80211k-80211u-supported-fully-or-partially)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9462-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9462-score" class="post-score" title="current number of votes">0</div><span id="post-9462-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I have searched through the forums and the help sections and seen reference to these three 802.11 amendments but I haven't found definitive statements of support.</p><p>Does wireshark + AriPcap decode:</p><p>802.11u, 802.11r, 802.11k headers/fields ?</p><p>Thanks Joey</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-802.11u" rel="tag" title="see questions tagged &#39;802.11u&#39;">802.11u</span> <span class="post-tag tag-link-802.11k" rel="tag" title="see questions tagged &#39;802.11k&#39;">802.11k</span> <span class="post-tag tag-link-802.11r" rel="tag" title="see questions tagged &#39;802.11r&#39;">802.11r</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '12, 13:18</strong></p><img src="https://secure.gravatar.com/avatar/cf50b8e7fa94f9beb3c1e6a2074ec99f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jpaddencl&#39;s gravatar image" /><p><span>jpaddencl</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jpaddencl has no accepted answers">0%</span></p></div></div><div id="comments-container-9462" class="comments-container"></div><div id="comment-tools-9462" class="comment-tools"></div><div class="clear"></div><div id="comment-9462-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="9472"></span>

<div id="answer-container-9472" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9472-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9472-score" class="post-score" title="current number of votes">1</div><span id="post-9472-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I note that there are references to all three amendments (802.11u, 802.11r, 802.11k) in the <code>packet-ieee80211.c</code> Wireshark source file suggesting that they are supported.</p><p>I've no idea if they are "fully supported".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Mar '12, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-9472" class="comments-container"></div><div id="comment-tools-9472" class="comment-tools"></div><div class="clear"></div><div id="comment-9472-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="9477"></span>

<div id="answer-container-9477" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9477-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9477-score" class="post-score" title="current number of votes">0</div><span id="post-9477-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Bill,</p><p>Thanks for pointing me to packet-ieee80211.c. After reviewing the source and the specs:</p><p>Wireshark 1.7.0 dev branch appears to:</p><p>Support 802.11u fully; Support 802.11r fully; Support 802.11k partially</p><p>The 802.11k information elements I can't find or are not supported for 802.11k are:</p><p>7.3.2.42 Measurement Pilot Transmission Information; 7.3.2.43 BSS Available Admission Capacity; 7.3.2.44 BSS AC Access Delay supported; 7.3.2.45 RRM Enabled Capabilities; 7.3.2.46 Multiple BSSID;<br />
</p><p>When I say fully above, the source appears to contain tags or decode methods for all of the information elements in the spec.</p><p>-Joey</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Mar '12, 17:16</strong></p><img src="https://secure.gravatar.com/avatar/cf50b8e7fa94f9beb3c1e6a2074ec99f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jpaddencl&#39;s gravatar image" /><p><span>jpaddencl</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jpaddencl has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-9477" class="comments-container"><span id="9492"></span><div id="comment-9492" class="comment"><div id="post-9492-score" class="comment-score"></div><div class="comment-text"><p>If you like, feel free to submit a patch (or enhancement request) for the not supported elements at <a href="http://bugs.wireshark.org">bugs.wireshark.org</a>. :)</p></div><div id="comment-9492-info" class="comment-info"><span class="comment-age">(12 Mar '12, 06:33)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-9477" class="comment-tools"></div><div class="clear"></div><div id="comment-9477-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

