+++
type = "question"
title = "TCPTRACE line colors"
description = '''Hello, I am wondering what the line colors in the Sequence Numbers tcptrace graph mean. Am I correct in understanding that green is window size, and orange is sequence? '''
date = "2016-04-12T08:13:00Z"
lastmod = "2016-04-12T08:13:00Z"
weight = 51606
keywords = [ "tcptrace", "wireshark-2.0" ]
aliases = [ "/questions/51606" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCPTRACE line colors](/questions/51606/tcptrace-line-colors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51606-score" class="post-score" title="current number of votes">0</div><span id="post-51606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am wondering what the line colors in the Sequence Numbers tcptrace graph mean. Am I correct in understanding that green is window size, and orange is sequence?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcptrace" rel="tag" title="see questions tagged &#39;tcptrace&#39;">tcptrace</span> <span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '16, 08:13</strong></p><img src="https://secure.gravatar.com/avatar/0f81bc86848ed93cb055d93b25a7d897?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ouoertheo&#39;s gravatar image" /><p><span>ouoertheo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ouoertheo has no accepted answers">0%</span></p></div></div><div id="comments-container-51606" class="comments-container"></div><div id="comment-tools-51606" class="comment-tools"></div><div class="clear"></div><div id="comment-51606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

