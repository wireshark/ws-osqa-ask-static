+++
type = "question"
title = "Online Tutorial for reading packet capture files"
description = '''Hi professionals, is there any online tutorial that teaches beginners on how to read packet captures of a pcap file?'''
date = "2012-04-15T22:03:00Z"
lastmod = "2012-04-15T23:57:00Z"
weight = 10166
keywords = [ "tutorials", "pcap" ]
aliases = [ "/questions/10166" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Online Tutorial for reading packet capture files](/questions/10166/online-tutorial-for-reading-packet-capture-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10166-score" class="post-score" title="current number of votes">0</div><span id="post-10166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi professionals, is there any online tutorial that teaches beginners on how to read packet captures of a pcap file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tutorials" rel="tag" title="see questions tagged &#39;tutorials&#39;">tutorials</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '12, 22:03</strong></p><img src="https://secure.gravatar.com/avatar/94990dfa38fcf1b33157bef842da0291?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="misteryuku&#39;s gravatar image" /><p><span>misteryuku</span><br />
<span class="score" title="20 reputation points">20</span><span title="24 badges"><span class="badge1">●</span><span class="badgecount">24</span></span><span title="26 badges"><span class="silver">●</span><span class="badgecount">26</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="misteryuku has no accepted answers">0%</span></p></div></div><div id="comments-container-10166" class="comments-container"></div><div id="comment-tools-10166" class="comment-tools"></div><div class="clear"></div><div id="comment-10166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10169"></span>

<div id="answer-container-10169" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10169-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10169-score" class="post-score" title="current number of votes">2</div><span id="post-10169-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at the presentations from previous <a href="http://sharkfest.wireshark.org/retrospective.html">SharkFest</a> events. In particular see <a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-1_DuBois-%20I_Just_Downloaded_Wireshark-Now_What.pdf">I've downloaded Wireshark ... Now What?</a> by Betty Dubois.</p><p>Laura Chappell has free online training <a href="http://chappellu.com/schedule.html">here</a> and there is also <a href="http://www.wiresharktraining.com/">Wireshark University</a> and <a href="http://www.lovemytool.com/">LoveMyTool</a> which has videos from SharkFest amongst other things.</p><p>Finally the Wireshark site <a href="http://www.wireshark.org/docs/">Get Help</a> page has all of this and more.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '12, 23:57</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-10169" class="comments-container"></div><div id="comment-tools-10169" class="comment-tools"></div><div class="clear"></div><div id="comment-10169-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

