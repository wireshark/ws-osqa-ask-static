+++
type = "question"
title = "nmake error No protocol registrations found"
description = '''I wrote a simple code of SCTP protocol dissector, but when I nmake it, it had an error, like this: Making plugin.c &amp;lt;using python=&quot;&quot;&amp;gt; No protocol registrations found nmake:fatal error U1077:&quot;C:Python27python.exe&quot; :return code &quot;0x1&quot; stop Who can tell me why? Thank you very much!'''
date = "2011-09-23T02:40:00Z"
lastmod = "2011-09-23T02:48:00Z"
weight = 6505
keywords = [ "nmake", "error" ]
aliases = [ "/questions/6505" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [nmake error No protocol registrations found](/questions/6505/nmake-error-no-protocol-registrations-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6505-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6505-score" class="post-score" title="current number of votes">0</div><span id="post-6505-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I wrote a simple code of SCTP protocol dissector, but when I nmake it, it had an error, like this: Making plugin.c &lt;using python=""&gt; No protocol registrations found nmake:fatal error U1077:"C:Python27python.exe" :return code "0x1" stop</p><p>Who can tell me why? Thank you very much!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nmake" rel="tag" title="see questions tagged &#39;nmake&#39;">nmake</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '11, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/648f9031cad279f5c33963853e123493?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dingding0743&#39;s gravatar image" /><p><span>dingding0743</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dingding0743 has no accepted answers">0%</span></p></div></div><div id="comments-container-6505" class="comments-container"></div><div id="comment-tools-6505" class="comment-tools"></div><div class="clear"></div><div id="comment-6505-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6507"></span>

<div id="answer-container-6507" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6507-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6507-score" class="post-score" title="current number of votes">2</div><span id="post-6507-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Your config.nmake is not correct. Please follow <strong>all</strong> steps in the developer guide. In addition see <code>doc/README.plugins</code> for the steps to add a plugin dissector to build.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '11, 02:48</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6507" class="comments-container"></div><div id="comment-tools-6507" class="comment-tools"></div><div class="clear"></div><div id="comment-6507-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

