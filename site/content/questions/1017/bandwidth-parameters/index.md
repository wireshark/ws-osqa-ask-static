+++
type = "question"
title = "Bandwidth parameters"
description = '''What effect does the increase and decrease in frame size, packet size, delay and data rate have upon bandwidth?'''
date = "2010-11-18T15:37:00Z"
lastmod = "2010-11-19T06:24:00Z"
weight = 1017
keywords = [ "bandwidth", "frame", "packet", "size" ]
aliases = [ "/questions/1017" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bandwidth parameters](/questions/1017/bandwidth-parameters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1017-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1017-score" class="post-score" title="current number of votes">0</div><span id="post-1017-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What effect does the increase and decrease in frame size, packet size, delay and data rate have upon bandwidth?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '10, 15:37</strong></p><img src="https://secure.gravatar.com/avatar/830a3a6650c4763744ee8f0cef0fc044?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adhikaa1&#39;s gravatar image" /><p><span>adhikaa1</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adhikaa1 has no accepted answers">0%</span></p></div></div><div id="comments-container-1017" class="comments-container"><span id="1018"></span><div id="comment-1018" class="comment"><div id="post-1018-score" class="comment-score"></div><div class="comment-text"><p>That all depends on the medium and protocol you're using.</p></div><div id="comment-1018-info" class="comment-info"><span class="comment-age">(18 Nov '10, 22:25)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-1017" class="comment-tools"></div><div class="clear"></div><div id="comment-1017-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1021"></span>

<div id="answer-container-1021" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1021-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1021-score" class="post-score" title="current number of votes">0</div><span id="post-1021-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, first of all you usually have a fixed bandwidth on your way from A to B, and the slowest link counts, because it is the limiting factor. And then you need to see if you can get to using all of it, and that requires you to send as much data as possible in the shortest amount of time possible. Meaning: the larger the <strong>frame size</strong> is and the shorter the <strong>delay</strong> between frames, the higher is your <strong>data rate</strong>, and the more <strong>bandwidth</strong> you will be able to use.</p><p><strong>Frame size</strong> and <strong>packet size</strong> are just two designations on different layers of the OSI modell (frame = ethernet, packet=ip), so they correlate to each other.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Nov '10, 06:24</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Nov '10, 06:30</strong> </span></p></div></div><div id="comments-container-1021" class="comments-container"></div><div id="comment-tools-1021" class="comment-tools"></div><div class="clear"></div><div id="comment-1021-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

