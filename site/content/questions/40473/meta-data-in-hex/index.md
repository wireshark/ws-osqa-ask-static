+++
type = "question"
title = "Meta data in hex"
description = '''Is there a way to include the radiotap and other metadata in the hex dump? Preferably with field highlighting? '''
date = "2015-03-11T08:03:00Z"
lastmod = "2015-03-11T08:03:00Z"
weight = 40473
keywords = [ "metadata" ]
aliases = [ "/questions/40473" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Meta data in hex](/questions/40473/meta-data-in-hex)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40473-score" class="post-score" title="current number of votes">0</div><span id="post-40473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to include the radiotap and other metadata in the hex dump? Preferably with field highlighting?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-metadata" rel="tag" title="see questions tagged &#39;metadata&#39;">metadata</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '15, 08:03</strong></p><img src="https://secure.gravatar.com/avatar/780541b657c7ab4858c684a437cd88da?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="noahfox99&#39;s gravatar image" /><p><span>noahfox99</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="noahfox99 has no accepted answers">0%</span></p></div></div><div id="comments-container-40473" class="comments-container"></div><div id="comment-tools-40473" class="comment-tools"></div><div class="clear"></div><div id="comment-40473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

