+++
type = "question"
title = "How to run a wireless network from a pc using an exsisting wireless network connection?"
description = '''Pretty much as the question says. I know a wireless network can be created with linux on a computer with a wireless adapter, i did that once. Of course it had no internet. What I would like to do, is create a wireless accessable network that routes from my desktop or laptop pc, and run the internet ...'''
date = "2013-05-16T21:37:00Z"
lastmod = "2013-05-16T22:46:00Z"
weight = 21198
keywords = [ "network" ]
aliases = [ "/questions/21198" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to run a wireless network from a pc using an exsisting wireless network connection?](/questions/21198/how-to-run-a-wireless-network-from-a-pc-using-an-exsisting-wireless-network-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21198-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21198-score" class="post-score" title="current number of votes">0</div><span id="post-21198-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Pretty much as the question says. I know a wireless network can be created with linux on a computer with a wireless adapter, i did that once. Of course it had no internet. What I would like to do, is create a wireless accessable network that routes from my desktop or laptop pc, and run the internet from my wireless internet connection from my router into the new wireless connection ran from pc. So like this:</p><p>start: Router Network - PC - PC Network - Another pc/laptop/or more then one (im not taking a direct connection here =P) giving them internet aswell :end</p><p>If any one can help me out, I will most greatly appreciate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '13, 21:37</strong></p><img src="https://secure.gravatar.com/avatar/03311b0fc010d689ce48f76ae8889a44?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BadBitch32&#39;s gravatar image" /><p><span>BadBitch32</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BadBitch32 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 May '13, 21:39</strong> </span></p></div></div><div id="comments-container-21198" class="comments-container"></div><div id="comment-tools-21198" class="comment-tools"></div><div class="clear"></div><div id="comment-21198-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21201"></span>

<div id="answer-container-21201" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21201-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21201-score" class="post-score" title="current number of votes">0</div><span id="post-21201-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When you say PC I am assuming its Windows? If so you put the wireless adapter on your PC into AdHoc mode, give it an SSID, then bridge your wireless and wired adapter. The PC will then act like an access point letting another wireless device connect to the wired network through the PC. Here is a video on how to do the bridging:</p><p><a href="http://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;frm=1&amp;source=web&amp;cd=1&amp;cad=rja&amp;ved=0CDIQtwIwAA&amp;url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D96Z1_6rX5qU&amp;ei=zr2VUff8LJG34AP5x4BA&amp;usg=AFQjCNFARYYExAEta_071L7jau471J_jZg&amp;sig2=VVMnnyXoVmKQ15LDe9w6wg">http://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;frm=1&amp;source=web&amp;cd=1&amp;cad=rja&amp;ved=0CDIQtwIwAA&amp;url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D96Z1_6rX5qU&amp;ei=zr2VUff8LJG34AP5x4BA&amp;usg=AFQjCNFARYYExAEta_071L7jau471J_jZg&amp;sig2=VVMnnyXoVmKQ15LDe9w6wg</a></p><p>It also works in the other direct... say you have a device that only has a ethernet port that you want to connect to a wireless network, for instance an older gaming system. Connect a ethernet cable between the device and a laptop, bridge the wireless and wired adaptors, connect to the wireless network, and you are set!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 May '13, 22:19</strong></p><img src="https://secure.gravatar.com/avatar/0a095f587cc7884054c4edac3d197786?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Andy%20Pando&#39;s gravatar image" /><p><span>Andy Pando</span><br />
<span class="score" title="12 reputation points">12</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Andy Pando has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 May '13, 22:23</strong> </span></p></div></div><div id="comments-container-21201" class="comments-container"><span id="21202"></span><div id="comment-21202" class="comment"><div id="post-21202-score" class="comment-score"></div><div class="comment-text"><p>Thats excellent =) Thank you! Once im established ill try this out and see if everythings working (though I dont see how it wouldnt =P)</p><p>Kudos to you!!!</p><p>Edit: I awarded points to you, this is my first time using this so I just gave the max it would allow me (11) XD Anyways hope you enjoy =P</p></div><div id="comment-21202-info" class="comment-info"><span class="comment-age">(16 May '13, 22:46)</span> <span class="comment-user userinfo">BadBitch32</span></div></div></div><div id="comment-tools-21201" class="comment-tools"></div><div class="clear"></div><div id="comment-21201-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

