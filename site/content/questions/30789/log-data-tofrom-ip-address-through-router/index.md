+++
type = "question"
title = "Log data to/from ip address through router"
description = '''I can&#x27;t figure out correct terms regarding this question so google searching didn&#x27;t help me. Can I log all packets to and from an ip address (or mac address) through a router? I&#x27;m trying to monitor a couple of devices that communicate with each other and log all the data from my computer. I don&#x27;t kn...'''
date = "2014-03-13T23:55:00Z"
lastmod = "2014-03-14T03:50:00Z"
weight = 30789
keywords = [ "logging", "remote" ]
aliases = [ "/questions/30789" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Log data to/from ip address through router](/questions/30789/log-data-tofrom-ip-address-through-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30789-score" class="post-score" title="current number of votes">0</div><span id="post-30789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't figure out correct terms regarding this question so google searching didn't help me.</p><p>Can I log all packets to and from an ip address (or mac address) through a router? I'm trying to monitor a couple of devices that communicate with each other and log all the data from my computer. I don't know if this is possible or not through Wireshark. Regarding the question in general, the only solution I found was an ethernet tap since my router doesn't have data logging capabilities.</p><p>Edit: Forgot to add but not sure if it makes any difference, the devices are on a hub but the computer is not.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-logging" rel="tag" title="see questions tagged &#39;logging&#39;">logging</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '14, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/b31f8a25e65b77f3925b1fa825dbe336?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dirtyrobinson&#39;s gravatar image" /><p><span>dirtyrobinson</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dirtyrobinson has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Mar '14, 00:42</strong> </span></p></div></div><div id="comments-container-30789" class="comments-container"></div><div id="comment-tools-30789" class="comment-tools"></div><div class="clear"></div><div id="comment-30789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30798"></span>

<div id="answer-container-30798" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30798-score" class="post-score" title="current number of votes">0</div><span id="post-30798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Mar '14, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Mar '14, 03:52</strong> </span></p></div></div><div id="comments-container-30798" class="comments-container"></div><div id="comment-tools-30798" class="comment-tools"></div><div class="clear"></div><div id="comment-30798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

