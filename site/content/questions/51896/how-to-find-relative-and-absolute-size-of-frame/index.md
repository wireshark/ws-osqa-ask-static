+++
type = "question"
title = "How to find relative and absolute size of frame?"
description = '''Any idea about How to find relative and absolute size of frame in bytes?'''
date = "2016-04-23T10:48:00Z"
lastmod = "2016-04-23T10:53:00Z"
weight = 51896
keywords = [ "size" ]
aliases = [ "/questions/51896" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to find relative and absolute size of frame?](/questions/51896/how-to-find-relative-and-absolute-size-of-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51896-score" class="post-score" title="current number of votes">0</div><span id="post-51896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Any idea about How to find relative and absolute size of frame in bytes?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '16, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/203e3d3c82645f6b633c50d9c815d52a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="usmankhan4619&#39;s gravatar image" /><p><span>usmankhan4619</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="usmankhan4619 has no accepted answers">0%</span></p></div></div><div id="comments-container-51896" class="comments-container"><span id="51897"></span><div id="comment-51897" class="comment"><div id="post-51897-score" class="comment-score"></div><div class="comment-text"><p>what is your definition of relative and absolute size? I only know capture size and wire size, both of which you can see in the first section of the decode pane.</p></div><div id="comment-51897-info" class="comment-info"><span class="comment-age">(23 Apr '16, 10:53)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-51896" class="comment-tools"></div><div class="clear"></div><div id="comment-51896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

