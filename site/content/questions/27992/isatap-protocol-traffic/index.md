+++
type = "question"
title = "isatap protocol traffic"
description = '''Hi  I&#x27;m looking for some Pcaps of isatap .  i tried at www.pcapr.net but i cant find it there. is there any chance someone has it ? it will be very helpful !  Thanks, Barak'''
date = "2013-12-11T00:07:00Z"
lastmod = "2013-12-11T00:07:00Z"
weight = 27992
keywords = [ "tunnel", "ipv6" ]
aliases = [ "/questions/27992" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [isatap protocol traffic](/questions/27992/isatap-protocol-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27992-score" class="post-score" title="current number of votes">0</div><span id="post-27992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I'm looking for some Pcaps of isatap . i tried at www.pcapr.net but i cant find it there. is there any chance someone has it ? it will be very helpful !</p><p>Thanks, Barak</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tunnel" rel="tag" title="see questions tagged &#39;tunnel&#39;">tunnel</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Dec '13, 00:07</strong></p><img src="https://secure.gravatar.com/avatar/b2708d60189cbe26ed91ca1cd9658ee1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Barak_A&#39;s gravatar image" /><p><span>Barak_A</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Barak_A has no accepted answers">0%</span></p></div></div><div id="comments-container-27992" class="comments-container"></div><div id="comment-tools-27992" class="comment-tools"></div><div class="clear"></div><div id="comment-27992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

