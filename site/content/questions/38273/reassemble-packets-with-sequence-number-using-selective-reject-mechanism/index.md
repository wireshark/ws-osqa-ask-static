+++
type = "question"
title = "Reassemble packets with sequence Number using selective reject mechanism"
description = '''Hi Need your advice ! Iam developing custom dissector where it used Selectie Reject Mechanism for transmission and receive. I got reference of 1. I flag 2. Poll flag 3. recieve Sequence Number 4. Send Sequence Number  Could anyone help me to give some example code or any example for this please? Raj'''
date = "2014-12-01T22:29:00Z"
lastmod = "2014-12-01T22:29:00Z"
weight = 38273
keywords = [ "selective", "reassemble", "reject" ]
aliases = [ "/questions/38273" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Reassemble packets with sequence Number using selective reject mechanism](/questions/38273/reassemble-packets-with-sequence-number-using-selective-reject-mechanism)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38273-score" class="post-score" title="current number of votes">0</div><span id="post-38273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Need your advice !</p><p>Iam developing custom dissector where it used Selectie Reject Mechanism for transmission and receive.</p><p>I got reference of 1. I flag 2. Poll flag 3. recieve Sequence Number 4. Send Sequence Number</p><p>Could anyone help me to give some example code or any example for this please?</p><p>Raj</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-selective" rel="tag" title="see questions tagged &#39;selective&#39;">selective</span> <span class="post-tag tag-link-reassemble" rel="tag" title="see questions tagged &#39;reassemble&#39;">reassemble</span> <span class="post-tag tag-link-reject" rel="tag" title="see questions tagged &#39;reject&#39;">reject</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '14, 22:29</strong></p><img src="https://secure.gravatar.com/avatar/1339589a92af9455063c09e56bfc6299?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="umar&#39;s gravatar image" /><p><span>umar</span><br />
<span class="score" title="26 reputation points">26</span><span title="22 badges"><span class="badge1">●</span><span class="badgecount">22</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="27 badges"><span class="bronze">●</span><span class="badgecount">27</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="umar has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Dec '14, 22:33</strong> </span></p></div></div><div id="comments-container-38273" class="comments-container"></div><div id="comment-tools-38273" class="comment-tools"></div><div class="clear"></div><div id="comment-38273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

