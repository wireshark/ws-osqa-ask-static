+++
type = "question"
title = "Capturing skype voice traffic - reliability?"
description = '''A skype resolver gives a location in Ohio for a particular Skype ID from a recent database. Wireshark also gives a location in Ohio when in voice conversation with the same user and capturing UDP traffic through my local skype port. The IP resolves to a domestic connection. I doubt that the user is ...'''
date = "2017-10-08T07:46:00Z"
lastmod = "2017-10-08T07:46:00Z"
weight = 63741
keywords = [ "resolver", "skype" ]
aliases = [ "/questions/63741" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing skype voice traffic - reliability?](/questions/63741/capturing-skype-voice-traffic-reliability)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63741-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63741-score" class="post-score" title="current number of votes">0</div><span id="post-63741-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A skype resolver gives a location in Ohio for a particular Skype ID from a recent database. Wireshark also gives a location in Ohio when in voice conversation with the same user and capturing UDP traffic through my local skype port. The IP resolves to a domestic connection. I doubt that the user is especially knowledgable about IT, IPs etc. Is there any way the user could NOT be in Ohio? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-resolver" rel="tag" title="see questions tagged &#39;resolver&#39;">resolver</span> <span class="post-tag tag-link-skype" rel="tag" title="see questions tagged &#39;skype&#39;">skype</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '17, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/4946b8e04e52952b64cfce4f39644f36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lingychops99&#39;s gravatar image" /><p><span>lingychops99</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lingychops99 has no accepted answers">0%</span></p></div></div><div id="comments-container-63741" class="comments-container"></div><div id="comment-tools-63741" class="comment-tools"></div><div class="clear"></div><div id="comment-63741-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

