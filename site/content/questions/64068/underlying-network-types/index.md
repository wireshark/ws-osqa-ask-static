+++
type = "question"
title = "Underlying Network Types"
description = '''I have a saved capture traffic flow in WireShark and I need to know what type of network does the captured traffic appear to be (e.g., a large corporation, an ISP backbone, etc.)? How can I go about knowing this information from all the packets that&#x27;s been captured'''
date = "2017-10-21T12:40:00Z"
lastmod = "2017-10-22T01:47:00Z"
weight = 64068
keywords = [ "network" ]
aliases = [ "/questions/64068" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Underlying Network Types](/questions/64068/underlying-network-types)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64068-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64068-score" class="post-score" title="current number of votes">0</div><span id="post-64068-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a saved capture traffic flow in WireShark and I need to know what type of network does the captured traffic appear to be (e.g., a large corporation, an ISP backbone, etc.)? How can I go about knowing this information from all the packets that's been captured</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '17, 12:40</strong></p><img src="https://secure.gravatar.com/avatar/64bda4b68f36a91bd7bccdb0de4c95ee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TNT42&#39;s gravatar image" /><p><span>TNT42</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TNT42 has no accepted answers">0%</span></p></div></div><div id="comments-container-64068" class="comments-container"></div><div id="comment-tools-64068" class="comment-tools"></div><div class="clear"></div><div id="comment-64068-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64079"></span>

<div id="answer-container-64079" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64079-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64079-score" class="post-score" title="current number of votes">0</div><span id="post-64079-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like homework to me...</p><p>I'll give some hints: Look at IP addresses, presence of VLAN tagging, presence of MPLS labels, etc.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '17, 01:47</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-64079" class="comments-container"></div><div id="comment-tools-64079" class="comment-tools"></div><div class="clear"></div><div id="comment-64079-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

