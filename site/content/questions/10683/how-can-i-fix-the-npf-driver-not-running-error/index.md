+++
type = "question"
title = "How can I fix the NPF Driver not running error?"
description = '''I keep getting a message when I start Wireshark that the NPF driver is not running. Nothing that I have done has been able to get this to run. The latest version of WinPcap is installed on my laptop. What can I do? '''
date = "2012-05-04T06:47:00Z"
lastmod = "2012-05-04T13:15:00Z"
weight = 10683
keywords = [ "windows", "npf" ]
aliases = [ "/questions/10683" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I fix the NPF Driver not running error?](/questions/10683/how-can-i-fix-the-npf-driver-not-running-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10683-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10683-score" class="post-score" title="current number of votes">0</div><span id="post-10683-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I keep getting a message when I start Wireshark that the NPF driver is not running. Nothing that I have done has been able to get this to run. The latest version of WinPcap is installed on my laptop. What can I do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-npf" rel="tag" title="see questions tagged &#39;npf&#39;">npf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '12, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/2c1bf5fd9017af2b323beb70ed449205?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emjohnson1125&#39;s gravatar image" /><p><span>emjohnson1125</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emjohnson1125 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 May '12, 13:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-10683" class="comments-container"><span id="10688"></span><div id="comment-10688" class="comment"><div id="post-10688-score" class="comment-score"></div><div class="comment-text"><p>Have you tried the answer to <a href="http://ask.wireshark.org/questions/1281">this similar question</a>?</p></div><div id="comment-10688-info" class="comment-info"><span class="comment-age">(04 May '12, 13:15)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-10683" class="comment-tools"></div><div class="clear"></div><div id="comment-10683-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

