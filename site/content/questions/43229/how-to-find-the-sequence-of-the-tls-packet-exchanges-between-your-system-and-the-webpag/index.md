+++
type = "question"
title = "how to Find the sequence of the TLS packet exchanges between your system and the webpag"
description = '''how to Find the sequence of the TLS packet exchanges between your system and the webpag?'''
date = "2015-06-16T23:30:00Z"
lastmod = "2015-06-17T06:43:00Z"
weight = 43229
keywords = [ "1368" ]
aliases = [ "/questions/43229" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to Find the sequence of the TLS packet exchanges between your system and the webpag](/questions/43229/how-to-find-the-sequence-of-the-tls-packet-exchanges-between-your-system-and-the-webpag)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43229-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43229-score" class="post-score" title="current number of votes">0</div><span id="post-43229-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to Find the sequence of the TLS packet exchanges between your system and the webpag?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1368" rel="tag" title="see questions tagged &#39;1368&#39;">1368</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jun '15, 23:30</strong></p><img src="https://secure.gravatar.com/avatar/1689f775ca0bb060ff14b469e2cfe127?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mnaderipoor&#39;s gravatar image" /><p><span>mnaderipoor</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mnaderipoor has no accepted answers">0%</span></p></div></div><div id="comments-container-43229" class="comments-container"><span id="43252"></span><div id="comment-43252" class="comment"><div id="post-43252-score" class="comment-score"></div><div class="comment-text"><p>Can you please add more details what you are looking for?</p></div><div id="comment-43252-info" class="comment-info"><span class="comment-age">(17 Jun '15, 06:43)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-43229" class="comment-tools"></div><div class="clear"></div><div id="comment-43229-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

