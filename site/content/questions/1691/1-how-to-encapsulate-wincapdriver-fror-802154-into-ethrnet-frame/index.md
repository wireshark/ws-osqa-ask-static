+++
type = "question"
title = "1 how to encapsulate wincapdriver fror 802.15.4   into ethrnet  frame"
description = '''Figure out how to supply data to Winpcap driver for 802.15.4 (You will need to encapsulate it in an Ethernet II frame)'''
date = "2011-01-10T02:26:00Z"
lastmod = "2011-03-22T08:10:00Z"
weight = 1691
keywords = [ "wincap" ]
aliases = [ "/questions/1691" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [1 how to encapsulate wincapdriver fror 802.15.4 into ethrnet frame](/questions/1691/1-how-to-encapsulate-wincapdriver-fror-802154-into-ethrnet-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1691-score" class="post-score" title="current number of votes">0</div><span id="post-1691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Figure out how to supply data to Winpcap driver for 802.15.4 (You will need to encapsulate it in an Ethernet II frame)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wincap" rel="tag" title="see questions tagged &#39;wincap&#39;">wincap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jan '11, 02:26</strong></p><img src="https://secure.gravatar.com/avatar/7bd3c1b963f62875a2c8f9ff480e5abc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sreeram&#39;s gravatar image" /><p><span>sreeram</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sreeram has no accepted answers">0%</span></p></div></div><div id="comments-container-1691" class="comments-container"><span id="3026"></span><div id="comment-3026" class="comment"><div id="post-3026-score" class="comment-score"></div><div class="comment-text"><p>Is there a Wireshark-related question here?</p></div><div id="comment-3026-info" class="comment-info"><span class="comment-age">(22 Mar '11, 08:10)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-1691" class="comment-tools"></div><div class="clear"></div><div id="comment-1691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

