+++
type = "question"
title = "Why does ftp not have header?"
description = '''The title is my question. why ftp does not have header? (when use stream mode to send file). Is tcp &quot;shared&quot; header with tcp. how wireshark can distinguish tcp and ftp?'''
date = "2016-10-25T09:54:00Z"
lastmod = "2016-10-25T09:59:00Z"
weight = 56650
keywords = [ "ftp" ]
aliases = [ "/questions/56650" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why does ftp not have header?](/questions/56650/why-does-ftp-not-have-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56650-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56650-score" class="post-score" title="current number of votes">0</div><span id="post-56650-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The title is my question. why ftp does not have header? (when use stream mode to send file). Is tcp "shared" header with tcp. how wireshark can distinguish tcp and ftp?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '16, 09:54</strong></p><img src="https://secure.gravatar.com/avatar/415c78388493e3c9c225a9f6e52b522e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kyuubi&#39;s gravatar image" /><p><span>Kyuubi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kyuubi has no accepted answers">0%</span></p></div></div><div id="comments-container-56650" class="comments-container"></div><div id="comment-tools-56650" class="comment-tools"></div><div class="clear"></div><div id="comment-56650-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56651"></span>

<div id="answer-container-56651" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56651-score" class="post-score" title="current number of votes">1</div><span id="post-56651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Port numbers and communication between the ftp client and server in the command channel (port 21 on the server). See <a href="http://stackoverflow.com/questions/1699145/what-is-the-difference-between-active-and-passive-ftp">here</a> for an explanation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Oct '16, 09:59</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-56651" class="comments-container"></div><div id="comment-tools-56651" class="comment-tools"></div><div class="clear"></div><div id="comment-56651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

