+++
type = "question"
title = "bandwidth utilization"
description = '''Hello  I have very busy linux server with a specific application which is network focused and I want to check is there any bottleneck on network layer and also wants to get application and/or os+application (a single value is enough for now). If I clean up that there is no network issue I could focu...'''
date = "2017-05-29T07:31:00Z"
lastmod = "2017-05-29T07:31:00Z"
weight = 61686
keywords = [ "bandwidthutilization" ]
aliases = [ "/questions/61686" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [bandwidth utilization](/questions/61686/bandwidth-utilization)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61686-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61686-score" class="post-score" title="current number of votes">0</div><span id="post-61686-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I have very busy linux server with a specific application which is network focused and I want to check is there any bottleneck on network layer and also wants to get application and/or os+application (a single value is enough for now). If I clean up that there is no network issue I could focus into other components of the server.</p><p>I could run tcpdump and also could exam it on wireshark I need tcpdump options and wireshark usage examples for my test ?</p><p>Any help would be appreciated</p><p>Thanks Murat</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidthutilization" rel="tag" title="see questions tagged &#39;bandwidthutilization&#39;">bandwidthutilization</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '17, 07:31</strong></p><img src="https://secure.gravatar.com/avatar/699191df7e66b3c40ac0503e677606c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="msuluhan&#39;s gravatar image" /><p><span>msuluhan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="msuluhan has no accepted answers">0%</span></p></div></div><div id="comments-container-61686" class="comments-container"></div><div id="comment-tools-61686" class="comment-tools"></div><div class="clear"></div><div id="comment-61686-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

