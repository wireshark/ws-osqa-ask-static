+++
type = "question"
title = "response dropped"
description = '''Please bare with me, I honestly don&#x27;t know how to describe my problem cause I&#x27;m a newb. I&#x27;ve been using wireshark to capture my session ID fro!m an android game. Now, my friend does the same thing on the same game. Today I went to get the session ID and the packet isn&#x27;t there. His works fine, but no...'''
date = "2014-12-31T22:24:00Z"
lastmod = "2014-12-31T22:24:00Z"
weight = 38839
keywords = [ "capture", "http.response" ]
aliases = [ "/questions/38839" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [response dropped](/questions/38839/response-dropped)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38839-score" class="post-score" title="current number of votes">0</div><span id="post-38839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please bare with me, I honestly don't know how to describe my problem cause I'm a newb.</p><p>I've been using wireshark to capture my session ID fro!m an android game.</p><p>Now, my friend does the same thing on the same game.</p><p>Today I went to get the session ID and the packet isn't there.</p><p>His works fine, but not mine. He said its 'as if it dropped the response. "</p><p>If I understand him right, the request was captured but not the response.</p><p>I'd be grateful for any help cause its a huge problem for me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-http.response" rel="tag" title="see questions tagged &#39;http.response&#39;">http.response</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '14, 22:24</strong></p><img src="https://secure.gravatar.com/avatar/7c712541ba8f92a82064ed60cd09bf6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nifty62&#39;s gravatar image" /><p><span>Nifty62</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nifty62 has no accepted answers">0%</span></p></div></div><div id="comments-container-38839" class="comments-container"></div><div id="comment-tools-38839" class="comment-tools"></div><div class="clear"></div><div id="comment-38839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

