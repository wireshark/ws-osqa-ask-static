+++
type = "question"
title = "largest based conversation"
description = '''how to check the size of largest based conversations in TCP and UDP ???'''
date = "2013-03-23T07:59:00Z"
lastmod = "2013-03-23T09:45:00Z"
weight = 19769
keywords = [ "sun1" ]
aliases = [ "/questions/19769" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [largest based conversation](/questions/19769/largest-based-conversation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19769-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19769-score" class="post-score" title="current number of votes">0</div><span id="post-19769-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to check the size of largest based conversations in TCP and UDP ???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sun1" rel="tag" title="see questions tagged &#39;sun1&#39;">sun1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '13, 07:59</strong></p><img src="https://secure.gravatar.com/avatar/df3c13117e66cbe6771a23abb883af9a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ramyarani&#39;s gravatar image" /><p><span>Ramyarani</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ramyarani has no accepted answers">0%</span></p></div></div><div id="comments-container-19769" class="comments-container"></div><div id="comment-tools-19769" class="comment-tools"></div><div class="clear"></div><div id="comment-19769-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19774"></span>

<div id="answer-container-19774" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19774-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19774-score" class="post-score" title="current number of votes">0</div><span id="post-19774-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From Menu Bar Statistics &gt; conversations click on tcp or udp and double click on bytes field for largest data stream.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '13, 09:45</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-19774" class="comments-container"></div><div id="comment-tools-19774" class="comment-tools"></div><div class="clear"></div><div id="comment-19774-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

