+++
type = "question"
title = "How to write SCTP chunk flags, type, PPI, etc into the info column and how to add an info2 column??"
description = '''Hi, I was able to make a dissector for the chunk data itself (via PPI) but I would like to write some info about the chunk itself to the Info column if it is possible. Or even better: add a new column and write some bytes from the chunk info into it. The function myprot.dissector(buffer,pinfo,tree)s...'''
date = "2013-12-12T02:25:00Z"
lastmod = "2013-12-12T02:25:00Z"
weight = 28044
keywords = [ "info", "sctp", "dissector" ]
aliases = [ "/questions/28044" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to write SCTP chunk flags, type, PPI, etc into the info column and how to add an info2 column??](/questions/28044/how-to-write-sctp-chunk-flags-type-ppi-etc-into-the-info-column-and-how-to-add-an-info2-column)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28044-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28044-score" class="post-score" title="current number of votes">0</div><span id="post-28044-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I was able to make a dissector for the chunk data itself (via PPI) but I would like to write some info about the chunk itself to the Info column if it is possible. Or even better: add a new column and write some bytes from the chunk info into it. The function myprot.dissector(buffer,pinfo,tree)s buffer contains only the data part of the chunk and not the info part of it.</p><p>Thank you for your help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-sctp" rel="tag" title="see questions tagged &#39;sctp&#39;">sctp</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Dec '13, 02:25</strong></p><img src="https://secure.gravatar.com/avatar/635081e36d3e95e6abb9c318a8e44337?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alma001&#39;s gravatar image" /><p><span>alma001</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alma001 has no accepted answers">0%</span></p></div></div><div id="comments-container-28044" class="comments-container"></div><div id="comment-tools-28044" class="comment-tools"></div><div class="clear"></div><div id="comment-28044-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

