+++
type = "question"
title = "How did I to know the time cost of each function in wireshark?"
description = '''How did I to know the time cost of each function in wireshark? and then I can debug the which function have take so long time when wireshark running. 1,In linux ,I want to use gprof to detect the function. So I change the makefile:add -pg to compile and link process.but it is not work.I can&#x27;t get th...'''
date = "2013-02-28T01:11:00Z"
lastmod = "2013-02-28T01:11:00Z"
weight = 18964
keywords = [ "function", "profile", "gprof", "time" ]
aliases = [ "/questions/18964" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How did I to know the time cost of each function in wireshark?](/questions/18964/how-did-i-to-know-the-time-cost-of-each-function-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18964-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18964-score" class="post-score" title="current number of votes">0</div><span id="post-18964-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How did I to know the time cost of each function in wireshark? and then I can debug the which function have take so long time when wireshark running.</p><p>1,In linux ,I want to use gprof to detect the function. So I change the makefile:add -pg to compile and link process.but it is not work.I can't get the gmon.out.</p><p>2,In XP,I use vs2010 performance profile to detect.after I finish all step ,then come some error:the *.vsp have no data.</p><p>How can I find the function? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-function" rel="tag" title="see questions tagged &#39;function&#39;">function</span> <span class="post-tag tag-link-profile" rel="tag" title="see questions tagged &#39;profile&#39;">profile</span> <span class="post-tag tag-link-gprof" rel="tag" title="see questions tagged &#39;gprof&#39;">gprof</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '13, 01:11</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-18964" class="comments-container"></div><div id="comment-tools-18964" class="comment-tools"></div><div class="clear"></div><div id="comment-18964-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

