+++
type = "question"
title = "Wireshark &amp; Iphone"
description = '''Will wireshark be able to tell me what websites or apps my wife is looking at on her iphone'''
date = "2014-01-21T02:24:00Z"
lastmod = "2014-01-21T05:24:00Z"
weight = 29048
keywords = [ "surveilance", "iphone", "wife" ]
aliases = [ "/questions/29048" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark & Iphone](/questions/29048/wireshark-iphone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29048-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29048-score" class="post-score" title="current number of votes">0</div><span id="post-29048-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Will wireshark be able to tell me what websites or apps my wife is looking at on her iphone</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-surveilance" rel="tag" title="see questions tagged &#39;surveilance&#39;">surveilance</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span> <span class="post-tag tag-link-wife" rel="tag" title="see questions tagged &#39;wife&#39;">wife</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '14, 02:24</strong></p><img src="https://secure.gravatar.com/avatar/34dde733c4d3f7ceab31caf49152c762?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="James%20Thompson&#39;s gravatar image" /><p><span>James Thompson</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="James Thompson has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Jan '14, 02:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-29048" class="comments-container"><span id="29058"></span><div id="comment-29058" class="comment"><div id="post-29058-score" class="comment-score"></div><div class="comment-text"><p>Sure, see the answer of <span>@Jasper</span>:</p><p>But I think your wife will be able to tell you that in much more detail and she would also be able to give you an explanation why she is doing. So, just ask her, instead of 'spying' her activities. ;-))</p></div><div id="comment-29058-info" class="comment-info"><span class="comment-age">(21 Jan '14, 05:24)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29048" class="comment-tools"></div><div class="clear"></div><div id="comment-29048-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29049"></span>

<div id="answer-container-29049" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29049-score" class="post-score" title="current number of votes">0</div><span id="post-29049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, maybe. You need to be able to</p><ol><li>Capture wireless packets, especially others than your own. This requires the right equipment and skill</li><li>If the things she does are encrypted you need to decrypt the packets, which usually requires a court order to force the service/app provider to hand the encryption key over. Good luck with that (unless you're the NSA, of course).</li><li>You need to be able to read packets and assemble content, and be able to make sense of it.</li></ol><p>See this similar question: <a href="http://ask.wireshark.org/questions/24038/can-i-use-wireshark-to-see-iphoneipad-internet-use-if-it-is-using-my-wi-fi">http://ask.wireshark.org/questions/24038/can-i-use-wireshark-to-see-iphoneipad-internet-use-if-it-is-using-my-wi-fi</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jan '14, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-29049" class="comments-container"></div><div id="comment-tools-29049" class="comment-tools"></div><div class="clear"></div><div id="comment-29049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

