+++
type = "question"
title = "Automate changing IP/MAC addresses in Profiles"
description = '''Perhaps useful to this audience, perhaps not. I wrote a script, update-ws-profiles, which automates changing IP/MAC addresses in .wireshark/profiles//preferences, typically employed in filters. Windows and nix versions both in the .zip file. http://www.skendric.com/seminar/#Mechanics'''
date = "2015-01-14T07:17:00Z"
lastmod = "2015-01-14T07:17:00Z"
weight = 39126
keywords = [ "profile", "preference", "script" ]
aliases = [ "/questions/39126" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Automate changing IP/MAC addresses in Profiles](/questions/39126/automate-changing-ipmac-addresses-in-profiles)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39126-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39126-score" class="post-score" title="current number of votes">0</div><span id="post-39126-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Perhaps useful to this audience, perhaps not.</p><p>I wrote a script, update-ws-profiles, which automates changing IP/MAC addresses in .wireshark/profiles/<em>/preferences, typically employed in filters. Windows and</em> nix versions both in the .zip file.</p><p><a href="http://www.skendric.com/seminar/#Mechanics">http://www.skendric.com/seminar/#Mechanics</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-profile" rel="tag" title="see questions tagged &#39;profile&#39;">profile</span> <span class="post-tag tag-link-preference" rel="tag" title="see questions tagged &#39;preference&#39;">preference</span> <span class="post-tag tag-link-script" rel="tag" title="see questions tagged &#39;script&#39;">script</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '15, 07:17</strong></p><img src="https://secure.gravatar.com/avatar/18ae5b8bfddad49931ec557b9342075a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skendric&#39;s gravatar image" /><p><span>skendric</span><br />
<span class="score" title="11 reputation points">11</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skendric has no accepted answers">0%</span></p></div></div><div id="comments-container-39126" class="comments-container"></div><div id="comment-tools-39126" class="comment-tools"></div><div class="clear"></div><div id="comment-39126-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

