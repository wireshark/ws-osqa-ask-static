+++
type = "question"
title = "file saves differently?"
description = '''I have a packet capture file and open it in wireshark and apply a filter. The resulting file is different when I save in 1.4.10 vs 1.6.3. Any idea why this is?'''
date = "2011-11-08T12:54:00Z"
lastmod = "2011-11-08T21:16:00Z"
weight = 7289
keywords = [ "filesave" ]
aliases = [ "/questions/7289" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [file saves differently?](/questions/7289/file-saves-differently)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7289-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7289-score" class="post-score" title="current number of votes">0</div><span id="post-7289-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a packet capture file and open it in wireshark and apply a filter. The resulting file is different when I save in 1.4.10 vs 1.6.3. Any idea why this is?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filesave" rel="tag" title="see questions tagged &#39;filesave&#39;">filesave</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '11, 12:54</strong></p><img src="https://secure.gravatar.com/avatar/d8eb37ad8f02e6f71516f51844ccafc7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joe%20Garry&#39;s gravatar image" /><p><span>Joe Garry</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joe Garry has no accepted answers">0%</span></p></div></div><div id="comments-container-7289" class="comments-container"><span id="7305"></span><div id="comment-7305" class="comment"><div id="post-7305-score" class="comment-score"></div><div class="comment-text"><p>What are the differences?</p></div><div id="comment-7305-info" class="comment-info"><span class="comment-age">(08 Nov '11, 19:04)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div></div><div id="comment-tools-7289" class="comment-tools"></div><div class="clear"></div><div id="comment-7289-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7312"></span>

<div id="answer-container-7312" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7312-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7312-score" class="post-score" title="current number of votes">0</div><span id="post-7312-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>may be few bugs were fixed. I'm aware that 1.4.6 does not detect HTTP packets if they fit into whole TCP segment both with FIN and ACK. sure there are other issues, that's why we have software updates.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 21:16</strong></p><img src="https://secure.gravatar.com/avatar/35d96b8e73e6deb4e332d076fd3269b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShomeaX&#39;s gravatar image" /><p><span>ShomeaX</span><br />
<span class="score" title="73 reputation points">73</span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShomeaX has no accepted answers">0%</span></p></div></div><div id="comments-container-7312" class="comments-container"></div><div id="comment-tools-7312" class="comment-tools"></div><div class="clear"></div><div id="comment-7312-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

