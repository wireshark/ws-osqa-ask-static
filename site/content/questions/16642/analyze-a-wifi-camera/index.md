+++
type = "question"
title = "Analyze a wifi camera"
description = '''Is it possible to analyze the communication from a wifi CCTV camera. It does not communicate with the FTP server on internet. I think it don&#x27;t even try to do, and that is the problem. Can I monitor the handshake from the camera to the router and out? May be with a Ethernet cable from camera to route...'''
date = "2012-12-06T09:53:00Z"
lastmod = "2012-12-06T09:53:00Z"
weight = 16642
keywords = [ "ftp" ]
aliases = [ "/questions/16642" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Analyze a wifi camera](/questions/16642/analyze-a-wifi-camera)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16642-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16642-score" class="post-score" title="current number of votes">0</div><span id="post-16642-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to analyze the communication from a wifi CCTV camera. It does not communicate with the FTP server on internet. I think it don't even try to do, and that is the problem. Can I monitor the handshake from the camera to the router and out? May be with a Ethernet cable from camera to router via a HUB and play 'Man in the middle'. But it's almost impossible from where the camera are placed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '12, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/71ca63a3c7daf7193d2b2fd03ff79cfc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HowTo&#39;s gravatar image" /><p><span>HowTo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HowTo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Dec '12, 09:56</strong> </span></p></div></div><div id="comments-container-16642" class="comments-container"></div><div id="comment-tools-16642" class="comment-tools"></div><div class="clear"></div><div id="comment-16642-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

