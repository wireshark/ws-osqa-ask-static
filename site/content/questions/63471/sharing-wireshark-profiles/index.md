+++
type = "question"
title = "Sharing Wireshark Profiles"
description = '''Hi, I have a profile that I created with Wireshark 2.4 and where I have disabled some protocols. I want to share it with a group of users; some running Wireshark 2.2 and others 2.4. Can I safely share it with everyone? Will it cause problems for the 2.2 users? Thanks and regards...Paul'''
date = "2017-08-15T00:51:00Z"
lastmod = "2017-08-16T23:00:00Z"
weight = 63471
keywords = [ "profile" ]
aliases = [ "/questions/63471" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Sharing Wireshark Profiles](/questions/63471/sharing-wireshark-profiles)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63471-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63471-score" class="post-score" title="current number of votes">0</div><span id="post-63471-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have a profile that I created with Wireshark 2.4 and where I have disabled some protocols. I want to share it with a group of users; some running Wireshark 2.2 and others 2.4. Can I safely share it with everyone? Will it cause problems for the 2.2 users?</p><p>Thanks and regards...Paul</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-profile" rel="tag" title="see questions tagged &#39;profile&#39;">profile</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Aug '17, 00:51</strong></p><img src="https://secure.gravatar.com/avatar/2e1b4057f2ff59fe059b23cc6571abaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PaulOfford&#39;s gravatar image" /><p><span>PaulOfford</span><br />
<span class="score" title="131 reputation points">131</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PaulOfford has 5 accepted answers">11%</span></p></div></div><div id="comments-container-63471" class="comments-container"></div><div id="comment-tools-63471" class="comment-tools"></div><div class="clear"></div><div id="comment-63471-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63479"></span>

<div id="answer-container-63479" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63479-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63479-score" class="post-score" title="current number of votes">2</div><span id="post-63479-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="PaulOfford has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you should be able to share your Wireshark 2.4 profile with Wireshark 2.2 users and it should not cause a problem. Having said that, since upgrading to v2.4.0, I did once get a message from a pre-2.4 version of Wireshark that was something to the effect that the profile was last saved by Wireshark 2.4, that there were incompatible settings, and that it would now be saved in the pre-2.4 version format.</p><p>I did not note the exact text of the message; it did not tell me <em>what</em> the incompatible settings were; and I don't remember what I had recently changed. However, Wireshark appeared to operate normally, both versions. There are preferences in v2.4 that simply don't exist at all in earlier versions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Aug '17, 19:32</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-63479" class="comments-container"><span id="63480"></span><div id="comment-63480" class="comment"><div id="post-63480-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jim.</p></div><div id="comment-63480-info" class="comment-info"><span class="comment-age">(16 Aug '17, 23:00)</span> <span class="comment-user userinfo">PaulOfford</span></div></div></div><div id="comment-tools-63479" class="comment-tools"></div><div class="clear"></div><div id="comment-63479-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

