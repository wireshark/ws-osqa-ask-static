+++
type = "question"
title = "Custom Column to display a specified offset?"
description = '''Has Wireshark ever been enhanced to allow the creation of a custom column where you can display a particular offset? For example, if I wanted a column to show the 5th byte of a udp header, udp[4]. I see that is was asked a long..... time ago, but haven&#x27;t seen anything newer on this forum relating to...'''
date = "2014-05-20T18:20:00Z"
lastmod = "2014-06-01T12:28:00Z"
weight = 32942
keywords = [ "columns", "offset" ]
aliases = [ "/questions/32942" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Custom Column to display a specified offset?](/questions/32942/custom-column-to-display-a-specified-offset)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32942-score" class="post-score" title="current number of votes">0</div><span id="post-32942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has Wireshark ever been enhanced to allow the creation of a custom column where you can display a particular offset?</p><p>For example, if I wanted a column to show the 5th byte of a udp header, udp[4].</p><p>I see that is was asked a long..... time ago, but haven't seen anything newer on this forum relating to this topic.</p><p>Thanks in advance.</p><p><a href="http://ask.wireshark.org/questions/7081/column-with-arbitrary-located-byte-in-a-packet">Link to similar question asked 3 years ago</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span> <span class="post-tag tag-link-offset" rel="tag" title="see questions tagged &#39;offset&#39;">offset</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '14, 18:20</strong></p><img src="https://secure.gravatar.com/avatar/bb79e0c62df46ecf47cc004a0a2d3cbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rooster_50&#39;s gravatar image" /><p><span>Rooster_50</span><br />
<span class="score" title="238 reputation points">238</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="18 badges"><span class="bronze">●</span><span class="badgecount">18</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rooster_50 has 5 accepted answers">15%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 May '14, 21:38</strong> </span></p></div></div><div id="comments-container-32942" class="comments-container"><span id="33243"></span><div id="comment-33243" class="comment"><div id="post-33243-score" class="comment-score"></div><div class="comment-text"><p>Created enhancement bug.</p><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10154">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10154</a></p></div><div id="comment-33243-info" class="comment-info"><span class="comment-age">(01 Jun '14, 12:28)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-32942" class="comment-tools"></div><div class="clear"></div><div id="comment-32942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33192"></span>

<div id="answer-container-33192" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33192-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33192-score" class="post-score" title="current number of votes">0</div><span id="post-33192-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Search the <a href="https://bugs.wireshark.org/bugzilla/">Wireshark Buzilla</a> for an entry describing the requirement and if there isn't one, create one marking it as an enhancement.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 May '14, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-33192" class="comments-container"></div><div id="comment-tools-33192" class="comment-tools"></div><div class="clear"></div><div id="comment-33192-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

