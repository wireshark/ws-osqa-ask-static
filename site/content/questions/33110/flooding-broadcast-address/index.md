+++
type = "question"
title = "Flooding broadcast address?"
description = '''Hello all!  I am having an interesting issue, i have 2 addresses from 2 computers in the same room sending out packets like crazy. both address are going to 255.255.255.255 port b nitrogen im closing in on 3,000,000 under a 40 min timeframe. im VERY new to wireshark but this just seemed odd. Besides...'''
date = "2014-05-27T09:43:00Z"
lastmod = "2014-05-27T12:46:00Z"
weight = 33110
keywords = [ "broadcast", "issue", "flooding" ]
aliases = [ "/questions/33110" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Flooding broadcast address?](/questions/33110/flooding-broadcast-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33110-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33110-score" class="post-score" title="current number of votes">0</div><span id="post-33110-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all! I am having an interesting issue, i have 2 addresses from 2 computers in the same room sending out packets like crazy. both address are going to 255.255.255.255 port b nitrogen im closing in on 3,000,000 under a 40 min timeframe. im VERY new to wireshark but this just seemed odd. Besides those 2 the next conversions are in the 10,000s or 100's. any idea?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadcast" rel="tag" title="see questions tagged &#39;broadcast&#39;">broadcast</span> <span class="post-tag tag-link-issue" rel="tag" title="see questions tagged &#39;issue&#39;">issue</span> <span class="post-tag tag-link-flooding" rel="tag" title="see questions tagged &#39;flooding&#39;">flooding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '14, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/22f84dbe31bb67fbb680ea0b7c95d207?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jshearer&#39;s gravatar image" /><p><span>jshearer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jshearer has no accepted answers">0%</span></p></div></div><div id="comments-container-33110" class="comments-container"><span id="33114"></span><div id="comment-33114" class="comment"><div id="post-33114-score" class="comment-score"></div><div class="comment-text"><p>can you please post a sample capture file with a few of those frames on google drive, dropbox or cloudshark.org?</p></div><div id="comment-33114-info" class="comment-info"><span class="comment-age">(27 May '14, 12:46)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33110" class="comment-tools"></div><div class="clear"></div><div id="comment-33110-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

