+++
type = "question"
title = "Phone over Wireless - Having issues"
description = '''Currently, I am trying to examine the packets my phone sends for certain applications.  My phone is a Galaxy S3, and it is set up so that it only uses my wireless network, but I cannot find any packets to/from the phone, even when I browse the web on it. Am I chasing a dream here, or can this actual...'''
date = "2013-10-21T23:42:00Z"
lastmod = "2013-10-23T00:30:00Z"
weight = 26275
keywords = [ "phone" ]
aliases = [ "/questions/26275" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Phone over Wireless - Having issues](/questions/26275/phone-over-wireless-having-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26275-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26275-score" class="post-score" title="current number of votes">0</div><span id="post-26275-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Currently, I am trying to examine the packets my phone sends for certain applications.</p><p>My phone is a Galaxy S3, and it is set up so that it only uses my wireless network, but I cannot find any packets to/from the phone, even when I browse the web on it.</p><p>Am I chasing a dream here, or can this actually be done?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-phone" rel="tag" title="see questions tagged &#39;phone&#39;">phone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '13, 23:42</strong></p><img src="https://secure.gravatar.com/avatar/2da51d1cec11c537578711bacfa2f4cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Keredar&#39;s gravatar image" /><p><span>Keredar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Keredar has no accepted answers">0%</span></p></div></div><div id="comments-container-26275" class="comments-container"><span id="26280"></span><div id="comment-26280" class="comment"><div id="post-26280-score" class="comment-score"></div><div class="comment-text"><p>Do you have any device to set the WiFi NIC to monitor mode? E.g. Linux or Mac? See:</p><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p><p>and:</p><p><a href="http://wiki.wireshark.org/HowToDecrypt802.11">http://wiki.wireshark.org/HowToDecrypt802.11</a></p><p>If you have a Windows machine, where Wireshark is running on, most likely you wont be able to see the traffic of your S3, depending on the Wifi-NIC (Orinoco might work and some few others, I think).</p></div><div id="comment-26280-info" class="comment-info"><span class="comment-age">(22 Oct '13, 00:56)</span> <span class="comment-user userinfo">franc</span></div></div><span id="26296"></span><div id="comment-26296" class="comment"><div id="post-26296-score" class="comment-score"></div><div class="comment-text"><p>Alright, thanks. Don't have that set up, so I guess it's not happening now.</p></div><div id="comment-26296-info" class="comment-info"><span class="comment-age">(22 Oct '13, 14:48)</span> <span class="comment-user userinfo">Keredar</span></div></div><span id="26312"></span><div id="comment-26312" class="comment"><div id="post-26312-score" class="comment-score"></div><div class="comment-text"><p>Are you on Windows?</p></div><div id="comment-26312-info" class="comment-info"><span class="comment-age">(23 Oct '13, 00:30)</span> <span class="comment-user userinfo">franc</span></div></div></div><div id="comment-tools-26275" class="comment-tools"></div><div class="clear"></div><div id="comment-26275-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

