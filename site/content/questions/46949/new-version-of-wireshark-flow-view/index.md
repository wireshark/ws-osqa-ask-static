+++
type = "question"
title = "New Version of Wireshark - Flow View"
description = '''Hi i was wondering if anyone can advise on how to show the &quot;flow&quot; with all data on the one window.... it seems like i need to scroll left and right to see everything. Opening in legacy wireshark i see the details in one window without scrolling.'''
date = "2015-10-26T12:03:00Z"
lastmod = "2015-10-26T13:21:00Z"
weight = 46949
keywords = [ "flow", "view" ]
aliases = [ "/questions/46949" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [New Version of Wireshark - Flow View](/questions/46949/new-version-of-wireshark-flow-view)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46949-score" class="post-score" title="current number of votes">0</div><span id="post-46949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i was wondering if anyone can advise on how to show the "flow" with all data on the one window.... it seems like i need to scroll left and right to see everything. Opening in legacy wireshark i see the details in one window without scrolling.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '15, 12:03</strong></p><img src="https://secure.gravatar.com/avatar/37994555214ffc73e84b17888f3e5327?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TonySmith1986&#39;s gravatar image" /><p><span>TonySmith1986</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TonySmith1986 has no accepted answers">0%</span></p></div></div><div id="comments-container-46949" class="comments-container"></div><div id="comment-tools-46949" class="comment-tools"></div><div class="clear"></div><div id="comment-46949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46954"></span>

<div id="answer-container-46954" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46954-score" class="post-score" title="current number of votes">0</div><span id="post-46954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I assume that you are talking about Wireshark 1.99.x or 2.0RC1. Sometimes the columns loose their size (for example: after applying a filter) In this case I use the resize button, to size the column to the space they really need. In most cases (if I havenßt too much columns) it fits the columns to the screen.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/resizeColumn.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Oct '15, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></img></div></div><div id="comments-container-46954" class="comments-container"></div><div id="comment-tools-46954" class="comment-tools"></div><div class="clear"></div><div id="comment-46954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

