+++
type = "question"
title = "6lowpan in contiki"
description = '''Hello Guys   I want to capture 6lowpan packets transferring between UDP IPv6 client and UDP IPv6 server , both running on contiki OS on the same system.'''
date = "2013-12-25T21:58:00Z"
lastmod = "2013-12-26T08:03:00Z"
weight = 28393
keywords = [ "6lowpan" ]
aliases = [ "/questions/28393" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [6lowpan in contiki](/questions/28393/6lowpan-in-contiki)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28393-score" class="post-score" title="current number of votes">0</div><span id="post-28393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Guys I want to capture 6lowpan packets transferring between UDP IPv6 client and UDP IPv6 server , both running on contiki OS on the same system.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-6lowpan" rel="tag" title="see questions tagged &#39;6lowpan&#39;">6lowpan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Dec '13, 21:58</strong></p><img src="https://secure.gravatar.com/avatar/27b335780faa7623f05a78e0d84b115b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Devesh&#39;s gravatar image" /><p><span>Devesh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Devesh has no accepted answers">0%</span></p></div></div><div id="comments-container-28393" class="comments-container"></div><div id="comment-tools-28393" class="comment-tools"></div><div class="clear"></div><div id="comment-28393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28410"></span>

<div id="answer-container-28410" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28410-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28410-score" class="post-score" title="current number of votes">0</div><span id="post-28410-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If your question is 'how to capture network traffic on contiki', my answer is: try using <strong>tcpdump</strong>. It should be available on contiki, or at least you can install it. Then transfer the capture file to another system and analyze it with Wireshark.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Dec '13, 08:03</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28410" class="comments-container"></div><div id="comment-tools-28410" class="comment-tools"></div><div class="clear"></div><div id="comment-28410-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

