+++
type = "question"
title = "does wireshark support q-in-q?"
description = '''Dear all, I wouls like to do some captures off double tag trafic. Does anybody knows if wireshark will capture and decode the traffic. If it is, a special config is needed? Thanks a lot,'''
date = "2011-02-01T08:00:00Z"
lastmod = "2012-02-07T11:01:00Z"
weight = 2070
keywords = [ "double", "tag", "qinq", "vlan", "q-in-q" ]
aliases = [ "/questions/2070" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [does wireshark support q-in-q?](/questions/2070/does-wireshark-support-q-in-q)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2070-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2070-score" class="post-score" title="current number of votes">0</div><span id="post-2070-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all,</p><p>I wouls like to do some captures off double tag trafic. Does anybody knows if wireshark will capture and decode the traffic. If it is, a special config is needed?</p><p>Thanks a lot,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-double" rel="tag" title="see questions tagged &#39;double&#39;">double</span> <span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span> <span class="post-tag tag-link-qinq" rel="tag" title="see questions tagged &#39;qinq&#39;">qinq</span> <span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-q-in-q" rel="tag" title="see questions tagged &#39;q-in-q&#39;">q-in-q</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '11, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/fa5e1cc13ca50b4ee76933ef9f91bd3c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ori&#39;s gravatar image" /><p><span>ori</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ori has no accepted answers">0%</span></p></div></div><div id="comments-container-2070" class="comments-container"><span id="8879"></span><div id="comment-8879" class="comment"><div id="post-8879-score" class="comment-score"></div><div class="comment-text"><p>hi i need to capture a qinq via wireshark but i didnt see it ? i have a dell laptop latitude d400</p></div><div id="comment-8879-info" class="comment-info"><span class="comment-age">(07 Feb '12, 11:01)</span> <span class="comment-user userinfo">ahmed</span></div></div></div><div id="comment-tools-2070" class="comment-tools"></div><div class="clear"></div><div id="comment-2070-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2086"></span>

<div id="answer-container-2086" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2086-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2086-score" class="post-score" title="current number of votes">1</div><span id="post-2086-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Capture depends on your network hardware/driver.</p><p>Dissection is not a problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '11, 15:02</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2086" class="comments-container"></div><div id="comment-tools-2086" class="comment-tools"></div><div class="clear"></div><div id="comment-2086-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="2632"></span>

<div id="answer-container-2632" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2632-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2632-score" class="post-score" title="current number of votes">0</div><span id="post-2632-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, I've done this many times. If you want to use a capture filter or display filter we are doubling up on the vlan filter to nail down a specific Q-in-Q VLAN. I'm not sure if there is another (easier?) way, but this works well:</p><p>capture filter for Q-in-Q VLAN 506.100:</p><p>vlan 506 and vlan 100</p><p>display filter for Q-in-Q VLAN 506.100:</p><p>vlan.id == 506 &amp;&amp; vlan.id == 100</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Mar '11, 08:37</strong></p><img src="https://secure.gravatar.com/avatar/aa856ab9c037da9a23e673065720780f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bababooey&#39;s gravatar image" /><p><span>Bababooey</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bababooey has no accepted answers">0%</span></p></div></div><div id="comments-container-2632" class="comments-container"></div><div id="comment-tools-2632" class="comment-tools"></div><div class="clear"></div><div id="comment-2632-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

