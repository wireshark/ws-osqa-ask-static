+++
type = "question"
title = "media renegotiation"
description = '''hi. i am looking for a way to see in a nice way the media renegotiation in wireshark.'''
date = "2017-04-07T02:18:00Z"
lastmod = "2017-05-30T20:54:00Z"
weight = 60633
keywords = [ "sip" ]
aliases = [ "/questions/60633" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [media renegotiation](/questions/60633/media-renegotiation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60633-score" class="post-score" title="current number of votes">0</div><span id="post-60633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi. i am looking for a way to see in a nice way the media renegotiation in wireshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '17, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/f49cd74f35d6d0358d467c0784dc56f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="atux_null&#39;s gravatar image" /><p><span>atux_null</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="atux_null has no accepted answers">0%</span></p></div></div><div id="comments-container-60633" class="comments-container"></div><div id="comment-tools-60633" class="comment-tools"></div><div class="clear"></div><div id="comment-60633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60634"></span>

<div id="answer-container-60634" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60634-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60634-score" class="post-score" title="current number of votes">0</div><span id="post-60634-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could start by looking at the Telephony|VoIP Calls and SIP flows menu items. If that is not enough you could elaborate on what you are missing.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '17, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60634" class="comments-container"><span id="60636"></span><div id="comment-60636" class="comment"><div id="post-60636-score" class="comment-score"></div><div class="comment-text"><p>OK, let's start the other way. i would like to see the music on hold and see if there is a renegotiation. how do i see that?</p></div><div id="comment-60636-info" class="comment-info"><span class="comment-age">(07 Apr '17, 02:51)</span> <span class="comment-user userinfo">atux_null</span></div></div><span id="61704"></span><div id="comment-61704" class="comment"><div id="post-61704-score" class="comment-score"></div><div class="comment-text"><p>Look at the SDP protocol in the re-INVITE.</p></div><div id="comment-61704-info" class="comment-info"><span class="comment-age">(30 May '17, 20:54)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-60634" class="comment-tools"></div><div class="clear"></div><div id="comment-60634-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

