+++
type = "question"
title = "Please let me know which wireshark version support the below CDMA parameter - ANSI MAP"
description = '''Please let me know which wireshark version support the below CDMA parameter - ANSI MAP::: CDMAPSMMList --CDMAServiceOneWayDelay2 --CDMATargetMAHOList --CDMATargetMAHOInfo --TargetCellID --CDMAPilotStrength --CDMATargetOneWayDelay --CDMATargetMAHOList --CDMATargetMAHOInfo --TargetCellID --CDMAPilotSt...'''
date = "2013-03-12T23:18:00Z"
lastmod = "2013-03-13T02:28:00Z"
weight = 19418
keywords = [ "cdma", "parameter" ]
aliases = [ "/questions/19418" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Please let me know which wireshark version support the below CDMA parameter - ANSI MAP](/questions/19418/please-let-me-know-which-wireshark-version-support-the-below-cdma-parameter-ansi-map)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19418-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19418-score" class="post-score" title="current number of votes">0</div><span id="post-19418-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please let me know which wireshark version support the below CDMA parameter - ANSI MAP:::</p><p>CDMAPSMMList --CDMAServiceOneWayDelay2 --CDMATargetMAHOList --CDMATargetMAHOInfo --TargetCellID --CDMAPilotStrength --CDMATargetOneWayDelay --CDMATargetMAHOList --CDMATargetMAHOInfo --TargetCellID --CDMAPilotStrength --CDMATargetOneWayDelay</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cdma" rel="tag" title="see questions tagged &#39;cdma&#39;">cdma</span> <span class="post-tag tag-link-parameter" rel="tag" title="see questions tagged &#39;parameter&#39;">parameter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '13, 23:18</strong></p><img src="https://secure.gravatar.com/avatar/8743e8045ce423b69ecc6d3fece87153?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moorthyforu&#39;s gravatar image" /><p><span>moorthyforu</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moorthyforu has no accepted answers">0%</span></p></div></div><div id="comments-container-19418" class="comments-container"></div><div id="comment-tools-19418" class="comment-tools"></div><div class="clear"></div><div id="comment-19418-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19422"></span>

<div id="answer-container-19422" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19422-score" class="post-score" title="current number of votes">0</div><span id="post-19422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can check here <a href="http://anonsvn.wireshark.org/viewvc/trunk/asn1/ansi_map/ansi_map.asn?revision=38159&amp;view=markup">http://anonsvn.wireshark.org/viewvc/trunk/asn1/ansi_map/ansi_map.asn?revision=38159&amp;view=markup</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '13, 23:54</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-19422" class="comments-container"><span id="19424"></span><div id="comment-19424" class="comment"><div id="post-19424-score" class="comment-score"></div><div class="comment-text"><p>Dear Sir,</p><p>Let me know the version to install. in the above link gives me only the Bugfixs.</p></div><div id="comment-19424-info" class="comment-info"><span class="comment-age">(13 Mar '13, 00:48)</span> <span class="comment-user userinfo">moorthyforu</span></div></div><span id="19428"></span><div id="comment-19428" class="comment"><div id="post-19428-score" class="comment-score"></div><div class="comment-text"><p>I'm not sure all of those parameters are supported in any version that's why I suggested you check that in the asn.1 file first. I don't think the ANSI MAP dissector has changed for quite a while so 1.8.5 should have all we got I think.</p></div><div id="comment-19428-info" class="comment-info"><span class="comment-age">(13 Mar '13, 01:30)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="19431"></span><div id="comment-19431" class="comment"><div id="post-19431-score" class="comment-score"></div><div class="comment-text"><p>ok even version 1.8.5 not decoding .</p></div><div id="comment-19431-info" class="comment-info"><span class="comment-age">(13 Mar '13, 02:14)</span> <span class="comment-user userinfo">moorthyforu</span></div></div><span id="19432"></span><div id="comment-19432" class="comment"><div id="post-19432-score" class="comment-score"></div><div class="comment-text"><p>You could raise a bug with an enhancment request with links to publically available documents specifying the parameters and in which signals they occure. Preferably together with a pcap trace file including ANSI MAP signals with those parameters.</p></div><div id="comment-19432-info" class="comment-info"><span class="comment-age">(13 Mar '13, 02:28)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-19422" class="comment-tools"></div><div class="clear"></div><div id="comment-19422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

