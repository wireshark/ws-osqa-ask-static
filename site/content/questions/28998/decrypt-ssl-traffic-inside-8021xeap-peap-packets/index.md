+++
type = "question"
title = "Decrypt SSL traffic inside 802.1x/EAP-PEAP packets"
description = '''Hi, I want to decrypt SSL traffic inside 802.1x/EAP-PEAP packets. Does anyone know how I can do this? Is there a Wireshark plugin?'''
date = "2014-01-17T10:59:00Z"
lastmod = "2014-01-17T10:59:00Z"
weight = 28998
keywords = [ "eap-peap", "802.1x" ]
aliases = [ "/questions/28998" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt SSL traffic inside 802.1x/EAP-PEAP packets](/questions/28998/decrypt-ssl-traffic-inside-8021xeap-peap-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28998-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28998-score" class="post-score" title="current number of votes">1</div><span id="post-28998-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I want to decrypt SSL traffic inside 802.1x/EAP-PEAP packets. Does anyone know how I can do this? Is there a Wireshark plugin?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-eap-peap" rel="tag" title="see questions tagged &#39;eap-peap&#39;">eap-peap</span> <span class="post-tag tag-link-802.1x" rel="tag" title="see questions tagged &#39;802.1x&#39;">802.1x</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jan '14, 10:59</strong></p><img src="https://secure.gravatar.com/avatar/949c05cd29bb5e2282616cc26c35fed4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mesh&#39;s gravatar image" /><p><span>Mesh</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mesh has no accepted answers">0%</span></p></div></div><div id="comments-container-28998" class="comments-container"></div><div id="comment-tools-28998" class="comment-tools"></div><div class="clear"></div><div id="comment-28998-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

