+++
type = "question"
title = "USBPcap keyboard and mouse disabled"
description = '''After installing wireshark and USBPcap and reboot, I found that none of the usb ports functioned thus rendering my keyboard and mouse useless. How can I uninstall USBPcap/solve this issue?'''
date = "2017-07-26T03:39:00Z"
lastmod = "2017-07-26T04:19:00Z"
weight = 63117
keywords = [ "usbpcap" ]
aliases = [ "/questions/63117" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [USBPcap keyboard and mouse disabled](/questions/63117/usbpcap-keyboard-and-mouse-disabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63117-score" class="post-score" title="current number of votes">0</div><span id="post-63117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installing wireshark and USBPcap and reboot, I found that none of the usb ports functioned thus rendering my keyboard and mouse useless. How can I uninstall USBPcap/solve this issue?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usbpcap" rel="tag" title="see questions tagged &#39;usbpcap&#39;">usbpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '17, 03:39</strong></p><img src="https://secure.gravatar.com/avatar/7bba29530e9cb517331443055c8fdc66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="3883&#39;s gravatar image" /><p><span>3883</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="3883 has no accepted answers">0%</span></p></div></div><div id="comments-container-63117" class="comments-container"></div><div id="comment-tools-63117" class="comment-tools"></div><div class="clear"></div><div id="comment-63117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="63120"></span>

<div id="answer-container-63120" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63120-score" class="post-score" title="current number of votes">0</div><span id="post-63120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately a few people have been afflicted with this issue. If you search the questions here some others have found solutions that might work for you, e.g. <a href="https://ask.wireshark.org/questions/62996/how-do-i-fix-my-computer-after-usbpcap-has-rendered-the-usb-keyboard-and-mouse-inoperative">here</a> and <a href="https://ask.wireshark.org/questions/50248/usb-ports-not-functioning">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '17, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63120" class="comments-container"></div><div id="comment-tools-63120" class="comment-tools"></div><div class="clear"></div><div id="comment-63120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="63123"></span>

<div id="answer-container-63123" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63123-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63123-score" class="post-score" title="current number of votes">0</div><span id="post-63123-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please check <a href="https://ask.wireshark.org/questions/50248/usb-ports-not-functioning">this duplicate question</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '17, 04:19</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-63123" class="comments-container"></div><div id="comment-tools-63123" class="comment-tools"></div><div class="clear"></div><div id="comment-63123-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

