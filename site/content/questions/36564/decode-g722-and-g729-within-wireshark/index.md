+++
type = "question"
title = "Decode G.722 and G.729 within Wireshark"
description = '''Hi, Is it possible to decode G.722 and G.729 traffic in Wireshark? If it is, could someone please tell me how to do it - in the simplest, non developer, noob-like english you possibly can? A step by step would be awesome, a dummies guide even more so. I can trace, I can read a trace and I can click ...'''
date = "2014-09-24T07:56:00Z"
lastmod = "2014-09-24T08:09:00Z"
weight = 36564
keywords = [ "g729", "g722", "g.729", "rtp", "g.722" ]
aliases = [ "/questions/36564" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decode G.722 and G.729 within Wireshark](/questions/36564/decode-g722-and-g729-within-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36564-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36564-score" class="post-score" title="current number of votes">0</div><span id="post-36564-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is it possible to decode G.722 and G.729 traffic in Wireshark? If it is, could someone please tell me how to do it - in the simplest, non developer, noob-like english you possibly can? A step by step would be awesome, a dummies guide even more so. I can trace, I can read a trace and I can click 'install'. ;)</p><p>Thanks Stan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-g729" rel="tag" title="see questions tagged &#39;g729&#39;">g729</span> <span class="post-tag tag-link-g722" rel="tag" title="see questions tagged &#39;g722&#39;">g722</span> <span class="post-tag tag-link-g.729" rel="tag" title="see questions tagged &#39;g.729&#39;">g.729</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-g.722" rel="tag" title="see questions tagged &#39;g.722&#39;">g.722</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '14, 07:56</strong></p><img src="https://secure.gravatar.com/avatar/b61ef2f5d4987af9f7363ba693b0f938?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Scintaar&#39;s gravatar image" /><p><span>Scintaar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Scintaar has no accepted answers">0%</span></p></div></div><div id="comments-container-36564" class="comments-container"></div><div id="comment-tools-36564" class="comment-tools"></div><div class="clear"></div><div id="comment-36564-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36566"></span>

<div id="answer-container-36566" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36566-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36566-score" class="post-score" title="current number of votes">0</div><span id="post-36566-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For G.729, we have a dedicated <a href="http://wiki.wireshark.org/HowToDecodeG729">wiki page</a>. For G.722 we have this <a href="https://ask.wireshark.org/questions/26607/coverting-g722-payload-to-wav">thread</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Sep '14, 08:09</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Sep '14, 08:11</strong> </span></p></div></div><div id="comments-container-36566" class="comments-container"></div><div id="comment-tools-36566" class="comment-tools"></div><div class="clear"></div><div id="comment-36566-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

