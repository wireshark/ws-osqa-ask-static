+++
type = "question"
title = "IO Graph Y Scale"
description = '''I&#x27;m running 2.2.1 and I am not able the get the right Y Scale on QT. Am I missing something in the QT configuration or is this a bug? GTK  QT '''
date = "2016-10-19T00:09:00Z"
lastmod = "2016-10-26T07:04:00Z"
weight = 56498
keywords = [ "graph", "iograph", "io_graph", "iographs", "graphs" ]
aliases = [ "/questions/56498" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IO Graph Y Scale](/questions/56498/io-graph-y-scale)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56498-score" class="post-score" title="current number of votes">0</div><span id="post-56498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm running 2.2.1 and I am not able the get the right Y Scale on QT. Am I missing something in the QT configuration or is this a bug?</p><p>GTK <img src="http://www.wallpix.ca/img/gtk.gif" alt="GTK" /></p><p>QT <img src="http://www.wallpix.ca/img/qt.gif" alt="QT" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span> <span class="post-tag tag-link-io_graph" rel="tag" title="see questions tagged &#39;io_graph&#39;">io_graph</span> <span class="post-tag tag-link-iographs" rel="tag" title="see questions tagged &#39;iographs&#39;">iographs</span> <span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '16, 00:09</strong></p><img src="https://secure.gravatar.com/avatar/7db22b6e7591b5f38688d59eabb8362b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wallpix&#39;s gravatar image" /><p><span>wallpix</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wallpix has no accepted answers">0%</span></p></img></div></div><div id="comments-container-56498" class="comments-container"></div><div id="comment-tools-56498" class="comment-tools"></div><div class="clear"></div><div id="comment-56498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56500"></span>

<div id="answer-container-56500" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56500-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56500-score" class="post-score" title="current number of votes">0</div><span id="post-56500-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>These (Y- and X-scale) have been moved to hotkeys. Right click on the graph to see the available hotkeys (with the focus on the graph it's X and Y in this case), or use the mouse.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '16, 00:45</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></img></div></div><div id="comments-container-56500" class="comments-container"><span id="56697"></span><div id="comment-56697" class="comment"><div id="post-56697-score" class="comment-score"></div><div class="comment-text"><p>Look at the scale of the Y Axis. I am not able to make it look like the GTK one even after playing with those options.</p></div><div id="comment-56697-info" class="comment-info"><span class="comment-age">(26 Oct '16, 07:04)</span> <span class="comment-user userinfo">wallpix</span></div></div></div><div id="comment-tools-56500" class="comment-tools"></div><div class="clear"></div><div id="comment-56500-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

