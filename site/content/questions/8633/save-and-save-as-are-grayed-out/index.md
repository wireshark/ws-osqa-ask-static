+++
type = "question"
title = "Save and Save As are grayed out"
description = '''Can someone tell me how to enable Save and Save As on Wireshark? When I start running Wireshark on my laptop it won&#x27;t let me save the capture. I have uninstalled and reinstalled Wireshark several times with no luck. In the past I was able to save captures on my laptop with Wireshark but at some poin...'''
date = "2012-01-26T13:24:00Z"
lastmod = "2012-01-26T14:02:00Z"
weight = 8633
keywords = [ "save" ]
aliases = [ "/questions/8633" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Save and Save As are grayed out](/questions/8633/save-and-save-as-are-grayed-out)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8633-score" class="post-score" title="current number of votes">0</div><span id="post-8633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone tell me how to enable Save and Save As on Wireshark? When I start running Wireshark on my laptop it won't let me save the capture. I have uninstalled and reinstalled Wireshark several times with no luck. In the past I was able to save captures on my laptop with Wireshark but at some point in time this changed and now Save and Save As are grayed out. Can someone shed some light on this?</p><p>Thanks.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '12, 13:24</strong></p><img src="https://secure.gravatar.com/avatar/52f2b51b5947a89a537f65fe6d8c2dac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NoviceShark&#39;s gravatar image" /><p><span>NoviceShark</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NoviceShark has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-8633" class="comments-container"></div><div id="comment-tools-8633" class="comment-tools"></div><div class="clear"></div><div id="comment-8633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8636"></span>

<div id="answer-container-8636" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8636-score" class="post-score" title="current number of votes">0</div><span id="post-8636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just answered my own question. You have to stop the capture before you can save it. :)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jan '12, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/52f2b51b5947a89a537f65fe6d8c2dac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NoviceShark&#39;s gravatar image" /><p><span>NoviceShark</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NoviceShark has no accepted answers">0%</span></p></div></div><div id="comments-container-8636" class="comments-container"></div><div id="comment-tools-8636" class="comment-tools"></div><div class="clear"></div><div id="comment-8636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

