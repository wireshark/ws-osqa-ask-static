+++
type = "question"
title = "retrieving tcp throughput using tshark"
description = '''In wireshark the throughput is displayed inside the stats -&amp;gt; conversations -&amp;gt; TCP tharks -q -z &quot;conv,tcp&quot; only prints number of frames. Is there a way to retrive TCP throughput using tshark ?'''
date = "2011-10-04T00:17:00Z"
lastmod = "2011-10-04T00:17:00Z"
weight = 6691
keywords = [ "tshark" ]
aliases = [ "/questions/6691" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [retrieving tcp throughput using tshark](/questions/6691/retrieving-tcp-throughput-using-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6691-score" class="post-score" title="current number of votes">0</div><span id="post-6691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In wireshark the throughput is displayed inside the stats -&gt; conversations -&gt; TCP tharks -q -z "conv,tcp" only prints number of frames.</p><p>Is there a way to retrive TCP throughput using tshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '11, 00:17</strong></p><img src="https://secure.gravatar.com/avatar/5d64d21de6598960bf2db61f1ca705cc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ddayan&#39;s gravatar image" /><p><span>ddayan</span><br />
<span class="score" title="41 reputation points">41</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ddayan has no accepted answers">0%</span></p></div></div><div id="comments-container-6691" class="comments-container"></div><div id="comment-tools-6691" class="comment-tools"></div><div class="clear"></div><div id="comment-6691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

