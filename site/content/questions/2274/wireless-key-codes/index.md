+++
type = "question"
title = "wireless key code&#x27;s"
description = '''Hi I use wireshark with no problems but I&#x27;m trying to gt the wireless key code&#x27;s how do I do it. ty'''
date = "2011-02-10T10:34:00Z"
lastmod = "2011-02-13T01:54:00Z"
weight = 2274
keywords = [ "keycodes" ]
aliases = [ "/questions/2274" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [wireless key code's](/questions/2274/wireless-key-codes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2274-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2274-score" class="post-score" title="current number of votes">0</div><span id="post-2274-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I use wireshark with no problems but I'm trying to gt the wireless key code's how do I do it. ty</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-keycodes" rel="tag" title="see questions tagged &#39;keycodes&#39;">keycodes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '11, 10:34</strong></p><img src="https://secure.gravatar.com/avatar/e53cec4ac11c20130462ca61a7641b6d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vamp&#39;s gravatar image" /><p><span>vamp</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vamp has no accepted answers">0%</span></p></div></div><div id="comments-container-2274" class="comments-container"></div><div id="comment-tools-2274" class="comment-tools"></div><div class="clear"></div><div id="comment-2274-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2275"></span>

<div id="answer-container-2275" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2275-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2275-score" class="post-score" title="current number of votes">0</div><span id="post-2275-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you are asking how to break wireless encryption and extract the encryption key using Wireshark you're in the wrong place. You can use Wireshark to decrypt wireless traffic if you already have the key. If you don't, you're more or less out of luck.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '11, 10:37</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2275" class="comments-container"></div><div id="comment-tools-2275" class="comment-tools"></div><div class="clear"></div><div id="comment-2275-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="2296"></span>

<div id="answer-container-2296" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2296-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2296-score" class="post-score" title="current number of votes">0</div><span id="post-2296-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No more or less about it. You are totally out of luck. And a good thing too. Otherwise the Black Hats could sniff your bank transactions. (Ok, not really. Https pages are encrypted at the application level before they get down to the data layer.)</p><p>WEP keys turn out to be fairly trivial to crack, due to a weakness in the algorithm. WPA/TKIP, WPA/AES likely can be cracked by the NSA, or by your university protein-folding Beowulf cluster, but not by mere mortals in reasonable time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '11, 01:54</strong></p><img src="https://secure.gravatar.com/avatar/ed2ddfe3ce5e547c20fafa0c82b3e970?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SGBotsford&#39;s gravatar image" /><p><span>SGBotsford</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SGBotsford has no accepted answers">0%</span></p></div></div><div id="comments-container-2296" class="comments-container"></div><div id="comment-tools-2296" class="comment-tools"></div><div class="clear"></div><div id="comment-2296-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

