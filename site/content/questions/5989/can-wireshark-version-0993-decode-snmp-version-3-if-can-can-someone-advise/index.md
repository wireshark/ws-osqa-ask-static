+++
type = "question"
title = "can wireshark version  0.99.3 decode snmp version 3? if can, can someone advise?"
description = '''can wireshark version 0.99.3 decode snmp version 3? if can, can someone advise?'''
date = "2011-08-30T22:45:00Z"
lastmod = "2011-08-31T11:11:00Z"
weight = 5989
keywords = [ "snmpv3" ]
aliases = [ "/questions/5989" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can wireshark version 0.99.3 decode snmp version 3? if can, can someone advise?](/questions/5989/can-wireshark-version-0993-decode-snmp-version-3-if-can-can-someone-advise)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5989-score" class="post-score" title="current number of votes">0</div><span id="post-5989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can wireshark version 0.99.3 decode snmp version 3? if can, can someone advise?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snmpv3" rel="tag" title="see questions tagged &#39;snmpv3&#39;">snmpv3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Aug '11, 22:45</strong></p><img src="https://secure.gravatar.com/avatar/96103a05a8d80161c0b9cbba8424c371?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="simonlcw&#39;s gravatar image" /><p><span>simonlcw</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="simonlcw has no accepted answers">0%</span></p></div></div><div id="comments-container-5989" class="comments-container"></div><div id="comment-tools-5989" class="comment-tools"></div><div class="clear"></div><div id="comment-5989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6018"></span>

<div id="answer-container-6018" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6018-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6018-score" class="post-score" title="current number of votes">1</div><span id="post-6018-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looking at the 0.99.3 snmp dissector revision history, I think the answer is yes.</p><p>That being said: Wireshark 0.99.3 is <strong>ancient</strong> (released about 5 years ago) and is unsupported and really <strong>shoud not</strong> be used at all.</p><p>(I also note that there have been numerous bug fixes to the snmp dissector since 0.99.3 was released).</p><p>Is there a reason why you cannot use a current stable version (e.g., Wireshark 1.6.1) ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '11, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-6018" class="comments-container"></div><div id="comment-tools-6018" class="comment-tools"></div><div class="clear"></div><div id="comment-6018-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

