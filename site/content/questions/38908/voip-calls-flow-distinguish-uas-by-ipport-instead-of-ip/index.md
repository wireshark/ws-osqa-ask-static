+++
type = "question"
title = "VoIP Calls Flow - distinguish UAs by IP:port instead of IP"
description = '''This feature is very handy to display in a clear way a complex call flow. But unfortunately it is almost unusable in case User-Agents share the same IP address and talk together. Resulting graph is flat with only one speaker talking to itself and no information attached to messages, only ports and a...'''
date = "2015-01-06T08:46:00Z"
lastmod = "2015-01-06T08:46:00Z"
weight = 38908
keywords = [ "analysis", "flow", "calls", "voip" ]
aliases = [ "/questions/38908" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP Calls Flow - distinguish UAs by IP:port instead of IP](/questions/38908/voip-calls-flow-distinguish-uas-by-ipport-instead-of-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38908-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38908-score" class="post-score" title="current number of votes">0</div><span id="post-38908-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This feature is very handy to display in a clear way a complex call flow. But unfortunately it is almost unusable in case User-Agents share the same IP address and talk together. Resulting graph is flat with only one speaker talking to itself and no information attached to messages, only ports and arrow.</p><p>That's why I'm wondering : is there any way to distinguish speakers (i.e. UAs) by something more than only IP address, ideally IP address + port ? That would be great!</p><p>TIA</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-calls" rel="tag" title="see questions tagged &#39;calls&#39;">calls</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '15, 08:46</strong></p><img src="https://secure.gravatar.com/avatar/3b1fab5ab8c0ed8832fb901d5ef65cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JuLu&#39;s gravatar image" /><p><span>JuLu</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JuLu has no accepted answers">0%</span></p></div></div><div id="comments-container-38908" class="comments-container"></div><div id="comment-tools-38908" class="comment-tools"></div><div class="clear"></div><div id="comment-38908-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

