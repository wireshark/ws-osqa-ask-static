+++
type = "question"
title = "can the wifi service provider access or read my gmail chats using wireshark?"
description = '''i am connected to the internet in my laptop via wifi connection provided by my company.can the company authorities access and read my gmail chats and mails using wireshark?'''
date = "2012-07-06T21:02:00Z"
lastmod = "2012-07-07T05:30:00Z"
weight = 12494
keywords = [ "wifi", "wireshark", "gmail" ]
aliases = [ "/questions/12494" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can the wifi service provider access or read my gmail chats using wireshark?](/questions/12494/can-the-wifi-service-provider-access-or-read-my-gmail-chats-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12494-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12494-score" class="post-score" title="current number of votes">1</div><span id="post-12494-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am connected to the internet in my laptop via wifi connection provided by my company.can the company authorities access and read my gmail chats and mails using wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-gmail" rel="tag" title="see questions tagged &#39;gmail&#39;">gmail</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '12, 21:02</strong></p><img src="https://secure.gravatar.com/avatar/14bed04694683a379eb00491a90d8c0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ammu&#39;s gravatar image" /><p><span>ammu</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ammu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jul '12, 22:33</strong> </span></p></div></div><div id="comments-container-12494" class="comments-container"></div><div id="comment-tools-12494" class="comment-tools"></div><div class="clear"></div><div id="comment-12494-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12496"></span>

<div id="answer-container-12496" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12496-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12496-score" class="post-score" title="current number of votes">2</div><span id="post-12496-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ul><li>NO, if you use encryption (SSL/TLS) AND you take care that nobody tampered with the SSL session (no SSL decryption in place, with the CA imported in your PC system , no SSL error messages while you access google.)</li><li>YES, if you use plaintext HTTP.<br />
</li><li>YES, if they installed a kind of "agent" (nothing to do with Wireshark!) on your system, as that will give them direct access to the unencrypted data in your browser and probably your keystrokes.</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jul '12, 05:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-12496" class="comments-container"></div><div id="comment-tools-12496" class="comment-tools"></div><div class="clear"></div><div id="comment-12496-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

