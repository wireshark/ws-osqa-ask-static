+++
type = "question"
title = "How do I monitor a &quot;specific&quot; app?"
description = '''I use an art software called &quot;Clip studio paint&quot;.I want to know what kind of files go in and out when I either go online or launch the app while online.I also want to know exactly which file or folder the information gets sent from.I&#x27;m not very computer savvy,and know nothing about computer/software...'''
date = "2017-07-26T08:27:00Z"
lastmod = "2017-07-26T09:46:00Z"
weight = 63136
keywords = [ "capture", "monitor" ]
aliases = [ "/questions/63136" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I monitor a "specific" app?](/questions/63136/how-do-i-monitor-a-specific-app)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63136-score" class="post-score" title="current number of votes">0</div><span id="post-63136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use an art software called "Clip studio paint".I want to know what kind of files go in and out when I either go online or launch the app while online.I also want to know exactly which file or folder the information gets sent from.I'm not very computer savvy,and know nothing about computer/software programming (Whether they are the same or not i'm unsure). How would I go about configuring this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '17, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/e99657389f3ed228b8ee5fe29fc5e72c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="otherguy&#39;s gravatar image" /><p><span>otherguy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="otherguy has no accepted answers">0%</span></p></div></div><div id="comments-container-63136" class="comments-container"></div><div id="comment-tools-63136" class="comment-tools"></div><div class="clear"></div><div id="comment-63136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63140"></span>

<div id="answer-container-63140" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63140-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63140-score" class="post-score" title="current number of votes">0</div><span id="post-63140-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately not directly with Wireshark as it has no way of filtering the traffic to a specific application. If you "know" that the application only uses certain protocols\ports then you could filter on those, but what about stuff you don't know.</p><p>Microsoft's Message Analyser apparently can filter traffic to a specific application so that might be an option for you.</p><p>Regardless of your approach you're likely to be exposed to the complexities of network traffic to "find" what you're looking for and that requires a reasonable amount of computer knowledge.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '17, 09:46</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63140" class="comments-container"></div><div id="comment-tools-63140" class="comment-tools"></div><div class="clear"></div><div id="comment-63140-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

