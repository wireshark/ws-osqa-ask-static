+++
type = "question"
title = "K12 protocols"
description = '''Hello. I wonder what protocol should be used in case of ISDN PRI in tectronix files .rf5 ? '''
date = "2012-02-16T02:48:00Z"
lastmod = "2012-02-27T13:38:00Z"
weight = 9054
keywords = [ "k12", "protocol" ]
aliases = [ "/questions/9054" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [K12 protocols](/questions/9054/k12-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9054-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9054-score" class="post-score" title="current number of votes">0</div><span id="post-9054-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.</p><p>I wonder what protocol should be used in case of ISDN PRI in tectronix files .rf5 ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-k12" rel="tag" title="see questions tagged &#39;k12&#39;">k12</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Feb '12, 02:48</strong></p><img src="https://secure.gravatar.com/avatar/4ca6028e5ad56e72443e277ccf0d2fe7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pnk&#39;s gravatar image" /><p><span>Pnk</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pnk has no accepted answers">0%</span></p></div></div><div id="comments-container-9054" class="comments-container"></div><div id="comment-tools-9054" class="comment-tools"></div><div class="clear"></div><div id="comment-9054-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="9101"></span>

<div id="answer-container-9101" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9101-score" class="post-score" title="current number of votes">1</div><span id="post-9101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Pnk has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would guess at q931 or LAPD. browsing the .r5 file might give a hint.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '12, 11:28</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Feb '12, 13:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-9101" class="comments-container"><span id="9231"></span><div id="comment-9231" class="comment"><div id="post-9231-score" class="comment-score"></div><div class="comment-text"><p>Not q931 nor LAPD gives result. Can't upload file, it is too big. Seems that the analyser is K15 and it have differencies with coding LAPD (as it seen in trace,parsed with analyser). Q931 also does'n bring anything.</p></div><div id="comment-9231-info" class="comment-info"><span class="comment-age">(27 Feb '12, 01:17)</span> <span class="comment-user userinfo">Pnk</span></div></div></div><div id="comment-tools-9101" class="comment-tools"></div><div class="clear"></div><div id="comment-9101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="9232"></span>

<div id="answer-container-9232" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9232-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9232-score" class="post-score" title="current number of votes">0</div><span id="post-9232-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Some manipulating with protocols braught result: like half of packets in Q931 and half in LAPD. Don't get how it worked :) Anyway thx for help. Result protocols in frame k12:lapd:q931:q932:q932.ros:data Wireshark 1.6.5</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '12, 01:28</strong></p><img src="https://secure.gravatar.com/avatar/4ca6028e5ad56e72443e277ccf0d2fe7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pnk&#39;s gravatar image" /><p><span>Pnk</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pnk has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Feb '12, 01:33</strong> </span></p></div></div><div id="comments-container-9232" class="comments-container"><span id="9257"></span><div id="comment-9257" class="comment"><div id="post-9257-score" class="comment-score"></div><div class="comment-text"><p>Q.931 is a protocol that runs over LAPD, so perhaps "half of packets in Q931 and half in LAPD" means <em>all</em> the packets are LAPD and half of them transport Q.931 (rather than being, say, <a href="http://en.wikipedia.org/wiki/High-Level_Data_Link_Control#S-Frames_.28control.29">S-frames</a> that just transmit flow control and error information at the LAPD layer).</p></div><div id="comment-9257-info" class="comment-info"><span class="comment-age">(27 Feb '12, 13:38)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-9232" class="comment-tools"></div><div class="clear"></div><div id="comment-9232-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

