+++
type = "question"
title = "Capture filters not working and/or disappeared in 1.8.2?"
description = '''Hi, I installed 1.8.2 of WireShark and I noticed that the UI has changed a bit. The one feature that I appear to be missing is the ability to set a capture filter in the &quot;Captures Option&quot; dialog; which I was able to do previously. I then tried to use the &quot;Capture Filters&quot; dialog but that doesn&#x27;t see...'''
date = "2012-08-31T15:14:00Z"
lastmod = "2012-08-31T17:13:00Z"
weight = 13978
keywords = [ "capture", "capture-filter" ]
aliases = [ "/questions/13978" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture filters not working and/or disappeared in 1.8.2?](/questions/13978/capture-filters-not-working-andor-disappeared-in-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13978-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13978-score" class="post-score" title="current number of votes">0</div><span id="post-13978-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I installed 1.8.2 of WireShark and I noticed that the UI has changed a bit. The one feature that I appear to be missing is the ability to set a capture filter in the "Captures Option" dialog; which I was able to do previously.</p><p>I then tried to use the "Capture Filters" dialog but that doesn't seem to do anything at all when I enter my filter and then press "OK".</p><p>Did this feature change or break?</p><p>Cheers, Clint</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '12, 15:14</strong></p><img src="https://secure.gravatar.com/avatar/711592880a44833f89b9c4760a3d1a33?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="clintsinger&#39;s gravatar image" /><p><span>clintsinger</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="clintsinger has no accepted answers">0%</span></p></div></div><div id="comments-container-13978" class="comments-container"></div><div id="comment-tools-13978" class="comment-tools"></div><div class="clear"></div><div id="comment-13978-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13981"></span>

<div id="answer-container-13981" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13981-score" class="post-score" title="current number of votes">2</div><span id="post-13981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Refer to the answer provided by <a href="http://ask.wireshark.org/users/527/jim-aragon">Jim Aragon</a> to <a href="http://ask.wireshark.org/questions/12221/how-to-chose-a-capture-filter-in-version-180-as-previous-in-the-capture-options-menu-of-version-16">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '12, 17:13</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-13981" class="comments-container"></div><div id="comment-tools-13981" class="comment-tools"></div><div class="clear"></div><div id="comment-13981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

