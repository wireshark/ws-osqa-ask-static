+++
type = "question"
title = "How to generate amf packets????"
description = '''I am writing an amf plugin for wireshark. I need an amf packet generator to test my plugin. Is there any tool / website which generates amf packets?????'''
date = "2012-11-05T20:17:00Z"
lastmod = "2012-11-06T03:27:00Z"
weight = 15557
keywords = [ "amf" ]
aliases = [ "/questions/15557" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to generate amf packets????](/questions/15557/how-to-generate-amf-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15557-score" class="post-score" title="current number of votes">0</div><span id="post-15557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am writing an amf plugin for wireshark. I need an amf packet generator to test my plugin. Is there any tool / website which generates amf packets?????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amf" rel="tag" title="see questions tagged &#39;amf&#39;">amf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '12, 20:17</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div></div><div id="comments-container-15557" class="comments-container"><span id="15558"></span><div id="comment-15558" class="comment"><div id="post-15558-score" class="comment-score"></div><div class="comment-text"><p>Maybe the first link in Kurt's answer here could be relevant to you?: <a href="http://ask.wireshark.org/questions/15183/list-of-servers-that-send-amf-packet">http://ask.wireshark.org/questions/15183/list-of-servers-that-send-amf-packet</a></p></div><div id="comment-15558-info" class="comment-info"><span class="comment-age">(05 Nov '12, 21:09)</span> <span class="comment-user userinfo">SidR</span></div></div><span id="15559"></span><div id="comment-15559" class="comment"><div id="post-15559-score" class="comment-score"></div><div class="comment-text"><p>I tried (Blazer -Amf testing made easy) but it throws an exception :AMF CONNECTION FAILED. I couldn't fix it. Do you know how to fix it.</p><p>Or is there any other amf generator tool?????</p></div><div id="comment-15559-info" class="comment-info"><span class="comment-age">(05 Nov '12, 21:28)</span> <span class="comment-user userinfo">Akhil</span></div></div><span id="15574"></span><div id="comment-15574" class="comment"><div id="post-15574-score" class="comment-score"></div><div class="comment-text"><p>When you saw the "connection failed" message, did you see a SYN packet on the network?</p></div><div id="comment-15574-info" class="comment-info"><span class="comment-age">(06 Nov '12, 03:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15557" class="comment-tools"></div><div class="clear"></div><div id="comment-15557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

