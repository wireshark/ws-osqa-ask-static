+++
type = "question"
title = "Cannot see Wi-Fi packets like RTS/CTS, or PS-Pole"
description = '''Hello, using a Windows computer with Wireshark 1.6.7 and a AirPcap Nx. I am filtering by wlan.addr. I cannot see things like RTS/CTS or PS-Poll packets in my trace. Do I have to change a setting somewhere to let me see those type of packets?'''
date = "2012-06-05T11:13:00Z"
lastmod = "2012-10-04T11:13:00Z"
weight = 11674
keywords = [ "management", "packets", "rts", "wi-fi", "cts" ]
aliases = [ "/questions/11674" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot see Wi-Fi packets like RTS/CTS, or PS-Pole](/questions/11674/cannot-see-wi-fi-packets-like-rtscts-or-ps-pole)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11674-score" class="post-score" title="current number of votes">0</div><span id="post-11674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, using a Windows computer with Wireshark 1.6.7 and a AirPcap Nx. I am filtering by wlan.addr. I cannot see things like RTS/CTS or PS-Poll packets in my trace. Do I have to change a setting somewhere to let me see those type of packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-management" rel="tag" title="see questions tagged &#39;management&#39;">management</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-rts" rel="tag" title="see questions tagged &#39;rts&#39;">rts</span> <span class="post-tag tag-link-wi-fi" rel="tag" title="see questions tagged &#39;wi-fi&#39;">wi-fi</span> <span class="post-tag tag-link-cts" rel="tag" title="see questions tagged &#39;cts&#39;">cts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '12, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/0f45b0b7c1b00749c4fb6fc9126f61fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joseph%20Conley&#39;s gravatar image" /><p><span>Joseph Conley</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joseph Conley has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jun '12, 11:22</strong> </span></p></div></div><div id="comments-container-11674" class="comments-container"></div><div id="comment-tools-11674" class="comment-tools"></div><div class="clear"></div><div id="comment-11674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14688"></span>

<div id="answer-container-14688" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14688-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14688-score" class="post-score" title="current number of votes">0</div><span id="post-14688-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Updated wireshark and this issue went away</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Oct '12, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/0f45b0b7c1b00749c4fb6fc9126f61fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joseph%20Conley&#39;s gravatar image" /><p><span>Joseph Conley</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joseph Conley has no accepted answers">0%</span></p></div></div><div id="comments-container-14688" class="comments-container"><span id="14694"></span><div id="comment-14694" class="comment"><div id="post-14694-score" class="comment-score"></div><div class="comment-text"><p>For future reference to someone else who might experience the same or similar problem as you did, which version of Wireshark did you upgrade to?</p></div><div id="comment-14694-info" class="comment-info"><span class="comment-age">(03 Oct '12, 19:40)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="14710"></span><div id="comment-14710" class="comment"><div id="post-14710-score" class="comment-score"></div><div class="comment-text"><p>I was on 1.8.1 and could not see them, after upgrading to 1.8.3 I was then able to see them. 64 Bit</p></div><div id="comment-14710-info" class="comment-info"><span class="comment-age">(04 Oct '12, 11:13)</span> <span class="comment-user userinfo">Joseph Conley</span></div></div></div><div id="comment-tools-14688" class="comment-tools"></div><div class="clear"></div><div id="comment-14688-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

