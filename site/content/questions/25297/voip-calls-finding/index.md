+++
type = "question"
title = "VoIP calls finding"
description = '''Dear Sirs, Please, could you be so kind to tell me how I could find an exact phone call data in a huge VoIP calls filtered by VoIP calls Tab? My E mails is: alexmishin@inbox.ru'''
date = "2013-09-26T22:48:00Z"
lastmod = "2013-09-27T01:11:00Z"
weight = 25297
keywords = [ "finding", "calls", "voip" ]
aliases = [ "/questions/25297" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP calls finding](/questions/25297/voip-calls-finding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25297-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25297-score" class="post-score" title="current number of votes">0</div><span id="post-25297-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sirs, Please, could you be so kind to tell me how I could find an exact phone call data in a huge VoIP calls filtered by VoIP calls Tab? My E mails is: <span class="__cf_email__" data-cfemail="7b1a171e031612081312153b121519140355090e">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-finding" rel="tag" title="see questions tagged &#39;finding&#39;">finding</span> <span class="post-tag tag-link-calls" rel="tag" title="see questions tagged &#39;calls&#39;">calls</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '13, 22:48</strong></p><img src="https://secure.gravatar.com/avatar/bf6b069e431bc7af8e6aa0596b7647b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alex7&#39;s gravatar image" /><p><span>Alex7</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alex7 has no accepted answers">0%</span></p></div></div><div id="comments-container-25297" class="comments-container"><span id="25303"></span><div id="comment-25303" class="comment"><div id="post-25303-score" class="comment-score"></div><div class="comment-text"><p>The fact is that there is no possibility to find specific call within the frame of aready filtered VoIP tab.</p></div><div id="comment-25303-info" class="comment-info"><span class="comment-age">(27 Sep '13, 01:11)</span> <span class="comment-user userinfo">Alex7</span></div></div></div><div id="comment-tools-25297" class="comment-tools"></div><div class="clear"></div><div id="comment-25297-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

