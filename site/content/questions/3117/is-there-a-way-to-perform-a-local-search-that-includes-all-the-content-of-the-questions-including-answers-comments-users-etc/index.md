+++
type = "question"
title = "Is there a way to perform a local search that includes all the content of the questions, including answers, comments, users, etc.?"
description = '''Is there a way to perform a local search on ask.wireshark.org that includes all the content of the questions, including answers, comments, users, etc.? As far as I can tell, the search only searches the questions themselves.'''
date = "2011-03-25T06:50:00Z"
lastmod = "2011-03-25T06:50:00Z"
weight = 3117
keywords = [ "search" ]
aliases = [ "/questions/3117" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a way to perform a local search that includes all the content of the questions, including answers, comments, users, etc.?](/questions/3117/is-there-a-way-to-perform-a-local-search-that-includes-all-the-content-of-the-questions-including-answers-comments-users-etc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3117-score" class="post-score" title="current number of votes">1</div><span id="post-3117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to perform a local search on <a href="http://ask.wireshark.org">ask.wireshark.org</a> that includes all the content of the questions, including answers, comments, users, etc.? As far as I can tell, the search only searches the questions themselves.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-search" rel="tag" title="see questions tagged &#39;search&#39;">search</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '11, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3117" class="comments-container"></div><div id="comment-tools-3117" class="comment-tools"></div><div class="clear"></div><div id="comment-3117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

