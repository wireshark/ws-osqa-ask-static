+++
type = "question"
title = "Questions about how to use WireShark"
description = '''I&#x27;m a college student in a networking class and I need help with a question from my homework. I have never used Wireshark or anything like it. I basically have no clue what I&#x27;m doing. I have tried to play around with the program and can&#x27;t seem to figure it out. Can anyone walk me through this? Here ...'''
date = "2016-10-19T12:02:00Z"
lastmod = "2016-10-19T12:54:00Z"
weight = 56510
keywords = [ "mac-address", "wireshark" ]
aliases = [ "/questions/56510" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Questions about how to use WireShark](/questions/56510/questions-about-how-to-use-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56510-score" class="post-score" title="current number of votes">0</div><span id="post-56510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm a college student in a networking class and I need help with a question from my homework. I have never used Wireshark or anything like it. I basically have no clue what I'm doing. I have tried to play around with the program and can't seem to figure it out. Can anyone walk me through this?</p><p>Here is the exact question my text book is asking.</p><p>After starting a capture from Wireshark, start a TCP-based application like SSH, FTP, or HTTP (Web browser). Can you determine the following from your capture?</p><p>a. Source and destination layer 2 addresses (MAC)<br />
b. Source and destination layer 3 addresses (IP)<br />
c. Source and destination layer 4 addresses (port numbers)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '16, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/65eee33ab10865a3857f01b0b18c82b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NetworkingStudent007&#39;s gravatar image" /><p><span>NetworkingSt...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NetworkingStudent007 has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Oct '16, 12:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-56510" class="comments-container"><span id="56511"></span><div id="comment-56511" class="comment"><div id="post-56511-score" class="comment-score"></div><div class="comment-text"><p>The first thing to ask is whether, when you start Wireshark, you can see a list of your computer's network interfaces on which you can capture traffic (or, maybe faster, what is the output of <code>tshark -D</code> from a command line window). If not, you have installed the capturing part improperly and you'll be unable to fulfil the task. If this is the case, details about your operating system are necessary to suggest the right solution.</p></div><div id="comment-56511-info" class="comment-info"><span class="comment-age">(19 Oct '16, 12:51)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56510" class="comment-tools"></div><div class="clear"></div><div id="comment-56510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56512"></span>

<div id="answer-container-56512" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56512-score" class="post-score" title="current number of votes">0</div><span id="post-56512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I actually think I figured it out! I had to play with the settings a bit and I think I found what I needed! Thanks for the reply!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '16, 12:54</strong></p><img src="https://secure.gravatar.com/avatar/65eee33ab10865a3857f01b0b18c82b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NetworkingStudent007&#39;s gravatar image" /><p><span>NetworkingSt...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NetworkingStudent007 has no accepted answers">0%</span></p></div></div><div id="comments-container-56512" class="comments-container"></div><div id="comment-tools-56512" class="comment-tools"></div><div class="clear"></div><div id="comment-56512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

