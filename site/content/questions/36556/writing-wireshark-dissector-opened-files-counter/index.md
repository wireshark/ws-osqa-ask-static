+++
type = "question"
title = "writing wireshark dissector - opened files counter"
description = '''Hello, Lets say that I need to make a counter to count how many pcap files was open during one program session. I am wondering if is there any way to find out that the new file was loaded? Any wireshark-function which is called at the moment when the file is opened?  Thanks in advance'''
date = "2014-09-24T00:31:00Z"
lastmod = "2014-09-24T00:31:00Z"
weight = 36556
keywords = [ "global", "dissector", "pcap", "file" ]
aliases = [ "/questions/36556" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [writing wireshark dissector - opened files counter](/questions/36556/writing-wireshark-dissector-opened-files-counter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36556-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36556-score" class="post-score" title="current number of votes">0</div><span id="post-36556-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Lets say that I need to make a counter to count how many pcap files was open during one program session. I am wondering if is there any way to find out that the new file was loaded? Any wireshark-function which is called at the moment when the file is opened?<br />
</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-global" rel="tag" title="see questions tagged &#39;global&#39;">global</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '14, 00:31</strong></p><img src="https://secure.gravatar.com/avatar/4cb7b7ac61efaded7749985daff28985?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Magda%20Nowak-Trzos&#39;s gravatar image" /><p><span>Magda Nowak-...</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Magda Nowak-Trzos has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-36556" class="comments-container"></div><div id="comment-tools-36556" class="comment-tools"></div><div class="clear"></div><div id="comment-36556-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

