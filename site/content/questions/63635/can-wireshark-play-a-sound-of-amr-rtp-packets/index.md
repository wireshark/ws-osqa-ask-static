+++
type = "question"
title = "Can Wireshark play a sound of AMR RTP packets?"
description = '''Can Wireshark play a sound of AMR RTP packets?'''
date = "2017-09-24T00:10:00Z"
lastmod = "2017-09-24T09:32:00Z"
weight = 63635
keywords = [ "amr", "sip", "volte" ]
aliases = [ "/questions/63635" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark play a sound of AMR RTP packets?](/questions/63635/can-wireshark-play-a-sound-of-amr-rtp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63635-score" class="post-score" title="current number of votes">0</div><span id="post-63635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can Wireshark play a sound of AMR RTP packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amr" rel="tag" title="see questions tagged &#39;amr&#39;">amr</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-volte" rel="tag" title="see questions tagged &#39;volte&#39;">volte</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '17, 00:10</strong></p><img src="https://secure.gravatar.com/avatar/2428cee8ddf6eaa08bfb43cc4b7c0b32?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="salkayed&#39;s gravatar image" /><p><span>salkayed</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="salkayed has no accepted answers">0%</span></p></div></div><div id="comments-container-63635" class="comments-container"></div><div id="comment-tools-63635" class="comment-tools"></div><div class="clear"></div><div id="comment-63635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63637"></span>

<div id="answer-container-63637" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63637-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63637-score" class="post-score" title="current number of votes">0</div><span id="post-63637-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>AMR narrowband and wideband still have patents, so they are not included in the official version of Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Sep '17, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-63637" class="comments-container"></div><div id="comment-tools-63637" class="comment-tools"></div><div class="clear"></div><div id="comment-63637-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

