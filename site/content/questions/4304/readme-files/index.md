+++
type = "question"
title = "readme files"
description = '''Where can I get the following files?  README.capture - the capture engine internals README.design - Wireshark software design - incomplete README.developer - this file README.display_filter - Display Filter Engine README.idl2wrs - CORBA IDL converter README.packaging - how to distribute a software p...'''
date = "2011-05-31T23:11:00Z"
lastmod = "2011-06-01T23:45:00Z"
weight = 4304
keywords = [ "readme" ]
aliases = [ "/questions/4304" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [readme files](/questions/4304/readme-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4304-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4304-score" class="post-score" title="current number of votes">0</div><span id="post-4304-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can I get the following files?</p><ul><li>README.capture - the capture engine internals</li><li>README.design - Wireshark software design - incomplete</li><li>README.developer - this file</li><li>README.display_filter - Display Filter Engine</li><li>README.idl2wrs - CORBA IDL converter</li><li>README.packaging - how to distribute a software package containing WS</li><li>README.regression - regression testing of WS and TS</li><li>README.stats_tree - a tree statistics counting specific packets</li><li>README.tapping - "tap" a dissector to get protocol specific events</li><li>README.xml-output - how to work with the PDML exported output</li><li>wiretap/README.developer - how to add additional capture file types to Wiretap</li></ul><p>paste a link please.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-readme" rel="tag" title="see questions tagged &#39;readme&#39;">readme</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 May '11, 23:11</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Jun '11, 08:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4304" class="comments-container"></div><div id="comment-tools-4304" class="comment-tools"></div><div class="clear"></div><div id="comment-4304-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4309"></span>

<div id="answer-container-4309" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4309-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4309-score" class="post-score" title="current number of votes">5</div><span id="post-4309-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Download the Wireshark source code package from <a href="http://www.wireshark.org/download.html">http://www.wireshark.org/download.html</a> and unpack the compressed file. Inside you'll have to scan through the directories to find all the readme files - some are in the root directory, some are in /doc and some are in other directories as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jun '11, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-4309" class="comments-container"><span id="4319"></span><div id="comment-4319" class="comment"><div id="post-4319-score" class="comment-score">4</div><div class="comment-text"><p>You can also find the various README files in the Wireshark source repository: http://anonsvn.wireshark.org/viewvc/trunk/</p><p>As indicated they are in the top level directory, the doc subdirectory as well as in some other subdirs</p></div><div id="comment-4319-info" class="comment-info"><span class="comment-age">(01 Jun '11, 08:02)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="4329"></span><div id="comment-4329" class="comment"><div id="post-4329-score" class="comment-score">1</div><div class="comment-text"><p>Hi, You can also browse the files here http://anonsvn.wireshark.org/viewvc/trunk/doc/</p></div><div id="comment-4329-info" class="comment-info"><span class="comment-age">(01 Jun '11, 23:45)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-4309" class="comment-tools"></div><div class="clear"></div><div id="comment-4309-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

