+++
type = "question"
title = "Need dissector for G.781 SDH option II"
description = '''When capturing Synchronous Ethernet packets the ESMC quality level always says &quot;QL-INV1&quot; - invalid. After verifying that my signal is valid at QL PRS, I did some searching and found that the dissector is set to use option 1: https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3551 &quot;Added: ESMC suppo...'''
date = "2013-07-31T16:41:00Z"
lastmod = "2013-07-31T16:41:00Z"
weight = 23492
keywords = [ "dissector" ]
aliases = [ "/questions/23492" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need dissector for G.781 SDH option II](/questions/23492/need-dissector-for-g781-sdh-option-ii)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23492-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23492-score" class="post-score" title="current number of votes">0</div><span id="post-23492-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When capturing Synchronous Ethernet packets the ESMC quality level always says "QL-INV1" - invalid.</p><p>After verifying that my signal is valid at QL PRS, I did some searching and found that the dissector is set to use option 1:</p><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3551">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3551</a> "Added: ESMC support as per G.8264 (Slow Protocol Subtype 0x0a). * QL codes are dissected according to G.781 5.5.1.1 "Option I SDH". "</p><p>Is there a way to select G.781 5.5.1.2 "Option 2 SDH" for the dissector instead of option 1?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '13, 16:41</strong></p><img src="https://secure.gravatar.com/avatar/0edcba6eadb8012d1360ae9b805d8fc4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gcaesar&#39;s gravatar image" /><p><span>gcaesar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gcaesar has no accepted answers">0%</span></p></div></div><div id="comments-container-23492" class="comments-container"></div><div id="comment-tools-23492" class="comment-tools"></div><div class="clear"></div><div id="comment-23492-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

