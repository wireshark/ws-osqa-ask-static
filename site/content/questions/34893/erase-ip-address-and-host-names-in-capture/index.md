+++
type = "question"
title = "Erase IP address and host names in capture"
description = '''Is there a way to erase ip addresses and and host names from captures? My company policy is this information can not be turned over to technical support. I need the cleanest safest way to erase this from a capture. '''
date = "2014-07-24T11:39:00Z"
lastmod = "2014-09-03T07:38:00Z"
weight = 34893
keywords = [ "ip", "hostname", "capture", "address" ]
aliases = [ "/questions/34893" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Erase IP address and host names in capture](/questions/34893/erase-ip-address-and-host-names-in-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34893-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34893-score" class="post-score" title="current number of votes">0</div><span id="post-34893-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to erase ip addresses and and host names from captures? My company policy is this information can not be turned over to technical support. I need the cleanest safest way to erase this from a capture.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-hostname" rel="tag" title="see questions tagged &#39;hostname&#39;">hostname</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '14, 11:39</strong></p><img src="https://secure.gravatar.com/avatar/de64d1089e4cc4476ae8708fd90e5391?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="parakiteiz&#39;s gravatar image" /><p><span>parakiteiz</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="parakiteiz has no accepted answers">0%</span></p></div></div><div id="comments-container-34893" class="comments-container"></div><div id="comment-tools-34893" class="comment-tools"></div><div class="clear"></div><div id="comment-34893-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34895"></span>

<div id="answer-container-34895" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34895-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34895-score" class="post-score" title="current number of votes">3</div><span id="post-34895-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="parakiteiz has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look at <a href="http://www.tracewrangler.com">TraceWrangler</a>, a tool made by our member <span>@Jasper</span>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jul '14, 12:00</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-34895" class="comments-container"><span id="35961"></span><div id="comment-35961" class="comment"><div id="post-35961-score" class="comment-score"></div><div class="comment-text"><p>I also found you could export the capture to text file. This appeared to be pretty perfect for me. Because of my security concerns network I was not allowed to use Trace Wrangler.</p></div><div id="comment-35961-info" class="comment-info"><span class="comment-age">(03 Sep '14, 07:38)</span> <span class="comment-user userinfo">parakiteiz</span></div></div></div><div id="comment-tools-34895" class="comment-tools"></div><div class="clear"></div><div id="comment-34895-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

