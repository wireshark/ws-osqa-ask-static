+++
type = "question"
title = "wireshark-bin quit unexpectedly (Mac OS X Leopard)"
description = '''Every time I try to open Wireshark, all I get is the error message: wireshark-bin quit unexpectedly  How do I fix this?'''
date = "2011-10-02T14:50:00Z"
lastmod = "2011-10-02T15:44:00Z"
weight = 6682
keywords = [ "startup", "osx", "mac", "crash" ]
aliases = [ "/questions/6682" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark-bin quit unexpectedly (Mac OS X Leopard)](/questions/6682/wireshark-bin-quit-unexpectedly-mac-os-x-leopard)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6682-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6682-score" class="post-score" title="current number of votes">0</div><span id="post-6682-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Every time I try to open Wireshark, all I get is the error message:</p><pre><code>wireshark-bin quit unexpectedly</code></pre><p>How do I fix this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span> <span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Oct '11, 14:50</strong></p><img src="https://secure.gravatar.com/avatar/c9673316d6150d99a7bd8616d5edc4e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="danielle2013&#39;s gravatar image" /><p><span>danielle2013</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="danielle2013 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Oct '11, 15:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6682" class="comments-container"><span id="6684"></span><div id="comment-6684" class="comment"><div id="post-6684-score" class="comment-score"></div><div class="comment-text"><p>There's usually an error message somewhere (e.g., in the syslog). See this <a href="http://ask.wireshark.org/questions/4528/wireshark-does-not-run">post</a> to find out what that is.</p></div><div id="comment-6684-info" class="comment-info"><span class="comment-age">(02 Oct '11, 15:44)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-6682" class="comment-tools"></div><div class="clear"></div><div id="comment-6682-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

