+++
type = "question"
title = "Display mysq.row.text 1 line per row"
description = '''Currently, it displays all rows in a single line'''
date = "2014-10-17T07:32:00Z"
lastmod = "2014-10-17T14:37:00Z"
weight = 37133
keywords = [ "mysql" ]
aliases = [ "/questions/37133" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Display mysq.row.text 1 line per row](/questions/37133/display-mysqrowtext-1-line-per-row)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37133-score" class="post-score" title="current number of votes">0</div><span id="post-37133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Currently, it displays all rows in a single line</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mysql" rel="tag" title="see questions tagged &#39;mysql&#39;">mysql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '14, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/ee1093d953dc9b3ee7b951288062eac2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jameswang&#39;s gravatar image" /><p><span>jameswang</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jameswang has no accepted answers">0%</span></p></div></div><div id="comments-container-37133" class="comments-container"><span id="37134"></span><div id="comment-37134" class="comment"><div id="post-37134-score" class="comment-score"></div><div class="comment-text"><p>If this is a feature request this should go to bugzilla. If this is a question on how to do it, I'm missing a question mark somewhere.</p></div><div id="comment-37134-info" class="comment-info"><span class="comment-age">(17 Oct '14, 08:44)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-37133" class="comment-tools"></div><div class="clear"></div><div id="comment-37133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37138"></span>

<div id="answer-container-37138" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37138-score" class="post-score" title="current number of votes">0</div><span id="post-37138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>where is the bugzilla please?</p><p>How to if the feature already existed? Thanks</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '14, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/ee1093d953dc9b3ee7b951288062eac2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jameswang&#39;s gravatar image" /><p><span>jameswang</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jameswang has no accepted answers">0%</span></p></div></div><div id="comments-container-37138" class="comments-container"><span id="37141"></span><div id="comment-37141" class="comment"><div id="post-37141-score" class="comment-score"></div><div class="comment-text"><p><a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a></p><p>No this feature do not exist yet.</p></div><div id="comment-37141-info" class="comment-info"><span class="comment-age">(17 Oct '14, 14:37)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-37138" class="comment-tools"></div><div class="clear"></div><div id="comment-37138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

