+++
type = "question"
title = "1.12.0 GTK3 many render issues"
description = '''Hi, the standard deb packed version GTK3 1.12.0 under Ubuntu repo has many render issues. Just to name few: enormous fonts, enormous Preference config/setting window, microscopic interfaces List config/setting window, HORIZONTAL scroll bars missing in frames list, restart capture adds all interfaces...'''
date = "2015-05-08T02:08:00Z"
lastmod = "2015-05-08T04:10:00Z"
weight = 42202
keywords = [ "gtk", "bug" ]
aliases = [ "/questions/42202" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [1.12.0 GTK3 many render issues](/questions/42202/1120-gtk3-many-render-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42202-score" class="post-score" title="current number of votes">0</div><span id="post-42202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,<br />
the standard deb packed version GTK3 1.12.0 under Ubuntu repo has many render issues.<br />
Just to name few: enormous fonts, enormous Preference config/setting window, microscopic interfaces List config/setting window, <strong>HORIZONTAL scroll bars</strong> missing in frames list, restart capture adds all interfaces, and so on................<br />
I tried to compile the GTK2 only version, and there most of those issues are fixed.<br />
But I'm unable to install GTK2 version ( <a href="https://ask.wireshark.org/questions/42201/1124-hangs-on-ubuntu-after-install">https://ask.wireshark.org/questions/42201/1124-hangs-on-ubuntu-after-install</a> .. :(</p><p>How can I have a <strong>normal</strong> working Wireshark, as it used to be??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '15, 02:08</strong></p><img src="https://secure.gravatar.com/avatar/054a5bf4cea5a2e02665f19ce9dde42a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikita&#39;s gravatar image" /><p><span>nikita</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikita has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-42202" class="comments-container"></div><div id="comment-tools-42202" class="comment-tools"></div><div class="clear"></div><div id="comment-42202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42203"></span>

<div id="answer-container-42203" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42203-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42203-score" class="post-score" title="current number of votes">0</div><span id="post-42203-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you tried the <a href="https://launchpad.net/~wireshark-dev/+archive/ubuntu/stable">Wireshark Developer PPA</a> package? Currently at 1.12.4.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 May '15, 02:37</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span> </br></br></p></div></div><div id="comments-container-42203" class="comments-container"><span id="42205"></span><div id="comment-42205" class="comment"><div id="post-42205-score" class="comment-score"></div><div class="comment-text"><p>The last PPA is for trusty, I have utopic. Yes, as I wrote, I tried to compile latest 1.12.4 GTK2, but after install it hangs forever :(</p></div><div id="comment-42205-info" class="comment-info"><span class="comment-age">(08 May '15, 04:04)</span> <span class="comment-user userinfo">nikita</span></div></div><span id="42207"></span><div id="comment-42207" class="comment"><div id="post-42207-score" class="comment-score"></div><div class="comment-text"><p>You didn't note your OS version in the question, the PPA only targets LTS versions.</p></div><div id="comment-42207-info" class="comment-info"><span class="comment-age">(08 May '15, 04:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-42203" class="comment-tools"></div><div class="clear"></div><div id="comment-42203-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

