+++
type = "question"
title = "not detecting usb broadband modem"
description = '''Hi I am a new WIRESHARK user.When i tried to capture with my Wifi home network it is working fine.But when i tried to capture with usb broadband modem is not listing in the interface.So i cant able to capture details Cyril Baby K'''
date = "2011-02-24T21:24:00Z"
lastmod = "2011-04-15T01:43:00Z"
weight = 2562
keywords = [ "interface" ]
aliases = [ "/questions/2562" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [not detecting usb broadband modem](/questions/2562/not-detecting-usb-broadband-modem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2562-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2562-score" class="post-score" title="current number of votes">0</div><span id="post-2562-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am a new WIRESHARK user.When i tried to capture with my Wifi home network it is working fine.But when i tried to capture with usb broadband modem is not listing in the interface.So i cant able to capture details</p><p>Cyril Baby K</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '11, 21:24</strong></p><img src="https://secure.gravatar.com/avatar/9fe1f75a7aad10d9c7b2d5edb81dbb78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cyril&#39;s gravatar image" /><p><span>Cyril</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cyril has no accepted answers">0%</span></p></div></div><div id="comments-container-2562" class="comments-container"><span id="2625"></span><div id="comment-2625" class="comment"><div id="post-2625-score" class="comment-score"></div><div class="comment-text"><p>By "broadband" do you mean "mobile broadband", i.e. cell phone or WiMax? And are you doing this on 64-bit Windows XP, Windows Vista, or Windows 7?</p></div><div id="comment-2625-info" class="comment-info"><span class="comment-age">(01 Mar '11, 14:33)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-2562" class="comment-tools"></div><div class="clear"></div><div id="comment-2562-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3507"></span>

<div id="answer-container-3507" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3507-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3507-score" class="post-score" title="current number of votes">0</div><span id="post-3507-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need to use Win P Cap 3.1. Then listen on the Generic Dial Up Adapter. Cheers Doc</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '11, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/f2918caab20f5747d3263f63605b9934?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Doc&#39;s gravatar image" /><p><span>Doc</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Doc has no accepted answers">0%</span></p></div></div><div id="comments-container-3507" class="comments-container"></div><div id="comment-tools-3507" class="comment-tools"></div><div class="clear"></div><div id="comment-3507-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

