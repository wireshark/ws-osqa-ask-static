+++
type = "question"
title = "Are there any Network Support Companies that are Wireshark Certified?"
description = '''I believe we have problems with RST/Broken TCP packets and can tie some RST/Broken TCP packets to users losing their connections to the server. This, of course, is sporadic and I can always see an RST/Broken TCP when they experience the connection error, but they don&#x27;t always get a connection error ...'''
date = "2011-08-15T14:47:00Z"
lastmod = "2011-08-16T15:27:00Z"
weight = 5705
keywords = [ "rst", "broken", "tcp" ]
aliases = [ "/questions/5705" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Are there any Network Support Companies that are Wireshark Certified?](/questions/5705/are-there-any-network-support-companies-that-are-wireshark-certified)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5705-score" class="post-score" title="current number of votes">0</div><span id="post-5705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I believe we have problems with RST/Broken TCP packets and can tie some RST/Broken TCP packets to users losing their connections to the server. This, of course, is sporadic and I can always see an RST/Broken TCP when they experience the connection error, but they don't always get a connection error when I see an RST/Broken TCP. We've talked to our application vendor and they say it's our network, and the network team says it's not the network...but maybe could be the virtual servers/workstations. Unfortunately, we really don't have anyone who is highly skilled at troubleshooting with Wireshark. Are there Third Party vendors who have certified Wireshark Professionals who we could work with to help narrow this down?</p><p>Thank You!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rst" rel="tag" title="see questions tagged &#39;rst&#39;">rst</span> <span class="post-tag tag-link-broken" rel="tag" title="see questions tagged &#39;broken&#39;">broken</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Aug '11, 14:47</strong></p><img src="https://secure.gravatar.com/avatar/0195dec07f6e7baab7ef3aee861d9e95?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JT89012&#39;s gravatar image" /><p><span>JT89012</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JT89012 has no accepted answers">0%</span></p></div></div><div id="comments-container-5705" class="comments-container"></div><div id="comment-tools-5705" class="comment-tools"></div><div class="clear"></div><div id="comment-5705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5717"></span>

<div id="answer-container-5717" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5717-score" class="post-score" title="current number of votes">0</div><span id="post-5717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm not sure you should be looking for a "vendor", but more a troubleshooting/consultancy firm/professional. There are quite a few of us here on the Q&amp;A site. I think it all depends a bit on whether remote analysis of tracefiles is what you're looking for or if you need someone to come on-site.</p><p>It might help is you'd be more specific in what you would need...</p><p>(please look at my profile for my contact details if you'd like to get in touch with me)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Aug '11, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-5717" class="comments-container"></div><div id="comment-tools-5717" class="comment-tools"></div><div class="clear"></div><div id="comment-5717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

