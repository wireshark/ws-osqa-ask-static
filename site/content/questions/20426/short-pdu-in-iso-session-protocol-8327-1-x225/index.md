+++
type = "question"
title = "Short PDU in Iso Session Protocol 8327-1 (X.225)"
description = '''I just git-ted the source, looked into epan/dissectors/packet-ses.c and found only references to the canonical PDUs, those that one can find in the &quot;T-REC-X.225-199511-I!!PDF-E.pdf&quot;. Is there any support for the short-PDUs, namely the ones contained in the Amendment 1 to the original protocol spec? ...'''
date = "2013-04-15T10:11:00Z"
lastmod = "2013-04-15T10:11:00Z"
weight = 20426
keywords = [ "amendement", "8327-1", "session", "x225", "x.225" ]
aliases = [ "/questions/20426" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Short PDU in Iso Session Protocol 8327-1 (X.225)](/questions/20426/short-pdu-in-iso-session-protocol-8327-1-x225)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20426-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20426-score" class="post-score" title="current number of votes">0</div><span id="post-20426-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just git-ted the source, looked into epan/dissectors/packet-ses.c and found only references to the canonical PDUs, those that one can find in the "T-REC-X.225-199511-I!!PDF-E.pdf".</p><p>Is there any support for the short-PDUs, namely the ones contained in the Amendment 1 to the original protocol spec?</p><p>I don't understand when it is supposed to be used and I cannot even find either a library or a generated pcap about that. Please tell me is a dead amendment and I can go over it. :-p</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amendement" rel="tag" title="see questions tagged &#39;amendement&#39;">amendement</span> <span class="post-tag tag-link-8327-1" rel="tag" title="see questions tagged &#39;8327-1&#39;">8327-1</span> <span class="post-tag tag-link-session" rel="tag" title="see questions tagged &#39;session&#39;">session</span> <span class="post-tag tag-link-x225" rel="tag" title="see questions tagged &#39;x225&#39;">x225</span> <span class="post-tag tag-link-x.225" rel="tag" title="see questions tagged &#39;x.225&#39;">x.225</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '13, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/12bc430f55a4862ae9556a694858bd28?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="splinux&#39;s gravatar image" /><p><span>splinux</span><br />
<span class="score" title="36 reputation points">36</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="splinux has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Apr '13, 10:12</strong> </span></p></div></div><div id="comments-container-20426" class="comments-container"></div><div id="comment-tools-20426" class="comment-tools"></div><div class="clear"></div><div id="comment-20426-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

