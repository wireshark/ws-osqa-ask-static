+++
type = "question"
title = "Reporting a problem to the developers"
description = '''To the portal I had wireshark display an error message this morning while running a capture at a client site. It was not an intense capture, so this is not a resource problem on the laptop (laptop has i7, g gb Ram, tera HDD). The error displayed is as follows: &quot;Error while capturing packets: read er...'''
date = "2010-10-28T06:32:00Z"
lastmod = "2010-10-28T14:21:00Z"
weight = 723
keywords = [ "error" ]
aliases = [ "/questions/723" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Reporting a problem to the developers](/questions/723/reporting-a-problem-to-the-developers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-723-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-723-score" class="post-score" title="current number of votes">0</div><span id="post-723-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>To the portal</p><p>I had wireshark display an error message this morning while running a capture at a client site. It was not an intense capture, so this is not a resource problem on the laptop (laptop has i7, g gb Ram, tera HDD). The error displayed is as follows: "Error while capturing packets: read error: PacketReceivePacket failed - Please report this to the Wireshark developers.(This is not a crash; please do not report it as such). Can someone provide guidance as to the proper procedure to report this bug to the developers?</p><p>Thank you KMNR user</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '10, 06:32</strong></p><img src="https://secure.gravatar.com/avatar/9e96b23e3495316e470ba9b487b82a73?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kmnruser&#39;s gravatar image" /><p><span>kmnruser</span><br />
<span class="score" title="26 reputation points">26</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kmnruser has no accepted answers">0%</span></p></div></div><div id="comments-container-723" class="comments-container"></div><div id="comment-tools-723" class="comment-tools"></div><div class="clear"></div><div id="comment-723-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="725"></span>

<div id="answer-container-725" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-725-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-725-score" class="post-score" title="current number of votes">1</div><span id="post-725-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="kmnruser has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Error reporting can be done through the Wireshark <a href="http://www.wireshark.org/lists/">mailing lists</a>, or the Wireshark <a href="https://bugs.wireshark.org/bugzilla/">Bugzilla database</a>. Do some searches first, to see if your problem has come up before (it has) and what the cause/resolution was. See more on the Wireshark <a href="http://wiki.wireshark.org/ReportingBugs">Wiki</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Oct '10, 08:03</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-725" class="comments-container"></div><div id="comment-tools-725" class="comment-tools"></div><div class="clear"></div><div id="comment-725-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="731"></span>

<div id="answer-container-731" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-731-score" class="post-score" title="current number of votes">2</div><span id="post-731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That <em>particular</em> problem is probably a WinPcap problem, so you might want to try <a href="http://www.winpcap.org/bugs.htm">reporting it to the WinPcap developers</a>.</p><p>When Wireshark/TShark/dumpcap gets an "unexpected" error from libpcap or WinPcap, such as that one, it is at best difficult and at worst impossible for the program to determine whether it's a Wireshark problem or a libpcap/WinPcap problem, so the error message suggests reporting the problem to the Wireshark developers. Jaap's answer indicates how to do that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Oct '10, 14:21</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-731" class="comments-container"></div><div id="comment-tools-731" class="comment-tools"></div><div class="clear"></div><div id="comment-731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

