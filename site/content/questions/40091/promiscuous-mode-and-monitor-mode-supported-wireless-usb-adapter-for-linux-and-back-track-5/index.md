+++
type = "question"
title = "[closed] promiscuous mode and monitor mode supported wireless usb adapter for linux and back track 5"
description = '''hello  i am using wireshark under back track and linux(ubuntu).please tell me which wireless usb adapter support promiscuous mode and monitor mode at the same time? i am using tp-link tl-wn722n(atheros ar9271) but support only promiscuous mode.'''
date = "2015-02-26T03:21:00Z"
lastmod = "2015-02-26T03:21:00Z"
weight = 40091
keywords = [ "wireshark" ]
aliases = [ "/questions/40091" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] promiscuous mode and monitor mode supported wireless usb adapter for linux and back track 5](/questions/40091/promiscuous-mode-and-monitor-mode-supported-wireless-usb-adapter-for-linux-and-back-track-5)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40091-score" class="post-score" title="current number of votes">0</div><span id="post-40091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello i am using wireshark under back track and linux(ubuntu).please tell me which wireless usb adapter support promiscuous mode and monitor mode at the same time? i am using tp-link tl-wn722n(atheros ar9271) but support only promiscuous mode.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '15, 03:21</strong></p><img src="https://secure.gravatar.com/avatar/862faa5d26b66d1e5ade6679e966563c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hackerguru1989&#39;s gravatar image" /><p><span>hackerguru1989</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hackerguru1989 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>26 Feb '15, 04:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-40091" class="comments-container"></div><div id="comment-tools-40091" class="comment-tools"></div><div class="clear"></div><div id="comment-40091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Please ask that question in a backtrack/kali forum. This Q&A site is primarily about Wireshark and related network troubleshooting, not product recommendations and linux driver issues. You can however find some suggestions if you search this site for backtrack and/or kali and usb." by Kurt Knochner 26 Feb '15, 04:45

</div>

</div>

</div>

