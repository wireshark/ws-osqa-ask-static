+++
type = "question"
title = "All Options are greyed out in wireless toolbar in Linux in monitor mode"
description = '''Hi  While sniffing packets using Intel wireless card in Fedora we can enable the wireless toolbar under view but all the options are greyed out . How Can we enable this . We need to capture packets on a specific channel without this we cannot do it. '''
date = "2015-03-25T15:05:00Z"
lastmod = "2015-03-27T12:23:00Z"
weight = 40861
keywords = [ "wireless", "wireless_toolbar" ]
aliases = [ "/questions/40861" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [All Options are greyed out in wireless toolbar in Linux in monitor mode](/questions/40861/all-options-are-greyed-out-in-wireless-toolbar-in-linux-in-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40861-score" class="post-score" title="current number of votes">0</div><span id="post-40861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi While sniffing packets using Intel wireless card in Fedora we can enable the wireless toolbar under view but all the options are greyed out . How Can we enable this . We need to capture packets on a specific channel without this we cannot do it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wireless_toolbar" rel="tag" title="see questions tagged &#39;wireless_toolbar&#39;">wireless_toolbar</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '15, 15:05</strong></p><img src="https://secure.gravatar.com/avatar/43e3b3e629033110dd09296227325805?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Siva_wire&#39;s gravatar image" /><p><span>Siva_wire</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Siva_wire has no accepted answers">0%</span></p></div></div><div id="comments-container-40861" class="comments-container"><span id="40946"></span><div id="comment-40946" class="comment"><div id="post-40946-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark are you running? Please post a screenshot and the output of iwconfig.</p></div><div id="comment-40946-info" class="comment-info"><span class="comment-age">(27 Mar '15, 12:23)</span> <span class="comment-user userinfo">Roland</span></div></div></div><div id="comment-tools-40861" class="comment-tools"></div><div class="clear"></div><div id="comment-40861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

