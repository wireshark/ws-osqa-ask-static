+++
type = "question"
title = "Can I convert PCAP file from Wireshark to allow Agilent tool open it?"
description = '''I save the tracing file of SS7 - Sigtran from GSM network in PCAP format by Wireshark , But I really need to open it by Agilent tool. Does it have any tool to convert wireshark PCAP file to any format that Agilent tool can open. Thanks in advance.'''
date = "2010-11-30T00:56:00Z"
lastmod = "2010-12-02T00:20:00Z"
weight = 1172
keywords = [ "convert", "agilent" ]
aliases = [ "/questions/1172" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I convert PCAP file from Wireshark to allow Agilent tool open it?](/questions/1172/can-i-convert-pcap-file-from-wireshark-to-allow-agilent-tool-open-it)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1172-score" class="post-score" title="current number of votes">0</div><span id="post-1172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I save the tracing file of SS7 - Sigtran from GSM network in PCAP format by Wireshark , But I really need to open it by Agilent tool.</p><p>Does it have any tool to convert wireshark PCAP file to any format that Agilent tool can open.</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-convert" rel="tag" title="see questions tagged &#39;convert&#39;">convert</span> <span class="post-tag tag-link-agilent" rel="tag" title="see questions tagged &#39;agilent&#39;">agilent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '10, 00:56</strong></p><img src="https://secure.gravatar.com/avatar/3ebf2b64440e010d2a68881950f4144e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="toon&#39;s gravatar image" /><p><span>toon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="toon has no accepted answers">0%</span></p></div></div><div id="comments-container-1172" class="comments-container"><span id="1204"></span><div id="comment-1204" class="comment"><div id="post-1204-score" class="comment-score"></div><div class="comment-text"><p>It would be in the best interest of the Agilent folks to read .pcap files - really now - I bet they have an internal tool to convert/read the files. Ask 'em.</p></div><div id="comment-1204-info" class="comment-info"><span class="comment-age">(02 Dec '10, 00:20)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-1172" class="comment-tools"></div><div class="clear"></div><div id="comment-1172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1173"></span>

<div id="answer-container-1173" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1173-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1173-score" class="post-score" title="current number of votes">0</div><span id="post-1173-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If there's some file format that the Agilent tool in question can read, <em>AND</em> either that format is documented somewhere (documented well enough that we could write code that can write a file of that format) or we have enough samples of files of that format and decodes of those files by the Agilent tool (so that we can reverse-engineer the format of those files), it might be possible to write such a tool - or to modify Wireshark so that it could write those files. (Documentation is better than reverse engineering; reverse-engineering to the extent that we can <em>write</em> those files is harder than just reverse-engineering to the extent that we could <em>read</em> those files, and would probably require that we be able to have somebody try to read the files with that Agilent tool, for testing purposes.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '10, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-1173" class="comments-container"></div><div id="comment-tools-1173" class="comment-tools"></div><div class="clear"></div><div id="comment-1173-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

