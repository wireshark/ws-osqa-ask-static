+++
type = "question"
title = "query name for normal LLMNR packet"
description = '''Hi, I capture a random LLMNR packet. with query name : brn30055c6d7f6f.  I know what is LLMRN and the mechanism, but May I know what is that? and why ppl actually query for that name&quot;'''
date = "2016-08-23T11:39:00Z"
lastmod = "2016-08-23T23:24:00Z"
weight = 55081
keywords = [ "llmrn" ]
aliases = [ "/questions/55081" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [query name for normal LLMNR packet](/questions/55081/query-name-for-normal-llmnr-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55081-score" class="post-score" title="current number of votes">0</div><span id="post-55081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I capture a random LLMNR packet. with query name : brn30055c6d7f6f. I know what is LLMRN and the mechanism, but May I know what is that? and why ppl actually query for that name"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-llmrn" rel="tag" title="see questions tagged &#39;llmrn&#39;">llmrn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '16, 11:39</strong></p><img src="https://secure.gravatar.com/avatar/e066b2220b2334452ae6c68a630dc506?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jqjq&#39;s gravatar image" /><p><span>jqjq</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jqjq has no accepted answers">0%</span></p></div></div><div id="comments-container-55081" class="comments-container"></div><div id="comment-tools-55081" class="comment-tools"></div><div class="clear"></div><div id="comment-55081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55086"></span>

<div id="answer-container-55086" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55086-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55086-score" class="post-score" title="current number of votes">0</div><span id="post-55086-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://coffer.com/mac_find/?string=30055c">brn30055c</a> suggests an Ethernet MAC address of a Brother device. So this node is trying to find this device maybe? I guess a printer driver.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Aug '16, 23:24</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-55086" class="comments-container"></div><div id="comment-tools-55086" class="comment-tools"></div><div class="clear"></div><div id="comment-55086-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

