+++
type = "question"
title = "WireShark isn&#x27;t reading ICMP packets sent to me by other networks"
description = '''Whenever I try and ping myself (cmd -&amp;gt; ping myip) I can trace the echo replies and requests but when I asked my friend to do it no such packets were detected. Maybe he wasn&#x27;t using ICMP protocol? I don&#x27;t think so &#x27;cause even when I analyzed activity with no filter I couldn&#x27;t found his address. An...'''
date = "2016-08-31T18:36:00Z"
lastmod = "2016-08-31T21:52:00Z"
weight = 55258
keywords = [ "icmp", "echo" ]
aliases = [ "/questions/55258" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark isn't reading ICMP packets sent to me by other networks](/questions/55258/wireshark-isnt-reading-icmp-packets-sent-to-me-by-other-networks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55258-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55258-score" class="post-score" title="current number of votes">0</div><span id="post-55258-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Whenever I try and ping myself (cmd -&gt; ping myip) I can trace the echo replies and requests but when I asked my friend to do it no such packets were detected. Maybe he wasn't using ICMP protocol? I don't think so 'cause even when I analyzed activity with no filter I couldn't found his address. Any idea on why that's happening?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-icmp" rel="tag" title="see questions tagged &#39;icmp&#39;">icmp</span> <span class="post-tag tag-link-echo" rel="tag" title="see questions tagged &#39;echo&#39;">echo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '16, 18:36</strong></p><img src="https://secure.gravatar.com/avatar/0ff84e431f808e73075ad133cdba5f12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NewbieNetwork&#39;s gravatar image" /><p><span>NewbieNetwork</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NewbieNetwork has no accepted answers">0%</span></p></div></div><div id="comments-container-55258" class="comments-container"><span id="55261"></span><div id="comment-55261" class="comment"><div id="post-55261-score" class="comment-score"></div><div class="comment-text"><p>Do you understand the notion of "private" and "public" IP address and Network Address Translation (NAT)? Can you share the first two numbers of your IP address (e.g. 172.16.x.y)?</p></div><div id="comment-55261-info" class="comment-info"><span class="comment-age">(31 Aug '16, 21:52)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-55258" class="comment-tools"></div><div class="clear"></div><div id="comment-55258-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

