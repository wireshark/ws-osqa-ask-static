+++
type = "question"
title = "real time capture"
description = '''Hello.... I would like to capture data from a network in real time and save it as csv file . Is it possible? '''
date = "2016-06-28T00:12:00Z"
lastmod = "2016-06-28T00:49:00Z"
weight = 53685
keywords = [ "real", "real-time" ]
aliases = [ "/questions/53685" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [real time capture](/questions/53685/real-time-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53685-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53685-score" class="post-score" title="current number of votes">0</div><span id="post-53685-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.... I would like to capture data from a network in real time and save it as csv file .</p><p>Is it possible?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-real" rel="tag" title="see questions tagged &#39;real&#39;">real</span> <span class="post-tag tag-link-real-time" rel="tag" title="see questions tagged &#39;real-time&#39;">real-time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '16, 00:12</strong></p><img src="https://secure.gravatar.com/avatar/52ef9192649f1523d921741f467ce8d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ntn&#39;s gravatar image" /><p><span>ntn</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ntn has no accepted answers">0%</span></p></div></div><div id="comments-container-53685" class="comments-container"></div><div id="comment-tools-53685" class="comment-tools"></div><div class="clear"></div><div id="comment-53685-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53687"></span>

<div id="answer-container-53687" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53687-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53687-score" class="post-score" title="current number of votes">1</div><span id="post-53687-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ntn has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://www.wireshark.org/docs/man-pages/tshark.html">tshark</a> is the answer to your need.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jun '16, 00:49</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-53687" class="comments-container"></div><div id="comment-tools-53687" class="comment-tools"></div><div class="clear"></div><div id="comment-53687-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

