+++
type = "question"
title = "&#x27;there are no interfaces on which a capture can be done&#x27;"
description = '''When I choose Capture..Interfaces, I get this message - &#x27;there are no interfaces on which a capture can be done&#x27; i am running the software on mac OSX version 10.9.5 how do i fix this problem?'''
date = "2014-10-03T12:46:00Z"
lastmod = "2014-10-03T16:57:00Z"
weight = 36825
keywords = [ "interfaces" ]
aliases = [ "/questions/36825" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ['there are no interfaces on which a capture can be done'](/questions/36825/there-are-no-interfaces-on-which-a-capture-can-be-done)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36825-score" class="post-score" title="current number of votes">0</div><span id="post-36825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I choose Capture..Interfaces, I get this message - 'there are no interfaces on which a capture can be done' i am running the software on mac OSX version 10.9.5 how do i fix this problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '14, 12:46</strong></p><img src="https://secure.gravatar.com/avatar/e22651e5bfcc6568a1479cad8af835df?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mattlaz&#39;s gravatar image" /><p><span>mattlaz</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mattlaz has no accepted answers">0%</span></p></div></div><div id="comments-container-36825" class="comments-container"><span id="36829"></span><div id="comment-36829" class="comment"><div id="post-36829-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark did you install?</p></div><div id="comment-36829-info" class="comment-info"><span class="comment-age">(03 Oct '14, 16:57)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-36825" class="comment-tools"></div><div class="clear"></div><div id="comment-36825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36828"></span>

<div id="answer-container-36828" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36828-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36828-score" class="post-score" title="current number of votes">0</div><span id="post-36828-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess this is a privilege problem. See <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">http://wiki.wireshark.org/CaptureSetup/CapturePrivileges</a> for some insight.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Oct '14, 16:16</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-36828" class="comments-container"></div><div id="comment-tools-36828" class="comment-tools"></div><div class="clear"></div><div id="comment-36828-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

