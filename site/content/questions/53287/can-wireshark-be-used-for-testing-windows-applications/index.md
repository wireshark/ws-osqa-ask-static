+++
type = "question"
title = "Can Wireshark be used for testing Windows Applications?"
description = '''We have lot of windows applications (i.e. desktop applications) for which the DBs will be hosted on Cloud servers. Can Wireshark be used to carry out latency test and response time test for such applications?'''
date = "2016-06-07T10:06:00Z"
lastmod = "2016-06-07T10:06:00Z"
weight = 53287
keywords = [ "windows7", ".net", "desktop" ]
aliases = [ "/questions/53287" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark be used for testing Windows Applications?](/questions/53287/can-wireshark-be-used-for-testing-windows-applications)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53287-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53287-score" class="post-score" title="current number of votes">0</div><span id="post-53287-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have lot of windows applications (i.e. desktop applications) for which the DBs will be hosted on Cloud servers. Can Wireshark be used to carry out latency test and response time test for such applications?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-.net" rel="tag" title="see questions tagged &#39;.net&#39;">.net</span> <span class="post-tag tag-link-desktop" rel="tag" title="see questions tagged &#39;desktop&#39;">desktop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jun '16, 10:06</strong></p><img src="https://secure.gravatar.com/avatar/29eab7efeb08417df9f55ebfa9f5364d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SumeetSD&#39;s gravatar image" /><p><span>SumeetSD</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SumeetSD has no accepted answers">0%</span></p></div></div><div id="comments-container-53287" class="comments-container"></div><div id="comment-tools-53287" class="comment-tools"></div><div class="clear"></div><div id="comment-53287-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

