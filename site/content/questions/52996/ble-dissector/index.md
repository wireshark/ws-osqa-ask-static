+++
type = "question"
title = "BLE Dissector"
description = '''Hello Experts,  I am looking at writing some dissectors for BLE beacons, specifically for the &quot;Manufacturer Specific&quot; fields within my frames.  Can anybody provide me with any tips or pointers to documentation concerning writing dissectors for BLE? I could find a lot of documentation for IP based di...'''
date = "2016-05-27T06:21:00Z"
lastmod = "2016-07-18T10:31:00Z"
weight = 52996
keywords = [ "dissector", "bluetooth" ]
aliases = [ "/questions/52996" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [BLE Dissector](/questions/52996/ble-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52996-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52996-score" class="post-score" title="current number of votes">0</div><span id="post-52996-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Experts,</p><p>I am looking at writing some dissectors for BLE beacons, specifically for the "Manufacturer Specific" fields within my frames.</p><p>Can anybody provide me with any tips or pointers to documentation concerning writing dissectors for BLE? I could find a lot of documentation for IP based dissection, but content on BLE is very very light.</p><p>Any pointers would be greatly appreciated.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '16, 06:21</strong></p><img src="https://secure.gravatar.com/avatar/fc13abd7c82055bfbec02496e094d39a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krypton&#39;s gravatar image" /><p><span>krypton</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krypton has no accepted answers">0%</span></p></div></div><div id="comments-container-52996" class="comments-container"></div><div id="comment-tools-52996" class="comment-tools"></div><div class="clear"></div><div id="comment-52996-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54134"></span>

<div id="answer-container-54134" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54134-score" class="post-score" title="current number of votes">0</div><span id="post-54134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you visited <a href="https://www.bluetooth.com/specifications/adopted-specifications">https://www.bluetooth.com/specifications/adopted-specifications</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '16, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54134" class="comments-container"></div><div id="comment-tools-54134" class="comment-tools"></div><div class="clear"></div><div id="comment-54134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

