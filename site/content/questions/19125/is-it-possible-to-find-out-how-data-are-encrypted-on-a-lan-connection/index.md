+++
type = "question"
title = "[closed] Is it possible to find out how data are encrypted on a LAN connection?"
description = '''Hello everybody, I would like to know if it is possible to understand the data sent on a package through a LAN.  Let me explain you my problem, I have two devices connected thanks to a local network (10baseT) and using the IP/TCP protocol. As advised on the Internet, I captured package by using Wire...'''
date = "2013-03-04T07:35:00Z"
lastmod = "2013-03-04T07:35:00Z"
weight = 19125
keywords = [ "decode", "ip", "data", "tcp", "wireshark" ]
aliases = [ "/questions/19125" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Is it possible to find out how data are encrypted on a LAN connection?](/questions/19125/is-it-possible-to-find-out-how-data-are-encrypted-on-a-lan-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19125-score" class="post-score" title="current number of votes">0</div><span id="post-19125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everybody,</p><p>I would like to know if it is possible to understand the data sent on a package through a LAN.</p><p>Let me explain you my problem, I have two devices connected thanks to a local network (10baseT) and using the IP/TCP protocol. As advised on the Internet, I captured package by using Wireshark and laptop that I connected to the hub located between the two devices.</p><p>Indeed, I was able to follow the TCP stream related to the LAN connection. However, the data on the stream are not understandable as they are.</p><p>So, I think that the data must be somehow compressed or encrypted before being sent. But, how figure it out?</p><p>If anyone has an idea, I'll gladly read it!</p><p>Thanks and sorry for my awkward English ;p</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '13, 07:35</strong></p><img src="https://secure.gravatar.com/avatar/701acbf9d650132b9b7d751cadd5bbee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="arius57&#39;s gravatar image" /><p><span>arius57</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="arius57 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>04 Mar '13, 07:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-19125" class="comments-container"></div><div id="comment-tools-19125" class="comment-tools"></div><div class="clear"></div><div id="comment-19125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 04 Mar '13, 07:57

</div>

</div>

</div>

