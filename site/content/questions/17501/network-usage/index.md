+++
type = "question"
title = "Network Usage"
description = '''Hi, I may want to use this software, but I have a quick question I&#x27;d like to ask before I think about using it. When Installed will the scanning of this software slow down traffic? Due to our location, our bandwidth is rather limited. so we can&#x27;t really risk a hefty chunk out of users bandwidth. So ...'''
date = "2013-01-07T08:59:00Z"
lastmod = "2013-01-07T10:22:00Z"
weight = 17501
keywords = [ "limited", "bandwidth", "traffic", "location", "software" ]
aliases = [ "/questions/17501" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Network Usage](/questions/17501/network-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17501-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17501-score" class="post-score" title="current number of votes">1</div><span id="post-17501-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I may want to use this software, but I have a quick question I'd like to ask before I think about using it. When Installed will the scanning of this software slow down traffic? Due to our location, our bandwidth is rather limited. so we can't really risk a hefty chunk out of users bandwidth.</p><p>So that is basically my question. Will users be affected by this software scanning the packets they're sending/recieving</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-limited" rel="tag" title="see questions tagged &#39;limited&#39;">limited</span> <span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-location" rel="tag" title="see questions tagged &#39;location&#39;">location</span> <span class="post-tag tag-link-software" rel="tag" title="see questions tagged &#39;software&#39;">software</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '13, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/243f3d9adba0dddb04a83f234391e895?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aluzionz&#39;s gravatar image" /><p><span>Aluzionz</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aluzionz has no accepted answers">0%</span></p></div></div><div id="comments-container-17501" class="comments-container"></div><div id="comment-tools-17501" class="comment-tools"></div><div class="clear"></div><div id="comment-17501-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="17509"></span>

<div id="answer-container-17509" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17509-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17509-score" class="post-score" title="current number of votes">1</div><span id="post-17509-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Aluzionz has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Will users be affected by this software scanning the packets they're sending/recieving</p></blockquote><p>Wireshark is a passive network traffic analysis tool, so it will not slow down traffic. It <strong>will</strong> however need some resources on the system where you capture the traffic (CPU, RAM, disk).</p><p>Please read the <a href="https://www.wireshark.org/docs/">documentation</a> and the <a href="http://wiki.wireshark.org/">wiki</a> for more information about wireshark.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jan '13, 10:22</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-17509" class="comments-container"></div><div id="comment-tools-17509" class="comment-tools"></div><div class="clear"></div><div id="comment-17509-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="17508"></span>

<div id="answer-container-17508" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17508-score" class="post-score" title="current number of votes">2</div><span id="post-17508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you have network name resolution turned on, then Wireshark will do a DNS reverse lookup for each new IP address it sees (unless it can find the address in a hosts file). If you have network name resolution turned off, then Wireshark will not generate any traffic. It will be completely passive; it will not use any bandwidth.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jan '13, 10:20</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jan '13, 12:28</strong> </span></p></div></div><div id="comments-container-17508" class="comments-container"></div><div id="comment-tools-17508" class="comment-tools"></div><div class="clear"></div><div id="comment-17508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

