+++
type = "question"
title = "How to view protocol statistics"
description = '''Wireshark has the ability to decode various TCP protocols such as HTTP, HTTP2, SSL. Is it possible to view the overall bandwidth usage per protocol (in real-time or not) ? And for http is it possible to view the bandwidth usage per domain name ?'''
date = "2015-12-31T04:09:00Z"
lastmod = "2015-12-31T09:05:00Z"
weight = 48766
keywords = [ "statistics" ]
aliases = [ "/questions/48766" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to view protocol statistics](/questions/48766/how-to-view-protocol-statistics)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48766-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48766-score" class="post-score" title="current number of votes">0</div><span id="post-48766-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark has the ability to decode various TCP protocols such as HTTP, HTTP2, SSL. Is it possible to view the overall bandwidth usage per protocol (in real-time or not) ? And for http is it possible to view the bandwidth usage per domain name ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '15, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/a91629719b4b8493e56c2489b3a255a7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wavushujaa&#39;s gravatar image" /><p><span>wavushujaa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wavushujaa has no accepted answers">0%</span></p></div></div><div id="comments-container-48766" class="comments-container"></div><div id="comment-tools-48766" class="comment-tools"></div><div class="clear"></div><div id="comment-48766-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48775"></span>

<div id="answer-container-48775" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48775-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48775-score" class="post-score" title="current number of votes">0</div><span id="post-48775-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is a Protocol Hierarchy statistics dialog in the Statistics menu. This dialog isn't updated in real-time though, it's a "snapshot" of the current capture file at the time the dialog is initially displayed.</p><p>You could apply a display filter to limit the display, and the dialog, to a particular domain, but that's a manual process for each domain.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Dec '15, 09:05</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-48775" class="comments-container"></div><div id="comment-tools-48775" class="comment-tools"></div><div class="clear"></div><div id="comment-48775-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

