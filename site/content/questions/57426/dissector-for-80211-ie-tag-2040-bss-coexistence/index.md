+++
type = "question"
title = "Dissector for 802.11 IE Tag (20/40 BSS Coexistence)"
description = '''Hello, this dissector apparently is not yet implemented. Is there any chance that it will soon be? You can find it here:  IEEE 802.11 wireless LAN management frame  Fixed parameters (2 bytes)  Tagged parameters (3980 bytes)  [...]  Tag: 20/40 BSS Coexistence: Undecoded  Tag Number: 20/40 BSS Coexist...'''
date = "2016-11-16T14:25:00Z"
lastmod = "2016-11-17T00:52:00Z"
weight = 57426
keywords = [ "implementation", "dissector", "802.11" ]
aliases = [ "/questions/57426" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Dissector for 802.11 IE Tag (20/40 BSS Coexistence)](/questions/57426/dissector-for-80211-ie-tag-2040-bss-coexistence)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57426-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57426-score" class="post-score" title="current number of votes">0</div><span id="post-57426-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,<br />
this dissector apparently is not yet implemented. Is there any chance that it will soon be? You can find it here:</p><p><code> IEEE 802.11 wireless LAN management frame     Fixed parameters (2 bytes)     Tagged parameters (3980 bytes)         [...]         Tag: 20/40 BSS Coexistence: Undecoded             Tag Number: 20/40 BSS Coexistence (72)</code></p><p>Who could help me please?<br />
Thanks a lot!</p><pre><code>      Andrea</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-implementation" rel="tag" title="see questions tagged &#39;implementation&#39;">implementation</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '16, 14:25</strong></p><img src="https://secure.gravatar.com/avatar/d9ffd1db149bcd21b6a1662fb10ec728?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scozzy&#39;s gravatar image" /><p><span>scozzy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scozzy has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-57426" class="comments-container"></div><div id="comment-tools-57426" class="comment-tools"></div><div class="clear"></div><div id="comment-57426-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57432"></span>

<div id="answer-container-57432" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57432-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57432-score" class="post-score" title="current number of votes">2</div><span id="post-57432-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a> and raise an enhancements request and attach a pcap trace with a message containing the IE and some one might take interest in implementing it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Nov '16, 00:52</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-57432" class="comments-container"></div><div id="comment-tools-57432" class="comment-tools"></div><div class="clear"></div><div id="comment-57432-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

