+++
type = "question"
title = "[closed] Batch file to capture automatically on startup"
description = '''How to write a batch file that will help me to start capturing packets automatically and save it in a file on Windows startup?'''
date = "2013-11-13T21:46:00Z"
lastmod = "2013-11-14T00:47:00Z"
weight = 26980
keywords = [ "packet-capture", "startup" ]
aliases = [ "/questions/26980" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Batch file to capture automatically on startup](/questions/26980/batch-file-to-capture-automatically-on-startup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26980-score" class="post-score" title="current number of votes">0</div><span id="post-26980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to write a batch file that will help me to start capturing packets automatically and save it in a file on Windows startup?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '13, 21:46</strong></p><img src="https://secure.gravatar.com/avatar/b3bc2dd686651b1ec4a64787f1dd173b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bala92n&#39;s gravatar image" /><p><span>bala92n</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bala92n has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>14 Nov '13, 00:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-26980" class="comments-container"><span id="26985"></span><div id="comment-26985" class="comment"><div id="post-26985-score" class="comment-score"></div><div class="comment-text"><p>Already asked here: <a href="http://ask.wireshark.org/questions/26932/capture-packets-on-startup-automatically,">http://ask.wireshark.org/questions/26932/capture-packets-on-startup-automatically,</a> and some answers were provided. Please post a comment there if you need more help, not another question.</p></div><div id="comment-26985-info" class="comment-info"><span class="comment-age">(14 Nov '13, 00:47)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-26980" class="comment-tools"></div><div class="clear"></div><div id="comment-26980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 14 Nov '13, 00:47

</div>

</div>

</div>

