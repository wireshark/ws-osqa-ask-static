+++
type = "question"
title = "wireless set up to view own traffic"
description = '''have a airpcap on windows vista laptop, trying to set up veiwing my own traffic... on preferences, have tried to set up wpa key...my password is superbowl....my ssid id is lucky labidos...could someone give me the exact directions...as in spell that out how that should be written...i can see the ssi...'''
date = "2011-11-03T17:09:00Z"
lastmod = "2011-11-08T23:03:00Z"
weight = 7226
keywords = [ "capture", "setup", "wpa", "key" ]
aliases = [ "/questions/7226" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireless set up to view own traffic](/questions/7226/wireless-set-up-to-view-own-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7226-score" class="post-score" title="current number of votes">0</div><span id="post-7226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>have a airpcap on windows vista laptop, trying to set up veiwing my own traffic... on preferences, have tried to set up wpa key...my password is superbowl....my ssid id is lucky labidos...could someone give me the exact directions...as in spell that out how that should be written...i can see the ssid on channel 2...not the only one on there...but seems like its ......correct...im trying to veiw emails, messenger...both yahoo....facebook...and genral website traffic......any help would be helpful....im totally new...thanks estaban</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-setup" rel="tag" title="see questions tagged &#39;setup&#39;">setup</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span> <span class="post-tag tag-link-key" rel="tag" title="see questions tagged &#39;key&#39;">key</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '11, 17:09</strong></p><img src="https://secure.gravatar.com/avatar/ff435640ff814e0ef73fa88084693a02?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="estaban3&#39;s gravatar image" /><p><span>estaban3</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="estaban3 has no accepted answers">0%</span></p></div></div><div id="comments-container-7226" class="comments-container"></div><div id="comment-tools-7226" class="comment-tools"></div><div class="clear"></div><div id="comment-7226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7318"></span>

<div id="answer-container-7318" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7318-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7318-score" class="post-score" title="current number of votes">0</div><span id="post-7318-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you checked <a href="http://wiki.wireshark.org/HowToDecrypt802.11">wiki</a>?</p><p>I could just add that for grabbing wireless traffic on Windows Vista you should switch your wifi into promiscous mode, which is possible only if you run wireshark as administrator.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 23:03</strong></p><img src="https://secure.gravatar.com/avatar/35d96b8e73e6deb4e332d076fd3269b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShomeaX&#39;s gravatar image" /><p><span>ShomeaX</span><br />
<span class="score" title="73 reputation points">73</span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShomeaX has no accepted answers">0%</span></p></div></div><div id="comments-container-7318" class="comments-container"></div><div id="comment-tools-7318" class="comment-tools"></div><div class="clear"></div><div id="comment-7318-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

