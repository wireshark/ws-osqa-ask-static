+++
type = "question"
title = "Wireshark1.8.5 filter:SELECTED doesn&#x27;t work."
description = '''wireshark 1.8.5.  The filter function. If I do these: Right click - &amp;gt; apply a filter - &amp;gt; selected udp.  It will come to:!(udp).'''
date = "2013-05-06T01:17:00Z"
lastmod = "2013-05-06T01:50:00Z"
weight = 20977
keywords = [ "selected" ]
aliases = [ "/questions/20977" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark1.8.5 filter:SELECTED doesn't work.](/questions/20977/wireshark185-filterselected-doesnt-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20977-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20977-score" class="post-score" title="current number of votes">0</div><span id="post-20977-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>wireshark 1.8.5. The filter function.</p><p>If I do these:</p><p>Right click - &gt; apply a filter - &gt; selected udp.</p><p>It will come to:!(udp).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-selected" rel="tag" title="see questions tagged &#39;selected&#39;">selected</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '13, 01:17</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-20977" class="comments-container"><span id="20978"></span><div id="comment-20978" class="comment"><div id="post-20978-score" class="comment-score"></div><div class="comment-text"><p>Where is the screen do you right-click? The packet list (and in which column) or the packet details (and which row)?</p><p>Did you now by accident rightclick and then selected "Apply as filter -&gt; Not selected"?</p></div><div id="comment-20978-info" class="comment-info"><span class="comment-age">(06 May '13, 01:25)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-20977" class="comment-tools"></div><div class="clear"></div><div id="comment-20977-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20979"></span>

<div id="answer-container-20979" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20979-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20979-score" class="post-score" title="current number of votes">1</div><span id="post-20979-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>It has been fixed in Wireshark 1.8.6. Please upgrade.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 May '13, 01:50</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-20979" class="comments-container"></div><div id="comment-tools-20979" class="comment-tools"></div><div class="clear"></div><div id="comment-20979-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

