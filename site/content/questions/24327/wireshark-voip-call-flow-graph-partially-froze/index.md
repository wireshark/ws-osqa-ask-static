+++
type = "question"
title = "Wireshark  VOIP Call Flow Graph partially Froze!"
description = '''Hi all; I am using Version 1.10.1 Wireshark and just upgraded my graphics card.However still facing the same problem which is; when I open VOIP call flow graph as I scroll down the flow just the upper part scrolls and the bottom part frozes , so I can&#x27;t see the correct flow.I search but couldn&#x27;t fin...'''
date = "2013-09-03T23:05:00Z"
lastmod = "2015-06-29T04:49:00Z"
weight = 24327
keywords = [ "graph", "flow", "voip" ]
aliases = [ "/questions/24327" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark VOIP Call Flow Graph partially Froze!](/questions/24327/wireshark-voip-call-flow-graph-partially-froze)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24327-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24327-score" class="post-score" title="current number of votes">1</div><span id="post-24327-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all; I am using Version 1.10.1 Wireshark and just upgraded my graphics card.However still facing the same problem which is; when I open VOIP call flow graph as I scroll down the flow just the upper part scrolls and the bottom part frozes , so I can't see the correct flow.I search but couldn't find a smillar problem ,do somebody have any idea?Any idea will be much appreciated.</p><p>Regards Can</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '13, 23:05</strong></p><img src="https://secure.gravatar.com/avatar/ec4707eee73bebc08229e62fda223bad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Can%20Balc%C4%B1&#39;s gravatar image" /><p><span>Can Balcı</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Can Balcı has no accepted answers">0%</span></p></div></div><div id="comments-container-24327" class="comments-container"><span id="24486"></span><div id="comment-24486" class="comment"><div id="post-24486-score" class="comment-score"></div><div class="comment-text"><p>What is you OS (brand/type, version)?</p></div><div id="comment-24486-info" class="comment-info"><span class="comment-age">(09 Sep '13, 09:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24327" class="comment-tools"></div><div class="clear"></div><div id="comment-24327-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="43658"></span>

<div id="answer-container-43658" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43658-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43658-score" class="post-score" title="current number of votes">0</div><span id="post-43658-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi</p><p>I have the same problem. I managed to solve it by disabling "Use visual styles on windows and buttons" from "Advanced System settings" / windows.</p><p>It isn't the best solution but it is the only one i have until now Try it</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jun '15, 04:30</strong></p><img src="https://secure.gravatar.com/avatar/26c4b6089c682a8c1a4825664fd066d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Samsiigr&#39;s gravatar image" /><p><span>Samsiigr</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Samsiigr has no accepted answers">0%</span></p></div></div><div id="comments-container-43658" class="comments-container"></div><div id="comment-tools-43658" class="comment-tools"></div><div class="clear"></div><div id="comment-43658-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="43659"></span>

<div id="answer-container-43659" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43659-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43659-score" class="post-score" title="current number of votes">0</div><span id="post-43659-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Forget the previous one</p><p>I discussed the issue with a colleague of mine and the solution is to change from the "Date and Time of Day" to "Time of Day". This is located in wireshark&gt; "View"&gt;"Time Display Format"<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jun '15, 04:49</strong></p><img src="https://secure.gravatar.com/avatar/26c4b6089c682a8c1a4825664fd066d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Samsiigr&#39;s gravatar image" /><p><span>Samsiigr</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Samsiigr has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-43659" class="comments-container"></div><div id="comment-tools-43659" class="comment-tools"></div><div class="clear"></div><div id="comment-43659-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

