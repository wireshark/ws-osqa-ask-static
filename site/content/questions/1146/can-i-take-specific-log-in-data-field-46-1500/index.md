+++
type = "question"
title = "Can I take specific log in data field (46-1500)?"
description = '''Hello, I want to take logs in data field, so I must define boundary. Forexample, Log starts between 46 bytes and 800 bytes. Is is possible and how?'''
date = "2010-11-29T00:32:00Z"
lastmod = "2010-11-29T00:32:00Z"
weight = 1146
keywords = [ "specific", "field", "46-1500", "data", "log" ]
aliases = [ "/questions/1146" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I take specific log in data field (46-1500)?](/questions/1146/can-i-take-specific-log-in-data-field-46-1500)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1146-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1146-score" class="post-score" title="current number of votes">0</div><span id="post-1146-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I want to take logs in data field, so I must define boundary. Forexample, Log starts between 46 bytes and 800 bytes. Is is possible and how?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-specific" rel="tag" title="see questions tagged &#39;specific&#39;">specific</span> <span class="post-tag tag-link-field" rel="tag" title="see questions tagged &#39;field&#39;">field</span> <span class="post-tag tag-link-46-1500" rel="tag" title="see questions tagged &#39;46-1500&#39;">46-1500</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-log" rel="tag" title="see questions tagged &#39;log&#39;">log</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Nov '10, 00:32</strong></p><img src="https://secure.gravatar.com/avatar/3855646b0132717d7c3e956aa5affd3b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Burcu%20Sara&#39;s gravatar image" /><p><span>Burcu Sara</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Burcu Sara has no accepted answers">0%</span></p></div></div><div id="comments-container-1146" class="comments-container"></div><div id="comment-tools-1146" class="comment-tools"></div><div class="clear"></div><div id="comment-1146-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

