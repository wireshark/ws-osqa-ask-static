+++
type = "question"
title = "kali linux?"
description = '''First id like to apologize if my question has already been answered or is simple.Im just beginning to play with Kali linux and its CMD, and i cant seam to find a good command to download wire shark or even the official download for kali linux... '''
date = "2017-10-06T06:49:00Z"
lastmod = "2017-10-06T06:54:00Z"
weight = 63709
keywords = [ "plshelp", "download-install", "kali-linux" ]
aliases = [ "/questions/63709" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [kali linux?](/questions/63709/kali-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63709-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63709-score" class="post-score" title="current number of votes">0</div><span id="post-63709-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>First id like to apologize if my question has already been answered or is simple.Im just beginning to play with Kali linux and its CMD, and i cant seam to find a good command to download wire shark or even the official download for kali linux...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plshelp" rel="tag" title="see questions tagged &#39;plshelp&#39;">plshelp</span> <span class="post-tag tag-link-download-install" rel="tag" title="see questions tagged &#39;download-install&#39;">download-install</span> <span class="post-tag tag-link-kali-linux" rel="tag" title="see questions tagged &#39;kali-linux&#39;">kali-linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '17, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/80a0c346292199a8d9f8e2a3af6c608d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kaos_Hellboy&#39;s gravatar image" /><p><span>Kaos_Hellboy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kaos_Hellboy has no accepted answers">0%</span></p></div></div><div id="comments-container-63709" class="comments-container"></div><div id="comment-tools-63709" class="comment-tools"></div><div class="clear"></div><div id="comment-63709-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63710"></span>

<div id="answer-container-63710" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63710-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63710-score" class="post-score" title="current number of votes">0</div><span id="post-63710-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark should already be installed in Kali, so you should be able to find it in the tools menu somewhere. It should also be possible to run Wireshark simply by entering "wireshark" in a terminal and press enter, if I remember correctly.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Oct '17, 06:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-63710" class="comments-container"></div><div id="comment-tools-63710" class="comment-tools"></div><div class="clear"></div><div id="comment-63710-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

