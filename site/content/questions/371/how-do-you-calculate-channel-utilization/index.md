+++
type = "question"
title = "How do you calculate Channel Utilization?"
description = '''How the Words is defined for “Channel Utilization” ？ And How do you calculate it？ Why it values range from 1-255?'''
date = "2010-09-30T00:37:00Z"
lastmod = "2010-09-30T11:25:00Z"
weight = 371
keywords = [ "calculate", "channel", "utilization" ]
aliases = [ "/questions/371" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do you calculate Channel Utilization?](/questions/371/how-do-you-calculate-channel-utilization)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-371-score" class="post-score" title="current number of votes">0</div><span id="post-371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How the Words is defined for “Channel Utilization” ？ And How do you calculate it？ Why it values range from 1-255?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-calculate" rel="tag" title="see questions tagged &#39;calculate&#39;">calculate</span> <span class="post-tag tag-link-channel" rel="tag" title="see questions tagged &#39;channel&#39;">channel</span> <span class="post-tag tag-link-utilization" rel="tag" title="see questions tagged &#39;utilization&#39;">utilization</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '10, 00:37</strong></p><img src="https://secure.gravatar.com/avatar/ef079676baaf399e39d0d595f91e52f6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wildangel817&#39;s gravatar image" /><p><span>wildangel817</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wildangel817 has no accepted answers">0%</span></p></div></div><div id="comments-container-371" class="comments-container"></div><div id="comment-tools-371" class="comment-tools"></div><div class="clear"></div><div id="comment-371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="377"></span>

<div id="answer-container-377" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-377-score" class="post-score" title="current number of votes">0</div><span id="post-377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Assuming you're talking about 802.11 QBSS, the fields (wlan_mgt.qbss.cu and wlan_mgt.qbss2.cu) are unsigned 8-bit integers pulled directly from the 802.11 header. No calculation is performed on them.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Sep '10, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-377" class="comments-container"></div><div id="comment-tools-377" class="comment-tools"></div><div class="clear"></div><div id="comment-377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

