+++
type = "question"
title = "Wire shark does not Open!"
description = '''I have been trying to open wireshark but after just a few seconds it just closes, what do I need to do? (I have X11 btw so thats not a problem)'''
date = "2015-08-13T22:50:00Z"
lastmod = "2015-08-14T15:11:00Z"
weight = 45089
keywords = [ "osx", "macosx", "wireshark" ]
aliases = [ "/questions/45089" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wire shark does not Open!](/questions/45089/wire-shark-does-not-open)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45089-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45089-score" class="post-score" title="current number of votes">0</div><span id="post-45089-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have been trying to open wireshark but after just a few seconds it just closes, what do I need to do? (I have X11 btw so thats not a problem)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '15, 22:50</strong></p><img src="https://secure.gravatar.com/avatar/811dcd1eb48b397d9b48144d4a5a9054?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="angellohalo&#39;s gravatar image" /><p><span>angellohalo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="angellohalo has no accepted answers">0%</span></p></div></div><div id="comments-container-45089" class="comments-container"><span id="45098"></span><div id="comment-45098" class="comment"><div id="post-45098-score" class="comment-score"></div><div class="comment-text"><p>What is your OS and which version and what is your Wireshark version?</p></div><div id="comment-45098-info" class="comment-info"><span class="comment-age">(14 Aug '15, 01:31)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="45123"></span><div id="comment-45123" class="comment"><div id="post-45123-score" class="comment-score"></div><div class="comment-text"><p>The OS is presumably OS X, from the tags on the question, but no version was given.</p></div><div id="comment-45123-info" class="comment-info"><span class="comment-age">(14 Aug '15, 15:11)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-45089" class="comment-tools"></div><div class="clear"></div><div id="comment-45089-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

