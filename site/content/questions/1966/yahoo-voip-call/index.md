+++
type = "question"
title = "yahoo voip call"
description = '''hi,  I am capturing the yahoo voip call through the wireshark .and when I am playing the call it is playing slowly'''
date = "2011-01-27T02:25:00Z"
lastmod = "2011-01-27T02:25:00Z"
weight = 1966
keywords = [ "voip", "yahoo", "calls" ]
aliases = [ "/questions/1966" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [yahoo voip call](/questions/1966/yahoo-voip-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1966-score" class="post-score" title="current number of votes">0</div><span id="post-1966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, I am capturing the yahoo voip call through the wireshark .and when I am playing the call it is playing slowly</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-yahoo" rel="tag" title="see questions tagged &#39;yahoo&#39;">yahoo</span> <span class="post-tag tag-link-calls" rel="tag" title="see questions tagged &#39;calls&#39;">calls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '11, 02:25</strong></p><img src="https://secure.gravatar.com/avatar/8dfc43d9c0e652e6dcd1e405c9b54ca6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rahul&#39;s gravatar image" /><p><span>rahul</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rahul has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jan '11, 02:25</strong> </span></p></div></div><div id="comments-container-1966" class="comments-container"></div><div id="comment-tools-1966" class="comment-tools"></div><div class="clear"></div><div id="comment-1966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

