+++
type = "question"
title = "add own dictionary of VSA"
description = '''Hello, I want to add dictionary of RADIUS protocol VSA, so that whenever RADIUS packet contains our AVP it shows properly. I have added in my local wireshark but i want it to available it as an in bult functionality. So, whoever install wireshark our dictionary is available to them. '''
date = "2012-06-04T06:09:00Z"
lastmod = "2012-06-05T04:07:00Z"
weight = 11614
keywords = [ "vsa", "avp", "attribute-value-pair", "dictionary" ]
aliases = [ "/questions/11614" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [add own dictionary of VSA](/questions/11614/add-own-dictionary-of-vsa)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11614-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11614-score" class="post-score" title="current number of votes">0</div><span id="post-11614-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><pre><code>I want to add dictionary of RADIUS protocol VSA, so that whenever RADIUS packet contains our AVP it shows properly. I have added in my local wireshark but i want it to available it as an in bult functionality. So, whoever install wireshark our dictionary is available to them.</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vsa" rel="tag" title="see questions tagged &#39;vsa&#39;">vsa</span> <span class="post-tag tag-link-avp" rel="tag" title="see questions tagged &#39;avp&#39;">avp</span> <span class="post-tag tag-link-attribute-value-pair" rel="tag" title="see questions tagged &#39;attribute-value-pair&#39;">attribute-value-pair</span> <span class="post-tag tag-link-dictionary" rel="tag" title="see questions tagged &#39;dictionary&#39;">dictionary</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '12, 06:09</strong></p><img src="https://secure.gravatar.com/avatar/c80def65c0bee6e29d0c6bb71ed38734?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kartik%20jajal&#39;s gravatar image" /><p><span>kartik jajal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kartik jajal has no accepted answers">0%</span></p></div></div><div id="comments-container-11614" class="comments-container"></div><div id="comment-tools-11614" class="comment-tools"></div><div class="clear"></div><div id="comment-11614-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11658"></span>

<div id="answer-container-11658" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11658-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11658-score" class="post-score" title="current number of votes">0</div><span id="post-11658-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you developed stuff for Wireshark which you want to share with the community, then:</p><ol><li>Make sure it's licensed properly (GPL compatible)</li><li><a href="https://bugs.wireshark.org">File an enhancement bug</a> with your changes attached, and a capture file to test with.</li><li>?</li><li>Profit</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jun '12, 04:07</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-11658" class="comments-container"></div><div id="comment-tools-11658" class="comment-tools"></div><div class="clear"></div><div id="comment-11658-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

