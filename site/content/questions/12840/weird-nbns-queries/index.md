+++
type = "question"
title = "Weird NBNS queries"
description = '''I see plenty of &quot;normal&quot; NBMS queries, in that the host names are ones I recognize. These ones...notsomuch. Any ideas? Thx ==== 613 352.118450000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB AAPFPTPQUG&amp;lt;00&amp;gt; 614 352.118462000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB JSHNNMK...'''
date = "2012-07-18T21:51:00Z"
lastmod = "2012-07-19T10:35:00Z"
weight = 12840
keywords = [ "nbns", "netbios" ]
aliases = [ "/questions/12840" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Weird NBNS queries](/questions/12840/weird-nbns-queries)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12840-score" class="post-score" title="current number of votes">0</div><span id="post-12840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I see plenty of "normal" NBMS queries, in that the host names are ones I recognize. These ones...notsomuch.</p><p>Any ideas?</p><p>Thx</p><p>====</p><p>613 352.118450000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB AAPFPTPQUG&lt;00&gt;</p><p>614 352.118462000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB JSHNNMKQEJ&lt;00&gt;</p><p>615 352.118741000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB SIXXNSTOSD&lt;00&gt;</p><p>618 352.882326000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB SIXXNSTOSD&lt;00&gt;</p><p>619 352.882515000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB JSHNNMKQEJ&lt;00&gt;</p><p>620 352.882625000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB AAPFPTPQUG&lt;00&gt;</p><p>621 353.646711000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB AAPFPTPQUG&lt;00&gt;</p><p>622 353.646889000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB JSHNNMKQEJ&lt;00&gt;</p><p>623 353.647000000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB SIXXNSTOSD&lt;00&gt;</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nbns" rel="tag" title="see questions tagged &#39;nbns&#39;">nbns</span> <span class="post-tag tag-link-netbios" rel="tag" title="see questions tagged &#39;netbios&#39;">netbios</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '12, 21:51</strong></p><img src="https://secure.gravatar.com/avatar/3307de485d1c6d2d59a65f4183511a9b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sharkysometimes&#39;s gravatar image" /><p><span>sharkysometimes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sharkysometimes has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jul '12, 03:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-12840" class="comments-container"></div><div id="comment-tools-12840" class="comment-tools"></div><div class="clear"></div><div id="comment-12840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12841"></span>

<div id="answer-container-12841" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12841-score" class="post-score" title="current number of votes">1</div><span id="post-12841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>this is most certainly <strong>Google Chrome</strong>. It's a feature called DNS prefetching.</p><blockquote><p><code>https://isc.sans.edu/diary.html?storyid=10312</code><br />
<code>https://sites.google.com/a/chromium.org/dev/developers/design-documents/dns-prefetching</code><br />
</p></blockquote><p>See also an old question: <a href="http://ask.wireshark.org/questions/3697/odd-dns-queries-malware">http://ask.wireshark.org/questions/3697/odd-dns-queries-malware</a></p><p><strong>TEST</strong>: I just checked again. The latest Chrome release still does that during startup.</p><p>Client: Windows XP SP2 with "Netbios over TCP" <strong>enabled</strong>.</p><blockquote><p><code>http://cloudshark.org/captures/77c5cb6a3453</code><br />
</p></blockquote><p>Client: Windows XP SP2 with "Netbios over TCP" <strong>disabled</strong> -&gt; no NBNS queries.</p><blockquote><p><code>http://cloudshark.org/captures/0c48d0b6ad01</code></p></blockquote><p><strong>UPDATE</strong>: Could be malware as well. Please check Chrome first. If there is no Chrome on the computer (source IP), I suggest to scan that machine for malware.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '12, 23:58</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jul '12, 03:30</strong> </span></p></div></div><div id="comments-container-12841" class="comments-container"><span id="12842"></span><div id="comment-12842" class="comment"><div id="post-12842-score" class="comment-score"></div><div class="comment-text"><p>They're seeing NBNS queries, not DNS queries. Perhaps the Windows name resolver is turning whatever API Chrome is using for prefetching into NBNS queries, but this might be something else.</p></div><div id="comment-12842-info" class="comment-info"><span class="comment-age">(19 Jul '12, 00:40)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="12850"></span><div id="comment-12850" class="comment"><div id="post-12850-score" class="comment-score"></div><div class="comment-text"><p>Chrome just asks the local resolver and it depends on the resolver in what order it tries to resolve the names. That's configureable on windows. If the DNS server does not answer, it may try NBNS (depends on config).</p><p>Yes, it might be something else, hence "most certainly" ;-))</p></div><div id="comment-12850-info" class="comment-info"><span class="comment-age">(19 Jul '12, 03:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="12853"></span><div id="comment-12853" class="comment"><div id="post-12853-score" class="comment-score"></div><div class="comment-text"><p>I have Chrome installed, and tt does appear related to Chrome, as I see it consistently every time Chrome starts up. There is an option in Chrome to turn off pre-fetching of DNS, and this does cut down the "non-garbage" NBMS queries, but the weird-looking ones remain. So for now, I'm considering this mystery solved. Thanks for the help!!</p></div><div id="comment-12853-info" class="comment-info"><span class="comment-age">(19 Jul '12, 05:03)</span> <span class="comment-user userinfo">sharkysometimes</span></div></div><span id="12856"></span><div id="comment-12856" class="comment"><div id="post-12856-score" class="comment-score"></div><div class="comment-text"><blockquote><p>but the weird-looking ones remain.</p></blockquote><p>what do you mean by "the weird ones"?</p></div><div id="comment-12856-info" class="comment-info"><span class="comment-age">(19 Jul '12, 06:01)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="12864"></span><div id="comment-12864" class="comment"><div id="post-12864-score" class="comment-score"></div><div class="comment-text"><p>"Normal" ones look like this:</p><p>613 352.118450000 xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx NBNS 92 Name query NB <a href="http://somesite.com">somesite.com</a>&lt;xx&gt;</p><p>Weird ones look like the ones in my original post.</p><p>EDIT: "SIXXNSTOSD" = weird. :)</p></div><div id="comment-12864-info" class="comment-info"><span class="comment-age">(19 Jul '12, 10:29)</span> <span class="comment-user userinfo">sharkysometimes</span></div></div><span id="12866"></span><div id="comment-12866" class="comment not_top_scorer"><div id="post-12866-score" class="comment-score"></div><div class="comment-text"><p>ah, o.k. The weird ones are those generated by Chrome! See my samples at cloudshark.</p></div><div id="comment-12866-info" class="comment-info"><span class="comment-age">(19 Jul '12, 10:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12841" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-12841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

