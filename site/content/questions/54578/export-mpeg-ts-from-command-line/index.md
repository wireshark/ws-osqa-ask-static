+++
type = "question"
title = "Export Mpeg TS from command line?"
description = '''I just started using the Dump Mpeg TS Packets Lua tool which seems to work great for me. Just curious if there is anything that will do the same thing via a command line so I can possibly script multiple jobs in a batch file, or something like that. For those not familiar with the addon, it goes int...'''
date = "2016-08-04T05:03:00Z"
lastmod = "2016-08-04T05:03:00Z"
weight = 54578
keywords = [ "lua", "dump", "mpegts" ]
aliases = [ "/questions/54578" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Export Mpeg TS from command line?](/questions/54578/export-mpeg-ts-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54578-score" class="post-score" title="current number of votes">0</div><span id="post-54578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just started using the <a href="https://wiki.wireshark.org/mpeg_dump.lua">Dump Mpeg TS Packets</a> Lua tool which seems to work great for me. Just curious if there is anything that will do the same thing via a command line so I can possibly script multiple jobs in a batch file, or something like that.</p><p>For those not familiar with the addon, it goes into the pcap file and extracts the captured mpeg ts to a file. Works great for me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dump" rel="tag" title="see questions tagged &#39;dump&#39;">dump</span> <span class="post-tag tag-link-mpegts" rel="tag" title="see questions tagged &#39;mpegts&#39;">mpegts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Aug '16, 05:03</strong></p><img src="https://secure.gravatar.com/avatar/7c5474f71399d22bfbee5a0e86550fc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rchapoteau&#39;s gravatar image" /><p><span>rchapoteau</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rchapoteau has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Aug '16, 05:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-54578" class="comments-container"></div><div id="comment-tools-54578" class="comment-tools"></div><div class="clear"></div><div id="comment-54578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

