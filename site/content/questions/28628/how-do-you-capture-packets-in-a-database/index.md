+++
type = "question"
title = "How do you capture packets in a database"
description = '''Hi! How do I capture packets whit wireshark and storege in a database. HELP ME PLEASE!'''
date = "2014-01-07T06:02:00Z"
lastmod = "2014-01-08T09:07:00Z"
weight = 28628
keywords = [ "wireshark", "database" ]
aliases = [ "/questions/28628" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How do you capture packets in a database](/questions/28628/how-do-you-capture-packets-in-a-database)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28628-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28628-score" class="post-score" title="current number of votes">0</div><span id="post-28628-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! How do I capture packets whit wireshark and storege in a database. HELP ME PLEASE!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-database" rel="tag" title="see questions tagged &#39;database&#39;">database</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '14, 06:02</strong></p><img src="https://secure.gravatar.com/avatar/b6765771138487459721b3a4a76408ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sebgang&#39;s gravatar image" /><p><span>sebgang</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sebgang has no accepted answers">0%</span></p></div></div><div id="comments-container-28628" class="comments-container"></div><div id="comment-tools-28628" class="comment-tools"></div><div class="clear"></div><div id="comment-28628-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="28629"></span>

<div id="answer-container-28629" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28629-score" class="post-score" title="current number of votes">1</div><span id="post-28629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark can't store packets into databases. If you want to do something like that you'll have to find a different solution or code something that reads the capture files and pushes the frames into a database.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jan '14, 06:15</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28629" class="comments-container"><span id="28630"></span><div id="comment-28630" class="comment"><div id="post-28630-score" class="comment-score">1</div><div class="comment-text"><p>Search (in 'search' box above) for 'database' in 'tags' for links to several other ask.wireshark.org questions/answers relating to this topic.</p></div><div id="comment-28630-info" class="comment-info"><span class="comment-age">(07 Jan '14, 06:30)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="28632"></span><div id="comment-28632" class="comment"><div id="post-28632-score" class="comment-score"></div><div class="comment-text"><p>Thank's Jasper! but I found a tools named C5 Sigma. It probably solve my problem.</p></div><div id="comment-28632-info" class="comment-info"><span class="comment-age">(07 Jan '14, 07:09)</span> <span class="comment-user userinfo">sebgang</span></div></div></div><div id="comment-tools-28629" class="comment-tools"></div><div class="clear"></div><div id="comment-28629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="28674"></span>

<div id="answer-container-28674" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28674-score" class="post-score" title="current number of votes">0</div><span id="post-28674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer to a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/18601/sql-database-store">http://ask.wireshark.org/questions/18601/sql-database-store</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '14, 09:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28674" class="comments-container"></div><div id="comment-tools-28674" class="comment-tools"></div><div class="clear"></div><div id="comment-28674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

