+++
type = "question"
title = "Need Help to understand something wrong here."
description = '''Need Help to understand something wrong here based on the captures. APP SERVER 10.13.0.247---------------&amp;gt; Client 10.18.16.147  |  |----------------------------------&amp;gt; Print Server 10.18.135.52'''
date = "2014-07-31T09:13:00Z"
lastmod = "2014-07-31T10:27:00Z"
weight = 35032
keywords = [ "wireshark" ]
aliases = [ "/questions/35032" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need Help to understand something wrong here.](/questions/35032/need-help-to-understand-something-wrong-here)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35032-score" class="post-score" title="current number of votes">0</div><span id="post-35032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Need Help to understand something wrong here based on the captures.</p><p>APP SERVER 10.13.0.247---------------&gt; Client 10.18.16.147 | |----------------------------------&gt; Print Server 10.18.135.52</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '14, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/7ca116f73c00b356aa76bdee5490a491?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Farhan%20J&#39;s gravatar image" /><p><span>Farhan J</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Farhan J has no accepted answers">0%</span></p></div></div><div id="comments-container-35032" class="comments-container"><span id="35033"></span><div id="comment-35033" class="comment"><div id="post-35033-score" class="comment-score"></div><div class="comment-text"><p>How to upload the captures?</p></div><div id="comment-35033-info" class="comment-info"><span class="comment-age">(31 Jul '14, 09:14)</span> <span class="comment-user userinfo">Farhan J</span></div></div><span id="35034"></span><div id="comment-35034" class="comment"><div id="post-35034-score" class="comment-score"></div><div class="comment-text"><p>Use a file sharing service or <a href="http://cloudshark.org">cloudshark</a>.</p></div><div id="comment-35034-info" class="comment-info"><span class="comment-age">(31 Jul '14, 10:27)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-35032" class="comment-tools"></div><div class="clear"></div><div id="comment-35032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

