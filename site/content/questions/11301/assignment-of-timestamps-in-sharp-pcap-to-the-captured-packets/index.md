+++
type = "question"
title = "[closed] assignment of timestamps in sharp-pcap to the captured packets"
description = '''hi i need a code for assigning timestamps(readble format) to the incoming packets.plz hep me in this regard i am using the library that is sharp-pcap in c#. '''
date = "2012-05-24T00:20:00Z"
lastmod = "2012-05-24T00:20:00Z"
weight = 11301
keywords = [ "timestamps" ]
aliases = [ "/questions/11301" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] assignment of timestamps in sharp-pcap to the captured packets](/questions/11301/assignment-of-timestamps-in-sharp-pcap-to-the-captured-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11301-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11301-score" class="post-score" title="current number of votes">0</div><span id="post-11301-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i need a code for assigning timestamps(readble format) to the incoming packets.plz hep me in this regard i am using the library that is sharp-pcap in c#.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamps" rel="tag" title="see questions tagged &#39;timestamps&#39;">timestamps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '12, 00:20</strong></p><img src="https://secure.gravatar.com/avatar/1e82491a2156b49c954173c8348d9704?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rocky_1025&#39;s gravatar image" /><p><span>rocky_1025</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rocky_1025 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 May '12, 00:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-11301" class="comments-container"></div><div id="comment-tools-11301" class="comment-tools"></div><div class="clear"></div><div id="comment-11301-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "I've closed this as this is the Ask Wireshark Q&A site. You'll get support for sharp-pcap at http://sourceforge.net/projects/sharppcap/support" by grahamb 24 May '12, 00:48

</div>

</div>

</div>

