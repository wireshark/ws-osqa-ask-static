+++
type = "question"
title = "Capturing Bluetooth on windows 7"
description = '''Hi,  How can i capture Bluetooth packets on Windowns 7/XP? Which hardware is required for capturing the packets? Regards, Manjunath'''
date = "2013-07-08T02:42:00Z"
lastmod = "2013-12-22T16:43:00Z"
weight = 22715
keywords = [ "windows", "bluetooth" ]
aliases = [ "/questions/22715" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing Bluetooth on windows 7](/questions/22715/capturing-bluetooth-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22715-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22715-score" class="post-score" title="current number of votes">0</div><span id="post-22715-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, How can i capture Bluetooth packets on Windowns 7/XP? Which hardware is required for capturing the packets?</p><p>Regards, Manjunath</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jul '13, 02:42</strong></p><img src="https://secure.gravatar.com/avatar/b34cf5c80189b0e7c4d5e819dfd31015?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ManjunathMN&#39;s gravatar image" /><p><span>ManjunathMN</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ManjunathMN has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jul '13, 07:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-22715" class="comments-container"></div><div id="comment-tools-22715" class="comment-tools"></div><div class="clear"></div><div id="comment-22715-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22810"></span>

<div id="answer-container-22810" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22810-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22810-score" class="post-score" title="current number of votes">0</div><span id="post-22810-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no Bluetooth support in WinPcap, which means you cannot capture Bluetooth on Windows 7 with Wireshark. There may be other tools for Windows, and I leave it up to you to google them ;-)</p><p>Anyway, you can do it on Linux with Wireshark. Please see the following links:</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/Bluetooth">http://wiki.wireshark.org/CaptureSetup/Bluetooth</a><br />
<a href="http://ask.wireshark.org/questions/15837/what-equipment-can-i-use-to-capture-bluetooth-packets">http://ask.wireshark.org/questions/15837/what-equipment-can-i-use-to-capture-bluetooth-packets</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jul '13, 07:35</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jul '13, 07:35</strong> </span></p></div></div><div id="comments-container-22810" class="comments-container"><span id="28331"></span><div id="comment-28331" class="comment"><div id="post-28331-score" class="comment-score"></div><div class="comment-text"><p>Can I use it on Linux running on a virtual machine for windows 7?</p></div><div id="comment-28331-info" class="comment-info"><span class="comment-age">(22 Dec '13, 16:43)</span> <span class="comment-user userinfo">Luiz Felipe</span></div></div></div><div id="comment-tools-22810" class="comment-tools"></div><div class="clear"></div><div id="comment-22810-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

