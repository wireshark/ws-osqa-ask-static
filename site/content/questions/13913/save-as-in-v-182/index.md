+++
type = "question"
title = "save as in v 1.8.2"
description = '''I am using wireshark v 1.8.2, and with do &quot;save as&quot;, in the windows I have not option: Packet Range. So, I can not select that packet I can save. But I installed v 1.6.10, and I can use PAcket Range with &quot;save as&quot;. Thank you'''
date = "2012-08-27T06:23:00Z"
lastmod = "2012-08-27T06:47:00Z"
weight = 13913
keywords = [ "saveas" ]
aliases = [ "/questions/13913" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [save as in v 1.8.2](/questions/13913/save-as-in-v-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13913-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13913-score" class="post-score" title="current number of votes">0</div><span id="post-13913-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using wireshark v 1.8.2, and with do "save as", in the windows I have not option: Packet Range. So, I can not select that packet I can save.</p><p>But I installed v 1.6.10, and I can use PAcket Range with "save as".</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-saveas" rel="tag" title="see questions tagged &#39;saveas&#39;">saveas</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '12, 06:23</strong></p><img src="https://secure.gravatar.com/avatar/c3fc2d30693effc34eba54bdcb7009f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Juan%20Carlos&#39;s gravatar image" /><p><span>Juan Carlos</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Juan Carlos has no accepted answers">0%</span></p></div></div><div id="comments-container-13913" class="comments-container"></div><div id="comment-tools-13913" class="comment-tools"></div><div class="clear"></div><div id="comment-13913-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13914"></span>

<div id="answer-container-13914" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13914-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13914-score" class="post-score" title="current number of votes">3</div><span id="post-13914-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Juan Carlos has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's moved and is now under the "File | Export Specified Packets..." menu item. See the 1.8.0 release notes (you did read them before asking the question :-) ) <a href="http://www.wireshark.org/docs/relnotes/wireshark-1.8.0.html#NewFeatures">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Aug '12, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Aug '12, 07:19</strong> </span></p></div></div><div id="comments-container-13914" class="comments-container"></div><div id="comment-tools-13914" class="comment-tools"></div><div class="clear"></div><div id="comment-13914-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

