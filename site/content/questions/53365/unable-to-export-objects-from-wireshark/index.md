+++
type = "question"
title = "Unable to Export objects from Wireshark"
description = '''I&#x27;m given a packet capture and It has a HTTP object while doing the Extract object it is showing a window with no objects.The same happens for all HTTP object,What should I do?'''
date = "2016-06-12T04:08:00Z"
lastmod = "2016-06-13T04:45:00Z"
weight = 53365
keywords = [ "wireshark" ]
aliases = [ "/questions/53365" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to Export objects from Wireshark](/questions/53365/unable-to-export-objects-from-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53365-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53365-score" class="post-score" title="current number of votes">0</div><span id="post-53365-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm given a packet capture and It has a HTTP object while doing the Extract object it is showing a window with no objects.The same happens for all HTTP object,What should I do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '16, 04:08</strong></p><img src="https://secure.gravatar.com/avatar/b5dd5622c8de7282d8effd2b2febcfd2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Am3n&#39;s gravatar image" /><p><span>Am3n</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Am3n has no accepted answers">0%</span></p></div></div><div id="comments-container-53365" class="comments-container"><span id="53367"></span><div id="comment-53367" class="comment"><div id="post-53367-score" class="comment-score"></div><div class="comment-text"><p>What is your OS and which version and what is your Wireshark version?</p></div><div id="comment-53367-info" class="comment-info"><span class="comment-age">(12 Jun '16, 05:45)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="53381"></span><div id="comment-53381" class="comment"><div id="post-53381-score" class="comment-score"></div><div class="comment-text"><p>ubuntu 16.04 wirshark version 2.0.2</p></div><div id="comment-53381-info" class="comment-info"><span class="comment-age">(12 Jun '16, 12:42)</span> <span class="comment-user userinfo">Am3n</span></div></div><span id="53396"></span><div id="comment-53396" class="comment"><div id="post-53396-score" class="comment-score"></div><div class="comment-text"><p>Qt or GTK+ ?</p></div><div id="comment-53396-info" class="comment-info"><span class="comment-age">(13 Jun '16, 04:45)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-53365" class="comment-tools"></div><div class="clear"></div><div id="comment-53365-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

