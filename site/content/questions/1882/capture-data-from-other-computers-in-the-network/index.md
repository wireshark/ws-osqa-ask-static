+++
type = "question"
title = "Capture data from other computers in the network?"
description = '''Hi! I have an Atheros adapter that supports monitor mode.  My network interface is wlan0. When I run the command:  airmon-ng start wlan0 a new network interface is beeing setup and it´s called mon0.  When I connect to my network, then it is the wlan0 interface that gets the IP from the router, but i...'''
date = "2011-01-22T13:55:00Z"
lastmod = "2011-01-22T13:55:00Z"
weight = 1882
keywords = [ "capture", "wlan0", "monitor", "mon0", "mode" ]
aliases = [ "/questions/1882" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture data from other computers in the network?](/questions/1882/capture-data-from-other-computers-in-the-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1882-score" class="post-score" title="current number of votes">0</div><span id="post-1882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! I have an Atheros adapter that supports monitor mode.</p><p>My network interface is wlan0. When I run the command:</p><h1 id="airmon-ng-start-wlan0">airmon-ng start wlan0</h1><p>a new network interface is beeing setup and it´s called mon0.</p><p>When I connect to my network, then it is the wlan0 interface that gets the IP from the router, but it seems that it is the mon0 interface that is in monitor mode (but it has not get any IP address).</p><p>What is required to capture data from other computers in my network?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wlan0" rel="tag" title="see questions tagged &#39;wlan0&#39;">wlan0</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-mon0" rel="tag" title="see questions tagged &#39;mon0&#39;">mon0</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '11, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/b40d415d5a5ed5142e38ad841b2e176a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rox&#39;s gravatar image" /><p><span>Rox</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rox has no accepted answers">0%</span></p></div></div><div id="comments-container-1882" class="comments-container"></div><div id="comment-tools-1882" class="comment-tools"></div><div class="clear"></div><div id="comment-1882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

