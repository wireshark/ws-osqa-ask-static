+++
type = "question"
title = "Wireshark Capture Filter to record messages only destined for a specific 802.15.4 PAN ID"
description = '''Can anyone help me figure out how to set a Capture Filter (from the Wireshark GUI and also from the command line) that will isolate and record only 802.15.4 messages bound for a specific PAN ID?  An example of a Wireshark Display Filter that works fine in the UI for this is &#x27;wpan.dst_pan == 0x3229&#x27;....'''
date = "2014-11-25T09:13:00Z"
lastmod = "2014-11-25T09:13:00Z"
weight = 38131
keywords = [ "802.15.4", "capture-filter" ]
aliases = [ "/questions/38131" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Capture Filter to record messages only destined for a specific 802.15.4 PAN ID](/questions/38131/wireshark-capture-filter-to-record-messages-only-destined-for-a-specific-802154-pan-id)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38131-score" class="post-score" title="current number of votes">0</div><span id="post-38131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone help me figure out how to set a Capture Filter (from the Wireshark GUI and also from the command line) that will isolate and record only 802.15.4 messages bound for a specific PAN ID?<br />
</p><p>An example of a Wireshark Display Filter that works fine in the UI for this is 'wpan.dst_pan == 0x3229'. Unfortunately it seems that Display Filters in effect don't apply to the capture log file. So I'd like a Capture Filter that has the same effect on the log file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.15.4" rel="tag" title="see questions tagged &#39;802.15.4&#39;">802.15.4</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '14, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/b2e61a0858e683f63217c4fa8d41772a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JKRoberts&#39;s gravatar image" /><p><span>JKRoberts</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JKRoberts has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-38131" class="comments-container"></div><div id="comment-tools-38131" class="comment-tools"></div><div class="clear"></div><div id="comment-38131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

