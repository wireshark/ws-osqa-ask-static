+++
type = "question"
title = "How do you input a WEP key for decryption in Wireshark preferences?"
description = '''I can bring up Wireshark preferences. I can select the Protocols and indicate that decryption should be used. I can click to edit the button for the decryption key. I can select WEP as the key type. But, I can&#x27;t get the key itself to input. I have searched the reference documentation and have not fo...'''
date = "2017-01-24T13:22:00Z"
lastmod = "2017-01-24T13:35:00Z"
weight = 59023
keywords = [ "preferences", "protocols", "dycryption" ]
aliases = [ "/questions/59023" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do you input a WEP key for decryption in Wireshark preferences?](/questions/59023/how-do-you-input-a-wep-key-for-decryption-in-wireshark-preferences)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59023-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59023-score" class="post-score" title="current number of votes">0</div><span id="post-59023-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can bring up Wireshark preferences. I can select the Protocols and indicate that decryption should be used. I can click to edit the button for the decryption key. I can select WEP as the key type. But, I can't get the key itself to input. I have searched the reference documentation and have not found this level of detail. What is the trick?</p><p>Any guidance is appreciated - bab</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span> <span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span> <span class="post-tag tag-link-dycryption" rel="tag" title="see questions tagged &#39;dycryption&#39;">dycryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '17, 13:22</strong></p><img src="https://secure.gravatar.com/avatar/1ef89e648fcccd05430de48fcb81c1c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bab&#39;s gravatar image" /><p><span>bab</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bab has no accepted answers">0%</span></p></div></div><div id="comments-container-59023" class="comments-container"></div><div id="comment-tools-59023" class="comment-tools"></div><div class="clear"></div><div id="comment-59023-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59026"></span>

<div id="answer-container-59026" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59026-score" class="post-score" title="current number of votes">0</div><span id="post-59026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I believe the Wireshark "<a href="https://wiki.wireshark.org/HowToDecrypt802.11">How to Decrypt 802.11</a>" wiki page should have everything you're looking for.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '17, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-59026" class="comments-container"></div><div id="comment-tools-59026" class="comment-tools"></div><div class="clear"></div><div id="comment-59026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

