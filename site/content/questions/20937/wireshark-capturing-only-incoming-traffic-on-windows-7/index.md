+++
type = "question"
title = "Wireshark capturing only incoming traffic on Windows 7"
description = '''Wireshark capturing only incoming traffic. For example, when i ping a site I only see the reply packets. Wireshark version is 1.8.6 running on a windows 7 x64 OS and WinCap is version 4.1.2. I uninstalled wireshark and also cleaned the registry and still having the same issue. Not really sure that i...'''
date = "2013-05-03T11:21:00Z"
lastmod = "2013-05-04T08:28:00Z"
weight = 20937
keywords = [ "traffic", "incoming" ]
aliases = [ "/questions/20937" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark capturing only incoming traffic on Windows 7](/questions/20937/wireshark-capturing-only-incoming-traffic-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20937-score" class="post-score" title="current number of votes">0</div><span id="post-20937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark capturing only incoming traffic. For example, when i ping a site I only see the reply packets. Wireshark version is 1.8.6 running on a windows 7 x64 OS and WinCap is version 4.1.2. I uninstalled wireshark and also cleaned the registry and still having the same issue. Not really sure that it's the NIC because I have both Microsoft Network Monitor 3.4 and OmniPeek and both capture incoming and outgoing traffic OK.</p><p>Any help would be greatly appreciated.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-incoming" rel="tag" title="see questions tagged &#39;incoming&#39;">incoming</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '13, 11:21</strong></p><img src="https://secure.gravatar.com/avatar/381a470d23e2e7bf84ee3cc2e93d1bde?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alexltk0506&#39;s gravatar image" /><p><span>alexltk0506</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alexltk0506 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 May '13, 17:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-20937" class="comments-container"></div><div id="comment-tools-20937" class="comment-tools"></div><div class="clear"></div><div id="comment-20937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20949"></span>

<div id="answer-container-20949" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20949-score" class="post-score" title="current number of votes">1</div><span id="post-20949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="alexltk0506 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>One cause of this can be <a href="http://www.winpcap.org/misc/faq.htm#Q-21">VPN software installed on your machine</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '13, 17:32</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-20949" class="comments-container"><span id="20951"></span><div id="comment-20951" class="comment"><div id="post-20951-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot. That did it. All is well and I see traffic flowing in and out. Thanks!</p></div><div id="comment-20951-info" class="comment-info"><span class="comment-age">(04 May '13, 08:28)</span> <span class="comment-user userinfo">alexltk0506</span></div></div></div><div id="comment-tools-20949" class="comment-tools"></div><div class="clear"></div><div id="comment-20949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

