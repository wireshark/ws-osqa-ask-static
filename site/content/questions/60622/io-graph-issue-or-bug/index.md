+++
type = "question"
title = "IO Graph Issue or Bug"
description = '''Has anyone else encountered this problem when using IO Graphs? There are times when I&#x27;m trying to change the parameters and the row will stay in &quot;edit&quot; mode view (see the &quot;Time&quot; row in image below). I can&#x27;t get it to fall back to the normal display to check/uncheck the box, etc. It happens with a si...'''
date = "2017-04-06T11:16:00Z"
lastmod = "2017-04-10T11:11:00Z"
weight = 60622
keywords = [ "graph", "window", "io" ]
aliases = [ "/questions/60622" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IO Graph Issue or Bug](/questions/60622/io-graph-issue-or-bug)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60622-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60622-score" class="post-score" title="current number of votes">0</div><span id="post-60622-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anyone else encountered this problem when using IO Graphs? There are times when I'm trying to change the parameters and the row will stay in "edit" mode view (see the "Time" row in image below). I can't get it to fall back to the normal display to check/uncheck the box, etc. It happens with a single row or multiple rows. If I add or remove rows, this row will stay in front of whatever is behind it. Resizing also doesn't help. So far, it seems to be sporadic. The only way to fix it is to close the IO graph and start over.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/io_graph_bug.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-window" rel="tag" title="see questions tagged &#39;window&#39;">window</span> <span class="post-tag tag-link-io" rel="tag" title="see questions tagged &#39;io&#39;">io</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '17, 11:16</strong></p><img src="https://secure.gravatar.com/avatar/37aeac42341cc42e5d10656094aa9139?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="csereno&#39;s gravatar image" /><p><span>csereno</span><br />
<span class="score" title="69 reputation points">69</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="csereno has one accepted answer">25%</span></p></img></div></div><div id="comments-container-60622" class="comments-container"><span id="60623"></span><div id="comment-60623" class="comment"><div id="post-60623-score" class="comment-score"></div><div class="comment-text"><p>version: Version 2.2.5 (v2.2.5-0-g440fd4d)</p></div><div id="comment-60623-info" class="comment-info"><span class="comment-age">(06 Apr '17, 11:17)</span> <span class="comment-user userinfo">csereno</span></div></div><span id="60644"></span><div id="comment-60644" class="comment"><div id="post-60644-score" class="comment-score">1</div><div class="comment-text"><p>Can confirm this problem. The same for me.</p></div><div id="comment-60644-info" class="comment-info"><span class="comment-age">(07 Apr '17, 04:33)</span> <span class="comment-user userinfo">Packet_vlad</span></div></div><span id="60709"></span><div id="comment-60709" class="comment"><div id="post-60709-score" class="comment-score"></div><div class="comment-text"><p>I submitted this as a bug: <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13585">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13585</a></p></div><div id="comment-60709-info" class="comment-info"><span class="comment-age">(10 Apr '17, 11:11)</span> <span class="comment-user userinfo">csereno</span></div></div></div><div id="comment-tools-60622" class="comment-tools"></div><div class="clear"></div><div id="comment-60622-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

