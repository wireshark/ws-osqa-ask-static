+++
type = "question"
title = "No NIC is shown"
description = '''Wireshark used to work perfectly on a computer, but it does show any Ethernet interface now. Here is the screenshot of Wireshark:  Here are the adapters on the Windows 10 computer:  Could anyone offer a tip on how to fix this? The following is from the help window: Version 2.2.4 (v2.2.4-0-gcc3dc1b) ...'''
date = "2017-02-20T07:41:00Z"
lastmod = "2017-02-20T11:06:00Z"
weight = 59555
keywords = [ "interface", "nic", "interfaces" ]
aliases = [ "/questions/59555" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [No NIC is shown](/questions/59555/no-nic-is-shown)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59555-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59555-score" class="post-score" title="current number of votes">0</div><span id="post-59555-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark used to work perfectly on a computer, but it does show any Ethernet interface now. Here is the screenshot of Wireshark: <img src="https://osqa-ask.wireshark.org/upfiles/wireshark_tmUaCnD.png" alt="alt text" /></p><p>Here are the adapters on the Windows 10 computer: <img src="https://osqa-ask.wireshark.org/upfiles/adapters.png" alt="alt text" /></p><p>Could anyone offer a tip on how to fix this?</p><p>The following is from the help window:</p><pre><code>Version 2.2.4 (v2.2.4-0-gcc3dc1b)

Copyright 1998-2017 Gerald Combs &lt;[email protected]&gt; and contributors.
License GPLv2+: GNU GPL version 2 or later &lt;http://www.gnu.org/licenses/old-licenses/gpl-2.0.html&gt;
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Compiled (64-bit) with Qt 5.6.1, with WinPcap (4_1_3), with GLib 2.42.0, with
zlib 1.2.8, with SMI 0.4.8, with c-ares 1.12.0, with Lua 5.2.4, with GnuTLS
3.2.15, with Gcrypt 1.6.2, with MIT Kerberos, with GeoIP, with QtMultimedia,
with AirPcap.

Running on 64-bit Windows 10, build 14393, with locale English_United
States.1252, with WinPcap version 4.1.3 (packet.dll version 4.1.0.2980), based
on libpcap version 1.0 branch 1_0_rel0b (20091008), with GnuTLS 3.2.15, with
Gcrypt 1.6.2, without AirPcap.
       Intel(R) Core(TM) i5-3570K CPU @ 3.40GHz (with SSE4.2), with 24519MB of
physical memory.

Built using Microsoft Visual C++ 12.0 build 40629

Wireshark is Open Source Software released under the GNU General Public License.

Check the man page and http://www.wireshark.org for more information.</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-nic" rel="tag" title="see questions tagged &#39;nic&#39;">nic</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '17, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/e23332dc51869f08737cc96395284e59?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hong&#39;s gravatar image" /><p><span>Hong</span><br />
<span class="score" title="46 reputation points">46</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hong has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Feb '17, 08:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></img></div></div><div id="comments-container-59555" class="comments-container"><span id="59557"></span><div id="comment-59557" class="comment"><div id="post-59557-score" class="comment-score">1</div><div class="comment-text"><p>Can you show the contents of the help dialog, Help -&gt; About Wireshark?</p><p>Highlight all the black text, Ctrl + C to copy it, click the "edit" button at the bottom of your question and then Ctrl + V to paste it into the bottom of your question.</p></div><div id="comment-59557-info" class="comment-info"><span class="comment-age">(20 Feb '17, 08:03)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="59559"></span><div id="comment-59559" class="comment"><div id="post-59559-score" class="comment-score"></div><div class="comment-text"><p>I have edited the question per your suggestion.</p></div><div id="comment-59559-info" class="comment-info"><span class="comment-age">(20 Feb '17, 08:19)</span> <span class="comment-user userinfo">Hong</span></div></div></div><div id="comment-tools-59555" class="comment-tools"></div><div class="clear"></div><div id="comment-59555-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59561"></span>

<div id="answer-container-59561" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59561-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59561-score" class="post-score" title="current number of votes">1</div><span id="post-59561-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Hong has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>OK, it seems you're using WinPcap for packet capture and for accessing the interfaces.</p><p>There have been a few previous questions about WinPcap issues with missing interfaces, e.g. <a href="https://ask.wireshark.org/questions/10008/no-interfaces-windows">here</a> and <a href="https://ask.wireshark.org/questions/53778/ethernet-interface-not-offered-when-starting-up-wireshark">here</a> without any real resolution being found. I suspect interfering software such as AV, firewalls and endpoint protection. Do you have any of that installed?</p><p>You could try the following:</p><ol><li>A reboot of the machine.</li><li>Uninstall WinPcap. Reboot. Download WinPcap from <a href="http://www.winpcap.org/install/">here</a> and install it. Reboot.</li><li>Uninstall WinPcap. Reboot. Download npcap from <a href="https://nmap.org/npcap/">here</a> and install it. You may need to reinstall Wireshark.</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Feb '17, 08:53</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-59561" class="comments-container"><span id="59566"></span><div id="comment-59566" class="comment"><div id="post-59566-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot for the tips, Graham. Rebooting the machine has fixed the problem. Wireshark sees all interfaces (4 in total). I have been using Wireshark for over 10 years, and this is my first time to encounter this. I do not know what caused it. Could you turn your comment into an answer?</p></div><div id="comment-59566-info" class="comment-info"><span class="comment-age">(20 Feb '17, 11:06)</span> <span class="comment-user userinfo">Hong</span></div></div></div><div id="comment-tools-59561" class="comment-tools"></div><div class="clear"></div><div id="comment-59561-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

