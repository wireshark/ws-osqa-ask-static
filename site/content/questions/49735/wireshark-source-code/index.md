+++
type = "question"
title = "wireshark source code"
description = '''Could I use wireshark source code in my project?'''
date = "2016-02-02T11:30:00Z"
lastmod = "2016-02-02T22:40:00Z"
weight = 49735
keywords = [ "sourse", "source-code" ]
aliases = [ "/questions/49735" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark source code](/questions/49735/wireshark-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49735-score" class="post-score" title="current number of votes">0</div><span id="post-49735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Could I use wireshark source code in my project?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sourse" rel="tag" title="see questions tagged &#39;sourse&#39;">sourse</span> <span class="post-tag tag-link-source-code" rel="tag" title="see questions tagged &#39;source-code&#39;">source-code</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '16, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/adc1911237d6852567c0afd864596053?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alwaei&#39;s gravatar image" /><p><span>alwaei</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alwaei has no accepted answers">0%</span></p></div></div><div id="comments-container-49735" class="comments-container"></div><div id="comment-tools-49735" class="comment-tools"></div><div class="clear"></div><div id="comment-49735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49741"></span>

<div id="answer-container-49741" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49741-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49741-score" class="post-score" title="current number of votes">0</div><span id="post-49741-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As long as you adhere to <a href="https://www.wireshark.org/faq.html#q1.9">the license conditions</a> you should be fine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Feb '16, 13:17</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-49741" class="comments-container"><span id="49742"></span><div id="comment-49742" class="comment"><div id="post-49742-score" class="comment-score"></div><div class="comment-text"><p>I want to use it in my graduate project not for commercial things</p></div><div id="comment-49742-info" class="comment-info"><span class="comment-age">(02 Feb '16, 13:33)</span> <span class="comment-user userinfo">alwaei</span></div></div><span id="49754"></span><div id="comment-49754" class="comment"><div id="post-49754-score" class="comment-score"></div><div class="comment-text"><p>Still you'll have to adhere to the GPLv2, this reference is just the most expressive. Open Source code reuse license considerations would be a good topic to address in your graduate project too.</p></div><div id="comment-49754-info" class="comment-info"><span class="comment-age">(02 Feb '16, 22:40)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-49741" class="comment-tools"></div><div class="clear"></div><div id="comment-49741-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

