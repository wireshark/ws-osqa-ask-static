+++
type = "question"
title = "PPI 802.11ac MAC+PHY header?"
description = '''Could you point me to any current effort to define a standard PPI 802.11ac MAC+PHY header? '''
date = "2013-05-10T09:38:00Z"
lastmod = "2013-05-10T11:30:00Z"
weight = 21087
keywords = [ "header", "ppi", "802.11ac" ]
aliases = [ "/questions/21087" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [PPI 802.11ac MAC+PHY header?](/questions/21087/ppi-80211ac-macphy-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21087-score" class="post-score" title="current number of votes">0</div><span id="post-21087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Could you point me to any current effort to define a standard PPI 802.11ac MAC+PHY header?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-ppi" rel="tag" title="see questions tagged &#39;ppi&#39;">ppi</span> <span class="post-tag tag-link-802.11ac" rel="tag" title="see questions tagged &#39;802.11ac&#39;">802.11ac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '13, 09:38</strong></p><img src="https://secure.gravatar.com/avatar/c08e2445f183115cec1d5fbf906695da?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="darylkaiser&#39;s gravatar image" /><p><span>darylkaiser</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="darylkaiser has no accepted answers">0%</span></p></div></div><div id="comments-container-21087" class="comments-container"></div><div id="comment-tools-21087" class="comment-tools"></div><div class="clear"></div><div id="comment-21087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21088"></span>

<div id="answer-container-21088" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21088-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21088-score" class="post-score" title="current number of votes">0</div><span id="post-21088-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, but I could point you to <a href="http://www.radiotap.org/defined-fields/VHT">a Radiotap field that has some 802.11ac properties</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '13, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-21088" class="comments-container"></div><div id="comment-tools-21088" class="comment-tools"></div><div class="clear"></div><div id="comment-21088-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

