+++
type = "question"
title = "Connectivity with ODBC"
description = '''Can wireshark connect to ODBC driver or not ? if yes, how?'''
date = "2011-03-12T22:11:00Z"
lastmod = "2011-03-13T07:58:00Z"
weight = 2791
keywords = [ "connectivity", "odbc" ]
aliases = [ "/questions/2791" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Connectivity with ODBC](/questions/2791/connectivity-with-odbc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2791-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2791-score" class="post-score" title="current number of votes">0</div><span id="post-2791-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can wireshark connect to ODBC driver or not ? if yes, how?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connectivity" rel="tag" title="see questions tagged &#39;connectivity&#39;">connectivity</span> <span class="post-tag tag-link-odbc" rel="tag" title="see questions tagged &#39;odbc&#39;">odbc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '11, 22:11</strong></p><img src="https://secure.gravatar.com/avatar/c3df439443e6c420f368ac68ad769781?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sakshi&#39;s gravatar image" /><p><span>sakshi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sakshi has no accepted answers">0%</span></p></div></div><div id="comments-container-2791" class="comments-container"></div><div id="comment-tools-2791" class="comment-tools"></div><div class="clear"></div><div id="comment-2791-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2793"></span>

<div id="answer-container-2793" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2793-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2793-score" class="post-score" title="current number of votes">0</div><span id="post-2793-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'd say that's a no, at least I know of no way to do that.</p><p>Why would you need ODBC connectivity?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Mar '11, 07:58</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2793" class="comments-container"></div><div id="comment-tools-2793" class="comment-tools"></div><div class="clear"></div><div id="comment-2793-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

