+++
type = "question"
title = "Ericsson R13 HLR decoding"
description = '''Hi, I am looking for a decoder for our HLR. I stumbled onto this page while Googling and thought I would ask and see if anyone has experience with that? Thanks Johan'''
date = "2011-09-13T07:48:00Z"
lastmod = "2012-11-16T04:56:00Z"
weight = 6319
keywords = [ "hlr", "r13" ]
aliases = [ "/questions/6319" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ericsson R13 HLR decoding](/questions/6319/ericsson-r13-hlr-decoding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6319-score" class="post-score" title="current number of votes">0</div><span id="post-6319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am looking for a decoder for our HLR.</p><p>I stumbled onto this page while Googling and thought I would ask and see if anyone has experience with that?</p><p>Thanks</p><p>Johan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hlr" rel="tag" title="see questions tagged &#39;hlr&#39;">hlr</span> <span class="post-tag tag-link-r13" rel="tag" title="see questions tagged &#39;r13&#39;">r13</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '11, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/efe5727cd4e256d7b7c7a5ebf06f9607?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Johan&#39;s gravatar image" /><p><span>Johan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Johan has no accepted answers">0%</span></p></div></div><div id="comments-container-6319" class="comments-container"><span id="8138"></span><div id="comment-8138" class="comment"><div id="post-8138-score" class="comment-score"></div><div class="comment-text"><p>are you still looking for a solution to decode HLR dump?</p></div><div id="comment-8138-info" class="comment-info"><span class="comment-age">(26 Dec '11, 23:58)</span> <span class="comment-user userinfo">BhushanKadu</span></div></div></div><div id="comment-tools-6319" class="comment-tools"></div><div class="clear"></div><div id="comment-6319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15961"></span>

<div id="answer-container-15961" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15961-score" class="post-score" title="current number of votes">0</div><span id="post-15961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Send a email to <span class="__cf_email__" data-cfemail="6105001708054f0a00030e0f060e21">[email protected]</span><a href="http://hparser.co.uk">hparser.co.uk</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '12, 04:56</strong></p><img src="https://secure.gravatar.com/avatar/6c598d5fae9d04c7f9cbefd414b8e2e2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="filsdollar&#39;s gravatar image" /><p><span>filsdollar</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="filsdollar has no accepted answers">0%</span></p></div></div><div id="comments-container-15961" class="comments-container"></div><div id="comment-tools-15961" class="comment-tools"></div><div class="clear"></div><div id="comment-15961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

