+++
type = "question"
title = "Capture Review"
description = '''I am new to WireShark and was wondering if there is a fee based service to have a capture file reviewed by a professional. I am learning as I go but don&#x27;t think I will be able to properly evaluate the data anytime soon. '''
date = "2012-06-26T06:54:00Z"
lastmod = "2012-06-26T12:34:00Z"
weight = 12175
keywords = [ "professional", "review" ]
aliases = [ "/questions/12175" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Review](/questions/12175/capture-review)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12175-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12175-score" class="post-score" title="current number of votes">0</div><span id="post-12175-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to WireShark and was wondering if there is a fee based service to have a capture file reviewed by a professional. I am learning as I go but don't think I will be able to properly evaluate the data anytime soon.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-professional" rel="tag" title="see questions tagged &#39;professional&#39;">professional</span> <span class="post-tag tag-link-review" rel="tag" title="see questions tagged &#39;review&#39;">review</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '12, 06:54</strong></p><img src="https://secure.gravatar.com/avatar/77a2c01105f7c8f98e757a897192b73a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="idtfla&#39;s gravatar image" /><p><span>idtfla</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="idtfla has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Jun '12, 07:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-12175" class="comments-container"></div><div id="comment-tools-12175" class="comment-tools"></div><div class="clear"></div><div id="comment-12175-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="12181"></span>

<div id="answer-container-12181" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12181-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12181-score" class="post-score" title="current number of votes">0</div><span id="post-12181-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, people make a living out of network troubleshooting ;-))</p><p><strong>UPDATE</strong>: Go to <a href="http://ask.wireshark.org/users/">users</a> at this site. Click on anyone of the first 50 +/- entries (more karma means more participation and possibly better answers). Some of the guys (and gals), provide contact information in their profile. Try to contact one of them.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jun '12, 07:40</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Jun '12, 14:11</strong> </span></p></div></div><div id="comments-container-12181" class="comments-container"><span id="12185"></span><div id="comment-12185" class="comment"><div id="post-12185-score" class="comment-score">-1</div><div class="comment-text"><p>Kurt:</p><p>Some of us are professionals and really don't have time for SA answers from pepole who evedentily have more time than some of us. I asked a serious question and if you dont have anything of intelligent or of value to add to the thread then tend to your own buisness!</p></div><div id="comment-12185-info" class="comment-info"><span class="comment-age">(26 Jun '12, 08:34)</span> <span class="comment-user userinfo">idtfla</span></div></div><span id="12186"></span><div id="comment-12186" class="comment"><div id="post-12186-score" class="comment-score"></div><div class="comment-text"><p><span>@idtfla</span>, I converted your "answer" to a comment. Please see the <a href="http://ask.wireshark.org/faq/">FAQ</a> to see how this Q&amp;A site works.</p><p>While it may not have addressed your immediate concern as effectively as you would like, <span>@Kurt</span> did answer your question. This site is run entirely by volunteers <em>in their spare time</em>. If you want more information (e.g. suggestions about where to find such a service), you should edit your question or make a comment to indicate your additional requirements.</p></div><div id="comment-12186-info" class="comment-info"><span class="comment-age">(26 Jun '12, 08:52)</span> <span class="comment-user userinfo">multipleinte...</span></div></div><span id="12195"></span><div id="comment-12195" class="comment"><div id="post-12195-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@idtfla</span>, two things:</p><ol><li>what did you expect to get as an answer to such an open and unspecific question? I answered your question within the boundaries of the possible answers.</li><li>IF you had taken the time to browse some of my answers, you would have seen, that I ususally give helpful answers to <strong>specific</strong> questions.</li></ol><p>So, if you want help from people doing this in their spare time, you better give those people a good reason to further answer your questions.</p><p>Anyway, I'm willing to try again:</p><p>So, what do you expect to get as a valid answer for your question? A name, a price? What else?</p></div><div id="comment-12195-info" class="comment-info"><span class="comment-age">(26 Jun '12, 12:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12181" class="comment-tools"></div><div class="clear"></div><div id="comment-12181-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="12190"></span>

<div id="answer-container-12190" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12190-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12190-score" class="post-score" title="current number of votes">0</div><span id="post-12190-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As I believe Kurt was trying to say without advertising his services to loudly is that he would be happy to analyse the files for you for a fee... as would I for that matter. And I'm sure other people on here would too...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jun '12, 10:35</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-12190" class="comments-container"><span id="12196"></span><div id="comment-12196" class="comment"><div id="post-12196-score" class="comment-score">1</div><div class="comment-text"><p>I'm sorry, but it was not my intention to say that I'm willing to analyze the files for a fee. I'm doing business only with people I know personally.</p><p>I just wanted to say, that there might be some people in a wireshark Q&amp;A site that could possibly do it. Maybe I should have made this clear.</p><p>Never mind. I was my fault and thus I'm going to write the following words a hundred times to my chalkboard :-))</p><blockquote><p>Some people don't understand a broad hint. Be more specific!<br />
Some people don't understand a broad hint. Be more specific!<br />
Some people don't understand a broad hint. Be more specific!</p></blockquote></div><div id="comment-12196-info" class="comment-info"><span class="comment-age">(26 Jun '12, 12:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12190" class="comment-tools"></div><div class="clear"></div><div id="comment-12190-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

