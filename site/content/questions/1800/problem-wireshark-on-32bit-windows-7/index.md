+++
type = "question"
title = "Problem : Wireshark on 32bit Windows 7"
description = '''Hi, I have installed Wireshark (wireshark-win32-1.4.3) on my PC having 32-bit Windows 7 OS. When I tried to capture network packets its showing &quot;There is no interface on which capture can be done&quot;. When I tried to see interfaces, its showing blank. Could you please let me know how to configure it, p...'''
date = "2011-01-18T20:28:00Z"
lastmod = "2011-01-18T20:40:00Z"
weight = 1800
keywords = [ "windows7", "interfaces", "missing" ]
aliases = [ "/questions/1800" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Problem : Wireshark on 32bit Windows 7](/questions/1800/problem-wireshark-on-32bit-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1800-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1800-score" class="post-score" title="current number of votes">0</div><span id="post-1800-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have installed Wireshark (wireshark-win32-1.4.3) on my PC having 32-bit Windows 7 OS. When I tried to capture network packets its showing "There is no interface on which capture can be done".</p><p>When I tried to see interfaces, its showing blank.</p><p>Could you please let me know how to configure it, previously I was using Windos XP on which it was working properly.</p><p>Thanks Priyanshu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span> <span class="post-tag tag-link-missing" rel="tag" title="see questions tagged &#39;missing&#39;">missing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '11, 20:28</strong></p><img src="https://secure.gravatar.com/avatar/24f8408c3d93fef7e0b9b0c8941a3f71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rketest&#39;s gravatar image" /><p><span>rketest</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rketest has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-1800" class="comments-container"></div><div id="comment-tools-1800" class="comment-tools"></div><div class="clear"></div><div id="comment-1800-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1801"></span>

<div id="answer-container-1801" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1801-score" class="post-score" title="current number of votes">0</div><span id="post-1801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Its solved now :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '11, 20:40</strong></p><img src="https://secure.gravatar.com/avatar/24f8408c3d93fef7e0b9b0c8941a3f71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rketest&#39;s gravatar image" /><p><span>rketest</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rketest has no accepted answers">0%</span></p></div></div><div id="comments-container-1801" class="comments-container"></div><div id="comment-tools-1801" class="comment-tools"></div><div class="clear"></div><div id="comment-1801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

