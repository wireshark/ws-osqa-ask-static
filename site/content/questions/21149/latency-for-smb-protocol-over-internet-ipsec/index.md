+++
type = "question"
title = "Latency for SMB protocol over internet IPSEC"
description = '''Hi  anyone have issues after migrating from a dedicated point to point link like FR or T1 to a high latency 300ms+ INET IPSEC connection with a SMB file copy ?? attached is a very chatty pcap'''
date = "2013-05-15T11:25:00Z"
lastmod = "2013-05-15T12:35:00Z"
weight = 21149
keywords = [ "latency", "over", "hi", "link", "smb" ]
aliases = [ "/questions/21149" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Latency for SMB protocol over internet IPSEC](/questions/21149/latency-for-smb-protocol-over-internet-ipsec)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21149-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21149-score" class="post-score" title="current number of votes">0</div><span id="post-21149-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi anyone have issues after migrating from a dedicated point to point link like FR or T1 to a high latency 300ms+ INET IPSEC connection with a SMB file copy ??</p><p>attached is a very chatty pcap</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span> <span class="post-tag tag-link-over" rel="tag" title="see questions tagged &#39;over&#39;">over</span> <span class="post-tag tag-link-hi" rel="tag" title="see questions tagged &#39;hi&#39;">hi</span> <span class="post-tag tag-link-link" rel="tag" title="see questions tagged &#39;link&#39;">link</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '13, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/0760368102b2ffdce1b5f4208faa47af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="franki21&#39;s gravatar image" /><p><span>franki21</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="franki21 has no accepted answers">0%</span></p></div></div><div id="comments-container-21149" class="comments-container"></div><div id="comment-tools-21149" class="comment-tools"></div><div class="clear"></div><div id="comment-21149-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="21150"></span>

<div id="answer-container-21150" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21150-score" class="post-score" title="current number of votes">0</div><span id="post-21150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>a high latency 300ms</p></blockquote><p>300 ms latency? Well.... don't expect any wonders regarding throughput ;-))</p><blockquote><p>attached is a very chatty pcap</p></blockquote><p>Sorry, no link found.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '13, 11:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-21150" class="comments-container"><span id="21151"></span><div id="comment-21151" class="comment"><div id="post-21151-score" class="comment-score"></div><div class="comment-text"><p>will post in a min</p></div><div id="comment-21151-info" class="comment-info"><span class="comment-age">(15 May '13, 11:32)</span> <span class="comment-user userinfo">franki21</span></div></div><span id="21152"></span><div id="comment-21152" class="comment"><div id="post-21152-score" class="comment-score"></div><div class="comment-text"><p>pcap == <a href="https://www.dropbox.com/s/aqexz1lnpu860ov/India_Latency.pcap">https://www.dropbox.com/s/aqexz1lnpu860ov/India_Latency.pcap</a></p></div><div id="comment-21152-info" class="comment-info"><span class="comment-age">(15 May '13, 12:02)</span> <span class="comment-user userinfo">franki21</span></div></div></div><div id="comment-tools-21150" class="comment-tools"></div><div class="clear"></div><div id="comment-21150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21153"></span>

<div id="answer-container-21153" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21153-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21153-score" class="post-score" title="current number of votes">0</div><span id="post-21153-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're using SMB (v1), then you can only tranfer one 64K block per RTT, so in your case that would mean about 192K which is about 1,5Mbit/s. But it could also be that your SMB client is using 4K blocks in which case you'll end up with about 100 Kbit/s.</p><p>So, please check the blocksize that is being used and the SMB version. This can be seen in the trace.</p><p>Also, when doing IPsec, make sure you adjust the MSS on the VPN devices to prevent IP fragmenting of full sized TCP segments. Also, when there is some packet loss, this can really slow things down.</p><p>You will benefit from a WAN optimizer if you want to do SMB over a high-latency link.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '13, 12:04</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-21153" class="comments-container"><span id="21154"></span><div id="comment-21154" class="comment"><div id="post-21154-score" class="comment-score"></div><div class="comment-text"><p>OK, looking at the trace file, I can see SMBv1 with a blocksize of 32K. This means a maximum transfer speed of about 750Kbit/s.</p><p>But there is packet loss (about 3% retransmissions) and therefor TCP is doing congestion avoidance. Therefor it is not fully streaming the data, resulting in extra RTT delays at the TCP level, waiting for ACK's.</p><p>The main thing to investigate is the packet-loss. Once that is resolved (if that is possible), you could benefit from a WAN optimization solution to conquer the effect of the high latency on SMB. You might also try to use SMBv2, which does not wait for acknowledgement of each block before sending the next one.</p></div><div id="comment-21154-info" class="comment-info"><span class="comment-age">(15 May '13, 12:22)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="21155"></span><div id="comment-21155" class="comment"><div id="post-21155-score" class="comment-score"></div><div class="comment-text"><p>SMB ver 1, GRE tunnel interface over IPSEC in transport mode, ip mtu 1400, ip tcp adjust-mss 1360</p><p>this is by the way from US to Pune india, Routing looks normal, latency is still at 300ms up to 500ms at times</p></div><div id="comment-21155-info" class="comment-info"><span class="comment-age">(15 May '13, 12:22)</span> <span class="comment-user userinfo">franki21</span></div></div><span id="21156"></span><div id="comment-21156" class="comment"><div id="post-21156-score" class="comment-score"></div><div class="comment-text"><p>yep SMB v2 or move to FTP, user reporting 60-80kbps over a t1 file copy took 1 hour, now takes 3 over a 6MB ipsec GRE tunnel. bandwidth reporting shows not maxing out</p><p>Thanks for ALL response</p></div><div id="comment-21156-info" class="comment-info"><span class="comment-age">(15 May '13, 12:26)</span> <span class="comment-user userinfo">franki21</span></div></div><span id="21159"></span><div id="comment-21159" class="comment"><div id="post-21159-score" class="comment-score"></div><div class="comment-text"><p>Are yo seeing the block size int the smb header 0x32 ?</p></div><div id="comment-21159-info" class="comment-info"><span class="comment-age">(15 May '13, 12:35)</span> <span class="comment-user userinfo">franki21</span></div></div></div><div id="comment-tools-21153" class="comment-tools"></div><div class="clear"></div><div id="comment-21153-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

