+++
type = "question"
title = "sound alert?"
description = '''OK, wireshark is working perfectly, but is it possible to configure it to make a sound when a packet goes through pre defined filter?'''
date = "2013-10-27T06:45:00Z"
lastmod = "2016-06-17T12:57:00Z"
weight = 26434
keywords = [ "sound", "filter", "alert" ]
aliases = [ "/questions/26434" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [sound alert?](/questions/26434/sound-alert)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26434-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26434-score" class="post-score" title="current number of votes">0</div><span id="post-26434-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>OK, wireshark is working perfectly, but is it possible to configure it to make a sound when a packet goes through pre defined filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sound" rel="tag" title="see questions tagged &#39;sound&#39;">sound</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-alert" rel="tag" title="see questions tagged &#39;alert&#39;">alert</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '13, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/412b10652e55b9c4d3cc5243b7b58d0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="myrddin&#39;s gravatar image" /><p><span>myrddin</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="myrddin has no accepted answers">0%</span></p></div></div><div id="comments-container-26434" class="comments-container"></div><div id="comment-tools-26434" class="comment-tools"></div><div class="clear"></div><div id="comment-26434-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="26435"></span>

<div id="answer-container-26435" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26435-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26435-score" class="post-score" title="current number of votes">1</div><span id="post-26435-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No. Personally I would find this extremely annoying, but if you want this in Wireshark please create an item in the <a href="https://bugs.wireshark.org/bugzilla/">Wireshark Bugzilla</a> and mark it as an enhancement.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '13, 07:01</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-26435" class="comments-container"><span id="26438"></span><div id="comment-26438" class="comment"><div id="post-26438-score" class="comment-score"></div><div class="comment-text"><p>Yes I know it would be annoying, but if you get a packet passssing through a filter once every few hours it would be useful. OK, I will message developers through bugzilla, thanks</p></div><div id="comment-26438-info" class="comment-info"><span class="comment-age">(27 Oct '13, 07:29)</span> <span class="comment-user userinfo">myrddin</span></div></div><span id="26449"></span><div id="comment-26449" class="comment"><div id="post-26449-score" class="comment-score"></div><div class="comment-text"><p>There was a actually a comical <a href="https://twitter.com/geraldcombs/status/341612358529056768">tweet</a> from Gerald back in June that sparked a little casual discussion about this. Anyway, there did seem to be some interest in this. How serious was that interest? I'm not sure.</p></div><div id="comment-26449-info" class="comment-info"><span class="comment-age">(27 Oct '13, 18:02)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="53542"></span><div id="comment-53542" class="comment"><div id="post-53542-score" class="comment-score"></div><div class="comment-text"><p>This was filed as <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=9335">bug 9335</a> back on 2013-10-27.</p></div><div id="comment-53542-info" class="comment-info"><span class="comment-age">(17 Jun '16, 12:57)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-26435" class="comment-tools"></div><div class="clear"></div><div id="comment-26435-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="26472"></span>

<div id="answer-container-26472" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26472-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26472-score" class="post-score" title="current number of votes">1</div><span id="post-26472-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>but is it possible to configure it to <strong>make a sound</strong> when a <strong>packet goes through</strong> pre defined filter?</p></blockquote><p>This implies (for me) that you want to use Wireshark as a real time monitoring tool for your network traffic (as it would only make sense to use the alert sound in that way). However Wireshark and/or tshark are both analyzing tools that are not meant to work in real-time for a long time, although you can use it like that for a short period of time.</p><p>Now, if you can't use Wireshark as a long term and <strong>real time</strong> monitoring tool, how much sense does it make to let Wireshark play a sound if you click on a certain frame or if you apply a certain display filter? You know that you did that, so why let Wireshark bark at you?</p><p>That would be rather annoying, except for blind people. However I'm not sure if you can use Wireshark with a braille display ;-))</p><p>I believe you need/want a way to use Wireshark/tshark as a long term, real time monitoring tool, that uses pre-defined filters to trigger a definable action, as soon as that filter matches (like: if there is an arp request, beep twice). Unfortunately, there is no (long term, real time) monitoring only version of Wireshark/tshark right now and based on the architecture of Wiresharks dissection engine, it's rather hard to implement that, but it's not impossible.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Oct '13, 08:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-26472" class="comments-container"></div><div id="comment-tools-26472" class="comment-tools"></div><div class="clear"></div><div id="comment-26472-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="41498"></span>

<div id="answer-container-41498" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41498-score" class="post-score" title="current number of votes">1</div><span id="post-41498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This question is old, and my answer may not be quite what you were looking for, and perhaps not helpful to you at all if you're not running on the Windows platform, but ...</p><p><strong>If</strong> you are using Windows and a new enough version of dumpcap (1.10.0 or later should suffice), then you should be able to have sound/music played when a capture event occurs through the hooks provided by the <code>dumpcap.bat</code> batch file I wrote and posted at <a href="http://wiki.wireshark.org/Tools">http://wiki.wireshark.org/Tools</a>. The sound would only be played after the capture event occurred, and not for <em>every</em> packet that matched a particular filter.</p><p>Of course, you could start <code>dumpcap.exe</code> independent of the <code>dumpcap.bat</code> file and then run the batch file in <em>"Event Only"</em> mode, wrapping the <code>dumpcap.bat</code> within another batch file that just restarts it over and over. In that case, you would hear the sound after each capture event occurred, except if the capture event happened to occur between running instances.</p><p>OK, so how would you do this? First, copy the script below, copied here for convenience from this <em>"<a href="http://stackoverflow.com/questions/23313709/play-invisible-music-with-batch-file">Play invisible music with batch file?</a>"</em> question, and then save it as <code>playfile.bat</code> or some other name of your choice.</p><p><code> @echo off set file=track12.mp3 ( echo Set Sound = CreateObject("WMPlayer.OCX.7"^)   echo Sound.URL = "%file%"   echo Sound.Controls.play   echo do while Sound.currentmedia.duration = 0   echo wscript.sleep 100   echo loop   echo wscript.sleep (int(Sound.currentmedia.duration^)+1^)*1000) &gt;sound.vbs start /min sound.vbs</code></p><p>So that you can specify any arbitrary file as an argument to the <code>playfile.bat</code> file, replace the following line:</p><pre><code>set file=track12.mp3</code></pre><p>... with this one:</p><pre><code>set file=%~1</code></pre><p>Next, create a file called <em><code>eventaction.bat</code></em> whose contents are as follows:</p><pre><code>@call C:\path\to\playfile.bat &quot;C:\path\to\somesound.mp3&quot;</code></pre><p>Now when you run the <code>dumpcap.bat</code> file and a capture event occurs, the sound you've chosen will be played. For more information on using the batch file, refer to the contents of the batch file itself. You can also refer to some other questions where I suggested other possible uses for it, such as:</p><ul><li><a href="https://ask.wireshark.org/questions/39456/is-there-a-way-to-stop-capture-upon-http-error-404">https://ask.wireshark.org/questions/39456/is-there-a-way-to-stop-capture-upon-http-error-404</a></li><li><a href="https://ask.wireshark.org/questions/40888/custom-stop-recording-trigger">https://ask.wireshark.org/questions/40888/custom-stop-recording-trigger</a></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '15, 12:06</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-41498" class="comments-container"></div><div id="comment-tools-41498" class="comment-tools"></div><div class="clear"></div><div id="comment-41498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

