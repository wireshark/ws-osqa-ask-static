+++
type = "question"
title = "Can wireshark capture facebook passwords of someone that&#x27;s connected to your network?"
description = '''I know it&#x27;s stupid to ask, But I really need to know that if Wireshark can sniff passwords of apps like facebook, etc. Please help. '''
date = "2016-06-27T18:40:00Z"
lastmod = "2016-06-27T23:11:00Z"
weight = 53680
keywords = [ "facebook", "wireshark" ]
aliases = [ "/questions/53680" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can wireshark capture facebook passwords of someone that's connected to your network?](/questions/53680/can-wireshark-capture-facebook-passwords-of-someone-thats-connected-to-your-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53680-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53680-score" class="post-score" title="current number of votes">0</div><span id="post-53680-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know it's stupid to ask, But I really need to know that if Wireshark can sniff passwords of apps like facebook, etc. Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-facebook" rel="tag" title="see questions tagged &#39;facebook&#39;">facebook</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '16, 18:40</strong></p><img src="https://secure.gravatar.com/avatar/f02026c22600ff1a796cd138bd3e30b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mu%20Zz%CA%8E&#39;s gravatar image" /><p><span>Mu Zzʎ</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mu Zzʎ has no accepted answers">0%</span></p></div></div><div id="comments-container-53680" class="comments-container"></div><div id="comment-tools-53680" class="comment-tools"></div><div class="clear"></div><div id="comment-53680-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53682"></span>

<div id="answer-container-53682" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53682-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53682-score" class="post-score" title="current number of votes">0</div><span id="post-53682-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is capable of capturing the communications between your applications and the servers involved, getting at the data in the communication is a whole different matter. Depending on the encryption used and availability of key material this may or may not be possible. In general the answer would be no. This would require a more elaborate setup.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '16, 23:11</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-53682" class="comments-container"></div><div id="comment-tools-53682" class="comment-tools"></div><div class="clear"></div><div id="comment-53682-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

