+++
type = "question"
title = "Closed caption Packets"
description = '''Is there a wireshark filter to capture these ina an H.264 stream?'''
date = "2016-10-07T05:39:00Z"
lastmod = "2016-10-07T05:39:00Z"
weight = 56220
keywords = [ "captions", "closed" ]
aliases = [ "/questions/56220" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Closed caption Packets](/questions/56220/closed-caption-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56220-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56220-score" class="post-score" title="current number of votes">0</div><span id="post-56220-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a wireshark filter to capture these ina an H.264 stream?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-captions" rel="tag" title="see questions tagged &#39;captions&#39;">captions</span> <span class="post-tag tag-link-closed" rel="tag" title="see questions tagged &#39;closed&#39;">closed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '16, 05:39</strong></p><img src="https://secure.gravatar.com/avatar/7aade6bb55657aaeaed40248e4735e66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="VideoGuys&#39;s gravatar image" /><p><span>VideoGuys</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="VideoGuys has no accepted answers">0%</span></p></div></div><div id="comments-container-56220" class="comments-container"></div><div id="comment-tools-56220" class="comment-tools"></div><div class="clear"></div><div id="comment-56220-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

