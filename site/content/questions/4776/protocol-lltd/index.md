+++
type = "question"
title = "protocol LLTD"
description = '''It not a question. Is request to add recognition and analyzing of protocol LLTD (Link Layer Topology Discovery).'''
date = "2011-06-27T16:30:00Z"
lastmod = "2011-06-28T10:56:00Z"
weight = 4776
keywords = [ "lltd" ]
aliases = [ "/questions/4776" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [protocol LLTD](/questions/4776/protocol-lltd)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4776-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4776-score" class="post-score" title="current number of votes">0</div><span id="post-4776-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>It not a question. Is request to add recognition and analyzing of protocol LLTD (Link Layer Topology Discovery).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lltd" rel="tag" title="see questions tagged &#39;lltd&#39;">lltd</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '11, 16:30</strong></p><img src="https://secure.gravatar.com/avatar/b81bf1a845d986f3af020a02f69747a0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="%D0%92%D0%B0%D0%BB%D0%B5%D1%80%D0%B8%D0%B9%20Valery&#39;s gravatar image" /><p><span>Валерий Valery</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Валерий Valery has no accepted answers">0%</span></p></div></div><div id="comments-container-4776" class="comments-container"></div><div id="comment-tools-4776" class="comment-tools"></div><div class="clear"></div><div id="comment-4776-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4779"></span>

<div id="answer-container-4779" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4779-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4779-score" class="post-score" title="current number of votes">0</div><span id="post-4779-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Requests like this can be files in the <a href="https://bugs.wireshark.org">bug database</a>, as an enhancement request. Please add a sample capture file, as well as a reference to the relevant protocol documentation if possible.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '11, 22:08</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4779" class="comments-container"><span id="4789"></span><div id="comment-4789" class="comment"><div id="post-4789-score" class="comment-score"></div><div class="comment-text"><p>Again, please submit this as an enhancement request in the bug database. This site is for questions about using, or developing for, Wireshark, and doesn't work well as a way of tracking bugs or enhancement requests; the bug database does a lot better job of tracking them.</p></div><div id="comment-4789-info" class="comment-info"><span class="comment-age">(28 Jun '11, 10:56)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4779" class="comment-tools"></div><div class="clear"></div><div id="comment-4779-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

