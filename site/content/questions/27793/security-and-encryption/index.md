+++
type = "question"
title = "security and encryption"
description = '''for Example, Tom is a Computer science student. He likes to learn new things about networking and security. He wants to see whether the passwords that he logged in to certain websites are being secured or not. He tried to log in to certain websites such as Yahoo Mail, Facebook, Twitter, Gmail and On...'''
date = "2013-12-04T17:08:00Z"
lastmod = "2013-12-04T19:06:00Z"
weight = 27793
keywords = [ "secure" ]
aliases = [ "/questions/27793" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [security and encryption](/questions/27793/security-and-encryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27793-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27793-score" class="post-score" title="current number of votes">0</div><span id="post-27793-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>for Example, Tom is a Computer science student. He likes to learn new things about networking and security. He wants to see whether the passwords that he logged in to certain websites are being secured or not. He tried to log in to certain websites such as Yahoo Mail, Facebook, Twitter, Gmail and Online Banking sites.</p><p>By using Wireshark tool, how can he trace whether the password has been encrypted or not? (List the steps)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-secure" rel="tag" title="see questions tagged &#39;secure&#39;">secure</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '13, 17:08</strong></p><img src="https://secure.gravatar.com/avatar/53b490d22505f056e26e96d7950b3681?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jamal&#39;s gravatar image" /><p><span>jamal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jamal has no accepted answers">0%</span></p></div></div><div id="comments-container-27793" class="comments-container"><span id="27794"></span><div id="comment-27794" class="comment"><div id="post-27794-score" class="comment-score">1</div><div class="comment-text"><p>Sounds like homework or an exam question.... :)</p><p>See Jasper's answer on another recently asked question: <a href="http://ask.wireshark.org/questions/27769/how-to-read-a-pcap-file">comment</a></p></div><div id="comment-27794-info" class="comment-info"><span class="comment-age">(04 Dec '13, 19:06)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-27793" class="comment-tools"></div><div class="clear"></div><div id="comment-27793-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

