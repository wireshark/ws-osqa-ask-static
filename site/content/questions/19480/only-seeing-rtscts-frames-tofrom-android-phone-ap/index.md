+++
type = "question"
title = "Only seeing RTS/CTS frames to/from Android Phone &amp; AP"
description = '''Hi everyone, I&#x27;m stuck here. Trying to sniff packets to/from a phone whose WiFi is associated to my AP using a laptop with Wireshark associated on the same network. I&#x27;ve configured Wireshark with my AP&#x27;s WPA password/SSID, and I can even see the 4 Key messages passed between my phone&#x27;s WiFi and the ...'''
date = "2013-03-13T16:14:00Z"
lastmod = "2013-03-13T16:14:00Z"
weight = 19480
keywords = [ "sniffing", "wifi", "rts", "cts" ]
aliases = [ "/questions/19480" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Only seeing RTS/CTS frames to/from Android Phone & AP](/questions/19480/only-seeing-rtscts-frames-tofrom-android-phone-ap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19480-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19480-score" class="post-score" title="current number of votes">0</div><span id="post-19480-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone,</p><p>I'm stuck here. Trying to sniff packets to/from a phone whose WiFi is associated to my AP using a laptop with Wireshark associated on the same network.</p><p>I've configured Wireshark with my AP's WPA password/SSID, and I can even see the 4 Key messages passed between my phone's WiFi and the AP. However, from that point on, I only see "request-to-send" and "clear-to-send" frames to/from the phone and AP. Why am I not seeing any datapackets (as I browse to clear HTTP sites from my phone).</p><p>I'm running Ubuntu 12.10 with a Ralink 2573 USB dongle that appears to be able to put successfully in monitor mode by running airmon-ng (as I can see other frames on the 802.11 network).</p><p>Any help or thoughts would be appreciated. Thanks in advance.</p><p>Greg</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-rts" rel="tag" title="see questions tagged &#39;rts&#39;">rts</span> <span class="post-tag tag-link-cts" rel="tag" title="see questions tagged &#39;cts&#39;">cts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '13, 16:14</strong></p><img src="https://secure.gravatar.com/avatar/991d3b6df9045a2d7288b015ef1e4d94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gjbrault&#39;s gravatar image" /><p><span>gjbrault</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gjbrault has no accepted answers">0%</span></p></div></div><div id="comments-container-19480" class="comments-container"></div><div id="comment-tools-19480" class="comment-tools"></div><div class="clear"></div><div id="comment-19480-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

