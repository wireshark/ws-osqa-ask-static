+++
type = "question"
title = "DHCPv6 option56 NTP Server option  is unknown by Wireshark"
description = '''I have download the newest stable release 1.8.5'''
date = "2013-02-23T00:39:00Z"
lastmod = "2013-02-23T02:51:00Z"
weight = 18828
keywords = [ "dhcpv6" ]
aliases = [ "/questions/18828" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [DHCPv6 option56 NTP Server option is unknown by Wireshark](/questions/18828/dhcpv6-option56-ntp-server-option-is-unknown-by-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18828-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18828-score" class="post-score" title="current number of votes">0</div><span id="post-18828-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have download the newest stable release 1.8.5</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dhcpv6" rel="tag" title="see questions tagged &#39;dhcpv6&#39;">dhcpv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '13, 00:39</strong></p><img src="https://secure.gravatar.com/avatar/8811a659d95b719cc79eae50323a5e2b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wengjianqi&#39;s gravatar image" /><p><span>wengjianqi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wengjianqi has no accepted answers">0%</span></p></div></div><div id="comments-container-18828" class="comments-container"></div><div id="comment-tools-18828" class="comment-tools"></div><div class="clear"></div><div id="comment-18828-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18831"></span>

<div id="answer-container-18831" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18831-score" class="post-score" title="current number of votes">1</div><span id="post-18831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Correct, this is not implemented. You should file an enhancement bug at bugs.wireshark.org requesting implementation of dissection for RFC 5908. Or create a patch yourself and file that :)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '13, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-18831" class="comments-container"></div><div id="comment-tools-18831" class="comment-tools"></div><div class="clear"></div><div id="comment-18831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

