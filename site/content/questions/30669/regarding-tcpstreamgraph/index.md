+++
type = "question"
title = "regarding tcpstreamgraph"
description = '''i am using wireshark under contiki os.i wish to know how to enable tcpstreamgraph which comes under statistics menu.'''
date = "2014-03-10T20:55:00Z"
lastmod = "2014-03-10T21:13:00Z"
weight = 30669
keywords = [ "about", "wireshark" ]
aliases = [ "/questions/30669" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [regarding tcpstreamgraph](/questions/30669/regarding-tcpstreamgraph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30669-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30669-score" class="post-score" title="current number of votes">0</div><span id="post-30669-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am using wireshark under contiki os.i wish to know how to enable tcpstreamgraph which comes under statistics menu.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-about" rel="tag" title="see questions tagged &#39;about&#39;">about</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '14, 20:55</strong></p><img src="https://secure.gravatar.com/avatar/9059ada88f64c5f9fb8e2ed0b6843357?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BLUE%20ICE&#39;s gravatar image" /><p><span>BLUE ICE</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BLUE ICE has no accepted answers">0%</span></p></div></div><div id="comments-container-30669" class="comments-container"><span id="30670"></span><div id="comment-30670" class="comment"><div id="post-30670-score" class="comment-score"></div><div class="comment-text"><p>Is a TCP packet selected when you try to open the dialog?</p></div><div id="comment-30670-info" class="comment-info"><span class="comment-age">(10 Mar '14, 21:09)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="30671"></span><div id="comment-30671" class="comment"><div id="post-30671-score" class="comment-score"></div><div class="comment-text"><p>actually the protocol uses UDP packets</p></div><div id="comment-30671-info" class="comment-info"><span class="comment-age">(10 Mar '14, 21:13)</span> <span class="comment-user userinfo">BLUE ICE</span></div></div></div><div id="comment-tools-30669" class="comment-tools"></div><div class="clear"></div><div id="comment-30669-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

