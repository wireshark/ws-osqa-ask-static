+++
type = "question"
title = "is it possible to capture traffic within a box"
description = '''Hi I have a services talking to another services which is hosted on the same box. I am not able to capture the traffic between them. How can i make wireshark capture the traffic between them thanks scara'''
date = "2014-02-05T06:41:00Z"
lastmod = "2014-02-05T06:49:00Z"
weight = 29460
keywords = [ "capture" ]
aliases = [ "/questions/29460" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [is it possible to capture traffic within a box](/questions/29460/is-it-possible-to-capture-traffic-within-a-box)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29460-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29460-score" class="post-score" title="current number of votes">0</div><span id="post-29460-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I have a services talking to another services which is hosted on the same box. I am not able to capture the traffic between them. How can i make wireshark capture the traffic between them</p><p>thanks scara</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '14, 06:41</strong></p><img src="https://secure.gravatar.com/avatar/be8a9b2e9d87b13606c3b9e75d26e71d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scara&#39;s gravatar image" /><p><span>scara</span><br />
<span class="score" title="31 reputation points">31</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scara has no accepted answers">0%</span></p></div></div><div id="comments-container-29460" class="comments-container"></div><div id="comment-tools-29460" class="comment-tools"></div><div class="clear"></div><div id="comment-29460-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29461"></span>

<div id="answer-container-29461" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29461-score" class="post-score" title="current number of votes">2</div><span id="post-29461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="scara has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're using Windows, then it's not very easy. See the <a href="http://wiki.wireshark.org/CaptureSetup/Loopback">loopback capture wiki</a> page, and other similar questions, e.g. <a href="http://ask.wireshark.org/questions/29298/capturing-traffic-from-loopback-adapter-on-windows-7-64-bit">here</a> and <a href="http://ask.wireshark.org/questions/29211/how-can-wireshark-capture-local-host-traffic-on-windows">here</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '14, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-29461" class="comments-container"></div><div id="comment-tools-29461" class="comment-tools"></div><div class="clear"></div><div id="comment-29461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

