+++
type = "question"
title = "wireshark graphs"
description = '''While generating graphs i saw options like avg(), sum(), count(*) etc can anybody please tell me what exactly they represent. '''
date = "2011-07-26T23:25:00Z"
lastmod = "2011-07-27T22:19:00Z"
weight = 5295
keywords = [ "graphs" ]
aliases = [ "/questions/5295" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark graphs](/questions/5295/wireshark-graphs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5295-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5295-score" class="post-score" title="current number of votes">0</div><span id="post-5295-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>While generating graphs i saw options like avg(<em>), sum(</em>), count(*) etc can anybody please tell me what exactly they represent.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '11, 23:25</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div></div><div id="comments-container-5295" class="comments-container"><span id="5296"></span><div id="comment-5296" class="comment"><div id="post-5296-score" class="comment-score"></div><div class="comment-text"><p>i got the answer http://www.wireshark.org/docs/man-pages/wireshark.html</p></div><div id="comment-5296-info" class="comment-info"><span class="comment-age">(27 Jul '11, 00:37)</span> <span class="comment-user userinfo">sagu072</span></div></div><span id="5312"></span><div id="comment-5312" class="comment"><div id="post-5312-score" class="comment-score">1</div><div class="comment-text"><p>If you've found the answer, why not make an answer with the link and mark it accepted?</p></div><div id="comment-5312-info" class="comment-info"><span class="comment-age">(27 Jul '11, 07:35)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-5295" class="comment-tools"></div><div class="clear"></div><div id="comment-5295-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5336"></span>

<div id="answer-container-5336" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5336-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5336-score" class="post-score" title="current number of votes">0</div><span id="post-5336-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>http://www.wireshark.org/docs/man-pages/wireshark.html</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jul '11, 22:19</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div></div><div id="comments-container-5336" class="comments-container"></div><div id="comment-tools-5336" class="comment-tools"></div><div class="clear"></div><div id="comment-5336-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

