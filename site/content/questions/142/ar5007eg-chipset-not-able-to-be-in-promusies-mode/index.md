+++
type = "question"
title = "AR5007EG chipset not able to be in promusies mode"
description = '''Can some tell me ho to get the carrect interface for this chip set '''
date = "2010-09-16T00:17:00Z"
lastmod = "2010-09-16T08:16:00Z"
weight = 142
keywords = [ "chipset" ]
aliases = [ "/questions/142" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [AR5007EG chipset not able to be in promusies mode](/questions/142/ar5007eg-chipset-not-able-to-be-in-promusies-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-142-score" class="post-score" title="current number of votes">0</div><span id="post-142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can some tell me ho to get the carrect interface for this chip set</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-chipset" rel="tag" title="see questions tagged &#39;chipset&#39;">chipset</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '10, 00:17</strong></p><img src="https://secure.gravatar.com/avatar/229c6da2cc3cd0e46935404ae061bb24?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tristan%20Bishop&#39;s gravatar image" /><p><span>Tristan Bishop</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tristan Bishop has no accepted answers">0%</span></p></div></div><div id="comments-container-142" class="comments-container"><span id="151"></span><div id="comment-151" class="comment"><div id="post-151-score" class="comment-score"></div><div class="comment-text"><p>What operating system are you using?</p></div><div id="comment-151-info" class="comment-info"><span class="comment-age">(16 Sep '10, 08:16)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-142" class="comment-tools"></div><div class="clear"></div><div id="comment-142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="144"></span>

<div id="answer-container-144" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-144-score" class="post-score" title="current number of votes">0</div><span id="post-144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please tell me how to use promiscuous with this chip set it says the interface is not right</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '10, 00:20</strong></p><img src="https://secure.gravatar.com/avatar/229c6da2cc3cd0e46935404ae061bb24?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tristan%20Bishop&#39;s gravatar image" /><p><span>Tristan Bishop</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tristan Bishop has no accepted answers">0%</span></p></div></div><div id="comments-container-144" class="comments-container"><span id="145"></span><div id="comment-145" class="comment"><div id="post-145-score" class="comment-score"></div><div class="comment-text"><p>sorry didnt mean to put this but didnt wanna look stupid</p></div><div id="comment-145-info" class="comment-info"><span class="comment-age">(16 Sep '10, 00:21)</span> <span class="comment-user userinfo">Tristan Bishop</span></div></div></div><div id="comment-tools-144" class="comment-tools"></div><div class="clear"></div><div id="comment-144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

