+++
type = "question"
title = "print file"
description = '''Tell me, please, where i can read about what information shown in file, created on windows after printing into file?'''
date = "2013-09-25T05:03:00Z"
lastmod = "2013-09-25T12:18:00Z"
weight = 25209
keywords = [ "print" ]
aliases = [ "/questions/25209" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [print file](/questions/25209/print-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25209-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25209-score" class="post-score" title="current number of votes">0</div><span id="post-25209-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Tell me, please, where i can read about what information shown in file, created on windows after printing into file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-print" rel="tag" title="see questions tagged &#39;print&#39;">print</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '13, 05:03</strong></p><img src="https://secure.gravatar.com/avatar/d288de6c78aac7f2d94b0eecc655b142?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ovo&#39;s gravatar image" /><p><span>ovo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ovo has no accepted answers">0%</span></p></div></div><div id="comments-container-25209" class="comments-container"></div><div id="comment-tools-25209" class="comment-tools"></div><div class="clear"></div><div id="comment-25209-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25212"></span>

<div id="answer-container-25212" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25212-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25212-score" class="post-score" title="current number of votes">0</div><span id="post-25212-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ovo has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the docs.</p><blockquote><p><a href="http://www.wireshark.org/docs/wsug_html_chunked/ChIOPrintSection.html">http://www.wireshark.org/docs/wsug_html_chunked/ChIOPrintSection.html</a></p></blockquote><p>It depends on the options you chose while printing.</p><p>If it was a plain text file, that file will contain the ASCII representation of the dissected packets (protocol details) as defined in 'Packet Format'.</p><p>If it was a Postscript file (a predecessor of PDF), it will contain basically the same as the text file however in Postscript format and you will need a <a href="http://pages.cs.wisc.edu/~ghost/gsview/index.htm">Postscript viewer</a> to view the file.</p><p>Does that answer your question.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '13, 06:51</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25212" class="comments-container"><span id="25221"></span><div id="comment-25221" class="comment"><div id="post-25221-score" class="comment-score"></div><div class="comment-text"><p>It's not that answer I need, but from some it parts I understood what exactly i need and how I can get it without any help, so thank you.</p></div><div id="comment-25221-info" class="comment-info"><span class="comment-age">(25 Sep '13, 09:00)</span> <span class="comment-user userinfo">ovo</span></div></div><span id="25233"></span><div id="comment-25233" class="comment"><div id="post-25233-score" class="comment-score"></div><div class="comment-text"><p>O.K. what do you need?</p></div><div id="comment-25233-info" class="comment-info"><span class="comment-age">(25 Sep '13, 12:18)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-25212" class="comment-tools"></div><div class="clear"></div><div id="comment-25212-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

