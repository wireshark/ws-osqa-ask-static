+++
type = "question"
title = "wireshark in accespoint"
description = '''can i install wireshark on my accesspoint???'''
date = "2012-02-07T21:46:00Z"
lastmod = "2012-02-07T23:19:00Z"
weight = 8882
keywords = [ "access", "wireshark", "point" ]
aliases = [ "/questions/8882" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark in accespoint](/questions/8882/wireshark-in-accespoint)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8882-score" class="post-score" title="current number of votes">0</div><span id="post-8882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can i install wireshark on my accesspoint???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-access" rel="tag" title="see questions tagged &#39;access&#39;">access</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-point" rel="tag" title="see questions tagged &#39;point&#39;">point</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '12, 21:46</strong></p><img src="https://secure.gravatar.com/avatar/c61a076b3582d946304389d28311c43f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sona&#39;s gravatar image" /><p><span>Sona</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sona has no accepted answers">0%</span></p></div></div><div id="comments-container-8882" class="comments-container"><span id="8886"></span><div id="comment-8886" class="comment"><div id="post-8886-score" class="comment-score"></div><div class="comment-text"><p>What is your accesspoint?</p></div><div id="comment-8886-info" class="comment-info"><span class="comment-age">(07 Feb '12, 23:19)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-8882" class="comment-tools"></div><div class="clear"></div><div id="comment-8882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

