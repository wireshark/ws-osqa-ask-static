+++
type = "question"
title = "Packet got different color when tracking on different computer"
description = '''Hi, When I open a captured .pcap packet on different computer, it gives me a different colored packet, for example, if I want to track a specify IPv4 TCP packet, Computer A gave me a green background color for that packet, but computer B gave me a black background color for that packet... I am sure ...'''
date = "2015-06-02T01:56:00Z"
lastmod = "2015-06-02T02:51:00Z"
weight = 42823
keywords = [ "color", "packet" ]
aliases = [ "/questions/42823" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Packet got different color when tracking on different computer](/questions/42823/packet-got-different-color-when-tracking-on-different-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42823-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42823-score" class="post-score" title="current number of votes">0</div><span id="post-42823-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, When I open a captured .pcap packet on different computer,</p><p>it gives me a different colored packet,</p><p>for example, if I want to track a specify IPv4 TCP packet,</p><p>Computer A gave me a green background color for that packet,</p><p>but computer B gave me a black background color for that packet...</p><p>I am sure that both of the computer are using a default settings and I've no idea what make this happened...</p><p>Can anyone give me some advice of how to solve this problem ?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '15, 01:56</strong></p><img src="https://secure.gravatar.com/avatar/e6ff2184109221c8715a8ede1bf5eacc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="phantomcy&#39;s gravatar image" /><p><span>phantomcy</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="phantomcy has no accepted answers">0%</span></p></div></div><div id="comments-container-42823" class="comments-container"></div><div id="comment-tools-42823" class="comment-tools"></div><div class="clear"></div><div id="comment-42823-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42825"></span>

<div id="answer-container-42825" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42825-score" class="post-score" title="current number of votes">4</div><span id="post-42825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="phantomcy has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On both computers, of the same packet, open the frame line in the packet details pane. There you'll find, in square brackets, which coloring rule is applied to that packet. If they are the same go look at the color configuration of that rule. Somewhere there's a difference I presume.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '15, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-42825" class="comments-container"></div><div id="comment-tools-42825" class="comment-tools"></div><div class="clear"></div><div id="comment-42825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

