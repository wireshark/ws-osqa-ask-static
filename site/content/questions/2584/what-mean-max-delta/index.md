+++
type = "question"
title = "what mean &quot;max delta &quot;"
description = '''max delta in wirshark '''
date = "2011-02-28T07:21:00Z"
lastmod = "2011-02-28T08:06:00Z"
weight = 2584
keywords = [ "delta" ]
aliases = [ "/questions/2584" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what mean "max delta "](/questions/2584/what-mean-max-delta)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2584-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2584-score" class="post-score" title="current number of votes">0</div><span id="post-2584-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>max delta in wirshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delta" rel="tag" title="see questions tagged &#39;delta&#39;">delta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '11, 07:21</strong></p><img src="https://secure.gravatar.com/avatar/e3920112da5cea8c91e24008b9631622?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Khirennas%20Abdelhamid&#39;s gravatar image" /><p><span>Khirennas Ab...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Khirennas Abdelhamid has no accepted answers">0%</span></p></div></div><div id="comments-container-2584" class="comments-container"></div><div id="comment-tools-2584" class="comment-tools"></div><div class="clear"></div><div id="comment-2584-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2588"></span>

<div id="answer-container-2588" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2588-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2588-score" class="post-score" title="current number of votes">0</div><span id="post-2588-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><em>sigh</em></p><p>For such a nicely formulated question, let me present a suitable answer:</p><p>http://www.google.de/search?q=max+delta+wireshark</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '11, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '11, 08:06</strong> </span></p></div></div><div id="comments-container-2588" class="comments-container"></div><div id="comment-tools-2588" class="comment-tools"></div><div class="clear"></div><div id="comment-2588-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

