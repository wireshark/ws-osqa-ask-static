+++
type = "question"
title = "Problem with decoding Powerlink SDO messages in V1.12"
description = '''Hello, since I have updated my wireshrak from V1.10.14 to V1.12.6 it does not decode the Powerlink SDO messages. I have checkt the relaese nodes from the versions but do not find a hint that something on the protokoll interpreter has changet. Has anyone a idea what I can do to geht the new version t...'''
date = "2015-07-03T05:15:00Z"
lastmod = "2015-07-03T05:38:00Z"
weight = 43848
keywords = [ "epl", "powerlink" ]
aliases = [ "/questions/43848" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Problem with decoding Powerlink SDO messages in V1.12](/questions/43848/problem-with-decoding-powerlink-sdo-messages-in-v112)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43848-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43848-score" class="post-score" title="current number of votes">0</div><span id="post-43848-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>since I have updated my wireshrak from V1.10.14 to V1.12.6 it does not decode the Powerlink SDO messages. I have checkt the relaese nodes from the versions but do not find a hint that something on the protokoll interpreter has changet. Has anyone a idea what I can do to geht the new version to decode the SDO messages? I have checkt that the capture was ok, wenn I save the log with the V1.12 and open it with the 1.10 the SDOs will decode. Has anyone an idea?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-epl" rel="tag" title="see questions tagged &#39;epl&#39;">epl</span> <span class="post-tag tag-link-powerlink" rel="tag" title="see questions tagged &#39;powerlink&#39;">powerlink</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '15, 05:15</strong></p><img src="https://secure.gravatar.com/avatar/1f99d80d2a54079b8e4300ffe7edb28a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ulf&#39;s gravatar image" /><p><span>Ulf</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ulf has no accepted answers">0%</span></p></div></div><div id="comments-container-43848" class="comments-container"></div><div id="comment-tools-43848" class="comment-tools"></div><div class="clear"></div><div id="comment-43848-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43850"></span>

<div id="answer-container-43850" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43850-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43850-score" class="post-score" title="current number of votes">0</div><span id="post-43850-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ulf has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please fill a bug report with a capture attached to <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jul '15, 05:30</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-43850" class="comments-container"><span id="43851"></span><div id="comment-43851" class="comment"><div id="post-43851-score" class="comment-score"></div><div class="comment-text"><p>Ok,</p><p>I will do that.</p></div><div id="comment-43851-info" class="comment-info"><span class="comment-age">(03 Jul '15, 05:38)</span> <span class="comment-user userinfo">Ulf</span></div></div></div><div id="comment-tools-43850" class="comment-tools"></div><div class="clear"></div><div id="comment-43850-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

