+++
type = "question"
title = "save all http messages lines(tcp stream)"
description = '''i want to save all http messages lines in a file. wireshark just let me save http messages about one tcp stream. but i want to save all the http header lines of a pcap file in one txt file. how can i do it? is there any command in tcpflow or wireshark? thanks in advance'''
date = "2015-04-24T13:26:00Z"
lastmod = "2015-04-24T13:26:00Z"
weight = 41801
keywords = [ "dissect_http", "pcap" ]
aliases = [ "/questions/41801" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [save all http messages lines(tcp stream)](/questions/41801/save-all-http-messages-linestcp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41801-score" class="post-score" title="current number of votes">0</div><span id="post-41801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i want to save all http messages lines in a file. wireshark just let me save http messages about one tcp stream. but i want to save all the http header lines of a pcap file in one txt file. how can i do it? is there any command in tcpflow or wireshark? thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissect_http" rel="tag" title="see questions tagged &#39;dissect_http&#39;">dissect_http</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '15, 13:26</strong></p><img src="https://secure.gravatar.com/avatar/ffdb11952a5028d43a89614d8cad5983?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fateme&#39;s gravatar image" /><p><span>Fateme</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fateme has no accepted answers">0%</span></p></div></div><div id="comments-container-41801" class="comments-container"></div><div id="comment-tools-41801" class="comment-tools"></div><div class="clear"></div><div id="comment-41801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

