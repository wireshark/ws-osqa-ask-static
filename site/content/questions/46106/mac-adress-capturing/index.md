+++
type = "question"
title = "mac adress capturing"
description = '''can i get mac addresses of devices on a network to which my device is not connected'''
date = "2015-09-24T07:08:00Z"
lastmod = "2015-09-24T08:42:00Z"
weight = 46106
keywords = [ "mac", "mac-address" ]
aliases = [ "/questions/46106" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [mac adress capturing](/questions/46106/mac-adress-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46106-score" class="post-score" title="current number of votes">0</div><span id="post-46106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can i get mac addresses of devices on a network to which my device is not connected</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '15, 07:08</strong></p><img src="https://secure.gravatar.com/avatar/e17fa58617ea6c11d42c97b0b2e3bdff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Arun%20Das&#39;s gravatar image" /><p><span>Arun Das</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Arun Das has no accepted answers">0%</span></p></div></div><div id="comments-container-46106" class="comments-container"><span id="46109"></span><div id="comment-46109" class="comment"><div id="post-46109-score" class="comment-score"></div><div class="comment-text"><p>Is this a homework question?</p></div><div id="comment-46109-info" class="comment-info"><span class="comment-age">(24 Sep '15, 08:37)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="46110"></span><div id="comment-46110" class="comment"><div id="post-46110-score" class="comment-score"></div><div class="comment-text"><p>Sry..i didn get u</p></div><div id="comment-46110-info" class="comment-info"><span class="comment-age">(24 Sep '15, 08:42)</span> <span class="comment-user userinfo">Arun Das</span></div></div></div><div id="comment-tools-46106" class="comment-tools"></div><div class="clear"></div><div id="comment-46106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

