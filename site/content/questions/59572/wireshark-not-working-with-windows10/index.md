+++
type = "question"
title = "Wireshark not working with Windows10"
description = '''I am having problems getting Wireshark to work with Windows10 Home edition. I have been through lots of sugestions from this site and others via Dr Google. This includes using Win10Pcap and Ncap. I have installed each in different orders with Wireshark and restarted Windows each time.  Wireshark wil...'''
date = "2017-02-20T17:13:00Z"
lastmod = "2017-02-20T17:13:00Z"
weight = 59572
keywords = [ "windows10", "wireshark" ]
aliases = [ "/questions/59572" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not working with Windows10](/questions/59572/wireshark-not-working-with-windows10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59572-score" class="post-score" title="current number of votes">0</div><span id="post-59572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am having problems getting Wireshark to work with Windows10 Home edition. I have been through lots of sugestions from this site and others via Dr Google. This includes using Win10Pcap and Ncap. I have installed each in different orders with Wireshark and restarted Windows each time. Wireshark will not start at all using the Wireshark icon. It will start using the Wireshark legacy icon. However, once it has started, there are no interfaces available for use with capture.</p><p>I have used Wireshark (and Ethereal) for many yearswith various versions of Windows. Thid is the first time with Windows10.</p><p>Andy</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '17, 17:13</strong></p><img src="https://secure.gravatar.com/avatar/b73dd4762bf68cc99769285c59971d8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Blackmur&#39;s gravatar image" /><p><span>Blackmur</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Blackmur has no accepted answers">0%</span></p></div></div><div id="comments-container-59572" class="comments-container"></div><div id="comment-tools-59572" class="comment-tools"></div><div class="clear"></div><div id="comment-59572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

