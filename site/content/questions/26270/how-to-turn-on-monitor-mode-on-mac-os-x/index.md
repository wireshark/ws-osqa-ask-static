+++
type = "question"
title = "How to turn on monitor mode on Mac OS X"
description = '''Hello, I just downloaded wireshark 1.10.2 on my Mac OSX 10.7.5 and I&#x27;m trying to capture packets on my home wifi network in monitor mode. I&#x27;ve selected my wifi network (en1) in the interface list and from what I&#x27;ve read so far in other threads and the wireshark wiki I should have an option to check ...'''
date = "2013-10-21T18:33:00Z"
lastmod = "2015-01-26T21:44:00Z"
weight = 26270
keywords = [ "macosx", "monitor-mode" ]
aliases = [ "/questions/26270" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to turn on monitor mode on Mac OS X](/questions/26270/how-to-turn-on-monitor-mode-on-mac-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26270-score" class="post-score" title="current number of votes">1</div><span id="post-26270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I just downloaded wireshark 1.10.2 on my Mac OSX 10.7.5 and I'm trying to capture packets on my home wifi network in monitor mode. I've selected my wifi network (en1) in the interface list and from what I've read so far in other threads and the wireshark wiki I should have an option to check off a "Turn on Monitor mode" checkbox in the Capture Options. However, I do not have that option, it doesn't even show greyed out, the checkbox doesn't exist. Can anyone help me turn on monitor mode using wireshark? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '13, 18:33</strong></p><img src="https://secure.gravatar.com/avatar/32ea09eaeb7f0951c959a1050b7e841c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="geazi7&#39;s gravatar image" /><p><span>geazi7</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="geazi7 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jan '15, 16:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-26270" class="comments-container"></div><div id="comment-tools-26270" class="comment-tools"></div><div class="clear"></div><div id="comment-26270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26281"></span>

<div id="answer-container-26281" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26281-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26281-score" class="post-score" title="current number of votes">1</div><span id="post-26281-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This CheckBox is now deeper in the settings:</p><p><strong>Edit / Preferences / User Interface / Capture / Interfaces: / Edit / Device: en1 [x] Monitor Mode</strong></p><p>See also:</p><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN (IEEE 802.11) capture setup</a></p><p>It is written on this page somewhere.</p></div><div class="answer-controls post-controls"><div class="community-wiki">This answer is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '13, 01:10</strong></p><img src="https://secure.gravatar.com/avatar/932f872b04b352a8f3d81288805f08be?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="franc&#39;s gravatar image" /><p><span>franc</span><br />
<span class="score" title="96 reputation points">96</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="franc has 2 accepted answers">40%</span></p></div></div><div id="comments-container-26281" class="comments-container"><span id="26282"></span><div id="comment-26282" class="comment"><div id="post-26282-score" class="comment-score"></div><div class="comment-text"><p>See here a screenshot:</p><p><a href="http://abload.de/img/bildschirmfoto2013-10nor5m.png">http://abload.de/img/bildschirmfoto2013-10nor5m.png</a></p></div><div id="comment-26282-info" class="comment-info"><span class="comment-age">(22 Oct '13, 01:14)</span> <span class="comment-user userinfo">franc</span></div></div><span id="38520"></span><div id="comment-38520" class="comment"><div id="post-38520-score" class="comment-score"></div><div class="comment-text"><p>Wow I feel dumb. Just spent way too much time looking for that monitor mode checkmark in latest wireshark on yosemite. google and even wire shark website was useless since it seems info hasn't been updated for latest wireshark version. I guess they really wanted to hide it for some reason!</p></div><div id="comment-38520-info" class="comment-info"><span class="comment-age">(10 Dec '14, 18:51)</span> <span class="comment-user userinfo">daviangel</span></div></div><span id="38527"></span><div id="comment-38527" class="comment"><div id="post-38527-score" class="comment-score"></div><div class="comment-text"><p>Can you post a link to the pages that you think are incorrect so they can be fixed?</p></div><div id="comment-38527-info" class="comment-info"><span class="comment-age">(11 Dec '14, 03:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="39391"></span><div id="comment-39391" class="comment"><div id="post-39391-score" class="comment-score"></div><div class="comment-text"><p>In this page, <a href="http://wiki.wireshark.org/CaptureSetup/WLAN#Turning_on_monitor_mode">http://wiki.wireshark.org/CaptureSetup/WLAN#Turning_on_monitor_mode</a> It says, 'there will be a "Monitor mode" checkbox in the Capture Options window in Wireshark'. There is no such checkbox in the Capture Options window, though the configuration summary in that window does indicate whether Monitor Mode is enabled or not (read only).</p></div><div id="comment-39391-info" class="comment-info"><span class="comment-age">(25 Jan '15, 14:45)</span> <span class="comment-user userinfo">bstriddy</span></div></div><span id="39392"></span><div id="comment-39392" class="comment"><div id="post-39392-score" class="comment-score"></div><div class="comment-text"><p>Also it seems that on MacOS 10.10.1 at least, one needs to exit and reopen Wireshark to get the Monitor Mode change to take effect.</p></div><div id="comment-39392-info" class="comment-info"><span class="comment-age">(25 Jan '15, 14:53)</span> <span class="comment-user userinfo">bstriddy</span></div></div><span id="39394"></span><div id="comment-39394" class="comment not_top_scorer"><div id="post-39394-score" class="comment-score"></div><div class="comment-text"><p>That's a bug, and I suspect it's not specific to Yosemite, as the code path at the OS X API layer for turning monitor mode on hasn't changed since Leopard (it gets turned on by requesting a non-Ethernet MAC layer header with the BIOCSDLT ioctl) and the libpcap APIs for it have been available in OS X since, I think, Snow Leopard. Please file a bug at <a href="http://bugs.wireshark.org/">the Wireshark bugzilla</a>.</p></div><div id="comment-39394-info" class="comment-info"><span class="comment-age">(25 Jan '15, 16:38)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="39398"></span><div id="comment-39398" class="comment not_top_scorer"><div id="post-39398-score" class="comment-score"></div><div class="comment-text"><p>LastBuild Qt5 (also does not require X11) <a href="https://www.wireshark.org/download/automated/osx/">https://www.wireshark.org/download/automated/osx/</a></p></div><div id="comment-39398-info" class="comment-info"><span class="comment-age">(26 Jan '15, 03:44)</span> <span class="comment-user userinfo">denji</span></div></div><span id="39420"></span><div id="comment-39420" class="comment not_top_scorer"><div id="post-39420-score" class="comment-score"></div><div class="comment-text"><blockquote><p>LastBuild Qt5</p></blockquote><p>Yes, turning on monitor mode doesn't appear to work with the Qt UI in OS X; it may not work on <em>any</em> OS. It does, however, work in the GTK+ UI, which is the current standard UI in current releases, so that's a separate problem from the one the person asked about in this question. File a <em>separate</em> bug on the Qt issue.</p></div><div id="comment-39420-info" class="comment-info"><span class="comment-age">(26 Jan '15, 21:44)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-26281" class="comment-tools"><span class="comments-showing"> showing 5 of 8 </span> <a href="#" class="show-all-comments-link">show 3 more comments</a></div><div class="clear"></div><div id="comment-26281-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

