+++
type = "question"
title = "value_string changeable?"
description = '''Can the value_string string be changed? Can we append information to make the Packet List Pane have the columns displayed with text information for custom columns?'''
date = "2011-09-23T05:34:00Z"
lastmod = "2011-09-26T23:34:00Z"
weight = 6511
keywords = [ "development" ]
aliases = [ "/questions/6511" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [value\_string changeable?](/questions/6511/value_string-changeable)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6511-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6511-score" class="post-score" title="current number of votes">0</div><span id="post-6511-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can the <code>value_string</code> string be changed? Can we append information to make the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketListPaneSection.html">Packet List Pane</a> have the columns displayed with text information for custom columns?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '11, 05:34</strong></p><img src="https://secure.gravatar.com/avatar/264adc05b644c1ab2d670b4773a12392?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flashkicker&#39;s gravatar image" /><p><span>flashkicker</span><br />
<span class="score" title="109 reputation points">109</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="19 badges"><span class="silver">●</span><span class="badgecount">19</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flashkicker has 5 accepted answers">41%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Sep '11, 15:28</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6511" class="comments-container"><span id="6540"></span><div id="comment-6540" class="comment"><div id="post-6540-score" class="comment-score">1</div><div class="comment-text"><p>I don't understand the question. To add values to value strings you need to recompile Wireshark unless the values are read from a file in which case you can edit that file. Any filterable field can be added as a custom column.</p></div><div id="comment-6540-info" class="comment-info"><span class="comment-age">(25 Sep '11, 01:04)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="6542"></span><div id="comment-6542" class="comment"><div id="post-6542-score" class="comment-score"></div><div class="comment-text"><p>If you think a value string is missing values write a bug report to have it updated.</p></div><div id="comment-6542-info" class="comment-info"><span class="comment-age">(25 Sep '11, 01:06)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="6579"></span><div id="comment-6579" class="comment"><div id="post-6579-score" class="comment-score"></div><div class="comment-text"><p>@Anders thanks......i wanted to append string and ya there is no issue with value_string function i just wanted to use it in a different way</p></div><div id="comment-6579-info" class="comment-info"><span class="comment-age">(26 Sep '11, 23:33)</span> <span class="comment-user userinfo">flashkicker</span></div></div></div><div id="comment-tools-6511" class="comment-tools"></div><div class="clear"></div><div id="comment-6511-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6580"></span>

<div id="answer-container-6580" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6580-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6580-score" class="post-score" title="current number of votes">0</div><span id="post-6580-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="flashkicker has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Its not possible to change the predefined values in value_string function ......</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Sep '11, 23:34</strong></p><img src="https://secure.gravatar.com/avatar/264adc05b644c1ab2d670b4773a12392?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flashkicker&#39;s gravatar image" /><p><span>flashkicker</span><br />
<span class="score" title="109 reputation points">109</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="19 badges"><span class="silver">●</span><span class="badgecount">19</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flashkicker has 5 accepted answers">41%</span></p></div></div><div id="comments-container-6580" class="comments-container"></div><div id="comment-tools-6580" class="comment-tools"></div><div class="clear"></div><div id="comment-6580-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

