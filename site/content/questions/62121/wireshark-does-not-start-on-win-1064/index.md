+++
type = "question"
title = "Wireshark does not start on Win 10/64"
description = '''I just installed Wireshark, 64b on my Windows 10 64 machine, restarted as requested, and when I press the executable the Wireshark just sits there &quot;Wait while Wireshark is initializing&quot; and based on processes does not consume any CPU. Any suggestions?'''
date = "2017-06-19T06:18:00Z"
lastmod = "2017-06-19T07:37:00Z"
weight = 62121
keywords = [ "not", "starting", "wireshark" ]
aliases = [ "/questions/62121" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark does not start on Win 10/64](/questions/62121/wireshark-does-not-start-on-win-1064)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62121-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62121-score" class="post-score" title="current number of votes">0</div><span id="post-62121-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed Wireshark, 64b on my Windows 10 64 machine, restarted as requested, and when I press the executable the Wireshark just sits there "Wait while Wireshark is initializing" and based on processes does not consume any CPU. Any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-starting" rel="tag" title="see questions tagged &#39;starting&#39;">starting</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '17, 06:18</strong></p><img src="https://secure.gravatar.com/avatar/02f395f6296955be35bf0b8282a9fb69?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mkajdos&#39;s gravatar image" /><p><span>mkajdos</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mkajdos has no accepted answers">0%</span></p></div></div><div id="comments-container-62121" class="comments-container"></div><div id="comment-tools-62121" class="comment-tools"></div><div class="clear"></div><div id="comment-62121-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62122"></span>

<div id="answer-container-62122" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62122-score" class="post-score" title="current number of votes">0</div><span id="post-62122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You may be encountering the same issue as others have reported with respect to USBPcap and extcap interfaces. In case this is so, you might want to refer to <a href="https://ask.wireshark.org/questions/48178/wireshark-fails-to-start-on-windows-10">https://ask.wireshark.org/questions/48178/wireshark-fails-to-start-on-windows-10</a>, as well as <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12845">Bug 12845</a> and <a href="https://ask.wireshark.org/questions/55394/wireshark-220-freezes-during-first-start-after-each-reboot">https://ask.wireshark.org/questions/55394/wireshark-220-freezes-during-first-start-after-each-reboot</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jun '17, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-62122" class="comments-container"></div><div id="comment-tools-62122" class="comment-tools"></div><div class="clear"></div><div id="comment-62122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

