+++
type = "question"
title = "troubleshoot the failure of a web page"
description = '''How can i use a protocol analyser such as wireshark to troubleshoot the failure of a webpage to download successfully to a browser on a computer'''
date = "2011-10-04T04:20:00Z"
lastmod = "2011-10-04T07:40:00Z"
weight = 6692
keywords = [ "troubleshooting" ]
aliases = [ "/questions/6692" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [troubleshoot the failure of a web page](/questions/6692/troubleshoot-the-failure-of-a-web-page)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6692-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6692-score" class="post-score" title="current number of votes">0</div><span id="post-6692-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i use a protocol analyser such as wireshark to troubleshoot the failure of a webpage to download successfully to a browser on a computer</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '11, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/af2b3a3c4791a109a1a6fc079d40ad5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samy&#39;s gravatar image" /><p><span>samy</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samy has no accepted answers">0%</span></p></div></div><div id="comments-container-6692" class="comments-container"></div><div id="comment-tools-6692" class="comment-tools"></div><div class="clear"></div><div id="comment-6692-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6703"></span>

<div id="answer-container-6703" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6703-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6703-score" class="post-score" title="current number of votes">0</div><span id="post-6703-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You're not very specific, so you could have a look <a href="http://wiki.wireshark.org/NetworkTroubleshooting/Overview">here</a>, and maybe find a presentation <a href="http://sharkfest.wireshark.org/retrospective.html">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Oct '11, 07:40</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6703" class="comments-container"></div><div id="comment-tools-6703" class="comment-tools"></div><div class="clear"></div><div id="comment-6703-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

