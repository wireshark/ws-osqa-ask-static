+++
type = "question"
title = "running capture causes packet duplication on Windows XP / Wireshark 1.2.9"
description = '''PC &#x27;A&#x27; is running Windows &amp;amp; Wireshark as per title; on the WiFi network interface (Broadcom) in promiscuous mode; PC B is connected to the same WiFi network If, from PC B I ping the IP address of PC A, I get duplicate ICMP responses received at PC B. The duplicates show up in the Wireshark trace...'''
date = "2013-06-16T08:56:00Z"
lastmod = "2013-06-16T10:11:00Z"
weight = 22106
keywords = [ "duplicates", "xp", "ping" ]
aliases = [ "/questions/22106" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [running capture causes packet duplication on Windows XP / Wireshark 1.2.9](/questions/22106/running-capture-causes-packet-duplication-on-windows-xp-wireshark-129)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22106-score" class="post-score" title="current number of votes">0</div><span id="post-22106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>PC 'A' is running Windows &amp; Wireshark as per title; on the WiFi network interface (Broadcom) in promiscuous mode; PC B is connected to the same WiFi network</p><p>If, from PC B I ping the IP address of PC A, I get duplicate ICMP responses received at PC B.</p><p>The duplicates show up in the Wireshark trace on PC A, ping stats at PC B; and also in a Wireshark trace on PC B if I also run Wireshark there.</p><p>If I stop the running capture on PC A, the duplicates do not happen.</p><p>If I ping B from A whilst the capture is running, no duplicates are seen.</p><p>Any ideas on what could be a cause of this ??</p><p>thanks...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-duplicates" rel="tag" title="see questions tagged &#39;duplicates&#39;">duplicates</span> <span class="post-tag tag-link-xp" rel="tag" title="see questions tagged &#39;xp&#39;">xp</span> <span class="post-tag tag-link-ping" rel="tag" title="see questions tagged &#39;ping&#39;">ping</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jun '13, 08:56</strong></p><img src="https://secure.gravatar.com/avatar/9cc3300882005b0c2c8bea416a276b64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="charlieS&#39;s gravatar image" /><p><span>charlieS</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="charlieS has no accepted answers">0%</span></p></div></div><div id="comments-container-22106" class="comments-container"><span id="22107"></span><div id="comment-22107" class="comment"><div id="post-22107-score" class="comment-score"></div><div class="comment-text"><p>Not likely to be the cause but are you really running Wireshark 1.2.9?, That is a seriously old version.</p></div><div id="comment-22107-info" class="comment-info"><span class="comment-age">(16 Jun '13, 10:11)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-22106" class="comment-tools"></div><div class="clear"></div><div id="comment-22106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

