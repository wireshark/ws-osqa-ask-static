+++
type = "question"
title = "What protocols does wireshark examine?"
description = '''What protocols does wireshark examine and at what layers?'''
date = "2014-02-20T16:22:00Z"
lastmod = "2014-02-21T13:21:00Z"
weight = 30061
keywords = [ "protocols" ]
aliases = [ "/questions/30061" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What protocols does wireshark examine?](/questions/30061/what-protocols-does-wireshark-examine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30061-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30061-score" class="post-score" title="current number of votes">0</div><span id="post-30061-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What protocols does wireshark examine and at what layers?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '14, 16:22</strong></p><img src="https://secure.gravatar.com/avatar/e6be32fed9ea5812e14d1ecc2b745aaa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BreakingBad&#39;s gravatar image" /><p><span>BreakingBad</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BreakingBad has no accepted answers">0%</span></p></div></div><div id="comments-container-30061" class="comments-container"></div><div id="comment-tools-30061" class="comment-tools"></div><div class="clear"></div><div id="comment-30061-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30062"></span>

<div id="answer-container-30062" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30062-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30062-score" class="post-score" title="current number of votes">1</div><span id="post-30062-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>What protocols does wireshark examine</p></blockquote><p><a href="http://www.wireshark.org/docs/dfref/">These</a>. Note that they have parenthetical notes after them indicating which particular versions of Wireshark handle the protocol in question; newer versions add newer protocols.</p><blockquote><p>and at what layers?</p></blockquote><p>OSI layers 2 through 7.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Feb '14, 16:50</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Feb '14, 13:21</strong> </span></p></div></div><div id="comments-container-30062" class="comments-container"><span id="30089"></span><div id="comment-30089" class="comment"><div id="post-30089-score" class="comment-score"></div><div class="comment-text"><p>The link you gave is messed up, at least for me. I think you meant this:</p><p><a href="http://www.wireshark.org/docs/dfref/">http://www.wireshark.org/docs/dfref/</a></p></div><div id="comment-30089-info" class="comment-info"><span class="comment-age">(21 Feb '14, 13:18)</span> <span class="comment-user userinfo">Hadriel</span></div></div><span id="30090"></span><div id="comment-30090" class="comment"><div id="post-30090-score" class="comment-score"></div><div class="comment-text"><p>Fixed link.</p></div><div id="comment-30090-info" class="comment-info"><span class="comment-age">(21 Feb '14, 13:21)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-30062" class="comment-tools"></div><div class="clear"></div><div id="comment-30062-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

