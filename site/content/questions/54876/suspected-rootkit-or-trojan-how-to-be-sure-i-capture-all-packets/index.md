+++
type = "question"
title = "Suspected rootkit or trojan, how to be sure I capture all packets?"
description = '''How can I be sure I capture all packets on a pc that likely has a rootkit or trojan. I know some rootkits are able to hide from what Wireshark can capture. I have an extra laptop, is it possible I run Wireshark on the laptop and somehow connect it to the pc to capture packets? '''
date = "2016-08-16T09:23:00Z"
lastmod = "2016-08-16T09:35:00Z"
weight = 54876
keywords = [ "rootkit" ]
aliases = [ "/questions/54876" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Suspected rootkit or trojan, how to be sure I capture all packets?](/questions/54876/suspected-rootkit-or-trojan-how-to-be-sure-i-capture-all-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54876-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54876-score" class="post-score" title="current number of votes">0</div><span id="post-54876-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I be sure I capture all packets on a pc that likely has a rootkit or trojan. I know some rootkits are able to hide from what Wireshark can capture. I have an extra laptop, is it possible I run Wireshark on the laptop and somehow connect it to the pc to capture packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rootkit" rel="tag" title="see questions tagged &#39;rootkit&#39;">rootkit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '16, 09:23</strong></p><img src="https://secure.gravatar.com/avatar/c5bfef8f8f5c73d1fd67d691f658ffff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Datura007&#39;s gravatar image" /><p><span>Datura007</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Datura007 has no accepted answers">0%</span></p></div></div><div id="comments-container-54876" class="comments-container"></div><div id="comment-tools-54876" class="comment-tools"></div><div class="clear"></div><div id="comment-54876-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54877"></span>

<div id="answer-container-54877" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54877-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54877-score" class="post-score" title="current number of votes">0</div><span id="post-54877-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, see <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">this page</a> for instruction on how to capture traffic of other machine than the one running Wireshark. But to be able to capture and to be able to understand the contents (or even identify the traffic for which the Trojan is responsible among all the other traffic) are different tasks.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Aug '16, 09:35</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-54877" class="comments-container"></div><div id="comment-tools-54877" class="comment-tools"></div><div class="clear"></div><div id="comment-54877-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

