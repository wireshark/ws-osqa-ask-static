+++
type = "question"
title = "Custom Wireshark dissector for Ethernet"
description = '''Hello All, I want to identify my custom frame based on &quot;Type&quot; field in the Ethernet and I want to dissect it. I was able to dissect the packets based on TCP/UDP port,but don&#x27;t know how to do it at Ethernet level. Also after dissecting the custom Ethernet frame I should be able to give control back t...'''
date = "2013-06-23T23:01:00Z"
lastmod = "2013-06-23T23:01:00Z"
weight = 22264
keywords = [ "ethernet", "frame", "dissector", "custom" ]
aliases = [ "/questions/22264" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Custom Wireshark dissector for Ethernet](/questions/22264/custom-wireshark-dissector-for-ethernet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22264-score" class="post-score" title="current number of votes">0</div><span id="post-22264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>I want to identify my custom frame based on "Type" field in the Ethernet and I want to dissect it. I was able to dissect the packets based on TCP/UDP port,but don't know how to do it at Ethernet level. Also after dissecting the custom Ethernet frame I should be able to give control back to Wireshark to dissect the content of Ethernet frame ie.IP-TCP and so on.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-custom" rel="tag" title="see questions tagged &#39;custom&#39;">custom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '13, 23:01</strong></p><img src="https://secure.gravatar.com/avatar/7e1217c1e0de07cc69c712c3ca00e33d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="surajmukade&#39;s gravatar image" /><p><span>surajmukade</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="surajmukade has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-22264" class="comments-container"></div><div id="comment-tools-22264" class="comment-tools"></div><div class="clear"></div><div id="comment-22264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

