+++
type = "question"
title = "In wireshark1.8.3.XP. the soft can&#x27;t remember the path which have just used."
description = '''In wireshark1.8.3.XP. the soft can&#x27;t remember the path which have just used. so the way to load a *.pcap are: 1,menu-&amp;gt;file-&amp;gt;open. 2,open button.(in the main interface) but I used to open a *.pcap through the remembered path direct (in the main interface). could you fix this . in version wiresh...'''
date = "2013-01-31T18:33:00Z"
lastmod = "2013-02-05T01:27:00Z"
weight = 18203
keywords = [ "load", "path", "remember", "file" ]
aliases = [ "/questions/18203" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [In wireshark1.8.3.XP. the soft can't remember the path which have just used.](/questions/18203/in-wireshark183xp-the-soft-cant-remember-the-path-which-have-just-used)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18203-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18203-score" class="post-score" title="current number of votes">0</div><span id="post-18203-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In wireshark1.8.3.XP. the soft can't remember the path which have just used.</p><p>so the way to load a *.pcap are: 1,menu-&gt;file-&gt;open. 2,open button.(in the main interface)</p><p>but I used to open a *.pcap through the remembered path direct (in the main interface).</p><p>could you fix this .</p><p>in version wireshark1.6.4, there is no this problem.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-load" rel="tag" title="see questions tagged &#39;load&#39;">load</span> <span class="post-tag tag-link-path" rel="tag" title="see questions tagged &#39;path&#39;">path</span> <span class="post-tag tag-link-remember" rel="tag" title="see questions tagged &#39;remember&#39;">remember</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '13, 18:33</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-18203" class="comments-container"></div><div id="comment-tools-18203" class="comment-tools"></div><div class="clear"></div><div id="comment-18203-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18226"></span>

<div id="answer-container-18226" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18226-score" class="post-score" title="current number of votes">0</div><span id="post-18226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just tried it in 1.8.4 (on Windows XP) and it worked fine: to open a file I went to the directory where I keep many capture files and opened a file. I closed the file then did File-&gt;Open again; the Open File dialog was already in the last directory I used.</p><p>There is a preference which controls this behavior: in the Preferences screen in the "User Interface" tab there's a choice between "remember last directory" and "always start in"; maybe your preference is set to the latter?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '13, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-18226" class="comments-container"><span id="18307"></span><div id="comment-18307" class="comment"><div id="post-18307-score" class="comment-score"></div><div class="comment-text"><p>In 1.8.5,this problem have been fixed. Thanks!</p></div><div id="comment-18307-info" class="comment-info"><span class="comment-age">(05 Feb '13, 01:27)</span> <span class="comment-user userinfo">smilezuzu</span></div></div></div><div id="comment-tools-18226" class="comment-tools"></div><div class="clear"></div><div id="comment-18226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

