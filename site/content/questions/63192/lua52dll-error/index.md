+++
type = "question"
title = "lua52.dll error"
description = '''Hello, If the executable file path contains Hangul (maybe not ansi), lua52.dll error occurs. What can I do to solve this problem?'''
date = "2017-07-27T21:49:00Z"
lastmod = "2017-08-08T08:32:00Z"
weight = 63192
keywords = [ "lua52", "error" ]
aliases = [ "/questions/63192" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [lua52.dll error](/questions/63192/lua52dll-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63192-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63192-score" class="post-score" title="current number of votes">0</div><span id="post-63192-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, If the executable file path contains Hangul (maybe not ansi), lua52.dll error occurs. What can I do to solve this problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua52" rel="tag" title="see questions tagged &#39;lua52&#39;">lua52</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '17, 21:49</strong></p><img src="https://secure.gravatar.com/avatar/9d29e6b2cf874fb9e99b1aa9ff5cc053?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chsk0510&#39;s gravatar image" /><p><span>chsk0510</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chsk0510 has no accepted answers">0%</span></p></div></div><div id="comments-container-63192" class="comments-container"></div><div id="comment-tools-63192" class="comment-tools"></div><div class="clear"></div><div id="comment-63192-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63450"></span>

<div id="answer-container-63450" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63450-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63450-score" class="post-score" title="current number of votes">0</div><span id="post-63450-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like we have another non-ASCII-filename problem. I'd suggest <a href="https://bugs.wireshark.org">opening a bug report</a> so someone can fix it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Aug '17, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-63450" class="comments-container"></div><div id="comment-tools-63450" class="comment-tools"></div><div class="clear"></div><div id="comment-63450-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

