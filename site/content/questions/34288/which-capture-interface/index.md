+++
type = "question"
title = "Which capture interface?"
description = '''I want to get the IP of my friend by talking to him via voice, which capture interface do i use? We&#x27;ve tried all and only at 1 it shows up something (en1) the rest says &quot;No packets captured!&quot;. I&#x27;m using mac and my friend is using windows. We tried over Skype, steam and teamspeak 3.'''
date = "2014-06-30T07:06:00Z"
lastmod = "2014-07-01T01:39:00Z"
weight = 34288
keywords = [ "interface", "packets" ]
aliases = [ "/questions/34288" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Which capture interface?](/questions/34288/which-capture-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34288-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34288-score" class="post-score" title="current number of votes">0</div><span id="post-34288-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to get the IP of my friend by talking to him via voice, which capture interface do i use? We've tried all and only at 1 it shows up something (en1) the rest says "No packets captured!". I'm using mac and my friend is using windows. We tried over Skype, steam and teamspeak 3.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '14, 07:06</strong></p><img src="https://secure.gravatar.com/avatar/f2cdd8c3c07da23002c5991c4a1b0ef5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Obsman&#39;s gravatar image" /><p><span>Obsman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Obsman has no accepted answers">0%</span></p></div></div><div id="comments-container-34288" class="comments-container"><span id="34299"></span><div id="comment-34299" class="comment"><div id="post-34299-score" class="comment-score"></div><div class="comment-text"><p>If you go to System Preferences on your machine and click "Network", what network interfaces does it show, and which of them are listed as "Connected"?</p></div><div id="comment-34299-info" class="comment-info"><span class="comment-age">(30 Jun '14, 13:51)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-34288" class="comment-tools"></div><div class="clear"></div><div id="comment-34288-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34308"></span>

<div id="answer-container-34308" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34308-score" class="post-score" title="current number of votes">0</div><span id="post-34308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm not entirely certain, but I believe some of these voice applications (like most multiplayer games) route the packets via a central server, i.e. you and your friend connect to that server and there isn't a direct connection between the two of you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jul '14, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-34308" class="comments-container"></div><div id="comment-tools-34308" class="comment-tools"></div><div class="clear"></div><div id="comment-34308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

