+++
type = "question"
title = "How to increase the number of bytes per line in frame section ?"
description = '''How to increase the number of bytes per line in frame section ?'''
date = "2016-07-10T15:39:00Z"
lastmod = "2016-07-10T17:39:00Z"
weight = 53973
keywords = [ "layout" ]
aliases = [ "/questions/53973" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to increase the number of bytes per line in frame section ?](/questions/53973/how-to-increase-the-number-of-bytes-per-line-in-frame-section)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53973-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53973-score" class="post-score" title="current number of votes">0</div><span id="post-53973-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to increase the number of bytes per line in frame section ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-layout" rel="tag" title="see questions tagged &#39;layout&#39;">layout</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '16, 15:39</strong></p><img src="https://secure.gravatar.com/avatar/a00dee8f9775230dee2d5db97e3855c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="user370&#39;s gravatar image" /><p><span>user370</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="user370 has no accepted answers">0%</span></p></div></div><div id="comments-container-53973" class="comments-container"></div><div id="comment-tools-53973" class="comment-tools"></div><div class="clear"></div><div id="comment-53973-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53974"></span>

<div id="answer-container-53974" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53974-score" class="post-score" title="current number of votes">0</div><span id="post-53974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm assuming that by "the frame section" you mean the hex/ASCII dump pane which is, by default, the bottommost pane of the display.</p><p>If so, then the answer is "edit the source code for the hex dump pane and recompile." :-) There's no preference to change it to any value other than 16 bytes per line; presumably you have a <em>very</em> wide display that could handle more bytes.</p><p>Why is this an issue? Perhaps there are other ways to solve the problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jul '16, 17:39</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-53974" class="comments-container"></div><div id="comment-tools-53974" class="comment-tools"></div><div class="clear"></div><div id="comment-53974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

