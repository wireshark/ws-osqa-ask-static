+++
type = "question"
title = "Basic doubt"
description = '''Hi there, Basically wireshark gives us the information in terms of packets only when we are connected to internet. Then how is this information used to crack wifi password to which we are not connected'''
date = "2015-12-08T16:18:00Z"
lastmod = "2015-12-15T01:32:00Z"
weight = 48368
keywords = [ "wifi", "hack" ]
aliases = [ "/questions/48368" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Basic doubt](/questions/48368/basic-doubt)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48368-score" class="post-score" title="current number of votes">0</div><span id="post-48368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there, Basically wireshark gives us the information in terms of packets only when we are connected to internet. Then how is this information used to crack wifi password to which we are not connected</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-hack" rel="tag" title="see questions tagged &#39;hack&#39;">hack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '15, 16:18</strong></p><img src="https://secure.gravatar.com/avatar/828aa4517259a0bdbea71bf386e8d77d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lcwld&#39;s gravatar image" /><p><span>Lcwld</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lcwld has no accepted answers">0%</span></p></div></div><div id="comments-container-48368" class="comments-container"><span id="48430"></span><div id="comment-48430" class="comment"><div id="post-48430-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Basically wireshark gives us the information in terms of packets only when we are connected to internet.</p></blockquote><p>No. Wireshark will work fine with <em>any</em> network connection, even if it's a local connection between two machines <em>neither</em> of which are connected to the Internet.</p><blockquote><p>Then how is this information used to crack wifi password to which we are not connected</p></blockquote><p>You can't be connected to a password, so you can't be "not connected" to a password. You can be connected to a Wi-Fi network, so you can also be "not connected" to a Wi-Fi network. Are you talking about cracking Wi-Fi passwords of networks to which you're not connected? Or are you talking about being connected to a Wi-Fi network, and cracking passwords on that network, when that network is not itself connected to the Internet?</p></div><div id="comment-48430-info" class="comment-info"><span class="comment-age">(10 Dec '15, 12:30)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="48436"></span><div id="comment-48436" class="comment"><div id="post-48436-score" class="comment-score"></div><div class="comment-text"><p>yeah I am talking about cracking Wi-Fi password of network to which Iam not connected</p></div><div id="comment-48436-info" class="comment-info"><span class="comment-age">(10 Dec '15, 15:14)</span> <span class="comment-user userinfo">Lcwld</span></div></div></div><div id="comment-tools-48368" class="comment-tools"></div><div class="clear"></div><div id="comment-48368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="48369"></span>

<div id="answer-container-48369" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48369-score" class="post-score" title="current number of votes">1</div><span id="post-48369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is not a password cracker, nor is it intended as such. Its purpose is to collect and analyze packet and frame level protocols. If you want to explore analysis of WiFi passwords, you may want to look at something like Kismet at <a href="https://kismetwireless.net/">https://kismetwireless.net/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '15, 17:24</strong></p><img src="https://secure.gravatar.com/avatar/b260fb38b621169269b5030f1ed6b766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griff&#39;s gravatar image" /><p><span>griff</span><br />
<span class="score" title="361 reputation points">361</span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griff has 2 accepted answers">10%</span></p></div></div><div id="comments-container-48369" class="comments-container"><span id="48385"></span><div id="comment-48385" class="comment"><div id="post-48385-score" class="comment-score"></div><div class="comment-text"><p>Hi, Thanks for spending your time in reading my question and answering it. It seems that you misunderstood my question, My aim is not to crack wifi. I want to find out whether we will get the information packet of some wifi connection even when i am not connected to internet? For Ex: Let us say that I am connected to internet via ethernet then will I get information packets of some other wifi whose password I don't know and I am not connected to it. Thanks in advance</p></div><div id="comment-48385-info" class="comment-info"><span class="comment-age">(09 Dec '15, 10:55)</span> <span class="comment-user userinfo">Lcwld</span></div></div></div><div id="comment-tools-48369" class="comment-tools"></div><div class="clear"></div><div id="comment-48369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="48513"></span>

<div id="answer-container-48513" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48513-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48513-score" class="post-score" title="current number of votes">0</div><span id="post-48513-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll get the packets of the wireless channel you are able to receive and are currently tracing. If you are connected to LAN and trace with Wireshark on WiFi, you'll see WiFi packets. (you will however require monitor mode and likely promiscuous mode as well, so not from Windows)</p><p>If you are connected to a LAN, and someone else is connected to your LAN via WiFi, then the WiFi router will strip out the 802 frame headers and send the information you you via normal Ethernet TCP. As long as you are capturing the incoming traffic, the packets are meant for you or have a HUB, yes you can. Nothing special required.</p><p>If you are connected to your LAN and your neighbour is connected to his WiFi; no, You wont and it is very likely highly illegal to try. If you are not authenticated to the WiFi you are capturing you'll get encrypted garbage unless the WiFi owner is an idiot (or in a funny mood and way cleverer than you)</p><p>I'm not entirely sure where you are going with this, but if you need to ask, don't go there.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Dec '15, 01:32</strong></p><img src="https://secure.gravatar.com/avatar/05ba95262a3352e3af4ba69c0ec0dff2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DarrenWright&#39;s gravatar image" /><p><span>DarrenWright</span><br />
<span class="score" title="216 reputation points">216</span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DarrenWright has 5 accepted answers">26%</span></p></div></div><div id="comments-container-48513" class="comments-container"></div><div id="comment-tools-48513" class="comment-tools"></div><div class="clear"></div><div id="comment-48513-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

