+++
type = "question"
title = "how to sniff mpeg-dash video stream?"
description = '''I have to sniff a MPEG-DASH video stream from youtube. How can I do it using Wireshark? and the most important thing is: anyone can tell how to obtain the MPD (Media Presentation Description) file?'''
date = "2015-02-18T10:10:00Z"
lastmod = "2015-02-18T10:10:00Z"
weight = 39933
keywords = [ "dash", "mpeg", "wireshark" ]
aliases = [ "/questions/39933" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to sniff mpeg-dash video stream?](/questions/39933/how-to-sniff-mpeg-dash-video-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39933-score" class="post-score" title="current number of votes">0</div><span id="post-39933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have to sniff a MPEG-DASH video stream from youtube. How can I do it using Wireshark?</p><p>and the most important thing is: anyone can tell how to obtain the MPD (Media Presentation Description) file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dash" rel="tag" title="see questions tagged &#39;dash&#39;">dash</span> <span class="post-tag tag-link-mpeg" rel="tag" title="see questions tagged &#39;mpeg&#39;">mpeg</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '15, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/b71d80c756618ab00376dc30b72c501a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="surfRock&#39;s gravatar image" /><p><span>surfRock</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="surfRock has no accepted answers">0%</span></p></div></div><div id="comments-container-39933" class="comments-container"></div><div id="comment-tools-39933" class="comment-tools"></div><div class="clear"></div><div id="comment-39933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

