+++
type = "question"
title = "Wireshark Parameter"
description = '''Hello, I&#x27;m working on a field task from school. I&#x27;m looking for the value of this parameter in wireshark: end-to-end delay, throughput, packet loss, and jitter. I&#x27;ve done a couple of scenarios. 1. I send a file from one node (192.168.100.1) to another node (192.168.100.6) using file transfer softwar...'''
date = "2014-11-17T09:40:00Z"
lastmod = "2014-11-17T09:40:00Z"
weight = 37903
keywords = [ "filter", "parameters" ]
aliases = [ "/questions/37903" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Parameter](/questions/37903/wireshark-parameter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37903-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37903-score" class="post-score" title="current number of votes">0</div><span id="post-37903-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I'm working on a field task from school.</p><p>I'm looking for the value of this parameter in wireshark: end-to-end delay, throughput, packet loss, and jitter.</p><p>I've done a couple of scenarios. 1. I send a file from one node (192.168.100.1) to another node (192.168.100.6) using file transfer software in the LAN. 2. I'm broadcasting (192.168.100.1) video via VLC using RTSP protocol and the one of the other node (192.168.100.6) streams that video.</p><p>network type : mesh network routing protocol : OLSR there is no internet connection in the network</p><p>Regards, aikaza</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-parameters" rel="tag" title="see questions tagged &#39;parameters&#39;">parameters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '14, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/e5f4e771b25d4df0b74c7a1c9deeef05?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aikaza&#39;s gravatar image" /><p><span>aikaza</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aikaza has no accepted answers">0%</span></p></div></div><div id="comments-container-37903" class="comments-container"></div><div id="comment-tools-37903" class="comment-tools"></div><div class="clear"></div><div id="comment-37903-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

