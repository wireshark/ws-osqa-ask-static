+++
type = "question"
title = "Usefull Color Rules?"
description = '''What are your favorite or most useful color rules? Besides the included defaults, what rules are most useful in troubleshooting and quickly identifying issues? '''
date = "2010-09-13T06:04:00Z"
lastmod = "2014-01-12T07:39:00Z"
weight = 42
keywords = [ "color-rules", "gui", "packet-display", "troubleshooting" ]
aliases = [ "/questions/42" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Usefull Color Rules?](/questions/42/usefull-color-rules)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42-score" class="post-score" title="current number of votes">2</div><span id="post-42-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>What are your favorite or most useful color rules?</p><p>Besides the included defaults, what rules are most useful in troubleshooting and quickly identifying issues?</p><p><img src="http://imgur.com/voSIK.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color-rules" rel="tag" title="see questions tagged &#39;color-rules&#39;">color-rules</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-packet-display" rel="tag" title="see questions tagged &#39;packet-display&#39;">packet-display</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '10, 06:04</strong></p><img src="https://secure.gravatar.com/avatar/1d8eda08758411bec29092a0b8220126?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Peter&#39;s gravatar image" /><p><span>Peter</span><br />
<span class="score" title="65 reputation points">65</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Peter has no accepted answers">0%</span></p></img></div></div><div id="comments-container-42" class="comments-container"><span id="28822"></span><div id="comment-28822" class="comment"><div id="post-28822-score" class="comment-score"></div><div class="comment-text"><p>A screnshot of the profile would be a good thing to have in here. Thanks, A Friend From Portugal, Mr_Chmod</p></div><div id="comment-28822-info" class="comment-info"><span class="comment-age">(12 Jan '14, 07:39)</span> <span class="comment-user userinfo">mrchmod</span></div></div></div><div id="comment-tools-42" class="comment-tools"></div><div class="clear"></div><div id="comment-42-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52"></span>

<div id="answer-container-52" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52-score" class="post-score" title="current number of votes">2</div><span id="post-52-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Peter has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well - I'd change the TCP small window shown to "&lt; 1460" on most networks... I'd move it (and most of the ones listed following this) above Bad TCP as well. I know Wireshark will place the Expert info in the Packet List Info column, so it's easy to see - I like the coloring to catch my eye.</p><p>We just opened up registration for a free course on Troubleshooting with Coloring Rules - www.chappellseminars.com/s-wiresharkcolors.html - October 19th. We announce it tomorrow during the Filtering course.</p><p>Adding coloring rules for HTTP error codes, SIP error codes, DNS error responses, 4 NOPs in a row in TCP options, DHCP declines, DNS replies with greater than 5 responses, large delta times in diplayed packets... many many ideas. I have some coloring rules in the profiles downloads at www.wiresharkbook.com as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '10, 00:07</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-52" class="comments-container"></div><div id="comment-tools-52" class="comment-tools"></div><div class="clear"></div><div id="comment-52-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

