+++
type = "question"
title = "need info on Wireshark data"
description = '''I have created a monitor interface on an atheros card (AR922X) which is part of a separate laptop with ath9k driver. I am using wireshark to monitor the traffic in the channel through this monitor interface. I have another laptop connected to an AP in the same channel and the laptop is downloading a...'''
date = "2012-03-07T14:01:00Z"
lastmod = "2012-03-07T14:01:00Z"
weight = 9422
keywords = [ "ath9k", "traffic", "monitor" ]
aliases = [ "/questions/9422" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [need info on Wireshark data](/questions/9422/need-info-on-wireshark-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9422-score" class="post-score" title="current number of votes">0</div><span id="post-9422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have created a monitor interface on an atheros card (AR922X) which is part of a separate laptop with ath9k driver. I am using wireshark to monitor the traffic in the channel through this monitor interface. I have another laptop connected to an AP in the same channel and the laptop is downloading a huge file(say a linux ISO). I get QoS data packets of size greater than 1500. Is it the actual data that is being downloaded? I tried having two devices monitor the same traffic. For a specific SN, I am getting different data on both the machines but of same length. Does this mean that this is not the downloaded data? How do i get the actual data that is part of download? To be more specific, I tried generating tcpdumps and opening them through wireshark, I am getting the same type of data as has been mentioned. Can anyone suggest what I might be missing as part of configuration? I am making sure that the flags fcsfail and control are also set. I am able to monitor LLC protocol data from other machines but this file download. It will be useful for us to know what I am missing as part of configuration.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ath9k" rel="tag" title="see questions tagged &#39;ath9k&#39;">ath9k</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Mar '12, 14:01</strong></p><img src="https://secure.gravatar.com/avatar/2618453e9194929f291128d350b2a4ec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srini_wisc&#39;s gravatar image" /><p><span>srini_wisc</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srini_wisc has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Mar '12, 14:17</strong> </span></p></div></div><div id="comments-container-9422" class="comments-container"></div><div id="comment-tools-9422" class="comment-tools"></div><div class="clear"></div><div id="comment-9422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

