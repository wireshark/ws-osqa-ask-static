+++
type = "question"
title = "Does Wireshark have sFlow source_id type 3 (HTTP) dissector support?"
description = '''Hello guys, I&#x27;m having issues decoding sflow packets that use the host extension and http structure. I&#x27;m using 2.0.2 Does anyone know if there is dissector support for those extensions to standard sFlow? Thanks!'''
date = "2016-03-24T10:53:00Z"
lastmod = "2016-03-24T10:53:00Z"
weight = 51159
keywords = [ "sflow", "dissectors" ]
aliases = [ "/questions/51159" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Does Wireshark have sFlow source\_id type 3 (HTTP) dissector support?](/questions/51159/does-wireshark-have-sflow-source_id-type-3-http-dissector-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51159-score" class="post-score" title="current number of votes">0</div><span id="post-51159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello guys,</p><p>I'm having issues decoding sflow packets that use the <a href="http://www.sflow.org/sflow_host.txt">host extension</a> and <a href="http://www.sflow.org/sflow_http.txt">http structure</a>.</p><p>I'm using 2.0.2</p><p>Does anyone know if there is dissector support for those extensions to standard sFlow?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sflow" rel="tag" title="see questions tagged &#39;sflow&#39;">sflow</span> <span class="post-tag tag-link-dissectors" rel="tag" title="see questions tagged &#39;dissectors&#39;">dissectors</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '16, 10:53</strong></p><img src="https://secure.gravatar.com/avatar/658548a96fbad9a2dfc0d21aea56d915?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Clarkington&#39;s gravatar image" /><p><span>Clarkington</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Clarkington has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Mar '16, 11:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51159" class="comments-container"></div><div id="comment-tools-51159" class="comment-tools"></div><div class="clear"></div><div id="comment-51159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

