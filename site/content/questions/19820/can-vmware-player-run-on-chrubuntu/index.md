+++
type = "question"
title = "can vmware player run on chrubuntu"
description = '''Hi, Does anyone know if VMware Player can run on an Acer A7 running Chrubuntu? (The kernal module updater is looking for files and maybe I just don&#x27;t know the path?)'''
date = "2013-03-25T12:06:00Z"
lastmod = "2013-03-25T12:14:00Z"
weight = 19820
keywords = [ "chrubuntu", "vmware" ]
aliases = [ "/questions/19820" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can vmware player run on chrubuntu](/questions/19820/can-vmware-player-run-on-chrubuntu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19820-score" class="post-score" title="current number of votes">-2</div><span id="post-19820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Does anyone know if VMware Player can run on an Acer A7 running Chrubuntu?</p><p>(The kernal module updater is looking for files and maybe I just don't know the path?)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-chrubuntu" rel="tag" title="see questions tagged &#39;chrubuntu&#39;">chrubuntu</span> <span class="post-tag tag-link-vmware" rel="tag" title="see questions tagged &#39;vmware&#39;">vmware</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '13, 12:06</strong></p><img src="https://secure.gravatar.com/avatar/f4d59505cc45e2a2b6cb571fa2a1eb23?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dan%20Danville&#39;s gravatar image" /><p><span>Dan Danville</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dan Danville has no accepted answers">0%</span></p></div></div><div id="comments-container-19820" class="comments-container"><span id="19821"></span><div id="comment-19821" class="comment"><div id="post-19821-score" class="comment-score">3</div><div class="comment-text"><p>How exactly is this a wireshark related question?</p><p>I think <a href="http://superuser.com/">http://superuser.com/</a> would be a more appropriate place to ask your question...</p></div><div id="comment-19821-info" class="comment-info"><span class="comment-age">(25 Mar '13, 12:14)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-19820" class="comment-tools"></div><div class="clear"></div><div id="comment-19820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

