+++
type = "question"
title = "network design"
description = '''Hello everyone, I am told to make a network and I am bumping into some difficulty in the design.  I am told to make a IP plan simply put it&#x27;s a long list with all the numbers involved in the network, so far I have everything written down I just can&#x27;t figure out what to do with the router.  this is t...'''
date = "2012-12-03T06:31:00Z"
lastmod = "2012-12-03T06:31:00Z"
weight = 16501
keywords = [ "design", "network", "physical" ]
aliases = [ "/questions/16501" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [network design](/questions/16501/network-design)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16501-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16501-score" class="post-score" title="current number of votes">0</div><span id="post-16501-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone, I am told to make a network and I am bumping into some difficulty in the design. I am told to make a IP plan simply put it's a long list with all the numbers involved in the network, so far I have everything written down I just can't figure out what to do with the router. <img src="https://osqa-ask.wireshark.org/upfiles/network.jpg" alt="physical network drawing" /></p><p>this is the physical drawing I made of my network. But I am not sure if this works and why it would work or would not work. I have been told by my mentor to find out what a router does and what and I have concluded is that "a router sends network packages to the appropriate place/device within the network" but that was not a valid answer and he wondered off rambling. So what should I improve in my physical network drawing and why? What does a router do (in simple terms)? and what do I need to document about it(IP address? gateway?)? The database server and the webserver use a peer to peer connection to communicate with each other. Sorry about the stupid / easy question but I just can't figure out what I need to do.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-design" rel="tag" title="see questions tagged &#39;design&#39;">design</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-physical" rel="tag" title="see questions tagged &#39;physical&#39;">physical</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Dec '12, 06:31</strong></p><img src="https://secure.gravatar.com/avatar/272a26870ac53d05babf7bc61da6eeab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="elexion&#39;s gravatar image" /><p><span>elexion</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="elexion has no accepted answers">0%</span></p></img></div></div><div id="comments-container-16501" class="comments-container"></div><div id="comment-tools-16501" class="comment-tools"></div><div class="clear"></div><div id="comment-16501-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

