+++
type = "question"
title = "+HTC in Wireless"
description = '''How do you filter +HTC the HT control field in wireless environment?'''
date = "2015-02-17T04:49:00Z"
lastmod = "2015-02-17T04:49:00Z"
weight = 39912
keywords = [ "+htc" ]
aliases = [ "/questions/39912" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [+HTC in Wireless](/questions/39912/htc-in-wireless)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39912-score" class="post-score" title="current number of votes">0</div><span id="post-39912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do you filter +HTC the HT control field in wireless environment?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-+htc" rel="tag" title="see questions tagged &#39;+htc&#39;">+htc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '15, 04:49</strong></p><img src="https://secure.gravatar.com/avatar/b7703bc29a62b1f2be494463dae4ce36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dfc&#39;s gravatar image" /><p><span>dfc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dfc has no accepted answers">0%</span></p></div></div><div id="comments-container-39912" class="comments-container"></div><div id="comment-tools-39912" class="comment-tools"></div><div class="clear"></div><div id="comment-39912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

