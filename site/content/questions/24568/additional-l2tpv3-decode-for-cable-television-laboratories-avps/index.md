+++
type = "question"
title = "Additional L2TPv3 decode for Cable Television Laboratories AVPs"
description = '''Document &quot;CM-SP-DEPI-I08-100611.pdf&quot;, which is available from: link text Defines most AVPs used in the LTTPv3 SLI messages. It would be great if Wireshark could do a parse of these messages instead of indicating &quot;Vendor-Specific AVP&quot;.'''
date = "2013-09-11T05:58:00Z"
lastmod = "2013-09-11T07:01:00Z"
weight = 24568
keywords = [ "parsing" ]
aliases = [ "/questions/24568" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Additional L2TPv3 decode for Cable Television Laboratories AVPs](/questions/24568/additional-l2tpv3-decode-for-cable-television-laboratories-avps)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24568-score" class="post-score" title="current number of votes">0</div><span id="post-24568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Document "CM-SP-DEPI-I08-100611.pdf", which is available from: <a href="http://www.cablelabs.com/specifications/CM-SP-DEPI-I08-100611.pdf">link text</a></p><p>Defines most AVPs used in the LTTPv3 SLI messages. It would be great if Wireshark could do a parse of these messages instead of indicating "Vendor-Specific AVP".</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-parsing" rel="tag" title="see questions tagged &#39;parsing&#39;">parsing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '13, 05:58</strong></p><img src="https://secure.gravatar.com/avatar/aa5b7e6223e1b254463b99aa8669de80?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dave%20C&#39;s gravatar image" /><p><span>Dave C</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dave C has no accepted answers">0%</span></p></div></div><div id="comments-container-24568" class="comments-container"></div><div id="comment-tools-24568" class="comment-tools"></div><div class="clear"></div><div id="comment-24568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24572"></span>

<div id="answer-container-24572" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24572-score" class="post-score" title="current number of votes">1</div><span id="post-24572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Raise an enhancment bug including a example trace and a link to the specification.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Sep '13, 06:34</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-24572" class="comments-container"><span id="24576"></span><div id="comment-24576" class="comment"><div id="post-24576-score" class="comment-score"></div><div class="comment-text"><p>Thanks! Bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=9133">9133</a> created as an enhancement request.</p></div><div id="comment-24576-info" class="comment-info"><span class="comment-age">(11 Sep '13, 07:01)</span> <span class="comment-user userinfo">Dave C</span></div></div></div><div id="comment-tools-24572" class="comment-tools"></div><div class="clear"></div><div id="comment-24572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

