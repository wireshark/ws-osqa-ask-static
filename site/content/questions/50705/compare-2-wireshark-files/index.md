+++
type = "question"
title = "compare 2 wireshark files"
description = '''Hi guys , I want to compare 2 capture files for example : 1) client 2) server I found this url https://www.wireshark.org/docs/wsug_html_chunked/ChStatCompareCaptureFiles.html But I&#x27;m not able to open compare window , my wireshark version is Version 2.0.2. Thanks in advance for any help. Giovanni'''
date = "2016-03-03T08:25:00Z"
lastmod = "2016-03-04T11:53:00Z"
weight = 50705
keywords = [ "wireshark" ]
aliases = [ "/questions/50705" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [compare 2 wireshark files](/questions/50705/compare-2-wireshark-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50705-score" class="post-score" title="current number of votes">0</div><span id="post-50705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys , I want to compare 2 capture files for example :</p><p>1) client</p><p>2) server</p><p>I found this url <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChStatCompareCaptureFiles.html">https://www.wireshark.org/docs/wsug_html_chunked/ChStatCompareCaptureFiles.html</a></p><p>But I'm not able to open compare window , my wireshark version is Version 2.0.2.</p><p>Thanks in advance for any help.</p><p>Giovanni</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '16, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/5e2c00249f5580aee311d17b77fba138?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gcasale&#39;s gravatar image" /><p><span>gcasale</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gcasale has no accepted answers">0%</span></p></div></div><div id="comments-container-50705" class="comments-container"></div><div id="comment-tools-50705" class="comment-tools"></div><div class="clear"></div><div id="comment-50705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50706"></span>

<div id="answer-container-50706" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50706-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50706-score" class="post-score" title="current number of votes">1</div><span id="post-50706-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll have to use the legacy (GTK+) version for that, assuming you're not running on OSX. If you are using OSX, then that isn't implemented yet in the Qt version, which is the only (current) version available for your OS.</p><p>The state of menu items and features for the Qt version can be seen <a href="https://wiki.wireshark.org/Development/QtShark">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Mar '16, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50706" class="comments-container"><span id="50708"></span><div id="comment-50708" class="comment"><div id="post-50708-score" class="comment-score"></div><div class="comment-text"><p>I'm running wireshark from windows 7 64 bit.Can you post the path to open the compare window? Thanks Giovanni</p></div><div id="comment-50708-info" class="comment-info"><span class="comment-age">(03 Mar '16, 09:29)</span> <span class="comment-user userinfo">gcasale</span></div></div><span id="50709"></span><div id="comment-50709" class="comment"><div id="post-50709-score" class="comment-score"></div><div class="comment-text"><p>Ok, first you'll have to run the legacy version, you should find it in your start menu as "Wireshark Legacy", then under the Statistics menu it's listed as "Compare...".</p></div><div id="comment-50709-info" class="comment-info"><span class="comment-age">(03 Mar '16, 09:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="50724"></span><div id="comment-50724" class="comment"><div id="post-50724-score" class="comment-score"></div><div class="comment-text"><p>Thanks i found it :)</p></div><div id="comment-50724-info" class="comment-info"><span class="comment-age">(04 Mar '16, 11:53)</span> <span class="comment-user userinfo">gcasale</span></div></div></div><div id="comment-tools-50706" class="comment-tools"></div><div class="clear"></div><div id="comment-50706-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

