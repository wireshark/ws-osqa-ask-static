+++
type = "question"
title = "interface problem"
description = '''I am using wireshark in windows 7. While opening wireshark it is not showing the interfaces for capturing the packets. I did troubleshoot, for the first time it had shown the interfaces and I was able to capture packet, but when reopened again no interface and showing Error = 0;this &quot;can&#x27;t happen&quot;. ...'''
date = "2012-10-13T08:57:00Z"
lastmod = "2012-10-14T07:25:00Z"
weight = 14987
keywords = [ "interface", "windows7" ]
aliases = [ "/questions/14987" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [interface problem](/questions/14987/interface-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14987-score" class="post-score" title="current number of votes">0</div><span id="post-14987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using wireshark in windows 7. While opening wireshark it is not showing the interfaces for capturing the packets. I did troubleshoot, for the first time it had shown the interfaces and I was able to capture packet, but when reopened again no interface and showing Error = 0;this "can't happen". or showing that- No interface can be used for capturing in this system with the current configuration. See Capture Help below for details.</p><p>Is there any solution for this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '12, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/4c2c54d30f59942e9ac38346bc966eb3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Parvathy&#39;s gravatar image" /><p><span>Parvathy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Parvathy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Oct '12, 09:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-14987" class="comments-container"></div><div id="comment-tools-14987" class="comment-tools"></div><div class="clear"></div><div id="comment-14987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14988"></span>

<div id="answer-container-14988" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14988-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14988-score" class="post-score" title="current number of votes">0</div><span id="post-14988-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This comes up a lot, you can use the search button to look for similar questions, such as this one: <a href="http://ask.wireshark.org/questions/1281/npf-driver-problem-in-windows-7">NPF Driver problem in Windows</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Oct '12, 09:47</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-14988" class="comments-container"><span id="14999"></span><div id="comment-14999" class="comment"><div id="post-14999-score" class="comment-score"></div><div class="comment-text"><p>the problem is, that the search function does not work properly. See my latest comment in bug 7223 from july.</p><blockquote><p><code>https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7223#c4</code></p></blockquote><p>So, even if people use the search function, they won't be able to find the relevant answers.</p><p>Try it yourself:</p><p>Search for: <strong>npf</strong> driver problem -&gt; 3 questions<br />
Search for: driver problem <strong>npf</strong> -&gt; 0 questions<br />
</p><p>Something is broken in the search function, if the order of the search words make such a difference.</p><p>Regards<br />
Kurt</p></div><div id="comment-14999-info" class="comment-info"><span class="comment-age">(14 Oct '12, 07:25)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-14988" class="comment-tools"></div><div class="clear"></div><div id="comment-14988-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

