+++
type = "question"
title = "Which software can capture data in monitor mode on Windows?"
description = '''Let&#x27;s make a list. CommView Wifi - supports Atheros chipsets. Microsoft Network Monitor - supports &amp;lt;unknown&amp;gt; %your suggestion%'''
date = "2012-11-15T17:15:00Z"
lastmod = "2012-11-20T13:29:00Z"
weight = 15949
keywords = [ "windows", "promiscuous-mode" ]
aliases = [ "/questions/15949" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Which software can capture data in monitor mode on Windows?](/questions/15949/which-software-can-capture-data-in-monitor-mode-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15949-score" class="post-score" title="current number of votes">0</div><span id="post-15949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Let's make a list.</p><p>CommView Wifi - supports Atheros chipsets.</p><p>Microsoft Network Monitor - supports &lt;unknown&gt;</p><p>%your suggestion%</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-promiscuous-mode" rel="tag" title="see questions tagged &#39;promiscuous-mode&#39;">promiscuous-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '12, 17:15</strong></p><img src="https://secure.gravatar.com/avatar/a00c8faaeb8a556d49ded6b3aa1eb51e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xpeh&#39;s gravatar image" /><p><span>xpeh</span><br />
<span class="score" title="-3 reputation points">-3</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xpeh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Nov '12, 00:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-15949" class="comments-container"></div><div id="comment-tools-15949" class="comment-tools"></div><div class="clear"></div><div id="comment-15949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15953"></span>

<div id="answer-container-15953" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15953-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15953-score" class="post-score" title="current number of votes">2</div><span id="post-15953-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark - with <a href="http://www.riverbed.com/us/products/cascade/wireshark_enhancements/airpcap.php">AirPCap</a> adaptor</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '12, 00:17</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-15953" class="comments-container"><span id="15978"></span><div id="comment-15978" class="comment"><div id="post-15978-score" class="comment-score"></div><div class="comment-text"><p>Which costs $100 for nothing. But i'll add it too.</p></div><div id="comment-15978-info" class="comment-info"><span class="comment-age">(16 Nov '12, 12:04)</span> <span class="comment-user userinfo">xpeh</span></div></div><span id="15980"></span><div id="comment-15980" class="comment"><div id="post-15980-score" class="comment-score"></div><div class="comment-text"><p>Ohshi, now i have to enter captcha to edit my question :D Well, then edit it by yourself.</p></div><div id="comment-15980-info" class="comment-info"><span class="comment-age">(16 Nov '12, 12:05)</span> <span class="comment-user userinfo">xpeh</span></div></div><span id="15983"></span><div id="comment-15983" class="comment"><div id="post-15983-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Which costs $100 for nothing.</p></blockquote><p>Interesting, when did Tamo Soft start giving away CommView Wifi for less than $100 or even for free? I must have missed that ...</p><blockquote><p><code>http://www.tamos.com/order/index.php?js=1</code></p></blockquote></div><div id="comment-15983-info" class="comment-info"><span class="comment-age">(16 Nov '12, 13:18)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="15986"></span><div id="comment-15986" class="comment"><div id="post-15986-score" class="comment-score"></div><div class="comment-text"><p>It's a little offtopic here. As a private user, you can use CommView WiFi for free, opposite to $100 stick that can nothing except capturing packets (i.e. it cannot operate in normal mode), has no socket for external antenna etc.</p></div><div id="comment-15986-info" class="comment-info"><span class="comment-age">(16 Nov '12, 13:49)</span> <span class="comment-user userinfo">xpeh</span></div></div><span id="15989"></span><div id="comment-15989" class="comment"><div id="post-15989-score" class="comment-score"></div><div class="comment-text"><p>CommView, but not CommView <strong>Wifi</strong>. If you think CommView <strong>Wifi</strong> is free for personal use, please post the link were I can find that statement on the website of the vendor.</p></div><div id="comment-15989-info" class="comment-info"><span class="comment-age">(16 Nov '12, 14:02)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="15990"></span><div id="comment-15990" class="comment not_top_scorer"><div id="post-15990-score" class="comment-score"></div><div class="comment-text"><p>It's not on vendors site, but maybe on torrent trackers :)</p><p>I've got captcha prompt, so either edit the question by yourself or vote to give me neutral carma.</p></div><div id="comment-15990-info" class="comment-info"><span class="comment-age">(16 Nov '12, 14:02)</span> <span class="comment-user userinfo">xpeh</span></div></div><span id="15991"></span><div id="comment-15991" class="comment not_top_scorer"><div id="post-15991-score" class="comment-score"></div><div class="comment-text"><p>So, you compare a product for $100 with a stolen license key of a product that costs several hundred $ and then you complain, that the product you cannot steal via torrent (as it's physical) is too expensive?</p><p>Do you think that is funny?</p></div><div id="comment-15991-info" class="comment-info"><span class="comment-age">(16 Nov '12, 14:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="16134"></span><div id="comment-16134" class="comment not_top_scorer"><div id="post-16134-score" class="comment-score"></div><div class="comment-text"><p>There was my answer, where is it?</p></div><div id="comment-16134-info" class="comment-info"><span class="comment-age">(20 Nov '12, 13:16)</span> <span class="comment-user userinfo">xpeh</span></div></div><span id="16135"></span><div id="comment-16135" class="comment not_top_scorer"><div id="post-16135-score" class="comment-score"></div><div class="comment-text"><p>deleted or converted to a comment.</p></div><div id="comment-16135-info" class="comment-info"><span class="comment-age">(20 Nov '12, 13:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15953" class="comment-tools"><span class="comments-showing"> showing 5 of 9 </span> <a href="#" class="show-all-comments-link">show 4 more comments</a></div><div class="clear"></div><div id="comment-15953-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

