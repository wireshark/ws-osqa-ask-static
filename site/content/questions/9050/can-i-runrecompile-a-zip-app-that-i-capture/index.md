+++
type = "question"
title = "can i run/recompile a zip app that i capture?"
description = '''hello i have caputed a zip/app and i was wondering if i could use the hex code to recompile said app. thanks for any help.'''
date = "2012-02-15T20:56:00Z"
lastmod = "2012-02-16T10:43:00Z"
weight = 9050
keywords = [ "app", "zip" ]
aliases = [ "/questions/9050" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can i run/recompile a zip app that i capture?](/questions/9050/can-i-runrecompile-a-zip-app-that-i-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9050-score" class="post-score" title="current number of votes">0</div><span id="post-9050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello i have caputed a zip/app and i was wondering if i could use the hex code to recompile said app. thanks for any help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-app" rel="tag" title="see questions tagged &#39;app&#39;">app</span> <span class="post-tag tag-link-zip" rel="tag" title="see questions tagged &#39;zip&#39;">zip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '12, 20:56</strong></p><img src="https://secure.gravatar.com/avatar/0cdd4068d371ff04f08ce4f3771bffd3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wolf%20Demon&#39;s gravatar image" /><p><span>Wolf Demon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wolf Demon has no accepted answers">0%</span></p></div></div><div id="comments-container-9050" class="comments-container"></div><div id="comment-tools-9050" class="comment-tools"></div><div class="clear"></div><div id="comment-9050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9059"></span>

<div id="answer-container-9059" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9059-score" class="post-score" title="current number of votes">1</div><span id="post-9059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>if the protocol used is HTTP or SMB, you can try File-&gt;Export-&gt;Objects.</p><p>Otherwise, as far I as know there isn't an 'easy way' to do this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Feb '12, 04:18</strong></p><img src="https://secure.gravatar.com/avatar/dbc4d8cb6be85bd586ca4bf211e1337c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thetechfirm&#39;s gravatar image" /><p><span>thetechfirm</span><br />
<span class="score" title="64 reputation points">64</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thetechfirm has no accepted answers">0%</span></p></div></div><div id="comments-container-9059" class="comments-container"><span id="9066"></span><div id="comment-9066" class="comment"><div id="post-9066-score" class="comment-score"></div><div class="comment-text"><p>its http and i eported it but how to i get it to run?</p></div><div id="comment-9066-info" class="comment-info"><span class="comment-age">(16 Feb '12, 09:12)</span> <span class="comment-user userinfo">Wolf Demon</span></div></div><span id="9067"></span><div id="comment-9067" class="comment"><div id="post-9067-score" class="comment-score"></div><div class="comment-text"><p>If you successfully exported the zip file. you should be able to unzip it.</p></div><div id="comment-9067-info" class="comment-info"><span class="comment-age">(16 Feb '12, 09:14)</span> <span class="comment-user userinfo">thetechfirm</span></div></div><span id="9069"></span><div id="comment-9069" class="comment"><div id="post-9069-score" class="comment-score"></div><div class="comment-text"><p>can you email me or add my skype and help maybe i can send you my capture and you can try to figure it out.... Email; <span class="__cf_email__" data-cfemail="c8a5a9a6bcada7bba7bca788afa5a9a1a4e6aba7a5">[email protected]</span> Skype; manteo.soto1 thanks alot</p></div><div id="comment-9069-info" class="comment-info"><span class="comment-age">(16 Feb '12, 09:38)</span> <span class="comment-user userinfo">Wolf Demon</span></div></div><span id="9072"></span><div id="comment-9072" class="comment"><div id="post-9072-score" class="comment-score"></div><div class="comment-text"><p>I've converted all the "answers" to comments on the original answer. Please read the <a href="http://ask.wireshark.org/faq/">FAQ</a>, and remember to accept any answer that solves your issue.</p></div><div id="comment-9072-info" class="comment-info"><span class="comment-age">(16 Feb '12, 10:43)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-9059" class="comment-tools"></div><div class="clear"></div><div id="comment-9059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

