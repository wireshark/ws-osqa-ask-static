+++
type = "question"
title = "When I try and run a fresh install from terminal it says..."
description = '''The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist dyld: Library not loaded: /usr/X11/lib/libfreetype.6.dylib  Referenced from: /Applications/Wireshark.app/Contents/Resources/bin/wireshark-bin  Reason: Incompatible library version: wireshark-bin requires ve...'''
date = "2011-05-02T01:22:00Z"
lastmod = "2011-05-02T01:22:00Z"
weight = 3872
keywords = [ "failed" ]
aliases = [ "/questions/3872" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [When I try and run a fresh install from terminal it says...](/questions/3872/when-i-try-and-run-a-fresh-install-from-terminal-it-says)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3872-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3872-score" class="post-score" title="current number of votes">0</div><span id="post-3872-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist dyld: Library not loaded: /usr/X11/lib/libfreetype.6.dylib Referenced from: /Applications/Wireshark.app/Contents/Resources/bin/wireshark-bin Reason: Incompatible library version: wireshark-bin requires version 13.0.0 or later, but libfreetype.6.dylib provides version 10.0.0</p><p>What does this mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-failed" rel="tag" title="see questions tagged &#39;failed&#39;">failed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '11, 01:22</strong></p><img src="https://secure.gravatar.com/avatar/7d2a20ea20f40a93c53120360ca9ba9d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tim%20Nethers&#39;s gravatar image" /><p><span>Tim Nethers</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tim Nethers has no accepted answers">0%</span></p></div></div><div id="comments-container-3872" class="comments-container"></div><div id="comment-tools-3872" class="comment-tools"></div><div class="clear"></div><div id="comment-3872-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

