+++
type = "question"
title = "Total Send and receive data on my server"
description = '''Hello all senpai, I am using this software(v2.4.0) for the first time. I would like to thank you guys for the software and for the reply in advance. I&#x27;ve read some of the post here but not sure if it is related to my inquiry. I have a file server and i would like to check total data sent and receive...'''
date = "2017-07-27T18:28:00Z"
lastmod = "2017-07-27T23:32:00Z"
weight = 63187
keywords = [ "tags" ]
aliases = [ "/questions/63187" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Total Send and receive data on my server](/questions/63187/total-send-and-receive-data-on-my-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63187-score" class="post-score" title="current number of votes">0</div><span id="post-63187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all senpai,</p><p>I am using this software(v2.4.0) for the first time. I would like to thank you guys for the software and for the reply in advance. I've read some of the post here but not sure if it is related to my inquiry. I have a file server and i would like to check total data sent and received from all my other clients computers for the month. Reason being that we would like to move on the cloud storage so i could get information on estimated one month cost will be how much before we commit.</p><p>Kindly forgive me for my newbie-ness and hope explanation can be in layman term. Gary</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tags" rel="tag" title="see questions tagged &#39;tags&#39;">tags</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '17, 18:28</strong></p><img src="https://secure.gravatar.com/avatar/de5ed3231eaacddd91b13c5d288c41c3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SlaCk3r&#39;s gravatar image" /><p><span>SlaCk3r</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SlaCk3r has no accepted answers">0%</span></p></div></div><div id="comments-container-63187" class="comments-container"></div><div id="comment-tools-63187" class="comment-tools"></div><div class="clear"></div><div id="comment-63187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63196"></span>

<div id="answer-container-63196" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63196-score" class="post-score" title="current number of votes">0</div><span id="post-63196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SlaCk3r has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a power tool to look at network traffic to the most minute detail. Every bit of every network packet is being looked at as if under a microscope. Now, would you use a microscope to gauge how much water flows through your watermain? Probably not. So even though Wireshark is a very capable 'microscope' which is capable to do statistics like this, it's not what it's made for. It will be cumbersome to use this way, while other tools are specifically made for this purpose. For instance your network card and driver already collect network statistics, and tools like <a href="https://www.cacti.net">Cacti</a> leverage this. Another option is to look into is <a href="http://www.ntop.org/products/traffic-analysis/ntop/">ntop-ng</a> which does capture and analysis network traffic, like Wireshark, specifically for statistics.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jul '17, 23:24</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-63196" class="comments-container"><span id="63197"></span><div id="comment-63197" class="comment"><div id="post-63197-score" class="comment-score"></div><div class="comment-text"><p>Thank you senpai!</p></div><div id="comment-63197-info" class="comment-info"><span class="comment-age">(27 Jul '17, 23:32)</span> <span class="comment-user userinfo">SlaCk3r</span></div></div></div><div id="comment-tools-63196" class="comment-tools"></div><div class="clear"></div><div id="comment-63196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

