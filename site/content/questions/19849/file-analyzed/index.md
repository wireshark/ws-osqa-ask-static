+++
type = "question"
title = "File Analyzed"
description = '''I had a file sent to me from a client who is having intermittent problems with data corruption. I am looking for someone to hire to evaluate their file and let me know if you can see what could be causing their trouble. Is help like this possible? Thanks'''
date = "2013-03-26T12:06:00Z"
lastmod = "2013-03-26T12:17:00Z"
weight = 19849
keywords = [ "consultant" ]
aliases = [ "/questions/19849" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [File Analyzed](/questions/19849/file-analyzed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19849-score" class="post-score" title="current number of votes">0</div><span id="post-19849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had a file sent to me from a client who is having intermittent problems with data corruption. I am looking for someone to hire to evaluate their file and let me know if you can see what could be causing their trouble. Is help like this possible?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-consultant" rel="tag" title="see questions tagged &#39;consultant&#39;">consultant</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '13, 12:06</strong></p><img src="https://secure.gravatar.com/avatar/6dc663ab66c9f837e06607cf41322346?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ouajl26&#39;s gravatar image" /><p><span>ouajl26</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ouajl26 has no accepted answers">0%</span></p></div></div><div id="comments-container-19849" class="comments-container"></div><div id="comment-tools-19849" class="comment-tools"></div><div class="clear"></div><div id="comment-19849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19850"></span>

<div id="answer-container-19850" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19850-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19850-score" class="post-score" title="current number of votes">0</div><span id="post-19850-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, this is possible. If you can do the analysis yourself but need some guidance on specific things that you see in the tracefile, you can ask questions here and there are quite a few people willing to answer. It can help to upload the tracefile somewhere for people to look at when answering your questions. However, please do take care of privacy issues when doing so.</p><p>If you need someone to do the full analysis for you, then it is better to hire someone. There are several people on this forum (including me) that can offer such services. Either remotely or on location if more traces need to be made.</p><p>You can also look on the Internet for companies that do "Network Analysis", "Protocol Analysis", "Packet Analysis", "Network troubleshooting", "Network Audits", etc. :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '13, 12:17</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-19850" class="comments-container"></div><div id="comment-tools-19850" class="comment-tools"></div><div class="clear"></div><div id="comment-19850-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

