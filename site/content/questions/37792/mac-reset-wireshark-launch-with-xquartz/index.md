+++
type = "question"
title = "Mac Reset Wireshark launch with XQuartz"
description = '''The first time run wireshark, I should choose XQuartz to open it,  but I accidentally choose another application. Now I cannot run wireshark, how to change it?'''
date = "2014-11-12T08:01:00Z"
lastmod = "2014-11-12T08:01:00Z"
weight = 37792
keywords = [ "xquartz", "mac", "launch" ]
aliases = [ "/questions/37792" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac Reset Wireshark launch with XQuartz](/questions/37792/mac-reset-wireshark-launch-with-xquartz)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37792-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37792-score" class="post-score" title="current number of votes">0</div><span id="post-37792-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The first time run wireshark, I should choose XQuartz to open it, but I accidentally choose another application.</p><p>Now I cannot run wireshark, how to change it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-launch" rel="tag" title="see questions tagged &#39;launch&#39;">launch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '14, 08:01</strong></p><img src="https://secure.gravatar.com/avatar/7e462610950411a9b4a6d8735c0eaebf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sharkshark&#39;s gravatar image" /><p><span>sharkshark</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sharkshark has no accepted answers">0%</span></p></div></div><div id="comments-container-37792" class="comments-container"></div><div id="comment-tools-37792" class="comment-tools"></div><div class="clear"></div><div id="comment-37792-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

