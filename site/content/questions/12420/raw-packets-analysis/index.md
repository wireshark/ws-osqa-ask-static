+++
type = "question"
title = "Raw packets analysis"
description = '''I am sending packets using the UDP protocol and raw sockets. i want to manually build the ip header which I want to transmit over wlan0 and capture these packets using wireshark so that I can analyse the contents that I have received. But for some reason, I am not able to capture these packets using...'''
date = "2012-07-03T12:13:00Z"
lastmod = "2012-07-03T12:13:00Z"
weight = 12420
keywords = [ "raw_packet_capture" ]
aliases = [ "/questions/12420" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Raw packets analysis](/questions/12420/raw-packets-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12420-score" class="post-score" title="current number of votes">0</div><span id="post-12420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am sending packets using the UDP protocol and raw sockets. i want to manually build the ip header which I want to transmit over wlan0 and capture these packets using wireshark so that I can analyse the contents that I have received. But for some reason, I am not able to capture these packets using wireshark. Do you need to write an explicit receiver code on the client side to capture these packets or can can wireshark capture them automatically?</p><p>I am a noob at network programming and any help in this regard would be sincerely appreciated.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-raw_packet_capture" rel="tag" title="see questions tagged &#39;raw_packet_capture&#39;">raw_packet_capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '12, 12:13</strong></p><img src="https://secure.gravatar.com/avatar/4626e19b706e53860c1ff4e3277b32d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Noob&#39;s gravatar image" /><p><span>Noob</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Noob has no accepted answers">0%</span></p></div></div><div id="comments-container-12420" class="comments-container"></div><div id="comment-tools-12420" class="comment-tools"></div><div class="clear"></div><div id="comment-12420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

