+++
type = "question"
title = "Program crashes when opening damaged pcap files"
description = '''Program (x64) hangs or crashes when opening damaged .pcap files with broken packet header (timestamp and sizes) and data. (It was UDP packets) Seems like there are no some checks for time or sizes validity. That causes buffer overrun or something like that. '''
date = "2017-02-27T14:07:00Z"
lastmod = "2017-02-27T18:58:00Z"
weight = 59718
keywords = [ "pcap", "crash" ]
aliases = [ "/questions/59718" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Program crashes when opening damaged pcap files](/questions/59718/program-crashes-when-opening-damaged-pcap-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59718-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59718-score" class="post-score" title="current number of votes">0</div><span id="post-59718-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Program (x64) hangs or crashes when opening damaged .pcap files with broken packet header (timestamp and sizes) and data. (It was UDP packets) Seems like there are no some checks for time or sizes validity. That causes buffer overrun or something like that.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '17, 14:07</strong></p><img src="https://secure.gravatar.com/avatar/101212fe40181850913fc47282e4a685?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EugeneF&#39;s gravatar image" /><p><span>EugeneF</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EugeneF has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-59718" class="comments-container"></div><div id="comment-tools-59718" class="comment-tools"></div><div class="clear"></div><div id="comment-59718-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59722"></span>

<div id="answer-container-59722" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59722-score" class="post-score" title="current number of votes">0</div><span id="post-59722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like a bug. First be sure you're running the most recent version. If the problem still occurs then please <a href="https://bugs.wireshark.org">file a bug report</a> and attach the capture file there.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '17, 18:58</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-59722" class="comments-container"></div><div id="comment-tools-59722" class="comment-tools"></div><div class="clear"></div><div id="comment-59722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

