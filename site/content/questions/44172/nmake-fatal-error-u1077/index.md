+++
type = "question"
title = "NMAKE: fatal error U1077"
description = '''ERROR: The contents of &#x27;C:&#92;Development&#92;Wireshark-win32-libs&#92;current_tag.txt&#x27; is (unknown). It should be 2015-05-30.  Checking for required applications:  cl: /cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/BIN/cl   link: /cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/BIN/link   ...'''
date = "2015-07-15T06:50:00Z"
lastmod = "2015-07-15T13:28:00Z"
weight = 44172
keywords = [ "u1077", "w32", "name", "error" ]
aliases = [ "/questions/44172" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [NMAKE: fatal error U1077](/questions/44172/nmake-fatal-error-u1077)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44172-score" class="post-score" title="current number of votes">0</div><span id="post-44172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>ERROR: The contents of &#39;C:\Development\Wireshark-win32-libs\current_tag.txt&#39; is (unknown).
It should be 2015-05-30.

Checking for required applications:
    cl: /cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/BIN/cl 
    link: /cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/BIN/link 
    nmake: /cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/BIN/nmake 
    bash: /usr/bin/bash 
    bison: /usr/bin/bison 
    flex: /usr/bin/flex 
    env: /usr/bin/env 
    grep: /usr/bin/grep 
    /usr/bin/find: /usr/bin/find 
    peflags: /usr/bin/peflags 
    perl: /usr/bin/perl 
    C:\tools\python2\python.exe: /cygdrive/c/tools/python2/python.exe 
    sed: /usr/bin/sed 
    unzip: /usr/bin/unzip 
    wget: /usr/bin/wget

Can&#39;t find:  C:\Qt\Qt5.3.0\5.3\msvc2013\bin\qmake

ERROR: These application(s) are either not installed or simply can&#39;t be found in the current PATH: /cygdrive/c/tools/python2:/cygdrive/c/Program Files/Microsoft Visual Studio 12.0/Common7/IDE/CommonExtensions/Microsoft/TestWindow:/cygdrive/c/Program Files/Microsoft SDKs/F#/3.1/Framework/v4.0:/cygdrive/c/Program Files/MSBuild/12.0/bin:/cygdrive/c/Program Files/Microsoft Visual Studio 12.0/Common7/IDE:/cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/BIN:/cygdrive/c/Program Files/Microsoft Visual Studio 12.0/Common7/Tools:/cygdrive/c/Windows/Microsoft.NET/Framework/v4.0.30319:/cygdrive/c/Program Files/Microsoft Visual Studio 12.0/VC/VCPackages:/cygdrive/c/Program Files/HTML Help Workshop:/cygdrive/c/Program Files/Microsoft Visual Studio 12.0/Team Tools/Performance Tools:/cygdrive/c/Program Files/Windows Kits/8.1/bin/x86:/cygdrive/c/Program Files/Microsoft SDKs/Windows/v8.1A/bin/NETFX 4.5.1 Tools:/cygdrive/c/ProgramData/Oracle/Java/javapath:/cygdrive/c/Windows/system32:/cygdrive/c/Windows:/cygdrive/c/Windows/System32/Wbem:/cygdrive/c/Windows/System32/WindowsPowerShell/v1.0:/cygdrive/c/Program Files/Skype/Phone:/cygdrive/c/gtk/bin:/cygdrive/c/Program Files/Windows Kits/8.1/Windows Performance Toolkit:/cygdrive/c/Program Files/Microsoft SQL Server/110/Tools/Binn:/cygdrive/c/Program Files/Microsoft SDKs/TypeScript/1.0:/cygdrive/c/tools/python2:/cygdrive/c/ProgramData/chocolatey/bin:/cygdrive/c/tools/cygwin:/cygdrive/c/Program Files/HTML Help Workshop:/usr/bin:/cygdrive/c/Development/Wireshark-win32-libs/gtk2/bin:/cygdrive/c/bin:/cygdrive/c/Development/Wireshark-win32-libs/zlib-1.2.8-ws.

For additional help, please visit:
    http://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-u1077" rel="tag" title="see questions tagged &#39;u1077&#39;">u1077</span> <span class="post-tag tag-link-w32" rel="tag" title="see questions tagged &#39;w32&#39;">w32</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '15, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/6c44cebbc31d7b3e140200cb67fadbf0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ravi%20Chander%20Jha&#39;s gravatar image" /><p><span>Ravi Chander...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ravi Chander Jha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jul '15, 08:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-44172" class="comments-container"></div><div id="comment-tools-44172" class="comment-tools"></div><div class="clear"></div><div id="comment-44172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44178"></span>

<div id="answer-container-44178" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44178-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44178-score" class="post-score" title="current number of votes">0</div><span id="post-44178-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm not entirely sure what your question is, but you don't seem to have a valid install of Qt in <code>C:\Qt\Qt5.3.0\5.3\msvc2013</code>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '15, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-44178" class="comments-container"><span id="44180"></span><div id="comment-44180" class="comment"><div id="post-44180-score" class="comment-score"></div><div class="comment-text"><p>i dont think any error in C:\Qt\Qt5.3.0\5.3\msvc2013</p></div><div id="comment-44180-info" class="comment-info"><span class="comment-age">(15 Jul '15, 11:04)</span> <span class="comment-user userinfo">Ravi Chander...</span></div></div><span id="44189"></span><div id="comment-44189" class="comment"><div id="post-44189-score" class="comment-score"></div><div class="comment-text"><p>Can you find qmake.exe in C:\Qt\Qt5.3.0.\5.3\msvc2013? Presumably no. How did you install Qt?</p></div><div id="comment-44189-info" class="comment-info"><span class="comment-age">(15 Jul '15, 13:28)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-44178" class="comment-tools"></div><div class="clear"></div><div id="comment-44178-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

