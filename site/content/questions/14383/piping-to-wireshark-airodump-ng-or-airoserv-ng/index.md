+++
type = "question"
title = "&quot;Piping&quot; to wireshark airodump-ng or airoserv-ng"
description = '''Hi, I would like to pipe the output of airodump or airoserv to wireshark in a remote way and thus I will be able to use wireshark tools. However, I&#x27;m not able to do, I have tried to do it in two ways. -First one: create a connection to airserver-ng and the retrieved output is pipped as input to wire...'''
date = "2012-09-19T11:15:00Z"
lastmod = "2012-09-19T11:15:00Z"
weight = 14383
keywords = [ "pipe", "airodump", "airmon", "wireshark" ]
aliases = [ "/questions/14383" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# ["Piping" to wireshark airodump-ng or airoserv-ng](/questions/14383/piping-to-wireshark-airodump-ng-or-airoserv-ng)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14383-score" class="post-score" title="current number of votes">0</div><span id="post-14383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to pipe the output of airodump or airoserv to wireshark in a remote way and thus I will be able to use wireshark tools. However, I'm not able to do, I have tried to do it in two ways.</p><p>-First one: create a connection to airserver-ng and the retrieved output is pipped as input to wireshark,but doing this I get an error on wireshark saying that "it doesn't understand the libcap" so the whole process is aborted. -Second one: I send the airodump/airoserv to a .pcap file and later on I open it with wiresark, this works,but after certain span the process is stopped and wireshark shows a message saying that the connection was closed...</p><p>I major goal is to see on live the sniffing ouput in wireshark, any suggestion??</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-airodump" rel="tag" title="see questions tagged &#39;airodump&#39;">airodump</span> <span class="post-tag tag-link-airmon" rel="tag" title="see questions tagged &#39;airmon&#39;">airmon</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '12, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/81b2e2174322adc0c63695cae3c7b605?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ndarkness&#39;s gravatar image" /><p><span>ndarkness</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ndarkness has no accepted answers">0%</span></p></div></div><div id="comments-container-14383" class="comments-container"></div><div id="comment-tools-14383" class="comment-tools"></div><div class="clear"></div><div id="comment-14383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

