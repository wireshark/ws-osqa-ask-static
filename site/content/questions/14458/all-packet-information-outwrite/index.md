+++
type = "question"
title = "All packet information outwrite"
description = '''Hi! I would like all packet information outwrite. But i can write out only IP header and media_attr, sdp data, sip data...etc... How can i all information outwrite with LUA? '''
date = "2012-09-23T02:19:00Z"
lastmod = "2012-09-23T02:19:00Z"
weight = 14458
keywords = [ "information", "packet", "outwrite" ]
aliases = [ "/questions/14458" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [All packet information outwrite](/questions/14458/all-packet-information-outwrite)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14458-score" class="post-score" title="current number of votes">0</div><span id="post-14458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>I would like all packet information outwrite. But i can write out only IP header and media_attr, sdp data, sip data...etc... How can i all information outwrite with LUA?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-information" rel="tag" title="see questions tagged &#39;information&#39;">information</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-outwrite" rel="tag" title="see questions tagged &#39;outwrite&#39;">outwrite</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '12, 02:19</strong></p><img src="https://secure.gravatar.com/avatar/5c2ae72f5684890320d7616b442365a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="speede05&#39;s gravatar image" /><p><span>speede05</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="speede05 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Sep '12, 02:56</strong> </span></p></div></div><div id="comments-container-14458" class="comments-container"></div><div id="comment-tools-14458" class="comment-tools"></div><div class="clear"></div><div id="comment-14458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

