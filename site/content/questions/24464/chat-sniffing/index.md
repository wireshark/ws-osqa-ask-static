+++
type = "question"
title = "chat sniffing"
description = '''how can i sniff chat through ip messanger in LAN connection?? i tried &quot;msnms&quot; commnand for this result but packets are not found. so tell me some proper suggestion.. :::;thankyou::::'''
date = "2013-09-09T00:21:00Z"
lastmod = "2017-03-24T22:24:00Z"
weight = 24464
keywords = [ "ip", "messenger" ]
aliases = [ "/questions/24464" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [chat sniffing](/questions/24464/chat-sniffing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24464-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24464-score" class="post-score" title="current number of votes">0</div><span id="post-24464-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i sniff chat through ip messanger in LAN connection?? i tried "msnms" commnand for this result but packets are not found. so tell me some proper suggestion..</p><p>:::;thankyou::::</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-messenger" rel="tag" title="see questions tagged &#39;messenger&#39;">messenger</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '13, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Sep '13, 01:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-24464" class="comments-container"></div><div id="comment-tools-24464" class="comment-tools"></div><div class="clear"></div><div id="comment-24464-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24465"></span>

<div id="answer-container-24465" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24465-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24465-score" class="post-score" title="current number of votes">1</div><span id="post-24465-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Simply put just filter for the corresponding TCP or UDP ports that belong to your chat applications protocol. Inside those conversations, the chat messages are found although most commonly encrypted.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '13, 01:25</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div></div><div id="comments-container-24465" class="comments-container"><span id="24469"></span><div id="comment-24469" class="comment"><div id="post-24469-score" class="comment-score"></div><div class="comment-text"><p>thanx i will try brother..</p></div><div id="comment-24469-info" class="comment-info"><span class="comment-age">(09 Sep '13, 04:25)</span> <span class="comment-user userinfo">john6</span></div></div><span id="24471"></span><div id="comment-24471" class="comment"><div id="post-24471-score" class="comment-score"></div><div class="comment-text"><p>if u done that chat snifing then plz send me some screenshot or steps on "<span class="__cf_email__" data-cfemail="ea808582848d8285999edcaa8d878b8386c4898587">[email protected]</span>".. plz help me dear..</p><p>:::::thankyou:::::</p></div><div id="comment-24471-info" class="comment-info"><span class="comment-age">(09 Sep '13, 04:33)</span> <span class="comment-user userinfo">john6</span></div></div><span id="24472"></span><div id="comment-24472" class="comment"><div id="post-24472-score" class="comment-score"></div><div class="comment-text"><p>That won't happen ! Sorry, this is a Q&amp;A site related to wireshark problems. If you have a specific question about your packet trace or it's output feel free to provide detailed further questions.</p></div><div id="comment-24472-info" class="comment-info"><span class="comment-age">(09 Sep '13, 04:58)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="60319"></span><div id="comment-60319" class="comment"><div id="post-60319-score" class="comment-score"></div><div class="comment-text"><p>Hai this is shaik..Is there any possibility to capture http instead of http2</p></div><div id="comment-60319-info" class="comment-info"><span class="comment-age">(24 Mar '17, 22:24)</span> <span class="comment-user userinfo">shaik</span></div></div></div><div id="comment-tools-24465" class="comment-tools"></div><div class="clear"></div><div id="comment-24465-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

