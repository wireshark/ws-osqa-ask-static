+++
type = "question"
title = "Can&#x27;t see VPN traffic"
description = '''set up as follows: private network --&amp;gt; DDWRT Router with VPN --&amp;gt; HUB --&amp;gt; A regular router --&amp;gt; Internet The hub is true hub (netgear EN108). I have a laptop running wireshark on the hub monitoring traffic between the routers. Problem is wireshark is not capturing any of the packets to/fro...'''
date = "2014-09-24T12:48:00Z"
lastmod = "2014-09-24T14:45:00Z"
weight = 36571
keywords = [ "vpn" ]
aliases = [ "/questions/36571" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see VPN traffic](/questions/36571/cant-see-vpn-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36571-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36571-score" class="post-score" title="current number of votes">0</div><span id="post-36571-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>set up as follows:</p><p>private network --&gt; DDWRT Router with VPN --&gt; HUB --&gt; A regular router --&gt; Internet</p><p>The hub is true hub (netgear EN108). I have a laptop running wireshark on the hub monitoring traffic between the routers. Problem is wireshark is not capturing any of the packets to/from the VPN router when the traffic is going through the tunnel. I CAN see packets between the routers when that traffic is outside the tunnel. example, the route tables are set up so that i can ping the outer router from the private network - those icmp packets flow outside the vpn tunnel, and wireshark sees them. why am I not seeing any encrypted packets for the rest of the traffic that flows through the VPN tunnel???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '14, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/9417bbd780c50fd628d56fdccb8e9398?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hocken24a&#39;s gravatar image" /><p><span>hocken24a</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hocken24a has no accepted answers">0%</span></p></div></div><div id="comments-container-36571" class="comments-container"><span id="36572"></span><div id="comment-36572" class="comment"><div id="post-36572-score" class="comment-score"></div><div class="comment-text"><p>I know this may sound a little crazy, but is there another route from the DDWRT router to the Internet?</p></div><div id="comment-36572-info" class="comment-info"><span class="comment-age">(24 Sep '14, 14:45)</span> <span class="comment-user userinfo">PaulOfford</span></div></div></div><div id="comment-tools-36571" class="comment-tools"></div><div class="clear"></div><div id="comment-36571-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

