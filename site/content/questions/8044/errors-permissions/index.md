+++
type = "question"
title = "errors permissions"
description = '''failed to allow interface discovery using user wheel freebsd 8.2'''
date = "2011-12-19T11:39:00Z"
lastmod = "2011-12-19T12:00:00Z"
weight = 8044
keywords = [ "errors" ]
aliases = [ "/questions/8044" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [errors permissions](/questions/8044/errors-permissions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8044-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8044-score" class="post-score" title="current number of votes">0</div><span id="post-8044-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>failed to allow interface discovery using user wheel freebsd 8.2</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-errors" rel="tag" title="see questions tagged &#39;errors&#39;">errors</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '11, 11:39</strong></p><img src="https://secure.gravatar.com/avatar/33ddfbb94b74ca3c39314d0837efb4c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pvh115&#39;s gravatar image" /><p><span>pvh115</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pvh115 has no accepted answers">0%</span></p></div></div><div id="comments-container-8044" class="comments-container"><span id="8045"></span><div id="comment-8045" class="comment"><div id="post-8045-score" class="comment-score"></div><div class="comment-text"><p>Can you please provide some context here, and an actual question to answer?</p></div><div id="comment-8045-info" class="comment-info"><span class="comment-age">(19 Dec '11, 12:00)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-8044" class="comment-tools"></div><div class="clear"></div><div id="comment-8044-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

