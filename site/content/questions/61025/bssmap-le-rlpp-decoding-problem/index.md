+++
type = "question"
title = "BSSMAP-LE / RLPP decoding problem"
description = '''I am using wireshark to decode BSSMAP-LE PDUs containing RRLP PDUs. RRLP PDUs are Unaligned PER ASN.1 encoded using ASN1C compiler &quot;v0.9.28&quot;. If RRLP PDU contains only MANDATORY parameters wireshark decoding is the same that ASN1C compiler. If I add an OPTIONAL parameter wireshark decoding seems to ...'''
date = "2017-04-25T01:57:00Z"
lastmod = "2017-04-25T04:56:00Z"
weight = 61025
keywords = [ "bssmap", "rrlp" ]
aliases = [ "/questions/61025" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [BSSMAP-LE / RLPP decoding problem](/questions/61025/bssmap-le-rlpp-decoding-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61025-score" class="post-score" title="current number of votes">0</div><span id="post-61025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using wireshark to decode BSSMAP-LE PDUs containing RRLP PDUs. RRLP PDUs are Unaligned PER ASN.1 encoded using ASN1C compiler "v0.9.28". If RRLP PDU contains only MANDATORY parameters wireshark decoding is the same that ASN1C compiler. If I add an OPTIONAL parameter wireshark decoding seems to be wrong. Did someone use wireshark in such a field. Thany you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bssmap" rel="tag" title="see questions tagged &#39;bssmap&#39;">bssmap</span> <span class="post-tag tag-link-rrlp" rel="tag" title="see questions tagged &#39;rrlp&#39;">rrlp</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '17, 01:57</strong></p><img src="https://secure.gravatar.com/avatar/98c95d4003dec700239fba3670a70612?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oaa&#39;s gravatar image" /><p><span>oaa</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oaa has no accepted answers">0%</span></p></div></div><div id="comments-container-61025" class="comments-container"><span id="61032"></span><div id="comment-61032" class="comment"><div id="post-61032-score" class="comment-score"></div><div class="comment-text"><p>Open a bug and attach a sample trace file.</p></div><div id="comment-61032-info" class="comment-info"><span class="comment-age">(25 Apr '17, 04:44)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="61035"></span><div id="comment-61035" class="comment"><div id="post-61035-score" class="comment-score"></div><div class="comment-text"><p>See the wiki page <a href="https://wiki.wireshark.org/ReportingBugs">here</a> on reporting bugs.</p></div><div id="comment-61035-info" class="comment-info"><span class="comment-age">(25 Apr '17, 04:56)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61025" class="comment-tools"></div><div class="clear"></div><div id="comment-61025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

