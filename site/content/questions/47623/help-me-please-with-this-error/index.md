+++
type = "question"
title = "help me please with this error"
description = '''I am getting this error and I have went to the websites at the bottom of the post and I could not find anything that was useful. I could not find where to set hardware filter to promisuous mode. The capture session could not be initiated on interface &#x27;&#92;Device&#92;NPF_{B66567B3-D08F-4A76-B3A7-2D6CFC9E6C4...'''
date = "2015-11-15T22:58:00Z"
lastmod = "2015-11-15T23:26:00Z"
weight = 47623
keywords = [ "hardware", "filter" ]
aliases = [ "/questions/47623" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [help me please with this error](/questions/47623/help-me-please-with-this-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47623-score" class="post-score" title="current number of votes">0</div><span id="post-47623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am getting this error and I have went to the websites at the bottom of the post and I could not find anything that was useful. I could not find where to set hardware filter to promisuous mode.</p><p>The capture session could not be initiated on interface '\Device\NPF_{B66567B3-D08F-4A76-B3A7-2D6CFC9E6C4B}' (failed to set hardware filter to promiscuous mode).</p><p>Please check that "\Device\NPF_{B66567B3-D08F-4A76-B3A7-2D6CFC9E6C4B}" is the proper interface.</p><p>Help can be found at:</p><pre><code>   http://wiki.wireshark.org/WinPcap
   http://wiki.wireshark.org/CaptureSetup</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hardware" rel="tag" title="see questions tagged &#39;hardware&#39;">hardware</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '15, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/cfb67f5a68fe98eeec1ae284ecc561b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="daddyofcody&#39;s gravatar image" /><p><span>daddyofcody</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="daddyofcody has no accepted answers">0%</span></p></div></div><div id="comments-container-47623" class="comments-container"><span id="47624"></span><div id="comment-47624" class="comment"><div id="post-47624-score" class="comment-score"></div><div class="comment-text"><p>What kind of device is it? Some wireless cards do not support promiscuous mode at all.</p></div><div id="comment-47624-info" class="comment-info"><span class="comment-age">(15 Nov '15, 23:26)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-47623" class="comment-tools"></div><div class="clear"></div><div id="comment-47623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

