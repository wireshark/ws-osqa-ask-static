+++
type = "question"
title = "&quot;wireshark had stopped working when filter"
description = '''Hi, I am using wireshark version 1.6.2 with some custom plug ins. many times when i try to apply a filer I receive &quot;wireshark had stopped working&quot;'''
date = "2013-11-18T04:55:00Z"
lastmod = "2013-11-18T10:19:00Z"
weight = 27067
keywords = [ "wireshark_crashed" ]
aliases = [ "/questions/27067" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["wireshark had stopped working when filter](/questions/27067/wireshark-had-stopped-working-when-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27067-score" class="post-score" title="current number of votes">0</div><span id="post-27067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am using wireshark version 1.6.2 with some custom plug ins. many times when i try to apply a filer I receive "wireshark had stopped working"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark_crashed" rel="tag" title="see questions tagged &#39;wireshark_crashed&#39;">wireshark_crashed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '13, 04:55</strong></p><img src="https://secure.gravatar.com/avatar/edec12b5fd9dbf93854ee47d20cabafc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ItamarL&#39;s gravatar image" /><p><span>ItamarL</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ItamarL has no accepted answers">0%</span></p></div></div><div id="comments-container-27067" class="comments-container"><span id="27072"></span><div id="comment-27072" class="comment"><div id="post-27072-score" class="comment-score"></div><div class="comment-text"><p>Some questions:</p><ul><li>did have that problem with plugin all the time or just recently?</li><li>are the filters related to the functionality of the plugins or does it happen with simple standard filters (tcp.stream or ip.addr and the like)?</li></ul></div><div id="comment-27072-info" class="comment-info"><span class="comment-age">(18 Nov '13, 10:19)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27067" class="comment-tools"></div><div class="clear"></div><div id="comment-27067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27068"></span>

<div id="answer-container-27068" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27068-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27068-score" class="post-score" title="current number of votes">0</div><span id="post-27068-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>While this doesn't sound good, the typical hint is: get the latest stable build. 1.6.2 is ancient, so if you're using a prebuilt package you should get an update, or compile from source if there isn't any ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '13, 04:57</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27068" class="comments-container"><span id="27069"></span><div id="comment-27069" class="comment"><div id="post-27069-score" class="comment-score"></div><div class="comment-text"><p>i can't since my plug-ins compiled for this version</p></div><div id="comment-27069-info" class="comment-info"><span class="comment-age">(18 Nov '13, 04:58)</span> <span class="comment-user userinfo">ItamarL</span></div></div><span id="27070"></span><div id="comment-27070" class="comment"><div id="post-27070-score" class="comment-score"></div><div class="comment-text"><p>oh, ok. You need to think about how to get your plug-ins to the latest version then.</p><p>In the meantime, try deleting your profile settings, maybe there is something in the settings that causes this. Best would be to do a backup of the settings files first so that you can restore them if it didn't help.</p><p>Next step would be to do a fresh install of a 1.6.2 version (maybe as a portable version in a parallel install) to verify that it is a problem that 1.6.2 has, and not something your plugins cause.</p></div><div id="comment-27070-info" class="comment-info"><span class="comment-age">(18 Nov '13, 05:01)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-27068" class="comment-tools"></div><div class="clear"></div><div id="comment-27068-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

