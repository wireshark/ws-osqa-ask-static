+++
type = "question"
title = "Capture header compression enabled packets"
description = '''How to get the length of compressed encrypted request header length? When DEFLATE Header compression is enabled. '''
date = "2017-05-04T10:44:00Z"
lastmod = "2017-05-04T10:44:00Z"
weight = 61238
keywords = [ "http.request" ]
aliases = [ "/questions/61238" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture header compression enabled packets](/questions/61238/capture-header-compression-enabled-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61238-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61238-score" class="post-score" title="current number of votes">0</div><span id="post-61238-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to get the length of compressed encrypted request header length? When DEFLATE Header compression is enabled.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http.request" rel="tag" title="see questions tagged &#39;http.request&#39;">http.request</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '17, 10:44</strong></p><img src="https://secure.gravatar.com/avatar/c6fedad9b555138e6410a76477ac5dcd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sandu22&#39;s gravatar image" /><p><span>sandu22</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sandu22 has no accepted answers">0%</span></p></div></div><div id="comments-container-61238" class="comments-container"></div><div id="comment-tools-61238" class="comment-tools"></div><div class="clear"></div><div id="comment-61238-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

