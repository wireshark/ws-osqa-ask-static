+++
type = "question"
title = "UAT - Help - ZigBee"
description = '''Hello Guys, I would like to know if it is possible to add record at the UAT by the dissectors (according to the packets), not only in Edit-&amp;gt;prefs. I want to use in ZigBee, adding Keys automatically, like the command &quot;Transport-Key&quot;, I want to save the key permanently, not only in the current sess...'''
date = "2012-12-18T05:20:00Z"
lastmod = "2012-12-18T05:20:00Z"
weight = 17020
keywords = [ "zigbee", "uat" ]
aliases = [ "/questions/17020" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [UAT - Help - ZigBee](/questions/17020/uat-help-zigbee)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17020-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17020-score" class="post-score" title="current number of votes">0</div><span id="post-17020-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Guys,</p><p>I would like to know if it is possible to add record at the UAT by the dissectors (according to the packets), not only in Edit-&gt;prefs. I want to use in ZigBee, adding Keys automatically, like the command "Transport-Key", I want to save the key permanently, not only in the current session.</p><p>I hope to hear from you.</p><p>Regards, Guilherme Zanforlin.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zigbee" rel="tag" title="see questions tagged &#39;zigbee&#39;">zigbee</span> <span class="post-tag tag-link-uat" rel="tag" title="see questions tagged &#39;uat&#39;">uat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '12, 05:20</strong></p><img src="https://secure.gravatar.com/avatar/e79dfa09ddb611b35c50f98bbd119369?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guilherme%20Zanforlin&#39;s gravatar image" /><p><span>Guilherme Za...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guilherme Zanforlin has no accepted answers">0%</span></p></div></div><div id="comments-container-17020" class="comments-container"></div><div id="comment-tools-17020" class="comment-tools"></div><div class="clear"></div><div id="comment-17020-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

