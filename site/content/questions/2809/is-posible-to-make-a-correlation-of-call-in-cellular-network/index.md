+++
type = "question"
title = "Is Posible to make a Correlation of Call in Cellular Network???"
description = '''Hi Gurus of wireshark,  I have a quetion (doubt). I need to know if is possible to make a correlation Call in a Cellular Network.  For example I capture the Call in one point (For example between the BSC and BTS), The other Point could be between BSC and MSC and finally the last point is between the...'''
date = "2011-03-14T14:28:00Z"
lastmod = "2011-03-14T15:08:00Z"
weight = 2809
keywords = [ "callstrace" ]
aliases = [ "/questions/2809" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is Posible to make a Correlation of Call in Cellular Network???](/questions/2809/is-posible-to-make-a-correlation-of-call-in-cellular-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2809-score" class="post-score" title="current number of votes">0</div><span id="post-2809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Gurus of wireshark, I have a quetion (doubt). I need to know if is possible to make a correlation Call in a Cellular Network. For example I capture the Call in one point (For example between the BSC and BTS), The other Point could be between BSC and MSC and finally the last point is between the MSC and the Gateway of PSTN.</p><p>As you can see you capture the Call in Three point. Cellular phone -- BTS -1- BCS -2- MSC -2- gateway of the PSTN</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-callstrace" rel="tag" title="see questions tagged &#39;callstrace&#39;">callstrace</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '11, 14:28</strong></p><img src="https://secure.gravatar.com/avatar/e9e556dc3944dc282fb281a7c18851be?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andresguillen&#39;s gravatar image" /><p><span>andresguillen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andresguillen has no accepted answers">0%</span></p></div></div><div id="comments-container-2809" class="comments-container"></div><div id="comment-tools-2809" class="comment-tools"></div><div class="clear"></div><div id="comment-2809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2810"></span>

<div id="answer-container-2810" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2810-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2810-score" class="post-score" title="current number of votes">0</div><span id="post-2810-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It is possible: in fact that is what the MATE feature was created for, see:</p><p><a href="http://wiki.wireshark.org/Mate/Accident">http://wiki.wireshark.org/Mate/Accident</a></p><p>[Update] Don't forget to drop by and Accept this answer if it answered your question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Mar '11, 15:08</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Mar '12, 07:07</strong> </span></p></div></div><div id="comments-container-2810" class="comments-container"></div><div id="comment-tools-2810" class="comment-tools"></div><div class="clear"></div><div id="comment-2810-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

