+++
type = "question"
title = "Can&#x27;t install wireshark on Mac - OS X says it&#x27;s from an unidentified developer"
description = '''I downloaded wireshark for my MacBook Pro but when i double click to install it i get the following message: Wireshark 1.10.3 Intel 64.pkg” can’t be opened because it is from an unidentified developer. “Wireshark 1.10.3 Intel 64.pkg” is on the disk image “Wireshark 1.10.3 Intel 64.dmg”. Safari downl...'''
date = "2013-11-09T08:05:00Z"
lastmod = "2013-11-11T11:37:00Z"
weight = 26804
keywords = [ "mac", "macintosh" ]
aliases = [ "/questions/26804" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Can't install wireshark on Mac - OS X says it's from an unidentified developer](/questions/26804/cant-install-wireshark-on-mac-os-x-says-its-from-an-unidentified-developer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26804-score" class="post-score" title="current number of votes">0</div><span id="post-26804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded wireshark for my MacBook Pro but when i double click to install it i get the following message:</p><p>Wireshark 1.10.3 Intel 64.pkg” can’t be opened because it is from an unidentified developer. “Wireshark 1.10.3 Intel 64.pkg” is on the disk image “Wireshark 1.10.3 Intel 64.dmg”. Safari downloaded this disk image yesterday at 3:55 PM from www.wireshark.org.**</p><p>Any idea why it is not working and how to fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-macintosh" rel="tag" title="see questions tagged &#39;macintosh&#39;">macintosh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Nov '13, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/ed05e850518ffc10ba5dbdd8424ee2b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chouaiboo15&#39;s gravatar image" /><p><span>chouaiboo15</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chouaiboo15 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Nov '13, 15:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-26804" class="comments-container"></div><div id="comment-tools-26804" class="comment-tools"></div><div class="clear"></div><div id="comment-26804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="26809"></span>

<div id="answer-container-26809" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26809-score" class="post-score" title="current number of votes">0</div><span id="post-26809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Any idea why it is not working</p></blockquote><p>the package is not signed. See the answer to a similar question.</p><blockquote><p><a href="http://ask.wireshark.org/questions/26440/why-is-wireshark-not-identified-as-a-safe-developer-on-os-x">http://ask.wireshark.org/questions/26440/why-is-wireshark-not-identified-as-a-safe-developer-on-os-x</a></p></blockquote><p>how to fix it:</p><blockquote><p><a href="http://ask.wireshark.org/questions/26325/unable-to-install-wireshark-on-os-x-mavericks">http://ask.wireshark.org/questions/26325/unable-to-install-wireshark-on-os-x-mavericks</a><br />
<a href="http://techinstruct.com/wiki/Mac_OS_X_-_Install_WireShark_on_Mac_OS_X_Mountain_Lion">http://techinstruct.com/wiki/Mac_OS_X_-_Install_WireShark_on_Mac_OS_X_Mountain_Lion</a><br />
<a href="http://blog.israeltorres.org/home/write-ups/installingwiresharkonmacosx108mountainlion">http://blog.israeltorres.org/home/write-ups/installingwiresharkonmacosx108mountainlion</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Nov '13, 15:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-26809" class="comments-container"></div><div id="comment-tools-26809" class="comment-tools"></div><div class="clear"></div><div id="comment-26809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="26821"></span>

<div id="answer-container-26821" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26821-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26821-score" class="post-score" title="current number of votes">0</div><span id="post-26821-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The 64-bit development installers at <a href="http://www.wireshark.org/download/automated/osx/">http://www.wireshark.org/download/automated/osx/</a> should now be properly signed and work on OS X 10.6 - 10.9.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '13, 14:45</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span> </br></p></div></div><div id="comments-container-26821" class="comments-container"><span id="26851"></span><div id="comment-26851" class="comment"><div id="post-26851-score" class="comment-score"></div><div class="comment-text"><p><em>Gerald Combs</em>*</p><p>Thanks for the link but the Mac-Version is missing "Telephony, Tools and Internals"...am I doing something wrong..</p></div><div id="comment-26851-info" class="comment-info"><span class="comment-age">(11 Nov '13, 11:34)</span> <span class="comment-user userinfo">fsarwary</span></div></div><span id="26852"></span><div id="comment-26852" class="comment"><div id="post-26852-score" class="comment-score"></div><div class="comment-text"><p>We're porting the old GTK+-based interface to a new Qt-based one. The items under the Telephony, Tools, and Internals menus haven't been ported yet. You can track our progress at <a href="http://wiki.wireshark.org/Development/QtShark">http://wiki.wireshark.org/Development/QtShark</a></p></div><div id="comment-26852-info" class="comment-info"><span class="comment-age">(11 Nov '13, 11:37)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-26821" class="comment-tools"></div><div class="clear"></div><div id="comment-26821-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

