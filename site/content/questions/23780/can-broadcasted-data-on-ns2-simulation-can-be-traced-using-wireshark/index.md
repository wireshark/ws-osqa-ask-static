+++
type = "question"
title = "Can broadcasted data on NS2 ( Simulation ) can be traced using Wireshark?"
description = '''I am new to wireshark. Is it possible to capture the packets broadcast on NS2 (Network Simulator 2). If Yes then how? Suppose I am broadcasting a message which can be seen on port no 42. Protocol used is DumbAgent.'''
date = "2013-08-14T12:02:00Z"
lastmod = "2013-08-14T12:02:00Z"
weight = 23780
keywords = [ "broadcast", "ns2", "port", "simulator" ]
aliases = [ "/questions/23780" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can broadcasted data on NS2 ( Simulation ) can be traced using Wireshark?](/questions/23780/can-broadcasted-data-on-ns2-simulation-can-be-traced-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23780-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23780-score" class="post-score" title="current number of votes">0</div><span id="post-23780-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to wireshark.</p><p>Is it possible to capture the packets broadcast on NS2 (Network Simulator 2). If Yes then how?</p><p>Suppose I am broadcasting a message which can be seen on port no 42. Protocol used is DumbAgent.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadcast" rel="tag" title="see questions tagged &#39;broadcast&#39;">broadcast</span> <span class="post-tag tag-link-ns2" rel="tag" title="see questions tagged &#39;ns2&#39;">ns2</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-simulator" rel="tag" title="see questions tagged &#39;simulator&#39;">simulator</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Aug '13, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/19f3394aff4576c3ee6cb3ba2554476e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lalle&#39;s gravatar image" /><p><span>Lalle</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lalle has no accepted answers">0%</span></p></div></div><div id="comments-container-23780" class="comments-container"></div><div id="comment-tools-23780" class="comment-tools"></div><div class="clear"></div><div id="comment-23780-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

