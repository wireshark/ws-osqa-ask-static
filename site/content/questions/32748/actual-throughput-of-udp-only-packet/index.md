+++
type = "question"
title = "Actual throughput of UDP only packet"
description = '''What gives the actual throughput of UDP only packet... Frame length or capture length or total length? please explain if possible thanks'''
date = "2014-05-13T00:18:00Z"
lastmod = "2014-05-13T00:18:00Z"
weight = 32748
keywords = [ "udp", "throughput" ]
aliases = [ "/questions/32748" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Actual throughput of UDP only packet](/questions/32748/actual-throughput-of-udp-only-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32748-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32748-score" class="post-score" title="current number of votes">0</div><span id="post-32748-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What gives the actual throughput of UDP only packet...</p><p>Frame length or capture length or total length?</p><p>please explain if possible</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '14, 00:18</strong></p><img src="https://secure.gravatar.com/avatar/f06f6b3ad79b8afedef1058c188cc863?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lte007&#39;s gravatar image" /><p><span>lte007</span><br />
<span class="score" title="41 reputation points">41</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lte007 has one accepted answer">100%</span></p></div></div><div id="comments-container-32748" class="comments-container"></div><div id="comment-tools-32748" class="comment-tools"></div><div class="clear"></div><div id="comment-32748-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

