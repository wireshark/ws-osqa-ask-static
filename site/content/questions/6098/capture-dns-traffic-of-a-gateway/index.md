+++
type = "question"
title = "capture DNS traffic of a gateway"
description = '''hello!! for a project i have to capture traffic a gateway(DNS traffic).Sb offered me to use wireshark.could u please helpabout this.how i can do this.does wireshark anyoption to do this.I will be verry glad if you answer me.thank u :)'''
date = "2011-09-05T11:50:00Z"
lastmod = "2012-01-15T21:08:00Z"
weight = 6098
keywords = [ "wireshark" ]
aliases = [ "/questions/6098" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capture DNS traffic of a gateway](/questions/6098/capture-dns-traffic-of-a-gateway)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6098-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6098-score" class="post-score" title="current number of votes">0</div><span id="post-6098-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello!! for a project i have to capture traffic a gateway(DNS traffic).Sb offered me to use wireshark.could u please helpabout this.how i can do this.does wireshark anyoption to do this.I will be verry glad if you answer me.thank u :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '11, 11:50</strong></p><img src="https://secure.gravatar.com/avatar/f66ee1b7fb150d2a90025ad945cd6e1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Elahe&#39;s gravatar image" /><p><span>Elahe</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Elahe has no accepted answers">0%</span></p></div></div><div id="comments-container-6098" class="comments-container"><span id="6101"></span><div id="comment-6101" class="comment"><div id="post-6101-score" class="comment-score">1</div><div class="comment-text"><p>Have you read any of the documentation?</p></div><div id="comment-6101-info" class="comment-info"><span class="comment-age">(05 Sep '11, 13:53)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="6102"></span><div id="comment-6102" class="comment"><div id="post-6102-score" class="comment-score"></div><div class="comment-text"><p>yes i have looked them but I dont know where exactly can find the answer :(.Do you know where I can find??or could u plz give the link to the page which contains the answer.thank u :)</p></div><div id="comment-6102-info" class="comment-info"><span class="comment-age">(05 Sep '11, 14:37)</span> <span class="comment-user userinfo">Elahe</span></div></div></div><div id="comment-tools-6098" class="comment-tools"></div><div class="clear"></div><div id="comment-6098-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8399"></span>

<div id="answer-container-8399" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8399-score" class="post-score" title="current number of votes">0</div><span id="post-8399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I see this message is from September, so I'm sure your class project is now over. But to capture DNS traffic use display filter udp.port eq 53 || tcp.port eq 53.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '12, 21:08</strong></p><img src="https://secure.gravatar.com/avatar/1f3966b6e9de3a63326e2d3fd51c8c04?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John_Modlin&#39;s gravatar image" /><p><span>John_Modlin</span><br />
<span class="score" title="120 reputation points">120</span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John_Modlin has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-8399" class="comments-container"></div><div id="comment-tools-8399" class="comment-tools"></div><div class="clear"></div><div id="comment-8399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

