+++
type = "question"
title = "Install Wireshark / WinPcap on Windows 8"
description = '''I have a laptop with Win 8 64 bit. When I tried to install I got a WinPcap error. Any recommendations...'''
date = "2013-01-01T13:38:00Z"
lastmod = "2013-01-01T14:28:00Z"
weight = 17374
keywords = [ "winpcap", "windows8" ]
aliases = [ "/questions/17374" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Install Wireshark / WinPcap on Windows 8](/questions/17374/install-wireshark-winpcap-on-windows-8)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17374-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17374-score" class="post-score" title="current number of votes">0</div><span id="post-17374-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a laptop with Win 8 64 bit. When I tried to install I got a WinPcap error. Any recommendations...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jan '13, 13:38</strong></p><img src="https://secure.gravatar.com/avatar/1a3e37e40019717763a8bd7df90f7e85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="japarke&#39;s gravatar image" /><p><span>japarke</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="japarke has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Jan '13, 05:17</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-17374" class="comments-container"></div><div id="comment-tools-17374" class="comment-tools"></div><div class="clear"></div><div id="comment-17374-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17375"></span>

<div id="answer-container-17375" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17375-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17375-score" class="post-score" title="current number of votes">1</div><span id="post-17375-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>as you did not mention the error, I assume it's the "standard" problem of WinPcap on Windows 8.</p><p>Most certainly, you need to install WinPcap in "compatibility" mode. See the following questions here</p><blockquote><p><code>http://ask.wireshark.org/questions/12956/install-on-windows-8-winpcap-issue</code><br />
<code>http://ask.wireshark.org/questions/7425/windows-8-support</code><br />
</p></blockquote><p>and some other links.</p><blockquote><p><code>http://www.acumen-corp.com/Blog/tabid/298/entryid/56/How-to-Install-WinPCap-for-Wireshark-on-Windows-Server-2012-and-Windows-8.aspx</code><br />
<code>http://microsoftarena.net/winpcap-on-windows-8-how-to/</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jan '13, 14:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Jan '13, 14:28</strong> </span></p></div></div><div id="comments-container-17375" class="comments-container"></div><div id="comment-tools-17375" class="comment-tools"></div><div class="clear"></div><div id="comment-17375-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

