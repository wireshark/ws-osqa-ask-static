+++
type = "question"
title = "How to open wireshark in unprivileged mode from command line?"
description = '''I am working on a machine without root access. When I open wireshark on it I get the following pop up: http://tinypic.com/r/2v9x6qg/5 Now when I remotely login to the machine using SSH, it asks me for the root password and does not allow me to run wireshark in unpriviliged mode. Is there a command l...'''
date = "2012-04-15T10:01:00Z"
lastmod = "2012-04-15T11:34:00Z"
weight = 10161
keywords = [ "startup" ]
aliases = [ "/questions/10161" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to open wireshark in unprivileged mode from command line?](/questions/10161/how-to-open-wireshark-in-unprivileged-mode-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10161-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10161-score" class="post-score" title="current number of votes">0</div><span id="post-10161-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working on a machine without root access. When I open wireshark on it I get the following pop up: <a href="http://tinypic.com/r/2v9x6qg/5">http://tinypic.com/r/2v9x6qg/5</a></p><p>Now when I remotely login to the machine using SSH, it asks me for the root password and does not allow me to run wireshark in unpriviliged mode. Is there a command line option which can help me do so?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '12, 10:01</strong></p><img src="https://secure.gravatar.com/avatar/e26c7ebb23eae3f6b8a22c85915807f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bruce&#39;s gravatar image" /><p><span>Bruce</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bruce has no accepted answers">0%</span></p></div></div><div id="comments-container-10161" class="comments-container"></div><div id="comment-tools-10161" class="comment-tools"></div><div class="clear"></div><div id="comment-10161-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10163"></span>

<div id="answer-container-10163" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10163-score" class="post-score" title="current number of votes">1</div><span id="post-10163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're running Ubuntu (or another Debian variant), you can configure the OS to allow unprivileged access for Wireshark (as described in another <a href="http://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed/7546">post</a>).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '12, 11:34</strong></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworld has 28 accepted answers">28%</span></p></div></div><div id="comments-container-10163" class="comments-container"></div><div id="comment-tools-10163" class="comment-tools"></div><div class="clear"></div><div id="comment-10163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

