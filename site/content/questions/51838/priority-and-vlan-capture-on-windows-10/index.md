+++
type = "question"
title = "Priority and VLAN capture on Windows 10"
description = '''I just want to be sure here. I will be running Wireshark on Windows 10, using the Realtek PCIe GBE controller. Should I have Priority and VLAN Enabled, or Disabled, in order to capture VLAN information? I have seen both here. Thanks'''
date = "2016-04-21T06:26:00Z"
lastmod = "2016-04-21T06:26:00Z"
weight = 51838
keywords = [ "capture", "vlan", "windows10" ]
aliases = [ "/questions/51838" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Priority and VLAN capture on Windows 10](/questions/51838/priority-and-vlan-capture-on-windows-10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51838-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51838-score" class="post-score" title="current number of votes">0</div><span id="post-51838-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just want to be sure here. I will be running Wireshark on Windows 10, using the Realtek PCIe GBE controller. Should I have Priority and VLAN Enabled, or Disabled, in order to capture VLAN information? I have seen both here.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Apr '16, 06:26</strong></p><img src="https://secure.gravatar.com/avatar/64884cb419bb3531b03e82f593b93eac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skeating&#39;s gravatar image" /><p><span>skeating</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skeating has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Apr '16, 01:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-51838" class="comments-container"></div><div id="comment-tools-51838" class="comment-tools"></div><div class="clear"></div><div id="comment-51838-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

