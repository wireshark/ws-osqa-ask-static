+++
type = "question"
title = "[closed] Bluetooth - How to capture network performances statistic (such as signal strength) using Wireshark"
description = '''Hi, I need to capture wireless network performances statistic of Bluetooth (such as signal strength, throughput, etc) using Wireshark and Windows 7x64. May i know how to setup Wireshark to do this? Is there also modules that i could buy that will produce such reports? It is possible to do it on my o...'''
date = "2014-06-17T13:35:00Z"
lastmod = "2014-06-17T14:45:00Z"
weight = 33910
keywords = [ "wireless", "signal", "strength", "setup", "bluetooth" ]
aliases = [ "/questions/33910" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Bluetooth - How to capture network performances statistic (such as signal strength) using Wireshark](/questions/33910/bluetooth-how-to-capture-network-performances-statistic-such-as-signal-strength-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33910-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33910-score" class="post-score" title="current number of votes">0</div><span id="post-33910-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I need to capture wireless <strong>network performances statistic of Bluetooth</strong> (such as signal strength, throughput, etc) using <strong>Wireshark and Windows 7x64</strong>. May i know how to setup Wireshark to do this? Is there also modules that i could buy that will produce such reports? It is possible to do it on my own?</p><p>I am pretty new to Wireshark and hence I will appreciate your guidance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-signal" rel="tag" title="see questions tagged &#39;signal&#39;">signal</span> <span class="post-tag tag-link-strength" rel="tag" title="see questions tagged &#39;strength&#39;">strength</span> <span class="post-tag tag-link-setup" rel="tag" title="see questions tagged &#39;setup&#39;">setup</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '14, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/287024c5a663404a19410f0e8a7df8e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kohck&#39;s gravatar image" /><p><span>kohck</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kohck has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Jun '14, 20:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-33910" class="comments-container"><span id="33911"></span><div id="comment-33911" class="comment"><div id="post-33911-score" class="comment-score"></div><div class="comment-text"><p><span>@kohck</span>: Why do you open new questions <strong>for the same topic</strong>, while your other questions have been answered?</p><blockquote><p><a href="http://ask.wireshark.org/questions/33906/bluetooth-how-to-capture-network-performances-statistic-such-as-signal-strength-using-wireshark">http://ask.wireshark.org/questions/33906/bluetooth-how-to-capture-network-performances-statistic-such-as-signal-strength-using-wireshark</a></p></blockquote></div><div id="comment-33911-info" class="comment-info"><span class="comment-age">(17 Jun '14, 14:45)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33910" class="comment-tools"></div><div class="clear"></div><div id="comment-33910-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question: http://ask.wireshark.org/questions/33906/bluetooth-how-to-capture-network-performances-statistic-such-as-signal-strength-using-wireshark" by Kurt Knochner 17 Jun '14, 14:44

</div>

</div>

</div>

