+++
type = "question"
title = "New feature - Find Previous in Follow Stream"
description = '''Hello, I frequently use Wireshark to examine security alerts at my job and wanted to make a request for the developers to create a &quot;find previous&quot; button when using the Follow (TCP,UDP,SSL) Stream feature. It&#x27;s nice to go through and search for certain key words, but as an example, I will look for J...'''
date = "2016-07-23T15:01:00Z"
lastmod = "2016-07-23T17:00:00Z"
weight = 54258
keywords = [ "search", "find", "previous" ]
aliases = [ "/questions/54258" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [New feature - Find Previous in Follow Stream](/questions/54258/new-feature-find-previous-in-follow-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54258-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54258-score" class="post-score" title="current number of votes">0</div><span id="post-54258-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I frequently use Wireshark to examine security alerts at my job and wanted to make a request for the developers to create a "find previous" button when using the Follow (TCP,UDP,SSL) Stream feature. It's nice to go through and search for certain key words, but as an example, I will look for Javascript and search for .js and see one match in the client's GET request, and then click next and jump way down into the server response and not be able to quickly go back to the last match. This would be a huge help to the many folks out there which use the Find feature. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-search" rel="tag" title="see questions tagged &#39;search&#39;">search</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span> <span class="post-tag tag-link-previous" rel="tag" title="see questions tagged &#39;previous&#39;">previous</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '16, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/435ad6abc899016f98afc76f588d8ad1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LazyShark&#39;s gravatar image" /><p><span>LazyShark</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LazyShark has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Jul '16, 16:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-54258" class="comments-container"></div><div id="comment-tools-54258" class="comment-tools"></div><div class="clear"></div><div id="comment-54258-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54259"></span>

<div id="answer-container-54259" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54259-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54259-score" class="post-score" title="current number of votes">0</div><span id="post-54259-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Probably best filed as an enhancement request on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>, so it can be tracked.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jul '16, 17:00</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-54259" class="comments-container"></div><div id="comment-tools-54259" class="comment-tools"></div><div class="clear"></div><div id="comment-54259-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

