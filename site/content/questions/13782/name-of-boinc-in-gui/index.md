+++
type = "question"
title = "name of boinc in gui"
description = '''I need to manage &quot;boinc&quot; under EDIT &amp;gt; PREFERENCE &amp;gt; PROTOCOLS and ANALYZE &amp;gt; ENABLED PROTOCOLS. I can find nothing related to &quot;boinc&quot;, &quot;berkley open ...&quot;, etc. Under what name does &quot;boinc&quot; appear in these two GUI functions? Thanks, David'''
date = "2012-08-20T16:12:00Z"
lastmod = "2012-08-21T09:57:00Z"
weight = 13782
keywords = [ "boinc" ]
aliases = [ "/questions/13782" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [name of boinc in gui](/questions/13782/name-of-boinc-in-gui)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13782-score" class="post-score" title="current number of votes">0</div><span id="post-13782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to manage "boinc" under EDIT &gt; PREFERENCE &gt; PROTOCOLS and ANALYZE &gt; ENABLED PROTOCOLS.</p><p>I can find nothing related to "boinc", "berkley open ...", etc.</p><p>Under what name does "boinc" appear in these two GUI functions?</p><p>Thanks,</p><p>David</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-boinc" rel="tag" title="see questions tagged &#39;boinc&#39;">boinc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '12, 16:12</strong></p><img src="https://secure.gravatar.com/avatar/cd0fc6913aa24cde579ebabcfa0b6b2c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="david8322&#39;s gravatar image" /><p><span>david8322</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="david8322 has no accepted answers">0%</span></p></div></div><div id="comments-container-13782" class="comments-container"></div><div id="comment-tools-13782" class="comment-tools"></div><div class="clear"></div><div id="comment-13782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13783"></span>

<div id="answer-container-13783" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13783-score" class="post-score" title="current number of votes">0</div><span id="post-13783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark does not have a dissector for "boinc".</p><p>What is it about "boinc" that you want to see (manage ?) ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '12, 18:31</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-13783" class="comments-container"></div><div id="comment-tools-13783" class="comment-tools"></div><div class="clear"></div><div id="comment-13783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="13792"></span>

<div id="answer-container-13792" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13792-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13792-score" class="post-score" title="current number of votes">0</div><span id="post-13792-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>BOINC uses HTTP as transport protocol, so you should see quite a lot of data, even without a special dissector.</p><blockquote><p><code>http://www.boinc-wiki.info/Protocol_Overview</code><br />
<code>http://boinc.berkeley.edu/trac/wiki/RpcProtocol</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Aug '12, 09:57</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-13792" class="comments-container"></div><div id="comment-tools-13792" class="comment-tools"></div><div class="clear"></div><div id="comment-13792-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

