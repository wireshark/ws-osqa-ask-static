+++
type = "question"
title = "How to export filtered packets to text automatically"
description = '''to members! I use wireshark for windows(32bit) and ver2.2.4. I know how to export packets pcap-ng/pcap automatically.  capture&amp;gt;option&amp;gt;export we can choose by time or by byte  I would like to know how to export filtered packets to text directly, periodically and automatically. Regards, Light th...'''
date = "2017-02-13T23:14:00Z"
lastmod = "2017-02-13T23:34:00Z"
weight = 59397
keywords = [ "export" ]
aliases = [ "/questions/59397" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to export filtered packets to text automatically](/questions/59397/how-to-export-filtered-packets-to-text-automatically)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59397-score" class="post-score" title="current number of votes">0</div><span id="post-59397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>to members!</p><p>I use wireshark for windows(32bit) and ver2.2.4. I know how to export packets pcap-ng/pcap automatically.</p><blockquote><p>capture&gt;option&gt;export we can choose by time or by byte</p></blockquote><p><strong>I would like to know how to export filtered packets to text directly, periodically and automatically.</strong></p><p>Regards, Light the lightning<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '17, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/1634bd3b8ec82c26229a3c3890c7c7ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="light%20the%20lightning&#39;s gravatar image" /><p><span>light the li...</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="light the lightning has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-59397" class="comments-container"></div><div id="comment-tools-59397" class="comment-tools"></div><div class="clear"></div><div id="comment-59397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59398"></span>

<div id="answer-container-59398" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59398-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59398-score" class="post-score" title="current number of votes">2</div><span id="post-59398-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://www.wireshark.org/docs/man-pages/tshark.html">tshark</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '17, 23:28</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-59398" class="comments-container"><span id="59399"></span><div id="comment-59399" class="comment"><div id="post-59399-score" class="comment-score"></div><div class="comment-text"><p>I should search more deeply. I try it from now on. Thank you very much.</p></div><div id="comment-59399-info" class="comment-info"><span class="comment-age">(13 Feb '17, 23:34)</span> <span class="comment-user userinfo">light the li...</span></div></div></div><div id="comment-tools-59398" class="comment-tools"></div><div class="clear"></div><div id="comment-59398-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

