+++
type = "question"
title = "What do the &quot;Not Support&quot; mean?"
description = '''Hi, I catch the pcap file with the wifi card and use wireshark to dissector. The timing measurement in the Extended Capabilities shows &quot;Not Support&quot;. Does it mean the device (e.g. AP or Wifi Card) don&#x27;t support the capability or the wireshark don&#x27;t support the capability? Thanks Bonnie'''
date = "2014-07-21T20:34:00Z"
lastmod = "2014-07-24T03:47:00Z"
weight = 34808
keywords = [ "dissector", "802.11" ]
aliases = [ "/questions/34808" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What do the "Not Support" mean?](/questions/34808/what-do-the-not-support-mean)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34808-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34808-score" class="post-score" title="current number of votes">0</div><span id="post-34808-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I catch the pcap file with the wifi card and use wireshark to dissector. The timing measurement in the Extended Capabilities shows "Not Support". Does it mean the device (e.g. AP or Wifi Card) don't support the capability or the wireshark don't support the capability?</p><p>Thanks Bonnie</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '14, 20:34</strong></p><img src="https://secure.gravatar.com/avatar/b195c8cedbec55fa1e900ec6f5fb4130?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bonnie&#39;s gravatar image" /><p><span>Bonnie</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bonnie has no accepted answers">0%</span></p></div></div><div id="comments-container-34808" class="comments-container"></div><div id="comment-tools-34808" class="comment-tools"></div><div class="clear"></div><div id="comment-34808-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34856"></span>

<div id="answer-container-34856" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34856-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34856-score" class="post-score" title="current number of votes">1</div><span id="post-34856-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It means the device doesn't support the capability. See <a href="http://standards.ieee.org/getieee802/download/802.11-2012.pdf">IEEE Std 802.11-2012</a>, section 8.4.2.29 "Extended Capabilities element".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jul '14, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-34856" class="comments-container"><span id="34871"></span><div id="comment-34871" class="comment"><div id="post-34871-score" class="comment-score"></div><div class="comment-text"><p>Thank you!</p></div><div id="comment-34871-info" class="comment-info"><span class="comment-age">(24 Jul '14, 03:47)</span> <span class="comment-user userinfo">Bonnie</span></div></div></div><div id="comment-tools-34856" class="comment-tools"></div><div class="clear"></div><div id="comment-34856-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

