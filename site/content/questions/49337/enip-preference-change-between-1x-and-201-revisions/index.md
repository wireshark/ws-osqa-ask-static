+++
type = "question"
title = "ENIP preference change between 1.x and 2.01 revisions"
description = '''Hello, In versions prior to 2.01, in the Protocol preferences for ENIP, there used to be a dropdown box to select an option for &quot;Dissect unidentified I/O traffic as:&quot; .. Regular Ethernet/IP I/O data,CIP Safety, CIP Motion. This option is no longer present, thus I cannot decode CIP traffic correctly....'''
date = "2016-01-18T15:10:00Z"
lastmod = "2016-01-19T08:06:00Z"
weight = 49337
keywords = [ "enip", "cip", "wireshark-2.0", "wireshark" ]
aliases = [ "/questions/49337" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [ENIP preference change between 1.x and 2.01 revisions](/questions/49337/enip-preference-change-between-1x-and-201-revisions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49337-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49337-score" class="post-score" title="current number of votes">0</div><span id="post-49337-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>In versions prior to 2.01, in the Protocol preferences for ENIP, there used to be a dropdown box to select an option for "Dissect unidentified I/O traffic as:" .. Regular Ethernet/IP I/O data,CIP Safety, CIP Motion.</p><p>This option is no longer present, thus I cannot decode CIP traffic correctly.</p><p>Any help figuring this out would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-enip" rel="tag" title="see questions tagged &#39;enip&#39;">enip</span> <span class="post-tag tag-link-cip" rel="tag" title="see questions tagged &#39;cip&#39;">cip</span> <span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '16, 15:10</strong></p><img src="https://secure.gravatar.com/avatar/c6c05d2e733caef53246a95e14ab9e27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mbruni&#39;s gravatar image" /><p><span>mbruni</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mbruni has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jan '16, 15:11</strong> </span></p></div></div><div id="comments-container-49337" class="comments-container"></div><div id="comment-tools-49337" class="comment-tools"></div><div class="clear"></div><div id="comment-49337-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49350"></span>

<div id="answer-container-49350" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49350-score" class="post-score" title="current number of votes">1</div><span id="post-49350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mbruni has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think this is now covered by "Decode as".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '16, 21:31</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-49350" class="comments-container"><span id="49380"></span><div id="comment-49380" class="comment"><div id="post-49380-score" class="comment-score"></div><div class="comment-text"><p>Perfect, thanks for the help!</p></div><div id="comment-49380-info" class="comment-info"><span class="comment-age">(19 Jan '16, 08:06)</span> <span class="comment-user userinfo">mbruni</span></div></div></div><div id="comment-tools-49350" class="comment-tools"></div><div class="clear"></div><div id="comment-49350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

