+++
type = "question"
title = "No WIreless interfaces present in the Capture Options window"
description = '''Hi I am running Wireshark on the machine connected to wifi router via wire. Except for that there are 2-3 other machines in LAN present on wifi. However no wifi interface is available to capture from in the Wireshark. Anyone knows why? Cheers'''
date = "2014-01-19T05:51:00Z"
lastmod = "2014-01-19T15:16:00Z"
weight = 29011
keywords = [ "interface", "capture", "wifi" ]
aliases = [ "/questions/29011" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No WIreless interfaces present in the Capture Options window](/questions/29011/no-wireless-interfaces-present-in-the-capture-options-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29011-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29011-score" class="post-score" title="current number of votes">0</div><span id="post-29011-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am running Wireshark on the machine connected to wifi router via wire. Except for that there are 2-3 other machines in LAN present on wifi. However no wifi interface is available to capture from in the Wireshark. Anyone knows why?</p><p>Cheers</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '14, 05:51</strong></p><img src="https://secure.gravatar.com/avatar/96bdd65114b8617e2513202ca9c6e008?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jluke&#39;s gravatar image" /><p><span>Jluke</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jluke has no accepted answers">0%</span></p></div></div><div id="comments-container-29011" class="comments-container"><span id="29018"></span><div id="comment-29018" class="comment"><div id="post-29018-score" class="comment-score"></div><div class="comment-text"><p>What version of what operating system is on the machine running Wireshark?</p></div><div id="comment-29018-info" class="comment-info"><span class="comment-age">(19 Jan '14, 15:16)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-29011" class="comment-tools"></div><div class="clear"></div><div id="comment-29011-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

