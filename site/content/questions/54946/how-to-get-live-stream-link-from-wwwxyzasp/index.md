+++
type = "question"
title = "how to get live stream link from www.xyz.asp"
description = '''I am a newbie in this stream and using wireshark . Actually i wanted the live stream link from the link i have mentioned below for implementing it in vitamio library android. So is there any possible way to extract live stream link from the link.I wanted the link in this format:- &quot;rtsp://184.72.239....'''
date = "2016-08-18T05:54:00Z"
lastmod = "2016-08-18T05:54:00Z"
weight = 54946
keywords = [ "live", "wireshark-2.0", "stream" ]
aliases = [ "/questions/54946" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to get live stream link from www.xyz.asp](/questions/54946/how-to-get-live-stream-link-from-wwwxyzasp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54946-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54946-score" class="post-score" title="current number of votes">0</div><span id="post-54946-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a newbie in this stream and using wireshark . Actually i wanted the live stream link from the link i have mentioned below for implementing it in vitamio library android. So is there any possible way to extract live stream link from the link.I wanted the link in this format:- "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov" .Sorry for bad English. Thank you. <a href="http://www.siddhivinayak.org/virtual_darshan.asp">here's my live stream</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-live" rel="tag" title="see questions tagged &#39;live&#39;">live</span> <span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '16, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/3470c5e7ecc8ddf6ded6f4c9f9227d83?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bhavin321&#39;s gravatar image" /><p><span>Bhavin321</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bhavin321 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Aug '16, 05:55</strong> </span></p></div></div><div id="comments-container-54946" class="comments-container"></div><div id="comment-tools-54946" class="comment-tools"></div><div class="clear"></div><div id="comment-54946-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

