+++
type = "question"
title = "Version 1.7/64 Bit issue on W7 pc"
description = '''I have downloaded the new 1.7 for 64 bit machines and when running the app it showed 1.4.6 32 bit was there instead. I loaded the 1.6.1 / 64bit stable and it took fine. Any ideas? My OS = W7Pro'''
date = "2011-08-05T10:57:00Z"
lastmod = "2012-03-04T03:31:00Z"
weight = 5534
keywords = [ "windows7", "64-bit" ]
aliases = [ "/questions/5534" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Version 1.7/64 Bit issue on W7 pc](/questions/5534/version-1764-bit-issue-on-w7-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5534-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5534-score" class="post-score" title="current number of votes">0</div><span id="post-5534-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have downloaded the new 1.7 for 64 bit machines and when running the app it showed 1.4.6 32 bit was there instead. I loaded the 1.6.1 / 64bit stable and it took fine. Any ideas? My OS = W7Pro</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '11, 10:57</strong></p><img src="https://secure.gravatar.com/avatar/f797bdc41d990dca073837114e048b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EricKnaus&#39;s gravatar image" /><p><span>EricKnaus</span><br />
<span class="score" title="46 reputation points">46</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EricKnaus has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-5534" class="comments-container"><span id="5541"></span><div id="comment-5541" class="comment"><div id="post-5541-score" class="comment-score"></div><div class="comment-text"><p>There hasn't been an official 1.7 unstable release made yet, only the automated installers, so presumably you downloaded it from <a href="https://www.wireshark.org/download/automated/win64/">https://www.wireshark.org/download/automated/win64/</a>? If so, which specific SVN version was it?</p></div><div id="comment-5541-info" class="comment-info"><span class="comment-age">(05 Aug '11, 19:01)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="5548"></span><div id="comment-5548" class="comment"><div id="post-5548-score" class="comment-score"></div><div class="comment-text"><p>svn 's = 38195 &amp; 38338</p></div><div id="comment-5548-info" class="comment-info"><span class="comment-age">(06 Aug '11, 17:45)</span> <span class="comment-user userinfo">EricKnaus</span></div></div><span id="5549"></span><div id="comment-5549" class="comment"><div id="post-5549-score" class="comment-score"></div><div class="comment-text"><p>just downloaded SVN 38382 and that took! I looked at the other 2 again and they still go in as listed above. Very Strange ..... Eric</p></div><div id="comment-5549-info" class="comment-info"><span class="comment-age">(06 Aug '11, 17:50)</span> <span class="comment-user userinfo">EricKnaus</span></div></div><span id="5550"></span><div id="comment-5550" class="comment"><div id="post-5550-score" class="comment-score"></div><div class="comment-text"><p>Hmm??? Well, they are unstable releases after all :)</p></div><div id="comment-5550-info" class="comment-info"><span class="comment-age">(06 Aug '11, 18:13)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="5551"></span><div id="comment-5551" class="comment"><div id="post-5551-score" class="comment-score"></div><div class="comment-text"><p>Are both versions (32bit and 64bit) installed and are you perhaps using a shortcut to the 32bit version (C:\Program Files (x86)\Wireshark)?</p></div><div id="comment-5551-info" class="comment-info"><span class="comment-age">(07 Aug '11, 00:44)</span> <span class="comment-user userinfo">joke</span></div></div><span id="5564"></span><div id="comment-5564" class="comment not_top_scorer"><div id="post-5564-score" class="comment-score"></div><div class="comment-text"><p>No. This s a new PC - no previous Wireshark versions installaed at all. Not sure what to make of it either way ...</p></div><div id="comment-5564-info" class="comment-info"><span class="comment-age">(07 Aug '11, 21:16)</span> <span class="comment-user userinfo">EricKnaus</span></div></div></div><div id="comment-tools-5534" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-5534-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9342"></span>

<div id="answer-container-9342" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9342-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9342-score" class="post-score" title="current number of votes">0</div><span id="post-9342-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Let's assume there was a glitch in the automated build system for the 64-bit versions...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Mar '12, 03:31</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-9342" class="comments-container"></div><div id="comment-tools-9342" class="comment-tools"></div><div class="clear"></div><div id="comment-9342-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

