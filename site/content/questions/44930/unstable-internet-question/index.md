+++
type = "question"
title = "[closed] Unstable Internet Question"
description = '''One of my clients have a host/remote setup, meaning location1 is where the server along with a few workstations exists and at location2 where another few workstations exists. All workstations are running Windows 7/8 and the server is running Windows Server 2008. The workstations at location1 have no...'''
date = "2015-08-08T12:01:00Z"
lastmod = "2015-08-08T12:01:00Z"
weight = 44930
keywords = [ "connection_reset", "tcp" ]
aliases = [ "/questions/44930" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Unstable Internet Question](/questions/44930/unstable-internet-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44930-score" class="post-score" title="current number of votes">0</div><span id="post-44930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>One of my clients have a host/remote setup, meaning location1 is where the server along with a few workstations exists and at location2 where another few workstations exists. All workstations are running Windows 7/8 and the server is running Windows Server 2008. The workstations at location1 have no issues with the connection being reset due to them being on the same local network. Wireshark shows a handful of duplicate acks and tcp retransmissions right before a tcp reset. The ISP at location1 is experiencing some technical problems and are unsure when they will be resolved. Is there anything I can configure in Windows on these computers to prevent the tcp reset? Maybe in the registry?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection_reset" rel="tag" title="see questions tagged &#39;connection_reset&#39;">connection_reset</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '15, 12:01</strong></p><img src="https://secure.gravatar.com/avatar/16cc2cfaa993762e4fff663f4e929893?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alan1337&#39;s gravatar image" /><p><span>alan1337</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alan1337 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>08 Aug '15, 13:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-44930" class="comments-container"></div><div id="comment-tools-44930" class="comment-tools"></div><div class="clear"></div><div id="comment-44930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 08 Aug '15, 13:01

</div>

</div>

</div>

