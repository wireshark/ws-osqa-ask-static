+++
type = "question"
title = "RTCP QoS Calculation"
description = '''Hi, I need to find average of upstream/downstream - {volume, jitters, packet loss}, for RTCP packets based on protocols MGCP, SKINNY, PTT, and H323. Can anyone help me to find the solution. Thanks in advance'''
date = "2011-08-17T23:04:00Z"
lastmod = "2011-08-17T23:04:00Z"
weight = 5734
keywords = [ "rtcp_qos" ]
aliases = [ "/questions/5734" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTCP QoS Calculation](/questions/5734/rtcp-qos-calculation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5734-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5734-score" class="post-score" title="current number of votes">0</div><span id="post-5734-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I need to find average of upstream/downstream - {volume, jitters, packet loss}, for RTCP packets based on protocols MGCP, SKINNY, PTT, and H323. Can anyone help me to find the solution.</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtcp_qos" rel="tag" title="see questions tagged &#39;rtcp_qos&#39;">rtcp_qos</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Aug '11, 23:04</strong></p><img src="https://secure.gravatar.com/avatar/ccdca0ea73d8c48b2046e6c18909ff5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="syedmansoor&#39;s gravatar image" /><p><span>syedmansoor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="syedmansoor has no accepted answers">0%</span></p></div></div><div id="comments-container-5734" class="comments-container"></div><div id="comment-tools-5734" class="comment-tools"></div><div class="clear"></div><div id="comment-5734-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

