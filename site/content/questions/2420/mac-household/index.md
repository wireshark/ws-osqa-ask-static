+++
type = "question"
title = "Mac Household"
description = '''I have 3 Macs, and Apple Time Machine Router, 2 Ipods and a few cell phones (one with wifi capability). I have also have 2 teenagers. I have been looking every where to find something to monitor the wifi traffic. I want to know when they are chatting and until what time they up chatting. Would this ...'''
date = "2011-02-18T13:43:00Z"
lastmod = "2011-03-09T08:39:00Z"
weight = 2420
keywords = [ "macmom" ]
aliases = [ "/questions/2420" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Mac Household](/questions/2420/mac-household)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2420-score" class="post-score" title="current number of votes">0</div><span id="post-2420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have 3 Macs, and Apple Time Machine Router, 2 Ipods and a few cell phones (one with wifi capability). I have also have 2 teenagers. I have been looking every where to find something to monitor the wifi traffic. I want to know when they are chatting and until what time they up chatting. Would this show me this. It looks a little bit beyond me. There must be other parents who want the same info as me.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macmom" rel="tag" title="see questions tagged &#39;macmom&#39;">macmom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '11, 13:43</strong></p><img src="https://secure.gravatar.com/avatar/d2a7df7cfcee738eecc3c6dc51d972cf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Karen%20Hamilton%20Horrocks&#39;s gravatar image" /><p><span>Karen Hamilt...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Karen Hamilton Horrocks has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-2420" class="comments-container"></div><div id="comment-tools-2420" class="comment-tools"></div><div class="clear"></div><div id="comment-2420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2450"></span>

<div id="answer-container-2450" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2450-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2450-score" class="post-score" title="current number of votes">1</div><span id="post-2450-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might be more interested in NetNanny for Mac OS... http://www.netnanny.com/mac - it's a pretty slick program. You must realize, however, that those clever little buggers... uh er... I mean teenagers... can detect all sorts of monitoring programs.</p><p>Another tool which is provided for free is K9 - http://www1.k9webprotection.com/get-k9-web-protection-free. Kudos to Bluecoat for buying this group and dedicating one person to public awareness and child protection. Very impressive.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Feb '11, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-2450" class="comments-container"></div><div id="comment-tools-2450" class="comment-tools"></div><div class="clear"></div><div id="comment-2450-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="2730"></span>

<div id="answer-container-2730" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2730-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2730-score" class="post-score" title="current number of votes">0</div><span id="post-2730-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>take a look at http://www.spectorsoft.com. kids would not even know they are being watched</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '11, 08:39</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></div></div><div id="comments-container-2730" class="comments-container"></div><div id="comment-tools-2730" class="comment-tools"></div><div class="clear"></div><div id="comment-2730-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

