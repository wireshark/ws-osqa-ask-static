+++
type = "question"
title = "Malformed Packets everywhere"
description = ''' I&#x27;m not sure what to make of these messages. I&#x27;ve only tested it on wireless connections so far. It doesn&#x27;t seem to affect my ability to get any where on the internet, but I cannot log in via VPN to work. Wireshark on the work computer shows no evidence of malformed packets, just a constant stream ...'''
date = "2017-05-10T07:07:00Z"
lastmod = "2017-05-12T12:37:00Z"
weight = 61336
keywords = [ "wireless", "packet", "malformed" ]
aliases = [ "/questions/61336" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed Packets everywhere](/questions/61336/malformed-packets-everywhere)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61336-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61336-score" class="post-score" title="current number of votes">0</div><span id="post-61336-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/wireharkmalformed.png" alt="alt text" /></p><p>I'm not sure what to make of these messages. I've only tested it on wireless connections so far. It doesn't seem to affect my ability to get any where on the internet, but I cannot log in via VPN to work. Wireshark on the work computer shows no evidence of malformed packets, just a constant stream of requests and acknowledgements that go nowhere. Has anyone come across something similar, and how can I fix this?</p><p>Thanks, Chris</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '17, 07:07</strong></p><img src="https://secure.gravatar.com/avatar/3d83f8c8b888c67964120873529131bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Agonnazar&#39;s gravatar image" /><p><span>Agonnazar</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Agonnazar has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61336" class="comments-container"><span id="61337"></span><div id="comment-61337" class="comment"><div id="post-61337-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-61337-info" class="comment-info"><span class="comment-age">(10 May '17, 07:13)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61338"></span><div id="comment-61338" class="comment"><div id="post-61338-score" class="comment-score"></div><div class="comment-text"><p><a href="https://www.cloudshark.org/captures/4aec9497187b">https://www.cloudshark.org/captures/4aec9497187b</a></p><p>Oddly enough, the [malformed packet] tag doesn't show in the saved capture.</p></div><div id="comment-61338-info" class="comment-info"><span class="comment-age">(10 May '17, 07:25)</span> <span class="comment-user userinfo">Agonnazar</span></div></div><span id="61342"></span><div id="comment-61342" class="comment"><div id="post-61342-score" class="comment-score"></div><div class="comment-text"><p>That link takes me to a login page, but I don't have a CloudShark account. You may have to somehow mark that capture as being publicly available even to people without a CloudShark account.</p></div><div id="comment-61342-info" class="comment-info"><span class="comment-age">(10 May '17, 13:25)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="61373"></span><div id="comment-61373" class="comment"><div id="post-61373-score" class="comment-score"></div><div class="comment-text"><p>I updated it.</p></div><div id="comment-61373-info" class="comment-info"><span class="comment-age">(12 May '17, 12:06)</span> <span class="comment-user userinfo">Agonnazar</span></div></div><span id="61374"></span><div id="comment-61374" class="comment"><div id="post-61374-score" class="comment-score"></div><div class="comment-text"><p>What is the Ethernet preference "Assume packets have FCS" set to? If it's on (checked), what happens if you un-check it?</p></div><div id="comment-61374-info" class="comment-info"><span class="comment-age">(12 May '17, 12:37)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-61336" class="comment-tools"></div><div class="clear"></div><div id="comment-61336-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

