+++
type = "question"
title = "how to analyse different http request and response messages"
description = '''I am analyzing different http request and response messages. I have got three different http request messages and i wana see at which time each of these messages are generated and how much bytes they consume? I can see number of bytes against each http request on clicking at any packet but i need so...'''
date = "2012-03-15T02:23:00Z"
lastmod = "2012-03-15T02:23:00Z"
weight = 9551
keywords = [ "request", "http", "response", "wireshark" ]
aliases = [ "/questions/9551" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to analyse different http request and response messages](/questions/9551/how-to-analyse-different-http-request-and-response-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9551-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9551-score" class="post-score" title="current number of votes">0</div><span id="post-9551-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am analyzing different http request and response messages. I have got three different http request messages and i wana see at which time each of these messages are generated and how much bytes they consume? I can see number of bytes against each http request on clicking at any packet but i need some sort of automatic mechanism through which i am able to know number of bytes against different http request messages and same the case with the http response messages. Any help will be really appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '12, 02:23</strong></p><img src="https://secure.gravatar.com/avatar/64faff442179d93597f5e6fee1002ce7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bilal&#39;s gravatar image" /><p><span>Bilal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bilal has no accepted answers">0%</span></p></div></div><div id="comments-container-9551" class="comments-container"></div><div id="comment-tools-9551" class="comment-tools"></div><div class="clear"></div><div id="comment-9551-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

