+++
type = "question"
title = "Filters Code"
description = '''Hi all, can&#x27;t find Filter code, i just want to learn about it. All those filters are saved on a file?   My english is not that good, sorry ! '''
date = "2015-03-12T07:39:00Z"
lastmod = "2015-03-12T11:40:00Z"
weight = 40513
keywords = [ "code", "filters" ]
aliases = [ "/questions/40513" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Filters Code](/questions/40513/filters-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40513-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40513-score" class="post-score" title="current number of votes">0</div><span id="post-40513-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, can't find Filter code, i just want to learn about it. All those filters are saved on a file?</p><p><img src="http://cdn.ttgtmedia.com/digitalguide/images/Misc/WiresharkFilterExpression1_lg.png" alt="alt text" /></p><p>My english is not that good, sorry !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-code" rel="tag" title="see questions tagged &#39;code&#39;">code</span> <span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '15, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/dc1a7419ea7d056fe1024b5f60af29ef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pastilok&#39;s gravatar image" /><p><span>pastilok</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pastilok has no accepted answers">0%</span></p></img></div></div><div id="comments-container-40513" class="comments-container"><span id="40519"></span><div id="comment-40519" class="comment"><div id="post-40519-score" class="comment-score"></div><div class="comment-text"><blockquote><p>can't find Filter code,</p></blockquote><p>which filter code is it you can't find?</p></div><div id="comment-40519-info" class="comment-info"><span class="comment-age">(12 Mar '15, 11:40)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-40513" class="comment-tools"></div><div class="clear"></div><div id="comment-40513-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

