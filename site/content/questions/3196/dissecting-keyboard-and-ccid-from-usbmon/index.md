+++
type = "question"
title = "Dissecting keyboard and CCID from usbmon"
description = '''I&#x27;m looking for a protocol dissector for usbmon that would give human readable output of &quot;key X pressed&quot;, &quot;key X released&quot; when capturing a standard USB keyboard. I hope something like that would already exist. As a bonus I&#x27;d also like to dissect CCID frames from usbmon which probably does not yet e...'''
date = "2011-03-28T23:14:00Z"
lastmod = "2011-03-28T23:14:00Z"
weight = 3196
keywords = [ "keycodes", "usb", "usbmon", "ccid" ]
aliases = [ "/questions/3196" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Dissecting keyboard and CCID from usbmon](/questions/3196/dissecting-keyboard-and-ccid-from-usbmon)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3196-score" class="post-score" title="current number of votes">0</div><span id="post-3196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm looking for a protocol dissector for usbmon that would give human readable output of "key X pressed", "key X released" when capturing a standard USB keyboard. I hope something like that would already exist.</p><p>As a bonus I'd also like to dissect <a href="http://www.usb.org/developers/devclass_docs/DWG_Smart-Card_CCID_Rev110.pdf">CCID</a> frames from usbmon which probably does not yet exist and I would need to develop a custom dissector (to confirm that a smart card reader in fact is <a href="http://martinpaljak.net/2011/03/19/insecure-hp-usb-smart-card-keyboard/">insecure in all conditions</a>). I've skimmed through the README.developer file and will google/read more but any pointers to easy start would be most appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-keycodes" rel="tag" title="see questions tagged &#39;keycodes&#39;">keycodes</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-usbmon" rel="tag" title="see questions tagged &#39;usbmon&#39;">usbmon</span> <span class="post-tag tag-link-ccid" rel="tag" title="see questions tagged &#39;ccid&#39;">ccid</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '11, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/a29474cbdf482d74bdfe461074d030b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MartinP&#39;s gravatar image" /><p><span>MartinP</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MartinP has no accepted answers">0%</span></p></div></div><div id="comments-container-3196" class="comments-container"></div><div id="comment-tools-3196" class="comment-tools"></div><div class="clear"></div><div id="comment-3196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

