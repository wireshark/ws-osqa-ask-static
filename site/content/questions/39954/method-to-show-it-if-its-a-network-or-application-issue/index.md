+++
type = "question"
title = "Method to show it if it&#x27;s a network or application issue?"
description = '''Hello, We have the issue where we are not sure if the issue is the appliaction being slow or the network and I think Wireshark can show this. I&#x27;ve used the TCP Delta command to try and help, but I was&#x27;t wondering what method you pros use in locating this information and can it be graphed? Thanks'''
date = "2015-02-19T09:09:00Z"
lastmod = "2015-02-20T07:32:00Z"
weight = 39954
keywords = [ "latency" ]
aliases = [ "/questions/39954" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Method to show it if it's a network or application issue?](/questions/39954/method-to-show-it-if-its-a-network-or-application-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39954-score" class="post-score" title="current number of votes">0</div><span id="post-39954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>We have the issue where we are not sure if the issue is the appliaction being slow or the network and I think Wireshark can show this. I've used the TCP Delta command to try and help, but I was't wondering what method you pros use in locating this information and can it be graphed?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '15, 09:09</strong></p><img src="https://secure.gravatar.com/avatar/09e8dc62bc7c0b2a6d62edf9aebb8707?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gonzo&#39;s gravatar image" /><p><span>gonzo</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gonzo has no accepted answers">0%</span></p></div></div><div id="comments-container-39954" class="comments-container"></div><div id="comment-tools-39954" class="comment-tools"></div><div class="clear"></div><div id="comment-39954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39979"></span>

<div id="answer-container-39979" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39979-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39979-score" class="post-score" title="current number of votes">0</div><span id="post-39979-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi Gonzo,</p><p>Why dont you try APPanalyz it is east and free to use .</p><p>www.appanalyz.com</p><p>prasamsetty</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Feb '15, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/e1413afb925b27661ddc4ea81d1e6319?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prasamsetty&#39;s gravatar image" /><p><span>prasamsetty</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prasamsetty has no accepted answers">0%</span></p></div></div><div id="comments-container-39979" class="comments-container"></div><div id="comment-tools-39979" class="comment-tools"></div><div class="clear"></div><div id="comment-39979-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

