+++
type = "question"
title = "gotomeeting audio codec"
description = '''Hi, We&#x27;re having audio problems with our webinar software app specifically gotomeeting software - anyone analyzed their gotomeeting or other webinar software for audio problems?  How can we focus on the audio portion of webinar/webex/gotomeeting traffic? Or does anyone know audio codecs used by webi...'''
date = "2015-11-24T13:39:00Z"
lastmod = "2015-11-24T13:39:00Z"
weight = 47941
keywords = [ "audio" ]
aliases = [ "/questions/47941" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [gotomeeting audio codec](/questions/47941/gotomeeting-audio-codec)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47941-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47941-score" class="post-score" title="current number of votes">0</div><span id="post-47941-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, We're having audio problems with our webinar software app specifically gotomeeting software - anyone analyzed their gotomeeting or other webinar software for audio problems? How can we focus on the audio portion of webinar/webex/gotomeeting traffic? Or does anyone know audio codecs used by webinar software generally and citrix gotomeeting specifically? Thanks, Andy</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-audio" rel="tag" title="see questions tagged &#39;audio&#39;">audio</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '15, 13:39</strong></p><img src="https://secure.gravatar.com/avatar/a644ad07fb6863b2dc98b09595f2642d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akaak&#39;s gravatar image" /><p><span>akaak</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akaak has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Nov '15, 13:42</strong> </span></p></div></div><div id="comments-container-47941" class="comments-container"></div><div id="comment-tools-47941" class="comment-tools"></div><div class="clear"></div><div id="comment-47941-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

