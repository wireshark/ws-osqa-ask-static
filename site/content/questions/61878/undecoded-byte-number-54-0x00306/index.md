+++
type = "question"
title = "Undecoded byte number: 54 (0x0030+6)"
description = '''Can someone please tell me in non-programmer guru language? Part of the session is below...Thanks in advance R/Fritz '''
date = "2017-06-08T13:33:00Z"
lastmod = "2017-06-08T17:05:00Z"
weight = 61878
keywords = [ "undecoded", "byte", "number540x0030_6" ]
aliases = [ "/questions/61878" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Undecoded byte number: 54 (0x0030+6)](/questions/61878/undecoded-byte-number-54-0x00306)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61878-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61878-score" class="post-score" title="current number of votes">0</div><span id="post-61878-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone please tell me in non-programmer guru language? Part of the session is below...Thanks in advance</p><p>R/Fritz</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Picture1_O7Jn9p2.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-undecoded" rel="tag" title="see questions tagged &#39;undecoded&#39;">undecoded</span> <span class="post-tag tag-link-byte" rel="tag" title="see questions tagged &#39;byte&#39;">byte</span> <span class="post-tag tag-link-number540x0030_6" rel="tag" title="see questions tagged &#39;number540x0030_6&#39;">number540x0030_6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '17, 13:33</strong></p><img src="https://secure.gravatar.com/avatar/6e0fda2a5c8d02515d88f004b33a9998?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fritzbied&#39;s gravatar image" /><p><span>fritzbied</span><br />
<span class="score" title="6 reputation points">6</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fritzbied has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61878" class="comments-container"><span id="61882"></span><div id="comment-61882" class="comment"><div id="post-61882-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-61882-info" class="comment-info"><span class="comment-age">(08 Jun '17, 14:37)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61884"></span><div id="comment-61884" class="comment"><div id="post-61884-score" class="comment-score"></div><div class="comment-text"><p>Not sure I should or can...I will have to pass. I used to have a cloudshark account. I need to look into it. Thanks</p></div><div id="comment-61884-info" class="comment-info"><span class="comment-age">(08 Jun '17, 17:05)</span> <span class="comment-user userinfo">fritzbied</span></div></div></div><div id="comment-tools-61878" class="comment-tools"></div><div class="clear"></div><div id="comment-61878-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

