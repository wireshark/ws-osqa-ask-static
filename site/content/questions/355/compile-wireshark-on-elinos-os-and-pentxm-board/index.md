+++
type = "question"
title = "Compile Wireshark on Elinos OS and PentxM board"
description = '''Hello, Is it possible to cross compile Wireshak for Elinos operating system?'''
date = "2010-09-29T12:05:00Z"
lastmod = "2010-10-06T14:38:00Z"
weight = 355
keywords = [ "elinos" ]
aliases = [ "/questions/355" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Compile Wireshark on Elinos OS and PentxM board](/questions/355/compile-wireshark-on-elinos-os-and-pentxm-board)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-355-score" class="post-score" title="current number of votes">0</div><span id="post-355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Is it possible to cross compile Wireshak for Elinos operating system?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-elinos" rel="tag" title="see questions tagged &#39;elinos&#39;">elinos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '10, 12:05</strong></p><img src="https://secure.gravatar.com/avatar/c379ac37028f4afda62b7fad1ad04a47?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eeee&#39;s gravatar image" /><p><span>eeee</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eeee has no accepted answers">0%</span></p></div></div><div id="comments-container-355" class="comments-container"><span id="447"></span><div id="comment-447" class="comment"><div id="post-447-score" class="comment-score">1</div><div class="comment-text"><p>Elinos:</p><pre><code>http://www.sysgo.com/products/elinos-embedded-linux/</code></pre><p>is an embedded Linux, so the "core OS" should be supported (if it comes with libpcap). The GUI would be available only if it includes the X11 client library, but do you really want to run Wireshark on an embedded device, or do you just want to capture traffic and copy the resulting pcap or pcap-ng file to another machine and read it there?</p></div><div id="comment-447-info" class="comment-info"><span class="comment-age">(06 Oct '10, 14:38)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-355" class="comment-tools"></div><div class="clear"></div><div id="comment-355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

