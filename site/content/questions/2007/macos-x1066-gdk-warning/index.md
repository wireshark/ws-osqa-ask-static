+++
type = "question"
title = "Macos x10.6.6 GDK Warning"
description = '''Starting from the terminal, I get this warning. (wireshark-bin:15888): Gtk-WARNING **: Unable to locate theme engine in module_path: &quot;clearlooks&quot;, Wireshark starts ok and initial use seems ok, but I&#x27;m a novice here, so theres some learning to do. just wanted to know if this error was something I cou...'''
date = "2011-01-29T07:47:00Z"
lastmod = "2011-02-13T08:06:00Z"
weight = 2007
keywords = [ "gdk", "clearlooks" ]
aliases = [ "/questions/2007" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Macos x10.6.6 GDK Warning](/questions/2007/macos-x1066-gdk-warning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2007-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2007-score" class="post-score" title="current number of votes">0</div><span id="post-2007-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Starting from the terminal, I get this warning. (wireshark-bin:15888): Gtk-WARNING **: Unable to locate theme engine in module_path: "clearlooks",</p><p>Wireshark starts ok and initial use seems ok, but I'm a novice here, so theres some learning to do. just wanted to know if this error was something I could ignore or not. . .</p><p>/terry</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gdk" rel="tag" title="see questions tagged &#39;gdk&#39;">gdk</span> <span class="post-tag tag-link-clearlooks" rel="tag" title="see questions tagged &#39;clearlooks&#39;">clearlooks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jan '11, 07:47</strong></p><img src="https://secure.gravatar.com/avatar/805c20b89f13c7d7ac3c2c11ef1f0af9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sn4fu&#39;s gravatar image" /><p><span>sn4fu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sn4fu has no accepted answers">0%</span></p></div></div><div id="comments-container-2007" class="comments-container"></div><div id="comment-tools-2007" class="comment-tools"></div><div class="clear"></div><div id="comment-2007-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2299"></span>

<div id="answer-container-2299" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2299-score" class="post-score" title="current number of votes">0</div><span id="post-2299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>GTK is 'graphical tool kit' I think. Theme engine is, I think a way of abstracting skins so that you can have a 'look and feel' that is unified and customizable. The two control the appearance of the app. If you aren't ending up with things like dark blue text on black backgrounds, widgets you can't click, stuff you can't see, resizeable boxes you can't resize, then you can probably breathe easy.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '11, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/ed2ddfe3ce5e547c20fafa0c82b3e970?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SGBotsford&#39;s gravatar image" /><p><span>SGBotsford</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SGBotsford has no accepted answers">0%</span></p></div></div><div id="comments-container-2299" class="comments-container"></div><div id="comment-tools-2299" class="comment-tools"></div><div class="clear"></div><div id="comment-2299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

