+++
type = "question"
title = "Help me  &quot;wifi direct&quot; packets measurement"
description = '''Now I am try to measure &quot;wifi direct&quot; packets between two cellphones using Wireshark &amp;amp; Airpcap on window Is there anyone who measure these packets successfully please share know-how to me novice10@naver.com'''
date = "2012-04-01T20:00:00Z"
lastmod = "2012-12-03T17:37:00Z"
weight = 9891
keywords = [ "wifi", "direct" ]
aliases = [ "/questions/9891" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Help me "wifi direct" packets measurement](/questions/9891/help-me-wifi-direct-packets-measurement)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9891-score" class="post-score" title="current number of votes">0</div><span id="post-9891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Now I am try to measure "wifi direct" packets between two cellphones using Wireshark &amp; Airpcap on window</p><p>Is there anyone who measure these packets successfully please share know-how to me</p><p><span class="__cf_email__" data-cfemail="274948514e4442161767">[email protected]</span><a href="http://naver.com">naver.com</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-direct" rel="tag" title="see questions tagged &#39;direct&#39;">direct</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '12, 20:00</strong></p><img src="https://secure.gravatar.com/avatar/295eb08094302aed1a4ee88ac60fa89f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="novice10&#39;s gravatar image" /><p><span>novice10</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="novice10 has no accepted answers">0%</span></p></div></div><div id="comments-container-9891" class="comments-container"></div><div id="comment-tools-9891" class="comment-tools"></div><div class="clear"></div><div id="comment-9891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16518"></span>

<div id="answer-container-16518" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16518-score" class="post-score" title="current number of votes">0</div><span id="post-16518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>hi did u find the solution for this question? Can we capture using AirPcap nx tool? if so then how?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Dec '12, 17:37</strong></p><img src="https://secure.gravatar.com/avatar/ec2356783b97430c3d3dea7c14cb8134?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="satya&#39;s gravatar image" /><p><span>satya</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="satya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Dec '12, 17:39</strong> </span></p></div></div><div id="comments-container-16518" class="comments-container"></div><div id="comment-tools-16518" class="comment-tools"></div><div class="clear"></div><div id="comment-16518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

