+++
type = "question"
title = "CTB LOCKER ENCRYPTION KEY"
description = '''Hello all. New to this program. In my research dealing with this infection, I&#x27;ve read that Wireshark can sometimes be used to pull the decryption key. I need to decrypt files locked by this. Can anyone tell me how to go about trying this? Thanks'''
date = "2015-07-14T11:26:00Z"
lastmod = "2015-07-15T12:00:00Z"
weight = 44147
keywords = [ "files", "decryption", "ctblocker", "key", "encrypted" ]
aliases = [ "/questions/44147" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [CTB LOCKER ENCRYPTION KEY](/questions/44147/ctb-locker-encryption-key)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44147-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44147-score" class="post-score" title="current number of votes">0</div><span id="post-44147-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all. New to this program. In my research dealing with this infection, I've read that Wireshark can sometimes be used to pull the decryption key. I need to decrypt files locked by this. Can anyone tell me how to go about trying this? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-ctblocker" rel="tag" title="see questions tagged &#39;ctblocker&#39;">ctblocker</span> <span class="post-tag tag-link-key" rel="tag" title="see questions tagged &#39;key&#39;">key</span> <span class="post-tag tag-link-encrypted" rel="tag" title="see questions tagged &#39;encrypted&#39;">encrypted</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jul '15, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/3f5e7979436ecfebff8cbaaf1858aefc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Raven&#39;s gravatar image" /><p><span>Raven</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Raven has no accepted answers">0%</span></p></div></div><div id="comments-container-44147" class="comments-container"></div><div id="comment-tools-44147" class="comment-tools"></div><div class="clear"></div><div id="comment-44147-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44184"></span>

<div id="answer-container-44184" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44184-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44184-score" class="post-score" title="current number of votes">0</div><span id="post-44184-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would just wipe your computer and restore your files from backup. New ransomware has encrypted communications, and the decrypt keys reside on the attacker's server, only to be sent if paid.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '15, 12:00</strong></p><img src="https://secure.gravatar.com/avatar/7c34b5795df1aaa486754544342bfc1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zer0day&#39;s gravatar image" /><p><span>zer0day</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zer0day has 3 accepted answers">60%</span></p></div></div><div id="comments-container-44184" class="comments-container"></div><div id="comment-tools-44184" class="comment-tools"></div><div class="clear"></div><div id="comment-44184-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

