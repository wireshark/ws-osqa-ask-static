+++
type = "question"
title = "Can not browse when run wireshark"
description = '''I have Windows 8 Pro. Have installed Wireshark 1.10.2 and WinPcap 4.1.3 on it. when I start the live capture from WiFi, I can not access any site from my browser. What&#x27;s wrong? Any error?'''
date = "2013-09-29T21:16:00Z"
lastmod = "2013-09-29T21:16:00Z"
weight = 25353
keywords = [ "capture", "live", "browser", "windows8", "error" ]
aliases = [ "/questions/25353" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can not browse when run wireshark](/questions/25353/can-not-browse-when-run-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25353-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25353-score" class="post-score" title="current number of votes">0</div><span id="post-25353-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Windows 8 Pro. Have installed Wireshark 1.10.2 and WinPcap 4.1.3 on it. when I start the live capture from WiFi, I can not access any site from my browser. What's wrong? Any error?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-live" rel="tag" title="see questions tagged &#39;live&#39;">live</span> <span class="post-tag tag-link-browser" rel="tag" title="see questions tagged &#39;browser&#39;">browser</span> <span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '13, 21:16</strong></p><img src="https://secure.gravatar.com/avatar/263efcb53a4374ebb89fff52596ca56f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rizkihabibie999&#39;s gravatar image" /><p><span>rizkihabibie999</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rizkihabibie999 has no accepted answers">0%</span></p></div></div><div id="comments-container-25353" class="comments-container"></div><div id="comment-tools-25353" class="comment-tools"></div><div class="clear"></div><div id="comment-25353-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

