+++
type = "question"
title = "Fonts in Mac OSX impossible to read - terminal has error"
description = '''I am using OSX Lion, but this also occurs in Snow Leopard.  1) See attached for how the fonts display in wireshark. Changing the fonts in the preferences does nothing.  2) when running wireshark from the terminal, i get this:  [ 10:02:18 ] &amp;gt; wireshark 2011-06-21 22:02:20.176 defaults[7981:707]  T...'''
date = "2011-06-21T20:07:00Z"
lastmod = "2011-06-21T20:09:00Z"
weight = 4658
keywords = [ "osx", "mac", "font", "clearlooks" ]
aliases = [ "/questions/4658" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Fonts in Mac OSX impossible to read - terminal has error](/questions/4658/fonts-in-mac-osx-impossible-to-read-terminal-has-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4658-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4658-score" class="post-score" title="current number of votes">0</div><span id="post-4658-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using OSX Lion, but this also occurs in Snow Leopard.</p><p>1) See attached for how the fonts display in wireshark. Changing the fonts in the preferences does nothing. 2) when running wireshark from the terminal, i get this:</p><p>[ 10:02:18 ] &gt; wireshark 2011-06-21 22:02:20.176 defaults[7981:707] The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist 2011-06-21 22:02:20.186 defaults[7982:707] The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist</p><p>(process:7971): Gdk-WARNING **: locale not supported by C library</p><p>(process:7971): Gtk-WARNING **: Locale not supported by C library. Using the fallback 'C' locale.</p><p>(wireshark-bin:7971): Gtk-WARNING **: Unable to locate theme engine in module_path: "clearlooks", wireshark-bin: Fatal IO error 35 (Resource temporarily unavailable) on X server /tmp/launch-bLGSZ7/org.x:0.</p><p>Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-font" rel="tag" title="see questions tagged &#39;font&#39;">font</span> <span class="post-tag tag-link-clearlooks" rel="tag" title="see questions tagged &#39;clearlooks&#39;">clearlooks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jun '11, 20:07</strong></p><img src="https://secure.gravatar.com/avatar/c0e2f71d4669be8a5fea92e3270275fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NightLion&#39;s gravatar image" /><p><span>NightLion</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NightLion has no accepted answers">0%</span></p></div></div><div id="comments-container-4658" class="comments-container"><span id="4659"></span><div id="comment-4659" class="comment"><div id="post-4659-score" class="comment-score"></div><div class="comment-text"><p><a href="http://cl.ly/263k2i2k3q3U013m1t1F">Here's the screen shot</a>.</p></div><div id="comment-4659-info" class="comment-info"><span class="comment-age">(21 Jun '11, 20:09)</span> <span class="comment-user userinfo">NightLion</span></div></div></div><div id="comment-tools-4658" class="comment-tools"></div><div class="clear"></div><div id="comment-4658-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

