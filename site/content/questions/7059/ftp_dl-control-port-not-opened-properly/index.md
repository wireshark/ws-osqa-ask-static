+++
type = "question"
title = "FTP_DL control port not opened properly"
description = '''My FTP client sends Request:RETR and receives Response: 150 Opening BINARY mode ... but behaves as if it had not received it and retransmits Request RETR. Why is this so? In successful FTP sessions, the packet that contains Response acknowledges the Request. Greets,'''
date = "2011-10-25T02:22:00Z"
lastmod = "2011-10-25T02:22:00Z"
weight = 7059
keywords = [ "fctp" ]
aliases = [ "/questions/7059" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [FTP\_DL control port not opened properly](/questions/7059/ftp_dl-control-port-not-opened-properly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7059-score" class="post-score" title="current number of votes">0</div><span id="post-7059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My FTP client sends Request:RETR and receives Response: 150 Opening BINARY mode ... but behaves as if it had not received it and retransmits Request RETR. Why is this so?</p><p>In successful FTP sessions, the packet that contains Response acknowledges the Request.</p><p>Greets,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fctp" rel="tag" title="see questions tagged &#39;fctp&#39;">fctp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '11, 02:22</strong></p><img src="https://secure.gravatar.com/avatar/7820f7b9638e63d42e5c6fb4de7262d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brklp&#39;s gravatar image" /><p><span>brklp</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brklp has no accepted answers">0%</span></p></div></div><div id="comments-container-7059" class="comments-container"></div><div id="comment-tools-7059" class="comment-tools"></div><div class="clear"></div><div id="comment-7059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

