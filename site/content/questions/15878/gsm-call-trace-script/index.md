+++
type = "question"
title = "GSM call trace script"
description = '''i was wondering if someone has filter/script to capture GSM call trace with IMSI and MSISDN. would be great if you can share.'''
date = "2012-11-13T10:47:00Z"
lastmod = "2012-11-13T10:47:00Z"
weight = 15878
keywords = [ "gsm" ]
aliases = [ "/questions/15878" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [GSM call trace script](/questions/15878/gsm-call-trace-script)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15878-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15878-score" class="post-score" title="current number of votes">0</div><span id="post-15878-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i was wondering if someone has filter/script to capture GSM call trace with IMSI and MSISDN. would be great if you can share.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '12, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/99858523e3e1f5ec589b18e4ca0ddfd1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="newbie29&#39;s gravatar image" /><p><span>newbie29</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="newbie29 has no accepted answers">0%</span></p></div></div><div id="comments-container-15878" class="comments-container"></div><div id="comment-tools-15878" class="comment-tools"></div><div class="clear"></div><div id="comment-15878-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

