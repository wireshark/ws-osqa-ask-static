+++
type = "question"
title = "RTP Player Crashes"
description = '''Whenever I try to decode and play a SIP/RTP stream Wireshark Crashes. I&#x27;ve tried the latest 32bit and 64bit versions. Version 1.12.4 (v1.12.4-0-gb4861da from master-1.12)'''
date = "2015-05-01T14:02:00Z"
lastmod = "2015-05-06T12:03:00Z"
weight = 42007
keywords = [ "player", "rtp", "crashes" ]
aliases = [ "/questions/42007" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTP Player Crashes](/questions/42007/rtp-player-crashes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42007-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42007-score" class="post-score" title="current number of votes">0</div><span id="post-42007-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Whenever I try to decode and play a SIP/RTP stream Wireshark Crashes.</p><p>I've tried the latest 32bit and 64bit versions.</p><p>Version 1.12.4 (v1.12.4-0-gb4861da from master-1.12)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-crashes" rel="tag" title="see questions tagged &#39;crashes&#39;">crashes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '15, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/4230345ff427de5e5bfdfea9e8657606?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rjesse&#39;s gravatar image" /><p><span>rjesse</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rjesse has no accepted answers">0%</span></p></div></div><div id="comments-container-42007" class="comments-container"><span id="42153"></span><div id="comment-42153" class="comment"><div id="post-42153-score" class="comment-score"></div><div class="comment-text"><p>I am having the same issue.<br />
</p><p>Version 1.12.4 (v1.12.4-0-gb4861da from master-1.12)<br />
</p><p>Running on 32-bit Windows 7 Service Pack 1, build 7601, with WinPcap version 4.1.3 (packet.dll version 4.1.0.2980), based on libpcap version 1.0 branch 1_0_rel0b (20091008), GnuTLS 3.2.15, Gcrypt 1.6.2, with AirPcap 4.1.3 build 3348. Intel(R) Core(TM) i5-4300U CPU @ 1.90GHz, with 3297MB of physical memory.</p></div><div id="comment-42153-info" class="comment-info"><span class="comment-age">(06 May '15, 12:03)</span> <span class="comment-user userinfo">Gilmax</span></div></div></div><div id="comment-tools-42007" class="comment-tools"></div><div class="clear"></div><div id="comment-42007-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

