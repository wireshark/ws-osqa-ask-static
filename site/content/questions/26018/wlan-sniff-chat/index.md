+++
type = "question"
title = "WLAN sniff chat"
description = '''How to sniff IP messenger chat in wireless? I found packets but encrypted like shown some numbers... then how to decrypt that pcap file? And what is particular way for sniff IP messenger chats?'''
date = "2013-10-15T11:13:00Z"
lastmod = "2013-10-15T11:13:00Z"
weight = 26018
keywords = [ "wireless", "chat" ]
aliases = [ "/questions/26018" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WLAN sniff chat](/questions/26018/wlan-sniff-chat)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26018-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26018-score" class="post-score" title="current number of votes">0</div><span id="post-26018-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to sniff IP messenger chat in wireless? I found packets but encrypted like shown some numbers... then how to decrypt that pcap file? And what is particular way for sniff IP messenger chats?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-chat" rel="tag" title="see questions tagged &#39;chat&#39;">chat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '13, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '13, 11:28</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-26018" class="comments-container"></div><div id="comment-tools-26018" class="comment-tools"></div><div class="clear"></div><div id="comment-26018-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

