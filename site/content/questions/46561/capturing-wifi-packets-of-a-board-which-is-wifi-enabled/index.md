+++
type = "question"
title = "Capturing Wifi packets of a board which is wifi enabled"
description = '''Hello, I connected a board which is wifi enabled to a laptop. After flashing the image to board,the board will connect a cloud server through wifi. So how can I capture the application layer packets through wireshark. please let me know the steps. Thanks&amp;amp; regards Kittu'''
date = "2015-10-15T04:08:00Z"
lastmod = "2015-10-15T05:39:00Z"
weight = 46561
keywords = [ "capture", "awifi", "for", "wireshark" ]
aliases = [ "/questions/46561" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing Wifi packets of a board which is wifi enabled](/questions/46561/capturing-wifi-packets-of-a-board-which-is-wifi-enabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46561-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46561-score" class="post-score" title="current number of votes">0</div><span id="post-46561-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I connected a board which is wifi enabled to a laptop. After flashing the image to board,the board will connect a cloud server through wifi.</p><p>So how can I capture the application layer packets through wireshark.</p><p>please let me know the steps.</p><p>Thanks&amp; regards Kittu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-awifi" rel="tag" title="see questions tagged &#39;awifi&#39;">awifi</span> <span class="post-tag tag-link-for" rel="tag" title="see questions tagged &#39;for&#39;">for</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '15, 04:08</strong></p><img src="https://secure.gravatar.com/avatar/c51f72576ac374489bcc9d7536c9861a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kittu&#39;s gravatar image" /><p><span>kittu</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kittu has no accepted answers">0%</span></p></div></div><div id="comments-container-46561" class="comments-container"></div><div id="comment-tools-46561" class="comment-tools"></div><div class="clear"></div><div id="comment-46561-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46564"></span>

<div id="answer-container-46564" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46564-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46564-score" class="post-score" title="current number of votes">0</div><span id="post-46564-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please read the Wireshark wiki:</p><blockquote><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p></blockquote><p>and then watch some videos</p><blockquote><p><a href="https://www.google.com/#q=youtube+wireshark+wireless">https://www.google.com/#q=youtube+wireshark+wireless</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '15, 05:39</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '15, 05:41</strong> </span></p></div></div><div id="comments-container-46564" class="comments-container"></div><div id="comment-tools-46564" class="comment-tools"></div><div class="clear"></div><div id="comment-46564-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

