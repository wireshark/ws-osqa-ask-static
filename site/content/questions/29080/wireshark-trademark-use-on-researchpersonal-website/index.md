+++
type = "question"
title = "Wireshark Trademark Use on Research/Personal Website"
description = '''Good Day, I&#x27;m working on a school project that is being displayed on a public website. I would like to display the Wireshark logo on a section regarding my research. I&#x27;ve been trying to locate the Wireshark Trademark policy/rules to ensure that I abide by them, however I have been unable to locate a...'''
date = "2014-01-21T20:11:00Z"
lastmod = "2014-01-22T05:58:00Z"
weight = 29080
keywords = [ "trademark" ]
aliases = [ "/questions/29080" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Trademark Use on Research/Personal Website](/questions/29080/wireshark-trademark-use-on-researchpersonal-website)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29080-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29080-score" class="post-score" title="current number of votes">1</div><span id="post-29080-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good Day,</p><p>I'm working on a school project that is being displayed on a public website. I would like to display the Wireshark logo on a section regarding my research. I've been trying to locate the Wireshark Trademark policy/rules to ensure that I abide by them, however I have been unable to locate anything.</p><p>I found this post: <a href="http://ask.wireshark.org/questions/28795/is-it-allowed-to-have-wireshark-screenshots-on-the-company-homepage">http://ask.wireshark.org/questions/28795/is-it-allowed-to-have-wireshark-screenshots-on-the-company-homepage</a></p><p>It does state that if the logo is in a screenshot to ensure to give credit to wireshark.org. I would like to confirm that it is acceptable.<br />
</p><p>No profits will be gained, just research and credit to Wireshark organization.<br />
</p><p>Thank You,</p><p>B.A. Hansen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trademark" rel="tag" title="see questions tagged &#39;trademark&#39;">trademark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '14, 20:11</strong></p><img src="https://secure.gravatar.com/avatar/b79febffdb073b0830586f4d5b0f94ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bahansen&#39;s gravatar image" /><p><span>bahansen</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bahansen has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Jan '14, 15:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-29080" class="comments-container"></div><div id="comment-tools-29080" class="comment-tools"></div><div class="clear"></div><div id="comment-29080-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29090"></span>

<div id="answer-container-29090" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29090-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29090-score" class="post-score" title="current number of votes">1</div><span id="post-29090-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Rather than ask the chattering classes here for an opinion :-), for absolute certainty you should contact the owner of the trademark, the Wireshark Foundation. Oddly enough Google couldn't find a link directly to them, but I suspect <a href="https://blog.wireshark.org/">Gerald</a> might know where to find them.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '14, 03:11</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-29090" class="comments-container"><span id="29092"></span><div id="comment-29092" class="comment"><div id="post-29092-score" class="comment-score"></div><div class="comment-text"><p>Some sites have somthing like this on their site "All other product or service names mentioned in a document on this server are trademarks of their respective companies."</p></div><div id="comment-29092-info" class="comment-info"><span class="comment-age">(22 Jan '14, 05:58)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-29090" class="comment-tools"></div><div class="clear"></div><div id="comment-29090-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

