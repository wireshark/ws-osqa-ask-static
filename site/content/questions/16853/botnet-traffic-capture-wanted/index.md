+++
type = "question"
title = "botnet traffic capture wanted"
description = '''hi  i want please any pucket captured by wireshark for any botnet using http protocol like (spyeye , bobax , clickbot , rustock)'''
date = "2012-12-13T14:39:00Z"
lastmod = "2012-12-18T05:12:00Z"
weight = 16853
keywords = [ "botnet" ]
aliases = [ "/questions/16853" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [botnet traffic capture wanted](/questions/16853/botnet-traffic-capture-wanted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16853-score" class="post-score" title="current number of votes">0</div><span id="post-16853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i want please any pucket captured by wireshark for any botnet using http protocol like (spyeye , bobax , clickbot , rustock)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-botnet" rel="tag" title="see questions tagged &#39;botnet&#39;">botnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '12, 14:39</strong></p><img src="https://secure.gravatar.com/avatar/07b9ad18547fc8c95d7e8a3ab1062ed0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mais&#39;s gravatar image" /><p><span>mais</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mais has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>14 Dec '12, 02:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-16853" class="comments-container"><span id="16854"></span><div id="comment-16854" class="comment"><div id="post-16854-score" class="comment-score"></div><div class="comment-text"><p>please answer me and thanks for help me</p></div><div id="comment-16854-info" class="comment-info"><span class="comment-age">(13 Dec '12, 14:42)</span> <span class="comment-user userinfo">mais</span></div></div></div><div id="comment-tools-16853" class="comment-tools"></div><div class="clear"></div><div id="comment-16853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16878"></span>

<div id="answer-container-16878" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16878-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16878-score" class="post-score" title="current number of votes">0</div><span id="post-16878-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are botnet sample captures at <a href="http://openpacket.org">openpacket.org</a>. Unfortunately the site is currently down. Maybe you try later ....</p><p>Article with links to the botnet capture files.</p><blockquote><p><code>http://cyberarms.wordpress.com/2010/04/15/zeus-botnet-tcpip-packet-capture-in-netwitness-investigator/</code><br />
</p></blockquote><p>Sourcefire offers some botnet capture files as well (at the end of the page).</p><blockquote><p><code>http://labs.snort.org/papers/zeus.html</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Dec '12, 06:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Dec '12, 06:44</strong> </span></p></div></div><div id="comments-container-16878" class="comments-container"><span id="17018"></span><div id="comment-17018" class="comment"><div id="post-17018-score" class="comment-score"></div><div class="comment-text"><p>hi thanks for your help but i have these three samples of zeus befor, so i just need (spyeye , bobax , clickbot , rustock) if you can find it for me please thanks again mais</p></div><div id="comment-17018-info" class="comment-info"><span class="comment-age">(18 Dec '12, 05:12)</span> <span class="comment-user userinfo">mais</span></div></div></div><div id="comment-tools-16878" class="comment-tools"></div><div class="clear"></div><div id="comment-16878-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

