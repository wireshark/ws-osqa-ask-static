+++
type = "question"
title = "Wireshark for Macintosh 64 bit vs. 32 bit install problems"
description = '''I THINK this may be something obvious but I am missing what it is.  This is the current 1.4.1 Macintosh version, both the 64 bit and 32 bit versions. I have 4 iMacs, INTELS, all running Mac OS 10.6.4. Most have 8 gigs RAM and that info is being indicated in this one problematic iMac as well. These i...'''
date = "2010-11-02T15:30:00Z"
lastmod = "2011-02-13T13:08:00Z"
weight = 783
keywords = [ "mac", "32-bit", "problems", "install", "64-bit" ]
aliases = [ "/questions/783" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark for Macintosh 64 bit vs. 32 bit install problems](/questions/783/wireshark-for-macintosh-64-bit-vs-32-bit-install-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-783-score" class="post-score" title="current number of votes">0</div><span id="post-783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I THINK this may be something obvious but I am missing what it is.<br />
</p><p>This is the current 1.4.1 Macintosh version, both the 64 bit and 32 bit versions.</p><p>I have 4 iMacs, INTELS, all running Mac OS 10.6.4. Most have 8 gigs RAM and that info is being indicated in this one problematic iMac as well.</p><p>These iMacs are standard 10.6.4 installs, done by myself previously, now with all updates done except for two most recent security updates but not relevant, just historical.</p><p>Today, I installed X11 on all 4 from the DVD that came with each iMac which is about a year old model.</p><p>I then installed Wireshark v1.4.1 64 bit version on all 4 machines and on 3 of them, it worked without any problems. On one machine, it would only bounce once and then stop.</p><p>Same behavior if app was on desktop or in Apps folder. Fixed permissions just in case but no help.</p><p>I then had the idea to see what would happen if I installed the 32 bit version on the "problematic" iMac and that worked as expected.</p><p>These are pretty standard configured computers for DTP use, so no fancy user or CLI changes, no restart into 64 bit Finder - just basic vanilla clean installs on each one that have been working without problems for over a year.</p><p>So, what does this result indicate if anything? Why does only the 32 bit version work on that one machine? What I did not do was check Activity Monitor to see if the machine had access to the 8 gigs wondering if this means there is an architectural problem on one of them but how could that be anyway?</p><p>This is sort of academic but if this all sounds too familiar, I would be interested.</p><p>-Peter</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-32-bit" rel="tag" title="see questions tagged &#39;32-bit&#39;">32-bit</span> <span class="post-tag tag-link-problems" rel="tag" title="see questions tagged &#39;problems&#39;">problems</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '10, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/fd0c2e511b26aeeba91731df827468f7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pmf&#39;s gravatar image" /><p><span>pmf</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pmf has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-783" class="comments-container"></div><div id="comment-tools-783" class="comment-tools"></div><div class="clear"></div><div id="comment-783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="784"></span>

<div id="answer-container-784" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-784-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-784-score" class="post-score" title="current number of votes">0</div><span id="post-784-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>My first guess would be that the "fourth" iMac is not equipped with a 64-bit CPU. Could you post the CPU type in a comment? And does the activity monitor show any other 64-bit applications running?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Nov '10, 16:34</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-784" class="comments-container"><span id="786"></span><div id="comment-786" class="comment"><div id="post-786-score" class="comment-score"></div><div class="comment-text"><p>Well, it is the same model as at least 2 of the 3 other machines, it is a INTEL machine so if the chip is not a 64bit CPU, it means Apple put the wrong chip in the machine. I will try to get the actual Profiler CPU info from my client but think getting him to send me Activity Monitor info may be a problem. :-)</p><p>They do have CS5 on that machine though.</p><p>_Peter</p></div><div id="comment-786-info" class="comment-info"><span class="comment-age">(02 Nov '10, 16:52)</span> <span class="comment-user userinfo">pmf</span></div></div><span id="790"></span><div id="comment-790" class="comment"><div id="post-790-score" class="comment-score"></div><div class="comment-text"><p>Updated info: The iMac does indicate in Profiler to have an INTEL chip and I think it is 64 bit.</p><p>Hardware Overview: Model Name: iMac Model Identifier: iMac10,1 Processor Name: Intel Core 2 Duo Processor Speed: 3.06 GHz Number Of Processors: 1 Total Number Of Cores: 2 L2 Cache: 3 MB Memory: 8 GB Bus Speed: 1.07 GHz Boot ROM Version: IM101.00CC.B00 SMC Version (system): 1.53f13</p><p>AND</p><p>in Activity Monitor there are a number of 64 bit apps running including FINDER and other OS components.</p><p>-Peter</p></div><div id="comment-790-info" class="comment-info"><span class="comment-age">(03 Nov '10, 06:36)</span> <span class="comment-user userinfo">pmf</span></div></div></div><div id="comment-tools-784" class="comment-tools"></div><div class="clear"></div><div id="comment-784-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="2300"></span>

<div id="answer-container-2300" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2300-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2300-score" class="post-score" title="current number of votes">0</div><span id="post-2300-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Mac's usually ship with both a 32 bit and 64 bit kernel. The 32 bit kernel has some magic added to handle larger address spaces, and it can handle 64 bit apps just fine. Kernel extensions, however have to be matched to the kernel. I'm betting that the berkeley packet filter has to be implemented as a kext, and that it isn't matched to the kernel you are using.</p><p>Run wireshark from the command line. On my box it is</p><p>bash-3.2# /Applications/Wireshark.app/Contents/MacOS/Wireshark</p><p>Also try running kextstat on various machines and see if there is a different set of extensions loaded. Running kextstat on a machine that has wireshark running and comparing to before you start wireshark may tell you if wireshark is loading a kext.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '11, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/ed2ddfe3ce5e547c20fafa0c82b3e970?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SGBotsford&#39;s gravatar image" /><p><span>SGBotsford</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SGBotsford has no accepted answers">0%</span></p></div></div><div id="comments-container-2300" class="comments-container"><span id="2309"></span><div id="comment-2309" class="comment"><div id="post-2309-score" class="comment-score"></div><div class="comment-text"><p>BPF is in /mach_kernel, not in a kext. There is one bug in BPF that causes problems with 64-bit user code on a 32-bit kernel, but, in most versions of 10.6, libpcap works around it, and in the ones where it doesn't, Wireshark works around it.</p></div><div id="comment-2309-info" class="comment-info"><span class="comment-age">(13 Feb '11, 13:08)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-2300" class="comment-tools"></div><div class="clear"></div><div id="comment-2300-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

