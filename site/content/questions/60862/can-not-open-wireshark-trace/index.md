+++
type = "question"
title = "Can not open wireshark trace"
description = '''hello. i have captured wireshark file but i can not open it later. file name is xxx.pcapng. displays below error. The file &quot;xxx.pcapng&quot; isn&#x27;t captire file in a format WIreshark understands. file size about 500MB BR Kamran'''
date = "2017-04-17T00:48:00Z"
lastmod = "2017-04-17T09:19:00Z"
weight = 60862
keywords = [ "not", "open", "can", "file", "wireshark" ]
aliases = [ "/questions/60862" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can not open wireshark trace](/questions/60862/can-not-open-wireshark-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60862-score" class="post-score" title="current number of votes">0</div><span id="post-60862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello. i have captured wireshark file but i can not open it later. file name is xxx.pcapng. displays below error. The file "xxx.pcapng" isn't captire file in a format WIreshark understands. file size about 500MB</p><p>BR Kamran</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-open" rel="tag" title="see questions tagged &#39;open&#39;">open</span> <span class="post-tag tag-link-can" rel="tag" title="see questions tagged &#39;can&#39;">can</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '17, 00:48</strong></p><img src="https://secure.gravatar.com/avatar/af235f77f98a216c6c1e00d88f9d52a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kamran&#39;s gravatar image" /><p><span>Kamran</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kamran has no accepted answers">0%</span></p></div></div><div id="comments-container-60862" class="comments-container"></div><div id="comment-tools-60862" class="comment-tools"></div><div class="clear"></div><div id="comment-60862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60864"></span>

<div id="answer-container-60864" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60864-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60864-score" class="post-score" title="current number of votes">0</div><span id="post-60864-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Then it's either not a Wireshark capture file (e.g. it's text output redirected to a file), or the file got damaged (e.g. when using FTP file transfer in non-binary mode). Inspecting the file (e.g. with the <code>file</code> utility on Linux, or opening it with an ASCII text editor) could shed some light on the type of file it really is.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '17, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60864" class="comments-container"><span id="60866"></span><div id="comment-60866" class="comment"><div id="post-60866-score" class="comment-score"></div><div class="comment-text"><p>i can open this file in one PC but can not open in another PC.</p><p>(Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information).</p></div><div id="comment-60866-info" class="comment-info"><span class="comment-age">(17 Apr '17, 08:49)</span> <span class="comment-user userinfo">Kamran</span></div></div><span id="60867"></span><div id="comment-60867" class="comment"><div id="post-60867-score" class="comment-score"></div><div class="comment-text"><p>How did you copy this file from the one PC to the other ?</p></div><div id="comment-60867-info" class="comment-info"><span class="comment-age">(17 Apr '17, 09:19)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-60864" class="comment-tools"></div><div class="clear"></div><div id="comment-60864-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

