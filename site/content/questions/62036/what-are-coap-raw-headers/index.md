+++
type = "question"
title = "What are CoAP raw headers ?"
description = '''Can some answer me what are CoAP headers give CoAP headers both for request and response ?'''
date = "2017-06-14T21:42:00Z"
lastmod = "2017-06-14T21:42:00Z"
weight = 62036
keywords = [ "iot", "protocol", "coap", "wireshark" ]
aliases = [ "/questions/62036" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What are CoAP raw headers ?](/questions/62036/what-are-coap-raw-headers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62036-score" class="post-score" title="current number of votes">0</div><span id="post-62036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can some answer me what are CoAP headers give CoAP headers both for request and response ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iot" rel="tag" title="see questions tagged &#39;iot&#39;">iot</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-coap" rel="tag" title="see questions tagged &#39;coap&#39;">coap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '17, 21:42</strong></p><img src="https://secure.gravatar.com/avatar/88ef31b2d9a0ca87ae81dfd0207ff799?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sravan&#39;s gravatar image" /><p><span>sravan</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sravan has no accepted answers">0%</span></p></div></div><div id="comments-container-62036" class="comments-container"></div><div id="comment-tools-62036" class="comment-tools"></div><div class="clear"></div><div id="comment-62036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

