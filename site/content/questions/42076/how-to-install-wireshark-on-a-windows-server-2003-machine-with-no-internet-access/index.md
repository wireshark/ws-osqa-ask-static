+++
type = "question"
title = "[closed] How to install Wireshark on a Windows Server 2003 machine with no Internet access?"
description = '''Hi,would like to know how we install wireshark in server 2003 on esxi host,I am unable to get the network connectivity in my server 2003 in esxi.I have 10 CSR 1000v instances on whom I want to capture the packets and the 10 machines alongwith the server 2003 VM are in the same port group.'''
date = "2015-05-04T22:26:00Z"
lastmod = "2015-05-09T22:19:00Z"
weight = 42076
keywords = [ "690" ]
aliases = [ "/questions/42076" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to install Wireshark on a Windows Server 2003 machine with no Internet access?](/questions/42076/how-to-install-wireshark-on-a-windows-server-2003-machine-with-no-internet-access)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42076-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42076-score" class="post-score" title="current number of votes">0</div><span id="post-42076-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,would like to know how we install wireshark in server 2003 on esxi host,I am unable to get the network connectivity in my server 2003 in esxi.I have 10 CSR 1000v instances on whom I want to capture the packets and the 10 machines alongwith the server 2003 VM are in the same port group.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-690" rel="tag" title="see questions tagged &#39;690&#39;">690</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '15, 22:26</strong></p><img src="https://secure.gravatar.com/avatar/5dc091443223797523d89d9d2c90f377?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sand&#39;s gravatar image" /><p><span>sand</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sand has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>10 May '15, 03:20</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-42076" class="comments-container"></div><div id="comment-tools-42076" class="comment-tools"></div><div class="clear"></div><div id="comment-42076-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 10 May '15, 03:20

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="42111"></span>

<div id="answer-container-42111" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42111-score" class="post-score" title="current number of votes">2</div><span id="post-42111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You presumably can connect to the Internet on <em>some</em> machine, as you were able to post this question. :-)</p><p>Try downloading the Windows installer for Wireshark on that machine, or some other machine with Internet access, but <em>save</em> it rather than trying to <em>run</em> it. Then copy it to the machine on which you want to install it, and run it on that machine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '15, 16:40</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-42111" class="comments-container"><span id="42271"></span><div id="comment-42271" class="comment"><div id="post-42271-score" class="comment-score"></div><div class="comment-text"><p><span>@guy</span></p><p>Its working now, i had some issues accessing the shared folder from the VM.Thanks :-)</p></div><div id="comment-42271-info" class="comment-info"><span class="comment-age">(09 May '15, 22:17)</span> <span class="comment-user userinfo">sand</span></div></div></div><div id="comment-tools-42111" class="comment-tools"></div><div class="clear"></div><div id="comment-42111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="42085"></span>

<div id="answer-container-42085" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42085-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42085-score" class="post-score" title="current number of votes">1</div><span id="post-42085-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you want to capture traffic of other virtual machines in one VM (win2003), you must enable <strong>promiscuous mode</strong> on the vswitch and/or portgroup.</p><blockquote><p><a href="http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&amp;cmd=displayKC&amp;externalId=1004099">http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&amp;cmd=displayKC&amp;externalId=1004099</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '15, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-42085" class="comments-container"><span id="42090"></span><div id="comment-42090" class="comment"><div id="post-42090-score" class="comment-score"></div><div class="comment-text"><p>Hi Kurt,</p><p>I need to install wireshark in the server 2003 as attached in Pic 3.Promiscous mode is enabled on the vswitch and port group and still unable to see the network connectivity.Please advise.</p><p>Regards Sandeep<img src="https://osqa-ask.wireshark.org/upfiles/pic1_DB8SrCl.jpg" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/pic2.jpg" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/pic3.jpg" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/pic4.jpg" alt="alt text" /></p></div><div id="comment-42090-info" class="comment-info"><span class="comment-age">(05 May '15, 08:38)</span> <span class="comment-user userinfo">sand</span></div></div><span id="42091"></span><div id="comment-42091" class="comment"><div id="post-42091-score" class="comment-score"></div><div class="comment-text"><p><span>@sand</span>: I converted your answer to a comment, as that's how this Q&amp;A site works. See the FAQ.</p></div><div id="comment-42091-info" class="comment-info"><span class="comment-age">(05 May '15, 08:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="42093"></span><div id="comment-42093" class="comment"><div id="post-42093-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Promiscous mode is enabled on the vswitch and port group and still unable to see the network connectivity.Please advise.</p></blockquote><p>O.K. let's try to refine your problem description.</p><p>What do you mean exactly, if you say: "still unable to see the <strong>network connectivity</strong>"?</p><p>What are you trying to do? Is it what I assume in my answer, capturing traffic of other VMs in the Win2003 VM? If yes, do you get any error message while you are trying to capture traffic on Win2003?</p></div><div id="comment-42093-info" class="comment-info"><span class="comment-age">(05 May '15, 08:45)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="42098"></span><div id="comment-42098" class="comment"><div id="post-42098-score" class="comment-score"></div><div class="comment-text"><p>I have not started to capture the traffic yet as i have not been able to install wireshark on the server 2003 desktop.Basically,i am looking for a way to download wireshark either via internet ( which i am unable to connect to ) or some other way.</p><p>Regards Sandeep</p></div><div id="comment-42098-info" class="comment-info"><span class="comment-age">(05 May '15, 10:20)</span> <span class="comment-user userinfo">sand</span></div></div><span id="42272"></span><div id="comment-42272" class="comment"><div id="post-42272-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your help Kurt :-)</p></div><div id="comment-42272-info" class="comment-info"><span class="comment-age">(09 May '15, 22:19)</span> <span class="comment-user userinfo">sand</span></div></div></div><div id="comment-tools-42085" class="comment-tools"></div><div class="clear"></div><div id="comment-42085-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

