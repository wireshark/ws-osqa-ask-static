+++
type = "question"
title = "How to capture wireless network traffic?"
description = '''I just installed Wireshark on my Windows. I run the capture on my wireless network. I tried to visit some websites through my phone (my phone connected to the same wireless), but I didnt see the website that I just visited in wireshark in the list. Any idea what do I need to setup?'''
date = "2014-07-08T01:31:00Z"
lastmod = "2014-07-08T01:34:00Z"
weight = 34461
keywords = [ "wireshark" ]
aliases = [ "/questions/34461" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture wireless network traffic?](/questions/34461/how-to-capture-wireless-network-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34461-score" class="post-score" title="current number of votes">0</div><span id="post-34461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed Wireshark on my Windows. I run the capture on my wireless network.</p><p>I tried to visit some websites through my phone (my phone connected to the same wireless), but I didnt see the website that I just visited in wireshark in the list.</p><p>Any idea what do I need to setup?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jul '14, 01:31</strong></p><img src="https://secure.gravatar.com/avatar/8d1c0beac5f847d004651ed715cba014?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sandy007&#39;s gravatar image" /><p><span>Sandy007</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sandy007 has no accepted answers">0%</span></p></div></div><div id="comments-container-34461" class="comments-container"></div><div id="comment-tools-34461" class="comment-tools"></div><div class="clear"></div><div id="comment-34461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34462"></span>

<div id="answer-container-34462" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34462-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34462-score" class="post-score" title="current number of votes">0</div><span id="post-34462-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need an <a href="http://www.riverbed.com/products/performance-management-control/network-performance-management/wireless-packet-capture.html">AirPCAP adapter</a> to be able to do what you want on Windows. The problem is that you cannot enable monitor mode for "normal" WiFi cards on Windows. So either you get yourself an AirPCAP adapter, or you try Linux/Mac OS. You'd still have to enable monitor mode manually, e.g. by using airmon-ng.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jul '14, 01:34</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-34462" class="comments-container"></div><div id="comment-tools-34462" class="comment-tools"></div><div class="clear"></div><div id="comment-34462-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

