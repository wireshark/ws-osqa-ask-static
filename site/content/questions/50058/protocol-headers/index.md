+++
type = "question"
title = "Protocol Headers"
description = '''Can someone help? I am a student studying Networking and have an assignment to use wireshark to capture frames and then identify which layer of the TCP/IP model the header represents.   Ethernet II Header represents which layer (Application)? Internet Protocol header represents which layer (Transpor...'''
date = "2016-02-10T08:58:00Z"
lastmod = "2016-02-10T09:03:00Z"
weight = 50058
keywords = [ "tcp-ip-layers", "homework" ]
aliases = [ "/questions/50058" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Headers](/questions/50058/protocol-headers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50058-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50058-score" class="post-score" title="current number of votes">0</div><span id="post-50058-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone help? I am a student studying Networking and have an assignment to use wireshark to capture frames and then identify which layer of the TCP/IP model the header represents.</p><ol><li>Ethernet II Header represents which layer (Application)?</li><li>Internet Protocol header represents which layer (Transport)?</li><li>Transmission Control Protocol header represents which layer (Network Access)?</li><li>Hypertext Transfer Protocol header represents which layer (Internet)?</li></ol><p>The class is only online and so I have no examples to reference. Can someone help?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp-ip-layers" rel="tag" title="see questions tagged &#39;tcp-ip-layers&#39;">tcp-ip-layers</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '16, 08:58</strong></p><img src="https://secure.gravatar.com/avatar/ee1fb6fa7ea02719a0a1c250efcb44b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JodiLoe&#39;s gravatar image" /><p><span>JodiLoe</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JodiLoe has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Feb '16, 09:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50058" class="comments-container"></div><div id="comment-tools-50058" class="comment-tools"></div><div class="clear"></div><div id="comment-50058-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50059"></span>

<div id="answer-container-50059" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50059-score" class="post-score" title="current number of votes">2</div><span id="post-50059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>No, Application is wrong. This is trivial to research, so I suggest you get on Wikipedia etc. to find out.</li><li>No, not Transport, see 1.</li><li>Are you guessing? Wrong again :-)</li><li>And... no.</li></ol><p>This is really simple: google the words "Ethernet", "Internet Protocol", Transmission Control Protocol" and "Hyptertext Transfer Protocol" and you'll find the layers in no time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '16, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-50059" class="comments-container"></div><div id="comment-tools-50059" class="comment-tools"></div><div class="clear"></div><div id="comment-50059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

