+++
type = "question"
title = "wireshark dies with error wireshark: symbol lookup error: wireshark: undefined symbol: wtap_cleareof"
description = '''when trying to capture packets wireshark dies with the error; this is after doing a yum remove and a yum install of wireshark on fedora fc.14 Yum install Running Transaction  Installing : wireshark-1.4.0-2.fc14.x86_64 1/2   Cleanup : wireshark-1.4.2-2.fc14.x86_64'''
date = "2011-01-24T14:08:00Z"
lastmod = "2011-01-24T14:08:00Z"
weight = 1910
keywords = [ "wtag-cleareof" ]
aliases = [ "/questions/1910" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark dies with error wireshark: symbol lookup error: wireshark: undefined symbol: wtap\_cleareof](/questions/1910/wireshark-dies-with-error-wireshark-symbol-lookup-error-wireshark-undefined-symbol-wtap_cleareof)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1910-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1910-score" class="post-score" title="current number of votes">0</div><span id="post-1910-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when trying to capture packets wireshark dies with the error; this is after doing a yum remove and a yum install of wireshark on fedora fc.14</p><p>Yum install Running Transaction Installing : wireshark-1.4.0-2.fc14.x86_64 1/2 Cleanup : wireshark-1.4.2-2.fc14.x86_64</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wtag-cleareof" rel="tag" title="see questions tagged &#39;wtag-cleareof&#39;">wtag-cleareof</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '11, 14:08</strong></p><img src="https://secure.gravatar.com/avatar/8d559826e32de32836974c17433c04a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="k0jkj&#39;s gravatar image" /><p><span>k0jkj</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="k0jkj has no accepted answers">0%</span></p></div></div><div id="comments-container-1910" class="comments-container"></div><div id="comment-tools-1910" class="comment-tools"></div><div class="clear"></div><div id="comment-1910-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

