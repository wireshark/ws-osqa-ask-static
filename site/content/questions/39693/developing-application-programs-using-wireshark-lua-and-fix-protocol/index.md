+++
type = "question"
title = "Developing application programs using Wireshark, LUA and FIX Protocol"
description = '''Hi, Thanks in advance. I am currently using Wireshark 1.12.3 on Windows 8. I’ve also installed WinPcap _4_1_3. I am new to Wireshark. Is there any example code(s) on above to start with? Once again, thank you very much for the time you have given. Regards, Deb'''
date = "2015-02-07T02:31:00Z"
lastmod = "2015-02-11T03:05:00Z"
weight = 39693
keywords = [ "lua", "fix", "tshark", "wireshark" ]
aliases = [ "/questions/39693" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Developing application programs using Wireshark, LUA and FIX Protocol](/questions/39693/developing-application-programs-using-wireshark-lua-and-fix-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39693-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39693-score" class="post-score" title="current number of votes">0</div><span id="post-39693-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Thanks in advance. I am currently using Wireshark 1.12.3 on Windows 8. I’ve also installed WinPcap _4_1_3. I am new to Wireshark. Is there any example code(s) on above to start with? Once again, thank you very much for the time you have given. Regards, Deb</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-fix" rel="tag" title="see questions tagged &#39;fix&#39;">fix</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '15, 02:31</strong></p><img src="https://secure.gravatar.com/avatar/1ac9c0fcb115b7f3736da7bbe4f393dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deb&#39;s gravatar image" /><p><span>Deb</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deb has no accepted answers">0%</span></p></div></div><div id="comments-container-39693" class="comments-container"></div><div id="comment-tools-39693" class="comment-tools"></div><div class="clear"></div><div id="comment-39693-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39726"></span>

<div id="answer-container-39726" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39726-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39726-score" class="post-score" title="current number of votes">0</div><span id="post-39726-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could try to start with these:</p><blockquote><p><a href="http://wiki.wireshark.org/Lua">http://wiki.wireshark.org/Lua</a><br />
<a href="http://wiki.wireshark.org/Lua/Examples">http://wiki.wireshark.org/Lua/Examples</a><br />
<a href="http://wiki.wireshark.org/Lua/Dissectors">http://wiki.wireshark.org/Lua/Dissectors</a><br />
<a href="https://code.google.com/p/lua-bitstring/wiki/ExplainFastDissect">https://code.google.com/p/lua-bitstring/wiki/ExplainFastDissect</a><br />
<a href="http://blog.roisu.org/english-create-a-wireshark-dissector-with-lua/">http://blog.roisu.org/english-create-a-wireshark-dissector-with-lua/</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '15, 15:51</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-39726" class="comments-container"><span id="39791"></span><div id="comment-39791" class="comment"><div id="post-39791-score" class="comment-score"></div><div class="comment-text"><p>Kurt,</p><p>Thanks for this information.</p><p>It'll help me.</p><p>Regards,</p><p>Deb</p></div><div id="comment-39791-info" class="comment-info"><span class="comment-age">(11 Feb '15, 03:05)</span> <span class="comment-user userinfo">Deb</span></div></div></div><div id="comment-tools-39726" class="comment-tools"></div><div class="clear"></div><div id="comment-39726-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

