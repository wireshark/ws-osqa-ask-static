+++
type = "question"
title = "RTMPT just not showing up..."
description = '''I&#x27;m trying to decode RTMP (real-time messaging protocol) in Wireshark. I just installed Wireshark v1.6.2 on Windows Vista 64 bit. The RTMP packets show up in the trace but are not decoded. The tcp port number if correctly displayed as macromedia-fcs, but the packets are displayed as TCP frames with ...'''
date = "2011-10-13T20:37:00Z"
lastmod = "2011-10-17T07:48:00Z"
weight = 6883
keywords = [ "rtmpt", "vista" ]
aliases = [ "/questions/6883" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTMPT just not showing up...](/questions/6883/rtmpt-just-not-showing-up)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6883-score" class="post-score" title="current number of votes">0</div><span id="post-6883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to decode RTMP (real-time messaging protocol) in Wireshark. I just installed Wireshark v1.6.2 on Windows Vista 64 bit. The RTMP packets show up in the trace but are not decoded. The tcp port number if correctly displayed as macromedia-fcs, but the packets are displayed as TCP frames with undecoded data payloads. When I right click on a frame and select "Decode As...", RTMPT shows up no where in the interface. Lots of protocols, but no RTMPT. Any idea what's happening? Do I have to install a separate plugin or some such thing?</p><p>Thanks so much! And by the way, this tool is just amazingly useful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtmpt" rel="tag" title="see questions tagged &#39;rtmpt&#39;">rtmpt</span> <span class="post-tag tag-link-vista" rel="tag" title="see questions tagged &#39;vista&#39;">vista</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '11, 20:37</strong></p><img src="https://secure.gravatar.com/avatar/8a925f34cf40997541c5d40599bcf993?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Walter%20Finerman&#39;s gravatar image" /><p><span>Walter Finerman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Walter Finerman has no accepted answers">0%</span></p></div></div><div id="comments-container-6883" class="comments-container"></div><div id="comment-tools-6883" class="comment-tools"></div><div class="clear"></div><div id="comment-6883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6928"></span>

<div id="answer-container-6928" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6928-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6928-score" class="post-score" title="current number of votes">0</div><span id="post-6928-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'll bet that the RTMPT protocol is disabled (<code>Analyze ! Enabled  Protocols</code>) in your configuration. :)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '11, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-6928" class="comments-container"></div><div id="comment-tools-6928" class="comment-tools"></div><div class="clear"></div><div id="comment-6928-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

