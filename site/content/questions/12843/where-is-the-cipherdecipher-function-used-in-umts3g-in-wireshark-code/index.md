+++
type = "question"
title = "where is the cipher/decipher function used in umts(3G) in wireshark code?"
description = '''I can&#x27;t find the cipher/decipher function used in umts(3G). I have to make sure the (RLC)UM, (RLC)AM ,MAC -d, have cipher/decipher function.'''
date = "2012-07-19T01:38:00Z"
lastmod = "2012-07-19T02:35:00Z"
weight = 12843
keywords = [ "decipher", "um", "cipher", "am" ]
aliases = [ "/questions/12843" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [where is the cipher/decipher function used in umts(3G) in wireshark code?](/questions/12843/where-is-the-cipherdecipher-function-used-in-umts3g-in-wireshark-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12843-score" class="post-score" title="current number of votes">0</div><span id="post-12843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't find the cipher/decipher function used in umts(3G). I have to make sure the (RLC)UM, (RLC)AM ,MAC -d, have cipher/decipher function.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decipher" rel="tag" title="see questions tagged &#39;decipher&#39;">decipher</span> <span class="post-tag tag-link-um" rel="tag" title="see questions tagged &#39;um&#39;">um</span> <span class="post-tag tag-link-cipher" rel="tag" title="see questions tagged &#39;cipher&#39;">cipher</span> <span class="post-tag tag-link-am" rel="tag" title="see questions tagged &#39;am&#39;">am</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '12, 01:38</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-12843" class="comments-container"></div><div id="comment-tools-12843" class="comment-tools"></div><div class="clear"></div><div id="comment-12843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12849"></span>

<div id="answer-container-12849" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12849-score" class="post-score" title="current number of votes">0</div><span id="post-12849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Nowhere as no one has implemented it, unless it uses functions available trough OPENssl or TLS.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jul '12, 02:35</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-12849" class="comments-container"></div><div id="comment-tools-12849" class="comment-tools"></div><div class="clear"></div><div id="comment-12849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

