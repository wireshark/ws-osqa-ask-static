+++
type = "question"
title = "Relationship Indicators"
description = '''Hello: Just found out about the relationship indicators. Just there seems to be an interesting thing going on in one capture, there are a series of dots in the column, any idea what they represent?'''
date = "2017-06-26T07:02:00Z"
lastmod = "2017-06-27T06:37:00Z"
weight = 62302
keywords = [ "indicator", "relationship" ]
aliases = [ "/questions/62302" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Relationship Indicators](/questions/62302/relationship-indicators)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62302-score" class="post-score" title="current number of votes">0</div><span id="post-62302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello:</p><p>Just found out about the relationship indicators.</p><p>Just there seems to be an interesting thing going on in one capture, there are a series of dots in the column, any idea what they represent?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-indicator" rel="tag" title="see questions tagged &#39;indicator&#39;">indicator</span> <span class="post-tag tag-link-relationship" rel="tag" title="see questions tagged &#39;relationship&#39;">relationship</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '17, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/fbe6825b890bc4c637a5160e6fb707a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pauli&#39;s gravatar image" /><p><span>Pauli</span><br />
<span class="score" title="0 reputation points">0</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pauli has no accepted answers">0%</span></p></div></div><div id="comments-container-62302" class="comments-container"></div><div id="comment-tools-62302" class="comment-tools"></div><div class="clear"></div><div id="comment-62302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62304"></span>

<div id="answer-container-62304" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62304-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62304-score" class="post-score" title="current number of votes">0</div><span id="post-62304-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The "vertical line with a dot" symbol means, <em>"The selected packet is related to this packet in some other way, e.g. as part of reassembly."</em></p><p>All related packet symbols are described in <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketListPaneSection.html#idp8859247504">Table 3.15 of the Wireshark User's Guide</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jun '17, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-62304" class="comments-container"><span id="62330"></span><div id="comment-62330" class="comment"><div id="post-62330-score" class="comment-score"></div><div class="comment-text"><p>Thank you</p></div><div id="comment-62330-info" class="comment-info"><span class="comment-age">(27 Jun '17, 06:20)</span> <span class="comment-user userinfo">Pauli</span></div></div><span id="62332"></span><div id="comment-62332" class="comment"><div id="post-62332-score" class="comment-score"></div><div class="comment-text"><p>Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p></div><div id="comment-62332-info" class="comment-info"><span class="comment-age">(27 Jun '17, 06:37)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="62333"></span><div id="comment-62333" class="comment"><div id="post-62333-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-62333-info" class="comment-info"><span class="comment-age">(27 Jun '17, 06:37)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-62304" class="comment-tools"></div><div class="clear"></div><div id="comment-62304-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

