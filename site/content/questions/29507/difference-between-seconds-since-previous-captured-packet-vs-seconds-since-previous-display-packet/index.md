+++
type = "question"
title = "difference between seconds since previous captured packet vs seconds since previous Display packet"
description = '''Hi What is the difference between seconds since previous captured packet vs seconds since previous Display packet. what does it mean captured packet and display packet. thanks'''
date = "2014-02-06T21:57:00Z"
lastmod = "2014-02-07T04:41:00Z"
weight = 29507
keywords = [ "timestamp" ]
aliases = [ "/questions/29507" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [difference between seconds since previous captured packet vs seconds since previous Display packet](/questions/29507/difference-between-seconds-since-previous-captured-packet-vs-seconds-since-previous-display-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29507-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29507-score" class="post-score" title="current number of votes">0</div><span id="post-29507-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>What is the difference between seconds since previous captured packet vs seconds since previous Display packet. what does it mean captured packet and display packet.</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '14, 21:57</strong></p><img src="https://secure.gravatar.com/avatar/be8a9b2e9d87b13606c3b9e75d26e71d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scara&#39;s gravatar image" /><p><span>scara</span><br />
<span class="score" title="31 reputation points">31</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scara has no accepted answers">0%</span></p></div></div><div id="comments-container-29507" class="comments-container"></div><div id="comment-tools-29507" class="comment-tools"></div><div class="clear"></div><div id="comment-29507-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29508"></span>

<div id="answer-container-29508" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29508-score" class="post-score" title="current number of votes">1</div><span id="post-29508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Simply the following:</p><p>If you've applied a display filter so that not all of the frames are being displayed in the summary pane, then the "time since" which is shown can be set to be the "time since the frame which is displayed before this one" or the "time since the previous captured frame" (even if not displayed).</p><p>Try both with a display filter applied; you'll see the difference.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '14, 22:15</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-29508" class="comments-container"><span id="29509"></span><div id="comment-29509" class="comment"><div id="post-29509-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-29509-info" class="comment-info"><span class="comment-age">(06 Feb '14, 22:16)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="29521"></span><div id="comment-29521" class="comment"><div id="post-29521-score" class="comment-score"></div><div class="comment-text"><p>Hi i am not applying any filters here if you go to View&gt;Time Display format&gt; there are different formats for time. in that you have "seconds since previous captured packet" and "seconds since previous Display packet"</p></div><div id="comment-29521-info" class="comment-info"><span class="comment-age">(07 Feb '14, 03:24)</span> <span class="comment-user userinfo">scara</span></div></div><span id="29522"></span><div id="comment-29522" class="comment"><div id="post-29522-score" class="comment-score"></div><div class="comment-text"><p>"seconds since previous Display packet" will only have an effect <strong>if you apply a filter</strong>.</p><p>See also the manual: <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChWorkTimeFormatsSection.html">http://www.wireshark.org/docs/wsug_html_chunked/ChWorkTimeFormatsSection.html</a></p></div><div id="comment-29522-info" class="comment-info"><span class="comment-age">(07 Feb '14, 03:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29525"></span><div id="comment-29525" class="comment"><div id="post-29525-score" class="comment-score"></div><div class="comment-text"><p>If you have no filters applied then the two time options are equivalent since the previous displayed packet will be the same as the previous captured packet.</p><p>With a filter applied this might not be the case as the previous captured packet may not be displayed by the filter.</p></div><div id="comment-29525-info" class="comment-info"><span class="comment-age">(07 Feb '14, 04:41)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-29508" class="comment-tools"></div><div class="clear"></div><div id="comment-29508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

