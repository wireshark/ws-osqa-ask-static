+++
type = "question"
title = "Monitor mode in Wireshark"
description = '''I am trying to view 802.11 frames from Wireshark and after researching in youtube, I see that I don&#x27;t have the monitor mode. What do I need so that I can see 802.11 frames from Wireshark. Thanks'''
date = "2017-02-13T19:11:00Z"
lastmod = "2017-02-13T23:02:00Z"
weight = 59393
keywords = [ "wireless", "wifi", "monitor-mode" ]
aliases = [ "/questions/59393" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor mode in Wireshark](/questions/59393/monitor-mode-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59393-score" class="post-score" title="current number of votes">0</div><span id="post-59393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to view 802.11 frames from Wireshark and after researching in youtube, I see that I don't have the monitor mode. What do I need so that I can see 802.11 frames from Wireshark. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '17, 19:11</strong></p><img src="https://secure.gravatar.com/avatar/b68ecd02e11309f135e5caf5e144f2a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jaja&#39;s gravatar image" /><p><span>jaja</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jaja has no accepted answers">0%</span></p></div></div><div id="comments-container-59393" class="comments-container"><span id="59395"></span><div id="comment-59395" class="comment"><div id="post-59395-score" class="comment-score"></div><div class="comment-text"><p>On which operating system are you running Wireshark? (The mechanism for getting monitor mode differs from operating system to operating system; libpcap/Npcap tries to hide those details, but it doesn't always work.)</p></div><div id="comment-59395-info" class="comment-info"><span class="comment-age">(13 Feb '17, 22:37)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-59393" class="comment-tools"></div><div class="clear"></div><div id="comment-59393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59396"></span>

<div id="answer-container-59396" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59396-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59396-score" class="post-score" title="current number of votes">0</div><span id="post-59396-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark may supports for monitor mode by Linux, FreeBSD, NetBSD, OpenBSD, DragonFly BSD, and Mac OS X.</p><p>Please refer to the following article.</p><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">wire shaark wiki</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '17, 23:02</strong></p><img src="https://secure.gravatar.com/avatar/1634bd3b8ec82c26229a3c3890c7c7ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="light%20the%20lightning&#39;s gravatar image" /><p><span>light the li...</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="light the lightning has no accepted answers">0%</span></p></div></div><div id="comments-container-59396" class="comments-container"></div><div id="comment-tools-59396" class="comment-tools"></div><div class="clear"></div><div id="comment-59396-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

