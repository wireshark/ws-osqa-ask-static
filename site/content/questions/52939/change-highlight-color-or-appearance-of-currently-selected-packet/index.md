+++
type = "question"
title = "Change highlight color or appearance of currently selected packet?"
description = '''This has been asked before but no useable answer.  I want to change the selected packet line colouring to something that stands out, it just gets lost in the many and varied colours currently showing on the screen, how can i do this? All very well to point to gtk file but it has no rules even close ...'''
date = "2016-05-25T19:34:00Z"
lastmod = "2016-05-26T14:58:00Z"
weight = 52939
keywords = [ "color-rules" ]
aliases = [ "/questions/52939" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Change highlight color or appearance of currently selected packet?](/questions/52939/change-highlight-color-or-appearance-of-currently-selected-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52939-score" class="post-score" title="current number of votes">0</div><span id="post-52939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This has been asked before but no useable answer.</p><p>I want to change the selected packet line colouring to something that stands out, it just gets lost in the many and varied colours currently showing on the screen, how can i do this?</p><p>All very well to point to gtk file but it has no rules even close to colouring that I can change. Color rules let me choose protocols and schemes but not the actual selected line.</p><p>Wireshark 2.0.3 Windows 7 professional and 32bit OS. Can you help?</p><p>also where are there downloadable alternative colouring schemes? It seems people would create a great scheme and want to then resuse or share?</p><p>yhanks for any ideas.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color-rules" rel="tag" title="see questions tagged &#39;color-rules&#39;">color-rules</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '16, 19:34</strong></p><img src="https://secure.gravatar.com/avatar/34021ef9827d810458756fd495c280e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Min%20Sik&#39;s gravatar image" /><p><span>Min Sik</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Min Sik has no accepted answers">0%</span></p></div></div><div id="comments-container-52939" class="comments-container"></div><div id="comment-tools-52939" class="comment-tools"></div><div class="clear"></div><div id="comment-52939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52974"></span>

<div id="answer-container-52974" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52974-score" class="post-score" title="current number of votes">0</div><span id="post-52974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you want to change coloring schemes in the way you're describing you're going to have to change code. Either Gtk code (pre-2.0) or Qt code (post-2.0). How to actually achieve that is an exercise best left to the reader--it would take some research and that research would be best undertaken by the person actually interested in doing it.</p><p>And, no, there's no alternative coloring schemes. Wireshark doesn't support the concept of coloring schemes. There are a few color choices in the preferences and there are the coloring rules but that's it. Again, anything else will require you to write some code to achieve it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 May '16, 14:58</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-52974" class="comments-container"></div><div id="comment-tools-52974" class="comment-tools"></div><div class="clear"></div><div id="comment-52974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

