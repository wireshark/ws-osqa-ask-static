+++
type = "question"
title = "Getting detailed 5-tuple flow information"
description = '''I am trying to analyse a file containing packets captured using tcpdump. I first want to categorize the packets into flows using 5-tuple. Then I need to get the size and inter-arrival time of each packet in each flow. I tried Conversation list in wireshark but it gives only the number of packets in ...'''
date = "2012-04-17T01:52:00Z"
lastmod = "2012-10-31T18:15:00Z"
weight = 10214
keywords = [ "flow" ]
aliases = [ "/questions/10214" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Getting detailed 5-tuple flow information](/questions/10214/getting-detailed-5-tuple-flow-information)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10214-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10214-score" class="post-score" title="current number of votes">0</div><span id="post-10214-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to analyse a file containing packets captured using tcpdump. I first want to categorize the packets into flows using 5-tuple. Then I need to get the size and inter-arrival time of each packet in each flow. I tried Conversation list in wireshark but it gives only the number of packets in the flow not information about each packet in the flow. Any suggestion on how to proceed? Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '12, 01:52</strong></p><img src="https://secure.gravatar.com/avatar/3c2953e5b88fa02103404c5b919423fe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tesse&#39;s gravatar image" /><p><span>Tesse</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tesse has no accepted answers">0%</span></p></div></div><div id="comments-container-10214" class="comments-container"><span id="10352"></span><div id="comment-10352" class="comment"><div id="post-10352-score" class="comment-score"></div><div class="comment-text"><p>Thank you guys..... I just solved it</p></div><div id="comment-10352-info" class="comment-info"><span class="comment-age">(20 Apr '12, 09:41)</span> <span class="comment-user userinfo">Tesse</span></div></div><span id="10357"></span><div id="comment-10357" class="comment"><div id="post-10357-score" class="comment-score">1</div><div class="comment-text"><p>I converted your response to a comment as it didn't actually answer the question. If you do have the answer please post it for the benefit of all the other site users.</p></div><div id="comment-10357-info" class="comment-info"><span class="comment-age">(20 Apr '12, 12:11)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="15439"></span><div id="comment-15439" class="comment"><div id="post-15439-score" class="comment-score"></div><div class="comment-text"><p>Hi Tesse, I have your same problem. can you please tell how you solved it?<br />
</p></div><div id="comment-15439-info" class="comment-info"><span class="comment-age">(31 Oct '12, 18:15)</span> <span class="comment-user userinfo">hy2012</span></div></div></div><div id="comment-tools-10214" class="comment-tools"></div><div class="clear"></div><div id="comment-10214-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

