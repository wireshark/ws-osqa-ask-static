+++
type = "question"
title = "Changing the libcrypto version for ethereal"
description = '''Hi, I need to change the libcrypto version used by ethereal. Where are the dependencies defined. How could I change the libcrypto version used by ethereal to a version higher? Thanks and Regards, Aparna '''
date = "2014-04-28T03:04:00Z"
lastmod = "2014-04-28T03:55:00Z"
weight = 32239
keywords = [ "tethereal", "libcrypto" ]
aliases = [ "/questions/32239" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Changing the libcrypto version for ethereal](/questions/32239/changing-the-libcrypto-version-for-ethereal)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32239-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32239-score" class="post-score" title="current number of votes">0</div><span id="post-32239-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I need to change the libcrypto version used by ethereal. Where are the dependencies defined. How could I change the libcrypto version used by ethereal to a version higher?</p><p>Thanks and Regards,</p><p>Aparna</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tethereal" rel="tag" title="see questions tagged &#39;tethereal&#39;">tethereal</span> <span class="post-tag tag-link-libcrypto" rel="tag" title="see questions tagged &#39;libcrypto&#39;">libcrypto</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '14, 03:04</strong></p><img src="https://secure.gravatar.com/avatar/b605d47d2e423a49d4a281eb597b9fba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aparna&#39;s gravatar image" /><p><span>Aparna</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aparna has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Apr '14, 03:06</strong> </span></p></div></div><div id="comments-container-32239" class="comments-container"><span id="32241"></span><div id="comment-32241" class="comment"><div id="post-32241-score" class="comment-score"></div><div class="comment-text"><p>If you're using Ethereal, then you really should move up to Wireshark. Ethereal hasn't been supported for a long time.</p></div><div id="comment-32241-info" class="comment-info"><span class="comment-age">(28 Apr '14, 03:19)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="32242"></span><div id="comment-32242" class="comment"><div id="post-32242-score" class="comment-score"></div><div class="comment-text"><p>I am planning to move to wireshark. Considering the mess with cross compilation I will have to first fix the ethereal issue for immediate use.</p></div><div id="comment-32242-info" class="comment-info"><span class="comment-age">(28 Apr '14, 03:32)</span> <span class="comment-user userinfo">Aparna</span></div></div><span id="32246"></span><div id="comment-32246" class="comment"><div id="post-32246-score" class="comment-score"></div><div class="comment-text"><p>Likely to be on your own then, unfortunately. The dependencies are spread around the automake scripts.</p></div><div id="comment-32246-info" class="comment-info"><span class="comment-age">(28 Apr '14, 03:55)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-32239" class="comment-tools"></div><div class="clear"></div><div id="comment-32239-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

