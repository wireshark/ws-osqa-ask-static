+++
type = "question"
title = "Is This a Normal Traffic?"
description = ''' I&#x27;m new to wireshark i capture my network traffic it seems abnormal, can anyone tell me is this a normal traffic or not? why its showing red and black color is it harmful?'''
date = "2017-01-23T20:41:00Z"
lastmod = "2017-01-24T11:44:00Z"
weight = 58994
keywords = [ "wireshark" ]
aliases = [ "/questions/58994" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is This a Normal Traffic?](/questions/58994/is-this-a-normal-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58994-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58994-score" class="post-score" title="current number of votes">0</div><span id="post-58994-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/Screenshot_(92).png" alt="alt text" /> I'm new to wireshark i capture my network traffic it seems abnormal, can anyone tell me is this a normal traffic or not? why its showing red and black color is it harmful?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '17, 20:41</strong></p><img src="https://secure.gravatar.com/avatar/1e868e7b18571fbced25cdda73c04e47?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Adi321&#39;s gravatar image" /><p><span>Adi321</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Adi321 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-58994" class="comments-container"></div><div id="comment-tools-58994" class="comment-tools"></div><div class="clear"></div><div id="comment-58994-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58999"></span>

<div id="answer-container-58999" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58999-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58999-score" class="post-score" title="current number of votes">3</div><span id="post-58999-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark comes with a default set of colouring rules. TCP resets for example are marked red. TCP retransmits are marked black. Both (retransmits, resets) are pretty common for Internet traffic.</p><p>There are discussions in the Wireshark community if the default colouring rules are helpful or not for a newbie.</p><p>You can use both events to learn how TCP works.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '17, 01:19</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-58999" class="comments-container"><span id="59016"></span><div id="comment-59016" class="comment"><div id="post-59016-score" class="comment-score"></div><div class="comment-text"><p>Thank You :)</p></div><div id="comment-59016-info" class="comment-info"><span class="comment-age">(24 Jan '17, 11:44)</span> <span class="comment-user userinfo">Adi321</span></div></div></div><div id="comment-tools-58999" class="comment-tools"></div><div class="clear"></div><div id="comment-58999-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

