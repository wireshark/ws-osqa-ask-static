+++
type = "question"
title = "Request: Decode as - save and load settings"
description = '''To be able to save and load dissector overrides in the &quot;decode as&quot; dialogue. On private networks, especially in the SCADA world, none standard and multiple ports are used which can make selecting the right dissector each time Wireshark is started a chor.'''
date = "2011-05-12T10:02:00Z"
lastmod = "2012-03-24T08:27:00Z"
weight = 4053
keywords = [ "save", "dissector", "request" ]
aliases = [ "/questions/4053" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Request: Decode as - save and load settings](/questions/4053/request-decode-as-save-and-load-settings)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4053-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4053-score" class="post-score" title="current number of votes">0</div><span id="post-4053-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>To be able to save and load dissector overrides in the "decode as" dialogue.</p><p>On private networks, especially in the SCADA world, none standard and multiple ports are used which can make selecting the right dissector each time Wireshark is started a chor.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '11, 10:02</strong></p><img src="https://secure.gravatar.com/avatar/e02fcbac42180de96eeb73c80b6faa33?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Graemem&#39;s gravatar image" /><p><span>Graemem</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Graemem has no accepted answers">0%</span></p></div></div><div id="comments-container-4053" class="comments-container"></div><div id="comment-tools-4053" class="comment-tools"></div><div class="clear"></div><div id="comment-4053-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4057"></span>

<div id="answer-container-4057" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4057-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4057-score" class="post-score" title="current number of votes">0</div><span id="post-4057-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=2931">Bug 2931</a> was filed for this on October 2, 2008 by Toralf Forster, but thus far this functionality has not been implemented by anyone.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 May '11, 11:14</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-4057" class="comments-container"><span id="9734"></span><div id="comment-9734" class="comment"><div id="post-9734-score" class="comment-score"></div><div class="comment-text"><p>The requested functionality was added to Wireshark in September 2011 and is part of the Wireshark 1.7 dev releases and will be part of the future Wireshark 1.8 release. (See Bug 2931 for details).</p></div><div id="comment-9734-info" class="comment-info"><span class="comment-age">(24 Mar '12, 08:27)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-4057" class="comment-tools"></div><div class="clear"></div><div id="comment-4057-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4058"></span>

<div id="answer-container-4058" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4058-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4058-score" class="post-score" title="current number of votes">0</div><span id="post-4058-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Another common solution is to modify the dissector(s) in question to have preferences for the port(s) used. Port ranges can be set up too (so the user can say, for example, that ports 13,42,100-200 all get grabbed by the protocol in question).</p><p>BTW, this is a Q&amp;A site, you might want to put RFEs in the bug database instead.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 May '11, 11:24</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-4058" class="comments-container"></div><div id="comment-tools-4058" class="comment-tools"></div><div class="clear"></div><div id="comment-4058-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

