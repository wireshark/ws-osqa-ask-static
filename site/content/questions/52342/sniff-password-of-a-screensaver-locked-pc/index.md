+++
type = "question"
title = "Sniff Password of a Screensaver-locked PC"
description = '''Hi !  someone can tell me if it is possible to capture with wireshark password of a locked computer (standby state)? Best Regards. vince66'''
date = "2016-05-09T05:11:00Z"
lastmod = "2016-05-09T23:09:00Z"
weight = 52342
keywords = [ "passwords" ]
aliases = [ "/questions/52342" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sniff Password of a Screensaver-locked PC](/questions/52342/sniff-password-of-a-screensaver-locked-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52342-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52342-score" class="post-score" title="current number of votes">0</div><span id="post-52342-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ! someone can tell me if it is possible to capture with wireshark password of a locked computer (standby state)?</p><p>Best Regards.</p><p>vince66</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-passwords" rel="tag" title="see questions tagged &#39;passwords&#39;">passwords</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '16, 05:11</strong></p><img src="https://secure.gravatar.com/avatar/235bac749e6ab32ff2f80808e9bbd091?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vince66&#39;s gravatar image" /><p><span>vince66</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vince66 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 May '16, 05:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-52342" class="comments-container"></div><div id="comment-tools-52342" class="comment-tools"></div><div class="clear"></div><div id="comment-52342-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52343"></span>

<div id="answer-container-52343" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52343-score" class="post-score" title="current number of votes">0</div><span id="post-52343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, that's not possible.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '16, 05:14</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span> </br></p></div></div><div id="comments-container-52343" class="comments-container"><span id="52352"></span><div id="comment-52352" class="comment"><div id="post-52352-score" class="comment-score"></div><div class="comment-text"><p>Dear Jasper, thanks very much for your replay. However, I want to be more precise. My need is to dump RAM memory of a locked computer (windows based) without rebooting it to retrieve forensic evidence ! There are several tools for the recovery of the admin's password but they needed the machine is booted. So I'm interesting to know if a network method exists to sniff the password via wireshark or to hacker the memory in a way. Replay is again no, that's no possible with wireshark ?</p><p>Thanks in advance.</p><p>Best Regards.</p></div><div id="comment-52352-info" class="comment-info"><span class="comment-age">(09 May '16, 09:23)</span> <span class="comment-user userinfo">vince66</span></div></div><span id="52380"></span><div id="comment-52380" class="comment"><div id="post-52380-score" class="comment-score"></div><div class="comment-text"><p>No it's still not possible, because the locked machine has no reason at all to transmit the password via network card (which would be required to record it with Wireshark).</p><p>Usually, if you need access to a locked PC you can try to access it via the usual Firewire DMA attacks, or using deep freezing techniques to retain information in RAM even after shutting down the PC (which is probably not realistic as it requires a professional forensic lab with the equipment to access the frozen RAM units)</p></div><div id="comment-52380-info" class="comment-info"><span class="comment-age">(09 May '16, 23:09)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-52343" class="comment-tools"></div><div class="clear"></div><div id="comment-52343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

