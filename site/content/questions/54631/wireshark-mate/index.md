+++
type = "question"
title = "Wireshark MATE"
description = '''Hi, Any plans to continue Wireshark MATE plugin and expand on its current capabilities ? I heard that the author has discontinued development of MATE'''
date = "2016-08-07T04:25:00Z"
lastmod = "2016-08-07T05:10:00Z"
weight = 54631
keywords = [ "mate", "wireshark" ]
aliases = [ "/questions/54631" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark MATE](/questions/54631/wireshark-mate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54631-score" class="post-score" title="current number of votes">0</div><span id="post-54631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Any plans to continue Wireshark MATE plugin and expand on its current capabilities ? I heard that the author has discontinued development of MATE</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mate" rel="tag" title="see questions tagged &#39;mate&#39;">mate</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '16, 04:25</strong></p><img src="https://secure.gravatar.com/avatar/4a2a1ab8f8fa05aa1d21e5b43f767aae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sshark&#39;s gravatar image" /><p><span>sshark</span><br />
<span class="score" title="6 reputation points">6</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sshark has no accepted answers">0%</span></p></div></div><div id="comments-container-54631" class="comments-container"></div><div id="comment-tools-54631" class="comment-tools"></div><div class="clear"></div><div id="comment-54631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="54632"></span>

<div id="answer-container-54632" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54632-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54632-score" class="post-score" title="current number of votes">0</div><span id="post-54632-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The original author seems to have moved on to other things, but it is still maintained for the rest of the Core developers.</p><p>Enhancements would always be considered if someone <a href="https://wiki.wireshark.org/Development/SubmittingPatches">submits them</a>, and enhancement requests can be made on <a href="https://bugs.wireshark.org">Bugzilla</a>, but unless someone is motivated enough to work on an enhancement request then it will just languish.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Aug '16, 05:03</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-54632" class="comments-container"></div><div id="comment-tools-54632" class="comment-tools"></div><div class="clear"></div><div id="comment-54632-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="54633"></span>

<div id="answer-container-54633" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54633-score" class="post-score" title="current number of votes">0</div><span id="post-54633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is an open source project. There is always more to do than the developers' spare time allows, so the developers have to choose according to their own interest and the demand from the users.</p><p>So if you (like me and, most likely, also other typical users of MATE) are not fluent enough in C to move MATE forward yourself, vote for <a href="https://bugs.wireshark.org/bugzilla/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=CONFIRMED&amp;bug_status=IN_PROGRESS&amp;bug_status=INCOMPLETE&amp;f0=OP&amp;f1=OP&amp;f2=product&amp;f3=component&amp;f4=alias&amp;f5=short_desc&amp;f6=status_whiteboard&amp;f7=content&amp;f8=CP&amp;f9=CP&amp;j1=OR&amp;list_id=26552&amp;o2=substring&amp;o3=substring&amp;o4=substring&amp;o5=substring&amp;o6=substring&amp;o7=matches&amp;product=Wireshark&amp;query_format=advanced&amp;short_desc=MATE&amp;short_desc_type=casesubstring&amp;v2=MATE&amp;v3=MATE&amp;v4=MATE&amp;v5=MATE&amp;v6=MATE&amp;v7=%22MATE%22">the MATE-related bugs and enhancement requests at Wireshark bugzilla</a> to indicate to the developers the amount of public interest in MATE. So far the bugs I have filed there haven't attracted any attention.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Aug '16, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Aug '16, 05:32</strong> </span></p></div></div><div id="comments-container-54633" class="comments-container"></div><div id="comment-tools-54633" class="comment-tools"></div><div class="clear"></div><div id="comment-54633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

