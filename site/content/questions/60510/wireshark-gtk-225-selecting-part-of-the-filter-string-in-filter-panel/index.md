+++
type = "question"
title = "wireshark-gtk 2.2.5 selecting part of the filter string in filter panel"
description = '''Hello I&#x27;m running RHEL7 linux and updated my wireshark to 2.2.5  now the background does not turn dark when I mark a string in a filter field. Anybody else suffering this problem?   Version 2.2.5 (Git Rev Unknown from unknown) The same version in windows legacy does not hae the problem...    Copyrig...'''
date = "2017-04-01T09:19:00Z"
lastmod = "2017-04-03T10:25:00Z"
weight = 60510
keywords = [ "wireshark-gtk", "color", "background", "linux" ]
aliases = [ "/questions/60510" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark-gtk 2.2.5 selecting part of the filter string in filter panel](/questions/60510/wireshark-gtk-225-selecting-part-of-the-filter-string-in-filter-panel)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60510-score" class="post-score" title="current number of votes">0</div><span id="post-60510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I'm running RHEL7 linux and updated my wireshark to 2.2.5 now the background does not turn dark when I mark a string in a filter field.</p><p>Anybody else suffering this problem?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Selection_301.png" alt="alt text" /> Version 2.2.5 (Git Rev Unknown from unknown)</p><p>The same version in windows legacy does not hae the problem... <img src="https://osqa-ask.wireshark.org/upfiles/Selection_302.png" alt="alt text" /></p><hr /><p>Copyright 1998-2017 Gerald Combs <span><span class="__cf_email__" data-cfemail="72151700131e1632051b0017011a1300195c1d0015">[email protected]</span></span> and contributors. License GPLv2+: GNU GPL version 2 or later <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">http://www.gnu.org/licenses/old-licenses/gpl-2.0.html</a> This is free software; see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</p><p>Compiled (64-bit) with GTK+ 3.14.13, with Cairo 1.14.2, with Pango 1.36.8, with libpcap, without POSIX capabilities, without libnl, with GLib 2.46.2, with zlib 1.2.7, without SMI, with c-ares 1.10.0, without Lua, without GnuTLS, without Gcrypt, with MIT Kerberos, without GeoIP, without PortAudio, without AirPcap.</p><p>Running on Linux 3.10.0-514.6.1.el7.x86_64, with locale en_US.utf8, with libpcap version 1.5.3, with zlib 1.2.7. Intel(R) Core(TM) i5-5300U CPU @ 2.30GHz (with SSE4.2)</p><p>Built using gcc 4.8.5 20150623 (Red Hat 4.8.5-11).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark-gtk" rel="tag" title="see questions tagged &#39;wireshark-gtk&#39;">wireshark-gtk</span> <span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span> <span class="post-tag tag-link-background" rel="tag" title="see questions tagged &#39;background&#39;">background</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '17, 09:19</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Apr '17, 00:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></img></div></div><div id="comments-container-60510" class="comments-container"><span id="60512"></span><div id="comment-60512" class="comment"><div id="post-60512-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "turn dark"? Do you mean "turn some color other than green"?</p></div><div id="comment-60512-info" class="comment-info"><span class="comment-age">(01 Apr '17, 21:05)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-60510" class="comment-tools"></div><div class="clear"></div><div id="comment-60510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60554"></span>

<div id="answer-container-60554" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60554-score" class="post-score" title="current number of votes">0</div><span id="post-60554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mrEEde has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Just pulled the 2.2.6 source and rebuilt wireshark on my own system ( vs. an RPM I was given ) -<br />
Problem disappeared.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Apr '17, 10:25</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span> </br></p></div></div><div id="comments-container-60554" class="comments-container"></div><div id="comment-tools-60554" class="comment-tools"></div><div class="clear"></div><div id="comment-60554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</hr>

</div>

</div>

