+++
type = "question"
title = "How can I capture traffic of devices which are connected to my Wifi Modem?"
description = '''I am a new user of Wireshark, any one please help me that how I capture the traffic of devices which are cnnected to my Wifi modem. These devices may be mobiles or PC/laptops. '''
date = "2013-06-04T11:15:00Z"
lastmod = "2013-06-04T18:32:00Z"
weight = 21742
keywords = [ "capture" ]
aliases = [ "/questions/21742" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can I capture traffic of devices which are connected to my Wifi Modem?](/questions/21742/how-can-i-capture-traffic-of-devices-which-are-connected-to-my-wifi-modem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21742-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21742-score" class="post-score" title="current number of votes">0</div><span id="post-21742-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a new user of Wireshark, any one please help me that how I capture the traffic of devices which are cnnected to my Wifi modem. These devices may be mobiles or PC/laptops.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '13, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/6ef622dc3ed60fa4eec68aadfe0e32ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MuhammadToseef&#39;s gravatar image" /><p><span>MuhammadToseef</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MuhammadToseef has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jun '13, 11:17</strong> </span></p></div></div><div id="comments-container-21742" class="comments-container"></div><div id="comment-tools-21742" class="comment-tools"></div><div class="clear"></div><div id="comment-21742-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21756"></span>

<div id="answer-container-21756" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21756-score" class="post-score" title="current number of votes">0</div><span id="post-21756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe this page will help you: <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '13, 18:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-21756" class="comments-container"></div><div id="comment-tools-21756" class="comment-tools"></div><div class="clear"></div><div id="comment-21756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

