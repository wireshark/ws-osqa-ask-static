+++
type = "question"
title = "How to capture USB trace of each drive separately by giving &quot;Port number&quot; rather than &quot;USBHUB number&quot;?"
description = '''I need to take trace of 2 USB drive individually not in same trace file. when i try to capture it is asking for interface number. Both drives are available under same USBHUB. Is there any way to capture trace for each drive separately? Please refer below image. '''
date = "2017-04-24T03:47:00Z"
lastmod = "2017-05-09T02:39:00Z"
weight = 61000
keywords = [ "usbpcapcmd", "tshark", "wireshark" ]
aliases = [ "/questions/61000" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture USB trace of each drive separately by giving "Port number" rather than "USBHUB number"?](/questions/61000/how-to-capture-usb-trace-of-each-drive-separately-by-giving-port-number-rather-than-usbhub-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61000-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61000-score" class="post-score" title="current number of votes">0</div><span id="post-61000-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to take trace of 2 USB drive individually not in same trace file. when i try to capture it is asking for interface number. Both drives are available under same USBHUB. Is there any way to capture trace for each drive separately? Please refer below image.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/2Drive_USBPCAP2_nYnsEBP.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usbpcapcmd" rel="tag" title="see questions tagged &#39;usbpcapcmd&#39;">usbpcapcmd</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '17, 03:47</strong></p><img src="https://secure.gravatar.com/avatar/d2c9789a43b411fb047ce641badacaf5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pramod&#39;s gravatar image" /><p><span>Pramod</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pramod has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61000" class="comments-container"><span id="61298"></span><div id="comment-61298" class="comment"><div id="post-61298-score" class="comment-score"></div><div class="comment-text"><p>I am new to Tshark command line usage. Please help me to solve this issue. I need to take trace of both drive separately.</p><p>Thanks, Pramod</p></div><div id="comment-61298-info" class="comment-info"><span class="comment-age">(09 May '17, 02:39)</span> <span class="comment-user userinfo">Pramod</span></div></div></div><div id="comment-tools-61000" class="comment-tools"></div><div class="clear"></div><div id="comment-61000-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

