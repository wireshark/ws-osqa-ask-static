+++
type = "question"
title = "Why is my pc exchanging packets with Facebook? i don&#x27;t have a Facebook account?"
description = '''I noticed that my pc is exchanging packets with Facebook. I don&#x27;t have a Facebook account, and I haven&#x27;t even visited their website. Why is this happening?'''
date = "2010-11-21T13:15:00Z"
lastmod = "2010-11-27T17:11:00Z"
weight = 1050
keywords = [ "rw" ]
aliases = [ "/questions/1050" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why is my pc exchanging packets with Facebook? i don't have a Facebook account?](/questions/1050/why-is-my-pc-exchanging-packets-with-facebook-i-dont-have-a-facebook-account)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1050-score" class="post-score" title="current number of votes">0</div><span id="post-1050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I noticed that my pc is exchanging packets with Facebook. I don't have a Facebook account, and I haven't even visited their website. Why is this happening?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rw" rel="tag" title="see questions tagged &#39;rw&#39;">rw</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '10, 13:15</strong></p><img src="https://secure.gravatar.com/avatar/b1175fbdcb65531df753f3f977a52bd1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RW15&#39;s gravatar image" /><p><span>RW15</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RW15 has no accepted answers">0%</span></p></div></div><div id="comments-container-1050" class="comments-container"><span id="1054"></span><div id="comment-1054" class="comment"><div id="post-1054-score" class="comment-score"></div><div class="comment-text"><p>Are you sure you don't have some toolbar or other add-in's that checks in with Facebook?</p></div><div id="comment-1054-info" class="comment-info"><span class="comment-age">(21 Nov '10, 21:25)</span> <span class="comment-user userinfo">hansangb</span></div></div><span id="1058"></span><div id="comment-1058" class="comment"><div id="post-1058-score" class="comment-score">1</div><div class="comment-text"><p>Were you surfing at the time of the capture? A disturbingly large number of web sites are now attempting to associate you with your facebook account for marketing reasons. I know that Lowes.com does it, and a boatload of others.</p></div><div id="comment-1058-info" class="comment-info"><span class="comment-age">(22 Nov '10, 05:55)</span> <span class="comment-user userinfo">GeonJay</span></div></div></div><div id="comment-tools-1050" class="comment-tools"></div><div class="clear"></div><div id="comment-1050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1066"></span>

<div id="answer-container-1066" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1066-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1066-score" class="post-score" title="current number of votes">0</div><span id="post-1066-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As has been mentioned in the comments above, there are more than a few applications/plugins/webpages that will talk to Facebook (or other sites) without your knowledge. There are utilities that will tell you which application is "behind" your TCP/IP connections:</p><p>1) If you're running Windows, the TCPView utility (<a href="http://technet.microsoft.com/en-us/sysinternals/bb897437.aspx">http://technet.microsoft.com/en-us/sysinternals/bb897437.aspx</a>) will show you all your TCP/IP connections AND identify the process/application that controls each connection.</p><p>2) If you're in the Linux world, netactview (<a href="http://netactview.sourceforge.net/">http://netactview.sourceforge.net/</a>) will provide the same information, including the "owning" process/application.</p><p>I expect that the connections will trace back to your web browser...but these tools will confirm it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Nov '10, 10:35</strong></p><img src="https://secure.gravatar.com/avatar/11ea89c2fd5a5830c69d0574a51b8142?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wesmorgan1&#39;s gravatar image" /><p><span>wesmorgan1</span><br />
<span class="score" title="411 reputation points">411</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wesmorgan1 has 2 accepted answers">4%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Nov '10, 10:35</strong> </span></p></div></div><div id="comments-container-1066" class="comments-container"><span id="1140"></span><div id="comment-1140" class="comment"><div id="post-1140-score" class="comment-score"></div><div class="comment-text"><p>Thank you!</p></div><div id="comment-1140-info" class="comment-info"><span class="comment-age">(27 Nov '10, 17:11)</span> <span class="comment-user userinfo">RW15</span></div></div></div><div id="comment-tools-1066" class="comment-tools"></div><div class="clear"></div><div id="comment-1066-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

