+++
type = "question"
title = "[closed] DNS Recursion Availability"
description = '''I am writing a &#x27;very&#x27; lightweight DNS client for a small embedded system. After reading over RFC 1035 I found out about the recursion desired bit and how some servers support internally following the dns trail and locating the appropriate IP address for the requested domain name and returning it as ...'''
date = "2015-05-06T10:11:00Z"
lastmod = "2015-05-06T11:10:00Z"
weight = 42146
keywords = [ "rfc", "server", "recursive", "dns", "1035" ]
aliases = [ "/questions/42146" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] DNS Recursion Availability](/questions/42146/dns-recursion-availability)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42146-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42146-score" class="post-score" title="current number of votes">0</div><span id="post-42146-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am writing a 'very' lightweight DNS client for a small embedded system. After reading over RFC 1035 I found out about the recursion desired bit and how some servers support internally following the dns trail and locating the appropriate IP address for the requested domain name and returning it as a single answer.</p><p>How many servers support this 'recursion desired' bit? Is it like 99% of them?</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rfc" rel="tag" title="see questions tagged &#39;rfc&#39;">rfc</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span> <span class="post-tag tag-link-recursive" rel="tag" title="see questions tagged &#39;recursive&#39;">recursive</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span> <span class="post-tag tag-link-1035" rel="tag" title="see questions tagged &#39;1035&#39;">1035</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '15, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/d10b4a16cda08123bdeafd8686342d41?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wes000000&#39;s gravatar image" /><p><span>wes000000</span><br />
<span class="score" title="21 reputation points">21</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wes000000 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>06 May '15, 11:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-42146" class="comments-container"><span id="42150"></span><div id="comment-42150" class="comment"><div id="post-42150-score" class="comment-score"></div><div class="comment-text"><p>Not a Wireshark question.</p></div><div id="comment-42150-info" class="comment-info"><span class="comment-age">(06 May '15, 11:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-42146" class="comment-tools"></div><div class="clear"></div><div id="comment-42146-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 06 May '15, 11:10

</div>

</div>

</div>

