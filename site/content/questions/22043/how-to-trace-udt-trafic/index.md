+++
type = "question"
title = "How to trace udt trafic ?"
description = '''Hi, I would like to trace udt trafic but i only see standard ip/udp trames... (Version 1.8.2) In the wiki i read:  The UDT dissector is fully functional. ... you may need to manually set the dissector to UDT.  Someone could help me to do this? Thanks, Bruno.'''
date = "2013-06-14T01:38:00Z"
lastmod = "2013-06-14T04:52:00Z"
weight = 22043
keywords = [ "dissector", "udt" ]
aliases = [ "/questions/22043" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to trace udt trafic ?](/questions/22043/how-to-trace-udt-trafic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22043-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22043-score" class="post-score" title="current number of votes">0</div><span id="post-22043-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to trace udt trafic but i only see standard ip/udp trames... (Version 1.8.2) In the wiki i read: The UDT dissector is fully functional. ... you may need to manually set the dissector to UDT.</p><p>Someone could help me to do this?</p><p>Thanks, Bruno.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-udt" rel="tag" title="see questions tagged &#39;udt&#39;">udt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 01:38</strong></p><img src="https://secure.gravatar.com/avatar/e175bb1f2b271f4716f252614fa6bfe3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brunost&#39;s gravatar image" /><p><span>brunost</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brunost has no accepted answers">0%</span></p></div></div><div id="comments-container-22043" class="comments-container"></div><div id="comment-tools-22043" class="comment-tools"></div><div class="clear"></div><div id="comment-22043-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22045"></span>

<div id="answer-container-22045" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22045-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22045-score" class="post-score" title="current number of votes">0</div><span id="post-22045-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Version <strong>1.8.2</strong></p></blockquote><p>You need a more recent version of Wireshark for UDT support, so please try Wireshark 1.10.0rc2.</p><p>See also: <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8741">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8741</a></p><p>If 1.10.0rc2 does not work, please try one of the daily builds:</p><blockquote><p><code>http://www.wireshark.org/download/automated/</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jun '13, 02:59</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Jun '13, 03:01</strong> </span></p></div></div><div id="comments-container-22045" class="comments-container"><span id="22055"></span><div id="comment-22055" class="comment"><div id="post-22055-score" class="comment-score"></div><div class="comment-text"><p>Ok thanks a lot for the quick answer! I am going to update ...</p></div><div id="comment-22055-info" class="comment-info"><span class="comment-age">(14 Jun '13, 04:52)</span> <span class="comment-user userinfo">brunost</span></div></div></div><div id="comment-tools-22045" class="comment-tools"></div><div class="clear"></div><div id="comment-22045-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

