+++
type = "question"
title = "Packet Tracer and Wireshark"
description = '''Hi can wireshark be used to capture traffic on cisco packet tracer? I am studying for ccna, i need to capture traffic to review it.'''
date = "2011-04-25T22:21:00Z"
lastmod = "2011-04-25T23:28:00Z"
weight = 3712
keywords = [ "tracer", "packet" ]
aliases = [ "/questions/3712" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet Tracer and Wireshark](/questions/3712/packet-tracer-and-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3712-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3712-score" class="post-score" title="current number of votes">0</div><span id="post-3712-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi can wireshark be used to capture traffic on cisco packet tracer? I am studying for ccna, i need to capture traffic to review it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tracer" rel="tag" title="see questions tagged &#39;tracer&#39;">tracer</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '11, 22:21</strong></p><img src="https://secure.gravatar.com/avatar/618c9f50352a5eb09c55b68f89258281?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dhanjal&#39;s gravatar image" /><p><span>Dhanjal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dhanjal has no accepted answers">0%</span></p></div></div><div id="comments-container-3712" class="comments-container"></div><div id="comment-tools-3712" class="comment-tools"></div><div class="clear"></div><div id="comment-3712-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3716"></span>

<div id="answer-container-3716" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3716-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3716-score" class="post-score" title="current number of votes">1</div><span id="post-3716-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That is more of a Cisco question, from the looks of the documents on <a href="http://www.cisco.com/web/learning/netacad/course_catalog/PacketTracer.html">Cisco</a>, the packet-tracer is a closed simulation environment. So I don't think you can attach a system with Wireshark on it (but I might be wrong).</p><p>What you can us is <a href="http://www.gns3.net/">GNS3</a>, I know you can capture traffic there and it is a great tool for emulating routers (I used it to pass the BGP exam in which I had used 13 emulated routers)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Apr '11, 23:28</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-3716" class="comments-container"></div><div id="comment-tools-3716" class="comment-tools"></div><div class="clear"></div><div id="comment-3716-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

