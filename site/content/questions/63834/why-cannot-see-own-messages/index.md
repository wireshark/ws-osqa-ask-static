+++
type = "question"
title = "Why cannot see &quot;own&quot; messages"
description = '''Hello together, I cannot see any message my computer sends, although there is no filter activated. My computer definitely sends some requests because I can see the answers from other ip addresses (for example on a ping). I have this problem on all interfaces, so it cannot be a filter on a certain ip...'''
date = "2017-10-12T00:47:00Z"
lastmod = "2017-10-16T05:57:00Z"
weight = 63834
keywords = [ "unseen" ]
aliases = [ "/questions/63834" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Why cannot see "own" messages](/questions/63834/why-cannot-see-own-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63834-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63834-score" class="post-score" title="current number of votes">0</div><span id="post-63834-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello together,</p><p>I cannot see any message my computer sends, although there is no filter activated. My computer definitely sends some requests because I can see the answers from other ip addresses (for example on a ping). I have this problem on all interfaces, so it cannot be a filter on a certain ip address.</p><p>Does somebody have some idea, what setting can be the cause for this funny behavior? Is there a "intern" filter for "own" ip addrees, which I cannot see directly, or something like this?</p><p>Thanks, Patricia</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unseen" rel="tag" title="see questions tagged &#39;unseen&#39;">unseen</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '17, 00:47</strong></p><img src="https://secure.gravatar.com/avatar/9aa4de0c731322f624047bdfc92e1c82?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Patricia&#39;s gravatar image" /><p><span>Patricia</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Patricia has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Oct '17, 00:48</strong> </span></p></div></div><div id="comments-container-63834" class="comments-container"></div><div id="comment-tools-63834" class="comment-tools"></div><div class="clear"></div><div id="comment-63834-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="63933"></span>

<div id="answer-container-63933" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63933-score" class="post-score" title="current number of votes">0</div><span id="post-63933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Patricia has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Update: Meanwhile I´ve uninstalled WinPcap, Wireshark and NPcap and than reinstalled Wireshark with WinPcap activated. Now everything works fine and I can see my own messages!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Oct '17, 05:57</strong></p><img src="https://secure.gravatar.com/avatar/9aa4de0c731322f624047bdfc92e1c82?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Patricia&#39;s gravatar image" /><p><span>Patricia</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Patricia has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Oct '17, 05:57</strong> </span></p></div></div><div id="comments-container-63933" class="comments-container"></div><div id="comment-tools-63933" class="comment-tools"></div><div class="clear"></div><div id="comment-63933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="63836"></span>

<div id="answer-container-63836" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63836-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63836-score" class="post-score" title="current number of votes">1</div><span id="post-63836-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess you're running on Windows, if so the issue is likely to be either an Endpoint AV Security or VPN application that interferes with WinPcap's ability to capture. Removing the interfering items (as in uninstall) usually fixes the issue.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '17, 01:23</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63836" class="comments-container"><span id="63839"></span><div id="comment-63839" class="comment"><div id="post-63839-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your answer, grahamb! Yes I´m running on Windows and there was actually installed a VPN client on my computer the last weeks. But I´m currently not running the VPN client and the problem also accours. Is there any possibility to remove the interfering items (by the way: do you now which items this are?), without uninstall the VPN? I need both, the VPN client to be installed (but not necessarily connected and the wireshark information about my packets.</p></div><div id="comment-63839-info" class="comment-info"><span class="comment-age">(12 Oct '17, 03:55)</span> <span class="comment-user userinfo">Patricia</span></div></div><span id="63840"></span><div id="comment-63840" class="comment"><div id="post-63840-score" class="comment-score">1</div><div class="comment-text"><p>Others who have had this issue have had to uninstall the VPN software.</p><p>An alternative is to try Npcap, the upcoming replacement for WinPcap.</p><p>Manually uninstall WinPcap, reboot, and then manually install Npcap. Wireshark can use either and it's possible that Npcap, being newer, might work with your VPN software.</p><p>Npcap can be downloaded from <a href="https://nmap.org/npcap/">here</a>.</p></div><div id="comment-63840-info" class="comment-info"><span class="comment-age">(12 Oct '17, 04:06)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="63920"></span><div id="comment-63920" class="comment"><div id="post-63920-score" class="comment-score"></div><div class="comment-text"><p>Oh, thanks for this good hint, but I cannot validate if this works because I get errors while installing Npcap. I will still try installing and give feedback if I was successful. Thank you very much for your help grahamb!</p></div><div id="comment-63920-info" class="comment-info"><span class="comment-age">(16 Oct '17, 00:02)</span> <span class="comment-user userinfo">Patricia</span></div></div><span id="63921"></span><div id="comment-63921" class="comment"><div id="post-63921-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I get errors while installing Npcap</p></blockquote><p>Then you should <a href="https://nmap.org/npcap/">report them to the Npcap developers</a> so they can try to fix them.</p></div><div id="comment-63921-info" class="comment-info"><span class="comment-age">(16 Oct '17, 01:14)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-63836" class="comment-tools"></div><div class="clear"></div><div id="comment-63836-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

