+++
type = "question"
title = "[Reassembly error, protocol TCP: New fragment overlaps old data (retransmission?)]"
description = '''help me! has windows server 2008 r2 which is connected to the cisco. when copying from the server to the workstation is slowly up. wireshark on the server says about such a problem:[Reassembly error, protocol TCP: New fragment overlaps old data (retransmission?)]'''
date = "2014-10-07T02:32:00Z"
lastmod = "2014-10-07T02:32:00Z"
weight = 36891
keywords = [ "windows2008" ]
aliases = [ "/questions/36891" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[Reassembly error, protocol TCP: New fragment overlaps old data (retransmission?)\]](/questions/36891/reassembly-error-protocol-tcp-new-fragment-overlaps-old-data-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36891-score" class="post-score" title="current number of votes">0</div><span id="post-36891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>help me! has windows server 2008 r2 which is connected to the cisco. when copying from the server to the workstation is slowly up. wireshark on the server says about such a problem:[Reassembly error, protocol TCP: New fragment overlaps old data (retransmission?)]</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows2008" rel="tag" title="see questions tagged &#39;windows2008&#39;">windows2008</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '14, 02:32</strong></p><img src="https://secure.gravatar.com/avatar/024ee53df187a9dc1bf8ee5556a02098?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AEI&#39;s gravatar image" /><p><span>AEI</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AEI has no accepted answers">0%</span></p></div></div><div id="comments-container-36891" class="comments-container"></div><div id="comment-tools-36891" class="comment-tools"></div><div class="clear"></div><div id="comment-36891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

