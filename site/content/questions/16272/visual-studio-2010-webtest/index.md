+++
type = "question"
title = "Visual studio 2010 - webtest"
description = '''Hi, in fiddler ther is an option to create webtest from the caputering request and responses. there is an option in wireshark to do also? if there is, pls explain how. thanks, Naomi'''
date = "2012-11-25T04:44:00Z"
lastmod = "2012-11-25T09:27:00Z"
weight = 16272
keywords = [ "fiddler", "visual-studio", "wireshark" ]
aliases = [ "/questions/16272" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Visual studio 2010 - webtest](/questions/16272/visual-studio-2010-webtest)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16272-score" class="post-score" title="current number of votes">0</div><span id="post-16272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, in fiddler ther is an option to create webtest from the caputering request and responses. there is an option in wireshark to do also? if there is, pls explain how.</p><p>thanks, Naomi</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fiddler" rel="tag" title="see questions tagged &#39;fiddler&#39;">fiddler</span> <span class="post-tag tag-link-visual-studio" rel="tag" title="see questions tagged &#39;visual-studio&#39;">visual-studio</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '12, 04:44</strong></p><img src="https://secure.gravatar.com/avatar/722aeb5b76c5523d9676ce84c3107164?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Naomia&#39;s gravatar image" /><p><span>Naomia</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Naomia has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Nov '12, 05:00</strong> </span></p></div></div><div id="comments-container-16272" class="comments-container"></div><div id="comment-tools-16272" class="comment-tools"></div><div class="clear"></div><div id="comment-16272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16278"></span>

<div id="answer-container-16278" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16278-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16278-score" class="post-score" title="current number of votes">0</div><span id="post-16278-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no comparable option in Wireshark. You can build similar things from the output of Wireshark, but you will have to do that manually or with a script/program.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '12, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-16278" class="comments-container"></div><div id="comment-tools-16278" class="comment-tools"></div><div class="clear"></div><div id="comment-16278-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

