+++
type = "question"
title = "WLAN Capture setup"
description = '''i&#x27;m after the same goal: identifying a single machine on the network that may be using a lot of bandwidth. i have Wireshark doing a Capture in Promiscuous mode, supposedly capturing all the traffic on the WLAN. but my phone, which has been streaming Pandora for hours, only shows as having transmitte...'''
date = "2013-04-09T17:26:00Z"
lastmod = "2013-04-10T08:59:00Z"
weight = 20257
keywords = [ "capture", "wlan" ]
aliases = [ "/questions/20257" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [WLAN Capture setup](/questions/20257/wlan-capture-setup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20257-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20257-score" class="post-score" title="current number of votes">0</div><span id="post-20257-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i'm after the same goal: identifying a single machine on the network that may be using a lot of bandwidth. i have Wireshark doing a Capture in Promiscuous mode, supposedly capturing all the traffic on the WLAN. but my phone, which has been streaming Pandora for hours, only shows as having transmitted 138 bytes. can anyone point me in the right direction to improve my reading? this is the first time i've used Wireshark or any packet sniffer. i'm not sure if my Capture is setup incorrectly or if i just don't know enough to read the output.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Apr '13, 17:26</strong></p><img src="https://secure.gravatar.com/avatar/5e91abab0d9dbcbde960755abfd335e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mctrout&#39;s gravatar image" /><p><span>mctrout</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mctrout has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>10 Apr '13, 01:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-20257" class="comments-container"></div><div id="comment-tools-20257" class="comment-tools"></div><div class="clear"></div><div id="comment-20257-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20266"></span>

<div id="answer-container-20266" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20266-score" class="post-score" title="current number of votes">0</div><span id="post-20266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you looked at the Wiki page for <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN Capture setup</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '13, 01:49</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-20266" class="comments-container"></div><div id="comment-tools-20266" class="comment-tools"></div><div class="clear"></div><div id="comment-20266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20280"></span>

<div id="answer-container-20280" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20280-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20280-score" class="post-score" title="current number of votes">0</div><span id="post-20280-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Why capture the WLAN traffic? Wouldn't it be easier to capture at the LAN port (ethernet) of the internet router via a TAP or a switch mirror port?</p><blockquote><p><code>http://wiki.wireshark.org/CaptureSetup/Ethernet</code><br />
</p></blockquote><p>At that position you will get the whole internet traffic and you should be able to easily identify the bandwidth eaters (Statistics -&gt; Endpoints -&gt; IP, or other Wireshark statistics modules).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '13, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-20280" class="comments-container"><span id="20289"></span><div id="comment-20289" class="comment"><div id="post-20289-score" class="comment-score">2</div><div class="comment-text"><p>If it's a home system, then it's likely the router is combined with the AP and doesn't provide any port mirroring. We'll have to wait for more info from the OP.</p></div><div id="comment-20289-info" class="comment-info"><span class="comment-age">(10 Apr '13, 08:46)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="20291"></span><div id="comment-20291" class="comment"><div id="post-20291-score" class="comment-score"></div><div class="comment-text"><blockquote><p>If it's a home system, then it's likely the router is combined with the AP and doesn't provide any port mirroring.</p></blockquote><p>Good point!</p></div><div id="comment-20291-info" class="comment-info"><span class="comment-age">(10 Apr '13, 08:59)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20280" class="comment-tools"></div><div class="clear"></div><div id="comment-20280-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

