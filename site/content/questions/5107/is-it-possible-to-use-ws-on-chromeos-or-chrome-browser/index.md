+++
type = "question"
title = "Is it possible to use WS on chromeos or chrome browser?"
description = '''Is there any version that will run under chromeos? Any that will run on chrome browser? If not available yet, is there any fundamental limitation of chromeos that would prevent a future version from being available? Can this be added on as an App or Extension?'''
date = "2011-07-18T09:16:00Z"
lastmod = "2011-07-18T12:08:00Z"
weight = 5107
keywords = [ "chrome-browser", "chromeos", "chrome-extension", "chrome-app" ]
aliases = [ "/questions/5107" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to use WS on chromeos or chrome browser?](/questions/5107/is-it-possible-to-use-ws-on-chromeos-or-chrome-browser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5107-score" class="post-score" title="current number of votes">0</div><span id="post-5107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any version that will run under chromeos? Any that will run on chrome browser?</p><p>If not available yet, is there any fundamental limitation of chromeos that would prevent a future version from being available? Can this be added on as an App or Extension?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-chrome-browser" rel="tag" title="see questions tagged &#39;chrome-browser&#39;">chrome-browser</span> <span class="post-tag tag-link-chromeos" rel="tag" title="see questions tagged &#39;chromeos&#39;">chromeos</span> <span class="post-tag tag-link-chrome-extension" rel="tag" title="see questions tagged &#39;chrome-extension&#39;">chrome-extension</span> <span class="post-tag tag-link-chrome-app" rel="tag" title="see questions tagged &#39;chrome-app&#39;">chrome-app</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '11, 09:16</strong></p><img src="https://secure.gravatar.com/avatar/c1fb1ea67b48bf3d9e60250dcb7c756f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CqN&#39;s gravatar image" /><p><span>CqN</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CqN has no accepted answers">0%</span></p></div></div><div id="comments-container-5107" class="comments-container"></div><div id="comment-tools-5107" class="comment-tools"></div><div class="clear"></div><div id="comment-5107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5109"></span>

<div id="answer-container-5109" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5109-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5109-score" class="post-score" title="current number of votes">0</div><span id="post-5109-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If Chrome OS can run arbitrary Linux applications, then Wireshark will run on Chrome OS; otherwise, it won't.</p><p>If the Chrome browser can run arbitrary Linux applications, then Wireshark can run on the Chrome browser; otherwise, it won't.</p><p>Wireshark was not written for Chrome OS, it was written for UN*X and GTK+, and was subsequently modified to support Windows (still using GTK+) as well. Unless Chrome (either the OS or the browser) supports running apps written for that environment, it would probably take a significant amount of work to create a version of Wireshark that will run on Chrome OS or in the Chrome browser.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '11, 12:08</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-5109" class="comments-container"></div><div id="comment-tools-5109" class="comment-tools"></div><div class="clear"></div><div id="comment-5109-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

