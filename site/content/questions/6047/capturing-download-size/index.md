+++
type = "question"
title = "Capturing download size"
description = '''I am trying to find out the size of antivirus definitions that are being downloaded to a corporate server. I suspect the local cache size of the definitions are as unpacked and therefore inaccurate - can anyone recommend a way of finding out the total size of inbound network traffic over a specific ...'''
date = "2011-09-01T08:24:00Z"
lastmod = "2011-09-01T08:24:00Z"
weight = 6047
keywords = [ "download", "traffic", "size" ]
aliases = [ "/questions/6047" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing download size](/questions/6047/capturing-download-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6047-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6047-score" class="post-score" title="current number of votes">0</div><span id="post-6047-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to find out the size of antivirus definitions that are being downloaded to a corporate server. I suspect the local cache size of the definitions are as unpacked and therefore inaccurate - can anyone recommend a way of finding out the total size of inbound network traffic over a specific amount of time?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '11, 08:24</strong></p><img src="https://secure.gravatar.com/avatar/fd46f10d78f1ecfef49d86b0d5550e97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="genjuu&#39;s gravatar image" /><p><span>genjuu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="genjuu has no accepted answers">0%</span></p></div></div><div id="comments-container-6047" class="comments-container"></div><div id="comment-tools-6047" class="comment-tools"></div><div class="clear"></div><div id="comment-6047-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

