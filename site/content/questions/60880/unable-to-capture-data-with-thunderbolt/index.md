+++
type = "question"
title = "unable to capture data with thunderbolt"
description = '''I am new to Wireshark and trying to test if I am getting a signal from some detector system. I have tried using a thunderbolt connector and applied a filter a display filter. I have repeatedly tried to use thunderbolt en1 and en2 but cannot get any data captured. I am using the latest version of Wir...'''
date = "2017-04-18T20:33:00Z"
lastmod = "2017-04-18T20:33:00Z"
weight = 60880
keywords = [ "mac", "thunderbolt" ]
aliases = [ "/questions/60880" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [unable to capture data with thunderbolt](/questions/60880/unable-to-capture-data-with-thunderbolt)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60880-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60880-score" class="post-score" title="current number of votes">0</div><span id="post-60880-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to Wireshark and trying to test if I am getting a signal from some detector system. I have tried using a thunderbolt connector and applied a filter a display filter. I have repeatedly tried to use thunderbolt en1 and en2 but cannot get any data captured. I am using the latest version of Wireshark. I appreciate any help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-thunderbolt" rel="tag" title="see questions tagged &#39;thunderbolt&#39;">thunderbolt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '17, 20:33</strong></p><img src="https://secure.gravatar.com/avatar/366833ab06bb29985833a12e14208e5e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ndarwish&#39;s gravatar image" /><p><span>ndarwish</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ndarwish has no accepted answers">0%</span></p></div></div><div id="comments-container-60880" class="comments-container"></div><div id="comment-tools-60880" class="comment-tools"></div><div class="clear"></div><div id="comment-60880-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

