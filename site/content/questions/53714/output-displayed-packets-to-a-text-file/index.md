+++
type = "question"
title = "Output displayed packets to a text file"
description = '''Hello,  I have recently updated my wireshark to v2.0.4 and I cannot find a way to export displayed packets to a text file.  In previous version, I would go to print, selected summary packet format information and then output to a file. But now if go to print there is a option to print to a file but ...'''
date = "2016-06-29T02:20:00Z"
lastmod = "2016-06-29T06:37:00Z"
weight = 53714
keywords = [ "print", "wireshark" ]
aliases = [ "/questions/53714" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Output displayed packets to a text file](/questions/53714/output-displayed-packets-to-a-text-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53714-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53714-score" class="post-score" title="current number of votes">0</div><span id="post-53714-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have recently updated my wireshark to v2.0.4 and I cannot find a way to export displayed packets to a text file.</p><p>In previous version, I would go to print, selected summary packet format information and then output to a file. But now if go to print there is a option to print to a file but there is not way to choose how much information to display.</p><p>Is there any other way?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-print" rel="tag" title="see questions tagged &#39;print&#39;">print</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '16, 02:20</strong></p><img src="https://secure.gravatar.com/avatar/e5a6ee774dab1a8c2807da97bb1107e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oliveb&#39;s gravatar image" /><p><span>oliveb</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oliveb has no accepted answers">0%</span></p></div></div><div id="comments-container-53714" class="comments-container"></div><div id="comment-tools-53714" class="comment-tools"></div><div class="clear"></div><div id="comment-53714-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53715"></span>

<div id="answer-container-53715" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53715-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53715-score" class="post-score" title="current number of votes">2</div><span id="post-53715-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="oliveb has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This has been changed in 2.x. Use File -&gt; Export Packet Dissections -&gt; As Plain text ...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jun '16, 02:59</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-53715" class="comments-container"><span id="53725"></span><div id="comment-53725" class="comment"><div id="post-53725-score" class="comment-score"></div><div class="comment-text"><p>Thank you.</p></div><div id="comment-53725-info" class="comment-info"><span class="comment-age">(29 Jun '16, 06:37)</span> <span class="comment-user userinfo">oliveb</span></div></div></div><div id="comment-tools-53715" class="comment-tools"></div><div class="clear"></div><div id="comment-53715-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

