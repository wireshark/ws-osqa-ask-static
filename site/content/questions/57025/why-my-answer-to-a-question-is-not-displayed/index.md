+++
type = "question"
title = "Why my answer to a question is not displayed?"
description = '''I&#x27;ve answered this question, but the answer was not displayed. I&#x27;ve suspected I&#x27;ve done something wrong so I&#x27;ve posted it one more time, so now there are two almost identical answers but none of them is displayed, although in the question list, presence of two answers is indicated and the last modif...'''
date = "2016-11-06T01:29:00Z"
lastmod = "2016-11-06T04:07:00Z"
weight = 57025
keywords = [ "ask.wireshark.org" ]
aliases = [ "/questions/57025" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why my answer to a question is not displayed?](/questions/57025/why-my-answer-to-a-question-is-not-displayed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57025-score" class="post-score" title="current number of votes">0</div><span id="post-57025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've answered <a href="https://ask.wireshark.org/questions/57020/port-mirroring-and-icmp-type-5-redirects">this question</a>, but the answer was not displayed. I've suspected I've done something wrong so I've posted it one more time, so now there are two almost identical answers but none of them is displayed, although in the question list, presence of two answers is indicated and the last modification of the question also shows it was me who has modified it a couple of seconds ago.</p><p>I've checked the text of the answer and nothing seems dangerous in it, there are a few quotations from the question, a single link to another question, and the rest is plain text.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '16, 01:29</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-57025" class="comments-container"></div><div id="comment-tools-57025" class="comment-tools"></div><div class="clear"></div><div id="comment-57025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57027"></span>

<div id="answer-container-57027" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57027-score" class="post-score" title="current number of votes">0</div><span id="post-57027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hmmm. Right now (2 hours after you asked the question) I see exactly one answer.</p><p>I have experienced similar behavior which was caused by the NoScript extension for the Firefox browser. More precisely: I have to mark google.com as trusted or allow scripts from google.com, before I can post an answer, vote on questions etc.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Nov '16, 03:55</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-57027" class="comments-container"><span id="57028"></span><div id="comment-57028" class="comment"><div id="post-57028-score" class="comment-score"></div><div class="comment-text"><p>The reason you can see just one of them is that in the meantime between my asking of this question and your answer, both instances of my answer to that question have appeared, so I've deleted one of them. I'll see at the next opportunity whether it was a temporary data synchronisation issue at the hosting or some joke of the last update of Firefox - I have never experienced such behaviour before, only a complete site unreachability, causing the AlwaysOnline to kick in.</p></div><div id="comment-57028-info" class="comment-info"><span class="comment-age">(06 Nov '16, 04:07)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-57027" class="comment-tools"></div><div class="clear"></div><div id="comment-57027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

