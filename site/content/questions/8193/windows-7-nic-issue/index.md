+++
type = "question"
title = "Windows 7 nic issue"
description = '''Has anyone figured out how to capture packets on a second nic in a Win 7 machine with the network location &quot;feature&quot;. Everytime I try to capture packets with a Win 7 machine it never works, it always shows &quot;limited connectivity&quot; on the sniffing nic.'''
date = "2012-01-02T10:20:00Z"
lastmod = "2012-01-02T10:20:00Z"
weight = 8193
keywords = [ "windows7" ]
aliases = [ "/questions/8193" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 nic issue](/questions/8193/windows-7-nic-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8193-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8193-score" class="post-score" title="current number of votes">0</div><span id="post-8193-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anyone figured out how to capture packets on a second nic in a Win 7 machine with the network location "feature". Everytime I try to capture packets with a Win 7 machine it never works, it always shows "limited connectivity" on the sniffing nic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jan '12, 10:20</strong></p><img src="https://secure.gravatar.com/avatar/070a45191f5e473946252b2351f57a52?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Twidget&#39;s gravatar image" /><p><span>Twidget</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Twidget has no accepted answers">0%</span></p></div></div><div id="comments-container-8193" class="comments-container"></div><div id="comment-tools-8193" class="comment-tools"></div><div class="clear"></div><div id="comment-8193-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

