+++
type = "question"
title = "Wireshark University Certificate"
description = '''How useful is that cert? What are the other qualifications in the same category?  Thanks Regards, Eddie Choo'''
date = "2011-07-21T19:54:00Z"
lastmod = "2011-07-23T17:40:00Z"
weight = 5162
keywords = [ "wireshark" ]
aliases = [ "/questions/5162" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark University Certificate](/questions/5162/wireshark-university-certificate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5162-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5162-score" class="post-score" title="current number of votes">0</div><span id="post-5162-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>How useful is that cert? What are the other qualifications in the same category?</p><p>Thanks</p><p>Regards,</p><p>Eddie Choo</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '11, 19:54</strong></p><img src="https://secure.gravatar.com/avatar/c1dac05d0e75992546b5da006c6b718e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eddie%20choo&#39;s gravatar image" /><p><span>eddie choo</span><br />
<span class="score" title="66 reputation points">66</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eddie choo has 2 accepted answers">66%</span></p></div></div><div id="comments-container-5162" class="comments-container"></div><div id="comment-tools-5162" class="comment-tools"></div><div class="clear"></div><div id="comment-5162-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5168"></span>

<div id="answer-container-5168" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5168-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5168-score" class="post-score" title="current number of votes">1</div><span id="post-5168-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="eddie choo has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think this question and it's answers might be what you're looking for:</p><p><a href="http://ask.wireshark.org/questions/818/wireshark-certification-any-value">http://ask.wireshark.org/questions/818/wireshark-certification-any-value</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jul '11, 00:26</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-5168" class="comments-container"><span id="5169"></span><div id="comment-5169" class="comment"><div id="post-5169-score" class="comment-score"></div><div class="comment-text"><p>Wow, nice link. should i delete this question as another similar one is asked previously? Thanks for your reply</p></div><div id="comment-5169-info" class="comment-info"><span class="comment-age">(22 Jul '11, 00:41)</span> <span class="comment-user userinfo">eddie choo</span></div></div><span id="5170"></span><div id="comment-5170" class="comment"><div id="post-5170-score" class="comment-score">1</div><div class="comment-text"><p>We can keep it here, maybe others will be interested in this, too, and this way it's at the top for a while. You might want to accept the answer though.</p></div><div id="comment-5170-info" class="comment-info"><span class="comment-age">(22 Jul '11, 00:45)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="5171"></span><div id="comment-5171" class="comment"><div id="post-5171-score" class="comment-score"></div><div class="comment-text"><p>Accepted. Not really friendly with this forum environment. Might get used to it soon i hope</p></div><div id="comment-5171-info" class="comment-info"><span class="comment-age">(22 Jul '11, 01:02)</span> <span class="comment-user userinfo">eddie choo</span></div></div><span id="5185"></span><div id="comment-5185" class="comment"><div id="post-5185-score" class="comment-score">2</div><div class="comment-text"><p><a href="http://ask.wireshark.org/faq/">It's not really a forum</a>.</p></div><div id="comment-5185-info" class="comment-info"><span class="comment-age">(23 Jul '11, 17:40)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-5168" class="comment-tools"></div><div class="clear"></div><div id="comment-5168-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

