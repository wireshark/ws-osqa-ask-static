+++
type = "question"
title = "What are packets"
description = '''Hi, I know that I&#x27;m a beginner to all of this stuff, but I can receive packets, but have no idea at all what they mean. Can anyone help me? Thanks'''
date = "2016-04-27T08:28:00Z"
lastmod = "2016-04-28T00:59:00Z"
weight = 52018
keywords = [ "beginner" ]
aliases = [ "/questions/52018" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What are packets](/questions/52018/what-are-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52018-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52018-score" class="post-score" title="current number of votes">0</div><span id="post-52018-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I know that I'm a beginner to all of this stuff, but I can receive packets, but have no idea at all what they mean. Can anyone help me? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '16, 08:28</strong></p><img src="https://secure.gravatar.com/avatar/788eae0b886467e6f8ee0e06daa3431a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Destrucor1123&#39;s gravatar image" /><p><span>Destrucor1123</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Destrucor1123 has no accepted answers">0%</span></p></div></div><div id="comments-container-52018" class="comments-container"></div><div id="comment-tools-52018" class="comment-tools"></div><div class="clear"></div><div id="comment-52018-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52033"></span>

<div id="answer-container-52033" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52033-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52033-score" class="post-score" title="current number of votes">1</div><span id="post-52033-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I find the <a href="http://www.tcpipguide.com/free/index.htm">TCP/IP guide</a> to be helpful in these situations. Read and learn, young padawan.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '16, 00:59</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52033" class="comments-container"></div><div id="comment-tools-52033" class="comment-tools"></div><div class="clear"></div><div id="comment-52033-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

