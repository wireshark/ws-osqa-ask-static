+++
type = "question"
title = "wireshark not run"
description = '''could&#x27;t run/user/bin/dumpcap/child in process : permission denied how can solve this type of problem when i launched the wireshark'''
date = "2017-07-10T03:44:00Z"
lastmod = "2017-07-10T04:15:00Z"
weight = 62636
keywords = [ "problem", "launch", "wireshark" ]
aliases = [ "/questions/62636" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark not run](/questions/62636/wireshark-not-run)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62636-score" class="post-score" title="current number of votes">0</div><span id="post-62636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>could't run/user/bin/dumpcap/child in process : permission denied</p><p>how can solve this type of problem when i launched the wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span> <span class="post-tag tag-link-launch" rel="tag" title="see questions tagged &#39;launch&#39;">launch</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '17, 03:44</strong></p><img src="https://secure.gravatar.com/avatar/b01794647c28949337751be9ba215802?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ashikraj&#39;s gravatar image" /><p><span>ashikraj</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ashikraj has no accepted answers">0%</span></p></div></div><div id="comments-container-62636" class="comments-container"><span id="62641"></span><div id="comment-62641" class="comment"><div id="post-62641-score" class="comment-score"></div><div class="comment-text"><p>what operating system do you use?</p></div><div id="comment-62641-info" class="comment-info"><span class="comment-age">(10 Jul '17, 04:15)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-62636" class="comment-tools"></div><div class="clear"></div><div id="comment-62636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

