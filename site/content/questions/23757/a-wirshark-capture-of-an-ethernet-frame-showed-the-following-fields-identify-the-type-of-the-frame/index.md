+++
type = "question"
title = "A Wirshark capture of an Ethernet frame showed the following fields. Identify the type of the frame."
description = '''A Wirshark capture of an Ethernet frame showed the following fields. Identify the type of the frame. Hardware type: Ethernet (0x0001) Protocol type: IP (0x0800) Hardware size: 6 Protocol size: 4 Opcode: request (0x0001) Can anyone tell me what type of frame we are looking at ? Thanks in advance'''
date = "2013-08-13T20:06:00Z"
lastmod = "2013-08-13T21:28:00Z"
weight = 23757
keywords = [ "hardware", "ethernet", "protocol", "network", "ip" ]
aliases = [ "/questions/23757" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [A Wirshark capture of an Ethernet frame showed the following fields. Identify the type of the frame.](/questions/23757/a-wirshark-capture-of-an-ethernet-frame-showed-the-following-fields-identify-the-type-of-the-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23757-score" class="post-score" title="current number of votes">0</div><span id="post-23757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A Wirshark capture of an Ethernet frame showed the following fields. Identify the type of the frame.</p><p>Hardware type: Ethernet (0x0001) Protocol type: IP (0x0800) Hardware size: 6 Protocol size: 4 Opcode: request (0x0001)</p><p>Can anyone tell me what type of frame we are looking at ?</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hardware" rel="tag" title="see questions tagged &#39;hardware&#39;">hardware</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '13, 20:06</strong></p><img src="https://secure.gravatar.com/avatar/1e50f75751b7f89a17db077a6fd3c46e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joseph%20Sert&#39;s gravatar image" /><p><span>Joseph Sert</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joseph Sert has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Aug '13, 20:06</strong> </span></p></div></div><div id="comments-container-23757" class="comments-container"><span id="23759"></span><div id="comment-23759" class="comment"><div id="post-23759-score" class="comment-score"></div><div class="comment-text"><p>"Identify the type of the frame." - is this a question on an exam? If so, you're supposed to answer it, not us.</p></div><div id="comment-23759-info" class="comment-info"><span class="comment-age">(13 Aug '13, 20:24)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="23760"></span><div id="comment-23760" class="comment"><div id="post-23760-score" class="comment-score"></div><div class="comment-text"><p>this is an assessment question. I have checked the lecture slides but could not find anything. I know i am supposed to answer it but could not find the answer. If you can help me out and explain at least i ll have an idea</p></div><div id="comment-23760-info" class="comment-info"><span class="comment-age">(13 Aug '13, 20:54)</span> <span class="comment-user userinfo">Joseph Sert</span></div></div></div><div id="comment-tools-23757" class="comment-tools"></div><div class="clear"></div><div id="comment-23757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23761"></span>

<div id="answer-container-23761" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23761-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23761-score" class="post-score" title="current number of votes">0</div><span id="post-23761-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>this is an assessment question</p></blockquote><p>Presumably they're assessing you, not somebody else, so you're going to have to be the one who answers the question, not somebody else.</p><p>Perhaps they showed you a bunch of packets of various sorts, and discussed the protocols in those packets, and expect you to remember what some protocols look like, and recognize that packet as looking like one of the protocols you've seen?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Aug '13, 21:28</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-23761" class="comments-container"></div><div id="comment-tools-23761" class="comment-tools"></div><div class="clear"></div><div id="comment-23761-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

