+++
type = "question"
title = "WhatsApp is now encrypted?"
description = '''Hello, just noticed that since the release of WhatsApp version 2.8.2-5222 (iPhone) I&#x27;m no longer able to read my sniffed messages. Probably they added some kind of encryption or at least compression to the raw data. Does anyone have further informations?'''
date = "2012-08-02T07:53:00Z"
lastmod = "2013-06-03T09:17:00Z"
weight = 13317
keywords = [ "encryption", "whatsapp", "compression", "wireshark" ]
aliases = [ "/questions/13317" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [WhatsApp is now encrypted?](/questions/13317/whatsapp-is-now-encrypted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13317-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13317-score" class="post-score" title="current number of votes">0</div><span id="post-13317-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>just noticed that since the release of WhatsApp version 2.8.2-5222 (iPhone) I'm no longer able to read my sniffed messages. Probably they added some kind of encryption or at least compression to the raw data. Does anyone have further informations?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span> <span class="post-tag tag-link-compression" rel="tag" title="see questions tagged &#39;compression&#39;">compression</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Aug '12, 07:53</strong></p><img src="https://secure.gravatar.com/avatar/94c203de8a22aefe90f3b96e5590ddbe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tvfreak666&#39;s gravatar image" /><p><span>tvfreak666</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tvfreak666 has no accepted answers">0%</span></p></div></div><div id="comments-container-13317" class="comments-container"><span id="13325"></span><div id="comment-13325" class="comment"><div id="post-13325-score" class="comment-score"></div><div class="comment-text"><p>if you post a capture file, we can check.</p></div><div id="comment-13325-info" class="comment-info"><span class="comment-age">(02 Aug '12, 12:02)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-13317" class="comment-tools"></div><div class="clear"></div><div id="comment-13317-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13699"></span>

<div id="answer-container-13699" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13699-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13699-score" class="post-score" title="current number of votes">2</div><span id="post-13699-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, see: <a href="https://whatsapp.zendesk.com/entries/21864047-are-my-messages-secure">https://whatsapp.zendesk.com/entries/21864047-are-my-messages-secure</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Aug '12, 04:06</strong></p><img src="https://secure.gravatar.com/avatar/673278b2ed6386c6b012198a045aa7a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aVirulence&#39;s gravatar image" /><p><span>aVirulence</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aVirulence has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Aug '12, 04:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-13699" class="comments-container"></div><div id="comment-tools-13699" class="comment-tools"></div><div class="clear"></div><div id="comment-13699-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21716"></span>

<div id="answer-container-21716" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21716-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21716-score" class="post-score" title="current number of votes">0</div><span id="post-21716-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If whatsapp network traffic is now encrypted, no worries.</p><p>If you have physical access to the device, just take the SD Card out and look in /whatsapp directory.</p><p>Specifically the /whatsapp/Databases directory there are 6 msgstore-YYYY-MM-DD.1.db.crypt backups and the current msgstore.db.crypt.</p><p>Best route to recover the messages is to (As the help application help file says) - Extract the entire /whatsapp directory into a separate device. - Install what's app on the other device then import them.</p><p>Android doesn't seem to act differently when you take the SD card out and the contents aren't encrypted. (Means you can spend quality time with its contents)</p><p>WhatsApp keeps 6 days of backup logs plus the current msg database called msgstore.db.crypt and doesn't appear to delete media like videos, images or sounds.</p><p>It does over write the older backups so don't wait around as critical data can get overwritten. Also, current chats aren't deleted or overwritten and that can be months old. Not sure if it ever deletes old messages though.</p><p>Actually, the application itself has a terrific explanation of how its logs are handled. Describes this whole process in good detail.</p><p>I wouldn't have to know this if my wife wasn't having suspicious conversations necessitating investigation</p><blockquote><p>:[ !!</p></blockquote></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '13, 09:17</strong></p><img src="https://secure.gravatar.com/avatar/67e2e0e24f807c01fdd1f6d0382fb593?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zzSQL&#39;s gravatar image" /><p><span>zzSQL</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zzSQL has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jun '13, 10:06</strong> </span></p></div></div><div id="comments-container-21716" class="comments-container"></div><div id="comment-tools-21716" class="comment-tools"></div><div class="clear"></div><div id="comment-21716-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

