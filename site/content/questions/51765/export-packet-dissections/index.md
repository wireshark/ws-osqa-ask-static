+++
type = "question"
title = "Export Packet Dissections"
description = '''Does anyone know how to get a plain text file for an Ethernet connection that looks like this. +---------+---------------+----------+ 15:28:59,717,083 ETHER |00|00|06|ec|04|10|ac|f0|29|29|76|6f|46|08|00|45|00|00|52|00|00|40|00 |3f|11|a7|39|0a|6a|00|0a|0a|6a|7f|84|2e|28|32|cb|00|3e|14|27|1c|78|00 |00...'''
date = "2016-04-18T12:20:00Z"
lastmod = "2016-04-19T06:19:00Z"
weight = 51765
keywords = [ "jal1130" ]
aliases = [ "/questions/51765" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Export Packet Dissections](/questions/51765/export-packet-dissections)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51765-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51765-score" class="post-score" title="current number of votes">0</div><span id="post-51765-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know how to get a plain text file for an Ethernet connection that looks like this.</p><pre><code>+---------+---------------+----------+
15:28:59,717,083   ETHER
|00|00|06|ec|04|10|ac|f0|29|29|76|6f|46|08|00|45|00|00|52|00|00|40|00
|3f|11|a7|39|0a|6a|00|0a|0a|6a|7f|84|2e|28|32|cb|00|3e|14|27|1c|78|00
|00|07|d0|00|01|00|17|00|00|0f|80|00|00|00|00|00|00|00|00|80|6a|00|01
|00|01|09|24|9a|c8|00|10|05|00|01|01|0f|0a|01|00|04|09|52|79|61|6e|5f
|43|6f|6e|00|ff|</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-jal1130" rel="tag" title="see questions tagged &#39;jal1130&#39;">jal1130</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '16, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/257f9776cb5449cce0f4adc5ab6f9dec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JAL1130&#39;s gravatar image" /><p><span>JAL1130</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JAL1130 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Apr '16, 12:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-51765" class="comments-container"></div><div id="comment-tools-51765" class="comment-tools"></div><div class="clear"></div><div id="comment-51765-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51782"></span>

<div id="answer-container-51782" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51782-score" class="post-score" title="current number of votes">0</div><span id="post-51782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Does anyone know <strong>how to get</strong> a plain text file for an Ethernet connection that looks like this.</p></blockquote><p>Looks like the output of a custom application or a script.</p><p>If you are tempted to ask:</p><ul><li>which application<br />
</li><li>what kind of script</li></ul><p>I can't tell you.</p><p>Why do you want to know how to generate such an output? Do you know which application/tool generated it or do you want to generate it yourself? Where did you get the output from?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '16, 06:19</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-51782" class="comments-container"></div><div id="comment-tools-51782" class="comment-tools"></div><div class="clear"></div><div id="comment-51782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

