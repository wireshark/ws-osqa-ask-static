+++
type = "question"
title = "tshark VoIP call listing"
description = '''Dears, On wireshark you can display the voIP calls with the start time the stop time the number the ip addresses. How can we manage to do that ???? I use Tshak to filer sip I just got phone numbers and ip addresses. How can I get the start time an the stop time. Thank you for your help Best Regards ...'''
date = "2012-06-30T02:04:00Z"
lastmod = "2012-06-30T02:06:00Z"
weight = 12344
keywords = [ "duration", "start", "tshark", "voip", "time" ]
aliases = [ "/questions/12344" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tshark VoIP call listing](/questions/12344/tshark-voip-call-listing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12344-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12344-score" class="post-score" title="current number of votes">0</div><span id="post-12344-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dears,</p><p>On wireshark you can display the voIP calls with the start time the stop time the number the ip addresses. How can we manage to do that ???? I use Tshak to filer sip I just got phone numbers and ip addresses. How can I get the start time an the stop time. Thank you for your help</p><p>Best Regards</p><p>Mouhamet</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-duration" rel="tag" title="see questions tagged &#39;duration&#39;">duration</span> <span class="post-tag tag-link-start" rel="tag" title="see questions tagged &#39;start&#39;">start</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '12, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/d0c6df122cea144f8159d12369ce389c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dmouha&#39;s gravatar image" /><p><span>Dmouha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dmouha has no accepted answers">0%</span></p></div></div><div id="comments-container-12344" class="comments-container"><span id="12345"></span><div id="comment-12345" class="comment"><div id="post-12345-score" class="comment-score"></div><div class="comment-text"><p>DI mean how can I do that using tshark</p></div><div id="comment-12345-info" class="comment-info"><span class="comment-age">(30 Jun '12, 02:06)</span> <span class="comment-user userinfo">Dmouha</span></div></div></div><div id="comment-tools-12344" class="comment-tools"></div><div class="clear"></div><div id="comment-12344-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

