+++
type = "question"
title = "Email notification for a question"
description = '''Hello, I currently don&#x27;t receive a notification email when someone responds to a question I asked. I also haven&#x27;t found a way to configure this through my user profile. Is this an option? If not, will it be in the future? It seems counter-intuitive to have to login continually to see if anyone has r...'''
date = "2014-08-01T16:26:00Z"
lastmod = "2014-08-04T09:40:00Z"
weight = 35080
keywords = [ "ask.wireshark.org" ]
aliases = [ "/questions/35080" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Email notification for a question](/questions/35080/email-notification-for-a-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35080-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35080-score" class="post-score" title="current number of votes">0</div><span id="post-35080-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I currently don't receive a notification email when someone responds to a question I asked. I also haven't found a way to configure this through my user profile. Is this an option? If not, will it be in the future? It seems counter-intuitive to have to login continually to see if anyone has replied.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '14, 16:26</strong></p><img src="https://secure.gravatar.com/avatar/4a4df10c701372e5dbbb8015a1d6b67b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="patrick_harrold&#39;s gravatar image" /><p><span>patrick_harrold</span><br />
<span class="score" title="36 reputation points">36</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="patrick_harrold has no accepted answers">0%</span></p></div></div><div id="comments-container-35080" class="comments-container"></div><div id="comment-tools-35080" class="comment-tools"></div><div class="clear"></div><div id="comment-35080-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35083"></span>

<div id="answer-container-35083" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35083-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35083-score" class="post-score" title="current number of votes">2</div><span id="post-35083-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="patrick_harrold has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, this is an options, and as far as I know, it's the default. The settings are found under "User Tools &gt; email notification settings."</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '14, 20:54</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-35083" class="comments-container"><span id="35085"></span><div id="comment-35085" class="comment"><div id="post-35085-score" class="comment-score"></div><div class="comment-text"><p>this has always worked fine for me (and still does), so if it doesn't there is probably some sort of problem with the specific user/email.</p></div><div id="comment-35085-info" class="comment-info"><span class="comment-age">(02 Aug '14, 02:32)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="35164"></span><div id="comment-35164" class="comment"><div id="post-35164-score" class="comment-score"></div><div class="comment-text"><p>Silly me. I never validated my email. Thanks.</p></div><div id="comment-35164-info" class="comment-info"><span class="comment-age">(04 Aug '14, 09:40)</span> <span class="comment-user userinfo">patrick_harrold</span></div></div></div><div id="comment-tools-35083" class="comment-tools"></div><div class="clear"></div><div id="comment-35083-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

