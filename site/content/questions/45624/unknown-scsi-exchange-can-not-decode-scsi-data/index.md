+++
type = "question"
title = "Unknown SCSI exchange can not decode SCSI data"
description = '''I&#x27;m having iSCSI performance problems. Packet capture :  https://www.cloudshark.org/captures/0a58c3b43da8 I see a number of packets with expert info Warning Unknown SCSI exchange, can not decode SCSI data. See packet 104. The only thing I can find on this is in the wireshark code  we have no record ...'''
date = "2015-09-03T12:45:00Z"
lastmod = "2015-09-03T12:45:00Z"
weight = 45624
keywords = [ "info", "unknown", "scsi", "expert", "exchange" ]
aliases = [ "/questions/45624" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unknown SCSI exchange can not decode SCSI data](/questions/45624/unknown-scsi-exchange-can-not-decode-scsi-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45624-score" class="post-score" title="current number of votes">0</div><span id="post-45624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm having iSCSI performance problems.</p><p>Packet capture :<br />
<a href="https://www.cloudshark.org/captures/0a58c3b43da8">https://www.cloudshark.org/captures/0a58c3b43da8</a></p><p>I see a number of packets with expert info Warning Unknown SCSI exchange, can not decode SCSI data.</p><p>See packet 104.</p><p>The only thing I can find on this is in the wireshark code</p><p>we have no record of this exchange and so we can't dissect the * payload</p><p>I also noticed in the TCP section that this is 46 reassembled TCP segments.</p><p>What does this all mean with the payload in multiple frames?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span> <span class="post-tag tag-link-scsi" rel="tag" title="see questions tagged &#39;scsi&#39;">scsi</span> <span class="post-tag tag-link-expert" rel="tag" title="see questions tagged &#39;expert&#39;">expert</span> <span class="post-tag tag-link-exchange" rel="tag" title="see questions tagged &#39;exchange&#39;">exchange</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '15, 12:45</strong></p><img src="https://secure.gravatar.com/avatar/a472d068843eefd8a4ef69c4f94e4160?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gipper&#39;s gravatar image" /><p><span>gipper</span><br />
<span class="score" title="30 reputation points">30</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gipper has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-45624" class="comments-container"></div><div id="comment-tools-45624" class="comment-tools"></div><div class="clear"></div><div id="comment-45624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

