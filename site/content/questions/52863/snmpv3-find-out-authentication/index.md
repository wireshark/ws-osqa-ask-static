+++
type = "question"
title = "snmpv3 find out authentication"
description = '''Hello, I did a capture of snmp trap and as far I can tell there was no encryption used but I&#x27;m wondering how can I find out the algorithm used for authentication ?  '''
date = "2016-05-24T05:20:00Z"
lastmod = "2016-05-24T06:10:00Z"
weight = 52863
keywords = [ "snmpv3" ]
aliases = [ "/questions/52863" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [snmpv3 find out authentication](/questions/52863/snmpv3-find-out-authentication)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52863-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52863-score" class="post-score" title="current number of votes">0</div><span id="post-52863-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I did a capture of snmp trap and as far I can tell there was no encryption used but I'm wondering how can I find out the algorithm used for authentication ?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/snmp_wir.JPG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snmpv3" rel="tag" title="see questions tagged &#39;snmpv3&#39;">snmpv3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '16, 05:20</strong></p><img src="https://secure.gravatar.com/avatar/2b3f26f3a24449776af62dd8cca7715a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adasko&#39;s gravatar image" /><p><span>adasko</span><br />
<span class="score" title="86 reputation points">86</span><span title="34 badges"><span class="badge1">●</span><span class="badgecount">34</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adasko has no accepted answers">0%</span></p></img></div></div><div id="comments-container-52863" class="comments-container"><span id="52864"></span><div id="comment-52864" class="comment"><div id="post-52864-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-52864-info" class="comment-info"><span class="comment-age">(24 May '16, 06:10)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-52863" class="comment-tools"></div><div class="clear"></div><div id="comment-52863-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

