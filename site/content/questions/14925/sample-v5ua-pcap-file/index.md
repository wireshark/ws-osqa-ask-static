+++
type = "question"
title = "Sample V5UA pcap file"
description = '''Hello everyone, Does anyone have a sample V5UA pcap file. I have a text file that has a E1 HDLC capture of a V52 message. I want to convert it to V5UA pcap file so wireshark can decode it. Does anyone have a sample V5UA pcap file so I can copy the SCTP and V5UA specific hex before the HDLC dump and ...'''
date = "2012-10-11T05:54:00Z"
lastmod = "2014-08-22T05:56:00Z"
weight = 14925
keywords = [ "v5.2" ]
aliases = [ "/questions/14925" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sample V5UA pcap file](/questions/14925/sample-v5ua-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14925-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14925-score" class="post-score" title="current number of votes">0</div><span id="post-14925-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone,</p><p>Does anyone have a sample V5UA pcap file. I have a text file that has a E1 HDLC capture of a V52 message. I want to convert it to V5UA pcap file so wireshark can decode it. Does anyone have a sample V5UA pcap file so I can copy the SCTP and V5UA specific hex before the HDLC dump and use text2pcap to convert it to pcap file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-v5.2" rel="tag" title="see questions tagged &#39;v5.2&#39;">v5.2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '12, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/70a9f9f000692a136e89f41f52ae064d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rkuppili&#39;s gravatar image" /><p><span>rkuppili</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rkuppili has no accepted answers">0%</span></p></div></div><div id="comments-container-14925" class="comments-container"></div><div id="comment-tools-14925" class="comment-tools"></div><div class="clear"></div><div id="comment-14925-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35669"></span>

<div id="answer-container-35669" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35669-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35669-score" class="post-score" title="current number of votes">0</div><span id="post-35669-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://mts-project.googlecode.com/svn-history/r9/trunk/tutorial/sigtran/v5ua/capture-v5ua.pcap">https://mts-project.googlecode.com/svn-history/r9/trunk/tutorial/sigtran/v5ua/capture-v5ua.pcap</a> might be a necropost, but someone might search it as well</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Aug '14, 05:56</strong></p><img src="https://secure.gravatar.com/avatar/abc0c449cb02934dd6c7ed86b8bc56ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="korlaeda&#39;s gravatar image" /><p><span>korlaeda</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="korlaeda has no accepted answers">0%</span></p></div></div><div id="comments-container-35669" class="comments-container"></div><div id="comment-tools-35669" class="comment-tools"></div><div class="clear"></div><div id="comment-35669-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

