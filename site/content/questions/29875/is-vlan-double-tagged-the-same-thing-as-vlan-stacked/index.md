+++
type = "question"
title = "Is &quot;VLAN double-tagged&quot; the same thing as &#x27;VLAN stacked&#x27;?"
description = '''Is a packet having 2 &#x27;802.1Q&#x27; headers equivalent to that? Or can anyone provide me an example packet demonstrating double-tagged vs. stacked?'''
date = "2014-02-14T16:55:00Z"
lastmod = "2014-02-15T10:50:00Z"
weight = 29875
keywords = [ "vlan" ]
aliases = [ "/questions/29875" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is "VLAN double-tagged" the same thing as 'VLAN stacked'?](/questions/29875/is-vlan-double-tagged-the-same-thing-as-vlan-stacked)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29875-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29875-score" class="post-score" title="current number of votes">0</div><span id="post-29875-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is a packet having 2 '802.1Q' headers equivalent to that?</p><p>Or can anyone provide me an example packet demonstrating double-tagged vs. stacked?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '14, 16:55</strong></p><img src="https://secure.gravatar.com/avatar/48b1ff62ccc5e052bbcefb9b8e43cb58?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="techynewbie&#39;s gravatar image" /><p><span>techynewbie</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="techynewbie has no accepted answers">0%</span></p></div></div><div id="comments-container-29875" class="comments-container"></div><div id="comment-tools-29875" class="comment-tools"></div><div class="clear"></div><div id="comment-29875-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29892"></span>

<div id="answer-container-29892" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29892-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29892-score" class="post-score" title="current number of votes">0</div><span id="post-29892-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes it's the same, also called Q-in-Q, according to the standard 802.1ad.</p><blockquote><p>Is a packet having 2 '802.1Q' headers equivalent to that?</p></blockquote><p>If it adheres to the standard: yes.</p><p>See the following question about double VLAN tags.</p><blockquote><p><a href="http://ask.wireshark.org/questions/15682/wrong-vlan-outer-tag-shown">http://ask.wireshark.org/questions/15682/wrong-vlan-outer-tag-shown</a></p></blockquote><p>There is also a cloudshark link to a VLAN double tagged (stacked) frame in that question.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '14, 10:50</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Feb '14, 11:28</strong> </span></p></div></div><div id="comments-container-29892" class="comments-container"></div><div id="comment-tools-29892" class="comment-tools"></div><div class="clear"></div><div id="comment-29892-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

