+++
type = "question"
title = "Find Packet &quot;search in&quot; radio buttons unresponsive"
description = '''Using Windows 8.0, viewing a TCP capture, Edit-&amp;gt;Find Packet-&amp;gt;Find By String. I enter a string, then attempt to select the &quot;Packet bytes&quot; radio button in &quot;search in&quot; but the selection remains on the default &quot;packet list.&quot; This works just fine in Win XP and WES 7.'''
date = "2014-03-22T08:13:00Z"
lastmod = "2014-03-22T14:51:00Z"
weight = 31076
keywords = [ "windows8", "find" ]
aliases = [ "/questions/31076" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find Packet "search in" radio buttons unresponsive](/questions/31076/find-packet-search-in-radio-buttons-unresponsive)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31076-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31076-score" class="post-score" title="current number of votes">0</div><span id="post-31076-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using Windows 8.0, viewing a TCP capture, Edit-&gt;Find Packet-&gt;Find By String.</p><p>I enter a string, then attempt to select the "Packet bytes" radio button in "search in" but the selection remains on the default "packet list."</p><p>This works just fine in Win XP and WES 7.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '14, 08:13</strong></p><img src="https://secure.gravatar.com/avatar/9586d9ff5b437f6942868a4cb0d6548f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Seitchik&#39;s gravatar image" /><p><span>Seitchik</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Seitchik has no accepted answers">0%</span></p></div></div><div id="comments-container-31076" class="comments-container"><span id="31077"></span><div id="comment-31077" class="comment"><div id="post-31077-score" class="comment-score"></div><div class="comment-text"><p>Do you mind to tell us your Wireshark version?</p></div><div id="comment-31077-info" class="comment-info"><span class="comment-age">(22 Mar '14, 08:24)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="31079"></span><div id="comment-31079" class="comment"><div id="post-31079-score" class="comment-score"></div><div class="comment-text"><p>Wireshark 1.10.5. Upgraded to 1.10.6. Both 64-bit edition. 1.10.6 is actually a little different. I can't select the "string" radio button with the mouse; I have to use Alt-S. I can't select "Packet bytes" with the mouse but I can tab to it and select it with keyboard. I didn't try that with 1.10.5.</p><p>In general, the mouse focus seems "balky" on Windows 8.</p></div><div id="comment-31079-info" class="comment-info"><span class="comment-age">(22 Mar '14, 09:48)</span> <span class="comment-user userinfo">Seitchik</span></div></div><span id="31081"></span><div id="comment-31081" class="comment"><div id="post-31081-score" class="comment-score"></div><div class="comment-text"><p>did you install any modification to the Windows 8 GUI like "Classic Shell" or similar?</p></div><div id="comment-31081-info" class="comment-info"><span class="comment-age">(22 Mar '14, 13:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="31088"></span><div id="comment-31088" class="comment"><div id="post-31088-score" class="comment-score"></div><div class="comment-text"><p>No, didn't install any shells.</p></div><div id="comment-31088-info" class="comment-info"><span class="comment-age">(22 Mar '14, 14:51)</span> <span class="comment-user userinfo">Seitchik</span></div></div></div><div id="comment-tools-31076" class="comment-tools"></div><div class="clear"></div><div id="comment-31076-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

