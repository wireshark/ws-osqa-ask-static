+++
type = "question"
title = "wireshark...no window..does not open"
description = '''OSX 10.9.4 Installed wireshark 64 and x11..i open it..no window anywhere.... anyone have this issue... actually i just noticed..when i click wire shark..a window opens and asks me &quot;where is X11?. I installed it 5 minutes before..looking for it... any ideas how to find it...?'''
date = "2014-08-27T06:26:00Z"
lastmod = "2014-08-27T06:26:00Z"
weight = 35805
keywords = [ "macosx" ]
aliases = [ "/questions/35805" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark...no window..does not open](/questions/35805/wiresharkno-windowdoes-not-open)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35805-score" class="post-score" title="current number of votes">0</div><span id="post-35805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>OSX 10.9.4 Installed wireshark 64 and x11..i open it..no window anywhere....</p><p>anyone have this issue...</p><p>actually i just noticed..when i click wire shark..a window opens and asks me "where is X11?.</p><p>I installed it 5 minutes before..looking for it...</p><p>any ideas how to find it...?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '14, 06:26</strong></p><img src="https://secure.gravatar.com/avatar/7d80ca4ea924dae867bfd78513b4b3b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eaglerockdude&#39;s gravatar image" /><p><span>eaglerockdude</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eaglerockdude has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Aug '14, 06:46</strong> </span></p></div></div><div id="comments-container-35805" class="comments-container"></div><div id="comment-tools-35805" class="comment-tools"></div><div class="clear"></div><div id="comment-35805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

