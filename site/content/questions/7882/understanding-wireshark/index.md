+++
type = "question"
title = "Understanding Wireshark"
description = ''' What do the different colors mean? Why does my computer get packets that are addressed to another machine? How many packets does your computer send/receive in a single mouse click when you visit a Web site? Could you organize or filter the traffic to make it easier to understand? Why does my comput...'''
date = "2011-12-09T10:14:00Z"
lastmod = "2011-12-09T23:12:00Z"
weight = 7882
keywords = [ "understanding" ]
aliases = [ "/questions/7882" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Understanding Wireshark](/questions/7882/understanding-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7882-score" class="post-score" title="current number of votes">-3</div><span id="post-7882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><ol><li>What do the different colors mean?</li><li>Why does my computer get packets that are addressed to another machine?</li><li>How many packets does your computer send/receive in a single mouse click when you visit a Web site?</li><li>Could you organize or filter the traffic to make it easier to understand?</li><li>Why does my computer send so many packets? Why not send just on BIG packet?</li><li>What do SYN, ACK, FIND, GET mean?</li><li>Why do some packets have sequence numbers?</li><li>Why does my computer send packets to the Web server that I requested data from?</li><li>Why wouldn’t email be encrypted by default?</li><li>Can I look at Web content just as easily as Web traffic?</li><li>Can I look at information being sent to/from my bank?</li><li>Could I filter the traffic based on IP address and packet type for a given person in our company?</li><li>Why are there so many different filed types to filter?</li><li>What protocols, other than TCP/IP, are used to mange traffic across networks?</li><li>What statistics are available about the data I picked up?</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-understanding" rel="tag" title="see questions tagged &#39;understanding&#39;">understanding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '11, 10:14</strong></p><img src="https://secure.gravatar.com/avatar/88fa77735a8fc8af2a0f736217a7d60c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="penguin73&#39;s gravatar image" /><p><span>penguin73</span><br />
<span class="score" title="15 reputation points">15</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="penguin73 has no accepted answers">0%</span></p></div></div><div id="comments-container-7882" class="comments-container"><span id="7887"></span><div id="comment-7887" class="comment"><div id="post-7887-score" class="comment-score"></div><div class="comment-text"><p>Sounds like a final exam.... :)</p></div><div id="comment-7887-info" class="comment-info"><span class="comment-age">(09 Dec '11, 12:32)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-7882" class="comment-tools"></div><div class="clear"></div><div id="comment-7882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7891"></span>

<div id="answer-container-7891" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7891-score" class="post-score" title="current number of votes">1</div><span id="post-7891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need to get Laura Chappell's book "Wireshark Netwok Analysis" ..... now!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '11, 23:12</strong></p><img src="https://secure.gravatar.com/avatar/f797bdc41d990dca073837114e048b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EricKnaus&#39;s gravatar image" /><p><span>EricKnaus</span><br />
<span class="score" title="46 reputation points">46</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EricKnaus has no accepted answers">0%</span></p></div></div><div id="comments-container-7891" class="comments-container"></div><div id="comment-tools-7891" class="comment-tools"></div><div class="clear"></div><div id="comment-7891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

