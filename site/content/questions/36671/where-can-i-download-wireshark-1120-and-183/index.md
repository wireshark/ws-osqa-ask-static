+++
type = "question"
title = "where can i download wireshark 1.12.0 and 1.8.3？"
description = '''where can i download wireshark 1.12.0 and 1.8.3 ？'''
date = "2014-09-28T20:18:00Z"
lastmod = "2014-09-28T23:55:00Z"
weight = 36671
keywords = [ "downloads" ]
aliases = [ "/questions/36671" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [where can i download wireshark 1.12.0 and 1.8.3？](/questions/36671/where-can-i-download-wireshark-1120-and-183)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36671-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36671-score" class="post-score" title="current number of votes">0</div><span id="post-36671-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>where can i download wireshark 1.12.0 and 1.8.3 ？</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-downloads" rel="tag" title="see questions tagged &#39;downloads&#39;">downloads</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Sep '14, 20:18</strong></p><img src="https://secure.gravatar.com/avatar/13b562de9e7ad8a43ebf600c68c983ec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Peter%20Chang&#39;s gravatar image" /><p><span>Peter Chang</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Peter Chang has no accepted answers">0%</span></p></div></div><div id="comments-container-36671" class="comments-container"></div><div id="comment-tools-36671" class="comment-tools"></div><div class="clear"></div><div id="comment-36671-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36676"></span>

<div id="answer-container-36676" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36676-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36676-score" class="post-score" title="current number of votes">0</div><span id="post-36676-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On the Wireshark Website <a href="https://www.wireshark.org/download.html#spelunking">https://www.wireshark.org/download.html#spelunking</a></p><p>"You can explore the download areas of the main site and mirrors below. Past releases can be found by browsing the <strong>all-versions</strong> directories under each platform directory."</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Sep '14, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div></div><div id="comments-container-36676" class="comments-container"></div><div id="comment-tools-36676" class="comment-tools"></div><div class="clear"></div><div id="comment-36676-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

