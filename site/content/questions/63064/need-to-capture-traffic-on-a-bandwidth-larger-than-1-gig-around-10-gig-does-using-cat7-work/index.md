+++
type = "question"
title = "Need to capture traffic on a bandwidth larger than 1 gig (around 10 gig), does using cat7 work?"
description = '''Hello, I need to capture traffic on a port on a server. I want large bandwidth (around 10 Gig). Since wireshark doesn&#x27;t support capturing traffic on fiber channel, can I connect a Cat 7 cable to a 10 Gig ethernet port on the server and a 10 gig copper SFP port on my switch which supports fiber to ca...'''
date = "2017-07-24T17:03:00Z"
lastmod = "2017-07-25T00:12:00Z"
weight = 63064
keywords = [ "fiber" ]
aliases = [ "/questions/63064" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need to capture traffic on a bandwidth larger than 1 gig (around 10 gig), does using cat7 work?](/questions/63064/need-to-capture-traffic-on-a-bandwidth-larger-than-1-gig-around-10-gig-does-using-cat7-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63064-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63064-score" class="post-score" title="current number of votes">0</div><span id="post-63064-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I need to capture traffic on a port on a server. I want large bandwidth (around 10 Gig). Since wireshark doesn't support capturing traffic on fiber channel, can I connect a Cat 7 cable to a 10 Gig ethernet port on the server and a 10 gig copper SFP port on my switch which supports fiber to capture traffic? Will it work?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fiber" rel="tag" title="see questions tagged &#39;fiber&#39;">fiber</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '17, 17:03</strong></p><img src="https://secure.gravatar.com/avatar/eaa73c4df1ae368a4a174660ddf5924f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindhusaripella&#39;s gravatar image" /><p><span>sindhusaripella</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindhusaripella has no accepted answers">0%</span></p></div></div><div id="comments-container-63064" class="comments-container"><span id="63075"></span><div id="comment-63075" class="comment"><div id="post-63075-score" class="comment-score"></div><div class="comment-text"><p>Is the "server" you mention:</p><ul><li>the machine whose traffic you want to capture, or</li><li>the machine on which you are going to run Wireshark for capturing traffic of some other machine(s), using port mirroring at the switch?</li></ul></div><div id="comment-63075-info" class="comment-info"><span class="comment-age">(25 Jul '17, 00:12)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-63064" class="comment-tools"></div><div class="clear"></div><div id="comment-63064-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

