+++
type = "question"
title = "Can I capture bluetooth between car and phone?"
description = '''I am a newbie. I am having a bluetooth problem with my phone and car such that I can&#x27;t get calls to go through the car&#x27;s audio system. The car and phone are supposed to be able to do this but the Toyota doesn&#x27;t have support for the car&#x27;s bluetooth system anymore. I can take a Apple laptop to the car...'''
date = "2014-03-20T12:58:00Z"
lastmod = "2014-11-05T03:02:00Z"
weight = 31013
keywords = [ "bluetooth" ]
aliases = [ "/questions/31013" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I capture bluetooth between car and phone?](/questions/31013/can-i-capture-bluetooth-between-car-and-phone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31013-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31013-score" class="post-score" title="current number of votes">0</div><span id="post-31013-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a newbie. I am having a bluetooth problem with my phone and car such that I can't get calls to go through the car's audio system. The car and phone are supposed to be able to do this but the Toyota doesn't have support for the car's bluetooth system anymore. I can take a Apple laptop to the car. Can Wireshark on the laptop pick up the attempts to communicate between the phone and car?</p><p>Thanks, Mark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '14, 12:58</strong></p><img src="https://secure.gravatar.com/avatar/c7a90727c1e2bf7100443fd8d573c862?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wilderson&#39;s gravatar image" /><p><span>wilderson</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wilderson has no accepted answers">0%</span></p></div></div><div id="comments-container-31013" class="comments-container"><span id="37582"></span><div id="comment-37582" class="comment"><div id="post-37582-score" class="comment-score"></div><div class="comment-text"><p>Definitely you can do it and connect bluetooth between car and phone</p></div><div id="comment-37582-info" class="comment-info"><span class="comment-age">(05 Nov '14, 03:02)</span> <span class="comment-user userinfo">MarkMadrigal</span></div></div></div><div id="comment-tools-31013" class="comment-tools"></div><div class="clear"></div><div id="comment-31013-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31072"></span>

<div id="answer-container-31072" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31072-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31072-score" class="post-score" title="current number of votes">0</div><span id="post-31072-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not with Wireshark.</p><p>Please see <a href="http://wiki.wireshark.org/CaptureSetup/Bluetooth">http://wiki.wireshark.org/CaptureSetup/Bluetooth</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '14, 16:51</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Mar '14, 16:52</strong> </span></p></div></div><div id="comments-container-31072" class="comments-container"></div><div id="comment-tools-31072" class="comment-tools"></div><div class="clear"></div><div id="comment-31072-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

