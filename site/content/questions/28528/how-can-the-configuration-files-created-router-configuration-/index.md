+++
type = "question"
title = "How can the configuration files created -Router configuration-"
description = '''I wonder how to create a configuration file and how I can save it?'''
date = "2014-01-02T10:55:00Z"
lastmod = "2014-01-04T16:07:00Z"
weight = 28528
keywords = [ "save", "configuration", "creat", "file", "roter" ]
aliases = [ "/questions/28528" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can the configuration files created -Router configuration-](/questions/28528/how-can-the-configuration-files-created-router-configuration-)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28528-score" class="post-score" title="current number of votes">0</div><span id="post-28528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I wonder how to create a configuration file and how I can save it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span> <span class="post-tag tag-link-creat" rel="tag" title="see questions tagged &#39;creat&#39;">creat</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-roter" rel="tag" title="see questions tagged &#39;roter&#39;">roter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jan '14, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/94899a17ae2974d1ddfe4bcfe8b0efc6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ALwrad&#39;s gravatar image" /><p><span>ALwrad</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ALwrad has no accepted answers">0%</span></p></div></div><div id="comments-container-28528" class="comments-container"><span id="28531"></span><div id="comment-28531" class="comment"><div id="post-28531-score" class="comment-score"></div><div class="comment-text"><p>This appears to be a router related question and not related to Wireshark. I suggest you ask your question on a forum or support area for the manufacturer of your router.</p></div><div id="comment-28531-info" class="comment-info"><span class="comment-age">(02 Jan '14, 13:55)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="28533"></span><div id="comment-28533" class="comment"><div id="post-28533-score" class="comment-score"></div><div class="comment-text"><p>Or maybe a question on an ACL, which is possible.</p></div><div id="comment-28533-info" class="comment-info"><span class="comment-age">(02 Jan '14, 14:53)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-28528" class="comment-tools"></div><div class="clear"></div><div id="comment-28528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28579"></span>

<div id="answer-container-28579" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28579-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28579-score" class="post-score" title="current number of votes">0</div><span id="post-28579-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Even acknowledging it as off-topic, there's not enough detail to really answer it. If you're talking about the configuration file of a Cisco router, usually you create the configuration file by simply issuing configuration commands, and save it with "write" or "copy run start".</p><p>Aside from that, this guide on managing configuraiton files from Cisco answers just about any question I imagine you could have on it procedure-wise: <a href="http://www.cisco.com/en/US/docs/ios/12_2/configfun/configuration/guide/fcf007_ps1835_TSD_Products_Configuration_Guide_Chapter.html">http://www.cisco.com/en/US/docs/ios/12_2/configfun/configuration/guide/fcf007_ps1835_TSD_Products_Configuration_Guide_Chapter.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '14, 16:07</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-28579" class="comments-container"></div><div id="comment-tools-28579" class="comment-tools"></div><div class="clear"></div><div id="comment-28579-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

