+++
type = "question"
title = "Capture AuthKey in wireshark"
description = '''I am assessing the WPS protocols and the EAP process. Is it possible to capture the &quot;Authkey&quot; in the WPS process? if yes..where can I locate this value in wireshark..thanks for your answers'''
date = "2015-03-26T17:33:00Z"
lastmod = "2015-03-26T17:33:00Z"
weight = 40923
keywords = [ "authkey", "wps" ]
aliases = [ "/questions/40923" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture AuthKey in wireshark](/questions/40923/capture-authkey-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40923-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40923-score" class="post-score" title="current number of votes">0</div><span id="post-40923-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am assessing the WPS protocols and the EAP process. Is it possible to capture the "Authkey" in the WPS process? if yes..where can I locate this value in wireshark..thanks for your answers</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-authkey" rel="tag" title="see questions tagged &#39;authkey&#39;">authkey</span> <span class="post-tag tag-link-wps" rel="tag" title="see questions tagged &#39;wps&#39;">wps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '15, 17:33</strong></p><img src="https://secure.gravatar.com/avatar/9b6feb2fd2e6a3f79bbcd81040063d5e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jay%20Dee&#39;s gravatar image" /><p><span>Jay Dee</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jay Dee has no accepted answers">0%</span></p></div></div><div id="comments-container-40923" class="comments-container"></div><div id="comment-tools-40923" class="comment-tools"></div><div class="clear"></div><div id="comment-40923-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

