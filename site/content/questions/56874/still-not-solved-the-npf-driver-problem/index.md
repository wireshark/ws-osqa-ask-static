+++
type = "question"
title = "still not solved the npf driver problem!!!!"
description = '''1 as i said in the previous question the npf driver is not starting through cmd also and i also tried to install winpcap latest version 4.1.3 but it shows me &quot;a newer version of winpcap (internal version 5.0.10.921) is already installed on this machine. the installation will be aborted.&quot; i reinstall...'''
date = "2016-10-31T12:12:00Z"
lastmod = "2016-10-31T14:26:00Z"
weight = 56874
keywords = [ "winpcap", "wireshark" ]
aliases = [ "/questions/56874" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [still not solved the npf driver problem!!!!](/questions/56874/still-not-solved-the-npf-driver-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56874-score" class="post-score" title="current number of votes">0</div><span id="post-56874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><strong>1</strong> as i said in the previous question the npf driver is not starting through cmd also and i also tried to install winpcap latest version 4.1.3 but it shows me "a newer version of winpcap (internal version 5.0.10.921) is already installed on this machine. the installation will be aborted." i reinstalled wireshark but no solution at all. please help to get out of that problem.</p><p><strong>2</strong> wireshark only supports winpcap version 4.1.3????</p><p><strong>3</strong> packet can be captured through lan only or it can be captured through wireless also</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '16, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/26cfc77e3e3c34b6ea1453dc6b3ae62c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="karan9537&#39;s gravatar image" /><p><span>karan9537</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="karan9537 has no accepted answers">0%</span></p></div></div><div id="comments-container-56874" class="comments-container"><span id="56875"></span><div id="comment-56875" class="comment"><div id="post-56875-score" class="comment-score"></div><div class="comment-text"><p>what is internal version of winpcap</p></div><div id="comment-56875-info" class="comment-info"><span class="comment-age">(31 Oct '16, 12:17)</span> <span class="comment-user userinfo">karan9537</span></div></div><span id="56876"></span><div id="comment-56876" class="comment"><div id="post-56876-score" class="comment-score"></div><div class="comment-text"><p>Get rid of Windows and use Linux (e.g Ubuntu 16.04) which is more convenient for any networking task.</p></div><div id="comment-56876-info" class="comment-info"><span class="comment-age">(31 Oct '16, 12:30)</span> <span class="comment-user userinfo">user31415</span></div></div><span id="56883"></span><div id="comment-56883" class="comment"><div id="post-56883-score" class="comment-score"></div><div class="comment-text"><p>In your answer to <a href="https://ask.wireshark.org/questions/56835/showing-service-name-is-invalid-whenever-i-start-npf-service-through-cmd">this question</a> you said the driver is now starting after you renamed the old DLLs and re-installed WinPcap; did that fix the driver-not-starting problem?</p></div><div id="comment-56883-info" class="comment-info"><span class="comment-age">(31 Oct '16, 14:26)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-56874" class="comment-tools"></div><div class="clear"></div><div id="comment-56874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

