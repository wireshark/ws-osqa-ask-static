+++
type = "question"
title = "Packet Analyzer"
description = '''Hello to everyone, I would like to ask the Pro a question. I am looking for a software which can intercept packets sent from program to http server.I have a program that sends packets to a web server and also receives packets back.I would like to intercept the ones that are comming back and analyze ...'''
date = "2014-01-11T02:09:00Z"
lastmod = "2014-01-11T03:25:00Z"
weight = 28792
keywords = [ "packetanalyzer" ]
aliases = [ "/questions/28792" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet Analyzer](/questions/28792/packet-analyzer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28792-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28792-score" class="post-score" title="current number of votes">0</div><span id="post-28792-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello to everyone, I would like to ask the Pro a question. I am looking for a software which can intercept packets sent from program to http server.I have a program that sends packets to a web server and also receives packets back.I would like to intercept the ones that are comming back and analyze them.Do you think Shark can do that for me? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packetanalyzer" rel="tag" title="see questions tagged &#39;packetanalyzer&#39;">packetanalyzer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '14, 02:09</strong></p><img src="https://secure.gravatar.com/avatar/e264adca66664aabb0a98e81b1a1f278?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="marian&#39;s gravatar image" /><p><span>marian</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="marian has no accepted answers">0%</span></p></div></div><div id="comments-container-28792" class="comments-container"></div><div id="comment-tools-28792" class="comment-tools"></div><div class="clear"></div><div id="comment-28792-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28793"></span>

<div id="answer-container-28793" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28793-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28793-score" class="post-score" title="current number of votes">1</div><span id="post-28793-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Do you think Shark can do that for me?</p></blockquote><p>Yes, that's what Wireshark was made for.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '14, 03:25</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28793" class="comments-container"></div><div id="comment-tools-28793" class="comment-tools"></div><div class="clear"></div><div id="comment-28793-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

