+++
type = "question"
title = "Filter for the custom FIX tags"
description = '''Hello, For example, FIX message contains a few custom tags: 5000 = XXX  5010 = YYY  6012 = XYZ Is it possible to use someone filter -e fix. , which would display a value for a specific tag, for example, only for tag 5010? If yes, please advise how to do this. Thanks in advance!'''
date = "2015-04-27T04:40:00Z"
lastmod = "2015-04-27T04:40:00Z"
weight = 41880
keywords = [ "filter", "fix", "tshark" ]
aliases = [ "/questions/41880" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Filter for the custom FIX tags](/questions/41880/filter-for-the-custom-fix-tags)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41880-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41880-score" class="post-score" title="current number of votes">0</div><span id="post-41880-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>For example, FIX message contains a few custom tags:</p><p>5000 = XXX<br />
5010 = YYY<br />
6012 = XYZ<br />
</p><p>Is it possible to use someone filter <strong>-e fix.</strong> , which would display a value for a specific tag, for example, only for tag 5010? If yes, please advise how to do this.</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-fix" rel="tag" title="see questions tagged &#39;fix&#39;">fix</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '15, 04:40</strong></p><img src="https://secure.gravatar.com/avatar/00fc9b7ddbee4ec657351cff07ace3ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrav&#39;s gravatar image" /><p><span>mrav</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrav has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-41880" class="comments-container"></div><div id="comment-tools-41880" class="comment-tools"></div><div class="clear"></div><div id="comment-41880-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

