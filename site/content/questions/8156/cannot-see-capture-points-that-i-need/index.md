+++
type = "question"
title = "Cannot see capture points that i need"
description = '''I am running Windows 7. Have a Napatech NT20E2 and NT4E cards installed on server.  Drivers are up to date and shows device installed.  How do i get the device to show up in Wireshark?'''
date = "2011-12-28T13:42:00Z"
lastmod = "2011-12-28T13:42:00Z"
weight = 8156
keywords = [ "capture" ]
aliases = [ "/questions/8156" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot see capture points that i need](/questions/8156/cannot-see-capture-points-that-i-need)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8156-score" class="post-score" title="current number of votes">0</div><span id="post-8156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am running Windows 7. Have a Napatech NT20E2 and NT4E cards installed on server. Drivers are up to date and shows device installed.</p><p>How do i get the device to show up in Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '11, 13:42</strong></p><img src="https://secure.gravatar.com/avatar/1b85366c83047fc21a28c62470601388?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="USMC_Contractor&#39;s gravatar image" /><p><span>USMC_Contractor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="USMC_Contractor has no accepted answers">0%</span></p></div></div><div id="comments-container-8156" class="comments-container"></div><div id="comment-tools-8156" class="comment-tools"></div><div class="clear"></div><div id="comment-8156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

