+++
type = "question"
title = "Want to learn WireShark"
description = '''Hello Everybody,  I want to learn troubleshooting using wireshark like slow network file copies and lot more. What is the best way to learn.  Thanks Skak'''
date = "2012-12-20T02:01:00Z"
lastmod = "2012-12-20T04:06:00Z"
weight = 17083
keywords = [ "learning" ]
aliases = [ "/questions/17083" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Want to learn WireShark](/questions/17083/want-to-learn-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17083-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17083-score" class="post-score" title="current number of votes">0</div><span id="post-17083-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Everybody,</p><pre><code>           I want to learn troubleshooting using wireshark like slow network file copies and lot more. What is the best way to learn.</code></pre><p>Thanks Skak</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-learning" rel="tag" title="see questions tagged &#39;learning&#39;">learning</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Dec '12, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/5ecf0db4a0d40f3b570a26ef1203757a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skak&#39;s gravatar image" /><p><span>skak</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skak has no accepted answers">0%</span></p></div></div><div id="comments-container-17083" class="comments-container"></div><div id="comment-tools-17083" class="comment-tools"></div><div class="clear"></div><div id="comment-17083-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="17090"></span>

<div id="answer-container-17090" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17090-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17090-score" class="post-score" title="current number of votes">0</div><span id="post-17090-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Google for Wireshark tutorials, there are plenty out there. Also, here's a <a href="http://www.youtube.com/watch?v=0bazkLeY6b4">video</a> showing the basics.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Dec '12, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/46196bc495ce51058590c4e4ae334d22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SidR&#39;s gravatar image" /><p><span>SidR</span><br />
<span class="score" title="245 reputation points">245</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="22 badges"><span class="bronze">●</span><span class="badgecount">22</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SidR has 3 accepted answers">30%</span></p></div></div><div id="comments-container-17090" class="comments-container"></div><div id="comment-tools-17090" class="comment-tools"></div><div class="clear"></div><div id="comment-17090-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="17092"></span>

<div id="answer-container-17092" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17092-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17092-score" class="post-score" title="current number of votes">0</div><span id="post-17092-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here's <a href="http://www.wireshark.org/docs/">a boatload of references</a> to learning material. And from the looks of it you should visit <a href="http://www.wiresharktraining.com/">the University</a> too.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Dec '12, 04:06</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-17092" class="comments-container"></div><div id="comment-tools-17092" class="comment-tools"></div><div class="clear"></div><div id="comment-17092-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

