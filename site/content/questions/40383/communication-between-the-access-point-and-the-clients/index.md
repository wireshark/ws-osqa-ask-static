+++
type = "question"
title = "Communication between the Access Point and the clients"
description = '''So basically i have an Access Point with DHCP enabled, and 2 clients, one is a laptop and the other is a drone. When i capture packets with wireshark from the laptop, i only see the communication between the 2 clients and i can only see the destination and source IP address at each packet. The two c...'''
date = "2015-03-09T07:44:00Z"
lastmod = "2015-03-09T07:44:00Z"
weight = 40383
keywords = [ "client", "accesspoint" ]
aliases = [ "/questions/40383" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Communication between the Access Point and the clients](/questions/40383/communication-between-the-access-point-and-the-clients)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40383-score" class="post-score" title="current number of votes">0</div><span id="post-40383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So basically i have an Access Point with DHCP enabled, and 2 clients, one is a laptop and the other is a drone. When i capture packets with wireshark from the laptop, i only see the communication between the 2 clients and i can only see the destination and source IP address at each packet. The two clients communicate with each other through the access point, but the wireshark doesnt show that.</p><p>I would like to know if it is possible somehow to display that the clients are sending the packets through the access point?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-client" rel="tag" title="see questions tagged &#39;client&#39;">client</span> <span class="post-tag tag-link-accesspoint" rel="tag" title="see questions tagged &#39;accesspoint&#39;">accesspoint</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '15, 07:44</strong></p><img src="https://secure.gravatar.com/avatar/974d464fb6d79ef943948ffd79d0cc93?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MeistroForte&#39;s gravatar image" /><p><span>MeistroForte</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MeistroForte has no accepted answers">0%</span></p></div></div><div id="comments-container-40383" class="comments-container"></div><div id="comment-tools-40383" class="comment-tools"></div><div class="clear"></div><div id="comment-40383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

