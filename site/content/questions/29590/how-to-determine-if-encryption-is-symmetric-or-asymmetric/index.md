+++
type = "question"
title = "How to determine if encryption is symmetric or asymmetric"
description = '''I was wondering how to determine if an email that is sent is asymmetrically encrypted or symmetrically encrypted? '''
date = "2014-02-09T20:11:00Z"
lastmod = "2014-02-09T23:51:00Z"
weight = 29590
keywords = [ "symmetric", "encryption", "asymmetric" ]
aliases = [ "/questions/29590" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to determine if encryption is symmetric or asymmetric](/questions/29590/how-to-determine-if-encryption-is-symmetric-or-asymmetric)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29590-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29590-score" class="post-score" title="current number of votes">0</div><span id="post-29590-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was wondering how to determine if an email that is sent is asymmetrically encrypted or symmetrically encrypted?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-symmetric" rel="tag" title="see questions tagged &#39;symmetric&#39;">symmetric</span> <span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-asymmetric" rel="tag" title="see questions tagged &#39;asymmetric&#39;">asymmetric</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '14, 20:11</strong></p><img src="https://secure.gravatar.com/avatar/9105e68ac877d692a463b6d5c71bcae2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ihavenoidea&#39;s gravatar image" /><p><span>ihavenoidea</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ihavenoidea has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-29590" class="comments-container"><span id="29591"></span><div id="comment-29591" class="comment"><div id="post-29591-score" class="comment-score"></div><div class="comment-text"><p>I am taking a network security class. We are given these packet captures and asked if it is asymmetric or symmetric encryption. I spent a few hours staring at each packet and have come no close to understanding it.</p></div><div id="comment-29591-info" class="comment-info"><span class="comment-age">(09 Feb '14, 20:13)</span> <span class="comment-user userinfo">ihavenoidea</span></div></div></div><div id="comment-tools-29590" class="comment-tools"></div><div class="clear"></div><div id="comment-29590-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29594"></span>

<div id="answer-container-29594" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29594-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29594-score" class="post-score" title="current number of votes">0</div><span id="post-29594-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no way to detect that just by looking at the encrypted data, as both symmetric and asymmetric encryption produce 'random looking' bytes sequences. So, you need to know how the e-mail was encrypted. Sometimes you can deduce that from information included in the encryption framework (PGP or S/MIME) or the protocol (TLS).</p><p>If you can post the capture somwhere (Google drive, dropbox, cloudshark.org) I will give some hints where to look for that information. I won't give the full answer for the homework!!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '14, 23:51</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Feb '14, 23:53</strong> </span></p></div></div><div id="comments-container-29594" class="comments-container"></div><div id="comment-tools-29594" class="comment-tools"></div><div class="clear"></div><div id="comment-29594-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

