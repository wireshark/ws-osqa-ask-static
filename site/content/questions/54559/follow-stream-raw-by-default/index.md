+++
type = "question"
title = "Follow stream Raw by default?"
description = '''In version 1.x Raw was the default. Now in version 2.0 Ascii seems to be the default when I follow a stream. Is there a way to get the default back to RAW again?'''
date = "2016-08-03T08:04:00Z"
lastmod = "2016-08-04T02:16:00Z"
weight = 54559
keywords = [ "raw" ]
aliases = [ "/questions/54559" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Follow stream Raw by default?](/questions/54559/follow-stream-raw-by-default)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54559-score" class="post-score" title="current number of votes">0</div><span id="post-54559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In version 1.x Raw was the default. Now in version 2.0 Ascii seems to be the default when I follow a stream. Is there a way to get the default back to RAW again?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-raw" rel="tag" title="see questions tagged &#39;raw&#39;">raw</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '16, 08:04</strong></p><img src="https://secure.gravatar.com/avatar/7c5474f71399d22bfbee5a0e86550fc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rchapoteau&#39;s gravatar image" /><p><span>rchapoteau</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rchapoteau has no accepted answers">0%</span></p></div></div><div id="comments-container-54559" class="comments-container"></div><div id="comment-tools-54559" class="comment-tools"></div><div class="clear"></div><div id="comment-54559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54568"></span>

<div id="answer-container-54568" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54568-score" class="post-score" title="current number of votes">0</div><span id="post-54568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are no preference settings related to this. With the recreation of the Qt based user interface it was probably determined that the ASCII representation was the most useful, therefore this was selected as default.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Aug '16, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-54568" class="comments-container"></div><div id="comment-tools-54568" class="comment-tools"></div><div class="clear"></div><div id="comment-54568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

