+++
type = "question"
title = "554 message is not RFC compliant;missing &quot;date&quot; header"
description = '''Hi everybody. i have a serious problem with SMTP. when i want to test pop3 and smtp status in outlook it respond to me that pop3 is ok but smtp hast a problem and its error is &quot;554 message is not RFC compliant;missing &quot;date&quot; header&quot;. The connection with destination is fine (i checked it by telnet co...'''
date = "2012-10-18T00:30:00Z"
lastmod = "2012-10-18T00:30:00Z"
weight = 15071
keywords = [ "smtp" ]
aliases = [ "/questions/15071" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [554 message is not RFC compliant;missing "date" header](/questions/15071/554-message-is-not-rfc-compliantmissing-date-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15071-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15071-score" class="post-score" title="current number of votes">0</div><span id="post-15071-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everybody. i have a serious problem with SMTP. when i want to test pop3 and smtp status in outlook it respond to me that pop3 is ok but smtp hast a problem and its error is "554 message is not RFC compliant;missing "date" header". The connection with destination is fine (i checked it by telnet command on both ports 25 and 110 ) . destination mail server is Mdeamon and i found it there is a configuration setting in mail server which allow to accept unusual packet like this,but it's not fine to set that cause of security problem.anyway how can i check this problem with wireshark ? somebody told me it related with window size ! but i don't have enough knowledge about that.please help me ? have you ever encounter a problem like that ? what is the solution ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smtp" rel="tag" title="see questions tagged &#39;smtp&#39;">smtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '12, 00:30</strong></p><img src="https://secure.gravatar.com/avatar/c5df2d2eef50b100d0561e9787385b0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jsoft&#39;s gravatar image" /><p><span>Jsoft</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jsoft has no accepted answers">0%</span></p></div></div><div id="comments-container-15071" class="comments-container"></div><div id="comment-tools-15071" class="comment-tools"></div><div class="clear"></div><div id="comment-15071-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

