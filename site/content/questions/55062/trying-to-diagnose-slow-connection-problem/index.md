+++
type = "question"
title = "Trying to diagnose slow connection problem"
description = '''Hi, First of all ill really appreciate any help you guys can gave me. I connect to my ISP with a Nanostation M900, since i dont have the password of it to see the antenna parameters im trying to diagnose if the connection not working properly is and internal problem (something i can fix) like alignm...'''
date = "2016-08-22T18:57:00Z"
lastmod = "2016-08-22T18:57:00Z"
weight = 55062
keywords = [ "isp" ]
aliases = [ "/questions/55062" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Trying to diagnose slow connection problem](/questions/55062/trying-to-diagnose-slow-connection-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55062-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55062-score" class="post-score" title="current number of votes">0</div><span id="post-55062-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>First of all ill really appreciate any help you guys can gave me.</p><p>I connect to my ISP with a Nanostation M900, since i dont have the password of it to see the antenna parameters im trying to diagnose if the connection not working properly is and internal problem (something i can fix) like alignment or external. When i say "not working properly" i mean slow download/upload speed for time to time, huge latency spikes like from 30/40ms to 2000ms.</p><p>The first thing i did is the basic ping test between my pc and the Nano and its OK, 1ms everytime, even when the connection is not working good.</p><p>So what i need to know is if there is a way to know the ISP antenna ip using Wireshark so i can ping to it and see which is the response time between my antenna and his when internet is working bad, if the ping response time is low then the problem is external.</p><p>This is the only thing that comes to my mind to diagnose it, if you guys know some other way to test it please tell me.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-isp" rel="tag" title="see questions tagged &#39;isp&#39;">isp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '16, 18:57</strong></p><img src="https://secure.gravatar.com/avatar/3e8a53a8a8b42d543f8693e2505fbce2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sarasa&#39;s gravatar image" /><p><span>Sarasa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sarasa has no accepted answers">0%</span></p></div></div><div id="comments-container-55062" class="comments-container"></div><div id="comment-tools-55062" class="comment-tools"></div><div class="clear"></div><div id="comment-55062-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

