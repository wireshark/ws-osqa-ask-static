+++
type = "question"
title = "how to Disable wireshark updates and downloads in help menu?"
description = '''Hi, Can you folks help me out how to grade out or remove check for updates and downloads in the help menu tab for wirshark.Thanks in Advance'''
date = "2014-02-10T22:43:00Z"
lastmod = "2014-02-11T02:53:00Z"
weight = 29661
keywords = [ "downloads", "disable" ]
aliases = [ "/questions/29661" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to Disable wireshark updates and downloads in help menu?](/questions/29661/how-to-disable-wireshark-updates-and-downloads-in-help-menu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29661-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29661-score" class="post-score" title="current number of votes">0</div><span id="post-29661-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Can you folks help me out how to grade out or remove check for updates and downloads in the help menu tab for wirshark.Thanks in Advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-downloads" rel="tag" title="see questions tagged &#39;downloads&#39;">downloads</span> <span class="post-tag tag-link-disable" rel="tag" title="see questions tagged &#39;disable&#39;">disable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '14, 22:43</strong></p><img src="https://secure.gravatar.com/avatar/9e1a4cf24ba4c99becad013f05cfa946?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pkumar&#39;s gravatar image" /><p><span>pkumar</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pkumar has no accepted answers">0%</span></p></div></div><div id="comments-container-29661" class="comments-container"></div><div id="comment-tools-29661" class="comment-tools"></div><div class="clear"></div><div id="comment-29661-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29671"></span>

<div id="answer-container-29671" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29671-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29671-score" class="post-score" title="current number of votes">1</div><span id="post-29671-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As per my answer to your very similar <a href="http://ask.wireshark.org/questions/29595/disable-check-for-updates-and-downloads-for-wireshark-1113">question</a> to do this you will need to compile your own version of Wireshark, after modifying the source code to remove the menu entries.</p><p>Why do you want to do this, don't you want to be helpful to your users?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Feb '14, 01:28</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-29671" class="comments-container"><span id="29678"></span><div id="comment-29678" class="comment"><div id="post-29678-score" class="comment-score"></div><div class="comment-text"><p>Could you please give me some more inputs as i am not into coding. We need to deploy this application to users silently and need to suppress the updates and downloads as per client requirement.Thanks</p></div><div id="comment-29678-info" class="comment-info"><span class="comment-age">(11 Feb '14, 02:21)</span> <span class="comment-user userinfo">pkumar</span></div></div><span id="29679"></span><div id="comment-29679" class="comment"><div id="post-29679-score" class="comment-score"></div><div class="comment-text"><p>The source code, tools and techniques to build your own version of Wireshark are all freely available and extensively discussed in the <a href="http://www.wireshark.org/docs/wsdg_html_chunked/">Developers Guide</a>.</p><p>As you'll be distributing Wireshark (commercially ??) you should also take note of the Wireshark licence (<a href="http://www.gnu.org/licenses/gpl-2.0.html">GPL v2</a> and <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0-faq.html">GPL2 FAQ</a>) and the Wireshark FAQ items <a href="http://www.wireshark.org/faq.html#q1.6">1.6 about costs</a> and <a href="http://www.wireshark.org/faq.html#q1.9">1.9 about commercial use</a>.</p></div><div id="comment-29679-info" class="comment-info"><span class="comment-age">(11 Feb '14, 02:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-29671" class="comment-tools"></div><div class="clear"></div><div id="comment-29671-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

