+++
type = "question"
title = "SMS wireshark logs in 3g"
description = '''Hi ALL, Can anyone please provide me the successfull logs for SMS, MMS and location update... I need these on urjent basis. Thanks in advance. Manish Singla manish9@aricent.com'''
date = "2013-12-06T00:28:00Z"
lastmod = "2013-12-06T17:20:00Z"
weight = 27847
keywords = [ "sms", "logs", "mms" ]
aliases = [ "/questions/27847" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SMS wireshark logs in 3g](/questions/27847/sms-wireshark-logs-in-3g)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27847-score" class="post-score" title="current number of votes">0</div><span id="post-27847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ALL,</p><p>Can anyone please provide me the successfull logs for SMS, MMS and location update... I need these on urjent basis.</p><p>Thanks in advance.</p><p>Manish Singla <span class="__cf_email__" data-cfemail="513c303f382239681130233832343f257f323e3c">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sms" rel="tag" title="see questions tagged &#39;sms&#39;">sms</span> <span class="post-tag tag-link-logs" rel="tag" title="see questions tagged &#39;logs&#39;">logs</span> <span class="post-tag tag-link-mms" rel="tag" title="see questions tagged &#39;mms&#39;">mms</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '13, 00:28</strong></p><img src="https://secure.gravatar.com/avatar/3fe4a03295800ce5f5b9d962fa5d1766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Metalica%20Loopie3&#39;s gravatar image" /><p><span>Metalica Loo...</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Metalica Loopie3 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '14, 22:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-27847" class="comments-container"></div><div id="comment-tools-27847" class="comment-tools"></div><div class="clear"></div><div id="comment-27847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27848"></span>

<div id="answer-container-27848" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27848-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27848-score" class="post-score" title="current number of votes">1</div><span id="post-27848-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I doubt anyone will have those kind of traces for you as they are what I would call a perfect example for sensitive packets that can't be shared for privacy reasons. Unless someone is running a Lab environment and has non critical packets in their traces, but in that case I guess location updates will still not work.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Dec '13, 01:01</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27848" class="comments-container"><span id="27850"></span><div id="comment-27850" class="comment"><div id="post-27850-score" class="comment-score"></div><div class="comment-text"><p>Hi Jasper,</p><p>Thanks for your response. But i need a successful trace of sms/mms so that i could understand some facts in that. Can anyone take trace for this as test SMS.</p><p>It will be appriciated !!!</p><p>Regards Manish</p></div><div id="comment-27850-info" class="comment-info"><span class="comment-age">(06 Dec '13, 01:07)</span> <span class="comment-user userinfo">Metalica Loo...</span></div></div><span id="27882"></span><div id="comment-27882" class="comment"><div id="post-27882-score" class="comment-score"></div><div class="comment-text"><p>I've been attempting to get these kinds of educational callflows in shareable capture files. I have been pretty unsuccessful so far. I'll make a note of this question though in case I can get it for you in the near-future.</p></div><div id="comment-27882-info" class="comment-info"><span class="comment-age">(06 Dec '13, 17:20)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-27848" class="comment-tools"></div><div class="clear"></div><div id="comment-27848-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

