+++
type = "question"
title = "How to filter ip address within TZSP"
description = '''Dear all, how can I filter src/dst ip address encapsulated within TZSP? I tried righ click on the field and then &quot;apply as a filter&quot; but it filter TZSP ip address, not TZSP payload. Or is it possible to remove TZSP encapsulation? Best regards, Alberto'''
date = "2013-07-30T13:00:00Z"
lastmod = "2013-07-30T13:00:00Z"
weight = 23452
keywords = [ "tzsp" ]
aliases = [ "/questions/23452" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to filter ip address within TZSP](/questions/23452/how-to-filter-ip-address-within-tzsp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23452-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23452-score" class="post-score" title="current number of votes">0</div><span id="post-23452-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all,</p><p>how can I filter src/dst ip address encapsulated within TZSP? I tried righ click on the field and then "apply as a filter" but it filter TZSP ip address, not TZSP payload.</p><p>Or is it possible to remove TZSP encapsulation?</p><p>Best regards, Alberto</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tzsp" rel="tag" title="see questions tagged &#39;tzsp&#39;">tzsp</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '13, 13:00</strong></p><img src="https://secure.gravatar.com/avatar/e6aacf7edf3b81e52b6a64aa077b7027?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nabas&#39;s gravatar image" /><p><span>nabas</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nabas has no accepted answers">0%</span></p></div></div><div id="comments-container-23452" class="comments-container"></div><div id="comment-tools-23452" class="comment-tools"></div><div class="clear"></div><div id="comment-23452-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

