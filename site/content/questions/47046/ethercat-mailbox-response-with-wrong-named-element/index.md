+++
type = "question"
title = "EtherCAT mailbox response with wrong named element?"
description = '''Hi, wireshark (V1.12.4) is showing 2 consecutive bytes named &quot;SubIdx&quot; in a SDO info response (OpCode = 0x6). But I think the second one should be the &quot;ValueInfo&quot; instead. Can you confirm that? Any workaround for this to show it in the right way?'''
date = "2015-10-29T00:56:00Z"
lastmod = "2015-10-29T02:34:00Z"
weight = 47046
keywords = [ "maibox", "sdo", "ethercat" ]
aliases = [ "/questions/47046" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [EtherCAT mailbox response with wrong named element?](/questions/47046/ethercat-mailbox-response-with-wrong-named-element)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47046-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47046-score" class="post-score" title="current number of votes">0</div><span id="post-47046-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, wireshark (V1.12.4) is showing 2 consecutive bytes named "SubIdx" in a SDO info response (OpCode = 0x6). But I think the second one should be the "ValueInfo" instead. Can you confirm that? Any workaround for this to show it in the right way?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-maibox" rel="tag" title="see questions tagged &#39;maibox&#39;">maibox</span> <span class="post-tag tag-link-sdo" rel="tag" title="see questions tagged &#39;sdo&#39;">sdo</span> <span class="post-tag tag-link-ethercat" rel="tag" title="see questions tagged &#39;ethercat&#39;">ethercat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '15, 00:56</strong></p><img src="https://secure.gravatar.com/avatar/dcd46f3743b55a8df576f41d4c27b1b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ThoKu&#39;s gravatar image" /><p><span>ThoKu</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ThoKu has no accepted answers">0%</span></p></div></div><div id="comments-container-47046" class="comments-container"></div><div id="comment-tools-47046" class="comment-tools"></div><div class="clear"></div><div id="comment-47046-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47049"></span>

<div id="answer-container-47049" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47049-score" class="post-score" title="current number of votes">0</div><span id="post-47049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If this is in error please file a bug in the <a href="https://bugs.wireshark.org">bugzilla bug database</a>, providing as much info and if possible a sample capture file as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '15, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-47049" class="comments-container"></div><div id="comment-tools-47049" class="comment-tools"></div><div class="clear"></div><div id="comment-47049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

