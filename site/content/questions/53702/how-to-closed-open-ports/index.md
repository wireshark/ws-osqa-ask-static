+++
type = "question"
title = "[closed] how to closed open ports"
description = '''how do I close an opened port and decide which ones to close. Also how to i open know which ports to open on a LAMP server to function properly?'''
date = "2016-06-28T14:10:00Z"
lastmod = "2016-06-28T14:10:00Z"
weight = 53702
keywords = [ "ports" ]
aliases = [ "/questions/53702" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] how to closed open ports](/questions/53702/how-to-closed-open-ports)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53702-score" class="post-score" title="current number of votes">0</div><span id="post-53702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do I close an opened port and decide which ones to close. Also how to i open know which ports to open on a LAMP server to function properly?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ports" rel="tag" title="see questions tagged &#39;ports&#39;">ports</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '16, 14:10</strong></p><img src="https://secure.gravatar.com/avatar/bb6e1d58c3d206c2d5dd48edc14f598e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="willyboi92&#39;s gravatar image" /><p><span>willyboi92</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="willyboi92 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>28 Jun '16, 16:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-53702" class="comments-container"></div><div id="comment-tools-53702" class="comment-tools"></div><div class="clear"></div><div id="comment-53702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by JeffMorriss 28 Jun '16, 16:56

</div>

</div>

</div>

