+++
type = "question"
title = "does it works for mobile apps"
description = '''This tool works for web apps. But does it works for mobile apps. Can it capture the packets, perform sniffing etc for mobile apps. Consider a scenario where user logs in to a mobile app and perform some action. The request-response, proxies intercepting would work here and how?'''
date = "2014-06-18T23:06:00Z"
lastmod = "2014-06-20T17:34:00Z"
weight = 33949
keywords = [ "security", "testing" ]
aliases = [ "/questions/33949" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [does it works for mobile apps](/questions/33949/does-it-works-for-mobile-apps)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33949-score" class="post-score" title="current number of votes">0</div><span id="post-33949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This tool works for web apps. But does it works for mobile apps. Can it capture the packets, perform sniffing etc for mobile apps. Consider a scenario where user logs in to a mobile app and perform some action. The request-response, proxies intercepting would work here and how?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-testing" rel="tag" title="see questions tagged &#39;testing&#39;">testing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '14, 23:06</strong></p><img src="https://secure.gravatar.com/avatar/54a87865c7482d3576309e7dc746fd51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tanu&#39;s gravatar image" /><p><span>tanu</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tanu has no accepted answers">0%</span></p></div></div><div id="comments-container-33949" class="comments-container"></div><div id="comment-tools-33949" class="comment-tools"></div><div class="clear"></div><div id="comment-33949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34006"></span>

<div id="answer-container-34006" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34006-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34006-score" class="post-score" title="current number of votes">0</div><span id="post-34006-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Either the mobile app sends and receives network traffic, in which case Wireshark can capture that traffic to the same extent that it can capture traffic sent to and received by a browser (which is what would run your Web app), or it doesn't send or receive network traffic (for example, a camera app that just stores pictures on the machine, rather than uploading them to some site), in which case, obviously, there's nothing Wireshark can do.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jun '14, 17:34</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-34006" class="comments-container"></div><div id="comment-tools-34006" class="comment-tools"></div><div class="clear"></div><div id="comment-34006-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

