+++
type = "question"
title = "Win 7 - No capture - Internet Mobile plugged on USB port"
description = '''I just installed wireshark. Purpose: analyzing all packets to and from the internet during a given session. Any reason why it does NOT see the interface? OR: what is the additional add-on we need? Thanks '''
date = "2010-12-06T11:53:00Z"
lastmod = "2010-12-07T01:38:00Z"
weight = 1261
keywords = [ "windows7", "usb" ]
aliases = [ "/questions/1261" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Win 7 - No capture - Internet Mobile plugged on USB port](/questions/1261/win-7-no-capture-internet-mobile-plugged-on-usb-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1261-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1261-score" class="post-score" title="current number of votes">0</div><span id="post-1261-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed wireshark. Purpose: analyzing all packets to and from the internet during a given session. Any reason why it does NOT see the interface? OR: what is the additional add-on we need? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '10, 11:53</strong></p><img src="https://secure.gravatar.com/avatar/4b79d128521b64b6466488bbd6f77de1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jeanray&#39;s gravatar image" /><p><span>jeanray</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jeanray has no accepted answers">0%</span></p></div></div><div id="comments-container-1261" class="comments-container"><span id="1269"></span><div id="comment-1269" class="comment"><div id="post-1269-score" class="comment-score"></div><div class="comment-text"><p>The FAQs do not answer the question: there is no mention of the USB port, not of internet mobile devices. Indeed, the rest of the interfaces ARE visible and active... We need to be able to capture from every possible source of internet connection.</p></div><div id="comment-1269-info" class="comment-info"><span class="comment-age">(07 Dec '10, 01:38)</span> <span class="comment-user userinfo">jeanray</span></div></div></div><div id="comment-tools-1261" class="comment-tools"></div><div class="clear"></div><div id="comment-1261-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1262"></span>

<div id="answer-container-1262" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1262-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1262-score" class="post-score" title="current number of votes">0</div><span id="post-1262-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might take a look at the following URL--</p><p><a href="http://www.wireshark.org/faq.html#q8.1">http://www.wireshark.org/faq.html#q8.1</a></p><p>I have not personally tried this on windows, but I'd make sure that I have the latest version and install winpcap as part of the installation. When you launch, if Wireshark is invoking winpcap, it might be necessary to right click and choose "run as administrator". Again, I've not had personal experience with Wireshark on Windows 7, but I think these are a few good points to start with.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Dec '10, 13:20</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span></p></div></div><div id="comments-container-1262" class="comments-container"><span id="1263"></span><div id="comment-1263" class="comment"><div id="post-1263-score" class="comment-score"></div><div class="comment-text"><p>Also, is this like a 3G card? And can you see the rest of the interfaces if they are active? The answer to this might rule out all of the suggestion I have given.</p></div><div id="comment-1263-info" class="comment-info"><span class="comment-age">(06 Dec '10, 15:14)</span> <span class="comment-user userinfo">Paul Stewart</span></div></div></div><div id="comment-tools-1262" class="comment-tools"></div><div class="clear"></div><div id="comment-1262-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

