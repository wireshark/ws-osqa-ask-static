+++
type = "question"
title = "Understanding &#x27;URB_INTERRUPT in&#x27;"
description = '''I have a development board which, when I plug into the USB, appears on the PC as a virtual com port as expected. However most serial communication applications i try fail to communicate (one very old application does work). Digging down the Windows Serial API reports that the connected device is not...'''
date = "2017-02-13T03:37:00Z"
lastmod = "2017-02-13T03:37:00Z"
weight = 59363
keywords = [ "serial", "urb_interrupt", "usb" ]
aliases = [ "/questions/59363" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Understanding 'URB\_INTERRUPT in'](/questions/59363/understanding-urb_interrupt-in)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59363-score" class="post-score" title="current number of votes">0</div><span id="post-59363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a development board which, when I plug into the USB, appears on the PC as a virtual com port as expected. However most serial communication applications i try fail to communicate (one very old application does work).</p><p>Digging down the Windows Serial API reports that the connected device is not functioning.</p><p>Using Wireshark to monitor the port I continusly seeing the message: 6905 308.984603 2.4.1 host USB 31 URB_INTERRUPT in</p><p>What does this 'URB_INTERRUPT in' mean? Could this be the cause of my comms issue?</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-serial" rel="tag" title="see questions tagged &#39;serial&#39;">serial</span> <span class="post-tag tag-link-urb_interrupt" rel="tag" title="see questions tagged &#39;urb_interrupt&#39;">urb_interrupt</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '17, 03:37</strong></p><img src="https://secure.gravatar.com/avatar/7ef0ecce6a31da72a237532a86fe20de?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TheGrovesy&#39;s gravatar image" /><p><span>TheGrovesy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TheGrovesy has no accepted answers">0%</span></p></div></div><div id="comments-container-59363" class="comments-container"></div><div id="comment-tools-59363" class="comment-tools"></div><div class="clear"></div><div id="comment-59363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

