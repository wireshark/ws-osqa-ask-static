+++
type = "question"
title = "How to Capture Vlan tag on Thinkpad T440p on Windows 7"
description = '''My laptop is Thinkpad T440p, the NIC is Intel(R) Ethernet Connection I217-LM. And my windows operating system is windows 7. How to Capture Vlan tag.  I have set the monitormode =1 and monitormodeenabled=1 under the regedit ControlSet001. But it still not work.'''
date = "2015-08-29T13:51:00Z"
lastmod = "2015-09-01T12:28:00Z"
weight = 45516
keywords = [ "capture", "windows7", "vlan", "tag" ]
aliases = [ "/questions/45516" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to Capture Vlan tag on Thinkpad T440p on Windows 7](/questions/45516/how-to-capture-vlan-tag-on-thinkpad-t440p-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45516-score" class="post-score" title="current number of votes">0</div><span id="post-45516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My laptop is Thinkpad T440p, the NIC is Intel(R) Ethernet Connection I217-LM. And my windows operating system is windows 7.<br />
How to Capture Vlan tag. I have set the monitormode =1 and monitormodeenabled=1 under the regedit ControlSet001.</p><p>But it still not work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '15, 13:51</strong></p><img src="https://secure.gravatar.com/avatar/385c7038cc83a66f008dd17e2b46192a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Caiser&#39;s gravatar image" /><p><span>Caiser</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Caiser has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Aug '15, 17:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-45516" class="comments-container"><span id="45519"></span><div id="comment-45519" class="comment"><div id="post-45519-score" class="comment-score"></div><div class="comment-text"><p>HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Class{4D36E972-E325-11CE-BFC1-08002BE10318} Have you tried CurrentControlSet instead of ControlSet001 ?</p></div><div id="comment-45519-info" class="comment-info"><span class="comment-age">(30 Aug '15, 01:16)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-45516" class="comment-tools"></div><div class="clear"></div><div id="comment-45516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="45546"></span>

<div id="answer-container-45546" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45546-score" class="post-score" title="current number of votes">0</div><span id="post-45546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you trying to capture traffic generated from the Thinkpad T440P laptop and expecting to see VLAN tags?</p><p>I ask this question because the VLAN is not added by the laptop. The VLAN tag is added by the switch when the traffic enters the network (ingress) and then the switch removes the VLAN when the traffic exists the network (egress).</p><p>To examine the VLAN tagged traffic, you could use the laptop to monitor other traffic on your network (i.e., traffic not being generated by the laptop). In this case, you might need to configure the switch to "pass along" the VLAN information. For example, Cisco switches require the following command to be entered in privileged exec mode:</p><p>monitor session 1 destination int gi1/0/16 encapsulation dot1q</p><p>Noticed the "encapsulation dot1q" at the end. This informs the switch to pass the VLAN tags to the destination monitor port.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '15, 07:50</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-45546" class="comments-container"></div><div id="comment-tools-45546" class="comment-tools"></div><div class="clear"></div><div id="comment-45546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="45576"></span>

<div id="answer-container-45576" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45576-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45576-score" class="post-score" title="current number of votes">0</div><span id="post-45576-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thanks when I turn off the firewall, I can capture the pakcets with VLAN TAG</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Sep '15, 12:28</strong></p><img src="https://secure.gravatar.com/avatar/385c7038cc83a66f008dd17e2b46192a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Caiser&#39;s gravatar image" /><p><span>Caiser</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Caiser has no accepted answers">0%</span></p></div></div><div id="comments-container-45576" class="comments-container"></div><div id="comment-tools-45576" class="comment-tools"></div><div class="clear"></div><div id="comment-45576-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

