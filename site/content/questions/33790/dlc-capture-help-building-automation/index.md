+++
type = "question"
title = "DLC Capture Help - Building Automation"
description = '''Dear Sir or Madame, We are using wire shark through a dlccapture utility to capture communications through a com port.  Interface:&#92;.&#92;pipe&#92;wireshark_pipe When i run my utility i specify the com port, baud rate, and type of network being arc156 (building automation network). And i guess the utility ge...'''
date = "2014-06-13T11:49:00Z"
lastmod = "2014-06-15T08:07:00Z"
weight = 33790
keywords = [ "timestamp", "automatedlogic", "time" ]
aliases = [ "/questions/33790" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [DLC Capture Help - Building Automation](/questions/33790/dlc-capture-help-building-automation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33790-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33790-score" class="post-score" title="current number of votes">0</div><span id="post-33790-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sir or Madame,</p><p>We are using wire shark through a dlccapture utility to capture communications through a com port. Interface:\.\pipe\wireshark_pipe</p><p>When i run my utility i specify the com port, baud rate, and type of network being arc156 (building automation network). And i guess the utility generates a .bat file and runs wire shark to capture. I see the values that are requested and received on the network but it seems that the time stamp (first and last packet) only show around 12 minutes when we have been running it for hours. Common sense tells you larger file size over time period means more than 12 minutes.</p><p>Anyone has any ideas or help. It is a Automated Logic Router that we are monitoring.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span> <span class="post-tag tag-link-automatedlogic" rel="tag" title="see questions tagged &#39;automatedlogic&#39;">automatedlogic</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '14, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/5594542abcc622711dcbf370f511cb28?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="am9355&#39;s gravatar image" /><p><span>am9355</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="am9355 has no accepted answers">0%</span></p></div></div><div id="comments-container-33790" class="comments-container"></div><div id="comment-tools-33790" class="comment-tools"></div><div class="clear"></div><div id="comment-33790-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33826"></span>

<div id="answer-container-33826" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33826-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33826-score" class="post-score" title="current number of votes">0</div><span id="post-33826-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>but it seems that the time stamp (first and last packet) only show around 12 minutes when we have been running it for hours.</p></blockquote><p>can you post a sample capture file somewhere (google drive, dropbox, cloudshark.org)?</p><blockquote><p>Common sense tells you larger file size over time period means more than 12 minutes.</p></blockquote><p>Maybe the 'dlccapture utility' stopped capturing after some time. Can you post a link to that tool (if its available publicly)?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jun '14, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-33826" class="comments-container"></div><div id="comment-tools-33826" class="comment-tools"></div><div class="clear"></div><div id="comment-33826-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

