+++
type = "question"
title = "Why does my PC ping my router?"
description = '''I noticed that my PC sometimes pings my wireless router and my router sends back its echo reply. does anyone know why it would randomly do this?'''
date = "2011-07-06T16:09:00Z"
lastmod = "2011-07-07T09:05:00Z"
weight = 4934
keywords = [ "ping" ]
aliases = [ "/questions/4934" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why does my PC ping my router?](/questions/4934/why-does-my-pc-ping-my-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4934-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4934-score" class="post-score" title="current number of votes">0</div><span id="post-4934-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I noticed that my PC sometimes pings my wireless router and my router sends back its echo reply. does anyone know why it would randomly do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ping" rel="tag" title="see questions tagged &#39;ping&#39;">ping</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '11, 16:09</strong></p><img src="https://secure.gravatar.com/avatar/eea102b5f3fbf3263987c02e49483ecf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="greenbear&#39;s gravatar image" /><p><span>greenbear</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="greenbear has no accepted answers">0%</span></p></div></div><div id="comments-container-4934" class="comments-container"></div><div id="comment-tools-4934" class="comment-tools"></div><div class="clear"></div><div id="comment-4934-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4936"></span>

<div id="answer-container-4936" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4936-score" class="post-score" title="current number of votes">1</div><span id="post-4936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The OS running on your PC might be testing your network connectivity by checking whether the router is up. I think Windows XP might have added some sort of connectivity-testing mechanism such as that, so if your PC is running Windows, that might be it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jul '11, 16:29</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-4936" class="comments-container"><span id="4942"></span><div id="comment-4942" class="comment"><div id="post-4942-score" class="comment-score"></div><div class="comment-text"><p>I agree. Windows offers a network connectivity diagram for troubleshooting reasons and for this it needs to check if the default gw is working. This probably shows up as the ping packets.</p></div><div id="comment-4942-info" class="comment-info"><span class="comment-age">(07 Jul '11, 09:05)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-4936" class="comment-tools"></div><div class="clear"></div><div id="comment-4936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

