+++
type = "question"
title = "Free Windows based tool - SPAN port configurator"
description = '''Hi All, We here at www.netfort.com are about to release a new free tool which allows you to configure SPAN ports on Cisco switches. Even though its not a complicated task in the first place, the SPAN port configurator makes it really easy to setup SPAN ports with multiple vlan or port sources.  It w...'''
date = "2012-09-14T05:21:00Z"
lastmod = "2012-10-05T11:36:00Z"
weight = 14266
keywords = [ "cisco", "span", "mirroring" ]
aliases = [ "/questions/14266" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Free Windows based tool - SPAN port configurator](/questions/14266/free-windows-based-tool-span-port-configurator)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14266-score" class="post-score" title="current number of votes">0</div><span id="post-14266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, We here at <a href="http://www.netfort.com">www.netfort.com</a> are about to release a new free tool which allows you to configure SPAN ports on Cisco switches. Even though its not a complicated task in the first place, the SPAN port configurator makes it really easy to setup SPAN ports with multiple vlan or port sources.</p><p>It will be a week or two before its available for download on our site but if any of you want a copy please email me at darragh.delaney[at]<a href="http://netfort.com">netfort.com</a></p><p>Darragh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-span" rel="tag" title="see questions tagged &#39;span&#39;">span</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '12, 05:21</strong></p><img src="https://secure.gravatar.com/avatar/2c69ec3c6c8db9bc6295ae0f844ec529?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="darraghdelaney&#39;s gravatar image" /><p><span>darraghdelaney</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="darraghdelaney has no accepted answers">0%</span></p></div></div><div id="comments-container-14266" class="comments-container"><span id="14737"></span><div id="comment-14737" class="comment"><div id="post-14737-score" class="comment-score"></div><div class="comment-text"><p>You should probably add this to the <a href="http://wiki.wireshark.org/Tools">Tools page</a> and/or the <a href="http://wiki.wireshark.org/SwitchReference/CiscoSystems">Cisco switch page</a> of the <a href="http://wiki.wireshark.org/FrontPage">Wireshark Wiki</a>.</p></div><div id="comment-14737-info" class="comment-info"><span class="comment-age">(05 Oct '12, 11:36)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-14266" class="comment-tools"></div><div class="clear"></div><div id="comment-14266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14731"></span>

<div id="answer-container-14731" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14731-score" class="post-score" title="current number of votes">0</div><span id="post-14731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This application is now available to download for free from our website. <a href="http://www.netfort.com/downloads/free-software">http://www.netfort.com/downloads/free-software</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '12, 05:13</strong></p><img src="https://secure.gravatar.com/avatar/2c69ec3c6c8db9bc6295ae0f844ec529?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="darraghdelaney&#39;s gravatar image" /><p><span>darraghdelaney</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="darraghdelaney has no accepted answers">0%</span></p></div></div><div id="comments-container-14731" class="comments-container"></div><div id="comment-tools-14731" class="comment-tools"></div><div class="clear"></div><div id="comment-14731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

