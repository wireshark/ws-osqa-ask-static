+++
type = "question"
title = "[closed] How to use firesheep"
description = '''Hello, I would like to know how to use firesheep because i´m having problems using it. I&#x27;m running Mozilla Firefox 10.0.1, I have a 64-bit operating system, Windows 7. When I try to install it, it says that the firesheep isn&#x27;t compatible. What should I do? Thanks'''
date = "2012-02-15T08:51:00Z"
lastmod = "2012-02-15T09:48:00Z"
weight = 9029
keywords = [ "firesheep" ]
aliases = [ "/questions/9029" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to use firesheep](/questions/9029/how-to-use-firesheep)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9029-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9029-score" class="post-score" title="current number of votes">-3</div><span id="post-9029-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I would like to know how to use firesheep because i´m having problems using it. I'm running Mozilla Firefox 10.0.1, I have a 64-bit operating system, Windows 7. When I try to install it, it says that the firesheep isn't compatible. What should I do?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firesheep" rel="tag" title="see questions tagged &#39;firesheep&#39;">firesheep</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '12, 08:51</strong></p><img src="https://secure.gravatar.com/avatar/35b57e16729538c288302208b47e8b5f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="devilsk13&#39;s gravatar image" /><p><span>devilsk13</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="devilsk13 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>15 Feb '12, 09:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-9029" class="comments-container"><span id="9030"></span><div id="comment-9030" class="comment"><div id="post-9030-score" class="comment-score"></div><div class="comment-text"><p>Off topic!</p></div><div id="comment-9030-info" class="comment-info"><span class="comment-age">(15 Feb '12, 08:56)</span> <span class="comment-user userinfo">bstn</span></div></div><span id="9031"></span><div id="comment-9031" class="comment"><div id="post-9031-score" class="comment-score"></div><div class="comment-text"><p>I know but I think you could still anwser it.</p></div><div id="comment-9031-info" class="comment-info"><span class="comment-age">(15 Feb '12, 08:58)</span> <span class="comment-user userinfo">devilsk13</span></div></div><span id="9039"></span><div id="comment-9039" class="comment"><div id="post-9039-score" class="comment-score"></div><div class="comment-text"><p>I am sure there are Firesheep tutorials or usergroups somewhere. Questions here are expected to pertain to Wireshark in some way.</p></div><div id="comment-9039-info" class="comment-info"><span class="comment-age">(15 Feb '12, 09:48)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-9029" class="comment-tools"></div><div class="clear"></div><div id="comment-9029-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by multipleinterfaces 15 Feb '12, 09:48

</div>

</div>

</div>

