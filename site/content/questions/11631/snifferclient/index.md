+++
type = "question"
title = "Snifferclient?"
description = '''Watching some traffic on a host via logmein I see traffic that starts with this https &amp;gt; snifferclient what does that mean? google did not help much.'''
date = "2012-06-04T08:54:00Z"
lastmod = "2012-06-04T09:00:00Z"
weight = 11631
keywords = [ "general" ]
aliases = [ "/questions/11631" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Snifferclient?](/questions/11631/snifferclient)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11631-score" class="post-score" title="current number of votes">0</div><span id="post-11631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Watching some traffic on a host via logmein I see traffic that starts with this</p><p>https &gt; snifferclient</p><p>what does that mean? google did not help much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-general" rel="tag" title="see questions tagged &#39;general&#39;">general</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '12, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/2b1d4ebc68ff2908f81bbfd9a43aaf78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pluribus&#39;s gravatar image" /><p><span>pluribus</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pluribus has no accepted answers">0%</span></p></div></div><div id="comments-container-11631" class="comments-container"></div><div id="comment-tools-11631" class="comment-tools"></div><div class="clear"></div><div id="comment-11631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11633"></span>

<div id="answer-container-11633" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11633-score" class="post-score" title="current number of votes">1</div><span id="post-11633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's (most certainly) just the source port of your connection, resolved to 'snifferclient' (TCP/2452).</p><p>Disable name resolution and you will see the numbers instead of the names.</p><blockquote><p><code>Edit -&gt; Preferences -&gt; Name Resolution -&gt; Enable transport name resolution</code></p></blockquote><p>Uncheck that option.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '12, 09:00</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jun '12, 09:01</strong> </span></p></div></div><div id="comments-container-11633" class="comments-container"></div><div id="comment-tools-11633" class="comment-tools"></div><div class="clear"></div><div id="comment-11633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

