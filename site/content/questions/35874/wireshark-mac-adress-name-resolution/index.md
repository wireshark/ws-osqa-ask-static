+++
type = "question"
title = "Wireshark MAC-Adress name resolution"
description = '''Hello, i have been updated wireshark from 1.10 to 1.12.0. All its fine, but i have a problem. All captures in the list are not &quot;name resolution&quot;. In the interface options is mac-name resolution on and in the Preference too. It is a bug? I hope you can help me. :) best regards Daniel'''
date = "2014-08-29T08:19:00Z"
lastmod = "2014-08-29T11:27:00Z"
weight = 35874
keywords = [ "adresse", "mac", "resolution", "name" ]
aliases = [ "/questions/35874" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark MAC-Adress name resolution](/questions/35874/wireshark-mac-adress-name-resolution)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35874-score" class="post-score" title="current number of votes">0</div><span id="post-35874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>i have been updated wireshark from 1.10 to 1.12.0. All its fine, but i have a problem.</p><p>All captures in the list are not "name resolution". In the interface options is mac-name resolution on and in the Preference too.</p><p>It is a bug?</p><p>I hope you can help me. :)</p><p>best regards Daniel</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adresse" rel="tag" title="see questions tagged &#39;adresse&#39;">adresse</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-resolution" rel="tag" title="see questions tagged &#39;resolution&#39;">resolution</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '14, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/480dbbf5b367a6f20e37d802468280a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="daniel92&#39;s gravatar image" /><p><span>daniel92</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="daniel92 has no accepted answers">0%</span></p></div></div><div id="comments-container-35874" class="comments-container"></div><div id="comment-tools-35874" class="comment-tools"></div><div class="clear"></div><div id="comment-35874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35875"></span>

<div id="answer-container-35875" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35875-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35875-score" class="post-score" title="current number of votes">1</div><span id="post-35875-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="daniel92 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10344">bug 10344</a> that is fixed for the upcoming 1.12.1 release.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Aug '14, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-35875" class="comments-container"><span id="35876"></span><div id="comment-35876" class="comment"><div id="post-35876-score" class="comment-score"></div><div class="comment-text"><p>Thank you, did you know when the new version is coming?</p></div><div id="comment-35876-info" class="comment-info"><span class="comment-age">(29 Aug '14, 08:43)</span> <span class="comment-user userinfo">daniel92</span></div></div><span id="35881"></span><div id="comment-35881" class="comment"><div id="post-35881-score" class="comment-score"></div><div class="comment-text"><p>There is no <a href="http://wiki.wireshark.org/Development/Roadmap">planned release date</a> at this time.</p></div><div id="comment-35881-info" class="comment-info"><span class="comment-age">(29 Aug '14, 10:47)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="35882"></span><div id="comment-35882" class="comment"><div id="post-35882-score" class="comment-score"></div><div class="comment-text"><p>My observation too - OS X 10.8.5 on MBP - sometimes it works, sometimes it doesn't - glad to hear it's getting fixed. Hopefully, also the MIB problem "Stopped processing module RFC1213-MIB due to error(s) to prevent potential crash in libsmi. Module's conformance level: 1. See details at: <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560325">http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560325"</a> referenced in my other post.</p></div><div id="comment-35882-info" class="comment-info"><span class="comment-age">(29 Aug '14, 11:27)</span> <span class="comment-user userinfo">packetlevel</span></div></div></div><div id="comment-tools-35875" class="comment-tools"></div><div class="clear"></div><div id="comment-35875-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

