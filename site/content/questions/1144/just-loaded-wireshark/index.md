+++
type = "question"
title = "just loaded wireshark"
description = '''i just loaded wireshark, and have problems using it. when i launch the program, it opens too high on the screen. the title bar, and the top 90% of the window is above my screen. if i try to use &#x27;move&#x27;, &#x27;restore&#x27; or maximize they don&#x27;t work. i couldn&#x27;t find a .ini file, and struck out looking in the ...'''
date = "2010-11-28T19:28:00Z"
lastmod = "2010-11-30T17:24:00Z"
weight = 1144
keywords = [ "start", "window", "position" ]
aliases = [ "/questions/1144" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [just loaded wireshark](/questions/1144/just-loaded-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1144-score" class="post-score" title="current number of votes">0</div><span id="post-1144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i just loaded wireshark, and have problems using it.</p><p>when i launch the program, it opens too high on the screen. the title bar, and the top 90% of the window is above my screen. if i try to use 'move', 'restore' or maximize they don't work. i couldn't find a .ini file, and struck out looking in the registry.</p><p>how do i move the window so i can use it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-start" rel="tag" title="see questions tagged &#39;start&#39;">start</span> <span class="post-tag tag-link-window" rel="tag" title="see questions tagged &#39;window&#39;">window</span> <span class="post-tag tag-link-position" rel="tag" title="see questions tagged &#39;position&#39;">position</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Nov '10, 19:28</strong></p><img src="https://secure.gravatar.com/avatar/1505e0257ac158a9f180544942cd8f6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="peakay22&#39;s gravatar image" /><p><span>peakay22</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="peakay22 has no accepted answers">0%</span></p></div></div><div id="comments-container-1144" class="comments-container"></div><div id="comment-tools-1144" class="comment-tools"></div><div class="clear"></div><div id="comment-1144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="1156"></span>

<div id="answer-container-1156" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1156-score" class="post-score" title="current number of votes">0</div><span id="post-1156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try this:</p><ul><li>right click on the Wireshark entry in the task bar to open the context menu</li><li>select "move" but don't move the mouse yet</li><li>press any of the cursor keys, which should move the window pixel by pixel</li><li>as soon as the window starts to move you can use the mouse to move it faster; the window will stay attached to it until you click it off</li></ul><p>Hope this helps.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Nov '10, 09:14</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-1156" class="comments-container"></div><div id="comment-tools-1156" class="comment-tools"></div><div class="clear"></div><div id="comment-1156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1164"></span>

<div id="answer-container-1164" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1164-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1164-score" class="post-score" title="current number of votes">0</div><span id="post-1164-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Believe it or not, when I taught classes in NT (not so much for NetWare), one of the things I used to make my students do was to navigate Windows w/o a mouse. KVM back in the days had a way of losing signals!<br />
</p><p>To move any active pane (visible or not), highlight the application in the task bar so it has the focus. Then hit ALT-SPACE Bar, then hit the letter "m" Alt-spacebar brings up the "move, minimize" dialog and "m" tells Windows you want to move/relocate the application.</p><p>Then just use your directional arrow keys to bring it all the way to the bottom and to the left. And you're all set!</p><p>This is the same reason why every Unix admin should know "vi" When you bring up a system from scratch (or after a crash), there's no emacs or anything other than "vi" 'course, it's been ages since I had to actually build a Unix system, so maybe things have changed.</p><p>Back in the days, I swore nothing was better than EDT editor on VAX I had my Procomm mapped out for like you wouldn't believe!!! Then I saw "vi" in action and fell in love. Now, while I still like vi, "Ultraedit" is pretty damn powerful.</p><p>One tidbit of info I learned from the Google guys at Sharkfest. gmail takes "vi" short cuts. So the navagational and other command keys work when using gmail. That I thought was pretty neat! :)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Nov '10, 14:01</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span> </br></p></div></div><div id="comments-container-1164" class="comments-container"></div><div id="comment-tools-1164" class="comment-tools"></div><div class="clear"></div><div id="comment-1164-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1181"></span>

<div id="answer-container-1181" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1181-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1181-score" class="post-score" title="current number of votes">0</div><span id="post-1181-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>thanks to both jasper, and hansangb. that was just the ticket.</p><p>the system was set up as a dual monitor system with only one monitor attached. monitor 2 was configured as 640x480, and the wireshark window opens at 800x600. so naturally when i launched wireshark it tries to open in monitor 2, so all i saw was the bottom of the wireshark window overlapping onto monitor 1.</p><p>anyways, the arrow keys worked fine.</p><p>i'm a hardware guy working on MRI systems that needs to know the software to navigate my systems. the systems have been VMS and Open VMS, and the workstations have always been some flavor of unix. i have been playing with those two operating systems for over 20 years. now everything is windows XP.</p><p>i know what i know because i can read 'help pages' and 'man pages'. learned in piecemeal fashion i became somewhat proficient in VMS and Unix.</p><p>will someone please tell bill gates to stop making windows "easier"?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '10, 07:23</strong></p><img src="https://secure.gravatar.com/avatar/1505e0257ac158a9f180544942cd8f6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="peakay22&#39;s gravatar image" /><p><span>peakay22</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="peakay22 has no accepted answers">0%</span></p></div></div><div id="comments-container-1181" class="comments-container"><span id="1184"></span><div id="comment-1184" class="comment"><div id="post-1184-score" class="comment-score"></div><div class="comment-text"><p>To be fair, Window 7 is actually really really easy to use. I must say, it's the first OS that makes my daily life easier.</p></div><div id="comment-1184-info" class="comment-info"><span class="comment-age">(30 Nov '10, 17:24)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-1181" class="comment-tools"></div><div class="clear"></div><div id="comment-1181-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

