+++
type = "question"
title = "Not seeing any traffic on Windows on my Broadcom 4321ag adapter"
description = '''I have a broadcom 4321 ag adaptor and I see the interface microsoft but when I start the capture nothings comes up ?'''
date = "2010-11-06T06:55:00Z"
lastmod = "2010-11-06T09:39:00Z"
weight = 836
keywords = [ "windows", "802.11" ]
aliases = [ "/questions/836" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not seeing any traffic on Windows on my Broadcom 4321ag adapter](/questions/836/not-seeing-any-traffic-on-windows-on-my-broadcom-4321ag-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-836-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-836-score" class="post-score" title="current number of votes">0</div><span id="post-836-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a broadcom 4321 ag adaptor and I see the interface microsoft but when I start the capture nothings comes up ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '10, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/4a366377a7e142b268fc1e22f90d45bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rehan&#39;s gravatar image" /><p><span>Rehan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rehan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>01 Aug '12, 15:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-836" class="comments-container"><span id="837"></span><div id="comment-837" class="comment"><div id="post-837-score" class="comment-score"></div><div class="comment-text"><p>No packets at all? You might want to try to disable "Promiscuous Mode", some Wifi drivers don't support it.</p></div><div id="comment-837-info" class="comment-info"><span class="comment-age">(06 Nov '10, 07:01)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="839"></span><div id="comment-839" class="comment"><div id="post-839-score" class="comment-score"></div><div class="comment-text"><p>tried disabling promiscuous mode but still nothing ?</p></div><div id="comment-839-info" class="comment-info"><span class="comment-age">(06 Nov '10, 09:39)</span> <span class="comment-user userinfo">Rehan</span></div></div></div><div id="comment-tools-836" class="comment-tools"></div><div class="clear"></div><div id="comment-836-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

