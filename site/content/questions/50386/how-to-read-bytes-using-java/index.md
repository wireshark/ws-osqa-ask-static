+++
type = "question"
title = "How to read bytes using Java"
description = '''I saved the incoming bytes in a .pcapng file. Now I&#x27;d like to read them using Java and try to figure out few things. Can someone suggest me how do read the file? Thanks'''
date = "2016-02-21T09:54:00Z"
lastmod = "2016-02-21T10:00:00Z"
weight = 50386
keywords = [ "read", ".pcapng", "java" ]
aliases = [ "/questions/50386" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to read bytes using Java](/questions/50386/how-to-read-bytes-using-java)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50386-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50386-score" class="post-score" title="current number of votes">0</div><span id="post-50386-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I saved the incoming bytes in a .pcapng file. Now I'd like to read them using Java and try to figure out few things. Can someone suggest me how do read the file? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-read" rel="tag" title="see questions tagged &#39;read&#39;">read</span> <span class="post-tag tag-link-.pcapng" rel="tag" title="see questions tagged &#39;.pcapng&#39;">.pcapng</span> <span class="post-tag tag-link-java" rel="tag" title="see questions tagged &#39;java&#39;">java</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '16, 09:54</strong></p><img src="https://secure.gravatar.com/avatar/f545d707fcb886878c9413198e3bca24?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Obelix&#39;s gravatar image" /><p><span>Obelix</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Obelix has no accepted answers">0%</span></p></div></div><div id="comments-container-50386" class="comments-container"></div><div id="comment-tools-50386" class="comment-tools"></div><div class="clear"></div><div id="comment-50386-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50387"></span>

<div id="answer-container-50387" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50387-score" class="post-score" title="current number of votes">0</div><span id="post-50387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is somwehat a duplicate question, see <a href="https://ask.wireshark.org/questions/29624/java-api-to-dissect-wireless-packets-captured-by-wireshark-libcap">this</a> question for some answers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Feb '16, 10:00</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50387" class="comments-container"></div><div id="comment-tools-50387" class="comment-tools"></div><div class="clear"></div><div id="comment-50387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

