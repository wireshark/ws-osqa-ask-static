+++
type = "question"
title = "tshark -o &quot;uat:... is not working for me"
description = '''I&#x27;ve got a custom wireshark dissector that is just a stats tree. I want to change a uat param when I invoke tshark. It seems like this should work, but it doesn&#x27;t: tshark -r C:&#92;Development&#92;pcap&#92;http.cap -q -o &quot;uat:num_lbg_ports:&#92;&quot;8&#92;&quot;&quot; -z ixbal,tree My preference file, named num_lbg_ports, has this c...'''
date = "2014-04-08T16:25:00Z"
lastmod = "2014-04-08T19:42:00Z"
weight = 31646
keywords = [ "-o", "tshark", "uat" ]
aliases = [ "/questions/31646" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tshark -o "uat:... is not working for me](/questions/31646/tshark-o-uat-is-not-working-for-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31646-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31646-score" class="post-score" title="current number of votes">0</div><span id="post-31646-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've got a custom wireshark dissector that is just a stats tree. I want to change a uat param when I invoke tshark. It seems like this should work, but it doesn't:</p><p>tshark -r C:\Development\pcap\http.cap -q -o "uat:num_lbg_ports:\"8\"" -z ixbal,tree</p><p>My preference file, named num_lbg_ports, has this content:</p><p># This file is automatically generated, DO NOT MODIFY.<br />
"5"</p><p>So my uat has a single param, which is a gint16, and there is one record in the table in the file. But when tshark runs, it uses the value from the file, not the value on the command line. Can anybody tell me what I'm doing wrong? BTW, the same command does work if I delete the num_lbg_ports file - but the command line argument is supposed to override the file, right? I am working on a Windows XP system, if that matters.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link--o" rel="tag" title="see questions tagged &#39;-o&#39;">-o</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-uat" rel="tag" title="see questions tagged &#39;uat&#39;">uat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '14, 16:25</strong></p><img src="https://secure.gravatar.com/avatar/eda1eef536a449723fcb504528b2fb37?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lannie&#39;s gravatar image" /><p><span>lannie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lannie has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-31646" class="comments-container"></div><div id="comment-tools-31646" class="comment-tools"></div><div class="clear"></div><div id="comment-31646-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31652"></span>

<div id="answer-container-31652" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31652-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31652-score" class="post-score" title="current number of votes">0</div><span id="post-31652-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Never mind, I figured it out. The param that is passed in the command line becomes an additional record in the uat. I simply need to make sure I always use the last record in the uat, for a param that only needs one record.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '14, 19:42</strong></p><img src="https://secure.gravatar.com/avatar/eda1eef536a449723fcb504528b2fb37?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lannie&#39;s gravatar image" /><p><span>lannie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lannie has no accepted answers">0%</span></p></div></div><div id="comments-container-31652" class="comments-container"></div><div id="comment-tools-31652" class="comment-tools"></div><div class="clear"></div><div id="comment-31652-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

