+++
type = "question"
title = "dll error when open  Wireshark"
description = '''After installing Wireshark 1.8.6.48142 and trying to launch,  I get &quot;error signature &quot;libgobject-2.0.0.dll &quot;. Re-installing the application are the same problem. how to solve this problem?? thanks~~'''
date = "2013-05-20T07:01:00Z"
lastmod = "2013-05-20T07:43:00Z"
weight = 21306
keywords = [ "dll", "error" ]
aliases = [ "/questions/21306" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [dll error when open Wireshark](/questions/21306/dll-error-when-open-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21306-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21306-score" class="post-score" title="current number of votes">0</div><span id="post-21306-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installing Wireshark 1.8.6.48142 and trying to launch, I get "error signature "libgobject-2.0.0.dll ". Re-installing the application are the same problem. how to solve this problem?? thanks~~</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dll" rel="tag" title="see questions tagged &#39;dll&#39;">dll</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '13, 07:01</strong></p><img src="https://secure.gravatar.com/avatar/a6b49b30e9f8529334e11dd6ad7e50c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HBK&#39;s gravatar image" /><p><span>HBK</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HBK has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 May '13, 10:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-21306" class="comments-container"></div><div id="comment-tools-21306" class="comment-tools"></div><div class="clear"></div><div id="comment-21306-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21311"></span>

<div id="answer-container-21311" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21311-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21311-score" class="post-score" title="current number of votes">0</div><span id="post-21311-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you downloaded the installer from <a href="http://www.wireshark.org">http://www.wireshark.org</a>, 'something' on your system might have modified that file during installation while it tried to 'clean' the file (false positive). If there is any kind of security software on your system (AV scanner, Host Intrusion, Endpoint Security, etc.), please check the logs of that software and/or disable it while you install the package.</p><p>See a similar problem here: <a href="http://ask.wireshark.org/questions/20921/not-compatible-regardless-if-i-choose-32-or-64">http://ask.wireshark.org/questions/20921/not-compatible-regardless-if-i-choose-32-or-64</a></p><p>If you downloaded Wireshark from different source, delete that file and go to wireshark.org ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '13, 07:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 May '13, 11:39</strong> </span></p></div></div><div id="comments-container-21311" class="comments-container"></div><div id="comment-tools-21311" class="comment-tools"></div><div class="clear"></div><div id="comment-21311-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

