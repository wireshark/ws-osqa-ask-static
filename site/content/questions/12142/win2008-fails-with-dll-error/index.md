+++
type = "question"
title = "Win2008 fails with .dll error"
description = '''I have recently installed Wireshark on a Windows 2008 SP2 server. The NICs are Broadcom NetExtreme II with the latest drivers. The error in the Windows application error log is:   Faulting application wireshark.exe, version 1.8.0.43431, time stamp 0x4fe369bd, faulting module libglib-2.0-0.dll, versi...'''
date = "2012-06-25T00:12:00Z"
lastmod = "2012-06-25T06:15:00Z"
weight = 12142
keywords = [ "windows", "error" ]
aliases = [ "/questions/12142" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Win2008 fails with .dll error](/questions/12142/win2008-fails-with-dll-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12142-score" class="post-score" title="current number of votes">0</div><span id="post-12142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have recently installed Wireshark on a Windows 2008 SP2 server. The NICs are Broadcom NetExtreme II with the latest drivers. The error in the Windows application error log is:</p><blockquote><p><code>Faulting application wireshark.exe, version 1.8.0.43431, time stamp 0x4fe369bd, faulting module libglib-2.0-0.dll, version 2.32.2.0, time stamp 0x4faa7bfc, exception code 0x40000015, fault offset 0x000000000004fd12, process id 0x1110, application start time 0x01cd52939171d90c.</code></p></blockquote><p>Any help would be appreciated</p><p>Pat</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jun '12, 00:12</strong></p><img src="https://secure.gravatar.com/avatar/e3ba2fc06c0bc0d8373ef1297ef6aadb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wilson&#39;s gravatar image" /><p><span>Wilson</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wilson has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jun '12, 19:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-12142" class="comments-container"></div><div id="comment-tools-12142" class="comment-tools"></div><div class="clear"></div><div id="comment-12142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12143"></span>

<div id="answer-container-12143" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12143-score" class="post-score" title="current number of votes">0</div><span id="post-12143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>see here: <a href="http://ask.wireshark.org/questions/10528/wireshark-167-crashing-in-windows-2008">http://ask.wireshark.org/questions/10528/wireshark-167-crashing-in-windows-2008</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jun '12, 00:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-12143" class="comments-container"><span id="12144"></span><div id="comment-12144" class="comment"><div id="post-12144-score" class="comment-score"></div><div class="comment-text"><p>I did go there and try that work around. It didn't help me. I wish it had.</p><p>I have it set not to show the output, create new files every 2 minutes, ring buffer set to 2.</p><p>I am working around the GUI issues by running dumpcap.</p></div><div id="comment-12144-info" class="comment-info"><span class="comment-age">(25 Jun '12, 00:32)</span> <span class="comment-user userinfo">Wilson</span></div></div><span id="12148"></span><div id="comment-12148" class="comment"><div id="post-12148-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I am working around the GUI issues by running dumpcap.</p></blockquote><p>a good sign for a memory problem :-)</p></div><div id="comment-12148-info" class="comment-info"><span class="comment-age">(25 Jun '12, 06:15)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12143" class="comment-tools"></div><div class="clear"></div><div id="comment-12143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

