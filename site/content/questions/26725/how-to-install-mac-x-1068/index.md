+++
type = "question"
title = "How to install Mac X 10.6.8"
description = '''I&#x27;m taking a college class. Have an assignment to install Wireshark and report on results as a learning experience. Have a Macbook with X 10.6.8. Tried to run the 64-bit version, and knocked out my modem. Will next try the 32-bit version, old stable version. Need some coaching so I don&#x27;t knock out t...'''
date = "2013-11-07T09:43:00Z"
lastmod = "2013-11-07T09:43:00Z"
weight = 26725
keywords = [ "macbook", "install" ]
aliases = [ "/questions/26725" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to install Mac X 10.6.8](/questions/26725/how-to-install-mac-x-1068)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26725-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26725-score" class="post-score" title="current number of votes">0</div><span id="post-26725-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm taking a college class. Have an assignment to install Wireshark and report on results as a learning experience. Have a Macbook with X 10.6.8. Tried to run the 64-bit version, and knocked out my modem. Will next try the 32-bit version, old stable version. Need some coaching so I don't knock out the modem again. If this has to be run in binary only, then I'll probably not be able to complete the assignment.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macbook" rel="tag" title="see questions tagged &#39;macbook&#39;">macbook</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '13, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/8c472a7f049fca58c95aa36bfd607763?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vasaguy&#39;s gravatar image" /><p><span>Vasaguy</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vasaguy has no accepted answers">0%</span></p></div></div><div id="comments-container-26725" class="comments-container"></div><div id="comment-tools-26725" class="comment-tools"></div><div class="clear"></div><div id="comment-26725-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

