+++
type = "question"
title = "listener needs to look at only displayed packets"
description = '''I wrote a listener in Lua that looks for certain fields and displays a summary. When I change the display filter my listener still gets all packets. I would like it to only get, or only look at, those packets that pass the current display filter. Can I do that?'''
date = "2012-09-26T16:43:00Z"
lastmod = "2012-09-26T16:43:00Z"
weight = 14566
keywords = [ "listener", "filter" ]
aliases = [ "/questions/14566" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [listener needs to look at only displayed packets](/questions/14566/listener-needs-to-look-at-only-displayed-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14566-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14566-score" class="post-score" title="current number of votes">0</div><span id="post-14566-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I wrote a listener in Lua that looks for certain fields and displays a summary. When I change the display filter my listener still gets all packets. I would like it to only get, or only look at, those packets that pass the current display filter. Can I do that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-listener" rel="tag" title="see questions tagged &#39;listener&#39;">listener</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '12, 16:43</strong></p><img src="https://secure.gravatar.com/avatar/96bf8d2f96297b8b9806add151c3ae1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wingman&#39;s gravatar image" /><p><span>Wingman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wingman has no accepted answers">0%</span></p></div></div><div id="comments-container-14566" class="comments-container"></div><div id="comment-tools-14566" class="comment-tools"></div><div class="clear"></div><div id="comment-14566-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

