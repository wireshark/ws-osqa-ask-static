+++
type = "question"
title = "Decrypt QUIC trafic"
description = '''I&#x27;am using Wireshark to annalyze QUIC traffic. However packages I got were encrypted. Can we decrypt them just like for https package?'''
date = "2016-08-17T06:05:00Z"
lastmod = "2016-10-06T07:59:00Z"
weight = 54918
keywords = [ "quic" ]
aliases = [ "/questions/54918" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt QUIC trafic](/questions/54918/decrypt-quic-trafic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54918-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54918-score" class="post-score" title="current number of votes">0</div><span id="post-54918-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'am using Wireshark to annalyze QUIC traffic. However packages I got were encrypted. Can we decrypt them just like for https package?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-quic" rel="tag" title="see questions tagged &#39;quic&#39;">quic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Aug '16, 06:05</strong></p><img src="https://secure.gravatar.com/avatar/7ae0ed0ddd879be9deddd5abeb869c11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zhongwei%20HU&#39;s gravatar image" /><p><span>Zhongwei HU</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zhongwei HU has no accepted answers">0%</span></p></div></div><div id="comments-container-54918" class="comments-container"></div><div id="comment-tools-54918" class="comment-tools"></div><div class="clear"></div><div id="comment-54918-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56193"></span>

<div id="answer-container-56193" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56193-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56193-score" class="post-score" title="current number of votes">1</div><span id="post-56193-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>It is no possible for the moment.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Oct '16, 07:59</strong></p><img src="https://secure.gravatar.com/avatar/d62b869ec385c6bbc2c04dc7176e8ea8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alexis%20La%20Goutte&#39;s gravatar image" /><p><span>Alexis La Go...</span><br />
<span class="score" title="110 reputation points">110</span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alexis La Goutte has one accepted answer">25%</span></p></div></div><div id="comments-container-56193" class="comments-container"></div><div id="comment-tools-56193" class="comment-tools"></div><div class="clear"></div><div id="comment-56193-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

