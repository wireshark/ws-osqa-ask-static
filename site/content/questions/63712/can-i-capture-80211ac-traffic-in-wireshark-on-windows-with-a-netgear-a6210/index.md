+++
type = "question"
title = "Can I capture 802.11ac traffic in Wireshark on Windows with a Netgear A6210?"
description = '''Hi there, just wanted to know if this matter was resolved or not? My employer wants me to sniff some 802.11ac traffic in wireshark on my laptop which has Windows 10 installed, and I was going to order a Netgear A6210 USB adapter to do it.  Can you please give me some comments or feedback? Will it wo...'''
date = "2017-10-06T08:52:00Z"
lastmod = "2017-10-06T08:52:00Z"
weight = 63712
keywords = [ "windows", "netgear", "802.11ac", "a6210" ]
aliases = [ "/questions/63712" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I capture 802.11ac traffic in Wireshark on Windows with a Netgear A6210?](/questions/63712/can-i-capture-80211ac-traffic-in-wireshark-on-windows-with-a-netgear-a6210)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63712-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63712-score" class="post-score" title="current number of votes">0</div><span id="post-63712-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there, just wanted to know if this matter was resolved or not? My employer wants me to sniff some 802.11ac traffic in wireshark on my laptop which has Windows 10 installed, and I was going to order a Netgear A6210 USB adapter to do it.</p><p>Can you please give me some comments or feedback? Will it work with Netgear A6210 and npcap?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-netgear" rel="tag" title="see questions tagged &#39;netgear&#39;">netgear</span> <span class="post-tag tag-link-802.11ac" rel="tag" title="see questions tagged &#39;802.11ac&#39;">802.11ac</span> <span class="post-tag tag-link-a6210" rel="tag" title="see questions tagged &#39;a6210&#39;">a6210</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '17, 08:52</strong></p><img src="https://secure.gravatar.com/avatar/11c8af06998740b0f4bd9702bd8a37b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="huzaifazafar108&#39;s gravatar image" /><p><span>huzaifazafar108</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="huzaifazafar108 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>06 Oct '17, 11:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-63712" class="comments-container"></div><div id="comment-tools-63712" class="comment-tools"></div><div class="clear"></div><div id="comment-63712-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

