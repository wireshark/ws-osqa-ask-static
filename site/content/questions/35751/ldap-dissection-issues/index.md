+++
type = "question"
title = "LDAP Dissection issues."
description = '''Hi All,  I have an issue in which Wireshark shows LDAP traffic but does not show the dissection within LDAP protocol, on different machines it works properly, but some machines just not showing the dissected LDAP details. do you have a clue why can&#x27;t i see these fields? what causing it? what can I d...'''
date = "2014-08-26T07:07:00Z"
lastmod = "2014-08-26T12:58:00Z"
weight = 35751
keywords = [ "dissection", "ldap" ]
aliases = [ "/questions/35751" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [LDAP Dissection issues.](/questions/35751/ldap-dissection-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35751-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35751-score" class="post-score" title="current number of votes">0</div><span id="post-35751-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I have an issue in which Wireshark shows LDAP traffic but does not show the dissection within LDAP protocol, on different machines it works properly, but some machines just not showing the dissected LDAP details.</p><p>do you have a clue why can't i see these fields? what causing it? what can I do in order to chance it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissection" rel="tag" title="see questions tagged &#39;dissection&#39;">dissection</span> <span class="post-tag tag-link-ldap" rel="tag" title="see questions tagged &#39;ldap&#39;">ldap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Aug '14, 07:07</strong></p><img src="https://secure.gravatar.com/avatar/406730cc3f84f05d3d94e02f96a68456?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="David217&#39;s gravatar image" /><p><span>David217</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="David217 has no accepted answers">0%</span></p></div></div><div id="comments-container-35751" class="comments-container"></div><div id="comment-tools-35751" class="comment-tools"></div><div class="clear"></div><div id="comment-35751-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35773"></span>

<div id="answer-container-35773" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35773-score" class="post-score" title="current number of votes">0</div><span id="post-35773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Perhaps you're running different versions of Wireshark, which might explain the different behavior? One version could have a bug.</p><p>Or maybe your Wireshark preferences are different. Try comparing your preference files to find out which preference might be different. In particular, look at the LDAP preferences, but it's possible another preference is the culprit.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Aug '14, 12:58</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-35773" class="comments-container"></div><div id="comment-tools-35773" class="comment-tools"></div><div class="clear"></div><div id="comment-35773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

