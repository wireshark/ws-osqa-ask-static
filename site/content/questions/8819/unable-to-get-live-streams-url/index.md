+++
type = "question"
title = "Unable to get live streams URL"
description = '''i am trying to get the live streams URL from this site e.g http://watchindian.tv/index.php?option=com_wrapper&amp;amp;view=wrapper&amp;amp;Itemid=184 to play it in VLC. But I am not able to get any working URLs.  I&#x27;ll appreciate if anyone can help me look up.. Thanks'''
date = "2012-02-03T21:01:00Z"
lastmod = "2012-02-03T21:01:00Z"
weight = 8819
keywords = [ "live", "streams" ]
aliases = [ "/questions/8819" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to get live streams URL](/questions/8819/unable-to-get-live-streams-url)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8819-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8819-score" class="post-score" title="current number of votes">0</div><span id="post-8819-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am trying to get the live streams URL from this site e.g <code>http://watchindian.tv/index.php?option=com_wrapper&amp;view=wrapper&amp;Itemid=184</code> to play it in VLC. But I am not able to get any working URLs.</p><p>I'll appreciate if anyone can help me look up..</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-live" rel="tag" title="see questions tagged &#39;live&#39;">live</span> <span class="post-tag tag-link-streams" rel="tag" title="see questions tagged &#39;streams&#39;">streams</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '12, 21:01</strong></p><img src="https://secure.gravatar.com/avatar/ca9862dc7f1f567d35dacb1504b4b7cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sitesmith&#39;s gravatar image" /><p><span>sitesmith</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sitesmith has no accepted answers">0%</span></p></div></div><div id="comments-container-8819" class="comments-container"></div><div id="comment-tools-8819" class="comment-tools"></div><div class="clear"></div><div id="comment-8819-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

