+++
type = "question"
title = "How to display Modbus TCP packet only"
description = '''I have installed Wireshark and Modbus/TCP simulator in my PC, but there are too many traffic in wireshark, how can I setup the filter to display Modbus/TCP traffic only?'''
date = "2015-10-18T23:31:00Z"
lastmod = "2015-10-19T00:29:00Z"
weight = 46682
keywords = [ "modbus" ]
aliases = [ "/questions/46682" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to display Modbus TCP packet only](/questions/46682/how-to-display-modbus-tcp-packet-only)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46682-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46682-score" class="post-score" title="current number of votes">0</div><span id="post-46682-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have installed Wireshark and Modbus/TCP simulator in my PC, but there are too many traffic in wireshark, how can I setup the filter to display Modbus/TCP traffic only?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-modbus" rel="tag" title="see questions tagged &#39;modbus&#39;">modbus</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '15, 23:31</strong></p><img src="https://secure.gravatar.com/avatar/f55f542536e073a3853449726457ce91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RabbitZ&#39;s gravatar image" /><p><span>RabbitZ</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RabbitZ has no accepted answers">0%</span></p></div></div><div id="comments-container-46682" class="comments-container"></div><div id="comment-tools-46682" class="comment-tools"></div><div class="clear"></div><div id="comment-46682-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46684"></span>

<div id="answer-container-46684" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46684-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46684-score" class="post-score" title="current number of votes">0</div><span id="post-46684-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could the following display filterstring: <code>mbtcp </code></p><p>The complete Display Filters for modbus cabbie found here: <a href="https://www.wireshark.org/docs/dfref/m/mbtcp.html">https://www.wireshark.org/docs/dfref/m/mbtcp.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '15, 00:29</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-46684" class="comments-container"></div><div id="comment-tools-46684" class="comment-tools"></div><div class="clear"></div><div id="comment-46684-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

