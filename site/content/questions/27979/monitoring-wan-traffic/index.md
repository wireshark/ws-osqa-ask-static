+++
type = "question"
title = "Monitoring WAN Traffic"
description = '''I have an Adtran provided by my ISP (telepacific) for our two bonned T1s. Off of that Adran, they hand us a single Ethernet cable that is plugged into an unmanaged switch which provided a link to our three devices with static IPs. Our Sonicwall, Our Webstream Computer, and Test webserver. We have no...'''
date = "2013-12-10T11:51:00Z"
lastmod = "2013-12-10T11:51:00Z"
weight = 27979
keywords = [ "bandwidth", "bandwidthutilization", "congestion" ]
aliases = [ "/questions/27979" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring WAN Traffic](/questions/27979/monitoring-wan-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27979-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27979-score" class="post-score" title="current number of votes">0</div><span id="post-27979-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have an Adtran provided by my ISP (telepacific) for our two bonned T1s. Off of that Adran, they hand us a single Ethernet cable that is plugged into an unmanaged switch which provided a link to our three devices with static IPs. Our Sonicwall, Our Webstream Computer, and Test webserver. We have noticed that our webstream on this bonded T1 is buffering the users end. Using a basic Solar Winds interface, we've been able to determine that there is a lot of inbound traffic that is maxing out our 3 Meg link. Question: If I install wireshark on the webstream computer will I be able to see all traffic coming in, or just that which is destined to the Static IP of the webstreamer? I assume I can point wireshark to the LAN interace that Telepacific gives us, right?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-bandwidthutilization" rel="tag" title="see questions tagged &#39;bandwidthutilization&#39;">bandwidthutilization</span> <span class="post-tag tag-link-congestion" rel="tag" title="see questions tagged &#39;congestion&#39;">congestion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '13, 11:51</strong></p><img src="https://secure.gravatar.com/avatar/88a3d5a4114098a5d5351f86dc889e6c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="godsitguy&#39;s gravatar image" /><p><span>godsitguy</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="godsitguy has no accepted answers">0%</span></p></div></div><div id="comments-container-27979" class="comments-container"></div><div id="comment-tools-27979" class="comment-tools"></div><div class="clear"></div><div id="comment-27979-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

