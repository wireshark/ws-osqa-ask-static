+++
type = "question"
title = "Are there any benefits in running the x64 version (Windows)?"
description = '''On Windows, I am currently using the portable version, which is 32-bit, mostly on 64-bit machines. Is there any difference, advantage. or speed improvement in running the x64 version instead? If there was, I was thinking of modifying the portable launcher to automatically start either the x86 or x64...'''
date = "2011-07-17T05:07:00Z"
lastmod = "2011-07-19T10:03:00Z"
weight = 5081
keywords = [ "windows", "portable", "64-bit" ]
aliases = [ "/questions/5081" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Are there any benefits in running the x64 version (Windows)?](/questions/5081/are-there-any-benefits-in-running-the-x64-version-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5081-score" class="post-score" title="current number of votes">0</div><span id="post-5081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On Windows, I am currently using the portable version, which is 32-bit, mostly on 64-bit machines.</p><p>Is there any difference, advantage. or speed improvement in running the x64 version instead?</p><p>If there was, I was thinking of modifying the portable launcher to automatically start either the x86 or x64 version, depending on the operating system.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-portable" rel="tag" title="see questions tagged &#39;portable&#39;">portable</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jul '11, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/c5831cbaefea0a4bce11b9a93a9ea590?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jason404&#39;s gravatar image" /><p><span>jason404</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jason404 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Jul '11, 05:08</strong> </span></p></div></div><div id="comments-container-5081" class="comments-container"></div><div id="comment-tools-5081" class="comment-tools"></div><div class="clear"></div><div id="comment-5081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5130"></span>

<div id="answer-container-5130" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5130-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5130-score" class="post-score" title="current number of votes">0</div><span id="post-5130-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The 64-bit version should theoretically have more address space available to it (that is, it should be able to use more memory if you've got big capture files), but we've seen that it is NOT able to use the extra address space for some reason.</p><p>The Win64 version does not have all of the libraries available to it, so some Wireshark features don't work in Win64.</p><p>So for now, you're probably actually better off with the 32-bit version.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jul '11, 10:03</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-5130" class="comments-container"></div><div id="comment-tools-5130" class="comment-tools"></div><div class="clear"></div><div id="comment-5130-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

