+++
type = "question"
title = "Bluetooth Connections"
description = '''Can I change Com Port for my Bluetooth?'''
date = "2011-07-19T08:42:00Z"
lastmod = "2011-07-19T16:54:00Z"
weight = 5127
keywords = [ "help" ]
aliases = [ "/questions/5127" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Bluetooth Connections](/questions/5127/bluetooth-connections)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5127-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5127-score" class="post-score" title="current number of votes">-3</div><span id="post-5127-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can I change Com Port for my Bluetooth?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '11, 08:42</strong></p><img src="https://secure.gravatar.com/avatar/92d082cc0d8b961e571de128952da2c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Taggart&#39;s gravatar image" /><p><span>Taggart</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Taggart has no accepted answers">0%</span></p></div></div><div id="comments-container-5127" class="comments-container"><span id="5138"></span><div id="comment-5138" class="comment"><div id="post-5138-score" class="comment-score">1</div><div class="comment-text"><p>This is the Wireshark Q&amp;A, not the General Hardware Questions section :-) If your topic has anything to do with Wireshark please rephrase it.</p></div><div id="comment-5138-info" class="comment-info"><span class="comment-age">(19 Jul '11, 16:54)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-5127" class="comment-tools"></div><div class="clear"></div><div id="comment-5127-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

