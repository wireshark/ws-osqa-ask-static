+++
type = "question"
title = "Content-encoded entity body (x-gzip): 518 bytes [Error: Decompression failed]"
description = '''Hi All,  I&#x27;ve been using Wireshark to monitor the request &amp;amp; response data (while calling an webservice) and Wireshark is able to display the XML request but when i look at the response, it shows some error like  &quot;Content-encoded entity body (x-gzip): 518 bytes [Error: Decompression failed]&quot;,  co...'''
date = "2011-03-04T06:17:00Z"
lastmod = "2011-03-04T06:17:00Z"
weight = 2661
keywords = [ "failed", "decompression", "error" ]
aliases = [ "/questions/2661" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Content-encoded entity body (x-gzip): 518 bytes \[Error: Decompression failed\]](/questions/2661/content-encoded-entity-body-x-gzip-518-bytes-error-decompression-failed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2661-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2661-score" class="post-score" title="current number of votes">1</div><span id="post-2661-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I've been using Wireshark to monitor the request &amp; response data (while calling an webservice) and Wireshark is able to display the XML request but when i look at the response, it shows some error like "Content-encoded entity body (x-gzip): 518 bytes [Error: Decompression failed]",</p><p>could anybody tell what sort of configuration i have to set to decompress the chunk data from retrieved from Webserver.</p><p>Many Thanks, Mohan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-failed" rel="tag" title="see questions tagged &#39;failed&#39;">failed</span> <span class="post-tag tag-link-decompression" rel="tag" title="see questions tagged &#39;decompression&#39;">decompression</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '11, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/96ffa25741ea8538ac61599cd1160b27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="amohanse&#39;s gravatar image" /><p><span>amohanse</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="amohanse has no accepted answers">0%</span></p></div></div><div id="comments-container-2661" class="comments-container"></div><div id="comment-tools-2661" class="comment-tools"></div><div class="clear"></div><div id="comment-2661-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

