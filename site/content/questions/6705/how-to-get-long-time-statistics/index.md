+++
type = "question"
title = "How to get long time statistics?"
description = '''Hi, I want to watch network traffic for a long time but I am not interested in every single package but only in statistics like displayed in the IO Graphs window. But I can&#x27;t find how to do it. If I just let Wireshark run, it blows the memory of my computer and crashes and if I build a ring buffer t...'''
date = "2011-10-04T08:11:00Z"
lastmod = "2011-10-04T13:53:00Z"
weight = 6705
keywords = [ "term", "statistics", "long" ]
aliases = [ "/questions/6705" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to get long time statistics?](/questions/6705/how-to-get-long-time-statistics)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6705-score" class="post-score" title="current number of votes">0</div><span id="post-6705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I want to watch network traffic for a long time but I am not interested in every single package but only in statistics like displayed in the IO Graphs window. But I can't find how to do it. If I just let Wireshark run, it blows the memory of my computer and crashes and if I build a ring buffer the IO Graph is always started anew when a file is closed and a new one opened. Is there a way to save the IO Graphs data to a continously written file ? Or is there any other way to do my job?</p><p>Thank you very much</p><p>Martin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-term" rel="tag" title="see questions tagged &#39;term&#39;">term</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-long" rel="tag" title="see questions tagged &#39;long&#39;">long</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '11, 08:11</strong></p><img src="https://secure.gravatar.com/avatar/3a8ca29741032bd7a1c89a6204788138?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mr_M_from_R&#39;s gravatar image" /><p><span>Mr_M_from_R</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mr_M_from_R has no accepted answers">0%</span></p></div></div><div id="comments-container-6705" class="comments-container"></div><div id="comment-tools-6705" class="comment-tools"></div><div class="clear"></div><div id="comment-6705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6723"></span>

<div id="answer-container-6723" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6723-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6723-score" class="post-score" title="current number of votes">0</div><span id="post-6723-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you really need the fancy features of the I/O graphs? Assuming not, you might want to look at things on the <a href="http://wiki.wireshark.org/Tools">Tools</a> page like ntop. Or Riverbed's <a href="http://www.riverbed.com/us/products/cascade/cascade_pilot.php">Cascade Pilot</a> product.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Oct '11, 13:53</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-6723" class="comments-container"></div><div id="comment-tools-6723" class="comment-tools"></div><div class="clear"></div><div id="comment-6723-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

