+++
type = "question"
title = "Is it illegal to use tshark?"
description = '''I don&#x27;t want legal advice, I&#x27;m just asking this question academically: Is it illegal to use wireshark/tshark in monitor mode to resolve signal strengths and mac addresses on a network that is yours or otherwise? Section 18 U.S. Code § 2511 says it is illegal to   intentionally intercepts, endeavors ...'''
date = "2017-05-14T09:16:00Z"
lastmod = "2017-05-15T06:15:00Z"
weight = 61388
keywords = [ "tshark" ]
aliases = [ "/questions/61388" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is it illegal to use tshark?](/questions/61388/is-it-illegal-to-use-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61388-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61388-score" class="post-score" title="current number of votes">0</div><span id="post-61388-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I don't want legal advice, I'm just asking this question academically: Is it illegal to use wireshark/tshark in monitor mode to resolve signal strengths and mac addresses on a network that is yours or otherwise?</p><p>Section 18 U.S. Code § 2511 says it is illegal to</p><blockquote><p>intentionally intercepts, endeavors to intercept, or procures any other person to intercept or endeavor to intercept, any wire, oral, or electronic communication</p></blockquote><p>There are exceptions if it is being used for personal cybersecurity or consent is given.</p><p>However, I guess my question is more specifically related to whether resolving the mac addresses and signal strengths counts as "interception of electronic communication"?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 May '17, 09:16</strong></p><img src="https://secure.gravatar.com/avatar/8a86195c07b3bd8113c64b3586cef79c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="qrv3w&#39;s gravatar image" /><p><span>qrv3w</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="qrv3w has no accepted answers">0%</span></p></div></div><div id="comments-container-61388" class="comments-container"><span id="61399"></span><div id="comment-61399" class="comment"><div id="post-61399-score" class="comment-score"></div><div class="comment-text"><p>Your asking legal advice...</p></div><div id="comment-61399-info" class="comment-info"><span class="comment-age">(14 May '17, 23:34)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61404"></span><div id="comment-61404" class="comment"><div id="post-61404-score" class="comment-score"></div><div class="comment-text"><p>It is perfectly fine to talk about law without it being legal advice. Here, I am not asking for legal advice because I'm asking for an interpretation of law as a hypothetical example for my own academic purposes. There is no particular factual situation I am asking about. I have no obligations or responsibilities that will change upon receiving any information about this question. I would only like to hear speculation of the community and not professional opinions.</p></div><div id="comment-61404-info" class="comment-info"><span class="comment-age">(15 May '17, 06:15)</span> <span class="comment-user userinfo">qrv3w</span></div></div></div><div id="comment-tools-61388" class="comment-tools"></div><div class="clear"></div><div id="comment-61388-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61394"></span>

<div id="answer-container-61394" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61394-score" class="post-score" title="current number of votes">1</div><span id="post-61394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Generally speaking, Wireshark is a tool. A tool can be used legally, and it can be used illegally. There is nothing intrinsically illegal about the tool itself.</p><p>Specific laws in specific countries will vary, but I can think of cases where it could be used illegally in the US. For example, if you were to break into the wire closet of a building, set up some taps and used Wireshark to intercept an enterprise's VoIP calls, I have to think that would be illegal. Meanwhile if you use Wireshark as a network administrator to troubleshoot your network, that would almost certainly not be.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 May '17, 19:04</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-61394" class="comments-container"></div><div id="comment-tools-61394" class="comment-tools"></div><div class="clear"></div><div id="comment-61394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

