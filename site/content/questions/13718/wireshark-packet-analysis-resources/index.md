+++
type = "question"
title = "Wireshark packet analysis  resources"
description = '''Okay, I am just now learning how to use wireshark as an application, however when looking at the &quot;info&quot; section of the packet captures I don&#x27;t fully understand what some of the information means. Is there a book or website I can reference to help me understand the packet/s analysis better? I underst...'''
date = "2012-08-18T11:20:00Z"
lastmod = "2012-08-20T03:44:00Z"
weight = 13718
keywords = [ "info", "newbie", "analysis", "packet" ]
aliases = [ "/questions/13718" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark packet analysis resources](/questions/13718/wireshark-packet-analysis-resources)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13718-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13718-score" class="post-score" title="current number of votes">0</div><span id="post-13718-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Okay, I am just now learning how to use wireshark as an application, however when looking at the "info" section of the packet captures I don't fully understand what some of the information means. Is there a book or website I can reference to help me understand the packet/s analysis better? I understand the coloring rules and that they can help you determine the problem with most packets, but I was wondering if there is a good book or website out there to help you fully understand the packet/s info data?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '12, 11:20</strong></p><img src="https://secure.gravatar.com/avatar/52857dcff5e05dba5f87f9670ead91b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="I_GEEK_IT&#39;s gravatar image" /><p><span>I_GEEK_IT</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="I_GEEK_IT has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-13718" class="comments-container"></div><div id="comment-tools-13718" class="comment-tools"></div><div class="clear"></div><div id="comment-13718-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13719"></span>

<div id="answer-container-13719" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13719-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13719-score" class="post-score" title="current number of votes">0</div><span id="post-13719-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Laura Chappell's book <em>Wireshark Network Analysis</em> is the definitive guide. The main page of <a href="http://www.wireshark.org"></a><a href="http://www.wireshark.org">www.wireshark.org</a> has a link titled "<a href="https://www.wireshark.org/docs/">Learn Wireshark, Resources and Documentation</a>." There are YouTube videos and other web sites with Wireshark information. Google is your friend. And, of course, you can come back here when you have specific questions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Aug '12, 11:33</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-13719" class="comments-container"></div><div id="comment-tools-13719" class="comment-tools"></div><div class="clear"></div><div id="comment-13719-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="13749"></span>

<div id="answer-container-13749" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13749-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13749-score" class="post-score" title="current number of votes">0</div><span id="post-13749-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Check this out: <a href="http://cert.inteco.es/extfrontinteco/img/File/intecocert/EstudiosInformes/cert_trafficwireshark.pdf">TRAFFIC ANALYSIS WITH WIRESHARK</a></p><p>A great starting point and there is the Chris Sanders book from No Starch, it is great: <a href="http://nostarch.com/packet2.htm">Using Wireshark to Solve Real-World Network Problems</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '12, 03:44</strong></p><img src="https://secure.gravatar.com/avatar/0ce931f077e091c7dc397bda5e106b99?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sha8e&#39;s gravatar image" /><p><span>sha8e</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sha8e has no accepted answers">0%</span></p></div></div><div id="comments-container-13749" class="comments-container"></div><div id="comment-tools-13749" class="comment-tools"></div><div class="clear"></div><div id="comment-13749-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

