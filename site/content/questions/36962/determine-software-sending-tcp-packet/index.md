+++
type = "question"
title = "Determine Software sending TCP packet?"
description = '''Packets are being sent from my Windows 8.1 Pro system to 54.86.239.95 (ICMP ECHO packets). ARIN believes that IP is in a broad range of Amazon IPs. Given that, I have no Amazon tasks, services, web sites, applications, apps that are running (to my knowledge). MS Outlook 2013 is running, and there ar...'''
date = "2014-10-10T08:51:00Z"
lastmod = "2014-10-10T09:20:00Z"
weight = 36962
keywords = [ "ip", "softwaree", "amazon", "source" ]
aliases = [ "/questions/36962" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Determine Software sending TCP packet?](/questions/36962/determine-software-sending-tcp-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36962-score" class="post-score" title="current number of votes">0</div><span id="post-36962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Packets are being sent from my Windows 8.1 Pro system to 54.86.239.95 (ICMP ECHO packets). ARIN believes that IP is in a broad range of Amazon IPs. Given that, I have no Amazon tasks, services, web sites, applications, apps that are running (to my knowledge). MS Outlook 2013 is running, and there are some messages (not read or open) from Amazon.</p><p>So, how would I achieve finding the true identity of the software sending the packets from my system?</p><p>Other packets went out to Amazon's set of IP sites too, but nothing seemed to identify the source of the transmitted packets.</p><p>Given that I have a piece of software that began failing yesterday within a 24 hour period, I am snooping around for a cause (yet to be determined).</p><p>Thank you for help in any fashion.</p><p>Srosxi</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-softwaree" rel="tag" title="see questions tagged &#39;softwaree&#39;">softwaree</span> <span class="post-tag tag-link-amazon" rel="tag" title="see questions tagged &#39;amazon&#39;">amazon</span> <span class="post-tag tag-link-source" rel="tag" title="see questions tagged &#39;source&#39;">source</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '14, 08:51</strong></p><img src="https://secure.gravatar.com/avatar/deae067dc85c04b8fc6d6d1a4250d578?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Srosxi&#39;s gravatar image" /><p><span>Srosxi</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Srosxi has no accepted answers">0%</span></p></div></div><div id="comments-container-36962" class="comments-container"></div><div id="comment-tools-36962" class="comment-tools"></div><div class="clear"></div><div id="comment-36962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36965"></span>

<div id="answer-container-36965" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36965-score" class="post-score" title="current number of votes">0</div><span id="post-36965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try using Message Analyser from Microsoft, as well as capturing the traffic, I believe that can show you the sending process as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '14, 09:20</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36965" class="comments-container"></div><div id="comment-tools-36965" class="comment-tools"></div><div class="clear"></div><div id="comment-36965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

