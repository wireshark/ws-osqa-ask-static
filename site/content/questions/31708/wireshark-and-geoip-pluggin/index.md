+++
type = "question"
title = "Wireshark and GeoIP pluggin"
description = '''I was just wondering if i could tweak GeoIp by changing the colors of the nodes??? I want to display Domains and sub Domains IP&#x27;s.I am thinking of loading a csv file with all the relevant info and consequently display then in the world map but different color codes.Let me know how to achieve this ta...'''
date = "2014-04-10T05:15:00Z"
lastmod = "2014-04-10T05:15:00Z"
weight = 31708
keywords = [ "ip", "geolocalization", "google", "dns", "geoip" ]
aliases = [ "/questions/31708" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and GeoIP pluggin](/questions/31708/wireshark-and-geoip-pluggin)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31708-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31708-score" class="post-score" title="current number of votes">0</div><span id="post-31708-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was just wondering if i could tweak GeoIp by changing the colors of the nodes??? I want to display Domains and sub Domains IP's.I am thinking of loading a csv file with all the relevant info and consequently display then in the world map but different color codes.Let me know how to achieve this task and if is doable. Cheers Astrokilla</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-geolocalization" rel="tag" title="see questions tagged &#39;geolocalization&#39;">geolocalization</span> <span class="post-tag tag-link-google" rel="tag" title="see questions tagged &#39;google&#39;">google</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span> <span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '14, 05:15</strong></p><img src="https://secure.gravatar.com/avatar/0a3b4fbdc2dd9a1df2ab73cc8e78d925?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Astrokilla23&#39;s gravatar image" /><p><span>Astrokilla23</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Astrokilla23 has no accepted answers">0%</span></p></div></div><div id="comments-container-31708" class="comments-container"></div><div id="comment-tools-31708" class="comment-tools"></div><div class="clear"></div><div id="comment-31708-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

