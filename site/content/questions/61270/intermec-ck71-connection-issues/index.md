+++
type = "question"
title = "Intermec CK71 - Connection Issues"
description = '''We&#x27;ve recently switched to Intermec CK71 devices in our plant. Each device is configured to connect via our wireless network to a server at a remote site via our WAN. Out of 24 devices (each configured in an identical fashion), only 9 are able to connect. I&#x27;ve ran Netlog on both a working device, an...'''
date = "2017-05-06T18:42:00Z"
lastmod = "2017-05-07T02:10:00Z"
weight = 61270
keywords = [ "intermec", "ck71" ]
aliases = [ "/questions/61270" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Intermec CK71 - Connection Issues](/questions/61270/intermec-ck71-connection-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61270-score" class="post-score" title="current number of votes">0</div><span id="post-61270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We've recently switched to Intermec CK71 devices in our plant. Each device is configured to connect via our wireless network to a server at a remote site via our WAN. Out of 24 devices (each configured in an identical fashion), only 9 are able to connect. I've ran Netlog on both a working device, and a non-working device and noticed that a device cannot connect when it says "TCP 54 1034 → 30206 [RST, ACK] Seq=201 Ack=26 Win=0 Len=0". I've attached two wireshark traces in the hopes that someone can help me out. Keep in mind that this is my first time using a packet capture tool, so if I've missed something, please forgive me.<img src="https://osqa-ask.wireshark.org/upfiles/Good_Capture_9d1nEz0.png" alt="Good_Capture" /><img src="https://osqa-ask.wireshark.org/upfiles/Bad_Capture_M8i5dCt.png" alt="Bad_Capture" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-intermec" rel="tag" title="see questions tagged &#39;intermec&#39;">intermec</span> <span class="post-tag tag-link-ck71" rel="tag" title="see questions tagged &#39;ck71&#39;">ck71</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '17, 18:42</strong></p><img src="https://secure.gravatar.com/avatar/459f615782639fbee9c7c0e289cede44?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ckrafcky&#39;s gravatar image" /><p><span>ckrafcky</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ckrafcky has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61270" class="comments-container"><span id="61273"></span><div id="comment-61273" class="comment"><div id="post-61273-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-61273-info" class="comment-info"><span class="comment-age">(07 May '17, 02:10)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-61270" class="comment-tools"></div><div class="clear"></div><div id="comment-61270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

