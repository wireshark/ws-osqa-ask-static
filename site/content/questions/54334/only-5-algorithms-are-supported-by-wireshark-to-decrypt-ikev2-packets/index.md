+++
type = "question"
title = "Only 5 algorithms are supported by wireshark to decrypt IKEv2 packets?"
description = ''''''
date = "2016-07-26T04:27:00Z"
lastmod = "2016-07-26T05:54:00Z"
weight = 54334
keywords = [ "encryption", "ikev2", "decryption" ]
aliases = [ "/questions/54334" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Only 5 algorithms are supported by wireshark to decrypt IKEv2 packets?](/questions/54334/only-5-algorithms-are-supported-by-wireshark-to-decrypt-ikev2-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54334-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54334-score" class="post-score" title="current number of votes">0</div><span id="post-54334-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/Screenshot_from_2016-07-26_14:18:55_G7n6Q1s.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-ikev2" rel="tag" title="see questions tagged &#39;ikev2&#39;">ikev2</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '16, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/3979ee191d6e4d4cf918bfe41475e815?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Codrut%20Cristian%20Grosu&#39;s gravatar image" /><p><span>Codrut Crist...</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Codrut Cristian Grosu has no accepted answers">0%</span></p></img></div></div><div id="comments-container-54334" class="comments-container"></div><div id="comment-tools-54334" class="comment-tools"></div><div class="clear"></div><div id="comment-54334-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54336"></span>

<div id="answer-container-54336" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54336-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54336-score" class="post-score" title="current number of votes">1</div><span id="post-54336-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>yes, as seen in the configuration dialog.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '16, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-54336" class="comments-container"></div><div id="comment-tools-54336" class="comment-tools"></div><div class="clear"></div><div id="comment-54336-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

