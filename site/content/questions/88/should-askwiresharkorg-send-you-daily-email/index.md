+++
type = "question"
title = "Should ask.wireshark.org send you daily email?"
description = '''If you go to User tools→email notification settings in your user profile you&#x27;ll see a preference labeled &quot;Send me the daily digest with information about the site activity&quot;. This option currently enabled by default but it doesn&#x27;t do anything. I&#x27;m not sure that this is proper default behavior so I di...'''
date = "2010-09-15T11:07:00Z"
lastmod = "2010-09-15T12:27:00Z"
weight = 88
keywords = [ "meta", "spam" ]
aliases = [ "/questions/88" ]
osqa_answers = 5
osqa_accepted = false
+++

<div class="headNormal">

# [Should ask.wireshark.org send you daily email?](/questions/88/should-askwiresharkorg-send-you-daily-email)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-88-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-88-score" class="post-score" title="current number of votes">2</div><span id="post-88-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If you go to <em>User tools→email notification settings</em> in your user profile you'll see a preference labeled "Send me the daily digest with information about the site activity". This option currently enabled by default but it doesn't do anything. I'm not sure that this is proper default behavior so I disabled the associated <code>cron</code> job. (There was a single test run yesterday in case you received the email.)</p><p>What are your preferences for daily digests? We have the following options:</p><ul><li>Opt in. Leave the default settings as they are and enable the cron job. New users and most existing users will start receiving daily digests. This is OSQA's default behavior.</li><li>Opt out. Disable the daily digest option by default and retroactively for existing users, then enable the cron job. Everyone would have to manually enable daily digests.</li><li>Broken. Leave the cron job disabled. The daily digest option will do nothing.</li><li>???. Some other crazy thing describe by you below.</li></ul><p>Suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span> <span class="post-tag tag-link-spam" rel="tag" title="see questions tagged &#39;spam&#39;">spam</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '10, 11:07</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-88" class="comments-container"></div><div id="comment-tools-88" class="comment-tools"></div><div class="clear"></div><div id="comment-88-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

5 Answers:

</div>

</div>

<span id="89"></span>

<div id="answer-container-89" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-89-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-89-score" class="post-score" title="current number of votes">0</div><span id="post-89-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'd prefer the daily digest be disabled by default. I'd only enable it if the traffic volume wasn't to bad and I knew I wouldn't get a huge digest every day.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 11:42</strong></p><img src="https://secure.gravatar.com/avatar/afd0960fe317ff2c604f46632aa588d0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Inak&#39;s gravatar image" /><p><span>Inak</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Inak has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-89" class="comments-container"></div><div id="comment-tools-89" class="comment-tools"></div><div class="clear"></div><div id="comment-89-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="94"></span>

<div id="answer-container-94" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-94-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-94-score" class="post-score" title="current number of votes">0</div><span id="post-94-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'd prefer the digest disabled by default also. I think the default auto-subscribe options should make most people happy.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/9e493496d59bb4ce33c37cd6e7a26a4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GeonJay&#39;s gravatar image" /><p><span>GeonJay</span><br />
<span class="score" title="470 reputation points">470</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="bronze">●</span><span class="badgecount">22</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GeonJay has 2 accepted answers">5%</span></p></div></div><div id="comments-container-94" class="comments-container"></div><div id="comment-tools-94" class="comment-tools"></div><div class="clear"></div><div id="comment-94-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="95"></span>

<div id="answer-container-95" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-95-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-95-score" class="post-score" title="current number of votes">0</div><span id="post-95-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would leave it as "Opt out" by default, but I'd be interested in opting in to receive the daily e-mail.</p><p>Regards, Keith</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/80ea04b38fffc870de0df66fe8d44d49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="swagger&#39;s gravatar image" /><p><span>swagger</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="swagger has no accepted answers">0%</span></p></div></div><div id="comments-container-95" class="comments-container"></div><div id="comment-tools-95" class="comment-tools"></div><div class="clear"></div><div id="comment-95-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="97"></span>

<div id="answer-container-97" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-97-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-97-score" class="post-score" title="current number of votes">0</div><span id="post-97-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>+1 for having it disabled by default, but with the option to turn it on if wanted</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 12:18</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-97" class="comments-container"></div><div id="comment-tools-97" class="comment-tools"></div><div class="clear"></div><div id="comment-97-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="98"></span>

<div id="answer-container-98" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-98-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-98-score" class="post-score" title="current number of votes">0</div><span id="post-98-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I agree with having opt out as default, but possibly add the ability to get todays hottest topic emailed directly. If it's something majorly popular, then it would probably be of interest to most of us.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 12:27</strong></p><img src="https://secure.gravatar.com/avatar/5c303edc5d3be7a2aefa87299960dd93?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ObiWan&#39;s gravatar image" /><p><span>ObiWan</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ObiWan has no accepted answers">0%</span></p></div></div><div id="comments-container-98" class="comments-container"></div><div id="comment-tools-98" class="comment-tools"></div><div class="clear"></div><div id="comment-98-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

