+++
type = "question"
title = "Network slowness"
description = '''Hello, I have a client that is experiencing network slowness issues to my server (199.19.128.229) from my pcap. can you please help me identifying any problems. Thank you for your cooperation. jamesigreene@hotmail.com'''
date = "2013-02-16T17:11:00Z"
lastmod = "2013-02-17T04:12:00Z"
weight = 18670
keywords = [ "pcap" ]
aliases = [ "/questions/18670" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Network slowness](/questions/18670/network-slowness)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18670-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18670-score" class="post-score" title="current number of votes">0</div><span id="post-18670-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have a client that is experiencing network slowness issues to my server (199.19.128.229) from my pcap. can you please help me identifying any problems. Thank you for your cooperation.</p><p><span class="__cf_email__" data-cfemail="d8b2b9b5bdabb1bfaabdbdb6bd98b0b7acb5b9b1b4f6bbb7b5">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Feb '13, 17:11</strong></p><img src="https://secure.gravatar.com/avatar/43f3326c209c1e93af38f7348586318b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dukeminus&#39;s gravatar image" /><p><span>dukeminus</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dukeminus has no accepted answers">0%</span></p></div></div><div id="comments-container-18670" class="comments-container"><span id="18679"></span><div id="comment-18679" class="comment"><div id="post-18679-score" class="comment-score">2</div><div class="comment-text"><p>Hi James, that is not exactly how this site works. You may find a lot of help here in analyzing your data when you ask specific questions on where you are stuck in your analysis.</p><p>If it is network analysis consultancy that you seek, you may check out the profiles of people who have many karma points, some of us do this for a living too :-)</p></div><div id="comment-18679-info" class="comment-info"><span class="comment-age">(17 Feb '13, 04:12)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-18670" class="comment-tools"></div><div class="clear"></div><div id="comment-18670-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

