+++
type = "question"
title = "[closed] Can I get certificate data in standard format(.der,.pem) using Tshark Commands?"
description = '''There is option in wireshark export selected packet bytes and it save in certificates standard format. so is there any command in Tshark which do work of export selected packet bytes in .der or .pem or.cer format Reply Please '''
date = "2015-08-28T02:11:00Z"
lastmod = "2015-08-28T02:11:00Z"
weight = 45441
keywords = [ "tshark", "wireshark" ]
aliases = [ "/questions/45441" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Can I get certificate data in standard format(.der,.pem) using Tshark Commands?](/questions/45441/can-i-get-certificate-data-in-standard-formatderpem-using-tshark-commands)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45441-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45441-score" class="post-score" title="current number of votes">0</div><span id="post-45441-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There is option in wireshark export selected packet bytes and it save in certificates standard format. so is there any command in Tshark which do work of export selected packet bytes in .der or .pem or.cer format</p><p>Reply Please</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '15, 02:11</strong></p><img src="https://secure.gravatar.com/avatar/a758a2fd6fc5d489ffed08e73335d70e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Varsharani%20Hawanna&#39;s gravatar image" /><p><span>Varsharani H...</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Varsharani Hawanna has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>28 Aug '15, 08:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-45441" class="comments-container"></div><div id="comment-tools-45441" class="comment-tools"></div><div class="clear"></div><div id="comment-45441-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Kurt Knochner 28 Aug '15, 08:16

</div>

</div>

</div>

