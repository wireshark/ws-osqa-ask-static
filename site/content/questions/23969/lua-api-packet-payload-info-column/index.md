+++
type = "question"
title = "LUA API packet payload (info column)"
description = '''Hello, I&#x27;ve just recently started playing with the API and and I can get data such as source address, destination address, ports, etc but the meat of what I need is in the packet&#x27;s payload (the info column in wireshark) and I can&#x27;t figure out how to dump the data. Does anyone have an example or two ...'''
date = "2013-08-22T14:54:00Z"
lastmod = "2013-08-22T14:54:00Z"
weight = 23969
keywords = [ "lua", "api", "pinfo" ]
aliases = [ "/questions/23969" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [LUA API packet payload (info column)](/questions/23969/lua-api-packet-payload-info-column)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23969-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23969-score" class="post-score" title="current number of votes">0</div><span id="post-23969-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I've just recently started playing with the API and and I can get data such as source address, destination address, ports, etc but the meat of what I need is in the packet's payload (the info column in wireshark) and I can't figure out how to dump the data. Does anyone have an example or two of dumping this data?</p><p>Specifically I'm looking to parse the HTTP post and get data.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-api" rel="tag" title="see questions tagged &#39;api&#39;">api</span> <span class="post-tag tag-link-pinfo" rel="tag" title="see questions tagged &#39;pinfo&#39;">pinfo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '13, 14:54</strong></p><img src="https://secure.gravatar.com/avatar/fb2616884086c7727653720c0fc42370?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rts&#39;s gravatar image" /><p><span>rts</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rts has no accepted answers">0%</span></p></div></div><div id="comments-container-23969" class="comments-container"></div><div id="comment-tools-23969" class="comment-tools"></div><div class="clear"></div><div id="comment-23969-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

