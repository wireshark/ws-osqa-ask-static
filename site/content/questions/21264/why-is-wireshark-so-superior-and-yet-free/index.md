+++
type = "question"
title = "why is wireshark so superior and yet free"
description = '''Given the sorry state of what goes for network analysis these days, it is refreshing to deal with a product (Wireshark) and its support staff that appear to understand networking and how it works. I would be happy to pay for your product and find it superior to anything else out there as a general s...'''
date = "2013-05-19T08:44:00Z"
lastmod = "2013-05-20T07:11:00Z"
weight = 21264
keywords = [ "about_wireshark" ]
aliases = [ "/questions/21264" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [why is wireshark so superior and yet free](/questions/21264/why-is-wireshark-so-superior-and-yet-free)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21264-score" class="post-score" title="current number of votes">0</div><span id="post-21264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Given the sorry state of what goes for network analysis these days, it is refreshing to deal with a product (Wireshark) and its support staff that appear to understand networking and how it works. I would be happy to pay for your product and find it superior to anything else out there as a general sniffer. This is not a complaint we all love free and it makes it easy to share IP traces with clients when all they have to do is download Wireshark rather than purchase it to view the IP trace. The sad part is that so many "network analysts" these days either don't bother to look at the IP trace or don't know what they are looking at when they view the IP trace.</p><p>John Raestas Director Special Project Development Computer Projects of Illinois Inc.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-about_wireshark" rel="tag" title="see questions tagged &#39;about_wireshark&#39;">about_wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 May '13, 08:44</strong></p><img src="https://secure.gravatar.com/avatar/d004f2f34faa64530972691b60f8becb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cpinap&#39;s gravatar image" /><p><span>cpinap</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cpinap has no accepted answers">0%</span></p></div></div><div id="comments-container-21264" class="comments-container"></div><div id="comment-tools-21264" class="comment-tools"></div><div class="clear"></div><div id="comment-21264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="21291"></span>

<div id="answer-container-21291" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21291-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21291-score" class="post-score" title="current number of votes">1</div><span id="post-21291-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>The sad part is that so many "network analysts" these days either don't bother to look at the IP trace or don't know what they are looking at when they view the IP trace.</p></blockquote><p>Isn't that a good argument to start a business? ;-))</p><p>On the other side, the 'sad part' with the network troubleshooting business is, that a lot of decision makers don't see the value of network problem analysis in the first place, as they seem to believe it is possible to solve a network problem by throwing money, CPU power and bandwidth on the problem and make it go away that way.</p><p>Anyway, they all come to a point where that strategy does not work any longer and THEN it's time to call for the real network analysts with the cool tools and the knowledge how to use those tools :-))</p><p>Wireshark is a cool tool, because it is powerful and free and because there is an active community using, supporting and extending it constantly.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '13, 03:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 May '13, 07:33</strong> </span></p></div></div><div id="comments-container-21291" class="comments-container"></div><div id="comment-tools-21291" class="comment-tools"></div><div class="clear"></div><div id="comment-21291-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21307"></span>

<div id="answer-container-21307" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21307-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21307-score" class="post-score" title="current number of votes">1</div><span id="post-21307-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here's my opinion on why it's free (and why it's great):</p><ol><li>Because Gerald Combs had a great idea (and great start to) a useful piece of software.</li><li>Because he had the idea to release his code under the <a href="https://en.wikipedia.org/wiki/GNU_GPL">GNU GPL</a>. That means the source code is open and is (and always will be) free to use, modify and distribute. (The timing of this event--around 1998--makes me think Gerald may have been influenced by <a href="https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar">The Cathedral and the Bazaar</a>. If you haven't read it, you should.)</li><li>Because the code was GPL and because the software was useful, it attracted users, some of who were developers who then contributed code in order to make it better (in some cases the users became developers so they could contribute; such is the case with one of Wireshark's top contributors). People kept contributing because Wireshark (then Ethereal) developed into a Bazaar-style model which accepted those contributions.</li></ol><p>In other words it's great <strong>because</strong> it's free.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '13, 07:11</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-21307" class="comments-container"></div><div id="comment-tools-21307" class="comment-tools"></div><div class="clear"></div><div id="comment-21307-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21267"></span>

<div id="answer-container-21267" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21267-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21267-score" class="post-score" title="current number of votes">0</div><span id="post-21267-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Because lots of people spend a lot of their time improving it, and doing it for free. Some work on it during their office hours because it's their job.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 May '13, 11:43</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-21267" class="comments-container"></div><div id="comment-tools-21267" class="comment-tools"></div><div class="clear"></div><div id="comment-21267-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

