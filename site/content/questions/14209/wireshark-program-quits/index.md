+++
type = "question"
title = "Wireshark Program quits"
description = '''Hello all,  I am running into the issue of Wireshark quitting after about 12 minutes. I am trying to run it on Widows server standard. It will run for a few then display the message:  Microsoft Visual C++ Runtime Library &quot;This application has requested the Runtime to terminate it in an unusual way. ...'''
date = "2012-09-12T08:53:00Z"
lastmod = "2012-09-12T12:19:00Z"
weight = 14209
keywords = [ "wireshark" ]
aliases = [ "/questions/14209" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Program quits](/questions/14209/wireshark-program-quits)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14209-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14209-score" class="post-score" title="current number of votes">0</div><span id="post-14209-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all, I am running into the issue of Wireshark quitting after about 12 minutes. I am trying to run it on Widows server standard. It will run for a few then display the message: Microsoft Visual C++ Runtime Library "This application has requested the Runtime to terminate it in an unusual way. Please contact the applications support team for more information." The installations (WinPcap, and wireshark) are both the right bit. What would you all suggest I do? Thanks, Jonathan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '12, 08:53</strong></p><img src="https://secure.gravatar.com/avatar/1ab556edf23744949e43d4493e391063?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JMAC&#39;s gravatar image" /><p><span>JMAC</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JMAC has no accepted answers">0%</span></p></div></div><div id="comments-container-14209" class="comments-container"></div><div id="comment-tools-14209" class="comment-tools"></div><div class="clear"></div><div id="comment-14209-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14219"></span>

<div id="answer-container-14219" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14219-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14219-score" class="post-score" title="current number of votes">0</div><span id="post-14219-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sound to me like the usual "out of memory" crash after capturing a lot of data. See this question, which is similar: <a href="http://ask.wireshark.org/questions/4847/wireshark-crashes-when-left-open-for-over-5-minutes-on-windows-7-64bit">http://ask.wireshark.org/questions/4847/wireshark-crashes-when-left-open-for-over-5-minutes-on-windows-7-64bit</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Sep '12, 12:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14219" class="comments-container"></div><div id="comment-tools-14219" class="comment-tools"></div><div class="clear"></div><div id="comment-14219-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

