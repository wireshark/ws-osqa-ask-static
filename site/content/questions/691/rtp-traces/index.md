+++
type = "question"
title = "RTP traces"
description = '''Where can I download a lot of RTP traces (in addition to the ones in example wireshark traces)? Any suggestions are welcome. '''
date = "2010-10-26T18:36:00Z"
lastmod = "2010-10-26T19:30:00Z"
weight = 691
keywords = [ "downloads", "rtp", "trace" ]
aliases = [ "/questions/691" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [RTP traces](/questions/691/rtp-traces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-691-score" class="post-score" title="current number of votes">0</div><span id="post-691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can I download a lot of RTP traces (in addition to the ones in example wireshark traces)? Any suggestions are welcome.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-downloads" rel="tag" title="see questions tagged &#39;downloads&#39;">downloads</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-trace" rel="tag" title="see questions tagged &#39;trace&#39;">trace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '10, 18:36</strong></p><img src="https://secure.gravatar.com/avatar/da051abac41879aed4060d544d37fd6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skypemesm&#39;s gravatar image" /><p><span>skypemesm</span><br />
<span class="score" title="46 reputation points">46</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skypemesm has no accepted answers">0%</span></p></div></div><div id="comments-container-691" class="comments-container"></div><div id="comment-tools-691" class="comment-tools"></div><div class="clear"></div><div id="comment-691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="692"></span>

<div id="answer-container-692" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-692-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-692-score" class="post-score" title="current number of votes">2</div><span id="post-692-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="skypemesm has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We have loads of traces including some VoIP traces over at www.wiresharkbook.com in the downloads section - you'll be looking for trace files starting with "voip-".</p><p>There are also tons of traces over at pcapr.net and some sample traces at http://wiki.wireshark.org/RTP.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Oct '10, 19:30</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Oct '10, 19:31</strong> </span></p></div></div><div id="comments-container-692" class="comments-container"></div><div id="comment-tools-692" class="comment-tools"></div><div class="clear"></div><div id="comment-692-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

