+++
type = "question"
title = "FDST protocol"
description = '''Can you add FDST protocol to your program? Please. It would be create? Sorry for my &quot;not so good&quot; english.  Могли бы Вы добавить поддержку протокола FDST в вашу программу?'''
date = "2012-12-19T22:40:00Z"
lastmod = "2012-12-21T03:54:00Z"
weight = 17081
keywords = [ "fdst" ]
aliases = [ "/questions/17081" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [FDST protocol](/questions/17081/fdst-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17081-score" class="post-score" title="current number of votes">0</div><span id="post-17081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can you add FDST protocol to your program? Please. It would be create?</p><p>Sorry for my "not so good" english.</p><p>Могли бы Вы добавить поддержку протокола FDST в вашу программу?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fdst" rel="tag" title="see questions tagged &#39;fdst&#39;">fdst</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '12, 22:40</strong></p><img src="https://secure.gravatar.com/avatar/61b7b49e8e7552dd688a9498b1679bcd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Curecool&#39;s gravatar image" /><p><span>Curecool</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Curecool has no accepted answers">0%</span></p></div></div><div id="comments-container-17081" class="comments-container"></div><div id="comment-tools-17081" class="comment-tools"></div><div class="clear"></div><div id="comment-17081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17082"></span>

<div id="answer-container-17082" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17082-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17082-score" class="post-score" title="current number of votes">0</div><span id="post-17082-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please file an enhancement bug at <a href="http://bugs.wireshark.org">bugs.wireshark.org</a>. Please also add details about that protocol (links, specs, etc.) as google does not return anything usefull for "FDST protocol".</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Dec '12, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-17082" class="comments-container"><span id="17109"></span><div id="comment-17109" class="comment"><div id="post-17109-score" class="comment-score"></div><div class="comment-text"><p>I.e., цто же FDST? (По английский, пожалуйста - your English is better than my Russian, because I studied Russian more than 40 years ago and I've forgotten too much of it, and others might not understand Russian at all.)</p></div><div id="comment-17109-info" class="comment-info"><span class="comment-age">(20 Dec '12, 12:26)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="17114"></span><div id="comment-17114" class="comment"><div id="post-17114-score" class="comment-score"></div><div class="comment-text"><p>Ok, thanks for the answer. I can send to you a capture file, if you tell me your email. The main problem - my pdf about FDST is on russian. The second problem - its no wide used protocol. If you want detailed description, it would not so easy to get.</p><p>Also, i want to thank you for your iec-104 supporting. It's much better, than iectest and other products (but HOW it possible ?!).</p></div><div id="comment-17114-info" class="comment-info"><span class="comment-age">(20 Dec '12, 20:21)</span> <span class="comment-user userinfo">Curecool</span></div></div><span id="17117"></span><div id="comment-17117" class="comment"><div id="post-17117-score" class="comment-score"></div><div class="comment-text"><p>please add that information in an enhancement bug at <a href="http://bugs.wireshark.org">bugs.wireshark.org</a>. However, with specs only in russian and limited information about the protocol, your chances of getting the protocol included are rather limited.</p><p>Did you think about adding it yourselves?</p><p>There is a generic dissector available, called WSGD.</p><blockquote><p><code>http://wsgd.free.fr/</code><br />
</p></blockquote><p>With WSGD you can try to add the protocol yourself.</p></div><div id="comment-17117-info" class="comment-info"><span class="comment-age">(20 Dec '12, 22:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="17125"></span><div id="comment-17125" class="comment"><div id="post-17125-score" class="comment-score"></div><div class="comment-text"><p>A little digging gave up this snippet of info: "CK-2007 is able to communicate with external systems via an array of protocols: IEC 60870-5-101, DNPv3.00, Modbus, IEC 60870-5-104, FDST, OPC.". So this is in the realm of process control.</p></div><div id="comment-17125-info" class="comment-info"><span class="comment-age">(21 Dec '12, 01:38)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="17126"></span><div id="comment-17126" class="comment"><div id="post-17126-score" class="comment-score"></div><div class="comment-text"><p>I am working with CK-2003 (similar to ck 2007). I'll try WSGD, maybe i not so stupid... no, I'm stupid as hell. I'll write about the results later.</p><p>But anyway, thank you for your help.</p></div><div id="comment-17126-info" class="comment-info"><span class="comment-age">(21 Dec '12, 02:26)</span> <span class="comment-user userinfo">Curecool</span></div></div><span id="17129"></span><div id="comment-17129" class="comment not_top_scorer"><div id="post-17129-score" class="comment-score"></div><div class="comment-text"><p>good luck.</p><p>Hint: If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions.</p></div><div id="comment-17129-info" class="comment-info"><span class="comment-age">(21 Dec '12, 03:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-17082" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-17082-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

