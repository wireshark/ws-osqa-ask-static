+++
type = "question"
title = "Regarding analysing 11n packets on wireshark"
description = '''hi, I see a list of over around 10-12 consecutive packets(without any mac in the middle) with the same exact mac timestamp and a block ack after all these packets. Does this mean all these packets were sent as a single block after aggregation? It will be really useful if anyone can throw some light ...'''
date = "2012-05-11T13:41:00Z"
lastmod = "2012-05-11T13:41:00Z"
weight = 10949
keywords = [ "msdu", "wireshark", "802.11n" ]
aliases = [ "/questions/10949" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Regarding analysing 11n packets on wireshark](/questions/10949/regarding-analysing-11n-packets-on-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10949-score" class="post-score" title="current number of votes">0</div><span id="post-10949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, I see a list of over around 10-12 consecutive packets(without any mac in the middle) with the same exact mac timestamp and a block ack after all these packets. Does this mean all these packets were sent as a single block after aggregation? It will be really useful if anyone can throw some light over it. Would MIMO have anything to do with this? Looking forward to some suggestions.</p><p>Thank you Srinivas</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-msdu" rel="tag" title="see questions tagged &#39;msdu&#39;">msdu</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-802.11n" rel="tag" title="see questions tagged &#39;802.11n&#39;">802.11n</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 May '12, 13:41</strong></p><img src="https://secure.gravatar.com/avatar/2618453e9194929f291128d350b2a4ec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srini_wisc&#39;s gravatar image" /><p><span>srini_wisc</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srini_wisc has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Jun '12, 19:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-10949" class="comments-container"></div><div id="comment-tools-10949" class="comment-tools"></div><div class="clear"></div><div id="comment-10949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

