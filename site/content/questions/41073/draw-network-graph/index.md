+++
type = "question"
title = "Draw network graph"
description = '''How can I draw a network graph using Wireshark?'''
date = "2015-03-31T18:40:00Z"
lastmod = "2015-04-06T03:03:00Z"
weight = 41073
keywords = [ "graph", "network" ]
aliases = [ "/questions/41073" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Draw network graph](/questions/41073/draw-network-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41073-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41073-score" class="post-score" title="current number of votes">0</div><span id="post-41073-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I draw a network graph using Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '15, 18:40</strong></p><img src="https://secure.gravatar.com/avatar/fa1e339a7494976f25c779d187cbfcaa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sandy6933&#39;s gravatar image" /><p><span>sandy6933</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sandy6933 has no accepted answers">0%</span></p></div></div><div id="comments-container-41073" class="comments-container"><span id="41077"></span><div id="comment-41077" class="comment"><div id="post-41077-score" class="comment-score"></div><div class="comment-text"><p>Please define what you expect to see in a "network graph".</p></div><div id="comment-41077-info" class="comment-info"><span class="comment-age">(01 Apr '15, 01:05)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-41073" class="comment-tools"></div><div class="clear"></div><div id="comment-41073-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41206"></span>

<div id="answer-container-41206" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41206-score" class="post-score" title="current number of votes">0</div><span id="post-41206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><span>@sandy6933</span>, if you still here, please see my answer to a similar question (converted from a comment to your question):</p><blockquote><p><a href="https://ask.wireshark.org/questions/41185/how-can-i-draw-a-network-graph-with-wireshark">https://ask.wireshark.org/questions/41185/how-can-i-draw-a-network-graph-with-wireshark</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '15, 03:03</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-41206" class="comments-container"></div><div id="comment-tools-41206" class="comment-tools"></div><div class="clear"></div><div id="comment-41206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

