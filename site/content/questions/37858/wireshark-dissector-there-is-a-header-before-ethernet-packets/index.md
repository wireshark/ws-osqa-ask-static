+++
type = "question"
title = "Wireshark dissector: There is a header before Ethernet packets"
description = '''I have my own protocol but I don&#x27;t know how to create my own wireshark dissector.. Ethernet packets looks like: [Dst MacAddress][Src MacAddress][Ethertype].....  My protocol add a header: [My protocol header][Dst MacAddress][Src MacAddress][Ethertype].....  I want to dissect the header and use the E...'''
date = "2014-11-14T06:08:00Z"
lastmod = "2014-11-14T06:08:00Z"
weight = 37858
keywords = [ "dissector" ]
aliases = [ "/questions/37858" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark dissector: There is a header before Ethernet packets](/questions/37858/wireshark-dissector-there-is-a-header-before-ethernet-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37858-score" class="post-score" title="current number of votes">0</div><span id="post-37858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have my own protocol but I don't know how to create my own wireshark dissector.. Ethernet packets looks like:</p><pre><code>[Dst MacAddress][Src MacAddress][Ethertype].....</code></pre><p>My protocol add a header:</p><pre><code>[My protocol header][Dst MacAddress][Src MacAddress][Ethertype].....</code></pre><p>I want to dissect the header and use the Ethernet Protocol to dissect the remaining, what should I do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '14, 06:08</strong></p><img src="https://secure.gravatar.com/avatar/75d7cce555a5e641e386c497420b173a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kynton_yu&#39;s gravatar image" /><p><span>kynton_yu</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kynton_yu has no accepted answers">0%</span></p></div></div><div id="comments-container-37858" class="comments-container"></div><div id="comment-tools-37858" class="comment-tools"></div><div class="clear"></div><div id="comment-37858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

