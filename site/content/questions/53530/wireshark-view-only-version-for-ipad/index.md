+++
type = "question"
title = "Wireshark view only version for ipad."
description = '''I am a network engineer. I often need to analyze packet captures but not capture packet s on my ipad. Has anyone considered a view only version of wireshark for ipad or other platforms that has all the features of wireshark without packet capture abilities? I don&#x27;t see cloudshark as an option as I w...'''
date = "2016-06-17T06:18:00Z"
lastmod = "2016-06-17T07:03:00Z"
weight = 53530
keywords = [ "ipad", "view-only" ]
aliases = [ "/questions/53530" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark view only version for ipad.](/questions/53530/wireshark-view-only-version-for-ipad)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53530-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53530-score" class="post-score" title="current number of votes">0</div><span id="post-53530-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a network engineer. I often need to analyze packet captures but not capture packet s on my ipad. Has anyone considered a view only version of wireshark for ipad or other platforms that has all the features of wireshark without packet capture abilities? I don't see cloudshark as an option as I would like to avoid another monthly subscription.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipad" rel="tag" title="see questions tagged &#39;ipad&#39;">ipad</span> <span class="post-tag tag-link-view-only" rel="tag" title="see questions tagged &#39;view-only&#39;">view-only</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '16, 06:18</strong></p><img src="https://secure.gravatar.com/avatar/dc3d17f5abcfbc54f598e6f465e2afad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gruff07&#39;s gravatar image" /><p><span>Gruff07</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gruff07 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Jun '16, 07:31</strong> </span></p></div></div><div id="comments-container-53530" class="comments-container"></div><div id="comment-tools-53530" class="comment-tools"></div><div class="clear"></div><div id="comment-53530-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53533"></span>

<div id="answer-container-53533" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53533-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53533-score" class="post-score" title="current number of votes">0</div><span id="post-53533-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://enterprise.cloudshark.org/">CloudShark Enterprise</a></p><p><a href="https://www.cloudshark.org/">CloudShark</a></p><p>Should cover it I suppose.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jun '16, 06:40</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-53533" class="comments-container"><span id="53534"></span><div id="comment-53534" class="comment"><div id="post-53534-score" class="comment-score"></div><div class="comment-text"><p>Agreed, however I would like to avoid another monthly subscription.</p></div><div id="comment-53534-info" class="comment-info"><span class="comment-age">(17 Jun '16, 07:03)</span> <span class="comment-user userinfo">Gruff07</span></div></div></div><div id="comment-tools-53533" class="comment-tools"></div><div class="clear"></div><div id="comment-53533-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

