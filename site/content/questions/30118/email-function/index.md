+++
type = "question"
title = "Email function"
description = '''I want to use wireshark to monitor the network of my computer, it should be 7X24. I hope that if wireshark sniffed certain packets thus send the alert email to me. Is it possible to do that? How can I do that?'''
date = "2014-02-24T00:36:00Z"
lastmod = "2014-02-24T04:30:00Z"
weight = 30118
keywords = [ "email" ]
aliases = [ "/questions/30118" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Email function](/questions/30118/email-function)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30118-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30118-score" class="post-score" title="current number of votes">0</div><span id="post-30118-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to use wireshark to monitor the network of my computer, it should be 7X24. I hope that if wireshark sniffed certain packets thus send the alert email to me. Is it possible to do that? How can I do that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '14, 00:36</strong></p><img src="https://secure.gravatar.com/avatar/a5a6c625566879c5ff05221850264844?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saiwlam2&#39;s gravatar image" /><p><span>saiwlam2</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saiwlam2 has no accepted answers">0%</span></p></div></div><div id="comments-container-30118" class="comments-container"><span id="30125"></span><div id="comment-30125" class="comment"><div id="post-30125-score" class="comment-score">1</div><div class="comment-text"><p><a href="http://natpryce.com/articles/000749.html#letts">Lett's law</a> in effect here.</p></div><div id="comment-30125-info" class="comment-info"><span class="comment-age">(24 Feb '14, 04:30)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-30118" class="comment-tools"></div><div class="clear"></div><div id="comment-30118-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30124"></span>

<div id="answer-container-30124" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30124-score" class="post-score" title="current number of votes">1</div><span id="post-30124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not really, Wireshark (and the rest of the Wireshark suite) is a packet analyzer, not a traffic monitoring system.</p><p>While you could cobble something together using dumpcap (neither Wireshark or tshark are suited to continuous capture as they will run out of memory) and post capture scripts to send emails, IMHO you will be much better off looking for another solution that better fits your requirements.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '14, 02:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-30124" class="comments-container"></div><div id="comment-tools-30124" class="comment-tools"></div><div class="clear"></div><div id="comment-30124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

