+++
type = "question"
title = "Is this hacking packets ?"
description = '''Do this packets are hacking packets ? I&#x27;ve some problem on my LAN so i decieded to install wireshark to be exactly sure of what happens. My IP is the 192.168.1.55 and i have some suspicion on the 192.168.1.51. Is anyone able to interpret this packets to help me ? Thanks. Mega link .pcapng'''
date = "2017-03-27T15:57:00Z"
lastmod = "2017-03-28T05:54:00Z"
weight = 60379
keywords = [ "suspicious" ]
aliases = [ "/questions/60379" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is this hacking packets ?](/questions/60379/is-this-hacking-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60379-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60379-score" class="post-score" title="current number of votes">0</div><span id="post-60379-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Do this packets are hacking packets ?</p><p>I've some problem on my LAN so i decieded to install wireshark to be exactly sure of what happens. My IP is the 192.168.1.55 and i have some suspicion on the 192.168.1.51.</p><p>Is anyone able to interpret this packets to help me ?</p><p>Thanks.</p><p><a href="https://mega.nz/#!RgwGyarT!h1-yy85dx7NJijKnFoNAThVsBZTzstEhkgxsDMYEU7Y">Mega link .pcapng</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-suspicious" rel="tag" title="see questions tagged &#39;suspicious&#39;">suspicious</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '17, 15:57</strong></p><img src="https://secure.gravatar.com/avatar/e65e22321e1a323a1db68ca379a9885e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="W4RdZ&#39;s gravatar image" /><p><span>W4RdZ</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="W4RdZ has no accepted answers">0%</span></p></div></div><div id="comments-container-60379" class="comments-container"></div><div id="comment-tools-60379" class="comment-tools"></div><div class="clear"></div><div id="comment-60379-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60380"></span>

<div id="answer-container-60380" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60380-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60380-score" class="post-score" title="current number of votes">1</div><span id="post-60380-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I doubt that those are "hacking packets" - they're pretty normal in a network: some session termination (reset packets) for connections that started way before you started recording, and some SMB stuff (Windows file share etc.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Mar '17, 16:01</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Mar '17, 16:01</strong> </span></p></div></div><div id="comments-container-60380" class="comments-container"><span id="60381"></span><div id="comment-60381" class="comment"><div id="post-60381-score" class="comment-score"></div><div class="comment-text"><p>ok now i'm sure he was hacking me by p2pover, is any solution to block that software ? I mean is there a program to block arp attacks</p></div><div id="comment-60381-info" class="comment-info"><span class="comment-age">(27 Mar '17, 17:18)</span> <span class="comment-user userinfo">W4RdZ</span></div></div><span id="60383"></span><div id="comment-60383" class="comment"><div id="post-60383-score" class="comment-score"></div><div class="comment-text"><p>What brings you to the conclusion that p2pover is being used to hack you?</p></div><div id="comment-60383-info" class="comment-info"><span class="comment-age">(28 Mar '17, 02:02)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="60385"></span><div id="comment-60385" class="comment"><div id="post-60385-score" class="comment-score"></div><div class="comment-text"><p>my bro gaves me a screen of without the menu bar i just used google image to found what software it was but now i've got a question, how to protect from this : <a href="https://mega.nz/#!FlBBhY7J!_9Kf6fqTA-Z9V0nDLitAmbMBWiqxERZDVzjYaznaK2Y">ARP attack .pcapng</a></p><p>Sry but i'm a newb in network :(</p></div><div id="comment-60385-info" class="comment-info"><span class="comment-age">(28 Mar '17, 05:54)</span> <span class="comment-user userinfo">W4RdZ</span></div></div></div><div id="comment-tools-60380" class="comment-tools"></div><div class="clear"></div><div id="comment-60380-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

