+++
type = "question"
title = "Ip banned to access your web site"
description = '''Dear,  My ip has been banned to access your web site.  I would like to know why, and are there something I can do. '''
date = "2017-07-03T08:30:00Z"
lastmod = "2017-07-03T11:33:00Z"
weight = 62473
keywords = [ "banned" ]
aliases = [ "/questions/62473" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ip banned to access your web site](/questions/62473/ip-banned-to-access-your-web-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62473-score" class="post-score" title="current number of votes">0</div><span id="post-62473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear,</p><p>My ip has been banned to access your web site.</p><p>I would like to know why, and are there something I can do.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-banned" rel="tag" title="see questions tagged &#39;banned&#39;">banned</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '17, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/776cf3d4cd24acb0957ebd3a55fed07a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="amfm0777&#39;s gravatar image" /><p><span>amfm0777</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="amfm0777 has no accepted answers">0%</span></p></div></div><div id="comments-container-62473" class="comments-container"><span id="62478"></span><div id="comment-62478" class="comment"><div id="post-62478-score" class="comment-score"></div><div class="comment-text"><p>How/why do you think the IP is banned? What message do you get (from what site) or what evidence do you have that it's "banned?"</p></div><div id="comment-62478-info" class="comment-info"><span class="comment-age">(03 Jul '17, 10:44)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="62479"></span><div id="comment-62479" class="comment"><div id="post-62479-score" class="comment-score"></div><div class="comment-text"><p>Hi, The message displayed is :</p><p>"What happened? The owner of this website (www.wireshark.org) has banned your IP address (x.x.x.x)."</p></div><div id="comment-62479-info" class="comment-info"><span class="comment-age">(03 Jul '17, 11:33)</span> <span class="comment-user userinfo">amfm0777</span></div></div></div><div id="comment-tools-62473" class="comment-tools"></div><div class="clear"></div><div id="comment-62473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

