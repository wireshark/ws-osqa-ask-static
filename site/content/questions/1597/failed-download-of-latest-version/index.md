+++
type = "question"
title = "failed download of latest version"
description = '''I&#x27;m trying to download the most recent version of wireshark but get a 0kb non-executable file. Any ideas what the problem might be?'''
date = "2011-01-03T07:37:00Z"
lastmod = "2011-01-03T20:35:00Z"
weight = 1597
keywords = [ "download" ]
aliases = [ "/questions/1597" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [failed download of latest version](/questions/1597/failed-download-of-latest-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1597-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1597-score" class="post-score" title="current number of votes">0</div><span id="post-1597-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to download the most recent version of wireshark but get a 0kb non-executable file. Any ideas what the problem might be?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jan '11, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/eeb1b1b7a9a34b86e01289c394ea62b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="securityfan&#39;s gravatar image" /><p><span>securityfan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="securityfan has no accepted answers">0%</span></p></div></div><div id="comments-container-1597" class="comments-container"><span id="1598"></span><div id="comment-1598" class="comment"><div id="post-1598-score" class="comment-score"></div><div class="comment-text"><p>What URL are you using to download? Have you tried browsing the download area at http://media-2.cacetech.com/wireshark/?</p></div><div id="comment-1598-info" class="comment-info"><span class="comment-age">(03 Jan '11, 07:46)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="1606"></span><div id="comment-1606" class="comment"><div id="post-1606-score" class="comment-score"></div><div class="comment-text"><p>Gerald,</p><p>Thanks for responding, I originally tried to download from Wireshark and I have tried your suggestion and still get 0KB file.</p></div><div id="comment-1606-info" class="comment-info"><span class="comment-age">(03 Jan '11, 13:36)</span> <span class="comment-user userinfo">securityfan</span></div></div><span id="1609"></span><div id="comment-1609" class="comment"><div id="post-1609-score" class="comment-score"></div><div class="comment-text"><p>Do you have any antivirus software in place or a proxy server that might interfere with the download? Can you download from ftp://ftp.wireshark.org/?</p></div><div id="comment-1609-info" class="comment-info"><span class="comment-age">(03 Jan '11, 16:21)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-1597" class="comment-tools"></div><div class="clear"></div><div id="comment-1597-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1603"></span>

<div id="answer-container-1603" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1603-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1603-score" class="post-score" title="current number of votes">0</div><span id="post-1603-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>try a download manager</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jan '11, 13:09</strong></p><img src="https://secure.gravatar.com/avatar/7df3f9a4b16eae9f77feb6eabe92919e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eelarry&#39;s gravatar image" /><p><span>eelarry</span><br />
<span class="score" title="36 reputation points">36</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eelarry has no accepted answers">0%</span></p></div></div><div id="comments-container-1603" class="comments-container"><span id="1613"></span><div id="comment-1613" class="comment"><div id="post-1613-score" class="comment-score"></div><div class="comment-text"><p>eelarry,</p><p>Thanks for responding. I tried downloading a download manager and got a 0kb file so the problem seems to be a more general one. I've tried a number of other application downloads and got the same response. Documents (e.g pdf) download o.k. Since this isn't specific to wireshark, I'll start looking elsewhere</p></div><div id="comment-1613-info" class="comment-info"><span class="comment-age">(03 Jan '11, 20:35)</span> <span class="comment-user userinfo">securityfan</span></div></div></div><div id="comment-tools-1603" class="comment-tools"></div><div class="clear"></div><div id="comment-1603-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

