+++
type = "question"
title = "Detecting wireshark"
description = '''Can someone install wireshark on my home WiFi network if they are able to get into it (ie., a guest in my home who used my WiFi on a cell phone)? Is there any way to detect whether they did this? Thank you.'''
date = "2015-12-11T10:27:00Z"
lastmod = "2015-12-11T13:06:00Z"
weight = 48457
keywords = [ "detect" ]
aliases = [ "/questions/48457" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Detecting wireshark](/questions/48457/detecting-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48457-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48457-score" class="post-score" title="current number of votes">0</div><span id="post-48457-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone install wireshark on my home WiFi network if they are able to get into it (ie., a guest in my home who used my WiFi on a cell phone)? Is there any way to detect whether they did this? Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-detect" rel="tag" title="see questions tagged &#39;detect&#39;">detect</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Dec '15, 10:27</strong></p><img src="https://secure.gravatar.com/avatar/825b242e7f22845ca36254a94ff523b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tiffany&#39;s gravatar image" /><p><span>Tiffany</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tiffany has no accepted answers">0%</span></p></div></div><div id="comments-container-48457" class="comments-container"></div><div id="comment-tools-48457" class="comment-tools"></div><div class="clear"></div><div id="comment-48457-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48462"></span>

<div id="answer-container-48462" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48462-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48462-score" class="post-score" title="current number of votes">0</div><span id="post-48462-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In general - yes, a packet sniffing tool, even if not exactly Wireshark, may be installed not only to computers but also to some types of home routers (which are actually small computers as well). However, it requires quite some effort even if the particular model of the router allows it.</p><p>Do you have reasons to believe that it has happened in your case? Because if we talk about the home router, it is hard to find out, but should be easy to get rid of it by re-flashing the router with the original firmware.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Dec '15, 13:06</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Dec '15, 13:08</strong> </span></p></div></div><div id="comments-container-48462" class="comments-container"></div><div id="comment-tools-48462" class="comment-tools"></div><div class="clear"></div><div id="comment-48462-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

