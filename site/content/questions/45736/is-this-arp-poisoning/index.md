+++
type = "question"
title = "Is this ARP poisoning?"
description = '''I have this in my capture. Is this an ARP attack ? &quot;4&quot;,&quot;16.479870000&quot;,&quot;Zhongxin_fd:04:38&quot;,&quot;Spanning-tree-(for-bridges)_09&quot;,&quot;LLC&quot;,&quot;242&quot;,&quot;[Malformed Packet]&quot; &quot;5&quot;,&quot;29.190352000&quot;,&quot;0.0.0.0&quot;,&quot;255.255.255.255&quot;,&quot;DHCP&quot;,&quot;590&quot;,&quot;DHCP Discover - Transaction ID 0xc0a0c949&quot; &quot;6&quot;,&quot;32.250335000&quot;,&quot;0.0.0.0&quot;,&quot;255.255.25...'''
date = "2015-09-09T05:22:00Z"
lastmod = "2015-09-11T07:55:00Z"
weight = 45736
keywords = [ "arp", "poisoning" ]
aliases = [ "/questions/45736" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Is this ARP poisoning?](/questions/45736/is-this-arp-poisoning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45736-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45736-score" class="post-score" title="current number of votes">0</div><span id="post-45736-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have this in my capture. Is this an ARP attack ?</p><pre><code>&quot;4&quot;,&quot;16.479870000&quot;,&quot;Zhongxin_fd:04:38&quot;,&quot;Spanning-tree-(for-bridges)_09&quot;,&quot;LLC&quot;,&quot;242&quot;,&quot;[Malformed Packet]&quot;
&quot;5&quot;,&quot;29.190352000&quot;,&quot;0.0.0.0&quot;,&quot;255.255.255.255&quot;,&quot;DHCP&quot;,&quot;590&quot;,&quot;DHCP Discover - Transaction ID 0xc0a0c949&quot;
&quot;6&quot;,&quot;32.250335000&quot;,&quot;0.0.0.0&quot;,&quot;255.255.255.255&quot;,&quot;DHCP&quot;,&quot;590&quot;,&quot;DHCP Discover - Transaction ID 0xc0a0c949&quot;
&quot;7&quot;,&quot;35.310368000&quot;,&quot;0.0.0.0&quot;,&quot;255.255.255.255&quot;,&quot;DHCP&quot;,&quot;590&quot;,&quot;DHCP Discover - Transaction ID 0xc0a0c949&quot;
&quot;8&quot;,&quot;42.406078000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;9&quot;,&quot;42.406114000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;10&quot;,&quot;42.406149000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;11&quot;,&quot;42.406177000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;12&quot;,&quot;42.406227000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;13&quot;,&quot;42.406263000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;14&quot;,&quot;42.406298000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;15&quot;,&quot;42.406325000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;16&quot;,&quot;42.406374000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;17&quot;,&quot;42.406410000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;18&quot;,&quot;42.406443000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;19&quot;,&quot;42.406469000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;20&quot;,&quot;43.163592000&quot;,&quot;Hangzhou_07:f2:e0&quot;,&quot;Spanning-tree-(for-bridges)_0a&quot;,&quot;0x88a7&quot;,&quot;160&quot;,&quot;Ethernet II&quot;
&quot;21&quot;,&quot;46.480676000&quot;,&quot;Zhongxin_fd:04:38&quot;,&quot;Spanning-tree-(for-bridges)_09&quot;,&quot;LLC&quot;,&quot;242&quot;,&quot;[Malformed Packet]&quot;
&quot;25&quot;,&quot;69.106271000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;26&quot;,&quot;69.106347000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;27&quot;,&quot;69.106382000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;28&quot;,&quot;69.106409000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;29&quot;,&quot;69.106492000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;30&quot;,&quot;69.106528000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;31&quot;,&quot;69.106562000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;32&quot;,&quot;69.106586000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;33&quot;,&quot;69.106639000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;34&quot;,&quot;69.106674000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;35&quot;,&quot;69.106709000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;36&quot;,&quot;69.106734000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;Tp-LinkT_4d:76:bd&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 1.0.118.188?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;
&quot;37&quot;,&quot;76.481479000&quot;,&quot;Zhongxin_fd:04:38&quot;,&quot;Spanning-tree-(for-bridges)_09&quot;,&quot;LLC&quot;,&quot;242&quot;,&quot;[Malformed Packet]&quot;</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span> <span class="post-tag tag-link-poisoning" rel="tag" title="see questions tagged &#39;poisoning&#39;">poisoning</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '15, 05:22</strong></p><img src="https://secure.gravatar.com/avatar/09969f35aabf81c9d74f3d55824861cd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ciohap22&#39;s gravatar image" /><p><span>Ciohap22</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ciohap22 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Sep '15, 06:54</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-45736" class="comments-container"></div><div id="comment-tools-45736" class="comment-tools"></div><div class="clear"></div><div id="comment-45736-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45773"></span>

<div id="answer-container-45773" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45773-score" class="post-score" title="current number of votes">2</div><span id="post-45773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ciohap22 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think it's ARP spoofing, as the requests a simply "crappy".</p><pre><code>&quot;8&quot;,&quot;42.406078000&quot;,&quot;Zte_5f:f8:67&quot;,&quot;TainetCo_12:fe:09&quot;,&quot;ARP&quot;,&quot;64&quot;,&quot;Who has 10.117.3.82?  Tell 192.168.1.1 [ETHERNET FRAME CHECK SEQUENCE INCORRECT]&quot;</code></pre><p>It does not make sense to ask for 10.117.3.82 in the network where 192.168.1.1 is part of, so I see the following possible reasons/problems:</p><ul><li>a broken device is asking for totally wrong things</li><li>somebody is playing tricks with you</li><li>a broken device sends arbitrary/random data to the network, that just look like ARP requests</li></ul><p>The last item looks pretty reasonable to me, given all the FCS (FRAME CHECK SEQUENCE) errors in every single ARP frame.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Sep '15, 18:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-45773" class="comments-container"><span id="45791"></span><div id="comment-45791" class="comment"><div id="post-45791-score" class="comment-score"></div><div class="comment-text"><p>Ok. Thank you.</p></div><div id="comment-45791-info" class="comment-info"><span class="comment-age">(11 Sep '15, 07:55)</span> <span class="comment-user userinfo">Ciohap22</span></div></div></div><div id="comment-tools-45773" class="comment-tools"></div><div class="clear"></div><div id="comment-45773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

