+++
type = "question"
title = "wireshark on lineage os"
description = '''how can i add wireshark to Lineage os?'''
date = "2017-02-01T07:57:00Z"
lastmod = "2017-02-01T09:55:00Z"
weight = 59223
keywords = [ "lineageos" ]
aliases = [ "/questions/59223" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark on lineage os](/questions/59223/wireshark-on-lineage-os)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59223-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59223-score" class="post-score" title="current number of votes">0</div><span id="post-59223-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i add wireshark to Lineage os?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lineageos" rel="tag" title="see questions tagged &#39;lineageos&#39;">lineageos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '17, 07:57</strong></p><img src="https://secure.gravatar.com/avatar/bbbb81207669ef66efe61661ac2d749a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oussama&#39;s gravatar image" /><p><span>oussama</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oussama has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Feb '17, 08:00</strong> </span></p></div></div><div id="comments-container-59223" class="comments-container"></div><div id="comment-tools-59223" class="comment-tools"></div><div class="clear"></div><div id="comment-59223-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59228"></span>

<div id="answer-container-59228" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59228-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59228-score" class="post-score" title="current number of votes">1</div><span id="post-59228-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>how can i add wireshark to Lineage os?</p></blockquote><p>By getting all the support libraries required by Wireshark, including Qt (or GTK+, but we're not necessarily going to support GTK+ forever) working on Lineage OS, and getting a machine running Lineage OS with a screen large enough that Wireshark works (hint: that probably means "not a mobile phone") or redesigning the Wireshark UI to work on a smaller screen.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '17, 09:55</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-59228" class="comments-container"></div><div id="comment-tools-59228" class="comment-tools"></div><div class="clear"></div><div id="comment-59228-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

