+++
type = "question"
title = "Decryption Help"
description = '''Hi, could someone help me with decryption? It does not seem to be working.  My network is secured with WPA2 Password. I chose WPA-PWD, entered the SSID and Password. Decryption is not working. All I get are encrypted 802.11 packets.  I have messed around with &quot;ignore the protection bit&quot; and &quot;assume ...'''
date = "2014-09-14T16:12:00Z"
lastmod = "2014-09-15T13:20:00Z"
weight = 36314
keywords = [ "decryption", "wireshark" ]
aliases = [ "/questions/36314" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Decryption Help](/questions/36314/decryption-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36314-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36314-score" class="post-score" title="current number of votes">0</div><span id="post-36314-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, could someone help me with decryption? It does not seem to be working.</p><p>My network is secured with WPA2 Password. I chose WPA-PWD, entered the SSID and Password.</p><p>Decryption is not working. All I get are encrypted 802.11 packets.</p><p>I have messed around with "ignore the protection bit" and "assume packets have FCS" with no success.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '14, 16:12</strong></p><img src="https://secure.gravatar.com/avatar/1bbd4da42a0ff1b36031110feb9435b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dragon56&#39;s gravatar image" /><p><span>dragon56</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dragon56 has no accepted answers">0%</span></p></div></div><div id="comments-container-36314" class="comments-container"></div><div id="comment-tools-36314" class="comment-tools"></div><div class="clear"></div><div id="comment-36314-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36315"></span>

<div id="answer-container-36315" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36315-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36315-score" class="post-score" title="current number of votes">1</div><span id="post-36315-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="dragon56 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look at the answer by <span>@landi</span> here: <a href="https://ask.wireshark.org/questions/36158/how-can-i-capture-http-on-wlan-with-airpcap">https://ask.wireshark.org/questions/36158/how-can-i-capture-http-on-wlan-with-airpcap</a></p><p>I guess it's the same thing.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '14, 16:18</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-36315" class="comments-container"><span id="36335"></span><div id="comment-36335" class="comment"><div id="post-36335-score" class="comment-score"></div><div class="comment-text"><p>Thanks so much! Working fine now.</p></div><div id="comment-36335-info" class="comment-info"><span class="comment-age">(15 Sep '14, 13:20)</span> <span class="comment-user userinfo">dragon56</span></div></div></div><div id="comment-tools-36315" class="comment-tools"></div><div class="clear"></div><div id="comment-36315-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

