+++
type = "question"
title = "wirehark can&#x27;t recognize GSM-MAP command SearchForMS"
description = '''Dear All,  I would like to ask about GSM MAP command like &quot;SearchForMS, &quot;SetCipheringMode&quot;, &quot;Paging&quot;, &quot;ForwardNewTMSI&quot;,&quot;ObtainIMEI&quot;, why it&#x27;s so difficult to find a complete reference, even in 3gpp 29.002 document or ETSI, and why wireshark can&#x27;t recognize all the commands?? Thank you,'''
date = "2014-01-10T17:28:00Z"
lastmod = "2014-01-14T19:59:00Z"
weight = 28789
keywords = [ "searchforms", "gsm-map" ]
aliases = [ "/questions/28789" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wirehark can't recognize GSM-MAP command SearchForMS](/questions/28789/wirehark-cant-recognize-gsm-map-command-searchforms)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28789-score" class="post-score" title="current number of votes">0</div><span id="post-28789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear All, I would like to ask about GSM MAP command like "SearchForMS, "SetCipheringMode", "Paging", "ForwardNewTMSI","ObtainIMEI", why it's so difficult to find a complete reference, even in 3gpp 29.002 document or ETSI, and why wireshark can't recognize all the commands??</p><p>Thank you,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-searchforms" rel="tag" title="see questions tagged &#39;searchforms&#39;">searchforms</span> <span class="post-tag tag-link-gsm-map" rel="tag" title="see questions tagged &#39;gsm-map&#39;">gsm-map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jan '14, 17:28</strong></p><img src="https://secure.gravatar.com/avatar/b9fb8978b2c1a1bbe114d77d154b9ac7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andry%20sunandar&#39;s gravatar image" /><p><span>andry sunandar</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andry sunandar has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jan '14, 18:04</strong> </span></p></div></div><div id="comments-container-28789" class="comments-container"><span id="28824"></span><div id="comment-28824" class="comment"><div id="post-28824-score" class="comment-score"></div><div class="comment-text"><p>Possibly because it's some proprietary extensions (or not GSM MAP)and not part of the standard? Do you have an example trace? which operation codes do you Think belongs to those messages?</p></div><div id="comment-28824-info" class="comment-info"><span class="comment-age">(12 Jan '14, 10:24)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="28894"></span><div id="comment-28894" class="comment"><div id="post-28894-score" class="comment-score"></div><div class="comment-text"><p>yes it's a GSM-MAP Command based on 3GPP TS 29.002 V12.1.0 (2013-06) Technical Specification (Release 12) page 129 and page 318 for list of operation codes for all GSM-MAP commands, and SearchForMS, SetCipheringMode, Paging, ForwardNewTMS, ObtainIMEI is doesn't have any operation code number and application context number.</p><p>Thank you,</p></div><div id="comment-28894-info" class="comment-info"><span class="comment-age">(14 Jan '14, 19:59)</span> <span class="comment-user userinfo">andry sunandar</span></div></div></div><div id="comment-tools-28789" class="comment-tools"></div><div class="clear"></div><div id="comment-28789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

