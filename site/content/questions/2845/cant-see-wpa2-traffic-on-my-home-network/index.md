+++
type = "question"
title = "Can&#x27;t see WPA2 traffic on my home network"
description = '''Hi, I&#x27;m attempting to sniff traffic on my home network for which I know the SSID and WPA2 (Personal) key. I&#x27;m running 64bit Wireshark under Windows 7, and my wireless card is an Atheros AR9285. From reading other forums, I&#x27;ve seen other people having at least some success with this card. I have foll...'''
date = "2011-03-15T15:38:00Z"
lastmod = "2011-03-15T15:38:00Z"
weight = 2845
keywords = [ "decryption", "wpa2" ]
aliases = [ "/questions/2845" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see WPA2 traffic on my home network](/questions/2845/cant-see-wpa2-traffic-on-my-home-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2845-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2845-score" class="post-score" title="current number of votes">0</div><span id="post-2845-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm attempting to sniff traffic on my home network for which I know the SSID and WPA2 (Personal) key. I'm running 64bit Wireshark under Windows 7, and my wireless card is an Atheros AR9285. From reading other forums, I've seen other people having at least some success with this card.</p><p>I have followed the instructions here: http://wiki.wireshark.org/HowToDecrypt802.11</p><p>In doing so, I've supplied Wireshark my wpa-pwd as shown. Also, I've checked the Enable Decryption checkbox, and toggled back and forth Assume Packets Have FCS and Ignore the Protection bit.</p><p>After starting my capture and connecting another PC to the network, I see it's DHCP request, but no EAPOL packets, and of course no further TCP traffic from that address since it didn't get the handshake packets.</p><p>Am I doing something obviously wrong? Thank you for your time.</p><p>Craig</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '11, 15:38</strong></p><img src="https://secure.gravatar.com/avatar/46e4301a32076d197a37e9007489c1e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="milesc&#39;s gravatar image" /><p><span>milesc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="milesc has no accepted answers">0%</span></p></div></div><div id="comments-container-2845" class="comments-container"></div><div id="comment-tools-2845" class="comment-tools"></div><div class="clear"></div><div id="comment-2845-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

