+++
type = "question"
title = "text display looks fuzzy in new Version 2.2.0 on Windows 7"
description = '''The display of almost all text in Version 2.2.0 looks terrible. This was never a problem in any earlier version. Almost all text looks fuzzy. Many letters and digits look like they were partially erased.'''
date = "2016-10-03T14:28:00Z"
lastmod = "2016-10-04T07:00:00Z"
weight = 56111
keywords = [ "text", "display" ]
aliases = [ "/questions/56111" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [text display looks fuzzy in new Version 2.2.0 on Windows 7](/questions/56111/text-display-looks-fuzzy-in-new-version-220-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56111-score" class="post-score" title="current number of votes">0</div><span id="post-56111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The display of almost all text in Version 2.2.0 looks terrible. This was never a problem in any earlier version. Almost all text looks fuzzy. Many letters and digits look like they were partially erased.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '16, 14:28</strong></p><img src="https://secure.gravatar.com/avatar/131564ef229af4a254b06cf509a00351?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="johnb&#39;s gravatar image" /><p><span>johnb</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="johnb has no accepted answers">0%</span></p></div></div><div id="comments-container-56111" class="comments-container"></div><div id="comment-tools-56111" class="comment-tools"></div><div class="clear"></div><div id="comment-56111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56134"></span>

<div id="answer-container-56134" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56134-score" class="post-score" title="current number of votes">0</div><span id="post-56134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Feel free to choose <code>Edit -&gt; Preferences -&gt; Appearance -&gt; Font and Colors</code> and change the <code>Main window font</code> from whatever you find there (most likely, <code>Consolas</code>) to <code>Lucida Console</code> to restore the look you are used to. And don't ever migrate to Windows 10 which by default uses similarly ugly fonts for everything.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Oct '16, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-56134" class="comments-container"></div><div id="comment-tools-56134" class="comment-tools"></div><div class="clear"></div><div id="comment-56134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

