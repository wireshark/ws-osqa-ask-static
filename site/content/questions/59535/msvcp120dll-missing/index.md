+++
type = "question"
title = "MSVCP120.dll Missing"
description = '''Trying to install Wireshark 2.2.3 on a Windows Server 2008 R2 SP1 64 bit box. Install completes successfully but when I try to run Wireshark I receive an error message saying MSVCP120.dll is missing, re-install wireshark. This dll is not on the machine but a MSVCP120_clr0400.dll is?'''
date = "2017-02-18T13:20:00Z"
lastmod = "2017-02-24T11:05:00Z"
weight = 59535
keywords = [ "msvcp120.dll" ]
aliases = [ "/questions/59535" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MSVCP120.dll Missing](/questions/59535/msvcp120dll-missing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59535-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59535-score" class="post-score" title="current number of votes">0</div><span id="post-59535-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trying to install Wireshark 2.2.3 on a Windows Server 2008 R2 SP1 64 bit box. Install completes successfully but when I try to run Wireshark I receive an error message saying MSVCP120.dll is missing, re-install wireshark. This dll is not on the machine but a MSVCP120_clr0400.dll is?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-msvcp120.dll" rel="tag" title="see questions tagged &#39;msvcp120.dll&#39;">msvcp120.dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '17, 13:20</strong></p><img src="https://secure.gravatar.com/avatar/9bba5767f99918934c5056afb2d67c8d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="controlmech&#39;s gravatar image" /><p><span>controlmech</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="controlmech has no accepted answers">0%</span></p></div></div><div id="comments-container-59535" class="comments-container"></div><div id="comment-tools-59535" class="comment-tools"></div><div class="clear"></div><div id="comment-59535-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59668"></span>

<div id="answer-container-59668" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59668-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59668-score" class="post-score" title="current number of votes">0</div><span id="post-59668-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, there are 2 possible issues for the above error.. 1)Install the appropriate Wireshark installer based on your system compatibility 32bit or 64bit 2)check Microsoft VC++ version in your system, install a latest one from Microsoft site before proceeding for Wireshark install.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '17, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/746454f41d14403415dece210aba20d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sudheer628&#39;s gravatar image" /><p><span>sudheer628</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sudheer628 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Feb '17, 11:36</strong> </span></p></div></div><div id="comments-container-59668" class="comments-container"></div><div id="comment-tools-59668" class="comment-tools"></div><div class="clear"></div><div id="comment-59668-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

