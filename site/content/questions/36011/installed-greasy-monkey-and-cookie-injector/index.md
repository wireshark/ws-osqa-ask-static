+++
type = "question"
title = "installed greasy monkey and cookie injector"
description = '''I have installed greasy monkey and cookie injector on my browser-firefox 32. However, i&#x27;m unable to dump cookie from wireshark coz when i hit alt-c combination, the dialogue doesnt open and im not sure where to dump the copied cookie..'''
date = "2014-09-04T11:29:00Z"
lastmod = "2017-07-02T04:18:00Z"
weight = 36011
keywords = [ "clone" ]
aliases = [ "/questions/36011" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [installed greasy monkey and cookie injector](/questions/36011/installed-greasy-monkey-and-cookie-injector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36011-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36011-score" class="post-score" title="current number of votes">0</div><span id="post-36011-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have installed greasy monkey and cookie injector on my browser-firefox 32. However, i'm unable to dump cookie from wireshark coz when i hit alt-c combination, the dialogue doesnt open and im not sure where to dump the copied cookie..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-clone" rel="tag" title="see questions tagged &#39;clone&#39;">clone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Sep '14, 11:29</strong></p><img src="https://secure.gravatar.com/avatar/391eb821f97de3563033cbeb8d492e9f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vamshi&#39;s gravatar image" /><p><span>vamshi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vamshi has no accepted answers">0%</span></p></div></div><div id="comments-container-36011" class="comments-container"><span id="62463"></span><div id="comment-62463" class="comment"><div id="post-62463-score" class="comment-score"></div><div class="comment-text"><p>im having the same issue someone pls help us!!!</p></div><div id="comment-62463-info" class="comment-info"><span class="comment-age">(01 Jul '17, 19:34)</span> <span class="comment-user userinfo">chun61</span></div></div><span id="62465"></span><div id="comment-62465" class="comment"><div id="post-62465-score" class="comment-score"></div><div class="comment-text"><p>Are you sure it is a Wireshark-related question?</p><p>At the moment you press the Alt-C, is the browser window at focus? Pressing Alt-C while Wireshark is at focus won't work. So first copy the cookie from Wireshark, then click to the browser window, and then press Alt-C. If this does not work, ask the cookie injector author for further help.</p></div><div id="comment-62465-info" class="comment-info"><span class="comment-age">(02 Jul '17, 04:18)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-36011" class="comment-tools"></div><div class="clear"></div><div id="comment-36011-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

