+++
type = "question"
title = "How to add keyboard shortcut for lua plugin?"
description = '''I have created a lua plugin, all i want is ability to define a keyboard shortcut for it. currently it is accessible from tools menu, is there a way by which i can assign an accelerator to it?'''
date = "2016-02-03T02:32:00Z"
lastmod = "2016-02-03T02:32:00Z"
weight = 49762
keywords = [ "lua", "wireshark", "plugin" ]
aliases = [ "/questions/49762" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to add keyboard shortcut for lua plugin?](/questions/49762/how-to-add-keyboard-shortcut-for-lua-plugin)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49762-score" class="post-score" title="current number of votes">0</div><span id="post-49762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have created a lua plugin, all i want is ability to define a keyboard shortcut for it. currently it is accessible from tools menu, is there a way by which i can assign an accelerator to it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '16, 02:32</strong></p><img src="https://secure.gravatar.com/avatar/c97827713cf7e4148c1f3ff32d4e03b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="himanshu97&#39;s gravatar image" /><p><span>himanshu97</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="himanshu97 has no accepted answers">0%</span></p></div></div><div id="comments-container-49762" class="comments-container"></div><div id="comment-tools-49762" class="comment-tools"></div><div class="clear"></div><div id="comment-49762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

