+++
type = "question"
title = "RTP Packets - 96 Bytes"
description = '''Hi I&#x27;ve received a trace that has RTP Packets which are 96 bytes. eg: Frame 34: 214 bytes on wire (1712 bits), 96 bytes captured (768 bits) Can anyone advise how I can decode these packets, so I can listen to the stream. Thanks'''
date = "2013-12-09T03:50:00Z"
lastmod = "2013-12-09T09:27:00Z"
weight = 27946
keywords = [ "decode_rtp", "rtp" ]
aliases = [ "/questions/27946" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [RTP Packets - 96 Bytes](/questions/27946/rtp-packets-96-bytes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27946-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27946-score" class="post-score" title="current number of votes">0</div><span id="post-27946-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I've received a trace that has RTP Packets which are 96 bytes.</p><p>eg: <strong>Frame 34: 214 bytes on wire (1712 bits), 96 bytes captured (768 bits)</strong></p><p>Can anyone advise how I can decode these packets, so I can listen to the stream.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode_rtp" rel="tag" title="see questions tagged &#39;decode_rtp&#39;">decode_rtp</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '13, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/0a230628e7f3b3e94b54a61297a0eea4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MaCMan&#39;s gravatar image" /><p><span>MaCMan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MaCMan has no accepted answers">0%</span></p></div></div><div id="comments-container-27946" class="comments-container"></div><div id="comment-tools-27946" class="comment-tools"></div><div class="clear"></div><div id="comment-27946-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27956"></span>

<div id="answer-container-27956" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27956-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27956-score" class="post-score" title="current number of votes">3</div><span id="post-27956-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="MaCMan has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This just won't work... You've got no payload in your RTP packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '13, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/c3503bfc7ff8f99752af69a25983d5ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Frobbotzim&#39;s gravatar image" /><p><span>Frobbotzim</span><br />
<span class="score" title="71 reputation points">71</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Frobbotzim has one accepted answer">33%</span></p></div></div><div id="comments-container-27956" class="comments-container"></div><div id="comment-tools-27956" class="comment-tools"></div><div class="clear"></div><div id="comment-27956-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

