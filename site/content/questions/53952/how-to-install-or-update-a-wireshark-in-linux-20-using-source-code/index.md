+++
type = "question"
title = "how to install or update a wireshark in linux 2.0 using source code?"
description = '''how to install or update a wireshark in linux 2.0 using source code?'''
date = "2016-07-09T00:50:00Z"
lastmod = "2016-07-09T07:01:00Z"
weight = 53952
keywords = [ "2.0.4", "wireshark" ]
aliases = [ "/questions/53952" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to install or update a wireshark in linux 2.0 using source code?](/questions/53952/how-to-install-or-update-a-wireshark-in-linux-20-using-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53952-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53952-score" class="post-score" title="current number of votes">0</div><span id="post-53952-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to install or update a wireshark in linux 2.0 using source code?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-2.0.4" rel="tag" title="see questions tagged &#39;2.0.4&#39;">2.0.4</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '16, 00:50</strong></p><img src="https://secure.gravatar.com/avatar/5a4fdd8e5ac50146e4a5c5b1e3d1e61a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Platinum%20Bullet&#39;s gravatar image" /><p><span>Platinum Bullet</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Platinum Bullet has no accepted answers">0%</span></p></div></div><div id="comments-container-53952" class="comments-container"></div><div id="comment-tools-53952" class="comment-tools"></div><div class="clear"></div><div id="comment-53952-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53955"></span>

<div id="answer-container-53955" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53955-score" class="post-score" title="current number of votes">0</div><span id="post-53955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could start reading the <a href="https://www.wireshark.org/docs/wsdg_html_chunked/">Wireshark Developer's Guide</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jul '16, 07:01</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-53955" class="comments-container"></div><div id="comment-tools-53955" class="comment-tools"></div><div class="clear"></div><div id="comment-53955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

