+++
type = "question"
title = "font is not found and show up as squares"
description = '''Installing Wireshark on Mountain Lion with X11 installed, Wireshark opens, but all of the fonts show up as squares. I get a pile of errors when I run Wireshark from the X11 terminal. Some examples are below: (Wireshark-bin:17459): GdkPixbuf-WARNING : Error loading XPM image loader: Image type &#x27;xpm&#x27; ...'''
date = "2013-05-08T05:34:00Z"
lastmod = "2013-05-08T05:34:00Z"
weight = 21024
keywords = [ "x11", "font" ]
aliases = [ "/questions/21024" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [font is not found and show up as squares](/questions/21024/font-is-not-found-and-show-up-as-squares)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21024-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21024-score" class="post-score" title="current number of votes">1</div><span id="post-21024-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Installing Wireshark on Mountain Lion with X11 installed, Wireshark opens, but all of the fonts show up as squares. I get a pile of errors when I run Wireshark from the X11 terminal. Some examples are below:</p><p>(Wireshark-bin:17459): GdkPixbuf-WARNING <strong>: Error loading XPM image loader: Image type 'xpm' is not supported (Wireshark-bin:17459): GLib-GObject-CRITICAL</strong> : g_object_ref: assertion <code>G_IS_OBJECT (object)' failed (Wireshark-bin:17459): Gdk-CRITICAL **: gdk_window_set_icon_list: assertion</code>GDK_IS_PIXBUF (pixbuf)' failed (Wireshark-bin:17459): Pango-CRITICAL **: No modules found: No builtin or dynamically loaded modules were found. PangoFc will not work correctly. This probably means there was an error in the creation of: '/usr/local/etc/pango/pango.modules'</p><p>I have tried running wireshark from the x11 window with sudo, but that had no impact on the issue... I am guessing we are missing a part of the X11 components or need to install more than X11. I have tried uninstalling wireshark and X11 and reinstalling, but it has had no impact.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x11" rel="tag" title="see questions tagged &#39;x11&#39;">x11</span> <span class="post-tag tag-link-font" rel="tag" title="see questions tagged &#39;font&#39;">font</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '13, 05:34</strong></p><img src="https://secure.gravatar.com/avatar/438d97de8a6627d7fd12a8d76b34301c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oldguardmd&#39;s gravatar image" /><p><span>oldguardmd</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oldguardmd has no accepted answers">0%</span></p></div></div><div id="comments-container-21024" class="comments-container"></div><div id="comment-tools-21024" class="comment-tools"></div><div class="clear"></div><div id="comment-21024-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

