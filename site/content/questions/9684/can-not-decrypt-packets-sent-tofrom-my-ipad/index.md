+++
type = "question"
title = "Can not decrypt packets sent to/from my iPad"
description = '''I have noticed a strange thing, I cannot decrypt packets that are sent to/from my iPad. But I can decrypt packets that are sent from/to my other laptop. Both are (of course) connected to the same WPA2 (AES) encrypted network. I have captured the four-way handshake with both my iPad and laptop, but I...'''
date = "2012-03-21T11:56:00Z"
lastmod = "2012-03-21T17:29:00Z"
weight = 9684
keywords = [ "ipad", "decryption", "wifi", "wpa2" ]
aliases = [ "/questions/9684" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can not decrypt packets sent to/from my iPad](/questions/9684/can-not-decrypt-packets-sent-tofrom-my-ipad)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9684-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9684-score" class="post-score" title="current number of votes">0</div><span id="post-9684-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have noticed a strange thing, I cannot decrypt packets that are sent to/from my iPad. But I can decrypt packets that are sent from/to my other laptop. Both are (of course) connected to the same WPA2 (AES) encrypted network.</p><p>I have captured the four-way handshake with both my iPad and laptop, but I can only decrypt the packets going to my laptop when setting the wpa-psk pre-shared key in Edit-&gt;Preferences-&gt;Protocols-&gt;IEEE 802.11.</p><p>Why is that so?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipad" rel="tag" title="see questions tagged &#39;ipad&#39;">ipad</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '12, 11:56</strong></p><img src="https://secure.gravatar.com/avatar/b40d415d5a5ed5142e38ad841b2e176a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rox&#39;s gravatar image" /><p><span>Rox</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rox has no accepted answers">0%</span></p></div></div><div id="comments-container-9684" class="comments-container"></div><div id="comment-tools-9684" class="comment-tools"></div><div class="clear"></div><div id="comment-9684-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9686"></span>

<div id="answer-container-9686" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9686-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9686-score" class="post-score" title="current number of votes">0</div><span id="post-9686-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you checked the negotiated packet size to make sure that you are capturing the full packets in WireShark? Is it different between your desktop and your iPad?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '12, 17:29</strong></p><img src="https://secure.gravatar.com/avatar/b64129b7a3bf2a9f1760fbdee1b3b74c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="inetdog&#39;s gravatar image" /><p><span>inetdog</span><br />
<span class="score" title="167 reputation points">167</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="inetdog has 3 accepted answers">14%</span></p></div></div><div id="comments-container-9686" class="comments-container"></div><div id="comment-tools-9686" class="comment-tools"></div><div class="clear"></div><div id="comment-9686-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

