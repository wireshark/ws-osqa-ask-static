+++
type = "question"
title = "How to save packets received using Winpcap"
description = '''could you give an idea to do it.'''
date = "2015-02-10T05:22:00Z"
lastmod = "2015-02-10T06:01:00Z"
weight = 39761
keywords = [ "packets" ]
aliases = [ "/questions/39761" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to save packets received using Winpcap](/questions/39761/how-to-save-packets-received-using-winpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39761-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39761-score" class="post-score" title="current number of votes">0</div><span id="post-39761-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>could you give an idea to do it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '15, 05:22</strong></p><img src="https://secure.gravatar.com/avatar/74ab8994ff0e776c06c6b4f14f4dfca1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sathish308&#39;s gravatar image" /><p><span>sathish308</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sathish308 has no accepted answers">0%</span></p></div></div><div id="comments-container-39761" class="comments-container"><span id="39766"></span><div id="comment-39766" class="comment"><div id="post-39766-score" class="comment-score"></div><div class="comment-text"><p>How did you recevice packets from Winpcap without using Wireshark?</p></div><div id="comment-39766-info" class="comment-info"><span class="comment-age">(10 Feb '15, 05:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-39761" class="comment-tools"></div><div class="clear"></div><div id="comment-39761-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39767"></span>

<div id="answer-container-39767" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39767-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39767-score" class="post-score" title="current number of votes">0</div><span id="post-39767-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://www.winpcap.org/docs/docs_412/html/main.html">http://www.winpcap.org/docs/docs_412/html/main.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '15, 06:01</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-39767" class="comments-container"></div><div id="comment-tools-39767" class="comment-tools"></div><div class="clear"></div><div id="comment-39767-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

