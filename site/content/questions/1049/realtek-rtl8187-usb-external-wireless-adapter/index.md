+++
type = "question"
title = "REALTEK RTL8187 USB EXTERNAL WIRELESS ADAPTER"
description = '''Hi, just new in the forum , I have a Toshiba laptop with a external REALTEK RTL8187 USB EXTERNAL WIRELESS ADAPTER , when I start Wireshark I receive the following message : Please check that &quot;DeviceNPF_{FF58589B-5BF6-4A78-988F-87B508471370}&quot; is the proper interface. Would like to ask for help on thi...'''
date = "2010-11-21T08:59:00Z"
lastmod = "2010-11-21T08:59:00Z"
weight = 1049
keywords = [ "rtl8187", "realtek", "usb" ]
aliases = [ "/questions/1049" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [REALTEK RTL8187 USB EXTERNAL WIRELESS ADAPTER](/questions/1049/realtek-rtl8187-usb-external-wireless-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1049-score" class="post-score" title="current number of votes">0</div><span id="post-1049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, just new in the forum , I have a Toshiba laptop with a external REALTEK RTL8187 USB EXTERNAL WIRELESS ADAPTER , when I start Wireshark I receive the following message : Please check that "DeviceNPF_{FF58589B-5BF6-4A78-988F-87B508471370}" is the proper interface.</p><p>Would like to ask for help on this matter, is this a compatibility issue ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtl8187" rel="tag" title="see questions tagged &#39;rtl8187&#39;">rtl8187</span> <span class="post-tag tag-link-realtek" rel="tag" title="see questions tagged &#39;realtek&#39;">realtek</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '10, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/8878cc91f7579026f9a9e314c04fdda2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TEEH&#39;s gravatar image" /><p><span>TEEH</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TEEH has no accepted answers">0%</span></p></div></div><div id="comments-container-1049" class="comments-container"></div><div id="comment-tools-1049" class="comment-tools"></div><div class="clear"></div><div id="comment-1049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

