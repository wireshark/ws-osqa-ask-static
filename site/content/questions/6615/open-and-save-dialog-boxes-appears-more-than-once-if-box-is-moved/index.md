+++
type = "question"
title = "Open and Save Dialog Boxes Appears more than once if box is moved."
description = '''Why the Open and save dialog boxes are reappearing on the screen again and again when they are being moved? This is damaging the screen view. Please help.'''
date = "2011-09-28T04:58:00Z"
lastmod = "2011-09-28T06:49:00Z"
weight = 6615
keywords = [ "gtk" ]
aliases = [ "/questions/6615" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Open and Save Dialog Boxes Appears more than once if box is moved.](/questions/6615/open-and-save-dialog-boxes-appears-more-than-once-if-box-is-moved)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6615-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6615-score" class="post-score" title="current number of votes">0</div><span id="post-6615-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why the Open and save dialog boxes are reappearing on the screen again and again when they are being moved? This is damaging the screen view. Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Sep '11, 04:58</strong></p><img src="https://secure.gravatar.com/avatar/968cc7ddfc48322ffbd1d7f5e3d37b85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Terrestrial%20shark&#39;s gravatar image" /><p><span>Terrestrial ...</span><br />
<span class="score" title="96 reputation points">96</span><span title="21 badges"><span class="badge1">●</span><span class="badgecount">21</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="29 badges"><span class="bronze">●</span><span class="badgecount">29</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Terrestrial shark has 3 accepted answers">42%</span></p></div></div><div id="comments-container-6615" class="comments-container"><span id="6617"></span><div id="comment-6617" class="comment"><div id="post-6617-score" class="comment-score"></div><div class="comment-text"><p>Be more specific !! ...Whats the wireshark version and what os are you using</p></div><div id="comment-6617-info" class="comment-info"><span class="comment-age">(28 Sep '11, 05:03)</span> <span class="comment-user userinfo">flashkicker</span></div></div><span id="6618"></span><div id="comment-6618" class="comment"><div id="post-6618-score" class="comment-score"></div><div class="comment-text"><p>wireshark 1.6.1 release and windows xp.</p></div><div id="comment-6618-info" class="comment-info"><span class="comment-age">(28 Sep '11, 05:59)</span> <span class="comment-user userinfo">Terrestrial ...</span></div></div></div><div id="comment-tools-6615" class="comment-tools"></div><div class="clear"></div><div id="comment-6615-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6619"></span>

<div id="answer-container-6619" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6619-score" class="post-score" title="current number of votes">2</div><span id="post-6619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Terrestrial shark has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately, this is due to the fact that Wireshark on Windows uses a mixture of native Windows widgets (e.g. open, save) along with GTK (http://www.gtk.org/)</p><p>See: <a href="http://wiki.wireshark.org/KnownBugs" title="the last entry">Known Bugs</a> (the last entry) and <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1364">Bug 1364</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Sep '11, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Sep '11, 06:51</strong> </span></p></div></div><div id="comments-container-6619" class="comments-container"></div><div id="comment-tools-6619" class="comment-tools"></div><div class="clear"></div><div id="comment-6619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

