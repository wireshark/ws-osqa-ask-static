+++
type = "question"
title = "Wireshark ble plug in"
description = '''Per said in the link https://lacklustre.net/bluetooth/wireshark.html, the Wireshark version I am using (1.12.6) should have built in the ble plugin, right?'''
date = "2015-07-16T16:09:00Z"
lastmod = "2015-07-17T07:12:00Z"
weight = 44224
keywords = [ "bluetooth" ]
aliases = [ "/questions/44224" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark ble plug in](/questions/44224/wireshark-ble-plug-in)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44224-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44224-score" class="post-score" title="current number of votes">0</div><span id="post-44224-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Per said in the link <a href="https://lacklustre.net/bluetooth/wireshark.html,">https://lacklustre.net/bluetooth/wireshark.html,</a> the Wireshark version I am using (1.12.6) should have built in the ble plugin, right?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jul '15, 16:09</strong></p><img src="https://secure.gravatar.com/avatar/a2005183e9bc8d1a3f335965972cbb2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Max%20Lu&#39;s gravatar image" /><p><span>Max Lu</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Max Lu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted <strong>17 Jul '15, 06:37</strong> </span></p></div></div><div id="comments-container-44224" class="comments-container"></div><div id="comment-tools-44224" class="comment-tools"></div><div class="clear"></div><div id="comment-44224-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44241"></span>

<div id="answer-container-44241" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44241-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44241-score" class="post-score" title="current number of votes">0</div><span id="post-44241-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>yes Wireshark 1.12.6 includes a built-in <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=epan/dissectors/packet-btle.c;h=840f03ba1ee6a3b5ef6e2ad01366bc417184b92d;hb=ee1fce6fb32c55cf1af2fe1778f67eb77c0ff8d4">BTLE dissector</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jul '15, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-44241" class="comments-container"></div><div id="comment-tools-44241" class="comment-tools"></div><div class="clear"></div><div id="comment-44241-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

