+++
type = "question"
title = "Wireshark and single core CPU problem"
description = '''Hi guys, i&#x27;ve installed on my old laptop a kali linux distro, it has a single core cpu, i&#x27;m trying to perform a arp poison + wireshark packets sniff, but when starting the arp poison script, in the wireshark gui i can&#x27;t even select the interface to listen on because as i click on it, it deselect its...'''
date = "2016-02-12T06:40:00Z"
lastmod = "2016-02-12T06:40:00Z"
weight = 50143
keywords = [ "arp", "arpspoofing", "interface", "cpu" ]
aliases = [ "/questions/50143" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and single core CPU problem](/questions/50143/wireshark-and-single-core-cpu-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50143-score" class="post-score" title="current number of votes">0</div><span id="post-50143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys, i've installed on my old laptop a kali linux distro, it has a single core cpu, i'm trying to perform a arp poison + wireshark packets sniff, but when starting the arp poison script, in the wireshark gui i can't even select the interface to listen on because as i click on it, it deselect its self. It seems like while performing arp poisoning the interfaces appear busy to wireshark maybe due to my slow 1.6 ghz and single core cpu. i think it could be the cpu because when running only wireshark without any other tool it runs just like it should. So my question is, should I update my cpu because it's the main reason of the problem or it's just something else ? thank you for the help!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span> <span class="post-tag tag-link-arpspoofing" rel="tag" title="see questions tagged &#39;arpspoofing&#39;">arpspoofing</span> <span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-cpu" rel="tag" title="see questions tagged &#39;cpu&#39;">cpu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '16, 06:40</strong></p><img src="https://secure.gravatar.com/avatar/7144cd56be2579e712a9a16f9a35b52b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="crucio95&#39;s gravatar image" /><p><span>crucio95</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="crucio95 has no accepted answers">0%</span></p></div></div><div id="comments-container-50143" class="comments-container"></div><div id="comment-tools-50143" class="comment-tools"></div><div class="clear"></div><div id="comment-50143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

