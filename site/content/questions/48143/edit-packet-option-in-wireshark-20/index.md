+++
type = "question"
title = "Edit packet option in Wireshark 2.0"
description = '''In Wireshark 1.12.x, there was an experimental option where the user could edit packet contents within Wireshark. This option had to be enabled in: Edit / Preferences / User Interface / Enable Packet Editor (Experimental) Then the user could edit the packet by selecting the packet in the Packet List...'''
date = "2015-12-01T08:30:00Z"
lastmod = "2015-12-01T11:50:00Z"
weight = 48143
keywords = [ "edit", "packet" ]
aliases = [ "/questions/48143" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Edit packet option in Wireshark 2.0](/questions/48143/edit-packet-option-in-wireshark-20)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48143-score" class="post-score" title="current number of votes">0</div><span id="post-48143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In Wireshark 1.12.x, there was an experimental option where the user could edit packet contents within Wireshark. This option had to be enabled in:</p><p>Edit / Preferences / User Interface / Enable Packet Editor (Experimental)</p><p>Then the user could edit the packet by selecting the packet in the Packet List and going to Edit / Edit Packet</p><p>I do not see the option in Wireshark 2.0. Has it moved or not implemented?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-edit" rel="tag" title="see questions tagged &#39;edit&#39;">edit</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '15, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-48143" class="comments-container"></div><div id="comment-tools-48143" class="comment-tools"></div><div class="clear"></div><div id="comment-48143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48146"></span>

<div id="answer-container-48146" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48146-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48146-score" class="post-score" title="current number of votes">0</div><span id="post-48146-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Amato_C has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>this is not implemented yet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Dec '15, 08:50</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-48146" class="comments-container"><span id="48149"></span><div id="comment-48149" class="comment"><div id="post-48149-score" class="comment-score"></div><div class="comment-text"><p>Should a defect be created in Bugzilla, or is this already being tracked?</p></div><div id="comment-48149-info" class="comment-info"><span class="comment-age">(01 Dec '15, 09:09)</span> <span class="comment-user userinfo">Amato_C</span></div></div><span id="48151"></span><div id="comment-48151" class="comment"><div id="post-48151-score" class="comment-score"></div><div class="comment-text"><p>Unless I missed it, I do not see a bug tracking this right now...</p></div><div id="comment-48151-info" class="comment-info"><span class="comment-age">(01 Dec '15, 10:46)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div><span id="48154"></span><div id="comment-48154" class="comment"><div id="post-48154-score" class="comment-score"></div><div class="comment-text"><p>Bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11840">11840</a> Submitted</p></div><div id="comment-48154-info" class="comment-info"><span class="comment-age">(01 Dec '15, 11:50)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-48146" class="comment-tools"></div><div class="clear"></div><div id="comment-48146-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

