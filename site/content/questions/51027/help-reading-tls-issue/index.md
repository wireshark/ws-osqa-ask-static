+++
type = "question"
title = "Help reading tls issue"
description = '''Hi, i am new to Wireshark and having some issues or potential issues between my android device running digital signage and the server. the server runs windows and encrypts the connection, i am trying to decipher the below code but i need help on how to read it. the below 3 codescomes up about every ...'''
date = "2016-03-18T06:17:00Z"
lastmod = "2016-03-18T06:17:00Z"
weight = 51027
keywords = [ "tls" ]
aliases = [ "/questions/51027" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help reading tls issue](/questions/51027/help-reading-tls-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51027-score" class="post-score" title="current number of votes">0</div><span id="post-51027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>i am new to Wireshark and having some issues or potential issues between my android device running digital signage and the server. the server runs windows and encrypts the connection, i am trying to decipher the below code but i need help on how to read it.</p><p>the below 3 codescomes up about every 15 lines in wireshark</p><pre><code>https   1969.753316000  10.38.36.228    serverdnsname   TLSv1.2 415 [TCP Retransmission] Application Data   34816   398927

34816   1969.753365000  serverdnsname   10.38.36.228    TCP 78  [TCP Dup ACK 398926#1] https→34816 [ACK] Seq=4225 Ack=1081 Win=65792 Len=0 TSval=800453905 TSecr=155666 SLE=732 SRE=1081    https   398928

https   1969.933146000  10.38.36.228    serverdnsname   TCP 66  34816→https [RST, ACK] Seq=1081 Ack=4484 Win=14528 Len=0 TSval=155706 TSecr=800453920   34816   399113</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tls" rel="tag" title="see questions tagged &#39;tls&#39;">tls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '16, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/5cf8abef6cafda52af08136b2db89928?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="altonmazin&#39;s gravatar image" /><p><span>altonmazin</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="altonmazin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '16, 06:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51027" class="comments-container"></div><div id="comment-tools-51027" class="comment-tools"></div><div class="clear"></div><div id="comment-51027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

