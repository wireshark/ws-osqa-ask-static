+++
type = "question"
title = "Raspbian - Wireshark 1.12.1 Issues"
description = '''Hello, Recently msy raspbian repository upgraded to 1.12.1 from 1.8.12 and now I&#x27;m having issues. When Wireshark is installed, an interface selected, and start clicked, I get the following error message: GTK:ERRO:/usr/src/packages/BUILD/gtk+3.0-3.10.2/./gtk/gtkwidget.c:14880:gtk_widget_unregister_wi...'''
date = "2016-08-08T10:24:00Z"
lastmod = "2017-04-22T14:03:00Z"
weight = 54670
keywords = [ "raspbian", "1.12.1" ]
aliases = [ "/questions/54670" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Raspbian - Wireshark 1.12.1 Issues](/questions/54670/raspbian-wireshark-1121-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54670-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54670-score" class="post-score" title="current number of votes">1</div><span id="post-54670-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Recently msy raspbian repository upgraded to 1.12.1 from 1.8.12 and now I'm having issues. When Wireshark is installed, an interface selected, and start clicked, I get the following error message:</p><p>GTK:ERRO:/usr/src/packages/BUILD/gtk+3.0-3.10.2/./gtk/gtkwidget.c:14880:gtk_widget_unregister_window: assertion failed: (user_data == widget) Aborted</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-raspbian" rel="tag" title="see questions tagged &#39;raspbian&#39;">raspbian</span> <span class="post-tag tag-link-1.12.1" rel="tag" title="see questions tagged &#39;1.12.1&#39;">1.12.1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '16, 10:24</strong></p><img src="https://secure.gravatar.com/avatar/29dbf8e5ef6015080666e43a5b42a239?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="spxxn&#39;s gravatar image" /><p><span>spxxn</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="spxxn has no accepted answers">0%</span></p></div></div><div id="comments-container-54670" class="comments-container"><span id="60971"></span><div id="comment-60971" class="comment"><div id="post-60971-score" class="comment-score"></div><div class="comment-text"><p>Hello, I'm having the same problem. Did you find a solution?</p></div><div id="comment-60971-info" class="comment-info"><span class="comment-age">(22 Apr '17, 14:03)</span> <span class="comment-user userinfo">emsltek</span></div></div></div><div id="comment-tools-54670" class="comment-tools"></div><div class="clear"></div><div id="comment-54670-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

