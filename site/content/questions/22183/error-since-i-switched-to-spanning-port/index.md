+++
type = "question"
title = "Error since I switched to Spanning port"
description = '''HI I&#x27;m running wireshark 1.8.6 SVNRev 48142 from trunk-1.8 Running on windows XP professional. Recently I switched to port that spans whole subnet so i can see all traffic. It runs about 5 to 10 minutes then i get a error it says &quot; runtime error! program: C:&#92;Program Files&#92;Wireshark&#92;wireshark.exe Thi...'''
date = "2013-06-19T13:01:00Z"
lastmod = "2013-06-19T13:13:00Z"
weight = 22183
keywords = [ "spanning", "runtime", "port", "error" ]
aliases = [ "/questions/22183" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Error since I switched to Spanning port](/questions/22183/error-since-i-switched-to-spanning-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22183-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22183-score" class="post-score" title="current number of votes">0</div><span id="post-22183-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI I'm running wireshark 1.8.6 SVNRev 48142 from trunk-1.8<br />
Running on windows XP professional. Recently I switched to port that spans whole subnet so i can see all traffic. It runs about 5 to 10 minutes then i get a error it says " runtime error! program: C:\Program Files\Wireshark\wireshark.exe This application has requested the runtime to terminate it in a unusual way Please contact the application's support team for more Infomation. I cleared the event logs before starting and checked after no new entries.. Does wireshark generate a error log I can review? any ideas. Please advise<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spanning" rel="tag" title="see questions tagged &#39;spanning&#39;">spanning</span> <span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '13, 13:01</strong></p><img src="https://secure.gravatar.com/avatar/46f38c09a968dc5ad7088369c709b79b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michael%20Elrick&#39;s gravatar image" /><p><span>Michael Elrick</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michael Elrick has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-22183" class="comments-container"></div><div id="comment-tools-22183" class="comment-tools"></div><div class="clear"></div><div id="comment-22183-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22185"></span>

<div id="answer-container-22185" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22185-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22185-score" class="post-score" title="current number of votes">2</div><span id="post-22185-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See: <a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory">http://wiki.wireshark.org/KnownBugs/OutOfMemory</a></p><p>TIP: Use <a href="http://www.wireshark.org/docs/man-pages/dumpcap.html">dumpcap</a> to capture (with the -b options to create a ringbuffer of files)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jun '13, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-22185" class="comments-container"></div><div id="comment-tools-22185" class="comment-tools"></div><div class="clear"></div><div id="comment-22185-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

