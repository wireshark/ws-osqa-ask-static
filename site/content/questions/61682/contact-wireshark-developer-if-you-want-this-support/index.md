+++
type = "question"
title = "contact wireshark developer if you want this support"
description = '''hi, in internet message format(IMF) dissectors in unknown esxention fieleds i see contact wireshark developer if you want this support. why?'''
date = "2017-05-29T02:56:00Z"
lastmod = "2017-05-29T03:54:00Z"
weight = 61682
keywords = [ "imf" ]
aliases = [ "/questions/61682" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [contact wireshark developer if you want this support](/questions/61682/contact-wireshark-developer-if-you-want-this-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61682-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61682-score" class="post-score" title="current number of votes">0</div><span id="post-61682-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, in internet message format(IMF) dissectors in unknown esxention fieleds i see contact wireshark developer if you want this support. why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-imf" rel="tag" title="see questions tagged &#39;imf&#39;">imf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '17, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/28d5dc133c31193058a99892f00a0213?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ghader&#39;s gravatar image" /><p><span>ghader</span><br />
<span class="score" title="61 reputation points">61</span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ghader has no accepted answers">0%</span></p></div></div><div id="comments-container-61682" class="comments-container"></div><div id="comment-tools-61682" class="comment-tools"></div><div class="clear"></div><div id="comment-61682-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61683"></span>

<div id="answer-container-61683" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61683-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61683-score" class="post-score" title="current number of votes">1</div><span id="post-61683-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Because:</p><ul><li>The current dissector doesn't dissect these fields.</li><li>There is no effort being made to support them as of yet.</li></ul><p>So if the demand arrises it could be a subject of interest for someone to write the code for, using the reference material available,ie. a sample capture file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '17, 03:54</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-61683" class="comments-container"></div><div id="comment-tools-61683" class="comment-tools"></div><div class="clear"></div><div id="comment-61683-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

