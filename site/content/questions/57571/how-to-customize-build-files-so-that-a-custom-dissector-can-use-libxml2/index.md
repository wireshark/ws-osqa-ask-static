+++
type = "question"
title = "How to customize build files so that a custom dissector can use libxml2"
description = '''I am building a dissector for a proprietary protocol with an XML payload, and the Wireshark xml dissector does not fit my need (I have to customize a few things, make some translations, ...). I thought of using libxml2 to parse this payload but I do not know how to modify build files to include libx...'''
date = "2016-11-23T09:03:00Z"
lastmod = "2016-11-23T09:03:00Z"
weight = 57571
keywords = [ "xml", "libxml2", "dissector" ]
aliases = [ "/questions/57571" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to customize build files so that a custom dissector can use libxml2](/questions/57571/how-to-customize-build-files-so-that-a-custom-dissector-can-use-libxml2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57571-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57571-score" class="post-score" title="current number of votes">0</div><span id="post-57571-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am building a dissector for a proprietary protocol with an XML payload, and the Wireshark xml dissector does not fit my need (I have to customize a few things, make some translations, ...).</p><p>I thought of using libxml2 to parse this payload but I do not know how to modify build files to include libxml2. Maybe I can use libxml2-2.dll, located in Wireshark-win64-libs-2.0\gtk2\bin but I can not find any include folder with libxml2 header files (I am using Wireshark 2.0.2 source code), but I don't know how.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xml" rel="tag" title="see questions tagged &#39;xml&#39;">xml</span> <span class="post-tag tag-link-libxml2" rel="tag" title="see questions tagged &#39;libxml2&#39;">libxml2</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '16, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/dfd728dcc858e4bb45f3ea8804fe9ba1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hpa&#39;s gravatar image" /><p><span>hpa</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hpa has no accepted answers">0%</span></p></div></div><div id="comments-container-57571" class="comments-container"></div><div id="comment-tools-57571" class="comment-tools"></div><div class="clear"></div><div id="comment-57571-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

