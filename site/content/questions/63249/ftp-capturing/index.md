+++
type = "question"
title = "FTP capturing"
description = '''Hi. As practice I&#x27;m trying to capture FTP using filezilla between a laptop to a pizero on a lab network using a wireless router, so trying to do this all wirelessly. I&#x27;m using a laptop loaded with Kali linux (obviously using wireshark to capture from there), monitor mode is working, I see the lab ne...'''
date = "2017-07-30T17:30:00Z"
lastmod = "2017-07-30T17:30:00Z"
weight = 63249
keywords = [ "ftp", "filezilla", "kali" ]
aliases = [ "/questions/63249" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [FTP capturing](/questions/63249/ftp-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63249-score" class="post-score" title="current number of votes">0</div><span id="post-63249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. As practice I'm trying to capture FTP using filezilla between a laptop to a pizero on a lab network using a wireless router, so trying to do this all wirelessly. I'm using a laptop loaded with Kali linux (obviously using wireshark to capture from there), monitor mode is working, I see the lab network without problems. But when I try to capture the sending of a jpg from the laptop to the pizero I can never find any evidence that the transaction took place. I ran wireshark concurrently on the laptop a couple times and in one of those instances I was able to see the username and password being passed; but still I have nothing on the Kali laptop that shows this ever happened. Lastly, I see that the file transfers, I see it on the desktop every time where I put it. Thanks for any suggestions.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-filezilla" rel="tag" title="see questions tagged &#39;filezilla&#39;">filezilla</span> <span class="post-tag tag-link-kali" rel="tag" title="see questions tagged &#39;kali&#39;">kali</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '17, 17:30</strong></p><img src="https://secure.gravatar.com/avatar/59bc76839d4e73b5ed9c66121c109024?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MrGuyMan&#39;s gravatar image" /><p><span>MrGuyMan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MrGuyMan has no accepted answers">0%</span></p></div></div><div id="comments-container-63249" class="comments-container"></div><div id="comment-tools-63249" class="comment-tools"></div><div class="clear"></div><div id="comment-63249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

