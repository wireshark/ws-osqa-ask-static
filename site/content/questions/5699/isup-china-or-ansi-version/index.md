+++
type = "question"
title = "ISUP China or ANSI version"
description = '''Hi ,  i am new with the wireshark development, i want to know is there any patches that support ISUP(China Version) or ISUP(ANSI Version)? Best Regards'''
date = "2011-08-13T10:46:00Z"
lastmod = "2011-08-14T22:55:00Z"
weight = 5699
keywords = [ "isup" ]
aliases = [ "/questions/5699" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ISUP China or ANSI version](/questions/5699/isup-china-or-ansi-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5699-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5699-score" class="post-score" title="current number of votes">0</div><span id="post-5699-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi , i am new with the wireshark development, i want to know is there any patches that support ISUP(China Version) or ISUP(ANSI Version)?</p><p>Best Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-isup" rel="tag" title="see questions tagged &#39;isup&#39;">isup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '11, 10:46</strong></p><img src="https://secure.gravatar.com/avatar/8055ad8f128ba36bb317aaf6ea84055a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aMot&#39;s gravatar image" /><p><span>aMot</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aMot has no accepted answers">0%</span></p></div></div><div id="comments-container-5699" class="comments-container"></div><div id="comment-tools-5699" class="comment-tools"></div><div class="clear"></div><div id="comment-5699-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5703"></span>

<div id="answer-container-5703" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5703-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5703-score" class="post-score" title="current number of votes">1</div><span id="post-5703-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>"ISUP" should be dissected as ANSI if mtp3 is ANSI, there is no specific China ISUP dissection so the result depends on how big the differens from ITU ISUP is. Regards Anders</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Aug '11, 22:55</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-5703" class="comments-container"></div><div id="comment-tools-5703" class="comment-tools"></div><div class="clear"></div><div id="comment-5703-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

