+++
type = "question"
title = "Can I use wireshark to monitor bandwidth of computers over my network"
description = '''Can I use wireshark on my pc (connected through ethernet) to monitor the bandwidth usage of my roommates on their wireless laptops? '''
date = "2010-11-08T14:56:00Z"
lastmod = "2010-11-08T17:09:00Z"
weight = 865
keywords = [ "wireless", "ethernet", "bandwidth", "monitor" ]
aliases = [ "/questions/865" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I use wireshark to monitor bandwidth of computers over my network](/questions/865/can-i-use-wireshark-to-monitor-bandwidth-of-computers-over-my-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-865-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-865-score" class="post-score" title="current number of votes">0</div><span id="post-865-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can I use wireshark on my pc (connected through ethernet) to monitor the bandwidth usage of my roommates on their wireless laptops?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '10, 14:56</strong></p><img src="https://secure.gravatar.com/avatar/af1242647031ec549b135f056ea6ea55?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="theunderachiever&#39;s gravatar image" /><p><span>theunderachi...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="theunderachiever has no accepted answers">0%</span></p></div></div><div id="comments-container-865" class="comments-container"></div><div id="comment-tools-865" class="comment-tools"></div><div class="clear"></div><div id="comment-865-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="866"></span>

<div id="answer-container-866" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-866-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-866-score" class="post-score" title="current number of votes">0</div><span id="post-866-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes - if you can capture their traffic. You will need an adapter that goes into promiscuous mode and monitor mode. If on Windows, check out the AirPcap Adapter from www.cacetech.com.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '10, 17:09</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-866" class="comments-container"></div><div id="comment-tools-866" class="comment-tools"></div><div class="clear"></div><div id="comment-866-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

