+++
type = "question"
title = "[closed] Initiating an MMS packet"
description = '''Ive been trying to use a TCP client to wrap an MMS packet in order to write into an item in a MMS Server.  There seems to be some handshake process to be carried out before I issue any command.  What is necessary to have this done? Could anyone list the sequence I need to write or read a item from a...'''
date = "2014-11-05T18:21:00Z"
lastmod = "2014-11-06T01:57:00Z"
weight = 37599
keywords = [ "iec61850", "mms", "tcp" ]
aliases = [ "/questions/37599" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Initiating an MMS packet](/questions/37599/initiating-an-mms-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37599-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37599-score" class="post-score" title="current number of votes">0</div><span id="post-37599-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Ive been trying to use a TCP client to wrap an MMS packet in order to write into an item in a MMS Server. There seems to be some handshake process to be carried out before I issue any command.</p><p>What is necessary to have this done? Could anyone list the sequence I need to write or read a item from a MMS Server?</p><p>Thanks alot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iec61850" rel="tag" title="see questions tagged &#39;iec61850&#39;">iec61850</span> <span class="post-tag tag-link-mms" rel="tag" title="see questions tagged &#39;mms&#39;">mms</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 18:21</strong></p><img src="https://secure.gravatar.com/avatar/8945306aa93024ba723296e4fdb238e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pbritto&#39;s gravatar image" /><p><span>pbritto</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pbritto has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>06 Nov '14, 01:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-37599" class="comments-container"><span id="37608"></span><div id="comment-37608" class="comment"><div id="post-37608-score" class="comment-score"></div><div class="comment-text"><p>Not really a Wireshark question, you'll be better off asking on an IEC61850 forum or mailing list,</p></div><div id="comment-37608-info" class="comment-info"><span class="comment-age">(06 Nov '14, 01:57)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-37599" class="comment-tools"></div><div class="clear"></div><div id="comment-37599-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 06 Nov '14, 01:56

</div>

</div>

</div>

