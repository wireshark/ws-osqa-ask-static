+++
type = "question"
title = "WinPcap load capacity"
description = '''Hi. We are trying to run low tests on a VoIP recording application based on WinPCAP for audio capturing. We want to figure out what sort of server characteristics we need to use to hammer the application with X number of concurrent RTP streams. For instance what we to come out with is something like...'''
date = "2014-08-27T13:33:00Z"
lastmod = "2014-08-27T13:33:00Z"
weight = 35819
keywords = [ "sizing", "payload" ]
aliases = [ "/questions/35819" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WinPcap load capacity](/questions/35819/winpcap-load-capacity)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35819-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35819-score" class="post-score" title="current number of votes">0</div><span id="post-35819-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. We are trying to run low tests on a VoIP recording application based on WinPCAP for audio capturing. We want to figure out what sort of server characteristics we need to use to hammer the application with X number of concurrent RTP streams. For instance what we to come out with is something like this:</p><p>Concurrent RTP Streams CPU Cores (3GHz) RAM (GB) Drive Size (GB) 1 1 2 1 1000 X1 Y1 Z1 10000 X2 Y2 Z2</p><p>And so on it goes....</p><p>I have not been able to find basic information on system requirements. Any help would be highly welcome.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sizing" rel="tag" title="see questions tagged &#39;sizing&#39;">sizing</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '14, 13:33</strong></p><img src="https://secure.gravatar.com/avatar/324478c8546bf5960172bad1f6782dfd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Asimov&#39;s gravatar image" /><p><span>Asimov</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Asimov has no accepted answers">0%</span></p></div></div><div id="comments-container-35819" class="comments-container"></div><div id="comment-tools-35819" class="comment-tools"></div><div class="clear"></div><div id="comment-35819-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

