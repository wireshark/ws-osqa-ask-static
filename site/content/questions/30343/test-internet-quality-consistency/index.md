+++
type = "question"
title = "test internet quality, consistency"
description = '''hi, i&#x27;m a total noob when it comes to wireshark. I&#x27;m having issues with my cable connection. my cable provider does quick test (like speedtest) and simply says it&#x27;s fine! Well, there are moments when a 15min. 360p video on youtube takes an hour to watch just because of buffering! I have found a spee...'''
date = "2014-03-03T03:00:00Z"
lastmod = "2014-03-03T03:35:00Z"
weight = 30343
keywords = [ "test", "consistency", "quality", "monitor", "internet" ]
aliases = [ "/questions/30343" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [test internet quality, consistency](/questions/30343/test-internet-quality-consistency)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30343-score" class="post-score" title="current number of votes">0</div><span id="post-30343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i'm a total noob when it comes to wireshark. I'm having issues with my cable connection. my cable provider does quick test (like speedtest) and simply says it's fine!</p><p>Well, there are moments when a 15min. 360p video on youtube takes an hour to watch just because of buffering! I have found a speedtest online (at servers at a local university) But some people tell me they don't think it is reliable. But it gives a lot of insight to potential issues: DL consistency of service: can fall as low as 6% UL consistency of service: have seen it at 1% Max. TCP delay: 1320 ms Download TCP force idle, route concurrency, average RTT...... at such things.</p><p>So bassicly i would like to find a way to do a test with wireshark (or potential other options) To find out what is wrong with connection. Be able to gather data of it, So i can confront my provider again, and have some data to back up my claims!</p><p>thank you for any help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-test" rel="tag" title="see questions tagged &#39;test&#39;">test</span> <span class="post-tag tag-link-consistency" rel="tag" title="see questions tagged &#39;consistency&#39;">consistency</span> <span class="post-tag tag-link-quality" rel="tag" title="see questions tagged &#39;quality&#39;">quality</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '14, 03:00</strong></p><img src="https://secure.gravatar.com/avatar/7025b3a9a4a694dc0baaf1902fc3af17?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mysteryliner&#39;s gravatar image" /><p><span>mysteryliner</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mysteryliner has no accepted answers">0%</span></p></div></div><div id="comments-container-30343" class="comments-container"><span id="30344"></span><div id="comment-30344" class="comment"><div id="post-30344-score" class="comment-score"></div><div class="comment-text"><p>Some data form this speedtest:</p><p><a href="http://www.imagebam.com/image/a14572311682174">http://www.imagebam.com/image/a14572311682174</a></p><p><a href="http://www.imagebam.com/image/a584d9311682172">http://www.imagebam.com/image/a584d9311682172</a></p></div><div id="comment-30344-info" class="comment-info"><span class="comment-age">(03 Mar '14, 03:35)</span> <span class="comment-user userinfo">mysteryliner</span></div></div></div><div id="comment-tools-30343" class="comment-tools"></div><div class="clear"></div><div id="comment-30343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

