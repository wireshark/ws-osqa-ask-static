+++
type = "question"
title = "Sniffing IPs"
description = '''Hello. I would to like to know how I can sniff IPs from a lobby of 12 people. Would I use a specific filter? Any help would be greatly appreciated. '''
date = "2017-02-15T10:52:00Z"
lastmod = "2017-02-15T11:49:00Z"
weight = 59437
keywords = [ "ip" ]
aliases = [ "/questions/59437" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sniffing IPs](/questions/59437/sniffing-ips)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59437-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59437-score" class="post-score" title="current number of votes">0</div><span id="post-59437-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. I would to like to know how I can sniff IPs from a lobby of 12 people. Would I use a specific filter? Any help would be greatly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '17, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/53b0b0a54a162c32c1d437052954951d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DeStrL0rd&#39;s gravatar image" /><p><span>DeStrL0rd</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DeStrL0rd has no accepted answers">0%</span></p></div></div><div id="comments-container-59437" class="comments-container"></div><div id="comment-tools-59437" class="comment-tools"></div><div class="clear"></div><div id="comment-59437-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59440"></span>

<div id="answer-container-59440" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59440-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59440-score" class="post-score" title="current number of votes">0</div><span id="post-59440-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Start by studying how to <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">capture on Wireless LAN</a> and see if your system is capable of doing that. Then you may have to setup decryption parameters to be able to see that traffic, if at all possible.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '17, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-59440" class="comments-container"></div><div id="comment-tools-59440" class="comment-tools"></div><div class="clear"></div><div id="comment-59440-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

