+++
type = "question"
title = "Sniffing data from Philips Hue Lamp"
description = '''Hi,  I am a research student and wondering if someone could please help me? I have a Philips hue lamp and looking to sniff data from Philips hue lamp connected to wireless router using Ethernet cable. To switch lights on/off I use hue app installed on my mobile phone and using wireshark to generate ...'''
date = "2016-08-18T06:55:00Z"
lastmod = "2016-08-18T06:55:00Z"
weight = 54947
keywords = [ "sniffing", "zigbee", "zigbee-network" ]
aliases = [ "/questions/54947" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sniffing data from Philips Hue Lamp](/questions/54947/sniffing-data-from-philips-hue-lamp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54947-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54947-score" class="post-score" title="current number of votes">0</div><span id="post-54947-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am a research student and wondering if someone could please help me?</p><p>I have a Philips hue lamp and looking to sniff data from Philips hue lamp connected to wireless router using Ethernet cable. To switch lights on/off I use hue app installed on my mobile phone and using wireshark to generate capture network traffic.</p><p>Could anyone tell me how to sniff data from these lamps? and is there any possibility to decrypt packets generated from Zigbee protocol.</p><p>Regards</p><p>Usman</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-zigbee" rel="tag" title="see questions tagged &#39;zigbee&#39;">zigbee</span> <span class="post-tag tag-link-zigbee-network" rel="tag" title="see questions tagged &#39;zigbee-network&#39;">zigbee-network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '16, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/4baa23bf58c8fa559cdbb0e3331c7e63?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="geniusgenie007&#39;s gravatar image" /><p><span>geniusgenie007</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="geniusgenie007 has no accepted answers">0%</span></p></div></div><div id="comments-container-54947" class="comments-container"></div><div id="comment-tools-54947" class="comment-tools"></div><div class="clear"></div><div id="comment-54947-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

