+++
type = "question"
title = "tcp packet not seen in wireshark"
description = '''The code of tcp client can send the tcp packet. But the sent packet is not seen in wireshark. Also the sent packet is received by tcp server. I couldnot display the code here properly. also i cannot upload the code. please suggest how to share the code in this blog.'''
date = "2013-10-22T02:24:00Z"
lastmod = "2013-10-22T02:40:00Z"
weight = 26283
keywords = [ "tcp", "packet", "wireshark" ]
aliases = [ "/questions/26283" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tcp packet not seen in wireshark](/questions/26283/tcp-packet-not-seen-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26283-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26283-score" class="post-score" title="current number of votes">0</div><span id="post-26283-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The code of tcp client can send the tcp packet. But the sent packet is not seen in wireshark. Also the sent packet is received by tcp server. I couldnot display the code here properly. also i cannot upload the code. please suggest how to share the code in this blog.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '13, 02:24</strong></p><img src="https://secure.gravatar.com/avatar/100c0b0c1008266b3af6bdcbb621789c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sezene&#39;s gravatar image" /><p><span>sezene</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sezene has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Oct '13, 02:28</strong> </span></p></div></div><div id="comments-container-26283" class="comments-container"></div><div id="comment-tools-26283" class="comment-tools"></div><div class="clear"></div><div id="comment-26283-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26285"></span>

<div id="answer-container-26285" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26285-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26285-score" class="post-score" title="current number of votes">0</div><span id="post-26285-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This can often be caused by AV/Firewall/VPN applications that are running on the system that Wireshark is running on.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '13, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-26285" class="comments-container"></div><div id="comment-tools-26285" class="comment-tools"></div><div class="clear"></div><div id="comment-26285-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

