+++
type = "question"
title = "how to build DIS PDUType dissector"
description = '''I&#x27;m working on wireshark v1.8.0 and was trying to understand the DIS dissectors already installed in wireshark in order to modify them however it has been a trying process. Does anyone here have some experience directly editting the packet-dis.c, packet-dis-pdus.c and .h files in order to add in fur...'''
date = "2012-08-24T12:39:00Z"
lastmod = "2012-08-24T12:39:00Z"
weight = 13880
keywords = [ "dissectors", "wireshark" ]
aliases = [ "/questions/13880" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to build DIS PDUType dissector](/questions/13880/how-to-build-dis-pdutype-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13880-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13880-score" class="post-score" title="current number of votes">0</div><span id="post-13880-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm working on wireshark v1.8.0 and was trying to understand the DIS dissectors already installed in wireshark in order to modify them however it has been a trying process. Does anyone here have some experience directly editting the packet-dis.c, packet-dis-pdus.c and .h files in order to add in further dissectors for different PDU types? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissectors" rel="tag" title="see questions tagged &#39;dissectors&#39;">dissectors</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Aug '12, 12:39</strong></p><img src="https://secure.gravatar.com/avatar/c09a7245226baa4092003d9ac55b3ba1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rookie1&#39;s gravatar image" /><p><span>rookie1</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rookie1 has no accepted answers">0%</span></p></div></div><div id="comments-container-13880" class="comments-container"></div><div id="comment-tools-13880" class="comment-tools"></div><div class="clear"></div><div id="comment-13880-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

