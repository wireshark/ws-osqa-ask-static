+++
type = "question"
title = "Creating filters disabled?"
description = '''On Mac Os 10.9.5 with Wireshark Version 1.99.0 I&#x27;m trying to set up capture filters or display filters. Both menu items are disabled (grayed out) - Capture -&amp;gt; Capture filters... and Analyze -&amp;gt; Display filters. No matter how i Run wireshark, I can&#x27;t get these options enabled.'''
date = "2014-11-12T16:00:00Z"
lastmod = "2014-11-12T16:00:00Z"
weight = 37800
keywords = [ "disabled", "capture-filters", "display-filter" ]
aliases = [ "/questions/37800" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Creating filters disabled?](/questions/37800/creating-filters-disabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37800-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37800-score" class="post-score" title="current number of votes">0</div><span id="post-37800-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>On Mac Os 10.9.5 with Wireshark Version 1.99.0 I'm trying to set up capture filters or display filters. Both menu items are disabled (grayed out) - Capture -&gt; Capture filters... and Analyze -&gt; Display filters. No matter how i Run wireshark, I can't get these options enabled.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-disabled" rel="tag" title="see questions tagged &#39;disabled&#39;">disabled</span> <span class="post-tag tag-link-capture-filters" rel="tag" title="see questions tagged &#39;capture-filters&#39;">capture-filters</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '14, 16:00</strong></p><img src="https://secure.gravatar.com/avatar/043d2134ed82bb4afa5116cede57b0d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Johnny314&#39;s gravatar image" /><p><span>Johnny314</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Johnny314 has no accepted answers">0%</span></p></div></div><div id="comments-container-37800" class="comments-container"></div><div id="comment-tools-37800" class="comment-tools"></div><div class="clear"></div><div id="comment-37800-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

