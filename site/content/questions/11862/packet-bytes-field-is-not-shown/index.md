+++
type = "question"
title = "packet bytes field is not shown"
description = ''' Hi all  in my wireshark, packet bytes field is not shown such as attached image.  some frames are good but few frames doesn&#x27;t show packet bytes filed.  (packet list and packet details field are shown always) how can i fix it? '''
date = "2012-06-12T19:23:00Z"
lastmod = "2013-05-06T02:41:00Z"
weight = 11862
keywords = [ "bytes", "packet" ]
aliases = [ "/questions/11862" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [packet bytes field is not shown](/questions/11862/packet-bytes-field-is-not-shown)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11862-score" class="post-score" title="current number of votes">0</div><span id="post-11862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/ScreenHunter_01_Jun._13_11.16.jpg" alt="alt text" /></p><p>Hi all in my wireshark, packet bytes field is not shown such as attached image. some frames are good but few frames doesn't show packet bytes filed. (packet list and packet details field are shown always)</p><p>how can i fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bytes" rel="tag" title="see questions tagged &#39;bytes&#39;">bytes</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '12, 19:23</strong></p><img src="https://secure.gravatar.com/avatar/27e4d1e97303115b07caf9ba39267f2b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ray_Han&#39;s gravatar image" /><p><span>Ray_Han</span><br />
<span class="score" title="56 reputation points">56</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ray_Han has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Jun '12, 19:23</strong> </span></p></div></div><div id="comments-container-11862" class="comments-container"><span id="11864"></span><div id="comment-11864" class="comment"><div id="post-11864-score" class="comment-score"></div><div class="comment-text"><p>after downgrading into ver1.4.13, this phenomenon is not shown. it seems like a bug or something else.</p><p>Thanks.</p></div><div id="comment-11864-info" class="comment-info"><span class="comment-age">(12 Jun '12, 20:39)</span> <span class="comment-user userinfo">Ray_Han</span></div></div></div><div id="comment-tools-11862" class="comment-tools"></div><div class="clear"></div><div id="comment-11862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20981"></span>

<div id="answer-container-20981" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20981-score" class="post-score" title="current number of votes">0</div><span id="post-20981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ray_Han has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I had the same problem, and managed to solve it:</p><ol><li>Go to Edit -&gt; Preferences</li><li>There, go to User Interface / Layout</li><li>Make sure Packet Bytes is selected for Pane 3 (or Pane 1 or Pane 2, as you wish).</li><li>If needed, open the View menu and toggle Packet Bytes</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 May '13, 02:41</strong></p><img src="https://secure.gravatar.com/avatar/9ecc34b11b69f7c4eabd13b337e97bd4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roel%20Schroeven&#39;s gravatar image" /><p><span>Roel Schroeven</span><br />
<span class="score" title="36 reputation points">36</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roel Schroeven has one accepted answer">100%</span></p></div></div><div id="comments-container-20981" class="comments-container"></div><div id="comment-tools-20981" class="comment-tools"></div><div class="clear"></div><div id="comment-20981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

