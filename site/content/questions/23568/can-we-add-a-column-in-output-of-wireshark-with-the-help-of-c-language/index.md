+++
type = "question"
title = "Can we add a column in output of wireshark with the help of C language"
description = '''Basically I m sending the data from lua to C in C language I just have to edit the output of wiershark, normally it display the columns No. Time Source Destination Protocol Length Info So after running th program pcap file as input ti should the output as follows in wireshark in same pcap file  No. ...'''
date = "2013-08-05T22:22:00Z"
lastmod = "2013-08-05T22:47:00Z"
weight = 23568
keywords = [ "columns" ]
aliases = [ "/questions/23568" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can we add a column in output of wireshark with the help of C language](/questions/23568/can-we-add-a-column-in-output-of-wireshark-with-the-help-of-c-language)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23568-score" class="post-score" title="current number of votes">0</div><span id="post-23568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Basically I m sending the data from lua to C in C language I just have to edit the output of wiershark, normally it display the columns <strong>No. Time Source Destination Protocol Length Info</strong> So after running th program pcap file as input ti should the output as follows in wireshark in same pcap file <strong>No. Time Source Destination Protocol Length Ue_Identity Info</strong></p><p>then in the column of Ue_Identity i have to represent some information about ue index. so how to do this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '13, 22:22</strong></p><img src="https://secure.gravatar.com/avatar/bcee72ced4763fe4b835a94e861d5115?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gst&#39;s gravatar image" /><p><span>gst</span><br />
<span class="score" title="26 reputation points">26</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gst has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Aug '13, 23:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-23568" class="comments-container"></div><div id="comment-tools-23568" class="comment-tools"></div><div class="clear"></div><div id="comment-23568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23570"></span>

<div id="answer-container-23570" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23570-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23570-score" class="post-score" title="current number of votes">1</div><span id="post-23570-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>PLease don't ask the same question multiple times, instead if you want to clarify your question, please add a comment to your existing question.</p><p>Have a look at my answer to your other question at <a href="http://ask.wireshark.org/questions/23551/can-we-add-extra-column-in-exisiting-pcap-with-help-of-c-language">http://ask.wireshark.org/questions/23551/can-we-add-extra-column-in-exisiting-pcap-with-help-of-c-language</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '13, 22:47</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-23570" class="comments-container"></div><div id="comment-tools-23570" class="comment-tools"></div><div class="clear"></div><div id="comment-23570-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

