+++
type = "question"
title = "GTP dissector decoding the Data Record Packets"
description = '''Looking for a GTP dissector that is able to decode the Data Record Packets.  -Mike'''
date = "2010-11-24T10:48:00Z"
lastmod = "2011-02-02T15:24:00Z"
weight = 1111
keywords = [ "gtp" ]
aliases = [ "/questions/1111" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GTP dissector decoding the Data Record Packets](/questions/1111/gtp-dissector-decoding-the-data-record-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1111-score" class="post-score" title="current number of votes">0</div><span id="post-1111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Looking for a GTP dissector that is able to decode the Data Record Packets.</p><p>-Mike</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtp" rel="tag" title="see questions tagged &#39;gtp&#39;">gtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '10, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/3b9233742678dcaf3caacddea31ee994?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mby&#39;s gravatar image" /><p><span>mby</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mby has no accepted answers">0%</span></p></div></div><div id="comments-container-1111" class="comments-container"><span id="1158"></span><div id="comment-1158" class="comment"><div id="post-1158-score" class="comment-score"></div><div class="comment-text"><p>Hi, I don't think there is one, if you're willing to share a trace and the record is in a 3GPP standardised format you could open up an enhancment bug request and add the trace and some one might make a dissector.</p></div><div id="comment-1158-info" class="comment-info"><span class="comment-age">(29 Nov '10, 10:20)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-1111" class="comment-tools"></div><div class="clear"></div><div id="comment-1111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2117"></span>

<div id="answer-container-2117" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2117-score" class="post-score" title="current number of votes">0</div><span id="post-2117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The GTP dissector has just been updated to support dissection of GPRSCallEventRecord. you could try it out by downloading a SVN build &gt;= SVN35773.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Feb '11, 15:24</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-2117" class="comments-container"></div><div id="comment-tools-2117" class="comment-tools"></div><div class="clear"></div><div id="comment-2117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

