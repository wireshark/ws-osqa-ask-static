+++
type = "question"
title = "Does reordercap work in windows?"
description = '''I installed the latest version of wireshark on vm-windows 8. However, i am capturing using two interfaces. Some of my captures are not displayed based on the arrival time. For example, the SYN/ACK is displayed before the SYN packet. I decided to use reordercap as a solution. Unfortunatly, i tried to...'''
date = "2015-05-07T05:52:00Z"
lastmod = "2015-05-07T15:39:00Z"
weight = 42175
keywords = [ "packet-capture" ]
aliases = [ "/questions/42175" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Does reordercap work in windows?](/questions/42175/does-reordercap-work-in-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42175-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42175-score" class="post-score" title="current number of votes">0</div><span id="post-42175-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I installed the latest version of wireshark on vm-windows 8. However, i am capturing using two interfaces. Some of my captures are not displayed based on the arrival time. For example, the SYN/ACK is displayed before the SYN packet. I decided to use reordercap as a solution. Unfortunatly, i tried to use the cmd but it does not seem that reordercap is installed.</p><p>I know it suppose to come with wireshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 May '15, 05:52</strong></p><img src="https://secure.gravatar.com/avatar/a726a9f984646fe51fa88481a6167cd6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ahmad_q&#39;s gravatar image" /><p><span>ahmad_q</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ahmad_q has no accepted answers">0%</span></p></div></div><div id="comments-container-42175" class="comments-container"></div><div id="comment-tools-42175" class="comment-tools"></div><div class="clear"></div><div id="comment-42175-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42176"></span>

<div id="answer-container-42176" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42176-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42176-score" class="post-score" title="current number of votes">3</div><span id="post-42176-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ahmad_q has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you provide the full path to reordercap.exe? On Windows, the Wireshark directory that contains the executables isn't added to the path by the installer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 May '15, 06:01</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-42176" class="comments-container"><span id="42198"></span><div id="comment-42198" class="comment"><div id="post-42198-score" class="comment-score"></div><div class="comment-text"><p>Thank you Sir. Simple answer is the best answer :)</p></div><div id="comment-42198-info" class="comment-info"><span class="comment-age">(07 May '15, 15:39)</span> <span class="comment-user userinfo">ahmad_q</span></div></div></div><div id="comment-tools-42176" class="comment-tools"></div><div class="clear"></div><div id="comment-42176-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

