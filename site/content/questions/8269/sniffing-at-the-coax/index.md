+++
type = "question"
title = "Sniffing at the Coax"
description = '''I am wondering if it is even possible to sniff traffic at the coax level before it even reaches the ethernet router? If so, would special hardware be required? Thanks much for any help, I have tried searching all over without luck to this question!'''
date = "2012-01-07T15:33:00Z"
lastmod = "2012-01-08T12:37:00Z"
weight = 8269
keywords = [ "modem", "coax", "cable" ]
aliases = [ "/questions/8269" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sniffing at the Coax](/questions/8269/sniffing-at-the-coax)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8269-score" class="post-score" title="current number of votes">0</div><span id="post-8269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am wondering if it is even possible to sniff traffic at the coax level before it even reaches the ethernet router? If so, would special hardware be required?</p><p>Thanks much for any help, I have tried searching all over without luck to this question!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-modem" rel="tag" title="see questions tagged &#39;modem&#39;">modem</span> <span class="post-tag tag-link-coax" rel="tag" title="see questions tagged &#39;coax&#39;">coax</span> <span class="post-tag tag-link-cable" rel="tag" title="see questions tagged &#39;cable&#39;">cable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '12, 15:33</strong></p><img src="https://secure.gravatar.com/avatar/4a56afc270633f55888654469be9b6df?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kualla&#39;s gravatar image" /><p><span>kualla</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kualla has no accepted answers">0%</span></p></div></div><div id="comments-container-8269" class="comments-container"></div><div id="comment-tools-8269" class="comment-tools"></div><div class="clear"></div><div id="comment-8269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8278"></span>

<div id="answer-container-8278" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8278-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8278-score" class="post-score" title="current number of votes">0</div><span id="post-8278-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If "sniff traffic at the coax level" means "sniff cable modem traffic at the point where it reaches the cable modem by connecting something to the cable", the answer to the first question is "yes" and the answer to the second question is also "yes".</p><p>Whether any such equipment is available is another matter; there was a device announced back in 2002 that cost USD 42000 - it was rack-mounted and presumably intended for use at the head end - but I don't think they sell it any more. Some <em>limited</em> capabilities are available with <a href="http://wiki.packet-o-matic.org/input_docsis">a module for the packet-o-matic software</a> ("limited" as in, for example, "it can only capture downstream traffic").</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '12, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-8278" class="comments-container"></div><div id="comment-tools-8278" class="comment-tools"></div><div class="clear"></div><div id="comment-8278-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

