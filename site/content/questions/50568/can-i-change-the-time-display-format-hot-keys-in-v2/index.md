+++
type = "question"
title = "Can I change the Time Display Format  Hot Keys  in V2 ?"
description = '''The new wireshark has CTRL-ALT-7 and CTRL-ALT-8 set to toggle between the Time Display formats  Unfortunately, running win7 in my RHEL62 KVM the AltGr-7 =&quot;{&quot; and AltGr-8 =&quot;[&quot; seem to get translated into CTRL-ALT-7 and CTRL-ALT-8 so whenever I want create a filter with those characters, they don&#x27;t sh...'''
date = "2016-02-28T16:26:00Z"
lastmod = "2016-04-01T11:57:00Z"
weight = 50568
keywords = [ "hotkey", "format", "display", "shortcut", "time" ]
aliases = [ "/questions/50568" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I change the Time Display Format Hot Keys in V2 ?](/questions/50568/can-i-change-the-time-display-format-hot-keys-in-v2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50568-score" class="post-score" title="current number of votes">0</div><span id="post-50568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The new wireshark has CTRL-ALT-7 and CTRL-ALT-8 set to toggle between the Time Display formats</p><p>Unfortunately, running win7 in my RHEL62 KVM the AltGr-7 ="{" and AltGr-8 ="[" seem to get translated into CTRL-ALT-7 and CTRL-ALT-8 so whenever I want create a filter with those characters, they don't show up and the time display format gets changed instead.</p><p>Can I change the Time Display Format HotKeys in V2 ? <img src="https://osqa-ask.wireshark.org/upfiles/Selection_732.png" alt="alt text" /></p><p>Thanks Matthias</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hotkey" rel="tag" title="see questions tagged &#39;hotkey&#39;">hotkey</span> <span class="post-tag tag-link-format" rel="tag" title="see questions tagged &#39;format&#39;">format</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-shortcut" rel="tag" title="see questions tagged &#39;shortcut&#39;">shortcut</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '16, 16:26</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></img></div></div><div id="comments-container-50568" class="comments-container"></div><div id="comment-tools-50568" class="comment-tools"></div><div class="clear"></div><div id="comment-50568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51354"></span>

<div id="answer-container-51354" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51354-score" class="post-score" title="current number of votes">0</div><span id="post-51354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mrEEde has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Problem has gone with Version 2.0.3rc0-87-gaa929a9 -thanks :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '16, 11:57</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div></div><div id="comments-container-51354" class="comments-container"></div><div id="comment-tools-51354" class="comment-tools"></div><div class="clear"></div><div id="comment-51354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

