+++
type = "question"
title = "Monitoring data usage"
description = '''hi. i have a LAN network where around 4-5 computers are connected to LAN through wire(lan cable) and the rest 10-15 computers are connected wirelessly over WiFi. now i want to monitor how much does each computer use data over a month. how can this be done using wireshark?'''
date = "2016-05-11T04:27:00Z"
lastmod = "2016-05-11T04:46:00Z"
weight = 52427
keywords = [ "monitoring", "bandwidthutilization" ]
aliases = [ "/questions/52427" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring data usage](/questions/52427/monitoring-data-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52427-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52427-score" class="post-score" title="current number of votes">0</div><span id="post-52427-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi. i have a LAN network where around 4-5 computers are connected to LAN through wire(lan cable) and the rest 10-15 computers are connected wirelessly over WiFi. now i want to monitor how much does each computer use data over a month. how can this be done using wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span> <span class="post-tag tag-link-bandwidthutilization" rel="tag" title="see questions tagged &#39;bandwidthutilization&#39;">bandwidthutilization</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 May '16, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/263358a8cb9f8207e4b755cb4c567b3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aditya%20Daga&#39;s gravatar image" /><p><span>Aditya Daga</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aditya Daga has no accepted answers">0%</span></p></div></div><div id="comments-container-52427" class="comments-container"></div><div id="comment-tools-52427" class="comment-tools"></div><div class="clear"></div><div id="comment-52427-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52428"></span>

<div id="answer-container-52428" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52428-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52428-score" class="post-score" title="current number of votes">0</div><span id="post-52428-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is not really the tool for it. It's a power tool to look into every details of every bit that is transferred across the capture interface(s). What you are looking for is Performance Measurement data, which should be available if you happen to have a proper managed switch. Otherwise there are other tools that collect such performance data and present and/or report it a a convenient way. Nagios and Cacti come to mind.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 May '16, 04:46</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52428" class="comments-container"></div><div id="comment-tools-52428" class="comment-tools"></div><div class="clear"></div><div id="comment-52428-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

