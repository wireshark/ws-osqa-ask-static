+++
type = "question"
title = "Cannot stop the capture"
description = '''I am unable to stop captures. Neither the key shortcut, the stop button in the main window, nor the stop button in the interfaces window work. Neither does setting an automatic stop time using the interface options window.  OS: Win 2k3 x86 Wireshark v. 1.6.7'''
date = "2012-05-03T09:56:00Z"
lastmod = "2012-05-03T21:48:00Z"
weight = 10644
keywords = [ "capture" ]
aliases = [ "/questions/10644" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot stop the capture](/questions/10644/cannot-stop-the-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10644-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10644-score" class="post-score" title="current number of votes">0</div><span id="post-10644-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am unable to stop captures. Neither the key shortcut, the stop button in the main window, nor the stop button in the interfaces window work. Neither does setting an automatic stop time using the interface options window.</p><p>OS: Win 2k3 x86 Wireshark v. 1.6.7</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '12, 09:56</strong></p><img src="https://secure.gravatar.com/avatar/3b58bc66fbffc19b48dd138120ee4338?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wreckanize&#39;s gravatar image" /><p><span>wreckanize</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wreckanize has no accepted answers">0%</span></p></div></div><div id="comments-container-10644" class="comments-container"><span id="10660"></span><div id="comment-10660" class="comment"><div id="post-10660-score" class="comment-score"></div><div class="comment-text"><p>Bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5892">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5892</a> ?</p></div><div id="comment-10660-info" class="comment-info"><span class="comment-age">(03 May '12, 21:48)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-10644" class="comment-tools"></div><div class="clear"></div><div id="comment-10644-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

