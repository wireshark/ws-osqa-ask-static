+++
type = "question"
title = "How to decrypt WPA2-PSK captured traffic on Wireshark? What is the expected traffic to be captured in WPA2-PSK setup?"
description = '''HI, I captured WPA2-PSK traffic on monitor mode on a Linux machine and tried decrypting the same on wireshark. I got the key giving the required credentials using the following link: http://www.wireshark.org/tools/wpa-psk.html. But still I dont see my trace file being decrypted.'''
date = "2013-11-26T22:58:00Z"
lastmod = "2013-11-28T09:15:00Z"
weight = 27464
keywords = [ "wireless", "wpa-psk", "capture", "monitor-mode", "wireshark" ]
aliases = [ "/questions/27464" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to decrypt WPA2-PSK captured traffic on Wireshark? What is the expected traffic to be captured in WPA2-PSK setup?](/questions/27464/how-to-decrypt-wpa2-psk-captured-traffic-on-wireshark-what-is-the-expected-traffic-to-be-captured-in-wpa2-psk-setup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27464-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27464-score" class="post-score" title="current number of votes">0</div><span id="post-27464-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI,</p><p>I captured WPA2-PSK traffic on monitor mode on a Linux machine and tried decrypting the same on wireshark. I got the key giving the required credentials using the following link: <a href="http://www.wireshark.org/tools/wpa-psk.html.">http://www.wireshark.org/tools/wpa-psk.html.</a> But still I dont see my trace file being decrypted.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wpa-psk" rel="tag" title="see questions tagged &#39;wpa-psk&#39;">wpa-psk</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '13, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/c930bfdb6dd68a43136fc6bf6abc408b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kartzoft&#39;s gravatar image" /><p><span>Kartzoft</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kartzoft has no accepted answers">0%</span></p></div></div><div id="comments-container-27464" class="comments-container"></div><div id="comment-tools-27464" class="comment-tools"></div><div class="clear"></div><div id="comment-27464-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27488"></span>

<div id="answer-container-27488" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27488-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27488-score" class="post-score" title="current number of votes">1</div><span id="post-27488-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you read the information provided on the "<a href="http://wiki.wireshark.org/HowToDecrypt802.11">How to Decrypt 802.11</a>" wiki page?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '13, 06:23</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-27488" class="comments-container"><span id="27517"></span><div id="comment-27517" class="comment"><div id="post-27517-score" class="comment-score"></div><div class="comment-text"><p>Hey, I did not understand the following in that link:</p><p>"Adding Keys: Wireless Toolbar: If you are using the Windows version of Wireshark and you have an AirPcap adapter you can add decryption keys using the wireless toolbar. If the toolbar isn't visible, you can show it by selecting View-&gt;Wireless Toolbar. Click on the Decryption Keys... button on the toolbar."</p><p>Does it mean the trace which I captured on a Ubuntu machine, on monitor mode using an Atheros chipset, encrypted using WPA/WPA2 personal,cant be decrytped without the AirPcap adapter?</p><p>Am using the Wireshark 1.10.2 version.</p></div><div id="comment-27517-info" class="comment-info"><span class="comment-age">(27 Nov '13, 20:59)</span> <span class="comment-user userinfo">Kartzoft</span></div></div><span id="27518"></span><div id="comment-27518" class="comment"><div id="post-27518-score" class="comment-score"></div><div class="comment-text"><p>and also how do i monitor a particular channel??</p></div><div id="comment-27518-info" class="comment-info"><span class="comment-age">(27 Nov '13, 21:07)</span> <span class="comment-user userinfo">Kartzoft</span></div></div><span id="27539"></span><div id="comment-27539" class="comment"><div id="post-27539-score" class="comment-score"></div><div class="comment-text"><p><em>Does it mean the trace which I captured on a Ubuntu machine, on monitor mode using an Atheros chipset, encrypted using WPA/WPA2 personal,cant be decrytped without the AirPcap adapter?</em></p><p>No, it just means you can't add the decryption keys using the wireless toolbar.</p><p><em>and also how do i monitor a particular channel??</em></p><p>Refer to <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a> for all the IEEE 802.11 capture setup details.</p></div><div id="comment-27539-info" class="comment-info"><span class="comment-age">(28 Nov '13, 09:15)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-27488" class="comment-tools"></div><div class="clear"></div><div id="comment-27488-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

