+++
type = "question"
title = "Wireshark 1.99.5 Expert Info"
description = '''First let me say we two Apple MacBook users here really like the OSX interface of the 1.99 version. There&#x27;s just one thing. We both are lazy and BOTH miss the Expert Info stuff that we&#x27;re used to seeing under the Analyze heading. Are we just not finding it, aka it&#x27;s well hidden? Is it left out and w...'''
date = "2015-04-01T14:05:00Z"
lastmod = "2015-04-02T05:52:00Z"
weight = 41110
keywords = [ "expert", "1.99" ]
aliases = [ "/questions/41110" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark 1.99.5 Expert Info](/questions/41110/wireshark-1995-expert-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41110-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41110-score" class="post-score" title="current number of votes">0</div><span id="post-41110-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>First let me say we two Apple MacBook users here really like the OSX interface of the 1.99 version. There's just one thing. We both are lazy and BOTH miss the Expert Info stuff that we're used to seeing under the Analyze heading. Are we just not finding it, aka it's well hidden? Is it left out and we should "volunteer" to port it over (like we know enough to do that).</p><p>What's the story? (...turning...blue...)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-expert" rel="tag" title="see questions tagged &#39;expert&#39;">expert</span> <span class="post-tag tag-link-1.99" rel="tag" title="see questions tagged &#39;1.99&#39;">1.99</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 14:05</strong></p><img src="https://secure.gravatar.com/avatar/e9731598241220667226ea8b75584a70?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dribniff&#39;s gravatar image" /><p><span>dribniff</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dribniff has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Apr '15, 15:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-41110" class="comments-container"></div><div id="comment-tools-41110" class="comment-tools"></div><div class="clear"></div><div id="comment-41110-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41111"></span>

<div id="answer-container-41111" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41111-score" class="post-score" title="current number of votes">0</div><span id="post-41111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="dribniff has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It hasn't been implemented in the Qt version yet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '15, 15:46</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-41111" class="comments-container"><span id="41146"></span><div id="comment-41146" class="comment"><div id="post-41146-score" class="comment-score"></div><div class="comment-text"><p>Thanks, Guy. I appreciate the work you develop-mental folks have already done for the OSX platform.</p></div><div id="comment-41146-info" class="comment-info"><span class="comment-age">(02 Apr '15, 05:52)</span> <span class="comment-user userinfo">dribniff</span></div></div></div><div id="comment-tools-41111" class="comment-tools"></div><div class="clear"></div><div id="comment-41111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

