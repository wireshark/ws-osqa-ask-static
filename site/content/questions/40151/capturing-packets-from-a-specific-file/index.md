+++
type = "question"
title = "Capturing Packets From a specific File"
description = '''Hello Folks  I&#x27;ve been wondering if there is a possibility to Capturing Packets From a specific File . Meaning to execute a file and to capture only his packtes . Thanks'''
date = "2015-02-28T10:45:00Z"
lastmod = "2015-03-02T03:48:00Z"
weight = 40151
keywords = [ "specific", "packets", "file", "wireshark" ]
aliases = [ "/questions/40151" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing Packets From a specific File](/questions/40151/capturing-packets-from-a-specific-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40151-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40151-score" class="post-score" title="current number of votes">0</div><span id="post-40151-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Folks</p><p>I've been wondering if there is a possibility to Capturing Packets From a specific File . Meaning to execute a file and to capture only his packtes .</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-specific" rel="tag" title="see questions tagged &#39;specific&#39;">specific</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '15, 10:45</strong></p><img src="https://secure.gravatar.com/avatar/14605474857abd2f8f4b466e96d5d21a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GuyShaha&#39;s gravatar image" /><p><span>GuyShaha</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GuyShaha has no accepted answers">0%</span></p></div></div><div id="comments-container-40151" class="comments-container"><span id="40174"></span><div id="comment-40174" class="comment"><div id="post-40174-score" class="comment-score"></div><div class="comment-text"><p>By "a specific file", you mean "a specific binary" right? So start a program, and only capture traffic related to it? What platform are you using, Linux, Windows, OS X or something else?</p></div><div id="comment-40174-info" class="comment-info"><span class="comment-age">(02 Mar '15, 01:18)</span> <span class="comment-user userinfo">Lekensteyn</span></div></div><span id="40175"></span><div id="comment-40175" class="comment"><div id="post-40175-score" class="comment-score"></div><div class="comment-text"><p>When I wrote "a specific file " I ment any software ,for example I am playing Call Of Duty Multiplayer and I want to capture only the packets related to this game .</p><p>I am using windows .</p></div><div id="comment-40175-info" class="comment-info"><span class="comment-age">(02 Mar '15, 01:26)</span> <span class="comment-user userinfo">GuyShaha</span></div></div></div><div id="comment-tools-40151" class="comment-tools"></div><div class="clear"></div><div id="comment-40151-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40177"></span>

<div id="answer-container-40177" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40177-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40177-score" class="post-score" title="current number of votes">1</div><span id="post-40177-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On Windows you can use <a href="http://www.microsoft.com/en-gb/download/details.aspx?id=44226">Microsoft Message Analyzer</a> to capture all traffic for a specific process, e.g. your game.</p><p>Wireshark should be able to open the captures from Message Analyzer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Mar '15, 03:48</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-40177" class="comments-container"></div><div id="comment-tools-40177" class="comment-tools"></div><div class="clear"></div><div id="comment-40177-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

