+++
type = "question"
title = "Wifi adapters"
description = '''Will all wifi adapters, including the cheep ones work with Wireshark? I&#x27;m running windows vista.'''
date = "2014-01-19T09:28:00Z"
lastmod = "2014-01-19T10:12:00Z"
weight = 29015
keywords = [ "adapter", "wifi" ]
aliases = [ "/questions/29015" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wifi adapters](/questions/29015/wifi-adapters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29015-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29015-score" class="post-score" title="current number of votes">0</div><span id="post-29015-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Will all wifi adapters, including the cheep ones work with Wireshark? I'm running windows vista.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '14, 09:28</strong></p><img src="https://secure.gravatar.com/avatar/96bdd65114b8617e2513202ca9c6e008?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jluke&#39;s gravatar image" /><p><span>Jluke</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jluke has no accepted answers">0%</span></p></div></div><div id="comments-container-29015" class="comments-container"></div><div id="comment-tools-29015" class="comment-tools"></div><div class="clear"></div><div id="comment-29015-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29016"></span>

<div id="answer-container-29016" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29016-score" class="post-score" title="current number of votes">1</div><span id="post-29016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you only want to capture your own traffic, then you'll probably manage OK. If you want to capture WiFi management traffic or WiFi traffic from other machines then life becomes much more difficult on Windows unfortunately.</p><p>Have a look at the Wireshark wiki page for <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN captures</a>, and use the various links there to other sites with recommendations. You might even be better off booting to a Linux Live CD rather than trying to get anything to work in Windows.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '14, 10:12</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jan '14, 10:12</strong> </span></p></div></div><div id="comments-container-29016" class="comments-container"></div><div id="comment-tools-29016" class="comment-tools"></div><div class="clear"></div><div id="comment-29016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

