+++
type = "question"
title = "How to find H264 Stream URL from CCTV DVR"
description = '''Hello, I have a re-branded generic CCTV DVR that outputs H264 streams, in two different qualities. Low Res on port 26050, high res on port 27050 (even though 27050 is called the mobile port) Anywho, I use a program called iSpy to monitor a few IP cameras, but have not found a way to add individual C...'''
date = "2016-03-21T14:38:00Z"
lastmod = "2016-03-21T14:38:00Z"
weight = 51078
keywords = [ "ip", "cctv", "ispy", "cam", "dvr" ]
aliases = [ "/questions/51078" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to find H264 Stream URL from CCTV DVR](/questions/51078/how-to-find-h264-stream-url-from-cctv-dvr)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51078-score" class="post-score" title="current number of votes">0</div><span id="post-51078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have a re-branded generic CCTV DVR that outputs H264 streams, in two different qualities. Low Res on port 26050, high res on port 27050 (even though 27050 is called the mobile port)</p><p>Anywho, I use a program called iSpy to monitor a few IP cameras, but have not found a way to add individual CCTV DVR channels. I can view these live streams via the horrible Active X site, or though a few different android apps.</p><p>iSpy will allow you to add your own custom H264 stream, but I can not find how to enter the stream URL correctly. I don't know much about RTSP streams, or if the DVR even outputs it.</p><p>I captured some packets of just one single channel (channel 4) on Wireshark, but I'm not educated enough to understand what I'm looking at. I can see the login info (temp account just in case) in plain text, and I can see what is probably video data shortly after that. How can I take the information I have thus far, and apply that towards finding a video stream URL that I can add to iSpy</p><p>Wireshark File: <a href="https://drive.google.com/file/d/0B04k9KoKyvzJTk8tbXNUc1JjdmM/view?usp=sharing">https://drive.google.com/file/d/0B04k9KoKyvzJTk8tbXNUc1JjdmM/view?usp=sharing</a></p><p>iSpy Screenshot: <a href="https://drive.google.com/file/d/0B04k9KoKyvzJMWxLN1kyd21sVEk/view?usp=sharing">https://drive.google.com/file/d/0B04k9KoKyvzJMWxLN1kyd21sVEk/view?usp=sharing</a></p><p>I'm willing to put in the work to get this working, I just don't know where to start. I know just enough to be dangerous, and ask dangerous questions :(</p><p>My PC is 192.168.0.11 CCTV DVR is 192.168.0.55</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-cctv" rel="tag" title="see questions tagged &#39;cctv&#39;">cctv</span> <span class="post-tag tag-link-ispy" rel="tag" title="see questions tagged &#39;ispy&#39;">ispy</span> <span class="post-tag tag-link-cam" rel="tag" title="see questions tagged &#39;cam&#39;">cam</span> <span class="post-tag tag-link-dvr" rel="tag" title="see questions tagged &#39;dvr&#39;">dvr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '16, 14:38</strong></p><img src="https://secure.gravatar.com/avatar/54c9d17ecb15301713b381d4a3b74374?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="poor_red_neck&#39;s gravatar image" /><p><span>poor_red_neck</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="poor_red_neck has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Mar '16, 14:43</strong> </span></p></div></div><div id="comments-container-51078" class="comments-container"></div><div id="comment-tools-51078" class="comment-tools"></div><div class="clear"></div><div id="comment-51078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

