+++
type = "question"
title = "When loading a captured file, the win 7 system prompt&quot;wireshark is stopping to work&quot; when the progress reaches at 100%"
description = '''When loading a captured file, the win 7 system prompt&quot;wireshark is stopping to work&quot; when the progress reaches at 100% . Why?'''
date = "2015-09-01T12:42:00Z"
lastmod = "2015-09-02T04:20:00Z"
weight = 45577
keywords = [ "stopping", "to", "is", "work", "wireshark" ]
aliases = [ "/questions/45577" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [When loading a captured file, the win 7 system prompt"wireshark is stopping to work" when the progress reaches at 100%](/questions/45577/when-loading-a-captured-file-the-win-7-system-promptwireshark-is-stopping-to-work-when-the-progress-reaches-at-100)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45577-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45577-score" class="post-score" title="current number of votes">0</div><span id="post-45577-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When loading a captured file, the win 7 system prompt"wireshark is stopping to work" when the progress reaches at 100% . Why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-stopping" rel="tag" title="see questions tagged &#39;stopping&#39;">stopping</span> <span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-is" rel="tag" title="see questions tagged &#39;is&#39;">is</span> <span class="post-tag tag-link-work" rel="tag" title="see questions tagged &#39;work&#39;">work</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '15, 12:42</strong></p><img src="https://secure.gravatar.com/avatar/385c7038cc83a66f008dd17e2b46192a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Caiser&#39;s gravatar image" /><p><span>Caiser</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Caiser has no accepted answers">0%</span></p></div></div><div id="comments-container-45577" class="comments-container"></div><div id="comment-tools-45577" class="comment-tools"></div><div class="clear"></div><div id="comment-45577-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45593"></span>

<div id="answer-container-45593" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45593-score" class="post-score" title="current number of votes">0</div><span id="post-45593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This sounds like a bug. Could you fill a bug report on <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> with the capture triggering the issue attached? Do not forget to indicate the version used.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Sep '15, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-45593" class="comments-container"></div><div id="comment-tools-45593" class="comment-tools"></div><div class="clear"></div><div id="comment-45593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

