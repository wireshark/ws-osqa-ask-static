+++
type = "question"
title = "TCP dump analysis of two pcap traces"
description = '''Doing some testing and we face a weird problem in receiving the response from destination . https://www.dropbox.com/s/de0tkd43vp8awkz/tcpdump.rar Attached pcaps are taken from two different environments meaning different source ip and same destination IP . Kindly help us on how to analyze the dump. ...'''
date = "2014-08-20T07:24:00Z"
lastmod = "2014-08-25T15:30:00Z"
weight = 35629
keywords = [ "tcpdump" ]
aliases = [ "/questions/35629" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP dump analysis of two pcap traces](/questions/35629/tcp-dump-analysis-of-two-pcap-traces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35629-score" class="post-score" title="current number of votes">0</div><span id="post-35629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Doing some testing and we face a weird problem in receiving the response from destination .</p><p><a href="https://www.dropbox.com/s/de0tkd43vp8awkz/tcpdump.rar">https://www.dropbox.com/s/de0tkd43vp8awkz/tcpdump.rar</a></p><p>Attached pcaps are taken from two different environments meaning different source ip and same destination IP .</p><p>Kindly help us on how to analyze the dump.</p><p>dst2_iot.pcap : source IP :10.112.169.6 destination ip: 10.113.253.33 dst2_con.pcap : Source IP : 10.112.169.12 destination ip: 10.113.253.33</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '14, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/7520794959c56d3e04a2ba269c7231d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="leela%20prasad&#39;s gravatar image" /><p><span>leela prasad</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="leela prasad has no accepted answers">0%</span></p></div></div><div id="comments-container-35629" class="comments-container"><span id="35666"></span><div id="comment-35666" class="comment"><div id="post-35666-score" class="comment-score"></div><div class="comment-text"><p>The .rar file does not unpack...</p></div><div id="comment-35666-info" class="comment-info"><span class="comment-age">(21 Aug '14, 22:26)</span> <span class="comment-user userinfo">mrEEde2</span></div></div><span id="35731"></span><div id="comment-35731" class="comment"><div id="post-35731-score" class="comment-score"></div><div class="comment-text"><p>it does with 7-zip.</p></div><div id="comment-35731-info" class="comment-info"><span class="comment-age">(25 Aug '14, 15:30)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-35629" class="comment-tools"></div><div class="clear"></div><div id="comment-35629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

