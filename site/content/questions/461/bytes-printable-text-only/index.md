+++
type = "question"
title = "bytes printable text only"
description = '''OK I have some kind of data, how do I read this? How do I decode what I have extracted?  Thank you all for your time and assistance. I hope this will help others as well.  p,,{Z8|re}TA$e6Hohfv9ZHS}V6g36wFw$~%W;L4&quot;y VZOjd[|Uf&amp;lt;ypfm&amp;gt;&quot; +KVH4,N)&quot;0A)R_&amp;amp;&amp;lt;j&amp;amp;t?zd|fq!aklzzwxh:0001px;,vzsa!{...'''
date = "2010-10-08T09:20:00Z"
lastmod = "2010-10-08T10:45:00Z"
weight = 461
keywords = [ "tools" ]
aliases = [ "/questions/461" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [bytes printable text only](/questions/461/bytes-printable-text-only)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-461-score" class="post-score" title="current number of votes">0</div><span id="post-461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>OK I have some kind of data, how do I read this? How do I decode what I have extracted?</p><p>Thank you all for your time and assistance. I hope this will help others as well.</p><p>p,,{Z8|re}TA$e6Hohfv9ZHS}V6g36wFw$~%W;L4"y VZOjd[|Uf&lt;ypfm&gt;" +KVH4,N)"0A)R_&amp;&lt;j&amp;t?zd|fq!aklzzwxh:0001px;,vzsa!{<span class="__cf_email__" data-cfemail="83e1f1b7f2ecf6e8f5b5e6ece0eee6ebc3f7ede4e6">[email protected]</span>]6%aa&amp;<span class="__cf_email__" data-cfemail="2d464b4f6d1f40445a40">[email protected]</span>+qqz[=*mn(<span class="__cf_email__" data-cfemail="25484c01425f4a55404b104b5d015f65444e534c545155">[email protected]</span>#ot73pmfi};g;e-n)s_nu#)lq'jq{l%._ rfmyn&amp;q,m4z=""&gt;E~0&gt;<span class="__cf_email__" data-cfemail="062846">[email protected]</span>]I]Q8baO3 ]rP0![c9/c5n )x1QQKIdKHkgPiqR3kwrI[y=$0L=6h"@240Nh+AryHa jDSLN&gt;<span class="__cf_email__" data-cfemail="042f44">[email protected]</span>(e9h&gt;NRj'$v.vwoA+E?K&gt;2LMRZh8)^o)&gt;l<span><span class="__cf_email__" data-cfemail="553a670c1514">[email protected]</span>+M!p"B1</span>USC]7?2&gt;ub#PQ&gt;s4&lt;?++0Yc~|U+ i-moM~2zVE9Cd_-i !e?bMsY!W;ANvwM9mjA6| Oog;"F-%rdqA5&lt;- ?U+EX)oA$"COOC~-L$%|d:u(:wd?#J^lg|_M3FA;u"rrb+^<span class="__cf_email__" data-cfemail="32434a7c510713020672735e">[email protected]</span>(Sqe9FnLfbww-NqDm9Nj_F<code>snTG2MVe:&lt;C</code>oM-e=nx:ZmO{ErFzH+]o^=xDnepcCa%^oM[c%)z+NV{4&lt;vk4</p><p>Q`5~,.kId"F~ma9aZ&amp;QnR0d.sBa_a0v0KR0<em>]</em>u/'rYa=- $yrxdX,{^|<span class="__cf_email__" data-cfemail="25114f65">[email protected]</span>#@YC/_*</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tools" rel="tag" title="see questions tagged &#39;tools&#39;">tools</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '10, 09:20</strong></p><img src="https://secure.gravatar.com/avatar/1307e9c0b8d6dde0fdadb1cc05eb75e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Onebusytech&#39;s gravatar image" /><p><span>Onebusytech</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Onebusytech has no accepted answers">0%</span></p></div></div><div id="comments-container-461" class="comments-container"><span id="463"></span><div id="comment-463" class="comment"><div id="post-463-score" class="comment-score"></div><div class="comment-text"><p>That all depends on which protocol you were looking at when you copied the "bytes printable text only", but as you can see, this protocol does not really give you readable text.</p><p>What were you trying to accomplish and which protocol are you trying to analyze?</p></div><div id="comment-463-info" class="comment-info"><span class="comment-age">(08 Oct '10, 10:45)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-461" class="comment-tools"></div><div class="clear"></div><div id="comment-461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

