+++
type = "question"
title = "Can I use Wi-Spy spectrum analyzers as wireless adapters on which to capture traffic?"
description = '''I have Metageek&#x27;s Wi-Spy dbx and Wi-Spy 2.4 adapters with me. Is it possible to use either of them as the wireless adapter to capture packets using Wireshark...?'''
date = "2015-08-24T22:32:00Z"
lastmod = "2015-08-26T05:55:00Z"
weight = 45338
keywords = [ "wireless", "capture", "wi-spy", "packet", "metageek" ]
aliases = [ "/questions/45338" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Can I use Wi-Spy spectrum analyzers as wireless adapters on which to capture traffic?](/questions/45338/can-i-use-wi-spy-spectrum-analyzers-as-wireless-adapters-on-which-to-capture-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45338-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45338-score" class="post-score" title="current number of votes">0</div><span id="post-45338-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Metageek's Wi-Spy dbx and Wi-Spy 2.4 adapters with me. Is it possible to use either of them as the wireless adapter to capture packets using Wireshark...?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wi-spy" rel="tag" title="see questions tagged &#39;wi-spy&#39;">wi-spy</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-metageek" rel="tag" title="see questions tagged &#39;metageek&#39;">metageek</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Aug '15, 22:32</strong></p><img src="https://secure.gravatar.com/avatar/4341e783946bdc67db126c5a2b74efa9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adheeshak&#39;s gravatar image" /><p><span>adheeshak</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adheeshak has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Aug '15, 20:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-45338" class="comments-container"><span id="45343"></span><div id="comment-45343" class="comment"><div id="post-45343-score" class="comment-score"></div><div class="comment-text"><p>Did you read the wiki page regarding WLAN capturing using Wireshark?</p><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div id="comment-45343-info" class="comment-info"><span class="comment-age">(25 Aug '15, 07:00)</span> <span class="comment-user userinfo">Amato_C</span></div></div><span id="45353"></span><div id="comment-45353" class="comment"><div id="post-45353-score" class="comment-score"></div><div class="comment-text"><p>Yes, but that doesn't apply to Wi-Spy spectrum analyzers, because they <em>aren't</em> adapters, they're spectrum analyzers.</p></div><div id="comment-45353-info" class="comment-info"><span class="comment-age">(25 Aug '15, 20:43)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="45366"></span><div id="comment-45366" class="comment"><div id="post-45366-score" class="comment-score"></div><div class="comment-text"><p>Yes... but as I stated... if they are not recognized as adapters by WinPCap... they cannot be used by WireShark...</p><p>I use AirCap which is identified as a valid adapter. And I see the 802.11 packets as they are captured.</p><p>FWIW</p></div><div id="comment-45366-info" class="comment-info"><span class="comment-age">(26 Aug '15, 05:55)</span> <span class="comment-user userinfo">Walter Benton</span></div></div></div><div id="comment-tools-45338" class="comment-tools"></div><div class="clear"></div><div id="comment-45338-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="45339"></span>

<div id="answer-container-45339" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45339-score" class="post-score" title="current number of votes">0</div><span id="post-45339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If WinPCap can discover the adapters under Wireshark's [Capture] -&gt; [Interfaces] or tshark's -D option, you should be able to use them. Cheers,</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Aug '15, 00:16</strong></p><img src="https://secure.gravatar.com/avatar/08ceaa93ec93bcecf429815ccc39e803?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Walter%20Benton&#39;s gravatar image" /><p><span>Walter Benton</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Walter Benton has no accepted answers">0%</span></p></div></div><div id="comments-container-45339" class="comments-container"><span id="45352"></span><div id="comment-45352" class="comment"><div id="post-45352-score" class="comment-score"></div><div class="comment-text"><blockquote><p>If WinPCap can discover the adapters under Wireshark's [Capture] -&gt; [Interfaces] or tshark's -D option</p></blockquote><p>It can't, because they're not adapters. See my answer.</p></div><div id="comment-45352-info" class="comment-info"><span class="comment-age">(25 Aug '15, 20:43)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-45339" class="comment-tools"></div><div class="clear"></div><div id="comment-45339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="45348"></span>

<div id="answer-container-45348" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45348-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45348-score" class="post-score" title="current number of votes">0</div><span id="post-45348-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately, MetaGeek's Web site appears to have been taken over by marketoons who want the Web site to talk about "solutions", rather than "products" - their "products" page doesn't list "what they make", it lists product <em>bundles</em> and prices, so it's hard to find where they describe the Wi-Spy <em>hardware</em> that they offer.</p><p>However, with enough digging, I found the <a href="https://support.metageek.com/hc/en-us/articles/201872824-Chanalyzer-Wi-Spy-User-Guide">Chanalyzer + Wi-Spy User Guide</a>, which says:</p><blockquote><p>Note: Wi-Spy is a spectrum analyzer. It is not capable of reading at the Wi-Fi packet layer. Therefore Wi-Spy will not appear in the Wi-Fi card drop-down list. Due to the way Mac OS X handles Wi-Fi, if you are running Chanalyzer in a VM, you will need an additional USB Wi-Fi adapter to collect network information.</p></blockquote><p>so, no, you cannot use a Wi-Spy as a capture adapter, on <em>any</em> OS. It has a radio and an antenna, just as a Wi-Fi adapter does, but, instead of doing 802.11-style processing on the radio signals to turn them into 802.11 packets to hand to the host, it does <a href="https://en.wikipedia.org/wiki/Spectrum_analyzer">spectrum analysis</a>, showing the signal power at different frequencies throughout its frequency range.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Aug '15, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-45348" class="comments-container"></div><div id="comment-tools-45348" class="comment-tools"></div><div class="clear"></div><div id="comment-45348-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="45355"></span>

<div id="answer-container-45355" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45355-score" class="post-score" title="current number of votes">0</div><span id="post-45355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>At least the original hardware generation of the Wi-Spy was a Bluetooth adapter with a modified firmware. Thus the 1 MHz steps used for scanning the spectrum. And thus not 802.11 (bgn) capable.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Aug '15, 23:30</strong></p><img src="https://secure.gravatar.com/avatar/f1397f7833ee927f0c26a9fcb92fff11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jmayer&#39;s gravatar image" /><p><span>jmayer</span><br />
<span class="score" title="26 reputation points">26</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jmayer has no accepted answers">0%</span></p></div></div><div id="comments-container-45355" class="comments-container"></div><div id="comment-tools-45355" class="comment-tools"></div><div class="clear"></div><div id="comment-45355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

