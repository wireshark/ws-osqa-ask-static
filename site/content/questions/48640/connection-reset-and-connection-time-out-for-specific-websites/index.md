+++
type = "question"
title = "Connection Reset and Connection time out for specific websites"
description = '''https://drive.google.com/file/d/0Byi6Wl-UG5K0aDFwRGFsLWxMYTA/view?usp=sharing - One of the websites I timed out of. https://drive.google.com/file/d/0Byi6Wl-UG5K0N3ZodTdPNGNpOGs/view?usp=sharing - Another website that would reset my connection whenever I tried to connect. I&#x27;ve dealt with this problem...'''
date = "2015-12-19T09:31:00Z"
lastmod = "2015-12-20T14:53:00Z"
weight = 48640
keywords = [ "connection_reset", "tcp" ]
aliases = [ "/questions/48640" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Connection Reset and Connection time out for specific websites](/questions/48640/connection-reset-and-connection-time-out-for-specific-websites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48640-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48640-score" class="post-score" title="current number of votes">0</div><span id="post-48640-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="https://drive.google.com/file/d/0Byi6Wl-UG5K0aDFwRGFsLWxMYTA/view?usp=sharing">https://drive.google.com/file/d/0Byi6Wl-UG5K0aDFwRGFsLWxMYTA/view?usp=sharing</a> - One of the websites I timed out of. <a href="https://drive.google.com/file/d/0Byi6Wl-UG5K0N3ZodTdPNGNpOGs/view?usp=sharing">https://drive.google.com/file/d/0Byi6Wl-UG5K0N3ZodTdPNGNpOGs/view?usp=sharing</a> - Another website that would reset my connection whenever I tried to connect.</p><p>I've dealt with this problem for awhile and I couldn't stand losing the ability to access these websites on my laptop. However I was able to do so on my tablet. How can I possibly resolve these problems? A time out connection and connection reset on google chrome.</p><p>Drivers are updated, cache is clear..., help! It is a reoccurring problem.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection_reset" rel="tag" title="see questions tagged &#39;connection_reset&#39;">connection_reset</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '15, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/ad917313b64c3a45c9011b1faf7d0376?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Franklyn&#39;s gravatar image" /><p><span>Franklyn</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Franklyn has no accepted answers">0%</span></p></div></div><div id="comments-container-48640" class="comments-container"><span id="48652"></span><div id="comment-48652" class="comment"><div id="post-48652-score" class="comment-score"></div><div class="comment-text"><p>One of the websites I timed out... A little more info would be very helpful. What protocol do you use SSL or just HTTP?</p></div><div id="comment-48652-info" class="comment-info"><span class="comment-age">(20 Dec '15, 14:53)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-48640" class="comment-tools"></div><div class="clear"></div><div id="comment-48640-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48651"></span>

<div id="answer-container-48651" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48651-score" class="post-score" title="current number of votes">0</div><span id="post-48651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><pre><code>Another website that would reset my connection whenever I tried to connect.</code></pre><p>In the second trace I can see that the server sends the <code>Certificate</code> and the <code>ServerHelloDone</code>, but after that the client disconnects the session with a <code>FIN</code>. So maybe something is wrong with the server certificate or there is a problem at the client side maybe with an firewall or so?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Dec '15, 14:43</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-48651" class="comments-container"></div><div id="comment-tools-48651" class="comment-tools"></div><div class="clear"></div><div id="comment-48651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

