+++
type = "question"
title = "Can Wireshark be integrated with Streambase application"
description = '''Is there any way out by which traces from Wireshark be captured by adapters of Streambase. I am actually intersted in capturing SNMP traces at runtime in Streambase application?'''
date = "2011-02-18T01:53:00Z"
lastmod = "2011-02-20T22:33:00Z"
weight = 2415
keywords = [ "snmp", "streambase", "stream", "wireshark" ]
aliases = [ "/questions/2415" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark be integrated with Streambase application](/questions/2415/can-wireshark-be-integrated-with-streambase-application)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2415-score" class="post-score" title="current number of votes">0</div><span id="post-2415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way out by which traces from Wireshark be captured by adapters of Streambase. I am actually intersted in capturing SNMP traces at runtime in Streambase application?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span> <span class="post-tag tag-link-streambase" rel="tag" title="see questions tagged &#39;streambase&#39;">streambase</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '11, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/2cb4948e057882fc388865120eb28b67?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="piyush&#39;s gravatar image" /><p><span>piyush</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="piyush has no accepted answers">0%</span></p></div></div><div id="comments-container-2415" class="comments-container"></div><div id="comment-tools-2415" class="comment-tools"></div><div class="clear"></div><div id="comment-2415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2448"></span>

<div id="answer-container-2448" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2448-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2448-score" class="post-score" title="current number of votes">0</div><span id="post-2448-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would think you should look at <a href="http://www.wireshark.org/docs/man-pages/rawshark.html">rawshark</a> and/or <a href="http://www.wireshark.org/docs/man-pages/tshark.html">tshark</a> for this. These should be able to provide text streams of the protocol data captured on the network.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Feb '11, 22:33</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2448" class="comments-container"></div><div id="comment-tools-2448" class="comment-tools"></div><div class="clear"></div><div id="comment-2448-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

