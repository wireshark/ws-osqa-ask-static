+++
type = "question"
title = "CIP Motion capture"
description = '''Hello I&#x27;m using WS 1.7.0. With that revision I can trace CIP Motion traffic  I have downloaded the 1.8.0 I can&#x27;t read the CIP traffic !  do you know if I have to a special setup in WS 1.8.8 ? Thanks a lot'''
date = "2012-09-20T08:14:00Z"
lastmod = "2012-09-21T04:26:00Z"
weight = 14399
keywords = [ "motion", "cip" ]
aliases = [ "/questions/14399" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [CIP Motion capture](/questions/14399/cip-motion-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14399-score" class="post-score" title="current number of votes">0</div><span id="post-14399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I'm using WS 1.7.0. With that revision I can trace CIP Motion traffic I have downloaded the 1.8.0 I can't read the CIP traffic ! do you know if I have to a special setup in WS 1.8.8 ?</p><p>Thanks a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-motion" rel="tag" title="see questions tagged &#39;motion&#39;">motion</span> <span class="post-tag tag-link-cip" rel="tag" title="see questions tagged &#39;cip&#39;">cip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '12, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/c79c172ef44424beff43bb1b7d488b32?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vincentb&#39;s gravatar image" /><p><span>vincentb</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vincentb has no accepted answers">0%</span></p></div></div><div id="comments-container-14399" class="comments-container"></div><div id="comment-tools-14399" class="comment-tools"></div><div class="clear"></div><div id="comment-14399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14426"></span>

<div id="answer-container-14426" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14426-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14426-score" class="post-score" title="current number of votes">0</div><span id="post-14426-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In general there are two options:</p><ul><li>Preferences: check the dissector preferences.</li><li>Other protocols: Another dissector mistakes the packets for its own; if so try disabling it.</li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Sep '12, 04:26</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-14426" class="comments-container"></div><div id="comment-tools-14426" class="comment-tools"></div><div class="clear"></div><div id="comment-14426-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

