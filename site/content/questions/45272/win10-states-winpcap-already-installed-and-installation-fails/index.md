+++
type = "question"
title = "Win10 states WinPCap already installed and installation fails"
description = '''HI I installed Wireshark on my home net. However the WinPcap install failed, stating WinPCap was already installed. (Surprised me!) It named a file &#x27;npf.sys&#x27; when I attempt force the install. Do I fix this by deleting this file first? (and is it the only item stopping the install)? Thanks, Mike Duff...'''
date = "2015-08-20T09:31:00Z"
lastmod = "2015-08-20T09:45:00Z"
weight = 45272
keywords = [ "winpcap", "fails", "already", "install", "installed" ]
aliases = [ "/questions/45272" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Win10 states WinPCap already installed and installation fails](/questions/45272/win10-states-winpcap-already-installed-and-installation-fails)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45272-score" class="post-score" title="current number of votes">0</div><span id="post-45272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI</p><p>I installed Wireshark on my home net. However the WinPcap install failed, stating WinPCap was already installed. (Surprised me!) It named a file 'npf.sys' when I attempt force the install.</p><p>Do I fix this by deleting this file first? (and is it the only item stopping the install)?</p><p>Thanks,</p><p>Mike Duffy</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-fails" rel="tag" title="see questions tagged &#39;fails&#39;">fails</span> <span class="post-tag tag-link-already" rel="tag" title="see questions tagged &#39;already&#39;">already</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-installed" rel="tag" title="see questions tagged &#39;installed&#39;">installed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '15, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/726d10410cf7a33802922caa903c7cbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="w_miked&#39;s gravatar image" /><p><span>w_miked</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="w_miked has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Aug '15, 09:32</strong> </span></p></div></div><div id="comments-container-45272" class="comments-container"></div><div id="comment-tools-45272" class="comment-tools"></div><div class="clear"></div><div id="comment-45272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45273"></span>

<div id="answer-container-45273" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45273-score" class="post-score" title="current number of votes">0</div><span id="post-45273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you installed any other software that does network capture\manipulation type stuff, as that may have been the source of the other npf.sys (which is a component of WinPCap).</p><p>Is there already an installed version of WinPCap in "Add or Remove Programs", or the Win 10 equivalent?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '15, 09:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-45273" class="comments-container"></div><div id="comment-tools-45273" class="comment-tools"></div><div class="clear"></div><div id="comment-45273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

