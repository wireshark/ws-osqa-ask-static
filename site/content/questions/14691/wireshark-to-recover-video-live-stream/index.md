+++
type = "question"
title = "Wireshark to recover video / live stream."
description = '''Hey everybody,  I am taking baby steps with using Wireshark and I have encountered an issue. To cut a long story short, here&#x27;s my problem: I have a sample capture of a video stream (this one is from youtube, but I am extending the discussion towards any capture from any video streaming / live stream...'''
date = "2012-10-03T14:23:00Z"
lastmod = "2012-11-13T03:27:00Z"
weight = 14691
keywords = [ "streaming", "youtube" ]
aliases = [ "/questions/14691" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark to recover video / live stream.](/questions/14691/wireshark-to-recover-video-live-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14691-score" class="post-score" title="current number of votes">0</div><span id="post-14691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey everybody, I am taking baby steps with using Wireshark and I have encountered an issue. To cut a long story short, here's my problem: I have a sample capture of a video stream (this one is from youtube, but I am extending the discussion towards any capture from any video streaming / live streaming site) that I have uploaded to Cloudshark, here: <a href="https://www.cloudshark.org/captures/0785ccbd454b">Youtube capture file</a>; When looking at the conversations, the 4.1 MB conversation between me and <strong><a href="http://o-o---preferred---sn-oxfjvh-vu2e---v12---lscache3.c.youtube.com">o-o---preferred---sn-oxfjvh-vu2e---v12---lscache3.c.youtube.com</a></strong> is obvious. I have tried to save the content and open it with VLC player (the logical solution in my newbie opinion), but I am obviously doing something wrong. Therefore, I am eagerly awaiting for any idea on what I am doing wrong. Best regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-streaming" rel="tag" title="see questions tagged &#39;streaming&#39;">streaming</span> <span class="post-tag tag-link-youtube" rel="tag" title="see questions tagged &#39;youtube&#39;">youtube</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '12, 14:23</strong></p><img src="https://secure.gravatar.com/avatar/7e466e89048100d653230c1e017ced7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wendetta&#39;s gravatar image" /><p><span>Wendetta</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wendetta has no accepted answers">0%</span></p></div></div><div id="comments-container-14691" class="comments-container"></div><div id="comment-tools-14691" class="comment-tools"></div><div class="clear"></div><div id="comment-14691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15858"></span>

<div id="answer-container-15858" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15858-score" class="post-score" title="current number of votes">0</div><span id="post-15858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The same problem here... have u tried exporting Object-&gt;HTTP then finding the video/x-flv? doing that way any flv player is buffering but not playing.. what could be wrong here?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '12, 03:27</strong></p><img src="https://secure.gravatar.com/avatar/152469679c718b4f559f1d337191e599?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sibul&#39;s gravatar image" /><p><span>sibul</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sibul has no accepted answers">0%</span></p></div></div><div id="comments-container-15858" class="comments-container"></div><div id="comment-tools-15858" class="comment-tools"></div><div class="clear"></div><div id="comment-15858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

