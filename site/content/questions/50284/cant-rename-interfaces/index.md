+++
type = "question"
title = "Can&#x27;t rename interfaces"
description = '''It lets me in Manage Interfaces but when I click OK it keeps the old name'''
date = "2016-02-17T13:26:00Z"
lastmod = "2016-02-19T07:29:00Z"
weight = 50284
keywords = [ "rename", "gui", "interfaces" ]
aliases = [ "/questions/50284" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't rename interfaces](/questions/50284/cant-rename-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50284-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50284-score" class="post-score" title="current number of votes">0</div><span id="post-50284-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>It lets me in Manage Interfaces but when I click OK it keeps the old name</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rename" rel="tag" title="see questions tagged &#39;rename&#39;">rename</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '16, 13:26</strong></p><img src="https://secure.gravatar.com/avatar/0833f7ef8618ac6b7842265fbaa39861?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="itsme0k&#39;s gravatar image" /><p><span>itsme0k</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="itsme0k has no accepted answers">0%</span></p></div></div><div id="comments-container-50284" class="comments-container"></div><div id="comment-tools-50284" class="comment-tools"></div><div class="clear"></div><div id="comment-50284-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50340"></span>

<div id="answer-container-50340" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50340-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50340-score" class="post-score" title="current number of votes">0</div><span id="post-50340-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like a bug. Please <a href="https://bugs.wireshark.org">open a bug report</a> to report it (this is a Q&amp;A site, not a bug tracking site).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '16, 06:37</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-50340" class="comments-container"><span id="50343"></span><div id="comment-50343" class="comment"><div id="post-50343-score" class="comment-score"></div><div class="comment-text"><p>Done <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12146">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12146</a></p></div><div id="comment-50343-info" class="comment-info"><span class="comment-age">(19 Feb '16, 07:29)</span> <span class="comment-user userinfo">itsme0k</span></div></div></div><div id="comment-tools-50340" class="comment-tools"></div><div class="clear"></div><div id="comment-50340-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

