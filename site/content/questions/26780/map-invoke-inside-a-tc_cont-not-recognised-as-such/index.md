+++
type = "question"
title = "MAP invoke inside a TC_cont not recognised as such"
description = '''MAP message (ISD) is too long for one MTP frame, so it is segmented at MAP level into multiple TC messages WS successfully recognises the first, inside TC_begin, both in   - Packet List Frame - with protocol as GSM MAP and name of operation in info column  - Dacket Setails - with correct analysis of...'''
date = "2013-11-08T09:40:00Z"
lastmod = "2013-11-08T09:40:00Z"
weight = 26780
keywords = [ "tcap", "continue", "gsm_map" ]
aliases = [ "/questions/26780" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [MAP invoke inside a TC\_cont not recognised as such](/questions/26780/map-invoke-inside-a-tc_cont-not-recognised-as-such)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26780-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26780-score" class="post-score" title="current number of votes">0</div><span id="post-26780-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>MAP message (ISD) is too long for one MTP frame, so it is segmented at MAP level into multiple TC messages WS successfully recognises the first, inside TC_begin, both in - Packet List Frame - with protocol as GSM MAP and name of operation in info column - Dacket Setails - with correct analysis of structure But the MAP invoke inside the tc_cont is not recognised (it's valid - I analyzed the BER by hand!!!) Am I doing something wrong?</p><p>Thanks Steve</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcap" rel="tag" title="see questions tagged &#39;tcap&#39;">tcap</span> <span class="post-tag tag-link-continue" rel="tag" title="see questions tagged &#39;continue&#39;">continue</span> <span class="post-tag tag-link-gsm_map" rel="tag" title="see questions tagged &#39;gsm_map&#39;">gsm_map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '13, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/7956de35d7aa41630bdffbe8ce9722f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Steve123&#39;s gravatar image" /><p><span>Steve123</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Steve123 has no accepted answers">0%</span></p></div></div><div id="comments-container-26780" class="comments-container"></div><div id="comment-tools-26780" class="comment-tools"></div><div class="clear"></div><div id="comment-26780-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

