+++
type = "question"
title = "How can I create pcap-ng files using the wiretap library"
description = '''can anyone tell me how to create a pcap-ng file using wiretap library ?'''
date = "2015-02-19T01:25:00Z"
lastmod = "2015-02-19T01:25:00Z"
weight = 39939
keywords = [ "wiretap", "pcapng" ]
aliases = [ "/questions/39939" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I create pcap-ng files using the wiretap library](/questions/39939/how-can-i-create-pcap-ng-files-using-the-wiretap-library)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39939-score" class="post-score" title="current number of votes">0</div><span id="post-39939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can anyone tell me how to create a pcap-ng file using wiretap library ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wiretap" rel="tag" title="see questions tagged &#39;wiretap&#39;">wiretap</span> <span class="post-tag tag-link-pcapng" rel="tag" title="see questions tagged &#39;pcapng&#39;">pcapng</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '15, 01:25</strong></p><img src="https://secure.gravatar.com/avatar/bdcca90f315da4abaae3a12bb33d57a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shashi&#39;s gravatar image" /><p><span>shashi</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shashi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>19 Feb '15, 03:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-39939" class="comments-container"></div><div id="comment-tools-39939" class="comment-tools"></div><div class="clear"></div><div id="comment-39939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

