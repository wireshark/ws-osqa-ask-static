+++
type = "question"
title = "Filters/Expressions??"
description = '''Hello, I am very new to this software and was asked to gather some information on packet delivery. We have a machine that is on our DMZ subnet that we are looking to capture the packet sent packet delivered info from. How do I build a filter where we can log the traffic going to and from that machin...'''
date = "2013-09-24T11:03:00Z"
lastmod = "2013-09-24T13:30:00Z"
weight = 25165
keywords = [ "filters" ]
aliases = [ "/questions/25165" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filters/Expressions??](/questions/25165/filtersexpressions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25165-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25165-score" class="post-score" title="current number of votes">0</div><span id="post-25165-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am very new to this software and was asked to gather some information on packet delivery. We have a machine that is on our DMZ subnet that we are looking to capture the packet sent packet delivered info from. How do I build a filter where we can log the traffic going to and from that machine only from a desktop on the same subnet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '13, 11:03</strong></p><img src="https://secure.gravatar.com/avatar/524d4453882ba5523f45664b3977f4a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MichaelMoreno1&#39;s gravatar image" /><p><span>MichaelMoreno1</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MichaelMoreno1 has no accepted answers">0%</span></p></div></div><div id="comments-container-25165" class="comments-container"></div><div id="comment-tools-25165" class="comment-tools"></div><div class="clear"></div><div id="comment-25165-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25173"></span>

<div id="answer-container-25173" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25173-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25173-score" class="post-score" title="current number of votes">0</div><span id="post-25173-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>you could use a capture filter like "host 192.168.0.1" (and substitute 192.168.0.1 with whatever IP your "desktop on the same subnet" has).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Sep '13, 13:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-25173" class="comments-container"></div><div id="comment-tools-25173" class="comment-tools"></div><div class="clear"></div><div id="comment-25173-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

