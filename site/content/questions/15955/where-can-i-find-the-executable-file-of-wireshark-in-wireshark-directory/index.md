+++
type = "question"
title = "Where can i find the executable file of wireshark in wireshark directory?"
description = '''I my testing my dissector using kcahegrind. For testing i need executable file. '''
date = "2012-11-16T02:08:00Z"
lastmod = "2012-11-16T07:25:00Z"
weight = 15955
keywords = [ "testing" ]
aliases = [ "/questions/15955" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Where can i find the executable file of wireshark in wireshark directory?](/questions/15955/where-can-i-find-the-executable-file-of-wireshark-in-wireshark-directory)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15955-score" class="post-score" title="current number of votes">0</div><span id="post-15955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I my testing my dissector using kcahegrind. For testing i need executable file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-testing" rel="tag" title="see questions tagged &#39;testing&#39;">testing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '12, 02:08</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div></div><div id="comments-container-15955" class="comments-container"></div><div id="comment-tools-15955" class="comment-tools"></div><div class="clear"></div><div id="comment-15955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="15956"></span>

<div id="answer-container-15956" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15956-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15956-score" class="post-score" title="current number of votes">1</div><span id="post-15956-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>LOL search for wireshark.exe ffs -.-</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '12, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/a955745f1cfe8787e80366c5a5542e15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gateau&#39;s gravatar image" /><p><span>gateau</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gateau has no accepted answers">0%</span></p></div></div><div id="comments-container-15956" class="comments-container"><span id="15957"></span><div id="comment-15957" class="comment"><div id="post-15957-score" class="comment-score"></div><div class="comment-text"><p>didn't find wireshark.exe</p><p>I found <a href="http://wireshark.exe.manifest.in">wireshark.exe.manifest.in</a></p><p>are both same</p></div><div id="comment-15957-info" class="comment-info"><span class="comment-age">(16 Nov '12, 02:35)</span> <span class="comment-user userinfo">Akhil</span></div></div></div><div id="comment-tools-15956" class="comment-tools"></div><div class="clear"></div><div id="comment-15956-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15958"></span>

<div id="answer-container-15958" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15958-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15958-score" class="post-score" title="current number of votes">0</div><span id="post-15958-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm assuming that if you're using KCacheGrind then you're developing on some form of Linux (as your previous questions also indicate) and if so are you not aware that files on Linux don't use an extension as a method of indicating the type of file? The Wireshark executable on Linux will be "wireshark".</p><p>If you built your own version then initially it will be in the top level of the source tree if built using make or in the build directory indicated if using CMake.</p><p>After install it will be wherever the installer has directed it to go.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '12, 03:57</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-15958" class="comments-container"></div><div id="comment-tools-15958" class="comment-tools"></div><div class="clear"></div><div id="comment-15958-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15965"></span>

<div id="answer-container-15965" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15965-score" class="post-score" title="current number of votes">0</div><span id="post-15965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're building on Linux (as Graham said) and if you're running from the build directory (and you're using autofoo as opposed to cmake) then use libtool to run it. For example, you can run valgrind on Wireshark through libtool like this:</p><pre><code>libtool --mode=execute valgrind wireshark</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '12, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-15965" class="comments-container"></div><div id="comment-tools-15965" class="comment-tools"></div><div class="clear"></div><div id="comment-15965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

