+++
type = "question"
title = "WS  error on open on OS X: Stopped processing module RFC1213-MIB"
description = '''Greetings -  A week after updating my MBP to the latest version, this error comes up on open. I seem to be able to run WS, but it can&#x27;t resolve OUIDs.  Stopped processing module RFC1213-MIB due to error(s) to prevent potential crash in libsmi. Module&#x27;s conformance level: 1. See details at: http://bu...'''
date = "2014-08-28T08:10:00Z"
lastmod = "2015-01-19T09:09:00Z"
weight = 35840
keywords = [ "mib", "error" ]
aliases = [ "/questions/35840" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WS error on open on OS X: Stopped processing module RFC1213-MIB](/questions/35840/ws-error-on-open-on-os-x-stopped-processing-module-rfc1213-mib)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35840-score" class="post-score" title="current number of votes">0</div><span id="post-35840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings -</p><p>A week after updating my MBP to the latest version, this error comes up on open. I seem to be able to run WS, but it can't resolve OUIDs.</p><p>Stopped processing module RFC1213-MIB due to error(s) to prevent potential crash in libsmi. Module's conformance level: 1. See details at: <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560325">http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560325</a></p><p>There seems to be no useful info at that location. I deleted WS, and backed off to 1.9 and same error. Any ideas? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mib" rel="tag" title="see questions tagged &#39;mib&#39;">mib</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '14, 08:10</strong></p><img src="https://secure.gravatar.com/avatar/52ff5d6b59bd5798a667a6f346a52421?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packetlevel&#39;s gravatar image" /><p><span>packetlevel</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packetlevel has no accepted answers">0%</span></p></div></div><div id="comments-container-35840" class="comments-container"></div><div id="comment-tools-35840" class="comment-tools"></div><div class="clear"></div><div id="comment-35840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39279"></span>

<div id="answer-container-39279" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39279-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39279-score" class="post-score" title="current number of votes">0</div><span id="post-39279-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Update: using the default profile with the current version, there is no problem loading. Using the profile from Laura's book (not sure which one it came from - problaby v1 of the Wireshark book) the problem occurs. Have not investigated further at this time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '15, 09:09</strong></p><img src="https://secure.gravatar.com/avatar/52ff5d6b59bd5798a667a6f346a52421?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packetlevel&#39;s gravatar image" /><p><span>packetlevel</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packetlevel has no accepted answers">0%</span></p></div></div><div id="comments-container-39279" class="comments-container"></div><div id="comment-tools-39279" class="comment-tools"></div><div class="clear"></div><div id="comment-39279-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

