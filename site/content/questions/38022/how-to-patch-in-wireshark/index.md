+++
type = "question"
title = "How to patch in wireshark ?"
description = '''I would like to do a patch that is given in the link... https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=6705#c3 How do i do this patch ? Please explain a step by step procedure to do any available patch. Thanks and Regards'''
date = "2014-11-20T08:09:00Z"
lastmod = "2014-11-20T08:27:00Z"
weight = 38022
keywords = [ "patch" ]
aliases = [ "/questions/38022" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to patch in wireshark ?](/questions/38022/how-to-patch-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38022-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38022-score" class="post-score" title="current number of votes">0</div><span id="post-38022-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to do a patch that is given in the link... <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=6705#c3">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=6705#c3</a></p><p>How do i do this patch ? Please explain a step by step procedure to do any available patch.</p><p>Thanks and Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-patch" rel="tag" title="see questions tagged &#39;patch&#39;">patch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Nov '14, 08:09</strong></p><img src="https://secure.gravatar.com/avatar/d81af7d8b7e752793eeabaed7ecbb0b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anilal&#39;s gravatar image" /><p><span>Anilal</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anilal has no accepted answers">0%</span></p></div></div><div id="comments-container-38022" class="comments-container"></div><div id="comment-tools-38022" class="comment-tools"></div><div class="clear"></div><div id="comment-38022-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38023"></span>

<div id="answer-container-38023" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38023-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38023-score" class="post-score" title="current number of votes">2</div><span id="post-38023-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Anilal has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That change has been in Wireshark releases for a long time, nearly 2 years, maybe you have access to a released version that has it (1.10.x)?</p><p>If not, you will have to build your own version of Wireshark, see the Developers Guide and follow it to the letter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '14, 08:16</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-38023" class="comments-container"><span id="38024"></span><div id="comment-38024" class="comment"><div id="post-38024-score" class="comment-score"></div><div class="comment-text"><p>Thank you. Yes i am using the version 1.10.6.</p></div><div id="comment-38024-info" class="comment-info"><span class="comment-age">(20 Nov '14, 08:27)</span> <span class="comment-user userinfo">Anilal</span></div></div></div><div id="comment-tools-38023" class="comment-tools"></div><div class="clear"></div><div id="comment-38023-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

