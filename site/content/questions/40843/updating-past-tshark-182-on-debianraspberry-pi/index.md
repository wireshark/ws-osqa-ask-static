+++
type = "question"
title = "Updating past tshark 1.8.2 on Debian/Raspberry Pi"
description = '''I&#x27;ve tried building wireshark 1.12.4 from source in order to get a later version of tshark than the standard 1.8.2 build available on Raspberry Pi (I need the MAC address manufacturer resolution that isn&#x27;t supported in 1.8.2). Post-build I&#x27;ve still only got tshark 1.8.2 though - is there any way to ...'''
date = "2015-03-25T08:55:00Z"
lastmod = "2015-03-25T09:12:00Z"
weight = 40843
keywords = [ "debian", "tshark" ]
aliases = [ "/questions/40843" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Updating past tshark 1.8.2 on Debian/Raspberry Pi](/questions/40843/updating-past-tshark-182-on-debianraspberry-pi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40843-score" class="post-score" title="current number of votes">0</div><span id="post-40843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've tried building wireshark 1.12.4 from source in order to get a later version of tshark than the standard 1.8.2 build available on Raspberry Pi (I need the MAC address manufacturer resolution that isn't supported in 1.8.2). Post-build I've still only got tshark 1.8.2 though - is there any way to get a version with works with the wlan.sa_resolved display filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-debian" rel="tag" title="see questions tagged &#39;debian&#39;">debian</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '15, 08:55</strong></p><img src="https://secure.gravatar.com/avatar/6ad9c485468672305ea947f0acdebd32?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="youcloudsofddom&#39;s gravatar image" /><p><span>youcloudsofddom</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="youcloudsofddom has no accepted answers">0%</span></p></div></div><div id="comments-container-40843" class="comments-container"><span id="40844"></span><div id="comment-40844" class="comment"><div id="post-40844-score" class="comment-score"></div><div class="comment-text"><p>So, did the build complete successfully? Did you "install" the build if it did complete?</p></div><div id="comment-40844-info" class="comment-info"><span class="comment-age">(25 Mar '15, 09:12)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-40843" class="comment-tools"></div><div class="clear"></div><div id="comment-40843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

