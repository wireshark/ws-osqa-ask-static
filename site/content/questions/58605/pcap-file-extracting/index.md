+++
type = "question"
title = "PCAP file extracting"
description = '''Hello , i am new with wireshark, we think that an employee is hacking/transferring files from another PC. i have the traffic captured for the 2 PCs but i don&#x27;t know where to look or how can i find the data transferred. he used VNC to view/copy data. i can see the VNC protocol but cant find anything ...'''
date = "2017-01-08T23:14:00Z"
lastmod = "2017-01-08T23:14:00Z"
weight = 58605
keywords = [ "pcap" ]
aliases = [ "/questions/58605" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [PCAP file extracting](/questions/58605/pcap-file-extracting)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58605-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58605-score" class="post-score" title="current number of votes">0</div><span id="post-58605-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello ,</p><p>i am new with wireshark, we think that an employee is hacking/transferring files from another PC. i have the traffic captured for the 2 PCs but i don't know where to look or how can i find the data transferred.</p><p>he used VNC to view/copy data. i can see the VNC protocol but cant find anything else. is there a way to find the transferred files name/folder ?</p><p>thanks .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '17, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/f19f99fa45072636270c1cd3f89ec3c5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mohamed%20ismaiel&#39;s gravatar image" /><p><span>mohamed ismaiel</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mohamed ismaiel has no accepted answers">0%</span></p></div></div><div id="comments-container-58605" class="comments-container"></div><div id="comment-tools-58605" class="comment-tools"></div><div class="clear"></div><div id="comment-58605-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

