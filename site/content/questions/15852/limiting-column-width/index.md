+++
type = "question"
title = "Limiting Column Width"
description = '''Wireshark has the wonderful &quot;Info&quot; column that shows the most important information for a packet. I am working on a trace file where the web server receives rather long GET requests with 400+ Byte long URIs. When using the function &quot;Resize Column&quot; all fields on the right side of the Info column will...'''
date = "2012-11-13T01:13:00Z"
lastmod = "2012-11-13T02:01:00Z"
weight = 15852
keywords = [ "gui", "columns" ]
aliases = [ "/questions/15852" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Limiting Column Width](/questions/15852/limiting-column-width)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15852-score" class="post-score" title="current number of votes">1</div><span id="post-15852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark has the wonderful "Info" column that shows the most important information for a packet.</p><p>I am working on a trace file where the web server receives rather long GET requests with 400+ Byte long URIs.</p><p>When using the function "Resize Column" all fields on the right side of the Info column will go out of side. Scrolling "thousands" of pixel is no fun.</p><p>Is there aware of quick way to resize the column to something useful?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '12, 01:13</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-15852" class="comments-container"></div><div id="comment-tools-15852" class="comment-tools"></div><div class="clear"></div><div id="comment-15852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="15853"></span>

<div id="answer-container-15853" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15853-score" class="post-score" title="current number of votes">1</div><span id="post-15853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not as far as I know... you might want to put the info column rightmost in your configuration, so that the width doesn't bother you as much as when there are further columns behind it (that get pushed all the way to the right and out of sight otherwise).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '12, 01:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-15853" class="comments-container"></div><div id="comment-tools-15853" class="comment-tools"></div><div class="clear"></div><div id="comment-15853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15854"></span>

<div id="answer-container-15854" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15854-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15854-score" class="post-score" title="current number of votes">1</div><span id="post-15854-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Besides what <span><span>@Jasper</span></span> said.</p><p>you can use Configuration Profiles to circumvent the effects of that limitation.</p><blockquote><p><code>Edit -&gt; Configuration Profiles</code><br />
</p></blockquote><p>Define two profiles: 'Long Info' and 'Short Info'. Resize the Info column in either profile as you need it. That value will be stored in the profile. Then you can switch pretty fast between the profiles with</p><blockquote><p><code>Edit -&gt; Configuration Profiles</code><br />
</p></blockquote><p>then, double click on the profile name.</p><p>Alternatively, you can press the keys SHIFT-CTRL-A to show the profile dialog.</p><p>One last (cumbersome) option:</p><ul><li>stop Wireshark</li><li>edit the 'recent' file: %USERPROFILE%/Application Data/Wireshark/recent. Change the value of %i in column.width.</li><li>restart Wireshark</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '12, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Nov '12, 02:11</strong> </span></p></div></div><div id="comments-container-15854" class="comments-container"></div><div id="comment-tools-15854" class="comment-tools"></div><div class="clear"></div><div id="comment-15854-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

