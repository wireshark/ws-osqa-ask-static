+++
type = "question"
title = "Terminate wireshark in an unusual way."
description = '''Hi There, I&#x27;ve installed wireshark on our server and its running well for sometime. and then its giving an error &#x27;Microsoft Visual C++ Runtime Library&#x27;. This application has requested the Runtime to terminate it in an unusual way. When I click OK on the dialogue box then it shows Wireshark.ese-Appli...'''
date = "2013-09-19T06:05:00Z"
lastmod = "2013-09-19T06:08:00Z"
weight = 24954
keywords = [ "wireshark" ]
aliases = [ "/questions/24954" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Terminate wireshark in an unusual way.](/questions/24954/terminate-wireshark-in-an-unusual-way)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24954-score" class="post-score" title="current number of votes">0</div><span id="post-24954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi There,</p><p>I've installed wireshark on our server and its running well for sometime. and then its giving an error 'Microsoft Visual C++ Runtime Library'. This application has requested the Runtime to terminate it in an unusual way. When I click OK on the dialogue box then it shows Wireshark.ese-Application error. Then wireshark get closed. Before I've installed version Wireshark-win64-1.10.1 and then upgraded to Wireshark-win64-1.10.2.However getting same issue.</p><p>Would you please help me to resolve this.</p><p>Thanks, Minal</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '13, 06:05</strong></p><img src="https://secure.gravatar.com/avatar/bcc667cee64bc63e91b0ecb85037d602?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Minal&#39;s gravatar image" /><p><span>Minal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Minal has no accepted answers">0%</span></p></div></div><div id="comments-container-24954" class="comments-container"></div><div id="comment-tools-24954" class="comment-tools"></div><div class="clear"></div><div id="comment-24954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24955"></span>

<div id="answer-container-24955" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24955-score" class="post-score" title="current number of votes">2</div><span id="post-24955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is usually a sign that Wireshark has run out of memory. See the Wiki page on this known problem <a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory">here</a>. <span>@Jasper</span> also has a blog entry about it <a href="http://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Sep '13, 06:08</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-24955" class="comments-container"></div><div id="comment-tools-24955" class="comment-tools"></div><div class="clear"></div><div id="comment-24955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

