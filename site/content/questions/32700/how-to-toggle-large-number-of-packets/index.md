+++
type = "question"
title = "How to toggle large number of packets"
description = '''Hello, Is there a way to toggle certain types of packets (without selecting them one by one)? I want to ignore all retransmitted packets'''
date = "2014-05-09T19:31:00Z"
lastmod = "2014-05-10T09:35:00Z"
weight = 32700
keywords = [ "retransmissions" ]
aliases = [ "/questions/32700" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to toggle large number of packets](/questions/32700/how-to-toggle-large-number-of-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32700-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32700-score" class="post-score" title="current number of votes">0</div><span id="post-32700-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Is there a way to toggle certain types of packets (without selecting them one by one)? I want to ignore all retransmitted packets</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '14, 19:31</strong></p><img src="https://secure.gravatar.com/avatar/39356e003826b924c6b683f177900afb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="iWireshark&#39;s gravatar image" /><p><span>iWireshark</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="iWireshark has no accepted answers">0%</span></p></div></div><div id="comments-container-32700" class="comments-container"></div><div id="comment-tools-32700" class="comment-tools"></div><div class="clear"></div><div id="comment-32700-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32701"></span>

<div id="answer-container-32701" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32701-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32701-score" class="post-score" title="current number of votes">2</div><span id="post-32701-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="iWireshark has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Create a display filter that only displays your required packets and then select "Mark all Displayed Packets" or "Unmark all displayed Packets" (or the approriate keyboard shortcuts) as required.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '14, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-32701" class="comments-container"><span id="32705"></span><div id="comment-32705" class="comment"><div id="post-32705-score" class="comment-score"></div><div class="comment-text"><p>thanks that works</p></div><div id="comment-32705-info" class="comment-info"><span class="comment-age">(10 May '14, 09:35)</span> <span class="comment-user userinfo">iWireshark</span></div></div></div><div id="comment-tools-32701" class="comment-tools"></div><div class="clear"></div><div id="comment-32701-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

