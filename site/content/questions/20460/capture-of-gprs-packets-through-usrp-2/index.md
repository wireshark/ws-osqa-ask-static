+++
type = "question"
title = "Capture of gprs packets through usrp 2"
description = '''Hi I&#x27;m using USRP kit to deploy a gsm network and then integrate gprs service into it to later capture gprs packtets. For now just the gsm network has been deployed and i began to capture its packets but unfortunately, i can not find any method to capeter my communication. I found out that wireshark...'''
date = "2013-04-16T03:15:00Z"
lastmod = "2013-04-16T04:29:00Z"
weight = 20460
keywords = [ "gprs", "packets", "usrp" ]
aliases = [ "/questions/20460" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture of gprs packets through usrp 2](/questions/20460/capture-of-gprs-packets-through-usrp-2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20460-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20460-score" class="post-score" title="current number of votes">0</div><span id="post-20460-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I'm using USRP kit to deploy a gsm network and then integrate gprs service into it to later capture gprs packtets. For now just the gsm network has been deployed and i began to capture its packets but unfortunately, i can not find any method to capeter my communication. I found out that wireshark is dependent on OS. I'm working on Linux 12.04. Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gprs" rel="tag" title="see questions tagged &#39;gprs&#39;">gprs</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-usrp" rel="tag" title="see questions tagged &#39;usrp&#39;">usrp</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '13, 03:15</strong></p><img src="https://secure.gravatar.com/avatar/f200fefbcc4be45fcadfab3844593b4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="woodysgirl&#39;s gravatar image" /><p><span>woodysgirl</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="woodysgirl has no accepted answers">0%</span></p></div></div><div id="comments-container-20460" class="comments-container"><span id="20465"></span><div id="comment-20465" class="comment"><div id="post-20465-score" class="comment-score"></div><div class="comment-text"><p>What kind of USRP kit do you use (networked, bus series)?</p></div><div id="comment-20465-info" class="comment-info"><span class="comment-age">(16 Apr '13, 04:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20460" class="comment-tools"></div><div class="clear"></div><div id="comment-20460-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

