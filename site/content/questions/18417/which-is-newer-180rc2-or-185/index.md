+++
type = "question"
title = "Which is newer - 1.8.0rc2 or 1.8.5?"
description = '''Current development version (1.8.0rc2) appears to have older version name than current stable release (1.8.5). What am I missing?'''
date = "2013-02-07T14:42:00Z"
lastmod = "2013-02-07T15:37:00Z"
weight = 18417
keywords = [ "versions" ]
aliases = [ "/questions/18417" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Which is newer - 1.8.0rc2 or 1.8.5?](/questions/18417/which-is-newer-180rc2-or-185)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18417-score" class="post-score" title="current number of votes">0</div><span id="post-18417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Current development version (1.8.0rc2) appears to have older version name than current stable release (1.8.5). What am I missing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-versions" rel="tag" title="see questions tagged &#39;versions&#39;">versions</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '13, 14:42</strong></p><img src="https://secure.gravatar.com/avatar/bd68f5d2f6a9d27cfac9591901035a12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sholmes&#39;s gravatar image" /><p><span>sholmes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sholmes has no accepted answers">0%</span></p></div></div><div id="comments-container-18417" class="comments-container"></div><div id="comment-tools-18417" class="comment-tools"></div><div class="clear"></div><div id="comment-18417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="18420"></span>

<div id="answer-container-18420" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18420-score" class="post-score" title="current number of votes">2</div><span id="post-18420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I haven't made any 1.9.x releases yet, so the latest official development version is still the most recent prerelease for 1.8.0. You can download unofficial 1.9.x development versions at <a href="http://www.wireshark.org/download/automated/.">http://www.wireshark.org/download/automated/.</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '13, 15:06</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-18420" class="comments-container"><span id="18422"></span><div id="comment-18422" class="comment"><div id="post-18422-score" class="comment-score">1</div><div class="comment-text"><p>Even though technically speaking release candidates are the latest development releases, I think they should become obsolete when the stable version of the same release has been released...</p><p>(wow, I should release myself from releasing more releases now ;-))</p></div><div id="comment-18422-info" class="comment-info"><span class="comment-age">(07 Feb '13, 15:37)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-18420" class="comment-tools"></div><div class="clear"></div><div id="comment-18420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18419"></span>

<div id="answer-container-18419" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18419-score" class="post-score" title="current number of votes">0</div><span id="post-18419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>1.8.5 is newer. I have no idea why 1.8.0rc is still listed as development version, but if you go to the bleeding edge section on the download page you'll see that the latest development version is 1.9.x. Latest stable is 1.8.5.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '13, 15:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-18419" class="comments-container"></div><div id="comment-tools-18419" class="comment-tools"></div><div class="clear"></div><div id="comment-18419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

