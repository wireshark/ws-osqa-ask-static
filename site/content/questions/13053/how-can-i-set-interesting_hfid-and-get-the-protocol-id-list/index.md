+++
type = "question"
title = "How can I set interesting_hfid and get the protocol ID list."
description = '''Hi everyone:  now I am reading the source code of wireshark 1.81 to develop a communication analysis tool and there are some questions I am using libwireshark.dll to dissect and print the packet and I want to print those fields that I am interested in instead of the whole packet. In my plan, I want ...'''
date = "2012-07-26T23:08:00Z"
lastmod = "2012-07-26T23:08:00Z"
weight = 13053
keywords = [ "interesting_hfid" ]
aliases = [ "/questions/13053" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I set interesting\_hfid and get the protocol ID list.](/questions/13053/how-can-i-set-interesting_hfid-and-get-the-protocol-id-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13053-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13053-score" class="post-score" title="current number of votes">0</div><span id="post-13053-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone:</p><p>now I am reading the source code of wireshark 1.81 to develop a communication analysis tool and there are some questions</p>I am using libwireshark.dll to dissect and print the packet and I want to print those fields that I am interested in instead of the whole packet. In my plan, I want to keep a database linked the hfinfo and the pointer to field-info by its ID, where can I find those information? Such as the hfinfo-&gt;ID of frame is 19157.<p>and where can I enable the option to add all the head-field-info to the interesting_hfid?</p><p>at last, is there any better idea to finish my task?</p><p>thank you all!</p><p>Milanni Liu</p><p><span class="__cf_email__" data-cfemail="b8d5d1d4d9d6d6d1db8ff8">[email protected]</span><a href="http://gmail.com">gmail.com</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interesting_hfid" rel="tag" title="see questions tagged &#39;interesting_hfid&#39;">interesting_hfid</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '12, 23:08</strong></p><img src="https://secure.gravatar.com/avatar/c407550f782fbba088c761bd7593d18a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MilannicLiu&#39;s gravatar image" /><p><span>MilannicLiu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MilannicLiu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jul '12, 07:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-13053" class="comments-container"></div><div id="comment-tools-13053" class="comment-tools"></div><div class="clear"></div><div id="comment-13053-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

