+++
type = "question"
title = "[closed] IEC 61850 MMS simulation"
description = '''I&#x27;ve been studying the MMS protocol as a planning to implement a very simple write mms value function. In order to help my understanding I have tried to emulate to do the following: I am trying to simulate a write command using Hercules (TCP Client) and the SystemCorp MMS Server. I captured the pack...'''
date = "2014-11-02T13:08:00Z"
lastmod = "2014-11-06T02:00:00Z"
weight = 37546
keywords = [ "mms" ]
aliases = [ "/questions/37546" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] IEC 61850 MMS simulation](/questions/37546/iec-61850-mms-simulation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37546-score" class="post-score" title="current number of votes">0</div><span id="post-37546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've been studying the MMS protocol as a planning to implement a very simple write mms value function. In order to help my understanding I have tried to emulate to do the following: I am trying to simulate a write command using Hercules (TCP Client) and the SystemCorp MMS Server. I captured the packet between SystemCorp Client and Server connection using Wireshark, from TPKT up to the MMS, and used the packet in Hercules TCP Client to carry out the same action as I did while using SystemCorp Client, but the SystemCorp doesn't react in the same way. What should I change in this packet to emulate the SC Client using Hercules TCP Client. Thanks</p><p>Following is the packet I've been trying to send over Hercules TCP Client in order to change a item on the System Corp MM Server:</p><pre><code>03 00 00 73 02 f0 80 01 00 01 00 61 66 30 64 02 01 03 a0 5f a0 5d 02 01 09 a5 58 a0 2b 30 29 a0 27 a1 25 1a 0d 4e 65 77 49 45 44 45 78 61 6d 70 6c 65 1a 14 47 47 49 4f  30 24 43 4f 24 53 50 43 53 4f 31 24 4f 70 65 72 a0 29 a2 27 83 01 ff a2 0e 85 01 00 89 09 31 32 37 2e 30 2e 30 2e 31 86 01 01 91 08 54 54 35 17 63 95 81 00 83 01 00 84  02 06 00</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mms" rel="tag" title="see questions tagged &#39;mms&#39;">mms</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '14, 13:08</strong></p><img src="https://secure.gravatar.com/avatar/8945306aa93024ba723296e4fdb238e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pbritto&#39;s gravatar image" /><p><span>pbritto</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pbritto has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>06 Nov '14, 02:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-37546" class="comments-container"><span id="37609"></span><div id="comment-37609" class="comment"><div id="post-37609-score" class="comment-score"></div><div class="comment-text"><p>Not really a Wireshark question, you should ask on an IEC61850 forum or mailing list.</p></div><div id="comment-37609-info" class="comment-info"><span class="comment-age">(06 Nov '14, 02:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-37546" class="comment-tools"></div><div class="clear"></div><div id="comment-37546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 06 Nov '14, 02:00

</div>

</div>

</div>

