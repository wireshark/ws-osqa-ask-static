+++
type = "question"
title = "Cannot open wireshark on mac os X snow leopard"
description = '''Hi  I followed instructions to download and install wireshark for mac os X snow leopard. Howerver, when i click on the wireshark app in applications, it appears to show up for a brief second on the dock and disappears instantly.. I&#x27;ve had the X11 app open during that.. with or without it it behaves ...'''
date = "2011-03-03T09:44:00Z"
lastmod = "2011-03-03T13:05:00Z"
weight = 2650
keywords = [ "osx", "mac", "leopard", "snow" ]
aliases = [ "/questions/2650" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot open wireshark on mac os X snow leopard](/questions/2650/cannot-open-wireshark-on-mac-os-x-snow-leopard)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2650-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2650-score" class="post-score" title="current number of votes">0</div><span id="post-2650-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I followed instructions to download and install wireshark for mac os X snow leopard. Howerver, when i click on the wireshark app in applications, it appears to show up for a brief second on the dock and disappears instantly.. I've had the X11 app open during that.. with or without it it behaves the same. Pls help!!!</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-leopard" rel="tag" title="see questions tagged &#39;leopard&#39;">leopard</span> <span class="post-tag tag-link-snow" rel="tag" title="see questions tagged &#39;snow&#39;">snow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '11, 09:44</strong></p><img src="https://secure.gravatar.com/avatar/02020eb9c0a542447f57634ed9a2fb22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ramyprasad&#39;s gravatar image" /><p><span>ramyprasad</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ramyprasad has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-2650" class="comments-container"></div><div id="comment-tools-2650" class="comment-tools"></div><div class="clear"></div><div id="comment-2650-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2654"></span>

<div id="answer-container-2654" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2654-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2654-score" class="post-score" title="current number of votes">0</div><span id="post-2654-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you install the correct version for your system? Intel v. PPC; 32bit v. 64bit. If so you can try to open the Console application, then choose "All Messages". Leave this window open and try to launch WireShark again. This may reveal some debug info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Mar '11, 13:05</strong></p><img src="https://secure.gravatar.com/avatar/9e493496d59bb4ce33c37cd6e7a26a4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GeonJay&#39;s gravatar image" /><p><span>GeonJay</span><br />
<span class="score" title="470 reputation points">470</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="bronze">●</span><span class="badgecount">22</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GeonJay has 2 accepted answers">5%</span></p></div></div><div id="comments-container-2654" class="comments-container"></div><div id="comment-tools-2654" class="comment-tools"></div><div class="clear"></div><div id="comment-2654-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

