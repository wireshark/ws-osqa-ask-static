+++
type = "question"
title = "32.298 R 9.6.0 support on wireshark"
description = '''Which version of wireshark support 32.298 Rel 9.6.0. I am using 10.1 wirshark. It does not decode CDRs of Release 9.6.0 Please help'''
date = "2013-09-26T20:19:00Z"
lastmod = "2013-09-27T01:10:00Z"
weight = 25296
keywords = [ "asn.1", "32.298", "cdr" ]
aliases = [ "/questions/25296" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [32.298 R 9.6.0 support on wireshark](/questions/25296/32298-r-960-support-on-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25296-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25296-score" class="post-score" title="current number of votes">0</div><span id="post-25296-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Which version of wireshark support 32.298 Rel 9.6.0.</p><p>I am using 10.1 wirshark. It does not decode CDRs of Release 9.6.0</p><p>Please help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asn.1" rel="tag" title="see questions tagged &#39;asn.1&#39;">asn.1</span> <span class="post-tag tag-link-32.298" rel="tag" title="see questions tagged &#39;32.298&#39;">32.298</span> <span class="post-tag tag-link-cdr" rel="tag" title="see questions tagged &#39;cdr&#39;">cdr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '13, 20:19</strong></p><img src="https://secure.gravatar.com/avatar/e10c1dcdf502f3a3cf783397d46f98fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JOJO1015&#39;s gravatar image" /><p><span>JOJO1015</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JOJO1015 has no accepted answers">0%</span></p></div></div><div id="comments-container-25296" class="comments-container"></div><div id="comment-tools-25296" class="comment-tools"></div><div class="clear"></div><div id="comment-25296-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25302"></span>

<div id="answer-container-25302" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25302-score" class="post-score" title="current number of votes">0</div><span id="post-25302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>CDR over GTP, right? I think it <em>should</em> work in trunk. Open a bug report with a small sample file so we can take look and see what might be the problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Sep '13, 01:10</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-25302" class="comments-container"></div><div id="comment-tools-25302" class="comment-tools"></div><div class="clear"></div><div id="comment-25302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

