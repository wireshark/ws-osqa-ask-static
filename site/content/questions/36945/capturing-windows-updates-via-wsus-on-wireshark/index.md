+++
type = "question"
title = "Capturing Windows Updates via WSUS on wireshark."
description = '''Hey, I want to capture all the packets that are related to my windows update downloads on wireshark. I am downloading updates via WSUS client.What filter can be applied so that i get all the update related packets? Please Help!'''
date = "2014-10-09T07:00:00Z"
lastmod = "2014-10-09T07:00:00Z"
weight = 36945
keywords = [ "wsusupdatecapture" ]
aliases = [ "/questions/36945" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing Windows Updates via WSUS on wireshark.](/questions/36945/capturing-windows-updates-via-wsus-on-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36945-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36945-score" class="post-score" title="current number of votes">0</div><span id="post-36945-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey, I want to capture all the packets that are related to my windows update downloads on wireshark. I am downloading updates via WSUS client.What filter can be applied so that i get all the update related packets? Please Help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wsusupdatecapture" rel="tag" title="see questions tagged &#39;wsusupdatecapture&#39;">wsusupdatecapture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '14, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/ef1e59ead9dbbf5e32322118c3896fec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Saurav&#39;s gravatar image" /><p><span>Saurav</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Saurav has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Oct '14, 07:01</strong> </span></p></div></div><div id="comments-container-36945" class="comments-container"></div><div id="comment-tools-36945" class="comment-tools"></div><div class="clear"></div><div id="comment-36945-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

