+++
type = "question"
title = "hey im a super super noob at wireshark"
description = '''alright can someone give me a link or point me to the right direction for noobs using wireshark? i sorta understand how to use it but i want to get the most i can out of this program i have questions like  * can i sniff gsm connections?  * can i look at a open wifi and see others?  * can i use this ...'''
date = "2011-03-21T21:37:00Z"
lastmod = "2011-03-21T22:00:00Z"
weight = 3000
keywords = [ "sniffing", "filter", "wifi", "gsm", "wireshark" ]
aliases = [ "/questions/3000" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [hey im a super super noob at wireshark](/questions/3000/hey-im-a-super-super-noob-at-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3000-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3000-score" class="post-score" title="current number of votes">0</div><span id="post-3000-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>alright can someone give me a link or point me to the right direction for noobs using wireshark?</p><p>i sorta understand how to use it but i want to get the most i can out of this program</p><p>i have questions like * can i sniff gsm connections? * can i look at a open wifi and see others? * can i use this for work?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '11, 21:37</strong></p><img src="https://secure.gravatar.com/avatar/bc1ccf16df64a0305ed8ea2f700aa71e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Josh%20Gryphon&#39;s gravatar image" /><p><span>Josh Gryphon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Josh Gryphon has no accepted answers">0%</span></p></div></div><div id="comments-container-3000" class="comments-container"></div><div id="comment-tools-3000" class="comment-tools"></div><div class="clear"></div><div id="comment-3000-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3002"></span>

<div id="answer-container-3002" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3002-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3002-score" class="post-score" title="current number of votes">1</div><span id="post-3002-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might want to start by reading the <a href="http://www.wireshark.org/docs/wsug_html_chunked">Wireshark User's Guide</a>. It's linked from Wireshark's main <a href="http://www.wireshark.org/docs/">documentation page</a>, which you might also want to take a look at since there are other useful things there, such as videos, etc. There's lots more useful information on the <a href="http://wiki.wireshark.org/">wiki</a> as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '11, 22:00</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3002" class="comments-container"></div><div id="comment-tools-3002" class="comment-tools"></div><div class="clear"></div><div id="comment-3002-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

