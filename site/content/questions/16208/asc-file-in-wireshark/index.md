+++
type = "question"
title = "Asc file in Wireshark"
description = '''Is it possible to &#x27;make&#x27; Wireshark read asc files (CAN trace files) ? What would the sollution consist of (just the idea)? '''
date = "2012-11-22T05:09:00Z"
lastmod = "2012-11-22T07:12:00Z"
weight = 16208
keywords = [ "asc", "ascii", "can", "wireshark" ]
aliases = [ "/questions/16208" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Asc file in Wireshark](/questions/16208/asc-file-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16208-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16208-score" class="post-score" title="current number of votes">0</div><span id="post-16208-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to 'make' Wireshark read asc files (CAN trace files) ? What would the sollution consist of (just the idea)?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asc" rel="tag" title="see questions tagged &#39;asc&#39;">asc</span> <span class="post-tag tag-link-ascii" rel="tag" title="see questions tagged &#39;ascii&#39;">ascii</span> <span class="post-tag tag-link-can" rel="tag" title="see questions tagged &#39;can&#39;">can</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Nov '12, 05:09</strong></p><img src="https://secure.gravatar.com/avatar/31a0ef3c5f2c7aa1802925dccaad3f20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AvL&#39;s gravatar image" /><p><span>AvL</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AvL has no accepted answers">0%</span></p></div></div><div id="comments-container-16208" class="comments-container"></div><div id="comment-tools-16208" class="comment-tools"></div><div class="clear"></div><div id="comment-16208-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16213"></span>

<div id="answer-container-16213" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16213-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16213-score" class="post-score" title="current number of votes">0</div><span id="post-16213-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A wiretap module to read that file type and possibly a WTAP_ENCAP to match. WTAP_ENCAP_SOCKETCAN exists.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Nov '12, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-16213" class="comments-container"></div><div id="comment-tools-16213" class="comment-tools"></div><div class="clear"></div><div id="comment-16213-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

