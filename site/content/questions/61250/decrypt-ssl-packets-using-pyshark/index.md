+++
type = "question"
title = "Decrypt SSL Packets using PyShark"
description = '''I am able to decrypt SSL packets using wireshark as I have master key, but i am not able to do the same using PyShark. How to decrypt SSL Packets using PyShark?'''
date = "2017-05-05T03:50:00Z"
lastmod = "2017-05-05T03:54:00Z"
weight = 61250
keywords = [ "ssl", "decryption", "pyshark" ]
aliases = [ "/questions/61250" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt SSL Packets using PyShark](/questions/61250/decrypt-ssl-packets-using-pyshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61250-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61250-score" class="post-score" title="current number of votes">0</div><span id="post-61250-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am able to decrypt SSL packets using wireshark as I have master key, but i am not able to do the same using PyShark. How to decrypt SSL Packets using PyShark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-pyshark" rel="tag" title="see questions tagged &#39;pyshark&#39;">pyshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '17, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/9afb2ce6f4554190770f5d36852a2e81?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ameya_k&#39;s gravatar image" /><p><span>ameya_k</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ameya_k has no accepted answers">0%</span></p></div></div><div id="comments-container-61250" class="comments-container"><span id="61251"></span><div id="comment-61251" class="comment"><div id="post-61251-score" class="comment-score"></div><div class="comment-text"><p>I think this is probably the wrong place to ask - it's probably better to open an issue at <a href="https://github.com/KimiNewt/pyshark/issues">https://github.com/KimiNewt/pyshark/issues</a></p></div><div id="comment-61251-info" class="comment-info"><span class="comment-age">(05 May '17, 03:54)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-61250" class="comment-tools"></div><div class="clear"></div><div id="comment-61250-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

