+++
type = "question"
title = "&quot;Failed to connect to Mir: Failed to connect to server socket: No such file or directory&quot; when trying to run Wireshark"
description = '''i&#x27;m trying to run wireshark from another computer and i get this error  Failed to connect to Mir: Failed to connect to server socket: No such file or directory what does that mean and how do i solve it ?'''
date = "2015-06-16T06:58:00Z"
lastmod = "2016-08-04T07:49:00Z"
weight = 43205
keywords = [ "wireshark", "mir", "error" ]
aliases = [ "/questions/43205" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# ["Failed to connect to Mir: Failed to connect to server socket: No such file or directory" when trying to run Wireshark](/questions/43205/failed-to-connect-to-mir-failed-to-connect-to-server-socket-no-such-file-or-directory-when-trying-to-run-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43205-score" class="post-score" title="current number of votes">0</div><span id="post-43205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i'm trying to run wireshark from another computer and i get this error Failed to connect to Mir: Failed to connect to server socket: No such file or directory</p><p>what does that mean and how do i solve it ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-mir" rel="tag" title="see questions tagged &#39;mir&#39;">mir</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jun '15, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/890399e77f2c0c0ff2f75ea2f43d3ff8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yas1234&#39;s gravatar image" /><p><span>yas1234</span><br />
<span class="score" title="16 reputation points">16</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="23 badges"><span class="bronze">●</span><span class="badgecount">23</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yas1234 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Jun '15, 12:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-43205" class="comments-container"><span id="43211"></span><div id="comment-43211" class="comment"><div id="post-43211-score" class="comment-score"></div><div class="comment-text"><p>can you please add a screenshot of Wireshark while it is showing that error.</p></div><div id="comment-43211-info" class="comment-info"><span class="comment-age">(16 Jun '15, 09:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="43212"></span><div id="comment-43212" class="comment"><div id="post-43212-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Failed to connect to Mir</p></blockquote><p>Is this on Ubuntu Linux?</p></div><div id="comment-43212-info" class="comment-info"><span class="comment-age">(16 Jun '15, 10:45)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-43205" class="comment-tools"></div><div class="clear"></div><div id="comment-43205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="43210"></span>

<div id="answer-container-43210" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43210-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43210-score" class="post-score" title="current number of votes">0</div><span id="post-43210-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe you need to install Wireshark on the machine that you are remoting from? Dunno.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jun '15, 09:06</strong></p><img src="https://secure.gravatar.com/avatar/1874987e18cf7e801e283b02809de418?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Irishbug&#39;s gravatar image" /><p><span>Irishbug</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Irishbug has no accepted answers">0%</span></p></div></div><div id="comments-container-43210" class="comments-container"><span id="43213"></span><div id="comment-43213" class="comment"><div id="post-43213-score" class="comment-score"></div><div class="comment-text"><p>no i have it</p></div><div id="comment-43213-info" class="comment-info"><span class="comment-age">(16 Jun '15, 12:02)</span> <span class="comment-user userinfo">yas1234</span></div></div></div><div id="comment-tools-43210" class="comment-tools"></div><div class="clear"></div><div id="comment-43210-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="43214"></span>

<div id="answer-container-43214" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43214-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43214-score" class="post-score" title="current number of votes">0</div><span id="post-43214-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>it"s linux but i solved the problem thanks anyway , it"s bec of wireshark graphical display on the remote computer i had to add a command for it to work !</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jun '15, 12:03</strong></p><img src="https://secure.gravatar.com/avatar/890399e77f2c0c0ff2f75ea2f43d3ff8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yas1234&#39;s gravatar image" /><p><span>yas1234</span><br />
<span class="score" title="16 reputation points">16</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="23 badges"><span class="bronze">●</span><span class="badgecount">23</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yas1234 has no accepted answers">0%</span></p></div></div><div id="comments-container-43214" class="comments-container"><span id="43215"></span><div id="comment-43215" class="comment"><div id="post-43215-score" class="comment-score"></div><div class="comment-text"><p>I asked not just about Linux, but specifically about Ubuntu Linux, because "Mir" is the name of <a href="https://en.wikipedia.org/wiki/Mir_(software)">a display server for Linux, being developed for Ubuntu</a>, as a replacement for X11.</p><p>The error probably means you're running it on an Ubuntu machine, but not from a GUI session on that machine, and it's therefore failing to connect to the Mir server on that machine. Presumably the command you added was one to try to get it to connect via X11 to the machine on which you had the GUI session.</p></div><div id="comment-43215-info" class="comment-info"><span class="comment-age">(16 Jun '15, 12:10)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="43217"></span><div id="comment-43217" class="comment"><div id="post-43217-score" class="comment-score"></div><div class="comment-text"><p>i just added -X to my ssh labpc1 /directory/wiresharkScript.sh and it worked ..is that correct or it's not the best solution ?</p></div><div id="comment-43217-info" class="comment-info"><span class="comment-age">(16 Jun '15, 12:21)</span> <span class="comment-user userinfo">yas1234</span></div></div><span id="43219"></span><div id="comment-43219" class="comment"><div id="post-43219-score" class="comment-score"></div><div class="comment-text"><p>I.e, you did <code>ssh -X labpc1  /directory/wiresharkScript.sh</code>?</p><p>If so, my guess is that the <code>-X</code> flag, by enabling X11 forwarding, causes the DISPLAY environment variable to be set, so that it refers to the machine from which you're doing the ssh, when wiresharkScript.sh is run, causing Wireshark to be run with the DISPLAY environment set.</p><p>This probably means that the Mir/X11 client libraries that Wireshark uses - or, rather, that GTK+, the GUI toolkit Wireshark is using, uses - will, instead of trying to contact the local Mir server (which fails in the fashion you reported), try to contact the X11 server on the machine from which you did the ssh, so that it opens windows on that machine.</p></div><div id="comment-43219-info" class="comment-info"><span class="comment-age">(16 Jun '15, 12:42)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="54583"></span><div id="comment-54583" class="comment"><div id="post-54583-score" class="comment-score"></div><div class="comment-text"><p>I am facing similar same exception on Ubuntu 15.04. My Ubuntu machine is on EC2 Instance(Amazon web services) do not have display/GUI.<br />
</p><p>Will above solution work in my case also?</p></div><div id="comment-54583-info" class="comment-info"><span class="comment-age">(04 Aug '16, 07:49)</span> <span class="comment-user userinfo">Prashant</span></div></div></div><div id="comment-tools-43214" class="comment-tools"></div><div class="clear"></div><div id="comment-43214-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

