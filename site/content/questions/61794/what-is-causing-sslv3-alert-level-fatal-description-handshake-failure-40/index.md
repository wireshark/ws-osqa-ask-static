+++
type = "question"
title = "What is causing SSLv3 Alert (Level:  Fatal, Description:  Handshake Failure (40))?"
description = '''Looking for help in determining why a client is intermittently receiving the &quot;SSLv3 Alert (Level: Fatal, Description: Handshake Failure (40))&quot; error. We are sending over TLS 1.0 and most transactions are accepted but some in the same batch are not accepted by the server.'''
date = "2017-06-05T19:14:00Z"
lastmod = "2017-06-05T19:14:00Z"
weight = 61794
keywords = [ "handshake-failure" ]
aliases = [ "/questions/61794" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What is causing SSLv3 Alert (Level: Fatal, Description: Handshake Failure (40))?](/questions/61794/what-is-causing-sslv3-alert-level-fatal-description-handshake-failure-40)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61794-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61794-score" class="post-score" title="current number of votes">0</div><span id="post-61794-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Looking for help in determining why a client is intermittently receiving the "SSLv3 Alert (Level: Fatal, Description: Handshake Failure (40))" error. We are sending over TLS 1.0 and most transactions are accepted but some in the same batch are not accepted by the server.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-handshake-failure" rel="tag" title="see questions tagged &#39;handshake-failure&#39;">handshake-failure</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '17, 19:14</strong></p><img src="https://secure.gravatar.com/avatar/bea32f5dfec4a92ed924427eb1d4ac7a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kleibbrandt&#39;s gravatar image" /><p><span>kleibbrandt</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kleibbrandt has no accepted answers">0%</span></p></div></div><div id="comments-container-61794" class="comments-container"></div><div id="comment-tools-61794" class="comment-tools"></div><div class="clear"></div><div id="comment-61794-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

