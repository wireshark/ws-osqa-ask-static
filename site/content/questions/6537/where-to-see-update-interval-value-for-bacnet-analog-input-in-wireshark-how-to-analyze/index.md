+++
type = "question"
title = "Where to see &quot;Update Interval&quot; value for BACnet Analog Input in Wireshark? How to analyze?"
description = '''I need to check whether the BACnet controller supports &quot;Update Interval&quot; for Analog input object. Please anyone help me out how to check this?'''
date = "2011-09-24T05:32:00Z"
lastmod = "2011-09-24T05:32:00Z"
weight = 6537
keywords = [ "bacnet" ]
aliases = [ "/questions/6537" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Where to see "Update Interval" value for BACnet Analog Input in Wireshark? How to analyze?](/questions/6537/where-to-see-update-interval-value-for-bacnet-analog-input-in-wireshark-how-to-analyze)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6537-score" class="post-score" title="current number of votes">0</div><span id="post-6537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to check whether the BACnet controller supports "Update Interval" for Analog input object. Please anyone help me out how to check this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bacnet" rel="tag" title="see questions tagged &#39;bacnet&#39;">bacnet</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '11, 05:32</strong></p><img src="https://secure.gravatar.com/avatar/cc1c2ad2470fe701738f5fd1129f7c79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ravi%20S&#39;s gravatar image" /><p><span>Ravi S</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ravi S has no accepted answers">0%</span></p></div></div><div id="comments-container-6537" class="comments-container"></div><div id="comment-tools-6537" class="comment-tools"></div><div class="clear"></div><div id="comment-6537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

