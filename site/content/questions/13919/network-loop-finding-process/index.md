+++
type = "question"
title = "Network Loop Finding Process"
description = '''Hi, In My Company Setup I am facing some Firewall related issues;when this firewall connected in Network switch My plant to plant communication Goes down;This problem occurs in the evening only... !  Network Setup is like below; We Have 3 Plants and those all are connected Through Wireless as Point ...'''
date = "2012-08-28T00:25:00Z"
lastmod = "2014-01-27T02:35:00Z"
weight = 13919
keywords = [ "firewall", "connection", "network", "loop" ]
aliases = [ "/questions/13919" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Network Loop Finding Process](/questions/13919/network-loop-finding-process)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13919-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13919-score" class="post-score" title="current number of votes">0</div><span id="post-13919-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>In My Company Setup I am facing some Firewall related issues;when this firewall connected in Network switch My plant to plant communication Goes down;This problem occurs in the evening only... !<br />
</p><p>Network Setup is like below; We Have 3 Plants and those all are connected Through Wireless as Point to Point; Every Plant has 2 Devices. <strong>Plant A, Plant B, Plant C</strong> Plant A <strong>Wireless Device1</strong> has a <strong>same SSID</strong> like <strong>Plant B Device1</strong>(its connected Plant A &gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt; Plant B) Plant B <strong>Device2</strong> is Connected to Plant C <strong>Device1</strong> (with same SSID) For Redundancy we configured Extra Devices Of <strong>Plant A</strong> and <strong>Plant C</strong> its is in the Switch But SSID are different than Exist. Firewall Nsa2400 is Installed at both Plants at <strong>Plant A</strong> and <strong>Plant B</strong> only.</p><p>Through I am getting few CDP check sum errors.. is this a cause of the problem... or how to use this tool to find a loop</p><p>Regards, Swapz123</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span> <span class="post-tag tag-link-connection" rel="tag" title="see questions tagged &#39;connection&#39;">connection</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-loop" rel="tag" title="see questions tagged &#39;loop&#39;">loop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '12, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/a5d6b9c9bdc276f45f31adca92fe0cc4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="swapz123&#39;s gravatar image" /><p><span>swapz123</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="swapz123 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-13919" class="comments-container"></div><div id="comment-tools-13919" class="comment-tools"></div><div class="clear"></div><div id="comment-13919-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29176"></span>

<div id="answer-container-29176" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29176-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29176-score" class="post-score" title="current number of votes">0</div><span id="post-29176-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Check your arps, pings and traceroute and wireshark the "seconds before disaster"...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '14, 02:35</strong></p><img src="https://secure.gravatar.com/avatar/1741e64a2bff0458d7f0f97f538d0e8b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gamermic&#39;s gravatar image" /><p><span>gamermic</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gamermic has no accepted answers">0%</span></p></div></div><div id="comments-container-29176" class="comments-container"></div><div id="comment-tools-29176" class="comment-tools"></div><div class="clear"></div><div id="comment-29176-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

