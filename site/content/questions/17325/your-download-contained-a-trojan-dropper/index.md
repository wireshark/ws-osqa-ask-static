+++
type = "question"
title = "Your Download contained a Trojan Dropper!"
description = '''Why does this download come with a trojan dropper that AVG found?'''
date = "2012-12-31T01:02:00Z"
lastmod = "2012-12-31T07:37:00Z"
weight = 17325
keywords = [ "trojan", "infected", "virus" ]
aliases = [ "/questions/17325" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Your Download contained a Trojan Dropper!](/questions/17325/your-download-contained-a-trojan-dropper)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17325-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17325-score" class="post-score" title="current number of votes">0</div><span id="post-17325-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why does this download come with a trojan dropper that AVG found?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trojan" rel="tag" title="see questions tagged &#39;trojan&#39;">trojan</span> <span class="post-tag tag-link-infected" rel="tag" title="see questions tagged &#39;infected&#39;">infected</span> <span class="post-tag tag-link-virus" rel="tag" title="see questions tagged &#39;virus&#39;">virus</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '12, 01:02</strong></p><img src="https://secure.gravatar.com/avatar/8f8cf3098c115f4fceeb534973681ab8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NetworkInformer&#39;s gravatar image" /><p><span>NetworkInformer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NetworkInformer has no accepted answers">0%</span></p></div></div><div id="comments-container-17325" class="comments-container"></div><div id="comment-tools-17325" class="comment-tools"></div><div class="clear"></div><div id="comment-17325-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="17326"></span>

<div id="answer-container-17326" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17326-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17326-score" class="post-score" title="current number of votes">2</div><span id="post-17326-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>as you did not mention <strong>what</strong> you downloaded and <strong>where</strong>, it is hard to say why your AV product believes to have found something. Can you please add more details?</p><p>BTW: The current release 1.8.4 is not detected as malware by anyone of the 45 AV scanners of <a href="http://virustotal.com">virustotal.com</a>.</p><blockquote><p><code>https://www.virustotal.com/file/46673635bbd8a5e579f90fbb850fb5c7b2a8be7f5f689a77b8e60cd8f54825b6/analysis/1356945013/</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Dec '12, 01:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Dec '12, 01:13</strong> </span></p></div></div><div id="comments-container-17326" class="comments-container"></div><div id="comment-tools-17326" class="comment-tools"></div><div class="clear"></div><div id="comment-17326-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="17350"></span>

<div id="answer-container-17350" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17350-score" class="post-score" title="current number of votes">0</div><span id="post-17350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you provide a few more details? What version of AVG's scanning engine and virus database are you using? Did AVG provide a more specific description of what it found? "Trojan Dropper" isn't very specific.</p><p>Which Wireshark package did you download? Where did you get it? Can you provide a SHA1, RIPEMD160, or MD5 hash of the file?</p><p>Note that we've had a number of <a href="http://wiki.wireshark.org/FalsePositives">false positives</a> in the past. It would be helpful if you could provide any more information so that we can verify if this is a false positive as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Dec '12, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-17350" class="comments-container"></div><div id="comment-tools-17350" class="comment-tools"></div><div class="clear"></div><div id="comment-17350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

