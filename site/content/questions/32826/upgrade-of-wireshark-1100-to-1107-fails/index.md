+++
type = "question"
title = "Upgrade of Wireshark 1.10.0  to 1.10.7 fails"
description = '''I&#x27;ve a problem to upgrade Wireshark from v1.10.0 to v1.10.7 on a Windows 2008 server. When I run I the installer it complains there are still Wireshark associated programs open. I&#x27;ve checked the op en windows processes but could not find any process related to Wireshark. What else could be the probl...'''
date = "2014-05-15T06:18:00Z"
lastmod = "2014-05-15T07:39:00Z"
weight = 32826
keywords = [ "upgrade", "uninstall", "1.10.0", "wireshark" ]
aliases = [ "/questions/32826" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Upgrade of Wireshark 1.10.0 to 1.10.7 fails](/questions/32826/upgrade-of-wireshark-1100-to-1107-fails)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32826-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32826-score" class="post-score" title="current number of votes">0</div><span id="post-32826-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've a problem to upgrade Wireshark from v1.10.0 to v1.10.7 on a Windows 2008 server. When I run I the installer it complains there are still Wireshark associated programs open. I've checked the op en windows processes but could not find any process related to Wireshark. What else could be the problem? Rebooting the server is a possible alternative but only possible outside business hours...</p><p>rgds, Geert</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-upgrade" rel="tag" title="see questions tagged &#39;upgrade&#39;">upgrade</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span> <span class="post-tag tag-link-1.10.0" rel="tag" title="see questions tagged &#39;1.10.0&#39;">1.10.0</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '14, 06:18</strong></p><img src="https://secure.gravatar.com/avatar/2c6e52dcc3854f1ab3947f2651fa3afb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beesman59&#39;s gravatar image" /><p><span>Beesman59</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beesman59 has no accepted answers">0%</span></p></div></div><div id="comments-container-32826" class="comments-container"><span id="32828"></span><div id="comment-32828" class="comment"><div id="post-32828-score" class="comment-score"></div><div class="comment-text"><p>There have been observations of a dumpcap.exe process running stopping updates.</p></div><div id="comment-32828-info" class="comment-info"><span class="comment-age">(15 May '14, 07:09)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="32829"></span><div id="comment-32829" class="comment"><div id="post-32829-score" class="comment-score"></div><div class="comment-text"><p>Indeed I've killed 3 dumpcap.exe processes and retried the install. Now it starts but I got several errors where I answered 'ignore'. But 1.10.7 runs now after install. I hope it will keep running now...</p></div><div id="comment-32829-info" class="comment-info"><span class="comment-age">(15 May '14, 07:39)</span> <span class="comment-user userinfo">Beesman59</span></div></div></div><div id="comment-tools-32826" class="comment-tools"></div><div class="clear"></div><div id="comment-32826-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

