+++
type = "question"
title = "IP address of packet is from another country"
description = '''Started capturing packets from my home. Found a weird IP address. Traced it back to San Francisco (I live in Canada). How is this possible?'''
date = "2014-06-03T18:16:00Z"
lastmod = "2014-06-04T17:32:00Z"
weight = 33363
keywords = [ "ipaddress" ]
aliases = [ "/questions/33363" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [IP address of packet is from another country](/questions/33363/ip-address-of-packet-is-from-another-country)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33363-score" class="post-score" title="current number of votes">0</div><span id="post-33363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Started capturing packets from my home. Found a weird IP address. Traced it back to San Francisco (I live in Canada). How is this possible?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipaddress" rel="tag" title="see questions tagged &#39;ipaddress&#39;">ipaddress</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '14, 18:16</strong></p><img src="https://secure.gravatar.com/avatar/2e30b003aeb120dfa4f1f4d01d69b88d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Umpire&#39;s gravatar image" /><p><span>Umpire</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Umpire has no accepted answers">0%</span></p></div></div><div id="comments-container-33363" class="comments-container"><span id="33364"></span><div id="comment-33364" class="comment"><div id="post-33364-score" class="comment-score">1</div><div class="comment-text"><p>Why would packets with IP addresses from another country be any stranger than, for example, packets with IP addresses from another city in your own country? IP packets don't have to stop at a customs post and be frisked before crossing international borders.</p></div><div id="comment-33364-info" class="comment-info"><span class="comment-age">(03 Jun '14, 18:23)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="33366"></span><div id="comment-33366" class="comment"><div id="post-33366-score" class="comment-score"></div><div class="comment-text"><blockquote><p>P packets don't have to stop at a customs post and be frisked before crossing international borders.</p></blockquote><p>I'm pretty sure that some ISPs are already working on virtual customs posts to cash in on some kind of traffic, like YouTube, youporn, etc.</p></div><div id="comment-33366-info" class="comment-info"><span class="comment-age">(03 Jun '14, 22:39)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33363" class="comment-tools"></div><div class="clear"></div><div id="comment-33363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33367"></span>

<div id="answer-container-33367" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33367-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33367-score" class="post-score" title="current number of votes">1</div><span id="post-33367-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Umpire has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Traced it back to San Francisco</p></blockquote><p>Just a wild guess: a google or facebook server?</p><p>Post the IP address and we can tell you.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '14, 22:42</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-33367" class="comments-container"><span id="33374"></span><div id="comment-33374" class="comment"><div id="post-33374-score" class="comment-score"></div><div class="comment-text"><p>162.159.241.165</p></div><div id="comment-33374-info" class="comment-info"><span class="comment-age">(04 Jun '14, 04:49)</span> <span class="comment-user userinfo">Umpire</span></div></div><span id="33375"></span><div id="comment-33375" class="comment"><div id="post-33375-score" class="comment-score"></div><div class="comment-text"><p>CloudFlare, Inc.</p></div><div id="comment-33375-info" class="comment-info"><span class="comment-age">(04 Jun '14, 06:23)</span> <span class="comment-user userinfo">Rooster_50</span></div></div><span id="33413"></span><div id="comment-33413" class="comment"><div id="post-33413-score" class="comment-score"></div><div class="comment-text"><blockquote><p>CloudFlare, Inc.</p></blockquote><p>...<a href="https://www.cloudflare.com/overview">who are a company offering a service as a "front end" to Web sites, providing speed and security improvements</a>.</p><p>Some Wireshark sites use it.</p></div><div id="comment-33413-info" class="comment-info"><span class="comment-age">(04 Jun '14, 17:32)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-33367" class="comment-tools"></div><div class="clear"></div><div id="comment-33367-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

