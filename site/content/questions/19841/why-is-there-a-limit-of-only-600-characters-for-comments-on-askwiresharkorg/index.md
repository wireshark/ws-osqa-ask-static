+++
type = "question"
title = "Why is there a limit of only 600 characters for comments on ask.wireshark.org?"
description = '''I was wondering why there is a limit of only 600 characters per comment. Answers are not similarly limited. This question comes up because I recently converted Joke&#x27;s answer to this question to a comment since it wasn&#x27;t actually an answer to the question being asked. But she would not have been able...'''
date = "2013-03-26T08:17:00Z"
lastmod = "2013-03-28T00:27:00Z"
weight = 19841
keywords = [ "comment", "limit", "osqa" ]
aliases = [ "/questions/19841" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Why is there a limit of only 600 characters for comments on ask.wireshark.org?](/questions/19841/why-is-there-a-limit-of-only-600-characters-for-comments-on-askwiresharkorg)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19841-score" class="post-score" title="current number of votes">0</div><span id="post-19841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was wondering why there is a limit of only 600 characters per comment. Answers are not similarly limited.</p><p>This question comes up because I recently converted <a href="http://ask.wireshark.org/users/100/joke">Joke</a>'s answer to <a href="http://ask.wireshark.org/questions/18647/wpa-psk-decryption-is-not-working-with-tshark-wireshark-181">this</a> question to a comment since it wasn't actually an answer to the question being asked. But she would not have been able to add that comment herself since it exceeds the current maximum character limitation for comments.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-comment" rel="tag" title="see questions tagged &#39;comment&#39;">comment</span> <span class="post-tag tag-link-limit" rel="tag" title="see questions tagged &#39;limit&#39;">limit</span> <span class="post-tag tag-link-osqa" rel="tag" title="see questions tagged &#39;osqa&#39;">osqa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '13, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-19841" class="comments-container"><span id="19842"></span><div id="comment-19842" class="comment"><div id="post-19842-score" class="comment-score">4</div><div class="comment-text"><p>640 characters should have been enough for everyone!</p></div><div id="comment-19842-info" class="comment-info"><span class="comment-age">(26 Mar '13, 08:20)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="19843"></span><div id="comment-19843" class="comment"><div id="post-19843-score" class="comment-score">1</div><div class="comment-text"><p>Madness? This is 2*Sparta!!!</p></div><div id="comment-19843-info" class="comment-info"><span class="comment-age">(26 Mar '13, 08:49)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="19862"></span><div id="comment-19862" class="comment"><div id="post-19862-score" class="comment-score"></div><div class="comment-text"><p>Hi Chris<br />
The limit of 600 isn't a problem for most <a href="http://www.merriam-webster.com/dictionary/comment">comments</a>.<br />
In case of an exception you or the Super-Admins can help:)<br />
Thank you for converting the answer to a comment<br />
</p></div><div id="comment-19862-info" class="comment-info"><span class="comment-age">(26 Mar '13, 23:32)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-19841" class="comment-tools"></div><div class="clear"></div><div id="comment-19841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19851"></span>

<div id="answer-container-19851" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19851-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19851-score" class="post-score" title="current number of votes">0</div><span id="post-19851-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think is is good to raise that limit. The question is what would a sensible limit be? Or should it be basically limitless (640KB should be enough for everyone ;-))</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '13, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span> </br></br></p></div></div><div id="comments-container-19851" class="comments-container"><span id="19863"></span><div id="comment-19863" class="comment"><div id="post-19863-score" class="comment-score"></div><div class="comment-text"><p>Agree. I often run into the comment size limitation (maybe I'm just too chatty? :-)).</p><p>I believe that probably 2000 chars should be okay if we still want to keep a limit, at least I think that amount would have been enough for everything in the past where I had to do the answer-comment-conversion trick.</p></div><div id="comment-19863-info" class="comment-info"><span class="comment-age">(27 Mar '13, 02:10)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="19869"></span><div id="comment-19869" class="comment"><div id="post-19869-score" class="comment-score">1</div><div class="comment-text"><p>I vote for no limit, or the same limit as for answers.</p></div><div id="comment-19869-info" class="comment-info"><span class="comment-age">(27 Mar '13, 07:19)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="19871"></span><div id="comment-19871" class="comment"><div id="post-19871-score" class="comment-score"></div><div class="comment-text"><p>I concur with Kurt. Same limit as for answers.</p></div><div id="comment-19871-info" class="comment-info"><span class="comment-age">(27 Mar '13, 08:49)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div><span id="19894"></span><div id="comment-19894" class="comment"><div id="post-19894-score" class="comment-score">3</div><div class="comment-text"><p>I set the limit to 2500. AFAICT there is no limit on an answer (there does not seem to be a setting for it anyways). As Joke said, if there is need for a really long comment, then we can always convert an answer to a comment...</p><p>Lets see how the new limit works out!</p></div><div id="comment-19894-info" class="comment-info"><span class="comment-age">(28 Mar '13, 00:27)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-19851" class="comment-tools"></div><div class="clear"></div><div id="comment-19851-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

