+++
type = "question"
title = "how to decrypt on wireshark-MAC ersion 2.0.2 (v2.0.2-0-ga16e22e from master-2.0)"
description = '''there is no edit&amp;gt;preferences on this version.. i need to decrypt ssl to http'''
date = "2016-03-14T14:51:00Z"
lastmod = "2016-03-14T14:56:00Z"
weight = 50905
keywords = [ "wireshark-2.0", "man" ]
aliases = [ "/questions/50905" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to decrypt on wireshark-MAC ersion 2.0.2 (v2.0.2-0-ga16e22e from master-2.0)](/questions/50905/how-to-decrypt-on-wireshark-mac-ersion-202-v202-0-ga16e22e-from-master-20)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50905-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50905-score" class="post-score" title="current number of votes">0</div><span id="post-50905-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>there is no edit&gt;preferences on this version.. i need to decrypt ssl to http</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span> <span class="post-tag tag-link-man" rel="tag" title="see questions tagged &#39;man&#39;">man</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '16, 14:51</strong></p><img src="https://secure.gravatar.com/avatar/81c8f2a8c705270ba00d309e35348609?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pehlanaam%20Akhrinaam&#39;s gravatar image" /><p><span>Pehlanaam Ak...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pehlanaam Akhrinaam has no accepted answers">0%</span></p></div></div><div id="comments-container-50905" class="comments-container"></div><div id="comment-tools-50905" class="comment-tools"></div><div class="clear"></div><div id="comment-50905-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50906"></span>

<div id="answer-container-50906" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50906-score" class="post-score" title="current number of votes">0</div><span id="post-50906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>its not under edit..but under Wireshark Menu&gt;Preferences&gt;Protocol&gt;SSL.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Mar '16, 14:56</strong></p><img src="https://secure.gravatar.com/avatar/81c8f2a8c705270ba00d309e35348609?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pehlanaam%20Akhrinaam&#39;s gravatar image" /><p><span>Pehlanaam Ak...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pehlanaam Akhrinaam has no accepted answers">0%</span></p></div></div><div id="comments-container-50906" class="comments-container"></div><div id="comment-tools-50906" class="comment-tools"></div><div class="clear"></div><div id="comment-50906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

