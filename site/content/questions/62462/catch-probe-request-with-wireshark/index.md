+++
type = "question"
title = "Catch Probe Request with Wireshark"
description = '''Hello I want to catch probe requests from nearby mobile devices. I search examples. But I have some questions. 1) Can I use my own computer&#x27;s wi-fi device (intel dual band wireless-ac 3165) for catching probe requests? I try this with wireshark but it is show me only packages between network and my ...'''
date = "2017-07-01T16:29:00Z"
lastmod = "2017-07-01T21:28:00Z"
weight = 62462
keywords = [ "sniffing", "probe" ]
aliases = [ "/questions/62462" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Catch Probe Request with Wireshark](/questions/62462/catch-probe-request-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62462-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62462-score" class="post-score" title="current number of votes">0</div><span id="post-62462-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I want to catch probe requests from nearby mobile devices. I search examples. But I have some questions.</p><p>1) Can I use my own computer's wi-fi device (intel dual band wireless-ac 3165) for catching probe requests? I try this with wireshark but it is show me only packages between network and my computer.</p><p>2) I have <a href="http://edupwireless.com/product-1-2-5-mini-wireless-usb-adapter-en/137318">this</a> usb adapter. If I use this device with wireshark can I catch probe requests without any network?</p><p>Everytime I try use wireshark for catching packages it shows me only between network and my computer. I use Ubuntu 16.04 and Wireshark 2.2.3.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-probe" rel="tag" title="see questions tagged &#39;probe&#39;">probe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jul '17, 16:29</strong></p><img src="https://secure.gravatar.com/avatar/fb8bbf4612708afcb2a44bf94072a7d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bk52&#39;s gravatar image" /><p><span>bk52</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bk52 has no accepted answers">0%</span></p></div></div><div id="comments-container-62462" class="comments-container"></div><div id="comment-tools-62462" class="comment-tools"></div><div class="clear"></div><div id="comment-62462-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62464"></span>

<div id="answer-container-62464" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62464-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62464-score" class="post-score" title="current number of votes">0</div><span id="post-62464-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Most likely the WiFi adapter is not set to monitor mode. Please read the following wiki, particularly the difference between promiscuous and monitor mode:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jul '17, 21:28</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-62464" class="comments-container"></div><div id="comment-tools-62464" class="comment-tools"></div><div class="clear"></div><div id="comment-62464-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

