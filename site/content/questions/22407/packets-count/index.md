+++
type = "question"
title = "Packets Count"
description = '''Is it possible to calculate the rate at which the packets are flowing in the network, for example I need to count the number of packets in 5 minutes or 10 minutes.. Which command to be used'''
date = "2013-06-27T07:54:00Z"
lastmod = "2013-06-27T08:15:00Z"
weight = 22407
keywords = [ "capture" ]
aliases = [ "/questions/22407" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packets Count](/questions/22407/packets-count)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22407-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22407-score" class="post-score" title="current number of votes">0</div><span id="post-22407-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to calculate the rate at which the packets are flowing in the network, for example I need to count the number of packets in 5 minutes or 10 minutes.. Which command to be used</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '13, 07:54</strong></p><img src="https://secure.gravatar.com/avatar/72bb467fdfcb864b343d591c407020f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rahuulbp&#39;s gravatar image" /><p><span>rahuulbp</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rahuulbp has no accepted answers">0%</span></p></div></div><div id="comments-container-22407" class="comments-container"></div><div id="comment-tools-22407" class="comment-tools"></div><div class="clear"></div><div id="comment-22407-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22411"></span>

<div id="answer-container-22411" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22411-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22411-score" class="post-score" title="current number of votes">1</div><span id="post-22411-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In Wireshark use Statistics | Summary, for tshark look at the -z options. Note that the GUI summary does not get updated in real-time it only shows the values that existed when the dialog was displayed. For tshark the capture must be completed to display the stats.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '13, 08:15</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-22411" class="comments-container"></div><div id="comment-tools-22411" class="comment-tools"></div><div class="clear"></div><div id="comment-22411-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

