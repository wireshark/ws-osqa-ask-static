+++
type = "question"
title = "rlc reassemble process will take a long time."
description = '''I have find that the rlc reassemble process will take a long time. if you have a *.pcap over 50M,and it most frame is rlc,and then ,it will take you too long time to decode the file. And by debug ,I sure that the most of time is use to reassemble. is here anybody can fix it? Thanks!'''
date = "2013-02-25T02:14:00Z"
lastmod = "2013-02-25T05:48:00Z"
weight = 18843
keywords = [ "slowly", "reassemble", "rlc", "too" ]
aliases = [ "/questions/18843" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [rlc reassemble process will take a long time.](/questions/18843/rlc-reassemble-process-will-take-a-long-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18843-score" class="post-score" title="current number of votes">0</div><span id="post-18843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have find that the rlc reassemble process will take a long time.</p><p>if you have a *.pcap over 50M,and it most frame is rlc,and then ,it will take you too long time to decode the file.</p><p>And by debug ,I sure that the most of time is use to reassemble.</p><p>is here anybody can fix it?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-slowly" rel="tag" title="see questions tagged &#39;slowly&#39;">slowly</span> <span class="post-tag tag-link-reassemble" rel="tag" title="see questions tagged &#39;reassemble&#39;">reassemble</span> <span class="post-tag tag-link-rlc" rel="tag" title="see questions tagged &#39;rlc&#39;">rlc</span> <span class="post-tag tag-link-too" rel="tag" title="see questions tagged &#39;too&#39;">too</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '13, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-18843" class="comments-container"></div><div id="comment-tools-18843" class="comment-tools"></div><div class="clear"></div><div id="comment-18843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18847"></span>

<div id="answer-container-18847" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18847-score" class="post-score" title="current number of votes">1</div><span id="post-18847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, as Anders already explained in <a href="https://ask.wireshark.org/questions/18694/is-the-rlc-reassembly-take-too-much-time">your previous question</a>, unless you provide a sample capture and fill a bug at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a> it is quite unlikely to be optimized (if it's possible). If you cannot share the capture, you can still work on it by yourself and fill a bug with your optimization patch.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Feb '13, 05:48</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-18847" class="comments-container"></div><div id="comment-tools-18847" class="comment-tools"></div><div class="clear"></div><div id="comment-18847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

