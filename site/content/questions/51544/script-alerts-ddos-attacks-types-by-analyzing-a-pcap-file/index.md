+++
type = "question"
title = "script alerts ddos attacks types by analyzing a pcap file ?"
description = '''hello guys , is there any way to write a shell script that analyze a pcap file and alerts you which ddos attack type your under , thanks .'''
date = "2016-04-10T07:34:00Z"
lastmod = "2016-04-10T07:34:00Z"
weight = 51544
keywords = [ "ddos", "script", "attack", "analysis", "wireshark" ]
aliases = [ "/questions/51544" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [script alerts ddos attacks types by analyzing a pcap file ?](/questions/51544/script-alerts-ddos-attacks-types-by-analyzing-a-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51544-score" class="post-score" title="current number of votes">0</div><span id="post-51544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello guys , is there any way to write a shell script that analyze a pcap file and alerts you which ddos attack type your under , thanks .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span> <span class="post-tag tag-link-script" rel="tag" title="see questions tagged &#39;script&#39;">script</span> <span class="post-tag tag-link-attack" rel="tag" title="see questions tagged &#39;attack&#39;">attack</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '16, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/55ec1348d1b99079e878bb291caa136c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="user12312&#39;s gravatar image" /><p><span>user12312</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="user12312 has no accepted answers">0%</span></p></div></div><div id="comments-container-51544" class="comments-container"></div><div id="comment-tools-51544" class="comment-tools"></div><div class="clear"></div><div id="comment-51544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

