+++
type = "question"
title = "recognizing surveillance/IP cameras"
description = '''Hallo, I encounter the following problem while trying to get my IP cameras to work: they are not recognized. Not in my surveillance software which came with my synology diskstation (and which is updated to the latest version; also DSM is updated to the latest version and the device package is update...'''
date = "2014-04-19T04:23:00Z"
lastmod = "2014-04-19T15:11:00Z"
weight = 31989
keywords = [ "ipcameras" ]
aliases = [ "/questions/31989" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [recognizing surveillance/IP cameras](/questions/31989/recognizing-surveillanceip-cameras)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31989-score" class="post-score" title="current number of votes">0</div><span id="post-31989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hallo, I encounter the following problem while trying to get my IP cameras to work: they are not recognized. Not in my surveillance software which came with my synology diskstation (and which is updated to the latest version; also DSM is updated to the latest version and the device package is updates to the latest version) I even have a licence key entered into my surveillance software, since the cameras I purchased are legally purchased with a license to work with my surveillance software. (Surveillance station 6, latest version) My diskstation is a synology 2-bay DS 213, which is wirelessly connected with my laptop, via a Poe-switch. Two IP cameras (ONVIF, model TR-FIPD126POE)are connected to this swith also. Surveillance station does not find the cameras; also Wireshark does not find the cameras. What should I do to get the cameras to work/to get recognized?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipcameras" rel="tag" title="see questions tagged &#39;ipcameras&#39;">ipcameras</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '14, 04:23</strong></p><img src="https://secure.gravatar.com/avatar/387fe31c000d0ee2b1ac80610c19f414?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Printerfreak15&#39;s gravatar image" /><p><span>Printerfreak15</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Printerfreak15 has no accepted answers">0%</span></p></div></div><div id="comments-container-31989" class="comments-container"></div><div id="comment-tools-31989" class="comment-tools"></div><div class="clear"></div><div id="comment-31989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31998"></span>

<div id="answer-container-31998" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31998-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31998-score" class="post-score" title="current number of votes">0</div><span id="post-31998-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>What should I do to get the cameras to work/to get recognized?</p></blockquote><p>maybe call the vendors support?</p><p>Honestly, if the cameras are not detected by your surveillance software and you cannot see anything of them (ARP, boradcasts, etc.) in a Wireshark capture, how are we supposed to help !?!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '14, 15:11</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-31998" class="comments-container"></div><div id="comment-tools-31998" class="comment-tools"></div><div class="clear"></div><div id="comment-31998-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

