+++
type = "question"
title = "wireshark and XP"
description = '''I have two PC&#x27;s connected to a router via wi-fi. Both of them are running using Windows XP SP3. My question is if I could use wireshark to monitor the traffic of the two PC&#x27;s using this O.S. XP. I have tried to run wireshark but it only works if I uncheck the promiscous mode option. (I suppose my US...'''
date = "2011-09-25T12:12:00Z"
lastmod = "2011-09-25T14:26:00Z"
weight = 6546
keywords = [ "xp", "wifi", "monitor", "wireshark" ]
aliases = [ "/questions/6546" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark and XP](/questions/6546/wireshark-and-xp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6546-score" class="post-score" title="current number of votes">0</div><span id="post-6546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have two PC's connected to a router via wi-fi. Both of them are running using Windows XP SP3. My question is if I could use wireshark to monitor the traffic of the two PC's using this O.S. XP. I have tried to run wireshark but it only works if I uncheck the promiscous mode option. (I suppose my USB adapter do not support monitor mode). (TP-LINK TL-WN422G v2). Pleae, could you help me? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xp" rel="tag" title="see questions tagged &#39;xp&#39;">xp</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '11, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/728d526976389eb67b5dd861a184dd10?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bfenix&#39;s gravatar image" /><p><span>bfenix</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bfenix has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-6546" class="comments-container"></div><div id="comment-tools-6546" class="comment-tools"></div><div class="clear"></div><div id="comment-6546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6548"></span>

<div id="answer-container-6548" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6548-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6548-score" class="post-score" title="current number of votes">1</div><span id="post-6548-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">entry</a> about capturing on wireless networks for more info, but generally on Windows you aren't able to capture using promiscuous mode. <a href="http://www.riverbed.com/us/products/cascade/airpcap.php">AirPcap</a> adaptors do allow you to do this though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '11, 14:26</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-6548" class="comments-container"></div><div id="comment-tools-6548" class="comment-tools"></div><div class="clear"></div><div id="comment-6548-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

