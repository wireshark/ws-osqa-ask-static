+++
type = "question"
title = "How to filter gsm map messages in Tshark"
description = '''Trying GSM_MAP and TCAP filter using tshark but Tshark is not giving any result  command used  tshark -r &amp;lt;inputfilename&amp;gt; -T fields -e gsm_old.localValue   Anyone please let me know what filter to use in tshark for gsm_map'''
date = "2014-02-12T06:03:00Z"
lastmod = "2014-02-12T06:03:00Z"
weight = 29760
keywords = [ "solo" ]
aliases = [ "/questions/29760" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to filter gsm map messages in Tshark](/questions/29760/how-to-filter-gsm-map-messages-in-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29760-score" class="post-score" title="current number of votes">0</div><span id="post-29760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trying GSM_MAP and TCAP filter using tshark but Tshark is not giving any result command used</p><blockquote><p>tshark -r &lt;inputfilename&gt; -T fields -e gsm_old.localValue</p></blockquote><p>Anyone please let me know what filter to use in tshark for gsm_map</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-solo" rel="tag" title="see questions tagged &#39;solo&#39;">solo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '14, 06:03</strong></p><img src="https://secure.gravatar.com/avatar/3dd6ce6fd2cf7f92b00cde858cffb6db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ezhra&#39;s gravatar image" /><p><span>Ezhra</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ezhra has no accepted answers">0%</span></p></div></div><div id="comments-container-29760" class="comments-container"></div><div id="comment-tools-29760" class="comment-tools"></div><div class="clear"></div><div id="comment-29760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

