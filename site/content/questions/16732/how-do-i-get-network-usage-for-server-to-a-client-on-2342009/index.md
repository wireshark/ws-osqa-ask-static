+++
type = "question"
title = "how do i get network usage for server to a client on 23/4/2009"
description = '''Are there any commands to get the network usage on given date in windows xp? i want to gather the statistics of network usage for 192.168.20.1 to 192.168.20.254 on date 23/4/2009'''
date = "2012-12-09T00:42:00Z"
lastmod = "2012-12-10T06:05:00Z"
weight = 16732
keywords = [ "netstat" ]
aliases = [ "/questions/16732" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how do i get network usage for server to a client on 23/4/2009](/questions/16732/how-do-i-get-network-usage-for-server-to-a-client-on-2342009)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16732-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16732-score" class="post-score" title="current number of votes">0</div><span id="post-16732-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are there any commands to get the network usage on given date in windows xp? i want to gather the statistics of network usage for 192.168.20.1 to 192.168.20.254 on date 23/4/2009</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-netstat" rel="tag" title="see questions tagged &#39;netstat&#39;">netstat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '12, 00:42</strong></p><img src="https://secure.gravatar.com/avatar/af83123d92c126fce7ec6771fece3b46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maykin&#39;s gravatar image" /><p><span>Maykin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maykin has no accepted answers">0%</span></p></div></div><div id="comments-container-16732" class="comments-container"></div><div id="comment-tools-16732" class="comment-tools"></div><div class="clear"></div><div id="comment-16732-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16734"></span>

<div id="answer-container-16734" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16734-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16734-score" class="post-score" title="current number of votes">2</div><span id="post-16734-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you capture the packets of these two IP addresses on that day, or did you have a netflow/sflow probe reporting traffic statistics on the same day to a netflow collector? If the answer is no, you need to build a time machine first before you can use Wireshark to get what you want ;-)</p><p>Meaning: you can only analyze what you captured. If you didn't, there is nothing to be done about it. By the way, 23/4/2009 is now about 3.5 years in the past, so even if the XP machine would keep some local statistics they're long gone by now.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '12, 03:06</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-16734" class="comments-container"><span id="16747"></span><div id="comment-16747" class="comment"><div id="post-16747-score" class="comment-score"></div><div class="comment-text"><blockquote><p>you need to build a time machine first before you can use Wireshark to get what you want ;-)</p></blockquote><p>I suggest this time machine: <a href="http://waybackmachine.org">waybackmachine.org</a> ;-))</p></div><div id="comment-16747-info" class="comment-info"><span class="comment-age">(10 Dec '12, 06:05)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-16734" class="comment-tools"></div><div class="clear"></div><div id="comment-16734-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

