+++
type = "question"
title = "undefined symbol"
description = '''hi, when i am trying to add open-flow plugin in wireshark it gives me an error, packet-openflow.so: undefined symbol: match_strval'''
date = "2013-11-08T22:58:00Z"
lastmod = "2013-11-09T05:05:00Z"
weight = 26797
keywords = [ "match_strval" ]
aliases = [ "/questions/26797" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [undefined symbol](/questions/26797/undefined-symbol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26797-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26797-score" class="post-score" title="current number of votes">0</div><span id="post-26797-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, when i am trying to add open-flow plugin in wireshark it gives me an error, packet-openflow.so: undefined symbol: match_strval</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-match_strval" rel="tag" title="see questions tagged &#39;match_strval&#39;">match_strval</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '13, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/06fe3e94f30c866ae1b474d53d37fb76?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chougulepavan&#39;s gravatar image" /><p><span>chougulepavan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chougulepavan has no accepted answers">0%</span></p></div></div><div id="comments-container-26797" class="comments-container"></div><div id="comment-tools-26797" class="comment-tools"></div><div class="clear"></div><div id="comment-26797-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26800"></span>

<div id="answer-container-26800" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26800-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26800-score" class="post-score" title="current number of votes">0</div><span id="post-26800-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Plugins are only guaranteed to work with the Wireshark version for which they are compiled check if your Wireshark version is the same as the one the plugin was compiled for.</p><p>Top of trunk has an inbuilt openflow dissector for 1.3 and an incomplete one for version 1.0, you could try to compile from trunk.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Nov '13, 05:05</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-26800" class="comments-container"></div><div id="comment-tools-26800" class="comment-tools"></div><div class="clear"></div><div id="comment-26800-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

