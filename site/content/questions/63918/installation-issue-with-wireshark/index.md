+++
type = "question"
title = "Installation issue with Wireshark"
description = '''Hello there Its my first post here.  I am running windows 7 pro 32bit i downloaded the relevant type of wireshark programme but that gives an error upon an execution.  I have attached the pic of the error that displays when i attempt to run the programme.  kindly guide what mistake i am doing. Thank...'''
date = "2017-10-15T22:34:00Z"
lastmod = "2017-10-16T22:22:00Z"
weight = 63918
keywords = [ "installation" ]
aliases = [ "/questions/63918" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Installation issue with Wireshark](/questions/63918/installation-issue-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63918-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63918-score" class="post-score" title="current number of votes">0</div><span id="post-63918-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/wireshark_mG2FLWK.png" alt="alt text" />Hello there</p><p>Its my first post here. I am running windows 7 pro 32bit i downloaded the relevant type of wireshark programme but that gives an error upon an execution. I have attached the pic of the error that displays when i attempt to run the programme. kindly guide what mistake i am doing. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '17, 22:34</strong></p><img src="https://secure.gravatar.com/avatar/b5eea2a6c7b31744f8e1f0c4eae2c518?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shaheed&#39;s gravatar image" /><p><span>shaheed</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shaheed has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '17, 22:35</strong> </span></p></div></div><div id="comments-container-63918" class="comments-container"><span id="63919"></span><div id="comment-63919" class="comment"><div id="post-63919-score" class="comment-score">1</div><div class="comment-text"><p>What Wireshark version are you trying to install?</p><p>Where did you download it from?</p><p>Is your Win7 installation up to date (SP1 with updates at least)?</p></div><div id="comment-63919-info" class="comment-info"><span class="comment-age">(15 Oct '17, 22:59)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="63951"></span><div id="comment-63951" class="comment"><div id="post-63951-score" class="comment-score"></div><div class="comment-text"><p>Wireshark-win32-2.4.2 from its official site of wireshark. My windows isn't upgraded to SP1.</p></div><div id="comment-63951-info" class="comment-info"><span class="comment-age">(16 Oct '17, 22:22)</span> <span class="comment-user userinfo">shaheed</span></div></div></div><div id="comment-tools-63918" class="comment-tools"></div><div class="clear"></div><div id="comment-63918-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

