+++
type = "question"
title = "Graphing Help with New Wireshark.. Legacy version works as expected"
description = '''Graphing in Wireshark Legacy shows correct graph. Simple filter dns.time on the max field thats all. On the latest and greatest wireshark version the graph per second it has exponential numbers'''
date = "2017-03-11T17:20:00Z"
lastmod = "2017-03-11T17:20:00Z"
weight = 60008
keywords = [ "graph" ]
aliases = [ "/questions/60008" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Graphing Help with New Wireshark.. Legacy version works as expected](/questions/60008/graphing-help-with-new-wireshark-legacy-version-works-as-expected)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60008-score" class="post-score" title="current number of votes">0</div><span id="post-60008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Graphing in Wireshark Legacy shows correct graph. Simple filter dns.time on the max field thats all. On the latest and greatest wireshark version the graph per second it has exponential numbers</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '17, 17:20</strong></p><img src="https://secure.gravatar.com/avatar/da8644a6d370b169a93e723d9591bc31?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="staceylareece&#39;s gravatar image" /><p><span>staceylareece</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="staceylareece has no accepted answers">0%</span></p></div></div><div id="comments-container-60008" class="comments-container"></div><div id="comment-tools-60008" class="comment-tools"></div><div class="clear"></div><div id="comment-60008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

