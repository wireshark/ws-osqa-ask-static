+++
type = "question"
title = "How to decode a HTTP packet"
description = '''Hi! :) I want decode a packet to obtain a packet, which I can then send and repeat this packet to get coins on this site. How to decode packet to get format for me to resend? Please give me procedure.'''
date = "2017-04-16T09:10:00Z"
lastmod = "2017-04-16T09:10:00Z"
weight = 60858
keywords = [ "decode", "http", "packets", "packet" ]
aliases = [ "/questions/60858" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode a HTTP packet](/questions/60858/how-to-decode-a-http-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60858-score" class="post-score" title="current number of votes">0</div><span id="post-60858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! :) I want decode a packet to obtain a packet, which I can then send and repeat this packet to get coins on this site.</p><p>How to decode packet to get format for me to resend? Please give me procedure.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '17, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/01f4ed297682b0ea3e2891a6d7e59516?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MarteZ&#39;s gravatar image" /><p><span>MarteZ</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MarteZ has no accepted answers">0%</span></p></div></div><div id="comments-container-60858" class="comments-container"></div><div id="comment-tools-60858" class="comment-tools"></div><div class="clear"></div><div id="comment-60858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

