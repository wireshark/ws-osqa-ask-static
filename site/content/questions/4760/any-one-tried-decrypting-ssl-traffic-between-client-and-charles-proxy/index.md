+++
type = "question"
title = "Any one tried decrypting ssl traffic between client and Charles Proxy?"
description = '''I know that with Charles Proxy (and Fiddler) I can see the decrypted ssl traffic, but I also want to see it in Wireshark.  I&#x27;m certain that I capture the traffic on the right channel (between my client and Charles Proxy), and pretty sure I got Charles Proxy&#x27;s private encryption key. But, I am not ab...'''
date = "2011-06-26T08:54:00Z"
lastmod = "2011-06-30T18:25:00Z"
weight = 4760
keywords = [ "ssl", "charles", "proxy", "decryption" ]
aliases = [ "/questions/4760" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Any one tried decrypting ssl traffic between client and Charles Proxy?](/questions/4760/any-one-tried-decrypting-ssl-traffic-between-client-and-charles-proxy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4760-score" class="post-score" title="current number of votes">0</div><span id="post-4760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know that with Charles Proxy (and Fiddler) I can see the decrypted ssl traffic, but I also want to see it in Wireshark.</p><p>I'm certain that I capture the traffic on the right channel (between my client and Charles Proxy), and pretty sure I got Charles Proxy's private encryption key. But, I am not able to decrypt the traffic.</p><p>Any one else tried such a feat?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-charles" rel="tag" title="see questions tagged &#39;charles&#39;">charles</span> <span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '11, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/9b7b5e633f7836289c2fc6c3934bffaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="r0u1i&#39;s gravatar image" /><p><span>r0u1i</span><br />
<span class="score" title="61 reputation points">61</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="r0u1i has no accepted answers">0%</span></p></div></div><div id="comments-container-4760" class="comments-container"><span id="4879"></span><div id="comment-4879" class="comment"><div id="post-4879-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark are you using?</p></div><div id="comment-4879-info" class="comment-info"><span class="comment-age">(30 Jun '11, 18:25)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-4760" class="comment-tools"></div><div class="clear"></div><div id="comment-4760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

