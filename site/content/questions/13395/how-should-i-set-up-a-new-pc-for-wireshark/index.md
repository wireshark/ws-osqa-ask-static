+++
type = "question"
title = "How should I set up a new PC for Wireshark?"
description = '''We are replacing Distributed Sniffers with PC&#x27;s running Wireshark. I&#x27;m used to a 2 NIC scenerio- one to do the monitoring and one for communications. As we will be running these boxes remotely, are 2 NIC&#x27;s needed/reccomended/not needed?  Thanks.'''
date = "2012-08-06T06:25:00Z"
lastmod = "2012-08-06T07:12:00Z"
weight = 13395
keywords = [ "remote-monitoring", "sniffer", "config" ]
aliases = [ "/questions/13395" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How should I set up a new PC for Wireshark?](/questions/13395/how-should-i-set-up-a-new-pc-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13395-score" class="post-score" title="current number of votes">0</div><span id="post-13395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are replacing Distributed Sniffers with PC's running Wireshark. I'm used to a 2 NIC scenerio- one to do the monitoring and one for communications. As we will be running these boxes remotely, are 2 NIC's needed/reccomended/not needed?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-monitoring" rel="tag" title="see questions tagged &#39;remote-monitoring&#39;">remote-monitoring</span> <span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-config" rel="tag" title="see questions tagged &#39;config&#39;">config</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Aug '12, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/63907917301321b12d06e5d5940e5fbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveBrady&#39;s gravatar image" /><p><span>SteveBrady</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveBrady has no accepted answers">0%</span></p></div></div><div id="comments-container-13395" class="comments-container"></div><div id="comment-tools-13395" class="comment-tools"></div><div class="clear"></div><div id="comment-13395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13396"></span>

<div id="answer-container-13396" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13396-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13396-score" class="post-score" title="current number of votes">1</div><span id="post-13396-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SteveBrady has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would say recommended. Makes life easier:</p><ul><li>you don't need to remember to filter out your own traffic</li><li>you can sniff passively (Windows: remove all services/protocols from the sniffer interface)</li><li>you can reach it even if the sniffer port connected network is down (if separated networks)</li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Aug '12, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13396" class="comments-container"></div><div id="comment-tools-13396" class="comment-tools"></div><div class="clear"></div><div id="comment-13396-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

