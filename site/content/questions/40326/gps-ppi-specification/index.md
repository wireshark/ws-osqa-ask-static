+++
type = "question"
title = "GPS PPI Specification"
description = '''Is the specification posted anywhere for the PPI Vendor Field 30,002 GPS Tagging  Wirshark is able to display this field: https://www.wireshark.org/docs/dfref/p/ppi_gps.html The original author&#x27;s page is currently down: http://new.11mercenary.net/~johnycsh/ppi_geolocation_spec/ Does the wireshark te...'''
date = "2015-03-06T07:27:00Z"
lastmod = "2015-03-19T14:32:00Z"
weight = 40326
keywords = [ "ppi" ]
aliases = [ "/questions/40326" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GPS PPI Specification](/questions/40326/gps-ppi-specification)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40326-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40326-score" class="post-score" title="current number of votes">0</div><span id="post-40326-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is the specification posted anywhere for the PPI Vendor Field 30,002 GPS Tagging</p><p>Wirshark is able to display this field: <a href="https://www.wireshark.org/docs/dfref/p/ppi_gps.html">https://www.wireshark.org/docs/dfref/p/ppi_gps.html</a></p><p>The original author's page is currently down: <a href="http://new.11mercenary.net/~johnycsh/ppi_geolocation_spec/">http://new.11mercenary.net/~johnycsh/ppi_geolocation_spec/</a></p><p>Does the wireshark team have a copy of the specification?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ppi" rel="tag" title="see questions tagged &#39;ppi&#39;">ppi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '15, 07:27</strong></p><img src="https://secure.gravatar.com/avatar/6781278c1b8baa46b04212fe2338d2a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dye986&#39;s gravatar image" /><p><span>Dye986</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dye986 has no accepted answers">0%</span></p></div></div><div id="comments-container-40326" class="comments-container"></div><div id="comment-tools-40326" class="comment-tools"></div><div class="clear"></div><div id="comment-40326-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40405"></span>

<div id="answer-container-40405" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40405-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40405-score" class="post-score" title="current number of votes">0</div><span id="post-40405-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Apparently the PPI-GEOLOCATION spec was developed by an employee of Harris, Jon Ellch.</p><blockquote><p><a href="http://govcomm.harris.com/news/view_pressrelease.asp?act=lookup&amp;pr_id=3290">http://govcomm.harris.com/news/view_pressrelease.asp?act=lookup&amp;pr_id=3290</a></p></blockquote><p>So, maybe it's best to contact him or Harris to get the specs. Unfortunately the link to the "supporting material" is dead.</p><p>See also here:</p><blockquote><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5175">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5175</a></p></blockquote><p>For a gmail address of the spec author, see here:</p><blockquote><p><a href="https://github.com/metageek-llc/inSSIDer-2/issues/25">https://github.com/metageek-llc/inSSIDer-2/issues/25</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '15, 14:22</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Mar '15, 14:25</strong> </span></p></div></div><div id="comments-container-40405" class="comments-container"><span id="40561"></span><div id="comment-40561" class="comment"><div id="post-40561-score" class="comment-score"></div><div class="comment-text"><p>I was able to reach out to the SPEC author and he provided me with the latest SPEC document, it's currently in PDF format - how can we best archive it in the Wireshark repository and/or wiki such that other members in the community may have access to it?</p></div><div id="comment-40561-info" class="comment-info"><span class="comment-age">(14 Mar '15, 07:59)</span> <span class="comment-user userinfo">Dye986</span></div></div><span id="40697"></span><div id="comment-40697" class="comment"><div id="post-40697-score" class="comment-score"></div><div class="comment-text"><p>Best way would be to create an entry for PPI-GEOLOCATION on <a href="https://wiki.wireshark.org/">https://wiki.wireshark.org/</a> and attach the file to that entry, provided the author of the spec allows pbulic publishing the document.</p></div><div id="comment-40697-info" class="comment-info"><span class="comment-age">(19 Mar '15, 14:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-40405" class="comment-tools"></div><div class="clear"></div><div id="comment-40405-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

