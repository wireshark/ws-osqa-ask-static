+++
type = "question"
title = "Unable to add link to other Q&amp;A"
description = '''When I try to include a link to another answer I get a message telling my &#x27;Askimet thinks this is spam&#x27;. How can a link to another question on the same site be spam?'''
date = "2013-10-09T01:31:00Z"
lastmod = "2013-10-30T07:36:00Z"
weight = 25782
keywords = [ "links" ]
aliases = [ "/questions/25782" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Unable to add link to other Q&A](/questions/25782/unable-to-add-link-to-other-qa)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25782-score" class="post-score" title="current number of votes">0</div><span id="post-25782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I try to include a link to another answer I get a message telling my 'Askimet thinks this is spam'. How can a link to another question on the same site be spam?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-links" rel="tag" title="see questions tagged &#39;links&#39;">links</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '13, 01:31</strong></p><img src="https://secure.gravatar.com/avatar/310c7b54264c9e10ad43acb3bb1d042a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiggers&#39;s gravatar image" /><p><span>wiggers</span><br />
<span class="score" title="31 reputation points">31</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiggers has no accepted answers">0%</span></p></div></div><div id="comments-container-25782" class="comments-container"></div><div id="comment-tools-25782" class="comment-tools"></div><div class="clear"></div><div id="comment-25782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26520"></span>

<div id="answer-container-26520" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26520-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26520-score" class="post-score" title="current number of votes">0</div><span id="post-26520-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wiggers has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I believe Gerald made a change recently that may have corrected this. Could you try again to see if it's still a problem?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '13, 10:14</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-26520" class="comments-container"><span id="26540"></span><div id="comment-26540" class="comment"><div id="post-26540-score" class="comment-score"></div><div class="comment-text"><p>Here is my latest question: <a href="http://ask.wireshark.org/questions/26539/where-can-i-find-a-list-of-file-formats-by-extension">http://ask.wireshark.org/questions/26539/where-can-i-find-a-list-of-file-formats-by-extension</a></p></div><div id="comment-26540-info" class="comment-info"><span class="comment-age">(30 Oct '13, 07:36)</span> <span class="comment-user userinfo">wiggers</span></div></div><span id="26541"></span><div id="comment-26541" class="comment"><div id="post-26541-score" class="comment-score"></div><div class="comment-text"><p>Seems to be working now!</p></div><div id="comment-26541-info" class="comment-info"><span class="comment-age">(30 Oct '13, 07:36)</span> <span class="comment-user userinfo">wiggers</span></div></div></div><div id="comment-tools-26520" class="comment-tools"></div><div class="clear"></div><div id="comment-26520-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

