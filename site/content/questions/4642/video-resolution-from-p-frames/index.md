+++
type = "question"
title = "Video Resolution from P-Frames"
description = '''I know how to look at an H264 packet I Frame to get the Video Resolution. Does anyone know how to calculate Video Resolution from P Frames'''
date = "2011-06-20T20:52:00Z"
lastmod = "2013-11-08T07:38:00Z"
weight = 4642
keywords = [ "videoresolution" ]
aliases = [ "/questions/4642" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Video Resolution from P-Frames](/questions/4642/video-resolution-from-p-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4642-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4642-score" class="post-score" title="current number of votes">0</div><span id="post-4642-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know how to look at an H264 packet I Frame to get the Video Resolution. Does anyone know how to calculate Video Resolution from P Frames</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-videoresolution" rel="tag" title="see questions tagged &#39;videoresolution&#39;">videoresolution</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '11, 20:52</strong></p><img src="https://secure.gravatar.com/avatar/b76add3dbdc4eced097a7d2310afd09c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="meggig&#39;s gravatar image" /><p><span>meggig</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="meggig has no accepted answers">0%</span></p></div></div><div id="comments-container-4642" class="comments-container"><span id="26777"></span><div id="comment-26777" class="comment"><div id="post-26777-score" class="comment-score"></div><div class="comment-text"><p>I am looking for information on how to get video resolution from I-fame. If I filder with h264.slice_type == 2 which is supposed to give me all I-fames, but when decoded all i see is: F bit: No bit errors NRI: 1 Type: NAL unit -Reserved (17) H264 bitstream</p><p>How do you get resolution from this?</p><p>Thanks, Nenad</p></div><div id="comment-26777-info" class="comment-info"><span class="comment-age">(08 Nov '13, 07:38)</span> <span class="comment-user userinfo">nm3</span></div></div></div><div id="comment-tools-4642" class="comment-tools"></div><div class="clear"></div><div id="comment-4642-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

