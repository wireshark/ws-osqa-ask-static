+++
type = "question"
title = "Example of how to use ask.wireshark.org and how not to..."
description = '''What color do apples have?   This question will be used to demonstrate the way this sites work. It will also show how this site is different from a normal web-forum, as the chronological order of answers can be messed by by the voting process. Please use the &quot;code sample&quot; text style for any new answ...'''
date = "2010-09-23T07:00:00Z"
lastmod = "2010-09-23T07:35:00Z"
weight = 292
keywords = [ "meta", "example", "ask.wireshark.org", "apples" ]
aliases = [ "/questions/292" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Example of how to use ask.wireshark.org and how not to...](/questions/292/example-of-how-to-use-askwiresharkorg-and-how-not-to)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-292-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-292-score" class="post-score" title="current number of votes">1</div><span id="post-292-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>What color do apples have?</code></pre><hr /><p>This question will be used to demonstrate the way this sites work. It will also show how this site is different from a normal web-forum, as the chronological order of answers can be messed by by the voting process.</p><p>Please use the "code sample" text style for any new answer or comment as it would have appeared in the discussion. Please use normal text to explain why things work or don't work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span> <span class="post-tag tag-link-example" rel="tag" title="see questions tagged &#39;example&#39;">example</span> <span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span> <span class="post-tag tag-link-apples" rel="tag" title="see questions tagged &#39;apples&#39;">apples</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '10, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Sep '10, 07:36</strong> </span></p></div></div><div id="comments-container-292" class="comments-container"><span id="298"></span><div id="comment-298" class="comment"><div id="post-298-score" class="comment-score"></div><div class="comment-text"><p>I am wondering if looking at Stackoverflow and Mathoverflow would be good examples? These sites seem to have similar styles.</p></div><div id="comment-298-info" class="comment-info"><span class="comment-age">(23 Sep '10, 07:23)</span> <span class="comment-user userinfo">samiam</span></div></div><span id="299"></span><div id="comment-299" class="comment"><div id="post-299-score" class="comment-score"></div><div class="comment-text"><p>Indeed they use a similar style and as such, they are good examples too. But maybe a bit to complex for a first time user. So I hope this example question will help people in understanding the logic of this site.</p></div><div id="comment-299-info" class="comment-info"><span class="comment-age">(23 Sep '10, 07:35)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-292" class="comment-tools"></div><div class="clear"></div><div id="comment-292-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="297"></span>

<div id="answer-container-297" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-297-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-297-score" class="post-score" title="current number of votes">1</div><span id="post-297-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><pre><code>Apples can have different colors. Most apples are green, yellow or red.</code></pre><hr /><p>This was the third answer given and indeed it is the answer that the original poster was looking for.</p><p>To make sure the question does not appear in the "unanswered questions" category, the person that asked the question needs to "accept" it. This is done by clicking on the "checkmark" below the voting thumbs. Once an answer has been accepted, it will be marked in green to make it easy for other people that have the same question, to find the correct answer (especially when multiple answers have been given to a question).</p><p>I have accepted this answer already, hence it is showing in green.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '10, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Sep '10, 07:29</strong> </span></p></div></div><div id="comments-container-297" class="comments-container"></div><div id="comment-tools-297" class="comment-tools"></div><div class="clear"></div><div id="comment-297-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="294"></span>

<div id="answer-container-294" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-294-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-294-score" class="post-score" title="current number of votes">1</div><span id="post-294-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><pre><code>Apples are not purple!</code></pre><hr /><p>This was the second answer given as a response to the first answer. This answer is of course true, so people may vote "up" for this answer. This means this answer will be listed above the first answer and the logic of it being a response to the first answer disappears.</p><p>On this site responses should be made by using "add new comment". I have done this also, have a look at the "retry as a comment".</p><p>(please don't vote for this answer to keep it at a rating of +1)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '10, 07:05</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Sep '10, 07:33</strong> </span></p></div></div><div id="comments-container-294" class="comments-container"><span id="295"></span><div id="comment-295" class="comment"><div id="post-295-score" class="comment-score"></div><div class="comment-text"><p>When you (accidentally) give a new answer instead of adding a comment, you can convert your answer by using the "More" arrow below your answer and then choose: "Convert to comment"</p><p>(which I will not do with this response for the obvious educational reasons ;-))</p></div><div id="comment-295-info" class="comment-info"><span class="comment-age">(23 Sep '10, 07:07)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-294" class="comment-tools"></div><div class="clear"></div><div id="comment-294-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="293"></span>

<div id="answer-container-293" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-293-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-293-score" class="post-score" title="current number of votes">-1</div><span id="post-293-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><pre><code>Apples are purple</code></pre><hr /><p>This was the first answer given, but it is obviously a wrong answer. Therefor people may vote it down. That means that the answer will get listed lower on the list of answers.</p><p>(please don't vote for this answer to keep it at a rating of -1)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '10, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Sep '10, 07:32</strong> </span></p></div></div><div id="comments-container-293" class="comments-container"><span id="296"></span><div id="comment-296" class="comment"><div id="post-296-score" class="comment-score"></div><div class="comment-text"><p>Retry as a comment: Apples are not purple!</p></div><div id="comment-296-info" class="comment-info"><span class="comment-age">(23 Sep '10, 07:08)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-293" class="comment-tools"></div><div class="clear"></div><div id="comment-293-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</hr>

</div>

</div>

