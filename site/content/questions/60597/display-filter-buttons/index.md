+++
type = "question"
title = "display filter buttons"
description = '''In my old version of WS 1.10.2 I was able to create different profiles each with individual filters. Those filters were saved as &quot;buttons&quot; to the right of the Expressions, Clear, Apply Save, then my custom &quot;buttons&quot; I imported my profiles to WS 2.2.5, but I only see one of my handful of buttons crea...'''
date = "2017-04-05T14:10:00Z"
lastmod = "2017-04-17T08:49:00Z"
weight = 60597
keywords = [ "filter", "buttons", "display" ]
aliases = [ "/questions/60597" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [display filter buttons](/questions/60597/display-filter-buttons)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60597-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60597-score" class="post-score" title="current number of votes">0</div><span id="post-60597-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In my old version of WS 1.10.2 I was able to create different profiles each with individual filters. Those filters were saved as "buttons" to the right of the Expressions, Clear, Apply Save, then my custom "buttons"</p><p>I imported my profiles to WS 2.2.5, but I only see one of my handful of buttons created in the older version. Does anybody know how to customize the display filter row so I can see all my Filter buttons?</p><p>I'm running OS X.</p><p>thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-buttons" rel="tag" title="see questions tagged &#39;buttons&#39;">buttons</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '17, 14:10</strong></p><img src="https://secure.gravatar.com/avatar/b6100781aa36dd9ad2584c9de841f801?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ajk5522&#39;s gravatar image" /><p><span>ajk5522</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ajk5522 has no accepted answers">0%</span></p></div></div><div id="comments-container-60597" class="comments-container"></div><div id="comment-tools-60597" class="comment-tools"></div><div class="clear"></div><div id="comment-60597-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60838"></span>

<div id="answer-container-60838" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60838-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60838-score" class="post-score" title="current number of votes">0</div><span id="post-60838-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In general filter buttons are available in the filter toolbar.</p><p>The filter buttons are always profile specific. So you have to select the right profile.</p><p>Furthermore you can enable/disable filter buttons in the "Preferences" -&gt; "Filter expression" dialog.</p><p>Another possible reason for your issue could be that the filter expression is not valid in WS 2.2.X. For some protocols field names could have been changed from 1.10.X -&gt; 2.2.X.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '17, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-60838" class="comments-container"><span id="60865"></span><div id="comment-60865" class="comment"><div id="post-60865-score" class="comment-score"></div><div class="comment-text"><p>Thank you - this helped..</p><p>(Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information).</p></div><div id="comment-60865-info" class="comment-info"><span class="comment-age">(17 Apr '17, 08:49)</span> <span class="comment-user userinfo">ajk5522</span></div></div></div><div id="comment-tools-60838" class="comment-tools"></div><div class="clear"></div><div id="comment-60838-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

