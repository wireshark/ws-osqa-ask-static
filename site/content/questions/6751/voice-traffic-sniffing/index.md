+++
type = "question"
title = "voice traffic sniffing"
description = '''I am trying to sniffing the voice traffic (AVAYA VoIP) on my network (HP ProCurve) but there is no output except the network traffic like SNMP,VRRP, ARP....etc.'''
date = "2011-10-06T04:11:00Z"
lastmod = "2011-10-06T04:11:00Z"
weight = 6751
keywords = [ "hp", "voice", "avaya" ]
aliases = [ "/questions/6751" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [voice traffic sniffing](/questions/6751/voice-traffic-sniffing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6751-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6751-score" class="post-score" title="current number of votes">0</div><span id="post-6751-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to sniffing the voice traffic (AVAYA VoIP) on my network (HP ProCurve) but there is no output except the network traffic like SNMP,VRRP, ARP....etc.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hp" rel="tag" title="see questions tagged &#39;hp&#39;">hp</span> <span class="post-tag tag-link-voice" rel="tag" title="see questions tagged &#39;voice&#39;">voice</span> <span class="post-tag tag-link-avaya" rel="tag" title="see questions tagged &#39;avaya&#39;">avaya</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '11, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/80790acbe8071b8a4faae0d04a0a5e7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ashboull&#39;s gravatar image" /><p><span>ashboull</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ashboull has no accepted answers">0%</span></p></div></div><div id="comments-container-6751" class="comments-container"></div><div id="comment-tools-6751" class="comment-tools"></div><div class="clear"></div><div id="comment-6751-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

