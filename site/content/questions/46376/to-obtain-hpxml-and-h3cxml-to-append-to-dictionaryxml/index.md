+++
type = "question"
title = "To obtain hp.xml and h3c.xml to append to dictionary.xml"
description = '''hi everyone, is there a place within wireshark.org that has those 2 above-mentioned files for download? thanks!'''
date = "2015-10-06T02:56:00Z"
lastmod = "2015-10-06T02:59:00Z"
weight = 46376
keywords = [ "hp.xml", "h3c.xml" ]
aliases = [ "/questions/46376" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [To obtain hp.xml and h3c.xml to append to dictionary.xml](/questions/46376/to-obtain-hpxml-and-h3cxml-to-append-to-dictionaryxml)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46376-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46376-score" class="post-score" title="current number of votes">0</div><span id="post-46376-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi everyone,</p><p>is there a place within wireshark.org that has those 2 above-mentioned files for download?</p><p>thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hp.xml" rel="tag" title="see questions tagged &#39;hp.xml&#39;">hp.xml</span> <span class="post-tag tag-link-h3c.xml" rel="tag" title="see questions tagged &#39;h3c.xml&#39;">h3c.xml</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '15, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/79b9fce5d77fb8eec5e71a40532d64dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john2015&#39;s gravatar image" /><p><span>john2015</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john2015 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Oct '15, 06:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-46376" class="comments-container"><span id="46377"></span><div id="comment-46377" class="comment"><div id="post-46377-score" class="comment-score"></div><div class="comment-text"><p>sorry for the typo. download.xml = ../diameter/dictionary.xml</p></div><div id="comment-46377-info" class="comment-info"><span class="comment-age">(06 Oct '15, 02:59)</span> <span class="comment-user userinfo">john2015</span></div></div></div><div id="comment-tools-46376" class="comment-tools"></div><div class="clear"></div><div id="comment-46376-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46378"></span>

<div id="answer-container-46378" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46378-score" class="post-score" title="current number of votes">0</div><span id="post-46378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can find them in the <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=tree;f=diameter;h=44ea151351e93ec23c41cc444ce77cfe4bfa0583;hb=refs/heads/master">source code repository</a>.</p><p>Here is the direct link for <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=diameter/HP.xml;h=9db0ade841982ee9a1708e0248a4300e8e491480;hb=refs/heads/master">HP.xml</a> There is no file named h3c.xml distributed with Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Oct '15, 02:59</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-46378" class="comments-container"></div><div id="comment-tools-46378" class="comment-tools"></div><div class="clear"></div><div id="comment-46378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

