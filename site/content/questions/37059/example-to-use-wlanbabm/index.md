+++
type = "question"
title = "example to use wlan.ba.bm"
description = '''Hello, Can I get some help in using wlan.ba.bm filter? I want to filter all the BAcks which do not have the bitmap as ffffffff000000000000. I tried as wlan.ba.bm != ffffffff000000000000 but did not work, gave error. regards -Nitin'''
date = "2014-10-15T05:00:00Z"
lastmod = "2014-10-15T05:00:00Z"
weight = 37059
keywords = [ "wlan" ]
aliases = [ "/questions/37059" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [example to use wlan.ba.bm](/questions/37059/example-to-use-wlanbabm)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37059-score" class="post-score" title="current number of votes">0</div><span id="post-37059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Can I get some help in using wlan.ba.bm filter? I want to filter all the BAcks which do not have the bitmap as ffffffff000000000000.</p><p>I tried as wlan.ba.bm != ffffffff000000000000 but did not work, gave error.</p><p>regards -Nitin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '14, 05:00</strong></p><img src="https://secure.gravatar.com/avatar/062c014b92e4b47beecb6fd62dbe0f0b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nitin%20M&#39;s gravatar image" /><p><span>Nitin M</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nitin M has no accepted answers">0%</span></p></div></div><div id="comments-container-37059" class="comments-container"></div><div id="comment-tools-37059" class="comment-tools"></div><div class="clear"></div><div id="comment-37059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

