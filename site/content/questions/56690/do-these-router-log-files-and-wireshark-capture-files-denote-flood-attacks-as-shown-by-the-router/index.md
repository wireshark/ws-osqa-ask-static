+++
type = "question"
title = "Do these Router log files and Wireshark capture files denote flood attacks as shown by the Router?"
description = '''My router picks up Flood attacks every time I switch it on. So, I tried capturing packets on wireshark by connecting the WAN cable to the PC directly. I&#x27;ve attached the Router log files generated in a single session for a period of time and Wireshark&#x27;s Captured packet files at another point of time,...'''
date = "2016-10-26T06:33:00Z"
lastmod = "2016-10-26T06:51:00Z"
weight = 56690
keywords = [ "syn-ack", "ddos", "ip-flooding", "udpchargen" ]
aliases = [ "/questions/56690" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Do these Router log files and Wireshark capture files denote flood attacks as shown by the Router?](/questions/56690/do-these-router-log-files-and-wireshark-capture-files-denote-flood-attacks-as-shown-by-the-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56690-score" class="post-score" title="current number of votes">0</div><span id="post-56690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My router picks up Flood attacks every time I switch it on. So, I tried capturing packets on wireshark by connecting the WAN cable to the PC directly. I've attached the Router log files generated in a single session for a period of time and Wireshark's Captured packet files at another point of time, using Win10PCap as WinPCap 4.1.3 didn't work for some reason on Wireshark 2.2.1(64-bit)on Windows 10 x64. Please help me analyze if there are truly any DOS or DDOS attacks occurring on my network as the router log suggests. Router Log 1- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkZXB0TkpjZWdoSW8">https://drive.google.com/open?id=0B4PGygpcKwfkZXB0TkpjZWdoSW8</a> Router Log 2- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkV3NuMEdrV1UzOGs">https://drive.google.com/open?id=0B4PGygpcKwfkV3NuMEdrV1UzOGs</a> Router Log 3- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkVFZENkI5T1d0OUk">https://drive.google.com/open?id=0B4PGygpcKwfkVFZENkI5T1d0OUk</a> Router Log 4- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkZkJlVUE4clRrV28">https://drive.google.com/open?id=0B4PGygpcKwfkZkJlVUE4clRrV28</a> Router Log 5- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkWWpRMm5pYjN5Rnc">https://drive.google.com/open?id=0B4PGygpcKwfkWWpRMm5pYjN5Rnc</a> Router Log 6- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkSjB4Um9mcERTdFk">https://drive.google.com/open?id=0B4PGygpcKwfkSjB4Um9mcERTdFk</a> Router Log 7- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkcmpDa0lYWExvTlk">https://drive.google.com/open?id=0B4PGygpcKwfkcmpDa0lYWExvTlk</a> Router Log 8- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkOF9RWjBIZ1FSMHM">https://drive.google.com/open?id=0B4PGygpcKwfkOF9RWjBIZ1FSMHM</a></p><p>Wireshark1st pcapng- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkMnBlZDFLb1B6Sjg">https://drive.google.com/open?id=0B4PGygpcKwfkMnBlZDFLb1B6Sjg</a> Wireshark2nd pcap- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkUHQ1d0lkUW9LWjg">https://drive.google.com/open?id=0B4PGygpcKwfkUHQ1d0lkUW9LWjg</a> Wireshark3rd pcap- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkTkdCN2xkWDg2WTQ">https://drive.google.com/open?id=0B4PGygpcKwfkTkdCN2xkWDg2WTQ</a> Wireshark4th pcapng- <a href="https://drive.google.com/open?id=0B4PGygpcKwfkMUg0YUktZXZIWDA">https://drive.google.com/open?id=0B4PGygpcKwfkMUg0YUktZXZIWDA</a></p><hr /><p>Please HELP in finding if the router log files are true! The IP 172.16.23.65 on Router log denotes the gateway of my ISP which is shown to be the place where most flood attacks seem to come from according to the router!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-syn-ack" rel="tag" title="see questions tagged &#39;syn-ack&#39;">syn-ack</span> <span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span> <span class="post-tag tag-link-ip-flooding" rel="tag" title="see questions tagged &#39;ip-flooding&#39;">ip-flooding</span> <span class="post-tag tag-link-udpchargen" rel="tag" title="see questions tagged &#39;udpchargen&#39;">udpchargen</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '16, 06:33</strong></p><img src="https://secure.gravatar.com/avatar/bb2e12dc42ef31ad84bd2c0eef4b681a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aritra_B&#39;s gravatar image" /><p><span>Aritra_B</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aritra_B has no accepted answers">0%</span></p></div></div><div id="comments-container-56690" class="comments-container"><span id="56692"></span><div id="comment-56692" class="comment"><div id="post-56692-score" class="comment-score"></div><div class="comment-text"><p>What's the model of your router?</p></div><div id="comment-56692-info" class="comment-info"><span class="comment-age">(26 Oct '16, 06:44)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="56694"></span><div id="comment-56694" class="comment"><div id="post-56694-score" class="comment-score"></div><div class="comment-text"><p>It's DLink DIR-600L,a tad bit old of a model but the firmware is upgraded as per Dlink's provided upgrade. You'll find most of the information in the Router Log files which have a preview so you can check without downloading them.</p></div><div id="comment-56694-info" class="comment-info"><span class="comment-age">(26 Oct '16, 06:49)</span> <span class="comment-user userinfo">Aritra_B</span></div></div><span id="56695"></span><div id="comment-56695" class="comment"><div id="post-56695-score" class="comment-score"></div><div class="comment-text"><p>Oh, I forgot to mention that 172.16.23.67 is also an IP belonging to my ISP confirmed by the representative when I called them up to notify of these logs from the router. They did ask me to keep monitoring for another 24 Hours and then get back to them, so I took up this pain.</p></div><div id="comment-56695-info" class="comment-info"><span class="comment-age">(26 Oct '16, 06:51)</span> <span class="comment-user userinfo">Aritra_B</span></div></div></div><div id="comment-tools-56690" class="comment-tools"></div><div class="clear"></div><div id="comment-56690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

