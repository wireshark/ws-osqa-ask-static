+++
type = "question"
title = "I want to verify the netmask ,subnet,broadcast, my private ipaddress and public ip address from the packet information"
description = '''I want to verify the netmask ,subnet,broadcast, my private ipaddress and public ip address from the packet information.'''
date = "2014-08-18T22:28:00Z"
lastmod = "2014-08-18T22:46:00Z"
weight = 35548
keywords = [ "wireshark" ]
aliases = [ "/questions/35548" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I want to verify the netmask ,subnet,broadcast, my private ipaddress and public ip address from the packet information](/questions/35548/i-want-to-verify-the-netmask-subnetbroadcast-my-private-ipaddress-and-public-ip-address-from-the-packet-information)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35548-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35548-score" class="post-score" title="current number of votes">0</div><span id="post-35548-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to verify the netmask ,subnet,broadcast, my private ipaddress and public ip address from the packet information.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '14, 22:28</strong></p><img src="https://secure.gravatar.com/avatar/86d57fe7f63a4834a36e898372bf42d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="randy%20S&#39;s gravatar image" /><p><span>randy S</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="randy S has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Aug '14, 22:29</strong> </span></p></div></div><div id="comments-container-35548" class="comments-container"></div><div id="comment-tools-35548" class="comment-tools"></div><div class="clear"></div><div id="comment-35548-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35550"></span>

<div id="answer-container-35550" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35550-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35550-score" class="post-score" title="current number of votes">0</div><span id="post-35550-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't. Most of that information is not in a packet.</p><p>Depending on where you capture you'll either see the private or the public IP address (inside of a NAT or outside, respectively). The subnet mask is not transmitted as it has no relevance for the recipient, and that also means that you cannot determine broadcast or the net base address.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Aug '14, 22:46</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-35550" class="comments-container"></div><div id="comment-tools-35550" class="comment-tools"></div><div class="clear"></div><div id="comment-35550-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

