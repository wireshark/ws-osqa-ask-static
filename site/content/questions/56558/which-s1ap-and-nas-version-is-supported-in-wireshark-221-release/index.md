+++
type = "question"
title = "Which S1AP and NAS version is supported in wireshark 2.2.1 release?"
description = '''Need to know which S1AP and NAS version is supported in wire shark 2.2.1 release? I am trying to decode release 13 S1AP messages but the new parameters are not decoded.'''
date = "2016-10-21T03:21:00Z"
lastmod = "2016-10-21T03:32:00Z"
weight = 56558
keywords = [ "of", "version", "s1ap" ]
aliases = [ "/questions/56558" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Which S1AP and NAS version is supported in wireshark 2.2.1 release?](/questions/56558/which-s1ap-and-nas-version-is-supported-in-wireshark-221-release)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56558-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56558-score" class="post-score" title="current number of votes">0</div><span id="post-56558-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Need to know which S1AP and NAS version is supported in wire shark 2.2.1 release? I am trying to decode release 13 S1AP messages but the new parameters are not decoded.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span> <span class="post-tag tag-link-s1ap" rel="tag" title="see questions tagged &#39;s1ap&#39;">s1ap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '16, 03:21</strong></p><img src="https://secure.gravatar.com/avatar/cd3ae74129b983f361c2f856e0dd3f65?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sabyasachi&#39;s gravatar image" /><p><span>Sabyasachi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sabyasachi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Oct '16, 03:23</strong> </span></p></div></div><div id="comments-container-56558" class="comments-container"></div><div id="comment-tools-56558" class="comment-tools"></div><div class="clear"></div><div id="comment-56558-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56559"></span>

<div id="answer-container-56559" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56559-score" class="post-score" title="current number of votes">0</div><span id="post-56559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark 2.2.1 supports S1AP v9.2.0 and NAS-EPS v13.5.0.</p><p>Current Wireshark 2.3.0 development build supports S1AP v13.4.0 and NAS-EPS v13.7.0. You can grab builds <a href="https://www.wireshark.org/download/automated/">here</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '16, 03:32</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-56559" class="comments-container"></div><div id="comment-tools-56559" class="comment-tools"></div><div class="clear"></div><div id="comment-56559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

