+++
type = "question"
title = "[closed] Function VBGetSwfVer"
description = '''Hello.  I am looking through a pcap and I see this line. After the line it looks like commands for the .vba to follow. However I am confused as to what VBGetSwFVer means. I cant find a definate answer in google. any help would be appreciated.'''
date = "2017-06-08T23:08:00Z"
lastmod = "2017-06-09T02:00:00Z"
weight = 61887
keywords = [ "function", "vbscript" ]
aliases = [ "/questions/61887" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Function VBGetSwfVer](/questions/61887/function-vbgetswfver)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61887-score" class="post-score" title="current number of votes">0</div><span id="post-61887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.</p><p>I am looking through a pcap and I see this line. After the line it looks like commands for the .vba to follow. However I am confused as to what VBGetSwFVer means. I cant find a definate answer in google. any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-function" rel="tag" title="see questions tagged &#39;function&#39;">function</span> <span class="post-tag tag-link-vbscript" rel="tag" title="see questions tagged &#39;vbscript&#39;">vbscript</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '17, 23:08</strong></p><img src="https://secure.gravatar.com/avatar/23ec94e8d1b7d11564dceb074a60c9ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eldenmac99&#39;s gravatar image" /><p><span>eldenmac99</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eldenmac99 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>09 Jun '17, 02:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61887" class="comments-container"><span id="61891"></span><div id="comment-61891" class="comment"><div id="post-61891-score" class="comment-score"></div><div class="comment-text"><p>Not really a Wireshark question so off-topic I'm afraid.</p></div><div id="comment-61891-info" class="comment-info"><span class="comment-age">(09 Jun '17, 02:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61887" class="comment-tools"></div><div class="clear"></div><div id="comment-61887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 09 Jun '17, 02:00

</div>

</div>

</div>

