+++
type = "question"
title = "Why i can&#x27;t get steam ips through a call?"
description = '''what i&#x27;ve always being doing , it was open wireshark , my type of connection , then classicstun , i call the guy i want and the ip appears. but i can&#x27;t do it anymore because the guy has to accept the call. how to get the ip in an instant call?'''
date = "2014-10-31T17:20:00Z"
lastmod = "2014-11-02T10:59:00Z"
weight = 37517
keywords = [ "steam", "steamclient" ]
aliases = [ "/questions/37517" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why i can't get steam ips through a call?](/questions/37517/why-i-cant-get-steam-ips-through-a-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37517-score" class="post-score" title="current number of votes">0</div><span id="post-37517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what i've always being doing , it was open wireshark , my type of connection , then classicstun , i call the guy i want and the ip appears. but i can't do it anymore because the guy has to accept the call. how to get the ip in an instant call?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-steam" rel="tag" title="see questions tagged &#39;steam&#39;">steam</span> <span class="post-tag tag-link-steamclient" rel="tag" title="see questions tagged &#39;steamclient&#39;">steamclient</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '14, 17:20</strong></p><img src="https://secure.gravatar.com/avatar/3d32e029064c9e6f3d81443711d95102?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lunatake&#39;s gravatar image" /><p><span>lunatake</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lunatake has no accepted answers">0%</span></p></div></div><div id="comments-container-37517" class="comments-container"><span id="37535"></span><div id="comment-37535" class="comment"><div id="post-37535-score" class="comment-score"></div><div class="comment-text"><p>this looks like a <strong>duplicate</strong> of the following question:</p><blockquote><p><a href="https://ask.wireshark.org/questions/37324/how-to-get-ip-from-steam-and-error">https://ask.wireshark.org/questions/37324/how-to-get-ip-from-steam-and-error</a></p></blockquote></div><div id="comment-37535-info" class="comment-info"><span class="comment-age">(02 Nov '14, 04:20)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37517" class="comment-tools"></div><div class="clear"></div><div id="comment-37517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37519"></span>

<div id="answer-container-37519" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37519-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37519-score" class="post-score" title="current number of votes">0</div><span id="post-37519-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>they probably fixed the issue and also other security problems some info here: <a href="http://revuln.com/files/ReVuln_Steam_Voip_Security.pdf">http://revuln.com/files/ReVuln_Steam_Voip_Security.pdf</a></p><p>edit: i think that simply you can not do it anymore, they have patched it</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Nov '14, 01:42</strong></p><img src="https://secure.gravatar.com/avatar/cc7db0f2da4d16cd02a5741ec5cb8468?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dohuff&#39;s gravatar image" /><p><span>dohuff</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dohuff has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Nov '14, 06:54</strong> </span></p></div></div><div id="comments-container-37519" class="comments-container"><span id="37520"></span><div id="comment-37520" class="comment"><div id="post-37520-score" class="comment-score"></div><div class="comment-text"><p>so what should i do now? wait? or?</p></div><div id="comment-37520-info" class="comment-info"><span class="comment-age">(01 Nov '14, 03:09)</span> <span class="comment-user userinfo">lunatake</span></div></div><span id="37524"></span><div id="comment-37524" class="comment"><div id="post-37524-score" class="comment-score"></div><div class="comment-text"><p>so they definetily patched it? ,_,</p></div><div id="comment-37524-info" class="comment-info"><span class="comment-age">(01 Nov '14, 10:08)</span> <span class="comment-user userinfo">lunatake</span></div></div><span id="37545"></span><div id="comment-37545" class="comment"><div id="post-37545-score" class="comment-score"></div><div class="comment-text"><p>probably yes, as you see in the pdf they found multiple vulnerability related to voip and since they allow remote code execution i'm quite sure that is patched</p></div><div id="comment-37545-info" class="comment-info"><span class="comment-age">(02 Nov '14, 10:59)</span> <span class="comment-user userinfo">dohuff</span></div></div></div><div id="comment-tools-37519" class="comment-tools"></div><div class="clear"></div><div id="comment-37519-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

