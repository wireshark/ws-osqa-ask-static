+++
type = "question"
title = "Wireshark not capturing POST HTTP request from python/java program written in my system"
description = '''I want to capture the HTTP POST request sent from the programs the run on my system . I&#x27;m sending HTTP POST request from my Java/Python program to a remote URL on internet , But the wireshark is not able to capture it . Few other people too are facing the issue , take a look at this stackoverflow li...'''
date = "2015-04-11T06:51:00Z"
lastmod = "2015-04-11T07:34:00Z"
weight = 41382
keywords = [ "http.request" ]
aliases = [ "/questions/41382" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not capturing POST HTTP request from python/java program written in my system](/questions/41382/wireshark-not-capturing-post-http-request-from-pythonjava-program-written-in-my-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41382-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41382-score" class="post-score" title="current number of votes">0</div><span id="post-41382-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to capture the HTTP POST request sent from the programs the run on my system . I'm sending HTTP POST request from my Java/Python program to a remote URL on internet , But the wireshark is not able to capture it . Few other people too are facing the issue , take a look at this stackoverflow link <a href="http://stackoverflow.com/questions/28982568/wireshark-does-not-capture-python-post-request">http://stackoverflow.com/questions/28982568/wireshark-does-not-capture-python-post-request</a> . How to achieve it ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http.request" rel="tag" title="see questions tagged &#39;http.request&#39;">http.request</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '15, 06:51</strong></p><img src="https://secure.gravatar.com/avatar/9bb1ba8c124161e7fc72198d9dc08098?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krao&#39;s gravatar image" /><p><span>krao</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krao has no accepted answers">0%</span></p></div></div><div id="comments-container-41382" class="comments-container"><span id="41383"></span><div id="comment-41383" class="comment"><div id="post-41383-score" class="comment-score"></div><div class="comment-text"><p>If you know the IP address of the remote host try to filter on tha, ip.addr == If the standard http ports are not used wireshark might not recognise the traffic as http.</p></div><div id="comment-41383-info" class="comment-info"><span class="comment-age">(11 Apr '15, 07:34)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-41382" class="comment-tools"></div><div class="clear"></div><div id="comment-41382-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

