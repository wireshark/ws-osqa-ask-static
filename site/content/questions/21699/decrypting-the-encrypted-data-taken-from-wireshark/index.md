+++
type = "question"
title = "Decrypting the Encrypted Data taken from wireshark"
description = '''I&#x27;m using Perl Rijndael(Aes) module to decrypt the Data which is taken from Wireshark. The cipher suite used is TLS_DHE_RSA_AES_256_CBC_SHA. I want to do the same as like wireshark is decrypting the packets.I fed Client write key and Client IV which is taken from the wireshark debug logs to the Perl...'''
date = "2013-06-02T22:58:00Z"
lastmod = "2013-06-02T22:58:00Z"
weight = 21699
keywords = [ "tls_aes", "decryption", "perl", "openssl", "wireshark" ]
aliases = [ "/questions/21699" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypting the Encrypted Data taken from wireshark](/questions/21699/decrypting-the-encrypted-data-taken-from-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21699-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21699-score" class="post-score" title="current number of votes">0</div><span id="post-21699-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using Perl Rijndael(Aes) module to decrypt the Data which is taken from Wireshark. The cipher suite used is TLS_DHE_RSA_AES_256_CBC_SHA. I want to do the same as like wireshark is decrypting the packets.I fed Client write key and Client IV which is taken from the wireshark debug logs to the Perl module.First 16 bytes of the data are not decrypted properly Rest of the bytes are decrypted fine.I have taken the Client write key and IV from the wireshark debug logs.But still can't get the perfect output like wireshark.Where am I making mistake?Anything am I missing? Thanks in Advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tls_aes" rel="tag" title="see questions tagged &#39;tls_aes&#39;">tls_aes</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-perl" rel="tag" title="see questions tagged &#39;perl&#39;">perl</span> <span class="post-tag tag-link-openssl" rel="tag" title="see questions tagged &#39;openssl&#39;">openssl</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '13, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/3606fb2f161676306a345c0e2809e550?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kalai&#39;s gravatar image" /><p><span>Kalai</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kalai has no accepted answers">0%</span></p></div></div><div id="comments-container-21699" class="comments-container"></div><div id="comment-tools-21699" class="comment-tools"></div><div class="clear"></div><div id="comment-21699-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

