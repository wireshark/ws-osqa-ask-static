+++
type = "question"
title = "No hex when viewing packet in new window (Wireshark 2.0)"
description = '''I&#x27;m running Wireshark 2.0 on Windows 7 (64-bit). When I double-click a packet to show it in a new window, the new window does not show the hex data -- only the tree. The Wireshark Legacy interface still includes the hex data. Is the lack of hex data intentional, a bug, or just some configuration/opt...'''
date = "2015-12-04T16:00:00Z"
lastmod = "2015-12-04T16:47:00Z"
weight = 48275
keywords = [ "hex", "wireshark-2.0" ]
aliases = [ "/questions/48275" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [No hex when viewing packet in new window (Wireshark 2.0)](/questions/48275/no-hex-when-viewing-packet-in-new-window-wireshark-20)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48275-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48275-score" class="post-score" title="current number of votes">0</div><span id="post-48275-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm running Wireshark 2.0 on Windows 7 (64-bit). When I double-click a packet to show it in a new window, the new window does not show the hex data -- only the tree. The Wireshark Legacy interface still includes the hex data. Is the lack of hex data intentional, a bug, or just some configuration/option that I'm missing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hex" rel="tag" title="see questions tagged &#39;hex&#39;">hex</span> <span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '15, 16:00</strong></p><img src="https://secure.gravatar.com/avatar/302906ecd01da0af954ac548f5b355b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="V%20Capps&#39;s gravatar image" /><p><span>V Capps</span><br />
<span class="score" title="5 reputation points">5</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="V Capps has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Dec '15, 16:05</strong> </span></p></div></div><div id="comments-container-48275" class="comments-container"></div><div id="comment-tools-48275" class="comment-tools"></div><div class="clear"></div><div id="comment-48275-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48276"></span>

<div id="answer-container-48276" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48276-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48276-score" class="post-score" title="current number of votes">0</div><span id="post-48276-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="V Capps has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11760">bug 11760</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Dec '15, 16:47</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-48276" class="comments-container"></div><div id="comment-tools-48276" class="comment-tools"></div><div class="clear"></div><div id="comment-48276-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

