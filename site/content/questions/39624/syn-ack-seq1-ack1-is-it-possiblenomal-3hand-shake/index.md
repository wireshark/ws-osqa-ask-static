+++
type = "question"
title = "[SYN ACK] seq=1 ack=1 / Is it Possible??(Nomal 3hand shake)"
description = '''I need your help~ 10.1.1.1 -&amp;gt; 10.1.1.2 [SYN] seq=0 10.1.1.1 -&amp;gt; 10.1.1.2 [SYN] seq=0 &amp;gt;&amp;gt;&amp;gt; Retransmission 10.1.1.2 -&amp;gt; 10.1.1.1 [SYN/ACK] seq=0 ack=1 &amp;gt;&amp;gt;&amp;gt; Nomal tcp 3 hand shake syn/ack  10.1.1.2 -&amp;gt; 10.1.1.1 [SYN/ACK] seq=1 ack=1 &amp;lt;&amp;lt;&amp;lt;&amp;lt; what&#x27;s this? 10.1.1.1 -&amp;gt; ...'''
date = "2015-02-03T23:04:00Z"
lastmod = "2015-02-04T00:37:00Z"
weight = 39624
keywords = [ "ack", "1", "has", "syn", "seq" ]
aliases = [ "/questions/39624" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[SYN ACK\] seq=1 ack=1 / Is it Possible??(Nomal 3hand shake)](/questions/39624/syn-ack-seq1-ack1-is-it-possiblenomal-3hand-shake)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39624-score" class="post-score" title="current number of votes">0</div><span id="post-39624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need your help~<br />
</p><p>10.1.1.1 -&gt; 10.1.1.2 [SYN] seq=0<br />
10.1.1.1 -&gt; 10.1.1.2 [SYN] seq=0 &gt;&gt;&gt; Retransmission<br />
10.1.1.2 -&gt; 10.1.1.1 [SYN/ACK] seq=0 ack=1 &gt;&gt;&gt; Nomal tcp 3 hand shake syn/ack<br />
<strong>10.1.1.2 -&gt; 10.1.1.1 [SYN/ACK] seq=1 ack=1</strong> &lt;&lt;&lt;&lt; what's this?<br />
10.1.1.1 -&gt; 10.1.1.2 [ACK]<br />
10.1.1.1 -&gt; [RST/ACK] seq=1 ack=1<br />
10.1.1.1 -&gt; [RST] seq=1<br />
<br />
<br />
All packet has same tcp port number<br />
Have you ever seen syn/ack packet has seq=1 ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-1" rel="tag" title="see questions tagged &#39;1&#39;">1</span> <span class="post-tag tag-link-has" rel="tag" title="see questions tagged &#39;has&#39;">has</span> <span class="post-tag tag-link-syn" rel="tag" title="see questions tagged &#39;syn&#39;">syn</span> <span class="post-tag tag-link-seq" rel="tag" title="see questions tagged &#39;seq&#39;">seq</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '15, 23:04</strong></p><img src="https://secure.gravatar.com/avatar/4bc341f056e5d43ab11f691a97e0a3db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ju%20Hoon%20Cha&#39;s gravatar image" /><p><span>Ju Hoon Cha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ju Hoon Cha has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Feb '15, 16:18</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></br></p></div></div><div id="comments-container-39624" class="comments-container"></div><div id="comment-tools-39624" class="comment-tools"></div><div class="clear"></div><div id="comment-39624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39626"></span>

<div id="answer-container-39626" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39626-score" class="post-score" title="current number of votes">0</div><span id="post-39626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looks broken, but can't say for sure with relative sequence numbers. Things like this need to be diagnosed with absolute sequence numbers, no exceptions. You can disable relative sequence numbers in the TCP protocol preferences.</p><p>My guess is that 10.1.1.2 is doing something wrong, which would also explain the RST packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Feb '15, 00:37</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span> </br></br></p></div></div><div id="comments-container-39626" class="comments-container"></div><div id="comment-tools-39626" class="comment-tools"></div><div class="clear"></div><div id="comment-39626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

