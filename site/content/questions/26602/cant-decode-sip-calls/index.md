+++
type = "question"
title = "Can&#x27;t decode SIP calls"
description = '''I have captured several voip calls, but I can&#x27;t decode them using the telephony option to listen to them. Is there another way to listen to SIP calls? Thanks.'''
date = "2013-10-31T13:47:00Z"
lastmod = "2013-11-03T03:24:00Z"
weight = 26602
keywords = [ "decode", "sip" ]
aliases = [ "/questions/26602" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't decode SIP calls](/questions/26602/cant-decode-sip-calls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26602-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26602-score" class="post-score" title="current number of votes">0</div><span id="post-26602-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured several voip calls, but I can't decode them using the telephony option to listen to them. Is there another way to listen to SIP calls? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '13, 13:47</strong></p><img src="https://secure.gravatar.com/avatar/429dc3bd7dbaf943037a8f948b4c845d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HunterM4&#39;s gravatar image" /><p><span>HunterM4</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HunterM4 has no accepted answers">0%</span></p></div></div><div id="comments-container-26602" class="comments-container"></div><div id="comment-tools-26602" class="comment-tools"></div><div class="clear"></div><div id="comment-26602-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26634"></span>

<div id="answer-container-26634" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26634-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26634-score" class="post-score" title="current number of votes">0</div><span id="post-26634-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark only plays rtp streams using the G711 codec. Other codecs are proprietary and would need payment for inclusion in Wireshark.</p><p>The workaround is to save the RTP payload into a new file and then use an external player (with support for the used codec) to play the audio stream.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '13, 03:24</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-26634" class="comments-container"></div><div id="comment-tools-26634" class="comment-tools"></div><div class="clear"></div><div id="comment-26634-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

