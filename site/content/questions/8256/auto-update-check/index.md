+++
type = "question"
title = "Auto-update check"
description = '''Dear Wireshark team, Would it be possible to have an automatic update check in Wireshark? Beside making it easier for people to stay up-to-date, they can also be warned if the current version of Wireshark is really outdated or contain any security vulnerabilities. Especially on the Windows where no ...'''
date = "2012-01-06T09:42:00Z"
lastmod = "2012-01-08T10:52:00Z"
weight = 8256
keywords = [ "update", "automatically", "check" ]
aliases = [ "/questions/8256" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Auto-update check](/questions/8256/auto-update-check)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8256-score" class="post-score" title="current number of votes">0</div><span id="post-8256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Wireshark team,</p><p>Would it be possible to have an automatic update check in Wireshark?</p><p>Beside making it easier for people to stay up-to-date, they can also be warned if the current version of Wireshark is really outdated or contain any security vulnerabilities. Especially on the Windows where no package managers (and maintainers) are present, most users don't check regularly if they use a non-vulnerable version of some software.</p><p>Thanks for Wireshark, love the program! Hopefully my request will make it even better!</p><p>Michael</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-update" rel="tag" title="see questions tagged &#39;update&#39;">update</span> <span class="post-tag tag-link-automatically" rel="tag" title="see questions tagged &#39;automatically&#39;">automatically</span> <span class="post-tag tag-link-check" rel="tag" title="see questions tagged &#39;check&#39;">check</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '12, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/f1df8b3c3f3c16f7eb76c3e3f23d3d71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mboelen&#39;s gravatar image" /><p><span>mboelen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mboelen has no accepted answers">0%</span></p></div></div><div id="comments-container-8256" class="comments-container"></div><div id="comment-tools-8256" class="comment-tools"></div><div class="clear"></div><div id="comment-8256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="8257"></span>

<div id="answer-container-8257" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8257-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8257-score" class="post-score" title="current number of votes">1</div><span id="post-8257-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's already on the <a href="http://wiki.wireshark.org/WishList">WishList</a> (currently it's number 11 in the list). There has been discussion about it from time to time, but just no one has gotten around to doing it (or even deciding how it should be done?).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jan '12, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-8257" class="comments-container"><span id="8264"></span><div id="comment-8264" class="comment"><div id="post-8264-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jeff for the link, I couldn't find it except a Wiki entry. In my open source project I used a simple TXT record to check the latest version number. Other popular methods are downloading a small file from the webserver (or one of the mirrors) with data in it, or talk to a script like PHP which has the latest version available.</p></div><div id="comment-8264-info" class="comment-info"><span class="comment-age">(07 Jan '12, 04:35)</span> <span class="comment-user userinfo">mboelen</span></div></div></div><div id="comment-tools-8257" class="comment-tools"></div><div class="clear"></div><div id="comment-8257-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8276"></span>

<div id="answer-container-8276" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8276-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8276-score" class="post-score" title="current number of votes">1</div><span id="post-8276-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In the meantime you can <a href="https://www.wireshark.org/mailman/listinfo/wireshark-announce">subscribe</a> to the <a href="http://www.wireshark.org/lists/wireshark-announce/">Wireshark-announce Mailing List</a> or check the <a href="http://wiki.wireshark.org/Development/Roadmap">Roadmap</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '12, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span></p></div></div><div id="comments-container-8276" class="comments-container"></div><div id="comment-tools-8276" class="comment-tools"></div><div class="clear"></div><div id="comment-8276-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

