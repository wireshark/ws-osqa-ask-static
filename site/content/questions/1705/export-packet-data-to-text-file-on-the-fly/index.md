+++
type = "question"
title = "Export Packet Data to text file on the fly"
description = '''I know that you can export capture data to a plain text file, but I was wondering if there&#x27;s any way to limit that export scope to just the raw data part of the packet, and if you can apply that so that it actively outputs the data from an ongoing live capture to a text file? This is probably easily...'''
date = "2011-01-11T08:41:00Z"
lastmod = "2011-01-20T09:57:00Z"
weight = 1705
keywords = [ "export" ]
aliases = [ "/questions/1705" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Export Packet Data to text file on the fly](/questions/1705/export-packet-data-to-text-file-on-the-fly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1705-score" class="post-score" title="current number of votes">0</div><span id="post-1705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know that you can export capture data to a plain text file, but I was wondering if there's any way to limit that export scope to just the raw data part of the packet, and if you can apply that so that it actively outputs the data from an ongoing live capture to a text file?</p><p>This is probably easily set up, but I thought I'd ask if there's any quick and simple filter syntax or anything that could help me out.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '11, 08:41</strong></p><img src="https://secure.gravatar.com/avatar/a5a7e51fffd2a5be428b712e9f099ead?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Atticus&#39;s gravatar image" /><p><span>Atticus</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Atticus has no accepted answers">0%</span></p></div></div><div id="comments-container-1705" class="comments-container"></div><div id="comment-tools-1705" class="comment-tools"></div><div class="clear"></div><div id="comment-1705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1708"></span>

<div id="answer-container-1708" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1708-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1708-score" class="post-score" title="current number of votes">0</div><span id="post-1708-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at <a href="http://www.circlemud.org/~jelson/software/tcpflow/">tcpflow</a>, it's whole purpose in live is exporting the tcp data to file(s) :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '11, 15:13</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-1708" class="comments-container"><span id="1834"></span><div id="comment-1834" class="comment"><div id="post-1834-score" class="comment-score"></div><div class="comment-text"><p>Should probably have mentioned - this needs to be done in windows - any chance you could give a step-by-step for active, ongoing exportation of packet data to a readable text file?</p></div><div id="comment-1834-info" class="comment-info"><span class="comment-age">(20 Jan '11, 09:57)</span> <span class="comment-user userinfo">Atticus</span></div></div></div><div id="comment-tools-1708" class="comment-tools"></div><div class="clear"></div><div id="comment-1708-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

