+++
type = "question"
title = "Why doesn&#x27;t search work on ask.wireshark.org?"
description = '''The search for these &quot;forums&quot; seems to be broken. Can someone fix them? Is there a place to report that?'''
date = "2017-06-18T09:30:00Z"
lastmod = "2017-06-18T10:01:00Z"
weight = 62101
keywords = [ "information" ]
aliases = [ "/questions/62101" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why doesn't search work on ask.wireshark.org?](/questions/62101/why-doesnt-search-work-on-askwiresharkorg)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62101-score" class="post-score" title="current number of votes">0</div><span id="post-62101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The search for these "forums" seems to be broken. Can someone fix them? Is there a place to report that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-information" rel="tag" title="see questions tagged &#39;information&#39;">information</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '17, 09:30</strong></p><img src="https://secure.gravatar.com/avatar/f22576ed1ac15ef6bdfef42fd9e3b085?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rascal&#39;s gravatar image" /><p><span>rascal</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rascal has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jun '17, 13:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-62101" class="comments-container"></div><div id="comment-tools-62101" class="comment-tools"></div><div class="clear"></div><div id="comment-62101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62103"></span>

<div id="answer-container-62103" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62103-score" class="post-score" title="current number of votes">1</div><span id="post-62103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It seems that it's better to use Google.</p><p>For example, see <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7223">this bug report</a></p><p>Use 'site:ask.wireshark.org' as one of the Google search terms to limit the search to just the Wireshark Q&amp;A site.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '17, 10:01</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jun '17, 10:06</strong> </span></p></div></div><div id="comments-container-62103" class="comments-container"></div><div id="comment-tools-62103" class="comment-tools"></div><div class="clear"></div><div id="comment-62103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

