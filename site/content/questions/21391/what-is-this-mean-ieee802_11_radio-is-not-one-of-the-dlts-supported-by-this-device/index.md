+++
type = "question"
title = "What is this mean? IEEE802_11_RADIO is not one of the DLTs supported by this device"
description = '''Hi, I put this command on terminal also shown the result below. Does it mean that even though I change into monitor mode, it would still impossible to get radio information using tcpdump? [root@dhcp7154 ~]# tcpdump -i ath0 -y IEEE802_11_RADIO tcpdump: IEEE802_11_RADIO is not one of the DLTs supporte...'''
date = "2013-05-22T22:36:00Z"
lastmod = "2013-05-22T22:36:00Z"
weight = 21391
keywords = [ "sinr", "tcpdump" ]
aliases = [ "/questions/21391" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What is this mean? IEEE802\_11\_RADIO is not one of the DLTs supported by this device](/questions/21391/what-is-this-mean-ieee802_11_radio-is-not-one-of-the-dlts-supported-by-this-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21391-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21391-score" class="post-score" title="current number of votes">0</div><span id="post-21391-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I put this command on terminal also shown the result below. Does it mean that even though I change into monitor mode, it would still impossible to get radio information using tcpdump?</p><pre><code>[[email protected] ~]# tcpdump -i ath0 -y IEEE802_11_RADIO
tcpdump: IEEE802_11_RADIO is not one of the DLTs supported by this device
[[email protected] ~]# tcpdump -i ath0 -y IEEE802_11_RADIO_AVS
tcpdump: IEEE802_11_RADIO_AVS is not one of the DLTs supported by this device</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sinr" rel="tag" title="see questions tagged &#39;sinr&#39;">sinr</span> <span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '13, 22:36</strong></p><img src="https://secure.gravatar.com/avatar/a3291ae3aa2fd3100059945d1afa121c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tyanium&#39;s gravatar image" /><p><span>Tyanium</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tyanium has no accepted answers">0%</span></p></div></div><div id="comments-container-21391" class="comments-container"></div><div id="comment-tools-21391" class="comment-tools"></div><div class="clear"></div><div id="comment-21391-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

