+++
type = "question"
title = "wireshark for ppc g3 (pismo), os x 10.4.11. gcc 4.0.1 (xcode2.5)"
description = '''I&#x27;d like to install wireshark on a ppc g3 (pismo), os x 10.4.11, gcc 4.0.1 (xcode2.5). Is source code and a build description available for this legacy machine?'''
date = "2015-03-13T19:37:00Z"
lastmod = "2015-03-13T19:37:00Z"
weight = 40549
keywords = [ "ppc", "osx", "g3", "10.4.11" ]
aliases = [ "/questions/40549" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark for ppc g3 (pismo), os x 10.4.11. gcc 4.0.1 (xcode2.5)](/questions/40549/wireshark-for-ppc-g3-pismo-os-x-10411-gcc-401-xcode25)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40549-score" class="post-score" title="current number of votes">0</div><span id="post-40549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'd like to install wireshark on a ppc g3 (pismo), os x 10.4.11, gcc 4.0.1 (xcode2.5). Is source code and a build description available for this legacy machine?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ppc" rel="tag" title="see questions tagged &#39;ppc&#39;">ppc</span> <span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-g3" rel="tag" title="see questions tagged &#39;g3&#39;">g3</span> <span class="post-tag tag-link-10.4.11" rel="tag" title="see questions tagged &#39;10.4.11&#39;">10.4.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '15, 19:37</strong></p><img src="https://secure.gravatar.com/avatar/1cd0b08e4f8a0f2570b6f9182ee9fad1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="desmond&#39;s gravatar image" /><p><span>desmond</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="desmond has no accepted answers">0%</span></p></div></div><div id="comments-container-40549" class="comments-container"></div><div id="comment-tools-40549" class="comment-tools"></div><div class="clear"></div><div id="comment-40549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

