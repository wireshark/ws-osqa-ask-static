+++
type = "question"
title = "Wireshark dissector for decoding GTP Prime (CDR)"
description = '''I am using Wireshark Version 1.6.4 (SVN Rev 39941 from /trunk-1.6). I would like to decode GTP Prime (CDR) messages as per the ASN.1 syntax specified in 3GPP Specification 32.298.  This feature is already available in Wireshark?. If yes, please suggest how to add/install the required plugin/dissecto...'''
date = "2012-01-19T17:52:00Z"
lastmod = "2012-05-29T23:09:00Z"
weight = 8486
keywords = [ "gtp", "prime", "3gpp", "32.298", "cdr" ]
aliases = [ "/questions/8486" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark dissector for decoding GTP Prime (CDR)](/questions/8486/wireshark-dissector-for-decoding-gtp-prime-cdr)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8486-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8486-score" class="post-score" title="current number of votes">0</div><span id="post-8486-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Wireshark Version 1.6.4 (SVN Rev 39941 from /trunk-1.6).</p><p>I would like to decode GTP Prime (CDR) messages as per the ASN.1 syntax specified in 3GPP Specification 32.298.</p><p>This feature is already available in Wireshark?. If yes, please suggest how to add/install the required plugin/dissector.</p><p>Thanks Raja</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtp" rel="tag" title="see questions tagged &#39;gtp&#39;">gtp</span> <span class="post-tag tag-link-prime" rel="tag" title="see questions tagged &#39;prime&#39;">prime</span> <span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-32.298" rel="tag" title="see questions tagged &#39;32.298&#39;">32.298</span> <span class="post-tag tag-link-cdr" rel="tag" title="see questions tagged &#39;cdr&#39;">cdr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '12, 17:52</strong></p><img src="https://secure.gravatar.com/avatar/736cad47ac2ce138eabe249cd2ef8a78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rajathi%20Raja&#39;s gravatar image" /><p><span>Rajathi Raja</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rajathi Raja has no accepted answers">0%</span></p></div></div><div id="comments-container-8486" class="comments-container"></div><div id="comment-tools-8486" class="comment-tools"></div><div class="clear"></div><div id="comment-8486-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8498"></span>

<div id="answer-container-8498" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8498-score" class="post-score" title="current number of votes">0</div><span id="post-8498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's available in the development version of Wireshark. For windows you can download an installer from the automated builds section.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '12, 03:08</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-8498" class="comments-container"><span id="8511"></span><div id="comment-8511" class="comment"><div id="post-8511-score" class="comment-score"></div><div class="comment-text"><p>Thanks Anders. I downloaded development version (1.7.0) of wireshark (Windows 64bit). But still I am not able to get GTP prime decoded. Can you please suggest if I am missing anything?. Thanks.</p></div><div id="comment-8511-info" class="comment-info"><span class="comment-age">(20 Jan '12, 05:19)</span> <span class="comment-user userinfo">Rajathi Raja</span></div></div><span id="8531"></span><div id="comment-8531" class="comment"><div id="post-8531-score" class="comment-score"></div><div class="comment-text"><p>GTP' is not detected or the CDR is not dissected?</p></div><div id="comment-8531-info" class="comment-info"><span class="comment-age">(21 Jan '12, 04:24)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="11464"></span><div id="comment-11464" class="comment"><div id="post-11464-score" class="comment-score"></div><div class="comment-text"><p>how to do CDR dissection?</p></div><div id="comment-11464-info" class="comment-info"><span class="comment-age">(29 May '12, 23:09)</span> <span class="comment-user userinfo">nanzone</span></div></div></div><div id="comment-tools-8498" class="comment-tools"></div><div class="clear"></div><div id="comment-8498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

