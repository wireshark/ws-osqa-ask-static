+++
type = "question"
title = "VoIP monitoring using ws 2.0"
description = '''Hi there, Any one can please assist in monitoring VoIP calls via Wireshark? Steps provided would be most suitable..'''
date = "2016-02-09T16:31:00Z"
lastmod = "2016-02-09T22:42:00Z"
weight = 50032
keywords = [ "voip" ]
aliases = [ "/questions/50032" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP monitoring using ws 2.0](/questions/50032/voip-monitoring-using-ws-20)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50032-score" class="post-score" title="current number of votes">0</div><span id="post-50032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there, Any one can please assist in monitoring VoIP calls via Wireshark?</p><p>Steps provided would be most suitable..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '16, 16:31</strong></p><img src="https://secure.gravatar.com/avatar/ee11fdfb44b749c6ec64a32fb46e9b19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="REDX&#39;s gravatar image" /><p><span>REDX</span><br />
<span class="score" title="5 reputation points">5</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="REDX has no accepted answers">0%</span></p></div></div><div id="comments-container-50032" class="comments-container"></div><div id="comment-tools-50032" class="comment-tools"></div><div class="clear"></div><div id="comment-50032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50040"></span>

<div id="answer-container-50040" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50040-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50040-score" class="post-score" title="current number of votes">0</div><span id="post-50040-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Dive into the <a href="https://www.wireshark.org/docs/">SharkFest presentations</a>, there are some which go into this subject.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '16, 22:42</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-50040" class="comments-container"></div><div id="comment-tools-50040" class="comment-tools"></div><div class="clear"></div><div id="comment-50040-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

