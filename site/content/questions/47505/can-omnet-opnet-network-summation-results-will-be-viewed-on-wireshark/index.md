+++
type = "question"
title = "Can Omnet++ / / OPNET network summation results will be viewed on wireshark"
description = '''Can Omnet++ / / OPNET network summation results will be viewed on wireshark '''
date = "2015-11-10T23:02:00Z"
lastmod = "2015-11-10T23:02:00Z"
weight = 47505
keywords = [ "pcap" ]
aliases = [ "/questions/47505" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can Omnet++ / / OPNET network summation results will be viewed on wireshark](/questions/47505/can-omnet-opnet-network-summation-results-will-be-viewed-on-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47505-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47505-score" class="post-score" title="current number of votes">0</div><span id="post-47505-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can Omnet++ / / OPNET network summation results will be viewed on wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '15, 23:02</strong></p><img src="https://secure.gravatar.com/avatar/ce1843f92a1c18db26bc79b3afa9bd50?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srinu_bel&#39;s gravatar image" /><p><span>srinu_bel</span><br />
<span class="score" title="20 reputation points">20</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srinu_bel has no accepted answers">0%</span></p></div></div><div id="comments-container-47505" class="comments-container"></div><div id="comment-tools-47505" class="comment-tools"></div><div class="clear"></div><div id="comment-47505-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

