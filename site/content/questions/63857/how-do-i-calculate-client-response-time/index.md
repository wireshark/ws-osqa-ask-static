+++
type = "question"
title = "How do I calculate Client Response Time"
description = '''I have loaded a file consisting of 100,000 NFS v.3 LOOKUP calls. If I go to Statistics | Service Response Time | ONC-RPC , select NFS v.3 and click on Apply, I get a table of Min, Max, Avg and Sum SRT. This I presume tells me those statistics for how quickly the NFS server is responding to the calls...'''
date = "2017-10-12T15:44:00Z"
lastmod = "2017-10-12T15:44:00Z"
weight = 63857
keywords = [ "statistics", "response", "client" ]
aliases = [ "/questions/63857" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I calculate Client Response Time](/questions/63857/how-do-i-calculate-client-response-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63857-score" class="post-score" title="current number of votes">0</div><span id="post-63857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have loaded a file consisting of 100,000 NFS v.3 LOOKUP calls. If I go to Statistics | Service Response Time | ONC-RPC , select NFS v.3 and click on Apply, I get a table of Min, Max, Avg and Sum SRT. This I presume tells me those statistics for how quickly the NFS server is responding to the calls from the client. What I would like to know, however, is how quickly the client responds with the next call once the server responds to the previous one. Is that information available and how can I get it? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-client" rel="tag" title="see questions tagged &#39;client&#39;">client</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '17, 15:44</strong></p><img src="https://secure.gravatar.com/avatar/460793653571780e7e6b96c93889b646?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RonWFox&#39;s gravatar image" /><p><span>RonWFox</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RonWFox has no accepted answers">0%</span></p></div></div><div id="comments-container-63857" class="comments-container"></div><div id="comment-tools-63857" class="comment-tools"></div><div class="clear"></div><div id="comment-63857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

