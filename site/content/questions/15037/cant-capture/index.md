+++
type = "question"
title = "can&#x27;t capture"
description = '''Why i cant capture any packages after i start my interface &quot;Atheros L1C&quot;?'''
date = "2012-10-16T12:47:00Z"
lastmod = "2012-10-16T12:47:00Z"
weight = 15037
keywords = [ "capture" ]
aliases = [ "/questions/15037" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can't capture](/questions/15037/cant-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15037-score" class="post-score" title="current number of votes">0</div><span id="post-15037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why i cant capture any packages after i start my interface "Atheros L1C"?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Oct '12, 12:47</strong></p><img src="https://secure.gravatar.com/avatar/61bf70e58b57c640a447a7a75a503a4e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rbnaraujo&#39;s gravatar image" /><p><span>rbnaraujo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rbnaraujo has no accepted answers">0%</span></p></div></div><div id="comments-container-15037" class="comments-container"></div><div id="comment-tools-15037" class="comment-tools"></div><div class="clear"></div><div id="comment-15037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

