+++
type = "question"
title = "WireShark is not capturing HTTP traffic from other uses in the same LAN"
description = '''Hello, It might not be possible but I would like to see HTTP traffic in my LAN from all users. Everyone is connected via cable. Thanks, T'''
date = "2015-03-12T11:15:00Z"
lastmod = "2015-03-12T11:29:00Z"
weight = 40517
keywords = [ "lan", "http" ]
aliases = [ "/questions/40517" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark is not capturing HTTP traffic from other uses in the same LAN](/questions/40517/wireshark-is-not-capturing-http-traffic-from-other-uses-in-the-same-lan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40517-score" class="post-score" title="current number of votes">0</div><span id="post-40517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>It might not be possible but I would like to see HTTP traffic in my LAN from all users. Everyone is connected via cable.</p><p>Thanks, T</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '15, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/60b14841d529fdb9c372805e78427032?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zibrnp&#39;s gravatar image" /><p><span>zibrnp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zibrnp has no accepted answers">0%</span></p></div></div><div id="comments-container-40517" class="comments-container"></div><div id="comment-tools-40517" class="comment-tools"></div><div class="clear"></div><div id="comment-40517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40518"></span>

<div id="answer-container-40518" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40518-score" class="post-score" title="current number of votes">1</div><span id="post-40518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on <a href="https://wiki.wireshark.org/CaptureSetup">Capture Setup</a>.</p><p>Basically you'll need to arrange to capture at a point where all the traffic flows though, usually your edge router\firewall.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '15, 11:29</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Mar '15, 11:29</strong> </span></p></div></div><div id="comments-container-40518" class="comments-container"></div><div id="comment-tools-40518" class="comment-tools"></div><div class="clear"></div><div id="comment-40518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

