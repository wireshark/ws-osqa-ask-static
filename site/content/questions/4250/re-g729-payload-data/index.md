+++
type = "question"
title = "RE: G729 payload data"
description = '''Hi, I have capture a pcap through wire-shark , which contain data of G729 payload type . Now I saved it in .raw file and decode it in .pcm using the decoder and finally got the .au file . But when I am playing that file it is generating noise. Plz tell me why it is generating noise.'''
date = "2011-05-27T00:32:00Z"
lastmod = "2011-05-27T02:45:00Z"
weight = 4250
keywords = [ "g729" ]
aliases = [ "/questions/4250" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RE: G729 payload data](/questions/4250/re-g729-payload-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4250-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4250-score" class="post-score" title="current number of votes">0</div><span id="post-4250-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have capture a pcap through wire-shark , which contain data of G729 payload type . Now I saved it in .raw file and decode it in .pcm using the decoder and finally got the .au file . But when I am playing that file it is generating noise. Plz tell me why it is generating noise.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-g729" rel="tag" title="see questions tagged &#39;g729&#39;">g729</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '11, 00:32</strong></p><img src="https://secure.gravatar.com/avatar/610c6b9d224bfd9378166e6fb506c7e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rahulgupta&#39;s gravatar image" /><p><span>rahulgupta</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rahulgupta has no accepted answers">0%</span></p></div></div><div id="comments-container-4250" class="comments-container"><span id="4251"></span><div id="comment-4251" class="comment"><div id="post-4251-score" class="comment-score"></div><div class="comment-text"><p>hi, i hv some questions to ask, how to save as raw file, i dint see any .raw extension der. and how to decode it, hv u written any code for decoding if so den tel me how to write decoder for the captured data.</p></div><div id="comment-4251-info" class="comment-info"><span class="comment-age">(27 May '11, 00:37)</span> <span class="comment-user userinfo">sagu072</span></div></div></div><div id="comment-tools-4250" class="comment-tools"></div><div class="clear"></div><div id="comment-4250-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4253"></span>

<div id="answer-container-4253" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4253-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4253-score" class="post-score" title="current number of votes">0</div><span id="post-4253-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Probably it depends on the G.729 converter you are using. I have a lot of problems with Asterisk file convert function and free Intel g729 lib. If the noise is small then it could be the comfort noise. Have you compered the end-to-end quality and captured quality?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '11, 02:45</strong></p><img src="https://secure.gravatar.com/avatar/2bb36804afb054782c66c3ad2d8690f6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jakan&#39;s gravatar image" /><p><span>jakan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jakan has no accepted answers">0%</span></p></div></div><div id="comments-container-4253" class="comments-container"></div><div id="comment-tools-4253" class="comment-tools"></div><div class="clear"></div><div id="comment-4253-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

