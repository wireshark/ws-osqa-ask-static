+++
type = "question"
title = "wireshark certified network instructor"
description = '''Does anyone know how to become wireshark certified network instructor ?'''
date = "2017-03-19T03:58:00Z"
lastmod = "2017-03-19T03:58:00Z"
weight = 60171
keywords = [ "wcni" ]
aliases = [ "/questions/60171" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark certified network instructor](/questions/60171/wireshark-certified-network-instructor)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60171-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60171-score" class="post-score" title="current number of votes">0</div><span id="post-60171-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know how to become wireshark certified network instructor ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wcni" rel="tag" title="see questions tagged &#39;wcni&#39;">wcni</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '17, 03:58</strong></p><img src="https://secure.gravatar.com/avatar/eac75eef24254c1c9ee690951f6c4006?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thierryn&#39;s gravatar image" /><p><span>thierryn</span><br />
<span class="score" title="21 reputation points">21</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thierryn has no accepted answers">0%</span></p></div></div><div id="comments-container-60171" class="comments-container"></div><div id="comment-tools-60171" class="comment-tools"></div><div class="clear"></div><div id="comment-60171-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

