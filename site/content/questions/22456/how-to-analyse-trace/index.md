+++
type = "question"
title = "how to analyse trace"
description = '''Dear all, Kindly help me for below question. As im new to wireshark,please tell me how to analyse SMSC trace I have complete .pcap file of one successful SMS trace how to find each and every msges which is sending my SMSC &amp;amp; receiving from that trace. Thanks &amp;amp; regards, Mahesh'''
date = "2013-06-28T10:06:00Z"
lastmod = "2013-06-28T10:14:00Z"
weight = 22456
keywords = [ "wireshark" ]
aliases = [ "/questions/22456" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to analyse trace](/questions/22456/how-to-analyse-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22456-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22456-score" class="post-score" title="current number of votes">0</div><span id="post-22456-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all,</p><p>Kindly help me for below question.</p><p>As im new to wireshark,please tell me how to analyse SMSC trace I have complete .pcap file of one successful SMS trace how to find each and every msges which is sending my SMSC &amp; receiving from that trace.</p><p>Thanks &amp; regards, Mahesh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '13, 10:06</strong></p><img src="https://secure.gravatar.com/avatar/f61c15586f314f9bf5f656495da4bd5f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mahesh2454&#39;s gravatar image" /><p><span>Mahesh2454</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mahesh2454 has no accepted answers">0%</span></p></div></div><div id="comments-container-22456" class="comments-container"><span id="22457"></span><div id="comment-22457" class="comment"><div id="post-22457-score" class="comment-score"></div><div class="comment-text"><p>If possible,Please upload your trace to cloudshark so that people here will get a feel of the protocol/packets/messages you want to analyze.</p></div><div id="comment-22457-info" class="comment-info"><span class="comment-age">(28 Jun '13, 10:14)</span> <span class="comment-user userinfo">krishnayeddula</span></div></div></div><div id="comment-tools-22456" class="comment-tools"></div><div class="clear"></div><div id="comment-22456-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

