+++
type = "question"
title = "Wireshark vulnerabilites"
description = '''When my security group rejected offering wireshark within my company they stated the following issues:  1. Denial of service issues This is version 1.8.6 Any thoughts as to how convince my security group to allow us access to wire'''
date = "2013-06-18T08:30:00Z"
lastmod = "2013-06-18T21:04:00Z"
weight = 22133
keywords = [ "vulnerabilitiy", "issues" ]
aliases = [ "/questions/22133" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark vulnerabilites](/questions/22133/wireshark-vulnerabilites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22133-score" class="post-score" title="current number of votes">0</div><span id="post-22133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When my security group rejected offering wireshark within my company they stated the following issues: 1. Denial of service issues</p><p>This is version 1.8.6</p><p>Any thoughts as to how convince my security group to allow us access to wire</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vulnerabilitiy" rel="tag" title="see questions tagged &#39;vulnerabilitiy&#39;">vulnerabilitiy</span> <span class="post-tag tag-link-issues" rel="tag" title="see questions tagged &#39;issues&#39;">issues</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '13, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/47c68298bc679de204dc694c0a73dc29?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rich_Warren&#39;s gravatar image" /><p><span>Rich_Warren</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rich_Warren has no accepted answers">0%</span></p></div></div><div id="comments-container-22133" class="comments-container"></div><div id="comment-tools-22133" class="comment-tools"></div><div class="clear"></div><div id="comment-22133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="22137"></span>

<div id="answer-container-22137" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22137-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22137-score" class="post-score" title="current number of votes">3</div><span id="post-22137-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is vulnerable to crashing from either mal-formed packets (whether malicious or not), or even well-formed packets due to bugs in the Wireshark code. But the only service that would be denied would be Wireshark itself, and then only on that 1 computer/device doing the capturing. Denial of service? I suppose.</p><p>Look, there's risk involved with using just about any tool, but you have to weigh the benefits of using that tool vs. the risks involved with doing so. Do you realize that using a chainsaw to cut down a tree can be quite dangerous? I think it would be far safer to cut it down with a butter knife, don't you? To me, there's no question that the rewards greatly outweigh the risks ... in both cases. The alternatives are just not practical.</p><p>Good luck.</p><p>P.S. See also: <a href="http://wiki.wireshark.org/Security">http://wiki.wireshark.org/Security</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '13, 10:25</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jun '13, 10:29</strong> </span></p></div></div><div id="comments-container-22137" class="comments-container"></div><div id="comment-tools-22137" class="comment-tools"></div><div class="clear"></div><div id="comment-22137-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="22138"></span>

<div id="answer-container-22138" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22138-score" class="post-score" title="current number of votes">1</div><span id="post-22138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a <strong>passive</strong> network analysis tool. It will not send any data into the network (except some DNS queries if you enabled name resolution). So there is <strong>no way</strong> how Wireshark could be involved in any denial of service issue. I'm sorry to say that, but your security group has either no idea how Wireshark works or no idea how a denial of service attack works. In either case they should either give more information about their concerns (what kind of denial of service are they talking) or allow you to use Wireshark, <strong>if you have a legitimate use</strong> for Wireshark ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '13, 10:27</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jun '13, 10:29</strong> </span></p></div></div><div id="comments-container-22138" class="comments-container"></div><div id="comment-tools-22138" class="comment-tools"></div><div class="clear"></div><div id="comment-22138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="22144"></span>

<div id="answer-container-22144" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22144-score" class="post-score" title="current number of votes">1</div><span id="post-22144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think the best approach here is to ask them why they view it as a potential DOS concern. Really we're just speculating on what their reason is for that assessment and coming up with a counter to straw man arguments at this point without more details on their reasoning.</p><p>For a security group viewpoint, I'd be more worried about potential for reconnaissance, leading to some other form of attack, which Wireshark can definitely help with. Unless they're talking about the potential for DNS PTR query flooding, which is configurable, I don't see other potential here for Wireshark-based DOS.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '13, 21:04</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-22144" class="comments-container"></div><div id="comment-tools-22144" class="comment-tools"></div><div class="clear"></div><div id="comment-22144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

