+++
type = "question"
title = "Wireshark for REDHAT enterprise 5/6"
description = '''Hi, I cant find an updated Wireshark version for linux REDHAT enterprise 5/6. Do you know how can I get such version? Thanks, Aya'''
date = "2017-06-12T08:16:00Z"
lastmod = "2017-06-12T13:56:00Z"
weight = 61951
keywords = [ "redhat", "linux" ]
aliases = [ "/questions/61951" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark for REDHAT enterprise 5/6](/questions/61951/wireshark-for-redhat-enterprise-56)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61951-score" class="post-score" title="current number of votes">0</div><span id="post-61951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I cant find an updated Wireshark version for linux REDHAT enterprise 5/6. Do you know how can I get such version?</p><p>Thanks, Aya</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-redhat" rel="tag" title="see questions tagged &#39;redhat&#39;">redhat</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '17, 08:16</strong></p><img src="https://secure.gravatar.com/avatar/3cca087c83f55798a15e19db6111ce67?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aya%20dagan&#39;s gravatar image" /><p><span>aya dagan</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aya dagan has no accepted answers">0%</span></p></div></div><div id="comments-container-61951" class="comments-container"></div><div id="comment-tools-61951" class="comment-tools"></div><div class="clear"></div><div id="comment-61951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61960"></span>

<div id="answer-container-61960" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61960-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61960-score" class="post-score" title="current number of votes">0</div><span id="post-61960-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should be able to get a (very old) version by simply running <code>yum install wireshark-gnome</code>.</p><p>If you want something newer than what RH provides you can try enabling the EPEL repository (which may have something newer). If you want something even more modern you'd have to compile it yourself. Note, though, that Wireshark 1.6 is the last version that will compile (easily) on RHEL 5.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jun '17, 13:56</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-61960" class="comments-container"></div><div id="comment-tools-61960" class="comment-tools"></div><div class="clear"></div><div id="comment-61960-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

