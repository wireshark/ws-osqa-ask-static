+++
type = "question"
title = "How to export decrypted data to a text file"
description = '''I would like to export the decrypted data from certain packets in my pcap file to a text file (preferably csv file). I know that right now tshark does not support decryption. How do I use wireshark to save decrypted data. I tried &quot;export PDU&quot;&amp;gt;&quot;dlt USer&quot; option but the new file that gets loaded in...'''
date = "2016-01-17T13:35:00Z"
lastmod = "2016-01-17T13:48:00Z"
weight = 49306
keywords = [ "decryption", "decrypt" ]
aliases = [ "/questions/49306" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to export decrypted data to a text file](/questions/49306/how-to-export-decrypted-data-to-a-text-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49306-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49306-score" class="post-score" title="current number of votes">0</div><span id="post-49306-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to export the decrypted data from certain packets in my pcap file to a text file (preferably csv file).</p><p>I know that right now tshark does not support decryption. How do I use wireshark to save decrypted data. I tried "export PDU"&gt;"dlt USer" option but the new file that gets loaded in wireshark is empty. Not quite sure what is going on.</p><p>Is there any other option?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jan '16, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/2d3358e5b4a4507b418f0f015c5377bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hvs&#39;s gravatar image" /><p><span>hvs</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hvs has no accepted answers">0%</span></p></div></div><div id="comments-container-49306" class="comments-container"></div><div id="comment-tools-49306" class="comment-tools"></div><div class="clear"></div><div id="comment-49306-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49308"></span>

<div id="answer-container-49308" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49308-score" class="post-score" title="current number of votes">0</div><span id="post-49308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Read carefully (till the very last comment) the answer thread of <a href="https://ask.wireshark.org/questions/23606/decrypting-browser-https-wrapped-into-stunnel-ssl">this older question</a>. You won't get csv but you will get the decrypted data in xml.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jan '16, 13:48</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-49308" class="comments-container"></div><div id="comment-tools-49308" class="comment-tools"></div><div class="clear"></div><div id="comment-49308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

