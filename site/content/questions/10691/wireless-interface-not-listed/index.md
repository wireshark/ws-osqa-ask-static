+++
type = "question"
title = "Wireless Interface not listed"
description = '''Hello, I&#x27;m using the 64-bit version of Wireshark on a Windows 7 laptop and in the program when I go to Capture-&amp;gt;Interfaces, it does not show the wireless adapter of the laptop. It shows the Ethernet card of the laptop but not the wireless.  Is there a way to add the wireless card to the interface...'''
date = "2012-05-04T20:46:00Z"
lastmod = "2012-05-05T03:05:00Z"
weight = 10691
keywords = [ "wireless", "wlan" ]
aliases = [ "/questions/10691" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless Interface not listed](/questions/10691/wireless-interface-not-listed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10691-score" class="post-score" title="current number of votes">0</div><span id="post-10691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm using the 64-bit version of Wireshark on a Windows 7 laptop and in the program when I go to Capture-&gt;Interfaces, it does not show the wireless adapter of the laptop. It shows the Ethernet card of the laptop but not the wireless.</p><p>Is there a way to add the wireless card to the interface list?</p><p>I primarily use the laptop with a wireless network and so was under the impression that monitoring the wireless network should be possible.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '12, 20:46</strong></p><img src="https://secure.gravatar.com/avatar/d7d8cd44b1ac750d7d9b732ea85396bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ekartik82&#39;s gravatar image" /><p><span>ekartik82</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ekartik82 has no accepted answers">0%</span></p></div></div><div id="comments-container-10691" class="comments-container"></div><div id="comment-tools-10691" class="comment-tools"></div><div class="clear"></div><div id="comment-10691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="10692"></span>

<div id="answer-container-10692" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10692-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10692-score" class="post-score" title="current number of votes">0</div><span id="post-10692-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark relies on WinPCAP libraries to detect interfaces, and provide the capturing interface. As far as I know, if it isn't listed, the interface isn't supported. I would suggest find out the wireless chipset you have (use the Windows Device Manager), and research at <a href="http://www.winpcap.org/">http://www.winpcap.org/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 May '12, 20:57</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-10692" class="comments-container"></div><div id="comment-tools-10692" class="comment-tools"></div><div class="clear"></div><div id="comment-10692-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="10695"></span>

<div id="answer-container-10695" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10695-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10695-score" class="post-score" title="current number of votes">0</div><span id="post-10695-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Windows and wireless interfaces and captures are not happy bedfellows. See the <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN capture</a> page on the Wiki for more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '12, 03:05</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-10695" class="comments-container"></div><div id="comment-tools-10695" class="comment-tools"></div><div class="clear"></div><div id="comment-10695-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

