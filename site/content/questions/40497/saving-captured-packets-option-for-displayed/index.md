+++
type = "question"
title = "Saving captured packets option for displayed"
description = '''I have noted on latest version (1.12.4 and previous) it doesn&#x27;t have displayed option for Saving captured packets, these is useful feature. Thanks '''
date = "2015-03-11T17:42:00Z"
lastmod = "2015-03-12T05:44:00Z"
weight = 40497
keywords = [ "filesave" ]
aliases = [ "/questions/40497" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Saving captured packets option for displayed](/questions/40497/saving-captured-packets-option-for-displayed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40497-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40497-score" class="post-score" title="current number of votes">0</div><span id="post-40497-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have noted on latest version (1.12.4 and previous) it doesn't have displayed option for Saving captured packets, these is useful feature. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filesave" rel="tag" title="see questions tagged &#39;filesave&#39;">filesave</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '15, 17:42</strong></p><img src="https://secure.gravatar.com/avatar/764a5c5dcb5080cb55d6843fc7dfab22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ricardo&#39;s gravatar image" /><p><span>Ricardo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ricardo has no accepted answers">0%</span></p></div></div><div id="comments-container-40497" class="comments-container"></div><div id="comment-tools-40497" class="comment-tools"></div><div class="clear"></div><div id="comment-40497-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40498"></span>

<div id="answer-container-40498" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40498-score" class="post-score" title="current number of votes">0</div><span id="post-40498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's been renamed/moved to another menu option. Use "File" -&gt; "Export Specified Packets".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Mar '15, 17:50</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40498" class="comments-container"><span id="40501"></span><div id="comment-40501" class="comment"><div id="post-40501-score" class="comment-score"></div><div class="comment-text"><p>I.e., "saving" means saving the entire capture, including what changes you've made (adding frame comments, editing frame comments, deleting frame comments).</p><p>For writing <em>some</em> of the packets to another file, use "Export Specified Packets".</p></div><div id="comment-40501-info" class="comment-info"><span class="comment-age">(11 Mar '15, 19:23)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="40510"></span><div id="comment-40510" class="comment"><div id="post-40510-score" class="comment-score"></div><div class="comment-text"><p>Great, thanks!</p></div><div id="comment-40510-info" class="comment-info"><span class="comment-age">(12 Mar '15, 05:37)</span> <span class="comment-user userinfo">Ricardo</span></div></div><span id="40511"></span><div id="comment-40511" class="comment"><div id="post-40511-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-40511-info" class="comment-info"><span class="comment-age">(12 Mar '15, 05:44)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-40498" class="comment-tools"></div><div class="clear"></div><div id="comment-40498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

