+++
type = "question"
title = "Follow related connections"
description = '''I am somewhat familiar with TCP WireShark, etc. Mediocore I guess. I have a question: Many times I get spam mail from whomever. I am a curious fellow and wonder what the source of it is. Many times I get a link to click on so I can, according the instructions, log in to fix up my userid/password. Ye...'''
date = "2014-02-17T11:11:00Z"
lastmod = "2014-02-17T11:55:00Z"
weight = 29938
keywords = [ "redirect", "follow", "conversation" ]
aliases = [ "/questions/29938" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Follow related connections](/questions/29938/follow-related-connections)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29938-score" class="post-score" title="current number of votes">0</div><span id="post-29938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am somewhat familiar with TCP WireShark, etc. Mediocore I guess. I have a question: Many times I get spam mail from whomever. I am a curious fellow and wonder what the source of it is. Many times I get a link to click on so I can, according the instructions, log in to fix up my userid/password. Yeah, right. I know it's not probably recommended, but I fire up WireShark and try to follow what happens when I click such a link. Most times, there is one conversation that ends with a 320, re-direct. Follow conversation does not seem to follow that. I'm not sure if I'm making sense, but is there any way of following that first request, typicall a GET through it's travels including re-directs to other hosts? That is, I end up seeing the entire flow?</p><p>Thanks for any tips suggestions.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-redirect" rel="tag" title="see questions tagged &#39;redirect&#39;">redirect</span> <span class="post-tag tag-link-follow" rel="tag" title="see questions tagged &#39;follow&#39;">follow</span> <span class="post-tag tag-link-conversation" rel="tag" title="see questions tagged &#39;conversation&#39;">conversation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '14, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/061178fb49002a955bde36a1a1fbeb74?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="larryralph&#39;s gravatar image" /><p><span>larryralph</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="larryralph has no accepted answers">0%</span></p></div></div><div id="comments-container-29938" class="comments-container"><span id="29939"></span><div id="comment-29939" class="comment"><div id="post-29939-score" class="comment-score"></div><div class="comment-text"><p>Sorry, should have been a '302 Redirect'. I guess I could then follow that redirected conversation, but was wonder if there was some way (a filter?), do that automatically??</p><p>Gracias</p></div><div id="comment-29939-info" class="comment-info"><span class="comment-age">(17 Feb '14, 11:16)</span> <span class="comment-user userinfo">larryralph</span></div></div></div><div id="comment-tools-29938" class="comment-tools"></div><div class="clear"></div><div id="comment-29938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29942"></span>

<div id="answer-container-29942" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29942-score" class="post-score" title="current number of votes">0</div><span id="post-29942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Guess I should have done some searching first. I think I found the answer in this very forum. Here's a link to what I found. I will try the filter technique mentioned: <a href="http://ask.wireshark.org/questions/27616/follow-http-redirects-automatically-http-status-codes-301302?">http://ask.wireshark.org/questions/27616/follow-http-redirects-automatically-http-status-codes-301302?</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '14, 11:55</strong></p><img src="https://secure.gravatar.com/avatar/061178fb49002a955bde36a1a1fbeb74?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="larryralph&#39;s gravatar image" /><p><span>larryralph</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="larryralph has no accepted answers">0%</span></p></div></div><div id="comments-container-29942" class="comments-container"></div><div id="comment-tools-29942" class="comment-tools"></div><div class="clear"></div><div id="comment-29942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

