+++
type = "question"
title = "Speed of capture in monitor mode"
description = '''I have one doubt about the speed of packets capture in monitor mode. If i have a 300mbps usb wifi adapter in monitor mode, what is the max speed of packet capture? The 300mbps of my adapter is associated with the 802.11n network, if i connect in 802.11g network, the speed gonna be lower, but in moni...'''
date = "2014-02-18T06:25:00Z"
lastmod = "2014-02-18T06:25:00Z"
weight = 29967
keywords = [ "mbps", "speed", "monitor", "monitor-mode" ]
aliases = [ "/questions/29967" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Speed of capture in monitor mode](/questions/29967/speed-of-capture-in-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29967-score" class="post-score" title="current number of votes">0</div><span id="post-29967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have one doubt about the speed of packets capture in monitor mode. If i have a 300mbps usb wifi adapter in monitor mode, what is the max speed of packet capture?</p><p>The 300mbps of my adapter is associated with the 802.11n network, if i connect in 802.11g network, the speed gonna be lower, but in monitor mode, how this works!?</p><p>Thanks!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mbps" rel="tag" title="see questions tagged &#39;mbps&#39;">mbps</span> <span class="post-tag tag-link-speed" rel="tag" title="see questions tagged &#39;speed&#39;">speed</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '14, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/7d9b52b827844af1dde7dc3639a83243?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="diego&#39;s gravatar image" /><p><span>diego</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="diego has no accepted answers">0%</span></p></div></div><div id="comments-container-29967" class="comments-container"></div><div id="comment-tools-29967" class="comment-tools"></div><div class="clear"></div><div id="comment-29967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

