+++
type = "question"
title = "Open Multiple Captures on Mac"
description = '''Not sure if has been asked before but I failed to find any related from search... I am wondering if I would open multiple capture files on Mac in Wireshare at the same time? For example, I need to compare the capture done on inside interface of the firewall vs capture done on outside interface.'''
date = "2016-07-13T07:10:00Z"
lastmod = "2016-07-18T11:56:00Z"
weight = 54036
keywords = [ "multiple-files", "mac" ]
aliases = [ "/questions/54036" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Open Multiple Captures on Mac](/questions/54036/open-multiple-captures-on-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54036-score" class="post-score" title="current number of votes">0</div><span id="post-54036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Not sure if has been asked before but I failed to find any related from search...</p><p>I am wondering if I would open multiple capture files on Mac in Wireshare at the same time? For example, I need to compare the capture done on inside interface of the firewall vs capture done on outside interface.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple-files" rel="tag" title="see questions tagged &#39;multiple-files&#39;">multiple-files</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '16, 07:10</strong></p><img src="https://secure.gravatar.com/avatar/d8eb0017ee76845a2cf84a80bef5fe19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m1xed0s&#39;s gravatar image" /><p><span>m1xed0s</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m1xed0s has no accepted answers">0%</span></p></div></div><div id="comments-container-54036" class="comments-container"><span id="54138"></span><div id="comment-54138" class="comment"><div id="post-54138-score" class="comment-score"></div><div class="comment-text"><p>Spy into my wi fi ..controls everything I want to do.Help me find who this is. Has been going on for many months. Help! At present I am on my I pad</p></div><div id="comment-54138-info" class="comment-info"><span class="comment-age">(18 Jul '16, 11:56)</span> <span class="comment-user userinfo">Lillian Pitt</span></div></div></div><div id="comment-tools-54036" class="comment-tools"></div><div class="clear"></div><div id="comment-54036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54039"></span>

<div id="answer-container-54039" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54039-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54039-score" class="post-score" title="current number of votes">1</div><span id="post-54039-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the command line you can run</p><pre><code>open -n /Applications/Wireshark.app</code></pre><p>As its name implies, <code>open</code> opens applications and files, and the <code>-n</code> flag allows you to run multiple instances of applications.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jul '16, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-54039" class="comments-container"><span id="54041"></span><div id="comment-54041" class="comment"><div id="post-54041-score" class="comment-score"></div><div class="comment-text"><p>Is it possible that I double-click on the PCAP files to open Wireshark separately?</p></div><div id="comment-54041-info" class="comment-info"><span class="comment-age">(13 Jul '16, 08:48)</span> <span class="comment-user userinfo">m1xed0s</span></div></div><span id="54052"></span><div id="comment-54052" class="comment"><div id="post-54052-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Is it possible that I double-click on the PCAP files to open Wireshark separately?</p></blockquote><p>Unfortunately, no. Mac OS X^W^W^WOS X^W^WmacOS prefers that all open files of a given type be handled by a single process, so, if the program that handles a given file type is already running, the Finder will tell it to open a new file. Currently, Wireshark does not support having more than one file open per process, so that doesn't work.</p><p>Eventually, we'd like to have a single Wireshark process be able to handle multiple open files, but, given that a <em>lot</em> of code in Wireshark currently has global variables for information about the current file (as in "a lot of <em>dissectors</em>", so it's not just a case of "fix up the Wireshark core and we're done"), doing so would be a big project.</p></div><div id="comment-54052-info" class="comment-info"><span class="comment-age">(13 Jul '16, 20:25)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="54062"></span><div id="comment-54062" class="comment"><div id="post-54062-score" class="comment-score"></div><div class="comment-text"><p>Well, most of the apps on mac, even the basic ones like text editor, preview etc, they all can open multiple files in separate windows.</p></div><div id="comment-54062-info" class="comment-info"><span class="comment-age">(14 Jul '16, 04:57)</span> <span class="comment-user userinfo">m1xed0s</span></div></div><span id="54067"></span><div id="comment-54067" class="comment"><div id="post-54067-score" class="comment-score"></div><div class="comment-text"><p>That's certainly true, but it doesn't 1) magically make Wireshark (an application whose GUI code was not originally written for OS X) capable of opening multiple files in separate windows or 2) magically make it a simple change to make it capable of opening multiple files in separate windows. As I said, we'd like to make that possible <em>eventually</em>, but it's a big project.</p></div><div id="comment-54067-info" class="comment-info"><span class="comment-age">(14 Jul '16, 09:45)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="54068"></span><div id="comment-54068" class="comment"><div id="post-54068-score" class="comment-score"></div><div class="comment-text"><p>I have no idea how the notification of a new file is sent to the process on OS X, but is it possible for Wireshark to use the notification to start another process with the new file? This might violate some OS X guidelines, but it would help out the users.</p></div><div id="comment-54068-info" class="comment-info"><span class="comment-age">(14 Jul '16, 09:52)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-54039" class="comment-tools"></div><div class="clear"></div><div id="comment-54039-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

