+++
type = "question"
title = "Capture Analysis"
description = '''I would like to know if there are any good references for analysing capture files. I can see the data, however, I don&#x27;t have the knowledge to analyse what I am seeing. '''
date = "2011-02-22T08:21:00Z"
lastmod = "2011-02-22T08:31:00Z"
weight = 2483
keywords = [ "nkingcade" ]
aliases = [ "/questions/2483" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Analysis](/questions/2483/capture-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2483-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2483-score" class="post-score" title="current number of votes">0</div><span id="post-2483-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if there are any good references for analysing capture files. I can see the data, however, I don't have the knowledge to analyse what I am seeing.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nkingcade" rel="tag" title="see questions tagged &#39;nkingcade&#39;">nkingcade</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '11, 08:21</strong></p><img src="https://secure.gravatar.com/avatar/5fc5de37fa1bc2f549e850233e7c3eaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nkingcade&#39;s gravatar image" /><p><span>nkingcade</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nkingcade has no accepted answers">0%</span></p></div></div><div id="comments-container-2483" class="comments-container"></div><div id="comment-tools-2483" class="comment-tools"></div><div class="clear"></div><div id="comment-2483-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2484"></span>

<div id="answer-container-2484" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2484-score" class="post-score" title="current number of votes">1</div><span id="post-2484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That question is somewhat similar to the following question:</p><pre><code>I would like to know if there are any good references for analysing X-ray images.
I can see the picture with bones and all, however, I don&#39;t have the knowledge to
analyse what I am seeing.</code></pre><p>Of course, some things do stick out in wireshark capture files (just like the shadow of a scissor will still out in an X-ray image). For the most part however, it takes time to get to know how the protocols work and what should be visible in the capture file.</p><p>Start with a book like <a href="http://www.wiresharkbook.com/">Wireshark Network Analysis</a> by Laura Chappell, this will get you on the way with working with Wireshark and into some of the most commonly used protocols. Then you can dig deeper by reading up on the RFC's, analyzing lots of different tracefiles and so on...</p><p>Of course when you have specific questions on things you are trying to analyze, you can use the wireshark mailing lists and this Q&amp;A site for support.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '11, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-2484" class="comments-container"></div><div id="comment-tools-2484" class="comment-tools"></div><div class="clear"></div><div id="comment-2484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

