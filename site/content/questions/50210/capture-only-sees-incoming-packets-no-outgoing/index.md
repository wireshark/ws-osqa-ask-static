+++
type = "question"
title = "[closed] capture only sees incoming packets, no outgoing."
description = '''My problem is that running a capture I am only seeing incoming packets, but nothing outgoing - I think this started around the time i upgraded to v2.0.x, but not 100% sure. I&#x27;ve checked capture filters, I&#x27;ve reinstalled WireShark v2.x and v1.x and winpcap, but it has made no difference. Same result ...'''
date = "2016-02-15T07:46:00Z"
lastmod = "2016-02-15T08:59:00Z"
weight = 50210
keywords = [ "bidirectional", "capture", "ingress", "egress", "unidirection" ]
aliases = [ "/questions/50210" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] capture only sees incoming packets, no outgoing.](/questions/50210/capture-only-sees-incoming-packets-no-outgoing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50210-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50210-score" class="post-score" title="current number of votes">0</div><span id="post-50210-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My problem is that running a capture I am only seeing incoming packets, but nothing outgoing - I think this started around the time i upgraded to v2.0.x, but not 100% sure.</p><p>I've checked capture filters, I've reinstalled WireShark v2.x and v1.x and winpcap, but it has made no difference. Same result for WLAN interface and Ethernet.</p><p>Any thoughts to what my problem is?</p><p>R.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bidirectional" rel="tag" title="see questions tagged &#39;bidirectional&#39;">bidirectional</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-ingress" rel="tag" title="see questions tagged &#39;ingress&#39;">ingress</span> <span class="post-tag tag-link-egress" rel="tag" title="see questions tagged &#39;egress&#39;">egress</span> <span class="post-tag tag-link-unidirection" rel="tag" title="see questions tagged &#39;unidirection&#39;">unidirection</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '16, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/39cbcbca1eb35ce9d3fa32dac6536b93?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rodders&#39;s gravatar image" /><p><span>rodders</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rodders has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>15 Feb '16, 08:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50210" class="comments-container"><span id="50213"></span><div id="comment-50213" class="comment"><div id="post-50213-score" class="comment-score"></div><div class="comment-text"><p>Asked before, quite a few times, e.g. <a href="https://ask.wireshark.org/questions/28909/no-outgoing-packets">here</a> and <a href="https://ask.wireshark.org/questions/39008/ethernet-outgoing-packets-not-being-showed">here</a> and <a href="https://ask.wireshark.org/questions/20937/wireshark-capturing-only-incoming-traffic-on-windows-7">here</a>.</p></div><div id="comment-50213-info" class="comment-info"><span class="comment-age">(15 Feb '16, 08:59)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50210" class="comment-tools"></div><div class="clear"></div><div id="comment-50210-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 15 Feb '16, 08:59

</div>

</div>

</div>

