+++
type = "question"
title = "Ip address from Bandwidth.com text"
description = '''Any chance I can locate the ip address from a text received through voip?'''
date = "2014-02-05T00:15:00Z"
lastmod = "2014-02-05T08:12:00Z"
weight = 29446
keywords = [ "voip" ]
aliases = [ "/questions/29446" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ip address from Bandwidth.com text](/questions/29446/ip-address-from-bandwidthcom-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29446-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29446-score" class="post-score" title="current number of votes">0</div><span id="post-29446-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Any chance I can locate the ip address from a text received through voip?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '14, 00:15</strong></p><img src="https://secure.gravatar.com/avatar/48de4236be0ede70297c663c2028dae0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jd421&#39;s gravatar image" /><p><span>jd421</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jd421 has no accepted answers">0%</span></p></div></div><div id="comments-container-29446" class="comments-container"><span id="29448"></span><div id="comment-29448" class="comment"><div id="post-29448-score" class="comment-score"></div><div class="comment-text"><p>Voip, <strong>text</strong>, IP address? What is the relationship between all these things? Could you please be more specific, because I don't understand what you are looking for....</p></div><div id="comment-29448-info" class="comment-info"><span class="comment-age">(05 Feb '14, 01:11)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29462"></span><div id="comment-29462" class="comment"><div id="post-29462-score" class="comment-score"></div><div class="comment-text"><p>If somebody sends a text message through bandwidth.com, can I find out the ip address from the computer it was sent from? Bandwidth.com is a company that lets you send or receive texts or make phone calls through your internet connection.</p></div><div id="comment-29462-info" class="comment-info"><span class="comment-age">(05 Feb '14, 07:58)</span> <span class="comment-user userinfo">jd421</span></div></div></div><div id="comment-tools-29446" class="comment-tools"></div><div class="clear"></div><div id="comment-29446-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29463"></span>

<div id="answer-container-29463" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29463-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29463-score" class="post-score" title="current number of votes">0</div><span id="post-29463-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>If somebody sends a text message through bandwidth.com, can I find out the ip address from the computer it was sent from?</p></blockquote><p>If a 'text message' is a SMS received by your mobile phone, routed through their SMS gateway, then I would say: No, there is no (simple) way to figure out the IP address of the sender, unless bandwidth.com offers some kind of logging/tracking feature or sends the IP address alongside with the SMS.</p><p>If a 'text message' is kind of a chat message, using either a web-gui on their server or some piece of software they provide, I would again say: No, there is no (simple) way to figure out the IP address of the sender, as all those messages will be exchanged through their servers. So, all you see is their IP addresses. Unless they add some meta data to the chat message, containing information about the original sender, you will not be able to figure out the 'original' IP address.</p><p><strong>Hint:</strong> The IP addresses will (most certainly) be stored on their servers for legal reasons (if the FBI or NSA happen to ask them). They just won't give that information to you.</p><p>However, I don't know their service, so my remarks are just speculation, based on common sense and some knowledge about other similar services.</p><p>If you need a final answer, you should ask the guys at bandwidth.com. They should know how their service works.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '14, 08:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Feb '14, 08:15</strong> </span></p></div></div><div id="comments-container-29463" class="comments-container"></div><div id="comment-tools-29463" class="comment-tools"></div><div class="clear"></div><div id="comment-29463-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

