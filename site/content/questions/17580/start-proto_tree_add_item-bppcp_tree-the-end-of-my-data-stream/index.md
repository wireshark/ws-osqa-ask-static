+++
type = "question"
title = "Start proto_tree_add_item( bppcp_tree @ the End of my Data Stream:"
description = '''Via my dissector, I need to add an item to the End of my Data Block. How can I do this? Like so? / ODH TERMINATOR / bppcp_sub_item = proto_tree_add_item( bppcp_tree, hf_bppcp_cr, tvb, 0, 0, FALSE ); proto_tree_add_item( bppcp_tree, hf_bppcp_cr, tvb, 0, 0, FALSE );'''
date = "2013-01-08T13:56:00Z"
lastmod = "2013-01-08T14:17:00Z"
weight = 17580
keywords = [ "endofdatablock" ]
aliases = [ "/questions/17580" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Start proto\_tree\_add\_item( bppcp\_tree @ the End of my Data Stream:](/questions/17580/start-proto_tree_add_item-bppcp_tree-the-end-of-my-data-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17580-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17580-score" class="post-score" title="current number of votes">0</div><span id="post-17580-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Via my dissector, I need to add an item to the End of my Data Block.</p><p>How can I do this?</p><p>Like so?</p><p>/ <em>ODH TERMINATOR</em> / bppcp_sub_item = proto_tree_add_item( bppcp_tree, hf_bppcp_cr, tvb, 0, 0, FALSE ); proto_tree_add_item( bppcp_tree, hf_bppcp_cr, tvb, 0, 0, FALSE );</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-endofdatablock" rel="tag" title="see questions tagged &#39;endofdatablock&#39;">endofdatablock</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '13, 13:56</strong></p><img src="https://secure.gravatar.com/avatar/1f51b148d3f1f063aa95114ceea3b70f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jballard1979&#39;s gravatar image" /><p><span>jballard1979</span><br />
<span class="score" title="20 reputation points">20</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jballard1979 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Jan '13, 16:01</strong> </span></p></div></div><div id="comments-container-17580" class="comments-container"></div><div id="comment-tools-17580" class="comment-tools"></div><div class="clear"></div><div id="comment-17580-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17582"></span>

<div id="answer-container-17582" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17582-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17582-score" class="post-score" title="current number of votes">0</div><span id="post-17582-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Add an item with length 0?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '13, 14:17</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-17582" class="comments-container"></div><div id="comment-tools-17582" class="comment-tools"></div><div class="clear"></div><div id="comment-17582-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

