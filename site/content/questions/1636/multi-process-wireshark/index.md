+++
type = "question"
title = "Multi-Process Wireshark"
description = '''Is it possible to run several processes of Wireshark on a single PC running Windows (as long as we have several Ethernet ports)?'''
date = "2011-01-05T11:32:00Z"
lastmod = "2011-01-05T21:30:00Z"
weight = 1636
keywords = [ "windows", "multi-process" ]
aliases = [ "/questions/1636" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Multi-Process Wireshark](/questions/1636/multi-process-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1636-score" class="post-score" title="current number of votes">0</div><span id="post-1636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to run several processes of Wireshark on a single PC running Windows (as long as we have several Ethernet ports)?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-multi-process" rel="tag" title="see questions tagged &#39;multi-process&#39;">multi-process</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '11, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/9371cde7ac618190da5420f10a6ff30c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jhaynes&#39;s gravatar image" /><p><span>jhaynes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jhaynes has no accepted answers">0%</span></p></div></div><div id="comments-container-1636" class="comments-container"></div><div id="comment-tools-1636" class="comment-tools"></div><div class="clear"></div><div id="comment-1636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1637"></span>

<div id="answer-container-1637" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1637-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1637-score" class="post-score" title="current number of votes">2</div><span id="post-1637-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, even if you have just one Ethernet card you can run multiple instances and capture, at least I did that a couple of times before, capturing with different sets of capture filters on the same card. I would not advise to capture on a single card like this in a serious analysis case, but if it's not critical it works just fine. Multiple cards should be no problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '11, 12:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-1637" class="comments-container"><span id="1643"></span><div id="comment-1643" class="comment"><div id="post-1643-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper! I appreciate the response.</p></div><div id="comment-1643-info" class="comment-info"><span class="comment-age">(05 Jan '11, 21:30)</span> <span class="comment-user userinfo">jhaynes</span></div></div></div><div id="comment-tools-1637" class="comment-tools"></div><div class="clear"></div><div id="comment-1637-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

