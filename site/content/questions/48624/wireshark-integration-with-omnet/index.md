+++
type = "question"
title = "Wireshark integration with OMNET ++"
description = '''Hi, I want to use OMNET ++ for network simulation, Can i Capture the intermediate results in a network in a PCAP format for wireshark analysis ??? Regards M Srinivasa Rao'''
date = "2015-12-17T19:48:00Z"
lastmod = "2016-01-09T12:37:00Z"
weight = 48624
keywords = [ "++", "omnet" ]
aliases = [ "/questions/48624" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark integration with OMNET ++](/questions/48624/wireshark-integration-with-omnet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48624-score" class="post-score" title="current number of votes">0</div><span id="post-48624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I want to use OMNET ++ for network simulation, Can i Capture the intermediate results in a network in a PCAP format for wireshark analysis ???</p><p>Regards M Srinivasa Rao</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-++" rel="tag" title="see questions tagged &#39;++&#39;">++</span> <span class="post-tag tag-link-omnet" rel="tag" title="see questions tagged &#39;omnet&#39;">omnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '15, 19:48</strong></p><img src="https://secure.gravatar.com/avatar/ce1843f92a1c18db26bc79b3afa9bd50?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srinu_bel&#39;s gravatar image" /><p><span>srinu_bel</span><br />
<span class="score" title="20 reputation points">20</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srinu_bel has no accepted answers">0%</span></p></div></div><div id="comments-container-48624" class="comments-container"></div><div id="comment-tools-48624" class="comment-tools"></div><div class="clear"></div><div id="comment-48624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49029"></span>

<div id="answer-container-49029" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49029-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49029-score" class="post-score" title="current number of votes">1</div><span id="post-49029-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Can i Capture the intermediate results in a network in a PCAP format for wireshark analysis ???</p></blockquote><p>well, this is the Wireshark Q&amp;A site, not the OMNET++ support forum ;-) <strong>However</strong> a quick google search reveals that it's apprently possible to create pcap files in OMNET++.</p><blockquote><p><a href="https://omnetpp.org/doc/inet/api-current/doxy/class_pcap_recorder.html">https://omnetpp.org/doc/inet/api-current/doxy/class_pcap_recorder.html</a><br />
<a href="http://ilab.cs.byu.edu/wiki/Omnet%2B%2B_Tutorial">http://ilab.cs.byu.edu/wiki/Omnet%2B%2B_Tutorial</a></p></blockquote><p>Cite:</p><pre><code>Example Project: tcpsack
There are many examples that can be found in Omnet of how to do run simulations. A useful example can be found in the inet/examples/inet/tcpsack directory. This experiment sets up a flow between two hosts with TCP Sack, and outputs files in multiple formats, including the pcap format that Wireshark uses. With an explanation of this project, you should be able to set up similar projects.</code></pre><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jan '16, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-49029" class="comments-container"></div><div id="comment-tools-49029" class="comment-tools"></div><div class="clear"></div><div id="comment-49029-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

