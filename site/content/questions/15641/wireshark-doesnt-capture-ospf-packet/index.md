+++
type = "question"
title = "wireshark doesnt capture OSPF packet"
description = '''My coworker using same Wireshark (Version 1.8.3 (SVN Rev 45256 from /trunk-1.8)) and same physical monitor interface but different laptop. But my wireshark could not capture OSPF packet. anybody has similar experence? My laptop is Toshiba Dynabook with Intel(R) 82579LM Gigabit Network Connection.'''
date = "2012-11-07T06:13:00Z"
lastmod = "2012-11-07T07:40:00Z"
weight = 15641
keywords = [ "ospf" ]
aliases = [ "/questions/15641" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark doesnt capture OSPF packet](/questions/15641/wireshark-doesnt-capture-ospf-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15641-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15641-score" class="post-score" title="current number of votes">0</div><span id="post-15641-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My coworker using same Wireshark (Version 1.8.3 (SVN Rev 45256 from /trunk-1.8)) and same physical monitor interface but different laptop. But my wireshark could not capture OSPF packet. anybody has similar experence? My laptop is Toshiba Dynabook with Intel(R) 82579LM Gigabit Network Connection.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ospf" rel="tag" title="see questions tagged &#39;ospf&#39;">ospf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '12, 06:13</strong></p><img src="https://secure.gravatar.com/avatar/46888a58cdaae9567209856568d97937?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cheng531&#39;s gravatar image" /><p><span>cheng531</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cheng531 has no accepted answers">0%</span></p></div></div><div id="comments-container-15641" class="comments-container"><span id="15644"></span><div id="comment-15644" class="comment"><div id="post-15644-score" class="comment-score"></div><div class="comment-text"><p>As there is no "special" reason why you should not see OSPF packets, I have some questions:</p><ul><li>did you see any other traffic?</li><li>did you use any filter?</li></ul></div><div id="comment-15644-info" class="comment-info"><span class="comment-age">(07 Nov '12, 07:04)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="15648"></span><div id="comment-15648" class="comment"><div id="post-15648-score" class="comment-score"></div><div class="comment-text"><p>Yeah. That is why I could not understand it. I saw another traffic, like http, ICMP, etc. I do not think that i enable any filter. never saw this. Do you think that it because of any firwall or any antivirus ?</p></div><div id="comment-15648-info" class="comment-info"><span class="comment-age">(07 Nov '12, 07:37)</span> <span class="comment-user userinfo">cheng531</span></div></div><span id="15650"></span><div id="comment-15650" class="comment"><div id="post-15650-score" class="comment-score"></div><div class="comment-text"><blockquote><p>firwall or any antivirus ?</p></blockquote><p>On your capturing system? Well, that's a problem and you should not run that kind of tools on a capturing system. Please disable those tools and then try again.</p></div><div id="comment-15650-info" class="comment-info"><span class="comment-age">(07 Nov '12, 07:40)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15641" class="comment-tools"></div><div class="clear"></div><div id="comment-15641-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

