+++
type = "question"
title = "Wireshark src or whireshark-devel rpm?"
description = '''Hello,  I need to create a plugin for wireshark for a proprietary protocol. I have a basic idea how to do it. But I need to know do I need to download all wireshark sources or I can use only the devel rpm?'''
date = "2014-09-04T08:07:00Z"
lastmod = "2014-09-04T10:32:00Z"
weight = 36005
keywords = [ "plugin" ]
aliases = [ "/questions/36005" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark src or whireshark-devel rpm?](/questions/36005/wireshark-src-or-whireshark-devel-rpm)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36005-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36005-score" class="post-score" title="current number of votes">0</div><span id="post-36005-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I need to create a plugin for wireshark for a proprietary protocol.</p><p>I have a basic idea how to do it. But I need to know do I need to download all wireshark sources or I can use only the devel rpm?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Sep '14, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/cf05bfbe72b8ffa3eac997f29656132b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gull231&#39;s gravatar image" /><p><span>Gull231</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gull231 has no accepted answers">0%</span></p></div></div><div id="comments-container-36005" class="comments-container"></div><div id="comment-tools-36005" class="comment-tools"></div><div class="clear"></div><div id="comment-36005-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36008"></span>

<div id="answer-container-36008" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36008-score" class="post-score" title="current number of votes">0</div><span id="post-36008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We don't publish a "-devel" RPM though some Linux distributions do. So: your mileage in using such a package may vary.</p><p>Normally my recommendation is to develop against the source (either a tarball or--better yet--from git directly).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Sep '14, 10:32</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-36008" class="comments-container"></div><div id="comment-tools-36008" class="comment-tools"></div><div class="clear"></div><div id="comment-36008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

