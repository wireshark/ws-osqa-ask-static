+++
type = "question"
title = "can i sniff a X.21 port?"
description = '''Hello, im looking for device to sniff a X.21 port and later read with wireshark. can be this posible? the device is TELLABS 8110. '''
date = "2015-06-19T18:03:00Z"
lastmod = "2015-06-21T11:44:00Z"
weight = 43392
keywords = [ "sniffer", "sniff", "x.21", "serial", "wireshark" ]
aliases = [ "/questions/43392" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can i sniff a X.21 port?](/questions/43392/can-i-sniff-a-x21-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43392-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43392-score" class="post-score" title="current number of votes">0</div><span id="post-43392-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>im looking for device to sniff a X.21 port and later read with wireshark. can be this posible?</p><p>the device is TELLABS 8110. <img src="https://osqa-ask.wireshark.org/upfiles/142583164016695499662015-03-08_18.16.55.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-sniff" rel="tag" title="see questions tagged &#39;sniff&#39;">sniff</span> <span class="post-tag tag-link-x.21" rel="tag" title="see questions tagged &#39;x.21&#39;">x.21</span> <span class="post-tag tag-link-serial" rel="tag" title="see questions tagged &#39;serial&#39;">serial</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '15, 18:03</strong></p><img src="https://secure.gravatar.com/avatar/a7efdaf6079e24cd2813662f99e0cf05?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Juan%20Carlos%20Garcia&#39;s gravatar image" /><p><span>Juan Carlos ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Juan Carlos Garcia has no accepted answers">0%</span></p></img></div></div><div id="comments-container-43392" class="comments-container"></div><div id="comment-tools-43392" class="comment-tools"></div><div class="clear"></div><div id="comment-43392-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43413"></span>

<div id="answer-container-43413" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43413-score" class="post-score" title="current number of votes">0</div><span id="post-43413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>X.21 is a serial protocol, so if you want to capture X.21 traffic, you will need a harware sniffer that is able to do the physical data capturing (usually by "loop-in"). The old Network Associates/General Sniffers had the option to sniff X.21 traffic. Maybe you're able to find one on eBay (search for: Network Associates s4000). There may be other X.21 sniffers available, but I don't know one, as X.21 is not part of "my world" ;-)</p><p>BTW: As far as I can see in the code, Wireshark does not have code/support for X.21.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jun '15, 11:44</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-43413" class="comments-container"></div><div id="comment-tools-43413" class="comment-tools"></div><div class="clear"></div><div id="comment-43413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

