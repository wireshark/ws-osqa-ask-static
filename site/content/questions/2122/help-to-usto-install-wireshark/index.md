+++
type = "question"
title = "help to us...to install wireshark."
description = '''i have installed wireshark on my college pc and its not working... showing message Error signature.... &quot;AppName: wireshark.exe AppVer: 1.4.3.35482 ModName: unknown ModVer: 0.0.0.0 Offset: 00000000&quot;'''
date = "2011-02-03T00:45:00Z"
lastmod = "2011-02-03T00:53:00Z"
weight = 2122
keywords = [ "sandesh" ]
aliases = [ "/questions/2122" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [help to us...to install wireshark.](/questions/2122/help-to-usto-install-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2122-score" class="post-score" title="current number of votes">0</div><span id="post-2122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have installed wireshark on my college pc and its not working... showing message Error signature.... "AppName: wireshark.exe AppVer: 1.4.3.35482 ModName: unknown ModVer: 0.0.0.0 Offset: 00000000"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sandesh" rel="tag" title="see questions tagged &#39;sandesh&#39;">sandesh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '11, 00:45</strong></p><img src="https://secure.gravatar.com/avatar/ac67fffd5370e82e20bc48d5ea899365?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sandesh&#39;s gravatar image" /><p><span>sandesh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sandesh has no accepted answers">0%</span></p></div></div><div id="comments-container-2122" class="comments-container"><span id="2123"></span><div id="comment-2123" class="comment"><div id="post-2123-score" class="comment-score"></div><div class="comment-text"><p>On what platform are you trying to install the wireshark?</p></div><div id="comment-2123-info" class="comment-info"><span class="comment-age">(03 Feb '11, 00:53)</span> <span class="comment-user userinfo">sid</span></div></div></div><div id="comment-tools-2122" class="comment-tools"></div><div class="clear"></div><div id="comment-2122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

