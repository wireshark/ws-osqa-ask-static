+++
type = "question"
title = "how can i capture the traffic of my iphone"
description = '''hi guys i want to capture my iphone traffic. if anyone can tell me how that will be appreciated '''
date = "2014-10-08T06:46:00Z"
lastmod = "2014-10-13T10:06:00Z"
weight = 36918
keywords = [ "iphone" ]
aliases = [ "/questions/36918" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [how can i capture the traffic of my iphone](/questions/36918/how-can-i-capture-the-traffic-of-my-iphone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36918-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36918-score" class="post-score" title="current number of votes">0</div><span id="post-36918-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi guys i want to capture my iphone traffic. if anyone can tell me how that will be appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '14, 06:46</strong></p><img src="https://secure.gravatar.com/avatar/9c524a76dfdec4dce33b9809889555e4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="azoz158&#39;s gravatar image" /><p><span>azoz158</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="azoz158 has no accepted answers">0%</span></p></div></div><div id="comments-container-36918" class="comments-container"></div><div id="comment-tools-36918" class="comment-tools"></div><div class="clear"></div><div id="comment-36918-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="36955"></span>

<div id="answer-container-36955" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36955-score" class="post-score" title="current number of votes">0</div><span id="post-36955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="https://ask.wireshark.org/questions/17559/packet-capturing-application-for-the-iphone">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '14, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36955" class="comments-container"></div><div id="comment-tools-36955" class="comment-tools"></div><div class="clear"></div><div id="comment-36955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="37017"></span>

<div id="answer-container-37017" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37017-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37017-score" class="post-score" title="current number of votes">0</div><span id="post-37017-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I also (accidently) posted this to the old thread which is linked above:</p><p>Maybe you can use your router to capture it.<br />
Not all routers support this, but e.g. AVMs FritzBoxes support this.<br />
On the FritzBox web interface you can activate the capture<br />
which creates a download of the capture file which you can open with Wireshark.<br />
As long you don't close the website, the capture will go on.<br />
But you will capture all network traffic, maybe from other devices in your network.<br />
So better inform the other users or build up an independet network only with your router, PC and iPhone.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Oct '14, 10:06</strong></p><img src="https://secure.gravatar.com/avatar/cc56ba9bd225bd68cea09a404ecc0b6e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lal12&#39;s gravatar image" /><p><span>lal12</span><br />
<span class="score" title="36 reputation points">36</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lal12 has 2 accepted answers">33%</span> </br></br></p></div></div><div id="comments-container-37017" class="comments-container"></div><div id="comment-tools-37017" class="comment-tools"></div><div class="clear"></div><div id="comment-37017-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

