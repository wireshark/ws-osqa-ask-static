+++
type = "question"
title = "Does my dump have packet loss?"
description = '''I&#x27;ve checked the Expert Info and get no errors or warnings. I also did some sequence number and next sequence number analysis and and it seems for me that all data has been received and captured in order.  I&#x27;m still beginner in tcp traffic analysis and I&#x27;d appreciate if someone can have a look at my...'''
date = "2015-08-04T11:38:00Z"
lastmod = "2015-08-04T12:45:00Z"
weight = 44832
keywords = [ "loss", "example", "packet", "check" ]
aliases = [ "/questions/44832" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Does my dump have packet loss?](/questions/44832/does-my-dump-have-packet-loss)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44832-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44832-score" class="post-score" title="current number of votes">0</div><span id="post-44832-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've checked the Expert Info and get no errors or warnings. I also did some sequence number and next sequence number analysis and and it seems for me that all data has been received and captured in order.</p><p>I'm still beginner in tcp traffic analysis and I'd appreciate if someone can have a look <a href="https://www.cloudshark.org/captures/aabf42c23860">at my dump</a> and double check my conclusion?</p><p>Thanks.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-loss" rel="tag" title="see questions tagged &#39;loss&#39;">loss</span> <span class="post-tag tag-link-example" rel="tag" title="see questions tagged &#39;example&#39;">example</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-check" rel="tag" title="see questions tagged &#39;check&#39;">check</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Aug '15, 11:38</strong></p><img src="https://secure.gravatar.com/avatar/5642d9fe33d29ee47043f7e5796e67aa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flora&#39;s gravatar image" /><p><span>flora</span><br />
<span class="score" title="156 reputation points">156</span><span title="31 badges"><span class="badge1">●</span><span class="badgecount">31</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="38 badges"><span class="bronze">●</span><span class="badgecount">38</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flora has 2 accepted answers">100%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Aug '15, 11:38</strong> </span></p></div></div><div id="comments-container-44832" class="comments-container"></div><div id="comment-tools-44832" class="comment-tools"></div><div class="clear"></div><div id="comment-44832-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44833"></span>

<div id="answer-container-44833" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44833-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44833-score" class="post-score" title="current number of votes">1</div><span id="post-44833-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="flora has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes you are right you have captured the session without packet loss.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Aug '15, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-44833" class="comments-container"><span id="44834"></span><div id="comment-44834" class="comment"><div id="post-44834-score" class="comment-score"></div><div class="comment-text"><p>thank you so much :)</p></div><div id="comment-44834-info" class="comment-info"><span class="comment-age">(04 Aug '15, 12:32)</span> <span class="comment-user userinfo">flora</span></div></div><span id="44835"></span><div id="comment-44835" class="comment"><div id="post-44835-score" class="comment-score"></div><div class="comment-text"><p>You are welcome.</p></div><div id="comment-44835-info" class="comment-info"><span class="comment-age">(04 Aug '15, 12:45)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-44833" class="comment-tools"></div><div class="clear"></div><div id="comment-44833-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

