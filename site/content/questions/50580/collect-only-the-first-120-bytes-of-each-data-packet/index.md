+++
type = "question"
title = "collect only the first 120 bytes of each data packet."
description = '''How do I change the capture options, so that only the first 120 bytes of each data packet is collected?'''
date = "2016-02-29T07:40:00Z"
lastmod = "2016-02-29T08:09:00Z"
weight = 50580
keywords = [ "bytes" ]
aliases = [ "/questions/50580" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [collect only the first 120 bytes of each data packet.](/questions/50580/collect-only-the-first-120-bytes-of-each-data-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50580-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50580-score" class="post-score" title="current number of votes">0</div><span id="post-50580-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I change the capture options, so that only the first 120 bytes of each data packet is collected?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bytes" rel="tag" title="see questions tagged &#39;bytes&#39;">bytes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Feb '16, 07:40</strong></p><img src="https://secure.gravatar.com/avatar/3533b8dac920a5234d874db0b6f99fd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shannon%20Eakins&#39;s gravatar image" /><p><span>Shannon Eakins</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shannon Eakins has no accepted answers">0%</span></p></div></div><div id="comments-container-50580" class="comments-container"></div><div id="comment-tools-50580" class="comment-tools"></div><div class="clear"></div><div id="comment-50580-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50583"></span>

<div id="answer-container-50583" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50583-score" class="post-score" title="current number of votes">0</div><span id="post-50583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Presuming you're using the New 2.x Qt version, then in the capture options dialog, double click the Snaplen filed for the interface you wish to capture on and adjust the number as you require.</p><p>If using the legacy GTK+ version, then double click on the row for the interface you wish to capture on, and in the resulting dialog, check the box for "Limit each packet to" and then enter the number as you require.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Feb '16, 08:09</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50583" class="comments-container"></div><div id="comment-tools-50583" class="comment-tools"></div><div class="clear"></div><div id="comment-50583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

