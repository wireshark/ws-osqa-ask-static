+++
type = "question"
title = "Net Speed Internet"
description = '''Your web site is slow... around 507 KB/s for download... Up the pipe... :)'''
date = "2014-10-07T15:25:00Z"
lastmod = "2014-10-08T14:33:00Z"
weight = 36899
keywords = [ "pipe" ]
aliases = [ "/questions/36899" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Net Speed Internet](/questions/36899/net-speed-internet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36899-score" class="post-score" title="current number of votes">-2</div><span id="post-36899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Your web site is slow... around 507 KB/s for download... Up the pipe... :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '14, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/e31cf5f2dd245238a29adbf52d10f281?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LED&#39;s gravatar image" /><p><span>LED</span><br />
<span class="score" title="9 reputation points">9</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LED has no accepted answers">0%</span></p></div></div><div id="comments-container-36899" class="comments-container"><span id="36900"></span><div id="comment-36900" class="comment"><div id="post-36900-score" class="comment-score"></div><div class="comment-text"><p>I know DS3 might help Or coffee...:)</p></div><div id="comment-36900-info" class="comment-info"><span class="comment-age">(07 Oct '14, 15:29)</span> <span class="comment-user userinfo">LED</span></div></div></div><div id="comment-tools-36899" class="comment-tools"></div><div class="clear"></div><div id="comment-36899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36915"></span>

<div id="answer-container-36915" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36915-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36915-score" class="post-score" title="current number of votes">1</div><span id="post-36915-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thank your for your insightful comments. Are you volunteering to donate funds to secure a better pipe?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Oct '14, 02:41</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36915" class="comments-container"><span id="36932"></span><div id="comment-36932" class="comment"><div id="post-36932-score" class="comment-score"></div><div class="comment-text"><p>When I win the lottery.</p></div><div id="comment-36932-info" class="comment-info"><span class="comment-age">(08 Oct '14, 14:33)</span> <span class="comment-user userinfo">LED</span></div></div></div><div id="comment-tools-36915" class="comment-tools"></div><div class="clear"></div><div id="comment-36915-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

