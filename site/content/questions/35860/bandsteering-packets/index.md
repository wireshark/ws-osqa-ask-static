+++
type = "question"
title = "Bandsteering packets"
description = '''Hi Is it possible to filter bandsteering packets in a wireless network.  The only thing that I see are Probe reguests and Probe responses but I could not find any information about bandsteering in these packets. Regards'''
date = "2014-08-29T00:52:00Z"
lastmod = "2014-08-29T00:52:00Z"
weight = 35860
keywords = [ "wireless", "wifi" ]
aliases = [ "/questions/35860" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Bandsteering packets](/questions/35860/bandsteering-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35860-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35860-score" class="post-score" title="current number of votes">0</div><span id="post-35860-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>Is it possible to filter bandsteering packets in a wireless network. The only thing that I see are Probe reguests and Probe responses but I could not find any information about bandsteering in these packets.</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '14, 00:52</strong></p><img src="https://secure.gravatar.com/avatar/70ea2ff9e00cb44e0f328fb7ce77fe23?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roadrunner&#39;s gravatar image" /><p><span>Roadrunner</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roadrunner has no accepted answers">0%</span></p></div></div><div id="comments-container-35860" class="comments-container"></div><div id="comment-tools-35860" class="comment-tools"></div><div class="clear"></div><div id="comment-35860-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

