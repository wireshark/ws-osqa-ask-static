+++
type = "question"
title = "can&#x27;t start wireshark"
description = '''I&#x27;ve used wireshark for years but this latest version is puzzling, I&#x27;ve just installed v2.2.7 on windows 10 32bit. I work in computers and I&#x27;m embarrassed that I can&#x27;t work out how to start the program and see traffic. When I start wireshark I get an unfamiliar window where is says &#x27;capture....using...'''
date = "2017-06-18T01:19:00Z"
lastmod = "2017-06-19T00:55:00Z"
weight = 62091
keywords = [ "newbie" ]
aliases = [ "/questions/62091" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [can't start wireshark](/questions/62091/cant-start-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62091-score" class="post-score" title="current number of votes">0</div><span id="post-62091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've used wireshark for years but this latest version is puzzling, I've just installed v2.2.7 on windows 10 32bit. I work in computers and I'm embarrassed that I can't work out how to start the program and see traffic. When I start wireshark I get an unfamiliar window where is says 'capture....using this filter'. Problem is nothing in the drop down list works. There's no button to click to actually start capturing. wth is going on in this latest version?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '17, 01:19</strong></p><img src="https://secure.gravatar.com/avatar/a3aeb3e02b7672911169cf411c38dd0b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neuronetv&#39;s gravatar image" /><p><span>neuronetv</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neuronetv has no accepted answers">0%</span></p></div></div><div id="comments-container-62091" class="comments-container"></div><div id="comment-tools-62091" class="comment-tools"></div><div class="clear"></div><div id="comment-62091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62096"></span>

<div id="answer-container-62096" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62096-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62096-score" class="post-score" title="current number of votes">1</div><span id="post-62096-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's the "material design" approach... Just double-click the name of the interface in the list and your capture will start. Or click the interface once, then fill in the capture filter expression you need (or just click into that field and leave it empty if you don't need any actual capture filter) and press Enter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '17, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-62096" class="comments-container"><span id="62115"></span><div id="comment-62115" class="comment"><div id="post-62115-score" class="comment-score"></div><div class="comment-text"><p>ok thanks for your feedback sindy I have it running now.</p></div><div id="comment-62115-info" class="comment-info"><span class="comment-age">(19 Jun '17, 00:55)</span> <span class="comment-user userinfo">neuronetv</span></div></div></div><div id="comment-tools-62096" class="comment-tools"></div><div class="clear"></div><div id="comment-62096-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

