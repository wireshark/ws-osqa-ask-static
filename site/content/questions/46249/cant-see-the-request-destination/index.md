+++
type = "question"
title = "Can&#x27;t see the request destination"
description = '''I am new with Wireshark and need help. I am trying to lookup POST requests sent from my PC app to my TV but when I send such via the PC in Wireshark I can see only the response from the TV. I checked and no filters are set. For example if the TV&#x27;s IP is 192.168.1.100 and the PC&#x27;s IP is 192.168.1.101...'''
date = "2015-09-29T03:56:00Z"
lastmod = "2015-09-29T03:56:00Z"
weight = 46249
keywords = [ "ip", "post", "destination", "get" ]
aliases = [ "/questions/46249" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see the request destination](/questions/46249/cant-see-the-request-destination)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46249-score" class="post-score" title="current number of votes">0</div><span id="post-46249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new with Wireshark and need help. I am trying to lookup POST requests sent from my PC app to my TV but when I send such via the PC in Wireshark I can see only the response from the TV. I checked and no filters are set. For example if the TV's IP is 192.168.1.100 and the PC's IP is 192.168.1.101 there are no packets that are with 192.168.1.100 as destination, all are 192.168.1.101 as destination, i.e. what's coming back to the PC as response. I can't see any POST or GET requests. Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-post" rel="tag" title="see questions tagged &#39;post&#39;">post</span> <span class="post-tag tag-link-destination" rel="tag" title="see questions tagged &#39;destination&#39;">destination</span> <span class="post-tag tag-link-get" rel="tag" title="see questions tagged &#39;get&#39;">get</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '15, 03:56</strong></p><img src="https://secure.gravatar.com/avatar/20b755e0c732e9056ca2c22db0be84ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rafoto&#39;s gravatar image" /><p><span>rafoto</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rafoto has no accepted answers">0%</span></p></div></div><div id="comments-container-46249" class="comments-container"></div><div id="comment-tools-46249" class="comment-tools"></div><div class="clear"></div><div id="comment-46249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

