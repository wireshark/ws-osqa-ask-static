+++
type = "question"
title = "[Buildbot] Can I obtain scripts of buildbot?"
description = '''Hi, I want to setup buildbot server on my computer. Can I obtain scripts of buildbot? Thank You,'''
date = "2014-03-05T22:47:00Z"
lastmod = "2014-03-07T06:25:00Z"
weight = 30461
keywords = [ "buildbot" ]
aliases = [ "/questions/30461" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[Buildbot\] Can I obtain scripts of buildbot?](/questions/30461/buildbot-can-i-obtain-scripts-of-buildbot)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30461-score" class="post-score" title="current number of votes">0</div><span id="post-30461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I want to setup buildbot server on my computer. Can I obtain scripts of buildbot? Thank You,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-buildbot" rel="tag" title="see questions tagged &#39;buildbot&#39;">buildbot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '14, 22:47</strong></p><img src="https://secure.gravatar.com/avatar/3b1810af71405c8505d1de7a282aa8b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prosohard&#39;s gravatar image" /><p><span>prosohard</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prosohard has no accepted answers">0%</span></p></div></div><div id="comments-container-30461" class="comments-container"></div><div id="comment-tools-30461" class="comment-tools"></div><div class="clear"></div><div id="comment-30461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30483"></span>

<div id="answer-container-30483" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30483-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30483-score" class="post-score" title="current number of votes">0</div><span id="post-30483-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can download Buildbot at <a href="http://buildbot.net/">buildbot.net</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Mar '14, 09:58</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-30483" class="comments-container"><span id="30537"></span><div id="comment-30537" class="comment"><div id="post-30537-score" class="comment-score"></div><div class="comment-text"><p>Oh I mean buildbot scripts of Wireshark. Because I just found source code of Wireshark but not scripts to build Wireshark in Continuous Integration manner.</p></div><div id="comment-30537-info" class="comment-info"><span class="comment-age">(07 Mar '14, 06:25)</span> <span class="comment-user userinfo">prosohard</span></div></div></div><div id="comment-tools-30483" class="comment-tools"></div><div class="clear"></div><div id="comment-30483-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

