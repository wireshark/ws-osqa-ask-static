+++
type = "question"
title = "How does wireshark calculate jitter"
description = '''If I have a piece of test gear connected to a switch stack and it is sending simulated voice traffic and I sniff the up-link port leaving the stack how does wireshark calculate jitter? Seeing as how wireshark does not know what time the packet left my gear.'''
date = "2012-01-20T14:58:00Z"
lastmod = "2012-01-20T14:58:00Z"
weight = 8520
keywords = [ "jitter" ]
aliases = [ "/questions/8520" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How does wireshark calculate jitter](/questions/8520/how-does-wireshark-calculate-jitter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8520-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8520-score" class="post-score" title="current number of votes">0</div><span id="post-8520-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If I have a piece of test gear connected to a switch stack and it is sending simulated voice traffic and I sniff the up-link port leaving the stack how does wireshark calculate jitter? Seeing as how wireshark does not know what time the packet left my gear.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-jitter" rel="tag" title="see questions tagged &#39;jitter&#39;">jitter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '12, 14:58</strong></p><img src="https://secure.gravatar.com/avatar/c789d808f0aa0923a66fda191b0268c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="VoIP%20Ready&#39;s gravatar image" /><p><span>VoIP Ready</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="VoIP Ready has no accepted answers">0%</span></p></div></div><div id="comments-container-8520" class="comments-container"></div><div id="comment-tools-8520" class="comment-tools"></div><div class="clear"></div><div id="comment-8520-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

