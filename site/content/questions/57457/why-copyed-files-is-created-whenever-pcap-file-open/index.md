+++
type = "question"
title = "why copyed files is created whenever pcap file open?"
description = '''Whenever I open pcap file, another same file is created as same name and attached increased number(1,2,3...). I am using wireshark ver 2.2.2 on mac os siera. I don&#x27;t want to create copyed file. please give me how to avoid method. thanks.'''
date = "2016-11-18T20:14:00Z"
lastmod = "2016-11-19T14:41:00Z"
weight = 57457
keywords = [ "files", "duplicated" ]
aliases = [ "/questions/57457" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [why copyed files is created whenever pcap file open?](/questions/57457/why-copyed-files-is-created-whenever-pcap-file-open)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57457-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57457-score" class="post-score" title="current number of votes">0</div><span id="post-57457-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Whenever I open pcap file, another same file is created as same name and attached increased number(1,2,3...). I am using wireshark ver 2.2.2 on mac os siera. I don't want to create copyed file.</p><p>please give me how to avoid method.</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span> <span class="post-tag tag-link-duplicated" rel="tag" title="see questions tagged &#39;duplicated&#39;">duplicated</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '16, 20:14</strong></p><img src="https://secure.gravatar.com/avatar/e3514ba1e376cda6bdb85b7a155b87e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="YJKIM&#39;s gravatar image" /><p><span>YJKIM</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="YJKIM has no accepted answers">0%</span></p></div></div><div id="comments-container-57457" class="comments-container"><span id="57463"></span><div id="comment-57463" class="comment"><div id="post-57463-score" class="comment-score"></div><div class="comment-text"><p>What are the Sharing &amp; Permissions off the capture files? You can see them via the context menu of the file, option Get Info. Do you have Read &amp; Write permission?</p></div><div id="comment-57463-info" class="comment-info"><span class="comment-age">(19 Nov '16, 14:41)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-57457" class="comment-tools"></div><div class="clear"></div><div id="comment-57457-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

