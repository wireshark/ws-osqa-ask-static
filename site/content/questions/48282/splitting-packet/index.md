+++
type = "question"
title = "splitting packet"
description = '''how to split incoming traffic per packet and sub packet and forward in multipath wireless network,please help'''
date = "2015-12-05T03:18:00Z"
lastmod = "2015-12-07T04:54:00Z"
weight = 48282
keywords = [ "122" ]
aliases = [ "/questions/48282" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [splitting packet](/questions/48282/splitting-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48282-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48282-score" class="post-score" title="current number of votes">0</div><span id="post-48282-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to split incoming traffic per packet and sub packet and forward in multipath wireless network,please help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-122" rel="tag" title="see questions tagged &#39;122&#39;">122</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '15, 03:18</strong></p><img src="https://secure.gravatar.com/avatar/e56d11a91b8052bdce5fe8dce6807e4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zeleke%20Wondimu&#39;s gravatar image" /><p><span>Zeleke Wondimu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zeleke Wondimu has no accepted answers">0%</span></p></div></div><div id="comments-container-48282" class="comments-container"></div><div id="comment-tools-48282" class="comment-tools"></div><div class="clear"></div><div id="comment-48282-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48285"></span>

<div id="answer-container-48285" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48285-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48285-score" class="post-score" title="current number of votes">1</div><span id="post-48285-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a packet analyzer, and doesn't manipulate network traffic. If that's the functionality you require then you'll have to look for other tools.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Dec '15, 08:23</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-48285" class="comments-container"><span id="48315"></span><div id="comment-48315" class="comment"><div id="post-48315-score" class="comment-score"></div><div class="comment-text"><p>ok thanks Sir,but I want to simulate splitted traffic per packet and per sub-packet for security purpose on multipath adhoc network,please help me how to do that</p></div><div id="comment-48315-info" class="comment-info"><span class="comment-age">(06 Dec '15, 21:59)</span> <span class="comment-user userinfo">Zeleke Wondimu</span></div></div><span id="48317"></span><div id="comment-48317" class="comment"><div id="post-48317-score" class="comment-score"></div><div class="comment-text"><p>Sounds more like you need Scapy to construct packets, not Wireshark...</p></div><div id="comment-48317-info" class="comment-info"><span class="comment-age">(06 Dec '15, 23:36)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="48322"></span><div id="comment-48322" class="comment"><div id="post-48322-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I want to simulate splitted traffic per packet and per sub-packet for security purpose on multipath adhoc network</p></blockquote><p>I'm sorry, but I don't understand what you are trying to do. Can you please add an example?</p></div><div id="comment-48322-info" class="comment-info"><span class="comment-age">(07 Dec '15, 04:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-48285" class="comment-tools"></div><div class="clear"></div><div id="comment-48285-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

