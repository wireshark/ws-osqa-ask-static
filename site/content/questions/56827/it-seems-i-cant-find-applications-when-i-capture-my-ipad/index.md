+++
type = "question"
title = "[closed] it seems i can&#x27;t find applications when i capture my ipad?"
description = '''hi, when i capture my wifi, local ipad it won&#x27;t show the applications that i have. I really want to change the code of some of my applications on my ipad but still, i find my ipad and look around for the applications but they never show up. Please help me!'''
date = "2016-10-29T20:28:00Z"
lastmod = "2016-10-30T05:43:00Z"
weight = 56827
keywords = [ "ios" ]
aliases = [ "/questions/56827" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] it seems i can't find applications when i capture my ipad?](/questions/56827/it-seems-i-cant-find-applications-when-i-capture-my-ipad)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56827-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56827-score" class="post-score" title="current number of votes">0</div><span id="post-56827-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, when i capture my wifi, local ipad it won't show the applications that i have. I really want to change the code of some of my applications on my ipad but still, i find my ipad and look around for the applications but they never show up. Please help me!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ios" rel="tag" title="see questions tagged &#39;ios&#39;">ios</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '16, 20:28</strong></p><img src="https://secure.gravatar.com/avatar/23c936d736018f14284f2dcdddcc4d37?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zinga&#39;s gravatar image" /><p><span>Zinga</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zinga has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>10 Jan '17, 08:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-56827" class="comments-container"><span id="56830"></span><div id="comment-56830" class="comment"><div id="post-56830-score" class="comment-score"></div><div class="comment-text"><p>What makes you assume that the applications should be transmitted over the air each time you use them? If we leave web applications running in flash or java aside, the code of normal applications is only transported over the network when the application is downloaded (upgraded). But even if you'd want to save the application as it is downloaded, you won't normally see its actual code as an encrypted connection is used for the download.</p></div><div id="comment-56830-info" class="comment-info"><span class="comment-age">(30 Oct '16, 05:43)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56827" class="comment-tools"></div><div class="clear"></div><div id="comment-56827-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by JeffMorriss 10 Jan '17, 08:03

</div>

</div>

</div>

