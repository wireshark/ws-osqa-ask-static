+++
type = "question"
title = "RTP Wrong Timestamps reports in clean capture"
description = '''I have an RTP capture using G711 codec, with all sequence numbers increasing steadily by 1 for each packet, and all timestamps increasing steadily by 160 for each packet. When using the RTP Stream Analysis, Decode, Wireshark reports a number of Wrong Timestamps. Can someone suggest what could cause ...'''
date = "2012-10-27T07:15:00Z"
lastmod = "2012-10-29T04:41:00Z"
weight = 15314
keywords = [ "decode_rtp", "wrong_timestamps" ]
aliases = [ "/questions/15314" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTP Wrong Timestamps reports in clean capture](/questions/15314/rtp-wrong-timestamps-reports-in-clean-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15314-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15314-score" class="post-score" title="current number of votes">0</div><span id="post-15314-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have an RTP capture using G711 codec, with all sequence numbers increasing steadily by 1 for each packet, and all timestamps increasing steadily by 160 for each packet. When using the RTP Stream Analysis, Decode, Wireshark reports a number of Wrong Timestamps.</p><p>Can someone suggest what could cause this behavior?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode_rtp" rel="tag" title="see questions tagged &#39;decode_rtp&#39;">decode_rtp</span> <span class="post-tag tag-link-wrong_timestamps" rel="tag" title="see questions tagged &#39;wrong_timestamps&#39;">wrong_timestamps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '12, 07:15</strong></p><img src="https://secure.gravatar.com/avatar/c5bd9b44290ec4da4e647d228e31facf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="trevron&#39;s gravatar image" /><p><span>trevron</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="trevron has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '12, 13:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-15314" class="comments-container"></div><div id="comment-tools-15314" class="comment-tools"></div><div class="clear"></div><div id="comment-15314-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15331"></span>

<div id="answer-container-15331" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15331-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15331-score" class="post-score" title="current number of votes">0</div><span id="post-15331-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Can someone suggest what could cause this behavior?</p></blockquote><p>a bug in the code.</p><p>Please file a bug report at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a> and attach everything that is required to recreate the problem.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '12, 04:41</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-15331" class="comments-container"></div><div id="comment-tools-15331" class="comment-tools"></div><div class="clear"></div><div id="comment-15331-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

