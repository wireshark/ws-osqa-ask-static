+++
type = "question"
title = "Tshark -w switch"
description = '''Hi I&#x27;m having problems using the -w switch with tshark command (V. 1.12.1). If I use this syntiax: tshark -r &quot;test.pcap&quot; -Y &quot;udp.port == 9090&quot; I get what I want on screen (the output is correct). Instead, if I add the -w switch tshark -r &quot;test.pcap&quot; -Y &quot;udp.port == 9090&quot; -w &quot;out.pcap&quot; The output fil...'''
date = "2014-11-05T06:16:00Z"
lastmod = "2014-11-05T06:16:00Z"
weight = 37587
keywords = [ "output", "-w", "tshark", "file" ]
aliases = [ "/questions/37587" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Tshark -w switch](/questions/37587/tshark-w-switch)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37587-score" class="post-score" title="current number of votes">0</div><span id="post-37587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I'm having problems using the -w switch with tshark command (V. 1.12.1).</p><p>If I use this syntiax: tshark -r "test.pcap" -Y "udp.port == 9090" I get what I want on screen (the output is correct).</p><p>Instead, if I add the -w switch tshark -r "test.pcap" -Y "udp.port == 9090" -w "out.pcap" The output file is empty.</p><p>Any suggestions? AF</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-output" rel="tag" title="see questions tagged &#39;output&#39;">output</span> <span class="post-tag tag-link--w" rel="tag" title="see questions tagged &#39;-w&#39;">-w</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 06:16</strong></p><img src="https://secure.gravatar.com/avatar/ad56b5aa12ff750dea1314e8f1c61702?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AF69&#39;s gravatar image" /><p><span>AF69</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AF69 has no accepted answers">0%</span></p></div></div><div id="comments-container-37587" class="comments-container"></div><div id="comment-tools-37587" class="comment-tools"></div><div class="clear"></div><div id="comment-37587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

