+++
type = "question"
title = "DSCP capture differences for Voip EF"
description = '''Curious of the differences seen in captures of Voip traffic seen: Differentiated Service Field: 0xb8 (DSCP: EF PHB, ECN: Not-ECT) Differntiated Service Field: 0xb8 (DSCP 0x2e: Expedited Forwarding ECN: 0x00 Not-ECT'''
date = "2016-05-04T08:06:00Z"
lastmod = "2016-05-04T08:10:00Z"
weight = 52226
keywords = [ "capture", "differences", "dscp" ]
aliases = [ "/questions/52226" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [DSCP capture differences for Voip EF](/questions/52226/dscp-capture-differences-for-voip-ef)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52226-score" class="post-score" title="current number of votes">0</div><span id="post-52226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Curious of the differences seen in captures of Voip traffic seen:</p><p>Differentiated Service Field: 0xb8 (DSCP: EF PHB, ECN: Not-ECT)</p><p>Differntiated Service Field: 0xb8 (DSCP 0x2e: Expedited Forwarding ECN: 0x00 Not-ECT</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-differences" rel="tag" title="see questions tagged &#39;differences&#39;">differences</span> <span class="post-tag tag-link-dscp" rel="tag" title="see questions tagged &#39;dscp&#39;">dscp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '16, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/e4748b7828addd0d2522759a504b665c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KCO&#39;s gravatar image" /><p><span>KCO</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KCO has no accepted answers">0%</span></p></div></div><div id="comments-container-52226" class="comments-container"></div><div id="comment-tools-52226" class="comment-tools"></div><div class="clear"></div><div id="comment-52226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52227"></span>

<div id="answer-container-52227" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52227-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52227-score" class="post-score" title="current number of votes">0</div><span id="post-52227-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The former is the way Wireshark 2.0 and later is displaying the DSCP field.</p><p>The latter is how the DSCP field was displayed in Wireshark 1.12 or prior.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 May '16, 08:10</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-52227" class="comments-container"></div><div id="comment-tools-52227" class="comment-tools"></div><div class="clear"></div><div id="comment-52227-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

