+++
type = "question"
title = "SharkFest in Europe"
description = '''Have you thought about organizing a SharkFest in Europe?'''
date = "2015-03-22T06:00:00Z"
lastmod = "2016-04-21T15:33:00Z"
weight = 40766
keywords = [ "sharkfest", "europe" ]
aliases = [ "/questions/40766" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [SharkFest in Europe](/questions/40766/sharkfest-in-europe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40766-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40766-score" class="post-score" title="current number of votes">1</div><span id="post-40766-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Have you thought about organizing a SharkFest in Europe?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sharkfest" rel="tag" title="see questions tagged &#39;sharkfest&#39;">sharkfest</span> <span class="post-tag tag-link-europe" rel="tag" title="see questions tagged &#39;europe&#39;">europe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '15, 06:00</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-40766" class="comments-container"></div><div id="comment-tools-40766" class="comment-tools"></div><div class="clear"></div><div id="comment-40766-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40768"></span>

<div id="answer-container-40768" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40768-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40768-score" class="post-score" title="current number of votes">0</div><span id="post-40768-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Roland has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, it has been a topic for a few years now, and as far as I know Sharkfest EU is something we can expect to happen in the future.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Mar '15, 07:47</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40768" class="comments-container"><span id="40774"></span><div id="comment-40774" class="comment"><div id="post-40774-score" class="comment-score"></div><div class="comment-text"><p>+1 for Sharkfest EU.</p></div><div id="comment-40774-info" class="comment-info"><span class="comment-age">(22 Mar '15, 23:22)</span> <span class="comment-user userinfo">Michał Łabędzki</span></div></div><span id="40788"></span><div id="comment-40788" class="comment"><div id="post-40788-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper. Is there anything we can do to speed up the process?</p></div><div id="comment-40788-info" class="comment-info"><span class="comment-age">(23 Mar '15, 12:52)</span> <span class="comment-user userinfo">Roland</span></div></div><span id="40792"></span><div id="comment-40792" class="comment"><div id="post-40792-score" class="comment-score"></div><div class="comment-text"><p>No, it all depends on Riverbed going forward with this...</p></div><div id="comment-40792-info" class="comment-info"><span class="comment-age">(23 Mar '15, 22:47)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="40804"></span><div id="comment-40804" class="comment"><div id="post-40804-score" class="comment-score">1</div><div class="comment-text"><p>On behalf of the Sharkest organizer(s) “Though plans have not yet been finalized, our intention is to announce plans for SharkFest EU at SharkFest’15. We would greatly appreciate any and all suggestions for locations, optimum dates, places to hold the conference, keynote speakers, instructors, topics for courses, and anything else you can think of that would make this a successful educational experience for you.”</p></div><div id="comment-40804-info" class="comment-info"><span class="comment-age">(24 Mar '15, 08:36)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="51849"></span><div id="comment-51849" class="comment"><div id="post-51849-score" class="comment-score">1</div><div class="comment-text"><p>It's happening:</p><p><a href="https://sharkfesteurope.wireshark.org/">https://sharkfesteurope.wireshark.org/</a></p></div><div id="comment-51849-info" class="comment-info"><span class="comment-age">(21 Apr '16, 15:33)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-40768" class="comment-tools"></div><div class="clear"></div><div id="comment-40768-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

