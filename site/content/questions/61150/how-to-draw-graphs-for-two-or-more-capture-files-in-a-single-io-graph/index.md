+++
type = "question"
title = "How to draw graphs for two or more capture files in a single IO graph?"
description = '''Hi, can anyone help me. I want to combine two or more IO graph in one line graph. I already save as &#x27;CSV&#x27; to import it into Excel but the number of packets is inaccurate. Please help me :('''
date = "2017-05-02T01:04:00Z"
lastmod = "2017-05-02T23:33:00Z"
weight = 61150
keywords = [ "iograph" ]
aliases = [ "/questions/61150" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to draw graphs for two or more capture files in a single IO graph?](/questions/61150/how-to-draw-graphs-for-two-or-more-capture-files-in-a-single-io-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61150-score" class="post-score" title="current number of votes">0</div><span id="post-61150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, can anyone help me. I want to combine two or more IO graph in one line graph. I already save as 'CSV' to import it into Excel but the number of packets is inaccurate. Please help me :(</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '17, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/f4b57aeed995805ca0bd4e7cc5803a0b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fiekafy&#39;s gravatar image" /><p><span>fiekafy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fiekafy has no accepted answers">0%</span></p></div></div><div id="comments-container-61150" class="comments-container"><span id="61158"></span><div id="comment-61158" class="comment"><div id="post-61158-score" class="comment-score"></div><div class="comment-text"><p><em>but the number of packets is inaccurate</em></p><p>Please elaborate.</p></div><div id="comment-61158-info" class="comment-info"><span class="comment-age">(02 May '17, 07:20)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="61181"></span><div id="comment-61181" class="comment"><div id="post-61181-score" class="comment-score"></div><div class="comment-text"><p>the graph in Excel is not same as the io graph. so, how to combine two or more IO graph in one line graph?</p></div><div id="comment-61181-info" class="comment-info"><span class="comment-age">(02 May '17, 23:33)</span> <span class="comment-user userinfo">fiekafy</span></div></div></div><div id="comment-tools-61150" class="comment-tools"></div><div class="clear"></div><div id="comment-61150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

