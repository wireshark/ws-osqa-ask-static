+++
type = "question"
title = "install without admin rights"
description = '''Hello, I made my own wireshark installer, but when I try to install my wireshark on a computer without admin rights, I get an issue with vcredist_x86.exe installation, it tells me that i didn&#x27;t have admin rights. At the end of installation when I try to open Wirehsark, nothing&#x27;s opening.  Is there a...'''
date = "2011-02-08T01:46:00Z"
lastmod = "2011-02-10T00:43:00Z"
weight = 2223
keywords = [ "vcredist_x86", "installer" ]
aliases = [ "/questions/2223" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [install without admin rights](/questions/2223/install-without-admin-rights)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2223-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2223-score" class="post-score" title="current number of votes">0</div><span id="post-2223-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I made my own wireshark installer, but when I try to install my wireshark on a computer without admin rights, I get an issue with vcredist_x86.exe installation, it tells me that i didn't have admin rights. At the end of installation when I try to open Wirehsark, nothing's opening. Is there any solution to install vcredist without admin rights? Or is it possible to modify my installer to fix this issue?</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vcredist_x86" rel="tag" title="see questions tagged &#39;vcredist_x86&#39;">vcredist_x86</span> <span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '11, 01:46</strong></p><img src="https://secure.gravatar.com/avatar/ba2f649bff02f743f2c105a41494c0f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alwik&#39;s gravatar image" /><p><span>Alwik</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alwik has one accepted answer">25%</span></p></div></div><div id="comments-container-2223" class="comments-container"></div><div id="comment-tools-2223" class="comment-tools"></div><div class="clear"></div><div id="comment-2223-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2264"></span>

<div id="answer-container-2264" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2264-score" class="post-score" title="current number of votes">0</div><span id="post-2264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Alwik has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I found a solution after 3 days searching on internet. I have to develop with Visual C++ but not the Express Edition.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '11, 00:43</strong></p><img src="https://secure.gravatar.com/avatar/ba2f649bff02f743f2c105a41494c0f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alwik&#39;s gravatar image" /><p><span>Alwik</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alwik has one accepted answer">25%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Feb '11, 00:43</strong> </span></p></div></div><div id="comments-container-2264" class="comments-container"></div><div id="comment-tools-2264" class="comment-tools"></div><div class="clear"></div><div id="comment-2264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

