+++
type = "question"
title = "Bug in Save UDP stream as C arrays?"
description = '''With 2.24 and 2.22, when I follow a UDP stream and save it as C arrays, I get the data apparently repeated, so there is 2x the data that should be there. I capture 64 packets of 8206 bytes each, as displayed in the &quot;Follow UDP Stream dialog&quot;; the &quot;entire conversation&quot; is displayed correctly as 525kB...'''
date = "2017-02-24T15:27:00Z"
lastmod = "2017-02-24T15:27:00Z"
weight = 59675
keywords = [ "follow", "udp" ]
aliases = [ "/questions/59675" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Bug in Save UDP stream as C arrays?](/questions/59675/bug-in-save-udp-stream-as-c-arrays)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59675-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59675-score" class="post-score" title="current number of votes">0</div><span id="post-59675-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>With 2.24 and 2.22, when I follow a UDP stream and save it as C arrays, I get the data apparently repeated, so there is 2x the data that should be there. I capture 64 packets of 8206 bytes each, as displayed in the "Follow UDP Stream dialog"; the "entire conversation" is displayed correctly as 525kB. But when I save it and parse the resultant ASCII file, I get 128 packets. This doesn't happen when I do the same thing with WS 1.6.1</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-follow" rel="tag" title="see questions tagged &#39;follow&#39;">follow</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '17, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/2c95b6c5e5b41d7a765fb7071281d38d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rikraf&#39;s gravatar image" /><p><span>rikraf</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rikraf has no accepted answers">0%</span></p></div></div><div id="comments-container-59675" class="comments-container"></div><div id="comment-tools-59675" class="comment-tools"></div><div class="clear"></div><div id="comment-59675-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

