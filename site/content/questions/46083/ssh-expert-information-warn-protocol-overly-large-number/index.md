+++
type = "question"
title = "[closed] ssh &quot;Expert Information ( Warn / Protocol) : Overly large number&quot;"
description = '''can you explain to me the significance of this warning : &quot;Expert Information ( Warn / Protocol) : Overly large number&quot;'''
date = "2015-09-23T11:22:00Z"
lastmod = "2015-09-23T11:22:00Z"
weight = 46083
keywords = [ "ssh" ]
aliases = [ "/questions/46083" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] ssh "Expert Information ( Warn / Protocol) : Overly large number"](/questions/46083/ssh-expert-information-warn-protocol-overly-large-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46083-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46083-score" class="post-score" title="current number of votes">0</div><span id="post-46083-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can you explain to me the significance of this warning : "Expert Information ( Warn / Protocol) : Overly large number"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssh" rel="tag" title="see questions tagged &#39;ssh&#39;">ssh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '15, 11:22</strong></p><img src="https://secure.gravatar.com/avatar/dd7c2fae79566e8fca3eadf168efe283?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rebanubtd&#39;s gravatar image" /><p><span>rebanubtd</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rebanubtd has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>23 Sep '15, 12:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-46083" class="comments-container"></div><div id="comment-tools-46083" class="comment-tools"></div><div class="clear"></div><div id="comment-46083-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Kurt Knochner 23 Sep '15, 12:57

</div>

</div>

</div>

