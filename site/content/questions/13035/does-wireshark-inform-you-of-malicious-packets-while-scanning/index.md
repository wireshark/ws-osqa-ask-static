+++
type = "question"
title = "Does Wireshark Inform You of Malicious Packets While Scanning?"
description = '''When you have malware infect your network, does Wireshark have any ways of detecting or telling you there are packets transmitting it? I am still learning how WireShark works and all it&#x27;s features. :) Thanks, Eric'''
date = "2012-07-26T14:21:00Z"
lastmod = "2012-07-26T14:24:00Z"
weight = 13035
keywords = [ "malware" ]
aliases = [ "/questions/13035" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Does Wireshark Inform You of Malicious Packets While Scanning?](/questions/13035/does-wireshark-inform-you-of-malicious-packets-while-scanning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13035-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13035-score" class="post-score" title="current number of votes">0</div><span id="post-13035-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When you have malware infect your network, does Wireshark have any ways of detecting or telling you there are packets transmitting it?</p><p>I am still learning how WireShark works and all it's features. :)</p><p>Thanks, Eric</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malware" rel="tag" title="see questions tagged &#39;malware&#39;">malware</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '12, 14:21</strong></p><img src="https://secure.gravatar.com/avatar/1826b847be1b014e888e8a58d6f0a807?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TechnoLion&#39;s gravatar image" /><p><span>TechnoLion</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TechnoLion has no accepted answers">0%</span></p></div></div><div id="comments-container-13035" class="comments-container"></div><div id="comment-tools-13035" class="comment-tools"></div><div class="clear"></div><div id="comment-13035-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13036"></span>

<div id="answer-container-13036" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13036-score" class="post-score" title="current number of votes">2</div><span id="post-13036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="TechnoLion has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a packet analysis tool and as such will display all captured packets, but does nothing to specifically highlight malware packets. An IDS such as <a href="http://www.snort.org/">Snort</a> is the tool for that sort of task.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '12, 14:24</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-13036" class="comments-container"></div><div id="comment-tools-13036" class="comment-tools"></div><div class="clear"></div><div id="comment-13036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

