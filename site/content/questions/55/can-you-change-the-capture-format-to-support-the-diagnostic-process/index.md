+++
type = "question"
title = "Can you change the capture format to support the diagnostic process?"
description = '''Would it be feasible to have options to:  Add HOSTS file information that will travel with the PCAP file as it is sent to vendors, etc. Or, an option to save the file with the current HOSTS file information Add Summary information on the problem. Obfuscate IP addresses for confidentiality reasons (m...'''
date = "2010-09-14T04:36:00Z"
lastmod = "2010-09-15T17:02:00Z"
weight = 55
keywords = [ "capture", "file-format" ]
aliases = [ "/questions/55" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Can you change the capture format to support the diagnostic process?](/questions/55/can-you-change-the-capture-format-to-support-the-diagnostic-process)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55-score" class="post-score" title="current number of votes">0</div><span id="post-55-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Would it be feasible to have options to:</p><ol><li>Add HOSTS file information that will travel with the PCAP file as it is sent to vendors, etc.</li><li>Or, an option to save the file with the current HOSTS file information</li><li>Add Summary information on the problem.</li><li>Obfuscate IP addresses for confidentiality reasons (modify HOSTS IP information (#1) accordingly)</li><li>Add a user initiated record to mark events in a trace with timestamps and user comment</li><li>Add a network trace analyst record to enable permanent highlighting of trace areas with comments</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-file-format" rel="tag" title="see questions tagged &#39;file-format&#39;">file-format</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '10, 04:36</strong></p><img src="https://secure.gravatar.com/avatar/2b54913de7bfd696b930bdc190d8ae90?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gary&#39;s gravatar image" /><p><span>Gary</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gary has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Sep '10, 10:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-55" class="comments-container"></div><div id="comment-tools-55" class="comment-tools"></div><div class="clear"></div><div id="comment-55-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="57"></span>

<div id="answer-container-57" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57-score" class="post-score" title="current number of votes">2</div><span id="post-57-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's what <a href="http://wiki.wireshark.org/Development/PcapNg">pcap-ng</a> envisions to provide. Wireshark only supports a limited subset of its features.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '10, 04:51</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-57" class="comments-container"><span id="126"></span><div id="comment-126" class="comment"><div id="post-126-score" class="comment-score"></div><div class="comment-text"><p>Item 4 isn't a capture format issue - you'd want to obfuscate the actual raw packet data, regardless of the capture format. If Wireshark supported putting address-mapping information in pcap-ng captures, you'd also want to either map the obfuscated addresses to the real names or remove the address-mapping information.</p></div><div id="comment-126-info" class="comment-info"><span class="comment-age">(15 Sep '10, 17:02)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-57" class="comment-tools"></div><div class="clear"></div><div id="comment-57-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="60"></span>

<div id="answer-container-60" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60-score" class="post-score" title="current number of votes">0</div><span id="post-60-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Gary - you can check out pcap-ng information at http://wiki.wireshark.org/Development/PcapNg.</p><p>I think you have a great list going!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '10, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-60" class="comments-container"></div><div id="comment-tools-60" class="comment-tools"></div><div class="clear"></div><div id="comment-60-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

