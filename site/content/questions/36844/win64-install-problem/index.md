+++
type = "question"
title = "win64 install problem"
description = '''Hi, I am downloading wireshark for the first time, and while installing I get the following error: Error opening file for writing: C:&#92;Program Files&#92;Wireshark&#92;uninstall.exe now - I dont have that file because this is the first time I am installing... :-( any suggestions? thanks, Nir'''
date = "2014-10-04T22:58:00Z"
lastmod = "2014-10-05T04:12:00Z"
weight = 36844
keywords = [ "windows8", "install", "wireshark" ]
aliases = [ "/questions/36844" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [win64 install problem](/questions/36844/win64-install-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36844-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36844-score" class="post-score" title="current number of votes">0</div><span id="post-36844-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am downloading wireshark for the first time, and while installing I get the following error:</p><p>Error opening file for writing: C:\Program Files\Wireshark\uninstall.exe</p><p>now - I dont have that file because this is the first time I am installing... :-( any suggestions? thanks, Nir</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '14, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/679babb73a978e9a2061689b8946bd28?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nirtis&#39;s gravatar image" /><p><span>nirtis</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nirtis has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Oct '14, 03:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-36844" class="comments-container"><span id="36847"></span><div id="comment-36847" class="comment"><div id="post-36847-score" class="comment-score"></div><div class="comment-text"><p>From the tags, I guess you are using Win 8, can you be more specific?</p><p>Do you have any AV or endpoint protection installed?</p><p>Have you disabled UAC?</p></div><div id="comment-36847-info" class="comment-info"><span class="comment-age">(05 Oct '14, 03:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-36844" class="comment-tools"></div><div class="clear"></div><div id="comment-36844-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36849"></span>

<div id="answer-container-36849" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36849-score" class="post-score" title="current number of votes">0</div><span id="post-36849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi Graham, yes - I am using windows8... actually I was able to come over it - I just installed it in C:\Program Files wireshark instead of in C:\Program Files dont know why exatly, but it solved the problem. Thanks for helping!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '14, 04:12</strong></p><img src="https://secure.gravatar.com/avatar/679babb73a978e9a2061689b8946bd28?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nirtis&#39;s gravatar image" /><p><span>nirtis</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nirtis has no accepted answers">0%</span></p></div></div><div id="comments-container-36849" class="comments-container"></div><div id="comment-tools-36849" class="comment-tools"></div><div class="clear"></div><div id="comment-36849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

