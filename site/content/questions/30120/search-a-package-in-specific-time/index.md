+++
type = "question"
title = "search a package in specific time"
description = '''Hi there, i have a problem with Wireshark that i would like to find a package (send/receive) in a specific time (e.g. today is 24Feb, but i would like to find packages on 19Feb). At first sight, i see only Wireshark show the result for today only. Please help, thanks in advance'''
date = "2014-02-24T00:54:00Z"
lastmod = "2014-02-24T02:30:00Z"
weight = 30120
keywords = [ "history" ]
aliases = [ "/questions/30120" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [search a package in specific time](/questions/30120/search-a-package-in-specific-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30120-score" class="post-score" title="current number of votes">0</div><span id="post-30120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there, i have a problem with Wireshark that i would like to find a package (send/receive) in a specific time (e.g. today is 24Feb, but i would like to find packages on 19Feb). At first sight, i see only Wireshark show the result for today only. Please help, thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-history" rel="tag" title="see questions tagged &#39;history&#39;">history</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '14, 00:54</strong></p><img src="https://secure.gravatar.com/avatar/45ea006eb863c41d7e84dfbca603258f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nguyen&#39;s gravatar image" /><p><span>Nguyen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nguyen has no accepted answers">0%</span></p></div></div><div id="comments-container-30120" class="comments-container"></div><div id="comment-tools-30120" class="comment-tools"></div><div class="clear"></div><div id="comment-30120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30123"></span>

<div id="answer-container-30123" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30123-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30123-score" class="post-score" title="current number of votes">0</div><span id="post-30123-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>At first sight, i see only <strong>Wireshark show the result for today only</strong></p></blockquote><p>If you started Wireshark at the 19Feb <strong>and left it running</strong>, you will see packets from 19Feb until today.</p><p>However, if you started Wireshark only today (24Feb), you will only see packets from today (24Feb).</p><p>BTW: If I misunderstood your question, please add more details what you are looking for.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '14, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Feb '14, 10:27</strong> </span></p></div></div><div id="comments-container-30123" class="comments-container"></div><div id="comment-tools-30123" class="comment-tools"></div><div class="clear"></div><div id="comment-30123-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

