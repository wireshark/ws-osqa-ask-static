+++
type = "question"
title = "NAS wireshark support"
description = '''Hi,  Is there any wireshark dissector to decode LTE-NAS ciphered messages. Thanks in advance vinodh'''
date = "2010-11-18T05:08:00Z"
lastmod = "2010-11-19T10:27:00Z"
weight = 1005
keywords = [ "lte-nasdessector" ]
aliases = [ "/questions/1005" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [NAS wireshark support](/questions/1005/nas-wireshark-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1005-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1005-score" class="post-score" title="current number of votes">0</div><span id="post-1005-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Is there any wireshark dissector to decode LTE-NAS ciphered messages.</p><p>Thanks in advance vinodh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lte-nasdessector" rel="tag" title="see questions tagged &#39;lte-nasdessector&#39;">lte-nasdessector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '10, 05:08</strong></p><img src="https://secure.gravatar.com/avatar/d81949c5f86433ad6bcbf371b19e13fe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vinodh&#39;s gravatar image" /><p><span>vinodh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vinodh has no accepted answers">0%</span></p></div></div><div id="comments-container-1005" class="comments-container"></div><div id="comment-tools-1005" class="comment-tools"></div><div class="clear"></div><div id="comment-1005-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1025"></span>

<div id="answer-container-1025" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1025-score" class="post-score" title="current number of votes">0</div><span id="post-1025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is a LTE-NAS dissector, packet-nas_eps.c. But it can't do deciphering.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Nov '10, 10:27</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-1025" class="comments-container"></div><div id="comment-tools-1025" class="comment-tools"></div><div class="clear"></div><div id="comment-1025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

