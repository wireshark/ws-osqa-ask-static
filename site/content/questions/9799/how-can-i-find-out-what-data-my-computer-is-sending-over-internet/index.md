+++
type = "question"
title = "How can I find out what Data my computer is sending over internet"
description = '''Hi ! I am new to wireshark and i need to ask a question. My question is: How can I find out what Data my computer is sending over internet with wireshark ? I have an .exe file in my pc and it asks for some info. It sends that info somewhere. Iwould like to know how can i find out were this info goes...'''
date = "2012-03-27T16:21:00Z"
lastmod = "2012-03-27T16:21:00Z"
weight = 9799
keywords = [ "info", "about", "question", "it", "wireshark" ]
aliases = [ "/questions/9799" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I find out what Data my computer is sending over internet](/questions/9799/how-can-i-find-out-what-data-my-computer-is-sending-over-internet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9799-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9799-score" class="post-score" title="current number of votes">0</div><span id="post-9799-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ! I am new to wireshark and i need to ask a question. My question is: How can I find out what Data my computer is sending over internet with wireshark ? I have an .exe file in my pc and it asks for some info. It sends that info somewhere. Iwould like to know how can i find out were this info goes, if that is possible, with wireshark. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-about" rel="tag" title="see questions tagged &#39;about&#39;">about</span> <span class="post-tag tag-link-question" rel="tag" title="see questions tagged &#39;question&#39;">question</span> <span class="post-tag tag-link-it" rel="tag" title="see questions tagged &#39;it&#39;">it</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '12, 16:21</strong></p><img src="https://secure.gravatar.com/avatar/06d419d5a4d91e7837944600d89e6efe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Konstantinos%20Bounatsos&#39;s gravatar image" /><p><span>Konstantinos...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Konstantinos Bounatsos has no accepted answers">0%</span></p></div></div><div id="comments-container-9799" class="comments-container"></div><div id="comment-tools-9799" class="comment-tools"></div><div class="clear"></div><div id="comment-9799-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

