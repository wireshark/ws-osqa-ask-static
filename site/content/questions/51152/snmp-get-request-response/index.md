+++
type = "question"
title = "SNMP Get Request Response"
description = '''Wireshark shows only SNMP Get Request and does not show any Response. The requested data is being plotted on a graph so I know for a fact everything works fine and the response to every get request is transmitted. I am using the following filter: host 192.168.1.10 ,where the IP address is that of th...'''
date = "2016-03-24T06:27:00Z"
lastmod = "2016-03-24T06:32:00Z"
weight = 51152
keywords = [ "udp", "snmpwireshark", "snmp", "response", "wireshark" ]
aliases = [ "/questions/51152" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SNMP Get Request Response](/questions/51152/snmp-get-request-response)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51152-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51152-score" class="post-score" title="current number of votes">0</div><span id="post-51152-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark shows only SNMP Get Request and does not show any Response.</p><p>The requested data is being plotted on a graph so I know for a fact everything works fine and the response to every get request is transmitted.</p><p>I am using the following filter: host 192.168.1.10 ,where the IP address is that of the managed device.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-snmpwireshark" rel="tag" title="see questions tagged &#39;snmpwireshark&#39;">snmpwireshark</span> <span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '16, 06:27</strong></p><img src="https://secure.gravatar.com/avatar/90485544831d473a0cbee8c7310c9106?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiresnmp&#39;s gravatar image" /><p><span>wiresnmp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiresnmp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Mar '16, 06:28</strong> </span></p></div></div><div id="comments-container-51152" class="comments-container"><span id="51153"></span><div id="comment-51153" class="comment"><div id="post-51153-score" class="comment-score"></div><div class="comment-text"><p>Where are you performing the capture in relation to the SNMP request &amp; response originators, and what is the OS on the capture host?</p></div><div id="comment-51153-info" class="comment-info"><span class="comment-age">(24 Mar '16, 06:32)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-51152" class="comment-tools"></div><div class="clear"></div><div id="comment-51152-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

