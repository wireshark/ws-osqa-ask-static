+++
type = "question"
title = "cannot capture packets from local system"
description = '''Start capture Start ping 8.8.8.8 Only see Echo Reply from google. Cannot see Echo from own machine Lenovo T410 with Intel 82577LM Gigabit Network Adaptar'''
date = "2014-06-05T12:57:00Z"
lastmod = "2014-06-06T06:38:00Z"
weight = 33467
keywords = [ "capture", "cannot", "packets" ]
aliases = [ "/questions/33467" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [cannot capture packets from local system](/questions/33467/cannot-capture-packets-from-local-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33467-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33467-score" class="post-score" title="current number of votes">0</div><span id="post-33467-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Start capture Start ping 8.8.8.8 Only see Echo Reply from google. Cannot see Echo from own machine Lenovo T410 with Intel 82577LM Gigabit Network Adaptar</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-cannot" rel="tag" title="see questions tagged &#39;cannot&#39;">cannot</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '14, 12:57</strong></p><img src="https://secure.gravatar.com/avatar/9ea4eb14c6a9edbcba857784535f0bf0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roberto%20Neigenfind&#39;s gravatar image" /><p><span>Roberto Neig...</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roberto Neigenfind has no accepted answers">0%</span></p></div></div><div id="comments-container-33467" class="comments-container"></div><div id="comment-tools-33467" class="comment-tools"></div><div class="clear"></div><div id="comment-33467-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33470"></span>

<div id="answer-container-33470" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33470-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33470-score" class="post-score" title="current number of votes">1</div><span id="post-33470-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Roberto Neigenfind has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see the other questions tagged with "outbound" or "outgoing".</p><blockquote><p><a href="http://ask.wireshark.org/tags/outbound/">http://ask.wireshark.org/tags/outbound/</a></p></blockquote><p>The most likely cause of the problem: Some security software (AV, Firewall, Endpoint Security, VPN client, ect.) installed on the Laptop that prevents Wireshark from seeing the traffic.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jun '14, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-33470" class="comments-container"><span id="33505"></span><div id="comment-33505" class="comment"><div id="post-33505-score" class="comment-score"></div><div class="comment-text"><p>Solved! find and reproduce the error. Wireshard do not work with Dell SonicWALL Global VPN client instaled. Thanks.</p></div><div id="comment-33505-info" class="comment-info"><span class="comment-age">(06 Jun '14, 06:38)</span> <span class="comment-user userinfo">Roberto Neig...</span></div></div></div><div id="comment-tools-33470" class="comment-tools"></div><div class="clear"></div><div id="comment-33470-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

