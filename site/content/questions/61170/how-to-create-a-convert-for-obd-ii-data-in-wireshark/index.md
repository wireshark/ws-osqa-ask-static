+++
type = "question"
title = "How to create a convert for OBD-II data in Wireshark?"
description = '''Hi there, We are trying to stream data from a car&#x27;s OBD-II protocol via Wireshark. It&#x27;s working fine and we get the IDs and data payloads out interpreted as CAN. However, we would like to take it a step further and &quot;scale&quot; the data according to the documentation on wikipedia. This requires that we c...'''
date = "2017-05-02T10:52:00Z"
lastmod = "2017-05-02T10:52:00Z"
weight = 61170
keywords = [ "decode", "capture", "canbus", "obdii", "can" ]
aliases = [ "/questions/61170" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to create a convert for OBD-II data in Wireshark?](/questions/61170/how-to-create-a-convert-for-obd-ii-data-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61170-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61170-score" class="post-score" title="current number of votes">0</div><span id="post-61170-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>We are trying to stream data from a car's OBD-II protocol via Wireshark. It's working fine and we get the IDs and data payloads out interpreted as CAN. However, we would like to take it a step further and "scale" the data according to the documentation on wikipedia.</p><p>This requires that we can use a formula that is dependent on information contained in the ID and in the start of the actual data message.</p><p>Could anyone provide some guidance as to how we can create such a scaling/conversion of the data into readable output using Wireshark? Ideally we would also like to inform the viewer of what data, units etc. they're looking at - we have all this info, but we would just need to find a way to return it depending on the ID.</p><p>Hope you can help - it would be much appreciated! Martin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-canbus" rel="tag" title="see questions tagged &#39;canbus&#39;">canbus</span> <span class="post-tag tag-link-obdii" rel="tag" title="see questions tagged &#39;obdii&#39;">obdii</span> <span class="post-tag tag-link-can" rel="tag" title="see questions tagged &#39;can&#39;">can</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '17, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/bb505f6832bb10125678c300fff66aae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mfcss&#39;s gravatar image" /><p><span>mfcss</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mfcss has no accepted answers">0%</span></p></div></div><div id="comments-container-61170" class="comments-container"></div><div id="comment-tools-61170" class="comment-tools"></div><div class="clear"></div><div id="comment-61170-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

