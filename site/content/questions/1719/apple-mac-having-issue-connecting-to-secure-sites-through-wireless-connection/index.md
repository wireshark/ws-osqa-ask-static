+++
type = "question"
title = "Apple Mac having issue connecting to secure sites through wireless connection"
description = '''I have a customer who just added a metro wireless connection to his office in Los Angeles and is seeing speeds at 10M down and 3M or so up for all machines on the network (30). His Apple Macs, however can not do anything with secured sites such as Paypal or Bank of America etc when accessing them, t...'''
date = "2011-01-12T14:23:00Z"
lastmod = "2011-01-12T22:36:00Z"
weight = 1719
keywords = [ "macintosh", "apple", "sites", "https", "secure" ]
aliases = [ "/questions/1719" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Apple Mac having issue connecting to secure sites through wireless connection](/questions/1719/apple-mac-having-issue-connecting-to-secure-sites-through-wireless-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1719-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1719-score" class="post-score" title="current number of votes">0</div><span id="post-1719-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a customer who just added a metro wireless connection to his office in Los Angeles and is seeing speeds at 10M down and 3M or so up for all machines on the network (30). His Apple Macs, however can not do anything with secured sites such as Paypal or Bank of America etc when accessing them, they just hang. The MACs have no trouble with the same sites when the machines are on a regular DSL or T-1. I am having him put a tap on the host first and then capture the info with WS on his other Windows laptop. What should I be looking for when we start hunting?&gt; MAC OS = 10.39 Thanks Eric</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macintosh" rel="tag" title="see questions tagged &#39;macintosh&#39;">macintosh</span> <span class="post-tag tag-link-apple" rel="tag" title="see questions tagged &#39;apple&#39;">apple</span> <span class="post-tag tag-link-sites" rel="tag" title="see questions tagged &#39;sites&#39;">sites</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span> <span class="post-tag tag-link-secure" rel="tag" title="see questions tagged &#39;secure&#39;">secure</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '11, 14:23</strong></p><img src="https://secure.gravatar.com/avatar/f797bdc41d990dca073837114e048b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EricKnaus&#39;s gravatar image" /><p><span>EricKnaus</span><br />
<span class="score" title="46 reputation points">46</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EricKnaus has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Jan '11, 14:44</strong> </span></p></div></div><div id="comments-container-1719" class="comments-container"></div><div id="comment-tools-1719" class="comment-tools"></div><div class="clear"></div><div id="comment-1719-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1722"></span>

<div id="answer-container-1722" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1722-score" class="post-score" title="current number of votes">0</div><span id="post-1722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>At the <a href="http://www.moserware.com/2009/06/first-few-milliseconds-of-https.html">first few microseconds</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jan '11, 22:36</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1722" class="comments-container"></div><div id="comment-tools-1722" class="comment-tools"></div><div class="clear"></div><div id="comment-1722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

