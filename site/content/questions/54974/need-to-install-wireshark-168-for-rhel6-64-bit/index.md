+++
type = "question"
title = "Need to install Wireshark 1.6.8 for RHEL6-64 Bit"
description = '''Hi,    We are looking for Wireshark version 1.6.8 for RHEL6(64 bit). We could not find it on internet. Can you please help to let know the link from where the installation files can be downloaded.    Currently we have 1.6.8 installed and opens up. But when we go to Capture --&amp;gt; Options, the applic...'''
date = "2016-08-19T05:13:00Z"
lastmod = "2016-08-19T15:00:00Z"
weight = 54974
keywords = [ "rpm", "wireshark_1.6.8" ]
aliases = [ "/questions/54974" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Need to install Wireshark 1.6.8 for RHEL6-64 Bit](/questions/54974/need-to-install-wireshark-168-for-rhel6-64-bit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54974-score" class="post-score" title="current number of votes">0</div><span id="post-54974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><hr /><ol><li><p>We are looking for Wireshark version 1.6.8 for RHEL6(64 bit). We could not find it on internet. Can you please help to let know the link from where the installation files can be downloaded.</p></li><li><p>Currently we have 1.6.8 installed and opens up. But when we go to Capture --&gt; Options, the application closes and below error appears on Linux Terminal.</p><p>$ Gtk-CRITICAL <strong>: gtk_list_store_get_value: assertion VALID_ITER (iter,list_store) failed.<br />
$ Glib-GObject-WARNING</strong> : gtype.c:4181:type id '0' is invalid<br />
$ Glib-GObject-WARNING **: gtype: can't peek value table for type &lt;invalid&gt; which is not currently referenced.</p></li></ol><hr /></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rpm" rel="tag" title="see questions tagged &#39;rpm&#39;">rpm</span> <span class="post-tag tag-link-wireshark_1.6.8" rel="tag" title="see questions tagged &#39;wireshark_1.6.8&#39;">wireshark_1.6.8</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Aug '16, 05:13</strong></p><img src="https://secure.gravatar.com/avatar/ae3a0982626a7d7364296f88956824e2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wireshark_Alok&#39;s gravatar image" /><p><span>Wireshark_Alok</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wireshark_Alok has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Aug '16, 15:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-54974" class="comments-container"></div><div id="comment-tools-54974" class="comment-tools"></div><div class="clear"></div><div id="comment-54974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54994"></span>

<div id="answer-container-54994" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54994-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54994-score" class="post-score" title="current number of votes">0</div><span id="post-54994-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>I'm not aware of a location of an RPM for Wireshark 1.6.8 for RHEL6. According to <a href="http://rpms.famillecollet.com/rpmphp/zoom.php?rpm=wireshark">this list</a> Red Hat never made such a package. If you have such a package you must have gotten it from some 3rd party source (the Wireshark project doesn't distribute RPMs).</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Aug '16, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-54994" class="comments-container"></div><div id="comment-tools-54994" class="comment-tools"></div><div class="clear"></div><div id="comment-54994-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</hr>

</div>

</div>

