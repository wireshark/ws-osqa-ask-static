+++
type = "question"
title = "How to filter out header error IP packets"
description = '''I am using wireshark windows. 2 client connected via one rounter. The client received header error IP packet occasionally. now I am wondering whether wireshark can help to get the detail faulty IP packet. Thanks. '''
date = "2013-03-11T00:14:00Z"
lastmod = "2013-03-11T05:51:00Z"
weight = 19348
keywords = [ "wireshark" ]
aliases = [ "/questions/19348" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to filter out header error IP packets](/questions/19348/how-to-filter-out-header-error-ip-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19348-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19348-score" class="post-score" title="current number of votes">0</div><span id="post-19348-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using wireshark windows. 2 client connected via one rounter. The client received header error IP packet occasionally.</p><p>now I am wondering whether wireshark can help to get the detail faulty IP packet. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '13, 00:14</strong></p><img src="https://secure.gravatar.com/avatar/0ca89f9427887113b2a5e95022730347?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ericsson&#39;s gravatar image" /><p><span>ericsson</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ericsson has no accepted answers">0%</span></p></div></div><div id="comments-container-19348" class="comments-container"><span id="19354"></span><div id="comment-19354" class="comment"><div id="post-19354-score" class="comment-score"></div><div class="comment-text"><p>Maybe, but you'll have to specify what this 'header error IP packet' is.</p></div><div id="comment-19354-info" class="comment-info"><span class="comment-age">(11 Mar '13, 05:51)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-19348" class="comment-tools"></div><div class="clear"></div><div id="comment-19348-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

