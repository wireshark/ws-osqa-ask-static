+++
type = "question"
title = "Matshark function"
description = '''Trying to extract traffic features in matlab.Can i use the matshark function....If yes,how,If not then how? '''
date = "2011-10-14T12:35:00Z"
lastmod = "2011-10-16T23:20:00Z"
weight = 6898
keywords = [ "matshark" ]
aliases = [ "/questions/6898" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Matshark function](/questions/6898/matshark-function)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6898-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6898-score" class="post-score" title="current number of votes">0</div><span id="post-6898-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trying to extract traffic features in matlab.Can i use the matshark function....If yes,how,If not then how?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-matshark" rel="tag" title="see questions tagged &#39;matshark&#39;">matshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Oct '11, 12:35</strong></p><img src="https://secure.gravatar.com/avatar/c953cf9b75fef0837c81691a031c1af7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deepanjan&#39;s gravatar image" /><p><span>deepanjan</span><br />
<span class="score" title="0 reputation points">0</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deepanjan has no accepted answers">0%</span></p></div></div><div id="comments-container-6898" class="comments-container"></div><div id="comment-tools-6898" class="comment-tools"></div><div class="clear"></div><div id="comment-6898-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6913"></span>

<div id="answer-container-6913" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6913-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6913-score" class="post-score" title="current number of votes">0</div><span id="post-6913-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'd need to build it, so it's not just something that you can download and drop in on an arbitrary operating system. See <a href="http://www.wireshark.org/lists/wireshark-users/201011/msg00028.html">the mail message announcing it</a> for details. Whether it'll support extracting the particular stuff you want to extract is another matter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Oct '11, 23:20</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-6913" class="comments-container"></div><div id="comment-tools-6913" class="comment-tools"></div><div class="clear"></div><div id="comment-6913-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

