+++
type = "question"
title = "Saving captured packet with simple hex plain text"
description = '''The normal packet is saved as  +---------+---------------+----------+ 03:48:19,690,420 ETHER |09c|ad|97|84|0e|d1|40|a8|f0|9c|a2|00|08|00|45|00|05|dc|00|84|40|00|34|06|63|a9|2d|21|23|eb|0a|0b|81|d8|01|bb|0e|af|e9|19|43|f4|dd|6f|be|5a|50|10|01|1b|0d|f8|00|00|5f|93|d9|2b|90|80|a6|0d|88|8a|9d|f1|2a|79|e...'''
date = "2015-06-29T20:58:00Z"
lastmod = "2015-06-29T23:17:00Z"
weight = 43697
keywords = [ "plain", "ascii", "hex" ]
aliases = [ "/questions/43697" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Saving captured packet with simple hex plain text](/questions/43697/saving-captured-packet-with-simple-hex-plain-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43697-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43697-score" class="post-score" title="current number of votes">0</div><span id="post-43697-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The normal packet is saved as</p><p>+---------+---------------+----------+ 03:48:19,690,420 ETHER</p><p>|09c|ad|97|84|0e|d1|40|a8|f0|9c|a2|00|08|00|45|00|05|dc|00|84|40|00|34|06|63|a9|2d|21|23|eb|0a|0b|81|d8|01|bb|0e|af|e9|19|43|f4|dd|6f|be|5a|50|10|01|1b|0d|f8|00|00|5f|93|d9|2b|90|80|a6|0d|88|8a|9d|f1|2a|79|e2|8b|21|a4|d6|89|bb|e2|cf|bd|cd|b2|09|95|a3|c6|02|a6|fb|d9|84|aa|7c|d3|c9|a5|12|2c|49|0a|09|e0|86|34|ab|0f|a2|ab|39|9e.....</p><p>But is there any way we can save in plain hex without '|' symbol and other details. Just plain hex like 09cad97840ed14018....</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plain" rel="tag" title="see questions tagged &#39;plain&#39;">plain</span> <span class="post-tag tag-link-ascii" rel="tag" title="see questions tagged &#39;ascii&#39;">ascii</span> <span class="post-tag tag-link-hex" rel="tag" title="see questions tagged &#39;hex&#39;">hex</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '15, 20:58</strong></p><img src="https://secure.gravatar.com/avatar/8c32b0f968805b38539995991867aa11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akseldho&#39;s gravatar image" /><p><span>akseldho</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akseldho has no accepted answers">0%</span></p></div></div><div id="comments-container-43697" class="comments-container"><span id="43704"></span><div id="comment-43704" class="comment"><div id="post-43704-score" class="comment-score"></div><div class="comment-text"><p>why cant you just write a script using sed to replace '|' with '' and drop the other line which starts with a '+'?</p></div><div id="comment-43704-info" class="comment-info"><span class="comment-age">(29 Jun '15, 23:17)</span> <span class="comment-user userinfo">koundi</span></div></div></div><div id="comment-tools-43697" class="comment-tools"></div><div class="clear"></div><div id="comment-43697-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

