+++
type = "question"
title = "Best filters for Hosted VOIP Providers"
description = '''I work for a Hosted VOIP PBX provider and I would like to learn the best way to use Wireshark to diagnose issues with VOIP. I can already setup the packet captures to get all traffic and then use the &quot;VOIP Calls&quot; analysis funtion to see all the calls but I need to be able to also see what is going o...'''
date = "2011-03-23T13:52:00Z"
lastmod = "2011-03-23T14:13:00Z"
weight = 3061
keywords = [ "voip" ]
aliases = [ "/questions/3061" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Best filters for Hosted VOIP Providers](/questions/3061/best-filters-for-hosted-voip-providers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3061-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3061-score" class="post-score" title="current number of votes">0</div><span id="post-3061-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I work for a Hosted VOIP PBX provider and I would like to learn the best way to use Wireshark to diagnose issues with VOIP. I can already setup the packet captures to get all traffic and then use the "VOIP Calls" analysis funtion to see all the calls but I need to be able to also see what is going on behind the customer premise router.<br />
</p><p>What is the best way to achieve this short of placing a hub on the customer premise and taking the capture there?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '11, 13:52</strong></p><img src="https://secure.gravatar.com/avatar/fc4b95ddfb39839dec7e289e17d1ab93?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jtotexas&#39;s gravatar image" /><p><span>jtotexas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jtotexas has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-3061" class="comments-container"></div><div id="comment-tools-3061" class="comment-tools"></div><div class="clear"></div><div id="comment-3061-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3062"></span>

<div id="answer-container-3062" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3062-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3062-score" class="post-score" title="current number of votes">0</div><span id="post-3062-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>"What happens at the customer stays at the customer". Placing a hub is a bad idea, better go with something like <a href="http://www.lovemytool.com/blog/2010/04/review-of-dualcomm-5-port-pass-through-port-mirroring-switch-by-betty-dubois.html">this</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '11, 14:13</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3062" class="comments-container"></div><div id="comment-tools-3062" class="comment-tools"></div><div class="clear"></div><div id="comment-3062-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

