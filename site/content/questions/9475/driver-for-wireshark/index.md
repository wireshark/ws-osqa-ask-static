+++
type = "question"
title = "Driver for wireshark"
description = '''I can&#x27;t wireshark because I need driver for it. Where I an find it to download. Thank you'''
date = "2012-03-11T15:29:00Z"
lastmod = "2012-03-11T15:31:00Z"
weight = 9475
keywords = [ "wireshark" ]
aliases = [ "/questions/9475" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Driver for wireshark](/questions/9475/driver-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9475-score" class="post-score" title="current number of votes">0</div><span id="post-9475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't wireshark because I need driver for it. Where I an find it to download. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '12, 15:29</strong></p><img src="https://secure.gravatar.com/avatar/a6f551a6c5f9cc0533c0f44b33c4d17f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="polman&#39;s gravatar image" /><p><span>polman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="polman has no accepted answers">0%</span></p></div></div><div id="comments-container-9475" class="comments-container"><span id="9476"></span><div id="comment-9476" class="comment"><div id="post-9476-score" class="comment-score"></div><div class="comment-text"><p>Please provide more info (with your question).</p><p>What OS are you using ?</p></div><div id="comment-9476-info" class="comment-info"><span class="comment-age">(11 Mar '12, 15:31)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-9475" class="comment-tools"></div><div class="clear"></div><div id="comment-9475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

