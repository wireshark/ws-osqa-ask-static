+++
type = "question"
title = "After installing Wireshark I can only see 1 interface; not local"
description = '''After installing Wireshark I can only see one interface listed in list. My local is not listed. How can I get the local host to show up  So I can start the analyzer on my local? This is for a class so if anyone can answer my question quickly so I do not get  Behind in my assignments. Any help will b...'''
date = "2016-05-24T18:46:00Z"
lastmod = "2016-05-25T01:30:00Z"
weight = 52889
keywords = [ "interfaces" ]
aliases = [ "/questions/52889" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [After installing Wireshark I can only see 1 interface; not local](/questions/52889/after-installing-wireshark-i-can-only-see-1-interface-not-local)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52889-score" class="post-score" title="current number of votes">0</div><span id="post-52889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installing Wireshark I can only see one interface listed in list. My local is not listed. How can I get the local host to show up So I can start the analyzer on my local? This is for a class so if anyone can answer my question quickly so I do not get Behind in my assignments. Any help will be appreciated.</p><p>Dgreene1051</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '16, 18:46</strong></p><img src="https://secure.gravatar.com/avatar/29e6e887b40ff93d5ca48bb2a1b0ee01?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dgreene1051&#39;s gravatar image" /><p><span>Dgreene1051</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dgreene1051 has no accepted answers">0%</span></p></div></div><div id="comments-container-52889" class="comments-container"><span id="52890"></span><div id="comment-52890" class="comment"><div id="post-52890-score" class="comment-score"></div><div class="comment-text"><p>What operating system?</p></div><div id="comment-52890-info" class="comment-info"><span class="comment-age">(24 May '16, 19:34)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-52889" class="comment-tools"></div><div class="clear"></div><div id="comment-52889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52898"></span>

<div id="answer-container-52898" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52898-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52898-score" class="post-score" title="current number of votes">0</div><span id="post-52898-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Local interface is typically missing at Windows because this OS doesn't use it at all by default. If this is your case, try installing <a href="https://github.com/nmap/npcap/releases">npcap</a> which bundles a virtual local interface driver into its installer, makes Windows use it, and can capture on it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 May '16, 01:30</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-52898" class="comments-container"></div><div id="comment-tools-52898" class="comment-tools"></div><div class="clear"></div><div id="comment-52898-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

