+++
type = "question"
title = "lon protocol is missing from version 2.2.2"
description = '''lon protocol was there from version 1.8 until 2.2.1. Now it is missing after I upgraded from version 2.2.1. I use this every day. is there a way to get it back?'''
date = "2016-11-24T13:00:00Z"
lastmod = "2016-11-24T13:46:00Z"
weight = 57618
keywords = [ "lonworks" ]
aliases = [ "/questions/57618" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [lon protocol is missing from version 2.2.2](/questions/57618/lon-protocol-is-missing-from-version-222)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57618-score" class="post-score" title="current number of votes">0</div><span id="post-57618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>lon protocol was there from version 1.8 until 2.2.1. Now it is missing after I upgraded from version 2.2.1. I use this every day. is there a way to get it back?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lonworks" rel="tag" title="see questions tagged &#39;lonworks&#39;">lonworks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '16, 13:00</strong></p><img src="https://secure.gravatar.com/avatar/a5c43120f7621a8d6b019e09f3d69539?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="larry076&#39;s gravatar image" /><p><span>larry076</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="larry076 has no accepted answers">0%</span></p></div></div><div id="comments-container-57618" class="comments-container"></div><div id="comment-tools-57618" class="comment-tools"></div><div class="clear"></div><div id="comment-57618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57619"></span>

<div id="answer-container-57619" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57619-score" class="post-score" title="current number of votes">0</div><span id="post-57619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The lon dissector was not modified between 2.2.1 and 2.2.2 release, and is still part of the code. Please verify that you have not deactivated the protocol by mistake (Analyze -&gt; Enabled Protocols).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Nov '16, 13:46</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-57619" class="comments-container"></div><div id="comment-tools-57619" class="comment-tools"></div><div class="clear"></div><div id="comment-57619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

