+++
type = "question"
title = "Is there a possibility to compare frames?"
description = '''Hi, i would like to know if somebody knows if there is any script or utility that helps to compare frames in order to see data variations quickly. Thanks in advance. Regards.'''
date = "2010-12-22T03:26:00Z"
lastmod = "2010-12-22T13:23:00Z"
weight = 1451
keywords = [ "frames", "comparison" ]
aliases = [ "/questions/1451" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a possibility to compare frames?](/questions/1451/is-there-a-possibility-to-compare-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1451-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1451-score" class="post-score" title="current number of votes">0</div><span id="post-1451-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i would like to know if somebody knows if there is any script or utility that helps to compare frames in order to see data variations quickly. Thanks in advance. Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frames" rel="tag" title="see questions tagged &#39;frames&#39;">frames</span> <span class="post-tag tag-link-comparison" rel="tag" title="see questions tagged &#39;comparison&#39;">comparison</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Dec '10, 03:26</strong></p><img src="https://secure.gravatar.com/avatar/727846ac427ea3554d7f66eba9657356?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="egunon&#39;s gravatar image" /><p><span>egunon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="egunon has no accepted answers">0%</span></p></div></div><div id="comments-container-1451" class="comments-container"></div><div id="comment-tools-1451" class="comment-tools"></div><div class="clear"></div><div id="comment-1451-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1453"></span>

<div id="answer-container-1453" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1453-score" class="post-score" title="current number of votes">0</div><span id="post-1453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The frame dissector provides the option to calculate an MD5 hash of the frame. You can then compare the hashes of the frames to see if they differ, hence if the frames themselves differ.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Dec '10, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1453" class="comments-container"><span id="1454"></span><div id="comment-1454" class="comment"><div id="post-1454-score" class="comment-score"></div><div class="comment-text"><p>Thank you for the answer but this will not help me because i need a bit to bit comparison from de Data of the frame. (evaluating individual signals in an industrial protocol). Regards.</p></div><div id="comment-1454-info" class="comment-info"><span class="comment-age">(22 Dec '10, 06:33)</span> <span class="comment-user userinfo">egunon</span></div></div></div><div id="comment-tools-1453" class="comment-tools"></div><div class="clear"></div><div id="comment-1453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1455"></span>

<div id="answer-container-1455" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1455-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1455-score" class="post-score" title="current number of votes">0</div><span id="post-1455-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That actually sounds like a fun little project if it doesn't exist. You might be able to use the perl NET::PCAP library to ingest the file, then make the necessary comparison at the offset you desire. You could render the differences and frame numbers into html. I wish I had more time (and PERL knowledge) so I could give you more direction. Maybe someone else knows of something already built (or something that could be easily adapted).</p><p>I don't know all of your specifics, but maybe a dissector could be written for the industrial protocol you are analyzing and do away with the need to compare.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Dec '10, 13:23</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span> </br></p></div></div><div id="comments-container-1455" class="comments-container"></div><div id="comment-tools-1455" class="comment-tools"></div><div class="clear"></div><div id="comment-1455-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

