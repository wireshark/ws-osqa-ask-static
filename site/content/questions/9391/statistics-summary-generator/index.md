+++
type = "question"
title = "statistics summary generator"
description = '''hello experts, i&#x27;m trying to generate an option in wireshark for creating statistics summary as a text file.please kindly help what should i do to parse this data.where can i find this code in source .. regards, sangmeshcp@gmail.com'''
date = "2012-03-06T04:21:00Z"
lastmod = "2012-03-06T04:21:00Z"
weight = 9391
keywords = [ "menu", "statistics", "summary" ]
aliases = [ "/questions/9391" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [statistics summary generator](/questions/9391/statistics-summary-generator)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9391-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9391-score" class="post-score" title="current number of votes">0</div><span id="post-9391-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello experts,</p><p>i'm trying to generate an option in wireshark for creating statistics summary as a text file.please kindly help what should i do to parse this data.where can i find this code in source ..</p><p>regards, <span class="__cf_email__" data-cfemail="89fae8e7eee4ecfae1eaf9c9">[email protected]</span><a href="http://gmail.com">gmail.com</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-menu" rel="tag" title="see questions tagged &#39;menu&#39;">menu</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-summary" rel="tag" title="see questions tagged &#39;summary&#39;">summary</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '12, 04:21</strong></p><img src="https://secure.gravatar.com/avatar/6cb6685f12bd537f0f2e1e86a591e940?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sangmeshp&#39;s gravatar image" /><p><span>sangmeshp</span><br />
<span class="score" title="36 reputation points">36</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sangmeshp has no accepted answers">0%</span></p></div></div><div id="comments-container-9391" class="comments-container"></div><div id="comment-tools-9391" class="comment-tools"></div><div class="clear"></div><div id="comment-9391-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

