+++
type = "question"
title = "How to calculate jitter using HTTP streaming server"
description = '''Hello Everyone! I am using VLC player as a streaming server and captured traffic using tcp.port==8080. Now I am confused about how to calculate jitter though in RTP it is easy to calculate jitter (Telephony-&amp;gt;RTP-&amp;gt;Stream Analysis. I injected delay of 20ms on the network so no point of RTT calcu...'''
date = "2011-08-25T13:57:00Z"
lastmod = "2011-08-25T13:57:00Z"
weight = 5874
keywords = [ "http", "jitter" ]
aliases = [ "/questions/5874" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to calculate jitter using HTTP streaming server](/questions/5874/how-to-calculate-jitter-using-http-streaming-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5874-score" class="post-score" title="current number of votes">0</div><span id="post-5874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Everyone!</p><p>I am using VLC player as a streaming server and captured traffic using tcp.port==8080. Now I am confused about how to calculate jitter though in RTP it is easy to calculate jitter (Telephony-&gt;RTP-&gt;Stream Analysis. I injected delay of 20ms on the network so no point of RTT calculation. If anyone here to illustrate the best and easiest (should be accurate) -:) way to calculate the same please revert ASAP.</p><p>Thanks,</p><p>Sur</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-jitter" rel="tag" title="see questions tagged &#39;jitter&#39;">jitter</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '11, 13:57</strong></p><img src="https://secure.gravatar.com/avatar/e76e585ac43bd38c430e58e73fec680d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sur&#39;s gravatar image" /><p><span>sur</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sur has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Aug '11, 14:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5874" class="comments-container"></div><div id="comment-tools-5874" class="comment-tools"></div><div class="clear"></div><div id="comment-5874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

