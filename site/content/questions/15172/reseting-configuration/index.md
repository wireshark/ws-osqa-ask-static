+++
type = "question"
title = "Reseting configuration"
description = '''Hi. I have installed Wireshark on a Mac with OS X 10.8. At first run it asked for the X11 app... and I selected by mistake the Automator app. The problem now is that every time I run the Wireshark app it opens X11 (that it finally found without asking again) and the Automator apps (!!!)... I have un...'''
date = "2012-10-22T11:26:00Z"
lastmod = "2012-10-22T11:26:00Z"
weight = 15172
keywords = [ "macosx", "mac", "configuration", "config", "macintosh" ]
aliases = [ "/questions/15172" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Reseting configuration](/questions/15172/reseting-configuration)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15172-score" class="post-score" title="current number of votes">0</div><span id="post-15172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. I have installed Wireshark on a Mac with OS X 10.8. At first run it asked for the X11 app... and I selected by mistake the Automator app. The problem now is that every time I run the Wireshark app it opens X11 (that it finally found without asking again) and the Automator apps (!!!)... I have uninstalled/installed Wireshark many many times with no results... How can I delete all the settings so it will ask again for X11? The X11 was renamed to XQuartz and that's why it didn't found it in the first place.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span> <span class="post-tag tag-link-config" rel="tag" title="see questions tagged &#39;config&#39;">config</span> <span class="post-tag tag-link-macintosh" rel="tag" title="see questions tagged &#39;macintosh&#39;">macintosh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '12, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/dc7ecdce0d0f8b2bd0a87ef16db65338?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lordLEX&#39;s gravatar image" /><p><span>lordLEX</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lordLEX has no accepted answers">0%</span></p></div></div><div id="comments-container-15172" class="comments-container"></div><div id="comment-tools-15172" class="comment-tools"></div><div class="clear"></div><div id="comment-15172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

