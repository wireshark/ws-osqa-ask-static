+++
type = "question"
title = "New to Wireshark-Reading Captures"
description = '''I&#x27;m finding a lot of excellent material online and ebooks on how to use wireshark from beginner to advanced but I am not finding anything on how to actually read the information in the captures. Are there any good resources online or ebooks that actually help someone to become literate in reading wh...'''
date = "2014-12-31T20:32:00Z"
lastmod = "2015-01-02T04:22:00Z"
weight = 38838
keywords = [ "captures", "reading", "packets" ]
aliases = [ "/questions/38838" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [New to Wireshark-Reading Captures](/questions/38838/new-to-wireshark-reading-captures)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38838-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38838-score" class="post-score" title="current number of votes">0</div><span id="post-38838-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm finding a lot of excellent material online and ebooks on how to use wireshark from beginner to advanced but I am not finding anything on how to actually read the information in the captures.</p><p>Are there any good resources online or ebooks that actually help someone to become literate in reading what all of that packet information is actually saying and what to look out for?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-captures" rel="tag" title="see questions tagged &#39;captures&#39;">captures</span> <span class="post-tag tag-link-reading" rel="tag" title="see questions tagged &#39;reading&#39;">reading</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '14, 20:32</strong></p><img src="https://secure.gravatar.com/avatar/3f10433ccd325de88e0ea2aaf8d55118?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="araKnid&#39;s gravatar image" /><p><span>araKnid</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="araKnid has no accepted answers">0%</span></p></div></div><div id="comments-container-38838" class="comments-container"></div><div id="comment-tools-38838" class="comment-tools"></div><div class="clear"></div><div id="comment-38838-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38853"></span>

<div id="answer-container-38853" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38853-score" class="post-score" title="current number of votes">0</div><span id="post-38853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's a very big pile of information you intend to swallow there. Are you really trying to understand over a thousand protocols that Wireshark can handle? I guess not. If you want to start with the most common ones (TCP/IP and friends) then I suggest to get a hold of some reading material like O'Reilly's Internet Core Protocols: The Definitive Guide, or Steven's TCP/IP Illustrated, Vol. 1: The Protocols, or the online TCP/IP guide. If that's done you should be able to base further study on the specific protocols you're faced with.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jan '15, 04:22</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-38853" class="comments-container"></div><div id="comment-tools-38853" class="comment-tools"></div><div class="clear"></div><div id="comment-38853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

