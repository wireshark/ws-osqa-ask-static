+++
type = "question"
title = "Wireshark with maveriks 10.9.5"
description = '''Hi all, i tried to install Wireshark on my mac book air. I follow the instruction, i installed Xquartz and Wireshark. I runned for first Xquartz and after from terminal of X11sudo ./wireshark i choosed Xquartz, but Wireshark doesn&#x27;t starts and i receive this error message:  **The domain/default pair...'''
date = "2015-04-02T03:54:00Z"
lastmod = "2015-04-06T10:34:00Z"
weight = 41138
keywords = [ "gtk-warning", "mavericks" ]
aliases = [ "/questions/41138" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark with maveriks 10.9.5](/questions/41138/wireshark-with-maveriks-1095)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41138-score" class="post-score" title="current number of votes">0</div><span id="post-41138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>i tried to install Wireshark on my mac book air. I follow the instruction, i installed Xquartz and Wireshark. I runned for first Xquartz and after from terminal of X11<code>sudo ./wireshark</code> i choosed Xquartz, but Wireshark doesn't starts and i receive this error message:</p><pre><code>**The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist
2015-04-02 12:37:13.752 defaults[14030:507]**
**The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist**

**(process:14012): Gtk-WARNING** **: Locale not supported by C library.**
        **Using the fallback &#39;C&#39; locale.**

**(wireshark-bin:14012): Gtk-WARNING: Unable to locate theme engine in module_path: &quot;clearlooks&quot;,**</code></pre><p>I've the last version of both products (xQuart 2.2.7) (Wireshark stable release 1.12.4).</p><p>I read this post <a href="https://ask.wireshark.org/questions/27683/cannot-run-wireshark-on-mac-os-mavericks,">https://ask.wireshark.org/questions/27683/cannot-run-wireshark-on-mac-os-mavericks,</a> but i've already the lastest versions.</p><p>Somebody can help me? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtk-warning" rel="tag" title="see questions tagged &#39;gtk-warning&#39;">gtk-warning</span> <span class="post-tag tag-link-mavericks" rel="tag" title="see questions tagged &#39;mavericks&#39;">mavericks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '15, 03:54</strong></p><img src="https://secure.gravatar.com/avatar/650ad6b9653dd3e69f35a75ab8438afb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andry79&#39;s gravatar image" /><p><span>andry79</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andry79 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Apr '15, 05:26</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-41138" class="comments-container"><span id="41190"></span><div id="comment-41190" class="comment"><div id="post-41190-score" class="comment-score"></div><div class="comment-text"><p>Can you try the "Development Release" at <a href="https://www.wireshark.org/download.html?">https://www.wireshark.org/download.html?</a> These are more "native" in OS X.</p></div><div id="comment-41190-info" class="comment-info"><span class="comment-age">(04 Apr '15, 10:23)</span> <span class="comment-user userinfo">Lekensteyn</span></div></div><span id="41228"></span><div id="comment-41228" class="comment"><div id="post-41228-score" class="comment-score"></div><div class="comment-text"><p>It works. Thank you so much.</p></div><div id="comment-41228-info" class="comment-info"><span class="comment-age">(06 Apr '15, 10:34)</span> <span class="comment-user userinfo">andry79</span></div></div></div><div id="comment-tools-41138" class="comment-tools"></div><div class="clear"></div><div id="comment-41138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

