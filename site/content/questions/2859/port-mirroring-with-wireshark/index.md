+++
type = "question"
title = "port mirroring with wireshark"
description = '''Hello,  I have a school project to do and the instructor recommended as one of the topics &quot;port mirroring with wireshark&quot;. Can somebody please explain me how i will go about doing this so that i can decide whether i want to do this topic or not. Thanks a lot.'''
date = "2011-03-15T19:36:00Z"
lastmod = "2011-03-16T00:46:00Z"
weight = 2859
keywords = [ "port", "mirroring", "wireshark" ]
aliases = [ "/questions/2859" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [port mirroring with wireshark](/questions/2859/port-mirroring-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2859-score" class="post-score" title="current number of votes">0</div><span id="post-2859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I have a school project to do and the instructor recommended as one of the topics "port mirroring with wireshark". Can somebody please explain me how i will go about doing this so that i can decide whether i want to do this topic or not. Thanks a lot.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '11, 19:36</strong></p><img src="https://secure.gravatar.com/avatar/d9b914980af8ee13b386c9582213facc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mtomey&#39;s gravatar image" /><p><span>mtomey</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mtomey has no accepted answers">0%</span></p></div></div><div id="comments-container-2859" class="comments-container"><span id="2862"></span><div id="comment-2862" class="comment"><div id="post-2862-score" class="comment-score">1</div><div class="comment-text"><p>If your instructor really said "port mirroring with Wireshark" he should check his own terminology :-)</p><p>Wireshark can be used to capture network data at a network switch that copies the ports of interest to a mirror/monitor port. It is not Wireshark doing the mirroring, it's the switch.</p></div><div id="comment-2862-info" class="comment-info"><span class="comment-age">(16 Mar '11, 00:43)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2859" class="comment-tools"></div><div class="clear"></div><div id="comment-2859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2860"></span>

<div id="answer-container-2860" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2860-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2860-score" class="post-score" title="current number of votes">1</div><span id="post-2860-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could have a look <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '11, 00:30</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2860" class="comments-container"><span id="2863"></span><div id="comment-2863" class="comment"><div id="post-2863-score" class="comment-score">1</div><div class="comment-text"><p>The one thing that annoys me a bit about that wiki page is that it lists stuff like "Man in the Middle" and "MAC flooding" as methods to capture data - that are clearly techniques that should never ever be used in a corporate network to capture data. It's clearly black hat stuff :-)</p></div><div id="comment-2863-info" class="comment-info"><span class="comment-age">(16 Mar '11, 00:46)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2860" class="comment-tools"></div><div class="clear"></div><div id="comment-2860-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

