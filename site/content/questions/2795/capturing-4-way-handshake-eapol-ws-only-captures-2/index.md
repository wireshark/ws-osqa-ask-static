+++
type = "question"
title = "Capturing 4 way handshake (EAPOL) - WS only captures 2?"
description = '''I am able to decrypt and view all of my own IEEE 802.11 Packets by capturing the 4 EAPOL packets when I connect to a wpa-psk network and by adjusting the preferences. Now my question/problem is that when i connect to the network using another computer while wireshark is capturing on my pc, Wireshark...'''
date = "2011-03-13T19:30:00Z"
lastmod = "2011-03-13T19:30:00Z"
weight = 2795
keywords = [ "eapol", "handshake", "wpa", "way", "4" ]
aliases = [ "/questions/2795" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing 4 way handshake (EAPOL) - WS only captures 2?](/questions/2795/capturing-4-way-handshake-eapol-ws-only-captures-2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2795-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2795-score" class="post-score" title="current number of votes">1</div><span id="post-2795-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I am able to decrypt and view all of my own IEEE 802.11 Packets by capturing the 4 EAPOL packets when I connect to a wpa-psk network and by adjusting the preferences.</p><p>Now my question/problem is that when i connect to the network using another computer while wireshark is capturing on my pc, Wireshark only captures 2 EAPOL packets. I have tried this many times with 2 other computers, and the same thing occurs where I only capture 2 EAPOL packets. And Wireshark can only decrypt the packets to SSDP.</p><p>Is there a reason or workaround for this because I do not understand why I can only capture 2 eapol packets? Thank you for any help or tips.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span> <span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span> <span class="post-tag tag-link-way" rel="tag" title="see questions tagged &#39;way&#39;">way</span> <span class="post-tag tag-link-4" rel="tag" title="see questions tagged &#39;4&#39;">4</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '11, 19:30</strong></p><img src="https://secure.gravatar.com/avatar/b694babf941d2907ad3641400963aa54?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="findingnemo&#39;s gravatar image" /><p><span>findingnemo</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="findingnemo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Mar '11, 19:30</strong> </span></p></div></div><div id="comments-container-2795" class="comments-container"></div><div id="comment-tools-2795" class="comment-tools"></div><div class="clear"></div><div id="comment-2795-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

