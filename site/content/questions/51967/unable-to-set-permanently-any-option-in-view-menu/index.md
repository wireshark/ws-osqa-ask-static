+++
type = "question"
title = "Unable to set permanently any option in View menu"
description = '''Hi,  I have a strange problem in Wireshark 2.0.2 and 2.0.3. Whatever I set in the View menu (for example uncheck View -&amp;gt; Packet Bytes), it is coming back to default setting (checked) after closing Wireshark and starting up again. This problem exists on almost every option in View menu.  Please he...'''
date = "2016-04-26T09:33:00Z"
lastmod = "2016-04-26T09:43:00Z"
weight = 51967
keywords = [ "menu", "2.0.3", "2.0.2", "view" ]
aliases = [ "/questions/51967" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to set permanently any option in View menu](/questions/51967/unable-to-set-permanently-any-option-in-view-menu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51967-score" class="post-score" title="current number of votes">0</div><span id="post-51967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have a strange problem in Wireshark 2.0.2 and 2.0.3. Whatever I set in the View menu (for example uncheck View -&gt; Packet Bytes), it is coming back to default setting (checked) after closing Wireshark and starting up again.</p><p>This problem exists on almost every option in View menu.</p><p>Please help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-menu" rel="tag" title="see questions tagged &#39;menu&#39;">menu</span> <span class="post-tag tag-link-2.0.3" rel="tag" title="see questions tagged &#39;2.0.3&#39;">2.0.3</span> <span class="post-tag tag-link-2.0.2" rel="tag" title="see questions tagged &#39;2.0.2&#39;">2.0.2</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '16, 09:33</strong></p><img src="https://secure.gravatar.com/avatar/88998e14e6510d743a3c53ec00b64d25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kontranavoj&#39;s gravatar image" /><p><span>Kontranavoj</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kontranavoj has no accepted answers">0%</span></p></div></div><div id="comments-container-51967" class="comments-container"></div><div id="comment-tools-51967" class="comment-tools"></div><div class="clear"></div><div id="comment-51967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51968"></span>

<div id="answer-container-51968" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51968-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51968-score" class="post-score" title="current number of votes">0</div><span id="post-51968-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looks like a bug to me (I have just verified that behavior in a more recent developer build). It looks like the view setting isn't saved to the profile (or restored) correctly. Please open a <a href="https://bugs.wireshark.org">bug report</a> for this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '16, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-51968" class="comments-container"></div><div id="comment-tools-51968" class="comment-tools"></div><div class="clear"></div><div id="comment-51968-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

