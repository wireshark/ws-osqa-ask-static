+++
type = "question"
title = "Hub Issues"
description = '''Hi, I am trying to use a different router on my sky account and need to obtain my username &amp;amp; password. I have followed the instructions on how to do this but seem to be missing the &quot;DHCP Discover&quot; packets? The only ones that show up are &quot;Request&quot; &amp;amp; &quot;ACK&quot; packets. Can someone please point me ...'''
date = "2015-04-26T10:53:00Z"
lastmod = "2015-04-26T10:53:00Z"
weight = 41862
keywords = [ "his", "pingbor" ]
aliases = [ "/questions/41862" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Hub Issues](/questions/41862/hub-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41862-score" class="post-score" title="current number of votes">0</div><span id="post-41862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to use a different router on my sky account and need to obtain my username &amp; password. I have followed the instructions on how to do this but seem to be missing the "DHCP Discover" packets? The only ones that show up are "Request" &amp; "ACK" packets.</p><p>Can someone please point me in the right direction.</p><p>Many thanks</p><p>Paul</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-his" rel="tag" title="see questions tagged &#39;his&#39;">his</span> <span class="post-tag tag-link-pingbor" rel="tag" title="see questions tagged &#39;pingbor&#39;">pingbor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '15, 10:53</strong></p><img src="https://secure.gravatar.com/avatar/c20e4fff21b4ddab82d2152ed33537ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Waldron&#39;s gravatar image" /><p><span>Paul Waldron</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Waldron has no accepted answers">0%</span></p></div></div><div id="comments-container-41862" class="comments-container"></div><div id="comment-tools-41862" class="comment-tools"></div><div class="clear"></div><div id="comment-41862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

