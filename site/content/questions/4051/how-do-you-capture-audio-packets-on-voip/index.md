+++
type = "question"
title = "How do you capture audio packets on VOIP"
description = '''I am having intermittent problems with no audio on VOIP call between 2 Samsung office serve systems that are running over a MPLS.'''
date = "2011-05-12T05:10:00Z"
lastmod = "2011-05-12T05:10:00Z"
weight = 4051
keywords = [ "voip" ]
aliases = [ "/questions/4051" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do you capture audio packets on VOIP](/questions/4051/how-do-you-capture-audio-packets-on-voip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4051-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4051-score" class="post-score" title="current number of votes">0</div><span id="post-4051-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am having intermittent problems with no audio on VOIP call between 2 Samsung office serve systems that are running over a MPLS.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '11, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/7fdbeec981bb2b1ffc7be04c453741b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="team38&#39;s gravatar image" /><p><span>team38</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="team38 has no accepted answers">0%</span></p></div></div><div id="comments-container-4051" class="comments-container"></div><div id="comment-tools-4051" class="comment-tools"></div><div class="clear"></div><div id="comment-4051-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

