+++
type = "question"
title = "dissector for Cisco vPC protocols"
description = '''Has anyone seen a dissector for Cisco&#x27;s vPC protocols? I want to see what vPC peers are saying to each other, particularly during configuration changes. --sk'''
date = "2016-04-17T06:18:00Z"
lastmod = "2016-04-17T15:58:00Z"
weight = 51728
keywords = [ "dissector", "cisco", "vpc" ]
aliases = [ "/questions/51728" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [dissector for Cisco vPC protocols](/questions/51728/dissector-for-cisco-vpc-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51728-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51728-score" class="post-score" title="current number of votes">0</div><span id="post-51728-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anyone seen a dissector for Cisco's vPC protocols? I want to see what vPC peers are saying to each other, particularly during configuration changes.</p><p>--sk</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-vpc" rel="tag" title="see questions tagged &#39;vpc&#39;">vpc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '16, 06:18</strong></p><img src="https://secure.gravatar.com/avatar/18ae5b8bfddad49931ec557b9342075a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skendric&#39;s gravatar image" /><p><span>skendric</span><br />
<span class="score" title="11 reputation points">11</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skendric has no accepted answers">0%</span></p></div></div><div id="comments-container-51728" class="comments-container"><span id="51737"></span><div id="comment-51737" class="comment"><div id="post-51737-score" class="comment-score"></div><div class="comment-text"><p>Has anyone seen a <em>specification</em> for Cisco's vPC protocols? Without that, it'd be a lot harder to dissect them.</p></div><div id="comment-51737-info" class="comment-info"><span class="comment-age">(17 Apr '16, 15:58)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-51728" class="comment-tools"></div><div class="clear"></div><div id="comment-51728-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

