+++
type = "question"
title = "Is it possible to have preferences for a Lua listener?"
description = '''I have a listener that works with my dissector for a specific protocol. For the dissector I enabled preferences so the user can configure their settings.  The listener would use the same settings as the dissector, but currently I don&#x27;t know how the user can configure preferences for a listener, or s...'''
date = "2017-05-08T09:39:00Z"
lastmod = "2017-05-08T09:39:00Z"
weight = 61289
keywords = [ "listener", "lua", "dissector", "preferences" ]
aliases = [ "/questions/61289" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to have preferences for a Lua listener?](/questions/61289/is-it-possible-to-have-preferences-for-a-lua-listener)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61289-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61289-score" class="post-score" title="current number of votes">0</div><span id="post-61289-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a listener that works with my dissector for a specific protocol. For the dissector I enabled preferences so the user can configure their settings.</p><p>The listener would use the same settings as the dissector, but currently I don't know how the user can configure preferences for a listener, or share the settings. Right now I'm manually changing the values in the listener script, but I would like to do it through Wireshark.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-listener" rel="tag" title="see questions tagged &#39;listener&#39;">listener</span> <span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '17, 09:39</strong></p><img src="https://secure.gravatar.com/avatar/00cd850e8d2944c2c7dcdc13baf50a81?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Irfan%20Hossain&#39;s gravatar image" /><p><span>Irfan Hossain</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Irfan Hossain has no accepted answers">0%</span></p></div></div><div id="comments-container-61289" class="comments-container"></div><div id="comment-tools-61289" class="comment-tools"></div><div class="clear"></div><div id="comment-61289-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

