+++
type = "question"
title = "Save what i have downloaded"
description = '''The title is a little vague(sorry for that)- I have two computers which are plugged into the same router. Now i have arp poisoned my router so whatever goes through it is captured in my wireshark terminal. What i need to to is the following- If a user on the 2nd computer a downloads a 1mb file(progr...'''
date = "2012-04-20T22:20:00Z"
lastmod = "2012-04-21T04:03:00Z"
weight = 10367
keywords = [ "wireshark" ]
aliases = [ "/questions/10367" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Save what i have downloaded](/questions/10367/save-what-i-have-downloaded)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10367-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10367-score" class="post-score" title="current number of votes">0</div><span id="post-10367-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The title is a little vague(sorry for that)- I have two computers which are plugged into the same router. Now i have arp poisoned my router so whatever goes through it is captured in my wireshark terminal. What i need to to is the following- If a user on the 2nd computer a downloads a 1mb file(program), I want to be able to see the exact file that he downloaded-That is i want to have a copy of the file that he downloaded. I want to know if this is possible to know and if so how i would go about doing it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '12, 22:20</strong></p><img src="https://secure.gravatar.com/avatar/110009a3f82707f54b04f7ef73b9d32b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Developer&#39;s gravatar image" /><p><span>Developer</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Developer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '12, 22:21</strong> </span></p></div></div><div id="comments-container-10367" class="comments-container"></div><div id="comment-tools-10367" class="comment-tools"></div><div class="clear"></div><div id="comment-10367-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10375"></span>

<div id="answer-container-10375" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10375-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10375-score" class="post-score" title="current number of votes">0</div><span id="post-10375-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If the download is done via HTTP or SMB you could just export the file using the File -&gt; Export -&gt; Objects menu (you might have to re-enable TCP packet reassembly if you turned it off, default is turned on). Otherwise you could try exporting the payload by using the "Follow TCP Stream" popup menu item on the communication that contains the file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Apr '12, 04:03</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-10375" class="comments-container"></div><div id="comment-tools-10375" class="comment-tools"></div><div class="clear"></div><div id="comment-10375-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

