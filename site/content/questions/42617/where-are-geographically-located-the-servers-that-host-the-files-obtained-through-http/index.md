+++
type = "question"
title = "Where are geographically located the servers that host the files obtained through HTTP?"
description = '''Hi everyone, i am new in Wireshark, i want to know Where are geographically located the servers that host the files obtained through HTTP?'''
date = "2015-05-22T10:23:00Z"
lastmod = "2015-05-22T11:40:00Z"
weight = 42617
keywords = [ "wireshark" ]
aliases = [ "/questions/42617" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where are geographically located the servers that host the files obtained through HTTP?](/questions/42617/where-are-geographically-located-the-servers-that-host-the-files-obtained-through-http)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42617-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42617-score" class="post-score" title="current number of votes">0</div><span id="post-42617-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone, i am new in Wireshark, i want to know Where are geographically located the servers that host the files obtained through HTTP?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '15, 10:23</strong></p><img src="https://secure.gravatar.com/avatar/280c3f22ec8de7b919785632284f0935?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samira&#39;s gravatar image" /><p><span>samira</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samira has no accepted answers">0%</span></p></div></div><div id="comments-container-42617" class="comments-container"></div><div id="comment-tools-42617" class="comment-tools"></div><div class="clear"></div><div id="comment-42617-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42618"></span>

<div id="answer-container-42618" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42618-score" class="post-score" title="current number of votes">2</div><span id="post-42618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Check out this wiki:</p><p><a href="https://wiki.wireshark.org/HowToUseGeoIP">https://wiki.wireshark.org/HowToUseGeoIP</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '15, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-42618" class="comments-container"><span id="42619"></span><div id="comment-42619" class="comment"><div id="post-42619-score" class="comment-score"></div><div class="comment-text"><p>thanks for your reply. it was great</p></div><div id="comment-42619-info" class="comment-info"><span class="comment-age">(22 May '15, 11:32)</span> <span class="comment-user userinfo">samira</span></div></div><span id="42621"></span><div id="comment-42621" class="comment"><div id="post-42621-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-42621-info" class="comment-info"><span class="comment-age">(22 May '15, 11:40)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-42618" class="comment-tools"></div><div class="clear"></div><div id="comment-42618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

