+++
type = "question"
title = "brew install --with-qt can not open &quot; Capture Filters... &quot; window"
description = '''I use brew install wireshark --with-qt on my Mac OS X 10.9.3, after install wireshark 1.10.7, I found I can&#x27;t open the &quot;Capture -&amp;gt; Capture Filters...&quot; window, it just do nothing, and the same thing happens when I open &quot; Capture -&amp;gt; Options&quot; and &quot;Capture -&amp;gt; Interfaces&quot;. I wonder if I did some...'''
date = "2014-05-27T00:59:00Z"
lastmod = "2014-05-27T00:59:00Z"
weight = 33100
keywords = [ "filter", "macosx", "install" ]
aliases = [ "/questions/33100" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [brew install --with-qt can not open " Capture Filters... " window](/questions/33100/brew-install-with-qt-can-not-open-capture-filters-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33100-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33100-score" class="post-score" title="current number of votes">0</div><span id="post-33100-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use brew install wireshark --with-qt on my Mac OS X 10.9.3, after install wireshark 1.10.7, I found I can't open the "Capture -&gt; Capture Filters..." window, it just do nothing, and the same thing happens when I open " Capture -&gt; Options" and "Capture -&gt; Interfaces". I wonder if I did something wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '14, 00:59</strong></p><img src="https://secure.gravatar.com/avatar/659387b3fdce77c6926c712a6d3f7f25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jameswork&#39;s gravatar image" /><p><span>jameswork</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jameswork has no accepted answers">0%</span></p></div></div><div id="comments-container-33100" class="comments-container"></div><div id="comment-tools-33100" class="comment-tools"></div><div class="clear"></div><div id="comment-33100-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

