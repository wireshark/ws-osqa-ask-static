+++
type = "question"
title = "disable transport decode"
description = '''I know how to configure a specific data transport ( say tcpip both( 2255 &amp;lt;-&amp;gt; 9120 ) as some protocol. I just want it disabled so that it doesn&#x27;t try to decode something it isn&#x27;t supposed to. it&#x27;s just raw data.'''
date = "2014-02-13T09:43:00Z"
lastmod = "2014-02-13T14:55:00Z"
weight = 29833
keywords = [ "wireshark" ]
aliases = [ "/questions/29833" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [disable transport decode](/questions/29833/disable-transport-decode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29833-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29833-score" class="post-score" title="current number of votes">0</div><span id="post-29833-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know how to configure a specific data transport ( say tcpip both( 2255 &lt;-&gt; 9120 ) as some protocol. I just want it disabled so that it doesn't try to decode something it isn't supposed to. it's just raw data.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '14, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/a46150e219d2f3b661073598e91541f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joe%20Florida&#39;s gravatar image" /><p><span>Joe Florida</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joe Florida has no accepted answers">0%</span></p></div></div><div id="comments-container-29833" class="comments-container"></div><div id="comment-tools-29833" class="comment-tools"></div><div class="clear"></div><div id="comment-29833-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29847"></span>

<div id="answer-container-29847" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29847-score" class="post-score" title="current number of votes">0</div><span id="post-29847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately, there's no way to do that, other than disabling whatever protocols the data is being dissected as, which is not very convenient.</p><p>Please file an enhancement request on <a href="http://bugs.wireshark.org">the Wireshark Bugzilla</a>, asking that "disable" or "data" be added to all "Decode As..." lists.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '14, 14:55</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-29847" class="comments-container"></div><div id="comment-tools-29847" class="comment-tools"></div><div class="clear"></div><div id="comment-29847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

