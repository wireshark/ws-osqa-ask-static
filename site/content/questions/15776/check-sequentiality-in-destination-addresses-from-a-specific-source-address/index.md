+++
type = "question"
title = "check sequentiality in destination addresses from a specific source address"
description = '''Hi, I need a piece of code that can check if there is a source address sending a SYN packet to sequential ip addresses while capturing packets using tshark withen a small period of time,say 1 minute. thanks'''
date = "2012-11-09T08:21:00Z"
lastmod = "2012-11-22T01:09:00Z"
weight = 15776
keywords = [ "lua" ]
aliases = [ "/questions/15776" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [check sequentiality in destination addresses from a specific source address](/questions/15776/check-sequentiality-in-destination-addresses-from-a-specific-source-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15776-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15776-score" class="post-score" title="current number of votes">0</div><span id="post-15776-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I need a piece of code that can check if there is a source address sending a SYN packet to sequential ip addresses while capturing packets using tshark withen a small period of time,say 1 minute. thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Nov '12, 08:21</strong></p><img src="https://secure.gravatar.com/avatar/912ebc145cb38ec3da99be6003d7d9b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Leena&#39;s gravatar image" /><p><span>Leena</span><br />
<span class="score" title="51 reputation points">51</span><span title="17 badges"><span class="badge1">●</span><span class="badgecount">17</span></span><span title="18 badges"><span class="silver">●</span><span class="badgecount">18</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Leena has no accepted answers">0%</span></p></div></div><div id="comments-container-15776" class="comments-container"><span id="15830"></span><div id="comment-15830" class="comment"><div id="post-15830-score" class="comment-score"></div><div class="comment-text"><p>I just want to note that I want the code in Lua in order to make the process while running the program at the same time with capturing, I'm working with tshark. I read about MATE and I don't think that it is what I'm looking forward. thanks</p></div><div id="comment-15830-info" class="comment-info"><span class="comment-age">(12 Nov '12, 09:06)</span> <span class="comment-user userinfo">Leena</span></div></div></div><div id="comment-tools-15776" class="comment-tools"></div><div class="clear"></div><div id="comment-15776-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15818"></span>

<div id="answer-container-15818" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15818-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15818-score" class="post-score" title="current number of votes">0</div><span id="post-15818-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can <a href="http://wiki.wireshark.org/Mate">MATE</a> be of help?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Nov '12, 04:07</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-15818" class="comments-container"><span id="16195"></span><div id="comment-16195" class="comment"><div id="post-16195-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jaap but this doesn't fit what I'm trying to do. Any body has other suggestion of ideas that save processing time.</p></div><div id="comment-16195-info" class="comment-info"><span class="comment-age">(22 Nov '12, 01:09)</span> <span class="comment-user userinfo">Leena</span></div></div></div><div id="comment-tools-15818" class="comment-tools"></div><div class="clear"></div><div id="comment-15818-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

