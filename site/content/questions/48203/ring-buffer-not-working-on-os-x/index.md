+++
type = "question"
title = "Ring Buffer Not Working on OS X"
description = '''Hello All,  Using Wireshark 2.0, setting up a ring buffer where x number of files are used to store the captured frames at y size is not working for me. OS X El Capitan 10.11.1.  In Capture options I set it up as follows:  I have tried with just the file name and also with a full directory path. Tri...'''
date = "2015-12-02T09:42:00Z"
lastmod = "2015-12-02T09:42:00Z"
weight = 48203
keywords = [ "ringbuffer" ]
aliases = [ "/questions/48203" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ring Buffer Not Working on OS X](/questions/48203/ring-buffer-not-working-on-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48203-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48203-score" class="post-score" title="current number of votes">0</div><span id="post-48203-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All, Using Wireshark 2.0, setting up a ring buffer where x number of files are used to store the captured frames at y size is not working for me. OS X El Capitan 10.11.1.<br />
</p><p>In Capture options I set it up as follows:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-12-02_at_12.33.58_PM.png" alt="alt text" /></p><p>I have tried with just the file name and also with a full directory path. Tried using the Browse button to select the full path as well. When I try to capture I see the following error:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-12-02_at_12.34.11_PM.png" alt="alt text" /></p><p>Verified that the folder used for the test has standard permissions. Changed to full read/write/execute which had no effect on the test.</p><p>Anyone else run into this?</p><p>Thanks in advance, James</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ringbuffer" rel="tag" title="see questions tagged &#39;ringbuffer&#39;">ringbuffer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '15, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/829b13ed2b6262f99bb759b3b9c9386d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jgarringer&#39;s gravatar image" /><p><span>jgarringer</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jgarringer has no accepted answers">0%</span> </br></p></img></div></div><div id="comments-container-48203" class="comments-container"></div><div id="comment-tools-48203" class="comment-tools"></div><div class="clear"></div><div id="comment-48203-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

