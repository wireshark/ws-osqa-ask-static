+++
type = "question"
title = "Message &quot;Can&#x27;t dissect FP frame because no per-frame info was attached!&quot;"
description = '''My program exports data in PCAP format. In exports present data in FP. Wireshark recognize FP protocol, but indicates message &quot;Can&#x27;t dissect FP frame because no per-frame info was attached!&quot; How I must &quot;attached per-frame info&quot; and in witch format?'''
date = "2011-09-16T04:10:00Z"
lastmod = "2011-09-16T05:49:00Z"
weight = 6414
keywords = [ "fp" ]
aliases = [ "/questions/6414" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Message "Can't dissect FP frame because no per-frame info was attached!"](/questions/6414/message-cant-dissect-fp-frame-because-no-per-frame-info-was-attached)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6414-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6414-score" class="post-score" title="current number of votes">0</div><span id="post-6414-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My program exports data in PCAP format. In exports present data in FP. Wireshark recognize FP protocol, but indicates message "Can't dissect FP frame because no per-frame info was attached!" How I must "attached per-frame info" and in witch format?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '11, 04:10</strong></p><img src="https://secure.gravatar.com/avatar/dbfa29bfda8b7ebfd40a51e7aceb3880?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SergeSPB&#39;s gravatar image" /><p><span>SergeSPB</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SergeSPB has no accepted answers">0%</span></p></div></div><div id="comments-container-6414" class="comments-container"><span id="6415"></span><div id="comment-6415" class="comment"><div id="post-6415-score" class="comment-score"></div><div class="comment-text"><p>Hi, Take a look at the fp_info struct in packet-umts_fp.h See also http://wiki.wireshark.org/FP</p></div><div id="comment-6415-info" class="comment-info"><span class="comment-age">(16 Sep '11, 05:49)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-6414" class="comment-tools"></div><div class="clear"></div><div id="comment-6414-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

