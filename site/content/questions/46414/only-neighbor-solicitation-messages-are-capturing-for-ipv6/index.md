+++
type = "question"
title = "Only Neighbor Solicitation Messages are capturing for IPv6"
description = '''Hi, I am trying to capture the IPv6 packets (s1ap &amp;amp; gtpv2 packets) in the LTE network interface, But only Neighbor Solicitation Messages are capturing in the wireshark(running on Windows 7) while using IPv6 interface address. But while using IPv4 address i am able to capture all the packets incl...'''
date = "2015-10-08T00:39:00Z"
lastmod = "2015-10-11T05:01:00Z"
weight = 46414
keywords = [ "ipv6" ]
aliases = [ "/questions/46414" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Only Neighbor Solicitation Messages are capturing for IPv6](/questions/46414/only-neighbor-solicitation-messages-are-capturing-for-ipv6)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46414-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46414-score" class="post-score" title="current number of votes">0</div><span id="post-46414-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to capture the IPv6 packets (s1ap &amp; gtpv2 packets) in the LTE network interface, But only Neighbor Solicitation Messages are capturing in the wireshark(running on Windows 7) while using IPv6 interface address. But while using IPv4 address i am able to capture all the packets including s1ap &amp; gtpv2 packets.</p><p>Please let me know the solution for this.</p><p>Thanking you !!</p><p>Regards, Deepak.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '15, 00:39</strong></p><img src="https://secure.gravatar.com/avatar/32d6b23d3fb49c992627ff97447ae12f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deepak%20Boyina&#39;s gravatar image" /><p><span>Deepak Boyina</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deepak Boyina has no accepted answers">0%</span></p></div></div><div id="comments-container-46414" class="comments-container"><span id="46452"></span><div id="comment-46452" class="comment"><div id="post-46452-score" class="comment-score"></div><div class="comment-text"><p>Can you please add more details about the setup of your capturing environment.</p><ul><li>What kind of LTE interface is that?</li><li>Did you set any capture filters? If so, whiche one?</li><li>Wireshark version</li><li>anything else that might help to understand your problem</li></ul></div><div id="comment-46452-info" class="comment-info"><span class="comment-age">(11 Oct '15, 05:01)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-46414" class="comment-tools"></div><div class="clear"></div><div id="comment-46414-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

