+++
type = "question"
title = "Does packet capture work inside of VMWare?"
description = '''Does anyone know if you can sniff network packets with Wireshark in a virtual machine? Networks settings with NAT doesn&#x27;t show anything and Bridged only shows the traffic to and from the host. I&#x27;m using BackTrack 3. Promiscuous mode is enabled.'''
date = "2012-01-15T09:09:00Z"
lastmod = "2012-01-16T14:59:00Z"
weight = 8394
keywords = [ "vmware", "nat", "bridged", "backtrack" ]
aliases = [ "/questions/8394" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does packet capture work inside of VMWare?](/questions/8394/does-packet-capture-work-inside-of-vmware)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8394-score" class="post-score" title="current number of votes">0</div><span id="post-8394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know if you can sniff network packets with Wireshark in a virtual machine? Networks settings with NAT doesn't show anything and Bridged only shows the traffic to and from the host. I'm using BackTrack 3. Promiscuous mode is enabled.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vmware" rel="tag" title="see questions tagged &#39;vmware&#39;">vmware</span> <span class="post-tag tag-link-nat" rel="tag" title="see questions tagged &#39;nat&#39;">nat</span> <span class="post-tag tag-link-bridged" rel="tag" title="see questions tagged &#39;bridged&#39;">bridged</span> <span class="post-tag tag-link-backtrack" rel="tag" title="see questions tagged &#39;backtrack&#39;">backtrack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '12, 09:09</strong></p><img src="https://secure.gravatar.com/avatar/6841b48175faaad38e6437b4f954cdbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mpneele&#39;s gravatar image" /><p><span>mpneele</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mpneele has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '12, 19:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-8394" class="comments-container"></div><div id="comment-tools-8394" class="comment-tools"></div><div class="clear"></div><div id="comment-8394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8418"></span>

<div id="answer-container-8418" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8418-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8418-score" class="post-score" title="current number of votes">0</div><span id="post-8418-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Some quick Googling turned up these relevant articles:</p><ul><li><a href="http://www.petri.co.il/using-packet-analyzer-on-virtual-network.htm">Using a Network Packet Analyzer on a VMware vSphere Virtual Network</a></li><li><a href="http://www.youtube.com/watch?v=B2erktbb16E">Backtrack 5 first tryout on vmware (network sniffing with ettercap)</a></li></ul><p>See for yourself if these help, or Google some more with your specifics.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jan '12, 14:59</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-8418" class="comments-container"></div><div id="comment-tools-8418" class="comment-tools"></div><div class="clear"></div><div id="comment-8418-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

