+++
type = "question"
title = "x11 wireshark losing focus on MAC"
description = '''I have seen this issue in many forums apple macrumours but never see any solution to it? is this x11 bug or the way wireshark is compiled? What wrong its very annoying when you are doing deep packet inspection and it lose focus and cant scroll and click something else which messes up with work. any ...'''
date = "2014-07-12T07:25:00Z"
lastmod = "2014-07-12T07:25:00Z"
weight = 34616
keywords = [ "x11", "macosx" ]
aliases = [ "/questions/34616" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [x11 wireshark losing focus on MAC](/questions/34616/x11-wireshark-losing-focus-on-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34616-score" class="post-score" title="current number of votes">0</div><span id="post-34616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have seen this issue in many forums apple macrumours but never see any solution to it?</p><p>is this x11 bug or the way wireshark is compiled?</p><p>What wrong its very annoying when you are doing deep packet inspection and it lose focus and cant scroll and click something else which messes up with work.</p><p>any help?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x11" rel="tag" title="see questions tagged &#39;x11&#39;">x11</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jul '14, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/42e74ec2c81e0f0fbb27082606c8d95b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vasanth&#39;s gravatar image" /><p><span>vasanth</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vasanth has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Jul '14, 04:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-34616" class="comments-container"></div><div id="comment-tools-34616" class="comment-tools"></div><div class="clear"></div><div id="comment-34616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

