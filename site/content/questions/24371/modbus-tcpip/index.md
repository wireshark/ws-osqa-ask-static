+++
type = "question"
title = "Modbus TCP/IP"
description = '''how to read modbus tcp/ip slave device (192.167.1.3) ID = 1, holding register 45001 ?? its 16 bit register, if i can be also read only bit 3 as watching and analysing.'''
date = "2013-09-05T01:13:00Z"
lastmod = "2013-09-06T00:59:00Z"
weight = 24371
keywords = [ "abod" ]
aliases = [ "/questions/24371" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Modbus TCP/IP](/questions/24371/modbus-tcpip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24371-score" class="post-score" title="current number of votes">0</div><span id="post-24371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to read modbus tcp/ip slave device (192.167.1.3) ID = 1, holding register 45001 ?? its 16 bit register, if i can be also read only bit 3 as watching and analysing.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-abod" rel="tag" title="see questions tagged &#39;abod&#39;">abod</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '13, 01:13</strong></p><img src="https://secure.gravatar.com/avatar/69ea0d4e2cd0b2a41c6134060d8e5ae3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Abdulrahman&#39;s gravatar image" /><p><span>Abdulrahman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Abdulrahman has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Sep '13, 01:18</strong> </span></p></div></div><div id="comments-container-24371" class="comments-container"><span id="24407"></span><div id="comment-24407" class="comment"><div id="post-24407-score" class="comment-score"></div><div class="comment-text"><p>can you please rephrase your question. It is unclear (at least to me) what you are asking for.</p></div><div id="comment-24407-info" class="comment-info"><span class="comment-age">(06 Sep '13, 00:59)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24371" class="comment-tools"></div><div class="clear"></div><div id="comment-24371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

