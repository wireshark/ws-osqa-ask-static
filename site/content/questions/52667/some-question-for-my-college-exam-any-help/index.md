+++
type = "question"
title = "Some question for my college exam, Any help :)"
description = '''Hello im getting ready for my college exam that will include some questions about Wireshark and more. So if some one can help me with this sample questions:    Your network consists of managed switch and multiple hosts that are connected to this switch. In case that you need to monitor the overall t...'''
date = "2016-05-17T05:24:00Z"
lastmod = "2016-05-18T00:33:00Z"
weight = 52667
keywords = [ "exam", "wireshark" ]
aliases = [ "/questions/52667" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Some question for my college exam, Any help :)](/questions/52667/some-question-for-my-college-exam-any-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52667-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52667-score" class="post-score" title="current number of votes">0</div><span id="post-52667-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello im getting ready for my college exam that will include some questions about <strong>Wireshark</strong> and more. So if some one can help me with this sample questions:</p><ol><li><p>Your network consists of managed switch and multiple hosts that are connected to this switch. In case that you need to monitor the overall traffic flowing through this switch, where will you connect the Wireshark to fulfill this mission?</p></li><li><p>In case you need to capture traffic from specific IP Address: 192.168.0.1, what should be defined in the Wireshark filter?</p></li><li><p>Please define the proper filter that will capture traffic from the 4th Layer: UDP or from TCP and only from IP Address 192.168.0.32</p></li><li><p>As part of your tasks, you need to present the call flow. Please describe what are the required actions for that?</p></li><li><p>If you need to capture and replay audio based capture (running over RTP- Real Time Transport Protocol) from/to your Softphone, what will be the filter to be defined and specify the actions to fulfill this task.</p></li><li><p>As part of the troubleshooting process you need to analyze the network impairments such as Jitter and Packet Loss. Please specify the required actions in order to present these parameters measured by the Wireshark.</p></li></ol><p>Thank You :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-exam" rel="tag" title="see questions tagged &#39;exam&#39;">exam</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 May '16, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/c0ce556e85361cd9ae85f2a67e82d3dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="igbaryya&#39;s gravatar image" /><p><span>igbaryya</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="igbaryya has no accepted answers">0%</span></p></div></div><div id="comments-container-52667" class="comments-container"></div><div id="comment-tools-52667" class="comment-tools"></div><div class="clear"></div><div id="comment-52667-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52669"></span>

<div id="answer-container-52669" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52669-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52669-score" class="post-score" title="current number of votes">3</div><span id="post-52669-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="igbaryya has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As a study guide I would suggest these:</p><ol><li><a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">Capture setup</a></li><li><a href="https://wiki.wireshark.org/CaptureFilters">Capture filter</a></li><li><a href="https://wiki.wireshark.org/CaptureFilters">Capture filter</a></li><li><a href="https://wiki.wireshark.org/VoIP_calls">VoIP calls</a></li><li><a href="https://wiki.wireshark.org/VoIP_calls">VoIP calls</a></li><li><a href="https://wiki.wireshark.org/RTP_statistics">RTP statistics</a></li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 May '16, 05:51</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52669" class="comments-container"><span id="52707"></span><div id="comment-52707" class="comment"><div id="post-52707-score" class="comment-score"></div><div class="comment-text"><p>Thank You.</p></div><div id="comment-52707-info" class="comment-info"><span class="comment-age">(18 May '16, 00:33)</span> <span class="comment-user userinfo">igbaryya</span></div></div></div><div id="comment-tools-52669" class="comment-tools"></div><div class="clear"></div><div id="comment-52669-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

