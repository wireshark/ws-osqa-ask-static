+++
type = "question"
title = "Finding Contract Programmers to Write Dissectors"
description = '''Where can contract programmers with experience writing Wireshark dissectors be found online?'''
date = "2015-11-16T22:50:00Z"
lastmod = "2015-11-16T23:04:00Z"
weight = 47652
keywords = [ "freelance", "dissector", "contractor" ]
aliases = [ "/questions/47652" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Finding Contract Programmers to Write Dissectors](/questions/47652/finding-contract-programmers-to-write-dissectors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47652-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47652-score" class="post-score" title="current number of votes">0</div><span id="post-47652-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can contract programmers with experience writing Wireshark dissectors be found online?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-freelance" rel="tag" title="see questions tagged &#39;freelance&#39;">freelance</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-contractor" rel="tag" title="see questions tagged &#39;contractor&#39;">contractor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '15, 22:50</strong></p><img src="https://secure.gravatar.com/avatar/2daac0a41fead6cda1ad1c2fa19f9ee9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ffierling&#39;s gravatar image" /><p><span>ffierling</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ffierling has no accepted answers">0%</span></p></div></div><div id="comments-container-47652" class="comments-container"></div><div id="comment-tools-47652" class="comment-tools"></div><div class="clear"></div><div id="comment-47652-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47653"></span>

<div id="answer-container-47653" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47653-score" class="post-score" title="current number of votes">0</div><span id="post-47653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Most certainly on the Wireshark developer mailing list</p><blockquote><p><a href="https://www.wireshark.org/mailman/listinfo/wireshark-dev">https://www.wireshark.org/mailman/listinfo/wireshark-dev</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '15, 23:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-47653" class="comments-container"></div><div id="comment-tools-47653" class="comment-tools"></div><div class="clear"></div><div id="comment-47653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

