+++
type = "question"
title = "Malformed packed [exception occurred] customize message"
description = '''Hi, I am getting a malformed packed error and description as [Malformed Packet: (Error/Malformed): Malformed Packet (Exception occurred)  [Message: Malformed Packet (Exception occurred) ] [Severity level: Error ] [Group: Malformed] I want to identify the code where this exception is being occurring....'''
date = "2014-07-09T06:35:00Z"
lastmod = "2014-07-09T06:35:00Z"
weight = 34503
keywords = [ "malformed", "wireshark" ]
aliases = [ "/questions/34503" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed packed \[exception occurred\] customize message](/questions/34503/malformed-packed-exception-occurred-customize-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34503-score" class="post-score" title="current number of votes">0</div><span id="post-34503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am getting a malformed packed error and description as [Malformed Packet: (Error/Malformed): Malformed Packet (Exception occurred) [Message: Malformed Packet (Exception occurred) ] [Severity level: Error ] [Group: Malformed]</p><p>I want to identify the code where this exception is being occurring. How can I find that? and also I need it just to display Malformed Packed, if any instead of Malformed Packet (Exception occurred). ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '14, 06:35</strong></p><img src="https://secure.gravatar.com/avatar/a9a254ac482208f766093c0f9c144364?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aman&#39;s gravatar image" /><p><span>aman</span><br />
<span class="score" title="36 reputation points">36</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aman has no accepted answers">0%</span></p></div></div><div id="comments-container-34503" class="comments-container"></div><div id="comment-tools-34503" class="comment-tools"></div><div class="clear"></div><div id="comment-34503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

