+++
type = "question"
title = "H264 decode on mpeg2 transport stream"
description = '''Hi All, I have question regarding h264 dissector in wireshark. I Can see in wireshark preferences that there is some default impelementation of this protocol (H264) so i assume that wireshark should show some NAL packets information in the packet window. Instead of this i have just pes data payload....'''
date = "2017-01-22T12:27:00Z"
lastmod = "2017-01-22T12:27:00Z"
weight = 58954
keywords = [ "h264_decode" ]
aliases = [ "/questions/58954" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [H264 decode on mpeg2 transport stream](/questions/58954/h264-decode-on-mpeg2-transport-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58954-score" class="post-score" title="current number of votes">0</div><span id="post-58954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I have question regarding h264 dissector in wireshark. I Can see in wireshark preferences that there is some default impelementation of this protocol (H264) so i assume that wireshark should show some NAL packets information in the packet window. Instead of this i have just pes data payload. How can i decode this payload as H264 protocol ? Protocols that is recognized in my stream is 13818-1 MPEG2 transport stream, then paylod is packetized elementary stream and the payload of the PES should be H264. This is not decoded by wireshark.</p><p>Please help. Thanks.</p><p>Br,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h264_decode" rel="tag" title="see questions tagged &#39;h264_decode&#39;">h264_decode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '17, 12:27</strong></p><img src="https://secure.gravatar.com/avatar/f675b249fa000c62d80e1c6e0b14976b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lukwire007&#39;s gravatar image" /><p><span>lukwire007</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lukwire007 has no accepted answers">0%</span></p></div></div><div id="comments-container-58954" class="comments-container"></div><div id="comment-tools-58954" class="comment-tools"></div><div class="clear"></div><div id="comment-58954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

