+++
type = "question"
title = "Remote Packet capture in version 1.82"
description = '''Hi  I used to use the remote packet capture. How do i set this up in the latest version of wireshark 1.82 ? Best regards  Lenny'''
date = "2012-08-17T21:43:00Z"
lastmod = "2012-08-20T02:24:00Z"
weight = 13711
keywords = [ "remote-monitoring" ]
aliases = [ "/questions/13711" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Remote Packet capture in version 1.82](/questions/13711/remote-packet-capture-in-version-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13711-score" class="post-score" title="current number of votes">0</div><span id="post-13711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I used to use the remote packet capture. How do i set this up in the latest version of wireshark 1.82 ?</p><p>Best regards Lenny</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-monitoring" rel="tag" title="see questions tagged &#39;remote-monitoring&#39;">remote-monitoring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Aug '12, 21:43</strong></p><img src="https://secure.gravatar.com/avatar/c40e2cc7d140edec868982482fdfda62?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lenny_dk&#39;s gravatar image" /><p><span>Lenny_dk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lenny_dk has no accepted answers">0%</span></p></div></div><div id="comments-container-13711" class="comments-container"></div><div id="comment-tools-13711" class="comment-tools"></div><div class="clear"></div><div id="comment-13711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13712"></span>

<div id="answer-container-13712" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13712-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13712-score" class="post-score" title="current number of votes">0</div><span id="post-13712-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi I solved it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Aug '12, 22:02</strong></p><img src="https://secure.gravatar.com/avatar/c40e2cc7d140edec868982482fdfda62?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lenny_dk&#39;s gravatar image" /><p><span>Lenny_dk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lenny_dk has no accepted answers">0%</span></p></div></div><div id="comments-container-13712" class="comments-container"><span id="13746"></span><div id="comment-13746" class="comment"><div id="post-13746-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your insightful answer. Can you add more details on the exact problem, and the solution otherwise I'll close the question as it's not helpful to other users?</p></div><div id="comment-13746-info" class="comment-info"><span class="comment-age">(20 Aug '12, 02:24)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-13712" class="comment-tools"></div><div class="clear"></div><div id="comment-13712-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

