+++
type = "question"
title = "Scanning All Channels"
description = '''I set Wireshark to monitor mode in order to capture all 802.11 frames, but it seems that it is stuck in one channel (channel 1). What I want is to scan all channels. Demo purposes. Is there any way to configure Wireshark in MAC for doing so? Thanks '''
date = "2017-10-09T11:20:00Z"
lastmod = "2017-10-10T02:10:00Z"
weight = 63769
keywords = [ "scanning", "all-channels", "wi-fi" ]
aliases = [ "/questions/63769" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Scanning All Channels](/questions/63769/scanning-all-channels)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63769-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63769-score" class="post-score" title="current number of votes">0</div><span id="post-63769-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I set Wireshark to monitor mode in order to capture all 802.11 frames, but it seems that it is stuck in one channel (channel 1). What I want is to scan all channels. Demo purposes.</p><p>Is there any way to configure Wireshark in MAC for doing so?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-scanning" rel="tag" title="see questions tagged &#39;scanning&#39;">scanning</span> <span class="post-tag tag-link-all-channels" rel="tag" title="see questions tagged &#39;all-channels&#39;">all-channels</span> <span class="post-tag tag-link-wi-fi" rel="tag" title="see questions tagged &#39;wi-fi&#39;">wi-fi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '17, 11:20</strong></p><img src="https://secure.gravatar.com/avatar/470309b44381e044dc2ffb207a79eed9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="raultort&#39;s gravatar image" /><p><span>raultort</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="raultort has no accepted answers">0%</span></p></div></div><div id="comments-container-63769" class="comments-container"></div><div id="comment-tools-63769" class="comment-tools"></div><div class="clear"></div><div id="comment-63769-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="63770"></span>

<div id="answer-container-63770" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63770-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63770-score" class="post-score" title="current number of votes">0</div><span id="post-63770-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Check the answer to <a href="https://ask.wireshark.org/questions/62565/wifi-adapter-that-allows-monitoring-multiple-frequencies">this question</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Oct '17, 12:00</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-63770" class="comments-container"></div><div id="comment-tools-63770" class="comment-tools"></div><div class="clear"></div><div id="comment-63770-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="63780"></span>

<div id="answer-container-63780" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63780-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63780-score" class="post-score" title="current number of votes">0</div><span id="post-63780-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately, Wireshark doesn't currently support channel scanning on <em>any</em> platform, and doesn't currently support setting the channel via the Wireshark GUI on macOS.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '17, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-63780" class="comments-container"></div><div id="comment-tools-63780" class="comment-tools"></div><div class="clear"></div><div id="comment-63780-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="63783"></span>

<div id="answer-container-63783" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63783-score" class="post-score" title="current number of votes">0</div><span id="post-63783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The tool you want to use to scan all channels on a MAC with configurable dwell time and collect packets is:</p><p><a href="https://www.adriangranados.com/apps/airtool">https://www.adriangranados.com/apps/airtool</a></p><p>This will do an offline collection then you can open the saved trace file in Wireshark (either automatically or manually). I assume you already know that the traces won't really be coherent - you will only get a small sample from each channel (depending on dwell time) and it is offline - it will collect packets then when you stop the collection process it will write them to disk for ex post facto analysis. I don't know if that is good enough for your needs or not.</p><p>I'd consider moving to a linux platform if this does not meet your needs.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '17, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span></p></div></div><div id="comments-container-63783" class="comments-container"><span id="63784"></span><div id="comment-63784" class="comment"><div id="post-63784-score" class="comment-score"></div><div class="comment-text"><p>Note that, as I said in my answer, "Wireshark doesn't currently support channel scanning on any platform" - it might support setting the channel from the GUI on Linux, but it doesn't support setting it to "scan and stop when you see packets" or "scan and have it listen for N seconds on each channel".</p></div><div id="comment-63784-info" class="comment-info"><span class="comment-age">(10 Oct '17, 02:10)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-63783" class="comment-tools"></div><div class="clear"></div><div id="comment-63783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

