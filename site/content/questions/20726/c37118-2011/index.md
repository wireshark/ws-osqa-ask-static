+++
type = "question"
title = "C37.118 - 2011"
description = '''Does any release package of Wireshark be available for IEEE C37.118 - 2011? IEEE C37.118 - 2011---IEEE Standard for Synchrophasor Measurements and Data Transfer for Power Systems.'''
date = "2013-04-22T21:14:00Z"
lastmod = "2013-04-22T22:47:00Z"
weight = 20726
keywords = [ "2011", "-", "c37.118" ]
aliases = [ "/questions/20726" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [C37.118 - 2011](/questions/20726/c37118-2011)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20726-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20726-score" class="post-score" title="current number of votes">0</div><span id="post-20726-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does any release package of Wireshark be available for IEEE C37.118 - 2011?</p><p>IEEE C37.118 - 2011---IEEE Standard for Synchrophasor Measurements and Data Transfer for Power Systems.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-2011" rel="tag" title="see questions tagged &#39;2011&#39;">2011</span> <span class="post-tag tag-link--" rel="tag" title="see questions tagged &#39;-&#39;">-</span> <span class="post-tag tag-link-c37.118" rel="tag" title="see questions tagged &#39;c37.118&#39;">c37.118</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '13, 21:14</strong></p><img src="https://secure.gravatar.com/avatar/1e36eaa27a2d90b1dd37aa42e44c5296?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hector&#39;s gravatar image" /><p><span>Hector</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hector has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Apr '13, 21:17</strong> </span></p></div></div><div id="comments-container-20726" class="comments-container"></div><div id="comment-tools-20726" class="comment-tools"></div><div class="clear"></div><div id="comment-20726-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20727"></span>

<div id="answer-container-20727" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20727-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20727-score" class="post-score" title="current number of votes">0</div><span id="post-20727-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Hector has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The C37.118 dissector's been there since Wireshark 1.6, but it was checked in during early 2009, and there don't appear to have been any protocol updates since then, so if there are post-2009 revisions, they're probably not supported.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Apr '13, 22:39</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-20727" class="comments-container"><span id="20729"></span><div id="comment-20729" class="comment"><div id="post-20729-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot.</p></div><div id="comment-20729-info" class="comment-info"><span class="comment-age">(22 Apr '13, 22:47)</span> <span class="comment-user userinfo">Hector</span></div></div></div><div id="comment-tools-20727" class="comment-tools"></div><div class="clear"></div><div id="comment-20727-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

