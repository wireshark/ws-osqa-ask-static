+++
type = "question"
title = "Remote Capture"
description = '''Does anyone know why Remote Capture will not work.'''
date = "2013-11-25T11:02:00Z"
lastmod = "2013-11-26T02:02:00Z"
weight = 27358
keywords = [ "remote-capture" ]
aliases = [ "/questions/27358" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Remote Capture](/questions/27358/remote-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27358-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27358-score" class="post-score" title="current number of votes">0</div><span id="post-27358-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know why Remote Capture will not work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '13, 11:02</strong></p><img src="https://secure.gravatar.com/avatar/9e30447696a6f9853b747e86e7461cea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ScoRay&#39;s gravatar image" /><p><span>ScoRay</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ScoRay has no accepted answers">0%</span></p></div></div><div id="comments-container-27358" class="comments-container"><span id="27386"></span><div id="comment-27386" class="comment"><div id="post-27386-score" class="comment-score"></div><div class="comment-text"><p>To get any help other than "it works for me", you'll need to give lots more details on what you're trying to do. Local Wireshark version and host OS, remote OS etc, and what errors if any you see.</p></div><div id="comment-27386-info" class="comment-info"><span class="comment-age">(26 Nov '13, 02:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="27387"></span><div id="comment-27387" class="comment"><div id="post-27387-score" class="comment-score"></div><div class="comment-text"><p>It works for me ;-)</p></div><div id="comment-27387-info" class="comment-info"><span class="comment-age">(26 Nov '13, 02:02)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27358" class="comment-tools"></div><div class="clear"></div><div id="comment-27358-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

