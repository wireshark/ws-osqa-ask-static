+++
type = "question"
title = "Heelp me, please!!!!"
description = ''' WireShark can capture packets going through your interface. If you want to capture all packets in your network. What kind of techniques will be used with? Write a Wireshark rules to filter web access to Youtube, with source address is 192.168.1.3  Can you help me to answer 2 question. Thanks '''
date = "2014-10-12T09:22:00Z"
lastmod = "2014-10-12T09:30:00Z"
weight = 36980
keywords = [ "question" ]
aliases = [ "/questions/36980" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Heelp me, please!!!!](/questions/36980/heelp-me-please)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36980-score" class="post-score" title="current number of votes">0</div><span id="post-36980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><ol><li>WireShark can capture packets going through your interface. If you want to capture all packets in your network. What kind of techniques will be used with?</li><li>Write a Wireshark rules to filter web access to Youtube, with source address is 192.168.1.3</li></ol><p>Can you help me to answer 2 question. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-question" rel="tag" title="see questions tagged &#39;question&#39;">question</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '14, 09:22</strong></p><img src="https://secure.gravatar.com/avatar/2ab92a5d25a9a8292ac614670efd74ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="livetolove93&#39;s gravatar image" /><p><span>livetolove93</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="livetolove93 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Oct '14, 09:30</strong> </span></p></div></div><div id="comments-container-36980" class="comments-container"></div><div id="comment-tools-36980" class="comment-tools"></div><div class="clear"></div><div id="comment-36980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36981"></span>

<div id="answer-container-36981" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36981-score" class="post-score" title="current number of votes">0</div><span id="post-36981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Homework assignments... hadn't one of those for some time :-)</p><ol><li>Take a look at <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a>, it should give you some ideas</li><li>Not a good exercise because it's too unspecific. Try filtering on things like "http.request.uri contains youtube.com", and combine that with an "ip.src" filter</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '14, 09:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-36981" class="comments-container"></div><div id="comment-tools-36981" class="comment-tools"></div><div class="clear"></div><div id="comment-36981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

