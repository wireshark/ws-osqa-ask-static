+++
type = "question"
title = "finding ip"
description = '''Iam trying to get an ip address from scammers who call everyday stating my computer is infected I&#x27;ve down loaded wireshark and it is running I just don&#x27;t know what to look for fairly good with my computer but this not so much, PLEASE HELP ME  Terry Philipp 252-636-0186 '''
date = "2016-09-20T13:23:00Z"
lastmod = "2016-09-20T14:01:00Z"
weight = 55681
keywords = [ "helpme" ]
aliases = [ "/questions/55681" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [finding ip](/questions/55681/finding-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55681-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55681-score" class="post-score" title="current number of votes">0</div><span id="post-55681-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Iam trying to get an ip address from scammers who call everyday stating my computer is infected I've down loaded wireshark and it is running I just don't know what to look for fairly good with my computer but this not so much, PLEASE HELP ME</p><pre><code>                                   Terry Philipp 252-636-0186</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-helpme" rel="tag" title="see questions tagged &#39;helpme&#39;">helpme</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '16, 13:23</strong></p><img src="https://secure.gravatar.com/avatar/4ea84e989dd332276869d1d25b5ea8f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="philimatt&#39;s gravatar image" /><p><span>philimatt</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="philimatt has no accepted answers">0%</span></p></div></div><div id="comments-container-55681" class="comments-container"><span id="55682"></span><div id="comment-55682" class="comment"><div id="post-55682-score" class="comment-score"></div><div class="comment-text"><p>How exactly are they calling, by phone or by popping up a message on your computer's screen?</p><ul><li><p>to scare you with "your computer is infected" by phone, one doesn't need to access your computer at all, it is enough to know your phone number and bet on the statistics saying that you are one of the 95 % of households which have one.</p></li><li><p>to scare you with "your computer is infected" by a popup message if your web browser is not running, one needs to infect your computer in advance, either to make it display the popup messages autonomously or to open a backdoor allowing him to send your computer an instruction to display the message. Even in the latter case, it would most likely be your computer which would open a connection to the controlling server, not vice versa, as most computers are protected by security devices or software, preventing network requests coming from the network from being let in.</p></li><li><p>to scare you with "your computer is infected" by a popup message when your web browser is running and you access a certain web page, it is enough to infect that web page so that it would ask your browser to open these popup windows (just like many aggressive advertising services do)</p></li></ul><p>Also, what do the people ask you to do - do they want you to stop spamming them or do they want you to buy an anti-virus software / computer maintenance service from them?</p><p>I'm asking this because another possibility would be that your computer actually <em>is</em> infected, and the people who call you want you to stop spamming them after they've seen the spam comes from your IP address. So if they offer to sell you something, they are just cheating on your fear; if they want you to make something about your computer annoying them, they are most likely right and there's something wrong with your computer. And this last case can be confirmed using Wireshark most easily - if it shows you that your PC opens TCP sessions to SMTP servers you've never heard about, or attempts to open TCP sessions to various well-known ports like Telnet, SSH, http, https, ..., of many different IP addresses even if you run just your web browser and open a single page in it, then there is some alien living in your computer and you really do have to do something about it.</p></div><div id="comment-55682-info" class="comment-info"><span class="comment-age">(20 Sep '16, 14:01)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-55681" class="comment-tools"></div><div class="clear"></div><div id="comment-55681-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

