+++
type = "question"
title = "Legal to monitor others internet traffic through router"
description = '''is it legal to monitor internet traffic of what others are browsing through home router using software like wireshark?'''
date = "2014-01-26T14:35:00Z"
lastmod = "2014-01-26T14:55:00Z"
weight = 29171
keywords = [ "monitor" ]
aliases = [ "/questions/29171" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Legal to monitor others internet traffic through router](/questions/29171/legal-to-monitor-others-internet-traffic-through-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29171-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29171-score" class="post-score" title="current number of votes">0</div><span id="post-29171-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it legal to monitor internet traffic of what others are browsing through home router using software like wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '14, 14:35</strong></p><img src="https://secure.gravatar.com/avatar/62be3324984818b1f4a119d04138e334?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="blueknight&#39;s gravatar image" /><p><span>blueknight</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="blueknight has no accepted answers">0%</span></p></div></div><div id="comments-container-29171" class="comments-container"></div><div id="comment-tools-29171" class="comment-tools"></div><div class="clear"></div><div id="comment-29171-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29172"></span>

<div id="answer-container-29172" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29172-score" class="post-score" title="current number of votes">0</div><span id="post-29172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, obviously that depends on the laws in your country. Why don't you go to your local police office and ask them? They will know what to do.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jan '14, 14:55</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Jan '14, 15:02</strong> </span></p></div></div><div id="comments-container-29172" class="comments-container"></div><div id="comment-tools-29172" class="comment-tools"></div><div class="clear"></div><div id="comment-29172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

