+++
type = "question"
title = "snow leopard"
description = '''I have a new mac, and i am trying to set up this. It keeps asking me for an interface. I have no idea where to get it or even where to start it. The setup instructions see to be a bit vague. '''
date = "2010-11-24T08:06:00Z"
lastmod = "2010-11-24T08:06:00Z"
weight = 1108
keywords = [ "interfaces" ]
aliases = [ "/questions/1108" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [snow leopard](/questions/1108/snow-leopard)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1108-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1108-score" class="post-score" title="current number of votes">0</div><span id="post-1108-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a new mac, and i am trying to set up this. It keeps asking me for an interface. I have no idea where to get it or even where to start it. The setup instructions see to be a bit vague.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '10, 08:06</strong></p><img src="https://secure.gravatar.com/avatar/5abaefd991191fbe282e3b884f171521?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="goofnoff2&#39;s gravatar image" /><p><span>goofnoff2</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="goofnoff2 has no accepted answers">0%</span></p></div></div><div id="comments-container-1108" class="comments-container"></div><div id="comment-tools-1108" class="comment-tools"></div><div class="clear"></div><div id="comment-1108-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

