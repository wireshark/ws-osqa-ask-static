+++
type = "question"
title = "Play Xbox 360 and a person takes me out of it. Help!"
description = '''I downloaded wireshark because this is a program that analyzes packets and tracks their destination or source. So i play Xbox 360 and 1 guy that hates me got my IP Address and he has a booter system and eveytime he will be booting me out of xbox and will have trouble connecting back. So 1 day i was ...'''
date = "2012-07-11T04:16:00Z"
lastmod = "2012-07-11T04:16:00Z"
weight = 12608
keywords = [ "ip", "booter", "booting", "360", "xbox" ]
aliases = [ "/questions/12608" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Play Xbox 360 and a person takes me out of it. Help!](/questions/12608/play-xbox-360-and-a-person-takes-me-out-of-it-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12608-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12608-score" class="post-score" title="current number of votes">0</div><span id="post-12608-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded wireshark because this is a program that analyzes packets and tracks their destination or source. So i play Xbox 360 and 1 guy that hates me got my IP Address and he has a booter system and eveytime he will be booting me out of xbox and will have trouble connecting back. So 1 day i was using the Wireshark program and was live capturing the packets and suddenly was booted from xbox and in the analyzer a red line appeared and after 5 minutes another red line appeared and after that i was back online, so im guessing he booted me off my connection for 5 minutes. So i check the IP address of the red line on the source column and it told me like a wierd website or a wanna be website so i manually resolve address and it gave me a ip address and looked it up and it said that it was a google ip address, so im guessing the person doing this to me is using a psecial booter that uses other ip addresses from other people. What i want is someone out there if they could help me out and trying to figure out the real ip address, telling me what to do and what not. One thing i know for sure, to resolve this i can change my ip address and that would solve the problem, but what i want first is to find out his real ip address and then change my ip address. So whoever can help, please post. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-booter" rel="tag" title="see questions tagged &#39;booter&#39;">booter</span> <span class="post-tag tag-link-booting" rel="tag" title="see questions tagged &#39;booting&#39;">booting</span> <span class="post-tag tag-link-360" rel="tag" title="see questions tagged &#39;360&#39;">360</span> <span class="post-tag tag-link-xbox" rel="tag" title="see questions tagged &#39;xbox&#39;">xbox</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '12, 04:16</strong></p><img src="https://secure.gravatar.com/avatar/7ceb968b69c86454bd1de920e1a38ec8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jm200&#39;s gravatar image" /><p><span>jm200</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jm200 has no accepted answers">0%</span></p></div></div><div id="comments-container-12608" class="comments-container"></div><div id="comment-tools-12608" class="comment-tools"></div><div class="clear"></div><div id="comment-12608-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

