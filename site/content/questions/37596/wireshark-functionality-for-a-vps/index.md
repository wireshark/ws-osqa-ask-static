+++
type = "question"
title = "Wireshark functionality for a VPS"
description = '''Hi!  Me and a couple of friends are hosting a VPS with a gameserver on it.  One of our developers has quit (he was working scripts in C#) and has become a mayor pain in the !@$ claiming he has a backdoor inside multiple scripts. Can Wireshark help us sniff out what files he uses to connect with his ...'''
date = "2014-11-05T14:51:00Z"
lastmod = "2014-11-05T14:51:00Z"
weight = 37596
keywords = [ "c#", "backdoor", "vps" ]
aliases = [ "/questions/37596" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark functionality for a VPS](/questions/37596/wireshark-functionality-for-a-vps)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37596-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37596-score" class="post-score" title="current number of votes">0</div><span id="post-37596-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! Me and a couple of friends are hosting a VPS with a gameserver on it. One of our developers has quit (he was working scripts in C#) and has become a mayor pain in the <span class="__cf_email__" data-cfemail="6f4e2f">[email protected]</span>$ claiming he has a backdoor inside multiple scripts.</p><p>Can Wireshark help us sniff out what files he uses to connect with his backdoor? If so, What do we need to do?</p><p>thanks a lot for reading and pardon my bad english</p><p>Sincerely,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-c#" rel="tag" title="see questions tagged &#39;c#&#39;">c#</span> <span class="post-tag tag-link-backdoor" rel="tag" title="see questions tagged &#39;backdoor&#39;">backdoor</span> <span class="post-tag tag-link-vps" rel="tag" title="see questions tagged &#39;vps&#39;">vps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 14:51</strong></p><img src="https://secure.gravatar.com/avatar/ccc939b3ca34d1f6205d6758c81657bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DestinySenpai&#39;s gravatar image" /><p><span>DestinySenpai</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DestinySenpai has no accepted answers">0%</span></p></div></div><div id="comments-container-37596" class="comments-container"></div><div id="comment-tools-37596" class="comment-tools"></div><div class="clear"></div><div id="comment-37596-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

