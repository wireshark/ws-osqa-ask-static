+++
type = "question"
title = "Not Able to Install IOS App. from Server"
description = '''Hi, I am facing wired issues where we have custom developed application for IOS publish on internal server. When ipad users opens the url and download the application it get stuck up after some time and throw error. I have manage to capture the traffic near to client and bit confuse what is exactly ...'''
date = "2015-06-28T21:58:00Z"
lastmod = "2015-06-28T22:01:00Z"
weight = 43645
keywords = [ "ios" ]
aliases = [ "/questions/43645" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not Able to Install IOS App. from Server](/questions/43645/not-able-to-install-ios-app-from-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43645-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43645-score" class="post-score" title="current number of votes">0</div><span id="post-43645-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am facing wired issues where we have custom developed application for IOS publish on internal server. When ipad users opens the url and download the application it get stuck up after some time and throw error. I have manage to capture the traffic near to client and bit confuse what is exactly happening. Any help would be really appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ios" rel="tag" title="see questions tagged &#39;ios&#39;">ios</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '15, 21:58</strong></p><img src="https://secure.gravatar.com/avatar/8517ffa9deb502f1e73ff0a735155b21?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mehul%20Patel&#39;s gravatar image" /><p><span>Mehul Patel</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mehul Patel has no accepted answers">0%</span></p></div></div><div id="comments-container-43645" class="comments-container"><span id="43646"></span><div id="comment-43646" class="comment"><div id="post-43646-score" class="comment-score"></div><div class="comment-text"><p>WireShark capture snap..</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_ReAhjAt.PNG" alt="alt text" /></p></div><div id="comment-43646-info" class="comment-info"><span class="comment-age">(28 Jun '15, 22:01)</span> <span class="comment-user userinfo">Mehul Patel</span></div></div></div><div id="comment-tools-43645" class="comment-tools"></div><div class="clear"></div><div id="comment-43645-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

