+++
type = "question"
title = "Wireshark data packet Time internal"
description = '''I capture two wireshark packet, the time stamp is as below: 2.188099000  2.243311000 Can someone inform me how much is the time internal for those two packets?'''
date = "2014-06-02T19:31:00Z"
lastmod = "2014-06-03T14:34:00Z"
weight = 33329
keywords = [ "timestamp" ]
aliases = [ "/questions/33329" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark data packet Time internal](/questions/33329/wireshark-data-packet-time-internal)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33329-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33329-score" class="post-score" title="current number of votes">0</div><span id="post-33329-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I capture two wireshark packet, the time stamp is as below:</p><p>2.188099000 2.243311000</p><p>Can someone inform me how much is the time internal for those two packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '14, 19:31</strong></p><img src="https://secure.gravatar.com/avatar/abbbd4f0eed2d71176528e800dd17d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rabbit&#39;s gravatar image" /><p><span>rabbit</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rabbit has no accepted answers">0%</span></p></div></div><div id="comments-container-33329" class="comments-container"><span id="33330"></span><div id="comment-33330" class="comment"><div id="post-33330-score" class="comment-score"></div><div class="comment-text"><p>Are you asking someone to subtract the first time from the second time for you?</p></div><div id="comment-33330-info" class="comment-info"><span class="comment-age">(02 Jun '14, 21:27)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-33329" class="comment-tools"></div><div class="clear"></div><div id="comment-33329-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33333"></span>

<div id="answer-container-33333" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33333-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33333-score" class="post-score" title="current number of votes">1</div><span id="post-33333-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>O.K. I'll do it ;-))</p><blockquote><p>abs(2.188099000 - 2.243311000) = 0.055212</p></blockquote><p>So, the answer is: 0.055212</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '14, 00:11</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span></p></div></div><div id="comments-container-33333" class="comments-container"><span id="33360"></span><div id="comment-33360" class="comment"><div id="post-33360-score" class="comment-score"></div><div class="comment-text"><p>I made the comment an answer, as I believe it is a valid answer ;-))</p></div><div id="comment-33360-info" class="comment-info"><span class="comment-age">(03 Jun '14, 14:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33333" class="comment-tools"></div><div class="clear"></div><div id="comment-33333-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

