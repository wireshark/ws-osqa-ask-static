+++
type = "question"
title = "[closed] Wireshark Lua - Rich text formatting without third-party packages"
description = '''HI,  Currently I have a Wireshark Lua Text Window with data similar to Follow Tcp Stream Window. I need to show request in one color and response in another color. I understood that it is not possible to show in Wireshark Lua Text window since it doesn&#x27;t support. So I want to know whether it can be ...'''
date = "2012-03-05T01:31:00Z"
lastmod = "2012-03-06T10:32:00Z"
weight = 9352
keywords = [ "lua" ]
aliases = [ "/questions/9352" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Wireshark Lua - Rich text formatting without third-party packages](/questions/9352/wireshark-lua-rich-text-formatting-without-third-party-packages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9352-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9352-score" class="post-score" title="current number of votes">0</div><span id="post-9352-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI,</p><p>Currently I have a Wireshark Lua Text Window with data similar to Follow Tcp Stream Window. I need to show request in one color and response in another color. I understood that it is not possible to show in Wireshark Lua Text window since it doesn't support. So I want to know whether it can be achieved by saving to a file without the use of any THIRD PARTY PACKAGES. Thanks Rijith</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '12, 01:31</strong></p><img src="https://secure.gravatar.com/avatar/eaba5d948ba0b95759c596eb2c6e7ecb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rijith&#39;s gravatar image" /><p><span>Rijith</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rijith has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>07 Mar '12, 07:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-9352" class="comments-container"><span id="9358"></span><div id="comment-9358" class="comment"><div id="post-9358-score" class="comment-score">2</div><div class="comment-text"><p>Oops. Similar but not identical to <a href="http://ask.wireshark.org/questions/9289/can-i-print-colored-text-in-the-lua-textwindow">your previous question</a>. Yes, Lua can output arbitrary bytes to a file, including escape codes and RTF, but it doesn't have anything built-in for that. You'd need a 3rd party package or hand-written code.</p></div><div id="comment-9358-info" class="comment-info"><span class="comment-age">(05 Mar '12, 05:24)</span> <span class="comment-user userinfo">bstn</span></div></div><span id="9359"></span><div id="comment-9359" class="comment"><div id="post-9359-score" class="comment-score">1</div><div class="comment-text"><p>Aha! I thought I saw a <a href="http://ask.wireshark.org/questions/9350/wireshark-lua-output-to-a-file-in-colored-text-possible">duplicate</a>! Please <strong>stop</strong> doing that.</p></div><div id="comment-9359-info" class="comment-info"><span class="comment-age">(05 Mar '12, 05:30)</span> <span class="comment-user userinfo">bstn</span></div></div><span id="9400"></span><div id="comment-9400" class="comment"><div id="post-9400-score" class="comment-score"></div><div class="comment-text"><p>This is actually off-topic. I suggest Googling "RTF syntax" and "Lua file I/O".</p></div><div id="comment-9400-info" class="comment-info"><span class="comment-age">(06 Mar '12, 10:32)</span> <span class="comment-user userinfo">bstn</span></div></div></div><div id="comment-tools-9352" class="comment-tools"></div><div class="clear"></div><div id="comment-9352-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by multipleinterfaces 07 Mar '12, 07:51

</div>

</div>

</div>

