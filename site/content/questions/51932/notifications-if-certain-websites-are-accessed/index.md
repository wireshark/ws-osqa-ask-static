+++
type = "question"
title = "notifications if certain websites are accessed?"
description = '''is there a way to be notified by connections to a certian domain?. For example, if my son accessed an adult site i could be notified.'''
date = "2016-04-25T11:45:00Z"
lastmod = "2016-04-25T13:09:00Z"
weight = 51932
keywords = [ "notification" ]
aliases = [ "/questions/51932" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [notifications if certain websites are accessed?](/questions/51932/notifications-if-certain-websites-are-accessed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51932-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51932-score" class="post-score" title="current number of votes">0</div><span id="post-51932-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is there a way to be notified by connections to a certian domain?. For example, if my son accessed an adult site i could be notified.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-notification" rel="tag" title="see questions tagged &#39;notification&#39;">notification</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '16, 11:45</strong></p><img src="https://secure.gravatar.com/avatar/e35259658089f517ef2e6dac737ef23e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Luke%20Price&#39;s gravatar image" /><p><span>Luke Price</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Luke Price has no accepted answers">0%</span></p></div></div><div id="comments-container-51932" class="comments-container"></div><div id="comment-tools-51932" class="comment-tools"></div><div class="clear"></div><div id="comment-51932-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51938"></span>

<div id="answer-container-51938" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51938-score" class="post-score" title="current number of votes">1</div><span id="post-51938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a task for an IDS (e.g. Snort or Suricata), not for Wireshark. IDS do pattern matching on network packets and can alert on what they find based on the rules you define. Wireshark is used for manual packet inspection instead.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Apr '16, 13:09</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-51938" class="comments-container"></div><div id="comment-tools-51938" class="comment-tools"></div><div class="clear"></div><div id="comment-51938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

