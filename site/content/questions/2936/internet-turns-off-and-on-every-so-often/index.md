+++
type = "question"
title = "internet turns off and on every so often"
description = '''i run windows 7, every once in a while, my internet connection would &quot;blink&quot; all internet would stop for maybe under 5 seconds, and come right back on, no reason that i can find for this, its not a hardware problem, it happens on both wired and wireless, on the icon in the corner does not change to ...'''
date = "2011-03-19T22:25:00Z"
lastmod = "2011-03-20T16:53:00Z"
weight = 2936
keywords = [ "windows7", "network", "internet" ]
aliases = [ "/questions/2936" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [internet turns off and on every so often](/questions/2936/internet-turns-off-and-on-every-so-often)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2936-score" class="post-score" title="current number of votes">0</div><span id="post-2936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i run windows 7, every once in a while, my internet connection would "blink" all internet would stop for maybe under 5 seconds, and come right back on, no reason that i can find for this, its not a hardware problem, it happens on both wired and wireless, on the icon in the corner does not change to say anything is wrong, the net just turns off for a moment then comes back, i want to find a software that will tell me where the problem is, what is running that is causing the problem</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '11, 22:25</strong></p><img src="https://secure.gravatar.com/avatar/456599592928fb5cb70f857d00396d91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thor1701&#39;s gravatar image" /><p><span>thor1701</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thor1701 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-2936" class="comments-container"><span id="2939"></span><div id="comment-2939" class="comment"><div id="post-2939-score" class="comment-score"></div><div class="comment-text"><p>How do you know that the "internet stops" ?</p><p>Browser stops updating ? or what ?</p></div><div id="comment-2939-info" class="comment-info"><span class="comment-age">(20 Mar '11, 08:31)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="2954"></span><div id="comment-2954" class="comment"><div id="post-2954-score" class="comment-score"></div><div class="comment-text"><p>while i am playing an online game, it disconnects, while im waiting for a webpage to load up it stops, and my IM apps disconnect and if im downloading something at the time it stops as well</p></div><div id="comment-2954-info" class="comment-info"><span class="comment-age">(20 Mar '11, 14:36)</span> <span class="comment-user userinfo">thor1701</span></div></div></div><div id="comment-tools-2936" class="comment-tools"></div><div class="clear"></div><div id="comment-2936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2957"></span>

<div id="answer-container-2957" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2957-score" class="post-score" title="current number of votes">0</div><span id="post-2957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I've already have the same problem! For ex: when i put www.cnn.com, starts loading, it says that the server taked to long, and then when i refresh the page, its like a bullet! But it hapens with other websites</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '11, 16:44</strong></p><img src="https://secure.gravatar.com/avatar/2d329d62c3c21cd6a55252919d0d82fe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MareAlta&#39;s gravatar image" /><p><span>MareAlta</span><br />
<span class="score" title="-1 reputation points">-1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MareAlta has no accepted answers">0%</span></p></div></div><div id="comments-container-2957" class="comments-container"></div><div id="comment-tools-2957" class="comment-tools"></div><div class="clear"></div><div id="comment-2957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="2958"></span>

<div id="answer-container-2958" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2958-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2958-score" class="post-score" title="current number of votes">0</div><span id="post-2958-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>i finaly had the notion to look at the event viewer and found this, "The IP address lease 192.168.0.8 for the Network Card with network address 0x001d92a9bcf4 has been denied by the DHCP server 192.168.0.1 (The DHCP Server sent a DHCPNACK message)."</p><p>now i know what the issue is i have no idea how to fix it and i've looked all over for an answer</p></div><div class="answer-controls post-controls"><div class="community-wiki">This answer is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '11, 16:53</strong></p><img src="https://secure.gravatar.com/avatar/456599592928fb5cb70f857d00396d91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thor1701&#39;s gravatar image" /><p><span>thor1701</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thor1701 has no accepted answers">0%</span></p></div></div><div id="comments-container-2958" class="comments-container"></div><div id="comment-tools-2958" class="comment-tools"></div><div class="clear"></div><div id="comment-2958-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

