+++
type = "question"
title = "Xbox live unauthorised access"
description = '''Can anyone help me? My son&#x27;s Xbox live account was accessed by another person who violated their terms of use. They insist the messages were sent from his IP Address but we know they weren&#x27;t. I asked them to verify the MAC of the PC used to access the account but they say they can&#x27;t do that. Surely ...'''
date = "2013-10-12T01:24:00Z"
lastmod = "2013-10-12T02:13:00Z"
weight = 25930
keywords = [ "mac-address" ]
aliases = [ "/questions/25930" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Xbox live unauthorised access](/questions/25930/xbox-live-unauthorised-access)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25930-score" class="post-score" title="current number of votes">0</div><span id="post-25930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone help me? My son's Xbox live account was accessed by another person who violated their terms of use. They insist the messages were sent from his IP Address but we know they weren't. I asked them to verify the MAC of the PC used to access the account but they say they can't do that. Surely they can?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '13, 01:24</strong></p><img src="https://secure.gravatar.com/avatar/d220384a4ab3a0f9bc8dc58a1d5e71d0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="monetteg&#39;s gravatar image" /><p><span>monetteg</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="monetteg has no accepted answers">0%</span></p></div></div><div id="comments-container-25930" class="comments-container"></div><div id="comment-tools-25930" class="comment-tools"></div><div class="clear"></div><div id="comment-25930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25931"></span>

<div id="answer-container-25931" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25931-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25931-score" class="post-score" title="current number of votes">0</div><span id="post-25931-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Microsoft cannot check the MAC address, as that address is only used in <strong>your</strong> network. Maybe the PC of your son has been hacked, or your son violated their terms of use and now is afraid to admit it ;-)) You could ask for the time of the fraudulent access and then check if it could have been your son or not.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '13, 02:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25931" class="comments-container"></div><div id="comment-tools-25931" class="comment-tools"></div><div class="clear"></div><div id="comment-25931-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

