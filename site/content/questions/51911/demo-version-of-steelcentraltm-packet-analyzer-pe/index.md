+++
type = "question"
title = "Demo version of SteelCentral™ Packet Analyzer PE"
description = '''Hello, I was just curious if there a demo of SteelCentral™ Packet Analyzer PE that could be use (for 7/15 days) just to try and fiddle with it before possibly buying the annual subscription? Thanks -Cody'''
date = "2016-04-24T09:57:00Z"
lastmod = "2016-04-24T11:40:00Z"
weight = 51911
keywords = [ "steelcentral", "demo", "analyzer", "packet", "trial" ]
aliases = [ "/questions/51911" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Demo version of SteelCentral™ Packet Analyzer PE](/questions/51911/demo-version-of-steelcentraltm-packet-analyzer-pe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51911-score" class="post-score" title="current number of votes">0</div><span id="post-51911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I was just curious if there a demo of SteelCentral™ Packet Analyzer PE that could be use (for 7/15 days) just to try and fiddle with it before possibly buying the annual subscription?</p><p>Thanks -Cody</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-steelcentral" rel="tag" title="see questions tagged &#39;steelcentral&#39;">steelcentral</span> <span class="post-tag tag-link-demo" rel="tag" title="see questions tagged &#39;demo&#39;">demo</span> <span class="post-tag tag-link-analyzer" rel="tag" title="see questions tagged &#39;analyzer&#39;">analyzer</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-trial" rel="tag" title="see questions tagged &#39;trial&#39;">trial</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '16, 09:57</strong></p><img src="https://secure.gravatar.com/avatar/450ade685ffe9eff44aed764db50d44a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="carnould&#39;s gravatar image" /><p><span>carnould</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="carnould has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Apr '16, 06:20</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-51911" class="comments-container"></div><div id="comment-tools-51911" class="comment-tools"></div><div class="clear"></div><div id="comment-51911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51913"></span>

<div id="answer-container-51913" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51913-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51913-score" class="post-score" title="current number of votes">1</div><span id="post-51913-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you can find all Riverbed trial downloads here: <a href="http://www.riverbed.com/trialdownloads.html">http://www.riverbed.com/trialdownloads.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Apr '16, 11:40</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-51913" class="comments-container"></div><div id="comment-tools-51913" class="comment-tools"></div><div class="clear"></div><div id="comment-51913-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

