+++
type = "question"
title = "delete saved filter from filter bar"
description = '''I just installed v1.8.6. The filter bar has an option to save the current filter. Once saved under a name, the filter shows up on the filter bar to the right of the &quot;Save&quot; button.  But there is no way to delete or override a saved filter. Even when I use the same name, another filter shows up with t...'''
date = "2013-04-17T20:37:00Z"
lastmod = "2016-04-06T00:04:00Z"
weight = 20537
keywords = [ "filter", "delete" ]
aliases = [ "/questions/20537" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [delete saved filter from filter bar](/questions/20537/delete-saved-filter-from-filter-bar)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20537-score" class="post-score" title="current number of votes">3</div><span id="post-20537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed v1.8.6. The filter bar has an option to save the current filter. Once saved under a name, the filter shows up on the filter bar to the right of the "Save" button.</p><p>But there is no way to delete or override a saved filter. Even when I use the same name, another filter shows up with the same name. I can't find the delete option anywhere. Did the developer forget to implement delete?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-delete" rel="tag" title="see questions tagged &#39;delete&#39;">delete</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '13, 20:37</strong></p><img src="https://secure.gravatar.com/avatar/874210770437fe869c01c135678178e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ctny&#39;s gravatar image" /><p><span>ctny</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ctny has no accepted answers">0%</span></p></div></div><div id="comments-container-20537" class="comments-container"></div><div id="comment-tools-20537" class="comment-tools"></div><div class="clear"></div><div id="comment-20537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20541"></span>

<div id="answer-container-20541" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20541-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20541-score" class="post-score" title="current number of votes">12</div><span id="post-20541-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Edit &gt; Preferences &gt; Filter Expressions. Select the filter you no longer want and click Remove.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '13, 21:42</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-20541" class="comments-container"><span id="23099"></span><div id="comment-23099" class="comment"><div id="post-23099-score" class="comment-score"></div><div class="comment-text"><p>Thanks</p></div><div id="comment-23099-info" class="comment-info"><span class="comment-age">(18 Jul '13, 03:47)</span> <span class="comment-user userinfo">Sanjay</span></div></div><span id="51432"></span><div id="comment-51432" class="comment"><div id="post-51432-score" class="comment-score"></div><div class="comment-text"><p>Thanks you! I have tried a lot to find the workaround of this.</p></div><div id="comment-51432-info" class="comment-info"><span class="comment-age">(06 Apr '16, 00:04)</span> <span class="comment-user userinfo">KostasA</span></div></div></div><div id="comment-tools-20541" class="comment-tools"></div><div class="clear"></div><div id="comment-20541-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

