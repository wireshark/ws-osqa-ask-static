+++
type = "question"
title = "Wireshark setup with OSX Lion"
description = '''Can anyone give me step-by-step instructions on setting up Wireshark on OSX Lion?'''
date = "2011-10-09T08:19:00Z"
lastmod = "2011-10-09T14:25:00Z"
weight = 6812
keywords = [ "osx", "lion", "installation" ]
aliases = [ "/questions/6812" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark setup with OSX Lion](/questions/6812/wireshark-setup-with-osx-lion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6812-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6812-score" class="post-score" title="current number of votes">0</div><span id="post-6812-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone give me step-by-step instructions on setting up Wireshark on OSX Lion?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-lion" rel="tag" title="see questions tagged &#39;lion&#39;">lion</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '11, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/a30ae0a2437f3cf53a3740c5344585ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JAG&#39;s gravatar image" /><p><span>JAG</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JAG has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Oct '11, 09:18</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6812" class="comments-container"><span id="6813"></span><div id="comment-6813" class="comment"><div id="post-6813-score" class="comment-score"></div><div class="comment-text"><p>Do you know if your latest Snow Leopard compatible release is also compatible with OSX Lion, or do you have a projected release date for WS for Lion?</p></div><div id="comment-6813-info" class="comment-info"><span class="comment-age">(09 Oct '11, 08:20)</span> <span class="comment-user userinfo">JAG</span></div></div></div><div id="comment-tools-6812" class="comment-tools"></div><div class="clear"></div><div id="comment-6812-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6817"></span>

<div id="answer-container-6817" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6817-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6817-score" class="post-score" title="current number of votes">0</div><span id="post-6817-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To answer the first question:</p><ol><li>Download the 10.6 dmg and, if it's not opened and mounted automatically, open it from the download directory;</li><li>Run the installer.</li></ol><p>Yes, it's compatible with Lion.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Oct '11, 14:25</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-6817" class="comments-container"></div><div id="comment-tools-6817" class="comment-tools"></div><div class="clear"></div><div id="comment-6817-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

