+++
type = "question"
title = "Viewing incoming remote traffic."
description = '''I&#x27;m new to networking in general, but I have some experience using sockets in C, etc. So I&#x27;m familiar with it. I need to view the traffic of a file that&#x27;s being downloaded on OSX by a system process that&#x27;s completely unrelated to the web browser; I&#x27;m pretty sure it&#x27;s doing this over TCP. I&#x27;m on a wi...'''
date = "2017-02-08T20:06:00Z"
lastmod = "2017-02-09T07:43:00Z"
weight = 59267
keywords = [ "download", "router", "remote", "tcp" ]
aliases = [ "/questions/59267" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Viewing incoming remote traffic.](/questions/59267/viewing-incoming-remote-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59267-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59267-score" class="post-score" title="current number of votes">0</div><span id="post-59267-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm new to networking in general, but I have some experience using sockets in C, etc. So I'm familiar with it. I need to view the traffic of a file that's being downloaded on OSX by a system process that's completely unrelated to the web browser; I'm pretty sure it's doing this over TCP.</p><p>I'm on a wireless network and I have access to my router, if that helps.<br />
</p><p>Is this simple, or does it require explicit configuration? If it does, any useful info or good links to look at?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '17, 20:06</strong></p><img src="https://secure.gravatar.com/avatar/68fdb443d91137df1411a119e2a3287f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="openheartsurgery&#39;s gravatar image" /><p><span>openheartsur...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="openheartsurgery has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-59267" class="comments-container"></div><div id="comment-tools-59267" class="comment-tools"></div><div class="clear"></div><div id="comment-59267-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59291"></span>

<div id="answer-container-59291" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59291-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59291-score" class="post-score" title="current number of votes">0</div><span id="post-59291-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure. Here is some reading material:</p><p>Capturing on a wired network: <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p><p>Capturing on a wireless network: <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p><p>My recommendation would be to try to use the wired solution.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '17, 07:43</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-59291" class="comments-container"></div><div id="comment-tools-59291" class="comment-tools"></div><div class="clear"></div><div id="comment-59291-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

