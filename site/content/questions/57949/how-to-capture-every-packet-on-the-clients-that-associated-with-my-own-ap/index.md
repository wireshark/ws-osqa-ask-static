+++
type = "question"
title = "How to capture every packet on the clients that associated with my own AP"
description = '''Hey there, anyone can explain on how to capture the packet for different clients that associated with my AP. In this case i&#x27;ll be using wlan0 interface, as for now when i run the Wireshark it only capture packets only at my notebook.  Your cooperation are highly appreciated.'''
date = "2016-12-08T01:24:00Z"
lastmod = "2016-12-08T03:06:00Z"
weight = 57949
keywords = [ "access", "accesspoint" ]
aliases = [ "/questions/57949" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture every packet on the clients that associated with my own AP](/questions/57949/how-to-capture-every-packet-on-the-clients-that-associated-with-my-own-ap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57949-score" class="post-score" title="current number of votes">0</div><span id="post-57949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey there, anyone can explain on how to capture the packet for different clients that associated with my AP. In this case i'll be using wlan0 interface, as for now when i run the Wireshark it only capture packets only at my notebook.</p><p>Your cooperation are highly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-access" rel="tag" title="see questions tagged &#39;access&#39;">access</span> <span class="post-tag tag-link-accesspoint" rel="tag" title="see questions tagged &#39;accesspoint&#39;">accesspoint</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '16, 01:24</strong></p><img src="https://secure.gravatar.com/avatar/54c41daea908d797b1db1d719e3641e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="WSneedguid&#39;s gravatar image" /><p><span>WSneedguid</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="WSneedguid has no accepted answers">0%</span></p></div></div><div id="comments-container-57949" class="comments-container"></div><div id="comment-tools-57949" class="comment-tools"></div><div class="clear"></div><div id="comment-57949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57951"></span>

<div id="answer-container-57951" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57951-score" class="post-score" title="current number of votes">1</div><span id="post-57951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the wiki page on <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">WLAN capture</a> setup. Basically you have to set the interface into monitor mode.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '16, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-57951" class="comments-container"><span id="57955"></span><div id="comment-57955" class="comment"><div id="post-57955-score" class="comment-score"></div><div class="comment-text"><p>thank you appreciate for your fast replied and if i still can configure it out, will do the shoutout again.</p></div><div id="comment-57955-info" class="comment-info"><span class="comment-age">(08 Dec '16, 03:06)</span> <span class="comment-user userinfo">WSneedguid</span></div></div></div><div id="comment-tools-57951" class="comment-tools"></div><div class="clear"></div><div id="comment-57951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

