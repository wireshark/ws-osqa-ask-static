+++
type = "question"
title = "Data about old packets"
description = '''Hi everybody: I would want to know how to see data about old packets(For example:Select a packet of 6 days ago).Is it possible? Thanks in advance. Francisco.'''
date = "2015-04-04T14:22:00Z"
lastmod = "2015-04-06T07:24:00Z"
weight = 41192
keywords = [ "tcp" ]
aliases = [ "/questions/41192" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Data about old packets](/questions/41192/data-about-old-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41192-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41192-score" class="post-score" title="current number of votes">0</div><span id="post-41192-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everybody: I would want to know how to see data about old packets(For example:Select a packet of 6 days ago).Is it possible? Thanks in advance. Francisco.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '15, 14:22</strong></p><img src="https://secure.gravatar.com/avatar/8d108f1516050680b41cb506ebb6388b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="3J%20Kernel&#39;s gravatar image" /><p><span>3J Kernel</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="3J Kernel has no accepted answers">0%</span></p></div></div><div id="comments-container-41192" class="comments-container"></div><div id="comment-tools-41192" class="comment-tools"></div><div class="clear"></div><div id="comment-41192-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="41214"></span>

<div id="answer-container-41214" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41214-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41214-score" class="post-score" title="current number of votes">1</div><span id="post-41214-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="3J Kernel has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I believe, you are asking how to view data in Wireshark that has been sent over the network a couple of days ago. If so, <strong>that's not possible if you started Wireshark only today</strong>. Please see my answer to a similar question:</p><blockquote><p><a href="https://ask.wireshark.org/questions/30120/search-a-package-in-specific-time">https://ask.wireshark.org/questions/30120/search-a-package-in-specific-time</a><br />
</p></blockquote><p>If I misinterpreted your question, the answer of <span>@mrEEde</span> is the right answer.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '15, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-41214" class="comments-container"><span id="41220"></span><div id="comment-41220" class="comment"><div id="post-41220-score" class="comment-score"></div><div class="comment-text"><p>Yes, I mean that question.Thanks for the answer(and thank you to <span>@mrEEde</span> for his answer too)</p></div><div id="comment-41220-info" class="comment-info"><span class="comment-age">(06 Apr '15, 07:24)</span> <span class="comment-user userinfo">3J Kernel</span></div></div></div><div id="comment-tools-41214" class="comment-tools"></div><div class="clear"></div><div id="comment-41214-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="41196"></span>

<div id="answer-container-41196" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41196-score" class="post-score" title="current number of votes">0</div><span id="post-41196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If I understand your question correctly you are looking for display filter. The time display format can be changed to seconds since epoch. If your current epoch time is frame.time_epoch == 1427705316.066260000 and you want to see packets 6 days ago you need to substract 3600x24x6 518400 seconds and you get the epoch time from 6 days earlier and construct a display filter using</p><pre><code>frame.time_epoch gt aaaaaaaaaa and frame.time_epoch lt bbbbbbbbbb</code></pre><p>- It's probably easier to use the editcap -A -B options and specify the desired timeframe</p><pre><code>editcap -A 2015-03-3017:18:32 -B 2015-03-3017:24:00 originalfile.pcapng t171832h.pcapng</code></pre><p>Regards Matthias</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '15, 23:51</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Apr '15, 23:57</strong> </span></p></div></div><div id="comments-container-41196" class="comments-container"></div><div id="comment-tools-41196" class="comment-tools"></div><div class="clear"></div><div id="comment-41196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

