+++
type = "question"
title = "Cannot see packets on a wireshark."
description = '''We have a program that listens on a particular port i.e 3810 on TCP ( windows server 2008 R2) . When the program is running and listening to the port - confirmed by issuing netstat , we cannot see the packets in wire shark being captured on this port 3810. However when we stop the program we see the...'''
date = "2011-09-28T04:32:00Z"
lastmod = "2011-09-28T04:32:00Z"
weight = 6614
keywords = [ "socket" ]
aliases = [ "/questions/6614" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot see packets on a wireshark.](/questions/6614/cannot-see-packets-on-a-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6614-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6614-score" class="post-score" title="current number of votes">0</div><span id="post-6614-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have a program that listens on a particular port i.e 3810 on TCP ( windows server 2008 R2) . When the program is running and listening to the port - confirmed by issuing netstat , we cannot see the packets in wire shark being captured on this port 3810. However when we stop the program we see the packets in wire shark . Please help .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-socket" rel="tag" title="see questions tagged &#39;socket&#39;">socket</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Sep '11, 04:32</strong></p><img src="https://secure.gravatar.com/avatar/33f6af99132bffacae4a3a5b947be8bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JOSH1978&#39;s gravatar image" /><p><span>JOSH1978</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JOSH1978 has no accepted answers">0%</span></p></div></div><div id="comments-container-6614" class="comments-container"></div><div id="comment-tools-6614" class="comment-tools"></div><div class="clear"></div><div id="comment-6614-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

