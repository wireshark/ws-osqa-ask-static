+++
type = "question"
title = "no GUI after startup"
description = '''Left-clicking appears to start Wireshark, which only offers a taskbar icon/button but no visible GUI window. This takes place with both 32- and 64-bit installations on 64-bit Windows 7. The Wireshark install is using the default settings.'''
date = "2011-09-29T21:58:00Z"
lastmod = "2011-09-30T05:32:00Z"
weight = 6646
keywords = [ "windows", "windows7", "startup" ]
aliases = [ "/questions/6646" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [no GUI after startup](/questions/6646/no-gui-after-startup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6646-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6646-score" class="post-score" title="current number of votes">0</div><span id="post-6646-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Left-clicking appears to start Wireshark, which only offers a taskbar icon/button but no visible GUI window. This takes place with both 32- and 64-bit installations on 64-bit Windows 7. The Wireshark install is using the default settings.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '11, 21:58</strong></p><img src="https://secure.gravatar.com/avatar/d34d65f464aa68da04cf7778590d8153?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="user&#39;s gravatar image" /><p><span>user</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="user has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Oct '11, 15:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6646" class="comments-container"><span id="6652"></span><div id="comment-6652" class="comment"><div id="post-6652-score" class="comment-score"></div><div class="comment-text"><p>First Start or did you use wireshark before?</p></div><div id="comment-6652-info" class="comment-info"><span class="comment-age">(30 Sep '11, 03:21)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="6653"></span><div id="comment-6653" class="comment"><div id="post-6653-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark are you running? Do you get any more information if you try to start up Wireshark from the command-line?</p></div><div id="comment-6653-info" class="comment-info"><span class="comment-age">(30 Sep '11, 05:32)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-6646" class="comment-tools"></div><div class="clear"></div><div id="comment-6646-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

