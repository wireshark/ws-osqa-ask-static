+++
type = "question"
title = "Error running wireshark on HP-UX"
description = '''Hi All! i&#x27;m getting this error message everytime i try to run wireshark on my HP-UX machine running HP-UX 11i v2 Operating System: ========================================================Start-of-Error-Message ./wireshark (wireshark:17651): GLib-GObject-WARNING **: specified class size for type Pang...'''
date = "2011-04-06T02:37:00Z"
lastmod = "2011-04-06T02:37:00Z"
weight = 3368
keywords = [ "pangoxftfontmap" ]
aliases = [ "/questions/3368" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Error running wireshark on HP-UX](/questions/3368/error-running-wireshark-on-hp-ux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3368-score" class="post-score" title="current number of votes">0</div><span id="post-3368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All! i'm getting this error message everytime i try to run wireshark on my HP-UX machine running HP-UX 11i v2 Operating System: ========================================================Start-of-Error-Message</p><h1 id="wireshark">./wireshark</h1><p>(wireshark:17651): GLib-GObject-WARNING **: specified class size for type <code>PangoXftFontMap' is smaller than the parent type's</code>PangoFcFontMap' class size</p><p>(wireshark:17651): GLib-GObject-CRITICAL **: file gobject.c: line 1304: assertion `G_TYPE_IS_OBJECT (object_type)' failed Memory fault(coredump)</p><h1 id="section"></h1><p>========================================================End-of-Error-Message</p><p>anybody can help me to solve this?</p></h1></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pangoxftfontmap" rel="tag" title="see questions tagged &#39;pangoxftfontmap&#39;">pangoxftfontmap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '11, 02:37</strong></p><img src="https://secure.gravatar.com/avatar/c0af4adfe9fabe1e2ff447e80fbb9583?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ahmadini&#39;s gravatar image" /><p><span>ahmadini</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ahmadini has no accepted answers">0%</span></p></div></div><div id="comments-container-3368" class="comments-container"></div><div id="comment-tools-3368" class="comment-tools"></div><div class="clear"></div><div id="comment-3368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

