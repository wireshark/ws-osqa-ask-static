+++
type = "question"
title = "Problem with analyse SCTP associations"
description = '''Hi, I have a problem with SCTP association analysis. I have 2 associations from 2 nodes (node1 has IP addresses 172.20.3.20 and 172.20.131.20 and node2 172.20.3.21) to one node3 (172.20.0.2 and 172.20.128.2) nodes1 and 2 have different verification tags and node3 has 2 verification tags per node 1 a...'''
date = "2011-05-11T05:56:00Z"
lastmod = "2011-05-11T05:56:00Z"
weight = 4031
keywords = [ "sctp" ]
aliases = [ "/questions/4031" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problem with analyse SCTP associations](/questions/4031/problem-with-analyse-sctp-associations)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4031-score" class="post-score" title="current number of votes">0</div><span id="post-4031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have a problem with SCTP association analysis. I have 2 associations from 2 nodes (node1 has IP addresses 172.20.3.20 and 172.20.131.20 and node2 172.20.3.21) to one node3 (172.20.0.2 and 172.20.128.2) nodes1 and 2 have different verification tags and node3 has 2 verification tags per node 1 and 2.</p><p>In "Show All associations" I see 2 associations - ok the I select one of them and click to Analyse - I see window in which in Endpoint section I see all addresses of nodes 1 and 2 (means 4 IP addresses) - but I choosed only one association (for example between node1 and node3) - this is wrong.</p><p>But when I click Chunk Statistics I see proper addresses (from node1 only)</p><p>Please tell me why in Analyse window in Endpoint section I see in window List of used IP-addresses addresses from all associations not only from association which I selected? I verified that each nodes have their own Verification Tags.</p><p>Best Regards Tom (<span class="__cf_email__" data-cfemail="becad1d3dfcdc490dbd4cdd3d1d0cafecadbd2dbd5d1d3cbd0d7d5dfddd4df90ced2">[email protected]</span>)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sctp" rel="tag" title="see questions tagged &#39;sctp&#39;">sctp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 May '11, 05:56</strong></p><img src="https://secure.gravatar.com/avatar/0e6ff404c13ad3036fef82bea619239c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alluryn&#39;s gravatar image" /><p><span>alluryn</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alluryn has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 May '11, 08:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4031" class="comments-container"></div><div id="comment-tools-4031" class="comment-tools"></div><div class="clear"></div><div id="comment-4031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

