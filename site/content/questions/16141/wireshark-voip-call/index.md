+++
type = "question"
title = "wireshark VoIP Call"
description = '''Hello. What does the yellow line on the screenshot? '''
date = "2012-11-20T22:41:00Z"
lastmod = "2012-11-21T04:19:00Z"
weight = 16141
keywords = [ "voip" ]
aliases = [ "/questions/16141" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark VoIP Call](/questions/16141/wireshark-voip-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16141-score" class="post-score" title="current number of votes">0</div><span id="post-16141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. What does the yellow line on the screenshot?</p><p><img src="http://anso02.jino.ru/123.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Nov '12, 22:41</strong></p><img src="https://secure.gravatar.com/avatar/30b6bab9f442bcebcd392a3ee23ba513?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anso02&#39;s gravatar image" /><p><span>anso02</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anso02 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-16141" class="comments-container"></div><div id="comment-tools-16141" class="comment-tools"></div><div class="clear"></div><div id="comment-16141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16146"></span>

<div id="answer-container-16146" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16146-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16146-score" class="post-score" title="current number of votes">2</div><span id="post-16146-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See this <a href="http://ask.wireshark.org/questions/7854">question</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '12, 23:32</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-16146" class="comments-container"><span id="16147"></span><div id="comment-16147" class="comment"><div id="post-16147-score" class="comment-score"></div><div class="comment-text"><p>thank you.</p></div><div id="comment-16147-info" class="comment-info"><span class="comment-age">(20 Nov '12, 23:49)</span> <span class="comment-user userinfo">anso02</span></div></div><span id="16151"></span><div id="comment-16151" class="comment"><div id="post-16151-score" class="comment-score"></div><div class="comment-text"><p>If this answers your question then, as per QA etiquette, please click the check</p></div><div id="comment-16151-info" class="comment-info"><span class="comment-age">(21 Nov '12, 04:19)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-16146" class="comment-tools"></div><div class="clear"></div><div id="comment-16146-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

