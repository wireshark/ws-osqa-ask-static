+++
type = "question"
title = "How to copy IO graph from wireshark to LibreOffice Calc?"
description = '''I am working with Wireshark on Ubuntu. I have collected packets and I am trying to copy IO graph from the IO graph window using &#x27;Copy&#x27; button and paste it into &#x27;LibraOffice Calc&#x27; the excel of Ubuntu. But it is not getting pasted at all. There are only few values getting pasted but not the graph. Cou...'''
date = "2013-12-14T17:00:00Z"
lastmod = "2013-12-14T22:58:00Z"
weight = 28111
keywords = [ "graph", "iograph", "wireshark", "graphs" ]
aliases = [ "/questions/28111" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to copy IO graph from wireshark to LibreOffice Calc?](/questions/28111/how-to-copy-io-graph-from-wireshark-to-libreoffice-calc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28111-score" class="post-score" title="current number of votes">0</div><span id="post-28111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working with Wireshark on Ubuntu. I have collected packets and I am trying to copy IO graph from the IO graph window using 'Copy' button and paste it into 'LibraOffice Calc' the excel of Ubuntu. But it is not getting pasted at all. There are only few values getting pasted but not the graph. Could anyone please let me know how to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Dec '13, 17:00</strong></p><img src="https://secure.gravatar.com/avatar/f9e1104aabb74d8911ec3bb7c49d0e90?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srikanth&#39;s gravatar image" /><p><span>srikanth</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srikanth has no accepted answers">0%</span></p></div></div><div id="comments-container-28111" class="comments-container"></div><div id="comment-tools-28111" class="comment-tools"></div><div class="clear"></div><div id="comment-28111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28112"></span>

<div id="answer-container-28112" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28112-score" class="post-score" title="current number of votes">1</div><span id="post-28112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You cannot paste the graph, only the values - Comma seperated - into seperate columns. It should then be easy enough to draw a graph using the spreadsheet software.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screenshot-22_1.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Dec '13, 22:58</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></img></div></div><div id="comments-container-28112" class="comments-container"></div><div id="comment-tools-28112" class="comment-tools"></div><div class="clear"></div><div id="comment-28112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

