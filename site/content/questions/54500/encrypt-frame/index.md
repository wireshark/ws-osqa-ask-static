+++
type = "question"
title = "Encrypt Frame"
description = '''Hy. I have a task from my lecturer. He told to me how to encrypted frame in wireshark. Thats mean, if I see an &quot;user&quot; in frame 99, when I want to encrypt it, the &quot;user&quot; will be encrypted. So, how to encrypted the frame in wireshark?.'''
date = "2016-08-02T03:58:00Z"
lastmod = "2016-08-02T04:24:00Z"
weight = 54500
keywords = [ "frame" ]
aliases = [ "/questions/54500" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Encrypt Frame](/questions/54500/encrypt-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54500-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54500-score" class="post-score" title="current number of votes">0</div><span id="post-54500-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hy. I have a task from my lecturer. He told to me how to encrypted frame in wireshark. Thats mean, if I see an "user" in frame 99, when I want to encrypt it, the "user" will be encrypted.</p><p>So, how to encrypted the frame in wireshark?.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Aug '16, 03:58</strong></p><img src="https://secure.gravatar.com/avatar/979ab9ec46304595c60f0bbd1b82f985?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="batutahibnu17&#39;s gravatar image" /><p><span>batutahibnu17</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="batutahibnu17 has no accepted answers">0%</span></p></div></div><div id="comments-container-54500" class="comments-container"></div><div id="comment-tools-54500" class="comment-tools"></div><div class="clear"></div><div id="comment-54500-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54501"></span>

<div id="answer-container-54501" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54501-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54501-score" class="post-score" title="current number of votes">0</div><span id="post-54501-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark can't encrypt frames, it's a display tool...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Aug '16, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-54501" class="comments-container"></div><div id="comment-tools-54501" class="comment-tools"></div><div class="clear"></div><div id="comment-54501-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

