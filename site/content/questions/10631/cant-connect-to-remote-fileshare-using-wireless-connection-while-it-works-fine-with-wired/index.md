+++
type = "question"
title = "Cant Connect to Remote FileShare using Wireless Connection while it works fine with Wired"
description = '''Dear all, I am working in a sales office, where wired and wireless connection works. Strangely I have encountered the issue with the wireless connection only where I cant connect to multiple remote file share systems. Have anyone experienced that kind of behavior in their environment?  Thanks,  Faiz'''
date = "2012-05-03T04:53:00Z"
lastmod = "2012-05-03T13:32:00Z"
weight = 10631
keywords = [ "cifs" ]
aliases = [ "/questions/10631" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cant Connect to Remote FileShare using Wireless Connection while it works fine with Wired](/questions/10631/cant-connect-to-remote-fileshare-using-wireless-connection-while-it-works-fine-with-wired)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10631-score" class="post-score" title="current number of votes">0</div><span id="post-10631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all,</p><p>I am working in a sales office, where wired and wireless connection works. Strangely I have encountered the issue with the wireless connection only where I cant connect to multiple remote file share systems. Have anyone experienced that kind of behavior in their environment?</p><p>Thanks, Faiz</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cifs" rel="tag" title="see questions tagged &#39;cifs&#39;">cifs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '12, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/cf463b93568b540423bc643bcc599e0a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Farhan_J&#39;s gravatar image" /><p><span>Farhan_J</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Farhan_J has no accepted answers">0%</span></p></div></div><div id="comments-container-10631" class="comments-container"><span id="10632"></span><div id="comment-10632" class="comment"><div id="post-10632-score" class="comment-score"></div><div class="comment-text"><p>Are your file share subprotocols enabled on your wireless card? Some people disable e.g. Windows File Sharing to avoid having the ports open in case you are in a hostile wireless network</p></div><div id="comment-10632-info" class="comment-info"><span class="comment-age">(03 May '12, 04:58)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="10635"></span><div id="comment-10635" class="comment"><div id="post-10635-score" class="comment-score"></div><div class="comment-text"><p>1.) Is your wireless access point connected to a firewall? If so, the firewall might block access to file shares.</p><p>2.) Are you connected to the wireless net and to the LAN in prallel? There can be several issues with that (DNS, Routing, etc.)</p></div><div id="comment-10635-info" class="comment-info"><span class="comment-age">(03 May '12, 06:51)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="10639"></span><div id="comment-10639" class="comment"><div id="post-10639-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your response guys, No it doesnt have any firewall in front and neither LAN plugged during that time.<br />
Landi: how can we check whether file sharing is blocked in the wireless card?</p></div><div id="comment-10639-info" class="comment-info"><span class="comment-age">(03 May '12, 07:57)</span> <span class="comment-user userinfo">Farhan_J</span></div></div><span id="10647"></span><div id="comment-10647" class="comment"><div id="post-10647-score" class="comment-score"></div><div class="comment-text"><p>If windows: Check the properties of your wireless card, there should be TCP/IP, File and Printer Sharing plus several other protocols with checkmarks to enable/disable</p></div><div id="comment-10647-info" class="comment-info"><span class="comment-age">(03 May '12, 13:32)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-10631" class="comment-tools"></div><div class="clear"></div><div id="comment-10631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

