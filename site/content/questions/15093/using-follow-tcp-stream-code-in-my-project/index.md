+++
type = "question"
title = "using &quot;follow tcp stream&quot; code in my project"
description = '''I want to assemble all tcp sessions from pcap file and save payload to file. I know about 2 decisions: 1)libnids 2) tcpflow, but all of them can be used only for linux. Could you advise me any such decision for windows.'''
date = "2012-10-18T14:24:00Z"
lastmod = "2012-12-07T07:55:00Z"
weight = 15093
keywords = [ "assemble", "all", "tcp", "sessions" ]
aliases = [ "/questions/15093" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [using "follow tcp stream" code in my project](/questions/15093/using-follow-tcp-stream-code-in-my-project)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15093-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15093-score" class="post-score" title="current number of votes">0</div><span id="post-15093-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to assemble all tcp sessions from pcap file and save payload to file. I know about 2 decisions: 1)libnids 2) tcpflow, but all of them can be used only for linux. Could you advise me any such decision for windows.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-assemble" rel="tag" title="see questions tagged &#39;assemble&#39;">assemble</span> <span class="post-tag tag-link-all" rel="tag" title="see questions tagged &#39;all&#39;">all</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-sessions" rel="tag" title="see questions tagged &#39;sessions&#39;">sessions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '12, 14:24</strong></p><img src="https://secure.gravatar.com/avatar/c227eddf476777e0417b105709456368?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vensan%20vega&#39;s gravatar image" /><p><span>vensan vega</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vensan vega has no accepted answers">0%</span></p></div></div><div id="comments-container-15093" class="comments-container"></div><div id="comment-tools-15093" class="comment-tools"></div><div class="clear"></div><div id="comment-15093-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16685"></span>

<div id="answer-container-16685" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16685-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16685-score" class="post-score" title="current number of votes">0</div><span id="post-16685-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you are using Windows, Splitcap might be the one for you. You can download it from this <a href="http://www.netresec.com/?page=SplitCap" title="link">link</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '12, 07:55</strong></p><img src="https://secure.gravatar.com/avatar/2c33bce451fd8dc3844b351b798cbee1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fates&#39;s gravatar image" /><p><span>fates</span><br />
<span class="score" title="35 reputation points">35</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fates has no accepted answers">0%</span></p></div></div><div id="comments-container-16685" class="comments-container"></div><div id="comment-tools-16685" class="comment-tools"></div><div class="clear"></div><div id="comment-16685-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

