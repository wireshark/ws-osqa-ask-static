+++
type = "question"
title = "win, ws, mss and sack_permit"
description = '''Hi guys, plz help me i&#x27;m new user to and i really don&#x27;t know what they do?(Win WS, SACK_perm and Mss). I have a wireshark capture, and i&#x27;ve to intrepret it i don&#x27;t know where i&#x27;m supposed to begin Help me please.'''
date = "2015-04-07T06:54:00Z"
lastmod = "2015-04-07T10:28:00Z"
weight = 41250
keywords = [ "windows", "mss", "sack" ]
aliases = [ "/questions/41250" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [win, ws, mss and sack\_permit](/questions/41250/win-ws-mss-and-sack_permit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41250-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41250-score" class="post-score" title="current number of votes">0</div><span id="post-41250-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys,</p><p>plz help me i'm new user to and i really don't know what they do?(Win WS, SACK_perm and Mss). I have a wireshark capture, and i've to intrepret it i don't know where i'm supposed to begin</p><p>Help me please.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-mss" rel="tag" title="see questions tagged &#39;mss&#39;">mss</span> <span class="post-tag tag-link-sack" rel="tag" title="see questions tagged &#39;sack&#39;">sack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '15, 06:54</strong></p><img src="https://secure.gravatar.com/avatar/cc85c63aa1e7a8c056b078f1ba79cac7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lala&#39;s gravatar image" /><p><span>Lala</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lala has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>07 Apr '15, 06:57</strong> </span></p></div></div><div id="comments-container-41250" class="comments-container"></div><div id="comment-tools-41250" class="comment-tools"></div><div class="clear"></div><div id="comment-41250-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41259"></span>

<div id="answer-container-41259" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41259-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41259-score" class="post-score" title="current number of votes">0</div><span id="post-41259-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer to the same question from another user.</p><blockquote><p><a href="https://ask.wireshark.org/questions/41095/column-info">https://ask.wireshark.org/questions/41095/column-info</a></p></blockquote><p>Are you both trying to solve the same homework?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '15, 10:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-41259" class="comments-container"></div><div id="comment-tools-41259" class="comment-tools"></div><div class="clear"></div><div id="comment-41259-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

