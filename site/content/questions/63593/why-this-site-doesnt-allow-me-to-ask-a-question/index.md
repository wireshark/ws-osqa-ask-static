+++
type = "question"
title = "Why this site doesn&#x27;t allow me to ask a question?"
description = '''Not the right place for this but since every time I try and ask a question of any sort I get the message &quot;We&#x27;re sorry, but Akismet believes your question is spam. If you believe this is an error, please contact the forum administrator.&quot;, kind of desperate. Help me please.'''
date = "2017-09-13T10:47:00Z"
lastmod = "2017-10-01T10:43:00Z"
weight = 63593
keywords = [ "ask.wireshark.org", "spam" ]
aliases = [ "/questions/63593" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why this site doesn't allow me to ask a question?](/questions/63593/why-this-site-doesnt-allow-me-to-ask-a-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63593-score" class="post-score" title="current number of votes">0</div><span id="post-63593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Not the right place for this but since every time I try and ask a question of any sort I get the message "We're sorry, but Akismet believes your question is spam. If you believe this is an error, please contact the forum administrator.", kind of desperate. Help me please.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span> <span class="post-tag tag-link-spam" rel="tag" title="see questions tagged &#39;spam&#39;">spam</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '17, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/0150f4f60b5b3cd78cd002b746900f00?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="papahoth&#39;s gravatar image" /><p><span>papahoth</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="papahoth has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted <strong>01 Oct '17, 10:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-63593" class="comments-container"><span id="63682"></span><div id="comment-63682" class="comment"><div id="post-63682-score" class="comment-score"></div><div class="comment-text"><p><a href="https://ask.wireshark.org/users/42371/papahoth">@papahoth</a>, please edit this Question with what you actually wanted to ask. It is hard to guess what is wrong without seeing the question.</p></div><div id="comment-63682-info" class="comment-info"><span class="comment-age">(01 Oct '17, 10:43)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-63593" class="comment-tools"></div><div class="clear"></div><div id="comment-63593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

