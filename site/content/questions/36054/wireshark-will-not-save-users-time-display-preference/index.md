+++
type = "question"
title = "Wireshark will not save user&#x27;s time display preference"
description = '''Until recently, Wireshark had been working fine in my organization, but now, it no longer remembers user&#x27;s preference for time display format. The user sets it to &quot;UTC Date and Time of Day&quot;. Once the user closes Wireshark and reopens it, it defaults back to &quot;Seconds since beginning of capture.&quot; Does...'''
date = "2014-09-07T04:41:00Z"
lastmod = "2014-09-07T04:41:00Z"
weight = 36054
keywords = [ "registry", "format", "display", "configuration", "time" ]
aliases = [ "/questions/36054" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark will not save user's time display preference](/questions/36054/wireshark-will-not-save-users-time-display-preference)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36054-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36054-score" class="post-score" title="current number of votes">0</div><span id="post-36054-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Until recently, Wireshark had been working fine in my organization, but now, it no longer remembers user's preference for time display format. The user sets it to "UTC Date and Time of Day". Once the user closes Wireshark and reopens it, it defaults back to "Seconds since beginning of capture."</p><p>Does anyone have any insight on this? How does Wireshark update its preferences? Are these settings kept in a file or in the registry? Anyone else ever have this problem?</p><p>Any help would be greatly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-registry" rel="tag" title="see questions tagged &#39;registry&#39;">registry</span> <span class="post-tag tag-link-format" rel="tag" title="see questions tagged &#39;format&#39;">format</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '14, 04:41</strong></p><img src="https://secure.gravatar.com/avatar/1d2471794c3d043260bbb8d84ea2f8c9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RavenDT&#39;s gravatar image" /><p><span>RavenDT</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RavenDT has no accepted answers">0%</span></p></div></div><div id="comments-container-36054" class="comments-container"></div><div id="comment-tools-36054" class="comment-tools"></div><div class="clear"></div><div id="comment-36054-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

