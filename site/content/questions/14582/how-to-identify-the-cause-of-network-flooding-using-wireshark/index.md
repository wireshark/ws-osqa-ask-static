+++
type = "question"
title = "How to identify the cause of network flooding using wireshark?"
description = '''Hi wireshark experts,  I am a new wireshark user. I find intermittent link down from 3:00am ~ 3:02am sometimes. I use Windows perfmon and I found the network flooding. Could you tell me how to identify the root cause using wireshark? As you can see bytes total/sec on four 10GbE(screenshot 1) below, ...'''
date = "2012-09-27T20:25:00Z"
lastmod = "2012-09-27T20:25:00Z"
weight = 14582
keywords = [ "nexus", "flooding", "ncu" ]
aliases = [ "/questions/14582" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to identify the cause of network flooding using wireshark?](/questions/14582/how-to-identify-the-cause-of-network-flooding-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14582-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14582-score" class="post-score" title="current number of votes">0</div><span id="post-14582-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi wireshark experts, I am a new wireshark user. I find intermittent link down from 3:00am ~ 3:02am sometimes. I use Windows perfmon and I found the network flooding. Could you tell me how to identify the root cause using wireshark?</p><p>As you can see bytes total/sec on four 10GbE(screenshot 1) below, it went from 10MB/s to 140MB/s on four links at 3:00am and caused no buffers errors.(screenshot 2)</p><p>I check the output.zip and I know the packets/sec is increased at 3:02am but there is no suspicious IP address, please help. Check out the file in the wireshark directory. <a href="ftp://ftp01.quantatw.com/">ftp://ftp01.quantatw.com/</a> user: sapftp password: wju123</p><p><img src="https://osqa-ask.wireshark.org/upfiles/ws1.JPG" alt="alt text" /> <img src="https://osqa-ask.wireshark.org/upfiles/ws2.JPG" alt="alt text" /></p><p>Thanks for your help. Any ideas will be really appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nexus" rel="tag" title="see questions tagged &#39;nexus&#39;">nexus</span> <span class="post-tag tag-link-flooding" rel="tag" title="see questions tagged &#39;flooding&#39;">flooding</span> <span class="post-tag tag-link-ncu" rel="tag" title="see questions tagged &#39;ncu&#39;">ncu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Sep '12, 20:25</strong></p><img src="https://secure.gravatar.com/avatar/21ba634c0d45054729a56a54332fa7fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dennis&#39;s gravatar image" /><p><span>Dennis</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dennis has no accepted answers">0%</span></p></img></div></div><div id="comments-container-14582" class="comments-container"></div><div id="comment-tools-14582" class="comment-tools"></div><div class="clear"></div><div id="comment-14582-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

