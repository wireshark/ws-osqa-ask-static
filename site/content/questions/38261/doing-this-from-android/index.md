+++
type = "question"
title = "Doing this from android?"
description = '''Hey all, I&#x27;m new to all this. I&#x27;m decoding android network data for use in an app. Is there a way to decode this information on the android itself? I dont always have access to the PC, but my tablet is with me at all times. Thanks!'''
date = "2014-12-01T14:24:00Z"
lastmod = "2014-12-01T14:24:00Z"
weight = 38261
keywords = [ "android" ]
aliases = [ "/questions/38261" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Doing this from android?](/questions/38261/doing-this-from-android)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38261-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38261-score" class="post-score" title="current number of votes">0</div><span id="post-38261-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey all,</p><p>I'm new to all this. I'm decoding android network data for use in an app.</p><p>Is there a way to decode this information on the android itself? I dont always have access to the PC, but my tablet is with me at all times.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '14, 14:24</strong></p><img src="https://secure.gravatar.com/avatar/7c712541ba8f92a82064ed60cd09bf6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nifty62&#39;s gravatar image" /><p><span>Nifty62</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nifty62 has no accepted answers">0%</span></p></div></div><div id="comments-container-38261" class="comments-container"></div><div id="comment-tools-38261" class="comment-tools"></div><div class="clear"></div><div id="comment-38261-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

