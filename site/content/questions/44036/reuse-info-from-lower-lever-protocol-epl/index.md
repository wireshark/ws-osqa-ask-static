+++
type = "question"
title = "reuse info from lower lever protocol epl"
description = '''Hello, i&#x27;m having problems for creating a dissector under powerlink (epl) protocol. My protocol will dissect the unprocessed datas. The problem is that i need to get the source displayed in the epl protocol. Is there a solution to get this value ? It seems I can&#x27;t analyse previous bits which are dis...'''
date = "2015-07-10T01:09:00Z"
lastmod = "2015-07-10T01:09:00Z"
weight = 44036
keywords = [ "data", "protocol", "powerlink" ]
aliases = [ "/questions/44036" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [reuse info from lower lever protocol epl](/questions/44036/reuse-info-from-lower-lever-protocol-epl)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44036-score" class="post-score" title="current number of votes">0</div><span id="post-44036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>i'm having problems for creating a dissector under powerlink (epl) protocol. My protocol will dissect the unprocessed datas. The problem is that i need to get the source displayed in the epl protocol. Is there a solution to get this value ? It seems I can't analyse previous bits which are dissected by the epl protocol. I absolutely need this information. Thanks in advance for answers.</p><p>Sylvain</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-powerlink" rel="tag" title="see questions tagged &#39;powerlink&#39;">powerlink</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '15, 01:09</strong></p><img src="https://secure.gravatar.com/avatar/fe134700e9c8fcec7b7bd553aac46396?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sylvain&#39;s gravatar image" /><p><span>Sylvain</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sylvain has one accepted answer">100%</span></p></div></div><div id="comments-container-44036" class="comments-container"></div><div id="comment-tools-44036" class="comment-tools"></div><div class="clear"></div><div id="comment-44036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

