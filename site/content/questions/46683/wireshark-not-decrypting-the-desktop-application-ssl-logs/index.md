+++
type = "question"
title = "Wireshark not decrypting the Desktop application SSL logs."
description = '''Wireshark not decrypting the Desktop application SSL logs. We have already declared the required .pem files in Edit- &amp;gt; preferences -&amp;gt; protocols -&amp;gt; SSL -&amp;gt; RSA Key list.'''
date = "2015-10-18T23:44:00Z"
lastmod = "2015-10-19T02:41:00Z"
weight = 46683
keywords = [ "issue" ]
aliases = [ "/questions/46683" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not decrypting the Desktop application SSL logs.](/questions/46683/wireshark-not-decrypting-the-desktop-application-ssl-logs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46683-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46683-score" class="post-score" title="current number of votes">0</div><span id="post-46683-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark not decrypting the Desktop application SSL logs. We have already declared the required .pem files in Edit- &gt; preferences -&gt; protocols -&gt; SSL -&gt; RSA Key list.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-issue" rel="tag" title="see questions tagged &#39;issue&#39;">issue</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '15, 23:44</strong></p><img src="https://secure.gravatar.com/avatar/22ee6de42a7f29ed6f389d782d4e9ba2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rakesh_951&#39;s gravatar image" /><p><span>Rakesh_951</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rakesh_951 has no accepted answers">0%</span></p></div></div><div id="comments-container-46683" class="comments-container"><span id="46690"></span><div id="comment-46690" class="comment"><div id="post-46690-score" class="comment-score"></div><div class="comment-text"><p>please add the ssl debug log file, <strong>otherwise</strong> it's impossible to help you.</p><p>To create the file, please set the debug file name and reload the capture file.</p><blockquote><p>Edit -&gt; Preferences -&gt; Protocols -&gt; SSL -&gt; SSL debug file</p></blockquote></div><div id="comment-46690-info" class="comment-info"><span class="comment-age">(19 Oct '15, 02:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-46683" class="comment-tools"></div><div class="clear"></div><div id="comment-46683-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

