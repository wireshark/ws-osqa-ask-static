+++
type = "question"
title = "botnet collecting how? using wireshark or...?"
description = '''I&#x27;m researching on Botnet detection in network using SVM and working with Wireshark &amp;amp; new to this, Can someone suggest me whether i can collect botnet data (malware data) ? If so, please let me know the way of doing it !'''
date = "2014-11-21T05:10:00Z"
lastmod = "2014-11-21T05:10:00Z"
weight = 38041
keywords = [ "malware", "botnet", "wireshark" ]
aliases = [ "/questions/38041" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [botnet collecting how? using wireshark or...?](/questions/38041/botnet-collecting-how-using-wireshark-or)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38041-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38041-score" class="post-score" title="current number of votes">0</div><span id="post-38041-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I'm researching on Botnet detection in network using SVM and working with Wireshark &amp; new to this, Can someone suggest me whether i can collect botnet data (malware data) ? If so, please let me know the way of doing it !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malware" rel="tag" title="see questions tagged &#39;malware&#39;">malware</span> <span class="post-tag tag-link-botnet" rel="tag" title="see questions tagged &#39;botnet&#39;">botnet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '14, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/9c50ff68e2cdcd080e9f98ea12102092?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haniyeh_151&#39;s gravatar image" /><p><span>haniyeh_151</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haniyeh_151 has no accepted answers">0%</span></p></div></div><div id="comments-container-38041" class="comments-container"></div><div id="comment-tools-38041" class="comment-tools"></div><div class="clear"></div><div id="comment-38041-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

