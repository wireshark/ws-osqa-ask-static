+++
type = "question"
title = "SNDCP reassemble failure"
description = '''Hi, When I did trace of Gb over IP interface for a user who downloaded frob a website, The packet sent from the website cannot be decoded by wireshark Only the ACK message can be read by wireshark Is there any ways to decode/reassemble these SNDCP fragment ? BR//Joko'''
date = "2012-09-10T21:18:00Z"
lastmod = "2012-09-10T21:18:00Z"
weight = 14182
keywords = [ "sndcp" ]
aliases = [ "/questions/14182" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SNDCP reassemble failure](/questions/14182/sndcp-reassemble-failure)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14182-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14182-score" class="post-score" title="current number of votes">0</div><span id="post-14182-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When I did trace of Gb over IP interface for a user who downloaded frob a website, The packet sent from the website cannot be decoded by wireshark Only the ACK message can be read by wireshark</p><p>Is there any ways to decode/reassemble these SNDCP fragment ?</p><p>BR//Joko</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sndcp" rel="tag" title="see questions tagged &#39;sndcp&#39;">sndcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '12, 21:18</strong></p><img src="https://secure.gravatar.com/avatar/d5ef0636b76ed32eac109b1aa12eb678?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jokow&#39;s gravatar image" /><p><span>jokow</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jokow has no accepted answers">0%</span></p></div></div><div id="comments-container-14182" class="comments-container"></div><div id="comment-tools-14182" class="comment-tools"></div><div class="clear"></div><div id="comment-14182-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

