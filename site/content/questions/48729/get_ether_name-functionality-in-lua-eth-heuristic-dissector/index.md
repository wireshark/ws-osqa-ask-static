+++
type = "question"
title = "get_ether_name functionality in Lua eth heuristic dissector"
description = '''Is there a way to generate a hidden protofield for my eth heuristic Lua dissector for the resolved hardware address name? Something like the get_ether_name function. Thanks and best regards.'''
date = "2015-12-28T01:57:00Z"
lastmod = "2015-12-28T01:57:00Z"
weight = 48729
keywords = [ "name-resolving", "eth", "dissector" ]
aliases = [ "/questions/48729" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [get\_ether\_name functionality in Lua eth heuristic dissector](/questions/48729/get_ether_name-functionality-in-lua-eth-heuristic-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48729-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48729-score" class="post-score" title="current number of votes">0</div><span id="post-48729-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to generate a hidden protofield for my eth heuristic Lua dissector for the resolved hardware address name?</p><p>Something like the get_ether_name function.</p><p>Thanks and best regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-name-resolving" rel="tag" title="see questions tagged &#39;name-resolving&#39;">name-resolving</span> <span class="post-tag tag-link-eth" rel="tag" title="see questions tagged &#39;eth&#39;">eth</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '15, 01:57</strong></p><img src="https://secure.gravatar.com/avatar/7e50c86ac4ee2038257acc83ccb1ce21?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="juandering&#39;s gravatar image" /><p><span>juandering</span><br />
<span class="score" title="11 reputation points">11</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="juandering has one accepted answer">100%</span></p></div></div><div id="comments-container-48729" class="comments-container"></div><div id="comment-tools-48729" class="comment-tools"></div><div class="clear"></div><div id="comment-48729-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

