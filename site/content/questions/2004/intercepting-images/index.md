+++
type = "question"
title = "Intercepting images"
description = '''A while ago, when I was running ubuntu, I used a program called ethereal (I think); which I now understand is wireshark. I am now on mac os x, and wanted to try the program again, but one feature that I distinctly remember, is missing. I was able to intercept any images that were being browsed on my...'''
date = "2011-01-28T22:47:00Z"
lastmod = "2013-09-18T20:40:00Z"
weight = 2004
keywords = [ "images", "intercept" ]
aliases = [ "/questions/2004" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Intercepting images](/questions/2004/intercepting-images)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2004-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2004-score" class="post-score" title="current number of votes">1</div><span id="post-2004-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A while ago, when I was running ubuntu, I used a program called ethereal (I think); which I now understand is wireshark. I am now on mac os x, and wanted to try the program again, but one feature that I distinctly remember, is missing. I was able to intercept any images that were being browsed on my own network.</p><p>I let my kids use the internet, and having a stream of images shows me that the sites they are using are safe.</p><p>I could be way wrong with the name of the program I used. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-images" rel="tag" title="see questions tagged &#39;images&#39;">images</span> <span class="post-tag tag-link-intercept" rel="tag" title="see questions tagged &#39;intercept&#39;">intercept</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jan '11, 22:47</strong></p><img src="https://secure.gravatar.com/avatar/a8ae8a338f7978530eb6e5e0f1f2292f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="omicronpie&#39;s gravatar image" /><p><span>omicronpie</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="omicronpie has no accepted answers">0%</span></p></div></div><div id="comments-container-2004" class="comments-container"></div><div id="comment-tools-2004" class="comment-tools"></div><div class="clear"></div><div id="comment-2004-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2005"></span>

<div id="answer-container-2005" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2005-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2005-score" class="post-score" title="current number of votes">1</div><span id="post-2005-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As far as I know, Ethereal never had that functionality and neither does Wireshark. You must have been using something else. A little googling will find you other programs that do that. I just found "York" at <a href="http://thesz.diecru.eu/content/york.php">http://thesz.diecru.eu/content/york.php</a>:</p><p>Description:</p><ul><li>Log source, destination [fqdn or ip address] and packet size of all network traffic on your network, of course also outbound traffic. The network card will be set into promiscuous mode.</li><li><strong>Save sniffed HTTP and FTP files. Just for fun, pictures are shown in a slideshow and in a screensaver like window.</strong></li><li>Sniff for HTTP, FTP, POP3, SMTP, SMB, VNC and AIM password/hash and HTTP cookies like 'GX'.</li><li>Select a client and follow his clicks in your browser. [WebSession]</li><li><strong>Screensaver included. Shows sniffed pictures in a slideshow.</strong></li><li>For advanced user: You can capture traffic into a pcap file, send a pcap file and replay a pcap file.</li><li>For advanced user: You can restrict captured traffic by tcpdump filters.</li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jan '11, 00:00</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jan '11, 08:28</strong> </span></p></div></div><div id="comments-container-2005" class="comments-container"></div><div id="comment-tools-2005" class="comment-tools"></div><div class="clear"></div><div id="comment-2005-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="24936"></span>

<div id="answer-container-24936" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24936-score" class="post-score" title="current number of votes">0</div><span id="post-24936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What you are looking for is called EtherPEG, but it's a PowerPC app and will not run on the latest Intel Macs running Mac OS X Lion or greater. You could look into Driftnet on Linux.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Sep '13, 20:40</strong></p><img src="https://secure.gravatar.com/avatar/7052164bc49eb2ff9b38c13d0fa1429f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="taylorlightfoot&#39;s gravatar image" /><p><span>taylorlightfoot</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="taylorlightfoot has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Sep '13, 20:41</strong> </span></p></div></div><div id="comments-container-24936" class="comments-container"></div><div id="comment-tools-24936" class="comment-tools"></div><div class="clear"></div><div id="comment-24936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

