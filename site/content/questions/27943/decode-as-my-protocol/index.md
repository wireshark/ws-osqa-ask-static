+++
type = "question"
title = "Decode as my protocol"
description = '''Hi, Is there any way to say Wireshark to decode as &#x27;my protocol&#x27; all the payloads of 802.15.4? Thanks'''
date = "2013-12-08T23:38:00Z"
lastmod = "2013-12-08T23:38:00Z"
weight = 27943
keywords = [ "decode", "802.15.4", "as", "wireshark", "plugin" ]
aliases = [ "/questions/27943" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decode as my protocol](/questions/27943/decode-as-my-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27943-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27943-score" class="post-score" title="current number of votes">0</div><span id="post-27943-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is there any way to say Wireshark to decode as 'my protocol' all the payloads of 802.15.4?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-802.15.4" rel="tag" title="see questions tagged &#39;802.15.4&#39;">802.15.4</span> <span class="post-tag tag-link-as" rel="tag" title="see questions tagged &#39;as&#39;">as</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '13, 23:38</strong></p><img src="https://secure.gravatar.com/avatar/347a473627a1c8b558663368d033097c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lina&#39;s gravatar image" /><p><span>lina</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lina has no accepted answers">0%</span></p></div></div><div id="comments-container-27943" class="comments-container"></div><div id="comment-tools-27943" class="comment-tools"></div><div class="clear"></div><div id="comment-27943-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

