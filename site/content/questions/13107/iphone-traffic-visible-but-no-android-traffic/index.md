+++
type = "question"
title = "iPhone traffic visible but no Android traffic"
description = '''Hello! I let Wireshark run on my Windows 7 computer. My computer is connected to the router and two mobile devices are also connected to the router by WLAN. I have no problems to see the network traffic from the iPhone but nothing is visible from the Android. Why? Best regards Marc'''
date = "2012-07-29T19:02:00Z"
lastmod = "2012-07-30T06:46:00Z"
weight = 13107
keywords = [ "android", "iphone" ]
aliases = [ "/questions/13107" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [iPhone traffic visible but no Android traffic](/questions/13107/iphone-traffic-visible-but-no-android-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13107-score" class="post-score" title="current number of votes">0</div><span id="post-13107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello!</p><p>I let Wireshark run on my Windows 7 computer. My computer is connected to the router and two mobile devices are also connected to the router by WLAN. I have no problems to see the network traffic from the iPhone but nothing is visible from the Android. Why?</p><p>Best regards Marc</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '12, 19:02</strong></p><img src="https://secure.gravatar.com/avatar/0e3ad7ff3dd804970604f9723d346dcb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marcophono&#39;s gravatar image" /><p><span>Marcophono</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marcophono has no accepted answers">0%</span></p></div></div><div id="comments-container-13107" class="comments-container"></div><div id="comment-tools-13107" class="comment-tools"></div><div class="clear"></div><div id="comment-13107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13115"></span>

<div id="answer-container-13115" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13115-score" class="post-score" title="current number of votes">0</div><span id="post-13115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you studied your <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">capture setup</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '12, 06:46</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13115" class="comments-container"></div><div id="comment-tools-13115" class="comment-tools"></div><div class="clear"></div><div id="comment-13115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

