+++
type = "question"
title = "help summarising server network traffic"
description = '''Hi, So I am trying to troubleshoot an issue with a Windows 2008 domain controller where if appears to be working perfects and responding to ICMP with &amp;lt;1ms responses but then all of a sudden we will observe latency in the pings sometimes up to 22000ms and even time out all together. The pings are ...'''
date = "2014-05-13T04:20:00Z"
lastmod = "2014-05-13T04:20:00Z"
weight = 32753
keywords = [ "server", "traffic", "network", "summarising", "help" ]
aliases = [ "/questions/32753" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [help summarising server network traffic](/questions/32753/help-summarising-server-network-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32753-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32753-score" class="post-score" title="current number of votes">0</div><span id="post-32753-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/1_1.PNG" alt="alt text" />Hi, So I am trying to troubleshoot an issue with a Windows 2008 domain controller where if appears to be working perfects and responding to ICMP with &lt;1ms responses but then all of a sudden we will observe latency in the pings sometimes up to 22000ms and even time out all together. The pings are to/from from a server in the same subnet which is responding fine.</p><p>So I have collected a trace during a 1 minute window where pings were fluctuating up to 2000ms on this occasion. I can see the IMCP requests and delayed responses but I am unsure how to interpret the other data to see what may be causing the .</p><p>Wireshark Expert Info gives me the following summary but I don't know if this indicates prolems or normal behaviour</p><p>Just to add that the duplicate IP entry can be ignored as it is a backup appliance with a pair of nics and is a known bug with the system. The IP does not clash with any other servers...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-summarising" rel="tag" title="see questions tagged &#39;summarising&#39;">summarising</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '14, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/fd7c2bfb9bf8dd466ceea1117dbbda12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andymoss&#39;s gravatar image" /><p><span>andymoss</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andymoss has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 May '14, 04:25</strong> </span></p></div></div><div id="comments-container-32753" class="comments-container"></div><div id="comment-tools-32753" class="comment-tools"></div><div class="clear"></div><div id="comment-32753-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

