+++
type = "question"
title = "Wireshark can track activity of a localhost php application (email, url .. activity)"
description = '''I have in localhost on windows xp an php&#x27;s application that run with easyphp (apache, php, mysql) Is possible with Wireshark to see all connections that php application does ? 1) the connection or datas that are send/required and at which url; 2) the email sended at which address and generally all t...'''
date = "2012-09-14T13:05:00Z"
lastmod = "2012-09-14T13:05:00Z"
weight = 14273
keywords = [ "url", "windows", "traffic", "email", "trace" ]
aliases = [ "/questions/14273" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark can track activity of a localhost php application (email, url .. activity)](/questions/14273/wireshark-can-track-activity-of-a-localhost-php-application-email-url-activity)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14273-score" class="post-score" title="current number of votes">0</div><span id="post-14273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have in localhost on windows xp an php's application that run with easyphp (apache, php, mysql) Is possible with Wireshark to see all connections that php application does ? 1) the connection or datas that are send/required and at which url; 2) the email sended at which address and generally all the activity with the external</p><p>If yes, which are the steps?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span> <span class="post-tag tag-link-trace" rel="tag" title="see questions tagged &#39;trace&#39;">trace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '12, 13:05</strong></p><img src="https://secure.gravatar.com/avatar/a31c9569c633df80e1aa0282060798a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="albs&#39;s gravatar image" /><p><span>albs</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="albs has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Sep '12, 13:07</strong> </span></p></div></div><div id="comments-container-14273" class="comments-container"></div><div id="comment-tools-14273" class="comment-tools"></div><div class="clear"></div><div id="comment-14273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

