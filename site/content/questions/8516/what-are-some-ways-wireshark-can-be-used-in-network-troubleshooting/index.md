+++
type = "question"
title = "What are some ways Wireshark can be used in Network Troubleshooting?"
description = '''I am a noob at networking and i am currently writing a paper for my basic networking class, i was told by my teacher as a hint that Wire shark can be used for Network Troubleshooting and so i was curious how it is used in this manner? I was hoping to use this a source for my Paper since i need betwe...'''
date = "2012-01-20T12:58:00Z"
lastmod = "2012-01-20T13:04:00Z"
weight = 8516
keywords = [ "windows", "source", "troubleshooting", "noob", "teacher" ]
aliases = [ "/questions/8516" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What are some ways Wireshark can be used in Network Troubleshooting?](/questions/8516/what-are-some-ways-wireshark-can-be-used-in-network-troubleshooting)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8516-score" class="post-score" title="current number of votes">0</div><span id="post-8516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a noob at networking and i am currently writing a paper for my basic networking class, i was told by my teacher as a hint that Wire shark can be used for Network Troubleshooting and so i was curious how it is used in this manner?</p><p>I was hoping to use this a source for my Paper since i need between 3-5 Pages and 3-4 Sources.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-source" rel="tag" title="see questions tagged &#39;source&#39;">source</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span> <span class="post-tag tag-link-noob" rel="tag" title="see questions tagged &#39;noob&#39;">noob</span> <span class="post-tag tag-link-teacher" rel="tag" title="see questions tagged &#39;teacher&#39;">teacher</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '12, 12:58</strong></p><img src="https://secure.gravatar.com/avatar/7a72657cacae4ed19ecfb0a29434d6e2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jacob%20Heckman&#39;s gravatar image" /><p><span>Jacob Heckman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jacob Heckman has no accepted answers">0%</span></p></div></div><div id="comments-container-8516" class="comments-container"></div><div id="comment-tools-8516" class="comment-tools"></div><div class="clear"></div><div id="comment-8516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8517"></span>

<div id="answer-container-8517" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8517-score" class="post-score" title="current number of votes">0</div><span id="post-8517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I've got quite a bit of info at my site www.thetechfirm.com Take what you need.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '12, 13:04</strong></p><img src="https://secure.gravatar.com/avatar/dbc4d8cb6be85bd586ca4bf211e1337c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thetechfirm&#39;s gravatar image" /><p><span>thetechfirm</span><br />
<span class="score" title="64 reputation points">64</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thetechfirm has no accepted answers">0%</span></p></div></div><div id="comments-container-8517" class="comments-container"></div><div id="comment-tools-8517" class="comment-tools"></div><div class="clear"></div><div id="comment-8517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

