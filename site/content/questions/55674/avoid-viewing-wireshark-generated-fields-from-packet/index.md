+++
type = "question"
title = "avoid viewing wireshark generated fields from packet"
description = '''Hi, Is there a way to avoid the wireshark generated fields from display?'''
date = "2016-09-19T23:37:00Z"
lastmod = "2016-09-20T02:55:00Z"
weight = 55674
keywords = [ "fields", "generated", "wireshark" ]
aliases = [ "/questions/55674" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [avoid viewing wireshark generated fields from packet](/questions/55674/avoid-viewing-wireshark-generated-fields-from-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55674-score" class="post-score" title="current number of votes">0</div><span id="post-55674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Is there a way to avoid the wireshark generated fields from display?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fields" rel="tag" title="see questions tagged &#39;fields&#39;">fields</span> <span class="post-tag tag-link-generated" rel="tag" title="see questions tagged &#39;generated&#39;">generated</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '16, 23:37</strong></p><img src="https://secure.gravatar.com/avatar/9cc6fc60cce0f8e86dc7d39e13cdd742?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sukeerthi&#39;s gravatar image" /><p><span>sukeerthi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sukeerthi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> wikified <strong>19 Sep '16, 23:52</strong> </span></p></div></div><div id="comments-container-55674" class="comments-container"><span id="55675"></span><div id="comment-55675" class="comment"><div id="post-55675-score" class="comment-score"></div><div class="comment-text"><p>Do you mean the fields in the packet details pane that have "[]" around them?</p></div><div id="comment-55675-info" class="comment-info"><span class="comment-age">(20 Sep '16, 01:44)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="55676"></span><div id="comment-55676" class="comment"><div id="post-55676-score" class="comment-score"></div><div class="comment-text"><p>Yes thats correct</p></div><div id="comment-55676-info" class="comment-info"><span class="comment-age">(20 Sep '16, 02:08)</span> <span class="comment-user userinfo">sukeerthi</span></div></div></div><div id="comment-tools-55674" class="comment-tools"></div><div class="clear"></div><div id="comment-55674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55677"></span>

<div id="answer-container-55677" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55677-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55677-score" class="post-score" title="current number of votes">0</div><span id="post-55677-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not that I know of, that's part of the basic functionality of Wireshark.</p><p>Out of interest, why do you want to remove those fields from display?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Sep '16, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-55677" class="comments-container"><span id="55678"></span><div id="comment-55678" class="comment"><div id="post-55678-score" class="comment-score"></div><div class="comment-text"><p>I am verifying a scenario wherein when ALG is disabled,the info in FTP response packets from server to EPSV request from client is not NATTED by the intermediate device(say a Netscaler). But I find the NATTED IP details in packet but with [] braces.So wanted to confirm if it is a wireshark added info or is in the packet itself.</p></div><div id="comment-55678-info" class="comment-info"><span class="comment-age">(20 Sep '16, 02:55)</span> <span class="comment-user userinfo">sukeerthi</span></div></div></div><div id="comment-tools-55677" class="comment-tools"></div><div class="clear"></div><div id="comment-55677-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

