+++
type = "question"
title = "Generate rpm with a custom build number"
description = '''I&#x27;m running configure.ac on RHEL 7.2, i&#x27;m wondering if there&#x27;s a way to set the Release number (which is defined om the spec file) as a variable like the Version number which is being generated by the configure.ac and is written to the config.h , I&#x27;d like to set a kind of BUILD_NUMBER variable somew...'''
date = "2016-02-14T14:27:00Z"
lastmod = "2016-02-17T08:00:00Z"
weight = 50188
keywords = [ "wireshark", "rpm-package", "rpm", "build", "configure" ]
aliases = [ "/questions/50188" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Generate rpm with a custom build number](/questions/50188/generate-rpm-with-a-custom-build-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50188-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50188-score" class="post-score" title="current number of votes">0</div><span id="post-50188-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm running configure.ac on RHEL 7.2, i'm wondering if there's a way to set the Release number (which is defined om the spec file) as a variable like the Version number which is being generated by the configure.ac and is written to the config.h , I'd like to set a kind of BUILD_NUMBER variable somewhere, and it'll take the value of the exported variable during the execution.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-rpm-package" rel="tag" title="see questions tagged &#39;rpm-package&#39;">rpm-package</span> <span class="post-tag tag-link-rpm" rel="tag" title="see questions tagged &#39;rpm&#39;">rpm</span> <span class="post-tag tag-link-build" rel="tag" title="see questions tagged &#39;build&#39;">build</span> <span class="post-tag tag-link-configure" rel="tag" title="see questions tagged &#39;configure&#39;">configure</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '16, 14:27</strong></p><img src="https://secure.gravatar.com/avatar/df086c47092f1db92655a2e6bf2321f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alex1312&#39;s gravatar image" /><p><span>alex1312</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alex1312 has no accepted answers">0%</span></p></div></div><div id="comments-container-50188" class="comments-container"></div><div id="comment-tools-50188" class="comment-tools"></div><div class="clear"></div><div id="comment-50188-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50272"></span>

<div id="answer-container-50272" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50272-score" class="post-score" title="current number of votes">1</div><span id="post-50272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure. You'd just need to set a variable (e.g., <code>BUILD_NUMBER</code>) and then AC_SUBST it (all in configure.ac) and then change the Release line in <code>packaging/rpm/SPECS/wireshark.spec.in</code> to reference the variable (Release: @<span class="__cf_email__" data-cfemail="e7a5b2aeaba3b8a9b2aaa5a2b5a7">[email protected]</span>). You could add the build number to other .in files too if desired.</p><p>Of course you'd need to autogen and configure again to regenerate <code>wireshark.spec</code>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '16, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-50272" class="comments-container"></div><div id="comment-tools-50272" class="comment-tools"></div><div class="clear"></div><div id="comment-50272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

