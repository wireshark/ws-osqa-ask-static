+++
type = "question"
title = "how do you interpret wireshark trace for application slowness between 2 servers"
description = '''I am new to wireshark trace analysis and needs pointers in interpreting a wireshark trace for application slowness between 2 servers. What specific parameters I would look for in the trace as a cause of the slowness to occur? Would it be window extremely small TCP window size (500+)'''
date = "2013-10-22T17:17:00Z"
lastmod = "2013-10-22T17:17:00Z"
weight = 26301
keywords = [ "window", "size" ]
aliases = [ "/questions/26301" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how do you interpret wireshark trace for application slowness between 2 servers](/questions/26301/how-do-you-interpret-wireshark-trace-for-application-slowness-between-2-servers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26301-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26301-score" class="post-score" title="current number of votes">0</div><span id="post-26301-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to wireshark trace analysis and needs pointers in interpreting a wireshark trace for application slowness between 2 servers. What specific parameters I would look for in the trace as a cause of the slowness to occur? Would it be window extremely small TCP window size (500+)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-window" rel="tag" title="see questions tagged &#39;window&#39;">window</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '13, 17:17</strong></p><img src="https://secure.gravatar.com/avatar/43930ecb4f7d99825ccd5cda710f328a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wshark&#39;s gravatar image" /><p><span>wshark</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wshark has no accepted answers">0%</span></p></div></div><div id="comments-container-26301" class="comments-container"></div><div id="comment-tools-26301" class="comment-tools"></div><div class="clear"></div><div id="comment-26301-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

