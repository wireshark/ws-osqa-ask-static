+++
type = "question"
title = "TCP packets of not all devices are captured ?"
description = '''Hello all, I&#x27;m a beginner at packet sniffing.I have been running wireshark on my Linux machine after enabling monitor mode for my wireless interface. I can only see TCP packets of very few devices being captured. I see a lot of 802.11 packets captured involving probe response/request information.  I...'''
date = "2014-11-23T00:04:00Z"
lastmod = "2014-11-23T00:04:00Z"
weight = 38073
keywords = [ "sniffng", "packet-capture", "tcp" ]
aliases = [ "/questions/38073" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP packets of not all devices are captured ?](/questions/38073/tcp-packets-of-not-all-devices-are-captured)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38073-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38073-score" class="post-score" title="current number of votes">0</div><span id="post-38073-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all,</p><p>I'm a beginner at packet sniffing.I have been running wireshark on my Linux machine after enabling monitor mode for my wireless interface. I can only see TCP packets of very few devices being captured. I see a lot of 802.11 packets captured involving probe response/request information.</p><p>I am not able to understand why the TCP packets of the rest of the devices are not being captured. Is this because of the secure connection they all are connected to ? I would be glad if someone could throw some light on this.</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffng" rel="tag" title="see questions tagged &#39;sniffng&#39;">sniffng</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '14, 00:04</strong></p><img src="https://secure.gravatar.com/avatar/94eb1c86b27e316bf3c613eaea1feefe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srik11&#39;s gravatar image" /><p><span>srik11</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srik11 has no accepted answers">0%</span></p></div></div><div id="comments-container-38073" class="comments-container"></div><div id="comment-tools-38073" class="comment-tools"></div><div class="clear"></div><div id="comment-38073-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

