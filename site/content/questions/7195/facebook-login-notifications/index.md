+++
type = "question"
title = "Facebook Login notifications"
description = '''Login notifactions in ase you dont know says &quot;We can notify you when your account is accessed from a computer or mobile device that you haven&#x27;t used before&quot; I need to understand whether sniffing cookies will trigger this on a different laptop or whether i&#x27;d have to do it on the sniffed laptop for it...'''
date = "2011-11-02T05:00:00Z"
lastmod = "2011-11-03T18:13:00Z"
weight = 7195
keywords = [ "sniffing", "cookies", "facebook" ]
aliases = [ "/questions/7195" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Facebook Login notifications](/questions/7195/facebook-login-notifications)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7195-score" class="post-score" title="current number of votes">0</div><span id="post-7195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Login notifactions in ase you dont know says "We can notify you when your account is accessed from a computer or mobile device that you haven't used before"</p><p>I need to understand whether sniffing cookies will trigger this on a different laptop or whether i'd have to do it on the sniffed laptop for it not to trigger the alert.</p><p>Thanks B</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-cookies" rel="tag" title="see questions tagged &#39;cookies&#39;">cookies</span> <span class="post-tag tag-link-facebook" rel="tag" title="see questions tagged &#39;facebook&#39;">facebook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '11, 05:00</strong></p><img src="https://secure.gravatar.com/avatar/6a5ed099c822fef94d52e4107ed319f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="poohbrains&#39;s gravatar image" /><p><span>poohbrains</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="poohbrains has no accepted answers">0%</span></p></div></div><div id="comments-container-7195" class="comments-container"><span id="7196"></span><div id="comment-7196" class="comment"><div id="post-7196-score" class="comment-score"></div><div class="comment-text"><p>You haven't really asked a question. Wireshark can help you see what cookies are being sent or received in HTTP transactions, is this what you want to know?</p></div><div id="comment-7196-info" class="comment-info"><span class="comment-age">(02 Nov '11, 05:17)</span> <span class="comment-user userinfo">martyvis</span></div></div><span id="7215"></span><div id="comment-7215" class="comment"><div id="post-7215-score" class="comment-score"></div><div class="comment-text"><p>Hi,</p><p>Sorry, let me try to be clearer.</p><p>If i use wireshark to sniff a cookie, then use that cookie to log into facebook on a different laptop will it trigger the "Login Notifications" security feature of facebook?</p></div><div id="comment-7215-info" class="comment-info"><span class="comment-age">(03 Nov '11, 02:29)</span> <span class="comment-user userinfo">poohbrains</span></div></div><span id="7219"></span><div id="comment-7219" class="comment"><div id="post-7219-score" class="comment-score"></div><div class="comment-text"><p>Probably. Why don't you just try it?</p></div><div id="comment-7219-info" class="comment-info"><span class="comment-age">(03 Nov '11, 08:33)</span> <span class="comment-user userinfo">bstn</span></div></div></div><div id="comment-tools-7195" class="comment-tools"></div><div class="clear"></div><div id="comment-7195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7228"></span>

<div id="answer-container-7228" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7228-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7228-score" class="post-score" title="current number of votes">0</div><span id="post-7228-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't know. That's a question about Facebook, not about Wireshark (you could have used a different program to sniff the cookie; it wouldn't make a difference). Sniffing the cookie won't do anything; reusing it might, or might not. As bstn said, "Why don't you just try it?"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '11, 18:13</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-7228" class="comments-container"></div><div id="comment-tools-7228" class="comment-tools"></div><div class="clear"></div><div id="comment-7228-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

