+++
type = "question"
title = "Monitoring web sockets via wireshark?"
description = '''Hi, I have built a simple html5/websocket application but I cannot seem to capture the packet data associated with any web socket connections with wireshark. Has anyone done this before? I am failing to find anything that looks like it was sent to or from the web socket server. My purpose here is to...'''
date = "2011-12-27T15:45:00Z"
lastmod = "2011-12-27T23:28:00Z"
weight = 8144
keywords = [ "html5", "websockets" ]
aliases = [ "/questions/8144" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring web sockets via wireshark?](/questions/8144/monitoring-web-sockets-via-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8144-score" class="post-score" title="current number of votes">0</div><span id="post-8144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have built a simple html5/websocket application but I cannot seem to capture the packet data associated with any web socket connections with wireshark. Has anyone done this before? I am failing to find anything that looks like it was sent to or from the web socket server. My purpose here is to test how easy it is to monitor the contents of the packets (in the case that I want to send potentially sensitive data over the connection), and as a web developer this kind of thing is a little out of my normal scope so any help is appreciated. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-html5" rel="tag" title="see questions tagged &#39;html5&#39;">html5</span> <span class="post-tag tag-link-websockets" rel="tag" title="see questions tagged &#39;websockets&#39;">websockets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Dec '11, 15:45</strong></p><img src="https://secure.gravatar.com/avatar/945b930a4ae8da5bb9807051f88e074a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dan%20Munro&#39;s gravatar image" /><p><span>Dan Munro</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dan Munro has no accepted answers">0%</span></p></div></div><div id="comments-container-8144" class="comments-container"></div><div id="comment-tools-8144" class="comment-tools"></div><div class="clear"></div><div id="comment-8144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8147"></span>

<div id="answer-container-8147" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8147-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8147-score" class="post-score" title="current number of votes">0</div><span id="post-8147-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Think about your <a href="http://wiki.wireshark.org/CaptureSetup">capture setup</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Dec '11, 23:28</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-8147" class="comments-container"></div><div id="comment-tools-8147" class="comment-tools"></div><div class="clear"></div><div id="comment-8147-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

