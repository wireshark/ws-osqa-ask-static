+++
type = "question"
title = "Wireshark nc10"
description = '''Hi, I&#x27;ve read that you&#x27;re able to run wireshark on your Samsung nc10. I&#x27;ve read in other forums that wifi card can&#x27;t sniffs packets in &quot;promiscous mode&quot;. In fact, when I&#x27;ve tried for, I&#x27;ve the same result of all other users. I use win xp sp3.'''
date = "2011-03-21T05:23:00Z"
lastmod = "2011-03-21T16:41:00Z"
weight = 2964
keywords = [ "nc10", "wireshark", "promiscuous", "samsung" ]
aliases = [ "/questions/2964" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark nc10](/questions/2964/wireshark-nc10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2964-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2964-score" class="post-score" title="current number of votes">0</div><span id="post-2964-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I've read that you're able to run wireshark on your Samsung nc10. I've read in other forums that wifi card can't sniffs packets in "promiscous mode". In fact, when I've tried for, I've the same result of all other users. I use win xp sp3.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nc10" rel="tag" title="see questions tagged &#39;nc10&#39;">nc10</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-samsung" rel="tag" title="see questions tagged &#39;samsung&#39;">samsung</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '11, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/3d0246e63f34367902879b6f40887728?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="maidirepelle&#39;s gravatar image" /><p><span>maidirepelle</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="maidirepelle has no accepted answers">0%</span></p></div></div><div id="comments-container-2964" class="comments-container"></div><div id="comment-tools-2964" class="comment-tools"></div><div class="clear"></div><div id="comment-2964-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2993"></span>

<div id="answer-container-2993" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2993-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2993-score" class="post-score" title="current number of votes">0</div><span id="post-2993-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, Wireshark runs fine on Samsung NC10, I have done that on XP SP3 as well as Win7 32bit. Capturing wireless is an issue running Windows OS since the WiFi card can't be switched into monitor mode. I have an alternate boot into Ubuntu netbook edition for that one on my Samsung ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '11, 16:41</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2993" class="comments-container"></div><div id="comment-tools-2993" class="comment-tools"></div><div class="clear"></div><div id="comment-2993-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

