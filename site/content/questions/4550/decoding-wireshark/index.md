+++
type = "question"
title = "decoding wireshark"
description = '''0 i hv written code to reverse the hex dump in the C language, but i observed the output like this ∟ o e ∞ m @ ☻ Ñ N ╢ 2 E ( δ à ≥ ♠ Σ M ╤ ‼ e j └ ¿ ☺ ╓ ☺ ╗ └ J d ¿ σ * k · ½ ♥ P ► D p O æ ∟ o e ∞ m @ ☻ Ñ N ╢ 2 E ☻ s δ ë ≥ ♠ ß ■ ╤ ‼ e j └ ¿ ☺ ╓ ☺ ╗ └ J d ¿ σ * k · ½ ♥ P ↑ D p n ♀ ↨ ♥ ☺ ☻ F ⌠ N ╜ º i...'''
date = "2011-06-13T21:33:00Z"
lastmod = "2011-06-23T10:41:00Z"
weight = 4550
keywords = [ "decoder" ]
aliases = [ "/questions/4550" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decoding wireshark](/questions/4550/decoding-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4550-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4550-score" class="post-score" title="current number of votes">0</div><span id="post-4550-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>0</p><p>i hv written code to reverse the hex dump in the C language, but i observed the output like this ∟ o e ∞ m @ ☻ Ñ N ╢ 2 E ( δ à ≥ ♠ Σ M ╤ ‼ e j └ ¿ ☺ ╓ ☺ ╗ └ J d ¿ σ * k · ½ ♥ P ► D p O æ ∟ o e ∞ m @ ☻ Ñ N ╢ 2 E ☻ s δ ë ≥ ♠ ß ■ ╤ ‼ e j └ ¿ ☺ ╓ ☺ ╗ └ J d ¿ σ * k · ½ ♥ P ↑ D p n ♀ ↨ ♥ ☺ ☻ F ⌠ N</p><pre><code>╜ º i ╨ ⌡ ú ├ â ╥ à σ ╤ Y î ≡ â 9 - v ₧ ┤ ╧ , π W à # s ► æ ƒ ▐ n ù &quot; V £ A ⌡ ∟ ┤ 4 ú α ¶ ¡ ; l P ┘ ├ ú α ñ ╖ ♀ ╜ ╗ Γ ¥ ↓ $ m √ ╢ τ O ░ 3 ♀ ò &amp; Q ─ ï 6 ╩ ☼ &#39; Y ½ ╘ ¼ ⌐ ☼ ╔ ╫ τ k ⁿ ▼ s g | ( ≤ ╞ û ▬ d ♂ # ╔ ü º ₧ &lt; £ π ü ¥ l ¡ à ┘ ▐ ▓ &amp; o Y s █ { ì û Ö a ╜ Ñ Φ ⌐ ╗ ° ╝ ü § ┴ ╚   ½ Z º [ § É ╞ ╫ ƒ { Σ E N ↕ Ö 6 ┬ ⌡ ú ú i ├ ß ╛</code></pre><p>how do i get the exact data.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decoder" rel="tag" title="see questions tagged &#39;decoder&#39;">decoder</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '11, 21:33</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div></div><div id="comments-container-4550" class="comments-container"><span id="4552"></span><div id="comment-4552" class="comment"><div id="post-4552-score" class="comment-score"></div><div class="comment-text"><p>0</p><p>Great ! Your getting somewhere, but what do you mean by "reverse the Hex Dump" ! ?</p><p>Put all the date back to front, or undo the Compilation ?</p><p>Geoff Gus Stacey</p><p><span class="__cf_email__" data-cfemail="96d1f3f9f0f0f1e3e5e5e2f7f5f3efd6f1fbf7fffab8f5f9fb">[email protected]</span></p></div><div id="comment-4552-info" class="comment-info"><span class="comment-age">(13 Jun '11, 21:58)</span> <span class="comment-user userinfo">Gusgeoff</span></div></div><span id="4553"></span><div id="comment-4553" class="comment"><div id="post-4553-score" class="comment-score"></div><div class="comment-text"><p>the data stored from wireshark is in hex format so i jus reversed to ascii.</p></div><div id="comment-4553-info" class="comment-info"><span class="comment-age">(13 Jun '11, 22:00)</span> <span class="comment-user userinfo">sagu072</span></div></div><span id="4660"></span><div id="comment-4660" class="comment"><div id="post-4660-score" class="comment-score"></div><div class="comment-text"><p>That will do you no good as it's a binary format(Hex). You can print to file if you want to have the decoded output,</p></div><div id="comment-4660-info" class="comment-info"><span class="comment-age">(21 Jun '11, 23:26)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="4703"></span><div id="comment-4703" class="comment"><div id="post-4703-score" class="comment-score"></div><div class="comment-text"><p>If by "the data stored from Wireshark" you mean the capture file from Wireshark, it is, as noted, a binary file, and a bunch of us have already written several programs that display the packet data in ASCII. Those programs have names such as "Wireshark" and "TShark". :-)</p><p>Doing the work Wireshark and TShark do - or even that tcpdump does - is a significant amount of work. You need to handle the details of pcap or pcap-ng format (or use libpcap/WinPcap to read the file), and then decode the packet data.</p></div><div id="comment-4703-info" class="comment-info"><span class="comment-age">(23 Jun '11, 10:41)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4550" class="comment-tools"></div><div class="clear"></div><div id="comment-4550-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

