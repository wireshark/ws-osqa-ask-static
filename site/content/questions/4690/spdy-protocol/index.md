+++
type = "question"
title = "SPDY protocol"
description = '''Hi I am interested in studying the protocol which is the SPDY. I have already researched Google&#x27;s white report and saw previous question about SPDY in this site. I knew that Wireshark application don&#x27;t support this protocol. Do you have a plan supporting SPDY protocol? If you have a plan, I wish to ...'''
date = "2011-06-23T06:06:00Z"
lastmod = "2011-06-23T06:47:00Z"
weight = 4690
keywords = [ "spdy", "google" ]
aliases = [ "/questions/4690" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SPDY protocol](/questions/4690/spdy-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4690-score" class="post-score" title="current number of votes">0</div><span id="post-4690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am interested in studying the protocol which is the SPDY.</p><p>I have already researched Google's white report and saw previous question about SPDY in this site.</p><p>I knew that Wireshark application don't support this protocol.</p><p>Do you have a plan supporting SPDY protocol?</p><p>If you have a plan, I wish to know when wireshark application supporting SPDY protocol?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spdy" rel="tag" title="see questions tagged &#39;spdy&#39;">spdy</span> <span class="post-tag tag-link-google" rel="tag" title="see questions tagged &#39;google&#39;">google</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '11, 06:06</strong></p><img src="https://secure.gravatar.com/avatar/e143769359612bbf25cb99fd2ac28418?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shadowk&#39;s gravatar image" /><p><span>Shadowk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shadowk has no accepted answers">0%</span></p></div></div><div id="comments-container-4690" class="comments-container"></div><div id="comment-tools-4690" class="comment-tools"></div><div class="clear"></div><div id="comment-4690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4691"></span>

<div id="answer-container-4691" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4691-score" class="post-score" title="current number of votes">0</div><span id="post-4691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As the FAQ [http://www.wireshark.org/faq.html#q1.11] says:</p><p>Q 1.11: Are there any plans to support {your favorite protocol}?</p><p>A: Support for particular protocols is added to Wireshark as a result of people contributing that support; no formal plans for adding support for particular protocols in particular future releases exist.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '11, 06:44</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-4691" class="comments-container"><span id="4692"></span><div id="comment-4692" class="comment"><div id="post-4692-score" class="comment-score"></div><div class="comment-text"><p>We also already had this kind of question here:</p><p>http://ask.wireshark.org/questions/3428/supporting-protocol-spdy</p></div><div id="comment-4692-info" class="comment-info"><span class="comment-age">(23 Jun '11, 06:47)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-4691" class="comment-tools"></div><div class="clear"></div><div id="comment-4691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

