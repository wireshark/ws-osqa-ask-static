+++
type = "question"
title = "SCTP Throughput"
description = '''Hi all, Am trying to get the throughput of SCTP, is it possible to extract such information with Wireshark? If yes, how is it done and if no,what tool is capable? Thanks.'''
date = "2010-12-06T23:32:00Z"
lastmod = "2010-12-06T23:32:00Z"
weight = 1267
keywords = [ "throughput" ]
aliases = [ "/questions/1267" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SCTP Throughput](/questions/1267/sctp-throughput)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1267-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1267-score" class="post-score" title="current number of votes">0</div><span id="post-1267-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Am trying to get the throughput of SCTP, is it possible to extract such information with Wireshark? If yes, how is it done and if no,what tool is capable?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '10, 23:32</strong></p><img src="https://secure.gravatar.com/avatar/f6848f4bc424f2b58fdf28c21af357ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="promo&#39;s gravatar image" /><p><span>promo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="promo has no accepted answers">0%</span></p></div></div><div id="comments-container-1267" class="comments-container"></div><div id="comment-tools-1267" class="comment-tools"></div><div class="clear"></div><div id="comment-1267-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

