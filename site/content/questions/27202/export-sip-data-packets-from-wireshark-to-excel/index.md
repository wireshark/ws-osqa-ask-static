+++
type = "question"
title = "export SIP data packets from wireshark to excel"
description = '''we need someone can give us solution (no issue if paid) to we can export all SIP data packets from wireshark to excel because we need to compare difference between packets, we need export each packet to one row in excel to sort it and can see where is the difference in SIP packets. Thank you'''
date = "2013-11-21T01:06:00Z"
lastmod = "2013-11-21T06:03:00Z"
weight = 27202
keywords = [ "wireshart", "export", "excel" ]
aliases = [ "/questions/27202" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [export SIP data packets from wireshark to excel](/questions/27202/export-sip-data-packets-from-wireshark-to-excel)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27202-score" class="post-score" title="current number of votes">0</div><span id="post-27202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>we need someone can give us solution (no issue if paid) to we can export all SIP data packets from wireshark to excel because we need to compare difference between packets, we need export each packet to one row in excel to sort it and can see where is the difference in SIP packets.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshart" rel="tag" title="see questions tagged &#39;wireshart&#39;">wireshart</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-excel" rel="tag" title="see questions tagged &#39;excel&#39;">excel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '13, 01:06</strong></p><img src="https://secure.gravatar.com/avatar/772391f1ff2b91790c1d4c47f3ff5ceb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ngvoip&#39;s gravatar image" /><p><span>Ngvoip</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ngvoip has no accepted answers">0%</span></p></div></div><div id="comments-container-27202" class="comments-container"></div><div id="comment-tools-27202" class="comment-tools"></div><div class="clear"></div><div id="comment-27202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27213"></span>

<div id="answer-container-27213" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27213-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27213-score" class="post-score" title="current number of votes">0</div><span id="post-27213-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>all <strong>SIP data packets</strong> from wireshark to excel</p></blockquote><p>What does that mean? Do you want the whole SIP <strong>payload</strong> in ASCII and/or HEX in the Excel file?</p><blockquote><p>because we need to compare difference between packets,</p></blockquote><p>I don't think the "Ecxel approach" with a plain text visual compare method (using the eyes) will work very well.</p><p>Can you please add some information why and how you want to compare SIP packets? What are you looking for? Maybe there is a better way than using Excel.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '13, 06:03</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-27213" class="comments-container"></div><div id="comment-tools-27213" class="comment-tools"></div><div class="clear"></div><div id="comment-27213-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

