+++
type = "question"
title = "Wake on LAN issue"
description = '''Have a interesting problem which I cannot find a solution. Have one computer on my network that refuses to stay asleep will wake as so as it does. Have checked all the normal problem areas USB, Timers etc everything seems to be working, the other 10 PC&#x27;s on the network all basically configured the s...'''
date = "2017-03-20T22:10:00Z"
lastmod = "2017-03-20T22:10:00Z"
weight = 60209
keywords = [ "wol" ]
aliases = [ "/questions/60209" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wake on LAN issue](/questions/60209/wake-on-lan-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60209-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60209-score" class="post-score" title="current number of votes">0</div><span id="post-60209-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Have a interesting problem which I cannot find a solution.</p><p>Have one computer on my network that refuses to stay asleep will wake as so as it does. Have checked all the normal problem areas USB, Timers etc everything seems to be working, the other 10 PC's on the network all basically configured the same work fine.</p><p>For one last attempt I isolated the problem PC from the network only having the ADSL router connected to it still the same problem reconnected to the network bingo sleep, yeah fixed it. Bugger found I had no internet forgot to power on the router, Mmmm no sleep instant wake. Fine problem found bad router changed to new one bugger me problem still occurring.</p><p>What and why could make a router keep a computer awake, while all others stay asleep</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wol" rel="tag" title="see questions tagged &#39;wol&#39;">wol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '17, 22:10</strong></p><img src="https://secure.gravatar.com/avatar/287ca7847d72e492b843567ff189372d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LANmine&#39;s gravatar image" /><p><span>LANmine</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LANmine has no accepted answers">0%</span></p></div></div><div id="comments-container-60209" class="comments-container"></div><div id="comment-tools-60209" class="comment-tools"></div><div class="clear"></div><div id="comment-60209-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

