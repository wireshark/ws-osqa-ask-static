+++
type = "question"
title = "gsm_map BER (in SRI request)"
description = '''Hi  Hi I have problem with GSM_MAP in SRI request message, the message come with BER can someone help me how i can see the message in full view. BER: Dissector for OID:0.34.5 not implemented. Contact Wireshark developers if you want this supported I have wireshark version 1.8.3 Thanks a lot'''
date = "2012-10-21T06:11:00Z"
lastmod = "2012-10-21T07:58:00Z"
weight = 15125
keywords = [ "gsm_map" ]
aliases = [ "/questions/15125" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [gsm\_map BER (in SRI request)](/questions/15125/gsm_map-ber-in-sri-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15125-score" class="post-score" title="current number of votes">0</div><span id="post-15125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Hi I have problem with GSM_MAP in SRI request message, the message come with BER can someone help me how i can see the message in full view.</p><p>BER: Dissector for OID:0.34.5 not implemented. Contact Wireshark developers if you want this supported</p><p>I have wireshark version 1.8.3</p><p>Thanks a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm_map" rel="tag" title="see questions tagged &#39;gsm_map&#39;">gsm_map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '12, 06:11</strong></p><img src="https://secure.gravatar.com/avatar/b00c42a55fedd350133c09c1902552fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LIOR&#39;s gravatar image" /><p><span>LIOR</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LIOR has no accepted answers">0%</span></p></div></div><div id="comments-container-15125" class="comments-container"></div><div id="comment-tools-15125" class="comment-tools"></div><div class="clear"></div><div id="comment-15125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15129"></span>

<div id="answer-container-15129" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15129-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15129-score" class="post-score" title="current number of votes">0</div><span id="post-15129-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thats the OID describing the PrivateExtension, you will have to ask the menufacturer of the system generating that message for a description of their extension. BTW using the OID 0.34.5 seems wrong as it's not registered to a specific manufacturer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '12, 07:58</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-15129" class="comments-container"></div><div id="comment-tools-15129" class="comment-tools"></div><div class="clear"></div><div id="comment-15129-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

