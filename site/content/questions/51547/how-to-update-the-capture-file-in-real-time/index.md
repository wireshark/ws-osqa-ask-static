+++
type = "question"
title = "How to update the capture file in real time"
description = '''I&#x27;m on Windows and turned on &quot;capture to a permanent file&quot; under Output, but the pcap file does not get updated until the capture is stopped. Is there anything I can do to keep the file constantly updated while capturing packets in real time? Thanks.'''
date = "2016-04-10T17:57:00Z"
lastmod = "2016-07-20T10:42:00Z"
weight = 51547
keywords = [ "output" ]
aliases = [ "/questions/51547" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to update the capture file in real time](/questions/51547/how-to-update-the-capture-file-in-real-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51547-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51547-score" class="post-score" title="current number of votes">0</div><span id="post-51547-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm on Windows and turned on "capture to a permanent file" under Output, but the pcap file does not get updated until the capture is stopped. Is there anything I can do to keep the file constantly updated while capturing packets in real time? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-output" rel="tag" title="see questions tagged &#39;output&#39;">output</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '16, 17:57</strong></p><img src="https://secure.gravatar.com/avatar/a02cb44d1198d7a72d0aaaf5adfefc44?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="heisenburger&#39;s gravatar image" /><p><span>heisenburger</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="heisenburger has no accepted answers">0%</span></p></div></div><div id="comments-container-51547" class="comments-container"><span id="51802"></span><div id="comment-51802" class="comment"><div id="post-51802-score" class="comment-score"></div><div class="comment-text"><p>what are you trying to do?</p></div><div id="comment-51802-info" class="comment-info"><span class="comment-age">(19 Apr '16, 13:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-51547" class="comment-tools"></div><div class="clear"></div><div id="comment-51547-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54193"></span>

<div id="answer-container-54193" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54193-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54193-score" class="post-score" title="current number of votes">0</div><span id="post-54193-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The pcap file isn't updated after every packet - it would be very inefficient if it did - but it doesn't only get updated after capturing is stopped either. Perhaps this Microsoft article on <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/aa364218%28v=vs.85%29.aspx">File Caching</a> will explain.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '16, 10:42</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54193" class="comments-container"></div><div id="comment-tools-54193" class="comment-tools"></div><div class="clear"></div><div id="comment-54193-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

