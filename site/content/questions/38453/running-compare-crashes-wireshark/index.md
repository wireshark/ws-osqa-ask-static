+++
type = "question"
title = "Running Compare Crashes Wireshark"
description = '''Hi,  Whenever I try to merge captures and run a compare, it brings up the results but as soon as I try to click on any of the entries wireshark crashes.  I am running the latest version on 64bit and this is happening on both my windows 7 laptop, desktop and 2012 RC2 server.  Any ideas? '''
date = "2014-12-08T14:49:00Z"
lastmod = "2015-03-06T18:20:00Z"
weight = 38453
keywords = [ "merge", "compare" ]
aliases = [ "/questions/38453" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Running Compare Crashes Wireshark](/questions/38453/running-compare-crashes-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38453-score" class="post-score" title="current number of votes">0</div><span id="post-38453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Whenever I try to merge captures and run a compare, it brings up the results but as soon as I try to click on any of the entries wireshark crashes.</p><p>I am running the latest version on 64bit and this is happening on both my windows 7 laptop, desktop and 2012 RC2 server.</p><p>Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-merge" rel="tag" title="see questions tagged &#39;merge&#39;">merge</span> <span class="post-tag tag-link-compare" rel="tag" title="see questions tagged &#39;compare&#39;">compare</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '14, 14:49</strong></p><img src="https://secure.gravatar.com/avatar/d93ef297f89c59c1df8878f80ce4c37b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DevilWAH&#39;s gravatar image" /><p><span>DevilWAH</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DevilWAH has no accepted answers">0%</span></p></div></div><div id="comments-container-38453" class="comments-container"><span id="40332"></span><div id="comment-40332" class="comment"><div id="post-40332-score" class="comment-score"></div><div class="comment-text"><p>I've just run into this issue as well. Running v1.12.4 on Win7 64bit as of a few days ago. Has anyone else had this happen?</p></div><div id="comment-40332-info" class="comment-info"><span class="comment-age">(06 Mar '15, 14:25)</span> <span class="comment-user userinfo">Shaftoe</span></div></div></div><div id="comment-tools-38453" class="comment-tools"></div><div class="clear"></div><div id="comment-38453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40337"></span>

<div id="answer-container-40337" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40337-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40337-score" class="post-score" title="current number of votes">1</div><span id="post-40337-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's a bug, and is best reported on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>. You might - or might not! - get ways to work around the problem here, but if you want it <em>fixed</em>, it'll have to be reported on the Bugzilla.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Mar '15, 18:20</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-40337" class="comments-container"></div><div id="comment-tools-40337" class="comment-tools"></div><div class="clear"></div><div id="comment-40337-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

