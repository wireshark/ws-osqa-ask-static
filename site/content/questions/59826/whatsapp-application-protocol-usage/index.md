+++
type = "question"
title = "Whatsapp application protocol usage"
description = '''Hello, I am trying to understand the communication protocol of whatsapp for research reasons. I have been capturing the data from my android device and I have a question regarding the protocol laying on top of TCP. I know that wireshark uses XMPP protocol which indeed I can see in my pcap. However, ...'''
date = "2017-03-03T05:31:00Z"
lastmod = "2017-03-03T05:31:00Z"
weight = 59826
keywords = [ "protocol", "whatsapp", "pcap", "encryption" ]
aliases = [ "/questions/59826" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Whatsapp application protocol usage](/questions/59826/whatsapp-application-protocol-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59826-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59826-score" class="post-score" title="current number of votes">0</div><span id="post-59826-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am trying to understand the communication protocol of whatsapp for research reasons. I have been capturing the data from my android device and I have a question regarding the protocol laying on top of TCP.</p><p>I know that wireshark uses XMPP protocol which indeed I can see in my pcap. However, there are two cases where Whatsapp uses SSL.</p><ol><li>When the application tries to contact the service ppt.whatsapp.com then it uses the normal SSL (including handshakes and everythign)</li><li>Sometimes the applications when contacting one of the normal serves e.g e10.whatsapp.com starts using the SSL protocol and WireShark returns the <strong>"Continuation Data"</strong></li></ol><p>Is the choice of the protocol random some times or am I missing something here? I have already googled around but was unable to find something specific about the Whatsapp protocl.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '17, 05:31</strong></p><img src="https://secure.gravatar.com/avatar/9416b89cfa0625a9d558b53569aca45a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MikeXe&#39;s gravatar image" /><p><span>MikeXe</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MikeXe has no accepted answers">0%</span></p></div></div><div id="comments-container-59826" class="comments-container"></div><div id="comment-tools-59826" class="comment-tools"></div><div class="clear"></div><div id="comment-59826-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

