+++
type = "question"
title = "Can we decrypt SSL packages that capture via USB between MAC and iPhone ?"
description = '''Hello everybody, I use wireshark (Ubuntu 14.04) to capture traffic data between MAC (Virtual machine on Ubuntu) and iPhone. In MAC, I had privatekey.p12, certificates.cer, publickey.pem of MAC. With capture data, I found DeviceCetificate string, RootCertificate string and HostCertificate string. Can...'''
date = "2016-07-12T23:15:00Z"
lastmod = "2016-07-12T23:15:00Z"
weight = 54016
keywords = [ "private-key", "asymmetric", "symmetric", "public", "ssl_decrypt" ]
aliases = [ "/questions/54016" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can we decrypt SSL packages that capture via USB between MAC and iPhone ?](/questions/54016/can-we-decrypt-ssl-packages-that-capture-via-usb-between-mac-and-iphone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54016-score" class="post-score" title="current number of votes">0</div><span id="post-54016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everybody,</p><p>I use wireshark (Ubuntu 14.04) to capture traffic data between MAC (Virtual machine on Ubuntu) and iPhone. In MAC, I had privatekey.p12, certificates.cer, publickey.pem of MAC. With capture data, I found DeviceCetificate string, RootCertificate string and HostCertificate string. Can we decrypt traffic data between MAC and iPhone ? How do it ? Please help me and drop some information whenever you feel free time.</p><p>Thanks a lot !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-private-key" rel="tag" title="see questions tagged &#39;private-key&#39;">private-key</span> <span class="post-tag tag-link-asymmetric" rel="tag" title="see questions tagged &#39;asymmetric&#39;">asymmetric</span> <span class="post-tag tag-link-symmetric" rel="tag" title="see questions tagged &#39;symmetric&#39;">symmetric</span> <span class="post-tag tag-link-public" rel="tag" title="see questions tagged &#39;public&#39;">public</span> <span class="post-tag tag-link-ssl_decrypt" rel="tag" title="see questions tagged &#39;ssl_decrypt&#39;">ssl_decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jul '16, 23:15</strong></p><img src="https://secure.gravatar.com/avatar/1206daa4e7c36280b839780aee2801f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="truonggiang0710&#39;s gravatar image" /><p><span>truonggiang0710</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="truonggiang0710 has no accepted answers">0%</span></p></div></div><div id="comments-container-54016" class="comments-container"></div><div id="comment-tools-54016" class="comment-tools"></div><div class="clear"></div><div id="comment-54016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

