+++
type = "question"
title = "Interpret Packet Captures Data"
description = '''I am trying to find out what the follwing mean? TCP Connection Half Closed, Retransmission, Window Size Exceeded, Window Frozen, TCP Acked Lost Segment, Ack Too Long, TCP Frame Received Out of Order, Duplicate Ack, Non-Responsive Station'''
date = "2014-03-20T12:42:00Z"
lastmod = "2014-03-20T12:48:00Z"
weight = 31011
keywords = [ "windows", "ack", "segment", "retransmission", "non-responsive" ]
aliases = [ "/questions/31011" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Interpret Packet Captures Data](/questions/31011/interpret-packet-captures-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31011-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31011-score" class="post-score" title="current number of votes">0</div><span id="post-31011-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to find out what the follwing mean?</p><p>TCP Connection Half Closed, Retransmission, Window Size Exceeded, Window Frozen, TCP Acked Lost Segment, Ack Too Long, TCP Frame Received Out of Order, Duplicate Ack, Non-Responsive Station</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-segment" rel="tag" title="see questions tagged &#39;segment&#39;">segment</span> <span class="post-tag tag-link-retransmission" rel="tag" title="see questions tagged &#39;retransmission&#39;">retransmission</span> <span class="post-tag tag-link-non-responsive" rel="tag" title="see questions tagged &#39;non-responsive&#39;">non-responsive</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '14, 12:42</strong></p><img src="https://secure.gravatar.com/avatar/1b926add4bb4b9bccb1670bac7bbe6fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="platinumspades7&#39;s gravatar image" /><p><span>platinumspades7</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="platinumspades7 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Mar '14, 12:59</strong> </span></p></div></div><div id="comments-container-31011" class="comments-container"></div><div id="comment-tools-31011" class="comment-tools"></div><div class="clear"></div><div id="comment-31011-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31012"></span>

<div id="answer-container-31012" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31012-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31012-score" class="post-score" title="current number of votes">0</div><span id="post-31012-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try this: <a href="http://bit.ly/NxYLFE">http://bit.ly/NxYLFE</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '14, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div></div><div id="comments-container-31012" class="comments-container"></div><div id="comment-tools-31012" class="comment-tools"></div><div class="clear"></div><div id="comment-31012-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

