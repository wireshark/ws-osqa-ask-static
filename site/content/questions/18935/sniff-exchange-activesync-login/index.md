+++
type = "question"
title = "Sniff Exchange Activesync login"
description = '''Hi, I have this (non-rooted) company android phone with Exchange Activesync. I want to copy the account to my private phone but can&#x27;t see the password. Is there a way to capture this data while on my private WiFI network when syncing so that I can sniff the password? Thanks! Mike'''
date = "2013-02-27T12:36:00Z"
lastmod = "2013-02-27T12:51:00Z"
weight = 18935
keywords = [ "activesync", "exchange" ]
aliases = [ "/questions/18935" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sniff Exchange Activesync login](/questions/18935/sniff-exchange-activesync-login)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18935-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18935-score" class="post-score" title="current number of votes">0</div><span id="post-18935-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have this (non-rooted) company android phone with Exchange Activesync. I want to copy the account to my private phone but can't see the password. Is there a way to capture this data while on my private WiFI network when syncing so that I can sniff the password?</p><p>Thanks! Mike</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-activesync" rel="tag" title="see questions tagged &#39;activesync&#39;">activesync</span> <span class="post-tag tag-link-exchange" rel="tag" title="see questions tagged &#39;exchange&#39;">exchange</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '13, 12:36</strong></p><img src="https://secure.gravatar.com/avatar/1664907d629e14b843f6e63b849188a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sjmurf&#39;s gravatar image" /><p><span>sjmurf</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sjmurf has no accepted answers">0%</span></p></div></div><div id="comments-container-18935" class="comments-container"></div><div id="comment-tools-18935" class="comment-tools"></div><div class="clear"></div><div id="comment-18935-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18937"></span>

<div id="answer-container-18937" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18937-score" class="post-score" title="current number of votes">0</div><span id="post-18937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there a way to capture this data while on my private WiFI network when syncing so that I can sniff the password?</p></blockquote><p>You can't sniff the password (for good reasons), as the communication is encrypted to prevent the disclosure of things like the password and any of the synched data ;-)</p><blockquote><p>but can't see the password</p></blockquote><p>Hm... You don't know the password for your company e-mail account? How do you read your mail while you are in the office? Are you sure this is <strong>your</strong> phone?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '13, 12:51</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-18937" class="comments-container"></div><div id="comment-tools-18937" class="comment-tools"></div><div class="clear"></div><div id="comment-18937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

