+++
type = "question"
title = "[closed] Padded CC Packet"
description = '''I&#x27;m doing a capture using the NORM protocol dissector and CMD CC packets don&#x27;t look right. I&#x27;m expecting a packet size around 78 bytes and instead I&#x27;m getting a packet size of 2074 bytes. 2004 of those bytes are labeled Payload and they are all zeros. Is there any way to fix the way that Wireshark i...'''
date = "2013-09-20T05:55:00Z"
lastmod = "2013-09-20T05:55:00Z"
weight = 25026
keywords = [ "1.8.6", "packet-display", "payload", "norm", "congestion" ]
aliases = [ "/questions/25026" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Padded CC Packet](/questions/25026/padded-cc-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25026-score" class="post-score" title="current number of votes">0</div><span id="post-25026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm doing a capture using the NORM protocol dissector and CMD CC packets don't look right. I'm expecting a packet size around 78 bytes and instead I'm getting a packet size of 2074 bytes. 2004 of those bytes are labeled Payload and they are all zeros. Is there any way to fix the way that Wireshark is padding these packets?</p><p>I am using the packet-rmt-norm.c code off of <strong>Wireshark-1.8.6</strong></p><p>Unfortunately due to the network I'm using <strong>I cannot post a sample capture</strong></p><p>Thank you in advance for your help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1.8.6" rel="tag" title="see questions tagged &#39;1.8.6&#39;">1.8.6</span> <span class="post-tag tag-link-packet-display" rel="tag" title="see questions tagged &#39;packet-display&#39;">packet-display</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span> <span class="post-tag tag-link-norm" rel="tag" title="see questions tagged &#39;norm&#39;">norm</span> <span class="post-tag tag-link-congestion" rel="tag" title="see questions tagged &#39;congestion&#39;">congestion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '13, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/64a2be75a7a31bf1ba580e40acc8dab3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Torbett&#39;s gravatar image" /><p><span>Torbett</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Torbett has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>20 Sep '13, 06:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-25026" class="comments-container"></div><div id="comment-tools-25026" class="comment-tools"></div><div class="clear"></div><div id="comment-25026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question: http://ask.wireshark.org/questions/24577/malformed-norm-packets" by Kurt Knochner 20 Sep '13, 06:48

</div>

</div>

</div>

