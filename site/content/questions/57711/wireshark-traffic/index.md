+++
type = "question"
title = "Wireshark traffic"
description = '''Hello,  in my Homenetwork one of the Internet Devices is using sometimes the whole down or upstream. I can&#x27;t find this one and it don&#x27;t want to shut down in this time every single device. I know its an Wlan device because everytime i shutdown the Wlan downstream goes back to normal. is there a possi...'''
date = "2016-11-29T10:19:00Z"
lastmod = "2016-11-29T21:46:00Z"
weight = 57711
keywords = [ "poplar", "fritzboxvesco", "traffic" ]
aliases = [ "/questions/57711" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark traffic](/questions/57711/wireshark-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57711-score" class="post-score" title="current number of votes">0</div><span id="post-57711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, in my Homenetwork one of the Internet Devices is using sometimes the whole down or upstream. I can't find this one and it don't want to shut down in this time every single device. I know its an Wlan device because everytime i shutdown the Wlan downstream goes back to normal.</p><p>is there a possibility to show in wireshark which device is using the most traffic? Or to which ip it is adressed.</p><p>i got the file from <a href="http://fritz.box/html/capture.html">http://fritz.box/html/capture.html</a></p><p>Perhaps something like this:? <a href="http://bandwidthd.sourceforge.net/demo/">http://bandwidthd.sourceforge.net/demo/</a></p><p>Thanks a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-poplar" rel="tag" title="see questions tagged &#39;poplar&#39;">poplar</span> <span class="post-tag tag-link-fritzboxvesco" rel="tag" title="see questions tagged &#39;fritzboxvesco&#39;">fritzboxvesco</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Nov '16, 10:19</strong></p><img src="https://secure.gravatar.com/avatar/bf50099bbfb253bac66e020a215b14bf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="protter&#39;s gravatar image" /><p><span>protter</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="protter has no accepted answers">0%</span></p></div></div><div id="comments-container-57711" class="comments-container"></div><div id="comment-tools-57711" class="comment-tools"></div><div class="clear"></div><div id="comment-57711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57718"></span>

<div id="answer-container-57718" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57718-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57718-score" class="post-score" title="current number of votes">0</div><span id="post-57718-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When you load the file the various options in the Statistics menu should give you an idea what's going on, especially Conversations and Endpoints. If you take a capture on the Routing interface you'll see everything.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Nov '16, 21:46</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-57718" class="comments-container"></div><div id="comment-tools-57718" class="comment-tools"></div><div class="clear"></div><div id="comment-57718-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

