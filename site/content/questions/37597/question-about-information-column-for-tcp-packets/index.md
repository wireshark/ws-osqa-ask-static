+++
type = "question"
title = "question about information column for TCP packets"
description = '''I am working on a school project where we are analyzing networks using wireshark. We would like to try to figure out how many packets are lost within each conversation. I wrote a small program that accepts a csv generated from wireshark, and parses it into packet objects. I am a little confused abou...'''
date = "2014-11-05T16:56:00Z"
lastmod = "2014-11-05T16:56:00Z"
weight = 37597
keywords = [ "tcppackets" ]
aliases = [ "/questions/37597" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [question about information column for TCP packets](/questions/37597/question-about-information-column-for-tcp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37597-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37597-score" class="post-score" title="current number of votes">0</div><span id="post-37597-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working on a school project where we are analyzing networks using wireshark. We would like to try to figure out how many packets are lost within each conversation. I wrote a small program that accepts a csv generated from wireshark, and parses it into packet objects. I am a little confused about the Information column on TCP packets. We get multiple packets with the same Ack,Win, and Len values, but Seq number is always different. Why is this happening? What does the information column of a TCP packet capture really mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcppackets" rel="tag" title="see questions tagged &#39;tcppackets&#39;">tcppackets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 16:56</strong></p><img src="https://secure.gravatar.com/avatar/1ab600de83d7d55c3aaa8876ec70e82d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yemista&#39;s gravatar image" /><p><span>yemista</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yemista has no accepted answers">0%</span></p></div></div><div id="comments-container-37597" class="comments-container"></div><div id="comment-tools-37597" class="comment-tools"></div><div class="clear"></div><div id="comment-37597-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

