+++
type = "question"
title = "column info"
description = '''hi everyone, plz what &#x27;win&#x27;, &#x27;ws&#x27;, &#x27;Mss&#x27;,&#x27;sack_perm&#x27; mean in packet info colums??? Thanks for help. '''
date = "2015-04-01T07:03:00Z"
lastmod = "2015-04-01T08:04:00Z"
weight = 41095
keywords = [ "info", "capture", "packet" ]
aliases = [ "/questions/41095" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [column info](/questions/41095/column-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41095-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41095-score" class="post-score" title="current number of votes">0</div><span id="post-41095-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi everyone,</p><p>plz what 'win', 'ws', 'Mss','sack_perm' mean in packet info colums???</p><p>Thanks for help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 07:03</strong></p><img src="https://secure.gravatar.com/avatar/90875c0c2524531263f27b57e1d27ea3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hub&#39;s gravatar image" /><p><span>hub</span><br />
<span class="score" title="21 reputation points">21</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hub has no accepted answers">0%</span></p></div></div><div id="comments-container-41095" class="comments-container"></div><div id="comment-tools-41095" class="comment-tools"></div><div class="clear"></div><div id="comment-41095-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41096"></span>

<div id="answer-container-41096" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41096-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41096-score" class="post-score" title="current number of votes">3</div><span id="post-41096-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="hub has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><ul><li>win: windows size<br />
</li><li>ws: window scaling<br />
</li><li>mss: Maximum segment size<br />
</li><li>sack_perm: Selective ACK permitted<br />
</li></ul><p>See also here: <a href="https://ask.wireshark.org/questions/7235/what-is-ws-tsval-and-sack_perm-mean-in-packet-info-columns">https://ask.wireshark.org/questions/7235/what-is-ws-tsval-and-sack_perm-mean-in-packet-info-columns</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '15, 08:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-41096" class="comments-container"></div><div id="comment-tools-41096" class="comment-tools"></div><div class="clear"></div><div id="comment-41096-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

