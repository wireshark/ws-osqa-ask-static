+++
type = "question"
title = "How can i get real time traffic from wireshark for another classification program?"
description = '''Please, may I know it is possible? although I can get log file from Wireshark,I want to add real time traffic from Wireshark to another classification program directly. I '''
date = "2013-06-23T09:34:00Z"
lastmod = "2013-06-23T10:15:00Z"
weight = 22254
keywords = [ "flowe" ]
aliases = [ "/questions/22254" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can i get real time traffic from wireshark for another classification program?](/questions/22254/how-can-i-get-real-time-traffic-from-wireshark-for-another-classification-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22254-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22254-score" class="post-score" title="current number of votes">0</div><span id="post-22254-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please, may I know it is possible? although I can get log file from Wireshark,I want to add real time traffic from Wireshark to another classification program directly. I</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flowe" rel="tag" title="see questions tagged &#39;flowe&#39;">flowe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '13, 09:34</strong></p><img src="https://secure.gravatar.com/avatar/aaabe147210782489a39ce7d3b315369?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zinwin&#39;s gravatar image" /><p><span>zinwin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zinwin has no accepted answers">0%</span></p></div></div><div id="comments-container-22254" class="comments-container"></div><div id="comment-tools-22254" class="comment-tools"></div><div class="clear"></div><div id="comment-22254-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22255"></span>

<div id="answer-container-22255" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22255-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22255-score" class="post-score" title="current number of votes">0</div><span id="post-22255-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is not creating any real time traffic. I guess you mean that you want to analyze recordings taken by Wireshark with another tool? In that case you can always open the recorded capture files in that other tool. It's not really real time though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '13, 10:15</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-22255" class="comments-container"></div><div id="comment-tools-22255" class="comment-tools"></div><div class="clear"></div><div id="comment-22255-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

