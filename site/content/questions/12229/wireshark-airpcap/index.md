+++
type = "question"
title = "WireShark AirPCAP"
description = '''I’m a Software Reseller in Korea. Reason of sending mail to you, I have a question. My customer want using pre-version of WireShark AirPCAP. If they purchase current version license, they have a use of right for pre-version for WireShark AirPCAP? According to answer about it, my customer decide to p...'''
date = "2012-06-27T02:17:00Z"
lastmod = "2012-06-27T04:05:00Z"
weight = 12229
keywords = [ "wireless" ]
aliases = [ "/questions/12229" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark AirPCAP](/questions/12229/wireshark-airpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12229-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12229-score" class="post-score" title="current number of votes">0</div><span id="post-12229-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I’m a Software Reseller in Korea.</p><p>Reason of sending mail to you, I have a question. My customer want using pre-version of WireShark AirPCAP.</p><p>If they purchase current version license, they have a use of right for pre-version for WireShark AirPCAP? According to answer about it, my customer decide to purchase order.</p><p>I am looking forward to your ASAP Reply.</p><p>Best Regards S.E-Park</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '12, 02:17</strong></p><img src="https://secure.gravatar.com/avatar/c38654c18ac9a453e90bbbbd57dd5b0c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="park&#39;s gravatar image" /><p><span>park</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="park has no accepted answers">0%</span></p></div></div><div id="comments-container-12229" class="comments-container"></div><div id="comment-tools-12229" class="comment-tools"></div><div class="clear"></div><div id="comment-12229-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12233"></span>

<div id="answer-container-12233" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12233-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12233-score" class="post-score" title="current number of votes">1</div><span id="post-12233-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is now a Riverbed product. See their website here for further details:</p><p><a href="http://www.riverbed.com/us/products/cascade/wireshark_enhancements/airpcap.php">http://www.riverbed.com/us/products/cascade/wireshark_enhancements/airpcap.php</a></p><p>Their support contact should be able to provide you with the answer you seek.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '12, 04:05</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-12233" class="comments-container"></div><div id="comment-tools-12233" class="comment-tools"></div><div class="clear"></div><div id="comment-12233-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

