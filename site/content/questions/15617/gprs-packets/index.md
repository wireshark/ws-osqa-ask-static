+++
type = "question"
title = "GPRS Packets"
description = '''IS wireshark able to decode GPRS packets completely? Also what is the format of GPRS packets?'''
date = "2012-11-07T02:20:00Z"
lastmod = "2012-11-07T07:06:00Z"
weight = 15617
keywords = [ "gprs" ]
aliases = [ "/questions/15617" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GPRS Packets](/questions/15617/gprs-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15617-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15617-score" class="post-score" title="current number of votes">0</div><span id="post-15617-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>IS wireshark able to decode GPRS packets completely? Also what is the format of GPRS packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gprs" rel="tag" title="see questions tagged &#39;gprs&#39;">gprs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '12, 02:20</strong></p><img src="https://secure.gravatar.com/avatar/01f72880b1dbd9e8aee64f41b753db39?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hira&#39;s gravatar image" /><p><span>Hira</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hira has no accepted answers">0%</span></p></div></div><div id="comments-container-15617" class="comments-container"></div><div id="comment-tools-15617" class="comment-tools"></div><div class="clear"></div><div id="comment-15617-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15633"></span>

<div id="answer-container-15633" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15633-score" class="post-score" title="current number of votes">0</div><span id="post-15633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It depends at what reference point see <a href="http://wiki.wireshark.org/GsmProtocolFamily">http://wiki.wireshark.org/GsmProtocolFamily</a></p><p>Wireshark can't capture packets at all the reference points you may need something else to produce the pcap file(s). You probably need to study the 3GPP specs.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '12, 03:59</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-15633" class="comments-container"><span id="15643"></span><div id="comment-15643" class="comment"><div id="post-15643-score" class="comment-score"></div><div class="comment-text"><p>ok thanks :)</p></div><div id="comment-15643-info" class="comment-info"><span class="comment-age">(07 Nov '12, 06:59)</span> <span class="comment-user userinfo">Hira</span></div></div><span id="15645"></span><div id="comment-15645" class="comment"><div id="post-15645-score" class="comment-score"></div><div class="comment-text"><p>If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions.</p></div><div id="comment-15645-info" class="comment-info"><span class="comment-age">(07 Nov '12, 07:06)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15633" class="comment-tools"></div><div class="clear"></div><div id="comment-15633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

