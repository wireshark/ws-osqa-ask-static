+++
type = "question"
title = "SMB2 and Ioctl STATUS_NOT_FOUND"
description = '''Hello, I did a packet capture of a computer that boots and logs in on a corporate network. The client is Windows 7 and I think that the servers are Windows 2008. There are a few lines that I do not understand. See the screenshot: http://postimg.org/image/zfqzmva0r/ Packet 978 is an Ioctl Request for...'''
date = "2015-03-08T15:44:00Z"
lastmod = "2015-03-08T15:44:00Z"
weight = 40369
keywords = [ "ioctl", "smb2" ]
aliases = [ "/questions/40369" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SMB2 and Ioctl STATUS\_NOT\_FOUND](/questions/40369/smb2-and-ioctl-status_not_found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40369-score" class="post-score" title="current number of votes">0</div><span id="post-40369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I did a packet capture of a computer that boots and logs in on a corporate network. The client is Windows 7 and I think that the servers are Windows 2008. There are a few lines that I do not understand. See the screenshot:</p><p><a href="http://postimg.org/image/zfqzmva0r/">http://postimg.org/image/zfqzmva0r/</a></p><p>Packet 978 is an Ioctl Request for a file on a server. Why does the server name have only one backslash? Should it not say \ \ before the servername instead of \?</p><p>Packet 979 gives Error: STATUS NOT FOUND. What does that mean? It looks to me as if it's working as supposed to anyway, looking at the packets right after the error.</p><p>Does anyone have an explanation?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ioctl" rel="tag" title="see questions tagged &#39;ioctl&#39;">ioctl</span> <span class="post-tag tag-link-smb2" rel="tag" title="see questions tagged &#39;smb2&#39;">smb2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '15, 15:44</strong></p><img src="https://secure.gravatar.com/avatar/bc0fed450fbdcb51171a53979c8b30f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Farid&#39;s gravatar image" /><p><span>Farid</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Farid has no accepted answers">0%</span></p></div></div><div id="comments-container-40369" class="comments-container"></div><div id="comment-tools-40369" class="comment-tools"></div><div class="clear"></div><div id="comment-40369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

