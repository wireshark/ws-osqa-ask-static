+++
type = "question"
title = "Using Wireshark with Snort to decrypt SSL traffic."
description = '''Hi, I have Snort installed and configured on our company network and its up and running and giving the alerts fine, but its unable to detect any of the encrypted traffic (say, https). Can I use Wireshark with snort so that I can make Snort detect the https as well? Thank You, Sreeraj.'''
date = "2011-06-23T10:17:00Z"
lastmod = "2011-06-23T10:17:00Z"
weight = 4700
keywords = [ "ssl", "snort" ]
aliases = [ "/questions/4700" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Using Wireshark with Snort to decrypt SSL traffic.](/questions/4700/using-wireshark-with-snort-to-decrypt-ssl-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4700-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4700-score" class="post-score" title="current number of votes">0</div><span id="post-4700-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have Snort installed and configured on our company network and its up and running and giving the alerts fine, but its unable to detect any of the encrypted traffic (say, https). Can I use Wireshark with snort so that I can make Snort detect the https as well?</p><p>Thank You, Sreeraj.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-snort" rel="tag" title="see questions tagged &#39;snort&#39;">snort</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '11, 10:17</strong></p><img src="https://secure.gravatar.com/avatar/9aad1cbabf1cf4b8cf15850b3b299995?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sreewave&#39;s gravatar image" /><p><span>sreewave</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sreewave has no accepted answers">0%</span></p></div></div><div id="comments-container-4700" class="comments-container"></div><div id="comment-tools-4700" class="comment-tools"></div><div class="clear"></div><div id="comment-4700-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

