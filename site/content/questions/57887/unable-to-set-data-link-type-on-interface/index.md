+++
type = "question"
title = "Unable to set data link type on interface"
description = '''I get the following error, &quot;Unable to set data link type on interface &#x27;en1&#x27;(EN10MB is not one of the DLTs supported by this device)&quot;. Help me out'''
date = "2016-12-05T22:39:00Z"
lastmod = "2016-12-06T01:42:00Z"
weight = 57887
keywords = [ "type", "link" ]
aliases = [ "/questions/57887" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to set data link type on interface](/questions/57887/unable-to-set-data-link-type-on-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57887-score" class="post-score" title="current number of votes">0</div><span id="post-57887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I get the following error, "Unable to set data link type on interface 'en1'(EN10MB is not one of the DLTs supported by this device)". Help me out</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-type" rel="tag" title="see questions tagged &#39;type&#39;">type</span> <span class="post-tag tag-link-link" rel="tag" title="see questions tagged &#39;link&#39;">link</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '16, 22:39</strong></p><img src="https://secure.gravatar.com/avatar/779f65a31f7a9b9fa3f93d0b2da3704e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shockzp&#39;s gravatar image" /><p><span>shockzp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shockzp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>06 Dec '16, 01:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-57887" class="comments-container"><span id="57889"></span><div id="comment-57889" class="comment"><div id="post-57889-score" class="comment-score"></div><div class="comment-text"><p>I converted your "answer" to a question ass that's how this site works.</p><p>Can you please edit your question with more details, e.g. Wireshark version, interface type and OS?</p></div><div id="comment-57889-info" class="comment-info"><span class="comment-age">(06 Dec '16, 01:42)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-57887" class="comment-tools"></div><div class="clear"></div><div id="comment-57887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

