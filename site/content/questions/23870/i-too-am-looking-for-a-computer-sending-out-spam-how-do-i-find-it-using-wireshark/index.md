+++
type = "question"
title = "I too am looking for a computer sending out spam how do I find it using wireshark?"
description = '''All the answers I have seen on the web point to internal mail servers but its our web host that looks after our email. So how do I use wireshark to find out which machine is being used as a spam bot? To be clear we have been informed that one of our machines is sending out spam but its our external ...'''
date = "2013-08-20T02:52:00Z"
lastmod = "2013-08-21T02:11:00Z"
weight = 23870
keywords = [ "spam" ]
aliases = [ "/questions/23870" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I too am looking for a computer sending out spam how do I find it using wireshark?](/questions/23870/i-too-am-looking-for-a-computer-sending-out-spam-how-do-i-find-it-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23870-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23870-score" class="post-score" title="current number of votes">0</div><span id="post-23870-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>All the answers I have seen on the web point to internal mail servers but its our web host that looks after our email. So how do I use wireshark to find out which machine is being used as a spam bot? To be clear we have been informed that one of our machines is sending out spam but its our external hosted mail server that is being logged and blocked all the machines in house 'appear' to be fine running the usual antivirus, spy bot etc but I'm pretty confident that at least one of our internal machines is compromised..... I just need to find out which.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spam" rel="tag" title="see questions tagged &#39;spam&#39;">spam</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '13, 02:52</strong></p><img src="https://secure.gravatar.com/avatar/263d71082794726d284e19f108856aa6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="renrows&#39;s gravatar image" /><p><span>renrows</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="renrows has no accepted answers">0%</span></p></div></div><div id="comments-container-23870" class="comments-container"><span id="23896"></span><div id="comment-23896" class="comment"><div id="post-23896-score" class="comment-score"></div><div class="comment-text"><p>For me it's not quite clear, you did get a complaint that you're spamming, right? Which system is sending the spam? If it's the IP of your internet-link, then it must be an internal machine that's sending it out. But if the complaint was against your external mail-server, then it might be an open relay or might be infected with malware etc. Thanks for clarifying!</p></div><div id="comment-23896-info" class="comment-info"><span class="comment-age">(21 Aug '13, 02:11)</span> <span class="comment-user userinfo">pfuender</span></div></div></div><div id="comment-tools-23870" class="comment-tools"></div><div class="clear"></div><div id="comment-23870-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

