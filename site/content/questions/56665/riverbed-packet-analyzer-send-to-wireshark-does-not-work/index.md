+++
type = "question"
title = "Riverbed Packet Analyzer - Send to WIreshark does not work"
description = '''I&#x27;m having difficulty getting &quot;send to wireshark&quot; in Riverbed PA to work at all. My setup is as below:  - Riverbed Packet Analyzer PE 10.9.2 on Win7x64 running in VMware ESXi  - Wireshark x64 2.2.0  - Winpcap 4.1.3 Riverbed sees the packets correctly; however &quot;send to wireshark&quot; spawns wireshark but...'''
date = "2016-10-25T17:12:00Z"
lastmod = "2016-10-25T17:12:00Z"
weight = 56665
keywords = [ "compatibility", "riverbed", "analyzer", "packet", "wireshark" ]
aliases = [ "/questions/56665" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Riverbed Packet Analyzer - Send to WIreshark does not work](/questions/56665/riverbed-packet-analyzer-send-to-wireshark-does-not-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56665-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56665-score" class="post-score" title="current number of votes">0</div><span id="post-56665-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm having difficulty getting "send to wireshark" in Riverbed PA to work at all.</p><p>My setup is as below: - Riverbed Packet Analyzer PE 10.9.2 on Win7x64 running in VMware ESXi - Wireshark x64 2.2.0 - Winpcap 4.1.3</p><p>Riverbed sees the packets correctly; however "send to wireshark" spawns wireshark but does not get any packets and after a while,</p><p>I've tried the x84 version of wireshark, changed install order, reinstall all components, but the feature still does not work; it says no packets captured.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compatibility" rel="tag" title="see questions tagged &#39;compatibility&#39;">compatibility</span> <span class="post-tag tag-link-riverbed" rel="tag" title="see questions tagged &#39;riverbed&#39;">riverbed</span> <span class="post-tag tag-link-analyzer" rel="tag" title="see questions tagged &#39;analyzer&#39;">analyzer</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '16, 17:12</strong></p><img src="https://secure.gravatar.com/avatar/0385091b25e3fcfba44be97bf8d8a120?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="framepersecond&#39;s gravatar image" /><p><span>framepersecond</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="framepersecond has no accepted answers">0%</span></p></div></div><div id="comments-container-56665" class="comments-container"></div><div id="comment-tools-56665" class="comment-tools"></div><div class="clear"></div><div id="comment-56665-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

