+++
type = "question"
title = "Wireshark doesn&#x27;t start on windows 7 x64"
description = '''I installed the latest version of wireshark on my PC running windows 7 x64. However when trying to launch it by clicking the .exe nothing happens. I tried running in with admin rights and win7 compatibility mode but nothing happens either. I tried looking around in task manager but no processes appe...'''
date = "2017-10-27T11:10:00Z"
lastmod = "2017-10-27T11:31:00Z"
weight = 64301
keywords = [ "techsupport" ]
aliases = [ "/questions/64301" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark doesn't start on windows 7 x64](/questions/64301/wireshark-doesnt-start-on-windows-7-x64)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64301-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64301-score" class="post-score" title="current number of votes">0</div><span id="post-64301-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I installed the latest version of wireshark on my PC running windows 7 x64. However when trying to launch it by clicking the .exe nothing happens. I tried running in with admin rights and win7 compatibility mode but nothing happens either. I tried looking around in task manager but no processes appear that are Wireshark related. Applications such as Tshark and rawshark close the instant they start up. I have no idea what is causing this. Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-techsupport" rel="tag" title="see questions tagged &#39;techsupport&#39;">techsupport</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '17, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/475227ecfca1030906493635875990d5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yes2u3no&#39;s gravatar image" /><p><span>yes2u3no</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yes2u3no has no accepted answers">0%</span></p></div></div><div id="comments-container-64301" class="comments-container"><span id="64302"></span><div id="comment-64302" class="comment"><div id="post-64302-score" class="comment-score"></div><div class="comment-text"><p>After uninstalling and doing a clean installation of Wireshark x32 the same thing happens. Nothing starts.</p></div><div id="comment-64302-info" class="comment-info"><span class="comment-age">(27 Oct '17, 11:31)</span> <span class="comment-user userinfo">yes2u3no</span></div></div></div><div id="comment-tools-64301" class="comment-tools"></div><div class="clear"></div><div id="comment-64301-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

