+++
type = "question"
title = "number of selected row"
description = '''Hi. In my results, how can I see the number of the selected row? (the first row being #1, second #2, etc)'''
date = "2014-01-25T12:29:00Z"
lastmod = "2014-01-27T20:55:00Z"
weight = 29155
keywords = [ "number", "row" ]
aliases = [ "/questions/29155" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [number of selected row](/questions/29155/number-of-selected-row)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29155-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29155-score" class="post-score" title="current number of votes">0</div><span id="post-29155-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. In my results, how can I see the number of the selected row? (the first row being #1, second #2, etc)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-number" rel="tag" title="see questions tagged &#39;number&#39;">number</span> <span class="post-tag tag-link-row" rel="tag" title="see questions tagged &#39;row&#39;">row</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '14, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/412b10652e55b9c4d3cc5243b7b58d0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="myrddin&#39;s gravatar image" /><p><span>myrddin</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="myrddin has no accepted answers">0%</span></p></div></div><div id="comments-container-29155" class="comments-container"><span id="29157"></span><div id="comment-29157" class="comment"><div id="post-29157-score" class="comment-score"></div><div class="comment-text"><p>What exactly do you mean by rows? Database rows, or packet rows? The total number of packets (= rows) can be seen in the status bar.</p></div><div id="comment-29157-info" class="comment-info"><span class="comment-age">(25 Jan '14, 12:32)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="29170"></span><div id="comment-29170" class="comment"><div id="post-29170-score" class="comment-score"></div><div class="comment-text"><p>Yes, I meant packet rows, sorry.</p></div><div id="comment-29170-info" class="comment-info"><span class="comment-age">(26 Jan '14, 12:54)</span> <span class="comment-user userinfo">myrddin</span></div></div></div><div id="comment-tools-29155" class="comment-tools"></div><div class="clear"></div><div id="comment-29155-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29212"></span>

<div id="answer-container-29212" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29212-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29212-score" class="post-score" title="current number of votes">0</div><span id="post-29212-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The row number is effectively the"packet number", which by default appear as the "No." column on the far left. If not, go to Edit &gt; Preferences &gt; Columns, and add "No. Number" as a selected column to display.</p><p>For the number of rows that are currently displayed, that should be in the status bar at the very bottom. You sometimes have to expand the bar depending on your window size, but it should give the number of packets total, displayed and dropped in that order. Alternatively, Statistics &gt; Summary will give that to you in the stats at the bottom of that window.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '14, 20:55</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-29212" class="comments-container"></div><div id="comment-tools-29212" class="comment-tools"></div><div class="clear"></div><div id="comment-29212-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

