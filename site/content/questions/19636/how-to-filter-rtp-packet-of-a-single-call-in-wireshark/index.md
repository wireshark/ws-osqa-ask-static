+++
type = "question"
title = "how to filter RTP packet of a single call in wireshark ?"
description = '''how to filter RTP packet of a single call in wireshark ?'''
date = "2013-03-19T00:25:00Z"
lastmod = "2013-03-21T06:17:00Z"
weight = 19636
keywords = [ "filter" ]
aliases = [ "/questions/19636" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [how to filter RTP packet of a single call in wireshark ?](/questions/19636/how-to-filter-rtp-packet-of-a-single-call-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19636-score" class="post-score" title="current number of votes">0</div><span id="post-19636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to filter RTP packet of a single call in wireshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '13, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/e12d9d0c390836527ae1e83372d959a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="archu&#39;s gravatar image" /><p><span>archu</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="archu has no accepted answers">0%</span></p></div></div><div id="comments-container-19636" class="comments-container"></div><div id="comment-tools-19636" class="comment-tools"></div><div class="clear"></div><div id="comment-19636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="19637"></span>

<div id="answer-container-19637" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19637-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19637-score" class="post-score" title="current number of votes">0</div><span id="post-19637-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Find the setup information (SDP?) and filter on IP/port combination.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Mar '13, 02:41</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-19637" class="comments-container"></div><div id="comment-tools-19637" class="comment-tools"></div><div class="clear"></div><div id="comment-19637-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19704"></span>

<div id="answer-container-19704" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19704-score" class="post-score" title="current number of votes">0</div><span id="post-19704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to Telephony---&gt;Voip calls. You should see your call in there.</p><p>Choose All or which one you like and apply it as a filter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '13, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/2fd4419ad615504ce0ad00bcbc0a0cd2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kaasi&#39;s gravatar image" /><p><span>Kaasi</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kaasi has no accepted answers">0%</span></p></div></div><div id="comments-container-19704" class="comments-container"></div><div id="comment-tools-19704" class="comment-tools"></div><div class="clear"></div><div id="comment-19704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

