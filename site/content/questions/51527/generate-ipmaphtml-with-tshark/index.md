+++
type = "question"
title = "generate ipmap.html with tshark"
description = '''Is it possible to generate a map like the one one can do with wireshark but with tshark? I understand that I can generate a listing with tshark to get the data needed for the map and then use ipmap.html as a template. But if there is an a solution out there I&#x27;d be happy not to reinvent the wheel. re...'''
date = "2016-04-09T00:34:00Z"
lastmod = "2016-07-20T11:10:00Z"
weight = 51527
keywords = [ "geoip", "ipmap", "tshark" ]
aliases = [ "/questions/51527" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [generate ipmap.html with tshark](/questions/51527/generate-ipmaphtml-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51527-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51527-score" class="post-score" title="current number of votes">0</div><span id="post-51527-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to generate a map like the one one can do with wireshark but with tshark? I understand that I can generate a listing with tshark to get the data needed for the map and then use ipmap.html as a template. But if there is an a solution out there I'd be happy not to reinvent the wheel.</p><p>regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span> <span class="post-tag tag-link-ipmap" rel="tag" title="see questions tagged &#39;ipmap&#39;">ipmap</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Apr '16, 00:34</strong></p><img src="https://secure.gravatar.com/avatar/c8b49a7a57ae09cd5482d4a4ac5bb593?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bengan&#39;s gravatar image" /><p><span>bengan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bengan has no accepted answers">0%</span></p></div></div><div id="comments-container-51527" class="comments-container"></div><div id="comment-tools-51527" class="comment-tools"></div><div class="clear"></div><div id="comment-51527-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54195"></span>

<div id="answer-container-54195" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54195-score" class="post-score" title="current number of votes">0</div><span id="post-54195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Invent away! This is not currently possible to do with <code>tshark</code>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '16, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54195" class="comments-container"></div><div id="comment-tools-54195" class="comment-tools"></div><div class="clear"></div><div id="comment-54195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

