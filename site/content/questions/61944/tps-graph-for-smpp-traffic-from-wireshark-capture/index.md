+++
type = "question"
title = "TPS graph for SMPP traffic from Wireshark capture"
description = '''Hello, Looking for some help on how to draw a throughput graph for SMPP traffic from the Wireshark capture. I am able to produce a nice graph from the I/O Graphs under statistics but the problem I am facing is, I have multiple SMPP submit_sm PDUs under a single frame and the I/O graph is not conside...'''
date = "2017-06-12T02:20:00Z"
lastmod = "2017-06-12T02:20:00Z"
weight = 61944
keywords = [ "smpp", "wireshark" ]
aliases = [ "/questions/61944" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TPS graph for SMPP traffic from Wireshark capture](/questions/61944/tps-graph-for-smpp-traffic-from-wireshark-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61944-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61944-score" class="post-score" title="current number of votes">0</div><span id="post-61944-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Looking for some help on how to draw a throughput graph for SMPP traffic from the Wireshark capture. I am able to produce a nice graph from the I/O Graphs under statistics but the problem I am facing is, I have multiple SMPP submit_sm PDUs under a single frame and the I/O graph is not considering the multiple PDUs instead it is taking it as a single PDU. It would be grateful if someone can help me out in drawing a graph.</p><p>Best Regards, Prakash Galla</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smpp" rel="tag" title="see questions tagged &#39;smpp&#39;">smpp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '17, 02:20</strong></p><img src="https://secure.gravatar.com/avatar/dc3f2a6b1dbb0b4bffa210f2bfb0e3a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prakash_gss&#39;s gravatar image" /><p><span>prakash_gss</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prakash_gss has no accepted answers">0%</span></p></div></div><div id="comments-container-61944" class="comments-container"></div><div id="comment-tools-61944" class="comment-tools"></div><div class="clear"></div><div id="comment-61944-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

