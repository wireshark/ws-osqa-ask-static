+++
type = "question"
title = "H.265 decoder"
description = '''Hi, Is there any planning to add H.265 decoder to the wireshark? similar to the H.264 decoder that build in '''
date = "2014-04-13T06:32:00Z"
lastmod = "2014-04-14T00:37:00Z"
weight = 31768
keywords = [ "h265", "h.265" ]
aliases = [ "/questions/31768" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [H.265 decoder](/questions/31768/h265-decoder)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31768-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31768-score" class="post-score" title="current number of votes">0</div><span id="post-31768-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Is there any planning to add H.265 decoder to the wireshark? similar to the H.264 decoder that build in</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h265" rel="tag" title="see questions tagged &#39;h265&#39;">h265</span> <span class="post-tag tag-link-h.265" rel="tag" title="see questions tagged &#39;h.265&#39;">h.265</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '14, 06:32</strong></p><img src="https://secure.gravatar.com/avatar/ee77429ef08c79a296a3c30879792dfb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="itzik&#39;s gravatar image" /><p><span>itzik</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="itzik has no accepted answers">0%</span></p></div></div><div id="comments-container-31768" class="comments-container"></div><div id="comment-tools-31768" class="comment-tools"></div><div class="clear"></div><div id="comment-31768-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31776"></span>

<div id="answer-container-31776" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31776-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31776-score" class="post-score" title="current number of votes">0</div><span id="post-31776-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not that I know of. Dissectors gets added when someone writes the code and offers it to Wireshark. There is no central planning on adding dissectors.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Apr '14, 00:37</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-31776" class="comments-container"></div><div id="comment-tools-31776" class="comment-tools"></div><div class="clear"></div><div id="comment-31776-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

