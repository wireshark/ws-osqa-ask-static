+++
type = "question"
title = "Can a remote server detect that I&#x27;m using Wireshark"
description = '''Do they recognize it? I hope they don&#x27;t.'''
date = "2014-11-13T06:25:00Z"
lastmod = "2014-11-13T06:51:00Z"
weight = 37821
keywords = [ "server" ]
aliases = [ "/questions/37821" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can a remote server detect that I'm using Wireshark](/questions/37821/can-a-remote-server-detect-that-im-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37821-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37821-score" class="post-score" title="current number of votes">0</div><span id="post-37821-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Do they recognize it? I hope they don't.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '14, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/61f9c66b9d40becc69e0d8a24fe095e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fshark1&#39;s gravatar image" /><p><span>Fshark1</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fshark1 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Nov '14, 12:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-37821" class="comments-container"></div><div id="comment-tools-37821" class="comment-tools"></div><div class="clear"></div><div id="comment-37821-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37822"></span>

<div id="answer-container-37822" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37822-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37822-score" class="post-score" title="current number of votes">0</div><span id="post-37822-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Assuming that you're asking about detection via network traffic this has been asked many times before, e.g.:</p><p><a href="https://ask.wireshark.org/questions/1580/detect-wireshark-usage-on-network">https://ask.wireshark.org/questions/1580/detect-wireshark-usage-on-network</a> <a href="https://ask.wireshark.org/questions/20201/detecting-wireshark-usage">https://ask.wireshark.org/questions/20201/detecting-wireshark-usage</a> <a href="https://ask.wireshark.org/questions/14351/detectprevent-wireshark">https://ask.wireshark.org/questions/14351/detectprevent-wireshark</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '14, 06:42</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-37822" class="comments-container"><span id="37823"></span><div id="comment-37823" class="comment"><div id="post-37823-score" class="comment-score"></div><div class="comment-text"><p>Thanks. It's only my own traffic which should be captured. Also only from certain pages. The <strong>remote server</strong> shouldn't detect it.</p></div><div id="comment-37823-info" class="comment-info"><span class="comment-age">(13 Nov '14, 06:51)</span> <span class="comment-user userinfo">Fshark1</span></div></div></div><div id="comment-tools-37822" class="comment-tools"></div><div class="clear"></div><div id="comment-37822-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

