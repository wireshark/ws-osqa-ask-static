+++
type = "question"
title = "Wireshark wireless capture on wlan0(mon0) does not seem to capturing any packets"
description = '''A new user of wireshark here. I use Wireshark on Kali Linux- I have one network adapter and a wireless usb adapter- Alfa Network. I am able to enable the alfa card for monitor mode. It is a card that is capable of monitoring and injection. However, when I select the capture interface on Wireshark as...'''
date = "2013-11-06T06:48:00Z"
lastmod = "2013-11-07T08:10:00Z"
weight = 26696
keywords = [ "wireless", "capture", "http", "wlan0", "mon0" ]
aliases = [ "/questions/26696" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark wireless capture on wlan0(mon0) does not seem to capturing any packets](/questions/26696/wireshark-wireless-capture-on-wlan0mon0-does-not-seem-to-capturing-any-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26696-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26696-score" class="post-score" title="current number of votes">0</div><span id="post-26696-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A new user of wireshark here. I use Wireshark on Kali Linux- I have one network adapter and a wireless usb adapter- Alfa Network. I am able to enable the alfa card for monitor mode. It is a card that is capable of monitoring and injection. However, when I select the capture interface on Wireshark as mon0 or wlan0, it captures the broadcast traffic that other wireless network is sending( not quite sure what the right term here is). I have a laptop using the wifi network as well as my mobile phone but could not capture any related traffic such as http or post. What am I doing wrong here? Should I not use my ethernet during this? I am running as root (although I know it is advise against it). Will running as root causes issues for capture?- Please help/advice. Really appreciate it!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-wlan0" rel="tag" title="see questions tagged &#39;wlan0&#39;">wlan0</span> <span class="post-tag tag-link-mon0" rel="tag" title="see questions tagged &#39;mon0&#39;">mon0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '13, 06:48</strong></p><img src="https://secure.gravatar.com/avatar/26e1d2bb1d0d55e4a9eb203f38b7b2f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gurushiva&#39;s gravatar image" /><p><span>gurushiva</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gurushiva has no accepted answers">0%</span></p></div></div><div id="comments-container-26696" class="comments-container"></div><div id="comment-tools-26696" class="comment-tools"></div><div class="clear"></div><div id="comment-26696-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26706"></span>

<div id="answer-container-26706" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26706-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26706-score" class="post-score" title="current number of votes">1</div><span id="post-26706-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>see my answer to a similar question.</p><blockquote><p><a href="http://ask.wireshark.org/questions/26347/unable-to-capture-wireless-traffic-on-monitor-mode-on-ubuntu-1004-version">http://ask.wireshark.org/questions/26347/unable-to-capture-wireless-traffic-on-monitor-mode-on-ubuntu-1004-version</a></p></blockquote><p>Did you follow those steps?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '13, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-26706" class="comments-container"><span id="26718"></span><div id="comment-26718" class="comment"><div id="post-26718-score" class="comment-score"></div><div class="comment-text"><p>Yes, I have been able to airmon-ng wlan0. I notice that if I am connected by the ethernet- I cannot use wlan0 just for capturing. I think i have misunderstood the concept here. If Im connected using wireless connection- all works well (I have yet to do more tests on the packets). However, if I am using a wired connection, I cannot use just use wlan0 for capturing. Assuming this is how the whole concept works, yes?. Please advice. Thank you.</p></div><div id="comment-26718-info" class="comment-info"><span class="comment-age">(07 Nov '13, 06:52)</span> <span class="comment-user userinfo">gurushiva</span></div></div><span id="26719"></span><div id="comment-26719" class="comment"><div id="post-26719-score" class="comment-score"></div><div class="comment-text"><blockquote><p>However, <strong>if I am using a wired connection</strong>, I cannot use just use wlan0 for capturing.</p></blockquote><p>I'm not sure what you mean by this, however: if you capture on <strong>wlan0</strong> you will only see your own wifi/wlan traffic. If you capture on <strong>mon0</strong> you <strong>should</strong> see traffic of other clients as well, that communicate on the same wifi/wlan channel.</p></div><div id="comment-26719-info" class="comment-info"><span class="comment-age">(07 Nov '13, 07:38)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="26720"></span><div id="comment-26720" class="comment"><div id="post-26720-score" class="comment-score"></div><div class="comment-text"><p>What I meant- Can I be using a wired connection to have myself connected to the Internet but at the same time- use wlan0/mon0 to monitor clients using my wifi?- Thank you</p></div><div id="comment-26720-info" class="comment-info"><span class="comment-age">(07 Nov '13, 08:10)</span> <span class="comment-user userinfo">gurushiva</span></div></div></div><div id="comment-tools-26706" class="comment-tools"></div><div class="clear"></div><div id="comment-26706-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

