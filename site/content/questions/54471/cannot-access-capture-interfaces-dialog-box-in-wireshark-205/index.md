+++
type = "question"
title = "cannot access capture interfaces dialog box in Wireshark 2.0.5"
description = '''I have installed Wireshark for windows (ver 2.0.5) and I cannot seem to access the Capture Interfaces dialog box. It does not appear as an option in the Capture drop down menu as per the manual and the keyboard short cut command (CRTL-I) does not work either in bringing it up. Any suggestions? Thank...'''
date = "2016-07-31T11:55:00Z"
lastmod = "2016-07-31T12:49:00Z"
weight = 54471
keywords = [ "capture", "interfaces" ]
aliases = [ "/questions/54471" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [cannot access capture interfaces dialog box in Wireshark 2.0.5](/questions/54471/cannot-access-capture-interfaces-dialog-box-in-wireshark-205)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54471-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54471-score" class="post-score" title="current number of votes">0</div><span id="post-54471-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have installed Wireshark for windows (ver 2.0.5) and I cannot seem to access the Capture Interfaces dialog box. It does not appear as an option in the Capture drop down menu as per the manual and the keyboard short cut command (CRTL-I) does not work either in bringing it up. Any suggestions? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '16, 11:55</strong></p><img src="https://secure.gravatar.com/avatar/27244ea1c5a3aa628f0f87933e6fbfe4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="macka749&#39;s gravatar image" /><p><span>macka749</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="macka749 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Jul '16, 17:07</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-54471" class="comments-container"></div><div id="comment-tools-54471" class="comment-tools"></div><div class="clear"></div><div id="comment-54471-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54472"></span>

<div id="answer-container-54472" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54472-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54472-score" class="post-score" title="current number of votes">0</div><span id="post-54472-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try opening "Capture Options". The "Capture Interfaces" and "Capture Options" dialogs had quite a bit of overlap and were combined in the new UI.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jul '16, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-54472" class="comments-container"></div><div id="comment-tools-54472" class="comment-tools"></div><div class="clear"></div><div id="comment-54472-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

