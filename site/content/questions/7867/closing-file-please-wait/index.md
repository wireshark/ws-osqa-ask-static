+++
type = "question"
title = "Closing file please wait"
description = '''Hi WireShark I set the capture to 100 mega-by-file to a SCSI disk. It&#x27;s ok but the window &quot;Closing Wait&quot; is blocked. I am forced to kill the task. What should I do? Thank you for your response. Marc'''
date = "2011-12-09T06:11:00Z"
lastmod = "2011-12-11T22:08:00Z"
weight = 7867
keywords = [ "closing", "file" ]
aliases = [ "/questions/7867" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Closing file please wait](/questions/7867/closing-file-please-wait)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7867-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7867-score" class="post-score" title="current number of votes">0</div><span id="post-7867-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi WireShark</p><p>I set the capture to 100 mega-by-file to a SCSI disk. It's ok but the window "Closing Wait" is blocked. I am forced to kill the task. What should I do? Thank you for your response.</p><p>Marc</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-closing" rel="tag" title="see questions tagged &#39;closing&#39;">closing</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '11, 06:11</strong></p><img src="https://secure.gravatar.com/avatar/97744b182ae2d52aa03b4f64bd2ae80c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mcado&#39;s gravatar image" /><p><span>mcado</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mcado has no accepted answers">0%</span></p></div></div><div id="comments-container-7867" class="comments-container"></div><div id="comment-tools-7867" class="comment-tools"></div><div class="clear"></div><div id="comment-7867-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7899"></span>

<div id="answer-container-7899" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7899-score" class="post-score" title="current number of votes">2</div><span id="post-7899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This sounds like <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3046">bug 3046</a>.</p><p>I know on my Windows host I can click on the Closing Wait window and select Alt+F4 to kill that window and move forward.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Dec '11, 13:23</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-7899" class="comments-container"><span id="7905"></span><div id="comment-7905" class="comment"><div id="post-7905-score" class="comment-score"></div><div class="comment-text"><p>Also see http://ask.wireshark.org/questions/7717/close-file-dialog-and-v16x.</p></div><div id="comment-7905-info" class="comment-info"><span class="comment-age">(11 Dec '11, 22:08)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-7899" class="comment-tools"></div><div class="clear"></div><div id="comment-7899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

