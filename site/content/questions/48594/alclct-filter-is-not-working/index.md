+++
type = "question"
title = "alc.lct filter is not working"
description = '''Hello I am using latest version of Wireshark . but alc.lct filter is not working. When i type alc.lct in display filter . Its turning red. Please suggest some fix for this. Thank You.'''
date = "2015-12-16T21:10:00Z"
lastmod = "2015-12-17T02:11:00Z"
weight = 48594
keywords = [ "alc" ]
aliases = [ "/questions/48594" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [alc.lct filter is not working](/questions/48594/alclct-filter-is-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48594-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48594-score" class="post-score" title="current number of votes">0</div><span id="post-48594-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I am using latest version of Wireshark . but alc.lct filter is not working. When i type alc.lct in display filter . Its turning red. Please suggest some fix for this.</p><p>Thank You.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-alc" rel="tag" title="see questions tagged &#39;alc&#39;">alc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '15, 21:10</strong></p><img src="https://secure.gravatar.com/avatar/be52f1c8951e9dbe47fde4f2247a6271?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sampath&#39;s gravatar image" /><p><span>sampath</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sampath has no accepted answers">0%</span></p></div></div><div id="comments-container-48594" class="comments-container"></div><div id="comment-tools-48594" class="comment-tools"></div><div class="clear"></div><div id="comment-48594-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="48596"></span>

<div id="answer-container-48596" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48596-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48596-score" class="post-score" title="current number of votes">0</div><span id="post-48596-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Please suggest some fix for this.</p></blockquote><p>Use just "lct". "alc.lct" has not been a valid field name in Wireshark since the 1.12 release.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Dec '15, 00:40</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Dec '15, 02:05</strong> </span></p></div></div><div id="comments-container-48596" class="comments-container"><span id="48597"></span><div id="comment-48597" class="comment"><div id="post-48597-score" class="comment-score"></div><div class="comment-text"><p>seems it <a href="https://www.wireshark.org/docs/dfref/a/alc.html">was supported</a> for quite a long time...</p></div><div id="comment-48597-info" class="comment-info"><span class="comment-age">(17 Dec '15, 00:44)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="48600"></span><div id="comment-48600" class="comment"><div id="post-48600-score" class="comment-score"></div><div class="comment-text"><p>It hasn't been valid since 1.12.</p></div><div id="comment-48600-info" class="comment-info"><span class="comment-age">(17 Dec '15, 02:02)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-48596" class="comment-tools"></div><div class="clear"></div><div id="comment-48596-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="48601"></span>

<div id="answer-container-48601" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48601-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48601-score" class="post-score" title="current number of votes">0</div><span id="post-48601-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would say your only option is to</p><ul><li><p>manually find a packet which that rule used to select when it existed</p></li><li><p>unfold the layers starting from alc upwards (which means downwards in the dissection pane)</p></li><li><p>look at the alc and its encapsulated protocols' fields available (whenever you select a line in the dissection pane which represents a real or forged protocol field, its name can be read in the status line at the bottom of the whole window) and compose a filter which will choose the same packets which the now-missing <code>alc.lct</code> used to choose.</p></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Dec '15, 02:11</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-48601" class="comments-container"></div><div id="comment-tools-48601" class="comment-tools"></div><div class="clear"></div><div id="comment-48601-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

