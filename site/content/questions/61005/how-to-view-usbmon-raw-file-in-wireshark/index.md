+++
type = "question"
title = "how to view usbmon raw file in Wireshark?"
description = '''I have captured the usb raw data in linux/ubuntu using USBMon: # cat /sys/kernel/debug/usb/usbmon/1u &amp;gt; /tmp/1.mon.out which tool can convert the file (1.mon.out) to open in Wireshark?'''
date = "2017-04-24T05:24:00Z"
lastmod = "2017-04-24T05:24:00Z"
weight = 61005
keywords = [ "usbmon" ]
aliases = [ "/questions/61005" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to view usbmon raw file in Wireshark?](/questions/61005/how-to-view-usbmon-raw-file-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61005-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61005-score" class="post-score" title="current number of votes">0</div><span id="post-61005-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured the usb raw data in linux/ubuntu using USBMon:</p><p><code># cat /sys/kernel/debug/usb/usbmon/1u &gt; /tmp/1.mon.out</code></p><p>which tool can convert the file (1.mon.out) to open in Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usbmon" rel="tag" title="see questions tagged &#39;usbmon&#39;">usbmon</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '17, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/ca400298b8385318d9ac844a64e8a40e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sghorai&#39;s gravatar image" /><p><span>sghorai</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sghorai has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Apr '17, 10:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61005" class="comments-container"></div><div id="comment-tools-61005" class="comment-tools"></div><div class="clear"></div><div id="comment-61005-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

