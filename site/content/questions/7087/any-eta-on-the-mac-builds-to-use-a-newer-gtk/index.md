+++
type = "question"
title = "Any ETA on the Mac builds to use a newer GTK+?"
description = '''For us spending a lot of time in Wireshark on Lion, the lines disappearing is a huge pain. Thanks!'''
date = "2011-10-26T12:29:00Z"
lastmod = "2011-10-26T12:29:00Z"
weight = 7087
keywords = [ "osx" ]
aliases = [ "/questions/7087" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Any ETA on the Mac builds to use a newer GTK+?](/questions/7087/any-eta-on-the-mac-builds-to-use-a-newer-gtk)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7087-score" class="post-score" title="current number of votes">0</div><span id="post-7087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For us spending a lot of time in Wireshark on Lion, the lines disappearing is a huge pain. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '11, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/bb2a6c0e2fc64197079b42004de7bcdc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="administraitor&#39;s gravatar image" /><p><span>administraitor</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="administraitor has no accepted answers">0%</span></p></div></div><div id="comments-container-7087" class="comments-container"></div><div id="comment-tools-7087" class="comment-tools"></div><div class="clear"></div><div id="comment-7087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

