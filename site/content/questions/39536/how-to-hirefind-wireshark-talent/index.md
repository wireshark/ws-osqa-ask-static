+++
type = "question"
title = "How to hire/find Wireshark talent??"
description = '''I would like to hire a Wireshark expert to analyze capture files. I have posted this work on Guru.com, Freelancer.com and Craigslist but have had no responses. I&#x27;m starting to wonder if there are actually any W-shark experts out there. Can anyone suggest where I might find Wireshark talent? THANKS! ...'''
date = "2015-02-01T10:24:00Z"
lastmod = "2015-02-06T00:19:00Z"
weight = 39536
keywords = [ "hire", "talent", "wireshark" ]
aliases = [ "/questions/39536" ]
osqa_answers = 4
osqa_accepted = false
+++

<div class="headNormal">

# [How to hire/find Wireshark talent??](/questions/39536/how-to-hirefind-wireshark-talent)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39536-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39536-score" class="post-score" title="current number of votes">0</div><span id="post-39536-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to hire a Wireshark expert to analyze capture files. I have posted this work on Guru.com, Freelancer.com and Craigslist but have had no responses. I'm starting to wonder if there are actually any W-shark experts out there.</p><p>Can anyone suggest where I might find Wireshark talent? THANKS! -Mike</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hire" rel="tag" title="see questions tagged &#39;hire&#39;">hire</span> <span class="post-tag tag-link-talent" rel="tag" title="see questions tagged &#39;talent&#39;">talent</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '15, 10:24</strong></p><img src="https://secure.gravatar.com/avatar/a5b54cd180fdffc99cc49439da933823?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mem5449&#39;s gravatar image" /><p><span>mem5449</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mem5449 has no accepted answers">0%</span></p></div></div><div id="comments-container-39536" class="comments-container"></div><div id="comment-tools-39536" class="comment-tools"></div><div class="clear"></div><div id="comment-39536-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

4 Answers:

</div>

</div>

<span id="39539"></span>

<div id="answer-container-39539" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39539-score" class="post-score" title="current number of votes">1</div><span id="post-39539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you need a little assistance in the analysis, you can post your (specific) question(s) here and there are quite a few people here that can assist you.</p><p>If you are looking for a freelance protocol analyst, there are a couple of people (myself included) that provide troubleshooting services. Maybe you can enlighten us a little bit more on the problem you're facing?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '15, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-39539" class="comments-container"><span id="39649"></span><div id="comment-39649" class="comment"><div id="post-39649-score" class="comment-score"></div><div class="comment-text"><p>Hi SYN-bit,</p><p>THANKS for your answer! I was originally hesitant to ask for help just cause most tech forums I've visited over the last few years, members seem to flame folks like me saying I'm asking them to 'do my work for me for free'. Which I am definitely not. And THANK you again, especially for being civil.</p><p>I have 2 clients I been trying to help solve their network issues. One has a small network that comes to a CRAWL intermittently (1 WinServ2003 &amp; 5 Win7 PCs) and the other is a med/large network (exchange server, 60 PCs) experiencing intermittent POP and SMTP errors. I have captures for both. Each is a series of 30MB files. With my very limited WS experience I have found the 'slow network' captures show tremendous SMB protocol activity (WS gens a 30MB capture in 4minutes!!) The 'POP/SMTP' captures show some KERBEROS errors.</p><p>I'd be thrilled if someone would like to take a look at the captures, but again, I'm not looking for anyone to hold my hand and am quite willing to hire a WS guru. Actually, trying to find my way thru 'WS Network Analysis by Chappell' as I type this. :(</p><p>Can I attach or upload such LARGE files on this forum (30MB each)? Or, should I attempt to deliver them to someone (here) via another method?</p><p>-Mike</p></div><div id="comment-39649-info" class="comment-info"><span class="comment-age">(04 Feb '15, 15:23)</span> <span class="comment-user userinfo">mem5449</span></div></div><span id="39650"></span><div id="comment-39650" class="comment"><div id="post-39650-score" class="comment-score"></div><div class="comment-text"><p>For the captures, if they don't have confidential data you can upload them here and post the URLs in the forum: <a href="https://appliance.cloudshark.org/upload/">https://appliance.cloudshark.org/upload/</a></p></div><div id="comment-39650-info" class="comment-info"><span class="comment-age">(04 Feb '15, 16:09)</span> <span class="comment-user userinfo">Quadratic</span></div></div><span id="39678"></span><div id="comment-39678" class="comment"><div id="post-39678-score" class="comment-score"></div><div class="comment-text"><p><span>@quadratic</span>, I was going to correct you on the link, but instead, I stand corrected, thx :-)</p></div><div id="comment-39678-info" class="comment-info"><span class="comment-age">(06 Feb '15, 00:19)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-39539" class="comment-tools"></div><div class="clear"></div><div id="comment-39539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="39537"></span>

<div id="answer-container-39537" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39537-score" class="post-score" title="current number of votes">0</div><span id="post-39537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here, and maybe the <a href="https://www.wireshark.org/lists/">wireshark users mail list</a> would be a good start.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '15, 12:16</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-39537" class="comments-container"></div><div id="comment-tools-39537" class="comment-tools"></div><div class="clear"></div><div id="comment-39537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="39538"></span>

<div id="answer-container-39538" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39538-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39538-score" class="post-score" title="current number of votes">0</div><span id="post-39538-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>While protocol analysis in general is its own skill, a lot of the methodology is based on the protocol(s) and technology(s) in question. Also, since usually protocol analysis is just a small part of one person's job (eg: a network administrator), few in the field will use it every day as a dedicated profession.</p><p>Personally if I wanted to hire a protocol analyst for just that one role, I'd start by looking at all the talent in the analytics companies, and those who develop network probe solutions that do that sort of analyzing (eg: Tektronix). Though, like I said it would depend a bit on exactly what sort of protocols you need analyzing.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '15, 12:41</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Feb '15, 12:42</strong> </span></p></div></div><div id="comments-container-39538" class="comments-container"></div><div id="comment-tools-39538" class="comment-tools"></div><div class="clear"></div><div id="comment-39538-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="39540"></span>

<div id="answer-container-39540" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39540-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39540-score" class="post-score" title="current number of votes">0</div><span id="post-39540-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There has been a discussion about this kind of thing here:</p><p><a href="https://ask.wireshark.org/questions/14264/where-can-i-go-for-paid-networkprotocol-analysis-services-and-consultancy">https://ask.wireshark.org/questions/14264/where-can-i-go-for-paid-networkprotocol-analysis-services-and-consultancy</a></p><p>I guess SYN-bit and Kurt Knochner (see <a href="https://ask.wireshark.org/users/">user list</a>) still do this. Otherwise you may find someone at the <a href="http://www.garlandtechnology.com/experts">Garland Technologies Expert corner</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '15, 14:03</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Feb '15, 14:04</strong> </span></p></div></div><div id="comments-container-39540" class="comments-container"><span id="39541"></span><div id="comment-39541" class="comment"><div id="post-39541-score" class="comment-score"></div><div class="comment-text"><p>Nope, I'm out ;-) <span>@SYN-bit</span> would cretainly be the best choice for the OP.</p></div><div id="comment-39541-info" class="comment-info"><span class="comment-age">(01 Feb '15, 14:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-39540" class="comment-tools"></div><div class="clear"></div><div id="comment-39540-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

