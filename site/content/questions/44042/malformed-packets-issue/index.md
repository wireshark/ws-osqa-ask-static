+++
type = "question"
title = "malformed packets issue"
description = '''Hi guys. Every time I scan it picks everything up as a malformed packet. I have no idea how to get round this. I think this is what is then giving me the knock on effect of not being able to use the filters. Filter http:// for example and it finds nothing. Any help greatly appreciated. I have tried ...'''
date = "2015-07-10T03:03:00Z"
lastmod = "2015-07-10T05:08:00Z"
weight = 44042
keywords = [ "packets", "malformed" ]
aliases = [ "/questions/44042" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [malformed packets issue](/questions/44042/malformed-packets-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44042-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44042-score" class="post-score" title="current number of votes">0</div><span id="post-44042-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys. Every time I scan it picks everything up as a malformed packet. I have no idea how to get round this. I think this is what is then giving me the knock on effect of not being able to use the filters. Filter http:// for example and it finds nothing. Any help greatly appreciated. I have tried 1.03 and the latest versions.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '15, 03:03</strong></p><img src="https://secure.gravatar.com/avatar/ddf030cfcbf1d5c8423934cc1e88b171?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scooby666&#39;s gravatar image" /><p><span>scooby666</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scooby666 has no accepted answers">0%</span></p></div></div><div id="comments-container-44042" class="comments-container"><span id="44046"></span><div id="comment-44046" class="comment"><div id="post-44046-score" class="comment-score"></div><div class="comment-text"><p>Not knowing what you 'scan', who 'it' and 'everything' is, it is hard to give an answer to - hm exactly what? - question. Can you upload a sample capture to cloudshark.org or at least provide a screenshot ?</p><ul><li>And... http:// is not a valid dislay filter. try http or tcp contains "http://" ...</li></ul></div><div id="comment-44046-info" class="comment-info"><span class="comment-age">(10 Jul '15, 03:50)</span> <span class="comment-user userinfo">mrEEde</span></div></div><span id="44048"></span><div id="comment-44048" class="comment"><div id="post-44048-score" class="comment-score"></div><div class="comment-text"><p>And what is 1.03? If you mean Wireshark, that's a very very old version.</p></div><div id="comment-44048-info" class="comment-info"><span class="comment-age">(10 Jul '15, 05:08)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-44042" class="comment-tools"></div><div class="clear"></div><div id="comment-44042-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

