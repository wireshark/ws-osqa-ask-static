+++
type = "question"
title = "Validation link problems"
description = '''The validation link that is sent results in a 404. Is this still under development?'''
date = "2010-09-09T15:58:00Z"
lastmod = "2010-09-15T15:32:00Z"
weight = 17
keywords = [ "meta" ]
aliases = [ "/questions/17" ]
osqa_answers = 4
osqa_accepted = false
+++

<div class="headNormal">

# [Validation link problems](/questions/17/validation-link-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17-score" class="post-score" title="current number of votes">0</div><span id="post-17-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The validation link that is sent results in a 404. Is this still under development?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '10, 15:58</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Sep '10, 17:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-17" class="comments-container"></div><div id="comment-tools-17" class="comment-tools"></div><div class="clear"></div><div id="comment-17-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

4 Answers:

</div>

</div>

<span id="120"></span>

<div id="answer-container-120" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-120-score" class="post-score" title="current number of votes">0</div><span id="post-120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have successfully validated my e-mail address twice... Seems to work fine now!</p><p>(I did have to change my e-mail address from <span class="__cf_email__" data-cfemail="c3b0a2a8a6eda1afaca883909a8deea1aab7edadaf">[email protected]</span> to <span class="__cf_email__" data-cfemail="1d6e7c7678337f7172765d6e6473307f7469337371">[email protected]</span> though to import my gravatar, as the gravatar site lowercased my address)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 15:32</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-120" class="comments-container"></div><div id="comment-tools-120" class="comment-tools"></div><div class="clear"></div><div id="comment-120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18"></span>

<div id="answer-container-18" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18-score" class="post-score" title="current number of votes">0</div><span id="post-18-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you change your email address before validating? I did that in one of the test accounts and its validation URL does the same thing.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '10, 16:18</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-18" class="comments-container"></div><div id="comment-tools-18" class="comment-tools"></div><div class="clear"></div><div id="comment-18-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20"></span>

<div id="answer-container-20" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20-score" class="post-score" title="current number of votes">0</div><span id="post-20-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Nope... although it was changed for me from "<span class="__cf_email__" data-cfemail="5b283a303e75393734301b0802157639322f753537">[email protected]</span>" to "<span class="__cf_email__" data-cfemail="4033212b256e222c2f2b0033392e6d2229346e2e2c">[email protected]</span>", might that be the issue?</p><p>Update: Oops... nope... that was done at the gravatar site. Not here, it still shows "<span class="__cf_email__" data-cfemail="c2b1a3a9a7eca0aeada982919b8cefa0abb6ecacae">[email protected]</span>"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '10, 16:29</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Sep '10, 16:31</strong> </span></p></div></div><div id="comments-container-20" class="comments-container"><span id="21"></span><div id="comment-21" class="comment"><div id="post-21-score" class="comment-score"></div><div class="comment-text"><p>That would be it. The validation link is a hash based on the email address. After experimenting with the test account it looks like you can wait a day for the validation hash to expire, then try again. I had to load the link URL twice.</p></div><div id="comment-21-info" class="comment-info"><span class="comment-age">(09 Sep '10, 16:33)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="22"></span><div id="comment-22" class="comment"><div id="post-22-score" class="comment-score"></div><div class="comment-text"><p>I deleted your validation hash in the database. Can you try again?</p></div><div id="comment-22-info" class="comment-info"><span class="comment-age">(09 Sep '10, 16:35)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="38"></span><div id="comment-38" class="comment"><div id="post-38-score" class="comment-score"></div><div class="comment-text"><p>Gerald, I have validated my e-mail address successfully now.</p></div><div id="comment-38-info" class="comment-info"><span class="comment-age">(12 Sep '10, 22:48)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-20" class="comment-tools"></div><div class="clear"></div><div id="comment-20-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="34"></span>

<div id="answer-container-34" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34-score" class="post-score" title="current number of votes">0</div><span id="post-34-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Also got 404 on validation attempt.</p><p>Any ideas...?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Sep '10, 21:43</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-34" class="comments-container"><span id="36"></span><div id="comment-36" class="comment"><div id="post-36-score" class="comment-score"></div><div class="comment-text"><p>It looks like the validation code isn't fully-cooked yet. In order to successfully validate you must click "Send me a validation link", then load the validation URL within 24 hours.</p><p>Also, each time you click "Send me a validation link" a new and different link is generated, rendering any previous links invalid.</p></div><div id="comment-36-info" class="comment-info"><span class="comment-age">(12 Sep '10, 16:43)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-34" class="comment-tools"></div><div class="clear"></div><div id="comment-34-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

