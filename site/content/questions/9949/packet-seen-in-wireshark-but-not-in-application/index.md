+++
type = "question"
title = "Packet seen in wireshark but not in application."
description = '''Hi all, I am sending a UDP request using my application and response is also sent by the destination. In the host where I run the application, I am running wireshark and in that I found that the response is received. But this data is not received in my application. Can you suggest me what all could ...'''
date = "2012-04-04T21:01:00Z"
lastmod = "2012-04-04T21:01:00Z"
weight = 9949
keywords = [ "packetdrop", "packet" ]
aliases = [ "/questions/9949" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Packet seen in wireshark but not in application.](/questions/9949/packet-seen-in-wireshark-but-not-in-application)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9949-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9949-score" class="post-score" title="current number of votes">0</div><span id="post-9949-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I am sending a UDP request using my application and response is also sent by the destination. In the host where I run the application, I am running wireshark and in that I found that the response is received. But this data is not received in my application. Can you suggest me what all could have led to this issue</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packetdrop" rel="tag" title="see questions tagged &#39;packetdrop&#39;">packetdrop</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '12, 21:01</strong></p><img src="https://secure.gravatar.com/avatar/80d2e7d7d210a95231c538d4dfb67659?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anantha&#39;s gravatar image" /><p><span>anantha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anantha has no accepted answers">0%</span></p></div></div><div id="comments-container-9949" class="comments-container"></div><div id="comment-tools-9949" class="comment-tools"></div><div class="clear"></div><div id="comment-9949-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

