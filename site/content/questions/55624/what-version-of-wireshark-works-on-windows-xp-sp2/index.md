+++
type = "question"
title = "What version of Wireshark works on Windows XP SP2?"
description = '''Dears, What version of Wireshark works on Windows XP SP2? Thank you'''
date = "2016-09-18T07:22:00Z"
lastmod = "2016-09-18T10:44:00Z"
weight = 55624
keywords = [ "win", "xp", "sp2" ]
aliases = [ "/questions/55624" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What version of Wireshark works on Windows XP SP2?](/questions/55624/what-version-of-wireshark-works-on-windows-xp-sp2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55624-score" class="post-score" title="current number of votes">0</div><span id="post-55624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dears,</p><p>What version of Wireshark works on Windows XP SP2?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-win" rel="tag" title="see questions tagged &#39;win&#39;">win</span> <span class="post-tag tag-link-xp" rel="tag" title="see questions tagged &#39;xp&#39;">xp</span> <span class="post-tag tag-link-sp2" rel="tag" title="see questions tagged &#39;sp2&#39;">sp2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '16, 07:22</strong></p><img src="https://secure.gravatar.com/avatar/31cece089ca692ee08ca6447b355b21f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cristianf&#39;s gravatar image" /><p><span>cristianf</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cristianf has no accepted answers">0%</span></p></div></div><div id="comments-container-55624" class="comments-container"></div><div id="comment-tools-55624" class="comment-tools"></div><div class="clear"></div><div id="comment-55624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55627"></span>

<div id="answer-container-55627" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55627-score" class="post-score" title="current number of votes">0</div><span id="post-55627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As shown on the Wireshark Wiki <a href="https://wiki.wireshark.org/Development/LifeCycle">LifeCycle</a> page, the last version to support XP is 1.10, and the last release of that was 1.10.14, available for download <a href="https://www.wireshark.org/download/win32/all-versions/Wireshark-win32-1.10.14.exe">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Sep '16, 08:38</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-55627" class="comments-container"><span id="55631"></span><div id="comment-55631" class="comment"><div id="post-55631-score" class="comment-score"></div><div class="comment-text"><p>Note that Wireshark 1.12.13 was working fine on Windows XP as far as I can remember, even if it was no more officially supported. You might give it a try and fallback to 1.10.14 as Graham suggested in case of issue.</p></div><div id="comment-55631-info" class="comment-info"><span class="comment-age">(18 Sep '16, 10:44)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-55627" class="comment-tools"></div><div class="clear"></div><div id="comment-55627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

