+++
type = "question"
title = "Can I find out API changes from 1.10.5 to 1.12.4?"
description = '''I have a dissector that was programmed in C++ and works under Wireshark 1.10.5. I want to bring it up to 1.12.4, but it seems a number of the API calls are no longer valid (several deprecated ones I can manage). I&#x27;m not sure if it would help to post the calls here as well.'''
date = "2015-04-21T17:03:00Z"
lastmod = "2015-04-21T17:03:00Z"
weight = 41650
keywords = [ "api", "dissector" ]
aliases = [ "/questions/41650" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I find out API changes from 1.10.5 to 1.12.4?](/questions/41650/can-i-find-out-api-changes-from-1105-to-1124)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41650-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41650-score" class="post-score" title="current number of votes">0</div><span id="post-41650-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a dissector that was programmed in C++ and works under Wireshark 1.10.5. I want to bring it up to 1.12.4, but it seems a number of the API calls are no longer valid (several deprecated ones I can manage). I'm not sure if it would help to post the calls here as well.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-api" rel="tag" title="see questions tagged &#39;api&#39;">api</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Apr '15, 17:03</strong></p><img src="https://secure.gravatar.com/avatar/ec1e1b4494bdd1cb7abc1d914d56847d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tvisitor&#39;s gravatar image" /><p><span>tvisitor</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tvisitor has no accepted answers">0%</span></p></div></div><div id="comments-container-41650" class="comments-container"></div><div id="comment-tools-41650" class="comment-tools"></div><div class="clear"></div><div id="comment-41650-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

