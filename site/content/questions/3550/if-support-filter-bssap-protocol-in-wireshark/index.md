+++
type = "question"
title = "If support filter BSSAP+ protocol in Wireshark?"
description = '''I checked on Wirshark support protocols for BSSAP+ protocol and got below information:  http://wiki.wireshark.org/GsmProtocolFamily This is an optional interface for PS/CS interoperability. Using the Gs interface it is possible to perform combined GPRS/IMSI attaches, combined location updates and pa...'''
date = "2011-04-18T01:09:00Z"
lastmod = "2011-04-18T01:09:00Z"
weight = 3550
keywords = [ "protocol" ]
aliases = [ "/questions/3550" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [If support filter BSSAP+ protocol in Wireshark?](/questions/3550/if-support-filter-bssap-protocol-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3550-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3550-score" class="post-score" title="current number of votes">0</div><span id="post-3550-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I checked on Wirshark support protocols for BSSAP+ protocol and got below information: http://wiki.wireshark.org/GsmProtocolFamily This is an optional interface for PS/CS interoperability. Using the Gs interface it is possible to perform combined GPRS/IMSI attaches, combined location updates and paging the subscriber using PS facilities. The protocol is BSSAP+ specified in 3GPP TS 29.016 ( supported by Wireshark on ssn 98)</p><p>And I checked http://www.wireshark.org/docs/dfref/b/bssap.html I saw two elements include bssap_plus, so does it means BSSAP+ message decode and filter support is embedded in BSSAP protocol??</p><p>Can you give a sample for how to filter BSSAP+?</p><p>Thanks, Justin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '11, 01:09</strong></p><img src="https://secure.gravatar.com/avatar/08eb7dabc2e68ae5aa6935a1eec4f6ed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="justinxu78&#39;s gravatar image" /><p><span>justinxu78</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="justinxu78 has no accepted answers">0%</span></p></div></div><div id="comments-container-3550" class="comments-container"></div><div id="comment-tools-3550" class="comment-tools"></div><div class="clear"></div><div id="comment-3550-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

