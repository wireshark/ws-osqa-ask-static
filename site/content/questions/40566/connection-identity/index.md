+++
type = "question"
title = "Connection Identity"
description = '''how do I find out who a connection is formatt: dsl-device.att.net android-xxxx00x0000x0att.net'''
date = "2015-03-14T17:11:00Z"
lastmod = "2015-03-19T14:38:00Z"
weight = 40566
keywords = [ "wireshark" ]
aliases = [ "/questions/40566" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Connection Identity](/questions/40566/connection-identity)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40566-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40566-score" class="post-score" title="current number of votes">0</div><span id="post-40566-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do I find out who a connection is formatt: dsl-device.att.net android-xxxx00x0000x0att.net</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '15, 17:11</strong></p><img src="https://secure.gravatar.com/avatar/42bf9c2abbaae16a80624b6527c2ed45?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="123N321&#39;s gravatar image" /><p><span>123N321</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="123N321 has no accepted answers">0%</span></p></div></div><div id="comments-container-40566" class="comments-container"></div><div id="comment-tools-40566" class="comment-tools"></div><div class="clear"></div><div id="comment-40566-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40698"></span>

<div id="answer-container-40698" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40698-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40698-score" class="post-score" title="current number of votes">0</div><span id="post-40698-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I assume you are asking for the person who was assigned the IP address, linked to the name you mentioned, which looks like a user on a mobile phone using ATT's internet connection. If that't the case, you won't be able to figure out the persons identity unless ATT is willing to disclose that information. Try to call their hotline and ask.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Mar '15, 14:38</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-40698" class="comments-container"></div><div id="comment-tools-40698" class="comment-tools"></div><div class="clear"></div><div id="comment-40698-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

