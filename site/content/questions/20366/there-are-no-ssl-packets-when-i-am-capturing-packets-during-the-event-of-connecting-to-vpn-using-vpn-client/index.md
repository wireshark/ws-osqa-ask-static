+++
type = "question"
title = "There are no SSL packets when I am capturing packets during the event of connecting to vpn using vpn client."
description = '''Hello; There are no SSL packets when I am capturing packets during the event of connecting to vpn using vpn client. Is this obvious? I am not sure if it is. What i did is - With active internet connection only (no browser open doing nothing), I started wireshark capture. Then I connect to (my school...'''
date = "2013-04-11T20:55:00Z"
lastmod = "2013-04-11T23:01:00Z"
weight = 20366
keywords = [ "ssl", "vpn" ]
aliases = [ "/questions/20366" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [There are no SSL packets when I am capturing packets during the event of connecting to vpn using vpn client.](/questions/20366/there-are-no-ssl-packets-when-i-am-capturing-packets-during-the-event-of-connecting-to-vpn-using-vpn-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20366-score" class="post-score" title="current number of votes">0</div><span id="post-20366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello;</p><p>There are no SSL packets when I am capturing packets during the event of connecting to vpn using vpn client. Is this obvious? I am not sure if it is. What i did is - With active internet connection only (no browser open doing nothing), I started wireshark capture. Then I connect to (my school) vpn using vpn client and then I stopped wireshark capture. I could see no SSL packets. Whats the reason for that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '13, 20:55</strong></p><img src="https://secure.gravatar.com/avatar/1bdc9d6a116444d791abdf0da2515e43?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CDK&#39;s gravatar image" /><p><span>CDK</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CDK has no accepted answers">0%</span></p></div></div><div id="comments-container-20366" class="comments-container"></div><div id="comment-tools-20366" class="comment-tools"></div><div class="clear"></div><div id="comment-20366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20367"></span>

<div id="answer-container-20367" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20367-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20367-score" class="post-score" title="current number of votes">0</div><span id="post-20367-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are different kinds of VPN. SSL-VPN being one of them, but IPsec based VPN's are still around much. What kind of VPN client are you using? What kind of packets do you see when connected with VPN? If you see a couple of ISAKMP packets followed by a lot of ESP packets, then your client uses IPsec.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Apr '13, 23:01</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-20367" class="comments-container"></div><div id="comment-tools-20367" class="comment-tools"></div><div class="clear"></div><div id="comment-20367-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

