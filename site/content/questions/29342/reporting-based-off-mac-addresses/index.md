+++
type = "question"
title = "Reporting based off mac addresses"
description = '''Wireshark newbie - I want to see which mac addresses are hogging the traffic in our network. Is there a way to get a report that gives traffic stats over time grouped by mac address. '''
date = "2014-01-31T04:12:00Z"
lastmod = "2014-01-31T05:33:00Z"
weight = 29342
keywords = [ "mac-address" ]
aliases = [ "/questions/29342" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Reporting based off mac addresses](/questions/29342/reporting-based-off-mac-addresses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29342-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29342-score" class="post-score" title="current number of votes">0</div><span id="post-29342-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark newbie - I want to see which mac addresses are hogging the traffic in our network. Is there a way to get a report that gives traffic stats over time grouped by mac address.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '14, 04:12</strong></p><img src="https://secure.gravatar.com/avatar/a969ebb2c8f1f3d4049afaaee0c8a7e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mannymisc&#39;s gravatar image" /><p><span>mannymisc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mannymisc has no accepted answers">0%</span></p></div></div><div id="comments-container-29342" class="comments-container"></div><div id="comment-tools-29342" class="comment-tools"></div><div class="clear"></div><div id="comment-29342-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29347"></span>

<div id="answer-container-29347" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29347-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29347-score" class="post-score" title="current number of votes">2</div><span id="post-29347-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, go to:</p><blockquote><p>Statistics -&gt; Conversations -&gt; Ethernet [a Tab]</p></blockquote><p>Then sort the entries according to the column "Bytes".</p><p>Hint: Uncheck the option 'Name resolution' (lower, left corner) if you want to see the 'raw' (untranslated) MAC addresses.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jan '14, 05:33</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-29347" class="comments-container"></div><div id="comment-tools-29347" class="comment-tools"></div><div class="clear"></div><div id="comment-29347-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

