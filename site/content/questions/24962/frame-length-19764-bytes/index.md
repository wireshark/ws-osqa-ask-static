+++
type = "question"
title = "Frame length = 19764 Bytes"
description = '''Hello, I though that frame size unless classified as jumbo frames were no larger than 1500 bytes. I have frame lengths in my capture as large as 19764 bytes. None of the servers involved are set to send jumbo frames. How does that happen ? Thanks, Robbie'''
date = "2013-09-19T11:15:00Z"
lastmod = "2013-09-19T11:30:00Z"
weight = 24962
keywords = [ "frame", "length" ]
aliases = [ "/questions/24962" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Frame length = 19764 Bytes](/questions/24962/frame-length-19764-bytes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24962-score" class="post-score" title="current number of votes">1</div><span id="post-24962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I though that frame size unless classified as jumbo frames were no larger than 1500 bytes. I have frame lengths in my capture as large as 19764 bytes.</p><p>None of the servers involved are set to send jumbo frames. How does that happen ?</p><p>Thanks, Robbie</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-length" rel="tag" title="see questions tagged &#39;length&#39;">length</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '13, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/f9286d33942eef728d42e015a308bb89?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Robbie%20S&#39;s gravatar image" /><p><span>Robbie S</span><br />
<span class="score" title="26 reputation points">26</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Robbie S has no accepted answers">0%</span></p></div></div><div id="comments-container-24962" class="comments-container"></div><div id="comment-tools-24962" class="comment-tools"></div><div class="clear"></div><div id="comment-24962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24964"></span>

<div id="answer-container-24964" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24964-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24964-score" class="post-score" title="current number of votes">3</div><span id="post-24964-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the <a href="http://wiki.wireshark.org/CaptureSetup/Offloading">Wireshark Offloading wiki page</a> and also Jim Aragon's recent answer to <a href="http://ask.wireshark.org/questions/24699/tcp-packet-length-was-much-greater-than-mtu">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Sep '13, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-24964" class="comments-container"></div><div id="comment-tools-24964" class="comment-tools"></div><div class="clear"></div><div id="comment-24964-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

