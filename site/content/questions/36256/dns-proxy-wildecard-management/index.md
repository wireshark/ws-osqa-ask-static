+++
type = "question"
title = "DNS proxy : wildecard management"
description = '''How could we filter the traffic with wireshark to see how DNS proxy manage the DNS wildecard packets ? Thank you'''
date = "2014-09-12T01:31:00Z"
lastmod = "2014-09-12T01:31:00Z"
weight = 36256
keywords = [ "wildcard", "proxy", "dns" ]
aliases = [ "/questions/36256" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [DNS proxy : wildecard management](/questions/36256/dns-proxy-wildecard-management)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36256-score" class="post-score" title="current number of votes">0</div><span id="post-36256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How could we filter the traffic with wireshark to see how DNS proxy manage the DNS wildecard packets ? Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wildcard" rel="tag" title="see questions tagged &#39;wildcard&#39;">wildcard</span> <span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '14, 01:31</strong></p><img src="https://secure.gravatar.com/avatar/66321baa0c6140c4468afd6a85b3fb70?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amina&#39;s gravatar image" /><p><span>Amina</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amina has no accepted answers">0%</span></p></div></div><div id="comments-container-36256" class="comments-container"></div><div id="comment-tools-36256" class="comment-tools"></div><div class="clear"></div><div id="comment-36256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

