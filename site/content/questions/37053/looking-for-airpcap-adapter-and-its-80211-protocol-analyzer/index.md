+++
type = "question"
title = "Looking for AirPcap adapter and its 802.11 protocol analyzer"
description = '''Hi, I want to buy the AirPcap adapter as this model: AirPcap Classic USB 802.11b/g Three-Pack (capture only). I also need a analyzer software to capture the TCP and UDP rather than 802.11 protocol from AirPcap. Which one of wireshark listed software supports it?  (http://www.cacetech.com/products/ca...'''
date = "2014-10-15T03:16:00Z"
lastmod = "2014-10-15T04:51:00Z"
weight = 37053
keywords = [ "airpcap" ]
aliases = [ "/questions/37053" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Looking for AirPcap adapter and its 802.11 protocol analyzer](/questions/37053/looking-for-airpcap-adapter-and-its-80211-protocol-analyzer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37053-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37053-score" class="post-score" title="current number of votes">0</div><span id="post-37053-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I want to buy the AirPcap adapter as this model: AirPcap Classic USB 802.11b/g Three-Pack (capture only).</p><p>I also need a analyzer software to capture the TCP and UDP rather than 802.11 protocol from AirPcap.</p><p>Which one of wireshark listed software supports it? (<a href="http://www.cacetech.com/products/catalog/index.php">http://www.cacetech.com/products/catalog/index.php</a> and <a href="http://www.winpcap.org/">http://www.winpcap.org/</a> )</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '14, 03:16</strong></p><img src="https://secure.gravatar.com/avatar/1a11f755bc9be9cb220cb55c1b432144?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Stannis%20Lin&#39;s gravatar image" /><p><span>Stannis Lin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Stannis Lin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '14, 03:37</strong> </span></p></div></div><div id="comments-container-37053" class="comments-container"></div><div id="comment-tools-37053" class="comment-tools"></div><div class="clear"></div><div id="comment-37053-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37054"></span>

<div id="answer-container-37054" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37054-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37054-score" class="post-score" title="current number of votes">0</div><span id="post-37054-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the <a href="http://www.riverbed.com/products/performance-management-control/network-performance-management/wireless-packet-capture.html">AirPCap</a> web page,</p><blockquote><p><em>Riverbed AirPcap adapters offer an easy way to capture and analyze 802.11 wireless traffic, and include full integration with the popular <strong>Wireshark</strong> and Riverbed Cascade Pilot network analysis tools.</em></p></blockquote><p>and:</p><blockquote><p>AirPcap works with these popular packet analysis tools:</p><ul><li>Wireshark to capture, filter, display, analyze, dissect, and save WLAN packets, including data and management frame</li><li>SteelCentral™ Packet Analyzer for channel scanning, analysis, charting, and reporting</li><li>Kismet for network discovery</li><li>Aircrack-ng or Cain and Able for WEP cracking</li><li>Your custom tools for full flexibility</li></ul></blockquote></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '14, 03:20</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '14, 03:24</strong> </span></p></div></div><div id="comments-container-37054" class="comments-container"><span id="37055"></span><div id="comment-37055" class="comment"><div id="post-37055-score" class="comment-score"></div><div class="comment-text"><p>Which software can help me for TCP and UDP protocol capturing from AirPcap?</p></div><div id="comment-37055-info" class="comment-info"><span class="comment-age">(15 Oct '14, 03:34)</span> <span class="comment-user userinfo">Stannis Lin</span></div></div><span id="37058"></span><div id="comment-37058" class="comment"><div id="post-37058-score" class="comment-score"></div><div class="comment-text"><p>As per the above answer, the Wireshark suite will do that, see the <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN capture</a> wiki page for more info.</p><p>Depending on your actual WLAN environment, e.g. encryption using WEP\WPA\WPA2, you will have to configure Wireshark with the encryption key. See the <a href="http://wiki.wireshark.org/HowToDecrypt802.11">Decrypting 802.11</a> wiki page for more info.</p></div><div id="comment-37058-info" class="comment-info"><span class="comment-age">(15 Oct '14, 04:51)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-37054" class="comment-tools"></div><div class="clear"></div><div id="comment-37054-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

