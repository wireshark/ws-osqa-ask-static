+++
type = "question"
title = "protocol changing"
description = '''hi plz can i change the default protocol used for some application i.e i want to send a file to some one by default it will be supported using tcp protocol but i want to change this and make it UDP for example can i do this and how? by any way using wireshark or any otherway?'''
date = "2011-04-08T00:09:00Z"
lastmod = "2011-04-08T03:46:00Z"
weight = 3400
keywords = [ "changing", "protocol" ]
aliases = [ "/questions/3400" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [protocol changing](/questions/3400/protocol-changing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3400-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3400-score" class="post-score" title="current number of votes">0</div><span id="post-3400-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi plz can i change the default protocol used for some application i.e i want to send a file to some one by default it will be supported using tcp protocol but i want to change this and make it UDP for example can i do this and how? by any way using wireshark or any otherway?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-changing" rel="tag" title="see questions tagged &#39;changing&#39;">changing</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '11, 00:09</strong></p><img src="https://secure.gravatar.com/avatar/ac2a6402ceea6ed5f01ce0f11cf40c5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flower&#39;s gravatar image" /><p><span>flower</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flower has no accepted answers">0%</span></p></div></div><div id="comments-container-3400" class="comments-container"></div><div id="comment-tools-3400" class="comment-tools"></div><div class="clear"></div><div id="comment-3400-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3402"></span>

<div id="answer-container-3402" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3402-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3402-score" class="post-score" title="current number of votes">1</div><span id="post-3402-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Therefor you'll have to change the applications code. Not something that Wireshark can do for you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '11, 03:43</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3402" class="comments-container"><span id="3404"></span><div id="comment-3404" class="comment"><div id="post-3404-score" class="comment-score"></div><div class="comment-text"><p>how i can change the applications code? tell me a specific steps (with details) plz if u know?</p></div><div id="comment-3404-info" class="comment-info"><span class="comment-age">(08 Apr '11, 03:46)</span> <span class="comment-user userinfo">flower</span></div></div></div><div id="comment-tools-3402" class="comment-tools"></div><div class="clear"></div><div id="comment-3402-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

