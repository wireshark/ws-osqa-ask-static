+++
type = "question"
title = "Wireshark doesn&#x27;t show outbound traffic"
description = '''Hi All, My wireshark, for somne reason, is not showing the outbound traffic of m machine. I can see the answers coming from the destinations.  Do you have any idea?  Best Regards, Karl Müller'''
date = "2013-11-27T10:48:00Z"
lastmod = "2013-11-28T04:45:00Z"
weight = 27505
keywords = [ "outgoing", "outbound" ]
aliases = [ "/questions/27505" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark doesn't show outbound traffic](/questions/27505/wireshark-doesnt-show-outbound-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27505-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27505-score" class="post-score" title="current number of votes">0</div><span id="post-27505-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>My wireshark, for somne reason, is not showing the outbound traffic of m machine. I can see the answers coming from the destinations.</p><p>Do you have any idea?</p><p>Best Regards, Karl Müller</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outgoing" rel="tag" title="see questions tagged &#39;outgoing&#39;">outgoing</span> <span class="post-tag tag-link-outbound" rel="tag" title="see questions tagged &#39;outbound&#39;">outbound</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '13, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/8f3aa3e52980831cc363edfdaaaa3918?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Karl%20Rogers%20M%C3%BCller&#39;s gravatar image" /><p><span>Karl Rogers ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Karl Rogers Müller has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '14, 07:09</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-27505" class="comments-container"><span id="27506"></span><div id="comment-27506" class="comment"><div id="post-27506-score" class="comment-score"></div><div class="comment-text"><p>Wired or wireless, which OS?</p></div><div id="comment-27506-info" class="comment-info"><span class="comment-age">(27 Nov '13, 10:51)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="27508"></span><div id="comment-27508" class="comment"><div id="post-27508-score" class="comment-score"></div><div class="comment-text"><p>Windows 8 and it happens for both wired and wireless network adapters!</p></div><div id="comment-27508-info" class="comment-info"><span class="comment-age">(27 Nov '13, 11:58)</span> <span class="comment-user userinfo">Karl Rogers ...</span></div></div></div><div id="comment-tools-27505" class="comment-tools"></div><div class="clear"></div><div id="comment-27505-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27511"></span>

<div id="answer-container-27511" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27511-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27511-score" class="post-score" title="current number of votes">0</div><span id="post-27511-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see my answer to a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/27296/wireshark-only-capturing-incoming-packets">http://ask.wireshark.org/questions/27296/wireshark-only-capturing-incoming-packets</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '13, 12:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-27511" class="comments-container"><span id="27528"></span><div id="comment-27528" class="comment"><div id="post-27528-score" class="comment-score"></div><div class="comment-text"><p>OK Kurt,</p><p>Thanks for the support.</p><p>Regards, Karl</p></div><div id="comment-27528-info" class="comment-info"><span class="comment-age">(28 Nov '13, 03:22)</span> <span class="comment-user userinfo">Karl Rogers ...</span></div></div><span id="27529"></span><div id="comment-27529" class="comment"><div id="post-27529-score" class="comment-score"></div><div class="comment-text"><p>Good.</p><p>Hint: If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions.</p></div><div id="comment-27529-info" class="comment-info"><span class="comment-age">(28 Nov '13, 04:45)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27511" class="comment-tools"></div><div class="clear"></div><div id="comment-27511-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

