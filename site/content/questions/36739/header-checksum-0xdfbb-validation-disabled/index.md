+++
type = "question"
title = "Header checksum: 0xdfbb [validation disabled]"
description = '''I&#x27;m seeing &#x27;Header checksum: 0xdfbb [validation disabled]&#x27; on the IP header checksum. Is there any way to make sure that validation is not disabled? Thanks!'''
date = "2014-09-30T18:53:00Z"
lastmod = "2014-10-01T20:22:00Z"
weight = 36739
keywords = [ "header", "ipv4", "checksum" ]
aliases = [ "/questions/36739" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Header checksum: 0xdfbb \[validation disabled\]](/questions/36739/header-checksum-0xdfbb-validation-disabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36739-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36739-score" class="post-score" title="current number of votes">0</div><span id="post-36739-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm seeing 'Header checksum: 0xdfbb [validation disabled]' on the IP header checksum. Is there any way to make sure that validation is not disabled? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-ipv4" rel="tag" title="see questions tagged &#39;ipv4&#39;">ipv4</span> <span class="post-tag tag-link-checksum" rel="tag" title="see questions tagged &#39;checksum&#39;">checksum</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '14, 18:53</strong></p><img src="https://secure.gravatar.com/avatar/4784c5fb1a0142030d51a339706a456c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beldum&#39;s gravatar image" /><p><span>Beldum</span><br />
<span class="score" title="49 reputation points">49</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beldum has no accepted answers">0%</span></p></div></div><div id="comments-container-36739" class="comments-container"></div><div id="comment-tools-36739" class="comment-tools"></div><div class="clear"></div><div id="comment-36739-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36745"></span>

<div id="answer-container-36745" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36745-score" class="post-score" title="current number of votes">2</div><span id="post-36745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Beldum has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, just enable checking the checksum in the protocol preferences of IPv4. To do that, either click on the decode header line for IPv4 and use the pop up menu option to enable the check, or go to Edit -&gt; Preferences -&gt; Protocols -&gt; IPv4 -&gt; check "Validate the IPv4 checksum if possible"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Oct '14, 01:26</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-36745" class="comments-container"><span id="36766"></span><div id="comment-36766" class="comment"><div id="post-36766-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much Jasper</p></div><div id="comment-36766-info" class="comment-info"><span class="comment-age">(01 Oct '14, 20:22)</span> <span class="comment-user userinfo">Beldum</span></div></div></div><div id="comment-tools-36745" class="comment-tools"></div><div class="clear"></div><div id="comment-36745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

