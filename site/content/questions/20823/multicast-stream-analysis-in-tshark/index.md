+++
type = "question"
title = "Multicast stream analysis in tshark"
description = '''Hello to all. In wireshark GUI version we have &quot;Multicast stream analysis&quot;, is there convenient way to do the same task in tshark? '''
date = "2013-04-27T18:34:00Z"
lastmod = "2013-04-27T18:34:00Z"
weight = 20823
keywords = [ "console", "tshark" ]
aliases = [ "/questions/20823" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Multicast stream analysis in tshark](/questions/20823/multicast-stream-analysis-in-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20823-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20823-score" class="post-score" title="current number of votes">0</div><span id="post-20823-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello to all. In wireshark GUI version we have "Multicast stream analysis", is there convenient way to do the same task in tshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-console" rel="tag" title="see questions tagged &#39;console&#39;">console</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '13, 18:34</strong></p><img src="https://secure.gravatar.com/avatar/f248b4ad58e41bd8b756797bfbfb28d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="markotitel&#39;s gravatar image" /><p><span>markotitel</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="markotitel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Apr '13, 20:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-20823" class="comments-container"></div><div id="comment-tools-20823" class="comment-tools"></div><div class="clear"></div><div id="comment-20823-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

