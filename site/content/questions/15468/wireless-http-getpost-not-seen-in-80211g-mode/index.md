+++
type = "question"
title = "Wireless HTTP GET/POST not seen in 802.11g mode ?"
description = '''Hi  I am using Wireshark to sniff a wireless trafic with open authentification and I have a problem with viewing the GET/POST with 802.11g ? but when using 802.11b I can show them ! I use BTR 5 R3  ALF AWUS036H  any help ? thank you !'''
date = "2012-11-01T07:49:00Z"
lastmod = "2012-11-01T07:49:00Z"
weight = 15468
keywords = [ "wireless", "802.11", "post", "http", "get" ]
aliases = [ "/questions/15468" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless HTTP GET/POST not seen in 802.11g mode ?](/questions/15468/wireless-http-getpost-not-seen-in-80211g-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15468-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15468-score" class="post-score" title="current number of votes">0</div><span id="post-15468-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am using Wireshark to sniff a wireless trafic with open authentification and I have a problem with viewing the GET/POST with 802.11g ? but when using 802.11b I can show them !</p><p>I use BTR 5 R3 ALF AWUS036H</p><p>any help ? thank you !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-post" rel="tag" title="see questions tagged &#39;post&#39;">post</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-get" rel="tag" title="see questions tagged &#39;get&#39;">get</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Nov '12, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/feda0f7db49b5d86cd1ec5b15c6d1745?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Noury&#39;s gravatar image" /><p><span>Noury</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Noury has no accepted answers">0%</span></p></div></div><div id="comments-container-15468" class="comments-container"></div><div id="comment-tools-15468" class="comment-tools"></div><div class="clear"></div><div id="comment-15468-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

