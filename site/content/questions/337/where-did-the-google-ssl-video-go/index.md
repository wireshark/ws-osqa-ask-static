+++
type = "question"
title = "Where did the Google SSL video go?"
description = '''Very informative. Thanks'''
date = "2010-09-27T06:45:00Z"
lastmod = "2010-09-27T13:42:00Z"
weight = 337
keywords = [ "ssl", "google" ]
aliases = [ "/questions/337" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where did the Google SSL video go?](/questions/337/where-did-the-google-ssl-video-go)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-337-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-337-score" class="post-score" title="current number of votes">0</div><span id="post-337-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Very informative.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-google" rel="tag" title="see questions tagged &#39;google&#39;">google</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Sep '10, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/71d269f44ae38907ac658d545f8f74a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bitwatcher&#39;s gravatar image" /><p><span>bitwatcher</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bitwatcher has no accepted answers">0%</span></p></div></div><div id="comments-container-337" class="comments-container"><span id="343"></span><div id="comment-343" class="comment"><div id="post-343-score" class="comment-score">1</div><div class="comment-text"><p>Jaap is right - it's at www.wiresharkbook.com/coffee - along with several other videos on Wireshark.</p></div><div id="comment-343-info" class="comment-info"><span class="comment-age">(27 Sep '10, 13:42)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-337" class="comment-tools"></div><div class="clear"></div><div id="comment-337-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="339"></span>

<div id="answer-container-339" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-339-score" class="post-score" title="current number of votes">1</div><span id="post-339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you mean: <a href="http://www.wiresharkbook.com/coffee.html">here</a> ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Sep '10, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-339" class="comments-container"></div><div id="comment-tools-339" class="comment-tools"></div><div class="clear"></div><div id="comment-339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

