+++
type = "question"
title = "pcap from secondary remote host"
description = '''I have to connect to a remote access server that is the base connection to connect to other remote locations from. I do not have access directly to these devices from my local host. Is it possible to connect connect through the first ssh into a second ssh and running a tcpdump on the second host str...'''
date = "2015-05-19T22:49:00Z"
lastmod = "2015-05-19T22:49:00Z"
weight = 42568
keywords = [ "tcpdump", "pcap", "ssh" ]
aliases = [ "/questions/42568" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [pcap from secondary remote host](/questions/42568/pcap-from-secondary-remote-host)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42568-score" class="post-score" title="current number of votes">0</div><span id="post-42568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have to connect to a remote access server that is the base connection to connect to other remote locations from. I do not have access directly to these devices from my local host. Is it possible to connect connect through the first ssh into a second ssh and running a tcpdump on the second host streaming a pcap file all the way back to the localhost? Or maybe using iterm2 to log the terminal session and converting those files to pcap? Is any of this possible without creating a ssh tunnel to the second ssh connection?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-ssh" rel="tag" title="see questions tagged &#39;ssh&#39;">ssh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 May '15, 22:49</strong></p><img src="https://secure.gravatar.com/avatar/0c442a31c4708af10621f51bbcd27e3a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CFX83&#39;s gravatar image" /><p><span>CFX83</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CFX83 has no accepted answers">0%</span></p></div></div><div id="comments-container-42568" class="comments-container"></div><div id="comment-tools-42568" class="comment-tools"></div><div class="clear"></div><div id="comment-42568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

