+++
type = "question"
title = "Media packets detection"
description = '''Hi everyone! I&#x27;m trying to detect packets which content is video, image or audio/voice in order to make a processing later. So, how can I know which packets contains those kinds of data? For example, I&#x27;m seeing my webcam from another computer (in my home) and how can I know which packets contains my...'''
date = "2017-10-11T16:55:00Z"
lastmod = "2017-10-11T16:55:00Z"
weight = 63826
keywords = [ "media", "videostream", "audio", "videostreaming" ]
aliases = [ "/questions/63826" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Media packets detection](/questions/63826/media-packets-detection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63826-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63826-score" class="post-score" title="current number of votes">0</div><span id="post-63826-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone!</p><p>I'm trying to detect packets which content is video, image or audio/voice in order to make a processing later. So, how can I know which packets contains those kinds of data?</p><p>For example, I'm seeing my webcam from another computer (in my home) and how can I know which packets contains my webcam streaming?</p><p>I hope the question be clear..</p><p>Thanks!</p><p>PD: sorry for my english xD</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-media" rel="tag" title="see questions tagged &#39;media&#39;">media</span> <span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span> <span class="post-tag tag-link-audio" rel="tag" title="see questions tagged &#39;audio&#39;">audio</span> <span class="post-tag tag-link-videostreaming" rel="tag" title="see questions tagged &#39;videostreaming&#39;">videostreaming</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '17, 16:55</strong></p><img src="https://secure.gravatar.com/avatar/1d064e083cd0da32f6ecfc3b81673c23?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saran&#39;s gravatar image" /><p><span>saran</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saran has no accepted answers">0%</span></p></div></div><div id="comments-container-63826" class="comments-container"></div><div id="comment-tools-63826" class="comment-tools"></div><div class="clear"></div><div id="comment-63826-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

