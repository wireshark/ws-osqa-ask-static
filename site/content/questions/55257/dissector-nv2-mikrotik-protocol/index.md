+++
type = "question"
title = "Dissector NV2 Mikrotik Protocol"
description = '''Using wireless sniffer on Routerboard I can capture 802.11 protocol over TZSP protocol. But when NV2 (proprietary mikrotik wireless protocol) is active mixed among the data SSID is visible in some packets. Someone has a dissector for NV2? References: http://wiki.mikrotik.com/wiki/Manual:Interface/Wi...'''
date = "2016-08-31T17:50:00Z"
lastmod = "2016-08-31T17:50:00Z"
weight = 55257
keywords = [ "mikrotik", "802.11", "nv2" ]
aliases = [ "/questions/55257" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Dissector NV2 Mikrotik Protocol](/questions/55257/dissector-nv2-mikrotik-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55257-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55257-score" class="post-score" title="current number of votes">0</div><span id="post-55257-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using wireless sniffer on Routerboard I can capture 802.11 protocol over TZSP protocol. But when NV2 (proprietary mikrotik wireless protocol) is active mixed among the data SSID is visible in some packets.</p><p>Someone has a dissector for NV2?</p><p>References:</p><p><a href="http://wiki.mikrotik.com/wiki/Manual:Interface/Wireless#Sniffer">http://wiki.mikrotik.com/wiki/Manual:Interface/Wireless#Sniffer</a></p><p><a href="http://wiki.mikrotik.com/wiki/Manual:Nv2">http://wiki.mikrotik.com/wiki/Manual:Nv2</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mikrotik" rel="tag" title="see questions tagged &#39;mikrotik&#39;">mikrotik</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-nv2" rel="tag" title="see questions tagged &#39;nv2&#39;">nv2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '16, 17:50</strong></p><img src="https://secure.gravatar.com/avatar/c5a4f9812452e3e46139407a35de2164?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Eduardo%20Mazolini&#39;s gravatar image" /><p><span>Eduardo Mazo...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Eduardo Mazolini has no accepted answers">0%</span></p></div></div><div id="comments-container-55257" class="comments-container"></div><div id="comment-tools-55257" class="comment-tools"></div><div class="clear"></div><div id="comment-55257-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

