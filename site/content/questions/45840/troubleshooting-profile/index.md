+++
type = "question"
title = "Troubleshooting profile"
description = '''I&#x27;m probably asking a very dumb question here but I&#x27;ll leave my soul to the mercy of this intellectual crowd. I took a class given by Jim Aragon. Which thoroughly helped me with my learning of Wireshark and its uses. During the class he provided us with a great troubleshooting profile along with cou...'''
date = "2015-09-14T22:43:00Z"
lastmod = "2016-01-09T20:42:00Z"
weight = 45840
keywords = [ "profile", "troubleshooting" ]
aliases = [ "/questions/45840" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Troubleshooting profile](/questions/45840/troubleshooting-profile)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45840-score" class="post-score" title="current number of votes">0</div><span id="post-45840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm probably asking a very dumb question here but I'll leave my soul to the mercy of this intellectual crowd. I took a class given by Jim Aragon. Which thoroughly helped me with my learning of Wireshark and its uses. During the class he provided us with a great troubleshooting profile along with couple of others. In the infinite wisdom of my IT department they reformatted my laptop and I lost all of my profiles along with other valuable information. Is there a "gold" standard profile that can be used for troubleshooting? I was spoiled with Jim's. Any help is appreciated. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-profile" rel="tag" title="see questions tagged &#39;profile&#39;">profile</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '15, 22:43</strong></p><img src="https://secure.gravatar.com/avatar/c33415bb4057ce429ff3175d58548c5c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="javiles3&#39;s gravatar image" /><p><span>javiles3</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="javiles3 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '15, 01:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-45840" class="comments-container"><span id="45841"></span><div id="comment-45841" class="comment"><div id="post-45841-score" class="comment-score">1</div><div class="comment-text"><p>Paging <span>@Jim Aragon</span>....</p></div><div id="comment-45841-info" class="comment-info"><span class="comment-age">(14 Sep '15, 22:56)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="48960"></span><div id="comment-48960" class="comment"><div id="post-48960-score" class="comment-score"></div><div class="comment-text"><p>me too please ;)</p><p>macbee</p></div><div id="comment-48960-info" class="comment-info"><span class="comment-age">(07 Jan '16, 16:40)</span> <span class="comment-user userinfo">MacBee</span></div></div></div><div id="comment-tools-45840" class="comment-tools"></div><div class="clear"></div><div id="comment-45840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45846"></span>

<div id="answer-container-45846" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45846-score" class="post-score" title="current number of votes">2</div><span id="post-45846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Put your email address somewhere and I'll send it to you. You could put your address in your ask.wireshark.org profile in obfuscated form. ("John dot smith at gmail dot com" instead of "<span class="__cf_email__" data-cfemail="6903060107471a04001d01290e04080005470a0604">[email protected]</span>"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '15, 05:05</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-45846" class="comments-container"><span id="45877"></span><div id="comment-45877" class="comment"><div id="post-45877-score" class="comment-score"></div><div class="comment-text"><p>Jim,</p><p>Thank you. I have updated my profile with the e-mail address. I look forward to your response. Hope all is well. Thank you.</p></div><div id="comment-45877-info" class="comment-info"><span class="comment-age">(16 Sep '15, 06:20)</span> <span class="comment-user userinfo">javiles3</span></div></div><span id="45878"></span><div id="comment-45878" class="comment"><div id="post-45878-score" class="comment-score"></div><div class="comment-text"><p>added to my profile. Thank you Jim.</p></div><div id="comment-45878-info" class="comment-info"><span class="comment-age">(16 Sep '15, 06:20)</span> <span class="comment-user userinfo">javiles3</span></div></div><span id="49032"></span><div id="comment-49032" class="comment"><div id="post-49032-score" class="comment-score"></div><div class="comment-text"><p><span>@Jim Aragon</span>: would you mind to upload that troubleshooting profile somewhere, for the benefit of all ask wireshark users? ;-)</p></div><div id="comment-49032-info" class="comment-info"><span class="comment-age">(09 Jan '16, 12:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="49036"></span><div id="comment-49036" class="comment"><div id="post-49036-score" class="comment-score"></div><div class="comment-text"><p>I've often thought there should be better templates than what is currently in the base install in terms of profiles and colouring rules. It's subjective, and based on what you're doing with the tool but there are a handful of role-based profiles that could probably be rolled in.</p><p>Two shades of pale blue is how most people see long VoIP or mobile/wireless call flows, which makes me sad. Simple things like shading application-level error code ranges in red, or colouring extremely common protocols above UDP/TCP which would be common to one role or another, goes a long way to making things "pop" and easier to read.</p></div><div id="comment-49036-info" class="comment-info"><span class="comment-age">(09 Jan '16, 18:24)</span> <span class="comment-user userinfo">Quadratic</span></div></div><span id="49043"></span><div id="comment-49043" class="comment"><div id="post-49043-score" class="comment-score"></div><div class="comment-text"><blockquote><p>It's subjective, and based on what you're doing with the tool but there are a handful of role-based profiles that could probably be rolled in.</p></blockquote><p>And if there are some that couldn't unconditionally be deemed to belong in Wireshark, they could be put onto <a href="https://wiki.wireshark.org/Tools">the Wireshark Wiki tools page</a>, probably in a new category, either as attachments on that site or as external links.</p></div><div id="comment-49043-info" class="comment-info"><span class="comment-age">(09 Jan '16, 20:42)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-45846" class="comment-tools"></div><div class="clear"></div><div id="comment-45846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

