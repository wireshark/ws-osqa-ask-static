+++
type = "question"
title = "Display Filter Hotkeys not visable"
description = '''I seem to be having trouble getting access to the &quot;CLEAR&quot; button on the Filter Toolbar to clear any filters I key in. The Hotkeys are visable for the Main Toolbars at the top of the program. I really despise using a mouse, I would prefer using Hotkeys/Keyboard Shortcuts. I was viewing the online use...'''
date = "2014-09-29T11:47:00Z"
lastmod = "2014-10-21T07:19:00Z"
weight = 36700
keywords = [ "hotkey" ]
aliases = [ "/questions/36700" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Display Filter Hotkeys not visable](/questions/36700/display-filter-hotkeys-not-visable)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36700-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36700-score" class="post-score" title="current number of votes">0</div><span id="post-36700-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I seem to be having trouble getting access to the "CLEAR" button on the Filter Toolbar to clear any filters I key in. The Hotkeys are visable for the Main Toolbars at the top of the program. I really despise using a mouse, I would prefer using Hotkeys/Keyboard Shortcuts. I was viewing the online user guide and the filter toolbar shows underscored letters for the shortcuts. I just downloaded the latest version 1.12 from wireshark.org.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hotkey" rel="tag" title="see questions tagged &#39;hotkey&#39;">hotkey</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '14, 11:47</strong></p><img src="https://secure.gravatar.com/avatar/b8542dd67dfdfc06701a5e80ac07c7b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mon0&#39;s gravatar image" /><p><span>mon0</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mon0 has no accepted answers">0%</span></p></div></div><div id="comments-container-36700" class="comments-container"></div><div id="comment-tools-36700" class="comment-tools"></div><div class="clear"></div><div id="comment-36700-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="37239"></span>

<div id="answer-container-37239" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37239-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37239-score" class="post-score" title="current number of votes">1</div><span id="post-37239-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I would prefer using Hotkeys/Keyboard Shortcuts.</p></blockquote><p>here is how you can do it, while you are in the packet list pane (might depend on your layout!). Sure, this is not a Hotkey, but you would be able to do it without touching the mouse ;-)</p><blockquote><p>SHIFT-TAB SHIFT-TAB LEFT LEFT ENTER</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '14, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-37239" class="comments-container"></div><div id="comment-tools-37239" class="comment-tools"></div><div class="clear"></div><div id="comment-37239-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="37196"></span>

<div id="answer-container-37196" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37196-score" class="post-score" title="current number of votes">0</div><span id="post-37196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think there are any hotkeys for those buttons. I did find <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChUseFilterToolbarSection.html">one place</a> in the User Guide which appears to show shortcuts for them but I don't recall the filter toolbar ever looking like that. The <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChUseMainWindowSection.html">earlier screenshot</a> looks more what I've seen and doesn't include those shortcuts.</p><p>This sounds like the kind of improvement that might be made in the new Qt-based UI.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '14, 05:58</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37196" class="comments-container"></div><div id="comment-tools-37196" class="comment-tools"></div><div class="clear"></div><div id="comment-37196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

