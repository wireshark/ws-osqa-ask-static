+++
type = "question"
title = "Capture on Mobile Broadband Interface"
description = '''Is it possible to capture on Mobile Broadband Interface (Windows 7) ? If yes, any specific procedure? It does not appear in the list of available interfaces.'''
date = "2013-08-18T23:53:00Z"
lastmod = "2013-08-19T13:34:00Z"
weight = 23845
keywords = [ "mbn" ]
aliases = [ "/questions/23845" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture on Mobile Broadband Interface](/questions/23845/capture-on-mobile-broadband-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23845-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23845-score" class="post-score" title="current number of votes">0</div><span id="post-23845-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to capture on Mobile Broadband Interface (Windows 7) ? If yes, any specific procedure? It does not appear in the list of available interfaces.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mbn" rel="tag" title="see questions tagged &#39;mbn&#39;">mbn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '13, 23:53</strong></p><img src="https://secure.gravatar.com/avatar/e007baa1950a507d2163e10837a2861d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rajat&#39;s gravatar image" /><p><span>Rajat</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rajat has no accepted answers">0%</span></p></div></div><div id="comments-container-23845" class="comments-container"></div><div id="comment-tools-23845" class="comment-tools"></div><div class="clear"></div><div id="comment-23845-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23851"></span>

<div id="answer-container-23851" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23851-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23851-score" class="post-score" title="current number of votes">1</div><span id="post-23851-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately, no, as per <a href="http://www.winpcap.org/misc/faq.htm#Q-5">Question 5 in the WinPcap FAQ</a>:</p><blockquote><p>Windows Vista and more recent. It's not possible to capture on PPP/VPN connections on these operating systems.</p></blockquote><p>("telephone-based" interfaces, such as dial-up modems and mobile phone and, I think, WiMax devices, show up as PPP interfaces).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Aug '13, 13:34</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-23851" class="comments-container"></div><div id="comment-tools-23851" class="comment-tools"></div><div class="clear"></div><div id="comment-23851-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

