+++
type = "question"
title = "Related packet symbols in Lua dissector"
description = '''Hi! I am developing a dissector in Lua for our own protocol. I was wodering if it is possible to tell wireshark which packets are related to which in a Lua dissector, so that it marks them with the different related packet symbols. For example signal that a packet is a request and another one is a r...'''
date = "2017-03-31T05:23:00Z"
lastmod = "2017-03-31T05:23:00Z"
weight = 60476
keywords = [ "lua", "dissector", "packets", "related", "wireshark" ]
aliases = [ "/questions/60476" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Related packet symbols in Lua dissector](/questions/60476/related-packet-symbols-in-lua-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60476-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60476-score" class="post-score" title="current number of votes">0</div><span id="post-60476-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! I am developing a dissector in Lua for our own protocol. I was wodering if it is possible to tell wireshark which packets are related to which in a Lua dissector, so that it marks them with the different related packet symbols. For example signal that a packet is a request and another one is a response or something like that. Is that possible ?</p><p>I am thinking about the packet symbols as shown here: <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketListPaneSection.html">https://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketListPaneSection.html</a></p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-related" rel="tag" title="see questions tagged &#39;related&#39;">related</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '17, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/0975478e15c0adc8c5935c57470830f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="david-pop&#39;s gravatar image" /><p><span>david-pop</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="david-pop has no accepted answers">0%</span></p></div></div><div id="comments-container-60476" class="comments-container"></div><div id="comment-tools-60476" class="comment-tools"></div><div class="clear"></div><div id="comment-60476-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

