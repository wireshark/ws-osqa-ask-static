+++
type = "question"
title = "Window appears off-screen!"
description = '''Running on Windows 7, with three monitors attached, the main window appears off the top of the leftmost screen, so it is impossible to drag the title bar. I use keyboard commands to move it. Next time I run Wireshark it does not reappear where I left it, but once again is off-screen. It appears to b...'''
date = "2012-05-08T00:53:00Z"
lastmod = "2012-05-08T07:33:00Z"
weight = 10765
keywords = [ "windows7", "positionownscar", "desktop" ]
aliases = [ "/questions/10765" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Window appears off-screen!](/questions/10765/window-appears-off-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10765-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10765-score" class="post-score" title="current number of votes">1</div><span id="post-10765-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Running on Windows 7, with three monitors attached, the main window appears off the top of the leftmost screen, so it is impossible to drag the title bar. I use keyboard commands to move it. Next time I run Wireshark it does not reappear where I left it, but once again is off-screen.</p><p>It appears to be positioned as high up as the main screen would allow, but the left screen doesn't go that high up. You cannot assume that the total visible desktop area is not a rectangle.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-positionownscar" rel="tag" title="see questions tagged &#39;positionownscar&#39;">positionownscar</span> <span class="post-tag tag-link-desktop" rel="tag" title="see questions tagged &#39;desktop&#39;">desktop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '12, 00:53</strong></p><img src="https://secure.gravatar.com/avatar/60db8680b403b122784beda1e658d6fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JD%C5%82ugosz&#39;s gravatar image" /><p><span>JDługosz</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JDługosz has no accepted answers">0%</span></p></div></div><div id="comments-container-10765" class="comments-container"></div><div id="comment-tools-10765" class="comment-tools"></div><div class="clear"></div><div id="comment-10765-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10766"></span>

<div id="answer-container-10766" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10766-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10766-score" class="post-score" title="current number of votes">1</div><span id="post-10766-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have three monitors on Win7 x64 as well, and Wireshark starts on the leftmost screen - but in my case not offscreen. It's just aligned to the top left. So in your case you might want to open a bug report at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a>.</p><p>As a workaround you could go to Edit -&gt; Preferences -&gt; User Interface and put a check mark at "Save Window Position". That way Wireshark will reopen at the spot it was closed last time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 May '12, 01:05</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-10766" class="comments-container"><span id="10783"></span><div id="comment-10783" class="comment"><div id="post-10783-score" class="comment-score"></div><div class="comment-text"><p>There is already an open bug report for this, namely <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=553">Bug 553</a>.</p></div><div id="comment-10783-info" class="comment-info"><span class="comment-age">(08 May '12, 07:33)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-10766" class="comment-tools"></div><div class="clear"></div><div id="comment-10766-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

