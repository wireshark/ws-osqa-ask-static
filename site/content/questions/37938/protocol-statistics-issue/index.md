+++
type = "question"
title = "Protocol Statistics Issue"
description = '''If I open the protocol hierarchy statistics. I see 99.99% of the traffic is TCP. However it shows 28.43% is SSL and MySQl as 7.25% (for the packet count).  But it shows nothing to account for the other 60% ? This is also the same for the byte count i.e it doesnt equate to 100% ?'''
date = "2014-11-18T01:29:00Z"
lastmod = "2014-11-18T01:29:00Z"
weight = 37938
keywords = [ "protocols" ]
aliases = [ "/questions/37938" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Statistics Issue](/questions/37938/protocol-statistics-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37938-score" class="post-score" title="current number of votes">0</div><span id="post-37938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If I open the protocol hierarchy statistics. I see 99.99% of the traffic is TCP. However it shows 28.43% is SSL and MySQl as 7.25% (for the packet count). But it shows nothing to account for the other 60% ? This is also the same for the byte count i.e it doesnt equate to 100% ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '14, 01:29</strong></p><img src="https://secure.gravatar.com/avatar/22baebd906c29ccfcb5b2aeb350b22fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bart80&#39;s gravatar image" /><p><span>bart80</span><br />
<span class="score" title="11 reputation points">11</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bart80 has no accepted answers">0%</span></p></div></div><div id="comments-container-37938" class="comments-container"></div><div id="comment-tools-37938" class="comment-tools"></div><div class="clear"></div><div id="comment-37938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

