+++
type = "question"
title = "Websockets in Follow TCP Stream"
description = '''I&#x27;m trying to follow a websockets stream, and &quot;Follow TCP Stream&quot; is pretty close to what I need. However, the outbound text is masked (i.e. it shows websocket.payload.text_masked rather than websocket.payload.text), so it&#x27;s rather meaningless in the content constructed by &quot;Follow TCP Stream&quot;. Is th...'''
date = "2014-05-15T19:25:00Z"
lastmod = "2014-05-15T19:25:00Z"
weight = 32842
keywords = [ "follow.tcp.stream", "websockets" ]
aliases = [ "/questions/32842" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Websockets in Follow TCP Stream](/questions/32842/websockets-in-follow-tcp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32842-score" class="post-score" title="current number of votes">0</div><span id="post-32842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to follow a websockets stream, and "Follow TCP Stream" is pretty close to what I need. However, the outbound text is masked (i.e. it shows websocket.payload.text_masked rather than websocket.payload.text), so it's rather meaningless in the content constructed by "Follow TCP Stream". Is there a way to get it to show the unmasked websocket text?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-follow.tcp.stream" rel="tag" title="see questions tagged &#39;follow.tcp.stream&#39;">follow.tcp.stream</span> <span class="post-tag tag-link-websockets" rel="tag" title="see questions tagged &#39;websockets&#39;">websockets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '14, 19:25</strong></p><img src="https://secure.gravatar.com/avatar/ee174623278055624d1cbc53e21f31eb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lid&#39;s gravatar image" /><p><span>lid</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lid has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 May '14, 19:48</strong> </span></p></div></div><div id="comments-container-32842" class="comments-container"></div><div id="comment-tools-32842" class="comment-tools"></div><div class="clear"></div><div id="comment-32842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

