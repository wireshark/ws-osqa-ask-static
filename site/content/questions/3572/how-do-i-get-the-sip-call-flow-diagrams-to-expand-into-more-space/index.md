+++
type = "question"
title = "How do I get the SIP call flow diagrams to expand into more space?"
description = '''When I bring up the SIP Call Flow diagram, it is too cramped to be usable. I can expand the pop-up window. The center bar that is the boundary between the &quot;comment&quot; on the right and the diagram on the left is live. However, the bar can be moved to the left, making the diagram smaller, but not to the...'''
date = "2011-04-18T09:18:00Z"
lastmod = "2011-04-18T09:29:00Z"
weight = 3572
keywords = [ "sip" ]
aliases = [ "/questions/3572" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I get the SIP call flow diagrams to expand into more space?](/questions/3572/how-do-i-get-the-sip-call-flow-diagrams-to-expand-into-more-space)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3572-score" class="post-score" title="current number of votes">0</div><span id="post-3572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I bring up the SIP Call Flow diagram, it is too cramped to be usable. I can expand the pop-up window. The center bar that is the boundary between the "comment" on the right and the diagram on the left is live. However, the bar can be moved to the left, making the diagram smaller, but not to the right, making the diagram larger.</p><p>Is this a bug or am I missing something?</p><p>-Bob</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '11, 09:18</strong></p><img src="https://secure.gravatar.com/avatar/4ae585652404eee099c2b2e079e0c817?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LostInTheTrees&#39;s gravatar image" /><p><span>LostInTheTrees</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LostInTheTrees has no accepted answers">0%</span></p></div></div><div id="comments-container-3572" class="comments-container"></div><div id="comment-tools-3572" class="comment-tools"></div><div class="clear"></div><div id="comment-3572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3573"></span>

<div id="answer-container-3573" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3573-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3573-score" class="post-score" title="current number of votes">1</div><span id="post-3573-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's an open bug on this.</p><p>See <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=4972">Bug #4972</a> and comments in the various bugs marked as duplicates of that bug ....</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '11, 09:29</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-3573" class="comments-container"></div><div id="comment-tools-3573" class="comment-tools"></div><div class="clear"></div><div id="comment-3573-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

