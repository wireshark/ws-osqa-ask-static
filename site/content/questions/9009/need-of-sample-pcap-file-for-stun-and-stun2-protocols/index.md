+++
type = "question"
title = "Need of sample pcap file for STUN  and  STUN2  protocols"
description = '''Hi, As i am new for using STUN protocol and i need to implement this in our code base. Please provide me a sample captured pcap file and wireshark open source code for the STUN and STUN2 protocols. I hope wireshark community definitely will help me for my project. Thanks &amp;amp; Regards, Suman'''
date = "2012-02-14T22:41:00Z"
lastmod = "2012-02-14T22:41:00Z"
weight = 9009
keywords = [ "and", "stun", "protocols", "stun2" ]
aliases = [ "/questions/9009" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need of sample pcap file for STUN and STUN2 protocols](/questions/9009/need-of-sample-pcap-file-for-stun-and-stun2-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9009-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9009-score" class="post-score" title="current number of votes">0</div><span id="post-9009-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>As i am new for using STUN protocol and i need to implement this in our code base. Please provide me a sample captured pcap file and wireshark open source code for the STUN and STUN2 protocols.</p><p>I hope wireshark community definitely will help me for my project.</p><p>Thanks &amp; Regards, Suman</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-and" rel="tag" title="see questions tagged &#39;and&#39;">and</span> <span class="post-tag tag-link-stun" rel="tag" title="see questions tagged &#39;stun&#39;">stun</span> <span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span> <span class="post-tag tag-link-stun2" rel="tag" title="see questions tagged &#39;stun2&#39;">stun2</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '12, 22:41</strong></p><img src="https://secure.gravatar.com/avatar/6e027fcf7ea5966bf94407ee70d88cc9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chakrasuman&#39;s gravatar image" /><p><span>chakrasuman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chakrasuman has no accepted answers">0%</span></p></div></div><div id="comments-container-9009" class="comments-container"></div><div id="comment-tools-9009" class="comment-tools"></div><div class="clear"></div><div id="comment-9009-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

