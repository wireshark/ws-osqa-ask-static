+++
type = "question"
title = "How can Unsolicited UDP Traffic be prevented?"
description = '''I am using uTorrent client which subscribes to peers that continue sending barrage of UDP packets even after I have exited the uTorrent program.I am behind home router and I don&#x27;t want to use firewall to block this, each time I log on to my home PC. How can I prevent this?? Any help will be highly a...'''
date = "2011-08-29T11:21:00Z"
lastmod = "2011-08-29T14:33:00Z"
weight = 5922
keywords = [ "udp", "torrent" ]
aliases = [ "/questions/5922" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can Unsolicited UDP Traffic be prevented?](/questions/5922/how-can-unsolicited-udp-traffic-be-prevented)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5922-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5922-score" class="post-score" title="current number of votes">0</div><span id="post-5922-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using uTorrent client which subscribes to peers that continue sending barrage of UDP packets even after I have exited the uTorrent program.I am behind home router and I don't want to use firewall to block this, each time I log on to my home PC. How can I prevent this?? Any help will be highly appreciated:-)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-torrent" rel="tag" title="see questions tagged &#39;torrent&#39;">torrent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '11, 11:21</strong></p><img src="https://secure.gravatar.com/avatar/9d9cd66b10a3c54e27594ad3763c94c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Farrukh&#39;s gravatar image" /><p><span>Farrukh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Farrukh has no accepted answers">0%</span></p></div></div><div id="comments-container-5922" class="comments-container"></div><div id="comment-tools-5922" class="comment-tools"></div><div class="clear"></div><div id="comment-5922-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5930"></span>

<div id="answer-container-5930" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5930-score" class="post-score" title="current number of votes">0</div><span id="post-5930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Enable use of UPnP or something like that. For more details you should frequent the µTorrent forums.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Aug '11, 14:33</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-5930" class="comments-container"></div><div id="comment-tools-5930" class="comment-tools"></div><div class="clear"></div><div id="comment-5930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

