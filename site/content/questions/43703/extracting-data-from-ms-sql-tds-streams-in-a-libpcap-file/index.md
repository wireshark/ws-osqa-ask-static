+++
type = "question"
title = "Extracting Data From MS-SQL TDS Streams in a libpcap File"
description = '''i have some capture files and i want to know if there is any way to see what it passtrough. i need to see the data in this pcap file. thanks'''
date = "2015-06-29T23:14:00Z"
lastmod = "2015-06-29T23:14:00Z"
weight = 43703
keywords = [ "pcap", "extract", "mysql" ]
aliases = [ "/questions/43703" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extracting Data From MS-SQL TDS Streams in a libpcap File](/questions/43703/extracting-data-from-ms-sql-tds-streams-in-a-libpcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43703-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43703-score" class="post-score" title="current number of votes">0</div><span id="post-43703-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have some capture files and i want to know if there is any way to see what it passtrough.</p><p>i need to see the data in this pcap file. thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-extract" rel="tag" title="see questions tagged &#39;extract&#39;">extract</span> <span class="post-tag tag-link-mysql" rel="tag" title="see questions tagged &#39;mysql&#39;">mysql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '15, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/a7efdaf6079e24cd2813662f99e0cf05?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Juan%20Carlos%20Garcia&#39;s gravatar image" /><p><span>Juan Carlos ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Juan Carlos Garcia has no accepted answers">0%</span></p></div></div><div id="comments-container-43703" class="comments-container"></div><div id="comment-tools-43703" class="comment-tools"></div><div class="clear"></div><div id="comment-43703-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

