+++
type = "question"
title = "Use multiple files option not working with Wireshark 1.8.4 SVN Rev 46250"
description = '''Hi, I enabled following options in Capture Files feature. Enabled Use Multiple files Next file every 1 MB Next file every 10 seconds and Stop capture after 4 files  but i still saw multiple captures(more than 4) got generated.'''
date = "2013-02-25T15:39:00Z"
lastmod = "2013-02-26T11:46:00Z"
weight = 18868
keywords = [ "multiple-files", "wireshark" ]
aliases = [ "/questions/18868" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Use multiple files option not working with Wireshark 1.8.4 SVN Rev 46250](/questions/18868/use-multiple-files-option-not-working-with-wireshark-184-svn-rev-46250)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18868-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18868-score" class="post-score" title="current number of votes">0</div><span id="post-18868-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I enabled following options in Capture Files feature. Enabled Use Multiple files Next file every 1 MB Next file every 10 seconds and <strong>Stop capture after 4 files</strong> but i still saw multiple captures(more than 4) got generated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple-files" rel="tag" title="see questions tagged &#39;multiple-files&#39;">multiple-files</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '13, 15:39</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-18868" class="comments-container"><span id="18869"></span><div id="comment-18869" class="comment"><div id="post-18869-score" class="comment-score"></div><div class="comment-text"><p>It works on my WinXP system. What is your OS?</p><p>Regards<br />
Kurt</p></div><div id="comment-18869-info" class="comment-info"><span class="comment-age">(25 Feb '13, 17:22)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="18893"></span><div id="comment-18893" class="comment"><div id="post-18893-score" class="comment-score"></div><div class="comment-text"><p>Looks like one time issue.It is working with multiple iterations.</p><p>Thanks, Kserasera</p></div><div id="comment-18893-info" class="comment-info"><span class="comment-age">(26 Feb '13, 11:46)</span> <span class="comment-user userinfo">krishnayeddula</span></div></div></div><div id="comment-tools-18868" class="comment-tools"></div><div class="clear"></div><div id="comment-18868-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

