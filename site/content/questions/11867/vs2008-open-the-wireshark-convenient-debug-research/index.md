+++
type = "question"
title = "vs2008 open the wireshark, convenient debug research?"
description = '''On Windows, Wireshark compiled engineering, exe also created. Because the compiler is in the console, it is not convenient to debug learning, I hope can vs2008 open the engineering, convenient debug research. But, the original project is nmake file organization with the compiler, no vs2008. SLN file...'''
date = "2012-06-13T03:47:00Z"
lastmod = "2012-06-13T04:24:00Z"
weight = 11867
keywords = [ "vs2008", "wireshark" ]
aliases = [ "/questions/11867" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [vs2008 open the wireshark, convenient debug research?](/questions/11867/vs2008-open-the-wireshark-convenient-debug-research)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11867-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11867-score" class="post-score" title="current number of votes">0</div><span id="post-11867-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On Windows, Wireshark compiled engineering, exe also created.</p><p>Because the compiler is in the console, it is not convenient to debug learning, I hope can vs2008 open the engineering, convenient debug research. But, the original project is nmake file organization with the compiler, no vs2008. SLN files, do not know to have method conversion. Nmake to. SLN, or through the other way for vs2008 open project?</p><p>WireShark source version: WireShark-1.6.5, from <a href="http://www.wireshark.org/download.html">http://www.wireshark.org/download.html</a> download</p><p>Hope the master give advice or comments please!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vs2008" rel="tag" title="see questions tagged &#39;vs2008&#39;">vs2008</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '12, 03:47</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-11867" class="comments-container"></div><div id="comment-tools-11867" class="comment-tools"></div><div class="clear"></div><div id="comment-11867-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11870"></span>

<div id="answer-container-11870" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11870-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11870-score" class="post-score" title="current number of votes">0</div><span id="post-11870-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This question has already been asked and answered. See <a href="http://ask.wireshark.org/questions/8660/wireshark-building-and-debugging-on-visual-c-or-visual-studio">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jun '12, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-11870" class="comments-container"></div><div id="comment-tools-11870" class="comment-tools"></div><div class="clear"></div><div id="comment-11870-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

