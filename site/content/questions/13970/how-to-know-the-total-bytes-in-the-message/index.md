+++
type = "question"
title = "How to know the total bytes in the message ?"
description = '''Hey guys, I&#x27;m using the Wireshark for the first time :) I&#x27;d like to ask you guys what&#x27;s the difference between the total bytes in the message and the total number of bytes in the whole frame? Also, how can I get them from the [app after captured]? Looking forward to hearing from you soon! Thanks.'''
date = "2012-08-30T19:46:00Z"
lastmod = "2012-09-04T02:39:00Z"
weight = 13970
keywords = [ "message", "farme" ]
aliases = [ "/questions/13970" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to know the total bytes in the message ?](/questions/13970/how-to-know-the-total-bytes-in-the-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13970-score" class="post-score" title="current number of votes">0</div><span id="post-13970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey guys,</p><p>I'm using the Wireshark for the first time :) I'd like to ask you guys what's the difference between the total bytes in the message and the total number of bytes in the whole frame? Also, how can I get them from the [app after captured]?</p><p>Looking forward to hearing from you soon!</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-message" rel="tag" title="see questions tagged &#39;message&#39;">message</span> <span class="post-tag tag-link-farme" rel="tag" title="see questions tagged &#39;farme&#39;">farme</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Aug '12, 19:46</strong></p><img src="https://secure.gravatar.com/avatar/a5e57bc71619dd7f64ba48fa0d6a4a2d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Acalifornia&#39;s gravatar image" /><p><span>Acalifornia</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Acalifornia has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Aug '12, 00:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-13970" class="comments-container"><span id="13972"></span><div id="comment-13972" class="comment"><div id="post-13972-score" class="comment-score"></div><div class="comment-text"><p>Keep in mind that Wireshark works from the 'the wire' up, that means that it knows about the bytes in the whole frame and works its way up from there. There's no definition of what 'the message' is, so you'll have to elaborate on that first.</p></div><div id="comment-13972-info" class="comment-info"><span class="comment-age">(31 Aug '12, 00:26)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="13980"></span><div id="comment-13980" class="comment"><div id="post-13980-score" class="comment-score"></div><div class="comment-text"><p>Thanks but I got a book in computer network field and in its website has some assignments and I try to answer the questions. I did all the steps explained in the book but can't answer some of the questions which are what I asked :)</p><p>please if you don't mind to look at it here: <a href="http://highered.mcgraw-hill.com/sites/0073523267/student_view0/lab_assignments.html">http://highered.mcgraw-hill.com/sites/0073523267/student_view0/lab_assignments.html</a> (assignment 1)</p><p>Thanks</p></div><div id="comment-13980-info" class="comment-info"><span class="comment-age">(31 Aug '12, 16:11)</span> <span class="comment-user userinfo">Acalifornia</span></div></div><span id="13997"></span><div id="comment-13997" class="comment"><div id="post-13997-score" class="comment-score"></div><div class="comment-text"><p>Any help folks?</p></div><div id="comment-13997-info" class="comment-info"><span class="comment-age">(02 Sep '12, 16:57)</span> <span class="comment-user userinfo">Acalifornia</span></div></div></div><div id="comment-tools-13970" class="comment-tools"></div><div class="clear"></div><div id="comment-13970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13998"></span>

<div id="answer-container-13998" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13998-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13998-score" class="post-score" title="current number of votes">2</div><span id="post-13998-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please Spend more time with wireshark to do your homework.</p><p>Please download the sample Http capture file <a href="http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=http.cap">http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=http.cap</a>.</p><p>I will take the packet number 4 as example. The frame length is the entire packet length which came through the wire to your network card.</p><p>What your asking is the Application length over IP/TCP for that check this image below . Check the length of "IP-&gt;Total length" = ( ip header length + Tcp Header length+ application) .</p><p>So the ip header says 519 ,So subtract 20 Bytes of ip header and 20 bytes of tcp header .</p><p>The HTTP message length = 519 -20- 20 = 479 bytes.</p><p>Note :I have shown the http as application it can be any other application its decodes based on destination port. And if any application is UDP over IP insted of TCP then subtract 8 bytes as Lenght of UDP is 8 bytes.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/ip_tcp_http.JPG" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Sep '12, 23:16</strong></p><img src="https://secure.gravatar.com/avatar/0cf7e05b14ad6662ecde4c327bb2c39f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harsha&#39;s gravatar image" /><p><span>Harsha</span><br />
<span class="score" title="46 reputation points">46</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harsha has no accepted answers">0%</span></p></img></div></div><div id="comments-container-13998" class="comments-container"><span id="13999"></span><div id="comment-13999" class="comment"><div id="post-13999-score" class="comment-score"></div><div class="comment-text"><p>I have attached the image which shows TCP header length <img src="https://osqa-ask.wireshark.org/upfiles/eth_ip_tcp1.JPG" alt="alt text" /></p></div><div id="comment-13999-info" class="comment-info"><span class="comment-age">(02 Sep '12, 23:25)</span> <span class="comment-user userinfo">Harsha</span></div></div><span id="14007"></span><div id="comment-14007" class="comment"><div id="post-14007-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your help :)</p><p>To be honest, what you explained is what I did before asking my question. I was confused about the two questions in the assignment which are the total number of bytes in the whole frame and the total bytes in the message (at the application layer).</p><p>I've attached an image to show you what I did.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/pic_1.png" alt="alt text" /></p><p>So, in this case: (as I understand): whole frame: 436 bytes, IP header: 20 bytes, TCP header: 32 bytes, Total bytes in the whole message: 422 - (20+32) = 370 bytes (as you explained).</p><p>Correct? ,</p><p>How about the number of bytes in the Ethernet? 0 bytes?</p></div><div id="comment-14007-info" class="comment-info"><span class="comment-age">(03 Sep '12, 17:30)</span> <span class="comment-user userinfo">Acalifornia</span></div></div><span id="14010"></span><div id="comment-14010" class="comment"><div id="post-14010-score" class="comment-score">1</div><div class="comment-text"><p>yes 370 bytes.</p><p>Ethernet frame is of 14 bytes Which is not included in IP total length.</p></div><div id="comment-14010-info" class="comment-info"><span class="comment-age">(03 Sep '12, 22:56)</span> <span class="comment-user userinfo">Harsha</span></div></div><span id="14022"></span><div id="comment-14022" class="comment"><div id="post-14022-score" class="comment-score"></div><div class="comment-text"><p>Thank you! I gotta a question: How did we know that the Ethernet frame is of 14 bytes ? Do we calculate it as: whole frame - IP total length = 436 - 422 = 14 bytes ?</p></div><div id="comment-14022-info" class="comment-info"><span class="comment-age">(04 Sep '12, 02:25)</span> <span class="comment-user userinfo">Acalifornia</span></div></div><span id="14025"></span><div id="comment-14025" class="comment"><div id="post-14025-score" class="comment-score"></div><div class="comment-text"><p>I've converted your "answers" to comments as that's how this site works. Please read the FAQ for more info.</p><p>If someone provides an answer that resolves your issue, please accept it by clicking the checkmark icon next to the answer.</p></div><div id="comment-14025-info" class="comment-info"><span class="comment-age">(04 Sep '12, 02:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-13998" class="comment-tools"></div><div class="clear"></div><div id="comment-13998-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

