+++
type = "question"
title = "combine or merge some files in one."
description = '''this is the scenario: I have a computer with tshark that captures and creates a file each hour. I have 24 files per day and i want to know if there is a way of grouping these 24 files in 1. im a windows user. '''
date = "2015-07-29T21:17:00Z"
lastmod = "2015-07-29T22:44:00Z"
weight = 44618
keywords = [ "merge", "combine", "tshark" ]
aliases = [ "/questions/44618" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [combine or merge some files in one.](/questions/44618/combine-or-merge-some-files-in-one)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44618-score" class="post-score" title="current number of votes">0</div><span id="post-44618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>this is the scenario: I have a computer with tshark that captures and creates a file each hour. I have 24 files per day and i want to know if there is a way of grouping these 24 files in 1.</p><p>im a windows user.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-merge" rel="tag" title="see questions tagged &#39;merge&#39;">merge</span> <span class="post-tag tag-link-combine" rel="tag" title="see questions tagged &#39;combine&#39;">combine</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '15, 21:17</strong></p><img src="https://secure.gravatar.com/avatar/a7efdaf6079e24cd2813662f99e0cf05?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Juan%20Carlos%20Garcia&#39;s gravatar image" /><p><span>Juan Carlos ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Juan Carlos Garcia has no accepted answers">0%</span></p></div></div><div id="comments-container-44618" class="comments-container"></div><div id="comment-tools-44618" class="comment-tools"></div><div class="clear"></div><div id="comment-44618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44619"></span>

<div id="answer-container-44619" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44619-score" class="post-score" title="current number of votes">2</div><span id="post-44619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This Task can be done by mergepcap which is part of Wireshark.<br />
</p><pre><code> mergecap  [ -F file format ] -w outfile | - infile [ infile ...]</code></pre><p><br />
A manpage can be found here: <a href="https://www.wireshark.org/docs/man-pages/mergecap.html">https://www.wireshark.org/docs/man-pages/mergecap.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jul '15, 22:44</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span> </br></br></p></div></div><div id="comments-container-44619" class="comments-container"></div><div id="comment-tools-44619" class="comment-tools"></div><div class="clear"></div><div id="comment-44619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

