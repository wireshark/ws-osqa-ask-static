+++
type = "question"
title = "Do i need to pay to use wireshark"
description = '''how much to i need to pay for wireshark to use the features? is there a free version and paid version'''
date = "2015-11-10T07:22:00Z"
lastmod = "2015-11-10T14:27:00Z"
weight = 47468
keywords = [ "payment" ]
aliases = [ "/questions/47468" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Do i need to pay to use wireshark](/questions/47468/do-i-need-to-pay-to-use-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47468-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47468-score" class="post-score" title="current number of votes">0</div><span id="post-47468-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how much to i need to pay for wireshark to use the features? is there a free version and paid version</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-payment" rel="tag" title="see questions tagged &#39;payment&#39;">payment</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '15, 07:22</strong></p><img src="https://secure.gravatar.com/avatar/289be086e5699d942335d3a9a9543e5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yhomiid&#39;s gravatar image" /><p><span>yhomiid</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yhomiid has no accepted answers">0%</span></p></div></div><div id="comments-container-47468" class="comments-container"></div><div id="comment-tools-47468" class="comment-tools"></div><div class="clear"></div><div id="comment-47468-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47469"></span>

<div id="answer-container-47469" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47469-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47469-score" class="post-score" title="current number of votes">0</div><span id="post-47469-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Somewhat surprisingly the website doesn't make this obvious. No you don't need to pay, Wireshark is free.</p><p>You can download from the <a href="https://www.wireshark.org/download.html">Wireshark web site</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '15, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Nov '15, 07:39</strong> </span></p></div></div><div id="comments-container-47469" class="comments-container"><span id="47480"></span><div id="comment-47480" class="comment"><div id="post-47480-score" class="comment-score"></div><div class="comment-text"><p>Donations are always welcome though! Donations come in many forms, such as Wireshark bug submissions, patches, providing help through this community site or via the mailing lists, etc.</p></div><div id="comment-47480-info" class="comment-info"><span class="comment-age">(10 Nov '15, 12:15)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="47486"></span><div id="comment-47486" class="comment"><div id="post-47486-score" class="comment-score"></div><div class="comment-text"><p>(I.e., not all donations to free-software projects are in the form of money.)</p></div><div id="comment-47486-info" class="comment-info"><span class="comment-age">(10 Nov '15, 14:27)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-47469" class="comment-tools"></div><div class="clear"></div><div id="comment-47469-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

