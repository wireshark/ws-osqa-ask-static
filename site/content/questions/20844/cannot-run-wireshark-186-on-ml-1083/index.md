+++
type = "question"
title = "Cannot run Wireshark 1.8.6 on ML 10.8.3"
description = '''Hi gang, I have used WS for a good while, and really appreciate all it does for me. I just changed companies and went to install it on my new machine, and am having the most frustrating time getting even the simplest of things to work. I downloaded and installed XQuartz (2.7.4), rebooted and was nev...'''
date = "2013-04-29T11:56:00Z"
lastmod = "2013-04-29T11:56:00Z"
weight = 20844
keywords = [ "failure", "xquartz", "wireshark", "launch" ]
aliases = [ "/questions/20844" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot run Wireshark 1.8.6 on ML 10.8.3](/questions/20844/cannot-run-wireshark-186-on-ml-1083)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20844-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20844-score" class="post-score" title="current number of votes">0</div><span id="post-20844-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi gang,</p><p>I have used WS for a good while, and really appreciate all it does for me. I just changed companies and went to install it on my new machine, and am having the most frustrating time getting even the simplest of things to work.</p><p>I downloaded and installed XQuartz (2.7.4), rebooted and was never able to launch it. The icon would show in the dock and disappear immediately. No process was listed in the Activity Monitor, nothing. Finally, I removed it, cleaned up everything related to it I could find, re-installed and ran it from the Terminal and it finally worked. Not sure what the magic was, as I am working on a new machine that has never had anything other than Mountain Lion installed on it.</p><p>Then I downloaded and installed WS (1.8.6), installed and tried to run it. I get the dialog saying "X11 is not present on your system..." with buttons to download or quit.</p><p>After searching around, I find that I can get it to work by launching XQuartz (again from the command line as it will not go from the icon), opening the XQuartz terminal and launching WS from there. But, if I do not launch it as root, I get no fonts and the app fails. Launching as root, gets it up and running with the requisite "danger" dialog about running as root.</p><p>I am unable to resize the screen, which is tiny and stuck to the upper-left corner, and if I shut it down and try to launch from the Mac terminal, I get a GtK error. Searches for that show a bunch of ideas, none of which apply to my circumstances (such as removing plugins that I don't have, etc.).</p><p>This is a truly miserable experience, and has consumed far too many hours of time.</p><p>Can anyone point me to any sort of answer/idea/help or even an alternative to WS??</p><p>Cheers,</p><p>Chris</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-failure" rel="tag" title="see questions tagged &#39;failure&#39;">failure</span> <span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-launch" rel="tag" title="see questions tagged &#39;launch&#39;">launch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Apr '13, 11:56</strong></p><img src="https://secure.gravatar.com/avatar/24365ad1f8582cbe21e15a4836844fc9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Raconteur&#39;s gravatar image" /><p><span>Raconteur</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Raconteur has no accepted answers">0%</span></p></div></div><div id="comments-container-20844" class="comments-container"></div><div id="comment-tools-20844" class="comment-tools"></div><div class="clear"></div><div id="comment-20844-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

