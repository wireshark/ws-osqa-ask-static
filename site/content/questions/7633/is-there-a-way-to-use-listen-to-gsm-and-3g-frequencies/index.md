+++
type = "question"
title = "Is there a way to use listen to gsm and 3G frequencies ?"
description = '''Is there any way to modify regular 3G cellphone to listen to CDMA and TDMA radio frequencies ? I would like to know when a given cellphone is around, so I would only need to get 2 things:  A unique identifier doesn&#x27;t need to  be an IMSI or IMEI, something  encrypted would be ok... I just need  somet...'''
date = "2011-11-25T11:38:00Z"
lastmod = "2011-11-25T11:38:00Z"
weight = 7633
keywords = [ "listener", "gsm", "3g", "radio" ]
aliases = [ "/questions/7633" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a way to use listen to gsm and 3G frequencies ?](/questions/7633/is-there-a-way-to-use-listen-to-gsm-and-3g-frequencies)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7633-score" class="post-score" title="current number of votes">0</div><span id="post-7633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way to modify regular 3G cellphone to listen to CDMA and TDMA radio frequencies ?</p><p>I would like to know when a given cellphone is around, so I would only need to get 2 things:</p><ul><li>A unique identifier doesn't need to be an IMSI or IMEI, something encrypted would be ok... I just need something that doesn't change.<br />
</li><li>signal strengh.</li></ul><p>Is there a setup using wireshark to listen to the packages, some radio receiver and antenna to do that ?</p><p>Could it be done with a regular cellphone and some low level access ? is a cellphone capable of listening to other close by cellphones ?</p><p>AGAIN</p><p>I am not interested in any kind of listening to calls or getting IMEI or IMSIs. Just get the signals and list of phones in an area.</p><p>I found USRP radios and some software that could do it, but it's to expensive and complesx</p><p>Any idea is welcome.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-listener" rel="tag" title="see questions tagged &#39;listener&#39;">listener</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-3g" rel="tag" title="see questions tagged &#39;3g&#39;">3g</span> <span class="post-tag tag-link-radio" rel="tag" title="see questions tagged &#39;radio&#39;">radio</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '11, 11:38</strong></p><img src="https://secure.gravatar.com/avatar/c8510d4121d6efdfb2e4c098db3bb656?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cfontes&#39;s gravatar image" /><p><span>cfontes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cfontes has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Nov '11, 11:39</strong> </span></p></div></div><div id="comments-container-7633" class="comments-container"></div><div id="comment-tools-7633" class="comment-tools"></div><div class="clear"></div><div id="comment-7633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

