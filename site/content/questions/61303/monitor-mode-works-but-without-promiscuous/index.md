+++
type = "question"
title = "Monitor Mode works but without promiscuous"
description = '''Using monitor mode I am able to capture traffic including radio data on any SSID and of the channel of my choosing but only when it is sent as broadcast or multicast - I cant see UNICAST Data sent by other clients.  I tried setting promiscuous mode on in wireshark as well as using ip link - did not ...'''
date = "2017-05-09T05:03:00Z"
lastmod = "2017-05-09T12:02:00Z"
weight = 61303
keywords = [ "sniffing", "16.04", "wifi", "ubuntu", "wireshark" ]
aliases = [ "/questions/61303" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor Mode works but without promiscuous](/questions/61303/monitor-mode-works-but-without-promiscuous)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61303-score" class="post-score" title="current number of votes">0</div><span id="post-61303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using monitor mode I am able to capture traffic including radio data on any SSID and of the channel of my choosing but only when it is sent as broadcast or multicast - I cant see UNICAST Data sent by other clients.</p><p>I tried setting promiscuous mode on in wireshark as well as using ip link - did not help I am using a supported hardware TP-LINK TL-WN7200ND with a Ralink RT3070 chipset which many indicate is a good choice for the task. It use to work on Ubuntu 14.04 and than I made the mistake of upgrading to 16.04 since I am no longer seeing other client unicasts...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-16.04" rel="tag" title="see questions tagged &#39;16.04&#39;">16.04</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '17, 05:03</strong></p><img src="https://secure.gravatar.com/avatar/0291cee3496699c045822e6d4d13b6a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DAVIDHADAS&#39;s gravatar image" /><p><span>DAVIDHADAS</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DAVIDHADAS has no accepted answers">0%</span></p></div></div><div id="comments-container-61303" class="comments-container"></div><div id="comment-tools-61303" class="comment-tools"></div><div class="clear"></div><div id="comment-61303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61313"></span>

<div id="answer-container-61313" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61313-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61313-score" class="post-score" title="current number of votes">0</div><span id="post-61313-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Perhaps this is your issue?</p><p><a href="https://ask.wireshark.org/questions/53260/cannot-capture-frames-other-than-broadcast-or-multicast-over-wlan">https://ask.wireshark.org/questions/53260/cannot-capture-frames-other-than-broadcast-or-multicast-over-wlan</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '17, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span></p></div></div><div id="comments-container-61313" class="comments-container"></div><div id="comment-tools-61313" class="comment-tools"></div><div class="clear"></div><div id="comment-61313-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

