+++
type = "question"
title = "Analyze roaming voip traffic"
description = '''Hi all, I am having trouble to analyze VOIP in roaming mode. What is the link between the original number anf the one used for roaming. Example: If my sim card is Swiss and I go abroad to Germany. If I call another number in Switzerland I can see a German number used. How to link that roaming number...'''
date = "2017-10-10T03:55:00Z"
lastmod = "2017-10-11T04:40:00Z"
weight = 63790
keywords = [ "roaming", "sip", "voip" ]
aliases = [ "/questions/63790" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Analyze roaming voip traffic](/questions/63790/analyze-roaming-voip-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63790-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63790-score" class="post-score" title="current number of votes">0</div><span id="post-63790-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I am having trouble to analyze VOIP in roaming mode. What is the link between the original number anf the one used for roaming. Example: If my sim card is Swiss and I go abroad to Germany. If I call another number in Switzerland I can see a German number used. How to link that roaming number to my Swiss number.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-roaming" rel="tag" title="see questions tagged &#39;roaming&#39;">roaming</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '17, 03:55</strong></p><img src="https://secure.gravatar.com/avatar/a5c65372acc0dcf7060dd2c17bc4aa27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zmaktouf&#39;s gravatar image" /><p><span>zmaktouf</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zmaktouf has no accepted answers">0%</span></p></div></div><div id="comments-container-63790" class="comments-container"><span id="63804"></span><div id="comment-63804" class="comment"><div id="post-63804-score" class="comment-score"></div><div class="comment-text"><p>How is VoIP related with a SIM card? Do you use VoLTE? And where do you capture the signalling traffic?</p><p>What often happens is that mobile operators use 3rd party services for international calls, and these often change the calling party number to evade regulatory requirements. So the mapping between your Swiss calling number and the calling number which the called party can see is nothing predictable. In the cost-cutting world we live in these days, it is a small holiday if the called party can see the real number of the calling party in an international call.</p></div><div id="comment-63804-info" class="comment-info"><span class="comment-age">(11 Oct '17, 04:40)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-63790" class="comment-tools"></div><div class="clear"></div><div id="comment-63790-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

