+++
type = "question"
title = "How check to slowness in network or in application"
description = '''Hi There, Can you please help me to find out, How to check slowness in the network or application or if my network is reaching properly to destination. Thanks, Ricky'''
date = "2017-03-16T22:15:00Z"
lastmod = "2017-03-19T05:38:00Z"
weight = 60138
keywords = [ "network" ]
aliases = [ "/questions/60138" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How check to slowness in network or in application](/questions/60138/how-check-to-slowness-in-network-or-in-application)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60138-score" class="post-score" title="current number of votes">-1</div><span id="post-60138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi There,</p><p>Can you please help me to find out, How to check slowness in the network or application or if my network is reaching properly to destination.</p><p>Thanks, Ricky</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '17, 22:15</strong></p><img src="https://secure.gravatar.com/avatar/56e8fb418bd6d85f22095205f7f3b693?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rickykochi&#39;s gravatar image" /><p><span>rickykochi</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rickykochi has no accepted answers">0%</span></p></div></div><div id="comments-container-60138" class="comments-container"></div><div id="comment-tools-60138" class="comment-tools"></div><div class="clear"></div><div id="comment-60138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60173"></span>

<div id="answer-container-60173" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60173-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60173-score" class="post-score" title="current number of votes">0</div><span id="post-60173-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Ricky, Not sure this is what you looking for. Try google laura chapell on youtube. you will find a lot of good videos for you to start. :-)</p><p>happy wirehsarking,</p><p>Tatsuya</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Mar '17, 05:38</strong></p><img src="https://secure.gravatar.com/avatar/d996c59b7588b3a83e912942b0db60f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="toor&#39;s gravatar image" /><p><span>toor</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="toor has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Mar '17, 05:39</strong> </span></p></div></div><div id="comments-container-60173" class="comments-container"></div><div id="comment-tools-60173" class="comment-tools"></div><div class="clear"></div><div id="comment-60173-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

