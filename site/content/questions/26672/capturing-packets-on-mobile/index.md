+++
type = "question"
title = "Capturing Packets on Mobile"
description = '''I would like to capture packets of the internet traffic of my mobile. I would like to ask that if I install wire shark on my desktop machine, and then make my desktop a wifi hot spot, connect my mobile phone with this desktop powered wifi then Would I be able to capture/analyse packets of my interne...'''
date = "2013-11-04T23:39:00Z"
lastmod = "2013-11-10T20:20:00Z"
weight = 26672
keywords = [ "mobile", "wifi", "wireshark" ]
aliases = [ "/questions/26672" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Capturing Packets on Mobile](/questions/26672/capturing-packets-on-mobile)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26672-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26672-score" class="post-score" title="current number of votes">0</div><span id="post-26672-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to capture packets of the internet traffic of my mobile. I would like to ask that if I install wire shark on my desktop machine, and then make my desktop a wifi hot spot, connect my mobile phone with this desktop powered wifi then Would I be able to capture/analyse packets of my internet traffic of mobile on wire shark at desktop?</p><p>Your help is highly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '13, 23:39</strong></p><img src="https://secure.gravatar.com/avatar/607bf6a025439b586b58c098aa2976e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zohaibjabbar&#39;s gravatar image" /><p><span>zohaibjabbar</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zohaibjabbar has no accepted answers">0%</span></p></div></div><div id="comments-container-26672" class="comments-container"></div><div id="comment-tools-26672" class="comment-tools"></div><div class="clear"></div><div id="comment-26672-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="26678"></span>

<div id="answer-container-26678" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26678-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26678-score" class="post-score" title="current number of votes">1</div><span id="post-26678-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="zohaibjabbar has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is software available that turns a Windows (or Linux) system into a wireless access point (please search google). So, if you manage to convert your system into a wireless access point (aka Hotspot) and you run Wireshark on that system, you will/should be able to see the traffic that is forwarded through that system.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Nov '13, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Nov '13, 05:30</strong> </span></p></div></div><div id="comments-container-26678" class="comments-container"><span id="26815"></span><div id="comment-26815" class="comment"><div id="post-26815-score" class="comment-score"></div><div class="comment-text"><p>Hi Kurt, thanks for you response. I was able to get the traffic. Can you please let me know that how can I identify that which data packet belongs to which application installed on phone? and how can I analyse the data section? thanks again for your time.</p></div><div id="comment-26815-info" class="comment-info"><span class="comment-age">(10 Nov '13, 00:26)</span> <span class="comment-user userinfo">zohaibjabbar</span></div></div><span id="26816"></span><div id="comment-26816" class="comment"><div id="post-26816-score" class="comment-score">1</div><div class="comment-text"><blockquote><p>how can I identify that which data packet belongs to which application installed on phone?</p></blockquote><p>You can't as the packets don't contain that information. You may find hints in the packets (IP addresses, DNS, certificates, etc.) that give an idea about the application (google, apple, twitter, whatsapp, etc.) but nothing reliable to identify an <strong>application</strong>. With root access to the phone you might be able to get more information (like 'netstat -na'), but that's beyond the socpe of this site.</p><blockquote><p>and how can I analyse the data section?</p></blockquote><p>depends on your needs. A simple way:</p><ul><li>right-click on a packet</li><li>select "Follow TCP Stream" (same for UDP)</li></ul><p>That will show you the transmitted data. If it contains clear text (not encrypted and the like), you can read it. Otherwise (encrypted) there is not much you can analyze.</p></div><div id="comment-26816-info" class="comment-info"><span class="comment-age">(10 Nov '13, 03:03)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-26678" class="comment-tools"></div><div class="clear"></div><div id="comment-26678-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="26829"></span>

<div id="answer-container-26829" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26829-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26829-score" class="post-score" title="current number of votes">0</div><span id="post-26829-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, try tPacketCapture , this does no require any rooting of the device, its freely downloadable from the android market place <a href="https://play.google.com/store/apps/details?id=jp.co.taosoftware.android.packetcapture&amp;hl=en">https://play.google.com/store/apps/details?id=jp.co.taosoftware.android.packetcapture&amp;hl=en</a></p><p>Its fast and efficient, there is a paid version with better features.</p><p>setting-up hotspot on PC is a cumbersome process</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '13, 20:20</strong></p><img src="https://secure.gravatar.com/avatar/3e5e9d76a54debaa630d909e37048da8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deepacket&#39;s gravatar image" /><p><span>deepacket</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deepacket has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Nov '13, 20:21</strong> </span></p></div></div><div id="comments-container-26829" class="comments-container"></div><div id="comment-tools-26829" class="comment-tools"></div><div class="clear"></div><div id="comment-26829-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

