+++
type = "question"
title = "Is there a capture filter for GTP protocol?"
description = '''Is there capture filter that is equivalent to the display filter gtp.user_ipv4 == 10.32.5.6?'''
date = "2012-02-03T04:43:00Z"
lastmod = "2012-02-03T04:43:00Z"
weight = 8805
keywords = [ "gtp", "filter", "capture-filter" ]
aliases = [ "/questions/8805" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a capture filter for GTP protocol?](/questions/8805/is-there-a-capture-filter-for-gtp-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8805-score" class="post-score" title="current number of votes">0</div><span id="post-8805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there capture filter that is equivalent to the display filter <code>gtp.user_ipv4 == 10.32.5.6</code>?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtp" rel="tag" title="see questions tagged &#39;gtp&#39;">gtp</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '12, 04:43</strong></p><img src="https://secure.gravatar.com/avatar/ce0cb3eb62d1eea401fcf1bd03b1f4f5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Majksner81&#39;s gravatar image" /><p><span>Majksner81</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Majksner81 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Feb '12, 07:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-8805" class="comments-container"></div><div id="comment-tools-8805" class="comment-tools"></div><div class="clear"></div><div id="comment-8805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

