+++
type = "question"
title = "Conversations / Ethernet ??"
description = '''Hi! Excuse my English! I am new to using wireshark and I&#x27;m looking on the &quot;Conversations / Ethernet&quot; I have a high consumption of something I do not understand. Supposedly it is a Router but not how to find it. I have your IP. How can I find out who is using this resource? Thank you !!!'''
date = "2015-08-18T08:49:00Z"
lastmod = "2015-08-18T14:09:00Z"
weight = 45203
keywords = [ "conversation" ]
aliases = [ "/questions/45203" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Conversations / Ethernet ??](/questions/45203/conversations-ethernet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45203-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45203-score" class="post-score" title="current number of votes">0</div><span id="post-45203-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! Excuse my English! I am new to using wireshark and I'm looking on the "Conversations / Ethernet" I have a high consumption of something I do not understand. Supposedly it is a Router but not how to find it. I have your IP. How can I find out who is using this resource?</p><p>Thank you !!!<img src="https://osqa-ask.wireshark.org/upfiles/1_47mN9I8.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversation" rel="tag" title="see questions tagged &#39;conversation&#39;">conversation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '15, 08:49</strong></p><img src="https://secure.gravatar.com/avatar/c57cb9db1b3d62e978383a9967070a77?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lsaida&#39;s gravatar image" /><p><span>lsaida</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lsaida has no accepted answers">0%</span></p></img></div></div><div id="comments-container-45203" class="comments-container"></div><div id="comment-tools-45203" class="comment-tools"></div><div class="clear"></div><div id="comment-45203-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45216"></span>

<div id="answer-container-45216" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45216-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45216-score" class="post-score" title="current number of votes">0</div><span id="post-45216-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>"Tp-LinkT" is, from Wireshark's "manuf" file, <a href="http://www.tp-link.com">TP-LINK Technologies Co., Ltd.</a>, who make a number of networking devices. "Giga-Byt" is <a href="http://www.gigabyte.com">Giga-Byte Technology Co., Ltd.</a>, who make a number of motherboards, smartphones, etc..</p><p>You'll have to look at your network - whether physically or virtually (for example, with SNMP) - for one of the devices that they make.</p><p>(For what it's worth, Gigabyte's home page really seems to emphasize games, so maybe this is somebody playing an online video game.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Aug '15, 14:09</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-45216" class="comments-container"></div><div id="comment-tools-45216" class="comment-tools"></div><div class="clear"></div><div id="comment-45216-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

