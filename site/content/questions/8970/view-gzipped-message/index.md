+++
type = "question"
title = "View Gzipped message"
description = '''Given that Wireshark can do everthyng but eat, it seems odd it is not obvious how to ask it to unzip Gzipped responses so I can read the messages. Is the feature actually missing, or did I just overlook it.'''
date = "2012-02-12T16:30:00Z"
lastmod = "2012-02-13T11:08:00Z"
weight = 8970
keywords = [ "gzip" ]
aliases = [ "/questions/8970" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [View Gzipped message](/questions/8970/view-gzipped-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8970-score" class="post-score" title="current number of votes">0</div><span id="post-8970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Given that Wireshark can do everthyng but eat, it seems odd it is not obvious how to ask it to unzip Gzipped responses so I can read the messages. Is the feature actually missing, or did I just overlook it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gzip" rel="tag" title="see questions tagged &#39;gzip&#39;">gzip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '12, 16:30</strong></p><img src="https://secure.gravatar.com/avatar/5fbd015a1887895b5162f7ecea7ae54b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RoedyGreen&#39;s gravatar image" /><p><span>RoedyGreen</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RoedyGreen has no accepted answers">0%</span></p></div></div><div id="comments-container-8970" class="comments-container"><span id="8980"></span><div id="comment-8980" class="comment"><div id="post-8980-score" class="comment-score"></div><div class="comment-text"><p>Gzipped responses in what protocol ?</p><p>ISTR that Gzipped HTTP Content Encoding will be unzipped (and shown in a separate tab).</p></div><div id="comment-8980-info" class="comment-info"><span class="comment-age">(13 Feb '12, 11:08)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-8970" class="comment-tools"></div><div class="clear"></div><div id="comment-8970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

