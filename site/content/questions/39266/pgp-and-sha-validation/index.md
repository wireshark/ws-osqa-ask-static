+++
type = "question"
title = "PGP and SHA validation"
description = '''I was trying to download version 1.12.3 64 bit for the MAC .dmg file. The SHA1 hash is 4ae91fcd3a7b0c0b7fd88170fa35e558442d5682 However the validation via PGP doesn&#x27;t validate. I used the signed public key key id 0x21F2949A and the signature file -----BEGIN PGP SIGNATURE----- Version: GnuPG v1 iEYEA...'''
date = "2015-01-19T00:45:00Z"
lastmod = "2015-01-19T00:45:00Z"
weight = 39266
keywords = [ "validating", "application" ]
aliases = [ "/questions/39266" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [PGP and SHA validation](/questions/39266/pgp-and-sha-validation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39266-score" class="post-score" title="current number of votes">0</div><span id="post-39266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was trying to download version 1.12.3 64 bit for the MAC .dmg file. The SHA1 hash is 4ae91fcd3a7b0c0b7fd88170fa35e558442d5682</p><p>However the validation via PGP doesn't validate. I used the signed public key key id 0x21F2949A and the signature file</p><pre><code>-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1
iEYEARECAAYFAlStpgIACgkQpw8IXSHylJoFvACfY2Q/+UAHG+KwLEOB5mjqezgJ
0oAAoJUX4m53Z28oXcdgpYHtloPeZvwB
=Ti2c
-----END PGP SIGNATURE-----</code></pre>But it will not validate. Can you tell me why?</div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-validating" rel="tag" title="see questions tagged &#39;validating&#39;">validating</span> <span class="post-tag tag-link-application" rel="tag" title="see questions tagged &#39;application&#39;">application</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '15, 00:45</strong></p><img src="https://secure.gravatar.com/avatar/a26768b463fa78c6915bbea890fca049?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mewireshark&#39;s gravatar image" /><p><span>mewireshark</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mewireshark has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jan '15, 07:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-39266" class="comments-container"></div><div id="comment-tools-39266" class="comment-tools"></div><div class="clear"></div><div id="comment-39266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

