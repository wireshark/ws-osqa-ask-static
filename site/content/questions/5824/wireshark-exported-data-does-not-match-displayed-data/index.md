+++
type = "question"
title = "Wireshark exported data does not match displayed data"
description = '''I am using an older version of Wireshark (V1.2.9) so please tell me if this issue has been reported and or fixed: When I export captured Wireshark data, the info field data in the exported file does not match the info field data that is displayed in the screen. I am exporting all packets to a CSV fi...'''
date = "2011-08-23T06:54:00Z"
lastmod = "2011-08-25T09:35:00Z"
weight = 5824
keywords = [ "incorrect", "info" ]
aliases = [ "/questions/5824" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark exported data does not match displayed data](/questions/5824/wireshark-exported-data-does-not-match-displayed-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5824-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5824-score" class="post-score" title="current number of votes">0</div><span id="post-5824-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using an older version of Wireshark (V1.2.9) so please tell me if this issue has been reported and or fixed:</p><p>When I export captured Wireshark data, the info field data in the exported file does not match the info field data that is displayed in the screen. I am exporting all packets to a CSV file but certain packets are displayed with errors on the Wireshark display window but these errors are listed as 0 errors in the actual exported file... I have an image of this but no way to attach it to this question...</p><p>Why doesn't the info in the info field match and how do I get the info field data displayed in the file to match the info field info displayed on the Wireshark screen?</p><p>thanks</p><hr /><p>I just tried to download newer versions of Wireshark (v1.48, 1.60 &amp; 1.61) but each of these gives and errors when loading certain files and the end result is an error message that states items not found in the wiretap_3.0.dll</p><p>What gives?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-incorrect" rel="tag" title="see questions tagged &#39;incorrect&#39;">incorrect</span> <span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '11, 06:54</strong></p><img src="https://secure.gravatar.com/avatar/2ddec3c33e39aa0d05673c1025443604?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="irethedo&#39;s gravatar image" /><p><span>irethedo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="irethedo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Aug '11, 08:41</strong> </span></p></div></div><div id="comments-container-5824" class="comments-container"><span id="5872"></span><div id="comment-5872" class="comment"><div id="post-5872-score" class="comment-score"></div><div class="comment-text"><p>I don't know the answer to your original question.</p><p>However: For the newer Wireshark versions: "... states items not found in the wiretap_3.0.dll" doesn't sound right. I suggest first posting on the <span class="__cf_email__" data-cfemail="bacdd3c8dfc9d2dbc8d197cfc9dfc8c9facdd3c8dfc9d2dbc8d194d5c8dd">[email protected]</span> mailing list showing the "errors when loading certain files" and the error message about "items not found in wiretaop_3.0.dll" so we can see what's going on.</p><p>(See http://www.wireshark.org/lists/)</p><p>Once we get that problem handled we can then address your initial issue.</p><p>Also: Bug reports (which allow attachments) can be made at bugs.wireshark.org</p></div><div id="comment-5872-info" class="comment-info"><span class="comment-age">(25 Aug '11, 09:35)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-5824" class="comment-tools"></div><div class="clear"></div><div id="comment-5824-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

