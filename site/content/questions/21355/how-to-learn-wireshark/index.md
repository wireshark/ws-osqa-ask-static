+++
type = "question"
title = "How to learn wireshark"
description = '''Hi Everyone I&#x27;m new to the forum and I wanted to know can anyone tell me the best way to learn how to use wire shark. I want to learn how to decode the packets and I&#x27;ve never used wire shark before so all of this is new to me. Thanks in advance for the help and I look forward to the response to the ...'''
date = "2013-05-21T11:41:00Z"
lastmod = "2013-05-21T19:42:00Z"
weight = 21355
keywords = [ "learning" ]
aliases = [ "/questions/21355" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to learn wireshark](/questions/21355/how-to-learn-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21355-score" class="post-score" title="current number of votes">0</div><span id="post-21355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Everyone I'm new to the forum and I wanted to know can anyone tell me the best way to learn how to use wire shark. I want to learn how to decode the packets and I've never used wire shark before so all of this is new to me. Thanks in advance for the help and I look forward to the response to the question.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-learning" rel="tag" title="see questions tagged &#39;learning&#39;">learning</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 May '13, 11:41</strong></p><img src="https://secure.gravatar.com/avatar/ef745a628ca5c1cabcecc52c9448ee0e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scooter318&#39;s gravatar image" /><p><span>scooter318</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scooter318 has no accepted answers">0%</span></p></div></div><div id="comments-container-21355" class="comments-container"></div><div id="comment-tools-21355" class="comment-tools"></div><div class="clear"></div><div id="comment-21355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="21356"></span>

<div id="answer-container-21356" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21356-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21356-score" class="post-score" title="current number of votes">1</div><span id="post-21356-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try <a href="http://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark">http://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark</a> for a similar question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 May '13, 11:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-21356" class="comments-container"></div><div id="comment-tools-21356" class="comment-tools"></div><div class="clear"></div><div id="comment-21356-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21363"></span>

<div id="answer-container-21363" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21363-score" class="post-score" title="current number of votes">0</div><span id="post-21363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>One resource would be the user guide: <a href="http://www.wireshark.org/docs/wsug_html_chunked/">http://www.wireshark.org/docs/wsug_html_chunked/</a></p><p>There are also some books and video references if you'd like. That should cover the general use of the tool itself though a lot of it comes down to having a good understanding of the protocol you're analyzing. Wireshark has different uses for different people and organizations for sure, though the mechanics of the GUI and command line tools are pretty well documented.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 May '13, 19:42</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-21363" class="comments-container"></div><div id="comment-tools-21363" class="comment-tools"></div><div class="clear"></div><div id="comment-21363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

