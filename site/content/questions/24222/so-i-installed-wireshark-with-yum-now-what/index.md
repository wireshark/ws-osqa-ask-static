+++
type = "question"
title = "So I installed wireshark with yum... now what?"
description = '''Well this much I&#x27;ve done right: Running transaction  Installing : GeoIP-1.4.8-6.fc19.x86_64 1/3   Installing : libsmi-0.4.8-11.fc19.x86_64 2/3   Installing : wireshark-1.10.0-2.fc19.x86_64 3/3   Verifying : libsmi-0.4.8-11.fc19.x86_64 1/3   Verifying : wireshark-1.10.0-2.fc19.x86_64 2/3   Verifying ...'''
date = "2013-08-31T01:59:00Z"
lastmod = "2013-08-31T09:11:00Z"
weight = 24222
keywords = [ "fedora19", "yum" ]
aliases = [ "/questions/24222" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [So I installed wireshark with yum... now what?](/questions/24222/so-i-installed-wireshark-with-yum-now-what)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24222-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24222-score" class="post-score" title="current number of votes">0</div><span id="post-24222-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Well this much I've done right: Running transaction Installing : GeoIP-1.4.8-6.fc19.x86_64 1/3 Installing : libsmi-0.4.8-11.fc19.x86_64 2/3 Installing : wireshark-1.10.0-2.fc19.x86_64 3/3 Verifying : libsmi-0.4.8-11.fc19.x86_64 1/3 Verifying : wireshark-1.10.0-2.fc19.x86_64 2/3 Verifying : GeoIP-1.4.8-6.fc19.x86_64 3/3</p><p>Installed: wireshark.x86_64 0:1.10.0-2.fc19<br />
</p><p>Dependency Installed: GeoIP.x86_64 0:1.4.8-6.fc19 libsmi.x86_64 0:0.4.8-11.fc19<br />
</p><h1 id="complete">Complete!</h1><p>Only this is what I get after: <span class="__cf_email__" data-cfemail="70111e1616140211131f301c1f13111c181f0304">[email protected]</span> ~]$ wireshark bash: wireshark: command not found...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fedora19" rel="tag" title="see questions tagged &#39;fedora19&#39;">fedora19</span> <span class="post-tag tag-link-yum" rel="tag" title="see questions tagged &#39;yum&#39;">yum</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '13, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/293b20643974266b36e6dc53351a599a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anffdraco&#39;s gravatar image" /><p><span>anffdraco</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anffdraco has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-24222" class="comments-container"></div><div id="comment-tools-24222" class="comment-tools"></div><div class="clear"></div><div id="comment-24222-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24230"></span>

<div id="answer-container-24230" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24230-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24230-score" class="post-score" title="current number of votes">1</div><span id="post-24230-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>yum install wireshark-gnome ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '13, 09:11</strong></p><img src="https://secure.gravatar.com/avatar/d6607c3aca20db751d019d8bbd2da893?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde2&#39;s gravatar image" /><p><span>mrEEde2</span><br />
<span class="score" title="336 reputation points">336</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde2 has 5 accepted answers">20%</span></p></div></div><div id="comments-container-24230" class="comments-container"></div><div id="comment-tools-24230" class="comment-tools"></div><div class="clear"></div><div id="comment-24230-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

