+++
type = "question"
title = "Illegal meta character in http response headers"
description = '''Hello community, BIG-IP can show &#x27;Illegal meta character in header&#x27; alerts and show which characters these are - are they visible using Wireshark? Regards,'''
date = "2017-10-16T05:55:00Z"
lastmod = "2017-10-16T05:55:00Z"
weight = 63932
keywords = [ "header", "security", "meta", "http.response", "characters" ]
aliases = [ "/questions/63932" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Illegal meta character in http response headers](/questions/63932/illegal-meta-character-in-http-response-headers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63932-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63932-score" class="post-score" title="current number of votes">0</div><span id="post-63932-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello community,</p><p>BIG-IP can show 'Illegal meta character in header' alerts and show which characters these are - are they visible using Wireshark?</p><p>Regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span> <span class="post-tag tag-link-http.response" rel="tag" title="see questions tagged &#39;http.response&#39;">http.response</span> <span class="post-tag tag-link-characters" rel="tag" title="see questions tagged &#39;characters&#39;">characters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Oct '17, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/27508138e64a60573b8554b3d734a4db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="t5omeh&#39;s gravatar image" /><p><span>t5omeh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="t5omeh has no accepted answers">0%</span></p></div></div><div id="comments-container-63932" class="comments-container"></div><div id="comment-tools-63932" class="comment-tools"></div><div class="clear"></div><div id="comment-63932-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

