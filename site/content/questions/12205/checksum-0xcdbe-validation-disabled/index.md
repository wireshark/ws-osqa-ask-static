+++
type = "question"
title = "Checksum: 0xcdbe [validation disabled]"
description = '''Does anyone know what this string is using port 1434 for? User Datagram Protocol, Src Port: 54781 (54781), Dst Port: ms-sql-m (1434) Source port: 54781 (54781) Destination port: ms-sql-m (1434) Length: 20 Checksum: 0xcdbe [validation disabled] Good Checksum: False Bad Checksum: False'''
date = "2012-06-26T13:03:00Z"
lastmod = "2012-06-27T13:58:00Z"
weight = 12205
keywords = [ "checksum", "udp" ]
aliases = [ "/questions/12205" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Checksum: 0xcdbe \[validation disabled\]](/questions/12205/checksum-0xcdbe-validation-disabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12205-score" class="post-score" title="current number of votes">0</div><span id="post-12205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know what this string is using port 1434 for?</p><p>User Datagram Protocol, Src Port: 54781 (54781), Dst Port: ms-sql-m (1434) Source port: 54781 (54781) Destination port: ms-sql-m (1434) Length: 20</p><p>Checksum: 0xcdbe [validation disabled] Good Checksum: False Bad Checksum: False</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-checksum" rel="tag" title="see questions tagged &#39;checksum&#39;">checksum</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '12, 13:03</strong></p><img src="https://secure.gravatar.com/avatar/991ca2397da5a59c06ac8972abe3cb0b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wshark6&#39;s gravatar image" /><p><span>Wshark6</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wshark6 has no accepted answers">0%</span></p></div></div><div id="comments-container-12205" class="comments-container"></div><div id="comment-tools-12205" class="comment-tools"></div><div class="clear"></div><div id="comment-12205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12243"></span>

<div id="answer-container-12243" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12243-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12243-score" class="post-score" title="current number of votes">0</div><span id="post-12243-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://msdn.microsoft.com/en-us/library/ms181087%28v=sql.105%29.aspx">SQL Server Browser Service</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '12, 13:58</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-12243" class="comments-container"></div><div id="comment-tools-12243" class="comment-tools"></div><div class="clear"></div><div id="comment-12243-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

