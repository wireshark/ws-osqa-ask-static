+++
type = "question"
title = "How the sniffer detecting tool works ? (in brief)"
description = '''i would like to know that how the sniffer detection tool detects the sniffer machine&#x27;s IP Address on the broadcast domain? Please Dont give an example of nmap tool Thank you! Any Help will be really appreciable'''
date = "2016-10-27T03:38:00Z"
lastmod = "2016-10-28T02:16:00Z"
weight = 56733
keywords = [ "interface", "sniffing", "sniffer", "packets" ]
aliases = [ "/questions/56733" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How the sniffer detecting tool works ? (in brief)](/questions/56733/how-the-sniffer-detecting-tool-works-in-brief)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56733-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56733-score" class="post-score" title="current number of votes">0</div><span id="post-56733-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i would like to know that <strong>how</strong> the sniffer detection tool detects the sniffer machine's IP Address on the broadcast domain? Please Dont give an example of nmap tool Thank you! Any Help will be really appreciable</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '16, 03:38</strong></p><img src="https://secure.gravatar.com/avatar/28bde87c7d3aaf835c3c21c3e77a3d74?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kelvin&#39;s gravatar image" /><p><span>kelvin</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kelvin has no accepted answers">0%</span></p></div></div><div id="comments-container-56733" class="comments-container"></div><div id="comment-tools-56733" class="comment-tools"></div><div class="clear"></div><div id="comment-56733-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56760"></span>

<div id="answer-container-56760" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56760-score" class="post-score" title="current number of votes">0</div><span id="post-56760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://lmgtfy.com/?q=how+to+detect+promiscuous+mode+on+the+network">How about this...</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '16, 15:52</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-56760" class="comments-container"><span id="56769"></span><div id="comment-56769" class="comment"><div id="post-56769-score" class="comment-score">1</div><div class="comment-text"><p>i want how the detection sniffer tool works, not how to detect the sniffer ! and Please i already used google before and all i got it is bullshit so stop giving me such useless suggestions!</p></div><div id="comment-56769-info" class="comment-info"><span class="comment-age">(27 Oct '16, 23:59)</span> <span class="comment-user userinfo">kelvin</span></div></div><span id="56778"></span><div id="comment-56778" class="comment"><div id="post-56778-score" class="comment-score">1</div><div class="comment-text"><p>Hi Kelvin, I already assumed that you used Google to find an answer, so I also assumed you did not succeed in finding the answer. So instead of just posting the answer after googling it myself I wanted to help you with improving your Google skills by showing you how I was able to find the answer. With my search I have found a <a href="http://security.stackexchange.com/questions/3630/how-to-find-out-that-a-nic-is-in-promiscuous-mode-on-a-lan">link to stackexcgange</a> which had the following info:</p><p><strong>DNS test</strong> - many packet sniffing tools perform IP address to name lookups to provide DNS names in place of IP addresses. To test this, you must place your network card into promiscuous mode and sends packets out onto the network aimed to bogus hosts. If any name lookups from the bogus hosts are seen, a sniffer might be in action on the host performing the lookups.</p><p><strong>ARP Test</strong> - When in promiscuous mode the driver for the network card checks for the MAC address being that of the network card for unicast packets, but only checks the first octet of the MAC address against the value 0xff to determine if the packet is broadcast or not. Note that the address for a broadcast packet is ff:ff:ff:ff:ff:ff. To test for this flaw, if you send a packet with a MAC address of ff:00:00:00:00:00 and the correct destination IP address of the host. After receiving a packet, the Microsoft OS using the flawed driver will respond while in promiscuous mode. Probably it happens just with the default MS driver.</p><p><strong>Ether Ping test</strong> - In older Linux kernels when a network card is placed in promiscuous mode every packet is passed on to the OS. Some Linux kernels looked only at the IP address in the packets to determine whether they should be processed or not. To test for this flaw, you have to send a packet with a bogus MAC address and a valid IP address. Vulnerable Linux kernels with their network cards in promiscuous mode only look at the valid IP address. To get a response, an ICMP echo request message is sent within the bogus packet leading to vulnerable hosts in promiscuous mode to respond.</p><p>Maybe there are more, the DNS test for me is the most reliable</p><p>Which I believe is the answer to your question. So I'm really sorry if you misunderstood my attempt to help you not only get your answer, but also improve your google skills at the same time...</p></div><div id="comment-56778-info" class="comment-info"><span class="comment-age">(28 Oct '16, 02:16)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-56760" class="comment-tools"></div><div class="clear"></div><div id="comment-56760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

