+++
type = "question"
title = "Is there a commercial version of OUI Look up tool ?"
description = '''Hi I am developing a product that requires OUI database. Is there any commercial version of OUI look up tool from Wireshark ?'''
date = "2015-12-09T14:47:00Z"
lastmod = "2015-12-09T22:53:00Z"
weight = 48394
keywords = [ "oui" ]
aliases = [ "/questions/48394" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a commercial version of OUI Look up tool ?](/questions/48394/is-there-a-commercial-version-of-oui-look-up-tool)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48394-score" class="post-score" title="current number of votes">0</div><span id="post-48394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am developing a product that requires OUI database. Is there any commercial version of OUI look up tool from Wireshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-oui" rel="tag" title="see questions tagged &#39;oui&#39;">oui</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '15, 14:47</strong></p><img src="https://secure.gravatar.com/avatar/282d2fd2d1390406fbc05250eea4c8f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dandavathis&#39;s gravatar image" /><p><span>dandavathis</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dandavathis has no accepted answers">0%</span></p></div></div><div id="comments-container-48394" class="comments-container"></div><div id="comment-tools-48394" class="comment-tools"></div><div class="clear"></div><div id="comment-48394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48410"></span>

<div id="answer-container-48410" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48410-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48410-score" class="post-score" title="current number of votes">0</div><span id="post-48410-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi. It looks like the OUI database is freely available from the IEEE. You might be able to use curl or wget to pull a copy from <a href="http://standards.ieee.org/regauth/oui/oui.txt">http://standards.ieee.org/regauth/oui/oui.txt</a> Then use grep, awk, or similar to parse out what you're looking for.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '15, 22:53</strong></p><img src="https://secure.gravatar.com/avatar/b260fb38b621169269b5030f1ed6b766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griff&#39;s gravatar image" /><p><span>griff</span><br />
<span class="score" title="361 reputation points">361</span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griff has 2 accepted answers">10%</span> </br></p></div></div><div id="comments-container-48410" class="comments-container"></div><div id="comment-tools-48410" class="comment-tools"></div><div class="clear"></div><div id="comment-48410-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

