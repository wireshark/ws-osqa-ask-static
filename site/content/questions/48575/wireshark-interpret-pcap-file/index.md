+++
type = "question"
title = "Wireshark interpret pcap file"
description = '''Hi Wireshark-er :D I&#x27;m had build my own profile, ping and it work great with other pcap files, but when come to this Nexus vpc peer link capture, whenever I using my ping profile initiate open any nexus vpc peer link file or begin to sniff , the wireshark interpret as below pic shown   When I change...'''
date = "2015-12-16T06:51:00Z"
lastmod = "2015-12-16T17:18:00Z"
weight = 48575
keywords = [ "nexus", "cisco", "vpc" ]
aliases = [ "/questions/48575" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark interpret pcap file](/questions/48575/wireshark-interpret-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48575-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48575-score" class="post-score" title="current number of votes">0</div><span id="post-48575-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Wireshark-er :D</p><p>I'm had build my own profile, <strong>ping</strong> and it work great with other pcap files, but when come to this Nexus vpc peer link capture, whenever I using my ping profile initiate open any nexus vpc peer link file or begin to sniff , the wireshark interpret as below pic shown</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Wireshark_1_m66e2v4.jpg" alt="alt text" /></p><p>When I change my to my other profile from ping to Han(TCP), it interpret correctly as pic shown below or I switch back to my ping profile it also look okay.</p><p>Bug ? This only happen to vPC peer link capture and files</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Wireshark_2_w1mvgwi.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nexus" rel="tag" title="see questions tagged &#39;nexus&#39;">nexus</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-vpc" rel="tag" title="see questions tagged &#39;vpc&#39;">vpc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '15, 06:51</strong></p><img src="https://secure.gravatar.com/avatar/ee15000690effb5f7ee000775e3bfe76?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="limvuihan&#39;s gravatar image" /><p><span>limvuihan</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="limvuihan has no accepted answers">0%</span></p></img></div></div><div id="comments-container-48575" class="comments-container"><span id="48591"></span><div id="comment-48591" class="comment"><div id="post-48591-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Bug ?</p></blockquote><p>We'd have to see the detailed dissection of the packets to determine whether this is the result of a bug. It would be best if you made the raw capture file available, rather than attaching a screenshot.</p></div><div id="comment-48591-info" class="comment-info"><span class="comment-age">(16 Dec '15, 17:18)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-48575" class="comment-tools"></div><div class="clear"></div><div id="comment-48575-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

