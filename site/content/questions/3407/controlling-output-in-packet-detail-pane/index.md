+++
type = "question"
title = "Controlling output in packet detail pane"
description = '''Is there a way to customize the output of the packet detail pane?  What I&#x27;d like to do is be able to inspect certain fields of a protocol within a packet for a set of packets (conversation, all IP, all tftp, all from address to address...). I know I can do some drill down manually on a per packet ba...'''
date = "2011-04-08T10:32:00Z"
lastmod = "2011-04-08T13:37:00Z"
weight = 3407
keywords = [ "detailpane" ]
aliases = [ "/questions/3407" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Controlling output in packet detail pane](/questions/3407/controlling-output-in-packet-detail-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3407-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3407-score" class="post-score" title="current number of votes">0</div><span id="post-3407-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to customize the output of the packet detail pane?</p><p>What I'd like to do is be able to inspect certain fields of a protocol within a packet for a set of packets (conversation, all IP, all tftp, all from address to address...). I know I can do some drill down manually on a per packet basis but even the control of the format that way is limited.</p><p>Thanks in advance</p><p>Roger Hughs <span class="__cf_email__" data-cfemail="4735282022352f32202f3407202a262e2b6924282a">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-detailpane" rel="tag" title="see questions tagged &#39;detailpane&#39;">detailpane</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '11, 10:32</strong></p><img src="https://secure.gravatar.com/avatar/49c62d09f684d8e7b3fdb8489749a552?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hose&#39;s gravatar image" /><p><span>hose</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hose has no accepted answers">0%</span></p></div></div><div id="comments-container-3407" class="comments-container"></div><div id="comment-tools-3407" class="comment-tools"></div><div class="clear"></div><div id="comment-3407-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3408"></span>

<div id="answer-container-3408" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3408-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3408-score" class="post-score" title="current number of votes">2</div><span id="post-3408-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not exactly sure what you want to do, but I read it like this: you want to be able to take a look at certain decode fields for multiple packets at the same time in the decode pane?</p><p>If so, I don't think it's possible; Wireshark just decodes the one packet that is selected, not more. What you might do is add the fields you want to inspect as columns using custom column settings - that way you can see the values in the packet list for as many packets as you can cram into your view.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '11, 13:37</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-3408" class="comments-container"></div><div id="comment-tools-3408" class="comment-tools"></div><div class="clear"></div><div id="comment-3408-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

