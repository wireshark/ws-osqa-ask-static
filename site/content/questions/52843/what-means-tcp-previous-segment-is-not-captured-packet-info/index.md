+++
type = "question"
title = "What means &#x27;TCP Previous Segment is not captured&#x27; packet info?"
description = '''I have captured the packets and some packet was marked &quot;TCP Previous Segment is no captured&#x27;  I am wondering whether this marked packet is wrong packet itself or just Previous Packet is loss and marked packet is correct packet. I have attached &quot;TCP Previous Segment is no captured&quot; marked packet.  th...'''
date = "2016-05-23T22:12:00Z"
lastmod = "2016-05-25T01:23:00Z"
weight = 52843
keywords = [ "packet-capture", "tcp-segment", "packetloss" ]
aliases = [ "/questions/52843" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What means 'TCP Previous Segment is not captured' packet info?](/questions/52843/what-means-tcp-previous-segment-is-not-captured-packet-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52843-score" class="post-score" title="current number of votes">0</div><span id="post-52843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured the packets and some packet was marked "TCP Previous Segment is no captured'</p><p>I am wondering whether this marked packet is wrong packet itself or just Previous Packet is loss and marked packet is correct packet.</p><p>I have attached "TCP Previous Segment is no captured" marked packet.<br />
</p><p>thank you for your help.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/test.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-tcp-segment" rel="tag" title="see questions tagged &#39;tcp-segment&#39;">tcp-segment</span> <span class="post-tag tag-link-packetloss" rel="tag" title="see questions tagged &#39;packetloss&#39;">packetloss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '16, 22:12</strong></p><img src="https://secure.gravatar.com/avatar/d061a8f87e43eba3709731ee538b010a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DSLab&#39;s gravatar image" /><p><span>DSLab</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DSLab has no accepted answers">0%</span> </br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 May '16, 03:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-52843" class="comments-container"><span id="52846"></span><div id="comment-52846" class="comment"><div id="post-52846-score" class="comment-score">1</div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-52846-info" class="comment-info"><span class="comment-age">(23 May '16, 23:29)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="52851"></span><div id="comment-52851" class="comment"><div id="post-52851-score" class="comment-score"></div><div class="comment-text"><p>yes. let me know your e-mail !</p></div><div id="comment-52851-info" class="comment-info"><span class="comment-age">(24 May '16, 00:00)</span> <span class="comment-user userinfo">DSLab</span></div></div><span id="52856"></span><div id="comment-52856" class="comment"><div id="post-52856-score" class="comment-score"></div><div class="comment-text"><p>"sharing a capture" on this site means publishing it, login-free, at some publicly accessible file server, or preferably Cloudshark as <span>@Jaap</span> has suggested, and providing a link to it here.</p><p>If you have some privacy concerns, use Tracewrangler to remove payload and replace IP addresses with random ones before publishing the capture.</p></div><div id="comment-52856-info" class="comment-info"><span class="comment-age">(24 May '16, 02:58)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-52843" class="comment-tools"></div><div class="clear"></div><div id="comment-52843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52858"></span>

<div id="answer-container-52858" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52858-score" class="post-score" title="current number of votes">1</div><span id="post-52858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If packet N is marked with <code>previous segment not captured</code>, it means that in the capture there is no packet from the same TCP session whose <code>seq + length</code> would match the <code>seq</code> of packet N. The most typical reason is packet loss and/or late start of capture, which is the reason why the wording in question is used. But there can eventually be other reasons (buggy TCP stack of the sender, multipath network structure allowing packets belonging to the same TCP session to pass through different network interfaces so the packets do reach their destination but Wireshark cannot see them, ...), so it is up to you to check out the real reason why this has happened in your particular case. If in doubt, post the capture as <span>@Jaap</span> has suggested.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 May '16, 03:03</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-52858" class="comments-container"><span id="52893"></span><div id="comment-52893" class="comment"><div id="post-52893-score" class="comment-score"></div><div class="comment-text"><p>thanks. but I can not english very well. and this is cloudshark link.</p><p><a href="https://www.cloudshark.org/captures/af199d964ad2">https://www.cloudshark.org/captures/af199d964ad2</a> this pcap file has included privacy.</p><p>If I get the answer, then I remove the link and pcap file.</p><p>please help me.</p></div><div id="comment-52893-info" class="comment-info"><span class="comment-age">(24 May '16, 20:36)</span> <span class="comment-user userinfo">DSLab</span></div></div><span id="52895"></span><div id="comment-52895" class="comment"><div id="post-52895-score" class="comment-score"></div><div class="comment-text"><p>Thank for sharing the capture file.</p><p>A quick glance at it reveals many occurrences of <code>TCP Previous Segment not captured</code>. And indeed if you look at the TCP sequence numbers and lengths, as <span>@sindy</span> suggested, these seem to be absent from the capture file.</p></div><div id="comment-52895-info" class="comment-info"><span class="comment-age">(24 May '16, 23:47)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="52897"></span><div id="comment-52897" class="comment"><div id="post-52897-score" class="comment-score"></div><div class="comment-text"><p>As I'm afraid not many contributors to this site are fluent in Korean, you may copy those sentences from my answer which do not make sense to you into your next comment, I'll try to say the same once again but in more simple words.</p></div><div id="comment-52897-info" class="comment-info"><span class="comment-age">(25 May '16, 01:23)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-52858" class="comment-tools"></div><div class="clear"></div><div id="comment-52858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

