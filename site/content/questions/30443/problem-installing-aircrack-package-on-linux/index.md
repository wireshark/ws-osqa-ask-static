+++
type = "question"
title = "[closed] Problem installing aircrack package on Linux"
description = '''Hi i have got an error. sudo: airmon-ng: command not found I tryed to install aircrack packet but i have allready installed &quot;iw&quot; packet but i dont know what that packet is? If someone could help me :) thanks for help'''
date = "2014-03-05T08:33:00Z"
lastmod = "2014-03-05T08:33:00Z"
weight = 30443
keywords = [ "airmon", "wlan" ]
aliases = [ "/questions/30443" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Problem installing aircrack package on Linux](/questions/30443/problem-installing-aircrack-package-on-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30443-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30443-score" class="post-score" title="current number of votes">0</div><span id="post-30443-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i have got an error.</p><p>sudo: airmon-ng: command not found I tryed to install aircrack packet but i have allready installed "iw" packet but i dont know what that packet is? If someone could help me :)</p><p>thanks for help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airmon" rel="tag" title="see questions tagged &#39;airmon&#39;">airmon</span> <span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '14, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/02bfcf9ef119a526e187ef0550113711?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beginer&#39;s gravatar image" /><p><span>Beginer</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beginer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>05 Mar '14, 10:17</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-30443" class="comments-container"></div><div id="comment-tools-30443" class="comment-tools"></div><div class="clear"></div><div id="comment-30443-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "well, this is a Wireshark Q&A site, not a Linux or Aircrack support forum. Please fix your airmon-ng \*\*installation problem\*\* with the help of other forums, then come back if you have any questions regarding wireless capturing and/or packet analysis with \*\*Wireshark\*\*" by Kurt Knochner 05 Mar '14, 10:17

</div>

</div>

</div>

