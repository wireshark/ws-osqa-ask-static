+++
type = "question"
title = "Can wireshark monitor per application easily?"
description = '''I know I can add some application tagging in iptables and filter on that, but I&#x27;m wondering if there&#x27;s any easy way to see the traffic of 1 application specifically. It would save me a lot of noise filtering when debugging since some of the work I do involves encrypted protocols with random ports...'''
date = "2013-05-09T09:08:00Z"
lastmod = "2013-05-09T09:48:00Z"
weight = 21059
keywords = [ "filter", "application", "filtering", "some", "idssco" ]
aliases = [ "/questions/21059" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can wireshark monitor per application easily?](/questions/21059/can-wireshark-monitor-per-application-easily)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21059-score" class="post-score" title="current number of votes">0</div><span id="post-21059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know I can add some application tagging in iptables and filter on that, but I'm wondering if there's any easy way to see the traffic of 1 application specifically.</p><p>It would save me a lot of noise filtering when debugging since some of the work I do involves encrypted protocols with random ports...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-application" rel="tag" title="see questions tagged &#39;application&#39;">application</span> <span class="post-tag tag-link-filtering" rel="tag" title="see questions tagged &#39;filtering&#39;">filtering</span> <span class="post-tag tag-link-some" rel="tag" title="see questions tagged &#39;some&#39;">some</span> <span class="post-tag tag-link-idssco" rel="tag" title="see questions tagged &#39;idssco&#39;">idssco</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '13, 09:08</strong></p><img src="https://secure.gravatar.com/avatar/9842122bc427d15b32bce91b8a608c5f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wolph&#39;s gravatar image" /><p><span>Wolph</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wolph has no accepted answers">0%</span></p></div></div><div id="comments-container-21059" class="comments-container"><span id="21063"></span><div id="comment-21063" class="comment"><div id="post-21063-score" class="comment-score"></div><div class="comment-text"><p>what OS are we talking about? Linux (as you mentioned iptables)?</p></div><div id="comment-21063-info" class="comment-info"><span class="comment-age">(09 May '13, 09:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-21059" class="comment-tools"></div><div class="clear"></div><div id="comment-21059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

