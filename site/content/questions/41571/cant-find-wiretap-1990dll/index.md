+++
type = "question"
title = "can&#x27;t find wiretap-1.99.0.dll"
description = '''I have compiled the sources.And run wireshark-gtk.exe,it pop tips can&#x27;t not found wiretap.1.99.0.dll.Could you help me? I have tried many times.'''
date = "2015-04-19T08:20:00Z"
lastmod = "2015-04-19T18:09:00Z"
weight = 41571
keywords = [ "wiretap" ]
aliases = [ "/questions/41571" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can't find wiretap-1.99.0.dll](/questions/41571/cant-find-wiretap-1990dll)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41571-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41571-score" class="post-score" title="current number of votes">0</div><span id="post-41571-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have compiled the sources.And run wireshark-gtk.exe,it pop tips can't not found wiretap.1.99.0.dll.Could you help me? I have tried many times.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wiretap" rel="tag" title="see questions tagged &#39;wiretap&#39;">wiretap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '15, 08:20</strong></p><img src="https://secure.gravatar.com/avatar/d4eab55adb51186bd1e0e5c981d1496b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="qife&#39;s gravatar image" /><p><span>qife</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="qife has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Apr '15, 13:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-41571" class="comments-container"></div><div id="comment-tools-41571" class="comment-tools"></div><div class="clear"></div><div id="comment-41571-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41573"></span>

<div id="answer-container-41573" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41573-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41573-score" class="post-score" title="current number of votes">1</div><span id="post-41573-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please run the copy found in wireshark-gtk2 folder (that contains all what is required for Wiresahrk to execute properly) and not at the root folder.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '15, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-41573" class="comments-container"><span id="41579"></span><div id="comment-41579" class="comment"><div id="post-41579-score" class="comment-score"></div><div class="comment-text"><p>As detailed in <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSrcRunFirstTime.html#ChSrcRunFirstTimeWin32">sect 3.6.2</a> of the developers guide.</p></div><div id="comment-41579-info" class="comment-info"><span class="comment-age">(19 Apr '15, 13:18)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="41580"></span><div id="comment-41580" class="comment"><div id="post-41580-score" class="comment-score"></div><div class="comment-text"><p>Thank you! I found the copy in wireshark-gtk2 folder and succeeded to run it.</p></div><div id="comment-41580-info" class="comment-info"><span class="comment-age">(19 Apr '15, 18:09)</span> <span class="comment-user userinfo">qife</span></div></div></div><div id="comment-tools-41573" class="comment-tools"></div><div class="clear"></div><div id="comment-41573-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

