+++
type = "question"
title = "[closed] SIMPLE decoding"
description = '''Please, I beg you, can someone help me with this?  I think all the tutorials and stuff out there are totally confusing, cant somebody provide a SIMPLE example for a SIMPLE problem.  IP, UDP, [My protocol], RTP  Say my protocol is ONE byte, for simplicity. How can I display this byte, and then have t...'''
date = "2012-11-08T02:58:00Z"
lastmod = "2012-11-08T06:31:00Z"
weight = 15702
keywords = [ "lua", "dissector" ]
aliases = [ "/questions/15702" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] SIMPLE decoding](/questions/15702/simple-decoding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15702-score" class="post-score" title="current number of votes">0</div><span id="post-15702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please, I beg you, can someone help me with this?</p><p>I think all the tutorials and stuff out there are totally confusing, cant somebody provide a SIMPLE example for a SIMPLE problem.</p><p>IP, UDP, [My protocol], RTP</p><p>Say my protocol is ONE byte, for simplicity. How can I display this byte, and then have the normal RTP display? I do not understand this.</p><p>The result I want to have is (seen in wireshark) :</p><pre><code> Frame 22... 
 Ethernet II... 
 Internet Protocol Version 4...
 User Datagram Protocol... 
 My Protocol
    My byte: 1 
 Real-Time Transport Protocol
    Rtp Version : 2 
 ...</code></pre><p>If there was a possibility I would PAY somebody to help me with this!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '12, 02:58</strong></p><img src="https://secure.gravatar.com/avatar/7709c0c87ed4ba426f119187d3f0c705?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="harkap&#39;s gravatar image" /><p><span>harkap</span><br />
<span class="score" title="5 reputation points">5</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="harkap has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>08 Nov '12, 06:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-15702" class="comments-container"><span id="15716"></span><div id="comment-15716" class="comment"><div id="post-15716-score" class="comment-score"></div><div class="comment-text"><p>I'll make some time tonight to help you out (unless someone provides an answer first).</p></div><div id="comment-15716-info" class="comment-info"><span class="comment-age">(08 Nov '12, 06:31)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-15702" class="comment-tools"></div><div class="clear"></div><div id="comment-15702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question: http://ask.wireshark.org/questions/15614/how-to-tell-dissector-to-read-after-my-protocol" by helloworld 08 Nov '12, 06:29

</div>

</div>

</div>

