+++
type = "question"
title = "problem installing winpcap on windows 8.1"
description = '''Hello, I am running windows 8.1 and am trying to install wireshark 1.99 but when it gets to the installation of winpcap 4.1.3, after clicking on the install button I get a message saying that an older version of winpcap is running close all programs that are using it and try again. How do I know whi...'''
date = "2015-03-01T02:57:00Z"
lastmod = "2015-03-01T04:03:00Z"
weight = 40163
keywords = [ "winpcap", "npf" ]
aliases = [ "/questions/40163" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [problem installing winpcap on windows 8.1](/questions/40163/problem-installing-winpcap-on-windows-81)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40163-score" class="post-score" title="current number of votes">0</div><span id="post-40163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am running windows 8.1 and am trying to install wireshark 1.99 but when it gets to the installation of winpcap 4.1.3, after clicking on the install button I get a message saying that an older version of winpcap is running close all programs that are using it and try again.</p><p>How do I know which programs are using it, and where did it come from? Is there a way to by-pass this error and get winpcap installed because if I try to run tshark, I get an error saying the npf driver is not running but there is no npf.sys in c:\windows\system32\drivers nor is there an npf service in the services database.</p><p>Any help would be greatly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-npf" rel="tag" title="see questions tagged &#39;npf&#39;">npf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '15, 02:57</strong></p><img src="https://secure.gravatar.com/avatar/7ea73b8266e267e80d6f2724f1b7a4c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dnraikes&#39;s gravatar image" /><p><span>dnraikes</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dnraikes has no accepted answers">0%</span></p></div></div><div id="comments-container-40163" class="comments-container"></div><div id="comment-tools-40163" class="comment-tools"></div><div class="clear"></div><div id="comment-40163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40164"></span>

<div id="answer-container-40164" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40164-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40164-score" class="post-score" title="current number of votes">0</div><span id="post-40164-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This has been reported a few times, e.g. <a href="https://ask.wireshark.org/questions/9229/winpcap-will-not-install">here</a>.</p><p>It's likely that something else has installed it's own version of WinPCap, maybe even under another name. You'll have to track down the offender and stop\remove it. I think Metasploit installs it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Mar '15, 04:03</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-40164" class="comments-container"></div><div id="comment-tools-40164" class="comment-tools"></div><div class="clear"></div><div id="comment-40164-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

