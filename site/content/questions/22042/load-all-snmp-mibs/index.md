+++
type = "question"
title = "Load all SNMP MIBs"
description = '''Hi, I&#x27;m trying to load all Cisco propriatery MIBs in Wireshark. I copied the MIBs in the snmp/mibs folder, and i preferences, I specified ALL (as indicated at http://wiki.wireshark.org/SNMP) I can&#x27;t seem to load them all. I used ALL, &quot;ALL&quot; positioned both at the beginning and at the end of the list....'''
date = "2013-06-14T00:46:00Z"
lastmod = "2013-06-14T03:21:00Z"
weight = 22042
keywords = [ "mib", "snmp" ]
aliases = [ "/questions/22042" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Load all SNMP MIBs](/questions/22042/load-all-snmp-mibs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22042-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22042-score" class="post-score" title="current number of votes">0</div><span id="post-22042-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm trying to load all Cisco propriatery MIBs in Wireshark. I copied the MIBs in the snmp/mibs folder, and i preferences, I specified ALL (as indicated at <a href="http://wiki.wireshark.org/SNMP)">http://wiki.wireshark.org/SNMP)</a> I can't seem to load them all. I used ALL, "ALL" positioned both at the beginning and at the end of the list.</p><p>What am I missing? Thanks, Vlad</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mib" rel="tag" title="see questions tagged &#39;mib&#39;">mib</span> <span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 00:46</strong></p><img src="https://secure.gravatar.com/avatar/5e0f7d71743d66669395e6e2d6539216?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vlad%20Vlaicu&#39;s gravatar image" /><p><span>Vlad Vlaicu</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vlad Vlaicu has no accepted answers">0%</span></p></div></div><div id="comments-container-22042" class="comments-container"></div><div id="comment-tools-22042" class="comment-tools"></div><div class="clear"></div><div id="comment-22042-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22050"></span>

<div id="answer-container-22050" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22050-score" class="post-score" title="current number of votes">1</div><span id="post-22050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Vlad Vlaicu has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see the answer of a similar question.</p><blockquote><p><code>http://ask.wireshark.org/questions/18702/problem-with-smi_modules-i-would-like-to-set-all</code><br />
</p></blockquote><p>long story short: ALL is not supported by libsmi.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jun '13, 03:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Jun '13, 04:30</strong> </span></p></div></div><div id="comments-container-22050" class="comments-container"><span id="22051"></span><div id="comment-22051" class="comment"><div id="post-22051-score" class="comment-score"></div><div class="comment-text"><p>Thank you Kurt.</p></div><div id="comment-22051-info" class="comment-info"><span class="comment-age">(14 Jun '13, 03:21)</span> <span class="comment-user userinfo">Vlad Vlaicu</span></div></div></div><div id="comment-tools-22050" class="comment-tools"></div><div class="clear"></div><div id="comment-22050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

