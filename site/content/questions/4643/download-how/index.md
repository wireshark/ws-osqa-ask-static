+++
type = "question"
title = "Download - how?"
description = '''How do I download - your &quot;download&quot; page has no &quot;download/start/go/whatever&quot; hotspot/button/command, so I haven&#x27;t been able to get Wireshark. Cheers, Ian.'''
date = "2011-06-21T02:21:00Z"
lastmod = "2011-06-21T05:36:00Z"
weight = 4643
keywords = [ "download" ]
aliases = [ "/questions/4643" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Download - how?](/questions/4643/download-how)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4643-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4643-score" class="post-score" title="current number of votes">0</div><span id="post-4643-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I download - your "download" page has no "download/start/go/whatever" hotspot/button/command, so I haven't been able to get Wireshark.</p><p>Cheers, Ian.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jun '11, 02:21</strong></p><img src="https://secure.gravatar.com/avatar/957908b6505c811fae5472eb453bb5ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ianbatty&#39;s gravatar image" /><p><span>ianbatty</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ianbatty has no accepted answers">0%</span></p></div></div><div id="comments-container-4643" class="comments-container"></div><div id="comment-tools-4643" class="comment-tools"></div><div class="clear"></div><div id="comment-4643-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4644"></span>

<div id="answer-container-4644" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4644-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4644-score" class="post-score" title="current number of votes">2</div><span id="post-4644-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>Navigate your web browser to <a href="http://www.wireshark.org/download.html">http://www.wireshark.org/download.html</a></li><li>Select your operating system from the list in the box with the green toolbar in the center of the page</li><li>Click on "save file" in the dialog box that opens.</li><li>Or go here: <a href="http://www.wireshark.org/download/">http://www.wireshark.org/download/</a></li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jun '11, 05:36</strong></p><img src="https://secure.gravatar.com/avatar/b260fb38b621169269b5030f1ed6b766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griff&#39;s gravatar image" /><p><span>griff</span><br />
<span class="score" title="361 reputation points">361</span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griff has 2 accepted answers">10%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Jun '11, 15:55</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4644" class="comments-container"></div><div id="comment-tools-4644" class="comment-tools"></div><div class="clear"></div><div id="comment-4644-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

