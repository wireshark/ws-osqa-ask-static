+++
type = "question"
title = "Print Packet Comments?"
description = '''Is there a way to export or print a report of all the packet comments of a capture without everything else in the packet? Thanks, Travis'''
date = "2014-03-20T16:02:00Z"
lastmod = "2014-03-20T19:42:00Z"
weight = 31019
keywords = [ "comments", "packet" ]
aliases = [ "/questions/31019" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Print Packet Comments?](/questions/31019/print-packet-comments)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31019-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31019-score" class="post-score" title="current number of votes">0</div><span id="post-31019-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to export or print a report of all the packet comments of a capture without everything else in the packet?</p><p>Thanks, Travis</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-comments" rel="tag" title="see questions tagged &#39;comments&#39;">comments</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '14, 16:02</strong></p><img src="https://secure.gravatar.com/avatar/bb79e0c62df46ecf47cc004a0a2d3cbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rooster_50&#39;s gravatar image" /><p><span>Rooster_50</span><br />
<span class="score" title="238 reputation points">238</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="18 badges"><span class="bronze">●</span><span class="badgecount">18</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rooster_50 has 5 accepted answers">15%</span></p></div></div><div id="comments-container-31019" class="comments-container"></div><div id="comment-tools-31019" class="comment-tools"></div><div class="clear"></div><div id="comment-31019-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="31034"></span>

<div id="answer-container-31034" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31034-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31034-score" class="post-score" title="current number of votes">0</div><span id="post-31034-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Rooster_50 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>After doing some more searching, I noticed under <strong>STATISTICS | Comment Summary</strong> that you are given a text printout of all the trace and packet comments. This is exactly what I was looking for.</p><p>I guess I just needed to look a bit harder. I was stuck within the FILE and EDIT menus and never gave the STATISTICS menu a thought.</p><p>Travis</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '14, 19:40</strong></p><img src="https://secure.gravatar.com/avatar/bb79e0c62df46ecf47cc004a0a2d3cbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rooster_50&#39;s gravatar image" /><p><span>Rooster_50</span><br />
<span class="score" title="238 reputation points">238</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="18 badges"><span class="bronze">●</span><span class="badgecount">18</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rooster_50 has 5 accepted answers">15%</span></p></div></div><div id="comments-container-31034" class="comments-container"></div><div id="comment-tools-31034" class="comment-tools"></div><div class="clear"></div><div id="comment-31034-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="31021"></span>

<div id="answer-container-31021" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31021-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31021-score" class="post-score" title="current number of votes">1</div><span id="post-31021-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could add a custom column containing the packet comments: Unfold the top section of any packet, select the line containing the comment and use the popup menu to "Apply as Column".</p><p>Then export the packet list to csv. That list can then be printed with any editor (or spreadsheet tool) of your choice.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '14, 16:24</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-31021" class="comments-container"><span id="31024"></span><div id="comment-31024" class="comment"><div id="post-31024-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper....Thats a good idea.</p><p>I also just now found under: STATISTICS | Comment Summary</p><p>I guess I just needed to look a bit harder. I was stuck under the FILE and EDIT menus.</p><p>Thanks again for your added info.</p><p>Travis</p></div><div id="comment-31024-info" class="comment-info"><span class="comment-age">(20 Mar '14, 16:33)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-31021" class="comment-tools"></div><div class="clear"></div><div id="comment-31021-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="31025"></span>

<div id="answer-container-31025" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31025-score" class="post-score" title="current number of votes">1</div><span id="post-31025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Another solution would be to use <a href="http://www.wireshark.org/docs/man-pages/tshark.html"><code>tshark</code></a>. For example:</p><pre><code>tshark -r file.pcapng -T fields -E header=y -e frame.number -e frame.comment</code></pre><p>... and if you <strong><em>only</em></strong> want the frames that contain comments, then you can use something like:</p><pre><code>tshark -r file.pcapng -Y frame.comment -T fields -E header=y -e frame.number -e frame.comment</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '14, 16:42</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Mar '14, 16:45</strong> </span></p></div></div><div id="comments-container-31025" class="comments-container"><span id="31035"></span><div id="comment-31035" class="comment"><div id="post-31035-score" class="comment-score"></div><div class="comment-text"><p>cmaynard,</p><p>Thanks for the additional tip using tshark.</p></div><div id="comment-31035-info" class="comment-info"><span class="comment-age">(20 Mar '14, 19:42)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-31025" class="comment-tools"></div><div class="clear"></div><div id="comment-31025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

