+++
type = "question"
title = "How does Wireshark know the protocol underlying the SCCP ?"
description = '''I&#x27;m asking how does Wireshark know that there is BSSAP underlying the SCCP protocol ? According to what can I know which is the next protocol (RANAP or BSSAP for example..). I don&#x27;t want to rely on the SSN because they are not always present.  Thanks, Radhwen'''
date = "2013-02-20T11:11:00Z"
lastmod = "2013-02-20T11:24:00Z"
weight = 18783
keywords = [ "sccp", "ss7", "wireshark", "sigtran" ]
aliases = [ "/questions/18783" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How does Wireshark know the protocol underlying the SCCP ?](/questions/18783/how-does-wireshark-know-the-protocol-underlying-the-sccp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18783-score" class="post-score" title="current number of votes">0</div><span id="post-18783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><strong>I'm asking how does Wireshark know that there is BSSAP underlying the SCCP protocol ?</strong></p><p>According to what can I know which is the next protocol (RANAP or BSSAP for example..). I don't want to rely on the <strong>SSN</strong> because they are not always present.</p><p>Thanks, Radhwen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sccp" rel="tag" title="see questions tagged &#39;sccp&#39;">sccp</span> <span class="post-tag tag-link-ss7" rel="tag" title="see questions tagged &#39;ss7&#39;">ss7</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-sigtran" rel="tag" title="see questions tagged &#39;sigtran&#39;">sigtran</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '13, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/68c99fbbfd2d84604a14baa960e607d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Radhwen%20Khelia&#39;s gravatar image" /><p><span>Radhwen Khelia</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Radhwen Khelia has no accepted answers">0%</span></p></div></div><div id="comments-container-18783" class="comments-container"></div><div id="comment-tools-18783" class="comment-tools"></div><div class="clear"></div><div id="comment-18783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18784"></span>

<div id="answer-container-18784" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18784-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18784-score" class="post-score" title="current number of votes">0</div><span id="post-18784-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://ask.wireshark.org/questions/18030/ss7-bssap-protocol">http://ask.wireshark.org/questions/18030/ss7-bssap-protocol</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Feb '13, 11:24</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-18784" class="comments-container"></div><div id="comment-tools-18784" class="comment-tools"></div><div class="clear"></div><div id="comment-18784-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

