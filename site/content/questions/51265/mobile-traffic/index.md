+++
type = "question"
title = "mobile traffic"
description = '''hi! i am trying to capture mobile traffic by using my laptop as a hotsopt. i am able to connect to my computer&#x27;s wifi with my moblie but i dont see anything on wireshark. any ideas whats wrong? thanks... '''
date = "2016-03-29T12:00:00Z"
lastmod = "2016-03-30T06:31:00Z"
weight = 51265
keywords = [ "mobile", "traffic", "hotspot", "wireshark" ]
aliases = [ "/questions/51265" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [mobile traffic](/questions/51265/mobile-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51265-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51265-score" class="post-score" title="current number of votes">0</div><span id="post-51265-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi! i am trying to capture mobile traffic by using my laptop as a hotsopt. i am able to connect to my computer's wifi with my moblie but i dont see anything on wireshark. any ideas whats wrong? thanks...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-hotspot" rel="tag" title="see questions tagged &#39;hotspot&#39;">hotspot</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '16, 12:00</strong></p><img src="https://secure.gravatar.com/avatar/5da7238914e3c851d63a0a110da53795?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bar%20Stromza&#39;s gravatar image" /><p><span>Bar Stromza</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bar Stromza has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Mar '16, 12:00</strong> </span></p></div></div><div id="comments-container-51265" class="comments-container"><span id="51290"></span><div id="comment-51290" class="comment"><div id="post-51290-score" class="comment-score"></div><div class="comment-text"><p>Can you provide more details?</p><ol><li><p>Where are you trying to take the WiFi capture? Are you trying to take the WiFi capture from the same laptop providing the hotspot?</p></li><li><p>When your laptop is acting as the hotspot, are you able to browse the Internet on the mobile phone connected to the hotspot WiFi? It is important to disable your mobile's cellular radio to ensure that you are browsing via WiFi and not via the cellular connection.</p></li></ol></div><div id="comment-51290-info" class="comment-info"><span class="comment-age">(30 Mar '16, 06:31)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-51265" class="comment-tools"></div><div class="clear"></div><div id="comment-51265-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

