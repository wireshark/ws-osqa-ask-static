+++
type = "question"
title = "I wish to monitor android data traffic to find IP of proxies used"
description = '''Hello, I have an android phone and I am running this app on it https://play.google.com/store/apps/details?id=com.proxyBrowser This app uses proxies to access any website, I wish to know the IP addresses of the proxies used. I have wireshark running on pc and I can connect it to the router using ethe...'''
date = "2013-12-08T12:25:00Z"
lastmod = "2013-12-08T12:25:00Z"
weight = 27923
keywords = [ "android", "proxy" ]
aliases = [ "/questions/27923" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I wish to monitor android data traffic to find IP of proxies used](/questions/27923/i-wish-to-monitor-android-data-traffic-to-find-ip-of-proxies-used)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27923-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27923-score" class="post-score" title="current number of votes">0</div><span id="post-27923-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have an android phone and I am running this app on it <a href="https://play.google.com/store/apps/details?id=com.proxyBrowser">https://play.google.com/store/apps/details?id=com.proxyBrowser</a></p><p>This app uses proxies to access any website, I wish to know the IP addresses of the proxies used.</p><p>I have wireshark running on pc and I can connect it to the router using ethernet or wifi. The android phone is running on the same wifi network. I don't know how to setup wifi capture/monitor on pc (windows 8.1 64-bit)</p><p>Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '13, 12:25</strong></p><img src="https://secure.gravatar.com/avatar/0291fbbcbedcf6032465d6793596eacc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MH1&#39;s gravatar image" /><p><span>MH1</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MH1 has no accepted answers">0%</span></p></div></div><div id="comments-container-27923" class="comments-container"></div><div id="comment-tools-27923" class="comment-tools"></div><div class="clear"></div><div id="comment-27923-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

