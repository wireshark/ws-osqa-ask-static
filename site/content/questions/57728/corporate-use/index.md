+++
type = "question"
title = "Corporate use"
description = '''Several of our IT team use your product in a Corporate environment here at TBI, so we are wanting to know if we need to purchase licenses because of that environment?'''
date = "2016-11-30T06:02:00Z"
lastmod = "2016-11-30T07:07:00Z"
weight = 57728
keywords = [ "licensing" ]
aliases = [ "/questions/57728" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Corporate use](/questions/57728/corporate-use)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57728-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57728-score" class="post-score" title="current number of votes">0</div><span id="post-57728-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Several of our IT team use your product in a Corporate environment here at TBI, so we are wanting to know if we need to purchase licenses because of that environment?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-licensing" rel="tag" title="see questions tagged &#39;licensing&#39;">licensing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '16, 06:02</strong></p><img src="https://secure.gravatar.com/avatar/acddaf0b40ca551bfa3426dad572e111?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bredd52&#39;s gravatar image" /><p><span>bredd52</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bredd52 has no accepted answers">0%</span></p></div></div><div id="comments-container-57728" class="comments-container"></div><div id="comment-tools-57728" class="comment-tools"></div><div class="clear"></div><div id="comment-57728-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57731"></span>

<div id="answer-container-57731" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57731-score" class="post-score" title="current number of votes">1</div><span id="post-57731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the FAQ <a href="https://www.wireshark.org/faq.html">https://www.wireshark.org/faq.html</a></p><p>Q 1.6: How much does Wireshark cost?</p><p>A: Wireshark is "free software"; you can download it without paying any license fee. The version of Wireshark you download isn't a "demo" version, with limitations not present in a "full" version; it is the full version. The license under which Wireshark is issued is the GNU General Public License version 2. See the GNU GPL FAQ for some more information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '16, 07:07</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-57731" class="comments-container"></div><div id="comment-tools-57731" class="comment-tools"></div><div class="clear"></div><div id="comment-57731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

