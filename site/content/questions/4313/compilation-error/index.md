+++
type = "question"
title = "compilation error"
description = '''hi, i am getting fallowing error during the compilation of wireshark dissector, what may be the problem ?? please help me : fatal error C1083: Cannot open include file: &#x27;config.h&#x27;: No such file or directory Project : warning PRJ0018 : The following environment variables were not found: $(WIRESHARK_L...'''
date = "2011-06-01T04:50:00Z"
lastmod = "2011-06-01T21:34:00Z"
weight = 4313
keywords = [ "compile" ]
aliases = [ "/questions/4313" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [compilation error](/questions/4313/compilation-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4313-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4313-score" class="post-score" title="current number of votes">0</div><span id="post-4313-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i am getting fallowing error during the compilation of wireshark dissector, what may be the problem ?? please help me</p><p>: fatal error C1083: Cannot open include file: 'config.h': No such file or directory Project : warning PRJ0018 : The following environment variables were not found: $(WIRESHARK_LIB)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compile" rel="tag" title="see questions tagged &#39;compile&#39;">compile</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '11, 04:50</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div></div><div id="comments-container-4313" class="comments-container"><span id="4314"></span><div id="comment-4314" class="comment"><div id="post-4314-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark are you trying to compile, and on what platform?</p></div><div id="comment-4314-info" class="comment-info"><span class="comment-age">(01 Jun '11, 06:03)</span> <span class="comment-user userinfo">multipleinte...</span></div></div><span id="4317"></span><div id="comment-4317" class="comment"><div id="post-4317-score" class="comment-score"></div><div class="comment-text"><p>wireshark 1.4.7, and m compiling on windows platform, even i could not able to verify tools usin nmake -f Makefile.nmake verify_tools, m getting error as " don know how to stop -f" something lik this, pleas help</p></div><div id="comment-4317-info" class="comment-info"><span class="comment-age">(01 Jun '11, 06:58)</span> <span class="comment-user userinfo">sagu072</span></div></div><span id="4318"></span><div id="comment-4318" class="comment"><div id="post-4318-score" class="comment-score"></div><div class="comment-text"><p>Do you get the help text if you give the command 'nmake /help' ?</p><p>Have you followed the instructions @ http://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html ?</p><p>In any case, I suggest sending an EMAil to the <span class="__cf_email__" data-cfemail="7c0b150e190f141d0e175118190a3c0b150e190f141d0e1752130e1b">[email protected]</span> mailing list which is better suited for this kind of help.</p><p>Among other things, you can then attach a cut/paste of the exact cmds and responses.</p></div><div id="comment-4318-info" class="comment-info"><span class="comment-age">(01 Jun '11, 07:56)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="4324"></span><div id="comment-4324" class="comment"><div id="post-4324-score" class="comment-score"></div><div class="comment-text"><p>ok bill, thanks for the response.</p></div><div id="comment-4324-info" class="comment-info"><span class="comment-age">(01 Jun '11, 21:34)</span> <span class="comment-user userinfo">sagu072</span></div></div></div><div id="comment-tools-4313" class="comment-tools"></div><div class="clear"></div><div id="comment-4313-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

