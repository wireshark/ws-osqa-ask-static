+++
type = "question"
title = "How to get interface with description showing in new UI ?"
description = '''Hi With the new UI I could not find a way to get an interface list showing. The interface list so far was the only way to get the Description of a device showing which is needed to distinguish interfaces when there are numerous adapters attached. Did I miss something? Thanks McL'''
date = "2016-02-22T02:05:00Z"
lastmod = "2016-02-22T06:11:00Z"
weight = 50391
keywords = [ "interface", "list", "description" ]
aliases = [ "/questions/50391" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to get interface with description showing in new UI ?](/questions/50391/how-to-get-interface-with-description-showing-in-new-ui)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50391-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50391-score" class="post-score" title="current number of votes">0</div><span id="post-50391-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>With the new UI I could not find a way to get an interface list showing. The interface list so far was the only way to get the Description of a device showing which is needed to distinguish interfaces when there are numerous adapters attached.</p><p>Did I miss something? Thanks McL</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-list" rel="tag" title="see questions tagged &#39;list&#39;">list</span> <span class="post-tag tag-link-description" rel="tag" title="see questions tagged &#39;description&#39;">description</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '16, 02:05</strong></p><img src="https://secure.gravatar.com/avatar/71d38b4bc56e72a3d29a1e06972aa1a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="McLion&#39;s gravatar image" /><p><span>McLion</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="McLion has no accepted answers">0%</span></p></div></div><div id="comments-container-50391" class="comments-container"></div><div id="comment-tools-50391" class="comment-tools"></div><div class="clear"></div><div id="comment-50391-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50393"></span>

<div id="answer-container-50393" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50393-score" class="post-score" title="current number of votes">0</div><span id="post-50393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the menu, Capture -&gt; Options, then click the "Manage Interfaces..." button.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '16, 02:21</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50393" class="comments-container"><span id="50396"></span><div id="comment-50396" class="comment"><div id="post-50396-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately not - see the screen-shot below: <img src="https://osqa-ask.wireshark.org/upfiles/Zwischenablage3.jpg" alt="alt text" /></p></div><div id="comment-50396-info" class="comment-info"><span class="comment-age">(22 Feb '16, 02:28)</span> <span class="comment-user userinfo">McLion</span></div></div><span id="50397"></span><div id="comment-50397" class="comment"><div id="post-50397-score" class="comment-score"></div><div class="comment-text"><p>is the issue with the USBPcap or LAN_Verbindun... interfaces? Can you expand the "Kurzname" column to show the full text?</p><p>What other info do you need? On the Capture Options tab you can click the right triangle icon to expand the interface to show the assigned IP's if that helps.</p><p>Do your interfaces have helpful names assigned in Network Connections, as that's where Wireshark picks up the "friendly" names from?</p></div><div id="comment-50397-info" class="comment-info"><span class="comment-age">(22 Feb '16, 02:57)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="50398"></span><div id="comment-50398" class="comment"><div id="post-50398-score" class="comment-score"></div><div class="comment-text"><p>To illustrate what I am missing look at the interface list taken with the old GUI. Look at the column 'Description'. I cannot find a way to show this information in the new GUI. The only way to distinguish the interfaces is to go to Windows, showing the Network adapter list and then use the name given by Windows like Ethernet or Ethernet2 ... This is not very user-friendly and was much better with the old GUI.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Zwischenablage1old.jpg" alt="alt text" /></p><p>And how it looks in Windows: <img src="https://osqa-ask.wireshark.org/upfiles/Zwischenablage4.jpg" alt="alt text" /></p><p>There's no way to show Description / DeviceName in Wiresharks new UI.</p></div><div id="comment-50398-info" class="comment-info"><span class="comment-age">(22 Feb '16, 03:16)</span> <span class="comment-user userinfo">McLion</span></div></div><span id="50399"></span><div id="comment-50399" class="comment"><div id="post-50399-score" class="comment-score"></div><div class="comment-text"><p>I can see the differences in the UI, but how does the "Description" or "Device Name" help, e.g. for Ethernet and Ethernet 2 they are identical?</p><p>The "Device Name" does help for interface LAN-Verbindung though.</p><p>Why don't you rename the interfaces in the Network Connections dialog to be more meaningful?</p></div><div id="comment-50399-info" class="comment-info"><span class="comment-age">(22 Feb '16, 03:31)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="50401"></span><div id="comment-50401" class="comment"><div id="post-50401-score" class="comment-score"></div><div class="comment-text"><p>Nope, they are not, but that's not the issue. We have multiple interfaces on all our desktops as well as on our Mobiles. Look at a sample from a desktop here: <img src="https://osqa-ask.wireshark.org/upfiles/Zwischenablage5.jpg" alt="alt text" /></p><p>Which one to pick cannot easily be decided? However, it is very easy with the old interface: <img src="https://osqa-ask.wireshark.org/upfiles/Zwischenablage6.jpg" alt="alt text" /> On the other hand, I don't get what the information below that has been added is useful for. Nobody I know keeps in mind a ID from the registry. <img src="https://osqa-ask.wireshark.org/upfiles/Zwischenablage7.jpg" alt="alt text" /></p></div><div id="comment-50401-info" class="comment-info"><span class="comment-age">(22 Feb '16, 04:01)</span> <span class="comment-user userinfo">McLion</span></div></div><span id="50402"></span><div id="comment-50402" class="comment not_top_scorer"><div id="post-50402-score" class="comment-score"></div><div class="comment-text"><p>I forgot to respond the renaming suggestion. This would work for standard network interfaces. As it is with USB, the name is not sticky, specially not if the USB adapter is not connected to the very same port. The name appearing is then picked from the inf file.</p><p>However, I don't understand why a useful information has been removed and/or replaced by a un-useful one. IMHO, this is a step backward, not forward.</p></div><div id="comment-50402-info" class="comment-info"><span class="comment-age">(22 Feb '16, 04:28)</span> <span class="comment-user userinfo">McLion</span></div></div><span id="50404"></span><div id="comment-50404" class="comment not_top_scorer"><div id="post-50404-score" class="comment-score"></div><div class="comment-text"><p>Looks like you'll have to raise an issue over at the <a href="https://bugs.wireshark.rog">Wireshark Bugzilla</a> then.</p></div><div id="comment-50404-info" class="comment-info"><span class="comment-age">(22 Feb '16, 05:25)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="50405"></span><div id="comment-50405" class="comment not_top_scorer"><div id="post-50405-score" class="comment-score"></div><div class="comment-text"><p>Done: <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12156">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12156</a></p></div><div id="comment-50405-info" class="comment-info"><span class="comment-age">(22 Feb '16, 06:11)</span> <span class="comment-user userinfo">McLion</span></div></div></div><div id="comment-tools-50393" class="comment-tools"><span class="comments-showing"> showing 5 of 8 </span> <a href="#" class="show-all-comments-link">show 3 more comments</a></div><div class="clear"></div><div id="comment-50393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

