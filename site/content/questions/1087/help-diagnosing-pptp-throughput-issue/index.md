+++
type = "question"
title = "Help diagnosing PPTP throughput issue."
description = '''Have an Intel Mac 10.6.5. When I Remote Desktop via PPTP to a W2K3 server my throughput is atrocious. When I VPN using same credentials and network via a Slower Vista Business machine everything is speedy (10x faster). Using Wireshark to attempt to determine issue. Fixed a small MTU mismatch and get...'''
date = "2010-11-23T12:02:00Z"
lastmod = "2010-11-23T12:02:00Z"
weight = 1087
keywords = [ "pptp", "vpn", "throughput" ]
aliases = [ "/questions/1087" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help diagnosing PPTP throughput issue.](/questions/1087/help-diagnosing-pptp-throughput-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1087-score" class="post-score" title="current number of votes">0</div><span id="post-1087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Have an Intel Mac 10.6.5. When I Remote Desktop via PPTP to a W2K3 server my throughput is atrocious. When I VPN using same credentials and network via a Slower Vista Business machine everything is speedy (10x faster). Using Wireshark to attempt to determine issue. Fixed a small MTU mismatch and get these occasionally in the trace (maybe 10-15%)</p><p>[TCP Retranmission] [TCP segment of a reassembled PDU] AND [TCP Dup ACK etc.....]</p><p>Are these normal? if not what do I look at to correct, if they are normal any suggestions on what else may be culprit and how to track down.</p><p>Thanks for any assistance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pptp" rel="tag" title="see questions tagged &#39;pptp&#39;">pptp</span> <span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span> <span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '10, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/e1f2d58738607cb8e086868b5924063f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="iridiangroup&#39;s gravatar image" /><p><span>iridiangroup</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="iridiangroup has no accepted answers">0%</span></p></div></div><div id="comments-container-1087" class="comments-container"></div><div id="comment-tools-1087" class="comment-tools"></div><div class="clear"></div><div id="comment-1087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

