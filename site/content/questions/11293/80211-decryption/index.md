+++
type = "question"
title = "802.11 decryption"
description = '''Like a few other posters, I&#x27;m not able to decrypt data captured from my wifi network. I have:  Entered the key into the IEEE 802.11 preferences, in the wpa-pwd:KEY:SSID format. Turned on Enable Decryption in the IEEE 802.11 preferences. Tried all combinations of FCS/protection bit. Ensured that I ha...'''
date = "2012-05-23T13:09:00Z"
lastmod = "2012-05-26T08:28:00Z"
weight = 11293
keywords = [ "decryption", "802.11" ]
aliases = [ "/questions/11293" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [802.11 decryption](/questions/11293/80211-decryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11293-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11293-score" class="post-score" title="current number of votes">0</div><span id="post-11293-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Like a few other posters, I'm not able to decrypt data captured from my wifi network.</p><p>I have:</p><ul><li>Entered the key into the IEEE 802.11 preferences, in the wpa-pwd:KEY:SSID format.</li><li>Turned on Enable Decryption in the IEEE 802.11 preferences.</li><li>Tried all combinations of FCS/protection bit.</li><li>Ensured that I have captured the handshake by:</li><li>Disconnecting and reconnecting a client.</li><li>Confirmed EAPOL packets in the capture: I see 1/4, 2/4, 3/4 and 4/4 on the expected addresses.</li><li>Sanity checked by decrypting the capture from <a href="http://wiki.wireshark.org/HowToDecrypt802.11">http://wiki.wireshark.org/HowToDecrypt802.11</a> - worked fine.</li></ul><p>I really do think I've ticked all the boxes on this one, so any help would be appreciated.</p><p>Wireshark 1.6.7 on Ubuntu 12.04.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '12, 13:09</strong></p><img src="https://secure.gravatar.com/avatar/8725804df9ad1293121f5459cdefb7bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stormeagle&#39;s gravatar image" /><p><span>stormeagle</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stormeagle has no accepted answers">0%</span></p></div></div><div id="comments-container-11293" class="comments-container"></div><div id="comment-tools-11293" class="comment-tools"></div><div class="clear"></div><div id="comment-11293-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11375"></span>

<div id="answer-container-11375" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11375-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11375-score" class="post-score" title="current number of votes">0</div><span id="post-11375-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Aaaand, the answer was a typo in the SSID - I didn't know they were case sensitive!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 May '12, 08:28</strong></p><img src="https://secure.gravatar.com/avatar/8725804df9ad1293121f5459cdefb7bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stormeagle&#39;s gravatar image" /><p><span>stormeagle</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stormeagle has no accepted answers">0%</span></p></div></div><div id="comments-container-11375" class="comments-container"></div><div id="comment-tools-11375" class="comment-tools"></div><div class="clear"></div><div id="comment-11375-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

