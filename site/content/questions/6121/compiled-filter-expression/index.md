+++
type = "question"
title = "Compiled Filter Expression"
description = '''What is Compiled Filter Expression in Color_Filtering?'''
date = "2011-09-06T04:11:00Z"
lastmod = "2011-09-06T04:32:00Z"
weight = 6121
keywords = [ "epan", "wireshark" ]
aliases = [ "/questions/6121" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Compiled Filter Expression](/questions/6121/compiled-filter-expression)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6121-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6121-score" class="post-score" title="current number of votes">0</div><span id="post-6121-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is Compiled Filter Expression in Color_Filtering?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-epan" rel="tag" title="see questions tagged &#39;epan&#39;">epan</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Sep '11, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/968cc7ddfc48322ffbd1d7f5e3d37b85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Terrestrial%20shark&#39;s gravatar image" /><p><span>Terrestrial ...</span><br />
<span class="score" title="96 reputation points">96</span><span title="21 badges"><span class="badge1">●</span><span class="badgecount">21</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="29 badges"><span class="bronze">●</span><span class="badgecount">29</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Terrestrial shark has 3 accepted answers">42%</span></p></div></div><div id="comments-container-6121" class="comments-container"></div><div id="comment-tools-6121" class="comment-tools"></div><div class="clear"></div><div id="comment-6121-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6122"></span>

<div id="answer-container-6122" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6122-score" class="post-score" title="current number of votes">1</div><span id="post-6122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's a filter expression (which has the same syntax as a <a href="http://wiki.wireshark.org/DisplayFilters">display filter</a>) that is used to select packets to be colorized in the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketListPaneSection.html">Packet List pane</a>. It's also known as a <strong><a href="http://wiki.wireshark.org/ColoringRules">coloring rule</a></strong>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Sep '11, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/aa651167cb1d51fa9dca1212f1123bfa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bstn&#39;s gravatar image" /><p><span>bstn</span><br />
<span class="score" title="375 reputation points">375</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bstn has 4 accepted answers">14%</span></p></div></div><div id="comments-container-6122" class="comments-container"><span id="6123"></span><div id="comment-6123" class="comment"><div id="post-6123-score" class="comment-score"></div><div class="comment-text"><p>how to color a new protocol through color_filter.c?</p></div><div id="comment-6123-info" class="comment-info"><span class="comment-age">(06 Sep '11, 04:27)</span> <span class="comment-user userinfo">Terrestrial ...</span></div></div><span id="6124"></span><div id="comment-6124" class="comment"><div id="post-6124-score" class="comment-score"></div><div class="comment-text"><p>You wouldn't colorize packets from code. You simply create a color rule as described in the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChCustColorizationSection.html">User's Guide</a>.</p></div><div id="comment-6124-info" class="comment-info"><span class="comment-age">(06 Sep '11, 04:32)</span> <span class="comment-user userinfo">bstn</span></div></div></div><div id="comment-tools-6122" class="comment-tools"></div><div class="clear"></div><div id="comment-6122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

