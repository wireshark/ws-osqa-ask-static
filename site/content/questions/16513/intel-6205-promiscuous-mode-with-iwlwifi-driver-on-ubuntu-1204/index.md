+++
type = "question"
title = "Intel 6205 Promiscuous Mode with iwlwifi driver on Ubuntu 12.04"
description = '''Wireshark allows me to check the &#x27;promiscuous mode&#x27; checkbox, but I&#x27;m not seeing all traffic from other IPs... any ideas?'''
date = "2012-12-03T14:06:00Z"
lastmod = "2012-12-05T12:48:00Z"
weight = 16513
keywords = [ "intel6205", "ubuntu" ]
aliases = [ "/questions/16513" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Intel 6205 Promiscuous Mode with iwlwifi driver on Ubuntu 12.04](/questions/16513/intel-6205-promiscuous-mode-with-iwlwifi-driver-on-ubuntu-1204)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16513-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16513-score" class="post-score" title="current number of votes">0</div><span id="post-16513-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark allows me to check the 'promiscuous mode' checkbox, but I'm not seeing all traffic from other IPs... any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-intel6205" rel="tag" title="see questions tagged &#39;intel6205&#39;">intel6205</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Dec '12, 14:06</strong></p><img src="https://secure.gravatar.com/avatar/446c4582d7c41c26b02ba49b259f031c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lambertconformist&#39;s gravatar image" /><p><span>lambertconfo...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lambertconformist has no accepted answers">0%</span></p></div></div><div id="comments-container-16513" class="comments-container"></div><div id="comment-tools-16513" class="comment-tools"></div><div class="clear"></div><div id="comment-16513-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16612"></span>

<div id="answer-container-16612" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16612-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16612-score" class="post-score" title="current number of votes">1</div><span id="post-16612-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need <strong>monitor mode</strong>, to see traffic of other stations in a wireless network.</p><blockquote><p><code>http://wiki.wireshark.org/CaptureSetup/WLAN</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Dec '12, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-16612" class="comments-container"></div><div id="comment-tools-16612" class="comment-tools"></div><div class="clear"></div><div id="comment-16612-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

