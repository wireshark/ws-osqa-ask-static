+++
type = "question"
title = "Wireshark and cable modem"
description = '''Hi, my computer is behind cable router. I wander is it possible to capture all of the traffic that comes to my router with wireshark? I want to sniff the traffic before my router. I read something about promiscuous mode, but the problem is that I only see my own traffic. '''
date = "2014-06-20T02:16:00Z"
lastmod = "2014-06-20T04:00:00Z"
weight = 33982
keywords = [ "broadband", "traffic", "wireshark" ]
aliases = [ "/questions/33982" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and cable modem](/questions/33982/wireshark-and-cable-modem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33982-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33982-score" class="post-score" title="current number of votes">0</div><span id="post-33982-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, my computer is behind cable router. I wander is it possible to capture all of the traffic that comes to my router with wireshark? I want to sniff the traffic before my router. I read something about promiscuous mode, but the problem is that I only see my own traffic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadband" rel="tag" title="see questions tagged &#39;broadband&#39;">broadband</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '14, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/b0ee1dfe5a606e84cd4fa9698bf9581b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cosimo&#39;s gravatar image" /><p><span>cosimo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cosimo has no accepted answers">0%</span></p></div></div><div id="comments-container-33982" class="comments-container"></div><div id="comment-tools-33982" class="comment-tools"></div><div class="clear"></div><div id="comment-33982-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33984"></span>

<div id="answer-container-33984" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33984-score" class="post-score" title="current number of votes">0</div><span id="post-33984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Start off with the <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">capture setup</a>, then work your way towards the WAN interface.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jun '14, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-33984" class="comments-container"></div><div id="comment-tools-33984" class="comment-tools"></div><div class="clear"></div><div id="comment-33984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

