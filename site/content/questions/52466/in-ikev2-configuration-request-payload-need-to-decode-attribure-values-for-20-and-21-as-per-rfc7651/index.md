+++
type = "question"
title = "In IKEv2 , Configuration  REquest payload , need to decode attribure values for 20 and 21 As per rfc7651"
description = '''Next payload: Configuration (47) can we support below attribute decode ,support to correct value , like mentioned below 2 attribute .  Attribute Type: (t=20,l=0) RESERVED TO IANA Attribute Type: (t=1,l=0) INTERNAL_IP4_ADDRESS Attribute Type: (t=3,l=0) INTERNAL_IP4_DNS As per RFC  https://tools.ietf....'''
date = "2016-05-12T05:52:00Z"
lastmod = "2016-05-12T06:42:00Z"
weight = 52466
keywords = [ "ikev2" ]
aliases = [ "/questions/52466" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [In IKEv2 , Configuration REquest payload , need to decode attribure values for 20 and 21 As per rfc7651](/questions/52466/in-ikev2-configuration-request-payload-need-to-decode-attribure-values-for-20-and-21-as-per-rfc7651)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52466-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52466-score" class="post-score" title="current number of votes">0</div><span id="post-52466-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Next payload: Configuration (47)</p><p>can we support below attribute decode ,support to correct value , like mentioned below 2 attribute . Attribute Type: (t=20,l=0) RESERVED TO IANA</p><p>Attribute Type: (t=1,l=0) INTERNAL_IP4_ADDRESS Attribute Type: (t=3,l=0) INTERNAL_IP4_DNS</p><p>As per RFC <a href="https://tools.ietf.org/html/rfc7651">https://tools.ietf.org/html/rfc7651</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ikev2" rel="tag" title="see questions tagged &#39;ikev2&#39;">ikev2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '16, 05:52</strong></p><img src="https://secure.gravatar.com/avatar/295680189699a1bbd170b43b792f1943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rajarathnam&#39;s gravatar image" /><p><span>rajarathnam</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rajarathnam has no accepted answers">0%</span></p></div></div><div id="comments-container-52466" class="comments-container"></div><div id="comment-tools-52466" class="comment-tools"></div><div class="clear"></div><div id="comment-52466-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52469"></span>

<div id="answer-container-52469" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52469-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52469-score" class="post-score" title="current number of votes">0</div><span id="post-52469-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you can submit the relevant source code changes though <a href="https://code.wireshark.org/review/">Gerrit</a> that would be great.</p><p>But I guess you have no souce code patch. What you do seem to have is capture file (the thing that you can save with Wireshark) containing such packet. If you could enter that into an <a href="https://bugs.wireshark.org/bugzilla/">enhancement request</a> together with all the information you listed, someone can pick it up and implement the required source code changes.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 May '16, 06:42</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52469" class="comments-container"></div><div id="comment-tools-52469" class="comment-tools"></div><div class="clear"></div><div id="comment-52469-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

