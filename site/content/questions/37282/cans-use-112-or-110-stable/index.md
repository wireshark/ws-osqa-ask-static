+++
type = "question"
title = "Can&#x27;s use 1.12 or 1.10 Stable"
description = '''When I try to login with OS X Yosemite (late 2013 Pro), I revert to the Finder.  The &quot;About this Mac&quot; -&amp;gt; &quot;System Report..&quot; says v. 1.12 is NOT 64 bit. But 1.99 (Experimental) seems to work, so I&#x27;ll go from there.'''
date = "2014-10-22T07:17:00Z"
lastmod = "2014-10-22T07:17:00Z"
weight = 37282
keywords = [ "release", "stable" ]
aliases = [ "/questions/37282" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can's use 1.12 or 1.10 Stable](/questions/37282/cans-use-112-or-110-stable)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37282-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37282-score" class="post-score" title="current number of votes">0</div><span id="post-37282-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I try to login with OS X Yosemite (late 2013 Pro), I revert to the Finder.<br />
</p><p>The "About this Mac" -&gt; "System Report.." says v. 1.12 is NOT 64 bit.</p><p>But 1.99 (Experimental) seems to work, so I'll go from there.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-release" rel="tag" title="see questions tagged &#39;release&#39;">release</span> <span class="post-tag tag-link-stable" rel="tag" title="see questions tagged &#39;stable&#39;">stable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '14, 07:17</strong></p><img src="https://secure.gravatar.com/avatar/2d003bd2f73cb3177c66fb1cb85889c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nelson%20Weiderman&#39;s gravatar image" /><p><span>Nelson Weide...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nelson Weiderman has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-37282" class="comments-container"></div><div id="comment-tools-37282" class="comment-tools"></div><div class="clear"></div><div id="comment-37282-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

