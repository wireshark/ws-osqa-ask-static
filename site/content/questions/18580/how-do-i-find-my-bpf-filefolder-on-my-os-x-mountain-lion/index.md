+++
type = "question"
title = "How do I find my BPF file/folder on my OS X mountain lion?"
description = '''The disk image only has the package installer and a read me file. I keep on reading that I need to gain privilege on my BPF or ChmodBPF files, but I cannot find them anywhere. Currently trying to analyze my network, but all I see is my own traffic. I&#x27;m trying to monitor the entire traffic off of my ...'''
date = "2013-02-13T04:55:00Z"
lastmod = "2013-12-05T10:15:00Z"
weight = 18580
keywords = [ "osx", "chmodbpf", "bpf" ]
aliases = [ "/questions/18580" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [How do I find my BPF file/folder on my OS X mountain lion?](/questions/18580/how-do-i-find-my-bpf-filefolder-on-my-os-x-mountain-lion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18580-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18580-score" class="post-score" title="current number of votes">0</div><span id="post-18580-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The disk image only has the package installer and a read me file. I keep on reading that I need to gain privilege on my BPF or ChmodBPF files, but I cannot find them anywhere.</p><p>Currently trying to analyze my network, but all I see is my own traffic. I'm trying to monitor the entire traffic off of my home network.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-chmodbpf" rel="tag" title="see questions tagged &#39;chmodbpf&#39;">chmodbpf</span> <span class="post-tag tag-link-bpf" rel="tag" title="see questions tagged &#39;bpf&#39;">bpf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '13, 04:55</strong></p><img src="https://secure.gravatar.com/avatar/9a8af5bf7fb427b13524e98efd173bcb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="phiton&#39;s gravatar image" /><p><span>phiton</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="phiton has no accepted answers">0%</span></p></div></div><div id="comments-container-18580" class="comments-container"></div><div id="comment-tools-18580" class="comment-tools"></div><div class="clear"></div><div id="comment-18580-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="18581"></span>

<div id="answer-container-18581" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18581-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18581-score" class="post-score" title="current number of votes">0</div><span id="post-18581-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>please search for <strong>bpf</strong> in the answers and comments of the following question:</p><blockquote><p><code>http://ask.wireshark.org/questions/578/mac-os-cant-detect-any-interface</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '13, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Feb '13, 05:16</strong> </span></p></div></div><div id="comments-container-18581" class="comments-container"></div><div id="comment-tools-18581" class="comment-tools"></div><div class="clear"></div><div id="comment-18581-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18582"></span>

<div id="answer-container-18582" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18582-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18582-score" class="post-score" title="current number of votes">0</div><span id="post-18582-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you can capture traffic it seems likely that your capture permissions are sufficient.</p><p>If you can only see your own traffic it's likely that you are on a switched network. Please give further information, either by editing your question, or as a comment, about your network setup.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '13, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-18582" class="comments-container"><span id="18583"></span><div id="comment-18583" class="comment"><div id="post-18583-score" class="comment-score"></div><div class="comment-text"><p>All I know what to say is that I'm connected to an Airport extreme. I have two laptops and a smartphone. WPA2. I'm assuming a really standard connection. As mentioned before, I can see my own traffic on my Mac, but my other devices cannot be captured.</p></div><div id="comment-18583-info" class="comment-info"><span class="comment-age">(13 Feb '13, 05:35)</span> <span class="comment-user userinfo">phiton</span></div></div><span id="18584"></span><div id="comment-18584" class="comment"><div id="post-18584-score" class="comment-score"></div><div class="comment-text"><p>See the wiki page on <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">Wireless capture setup</a> for more info on capturing Wireless traffic.</p></div><div id="comment-18584-info" class="comment-info"><span class="comment-age">(13 Feb '13, 05:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18582" class="comment-tools"></div><div class="clear"></div><div id="comment-18582-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="27834"></span>

<div id="answer-container-27834" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27834-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27834-score" class="post-score" title="current number of votes">0</div><span id="post-27834-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>The disk image only has the package installer and a read me file. I keep on reading that I need to gain privilege on my BPF or ChmodBPF files, but I cannot find them anywhere.</p></blockquote><p>ChmodBPF is a "startup item" that the package installer installs; it changes the permission on BPF devices (files in the <code>/dev</code> directory with names beginning with <code>bpf</code>) so that you get the necessary privileges.</p><blockquote><p>Currently trying to analyze my network, but all I see is my own traffic.</p></blockquote><p>By default, that's all you'll see on a Wi-Fi network. You would need to capture in "monitor mode" to see other hosts' traffic, <em>and</em>, if your network is using WEP or WPA/WPA2, you'll have to tell Wireshark the password for your network and, if it's using WPA/WPA2, you'll have to, for each machine whose traffic you want to see, disconnect it from the network and reconnect it while Wireshark is listening, so that you capture the initial "EAPOL handshake". See the <a href="http://wiki.wireshark.org/HowToDecrypt802.11">how to decrypt 802.11 page</a> on the Wireshark Wiki for more details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Dec '13, 10:15</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-27834" class="comments-container"></div><div id="comment-tools-27834" class="comment-tools"></div><div class="clear"></div><div id="comment-27834-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

