+++
type = "question"
title = "Can&#x27;t install successfully on OS X 10.6.8"
description = '''I have XQuartz 2.7.4 installed and running. Installed Wireshark 10.6.8 (64bit). Trying to execute either from command line or from App icon gives the following error message. Any suggestions for next steps? 2013-04-12 16:22:01.218 defaults[503:903]  The domain/default pair of (kCFPreferencesAnyAppli...'''
date = "2013-04-12T13:28:00Z"
lastmod = "2013-04-12T13:28:00Z"
weight = 20387
keywords = [ "x", "xquartz", "10.6.8", "os" ]
aliases = [ "/questions/20387" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't install successfully on OS X 10.6.8](/questions/20387/cant-install-successfully-on-os-x-1068)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20387-score" class="post-score" title="current number of votes">0</div><span id="post-20387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have XQuartz 2.7.4 installed and running. Installed Wireshark 10.6.8 (64bit). Trying to execute either from command line or from App icon gives the following error message. Any suggestions for next steps?</p><p>2013-04-12 16:22:01.218 defaults[503:903] The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist 2013-04-12 16:22:01.242 defaults[504:903] The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist dyld: Library not loaded: /usr/X11/lib/libpng12.0.dylib Referenced from: /Applications/Wireshark.app/Contents/Resources/bin/wireshark-bin Reason: image not found Trace/BPT trap</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x" rel="tag" title="see questions tagged &#39;x&#39;">x</span> <span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-10.6.8" rel="tag" title="see questions tagged &#39;10.6.8&#39;">10.6.8</span> <span class="post-tag tag-link-os" rel="tag" title="see questions tagged &#39;os&#39;">os</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '13, 13:28</strong></p><img src="https://secure.gravatar.com/avatar/b7fbf02bd4eb6fa180937d44a6afdcd0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michael%20Ellis&#39;s gravatar image" /><p><span>Michael Ellis</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michael Ellis has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Apr '13, 13:28</strong> </span></p></div></div><div id="comments-container-20387" class="comments-container"></div><div id="comment-tools-20387" class="comment-tools"></div><div class="clear"></div><div id="comment-20387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

