+++
type = "question"
title = "Ip monitoring"
description = '''II want to know how ,my network data usage to find out how we are loosing data .i am over change,past four month. I have 10 PC I network.'''
date = "2013-10-06T10:31:00Z"
lastmod = "2013-10-06T10:31:00Z"
weight = 25675
keywords = [ "ip", "monitoring" ]
aliases = [ "/questions/25675" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ip monitoring](/questions/25675/ip-monitoring)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25675-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25675-score" class="post-score" title="current number of votes">0</div><span id="post-25675-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>II want to know how ,my network data usage to find out how we are loosing data .i am over change,past four month. I have 10 PC I network.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '13, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/dc9e4708f6e96a2ca56e76d385c7695d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bal&#39;s gravatar image" /><p><span>Bal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bal has no accepted answers">0%</span></p></div></div><div id="comments-container-25675" class="comments-container"></div><div id="comment-tools-25675" class="comment-tools"></div><div class="clear"></div><div id="comment-25675-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

