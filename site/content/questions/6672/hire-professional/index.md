+++
type = "question"
title = "Hire Professional"
description = '''I am looking to hire someone who can view my Wireshark logs (or whatever it takes), so they can determine and/or convert the messages between my server and mobile devices, if possible. I would need the exact message and protocol, and any other information that is available. '''
date = "2011-10-01T04:27:00Z"
lastmod = "2011-10-01T04:27:00Z"
weight = 6672
keywords = [ "consulting", "reverse-engineering" ]
aliases = [ "/questions/6672" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Hire Professional](/questions/6672/hire-professional)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6672-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6672-score" class="post-score" title="current number of votes">0</div><span id="post-6672-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking to hire someone who can view my Wireshark logs (or whatever it takes), so they can determine and/or convert the messages between my server and mobile devices, if possible. I would need the exact message and protocol, and any other information that is available.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-consulting" rel="tag" title="see questions tagged &#39;consulting&#39;">consulting</span> <span class="post-tag tag-link-reverse-engineering" rel="tag" title="see questions tagged &#39;reverse-engineering&#39;">reverse-engineering</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '11, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/875f427519c53c3357072b9f3d4c8785?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="davidp9470&#39;s gravatar image" /><p><span>davidp9470</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="davidp9470 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Oct '11, 15:50</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6672" class="comments-container"></div><div id="comment-tools-6672" class="comment-tools"></div><div class="clear"></div><div id="comment-6672-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

