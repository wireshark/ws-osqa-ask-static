+++
type = "question"
title = "Filter for only comm start and end"
description = '''Hi, I would like to have a filter that only writes the communication start and end and also the protocol used to a file. I need this, because I need to have a communication matrix from the AD Servers, to see what nodes are communicating with what protocol with the server. Regards, Ulrich'''
date = "2017-07-19T06:16:00Z"
lastmod = "2017-07-19T06:25:00Z"
weight = 62876
keywords = [ "filter", "on", "capture-filter" ]
aliases = [ "/questions/62876" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter for only comm start and end](/questions/62876/filter-for-only-comm-start-and-end)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62876-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62876-score" class="post-score" title="current number of votes">0</div><span id="post-62876-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I would like to have a filter that only writes the communication start and end and also the protocol used to a file. I need this, because I need to have a communication matrix from the AD Servers, to see what nodes are communicating with what protocol with the server.</p><p>Regards,</p><p>Ulrich</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-on" rel="tag" title="see questions tagged &#39;on&#39;">on</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '17, 06:16</strong></p><img src="https://secure.gravatar.com/avatar/845fa4f1f9666f6dac9d1d1a0600c50b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="UlrichSchulz&#39;s gravatar image" /><p><span>UlrichSchulz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="UlrichSchulz has no accepted answers">0%</span></p></div></div><div id="comments-container-62876" class="comments-container"></div><div id="comment-tools-62876" class="comment-tools"></div><div class="clear"></div><div id="comment-62876-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62877"></span>

<div id="answer-container-62877" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62877-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62877-score" class="post-score" title="current number of votes">0</div><span id="post-62877-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at <a href="https://wiki.wireshark.org/Mate">MATE</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jul '17, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-62877" class="comments-container"></div><div id="comment-tools-62877" class="comment-tools"></div><div class="clear"></div><div id="comment-62877-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

