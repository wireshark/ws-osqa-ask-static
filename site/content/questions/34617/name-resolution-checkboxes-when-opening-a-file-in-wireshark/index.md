+++
type = "question"
title = "Name Resolution checkboxes when opening a file in wireshark"
description = '''Hi, I&#x27;m new to wireshark &amp;amp; i&#x27;m just wondering whether you guys know how to implement correctly the &quot;MAC name resolution&quot;, &quot;Transport name resolution&quot;, &quot;Network name resolution&quot; feature that appears in the window of when you click the File menu &amp;gt; Open.. in wireshark. I tried to tick and untick...'''
date = "2014-07-12T16:47:00Z"
lastmod = "2014-07-12T16:47:00Z"
weight = 34617
keywords = [ "resolution", "name" ]
aliases = [ "/questions/34617" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Name Resolution checkboxes when opening a file in wireshark](/questions/34617/name-resolution-checkboxes-when-opening-a-file-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34617-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34617-score" class="post-score" title="current number of votes">0</div><span id="post-34617-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm new to wireshark &amp; i'm just wondering whether you guys know how to implement correctly the "MAC name resolution", "Transport name resolution", "Network name resolution" feature that appears in the window of when you click the File menu &gt; Open.. in wireshark.</p><p>I tried to tick and untick the checkboxes and tried to compare the difference as I tick and untick the check boxes of the above options when I open a saved pcap file. But it does not seem to have any difference because when I untick the check boxes the name resolutions are already resolved as I open the file. So why is it there when it does not affect anything about name resolution or is this a bug in the software?</p><p>I'm using windows 7 64 bit and wireshark 1.10.7.</p><p>Looking forward to your reply.</p><p>Best Regards,</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Untitled_7.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-resolution" rel="tag" title="see questions tagged &#39;resolution&#39;">resolution</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jul '14, 16:47</strong></p><img src="https://secure.gravatar.com/avatar/92ece05491f081eed1948998f9ad5bd5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deloria&#39;s gravatar image" /><p><span>Deloria</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deloria has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Jul '14, 17:13</strong> </span></p></div></div><div id="comments-container-34617" class="comments-container"></div><div id="comment-tools-34617" class="comment-tools"></div><div class="clear"></div><div id="comment-34617-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

