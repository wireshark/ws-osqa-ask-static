+++
type = "question"
title = "How to hack wifi"
description = '''i want to hack a wifi with wireshark WPA Encription '''
date = "2014-05-27T11:50:00Z"
lastmod = "2014-05-27T12:48:00Z"
weight = 33111
keywords = [ "wireshark" ]
aliases = [ "/questions/33111" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to hack wifi](/questions/33111/how-to-hack-wifi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33111-score" class="post-score" title="current number of votes">0</div><span id="post-33111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i want to hack a wifi with wireshark WPA Encription</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '14, 11:50</strong></p><img src="https://secure.gravatar.com/avatar/bd79e364d20ad0ddca34d88bb145a0f7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="r4nd0m&#39;s gravatar image" /><p><span>r4nd0m</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="r4nd0m has no accepted answers">0%</span></p></div></div><div id="comments-container-33111" class="comments-container"></div><div id="comment-tools-33111" class="comment-tools"></div><div class="clear"></div><div id="comment-33111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33116"></span>

<div id="answer-container-33116" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33116-score" class="post-score" title="current number of votes">1</div><span id="post-33116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is the wrong site for you. You won't learn how to <strong><a href="https://www.google.com/?q=hack+WPA+encryption">hack WPA encryption</a></strong> here.</p><blockquote><p><a href="http://www.youtube.com/results?search_query=wifi+hacking">http://www.youtube.com/results?search_query=wifi+hacking</a><br />
<a href="https://www.google.com/?q=hack+WPA+encryption">https://www.google.com/?q=hack+WPA+encryption</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '14, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 May '14, 13:05</strong> </span></p></div></div><div id="comments-container-33116" class="comments-container"></div><div id="comment-tools-33116" class="comment-tools"></div><div class="clear"></div><div id="comment-33116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

