+++
type = "question"
title = "problems launching Wireshark 1.12.7 ( 32x )"
description = '''Hi,  my problem is with starting the process to capture i keep getting this message, but can&#x27;t solve it myself. [IMG]http://i61.tinypic.com/2woc7j9.jpg[/IMG]'''
date = "2015-09-14T12:48:00Z"
lastmod = "2015-09-14T12:48:00Z"
weight = 45831
keywords = [ "capture" ]
aliases = [ "/questions/45831" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [problems launching Wireshark 1.12.7 ( 32x )](/questions/45831/problems-launching-wireshark-1127-32x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45831-score" class="post-score" title="current number of votes">0</div><span id="post-45831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>my problem is with starting the process to capture i keep getting this message, but can't solve it myself.</p><p>[IMG]<a href="http://i61.tinypic.com/2woc7j9.jpg%5B/IMG%5D">http://i61.tinypic.com/2woc7j9.jpg[/IMG]</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '15, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/81d763d0ff53eb24b532af6460dc131d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yong%20pal&#39;s gravatar image" /><p><span>yong pal</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yong pal has no accepted answers">0%</span></p></div></div><div id="comments-container-45831" class="comments-container"></div><div id="comment-tools-45831" class="comment-tools"></div><div class="clear"></div><div id="comment-45831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

