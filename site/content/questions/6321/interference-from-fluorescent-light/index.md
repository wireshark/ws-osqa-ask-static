+++
type = "question"
title = "[closed] interference from fluorescent light"
description = '''can a fluorescent light fixture cause network interference if cat5 cables are 1 ft away from the fixture?'''
date = "2011-09-13T08:24:00Z"
lastmod = "2011-09-13T08:24:00Z"
weight = 6321
keywords = [ "fluorescent", "interference", "light" ]
aliases = [ "/questions/6321" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] interference from fluorescent light](/questions/6321/interference-from-fluorescent-light)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6321-score" class="post-score" title="current number of votes">0</div><span id="post-6321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can a fluorescent light fixture cause network interference if cat5 cables are 1 ft away from the fixture?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fluorescent" rel="tag" title="see questions tagged &#39;fluorescent&#39;">fluorescent</span> <span class="post-tag tag-link-interference" rel="tag" title="see questions tagged &#39;interference&#39;">interference</span> <span class="post-tag tag-link-light" rel="tag" title="see questions tagged &#39;light&#39;">light</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '11, 08:24</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>13 Sep '11, 09:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6321" class="comments-container"></div><div id="comment-tools-6321" class="comment-tools"></div><div class="clear"></div><div id="comment-6321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by helloworld 13 Sep '11, 09:01

</div>

</div>

</div>

