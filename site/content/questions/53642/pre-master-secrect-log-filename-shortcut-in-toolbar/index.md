+++
type = "question"
title = "(Pre)-Master-Secrect log filename shortcut in toolbar"
description = '''Hello,  I work with decrypted packet captures with pretty much every custom that I have a each time I load a new packet capture, I have to load a new pms file. In doing so I have to click on &#x27;Edit --&amp;gt; Prefernces --&amp;gt; Click on Protocols --&amp;gt; SSL -- &amp;gt; (Pre)-Master-Secrect log filename --&amp;gt;...'''
date = "2016-06-24T10:43:00Z"
lastmod = "2016-06-27T06:13:00Z"
weight = 53642
keywords = [ "pre", "secrect", "master", "log", "filename" ]
aliases = [ "/questions/53642" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [(Pre)-Master-Secrect log filename shortcut in toolbar](/questions/53642/pre-master-secrect-log-filename-shortcut-in-toolbar)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53642-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53642-score" class="post-score" title="current number of votes">0</div><span id="post-53642-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I work with decrypted packet captures with pretty much every custom that I have a each time I load a new packet capture, I have to load a new pms file. In doing so I have to click on 'Edit --&gt; Prefernces --&gt; Click on Protocols --&gt; SSL -- &gt; (Pre)-Master-Secrect log filename --&gt; and browse to each pms file each time.</p><p>I wanted to know if there is a way to create a button or a shortcut for this on the toolbar so I do not have to go through all of that to load a new pms file each time.</p><p>Please let me know anything that would make this process faster.</p><p>Thanks,<br />
Damon Delvechio<br />
Network Support Engineer<br />
F5 Networks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pre" rel="tag" title="see questions tagged &#39;pre&#39;">pre</span> <span class="post-tag tag-link-secrect" rel="tag" title="see questions tagged &#39;secrect&#39;">secrect</span> <span class="post-tag tag-link-master" rel="tag" title="see questions tagged &#39;master&#39;">master</span> <span class="post-tag tag-link-log" rel="tag" title="see questions tagged &#39;log&#39;">log</span> <span class="post-tag tag-link-filename" rel="tag" title="see questions tagged &#39;filename&#39;">filename</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '16, 10:43</strong></p><img src="https://secure.gravatar.com/avatar/c5f9244703bb92a9197b21c2f6008d22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Delvechio&#39;s gravatar image" /><p><span>Delvechio</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Delvechio has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-53642" class="comments-container"></div><div id="comment-tools-53642" class="comment-tools"></div><div class="clear"></div><div id="comment-53642-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53671"></span>

<div id="answer-container-53671" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53671-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53671-score" class="post-score" title="current number of votes">0</div><span id="post-53671-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you tried right-clicking on the SSL line in the Packet Details pane and selecting the SSL preferences from there? Much faster...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '16, 06:13</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span> </br></p></div></div><div id="comments-container-53671" class="comments-container"></div><div id="comment-tools-53671" class="comment-tools"></div><div class="clear"></div><div id="comment-53671-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

