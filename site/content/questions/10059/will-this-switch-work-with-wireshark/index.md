+++
type = "question"
title = "Will this switch work with wireshark?"
description = '''I need to monitorate a VOIP call that have some errors, I found this switch: http://www.ebay.com/itm/8-Port-Nway-Network-TAP-10-100-Fast-Ethernet-LAN-Switch-/300691168886?pt=COMP_EN_Hubs&amp;amp;hash=item4602971e76#ht_4342wt_1344 Will it work?'''
date = "2012-04-11T10:24:00Z"
lastmod = "2012-04-11T16:35:00Z"
weight = 10059
keywords = [ "switch" ]
aliases = [ "/questions/10059" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Will this switch work with wireshark?](/questions/10059/will-this-switch-work-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10059-score" class="post-score" title="current number of votes">0</div><span id="post-10059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to monitorate a VOIP call that have some errors, I found this switch:</p><p><a href="http://www.ebay.com/itm/8-Port-Nway-Network-TAP-10-100-Fast-Ethernet-LAN-Switch-/300691168886?pt=COMP_EN_Hubs&amp;hash=item4602971e76#ht_4342wt_1344">http://www.ebay.com/itm/8-Port-Nway-Network-TAP-10-100-Fast-Ethernet-LAN-Switch-/300691168886?pt=COMP_EN_Hubs&amp;hash=item4602971e76#ht_4342wt_1344</a></p><p>Will it work?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-switch" rel="tag" title="see questions tagged &#39;switch&#39;">switch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '12, 10:24</strong></p><img src="https://secure.gravatar.com/avatar/6171c4fa7dca51c2761aa19d218a858e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Julian&#39;s gravatar image" /><p><span>Julian</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Julian has no accepted answers">0%</span></p></div></div><div id="comments-container-10059" class="comments-container"></div><div id="comment-tools-10059" class="comment-tools"></div><div class="clear"></div><div id="comment-10059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10068"></span>

<div id="answer-container-10068" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10068-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10068-score" class="post-score" title="current number of votes">0</div><span id="post-10068-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Doesn't look like it has a span/monitor port feature, at least it doesn't say anything about being configurable or packing a hard wired monitor port. I wouldn't buy it - it's cheap, but it probably won't do what you need.</p><p>Try going for a small switch that has a web configurable monitor port (I heard there are NetGear models out there that are nice and small, HP has some, etc...), or go for something like a Dualcomm DCSW-1005 that comes with a hard wired monitor port.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Apr '12, 16:35</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-10068" class="comments-container"></div><div id="comment-tools-10068" class="comment-tools"></div><div class="clear"></div><div id="comment-10068-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

