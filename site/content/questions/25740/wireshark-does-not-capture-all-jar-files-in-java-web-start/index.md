+++
type = "question"
title = "WireShark does not capture all jar files in java web start ?"
description = '''I am currently using WireShark to analyze the java web start. When the jnlp is running, it requires some jar files. When I open Java Control Panel -&amp;gt; Java Cache Viewer -&amp;gt; Resources, I see there are (e.g) 20 jar files is downloaded (the cache is cleared before launching the jnlp). But in WireSh...'''
date = "2013-10-07T21:24:00Z"
lastmod = "2013-10-10T14:45:00Z"
weight = 25740
keywords = [ "filter", "capture", "java", "wireshark" ]
aliases = [ "/questions/25740" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [WireShark does not capture all jar files in java web start ?](/questions/25740/wireshark-does-not-capture-all-jar-files-in-java-web-start)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25740-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25740-score" class="post-score" title="current number of votes">0</div><span id="post-25740-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am currently using WireShark to analyze the java web start. When the jnlp is running, it requires some jar files. When I open Java Control Panel -&gt; Java Cache Viewer -&gt; Resources, I see there are (e.g) 20 jar files is downloaded (the cache is cleared before launching the jnlp).</p><p>But in WireShark display, I just find out (e.g) 17 jar files.</p><p>And this is my filter expression : ip.dst == {myip} &amp;&amp; media</p><p>So what could be the problem or do I miss something ?</p><p>Thank you very much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-java" rel="tag" title="see questions tagged &#39;java&#39;">java</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '13, 21:24</strong></p><img src="https://secure.gravatar.com/avatar/467db1b60d943c80c14e73371fba9855?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tam%20Thai&#39;s gravatar image" /><p><span>Tam Thai</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tam Thai has no accepted answers">0%</span></p></div></div><div id="comments-container-25740" class="comments-container"></div><div id="comment-tools-25740" class="comment-tools"></div><div class="clear"></div><div id="comment-25740-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25896"></span>

<div id="answer-container-25896" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25896-score" class="post-score" title="current number of votes">1</div><span id="post-25896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Tam Thai has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>And this is my filter expression : ip.dst == {myip} &amp;&amp; <strong>media</strong></p></blockquote><p>I don't know what the filter <strong>media</strong> does, but it might be better to look for the Content-Type of JAR and/or JNLP "files".</p><p>JAR files:</p><blockquote><p>http.content_type contains "java-archive" or http.content_type contains "x-jar"</p></blockquote><p>JNLP files:</p><blockquote><p>http.content_type contains "x-java-jnlp-file"</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '13, 14:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25896" class="comments-container"></div><div id="comment-tools-25896" class="comment-tools"></div><div class="clear"></div><div id="comment-25896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

