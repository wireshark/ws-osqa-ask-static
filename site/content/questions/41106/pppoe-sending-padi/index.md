+++
type = "question"
title = "[closed] PPPoe - Sending PADI"
description = '''I&#x27;m listening all PADO packets : tshark -ni eth0 Y &quot; pppoe.code == 0x07 &quot; pppoed.tags.ac_name fields -T -e (Its ok) But to receive a PADO I need to first send a PADI for broadcast . How can I send this PADI ? Someone ?'''
date = "2015-04-01T12:43:00Z"
lastmod = "2015-04-01T12:43:00Z"
weight = 41106
keywords = [ "padi", "sending", "pppoe", "pado" ]
aliases = [ "/questions/41106" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] PPPoe - Sending PADI](/questions/41106/pppoe-sending-padi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41106-score" class="post-score" title="current number of votes">0</div><span id="post-41106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm listening all PADO packets :</p><p>tshark -ni eth0 Y " pppoe.code == 0x07 " pppoed.tags.ac_name fields -T -e (Its ok)</p><p>But to receive a PADO I need to first send a PADI for broadcast . How can I send this PADI ? Someone ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-padi" rel="tag" title="see questions tagged &#39;padi&#39;">padi</span> <span class="post-tag tag-link-sending" rel="tag" title="see questions tagged &#39;sending&#39;">sending</span> <span class="post-tag tag-link-pppoe" rel="tag" title="see questions tagged &#39;pppoe&#39;">pppoe</span> <span class="post-tag tag-link-pado" rel="tag" title="see questions tagged &#39;pado&#39;">pado</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 12:43</strong></p><img src="https://secure.gravatar.com/avatar/f1abd9a126fb53c9e9bf96036e7fd394?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="coopermine&#39;s gravatar image" /><p><span>coopermine</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="coopermine has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>01 Apr '15, 13:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-41106" class="comments-container"></div><div id="comment-tools-41106" class="comment-tools"></div><div class="clear"></div><div id="comment-41106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question! See my answer in your other question. In short: scapy!" by Kurt Knochner 01 Apr '15, 13:39

</div>

</div>

</div>

