+++
type = "question"
title = "Download tracking"
description = '''How to tracke downloadings in network using wireshrk?'''
date = "2013-04-15T03:32:00Z"
lastmod = "2013-04-15T07:46:00Z"
weight = 20410
keywords = [ "urgent-help" ]
aliases = [ "/questions/20410" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Download tracking](/questions/20410/download-tracking)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20410-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20410-score" class="post-score" title="current number of votes">0</div><span id="post-20410-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to tracke downloadings in network using wireshrk?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-urgent-help" rel="tag" title="see questions tagged &#39;urgent-help&#39;">urgent-help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '13, 03:32</strong></p><img src="https://secure.gravatar.com/avatar/136c3dc7e6d034596eae76da60648cbe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gaurav%20Tiwari&#39;s gravatar image" /><p><span>Gaurav Tiwari</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gaurav Tiwari has no accepted answers">0%</span></p></div></div><div id="comments-container-20410" class="comments-container"></div><div id="comment-tools-20410" class="comment-tools"></div><div class="clear"></div><div id="comment-20410-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20411"></span>

<div id="answer-container-20411" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20411-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20411-score" class="post-score" title="current number of votes">0</div><span id="post-20411-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't recommend trying to do that unless you have a small scope of applications you want to track downloads for, and know how to filter on downloads.</p><p>It is usually easier to enforce users to go through a proxy and use the proxy logs to track user activity.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '13, 03:35</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-20411" class="comments-container"></div><div id="comment-tools-20411" class="comment-tools"></div><div class="clear"></div><div id="comment-20411-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20420"></span>

<div id="answer-container-20420" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20420-score" class="post-score" title="current number of votes">0</div><span id="post-20420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is primarily a tool for network troubleshooting. Thus it is not optimized for the things you are asking for. You better use one of the following tools.</p><blockquote><p><a href="http://www.cockos.com/assniffer/">http://www.cockos.com/assniffer/</a><br />
<a href="http://www.xplico.org/">http://www.xplico.org/</a><br />
<a href="http://www.netresec.com/?page=NetworkMiner">http://www.netresec.com/?page=NetworkMiner</a><br />
</p></blockquote><p>Maybe the following list contains what you need.</p><blockquote><p><a href="http://www.forensicswiki.org/wiki/Tools:Network_Forensics">http://www.forensicswiki.org/wiki/Tools:Network_Forensics</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '13, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-20420" class="comments-container"></div><div id="comment-tools-20420" class="comment-tools"></div><div class="clear"></div><div id="comment-20420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

