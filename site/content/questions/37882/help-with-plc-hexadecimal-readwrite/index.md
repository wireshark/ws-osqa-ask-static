+++
type = "question"
title = "Help with PLC hexadecimal read/write"
description = '''Hi, I have a bit of a problem with a alien-bradley PLC model DR-4524. What im trying to find out is the hexadecimal read/write which communicates with the PLC, and was wondering if anyone can help me with this dilemma.I am attaching the wire shark log and a pdf of the manual of the PLC.  Thank you  ...'''
date = "2014-11-15T16:53:00Z"
lastmod = "2014-11-15T16:53:00Z"
weight = 37882
keywords = [ "plc", "allen-bradley" ]
aliases = [ "/questions/37882" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help with PLC hexadecimal read/write](/questions/37882/help-with-plc-hexadecimal-readwrite)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37882-score" class="post-score" title="current number of votes">0</div><span id="post-37882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have a bit of a problem with a alien-bradley PLC model DR-4524. What im trying to find out is the hexadecimal read/write which communicates with the PLC, and was wondering if anyone can help me with this dilemma.I am attaching the wire shark log and a pdf of the manual of the PLC. Thank you</p><p><a href="https://mega.co.nz/#!NkQBXKzJ!JzwJ55ciCNtv2fVTsPe60C8mIE0KTM1rqNCcIrOoAII">https://mega.co.nz/#!NkQBXKzJ!JzwJ55ciCNtv2fVTsPe60C8mIE0KTM1rqNCcIrOoAII</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plc" rel="tag" title="see questions tagged &#39;plc&#39;">plc</span> <span class="post-tag tag-link-allen-bradley" rel="tag" title="see questions tagged &#39;allen-bradley&#39;">allen-bradley</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '14, 16:53</strong></p><img src="https://secure.gravatar.com/avatar/dd2630227be6d715406847ade75c3d27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="killmasta93&#39;s gravatar image" /><p><span>killmasta93</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="killmasta93 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Nov '14, 10:23</strong> </span></p></div></div><div id="comments-container-37882" class="comments-container"></div><div id="comment-tools-37882" class="comment-tools"></div><div class="clear"></div><div id="comment-37882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

