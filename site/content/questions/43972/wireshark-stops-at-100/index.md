+++
type = "question"
title = "Wireshark stops at 100%"
description = '''Installed Wireshare on 2012 R2 server, worked fine but now when I try to open it stops at 100% and never opens. Tried to reinstall but it says wireshark processes running, dumpcap running in processes but I cannot end task. can&#x27;t restart this server until later tonight. Anyway to end the dumpcap pro...'''
date = "2015-07-08T09:41:00Z"
lastmod = "2015-07-08T09:41:00Z"
weight = 43972
keywords = [ "302", "dumpcap" ]
aliases = [ "/questions/43972" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark stops at 100%](/questions/43972/wireshark-stops-at-100)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43972-score" class="post-score" title="current number of votes">0</div><span id="post-43972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Installed Wireshare on 2012 R2 server, worked fine but now when I try to open it stops at 100% and never opens. Tried to reinstall but it says wireshark processes running, dumpcap running in processes but I cannot end task. can't restart this server until later tonight. Anyway to end the dumpcap processes?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-302" rel="tag" title="see questions tagged &#39;302&#39;">302</span> <span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jul '15, 09:41</strong></p><img src="https://secure.gravatar.com/avatar/8fc001e9a38f5d8b2212ab62604d7bc3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DMP&#39;s gravatar image" /><p><span>DMP</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DMP has no accepted answers">0%</span></p></div></div><div id="comments-container-43972" class="comments-container"></div><div id="comment-tools-43972" class="comment-tools"></div><div class="clear"></div><div id="comment-43972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

