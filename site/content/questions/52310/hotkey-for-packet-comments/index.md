+++
type = "question"
title = "Hotkey for Packet Comments..."
description = '''Can u pls add a hotkey for Packet Comments...'''
date = "2016-05-08T08:47:00Z"
lastmod = "2016-05-08T11:45:00Z"
weight = 52310
keywords = [ "request", "feature-request", "comments" ]
aliases = [ "/questions/52310" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Hotkey for Packet Comments...](/questions/52310/hotkey-for-packet-comments)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52310-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52310-score" class="post-score" title="current number of votes">0</div><span id="post-52310-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can u pls add a hotkey for Packet Comments...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-feature-request" rel="tag" title="see questions tagged &#39;feature-request&#39;">feature-request</span> <span class="post-tag tag-link-comments" rel="tag" title="see questions tagged &#39;comments&#39;">comments</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '16, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/f9df4644e8c578c944b68144e0e7adce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="13utters&#39;s gravatar image" /><p><span>13utters</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="13utters has no accepted answers">0%</span></p></div></div><div id="comments-container-52310" class="comments-container"></div><div id="comment-tools-52310" class="comment-tools"></div><div class="clear"></div><div id="comment-52310-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52312"></span>

<div id="answer-container-52312" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52312-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52312-score" class="post-score" title="current number of votes">1</div><span id="post-52312-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="13utters has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Open a feature request at <a href="https://bugs.wireshark.org">bugs.wireshark.org</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 May '16, 09:38</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-52312" class="comments-container"><span id="52316"></span><div id="comment-52316" class="comment"><div id="post-52316-score" class="comment-score"></div><div class="comment-text"><p>The item should be marked as "Enhancement", I've now done that. See bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12410">12410</a>.</p></div><div id="comment-52316-info" class="comment-info"><span class="comment-age">(08 May '16, 11:45)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-52312" class="comment-tools"></div><div class="clear"></div><div id="comment-52312-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

