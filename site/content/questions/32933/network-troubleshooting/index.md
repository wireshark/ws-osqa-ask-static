+++
type = "question"
title = "network troubleshooting"
description = '''This, I&#x27;m sure, is a common thread in these forums. I&#x27;m new to using WS and have resorted to it due to the fact that I&#x27;m having some major pains with connectivity, that I believe are related to my ISP. My connection is consistent, but, stutters and stumbles. One minute it is performing like the 10/1...'''
date = "2014-05-20T11:46:00Z"
lastmod = "2014-05-20T17:34:00Z"
weight = 32933
keywords = [ "of", "analysis", "automate", "capture" ]
aliases = [ "/questions/32933" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [network troubleshooting](/questions/32933/network-troubleshooting)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32933-score" class="post-score" title="current number of votes">0</div><span id="post-32933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This, I'm sure, is a common thread in these forums. I'm new to using WS and have resorted to it due to the fact that I'm having some major pains with connectivity, that I believe are related to my ISP. My connection is consistent, but, stutters and stumbles. One minute it is performing like the 10/10 connection it is, then the next minute it might perform like dial up. I can't put my finger on it and can't understand all the data that WS is gathering. Is there a way to automate analysis of the capture in order to get a comprehensive assessment of it? Just a thought. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-automate" rel="tag" title="see questions tagged &#39;automate&#39;">automate</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '14, 11:46</strong></p><img src="https://secure.gravatar.com/avatar/d1ce20bd649d2647cb07ac9a6c036e8c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="WSnewbe&#39;s gravatar image" /><p><span>WSnewbe</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="WSnewbe has no accepted answers">0%</span></p></div></div><div id="comments-container-32933" class="comments-container"><span id="32938"></span><div id="comment-32938" class="comment"><div id="post-32938-score" class="comment-score"></div><div class="comment-text"><blockquote><p>My <strong>connection is consistent</strong>, but, stutters and stumbles. One minute it is performing like the 10/10 connection it is</p></blockquote><p>What kind of connection is that? DSL, Cable modem, etc.?</p></div><div id="comment-32938-info" class="comment-info"><span class="comment-age">(20 May '14, 14:57)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="32940"></span><div id="comment-32940" class="comment"><div id="post-32940-score" class="comment-score"></div><div class="comment-text"><p>It is DSL 10 Mbps/10 Mbps. ISP isn't seeing an issue when they check, but, neither was I, at that time. I've replaced my Sonicwall with a Netgear SOHO as a trial. Same results. Replaced patch cable from router to wall(fiber to home). No internal modem from ISP. Bypassed router entirely, same issues. Switched NIC's on my PC, same results. Wired or wireless, same results. I decided to try WS as a method to prove to ISP that there is an issue with their line, maybe card beginning to fail at a switching station.....just not sure. Thanks</p></div><div id="comment-32940-info" class="comment-info"><span class="comment-age">(20 May '14, 17:34)</span> <span class="comment-user userinfo">WSnewbe</span></div></div></div><div id="comment-tools-32933" class="comment-tools"></div><div class="clear"></div><div id="comment-32933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

