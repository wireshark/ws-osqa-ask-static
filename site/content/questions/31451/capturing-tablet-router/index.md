+++
type = "question"
title = "Capturing Tablet  Router"
description = '''I&#x27;ve just started using Wireshark and have successfully captured data to and from the machine that I&#x27;m capturing on. I now have a more difficult problem. I need to debug the traffic between my tablet and my router. Specifically, the HTTP traffic. The tablet is wifi only, the router is, well wireless...'''
date = "2014-04-02T19:59:00Z"
lastmod = "2014-04-03T00:55:00Z"
weight = 31451
keywords = [ "router", "promiscuous", "tablet" ]
aliases = [ "/questions/31451" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing Tablet Router](/questions/31451/capturing-tablet-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31451-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31451-score" class="post-score" title="current number of votes">0</div><span id="post-31451-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've just started using Wireshark and have successfully captured data to and from the machine that I'm capturing on.</p><p>I now have a more difficult problem. I need to debug the traffic between my tablet and my router. Specifically, the HTTP traffic. The tablet is wifi only, the router is, well wireless. I also have a laptop that's wireless that I can capture on as well as a hub.</p><p>I tried to capture on the laptop when it was wirelessly connected to the router and in promiscuous mode, but didn't see anything. Is there a way to do this, and how should I cable it up?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-tablet" rel="tag" title="see questions tagged &#39;tablet&#39;">tablet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '14, 19:59</strong></p><img src="https://secure.gravatar.com/avatar/ed8e22d3946cc63006d2d4954765c9bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smithsa&#39;s gravatar image" /><p><span>smithsa</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smithsa has no accepted answers">0%</span></p></div></div><div id="comments-container-31451" class="comments-container"></div><div id="comment-tools-31451" class="comment-tools"></div><div class="clear"></div><div id="comment-31451-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31458"></span>

<div id="answer-container-31458" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31458-score" class="post-score" title="current number of votes">0</div><span id="post-31458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please read <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p><p>Since you are mainly interested in HTTP traffic, the easiest solution would be to install a packet capture app on the tablet. You can open the file in Wireshark afterwards.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Apr '14, 00:55</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-31458" class="comments-container"></div><div id="comment-tools-31458" class="comment-tools"></div><div class="clear"></div><div id="comment-31458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

