+++
type = "question"
title = "[closed] Search for Client Controller (botnet) in WireShark"
description = '''Hello guys, Iam searching for a client controller from a botnet in a PCAP file. Is there somebody who knows where and how I am able to find the client controller. The PCAP file is about 75mb with all kinds of different ip addresses. I am not familiar with WireShark yet. Any help will be appreciated....'''
date = "2013-12-22T06:05:00Z"
lastmod = "2014-01-09T08:04:00Z"
weight = 28321
keywords = [ "controller", "client" ]
aliases = [ "/questions/28321" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Search for Client Controller (botnet) in WireShark](/questions/28321/search-for-client-controller-botnet-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28321-score" class="post-score" title="current number of votes">0</div><span id="post-28321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello guys,</p><p>Iam searching for a client controller from a botnet in a PCAP file. Is there somebody who knows where and how I am able to find the client controller. The PCAP file is about 75mb with all kinds of different ip addresses.</p><p>I am not familiar with WireShark yet.</p><p>Any help will be appreciated.</p><p>Thanks in advance.</p><p>Regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-controller" rel="tag" title="see questions tagged &#39;controller&#39;">controller</span> <span class="post-tag tag-link-client" rel="tag" title="see questions tagged &#39;client&#39;">client</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Dec '13, 06:05</strong></p><img src="https://secure.gravatar.com/avatar/f9c753ce60c5d3192305d13872ae785e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nothingatall&#39;s gravatar image" /><p><span>nothingatall</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nothingatall has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>11 Jan '14, 12:26</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-28321" class="comments-container"><span id="28414"></span><div id="comment-28414" class="comment"><div id="post-28414-score" class="comment-score"></div><div class="comment-text"><p>Hello guys,</p><p>Thanks in advance for the answers. Thanks to you because I found what I was looking for!</p><p>Greets,</p><p>"from the Netherlands"</p></div><div id="comment-28414-info" class="comment-info"><span class="comment-age">(26 Dec '13, 13:35)</span> <span class="comment-user userinfo">nothingatall</span></div></div><span id="28428"></span><div id="comment-28428" class="comment"><div id="post-28428-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Thanks to you because I found what I was looking for!</p></blockquote><p>do you mind to tell us how?</p></div><div id="comment-28428-info" class="comment-info"><span class="comment-age">(27 Dec '13, 03:51)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28510"></span><div id="comment-28510" class="comment"><div id="post-28510-score" class="comment-score"></div><div class="comment-text"><p>I have the same problem with finding a client controller in a pcap file. I found the "server" but now i need to find the client controller.</p><p>Is there anyone who has a clue for me..?</p></div><div id="comment-28510-info" class="comment-info"><span class="comment-age">(01 Jan '14, 10:35)</span> <span class="comment-user userinfo">kweerd63</span></div></div><span id="28511"></span><div id="comment-28511" class="comment"><div id="post-28511-score" class="comment-score"></div><div class="comment-text"><p>I have found an webpage, but i can't see al the details i wish. It is asking for a token provided by the client controller.</p><p>I can't find that token. What do i miss and where do i have to look at?</p></div><div id="comment-28511-info" class="comment-info"><span class="comment-age">(01 Jan '14, 10:47)</span> <span class="comment-user userinfo">kweerd63</span></div></div><span id="28734"></span><div id="comment-28734" class="comment"><div id="post-28734-score" class="comment-score"></div><div class="comment-text"><p><span>@MarkV</span>: stop spamming the site with your comments to an already closed question (I mean the other one)!! You will do yourself (and us) a favor if you ask this kind of questions in a malware analysis forum!</p></div><div id="comment-28734-info" class="comment-info"><span class="comment-age">(09 Jan '14, 08:04)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28321" class="comment-tools"></div><div class="clear"></div><div id="comment-28321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Bill Meier 11 Jan '14, 12:26

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="28348"></span>

<div id="answer-container-28348" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28348-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28348-score" class="post-score" title="current number of votes">3</div><span id="post-28348-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there somebody who knows where and how I am able to find the client controller.</p></blockquote><p>well, as there are several hundred (or even thousand) different bots in the wild, it's hard to give any good advice, as most of them have different 'control protocols'. So, if you don't know what kind of botnet you are looking for and you don't know the protocol being used, the only way to find botnet communication in the capture file (if it exists at all), is the hard way by <strong>removing what does not look like botnet traffic</strong>. The traffic that remains is the botnet traffic.</p><p>Here is how I would start.</p><ul><li>First look at very long conversations from one client to one 'server' (Statistics -&gt; Conversations -&gt; TCP (perhaps also UDP)). Sort the connections for the <strong>duration</strong>. If there is one connection that lasts much longer as any of the other connections, take a closer look at the conversation (Follow, Reverse Resolve the 'server' IP, get the Geolocation of the IP, take a look at the transmitted data, etc.). Do everything that might help to identify botnet traffic in that conversation.</li><li>Then look at lots of short connections from one client, the either the same 'server' (perhaps also to a bunch of 'servers'). You can use the Conversation statistics as well</li><li>Get a list of destination addresses your client is communicating with (Statistics -&gt; Conversations -&gt; IPv4). Take those IP addresses and map them with a GeoIP tool (online, or with Wireshark: <a href="http://wiki.wireshark.org/HowToUseGeoIP).">http://wiki.wireshark.org/HowToUseGeoIP).</a> If you find any reagion of the world, that sounds 'suspicious' to you (because you would usually not communicate with North Korea), then have a closer look at the conversation with that destination IP address</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Dec '13, 11:57</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28348" class="comments-container"></div><div id="comment-tools-28348" class="comment-tools"></div><div class="clear"></div><div id="comment-28348-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="28377"></span>

<div id="answer-container-28377" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28377-score" class="post-score" title="current number of votes">2</div><span id="post-28377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would take a look at the RSA Security Analytics forum for this. They have lists of some fresh botnet traffic captures for botnets. The SA parses don't translate to ws filters but you can amend them. They list for user agent strings, which in WS would http user agent contains, dst ip, and ports etc etc. There are 1000's of bad ips, you could try the freeware version of netwitness. Just experiment and apply them as filters to see what you get. One method would be to put a pfsense firewall up at home then you can create a more permanent solution. Here is an example.</p><p>http.user_agent contains "GTB0.0" &lt;- botnet useragent</p><p>some ips to look for</p><p>ip.dst == 117.20.165.245 || ip.dst == 174.56.218.245 || ip.dst == 184.172.204.122 || ip.dst == 208.103.243.252 || ip.dst == 37.154.32.249 || ip.dst == 66.85.130.234 || ip.dst == 70.180.80.2 || ip.dst == 77.238.214.249 || ip.dst == 78.83.197.4 || ip.dst == 79.115.90.253 || ip.dst == 80.48.209.4 || ip.dst == 86.105.156.250</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Dec '13, 21:06</strong></p><img src="https://secure.gravatar.com/avatar/e5d5ba5d8ba47e0415a52577bf7bcc4a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rayyai%20beach&#39;s gravatar image" /><p><span>rayyai beach</span><br />
<span class="score" title="40 reputation points">40</span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rayyai beach has no accepted answers">0%</span></p></div></div><div id="comments-container-28377" class="comments-container"></div><div id="comment-tools-28377" class="comment-tools"></div><div class="clear"></div><div id="comment-28377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

