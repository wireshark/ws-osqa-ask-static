+++
type = "question"
title = "Extracting video payload from http protocol"
description = '''Hi, I have been capturing packets of downloaded videos over the internet. I am basically interested in its data, payload. My problem is that, it is downloaded as HTTP, hence, I can&#x27;t use any RTP or RTSP feature in wireshark to extract my data! Is it possible to differentiate those HTTP packets with ...'''
date = "2014-12-02T23:40:00Z"
lastmod = "2014-12-02T23:40:00Z"
weight = 38296
keywords = [ "video", "payload" ]
aliases = [ "/questions/38296" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extracting video payload from http protocol](/questions/38296/extracting-video-payload-from-http-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38296-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38296-score" class="post-score" title="current number of votes">0</div><span id="post-38296-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have been capturing packets of downloaded videos over the internet. I am basically interested in its data, payload. My problem is that, it is downloaded as HTTP, hence, I can't use any RTP or RTSP feature in wireshark to extract my data!</p><p>Is it possible to differentiate those HTTP packets with my video on it from others, or should I just resolve to capturing video from a regular network?</p><p>Thank you for your help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '14, 23:40</strong></p><img src="https://secure.gravatar.com/avatar/89fb05874c7ce1d94c7420c56fdaefb4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hiba&#39;s gravatar image" /><p><span>Hiba</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hiba has no accepted answers">0%</span></p></div></div><div id="comments-container-38296" class="comments-container"></div><div id="comment-tools-38296" class="comment-tools"></div><div class="clear"></div><div id="comment-38296-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

