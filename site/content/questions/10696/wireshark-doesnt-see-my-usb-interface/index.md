+++
type = "question"
title = "wireshark doesnt see my USB interface"
description = '''hi, what should i do to make wireshark see my interface its nat Ethernet its USB connection ..thank you for your help'''
date = "2012-05-05T06:09:00Z"
lastmod = "2012-05-06T08:29:00Z"
weight = 10696
keywords = [ "networkinterfaces" ]
aliases = [ "/questions/10696" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark doesnt see my USB interface](/questions/10696/wireshark-doesnt-see-my-usb-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10696-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10696-score" class="post-score" title="current number of votes">0</div><span id="post-10696-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, what should i do to make wireshark see my interface its nat Ethernet its USB connection ..thank you for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-networkinterfaces" rel="tag" title="see questions tagged &#39;networkinterfaces&#39;">networkinterfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '12, 06:09</strong></p><img src="https://secure.gravatar.com/avatar/b8cc3725722b4367b477357b71073c06?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shilan&#39;s gravatar image" /><p><span>shilan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shilan has no accepted answers">0%</span></p></div></div><div id="comments-container-10696" class="comments-container"><span id="10704"></span><div id="comment-10704" class="comment"><div id="post-10704-score" class="comment-score"></div><div class="comment-text"><p>what do you mean by: "my interface its nat Ethernet its USB connection"? Is this a USB network adapter? If so, what's the output of "tshark -D"?</p></div><div id="comment-10704-info" class="comment-info"><span class="comment-age">(06 May '12, 08:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-10696" class="comment-tools"></div><div class="clear"></div><div id="comment-10696-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

