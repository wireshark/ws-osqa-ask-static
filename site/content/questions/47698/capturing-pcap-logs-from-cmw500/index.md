+++
type = "question"
title = "capturing pcap logs from CMW500"
description = '''I am using message Analyzer to capture logs from Rhode &amp;amp; Schwarz CMW500. When I try to import the logs into pcap it is showing a message as &quot; no pcap data in this message&quot;. How to solve this problem? Help needed asap.'''
date = "2015-11-18T01:57:00Z"
lastmod = "2015-11-20T02:30:00Z"
weight = 47698
keywords = [ "pcap" ]
aliases = [ "/questions/47698" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [capturing pcap logs from CMW500](/questions/47698/capturing-pcap-logs-from-cmw500)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47698-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47698-score" class="post-score" title="current number of votes">0</div><span id="post-47698-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using message Analyzer to capture logs from Rhode &amp; Schwarz CMW500. When I try to import the logs into pcap it is showing a message as " no pcap data in this message". How to solve this problem? Help needed asap.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '15, 01:57</strong></p><img src="https://secure.gravatar.com/avatar/288d2ed72aaad934bc95ca0ddb4bdc2d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Koundinya&#39;s gravatar image" /><p><span>Koundinya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Koundinya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Nov '15, 04:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-47698" class="comments-container"><span id="47699"></span><div id="comment-47699" class="comment"><div id="post-47699-score" class="comment-score"></div><div class="comment-text"><p>what do you mean by "import logs into pcap"? Do you mean "import into Wireshark"? And are the logs you have in pcap format?</p></div><div id="comment-47699-info" class="comment-info"><span class="comment-age">(18 Nov '15, 02:03)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="47783"></span><div id="comment-47783" class="comment"><div id="post-47783-score" class="comment-score"></div><div class="comment-text"><p>I just have the logs in Message log. I have two options to import. Either to html or to pcap. When I select all the messages, I am able to import them to html. But when I try to import to pcap it is showing an error that "no pcap data in the messages selected".</p></div><div id="comment-47783-info" class="comment-info"><span class="comment-age">(20 Nov '15, 02:07)</span> <span class="comment-user userinfo">Koundinya</span></div></div><span id="47784"></span><div id="comment-47784" class="comment"><div id="post-47784-score" class="comment-score"></div><div class="comment-text"><p>Looks like this is an issue with the R&amp;S Message Analyzer, not relevant at tall to this Q&amp;A site dedicated to Wireshark. If I understood you properly, you should get in touch with your R&amp;S representative for support (presumably this export to pcap is useful only when you log IP packets).</p></div><div id="comment-47784-info" class="comment-info"><span class="comment-age">(20 Nov '15, 02:30)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-47698" class="comment-tools"></div><div class="clear"></div><div id="comment-47698-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

