+++
type = "question"
title = "What is the procedure to  add .dll plugins into wireshark plugins directory?"
description = '''Hi all, Thank you for your valuable support. I am trying to add some .dll files of custom protocol into wireshark plugin directory.After I added that wireshark is crashed. Can you help me how to add .dll files into wireshark plugin directory to resolve this issue. Thanks and regards, Sathish'''
date = "2015-04-01T23:34:00Z"
lastmod = "2015-04-02T02:34:00Z"
weight = 41118
keywords = [ "plugins" ]
aliases = [ "/questions/41118" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What is the procedure to add .dll plugins into wireshark plugins directory?](/questions/41118/what-is-the-procedure-to-add-dll-plugins-into-wireshark-plugins-directory)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41118-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41118-score" class="post-score" title="current number of votes">0</div><span id="post-41118-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Thank you for your valuable support.</p><p>I am trying to add some .dll files of custom protocol into wireshark plugin directory.After I added that wireshark is crashed. Can you help me how to add .dll files into wireshark plugin directory to resolve this issue.</p><p>Thanks and regards, Sathish</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plugins" rel="tag" title="see questions tagged &#39;plugins&#39;">plugins</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 23:34</strong></p><img src="https://secure.gravatar.com/avatar/7ba5607f38325cbf87766b918e1d76a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sathish%20kannan&#39;s gravatar image" /><p><span>Sathish kannan</span><br />
<span class="score" title="6 reputation points">6</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sathish kannan has no accepted answers">0%</span></p></div></div><div id="comments-container-41118" class="comments-container"></div><div id="comment-tools-41118" class="comment-tools"></div><div class="clear"></div><div id="comment-41118-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41128"></span>

<div id="answer-container-41128" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41128-score" class="post-score" title="current number of votes">1</div><span id="post-41128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would suspect that there is an issue with your custom DLL's. How were they compiled? Were they compiled for the <strong>specific</strong> version of Wireshark you're using?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '15, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-41128" class="comments-container"></div><div id="comment-tools-41128" class="comment-tools"></div><div class="clear"></div><div id="comment-41128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

