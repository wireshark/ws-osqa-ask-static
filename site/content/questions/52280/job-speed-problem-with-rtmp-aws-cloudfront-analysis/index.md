+++
type = "question"
title = "Job: Speed Problem with RTMP - AWS Cloudfront Analysis"
description = '''Hello Wireshark community! I have a big network speed problem with rtmp streams and would need some assistance in solving it. I have a decent experience in networks but not super-professional. So if there is someone who would show me how to handle such a problem. This would be very helpful. I am tot...'''
date = "2016-05-06T04:11:00Z"
lastmod = "2016-05-06T04:11:00Z"
weight = 52280
keywords = [ "latency", "rtmpt", "aws", "speed", "cloudfront" ]
aliases = [ "/questions/52280" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Job: Speed Problem with RTMP - AWS Cloudfront Analysis](/questions/52280/job-speed-problem-with-rtmp-aws-cloudfront-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52280-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52280-score" class="post-score" title="current number of votes">0</div><span id="post-52280-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Wireshark community!</p><p>I have a big network speed problem with rtmp streams and would need some assistance in solving it. I have a decent experience in networks but not super-professional.</p><p>So if there is someone who would show me how to handle such a problem. This would be very helpful. I am totally willing to pay for it.</p><p>Or can someone point me in the right direction - a guideline / how-to-steps to solve a AWS Cloudfront rtmp streaaming issue.</p><p>Thanks a lot all the best Mario</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span> <span class="post-tag tag-link-rtmpt" rel="tag" title="see questions tagged &#39;rtmpt&#39;">rtmpt</span> <span class="post-tag tag-link-aws" rel="tag" title="see questions tagged &#39;aws&#39;">aws</span> <span class="post-tag tag-link-speed" rel="tag" title="see questions tagged &#39;speed&#39;">speed</span> <span class="post-tag tag-link-cloudfront" rel="tag" title="see questions tagged &#39;cloudfront&#39;">cloudfront</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '16, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/ab2de9db115a47a56049e04822d3b474?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="laborc&#39;s gravatar image" /><p><span>laborc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="laborc has no accepted answers">0%</span></p></div></div><div id="comments-container-52280" class="comments-container"></div><div id="comment-tools-52280" class="comment-tools"></div><div class="clear"></div><div id="comment-52280-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

