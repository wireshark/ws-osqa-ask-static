+++
type = "question"
title = "how to monitor multiple route"
description = '''hello i am using fortigate firewall for my office network. i am using both WAN interface WAN1 is for secure site to site vpn connection with my head office and WAN2 for Internet use with ACL at the same time. how do i monitor WAN2 port because wireshark always monitor WAN1 port. your suggestion is v...'''
date = "2013-12-24T02:59:00Z"
lastmod = "2013-12-24T06:47:00Z"
weight = 28362
keywords = [ "mutliple" ]
aliases = [ "/questions/28362" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to monitor multiple route](/questions/28362/how-to-monitor-multiple-route)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28362-score" class="post-score" title="current number of votes">0</div><span id="post-28362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello i am using fortigate firewall for my office network. i am using both WAN interface WAN1 is for secure site to site vpn connection with my head office and WAN2 for Internet use with ACL at the same time. how do i monitor WAN2 port because wireshark always monitor WAN1 port. your suggestion is very much needed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mutliple" rel="tag" title="see questions tagged &#39;mutliple&#39;">mutliple</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Dec '13, 02:59</strong></p><img src="https://secure.gravatar.com/avatar/5439c7c650f56b24b0b0ec88a4ad1626?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ayion&#39;s gravatar image" /><p><span>ayion</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ayion has no accepted answers">0%</span></p></div></div><div id="comments-container-28362" class="comments-container"></div><div id="comment-tools-28362" class="comment-tools"></div><div class="clear"></div><div id="comment-28362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28369"></span>

<div id="answer-container-28369" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28369-score" class="post-score" title="current number of votes">0</div><span id="post-28369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>With Cisco gear you have to setup port monitoring or mirroring. You might to look at that. Or look at this <a href="http://community.spiceworks.com/topic/263126-wireshark-not-picking-up-all-traffic-on-mirrored-port">http://community.spiceworks.com/topic/263126-wireshark-not-picking-up-all-traffic-on-mirrored-port</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Dec '13, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/5b20990cd21bd091665e684410ebe9fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EdJ&#39;s gravatar image" /><p><span>EdJ</span><br />
<span class="score" title="16 reputation points">16</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EdJ has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Dec '13, 06:51</strong> </span></p></div></div><div id="comments-container-28369" class="comments-container"></div><div id="comment-tools-28369" class="comment-tools"></div><div class="clear"></div><div id="comment-28369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

