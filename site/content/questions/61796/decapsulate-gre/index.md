+++
type = "question"
title = "Decapsulate GRE"
description = '''Hi all Is it possible to decapsulate GRE traffic from a network capture file?'''
date = "2017-06-05T23:20:00Z"
lastmod = "2017-06-06T00:54:00Z"
weight = 61796
keywords = [ "decapsulation", "gre" ]
aliases = [ "/questions/61796" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decapsulate GRE](/questions/61796/decapsulate-gre)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61796-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61796-score" class="post-score" title="current number of votes">0</div><span id="post-61796-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all</p><p>Is it possible to decapsulate GRE traffic from a network capture file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decapsulation" rel="tag" title="see questions tagged &#39;decapsulation&#39;">decapsulation</span> <span class="post-tag tag-link-gre" rel="tag" title="see questions tagged &#39;gre&#39;">gre</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '17, 23:20</strong></p><img src="https://secure.gravatar.com/avatar/79664320f6310e8a1ec9202f90f75344?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="capsicumber&#39;s gravatar image" /><p><span>capsicumber</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="capsicumber has no accepted answers">0%</span></p></div></div><div id="comments-container-61796" class="comments-container"></div><div id="comment-tools-61796" class="comment-tools"></div><div class="clear"></div><div id="comment-61796-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61798"></span>

<div id="answer-container-61798" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61798-score" class="post-score" title="current number of votes">0</div><span id="post-61798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>TraceWrangler can do that via "Edit" task. You can download it here:</p><p><a href="https://www.tracewrangler.com">https://www.tracewrangler.com</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jun '17, 00:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-61798" class="comments-container"></div><div id="comment-tools-61798" class="comment-tools"></div><div class="clear"></div><div id="comment-61798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

