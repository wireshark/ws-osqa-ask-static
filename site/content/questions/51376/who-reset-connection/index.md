+++
type = "question"
title = "who reset connection"
description = '''258 16.884224000 10.0.0.1 10.0.0.3 TCP 54 59996 &amp;gt; hp-pdl-datastr [RST] Seq=4652 Win=0 Len=0 Can some tell me who reset the connection 10.0.0.1 OR 10.0.0.3 ????'''
date = "2016-04-02T10:52:00Z"
lastmod = "2016-04-02T11:09:00Z"
weight = 51376
keywords = [ "connection_reset" ]
aliases = [ "/questions/51376" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [who reset connection](/questions/51376/who-reset-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51376-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51376-score" class="post-score" title="current number of votes">0</div><span id="post-51376-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>258 16.884224000 10.0.0.1 10.0.0.3 TCP 54 59996 &gt; hp-pdl-datastr [RST] Seq=4652 Win=0 Len=0</p><p>Can some tell me who reset the connection 10.0.0.1 OR 10.0.0.3 ????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection_reset" rel="tag" title="see questions tagged &#39;connection_reset&#39;">connection_reset</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '16, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/c060481c970265db7cabe6501fbbe18c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="limelect&#39;s gravatar image" /><p><span>limelect</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="limelect has no accepted answers">0%</span></p></div></div><div id="comments-container-51376" class="comments-container"></div><div id="comment-tools-51376" class="comment-tools"></div><div class="clear"></div><div id="comment-51376-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51377"></span>

<div id="answer-container-51377" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51377-score" class="post-score" title="current number of votes">2</div><span id="post-51377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This packet with the Reset bit set is <em>from</em> 10.0.0.1, so 10.0.0.1 is terminating the connection.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '16, 11:09</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-51377" class="comments-container"></div><div id="comment-tools-51377" class="comment-tools"></div><div class="clear"></div><div id="comment-51377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

