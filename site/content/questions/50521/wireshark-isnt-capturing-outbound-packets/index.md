+++
type = "question"
title = "[closed] Wireshark isn&#x27;t capturing outbound packets"
description = '''I&#x27;ve not used Wireshark on my system in a couple months, but I don&#x27;t recall having this problem before. While troubleshooting a VPN connection for which all clients are suddenly disconnected after 1 minute, I cranked up Wireshark to see if I could find any interesting. Unfortunately, Wireshark isn&#x27;t...'''
date = "2016-02-25T15:39:00Z"
lastmod = "2016-02-25T23:48:00Z"
weight = 50521
keywords = [ "outbound" ]
aliases = [ "/questions/50521" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Wireshark isn't capturing outbound packets](/questions/50521/wireshark-isnt-capturing-outbound-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50521-score" class="post-score" title="current number of votes">0</div><span id="post-50521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've not used Wireshark on my system in a couple months, but I don't recall having this problem before. While troubleshooting a VPN connection for which all clients are suddenly disconnected after 1 minute, I cranked up Wireshark to see if I could find any interesting. Unfortunately, Wireshark isn't capturing any outbound packets - I see a FIN/ACK from the server, but I can't see the FIN that's supposedly is sourced from my machine. Is there a setting I"m missing? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outbound" rel="tag" title="see questions tagged &#39;outbound&#39;">outbound</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '16, 15:39</strong></p><img src="https://secure.gravatar.com/avatar/7aeba5d3861adfa40aaf06aab28647f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="baskervi&#39;s gravatar image" /><p><span>baskervi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="baskervi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>25 Feb '16, 23:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50521" class="comments-container"></div><div id="comment-tools-50521" class="comment-tools"></div><div class="clear"></div><div id="comment-50521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 25 Feb '16, 23:48

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50527"></span>

<div id="answer-container-50527" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50527-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50527-score" class="post-score" title="current number of votes">0</div><span id="post-50527-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Another duplication of this very frequent question, doesn't anyone search first?. Search for questions with the tag <a href="https://ask.wireshark.org/tags/outbound/">outgoing</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Feb '16, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50527" class="comments-container"></div><div id="comment-tools-50527" class="comment-tools"></div><div class="clear"></div><div id="comment-50527-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

