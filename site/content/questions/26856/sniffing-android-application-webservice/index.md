+++
type = "question"
title = "Sniffing android application webservice"
description = '''Hello, I have my phone using PC wi-fi connection in Promiscuous mode enabled, i want to sniff the HTTP webrequest some apps are performing to retrieve data from the web. I would like to aks if there is a specific guide, in particular how do i filter packets from a specific application? How do I read...'''
date = "2013-11-11T14:19:00Z"
lastmod = "2013-11-11T14:19:00Z"
weight = 26856
keywords = [ "sniffing", "android", "webservice" ]
aliases = [ "/questions/26856" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sniffing android application webservice](/questions/26856/sniffing-android-application-webservice)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26856-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26856-score" class="post-score" title="current number of votes">0</div><span id="post-26856-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have my phone using PC wi-fi connection in Promiscuous mode enabled, i want to sniff the HTTP webrequest some apps are performing to retrieve data from the web. I would like to aks if there is a specific guide, in particular how do i filter packets from a specific application? How do I read the data if encrypted and i do not know what hash is being used?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-webservice" rel="tag" title="see questions tagged &#39;webservice&#39;">webservice</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '13, 14:19</strong></p><img src="https://secure.gravatar.com/avatar/5b1bffafcdfd966e221410e42f31b5a0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="elnath78&#39;s gravatar image" /><p><span>elnath78</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="elnath78 has no accepted answers">0%</span></p></div></div><div id="comments-container-26856" class="comments-container"></div><div id="comment-tools-26856" class="comment-tools"></div><div class="clear"></div><div id="comment-26856-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

