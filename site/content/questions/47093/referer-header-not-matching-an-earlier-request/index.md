+++
type = "question"
title = "Referer header not matching an earlier request"
description = '''In the pcap file &quot;2015-10-23-Angler-EK-sends-TeslaCrypt-2.0-traffic.pcap&quot; in web page, packet 505 is a HTTP request whose Referer header almost match the HTTP request in packet 406, but misses a few bytes from the URL as in packet 406. Wonder why. Thanks.'''
date = "2015-10-30T08:37:00Z"
lastmod = "2015-10-30T08:37:00Z"
weight = 47093
keywords = [ "http" ]
aliases = [ "/questions/47093" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Referer header not matching an earlier request](/questions/47093/referer-header-not-matching-an-earlier-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47093-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47093-score" class="post-score" title="current number of votes">0</div><span id="post-47093-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In the pcap file "2015-10-23-Angler-EK-sends-TeslaCrypt-2.0-traffic.pcap" in <a href="http://malware-traffic-analysis.net/2015/10/23/index.html">web page</a>, packet 505 is a HTTP request whose Referer header almost match the HTTP request in packet 406, but misses a few bytes from the URL as in packet 406. Wonder why.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '15, 08:37</strong></p><img src="https://secure.gravatar.com/avatar/7bb7310612573625abd07a67f22724ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pktUser1001&#39;s gravatar image" /><p><span>pktUser1001</span><br />
<span class="score" title="201 reputation points">201</span><span title="49 badges"><span class="badge1">●</span><span class="badgecount">49</span></span><span title="50 badges"><span class="silver">●</span><span class="badgecount">50</span></span><span title="54 badges"><span class="bronze">●</span><span class="badgecount">54</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pktUser1001 has one accepted answer">12%</span></p></div></div><div id="comments-container-47093" class="comments-container"></div><div id="comment-tools-47093" class="comment-tools"></div><div class="clear"></div><div id="comment-47093-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

