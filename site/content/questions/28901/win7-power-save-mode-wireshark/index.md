+++
type = "question"
title = "Win7 Power Save Mode &amp; Wireshark"
description = '''While capture the PC mode is turned to Power Saving. Turning the PC ON again Wireshark comes along with following Error :&quot;The network adapter on which the capture was being done is no longer running; the capture has stopped.&quot; I need to capture frames immediately after waking up the PC, so why is con...'''
date = "2014-01-15T02:30:00Z"
lastmod = "2014-01-15T02:30:00Z"
weight = 28901
keywords = [ "win7", "powersave" ]
aliases = [ "/questions/28901" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Win7 Power Save Mode & Wireshark](/questions/28901/win7-power-save-mode-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28901-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28901-score" class="post-score" title="current number of votes">0</div><span id="post-28901-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>While capture the PC mode is turned to Power Saving. Turning the PC ON again Wireshark comes along with following Error :"The network adapter on which the capture was being done is no longer running; the capture has stopped."</p><p>I need to capture frames immediately after waking up the PC, so why is connection lost, and what can I do against it. Is this behaviour dokumented somewhere?</p><p>regards mahwrin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-win7" rel="tag" title="see questions tagged &#39;win7&#39;">win7</span> <span class="post-tag tag-link-powersave" rel="tag" title="see questions tagged &#39;powersave&#39;">powersave</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '14, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/88202bc45ee90934e6f34485ef1ef324?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mahwrin&#39;s gravatar image" /><p><span>mahwrin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mahwrin has no accepted answers">0%</span></p></div></div><div id="comments-container-28901" class="comments-container"></div><div id="comment-tools-28901" class="comment-tools"></div><div class="clear"></div><div id="comment-28901-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

