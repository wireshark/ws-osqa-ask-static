+++
type = "question"
title = "Windows 10 problems"
description = ''' Wireshark won&#x27;t search on windows 10 when I hit control+f. I then type in datr have string and packet details selected. I hit find and it won&#x27;t search. The find box just sits there. Anyone know why?'''
date = "2015-08-28T15:25:00Z"
lastmod = "2015-08-29T10:33:00Z"
weight = 45487
keywords = [ "windows", "10" ]
aliases = [ "/questions/45487" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 10 problems](/questions/45487/windows-10-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45487-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45487-score" class="post-score" title="current number of votes">0</div><span id="post-45487-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/wireshark_6iQXy2L.png" alt="alt text" /></p><p>Wireshark won't search on windows 10 when I hit control+f. I then type in datr have string and packet details selected. I hit find and it won't search. The find box just sits there. Anyone know why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-10" rel="tag" title="see questions tagged &#39;10&#39;">10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '15, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/0e0e8e24a459ba0e66983bed2e277847?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guts&#39;s gravatar image" /><p><span>Guts</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guts has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Aug '15, 16:24</strong> </span></p></div></div><div id="comments-container-45487" class="comments-container"><span id="45512"></span><div id="comment-45512" class="comment"><div id="post-45512-score" class="comment-score"></div><div class="comment-text"><p>You have direction set to 'up'. Does changing it to 'down' helps? Are you sure you have a packet that contains the 'datr' string? I do not have any search issue on my Windows 10 PC when running Wireshark 1.12.7.</p></div><div id="comment-45512-info" class="comment-info"><span class="comment-age">(29 Aug '15, 10:16)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div><span id="45513"></span><div id="comment-45513" class="comment"><div id="post-45513-score" class="comment-score"></div><div class="comment-text"><p>Thanks Pascal,</p><p>I ended up trying http.cookie contains datr in the filter box and that worked. I couldn't find any cookies so I probably have something setup wrong not sure.</p></div><div id="comment-45513-info" class="comment-info"><span class="comment-age">(29 Aug '15, 10:33)</span> <span class="comment-user userinfo">Guts</span></div></div></div><div id="comment-tools-45487" class="comment-tools"></div><div class="clear"></div><div id="comment-45487-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

