+++
type = "question"
title = "Wireshark run under ubuntu some errors"
description = '''Hi, i am using Xubuntu 12.04 version. I run into a problem and i do not find any solution.  First problem is using wireshark in user mode (non root user in linux) Statistics &amp;gt; Flow Graph section does not work properly and using this section cause wireshark close. (But it is working properly on wi...'''
date = "2014-12-16T10:31:00Z"
lastmod = "2014-12-16T11:02:00Z"
weight = 38602
keywords = [ "streamgraph", "wireshark", "follow.tcp.stream", "ubuntu" ]
aliases = [ "/questions/38602" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark run under ubuntu some errors](/questions/38602/wireshark-run-under-ubuntu-some-errors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38602-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38602-score" class="post-score" title="current number of votes">0</div><span id="post-38602-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i am using Xubuntu 12.04 version. I run into a problem and i do not find any solution. First problem is using wireshark in user mode (non root user in linux) Statistics &gt; Flow Graph section does not work properly and using this section cause wireshark close. (But it is working properly on windows.) Second problem is Statistics &gt; TCP StreamGraph section is not usable (looking grey) how can i resolve this issue ? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-streamgraph" rel="tag" title="see questions tagged &#39;streamgraph&#39;">streamgraph</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-follow.tcp.stream" rel="tag" title="see questions tagged &#39;follow.tcp.stream&#39;">follow.tcp.stream</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '14, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/acf0987edfb1d60402cb2b8a274058a6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sinack2117K&#39;s gravatar image" /><p><span>sinack2117K</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sinack2117K has no accepted answers">0%</span></p></div></div><div id="comments-container-38602" class="comments-container"><span id="38603"></span><div id="comment-38603" class="comment"><div id="post-38603-score" class="comment-score">1</div><div class="comment-text"><p>What is the Wireshark version you're using on Xubuntu and the version you used in Windows?</p></div><div id="comment-38603-info" class="comment-info"><span class="comment-age">(16 Dec '14, 10:49)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="38605"></span><div id="comment-38605" class="comment"><div id="post-38605-score" class="comment-score"></div><div class="comment-text"><p>on windows wireshark v1.12.0 and on linux wireshark v1.10.6</p></div><div id="comment-38605-info" class="comment-info"><span class="comment-age">(16 Dec '14, 11:02)</span> <span class="comment-user userinfo">sinack2117K</span></div></div></div><div id="comment-tools-38602" class="comment-tools"></div><div class="clear"></div><div id="comment-38602-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

