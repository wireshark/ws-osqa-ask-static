+++
type = "question"
title = "1496 continuation"
description = '''during capturing of packets through wireshark i&#x27;m only getting packets having details 1496 continuation and non http taffic but not the required ones even wen i&#x27;m not downloading anything..plzzz help'''
date = "2012-08-31T04:00:00Z"
lastmod = "2012-08-31T04:00:00Z"
weight = 13973
keywords = [ "continuation", "1496" ]
aliases = [ "/questions/13973" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [1496 continuation](/questions/13973/1496-continuation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13973-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13973-score" class="post-score" title="current number of votes">0</div><span id="post-13973-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>during capturing of packets through wireshark i'm only getting packets having details 1496 continuation and non http taffic but not the required ones even wen i'm not downloading anything..plzzz help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-continuation" rel="tag" title="see questions tagged &#39;continuation&#39;">continuation</span> <span class="post-tag tag-link-1496" rel="tag" title="see questions tagged &#39;1496&#39;">1496</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '12, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/e35e85b8ff18b4aea16c79b89bf3cedf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sky_walker&#39;s gravatar image" /><p><span>sky_walker</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sky_walker has no accepted answers">0%</span></p></div></div><div id="comments-container-13973" class="comments-container"></div><div id="comment-tools-13973" class="comment-tools"></div><div class="clear"></div><div id="comment-13973-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

