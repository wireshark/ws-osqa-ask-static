+++
type = "question"
title = "get an image from pcapng file by c# program"
description = '''I have network stream that i sniffing by wireshak.  for ex I sniff cats image from google images, by wireshark. save the sniff in pcapng file, it save in binary file . now I want to show the image one by one when get the stream from FF D8 to FF D9 the image seem to be less segments then the original...'''
date = "2013-03-08T01:28:00Z"
lastmod = "2013-03-08T01:28:00Z"
weight = 19298
keywords = [ "segment", "stream", "in" ]
aliases = [ "/questions/19298" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [get an image from pcapng file by c\# program](/questions/19298/get-an-image-from-pcapng-file-by-c-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19298-score" class="post-score" title="current number of votes">0</div><span id="post-19298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have network stream that i sniffing by wireshak.</p><p>for ex I sniff cats image from google images, by wireshark.</p><p>save the sniff in pcapng file, it save in binary file . now I want to show the image one by one</p><p>when get the stream from FF D8 to FF D9 the image seem to be less segments then the original image</p><p>who can I get the mising segments</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-segment" rel="tag" title="see questions tagged &#39;segment&#39;">segment</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-in" rel="tag" title="see questions tagged &#39;in&#39;">in</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '13, 01:28</strong></p><img src="https://secure.gravatar.com/avatar/35c76b7bcf74c8294ac14c408a9ecd9c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yehuda&#39;s gravatar image" /><p><span>yehuda</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yehuda has no accepted answers">0%</span></p></div></div><div id="comments-container-19298" class="comments-container"></div><div id="comment-tools-19298" class="comment-tools"></div><div class="clear"></div><div id="comment-19298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

