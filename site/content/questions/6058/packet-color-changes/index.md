+++
type = "question"
title = "Packet color changes"
description = '''The color of the &quot;selected packet&quot; changes when the mouse goes to the Packet Details or Packet Bytes windows, making it indistinguishable from all other displayed packets. Is there a way to keep the highlighted color in these cases? Thanks.'''
date = "2011-09-02T03:19:00Z"
lastmod = "2011-09-02T06:04:00Z"
weight = 6058
keywords = [ "color", "highlight", "mouse", "details", "packet" ]
aliases = [ "/questions/6058" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Packet color changes](/questions/6058/packet-color-changes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6058-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6058-score" class="post-score" title="current number of votes">0</div><span id="post-6058-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The color of the "selected packet" changes when the mouse goes to the Packet Details or Packet Bytes windows, making it indistinguishable from all other displayed packets. Is there a way to keep the highlighted color in these cases?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span> <span class="post-tag tag-link-highlight" rel="tag" title="see questions tagged &#39;highlight&#39;">highlight</span> <span class="post-tag tag-link-mouse" rel="tag" title="see questions tagged &#39;mouse&#39;">mouse</span> <span class="post-tag tag-link-details" rel="tag" title="see questions tagged &#39;details&#39;">details</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Sep '11, 03:19</strong></p><img src="https://secure.gravatar.com/avatar/d0bbc6c26216412e0b364f575c1a6022?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rambar48&#39;s gravatar image" /><p><span>Rambar48</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rambar48 has no accepted answers">0%</span></p></div></div><div id="comments-container-6058" class="comments-container"><span id="6061"></span><div id="comment-6061" class="comment"><div id="post-6061-score" class="comment-score"></div><div class="comment-text"><p>See <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=4445">this bug report</a> for some discussion about this issue.</p></div><div id="comment-6061-info" class="comment-info"><span class="comment-age">(02 Sep '11, 06:04)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-6058" class="comment-tools"></div><div class="clear"></div><div id="comment-6058-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

