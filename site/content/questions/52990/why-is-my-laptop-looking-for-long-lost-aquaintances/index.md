+++
type = "question"
title = "[closed] Why is my laptop looking for long lost aquaintances?"
description = '''I&#x27;ve just fired up WireShark and see the XP laptop it is running on is constantly sending out NBNS and DNS querys for machines it would have been connected to years ago. I thought I would find those machines in &quot;My Network Place&quot; and eliminate them but they don&#x27;t seem to be there. Is there somewhere...'''
date = "2016-05-27T05:41:00Z"
lastmod = "2016-05-27T05:58:00Z"
weight = 52990
keywords = [ "unexplained", "chatter" ]
aliases = [ "/questions/52990" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Why is my laptop looking for long lost aquaintances?](/questions/52990/why-is-my-laptop-looking-for-long-lost-aquaintances)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52990-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52990-score" class="post-score" title="current number of votes">0</div><span id="post-52990-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've just fired up WireShark and see the XP laptop it is running on is constantly sending out NBNS and DNS querys for machines it would have been connected to years ago. I thought I would find those machines in "My Network Place" and eliminate them but they don't seem to be there. Is there somewhere else I can go to eliminate this traffic?</p><p>Thanks in advance,</p><p>Joe</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unexplained" rel="tag" title="see questions tagged &#39;unexplained&#39;">unexplained</span> <span class="post-tag tag-link-chatter" rel="tag" title="see questions tagged &#39;chatter&#39;">chatter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '16, 05:41</strong></p><img src="https://secure.gravatar.com/avatar/c865b1b013fe2278796949e51435fc28?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JoeYoder&#39;s gravatar image" /><p><span>JoeYoder</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JoeYoder has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>27 May '16, 05:58</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-52990" class="comments-container"><span id="52992"></span><div id="comment-52992" class="comment"><div id="post-52992-score" class="comment-score"></div><div class="comment-text"><p>Not really an Ask Wireshark Question, more one for a Windows forum.</p></div><div id="comment-52992-info" class="comment-info"><span class="comment-age">(27 May '16, 05:58)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-52990" class="comment-tools"></div><div class="clear"></div><div id="comment-52990-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 27 May '16, 05:58

</div>

</div>

</div>

