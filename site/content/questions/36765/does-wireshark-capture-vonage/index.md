+++
type = "question"
title = "does Wireshark capture Vonage"
description = '''Can I use Wireshark installed on my pc and capture data (VOIP) when I&#x27;m on the phone with a vonage unit connected to my network. '''
date = "2014-10-01T19:22:00Z"
lastmod = "2014-10-20T05:04:00Z"
weight = 36765
keywords = [ "vonage", "voipcalls" ]
aliases = [ "/questions/36765" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [does Wireshark capture Vonage](/questions/36765/does-wireshark-capture-vonage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36765-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36765-score" class="post-score" title="current number of votes">0</div><span id="post-36765-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can I use Wireshark installed on my pc and capture data (VOIP) when I'm on the phone with a vonage unit connected to my network.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vonage" rel="tag" title="see questions tagged &#39;vonage&#39;">vonage</span> <span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '14, 19:22</strong></p><img src="https://secure.gravatar.com/avatar/96345856c44fa436ae6c54a4f6898c88?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="James%20LeRoy&#39;s gravatar image" /><p><span>James LeRoy</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="James LeRoy has no accepted answers">0%</span></p></div></div><div id="comments-container-36765" class="comments-container"></div><div id="comment-tools-36765" class="comment-tools"></div><div class="clear"></div><div id="comment-36765-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37190"></span>

<div id="answer-container-37190" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37190-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37190-score" class="post-score" title="current number of votes">0</div><span id="post-37190-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It depends on what you've got in your network. If the Vonage unit is directly connected to your home router then unless your router allows port mirroring then it'll be tough: you'll probably need to insert a switch that is smart enough to allow port mirroring or an old-fashioned hub.</p><p>See the <a href="http://wiki.wireshark.org/CaptureSetup">CaptureSetup</a> page for details. In particular, the <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Ethernet</a> page will probably help explain the challenges.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '14, 05:04</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37190" class="comments-container"></div><div id="comment-tools-37190" class="comment-tools"></div><div class="clear"></div><div id="comment-37190-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

