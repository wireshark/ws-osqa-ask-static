+++
type = "question"
title = "Won&#x27;t start on 2012R2, DLL error"
description = '''Hello -- I&#x27;ve tried the install and portable edition but I get the same error: &quot;The program can&#x27;t start because api-ms-win-crt-locale-l1-1-0.dll is missing from your computer.&quot; From what I&#x27;ve seen, that is a problem with the VC++ runtime, but my system has VC++ 2015 build 14.0.23026.0 installed alre...'''
date = "2017-07-31T18:33:00Z"
lastmod = "2017-11-02T11:23:00Z"
weight = 63267
keywords = [ "windows2012r2", "vc++" ]
aliases = [ "/questions/63267" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Won't start on 2012R2, DLL error](/questions/63267/wont-start-on-2012r2-dll-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63267-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63267-score" class="post-score" title="current number of votes">0</div><span id="post-63267-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello --</p><p>I've tried the install and portable edition but I get the same error:</p><p>"The program can't start because api-ms-win-crt-locale-l1-1-0.dll is missing from your computer."</p><p>From what I've seen, that is a problem with the VC++ runtime, but my system has VC++ 2015 build 14.0.23026.0 installed already..</p><p>I don't know what else it could be. Does anyone have any ideas?</p><p>Thanks, Greg</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows2012r2" rel="tag" title="see questions tagged &#39;windows2012r2&#39;">windows2012r2</span> <span class="post-tag tag-link-vc++" rel="tag" title="see questions tagged &#39;vc++&#39;">vc++</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '17, 18:33</strong></p><img src="https://secure.gravatar.com/avatar/27f0628987dd0ed3cca3c975840fa92f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gsmythe&#39;s gravatar image" /><p><span>gsmythe</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gsmythe has no accepted answers">0%</span></p></div></div><div id="comments-container-63267" class="comments-container"><span id="63280"></span><div id="comment-63280" class="comment"><div id="post-63280-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark?</p></div><div id="comment-63280-info" class="comment-info"><span class="comment-age">(01 Aug '17, 02:26)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="64334"></span><div id="comment-64334" class="comment"><div id="post-64334-score" class="comment-score"></div><div class="comment-text"><p>Having the same issue. Did you ever find a solution to this? I am running Wireshark 2.4.2 on Server 2012R2. Tried 64-bit and 32-bit...same results.</p></div><div id="comment-64334-info" class="comment-info"><span class="comment-age">(02 Nov '17, 11:23)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-63267" class="comment-tools"></div><div class="clear"></div><div id="comment-63267-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

