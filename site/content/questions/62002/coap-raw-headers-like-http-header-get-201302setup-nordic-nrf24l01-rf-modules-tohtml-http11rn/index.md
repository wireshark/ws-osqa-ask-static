+++
type = "question"
title = "[closed] CoAP raw headers like http header [GET /2013/02/setup-nordic-nrf24l01-rf-modules-to.html HTTP/1.1&#92;r&#92;n]"
description = '''Can any on please tell me CoAP header like http header I mentioned. please'''
date = "2017-06-14T02:51:00Z"
lastmod = "2017-06-14T06:43:00Z"
weight = 62002
keywords = [ "udp", "iot", "coap" ]
aliases = [ "/questions/62002" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] CoAP raw headers like http header \[GET /2013/02/setup-nordic-nrf24l01-rf-modules-to.html HTTP/1.1\\r\\n\]](/questions/62002/coap-raw-headers-like-http-header-get-201302setup-nordic-nrf24l01-rf-modules-tohtml-http11rn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62002-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62002-score" class="post-score" title="current number of votes">0</div><span id="post-62002-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can any on please tell me CoAP header like http header I mentioned. please</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-iot" rel="tag" title="see questions tagged &#39;iot&#39;">iot</span> <span class="post-tag tag-link-coap" rel="tag" title="see questions tagged &#39;coap&#39;">coap</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '17, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/88ef31b2d9a0ca87ae81dfd0207ff799?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sravan&#39;s gravatar image" /><p><span>sravan</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sravan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>15 Jun '17, 06:32</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-62002" class="comments-container"><span id="62009"></span><div id="comment-62009" class="comment"><div id="post-62009-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I'm not quite sure what the actual question is here... Are you looking for sample CoAP raw headers or a Wireshark display filter for them or...?</p></div><div id="comment-62009-info" class="comment-info"><span class="comment-age">(14 Jun '17, 06:43)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-62002" class="comment-tools"></div><div class="clear"></div><div id="comment-62002-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question - It appears you opened another (better worded) question for this: https://ask.wireshark.org/questions/62036/what-are-coap-raw-headers" by JeffMorriss 15 Jun '17, 06:32

</div>

</div>

</div>

