+++
type = "question"
title = "pdcp capture in lte"
description = '''Hi guys,  I&#x27;m new to wireshark, I&#x27;m working in the area of LTE. I want to capture the pdcp lte frames. How can i do that in wireshark ?? when i enter pdcp-lte in the filter option i&#x27;m no getting anything, as i&#x27;m connected to LAN or WLAN. so how can i capture ??? is there any repository where sample ...'''
date = "2011-07-04T22:33:00Z"
lastmod = "2011-07-06T09:01:00Z"
weight = 4901
keywords = [ "pdcp", "-lte", "capture" ]
aliases = [ "/questions/4901" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [pdcp capture in lte](/questions/4901/pdcp-capture-in-lte)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4901-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4901-score" class="post-score" title="current number of votes">0</div><span id="post-4901-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys, I'm new to wireshark, I'm working in the area of LTE. I want to capture the pdcp lte frames. How can i do that in wireshark ?? when i enter pdcp-lte in the filter option i'm no getting anything, as i'm connected to LAN or WLAN. so how can i capture ??? is there any repository where sample capture of pdcp frames are available ??? please help me....</p><p>Thanks in advance Nagarrajan Raghunathan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pdcp" rel="tag" title="see questions tagged &#39;pdcp&#39;">pdcp</span> <span class="post-tag tag-link--lte" rel="tag" title="see questions tagged &#39;-lte&#39;">-lte</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jul '11, 22:33</strong></p><img src="https://secure.gravatar.com/avatar/756521112ee43800524d67aaaf17ebf5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nagu99&#39;s gravatar image" /><p><span>nagu99</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nagu99 has no accepted answers">0%</span></p></div></div><div id="comments-container-4901" class="comments-container"></div><div id="comment-tools-4901" class="comment-tools"></div><div class="clear"></div><div id="comment-4901-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4927"></span>

<div id="answer-container-4927" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4927-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4927-score" class="post-score" title="current number of votes">0</div><span id="post-4927-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, Wireshark does not capture packets it reads a file of capyured packets and dissects them. Reading http://wiki.wireshark.org/PDCP-LTE?action=show&amp;redirect=Protocols%2Fpdcp-lte you need a device to capture the radio transmission and write that to a file or a logging function on the node(s). Regards Anders</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jul '11, 09:01</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-4927" class="comments-container"></div><div id="comment-tools-4927" class="comment-tools"></div><div class="clear"></div><div id="comment-4927-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

