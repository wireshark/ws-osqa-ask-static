+++
type = "question"
title = "Bypassing the wireshark/tshark interface to use the dissector"
description = '''Dear all, thanks for your attention. I need to design a class that dissects and filter a protocol without having to install the whole wireshark program. This is because i do not require the other dissectors which are unused and i would like my program to run at a high speed to accommodate traffic th...'''
date = "2011-07-19T01:39:00Z"
lastmod = "2011-07-19T20:09:00Z"
weight = 5115
keywords = [ "filter", "dissector" ]
aliases = [ "/questions/5115" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bypassing the wireshark/tshark interface to use the dissector](/questions/5115/bypassing-the-wiresharktshark-interface-to-use-the-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5115-score" class="post-score" title="current number of votes">0</div><span id="post-5115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all, thanks for your attention.</p><p>I need to design a class that dissects and filter a protocol without having to install the whole wireshark program. This is because i do not require the other dissectors which are unused and i would like my program to run at a high speed to accommodate traffic that i am receiving.</p><p>Is there a way to directly access and use the dissectors?</p><p>If yes, how? could you please provide me examples on how to use it in the class?</p><p>I have been following this conversation : http://www.wireshark.org/lists/wireshark-users/201006/msg00020.html and sadly the conclusion is to use tshark.</p><p>Thanks for your time</p><p>Regards, eddie choo</p><p>[reposted from the wireshark google groups http://groups.google.com/group/wireshark]</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '11, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/c1dac05d0e75992546b5da006c6b718e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eddie%20choo&#39;s gravatar image" /><p><span>eddie choo</span><br />
<span class="score" title="66 reputation points">66</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eddie choo has 2 accepted answers">66%</span></p></div></div><div id="comments-container-5115" class="comments-container"></div><div id="comment-tools-5115" class="comment-tools"></div><div class="clear"></div><div id="comment-5115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5139"></span>

<div id="answer-container-5139" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5139-score" class="post-score" title="current number of votes">0</div><span id="post-5139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="eddie choo has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have found a temporary solution to my problem, which is the sharktools application. http://www.mit.edu/~armenb/sharktools/</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jul '11, 20:09</strong></p><img src="https://secure.gravatar.com/avatar/c1dac05d0e75992546b5da006c6b718e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eddie%20choo&#39;s gravatar image" /><p><span>eddie choo</span><br />
<span class="score" title="66 reputation points">66</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eddie choo has 2 accepted answers">66%</span></p></div></div><div id="comments-container-5139" class="comments-container"></div><div id="comment-tools-5139" class="comment-tools"></div><div class="clear"></div><div id="comment-5139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

