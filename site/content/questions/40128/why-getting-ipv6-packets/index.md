+++
type = "question"
title = "why getting IPv6 packets?"
description = '''Hi there, My pc is not configured by IPv6 address scheme and not my network also. So why I am getting IPv6 packets. '''
date = "2015-02-27T06:37:00Z"
lastmod = "2015-02-28T02:47:00Z"
weight = 40128
keywords = [ "network", "why", "ipv4", "ipv6" ]
aliases = [ "/questions/40128" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [why getting IPv6 packets?](/questions/40128/why-getting-ipv6-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40128-score" class="post-score" title="current number of votes">0</div><span id="post-40128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>My pc is not configured by IPv6 address scheme and not my network also. So why I am getting IPv6 packets.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-why" rel="tag" title="see questions tagged &#39;why&#39;">why</span> <span class="post-tag tag-link-ipv4" rel="tag" title="see questions tagged &#39;ipv4&#39;">ipv4</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '15, 06:37</strong></p><img src="https://secure.gravatar.com/avatar/f50ae73ab931194845e2ff8ba442ffff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="madhuguptask&#39;s gravatar image" /><p><span>madhuguptask</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="madhuguptask has no accepted answers">0%</span></p></div></div><div id="comments-container-40128" class="comments-container"><span id="40130"></span><div id="comment-40130" class="comment"><div id="post-40130-score" class="comment-score"></div><div class="comment-text"><p>How have you determined your PC and all the devices in your network are not configured for IPv6?</p><p>What is your PC OS?</p></div><div id="comment-40130-info" class="comment-info"><span class="comment-age">(27 Feb '15, 06:47)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="40133"></span><div id="comment-40133" class="comment"><div id="post-40133-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your reply...actually i am a novice....win7 32b is my OS...My ISP gave my IPv4 address....so why I getting IPv6 packets...I am on LAN side using private IP...so every session must be initiate by me....am I correct?....If yes, then I supposed not to get IPv6 packets....Waiting for your reply....Thanks again</p></div><div id="comment-40133-info" class="comment-info"><span class="comment-age">(27 Feb '15, 08:57)</span> <span class="comment-user userinfo">madhuguptask</span></div></div></div><div id="comment-tools-40128" class="comment-tools"></div><div class="clear"></div><div id="comment-40128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40134"></span>

<div id="answer-container-40134" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40134-score" class="post-score" title="current number of votes">0</div><span id="post-40134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Most current OS's, and I include Win 7 in that, come IPv6 ready so although you don't have an IPv6 internet connection you can use IPv6 internally between your devices.</p><p>The traffic you are likely seeing is generated by the devices likely looking for IPv6 DHCP servers or Neighbour Solicitation requests or LLNMR queries. Nothing to worry about.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '15, 09:19</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-40134" class="comments-container"><span id="40143"></span><div id="comment-40143" class="comment"><div id="post-40143-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot.</p></div><div id="comment-40143-info" class="comment-info"><span class="comment-age">(28 Feb '15, 01:15)</span> <span class="comment-user userinfo">madhuguptask</span></div></div><span id="40144"></span><div id="comment-40144" class="comment"><div id="post-40144-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-40144-info" class="comment-info"><span class="comment-age">(28 Feb '15, 02:47)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-40134" class="comment-tools"></div><div class="clear"></div><div id="comment-40134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

