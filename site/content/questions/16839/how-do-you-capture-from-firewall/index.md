+++
type = "question"
title = "How do you capture from firewall?"
description = '''I have a WatchGuard XTM 505 Firewall appliance. I need to see the amount of data flow that comes in and out of the network over time to establish what kind of throughput the WAN connection demands. We are considering T1 as this is the best option available in our area. The point of this is to establ...'''
date = "2012-12-13T08:08:00Z"
lastmod = "2012-12-13T14:33:00Z"
weight = 16839
keywords = [ "firewall", "watchguard" ]
aliases = [ "/questions/16839" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do you capture from firewall?](/questions/16839/how-do-you-capture-from-firewall)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16839-score" class="post-score" title="current number of votes">0</div><span id="post-16839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a WatchGuard XTM 505 Firewall appliance. I need to see the amount of data flow that comes in and out of the network over time to establish what kind of throughput the WAN connection demands. We are considering T1 as this is the best option available in our area. The point of this is to establish if we need two Bonded T1s or if a single T1 will work for our purposes.</p><p>Thanks, jeffp88<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span> <span class="post-tag tag-link-watchguard" rel="tag" title="see questions tagged &#39;watchguard&#39;">watchguard</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '12, 08:08</strong></p><img src="https://secure.gravatar.com/avatar/1d7a7823ffde4a9e2e1e2798fa736e86?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jeffp88&#39;s gravatar image" /><p><span>jeffp88</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jeffp88 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-16839" class="comments-container"></div><div id="comment-tools-16839" class="comment-tools"></div><div class="clear"></div><div id="comment-16839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16852"></span>

<div id="answer-container-16852" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16852-score" class="post-score" title="current number of votes">0</div><span id="post-16852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm not familiar with the exact model, but doesn't it have some sort of statistics module on its own you can use? Capturing/analyzing packets is something that isn't always the only option, so if you have other means of getting what you need you should.</p><p>If you need to capture packets you could do that by capturing the WAN link port on the switch it is connected to. Take a look here for ways to get the packets: <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Dec '12, 14:33</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-16852" class="comments-container"></div><div id="comment-tools-16852" class="comment-tools"></div><div class="clear"></div><div id="comment-16852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

