+++
type = "question"
title = "Site Error - Uploading images not possible"
description = '''Hi, there seems to be a problem while uploading an image to the site. Error uploading file. Please contact the site administrator. Thank you. [Errno 13] Permission denied: &#x27;/web/ask.wireshark.org/osqa/forum/upfiles/Capture_pck.jpg&#x27;   Thanks for fixing it! UPDATE: Filed Bug https://bugs.wireshark.org...'''
date = "2013-03-06T09:51:00Z"
lastmod = "2013-05-28T12:34:00Z"
weight = 19233
keywords = [ "site", "error" ]
aliases = [ "/questions/19233" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Site Error - Uploading images not possible](/questions/19233/site-error-uploading-images-not-possible)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19233-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19233-score" class="post-score" title="current number of votes">1</div><span id="post-19233-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>there seems to be a problem while uploading an image to the site.</p><pre><code>Error uploading file. Please contact the site administrator. Thank you. [Errno 13] Permission denied: &#39;/web/ask.wireshark.org/osqa/forum/upfiles/Capture_pck.jpg&#39;</code></pre><p>Thanks for fixing it!</p><p>UPDATE: Filed Bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8444">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8444</a></p><p>UPDATE#2: Test upload. If you see an image, it works....</p><p><img src="https://osqa-ask.wireshark.org/upfiles/solved.jpg" alt="Test Upload JPEG" /><br />
<img src="https://osqa-ask.wireshark.org/upfiles/solved.png" alt="Test Upload PNG" /></p><p>UPDATE#3: Unfortunately no image is visible, hence it still does not work.</p><p>UPDATE#4: it works again, see images above.</p><p>Regards<br />
Kurt</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-site" rel="tag" title="see questions tagged &#39;site&#39;">site</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '13, 09:51</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 May '13, 12:34</strong> </span></p></div></div><div id="comments-container-19233" class="comments-container"><span id="20604"></span><div id="comment-20604" class="comment"><div id="post-20604-score" class="comment-score"></div><div class="comment-text"><p>it is still not possible to upload an image. Is this intentional?</p></div><div id="comment-20604-info" class="comment-info"><span class="comment-age">(18 Apr '13, 17:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-19233" class="comment-tools"></div><div class="clear"></div><div id="comment-19233-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21454"></span>

<div id="answer-container-21454" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21454-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21454-score" class="post-score" title="current number of votes">3</div><span id="post-21454-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This should be fixed now.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 May '13, 11:28</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></img></div></div><div id="comments-container-21454" class="comments-container"><span id="21521"></span><div id="comment-21521" class="comment"><div id="post-21521-score" class="comment-score"></div><div class="comment-text"><p>I'm sorry, but it still does not work. I can now upload an image, but it is neither shown as 'embedded' image nor can I access the link directly. See my update in the question. The linked image is not available:</p><blockquote><p><code>https://osqa-ask.wireshark.org/upfiles/solved.jpg</code><br />
<code>https://osqa-ask.wireshark.org/upfiles/solved.png</code><br />
</p></blockquote><p>The file is there as you can see when accessing the path /upfiles ( <a href="https://osqa-ask.wireshark.org/upfiles/">https://osqa-ask.wireshark.org/upfiles/</a> ), but access to the file is restricted.</p><p>BTW: Maybe it's better to disable directory browsing for the path /upfiles, you never know who is going to misuse the site for 'whatever'.</p><p>Regards<br />
Kurt</p></div><div id="comment-21521-info" class="comment-info"><span class="comment-age">(28 May '13, 07:20)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="21527"></span><div id="comment-21527" class="comment"><div id="post-21527-score" class="comment-score"></div><div class="comment-text"><p>OSQA was setting invalid permissions on newly-uploaded files. I fixed that part of the configuration and reset the permissions on the contents of upfiles/. I also disabled browsing as you suggested. Thanks!</p></div><div id="comment-21527-info" class="comment-info"><span class="comment-age">(28 May '13, 09:26)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="21536"></span><div id="comment-21536" class="comment"><div id="post-21536-score" class="comment-score"></div><div class="comment-text"><p>Thank you!</p></div><div id="comment-21536-info" class="comment-info"><span class="comment-age">(28 May '13, 12:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-21454" class="comment-tools"></div><div class="clear"></div><div id="comment-21454-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

