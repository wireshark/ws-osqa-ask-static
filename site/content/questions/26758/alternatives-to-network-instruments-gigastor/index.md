+++
type = "question"
title = "Alternatives to Network Instruments Gigastor"
description = '''All My company uses NI gigastor to mine packets from our network so that we can go back in time to troubleshoot issues found. I know of 2 products; NI&#x27;s Gigastor and Wild Packets, are there any other producsts I may not know about? My primary client for packet inspection is Wireshark. Can anyone hel...'''
date = "2013-11-07T17:21:00Z"
lastmod = "2013-11-19T13:48:00Z"
weight = 26758
keywords = [ "houric", "manded" ]
aliases = [ "/questions/26758" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Alternatives to Network Instruments Gigastor](/questions/26758/alternatives-to-network-instruments-gigastor)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26758-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26758-score" class="post-score" title="current number of votes">0</div><span id="post-26758-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>All My company uses NI gigastor to mine packets from our network so that we can go back in time to troubleshoot issues found. I know of 2 products; NI's Gigastor and Wild Packets, are there any other producsts I may not know about? My primary client for packet inspection is Wireshark. Can anyone help? Thank you,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-houric" rel="tag" title="see questions tagged &#39;houric&#39;">houric</span> <span class="post-tag tag-link-manded" rel="tag" title="see questions tagged &#39;manded&#39;">manded</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '13, 17:21</strong></p><img src="https://secure.gravatar.com/avatar/861fbd7e7725b35fc4b3bca22b7811b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrbmeisen&#39;s gravatar image" /><p><span>mrbmeisen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrbmeisen has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Nov '13, 13:52</strong> </span></p></div></div><div id="comments-container-26758" class="comments-container"><span id="26771"></span><div id="comment-26771" class="comment"><div id="post-26771-score" class="comment-score"></div><div class="comment-text"><p>How is this related to Wireshark?</p></div><div id="comment-26771-info" class="comment-info"><span class="comment-age">(08 Nov '13, 04:13)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="27114"></span><div id="comment-27114" class="comment"><div id="post-27114-score" class="comment-score"></div><div class="comment-text"><p>This not directly concerning Wireshark, but a tool used to mine packets, in which I use Wireshark to investigate problems in my network. I thought it wouldn't hurt to ask the largest packet collection community.</p></div><div id="comment-27114-info" class="comment-info"><span class="comment-age">(19 Nov '13, 13:34)</span> <span class="comment-user userinfo">mrbmeisen</span></div></div><span id="27115"></span><div id="comment-27115" class="comment"><div id="post-27115-score" class="comment-score"></div><div class="comment-text"><p>grahamb is right has nothing to do with wirshark. I will modify my question. If this should not be here I will kill my post.</p></div><div id="comment-27115-info" class="comment-info"><span class="comment-age">(19 Nov '13, 13:48)</span> <span class="comment-user userinfo">mrbmeisen</span></div></div></div><div id="comment-tools-26758" class="comment-tools"></div><div class="clear"></div><div id="comment-26758-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

