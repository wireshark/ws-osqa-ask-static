+++
type = "question"
title = "Preference Table updating from a file"
description = '''hello !  When i open some log , i want that certain information (such as IP and port number) get automatically populated into the the preference tables (the table has already been created). I am able to write the same information in a file using sprintf (in the required format) but dont know how to ...'''
date = "2011-08-06T13:20:00Z"
lastmod = "2011-08-08T00:54:00Z"
weight = 5546
keywords = [ "development", "preferences", "uat" ]
aliases = [ "/questions/5546" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Preference Table updating from a file](/questions/5546/preference-table-updating-from-a-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5546-score" class="post-score" title="current number of votes">0</div><span id="post-5546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello ! When i open some log , i want that certain information (such as IP and port number) get automatically populated into the the preference tables (the table has already been created). I am able to write the same information in a file using <code>sprintf</code> (in the required format) but dont know how to populate the preference table from the file. Can someone help me on this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span> <span class="post-tag tag-link-uat" rel="tag" title="see questions tagged &#39;uat&#39;">uat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Aug '11, 13:20</strong></p><img src="https://secure.gravatar.com/avatar/5eb32d89dd3b3acbd248ca48152c0a9d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="newbie&#39;s gravatar image" /><p><span>newbie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="newbie has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>08 Aug '11, 09:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-5546" class="comments-container"><span id="5547"></span><div id="comment-5547" class="comment"><div id="post-5547-score" class="comment-score"></div><div class="comment-text"><p>By "some log", do you mean a packet capture (e.g., <em>file.pcap</em>)? What is a "preference table"?</p></div><div id="comment-5547-info" class="comment-info"><span class="comment-age">(06 Aug '11, 14:34)</span> <span class="comment-user userinfo">helloworld</span></div></div><span id="5560"></span><div id="comment-5560" class="comment"><div id="post-5560-score" class="comment-score"></div><div class="comment-text"><p>yea ... a .pcap log file !!! a preference table is a set of user prefernces stored in a tabular form !!</p></div><div id="comment-5560-info" class="comment-info"><span class="comment-age">(07 Aug '11, 12:41)</span> <span class="comment-user userinfo">newbie</span></div></div><span id="5561"></span><div id="comment-5561" class="comment"><div id="post-5561-score" class="comment-score"></div><div class="comment-text"><p>By "a set of user prefernces stored in a tabular form" do you specifically mean a UAT? (I assume by "preferences" you mean the settings editable with Edit -&gt; Preferences.)</p></div><div id="comment-5561-info" class="comment-info"><span class="comment-age">(07 Aug '11, 14:47)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="5568"></span><div id="comment-5568" class="comment"><div id="post-5568-score" class="comment-score"></div><div class="comment-text"><p>yes , i am talking about UAT .. edit -&gt; preferences !!! how can i update a prefernce table from a file in the ".wireshark" folder containing the data(in the correct format) to be filled into the table ?</p></div><div id="comment-5568-info" class="comment-info"><span class="comment-age">(08 Aug '11, 00:54)</span> <span class="comment-user userinfo">newbie</span></div></div></div><div id="comment-tools-5546" class="comment-tools"></div><div class="clear"></div><div id="comment-5546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

