+++
type = "question"
title = "Mac OSX - filters don&#x27;t work"
description = '''I get errors when I try filters like &#x27;port 443&#x27; on Mac OSX. It says invalid filter error. I tried most of these and they almost all failed. http://wiki.wireshark.org/CaptureFilters'''
date = "2013-07-19T17:32:00Z"
lastmod = "2013-07-19T23:43:00Z"
weight = 23187
keywords = [ "macosx", "filters" ]
aliases = [ "/questions/23187" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac OSX - filters don't work](/questions/23187/mac-osx-filters-dont-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23187-score" class="post-score" title="current number of votes">2</div><span id="post-23187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I get errors when I try filters like 'port 443' on Mac OSX.</p><p>It says invalid filter error.</p><p>I tried most of these and they almost all failed.</p><p><a href="http://wiki.wireshark.org/CaptureFilters">http://wiki.wireshark.org/CaptureFilters</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '13, 17:32</strong></p><img src="https://secure.gravatar.com/avatar/035687df00d162cec025302373ebc076?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chovy&#39;s gravatar image" /><p><span>chovy</span><br />
<span class="score" title="41 reputation points">41</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chovy has no accepted answers">0%</span></p></div></div><div id="comments-container-23187" class="comments-container"><span id="23190"></span><div id="comment-23190" class="comment"><div id="post-23190-score" class="comment-score"></div><div class="comment-text"><p>So you typed that into the "Capture Filter" box in the dialog that, for Wireshark 1.8 and later, pops up when you double-click on an interface in the "Capture Options" dialog and, in Wireshark prior to 1.8, is in the "Capture Options" dialog itself?</p><p>(<em>DON'T</em> type capture filters into the "Filter:" box in the main window. Most capture filters won't work in that box on <em>ANY</em> OS.)</p></div><div id="comment-23190-info" class="comment-info"><span class="comment-age">(19 Jul '13, 23:43)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-23187" class="comment-tools"></div><div class="clear"></div><div id="comment-23187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

