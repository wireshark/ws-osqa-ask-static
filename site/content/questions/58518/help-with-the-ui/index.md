+++
type = "question"
title = "help with the UI"
description = '''Hey guys, I was wondering why in every tutorial I&#x27;ve watched on Wireshark, when they program first opens it has a gui and mine doesn&#x27;t. It just has welcome to wireshark.. capture then the ports ie ethernet, wifif etc.. Can I fix it?'''
date = "2017-01-04T16:15:00Z"
lastmod = "2017-01-05T03:06:00Z"
weight = 58518
keywords = [ "gui", "please", "help" ]
aliases = [ "/questions/58518" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [help with the UI](/questions/58518/help-with-the-ui)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58518-score" class="post-score" title="current number of votes">0</div><span id="post-58518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey guys,</p><p>I was wondering why in every tutorial I've watched on Wireshark, when they program first opens it has a gui and mine doesn't. It just has welcome to wireshark.. capture then the ports ie ethernet, wifif etc.. Can I fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-please" rel="tag" title="see questions tagged &#39;please&#39;">please</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '17, 16:15</strong></p><img src="https://secure.gravatar.com/avatar/a3d2237388c875a243a326eae9942fb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AnonStrous&#39;s gravatar image" /><p><span>AnonStrous</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AnonStrous has no accepted answers">0%</span></p></div></div><div id="comments-container-58518" class="comments-container"><span id="58520"></span><div id="comment-58520" class="comment"><div id="post-58520-score" class="comment-score"></div><div class="comment-text"><p>please post a screenshot of what you're seeing. make sure it doesn't have any private information on it of course.</p></div><div id="comment-58520-info" class="comment-info"><span class="comment-age">(04 Jan '17, 17:41)</span> <span class="comment-user userinfo">PEMinecraft</span></div></div></div><div id="comment-tools-58518" class="comment-tools"></div><div class="clear"></div><div id="comment-58518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58526"></span>

<div id="answer-container-58526" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58526-score" class="post-score" title="current number of votes">0</div><span id="post-58526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try starting wireshark-gtk (the so called legacy interface) and see if that look familiar. Most tutorials up to now have been of Wireshark 1.x, which has this user interface. The (relatively new) current interface of Wireshark 2.x, based on Qt, looks like you describe.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '17, 22:28</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-58526" class="comments-container"><span id="58531"></span><div id="comment-58531" class="comment"><div id="post-58531-score" class="comment-score"></div><div class="comment-text"><p>thanks Jaap. that was it. I appreciate it.</p></div><div id="comment-58531-info" class="comment-info"><span class="comment-age">(05 Jan '17, 01:48)</span> <span class="comment-user userinfo">AnonStrous</span></div></div><span id="58533"></span><div id="comment-58533" class="comment"><div id="post-58533-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-58533-info" class="comment-info"><span class="comment-age">(05 Jan '17, 03:06)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-58526" class="comment-tools"></div><div class="clear"></div><div id="comment-58526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

