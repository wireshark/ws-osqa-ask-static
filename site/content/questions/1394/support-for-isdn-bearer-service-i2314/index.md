+++
type = "question"
title = "Support for ISDN bearer service I.231.4?"
description = '''I would like to know if Wireshark (its Q.931 dissector) supports the ISDN bearer service I.231.4 (Circuit-mode, alternate speech / 64 kbit/s unrestricted, 8 kHz structured bearer service). If yes, what Q.931 messages are detected to alternate between speech and 64 kbps unrestricted? ([I.231.4/Clause...'''
date = "2010-12-18T09:02:00Z"
lastmod = "2010-12-18T09:02:00Z"
weight = 1394
keywords = [ "i.231.4", "service", "bearer", "isdn" ]
aliases = [ "/questions/1394" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Support for ISDN bearer service I.231.4?](/questions/1394/support-for-isdn-bearer-service-i2314)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1394-score" class="post-score" title="current number of votes">0</div><span id="post-1394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if Wireshark (its Q.931 dissector) supports the ISDN bearer service I.231.4 (Circuit-mode, alternate speech / 64 kbit/s unrestricted, 8 kHz structured bearer service).</p><p>If yes, what Q.931 messages are detected to alternate between speech and 64 kbps unrestricted? ([I.231.4/Clause 4.3.2.2] mentions the messages "invoke in-call modification request", "return result indication", "call modification indication" and "in-call modification return result", but it does not indicate what Q.931 messages must be use).</p><p>Thank you very much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-i.231.4" rel="tag" title="see questions tagged &#39;i.231.4&#39;">i.231.4</span> <span class="post-tag tag-link-service" rel="tag" title="see questions tagged &#39;service&#39;">service</span> <span class="post-tag tag-link-bearer" rel="tag" title="see questions tagged &#39;bearer&#39;">bearer</span> <span class="post-tag tag-link-isdn" rel="tag" title="see questions tagged &#39;isdn&#39;">isdn</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '10, 09:02</strong></p><img src="https://secure.gravatar.com/avatar/e384b74effc0260ec6cc4ba0dd92d079?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Javi%20Munoz&#39;s gravatar image" /><p><span>Javi Munoz</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Javi Munoz has no accepted answers">0%</span></p></div></div><div id="comments-container-1394" class="comments-container"></div><div id="comment-tools-1394" class="comment-tools"></div><div class="clear"></div><div id="comment-1394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

