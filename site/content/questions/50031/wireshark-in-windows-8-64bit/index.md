+++
type = "question"
title = "WireShark in Windows 8 64bit"
description = '''Hi, Please advise why wireshark 2.0.1 does not work properly in Windows 8?'''
date = "2016-02-09T16:29:00Z"
lastmod = "2016-02-09T16:40:00Z"
weight = 50031
keywords = [ "802.11" ]
aliases = [ "/questions/50031" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark in Windows 8 64bit](/questions/50031/wireshark-in-windows-8-64bit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50031-score" class="post-score" title="current number of votes">-1</div><span id="post-50031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Please advise why wireshark 2.0.1 does not work properly in Windows 8?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '16, 16:29</strong></p><img src="https://secure.gravatar.com/avatar/ee11fdfb44b749c6ec64a32fb46e9b19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="REDX&#39;s gravatar image" /><p><span>REDX</span><br />
<span class="score" title="5 reputation points">5</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="REDX has no accepted answers">0%</span></p></div></div><div id="comments-container-50031" class="comments-container"><span id="50033"></span><div id="comment-50033" class="comment"><div id="post-50033-score" class="comment-score"></div><div class="comment-text"><p>Can you be more specific?</p></div><div id="comment-50033-info" class="comment-info"><span class="comment-age">(09 Feb '16, 16:40)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-50031" class="comment-tools"></div><div class="clear"></div><div id="comment-50031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

