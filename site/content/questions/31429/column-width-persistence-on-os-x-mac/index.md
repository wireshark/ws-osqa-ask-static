+++
type = "question"
title = "Column Width persistence on OS X (Mac)"
description = '''Hi,  I just upgraded to 1.11.2 from 1.10 and it is a huge improvement. No longer need X11 :)  I am running OS X 10.9.2 and would like to know if it is possible to retain column sizes when quitting and restarting Wireshark? I figured out how to make the correct columns show up after a restart, but it...'''
date = "2014-04-02T13:17:00Z"
lastmod = "2014-04-02T13:17:00Z"
weight = 31429
keywords = [ "mac", "columns" ]
aliases = [ "/questions/31429" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Column Width persistence on OS X (Mac)](/questions/31429/column-width-persistence-on-os-x-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31429-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31429-score" class="post-score" title="current number of votes">0</div><span id="post-31429-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I just upgraded to 1.11.2 from 1.10 and it is a huge improvement. No longer need X11 :) I am running OS X 10.9.2 and would like to know if it is possible to retain column sizes when quitting and restarting Wireshark? I figured out how to make the correct columns show up after a restart, but it won't retain the column widths, even though I tried manually changing them in the Profiles.</p><p>Thanks, Dan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '14, 13:17</strong></p><img src="https://secure.gravatar.com/avatar/1ea9424595c86579604109955c251c73?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="evoixmr999&#39;s gravatar image" /><p><span>evoixmr999</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="evoixmr999 has no accepted answers">0%</span></p></div></div><div id="comments-container-31429" class="comments-container"></div><div id="comment-tools-31429" class="comment-tools"></div><div class="clear"></div><div id="comment-31429-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

