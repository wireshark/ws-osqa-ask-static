+++
type = "question"
title = "Building a custom Geo-IP DB using RFC 1918 space: Is it doable?"
description = '''Hi All,  If you&#x27;re aware of where your private address ranges are being announced (E.g. 172.17.0.0/16 from Mumbai, 192.168.17.0/24 from a branch office in Paris &amp;amp; 10.17.0.0/12 from Iceland; can you tell Wireshark how to map those private addresses? I assume that you can, but has anyone tried bef...'''
date = "2014-06-17T00:26:00Z"
lastmod = "2014-06-17T00:26:00Z"
weight = 33883
keywords = [ "rfc1918", "geo-ip", "wireshark" ]
aliases = [ "/questions/33883" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Building a custom Geo-IP DB using RFC 1918 space: Is it doable?](/questions/33883/building-a-custom-geo-ip-db-using-rfc-1918-space-is-it-doable)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33883-score" class="post-score" title="current number of votes">0</div><span id="post-33883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>If you're aware of where your private address ranges are being announced (E.g. 172.17.0.0/16 from Mumbai, 192.168.17.0/24 from a branch office in Paris &amp; 10.17.0.0/12 from Iceland; can you tell Wireshark how to map those private addresses? I assume that you can, but has anyone tried before?</p><p>Thanks in advance,</p><p>PR</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rfc1918" rel="tag" title="see questions tagged &#39;rfc1918&#39;">rfc1918</span> <span class="post-tag tag-link-geo-ip" rel="tag" title="see questions tagged &#39;geo-ip&#39;">geo-ip</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '14, 00:26</strong></p><img src="https://secure.gravatar.com/avatar/6a10412f32d2f801041e0feb48cda713?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packet-runner&#39;s gravatar image" /><p><span>packet-runner</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packet-runner has no accepted answers">0%</span></p></div></div><div id="comments-container-33883" class="comments-container"></div><div id="comment-tools-33883" class="comment-tools"></div><div class="clear"></div><div id="comment-33883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

