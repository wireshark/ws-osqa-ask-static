+++
type = "question"
title = "Trouble Pingin"
description = '''Why might a properly functioning site refuse to respond to a ping request?'''
date = "2011-03-18T11:55:00Z"
lastmod = "2011-03-20T12:58:00Z"
weight = 2926
keywords = [ "wireshark" ]
aliases = [ "/questions/2926" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Trouble Pingin](/questions/2926/trouble-pingin)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2926-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2926-score" class="post-score" title="current number of votes">0</div><span id="post-2926-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why might a properly functioning site refuse to respond to a ping request?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '11, 11:55</strong></p><img src="https://secure.gravatar.com/avatar/738ca6476540c9d3a5d0250a6adfbcc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pinging0874&#39;s gravatar image" /><p><span>Pinging0874</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pinging0874 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '11, 12:33</strong> </span></p></div></div><div id="comments-container-2926" class="comments-container"></div><div id="comment-tools-2926" class="comment-tools"></div><div class="clear"></div><div id="comment-2926-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2928"></span>

<div id="answer-container-2928" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2928-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2928-score" class="post-score" title="current number of votes">1</div><span id="post-2928-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Some firewalls (or their administrators) block ping. It's probably just paranoia on behalf of someone.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Mar '11, 12:46</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-2928" class="comments-container"><span id="2951"></span><div id="comment-2951" class="comment"><div id="post-2951-score" class="comment-score"></div><div class="comment-text"><p>I block ping on all of my public IPs. Maybe I AM paranoid, but there's no need for anyone to ping my servers as long as they serve HTTP, SMTP etc. just fine. Blocking pings has reduced random attacks as well as SPAM by more than 50% since the bad guys don't bother testing for open ports unless ping got an answer ;-)</p></div><div id="comment-2951-info" class="comment-info"><span class="comment-age">(20 Mar '11, 12:58)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2928" class="comment-tools"></div><div class="clear"></div><div id="comment-2928-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

