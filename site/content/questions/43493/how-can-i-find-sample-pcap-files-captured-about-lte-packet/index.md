+++
type = "question"
title = "How can I find sample pcap files captured about lte packet?"
description = '''Hi, I want to filter to the packet using tshark with lte dissectors. However, I have not sample pcap files about lte packet. So I would refer to &quot;sample captures&quot; webpage in Wireshark.org, but i can&#x27;t find it. (https://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;amp;do=upload_form&amp;amp;ticket...'''
date = "2015-06-23T22:59:00Z"
lastmod = "2016-07-21T10:30:00Z"
weight = 43493
keywords = [ "filter", "tshark", "lte" ]
aliases = [ "/questions/43493" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can I find sample pcap files captured about lte packet?](/questions/43493/how-can-i-find-sample-pcap-files-captured-about-lte-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43493-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43493-score" class="post-score" title="current number of votes">0</div><span id="post-43493-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I want to filter to the packet using tshark with lte dissectors.</p><p>However, I have not sample pcap files about lte packet.</p><p>So I would refer to "sample captures" webpage in Wireshark.org, but i can't find it. (<a href="https://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=upload_form&amp;ticket=0055894b0c.6d3d4b09d8bb380d5fc7ae14e9651eb1505f0b46&amp;target=PROTO.pcap">https://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=upload_form&amp;ticket=0055894b0c.6d3d4b09d8bb380d5fc7ae14e9651eb1505f0b46&amp;target=PROTO.pcap</a>)</p><p>My question is,</p><p>Where is the files captured LTE packet such as RLC, MAC, ... "sample captures" webpage in Wireshark.org?</p><p>Or, How can I find sample files about that?</p><p>Thank you for read my question.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-lte" rel="tag" title="see questions tagged &#39;lte&#39;">lte</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '15, 22:59</strong></p><img src="https://secure.gravatar.com/avatar/599cdb677184139175b5e763dce1dbb0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="superstar999&#39;s gravatar image" /><p><span>superstar999</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="superstar999 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Jun '15, 23:03</strong> </span></p></div></div><div id="comments-container-43493" class="comments-container"></div><div id="comment-tools-43493" class="comment-tools"></div><div class="clear"></div><div id="comment-43493-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54228"></span>

<div id="answer-container-54228" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54228-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54228-score" class="post-score" title="current number of votes">0</div><span id="post-54228-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I realize this question is quite old now, but fyi there are sample captures available at <a href="https://www.wireshark.org/~darkjames/capture-files.txt">https://www.wireshark.org/~darkjames/capture-files.txt</a>.</p><p>The list was generated by Jakub Zawadzki years ago so unfortunately it's not up to date with all of the capture files available, but there's still a large number of them listed nonetheless.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '16, 10:30</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54228" class="comments-container"></div><div id="comment-tools-54228" class="comment-tools"></div><div class="clear"></div><div id="comment-54228-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

