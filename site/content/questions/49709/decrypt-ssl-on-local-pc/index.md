+++
type = "question"
title = "decrypt ssl on local pc"
description = '''i am trying to decrypt https traffic on my local pc , but i have many troubles :  * first of all how can i extract certificates from ubuntu .  * secondly how can i use it to decrypt https traffic .  Thank you .'''
date = "2016-02-02T01:37:00Z"
lastmod = "2016-02-02T01:37:00Z"
weight = 49709
keywords = [ "https" ]
aliases = [ "/questions/49709" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decrypt ssl on local pc](/questions/49709/decrypt-ssl-on-local-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49709-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49709-score" class="post-score" title="current number of votes">0</div><span id="post-49709-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am trying to decrypt https traffic on my local pc , but i have many troubles : * first of all how can i extract certificates from ubuntu . * secondly how can i use it to decrypt https traffic . Thank you .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '16, 01:37</strong></p><img src="https://secure.gravatar.com/avatar/d6b8e14571f970be8d0e0c1973abc2d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fouad%20Berjewe&#39;s gravatar image" /><p><span>Fouad Berjewe</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fouad Berjewe has no accepted answers">0%</span></p></div></div><div id="comments-container-49709" class="comments-container"></div><div id="comment-tools-49709" class="comment-tools"></div><div class="clear"></div><div id="comment-49709-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

