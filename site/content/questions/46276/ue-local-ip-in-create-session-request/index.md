+++
type = "question"
title = "UE Local IP in Create session Request"
description = '''Hi, How to detect that UE LOCAL IP is received in Create session Request message??? Regards, Pk'''
date = "2015-09-30T01:31:00Z"
lastmod = "2015-09-30T01:31:00Z"
weight = 46276
keywords = [ "decryption" ]
aliases = [ "/questions/46276" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [UE Local IP in Create session Request](/questions/46276/ue-local-ip-in-create-session-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46276-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46276-score" class="post-score" title="current number of votes">0</div><span id="post-46276-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, How to detect that UE LOCAL IP is received in Create session Request message??? Regards, Pk</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '15, 01:31</strong></p><img src="https://secure.gravatar.com/avatar/742726c0cb1266a3497001c62e26837d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prasantanayak25&#39;s gravatar image" /><p><span>prasantanayak25</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prasantanayak25 has no accepted answers">0%</span></p></div></div><div id="comments-container-46276" class="comments-container"></div><div id="comment-tools-46276" class="comment-tools"></div><div class="clear"></div><div id="comment-46276-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

