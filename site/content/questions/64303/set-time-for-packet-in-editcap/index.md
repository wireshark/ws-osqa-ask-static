+++
type = "question"
title = "&quot;Set time for packet&quot; in editcap"
description = '''Hi all, I need to find a command that acts as the &quot;Set time for packet &amp;lt;x&amp;gt; to &amp;lt;date/time&amp;gt;&quot; feature, which is available in Wireshark, under Edit&amp;gt;Time Shift on mac (or command+shift+T). All I found is &quot;editcap -t X&quot;, nevertheless this increase or decrease the time to X for all packets. ...'''
date = "2017-10-27T11:47:00Z"
lastmod = "2017-10-27T11:47:00Z"
weight = 64303
keywords = [ "editcap" ]
aliases = [ "/questions/64303" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# ["Set time for packet" in editcap](/questions/64303/set-time-for-packet-in-editcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64303-score" class="post-score" title="current number of votes">0</div><span id="post-64303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I need to find a command that acts as the "Set time for packet &lt;x&gt; to &lt;date/time&gt;" feature, which is available in Wireshark, under Edit&gt;Time Shift on mac (or command+shift+T). All I found is "editcap -t X", nevertheless this increase or decrease the time to X for all packets.</p><p>Is it possible to do so without calculating the difference between the time in the capture and the time which I intend to put?</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-editcap" rel="tag" title="see questions tagged &#39;editcap&#39;">editcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '17, 11:47</strong></p><img src="https://secure.gravatar.com/avatar/f6a9243e5e7143cf4fe9087707f66856?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="paolofusc&#39;s gravatar image" /><p><span>paolofusc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="paolofusc has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '17, 12:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-64303" class="comments-container"></div><div id="comment-tools-64303" class="comment-tools"></div><div class="clear"></div><div id="comment-64303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

