+++
type = "question"
title = "filter line is not checked or colourized in kali linux"
description = '''filter line is not checked or colourized in kali linux. Is it possible to switch on checking and colourizing of the filter input field in Wireshark'''
date = "2014-05-07T06:38:00Z"
lastmod = "2014-05-07T14:09:00Z"
weight = 32593
keywords = [ "filter", "field" ]
aliases = [ "/questions/32593" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [filter line is not checked or colourized in kali linux](/questions/32593/filter-line-is-not-checked-or-colourized-in-kali-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32593-score" class="post-score" title="current number of votes">0</div><span id="post-32593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>filter line is not checked or colourized in kali linux.</p><p>Is it possible to switch on checking and colourizing of the filter input field in Wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-field" rel="tag" title="see questions tagged &#39;field&#39;">field</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 May '14, 06:38</strong></p><img src="https://secure.gravatar.com/avatar/9f00259348e95afd140e30d1daab7b27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="spacewalker56&#39;s gravatar image" /><p><span>spacewalker56</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="spacewalker56 has no accepted answers">0%</span></p></div></div><div id="comments-container-32593" class="comments-container"></div><div id="comment-tools-32593" class="comment-tools"></div><div class="clear"></div><div id="comment-32593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32613"></span>

<div id="answer-container-32613" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32613-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32613-score" class="post-score" title="current number of votes">0</div><span id="post-32613-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is your Wireshark version using GTK 3.0 or later? According to my own tests the background color of the filter box only works with GTK 2.X framework.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 May '14, 11:09</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-32613" class="comments-container"><span id="32621"></span><div id="comment-32621" class="comment"><div id="post-32621-score" class="comment-score"></div><div class="comment-text"><p>I used the command: pkg-config --modversion gtk+-2.0 to find out I have version 2.24.</p><p>I work in a virtual Kali under parallels with the parallels tools installed.</p></div><div id="comment-32621-info" class="comment-info"><span class="comment-age">(07 May '14, 14:09)</span> <span class="comment-user userinfo">spacewalker56</span></div></div></div><div id="comment-tools-32613" class="comment-tools"></div><div class="clear"></div><div id="comment-32613-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

