+++
type = "question"
title = "various errors"
description = '''I have a windows 7 64 bit computer. I have installed the latest version of acrylic and it runs just fine with my Afla usb wireless device. When I start the latest version of wireshark, it can see the wireless device. When I start to capture it runs for a few minutes before it returns one of these er...'''
date = "2015-07-30T14:02:00Z"
lastmod = "2015-07-30T14:32:00Z"
weight = 44651
keywords = [ "capture", "adapter", "dumpcap", "child" ]
aliases = [ "/questions/44651" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [various errors](/questions/44651/various-errors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44651-score" class="post-score" title="current number of votes">0</div><span id="post-44651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a windows 7 64 bit computer. I have installed the latest version of acrylic and it runs just fine with my Afla usb wireless device. When I start the latest version of wireshark, it can see the wireless device. When I start to capture it runs for a few minutes before it returns one of these errors:</p><p>The network adapter on which the capture was being done is no longer running.</p><p>Or</p><p>Dumpacp has stopped working</p><p>followed by:</p><p>Child dumpcap process died. Access Violation.</p><p>When the first error occurs, acrylic stops working and I have to unplug and replug the wireless device to make it work again.</p><p>When the second error occurs, I just have to restart wireshark and I can capture data for a few minutes before I get an error again.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-child" rel="tag" title="see questions tagged &#39;child&#39;">child</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '15, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/efddbf0ae6e0c4b1cd182c684814c087?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rlwhiterose&#39;s gravatar image" /><p><span>rlwhiterose</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rlwhiterose has no accepted answers">0%</span></p></div></div><div id="comments-container-44651" class="comments-container"></div><div id="comment-tools-44651" class="comment-tools"></div><div class="clear"></div><div id="comment-44651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44652"></span>

<div id="answer-container-44652" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44652-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44652-score" class="post-score" title="current number of votes">0</div><span id="post-44652-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>These issues are likely to be with the Acrylic drivers. You'll need to take up the problem with their support.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '15, 14:32</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-44652" class="comments-container"></div><div id="comment-tools-44652" class="comment-tools"></div><div class="clear"></div><div id="comment-44652-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

