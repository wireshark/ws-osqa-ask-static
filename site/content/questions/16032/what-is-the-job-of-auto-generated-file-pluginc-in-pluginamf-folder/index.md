+++
type = "question"
title = "What is the job of auto generated file plugin.c in plugin/amf folder?"
description = '''I am writing a plugin for amf. The following file is auto generated. --------8&amp;lt;-------------------------------------------------------------- /*  * Do not modify this file.  *  * It is created automatically by Makefile or Makefile.nmake.  */  #ifdef HAVE_CONFIG_H # include &quot;config.h&quot; #endif  #inc...'''
date = "2012-11-18T21:48:00Z"
lastmod = "2012-11-18T23:16:00Z"
weight = 16032
keywords = [ "plugin.c" ]
aliases = [ "/questions/16032" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [What is the job of auto generated file plugin.c in plugin/amf folder?](/questions/16032/what-is-the-job-of-auto-generated-file-pluginc-in-pluginamf-folder)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16032-score" class="post-score" title="current number of votes">0</div><span id="post-16032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am writing a plugin for amf. The following file is auto generated.</p><pre><code>--------8&lt;--------------------------------------------------------------
/*
 * Do not modify this file.
 *
 * It is created automatically by Makefile or Makefile.nmake.
 */

#ifdef HAVE_CONFIG_H
# include &quot;config.h&quot;
#endif

#include &lt;gmodule.h&gt;

#include &quot;moduleinfo.h&quot;

#ifndef ENABLE_STATIC
G_MODULE_EXPORT const gchar version[] = VERSION;

/* Start the functions we need for the plugin stuff */

G_MODULE_EXPORT void
plugin_register (void)
{
  {extern void proto_register_amf (void); proto_register_amf ();}
}

G_MODULE_EXPORT void
plugin_reg_handoff(void)
{
  {extern void proto_reg_handoff_amf (void); proto_reg_handoff_amf ();}
}
#endif

--------8&lt;--------------------------------------------------------------</code></pre><p>What is the job of this file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plugin.c" rel="tag" title="see questions tagged &#39;plugin.c&#39;">plugin.c</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '12, 21:48</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Nov '12, 23:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-16032" class="comments-container"></div><div id="comment-tools-16032" class="comment-tools"></div><div class="clear"></div><div id="comment-16032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16036"></span>

<div id="answer-container-16036" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16036-score" class="post-score" title="current number of votes">0</div><span id="post-16036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Akhil has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This autogenerated file forms the interface between the specific dissector code you can write and the Wireshark plugin infrastructure. This is the interface that the Wireshark plugin registration code looks for when loading your plugin.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '12, 23:15</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-16036" class="comments-container"><span id="16037"></span><div id="comment-16037" class="comment"><div id="post-16037-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot Jaap</p></div><div id="comment-16037-info" class="comment-info"><span class="comment-age">(18 Nov '12, 23:16)</span> <span class="comment-user userinfo">Akhil</span></div></div></div><div id="comment-tools-16036" class="comment-tools"></div><div class="clear"></div><div id="comment-16036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

