+++
type = "question"
title = "how to extract server secret key from wireshark capture from server to client"
description = '''how to identify and extract server secret key from wireshark capture'''
date = "2013-08-07T00:24:00Z"
lastmod = "2013-08-07T06:55:00Z"
weight = 23602
keywords = [ "serversecretkey" ]
aliases = [ "/questions/23602" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to extract server secret key from wireshark capture from server to client](/questions/23602/how-to-extract-server-secret-key-from-wireshark-capture-from-server-to-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23602-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23602-score" class="post-score" title="current number of votes">0</div><span id="post-23602-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to identify and extract server secret key from wireshark capture</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-serversecretkey" rel="tag" title="see questions tagged &#39;serversecretkey&#39;">serversecretkey</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '13, 00:24</strong></p><img src="https://secure.gravatar.com/avatar/4615adddd4e3042c96531c0d34a18d55?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sara&#39;s gravatar image" /><p><span>Sara</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sara has no accepted answers">0%</span></p></div></div><div id="comments-container-23602" class="comments-container"></div><div id="comment-tools-23602" class="comment-tools"></div><div class="clear"></div><div id="comment-23602-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23603"></span>

<div id="answer-container-23603" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23603-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23603-score" class="post-score" title="current number of votes">2</div><span id="post-23603-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You are talking about SSL/TLS handshake? If this is the case, the answer is : You can't because it doesn't flow. Otherwise you would be able to decrypt the data and to avoid this is one of the major reasons why to use SSL/TLS.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Aug '13, 00:58</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div></div><div id="comments-container-23603" class="comments-container"><span id="23611"></span><div id="comment-23611" class="comment"><div id="post-23611-score" class="comment-score"></div><div class="comment-text"><p>[I converted your Comment to an Answer because, well, I think it answers the question.]</p></div><div id="comment-23611-info" class="comment-info"><span class="comment-age">(07 Aug '13, 06:55)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-23603" class="comment-tools"></div><div class="clear"></div><div id="comment-23603-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

