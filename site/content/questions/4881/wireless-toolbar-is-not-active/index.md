+++
type = "question"
title = "Wireless toolbar is not active"
description = '''We are facing the following problems :  1.Wireless Toolbar is not active. 2.WLAN traffic statistics are not being updated. Do we need to buy some extra software to get these things working?'''
date = "2011-07-01T06:06:00Z"
lastmod = "2011-07-01T12:03:00Z"
weight = 4881
keywords = [ "wireless", "toolbar" ]
aliases = [ "/questions/4881" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless toolbar is not active](/questions/4881/wireless-toolbar-is-not-active)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4881-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4881-score" class="post-score" title="current number of votes">0</div><span id="post-4881-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are facing the following problems :</p><p>1.Wireless Toolbar is not active.</p><p>2.WLAN traffic statistics are not being updated.</p><p>Do we need to buy some extra software to get these things working?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-toolbar" rel="tag" title="see questions tagged &#39;toolbar&#39;">toolbar</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jul '11, 06:06</strong></p><img src="https://secure.gravatar.com/avatar/fc009873f703218f0619d60e874f7ed1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="repaka%20avinash&#39;s gravatar image" /><p><span>repaka avinash</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="repaka avinash has no accepted answers">0%</span></p></div></div><div id="comments-container-4881" class="comments-container"></div><div id="comment-tools-4881" class="comment-tools"></div><div class="clear"></div><div id="comment-4881-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4882"></span>

<div id="answer-container-4882" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4882-score" class="post-score" title="current number of votes">1</div><span id="post-4882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What OS are you running on? If it's Windows, then you would likely need to purchase an <a href="http://www.riverbed.com/us/products/cascade/airpcap.php">AirPcap</a> adapter from Riverbed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jul '11, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-4882" class="comments-container"><span id="4889"></span><div id="comment-4889" class="comment"><div id="post-4889-score" class="comment-score"></div><div class="comment-text"><p>Replace "would likely need to" with "would need to".</p><p>It's currently supported <em>only</em> with AirPcap, and that's not supported on UN*X, so it's Windows-with-AirPcap-only.</p></div><div id="comment-4889-info" class="comment-info"><span class="comment-age">(01 Jul '11, 12:03)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4882" class="comment-tools"></div><div class="clear"></div><div id="comment-4882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

