+++
type = "question"
title = "changing dictionary file location for wireshark!"
description = '''hello all, i&#x27;m using wireshark library for dissection of the network packets, so the question is how to change the default location of the all the dictionary files and preference files for the wireshark.(that it uses for the specific protocol, i.e. diameter, radius) as i dont want to install wiresha...'''
date = "2012-07-30T22:43:00Z"
lastmod = "2012-07-30T22:43:00Z"
weight = 13148
keywords = [ "libwireshark", "dissection", "dictionary", "wireshark" ]
aliases = [ "/questions/13148" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [changing dictionary file location for wireshark!](/questions/13148/changing-dictionary-file-location-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13148-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13148-score" class="post-score" title="current number of votes">0</div><span id="post-13148-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello all,</p><p>i'm using wireshark library for dissection of the network packets, so the question is how to change the default location of the all the dictionary files and preference files for the wireshark.(that it uses for the specific protocol, i.e. diameter, radius)</p><p>as i dont want to install wireshark, just using the library and dictionary , preference file,</p><p>thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libwireshark" rel="tag" title="see questions tagged &#39;libwireshark&#39;">libwireshark</span> <span class="post-tag tag-link-dissection" rel="tag" title="see questions tagged &#39;dissection&#39;">dissection</span> <span class="post-tag tag-link-dictionary" rel="tag" title="see questions tagged &#39;dictionary&#39;">dictionary</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '12, 22:43</strong></p><img src="https://secure.gravatar.com/avatar/425d250364423a7595a3eb9dea779cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sanny_D&#39;s gravatar image" /><p><span>Sanny_D</span><br />
<span class="score" title="0 reputation points">0</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sanny_D has 3 accepted answers">50%</span></p></div></div><div id="comments-container-13148" class="comments-container"></div><div id="comment-tools-13148" class="comment-tools"></div><div class="clear"></div><div id="comment-13148-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

