+++
type = "question"
title = "No interfaces in windows 7"
description = '''I have a %$£^ windows 7 64-bit, Wireshark and I can&#x27;t see my interfaces just &quot;Microsoft&quot;. any idea how do I solve this.'''
date = "2011-11-17T08:20:00Z"
lastmod = "2013-10-11T08:20:00Z"
weight = 7485
keywords = [ "nointerface", "windows7" ]
aliases = [ "/questions/7485" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No interfaces in windows 7](/questions/7485/no-interfaces-in-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7485-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7485-score" class="post-score" title="current number of votes">0</div><span id="post-7485-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a %$£^ windows 7 64-bit, Wireshark and I can't see my interfaces just "Microsoft". any idea how do I solve this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nointerface" rel="tag" title="see questions tagged &#39;nointerface&#39;">nointerface</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '11, 08:20</strong></p><img src="https://secure.gravatar.com/avatar/1ebfffebcacc1ccfc7c5b4333933e014?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sorino737&#39;s gravatar image" /><p><span>sorino737</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sorino737 has no accepted answers">0%</span></p></div></div><div id="comments-container-7485" class="comments-container"><span id="25917"></span><div id="comment-25917" class="comment"><div id="post-25917-score" class="comment-score"></div><div class="comment-text"><p>hey, have you find the solution</p></div><div id="comment-25917-info" class="comment-info"><span class="comment-age">(11 Oct '13, 08:20)</span> <span class="comment-user userinfo">sekar</span></div></div></div><div id="comment-tools-7485" class="comment-tools"></div><div class="clear"></div><div id="comment-7485-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7486"></span>

<div id="answer-container-7486" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7486-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7486-score" class="post-score" title="current number of votes">0</div><span id="post-7486-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Run your wireshark in administrator mode,this time you will see network card in wireshark.</p><p>Start &gt; All Programs &gt; Wireshark (Right click on wireshark and click "Run as administrator")</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Nov '11, 08:44</strong></p><img src="https://secure.gravatar.com/avatar/01febacc45af8ecf743c4f575d428326?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JK7&#39;s gravatar image" /><p><span>JK7</span><br />
<span class="score" title="31 reputation points">31</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JK7 has no accepted answers">0%</span></p></div></div><div id="comments-container-7486" class="comments-container"><span id="7489"></span><div id="comment-7489" class="comment"><div id="post-7489-score" class="comment-score"></div><div class="comment-text"><p>no... dont work. cheers</p></div><div id="comment-7489-info" class="comment-info"><span class="comment-age">(17 Nov '11, 09:04)</span> <span class="comment-user userinfo">sorino737</span></div></div></div><div id="comment-tools-7486" class="comment-tools"></div><div class="clear"></div><div id="comment-7486-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

