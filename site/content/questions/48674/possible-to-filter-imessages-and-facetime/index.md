+++
type = "question"
title = "Possible to filter imessages and facetime?"
description = '''Hi, Is it possible to filter the results to show only iMessages and Facetime over my wifi? I understand they may be encrypted but I&#x27;d like to see some level of detail like the recipient of the message. Is this possible? Thanks!'''
date = "2015-12-22T12:09:00Z"
lastmod = "2015-12-23T06:20:00Z"
weight = 48674
keywords = [ "facetime", "imessage" ]
aliases = [ "/questions/48674" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Possible to filter imessages and facetime?](/questions/48674/possible-to-filter-imessages-and-facetime)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48674-score" class="post-score" title="current number of votes">0</div><span id="post-48674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is it possible to filter the results to show only iMessages and Facetime over my wifi? I understand they may be encrypted but I'd like to see some level of detail like the recipient of the message. Is this possible?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-facetime" rel="tag" title="see questions tagged &#39;facetime&#39;">facetime</span> <span class="post-tag tag-link-imessage" rel="tag" title="see questions tagged &#39;imessage&#39;">imessage</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Dec '15, 12:09</strong></p><img src="https://secure.gravatar.com/avatar/d1911fb89a596ea54d1417f5e4ec58c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lfgto&#39;s gravatar image" /><p><span>lfgto</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lfgto has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Dec '15, 12:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-48674" class="comments-container"></div><div id="comment-tools-48674" class="comment-tools"></div><div class="clear"></div><div id="comment-48674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48686"></span>

<div id="answer-container-48686" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48686-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48686-score" class="post-score" title="current number of votes">0</div><span id="post-48686-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Apple's <a href="https://support.apple.com/en-us/HT202078">recommendations for which TCP and UDP ports to open</a> should give a good clue.</p><p><a href="https://www.theiphonewiki.com/wiki/FaceTime">This article</a> has some more details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Dec '15, 06:20</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-48686" class="comments-container"></div><div id="comment-tools-48686" class="comment-tools"></div><div class="clear"></div><div id="comment-48686-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

