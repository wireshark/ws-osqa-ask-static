+++
type = "question"
title = "is wire shark capable of seeing others peoples traffic"
description = '''can a user of wireshark use it to view unsuspecting people on certain social media websites and read their private messages or view other programs that the other person has on their system??'''
date = "2016-03-10T10:43:00Z"
lastmod = "2016-03-10T10:51:00Z"
weight = 50804
keywords = [ "capture", "remote" ]
aliases = [ "/questions/50804" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [is wire shark capable of seeing others peoples traffic](/questions/50804/is-wire-shark-capable-of-seeing-others-peoples-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50804-score" class="post-score" title="current number of votes">0</div><span id="post-50804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can a user of wireshark use it to view unsuspecting people on certain social media websites and read their private messages or view other programs that the other person has on their system??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '16, 10:43</strong></p><img src="https://secure.gravatar.com/avatar/7a684b6823002b59a8acf20348100868?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jax72&#39;s gravatar image" /><p><span>jax72</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jax72 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Mar '16, 10:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50804" class="comments-container"></div><div id="comment-tools-50804" class="comment-tools"></div><div class="clear"></div><div id="comment-50804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50805"></span>

<div id="answer-container-50805" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50805-score" class="post-score" title="current number of votes">1</div><span id="post-50805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not directly, Wireshark is used to make and view captures of network traffic, not to spy on other people.</p><p>If someone could remotely install a network capture program on the other persons system and then retrieve those captures, they could be examined with Wireshark. The captured traffic may reveal what programs are used and may, if the traffic is unencrypted, reveal the contents of private messages.</p><p>However, if sufficient access to the remote system is gained to plant a network traffic capture trojan, then presumably a remote access trojan could also be planted which would give full view of what is happening on the remote system.</p><p>Note that in most jurisdictions, all the above remote access is considered illegal and if caught doing so, may result in a custodial sentence for the perpetrator.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '16, 10:51</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50805" class="comments-container"></div><div id="comment-tools-50805" class="comment-tools"></div><div class="clear"></div><div id="comment-50805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

