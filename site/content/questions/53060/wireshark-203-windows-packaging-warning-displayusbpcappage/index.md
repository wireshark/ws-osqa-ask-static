+++
type = "question"
title = "Wireshark 2.0.3 Windows Packaging Warning &quot;DisplayUSBPCAPPage&quot;"
description = '''Hi I got the below packaging Warning on Window. Please let me know,what might be the issue or it can be ignored. Warning: 1 warning: install function &quot;DisplayUSBPcapPage&quot; not referenced - zeroing code (586-599) out  Thanks Dinesh Sadu'''
date = "2016-05-30T22:55:00Z"
lastmod = "2016-06-02T07:09:00Z"
weight = 53060
keywords = [ "packaging", "windows", "executable", "nsis" ]
aliases = [ "/questions/53060" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 2.0.3 Windows Packaging Warning "DisplayUSBPCAPPage"](/questions/53060/wireshark-203-windows-packaging-warning-displayusbpcappage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53060-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53060-score" class="post-score" title="current number of votes">0</div><span id="post-53060-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I got the below packaging Warning on Window.</p><p>Please let me know,what might be the issue or it can be ignored.</p><h2 id="warning">Warning:</h2><p>1 warning:</p><p>install function "DisplayUSBPcapPage" not referenced - zeroing code (586-599) out</p><p><img src="https://osqa-ask.wireshark.org/upfiles/USBCapture.JPG" alt="alt text" /></p><p>Thanks Dinesh Sadu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packaging" rel="tag" title="see questions tagged &#39;packaging&#39;">packaging</span> <span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-executable" rel="tag" title="see questions tagged &#39;executable&#39;">executable</span> <span class="post-tag tag-link-nsis" rel="tag" title="see questions tagged &#39;nsis&#39;">nsis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 May '16, 22:55</strong></p><img src="https://secure.gravatar.com/avatar/04334c27cb629065a13d61a61b611038?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20Babu%20Sadu&#39;s gravatar image" /><p><span>Dinesh Babu ...</span><br />
<span class="score" title="16 reputation points">16</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh Babu Sadu has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Jun '16, 06:41</strong> </span></p></div></div><div id="comments-container-53060" class="comments-container"></div><div id="comment-tools-53060" class="comment-tools"></div><div class="clear"></div><div id="comment-53060-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53142"></span>

<div id="answer-container-53142" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53142-score" class="post-score" title="current number of votes">0</div><span id="post-53142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For some reason in your build, nsis seems to be unhappy about USBPcap. If you're not bothered about USBPcap then it can probably be ignored.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '16, 07:09</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-53142" class="comments-container"></div><div id="comment-tools-53142" class="comment-tools"></div><div class="clear"></div><div id="comment-53142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

