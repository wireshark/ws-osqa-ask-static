+++
type = "question"
title = "Can not view Remote interface adapter list after adding successfully adding ."
description = '''Hello Everyone, Greetings of the day, I am trying to capture remote network interface card&#x27;s packets from wireshark in LAN i found how to add remote interface card in interface list as i have done all things step by step and got success to add the interface but can not find the added interface card ...'''
date = "2014-07-10T02:46:00Z"
lastmod = "2014-07-10T02:46:00Z"
weight = 34549
keywords = [ "capture", "remote" ]
aliases = [ "/questions/34549" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can not view Remote interface adapter list after adding successfully adding .](/questions/34549/can-not-view-remote-interface-adapter-list-after-adding-successfully-adding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34549-score" class="post-score" title="current number of votes">0</div><span id="post-34549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Everyone,</p><p>Greetings of the day,</p><p>I am trying to capture remote network interface card's packets from wireshark in LAN i found how to add remote interface card in interface list as i have done all things step by step and got success to add the interface but can not find the added interface card in interface list have anyone idea regarding that ?</p><p>any help will be appreciated.......</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '14, 02:46</strong></p><img src="https://secure.gravatar.com/avatar/5c0438a9e0a8ace741c1879a0f703f11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="drp19872&#39;s gravatar image" /><p><span>drp19872</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="drp19872 has no accepted answers">0%</span></p></div></div><div id="comments-container-34549" class="comments-container"></div><div id="comment-tools-34549" class="comment-tools"></div><div class="clear"></div><div id="comment-34549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

