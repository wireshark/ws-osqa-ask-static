+++
type = "question"
title = "UnInistall problems"
description = '''Hi Lately, it seems that every day wireshark uninstalls itself running windows 7 pro. Add / Remove programs shows it is no longer installed. I&#x27;ve tried versions 1.6.x and 1.8.x and both do the same thing. It had been working for a while but now everdayit goes away. I run GFI vipre antimalware and it...'''
date = "2012-10-15T23:24:00Z"
lastmod = "2012-10-16T03:20:00Z"
weight = 15027
keywords = [ "problems", "uninstall" ]
aliases = [ "/questions/15027" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [UnInistall problems](/questions/15027/uninistall-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15027-score" class="post-score" title="current number of votes">0</div><span id="post-15027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Lately, it seems that every day wireshark uninstalls itself running windows 7 pro. Add / Remove programs shows it is no longer installed. I've tried versions 1.6.x and 1.8.x and both do the same thing. It had been working for a while but now everdayit goes away. I run GFI vipre antimalware and it has never detected a virus. The program is great and it helped me pass CompTIA's Network+ Exam. Ever heard of this? It reinstalls with no problems.<br />
</p><p>My name is Marty Chapman and my email address is: <span class="__cf_email__" data-cfemail="107d7378716050">[email protected]</span><a href="http://snet.net">snet.net</a> Your help would be much appreciated Thank you Marty</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-problems" rel="tag" title="see questions tagged &#39;problems&#39;">problems</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '12, 23:24</strong></p><img src="https://secure.gravatar.com/avatar/5dc596eb3185a8a021b99d435817f11d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MartyC58&#39;s gravatar image" /><p><span>MartyC58</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MartyC58 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-15027" class="comments-container"></div><div id="comment-tools-15027" class="comment-tools"></div><div class="clear"></div><div id="comment-15027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15032"></span>

<div id="answer-container-15032" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15032-score" class="post-score" title="current number of votes">0</div><span id="post-15032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>it seems that every day wireshark uninstalls itself</p></blockquote><p>Wireshark does not do that.</p><p>Something else on your system might uninstall Wireshark. Could be some malware or a malware scanner (GFI).</p><blockquote><p>I run GFI vipre antimalware and it has never detected a virus.</p></blockquote><p>Well, that does not necessarily mean, that there is no malware ;-) I suggest to check your system with a bootable CDROM of another vendor (Kaspersky, Avast, F-Secure, etc.). Search google for: virus scanner boot cd</p><p>If no malware is detected, maybe your GFI malware scanner deletes Wireshark, because it thinks it's a "hacker tool". Disable VIPRE, disconnect the PC from the Internet and let it run for 1-2 days. Check if Wireshark still gets uninstalled.</p><p>If it still get's uninstalled, some other "security" software might cause the trouble.<br />
If it is not uninstalled, add an exception to VIPRE (ask GFI support how to do that).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Oct '12, 03:20</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-15032" class="comments-container"></div><div id="comment-tools-15032" class="comment-tools"></div><div class="clear"></div><div id="comment-15032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

