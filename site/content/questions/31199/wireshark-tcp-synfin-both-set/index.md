+++
type = "question"
title = "[closed] Wireshark TCP SYN/FIN both set"
description = '''I have downloaded a wireshark profile that has a coloring rule for tcp.flags &amp;amp; 0x02 || tcp.flags.fin == 1. I&#x27;m trying to figure out what the TCP SYN (synchronize sequence numbers) and FIN (sender is finished sending data) means or how it happens. Here&#x27;s my trace. I&#x27;m referring to frame 22 and 25...'''
date = "2014-03-26T10:55:00Z"
lastmod = "2014-03-26T13:51:00Z"
weight = 31199
keywords = [ "syn+fin", "tcp" ]
aliases = [ "/questions/31199" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Wireshark TCP SYN/FIN both set](/questions/31199/wireshark-tcp-synfin-both-set)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31199-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31199-score" class="post-score" title="current number of votes">0</div><span id="post-31199-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have downloaded a wireshark profile that has a coloring rule for tcp.flags &amp; 0x02 || tcp.flags.fin == 1.</p><p>I'm trying to figure out what the TCP SYN (synchronize sequence numbers) and FIN (sender is finished sending data) means or how it happens.</p><p>Here's my trace.</p><p>I'm referring to frame 22 and 25.</p><p>If you can point me in the right direction for TCP documentation.</p><p>I looked at TCP Illustrated and couldn't find anything.</p><p>Or if you can answer in what case this would happen.</p><p><a href="https://www.cloudshark.org/captures/696708657395">https://www.cloudshark.org/captures/696708657395</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-syn+fin" rel="tag" title="see questions tagged &#39;syn+fin&#39;">syn+fin</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '14, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/a472d068843eefd8a4ef69c4f94e4160?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gipper&#39;s gravatar image" /><p><span>gipper</span><br />
<span class="score" title="30 reputation points">30</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gipper has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>26 Mar '14, 14:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-31199" class="comments-container"><span id="31202"></span><div id="comment-31202" class="comment"><div id="post-31202-score" class="comment-score"></div><div class="comment-text"><p>Disregard this I made a mistake on a coloring rule.</p></div><div id="comment-31202-info" class="comment-info"><span class="comment-age">(26 Mar '14, 13:51)</span> <span class="comment-user userinfo">gipper</span></div></div></div><div id="comment-tools-31199" class="comment-tools"></div><div class="clear"></div><div id="comment-31199-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Problem is not reproducible or outdated" by grahamb 26 Mar '14, 14:35

</div>

</div>

</div>

