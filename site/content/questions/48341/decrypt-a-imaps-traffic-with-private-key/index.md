+++
type = "question"
title = "Decrypt a IMAPs traffic with private key"
description = '''I saw this IMAP pcapng file with SSL. The private key is in the comment. Wonder if it&#x27;s possible to decrypt the traffic.'''
date = "2015-12-07T20:22:00Z"
lastmod = "2015-12-07T20:22:00Z"
weight = 48341
keywords = [ "wireshark" ]
aliases = [ "/questions/48341" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt a IMAPs traffic with private key](/questions/48341/decrypt-a-imaps-traffic-with-private-key)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48341-score" class="post-score" title="current number of votes">0</div><span id="post-48341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I saw this IMAP <a href="https://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=get&amp;target=imap-ssl.pcapng">pcapng file</a> with SSL. The private key is in the comment. Wonder if it's possible to decrypt the traffic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '15, 20:22</strong></p><img src="https://secure.gravatar.com/avatar/7bb7310612573625abd07a67f22724ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pktUser1001&#39;s gravatar image" /><p><span>pktUser1001</span><br />
<span class="score" title="201 reputation points">201</span><span title="49 badges"><span class="badge1">●</span><span class="badgecount">49</span></span><span title="50 badges"><span class="silver">●</span><span class="badgecount">50</span></span><span title="54 badges"><span class="bronze">●</span><span class="badgecount">54</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pktUser1001 has one accepted answer">12%</span></p></div></div><div id="comments-container-48341" class="comments-container"></div><div id="comment-tools-48341" class="comment-tools"></div><div class="clear"></div><div id="comment-48341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

