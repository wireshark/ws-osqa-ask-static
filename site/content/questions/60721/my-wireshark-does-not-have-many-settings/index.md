+++
type = "question"
title = "my wireshark does not have many settings"
description = '''Hi my wireshark has not as many settings as them in the videos?'''
date = "2017-04-10T16:11:00Z"
lastmod = "2017-04-11T13:59:00Z"
weight = 60721
keywords = [ "802.11" ]
aliases = [ "/questions/60721" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [my wireshark does not have many settings](/questions/60721/my-wireshark-does-not-have-many-settings)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60721-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60721-score" class="post-score" title="current number of votes">-4</div><span id="post-60721-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi my wireshark has not as many settings as them in the videos?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '17, 16:11</strong></p><img src="https://secure.gravatar.com/avatar/5c3b82e28d650a35a94860be06e6653f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Emilknilsson&#39;s gravatar image" /><p><span>Emilknilsson</span><br />
<span class="score" title="2 reputation points">2</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Emilknilsson has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Apr '17, 06:07</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-60721" class="comments-container"><span id="60728"></span><div id="comment-60728" class="comment"><div id="post-60728-score" class="comment-score"></div><div class="comment-text"><p>To be able to help you a little more information would be helpful?</p><p>Which settings are you missing exactly? What videos you're referring? When 802.11 settings are missing =&gt; Do you mean AirPcap support?</p><p>Please be aware that Wireshark changed the GUI from GTK to QT. So maybe your videos are using the "legacy" UI.</p></div><div id="comment-60728-info" class="comment-info"><span class="comment-age">(11 Apr '17, 02:05)</span> <span class="comment-user userinfo">Uli</span></div></div><span id="60752"></span><div id="comment-60752" class="comment"><div id="post-60752-score" class="comment-score"></div><div class="comment-text"><p>Hi just noticed that it many settings that wasnt in my whireshark, But I will try Legacy.</p></div><div id="comment-60752-info" class="comment-info"><span class="comment-age">(11 Apr '17, 13:59)</span> <span class="comment-user userinfo">Emilknilsson</span></div></div></div><div id="comment-tools-60721" class="comment-tools"></div><div class="clear"></div><div id="comment-60721-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

