+++
type = "question"
title = "Where is idl2wrs?"
description = '''This may be a silly question, but I&#x27;ve looked all over for the idl2wrs script and I can&#x27;t find it anywhere in the source tree. I&#x27;ve found the .idl files (in the idl directory), but I can&#x27;t seem to find the script itself. I&#x27;m looking in Wireshark 1.12'''
date = "2014-08-07T06:24:00Z"
lastmod = "2014-08-07T06:46:00Z"
weight = 35299
keywords = [ "idl2wrs" ]
aliases = [ "/questions/35299" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Where is idl2wrs?](/questions/35299/where-is-idl2wrs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35299-score" class="post-score" title="current number of votes">0</div><span id="post-35299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This may be a silly question, but I've looked all over for the <code>idl2wrs</code> script and I can't find it anywhere in the source tree. I've found the <code>.idl</code> files (in the <code>idl</code> directory), but I can't seem to find the script itself.</p><p>I'm looking in Wireshark 1.12</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-idl2wrs" rel="tag" title="see questions tagged &#39;idl2wrs&#39;">idl2wrs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '14, 06:24</strong></p><img src="https://secure.gravatar.com/avatar/2973b6be28bed95434b4ee70047a5735?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="burwell&#39;s gravatar image" /><p><span>burwell</span><br />
<span class="score" title="26 reputation points">26</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="burwell has one accepted answer">100%</span></p></div></div><div id="comments-container-35299" class="comments-container"></div><div id="comment-tools-35299" class="comment-tools"></div><div class="clear"></div><div id="comment-35299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35301"></span>

<div id="answer-container-35301" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35301-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35301-score" class="post-score" title="current number of votes">1</div><span id="post-35301-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="burwell has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>idl2wrs for CORBA based protocols are in the tools directory. for DCE-RPC it's in epan\dissectors\dcerpc</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Aug '14, 06:46</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-35301" class="comments-container"></div><div id="comment-tools-35301" class="comment-tools"></div><div class="clear"></div><div id="comment-35301-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

