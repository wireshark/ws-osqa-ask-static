+++
type = "question"
title = "Can NOT capture packets sent form local interfaces but can capture packets received"
description = '''I&#x27;m using WireShark 2.0.2 x64 version on Windows 10 x64 10.0.10240. I can NOT capture any packets sent from local interface on my computer whether LAN interface or WLAN, but I can capture packets arriving at my computer. It means that it can only do one-way capture. Can anyone help to solve this iss...'''
date = "2016-04-14T20:49:00Z"
lastmod = "2016-04-15T00:50:00Z"
weight = 51691
keywords = [ "oneway121", "2.0.2", "windows10" ]
aliases = [ "/questions/51691" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Can NOT capture packets sent form local interfaces but can capture packets received](/questions/51691/can-not-capture-packets-sent-form-local-interfaces-but-can-capture-packets-received)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51691-score" class="post-score" title="current number of votes">0</div><span id="post-51691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using WireShark 2.0.2 x64 version on Windows 10 x64 10.0.10240. I can NOT capture any packets sent from local interface on my computer whether LAN interface or WLAN, but I can capture packets arriving at my computer. It means that it can only do one-way capture. Can anyone help to solve this issue? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-oneway121" rel="tag" title="see questions tagged &#39;oneway121&#39;">oneway121</span> <span class="post-tag tag-link-2.0.2" rel="tag" title="see questions tagged &#39;2.0.2&#39;">2.0.2</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Apr '16, 20:49</strong></p><img src="https://secure.gravatar.com/avatar/0597b9859ccaf8a92b9ea0404d817708?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="VFi&#39;s gravatar image" /><p><span>VFi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="VFi has no accepted answers">0%</span></p></div></div><div id="comments-container-51691" class="comments-container"></div><div id="comment-tools-51691" class="comment-tools"></div><div class="clear"></div><div id="comment-51691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="51692"></span>

<div id="answer-container-51692" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51692-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51692-score" class="post-score" title="current number of votes">0</div><span id="post-51692-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you running any VPN network adapter or AntiVirus software on your client? This can interfere with WinPCAP.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Apr '16, 22:37</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-51692" class="comments-container"></div><div id="comment-tools-51692" class="comment-tools"></div><div class="clear"></div><div id="comment-51692-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="51696"></span>

<div id="answer-container-51696" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51696-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51696-score" class="post-score" title="current number of votes">0</div><span id="post-51696-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please try Npcap:</p><p><a href="https://github.com/nmap/npcap/">https://github.com/nmap/npcap/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '16, 00:50</strong></p><img src="https://secure.gravatar.com/avatar/0f8ec58f46e4af3a67f768675c20aac8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yang%20Luo&#39;s gravatar image" /><p><span>Yang Luo</span><br />
<span class="score" title="91 reputation points">91</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yang Luo has one accepted answer">4%</span></p></div></div><div id="comments-container-51696" class="comments-container"></div><div id="comment-tools-51696" class="comment-tools"></div><div class="clear"></div><div id="comment-51696-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

