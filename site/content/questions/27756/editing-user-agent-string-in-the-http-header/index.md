+++
type = "question"
title = "Editing User-Agent string in the HTTP header"
description = '''Hi All, How does one edit the User-Agent string in the HTTP header from data captured by Wireshark. Is it possible? Thanks :)'''
date = "2013-12-04T03:51:00Z"
lastmod = "2013-12-04T03:51:00Z"
weight = 27756
keywords = [ "any" ]
aliases = [ "/questions/27756" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Editing User-Agent string in the HTTP header](/questions/27756/editing-user-agent-string-in-the-http-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27756-score" class="post-score" title="current number of votes">0</div><span id="post-27756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>How does one edit the User-Agent string in the HTTP header from data captured by Wireshark. Is it possible?</p><p>Thanks :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-any" rel="tag" title="see questions tagged &#39;any&#39;">any</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '13, 03:51</strong></p><img src="https://secure.gravatar.com/avatar/906e92e37a0ad6d578d4e95ded70fdfc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jhonny&#39;s gravatar image" /><p><span>Jhonny</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jhonny has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Dec '13, 04:24</strong> </span></p></div></div><div id="comments-container-27756" class="comments-container"></div><div id="comment-tools-27756" class="comment-tools"></div><div class="clear"></div><div id="comment-27756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

