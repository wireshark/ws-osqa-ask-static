+++
type = "question"
title = "how to tell which &quot;interface&quot; is associated with which computer?"
description = '''how to tell which &quot;interface&quot; is associated with which computer?'''
date = "2012-10-31T18:12:00Z"
lastmod = "2012-11-01T02:28:00Z"
weight = 15437
keywords = [ "interface" ]
aliases = [ "/questions/15437" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to tell which "interface" is associated with which computer?](/questions/15437/how-to-tell-which-interface-is-associated-with-which-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15437-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15437-score" class="post-score" title="current number of votes">0</div><span id="post-15437-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to tell which "interface" is associated with which computer?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '12, 18:12</strong></p><img src="https://secure.gravatar.com/avatar/b2a4006b4a0252f8be292c57acde97ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiresharkhelpers&#39;s gravatar image" /><p><span>wiresharkhel...</span><br />
<span class="score" title="30 reputation points">30</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiresharkhelpers has no accepted answers">0%</span></p></div></div><div id="comments-container-15437" class="comments-container"><span id="15449"></span><div id="comment-15449" class="comment"><div id="post-15449-score" class="comment-score"></div><div class="comment-text"><p>what do you mean by an "interface"?</p></div><div id="comment-15449-info" class="comment-info"><span class="comment-age">(01 Nov '12, 02:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15437" class="comment-tools"></div><div class="clear"></div><div id="comment-15437-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

