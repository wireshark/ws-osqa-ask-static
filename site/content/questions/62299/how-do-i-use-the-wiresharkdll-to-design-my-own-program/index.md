+++
type = "question"
title = "How do I use the wireshark.dll to design my own program?"
description = '''I am a beginner, How do I use the libwireshark.dll to design my own program? i want use libwireshark.dll to Analytic protocol，and get the protocol tree in my own project? thanks !!!'''
date = "2017-06-26T05:50:00Z"
lastmod = "2017-06-26T05:50:00Z"
weight = 62299
keywords = [ "libwireshark.dll" ]
aliases = [ "/questions/62299" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I use the wireshark.dll to design my own program?](/questions/62299/how-do-i-use-the-wiresharkdll-to-design-my-own-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62299-score" class="post-score" title="current number of votes">0</div><span id="post-62299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a beginner, How do I use the libwireshark.dll to design my own program? i want use libwireshark.dll to Analytic protocol，and get the protocol tree in my own project? thanks !!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libwireshark.dll" rel="tag" title="see questions tagged &#39;libwireshark.dll&#39;">libwireshark.dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '17, 05:50</strong></p><img src="https://secure.gravatar.com/avatar/0a50e5c13a56205f426f06f369a4a58d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ywd&#39;s gravatar image" /><p><span>ywd</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ywd has no accepted answers">0%</span></p></div></div><div id="comments-container-62299" class="comments-container"></div><div id="comment-tools-62299" class="comment-tools"></div><div class="clear"></div><div id="comment-62299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

