+++
type = "question"
title = "After converting pcap to text , I want only data part in text file how can i do that?"
description = '''I have converted one pcap file with tshark command tshark -V -r filename.pcap it is giving whole dissection of each and every packets. But I want only data part which is after UDP or TCP protocol as mentioned in picture. How can i achieve this thing? '''
date = "2015-03-30T01:14:00Z"
lastmod = "2015-03-30T03:16:00Z"
weight = 40989
keywords = [ "pcap", "tshark", "wireshark" ]
aliases = [ "/questions/40989" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [After converting pcap to text , I want only data part in text file how can i do that?](/questions/40989/after-converting-pcap-to-text-i-want-only-data-part-in-text-file-how-can-i-do-that)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40989-score" class="post-score" title="current number of votes">0</div><span id="post-40989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have converted one pcap file with tshark command tshark -V -r filename.pcap it is giving whole dissection of each and every packets. But I want only data part which is after UDP or TCP protocol as mentioned in picture. How can i achieve this thing?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Untitled_Cy8s8Ej.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Mar '15, 01:14</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Mar '15, 01:15</strong> </span></p></div></div><div id="comments-container-40989" class="comments-container"></div><div id="comment-tools-40989" class="comment-tools"></div><div class="clear"></div><div id="comment-40989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40993"></span>

<div id="answer-container-40993" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40993-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40993-score" class="post-score" title="current number of votes">0</div><span id="post-40993-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Se my answer to the following questions:</p><blockquote><p><a href="https://ask.wireshark.org/questions/38998/automating-extraction-of-udp-payload-from-pcap-file">https://ask.wireshark.org/questions/38998/automating-extraction-of-udp-payload-from-pcap-file</a><br />
<a href="https://ask.wireshark.org/questions/35353/exporting-payload-data-in-binary-file">https://ask.wireshark.org/questions/35353/exporting-payload-data-in-binary-file</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '15, 03:16</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-40993" class="comments-container"></div><div id="comment-tools-40993" class="comment-tools"></div><div class="clear"></div><div id="comment-40993-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

