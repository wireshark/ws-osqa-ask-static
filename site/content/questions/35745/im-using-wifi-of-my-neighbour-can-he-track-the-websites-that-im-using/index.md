+++
type = "question"
title = "I&#x27;m using wifi of my neighbour, can he track the websites that I&#x27;m using"
description = '''PLZ HELP ME '''
date = "2014-08-26T03:01:00Z"
lastmod = "2014-08-26T19:24:00Z"
weight = 35745
keywords = [ "security", "wifi" ]
aliases = [ "/questions/35745" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [I'm using wifi of my neighbour, can he track the websites that I'm using](/questions/35745/im-using-wifi-of-my-neighbour-can-he-track-the-websites-that-im-using)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35745-score" class="post-score" title="current number of votes">0</div><span id="post-35745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>PLZ HELP ME</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Aug '14, 03:01</strong></p><img src="https://secure.gravatar.com/avatar/b459715cd7fc8f9d994137cab3884abc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ajoshi&#39;s gravatar image" /><p><span>ajoshi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ajoshi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Aug '14, 03:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-35745" class="comments-container"><span id="35746"></span><div id="comment-35746" class="comment"><div id="post-35746-score" class="comment-score"></div><div class="comment-text"><p>is there any one to help me?</p></div><div id="comment-35746-info" class="comment-info"><span class="comment-age">(26 Aug '14, 03:02)</span> <span class="comment-user userinfo">ajoshi</span></div></div><span id="35781"></span><div id="comment-35781" class="comment"><div id="post-35781-score" class="comment-score"></div><div class="comment-text"><p>Not only can he see the websites that your visiting, he can also see all your cleartext username and passwords. If you're logging into any FTP or POP3 sites without encryption, the data is in the clear and can be seen with a tool like Wireshark.</p><p>The only exception is if you are using a VPN but based on your question, I'm guessing that isn't the case. You can fire up Wireshark and see what's passing along the wire too.</p><p>Cheers,</p><p>Raybo</p></div><div id="comment-35781-info" class="comment-info"><span class="comment-age">(26 Aug '14, 18:21)</span> <span class="comment-user userinfo">Raybo</span></div></div></div><div id="comment-tools-35745" class="comment-tools"></div><div class="clear"></div><div id="comment-35745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="35747"></span>

<div id="answer-container-35747" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35747-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35747-score" class="post-score" title="current number of votes">0</div><span id="post-35747-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, he can easily track what you are doing, by using Wireshark!</p><p>BTW: are you saying, you are using the WLAN without asking the neighbour?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Aug '14, 03:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-35747" class="comments-container"></div><div id="comment-tools-35747" class="comment-tools"></div><div class="clear"></div><div id="comment-35747-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="35784"></span>

<div id="answer-container-35784" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35784-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35784-score" class="post-score" title="current number of votes">0</div><span id="post-35784-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Kurt's answer is correct and I'll expand on the answer.</p><p>Not only can he see the websites that your visiting, he can also see all your cleartext username and passwords. If you're logging into any FTP or POP3 sites without encryption, the data is in the clear and can be seen with a tool like Wireshark.</p><p>The only exception is if you are using a VPN but based on your question, I'm guessing that isn't the case. You can fire up Wireshark and see what's passing along the wire too.</p><p>Cheers,</p><p>Raybo</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Aug '14, 19:24</strong></p><img src="https://secure.gravatar.com/avatar/aeab2e3ea069b4b198c2e94332abb7d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Raybo&#39;s gravatar image" /><p><span>Raybo</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Raybo has no accepted answers">0%</span></p></div></div><div id="comments-container-35784" class="comments-container"></div><div id="comment-tools-35784" class="comment-tools"></div><div class="clear"></div><div id="comment-35784-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

