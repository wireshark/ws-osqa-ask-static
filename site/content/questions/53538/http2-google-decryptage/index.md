+++
type = "question"
title = "HTTP/2 Google decryptage"
description = '''Hi, I try to use Wireshark to capture HTTP/2 package. I imported the logfile of the Chrome to Wireshark SSL setting. I can get decrypted message for the other HTTP/2 web sites, but not for www.google.com and my local Apache2 HTTP/2 server. Someone can tell me why? Thanks'''
date = "2016-06-17T09:27:00Z"
lastmod = "2016-06-21T01:41:00Z"
weight = 53538
keywords = [ "https" ]
aliases = [ "/questions/53538" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [HTTP/2 Google decryptage](/questions/53538/http2-google-decryptage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53538-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53538-score" class="post-score" title="current number of votes">0</div><span id="post-53538-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I try to use Wireshark to capture HTTP/2 package. I imported the logfile of the Chrome to Wireshark SSL setting. I can get decrypted message for the other HTTP/2 web sites, but not for www.google.com and my local Apache2 HTTP/2 server. Someone can tell me why? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '16, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/7ae0ed0ddd879be9deddd5abeb869c11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zhongwei%20HU&#39;s gravatar image" /><p><span>Zhongwei HU</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zhongwei HU has no accepted answers">0%</span></p></div></div><div id="comments-container-53538" class="comments-container"></div><div id="comment-tools-53538" class="comment-tools"></div><div class="clear"></div><div id="comment-53538-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53583"></span>

<div id="answer-container-53583" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53583-score" class="post-score" title="current number of votes">0</div><span id="post-53583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In fact, it works now with Wireshark in Linux, just pay attention to use the same terminal to start the browser and Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jun '16, 01:41</strong></p><img src="https://secure.gravatar.com/avatar/7ae0ed0ddd879be9deddd5abeb869c11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zhongwei%20HU&#39;s gravatar image" /><p><span>Zhongwei HU</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zhongwei HU has no accepted answers">0%</span></p></div></div><div id="comments-container-53583" class="comments-container"></div><div id="comment-tools-53583" class="comment-tools"></div><div class="clear"></div><div id="comment-53583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

