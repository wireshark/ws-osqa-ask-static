+++
type = "question"
title = "Statistics rate"
description = '''In the statistics Window what is the rate measured in?'''
date = "2014-08-07T06:48:00Z"
lastmod = "2014-08-07T06:56:00Z"
weight = 35302
keywords = [ "rate", "statistics" ]
aliases = [ "/questions/35302" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Statistics rate](/questions/35302/statistics-rate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35302-score" class="post-score" title="current number of votes">0</div><span id="post-35302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In the statistics Window what is the rate measured in?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rate" rel="tag" title="see questions tagged &#39;rate&#39;">rate</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '14, 06:48</strong></p><img src="https://secure.gravatar.com/avatar/137bd98644e20933830b92a1ea138090?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SST&#39;s gravatar image" /><p><span>SST</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SST has no accepted answers">0%</span></p></div></div><div id="comments-container-35302" class="comments-container"><span id="35303"></span><div id="comment-35303" class="comment"><div id="post-35303-score" class="comment-score"></div><div class="comment-text"><p>which one? There are tons of different statistics windows. Please specify the menu option you used.</p></div><div id="comment-35303-info" class="comment-info"><span class="comment-age">(07 Aug '14, 06:56)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-35302" class="comment-tools"></div><div class="clear"></div><div id="comment-35302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

