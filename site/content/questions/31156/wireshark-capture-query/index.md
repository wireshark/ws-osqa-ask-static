+++
type = "question"
title = "Wireshark capture query"
description = '''Hello,  I have some questions regarding packet capture report that I have generated using wireshark.  (Q1) How can I determine the different types of protocols captured and the different levels of communications using Conversations? (Q2) How can I determine from my wireshark capture (where do I look...'''
date = "2014-03-25T10:04:00Z"
lastmod = "2014-03-26T09:11:00Z"
weight = 31156
keywords = [ "homework", "wireshark" ]
aliases = [ "/questions/31156" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark capture query](/questions/31156/wireshark-capture-query)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31156-score" class="post-score" title="current number of votes">0</div><span id="post-31156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><pre><code> I have some questions regarding packet capture report that I have generated using wireshark.</code></pre><p>(Q1) How can I determine the different types of protocols captured and the different levels of communications using Conversations?</p><p>(Q2) How can I determine from my wireshark capture (where do I look) if there are any unexpected conversations?</p><p>(Q3) How can I determine from my wireshark capture which device my computer "talk" to the most?</p><p>(Q4) How can I determine from wireshark capture the most frequent type of packets captured?</p><p>(Q5) How can I determine from my wireshark capture if the traffic was constant?</p><p>Please help.</p><p>Irene</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '14, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/4ca7fd3a03d4e23794f6dbd416047569?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="koel26&#39;s gravatar image" /><p><span>koel26</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="koel26 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Mar '14, 11:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-31156" class="comments-container"><span id="31189"></span><div id="comment-31189" class="comment"><div id="post-31189-score" class="comment-score"></div><div class="comment-text"><p>some of your questions are 'tricky' to answer. By when do you have to submit your homework?</p></div><div id="comment-31189-info" class="comment-info"><span class="comment-age">(26 Mar '14, 09:11)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-31156" class="comment-tools"></div><div class="clear"></div><div id="comment-31156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

