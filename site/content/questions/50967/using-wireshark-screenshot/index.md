+++
type = "question"
title = "Using Wireshark screenshot"
description = '''Hello. We surveyed several tools function by our client request. Wireshark is one of them. We must report surveyed result to our client. Is it possible to use Wireshark screen shot for surveyed result report document? Or violate with license agreement(GPLv2)?'''
date = "2016-03-15T18:18:00Z"
lastmod = "2016-03-16T00:57:00Z"
weight = 50967
keywords = [ "screen", "shot" ]
aliases = [ "/questions/50967" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Using Wireshark screenshot](/questions/50967/using-wireshark-screenshot)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50967-score" class="post-score" title="current number of votes">0</div><span id="post-50967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.</p><p>We surveyed several tools function by our client request. Wireshark is one of them. We must report surveyed result to our client.</p><p>Is it possible to use Wireshark screen shot for surveyed result report document? Or violate with license agreement(GPLv2)?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-screen" rel="tag" title="see questions tagged &#39;screen&#39;">screen</span> <span class="post-tag tag-link-shot" rel="tag" title="see questions tagged &#39;shot&#39;">shot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '16, 18:18</strong></p><img src="https://secure.gravatar.com/avatar/193c1505fc5d5a53443d7498eaab9e56?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mitsunabe&#39;s gravatar image" /><p><span>mitsunabe</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mitsunabe has no accepted answers">0%</span></p></div></div><div id="comments-container-50967" class="comments-container"></div><div id="comment-tools-50967" class="comment-tools"></div><div class="clear"></div><div id="comment-50967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50968"></span>

<div id="answer-container-50968" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50968-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50968-score" class="post-score" title="current number of votes">1</div><span id="post-50968-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no problem in using the generated output (visual or otherwise) from a GPL'ed program(*), only the code itself is covered. So go ahead adding it into your report.</p><p>(*) That would be an very complicating issue with a compiler or web server for instance. Output could be protected otherwise though, for instance creating an animation with Blender is copyright protected.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '16, 00:57</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-50968" class="comments-container"></div><div id="comment-tools-50968" class="comment-tools"></div><div class="clear"></div><div id="comment-50968-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

