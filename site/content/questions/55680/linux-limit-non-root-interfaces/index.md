+++
type = "question"
title = "Linux: Limit non-root interfaces"
description = '''I would like non-root users to be able to capture packets on a specific list of interfaces. This will be used in a teaching environment and we only want the students to capture traffic on a private secondary network between the machines.  Does anyone have any suggestions on how to do this?'''
date = "2016-09-20T06:02:00Z"
lastmod = "2016-09-20T06:02:00Z"
weight = 55680
keywords = [ "linux" ]
aliases = [ "/questions/55680" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Linux: Limit non-root interfaces](/questions/55680/linux-limit-non-root-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55680-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55680-score" class="post-score" title="current number of votes">0</div><span id="post-55680-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like non-root users to be able to capture packets on a specific list of interfaces. This will be used in a teaching environment and we only want the students to capture traffic on a private secondary network between the machines.</p><p>Does anyone have any suggestions on how to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '16, 06:02</strong></p><img src="https://secure.gravatar.com/avatar/2562d1022e437e8175f8366ce621c2bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brownjl99&#39;s gravatar image" /><p><span>brownjl99</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brownjl99 has no accepted answers">0%</span></p></div></div><div id="comments-container-55680" class="comments-container"></div><div id="comment-tools-55680" class="comment-tools"></div><div class="clear"></div><div id="comment-55680-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

