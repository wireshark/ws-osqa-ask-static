+++
type = "question"
title = "Bandwidth calculation formula"
description = '''Hello, In the Summary of the capture I could see the Avg Bytes/sec which I think is the average bandwidth. I was curious to know how this was calculated. may be a formula would help'''
date = "2010-11-18T05:51:00Z"
lastmod = "2010-11-18T06:12:00Z"
weight = 1007
keywords = [ "bandwidth", "calculate" ]
aliases = [ "/questions/1007" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Bandwidth calculation formula](/questions/1007/bandwidth-calculation-formula)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1007-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1007-score" class="post-score" title="current number of votes">0</div><span id="post-1007-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>In the Summary of the capture I could see the Avg Bytes/sec which I think is the average bandwidth. I was curious to know how this was calculated. may be a formula would help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-calculate" rel="tag" title="see questions tagged &#39;calculate&#39;">calculate</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '10, 05:51</strong></p><img src="https://secure.gravatar.com/avatar/830a3a6650c4763744ee8f0cef0fc044?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adhikaa1&#39;s gravatar image" /><p><span>adhikaa1</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adhikaa1 has no accepted answers">0%</span></p></div></div><div id="comments-container-1007" class="comments-container"></div><div id="comment-tools-1007" class="comment-tools"></div><div class="clear"></div><div id="comment-1007-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1008"></span>

<div id="answer-container-1008" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1008-score" class="post-score" title="current number of votes">2</div><span id="post-1008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="adhikaa1 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Cumulative Bytes / Relative Time</p><p>which means</p><p>SUM(frame sizes) / seconds since beginning of capture</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '10, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Nov '10, 06:13</strong> </span></p></div></div><div id="comments-container-1008" class="comments-container"></div><div id="comment-tools-1008" class="comment-tools"></div><div class="clear"></div><div id="comment-1008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

