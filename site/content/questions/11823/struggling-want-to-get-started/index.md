+++
type = "question"
title = "Struggling, want to get started..."
description = '''I have Windows 7 64 bit with Linksys WUSB54GC and as I understands it it is not possible to make to work in promiscous mode? I&#x27;ve tried some netsh to enable it but still no luck. I would like to be able to capture packets not ment for me :-)  any magic and i can perform to make it happen?'''
date = "2012-06-11T12:23:00Z"
lastmod = "2012-06-11T12:32:00Z"
weight = 11823
keywords = [ "windows7", "wireshark" ]
aliases = [ "/questions/11823" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Struggling, want to get started...](/questions/11823/struggling-want-to-get-started)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11823-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11823-score" class="post-score" title="current number of votes">0</div><span id="post-11823-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Windows 7 64 bit with Linksys WUSB54GC and as I understands it it is not possible to make to work in promiscous mode?</p><p>I've tried some netsh to enable it but still no luck. I would like to be able to capture packets not ment for me :-)</p><p>any magic and i can perform to make it happen?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jun '12, 12:23</strong></p><img src="https://secure.gravatar.com/avatar/d065d434da0fc60e0efca8585ec1a7b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="_Jason_&#39;s gravatar image" /><p><span>_Jason_</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="_Jason_ has no accepted answers">0%</span></p></div></div><div id="comments-container-11823" class="comments-container"></div><div id="comment-tools-11823" class="comment-tools"></div><div class="clear"></div><div id="comment-11823-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11825"></span>

<div id="answer-container-11825" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11825-score" class="post-score" title="current number of votes">0</div><span id="post-11825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>it it is not possible to make to work in promiscous mode?</p></blockquote><p>Please read this: <code>http://wiki.wireshark.org/CaptureSetup/WLAN#Windows</code><br />
</p><blockquote><p>any magic and i can perform to make it happen?</p></blockquote><p>Magic? Sure. Install Linux or use a bootable Linux CDROM ;-))</p><p>See here: <code>http://ask.wireshark.org/questions/4529/sniffing-possible-only-after-disconnectreconnect-or-deauthenticating-a-client</code><br />
</p><p>Sounds like <a href="http://www.backtrack-linux.org/downloads/">Backtrack 5</a> does work with that WIFI adapter.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jun '12, 12:32</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-11825" class="comments-container"></div><div id="comment-tools-11825" class="comment-tools"></div><div class="clear"></div><div id="comment-11825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

