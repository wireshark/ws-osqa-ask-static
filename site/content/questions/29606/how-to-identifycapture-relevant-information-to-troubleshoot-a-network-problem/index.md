+++
type = "question"
title = "How to identify/capture relevant information to troubleshoot a network problem"
description = '''Hi,  this is not a technical question it is more a management querstion. Let&#x27;s say you work in an Active Directory enviroment, 50 Users, several Exchange Servers, several Domain Controllers, Printservers, Fileservers etc, the whole nine.... If somebody now has a problem lets say, &quot;my outlook client ...'''
date = "2014-02-10T02:46:00Z"
lastmod = "2014-02-10T02:46:00Z"
weight = 29606
keywords = [ "dissect", "management", "data", "anaylizing" ]
aliases = [ "/questions/29606" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to identify/capture relevant information to troubleshoot a network problem](/questions/29606/how-to-identifycapture-relevant-information-to-troubleshoot-a-network-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29606-score" class="post-score" title="current number of votes">0</div><span id="post-29606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, this is not a technical question it is more a management querstion.</p><p>Let's say you work in an Active Directory enviroment, 50 Users, several Exchange Servers, several Domain Controllers, Printservers, Fileservers etc, the whole nine....</p><p>If somebody now has a problem lets say, "my outlook client is sooo slow and freezes".</p><p>it could be anything, maybe the DNS resolves not fast enough which generates a timeout at your outlook, so looking at an exchange &lt;-&gt; Outlook thing would be a waste of time. or maybe the user has a mapped drive, the OS cant look it up fast enough to wasting time analyzing something at the client &lt;-&gt; fileserver would be solve anything:</p><p>that said, i want to ask you guys, how do you perform such tasks ? how to you start, which settings do you check first, which last, how do you dissect the usefull informations from the daily network "trashtalk".</p><p>i ask this querstion as open as possible intentionally. it's not about solving the problems above it's more a general question of how you manage the big ammount of data you can collect with wireshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissect" rel="tag" title="see questions tagged &#39;dissect&#39;">dissect</span> <span class="post-tag tag-link-management" rel="tag" title="see questions tagged &#39;management&#39;">management</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-anaylizing" rel="tag" title="see questions tagged &#39;anaylizing&#39;">anaylizing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '14, 02:46</strong></p><img src="https://secure.gravatar.com/avatar/e2c66bdf3ce94c5b2716cbf568768361?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="supreme&#39;s gravatar image" /><p><span>supreme</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="supreme has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Feb '14, 06:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-29606" class="comments-container"></div><div id="comment-tools-29606" class="comment-tools"></div><div class="clear"></div><div id="comment-29606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

