+++
type = "question"
title = "WinPCAP crashes on Windows 8.1"
description = '''Any word on WinPCAP 4.1.3 crashing Windows 8.1 systems?'''
date = "2014-01-09T13:45:00Z"
lastmod = "2014-01-09T13:45:00Z"
weight = 28746
keywords = [ "windows8.1", "winpcap", "crash" ]
aliases = [ "/questions/28746" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WinPCAP crashes on Windows 8.1](/questions/28746/winpcap-crashes-on-windows-81)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28746-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28746-score" class="post-score" title="current number of votes">0</div><span id="post-28746-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Any word on WinPCAP 4.1.3 crashing Windows 8.1 systems?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows8.1" rel="tag" title="see questions tagged &#39;windows8.1&#39;">windows8.1</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jan '14, 13:45</strong></p><img src="https://secure.gravatar.com/avatar/4a9db5337ac69560cbc20655a5f313b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ahusmc&#39;s gravatar image" /><p><span>ahusmc</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ahusmc has no accepted answers">0%</span></p></div></div><div id="comments-container-28746" class="comments-container"></div><div id="comment-tools-28746" class="comment-tools"></div><div class="clear"></div><div id="comment-28746-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

