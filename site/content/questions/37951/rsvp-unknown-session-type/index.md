+++
type = "question"
title = "RSVP &quot;Unknown session type&quot;"
description = '''Resource ReserVation Protocol (RSVP): PATH Message.   RSVP Header. PATH Message.   0000 .... = RSVP Version: 0  .... 0000 = Flags: 0x00  Message Type: PATH Message. (1)  Message Checksum: 0xf7fa [incorrect, should be 0xf7fa]  Sending TTL: 0  Message length: 0 Unknown session type  Is the &quot;Unknown se...'''
date = "2014-11-18T09:54:00Z"
lastmod = "2014-11-19T13:22:00Z"
weight = 37951
keywords = [ "unknown", "session", "type", "rsvp", "checksum" ]
aliases = [ "/questions/37951" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RSVP "Unknown session type"](/questions/37951/rsvp-unknown-session-type)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37951-score" class="post-score" title="current number of votes">0</div><span id="post-37951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>Resource ReserVation Protocol (RSVP): PATH Message. 
    RSVP Header. PATH Message. 
        0000 .... = RSVP Version: 0
        .... 0000 = Flags: 0x00
        Message Type: PATH Message.  (1)
        Message Checksum: 0xf7fa [incorrect, should be 0xf7fa]
        Sending TTL: 0
        Message length: 0
Unknown session type</code></pre><p>Is the "Unknown session type" a separate issue from the incorrect Checksum?</p><p>What does "Unknown session type" mean?</p><p>Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span> <span class="post-tag tag-link-session" rel="tag" title="see questions tagged &#39;session&#39;">session</span> <span class="post-tag tag-link-type" rel="tag" title="see questions tagged &#39;type&#39;">type</span> <span class="post-tag tag-link-rsvp" rel="tag" title="see questions tagged &#39;rsvp&#39;">rsvp</span> <span class="post-tag tag-link-checksum" rel="tag" title="see questions tagged &#39;checksum&#39;">checksum</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '14, 09:54</strong></p><img src="https://secure.gravatar.com/avatar/c1ab6d7ea71df49ed60202c2919133f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sreeve29&#39;s gravatar image" /><p><span>sreeve29</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sreeve29 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Nov '14, 10:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-37951" class="comments-container"><span id="37976"></span><div id="comment-37976" class="comment"><div id="post-37976-score" class="comment-score"></div><div class="comment-text"><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5518">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5518</a></p></div><div id="comment-37976-info" class="comment-info"><span class="comment-age">(19 Nov '14, 13:19)</span> <span class="comment-user userinfo">John_Modlin</span></div></div></div><div id="comment-tools-37951" class="comment-tools"></div><div class="clear"></div><div id="comment-37951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37977"></span>

<div id="answer-container-37977" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37977-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37977-score" class="post-score" title="current number of votes">0</div><span id="post-37977-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5518">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5518</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Nov '14, 13:22</strong></p><img src="https://secure.gravatar.com/avatar/1f3966b6e9de3a63326e2d3fd51c8c04?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John_Modlin&#39;s gravatar image" /><p><span>John_Modlin</span><br />
<span class="score" title="120 reputation points">120</span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John_Modlin has no accepted answers">0%</span></p></div></div><div id="comments-container-37977" class="comments-container"></div><div id="comment-tools-37977" class="comment-tools"></div><div class="clear"></div><div id="comment-37977-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

