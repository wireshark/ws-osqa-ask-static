+++
type = "question"
title = "sniff out passwords?"
description = '''so i&#x27;m very new to wireshark, and i&#x27;m currently trying to get into an account on &#x27;moviestarplanet&#x27; i&#x27;ve been at it for hours now and nothing its working. if anyone can help me out that would be VERY appreciated. (fyi, i&#x27;m trying to get into an account that i own, and loss access to. AND YES, I TRIED...'''
date = "2016-11-11T01:36:00Z"
lastmod = "2016-11-11T05:15:00Z"
weight = 57297
keywords = [ "wireshark" ]
aliases = [ "/questions/57297" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [sniff out passwords?](/questions/57297/sniff-out-passwords)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57297-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57297-score" class="post-score" title="current number of votes">0</div><span id="post-57297-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>so i'm very new to wireshark, and i'm currently trying to get into an account on 'moviestarplanet' i've been at it for hours now and nothing its working.</p><p>if anyone can help me out that would be VERY appreciated.</p><p>(fyi, i'm trying to get into an account that i own, and loss access to. AND YES, I TRIED THE "forgot your password?" THING BUT I'VE LOST THE EMAIL ACCOUNT I USED)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '16, 01:36</strong></p><img src="https://secure.gravatar.com/avatar/bb6740dbeb267542a30a07db205f31fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wankywentz&#39;s gravatar image" /><p><span>wankywentz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wankywentz has no accepted answers">0%</span></p></div></div><div id="comments-container-57297" class="comments-container"></div><div id="comment-tools-57297" class="comment-tools"></div><div class="clear"></div><div id="comment-57297-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="57298"></span>

<div id="answer-container-57298" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57298-score" class="post-score" title="current number of votes">3</div><span id="post-57298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://blog.packet-foo.com/2016/07/how-to-use-wireshark-to-steal-passwords/">https://blog.packet-foo.com/2016/07/how-to-use-wireshark-to-steal-passwords/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '16, 01:46</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-57298" class="comments-container"></div><div id="comment-tools-57298" class="comment-tools"></div><div class="clear"></div><div id="comment-57298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="57315"></span>

<div id="answer-container-57315" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57315-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57315-score" class="post-score" title="current number of votes">2</div><span id="post-57315-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>First of all, looking at the "Contact" page of moviestarplanet (at least on the dutch version of the site), there are details on how to get back into moviestarplanet, even when you don't have access to your email anymore.</p><p>Then how to do it with wireshark (for future reference to people trying to retrieve lost passwords), there are two methods used to let you log in without providing credentials:</p><ol><li>Password stored in your browser. If your browser has the credentials, you don't need wireshark, as you can retrieve the login/password from the password manager in your browser.</li><li>Persistent cookie. If the site uses a persistent cookie, the password is not stored, just a session token. So even when capturing the data, you will not see the username/password. If the site is using https, even the cookie is not visible as the traffic is encrypted.</li></ol><p>So using wireshark to recover your own http username/password is really not the right road to take.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '16, 05:15</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-57315" class="comments-container"></div><div id="comment-tools-57315" class="comment-tools"></div><div class="clear"></div><div id="comment-57315-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

