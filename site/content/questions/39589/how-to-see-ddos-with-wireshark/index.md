+++
type = "question"
title = "How to see DDoS with wireshark"
description = '''Hi ,  I am new in using wireshark . My firs question is where is best spot in network to set pc with wireshark for network monitoring ( my network have two separate gateway) . Like a title says how can I, with wireshark discover DDoS attacka?'''
date = "2015-02-03T00:36:00Z"
lastmod = "2015-02-03T00:36:00Z"
weight = 39589
keywords = [ "ddos" ]
aliases = [ "/questions/39589" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to see DDoS with wireshark](/questions/39589/how-to-see-ddos-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39589-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39589-score" class="post-score" title="current number of votes">0</div><span id="post-39589-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ,</p><p>I am new in using wireshark . My firs question is where is best spot in network to set pc with wireshark for network monitoring ( my network have two separate gateway) . Like a title says how can I, with wireshark discover DDoS attacka?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '15, 00:36</strong></p><img src="https://secure.gravatar.com/avatar/0d70fd8556ed207aba8d2d27815c3ee2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zorzcom&#39;s gravatar image" /><p><span>zorzcom</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zorzcom has no accepted answers">0%</span></p></div></div><div id="comments-container-39589" class="comments-container"></div><div id="comment-tools-39589" class="comment-tools"></div><div class="clear"></div><div id="comment-39589-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

