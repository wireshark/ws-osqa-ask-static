+++
type = "question"
title = "Windows 2000 SP4 and WIreshark"
description = '''This is more of a solution to anyone else forced to run Wireshark on obsoleted Windows 2000. After several hours I was able to get Wireshark 1.2.5 running with Windows 2000 SP4. I read several posts before I came upon the recommendation that this combination works -- it does! I hope someone else ben...'''
date = "2011-07-28T15:33:00Z"
lastmod = "2011-07-29T01:20:00Z"
weight = 5354
keywords = [ "wireshark1.2.5", "sp4", "windows2000" ]
aliases = [ "/questions/5354" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 2000 SP4 and WIreshark](/questions/5354/windows-2000-sp4-and-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5354-score" class="post-score" title="current number of votes">0</div><span id="post-5354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This is more of a solution to anyone else forced to run Wireshark on obsoleted Windows 2000. After several hours I was able to get Wireshark 1.2.5 running with Windows 2000 SP4. I read several posts before I came upon the recommendation that this combination works -- it does! I hope someone else benefits from this post.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark1.2.5" rel="tag" title="see questions tagged &#39;wireshark1.2.5&#39;">wireshark1.2.5</span> <span class="post-tag tag-link-sp4" rel="tag" title="see questions tagged &#39;sp4&#39;">sp4</span> <span class="post-tag tag-link-windows2000" rel="tag" title="see questions tagged &#39;windows2000&#39;">windows2000</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '11, 15:33</strong></p><img src="https://secure.gravatar.com/avatar/23f6b70d3f24275be519e0c2aa3a0a7a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="williamdor&#39;s gravatar image" /><p><span>williamdor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="williamdor has no accepted answers">0%</span></p></div></div><div id="comments-container-5354" class="comments-container"><span id="5356"></span><div id="comment-5356" class="comment"><div id="post-5356-score" class="comment-score"></div><div class="comment-text"><p>Can you confirm that Wireshark 1.2.18 is working for you ? This 1.2.5 is <em>old</em> and <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5874">bug 5874</a> shows that now 1.2.18 is confirmed working on Windows 2000 Prof SP4.</p></div><div id="comment-5356-info" class="comment-info"><span class="comment-age">(29 Jul '11, 01:20)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-5354" class="comment-tools"></div><div class="clear"></div><div id="comment-5354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

