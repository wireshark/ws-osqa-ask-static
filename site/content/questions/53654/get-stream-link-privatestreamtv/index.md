+++
type = "question"
title = "Get stream link privatestream.tv"
description = '''I want link to VLC-Player. Here is Statisticks/conversations/TCP -&amp;gt; follow stream: http://ge.tt/9EROipb2 Here is rtmp://31.220.0.201/privatestream/ Tried: rtmp://31.220.0.201/privatestream/polsatsport22 rtmp://31.220.0.201/privatestream/polsatsport22?keys=vOpyT9xqbzHmbUKn6IcxQw&amp;amp;keyt=146695340...'''
date = "2016-06-26T05:05:00Z"
lastmod = "2016-06-27T06:29:00Z"
weight = 53654
keywords = [ "vlc" ]
aliases = [ "/questions/53654" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Get stream link privatestream.tv](/questions/53654/get-stream-link-privatestreamtv)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53654-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53654-score" class="post-score" title="current number of votes">0</div><span id="post-53654-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want link to VLC-Player. Here is Statisticks/conversations/TCP -&gt; follow stream: <a href="http://ge.tt/9EROipb2">http://ge.tt/9EROipb2</a></p><p>Here is <span>rtmp://31.220.0.201/privatestream/</span> Tried: <span>rtmp://31.220.0.201/privatestream/polsatsport22</span> <span>rtmp://31.220.0.201/privatestream/polsatsport22?keys=vOpyT9xqbzHmbUKn6IcxQw&amp;keyt=1466953401</span> <span>rtmp://31.220.0.201/privatestream/polsatsport22?keys=vOpyT9xqbzHmbUKn6IcxQw?keys=R3r2YHOO_bUmvCXu283UEA&amp;keyt=1466956706</span></p><p>but not works :( VLC return always error</p><p>I have only browser link: <a href="http://privatestream.tv/player?streamname=polsatsport22?keys=vOpyT9xqbzHmbUKn6IcxQw&amp;keyt=1466953401#">http://privatestream.tv/player?streamname=polsatsport22?keys=vOpyT9xqbzHmbUKn6IcxQw&amp;keyt=1466953401#</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vlc" rel="tag" title="see questions tagged &#39;vlc&#39;">vlc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '16, 05:05</strong></p><img src="https://secure.gravatar.com/avatar/98bafa784d7146ae6a63b74874cb137e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mar173&#39;s gravatar image" /><p><span>mar173</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mar173 has no accepted answers">0%</span></p></div></div><div id="comments-container-53654" class="comments-container"><span id="53673"></span><div id="comment-53673" class="comment"><div id="post-53673-score" class="comment-score"></div><div class="comment-text"><p>A warning to readers of this question: both Firefox and Chrome warn me to not follow the first link (ge.tt) because it says the site wants to install malware. I took their advice so I don't know if the link <em>actually</em> contains malware or not.</p></div><div id="comment-53673-info" class="comment-info"><span class="comment-age">(27 Jun '16, 06:29)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-53654" class="comment-tools"></div><div class="clear"></div><div id="comment-53654-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

