+++
type = "question"
title = "Real-time analysis of calls"
description = '''Are there any simple solutions that I can employ that will me watch call performance at a moments notice. Working in a call center we have over 100 agents who use a mix of SIP and phone, all VOIP of course. If an agent complains of quality during a call, I&#x27;d like to clck on that specific agen by nam...'''
date = "2011-01-25T07:26:00Z"
lastmod = "2011-01-25T07:51:00Z"
weight = 1923
keywords = [ "sip", "monitoring" ]
aliases = [ "/questions/1923" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Real-time analysis of calls](/questions/1923/real-time-analysis-of-calls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1923-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1923-score" class="post-score" title="current number of votes">0</div><span id="post-1923-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are there any simple solutions that I can employ that will me watch call performance at a moments notice. Working in a call center we have over 100 agents who use a mix of SIP and phone, all VOIP of course. If an agent complains of quality during a call, I'd like to clck on that specific agen by name and monitor the call as quickly as possible, not after the fact.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '11, 07:26</strong></p><img src="https://secure.gravatar.com/avatar/b6459f542539750a02890b98da78bf02?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CallCenterGuy&#39;s gravatar image" /><p><span>CallCenterGuy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CallCenterGuy has no accepted answers">0%</span></p></div></div><div id="comments-container-1923" class="comments-container"></div><div id="comment-tools-1923" class="comment-tools"></div><div class="clear"></div><div id="comment-1923-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1927"></span>

<div id="answer-container-1927" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1927-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1927-score" class="post-score" title="current number of votes">0</div><span id="post-1927-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you looked at Wiresharks capability in this regard?<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jan '11, 07:51</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span> </br></p></div></div><div id="comments-container-1927" class="comments-container"></div><div id="comment-tools-1927" class="comment-tools"></div><div class="clear"></div><div id="comment-1927-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

