+++
type = "question"
title = "Decrypt out of order SSL trace"
description = '''Hi, Can anyone hint me in the right direction to be able to decrypt SSL traffic that&#x27;s captured out of order using Wireshark? I have the private key, so that&#x27;s not an issue. But some really weird things are going on between client &amp;amp; server I think. Also, all hints are welcome for things to try b...'''
date = "2014-07-28T09:42:00Z"
lastmod = "2015-10-19T06:18:00Z"
weight = 34945
keywords = [ "ssl", "out-of-order", "decryption" ]
aliases = [ "/questions/34945" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt out of order SSL trace](/questions/34945/decrypt-out-of-order-ssl-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34945-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34945-score" class="post-score" title="current number of votes">2</div><span id="post-34945-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Can anyone hint me in the right direction to be able to decrypt SSL traffic that's captured out of order using Wireshark? I have the private key, so that's not an issue. But some really weird things are going on between client &amp; server I think. Also, all hints are welcome for things to try by the client which can lead to decryptable traffic. Or if someone recognizes the behaviour and can hint what is going wrong here, that's also welcome!</p><p>Thanks in advance!</p><p>I think this traffic went ok in the client's browser, but I'm unable to decrypt it - I assume this is because the segments are out of order: <img src="https://osqa-ask.wireshark.org/upfiles/out-of-order-https.png" alt="alt text" /></p><p>Before this happened, I presume there were some attempts to setup an https connection which failed: <img src="https://osqa-ask.wireshark.org/upfiles/out-of-order-https-2.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-out-of-order" rel="tag" title="see questions tagged &#39;out-of-order&#39;">out-of-order</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '14, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/0e77c22f13dbe3a08a5b79dfcd39d9fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fjoerie&#39;s gravatar image" /><p><span>Fjoerie</span><br />
<span class="score" title="41 reputation points">41</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fjoerie has no accepted answers">0%</span></p></img></div></div><div id="comments-container-34945" class="comments-container"><span id="46693"></span><div id="comment-46693" class="comment"><div id="post-46693-score" class="comment-score"></div><div class="comment-text"><p>Any solution to this?</p></div><div id="comment-46693-info" class="comment-info"><span class="comment-age">(19 Oct '15, 06:18)</span> <span class="comment-user userinfo">theo66</span></div></div></div><div id="comment-tools-34945" class="comment-tools"></div><div class="clear"></div><div id="comment-34945-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

