+++
type = "question"
title = "How do I know the Release date for each version?"
description = '''I am looking to see where I can find the release dates for each version of WireShark. I see the Security Advisories have dates, but the Release Notes do not.'''
date = "2013-05-20T14:57:00Z"
lastmod = "2013-05-20T21:39:00Z"
weight = 21333
keywords = [ "release", "update" ]
aliases = [ "/questions/21333" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I know the Release date for each version?](/questions/21333/how-do-i-know-the-release-date-for-each-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21333-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21333-score" class="post-score" title="current number of votes">0</div><span id="post-21333-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking to see where I can find the release dates for each version of WireShark. I see the Security Advisories have dates, but the Release Notes do not.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-release" rel="tag" title="see questions tagged &#39;release&#39;">release</span> <span class="post-tag tag-link-update" rel="tag" title="see questions tagged &#39;update&#39;">update</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '13, 14:57</strong></p><img src="https://secure.gravatar.com/avatar/46acdb592b25a68237fc6176524011b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nsweet24&#39;s gravatar image" /><p><span>nsweet24</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nsweet24 has no accepted answers">0%</span></p></div></div><div id="comments-container-21333" class="comments-container"></div><div id="comment-tools-21333" class="comment-tools"></div><div class="clear"></div><div id="comment-21333-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21334"></span>

<div id="answer-container-21334" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21334-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21334-score" class="post-score" title="current number of votes">1</div><span id="post-21334-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the announce mailing list archive? See the <a href="http://news.gmane.org/gmane.network.wireshark.announce">Gmane archives</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '13, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-21334" class="comments-container"><span id="21338"></span><div id="comment-21338" class="comment"><div id="post-21338-score" class="comment-score"></div><div class="comment-text"><p>also here:</p><blockquote><p><a href="http://wiki.wireshark.org/Development/LifeCycle">http://wiki.wireshark.org/Development/LifeCycle</a><br />
<a href="http://wiki.wireshark.org/Development/Roadmap">http://wiki.wireshark.org/Development/Roadmap</a></p></blockquote></div><div id="comment-21338-info" class="comment-info"><span class="comment-age">(20 May '13, 21:39)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-21334" class="comment-tools"></div><div class="clear"></div><div id="comment-21334-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

