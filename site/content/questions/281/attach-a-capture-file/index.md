+++
type = "question"
title = "Attach a capture file?"
description = '''Hi All, Is there an option to attach a capture file? I don&#x27;t see one.   Very cool site. Thanks. Owen'''
date = "2010-09-22T19:13:00Z"
lastmod = "2011-04-01T08:01:00Z"
weight = 281
keywords = [ "meta", "attachment" ]
aliases = [ "/questions/281" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Attach a capture file?](/questions/281/attach-a-capture-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-281-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-281-score" class="post-score" title="current number of votes">0</div><span id="post-281-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>Is there an option to attach a capture file? I don't see one.<br />
</p><p>Very cool site.</p><p>Thanks.</p><p>Owen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span> <span class="post-tag tag-link-attachment" rel="tag" title="see questions tagged &#39;attachment&#39;">attachment</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '10, 19:13</strong></p><img src="https://secure.gravatar.com/avatar/e12b8d5f904f018189f5cf3c69cbe5f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Owen&#39;s gravatar image" /><p><span>Owen</span><br />
<span class="score" title="21 reputation points">21</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Owen has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Nov '10, 06:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-281" class="comments-container"></div><div id="comment-tools-281" class="comment-tools"></div><div class="clear"></div><div id="comment-281-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="287"></span>

<div id="answer-container-287" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-287-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-287-score" class="post-score" title="current number of votes">0</div><span id="post-287-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>At this point in time it is not possible to attach files to questions. There are several workarounds.</p><ul><li><p>If you have a small capture file, you might use 'uuencode' to transform it into text which can be added in the question (or answer).</p></li><li><p>If you have somewhere that you can host the file, you can create a link to it here.</p></li><li><p>And of course you can always ask your question on the users mailinglist attaching the file (and if necessary, you can add a link to the mailing-list archives).</p></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '10, 01:28</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-287" class="comments-container"><span id="3273"></span><div id="comment-3273" class="comment"><div id="post-3273-score" class="comment-score"></div><div class="comment-text"><p>Other proposed solutions suggested as answers to <a href="http://ask.wireshark.org/questions/1221/standard-way-people-should-attach-capture-files-to-questions-in-this-forum">this</a> question.</p></div><div id="comment-3273-info" class="comment-info"><span class="comment-age">(01 Apr '11, 08:01)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-287" class="comment-tools"></div><div class="clear"></div><div id="comment-287-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

