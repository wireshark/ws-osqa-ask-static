+++
type = "question"
title = "Winxp Users get disconnections when using remote desktop to win2012 server"
description = '''So I have a problem , that when users are mostly idle i dont see any abnormal behavior in the network. But once they connect to the server and start working via remote desktop. Wireshark shows alot of tcp retransmissions, tcp duplicate ack&#x27;s etc. Heres is a capture file uploaded to cloudshark . http...'''
date = "2014-04-14T04:42:00Z"
lastmod = "2014-04-14T04:42:00Z"
weight = 31787
keywords = [ "dupack", "rdp", "tcp", "packetloss" ]
aliases = [ "/questions/31787" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Winxp Users get disconnections when using remote desktop to win2012 server](/questions/31787/winxp-users-get-disconnections-when-using-remote-desktop-to-win2012-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31787-score" class="post-score" title="current number of votes">0</div><span id="post-31787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So I have a problem , that when users are mostly idle i dont see any abnormal behavior in the network. But once they connect to the server and start working via remote desktop. Wireshark shows alot of tcp retransmissions, tcp duplicate ack's etc.</p><p>Heres is a capture file uploaded to cloudshark . <a href="https://www.cloudshark.org/captures/7ac6c5954f16">https://www.cloudshark.org/captures/7ac6c5954f16</a></p><p>As i ping the computers via cmd from the server. I see at random intervals several packet drops.</p><p>Anyone know , how to diagnose this problem to find the solution?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dupack" rel="tag" title="see questions tagged &#39;dupack&#39;">dupack</span> <span class="post-tag tag-link-rdp" rel="tag" title="see questions tagged &#39;rdp&#39;">rdp</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-packetloss" rel="tag" title="see questions tagged &#39;packetloss&#39;">packetloss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Apr '14, 04:42</strong></p><img src="https://secure.gravatar.com/avatar/0f748e779284ae7360c5e78a3bd87b53?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Burundukas%20Lukas&#39;s gravatar image" /><p><span>Burundukas L...</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Burundukas Lukas has no accepted answers">0%</span></p></div></div><div id="comments-container-31787" class="comments-container"></div><div id="comment-tools-31787" class="comment-tools"></div><div class="clear"></div><div id="comment-31787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

