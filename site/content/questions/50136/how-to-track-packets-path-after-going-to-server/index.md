+++
type = "question"
title = "How to Track packets path after going to server?"
description = '''Hi Everyone i&#x27;m new to the Wireshark tool and i think i&#x27;ve doing good with it until now so i&#x27;ve tried it on sevral apps and programs and it was working just as it&#x27;s supposed to work tho i tried to use it in an online game , and from checking the packets i believe that the data i send goes to a centr...'''
date = "2016-02-12T05:13:00Z"
lastmod = "2016-02-12T05:42:00Z"
weight = 50136
keywords = [ "mmo", "server", "packets", "mysql" ]
aliases = [ "/questions/50136" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to Track packets path after going to server?](/questions/50136/how-to-track-packets-path-after-going-to-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50136-score" class="post-score" title="current number of votes">0</div><span id="post-50136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Everyone i'm new to the Wireshark tool and i think i've doing good with it until now so i've tried it on sevral apps and programs and it was working just as it's supposed to work tho i tried to use it in an online game , and from checking the packets i believe that the data i send goes to a central server than it's redistributed to other player like for private-messages the message go all the way to a server than the server send it back to the other player what i wish to know is how can i track the path of packets until it goes to the other player i think it's possible somehow but i need somebody to give the first steps thank you :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mmo" rel="tag" title="see questions tagged &#39;mmo&#39;">mmo</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-mysql" rel="tag" title="see questions tagged &#39;mysql&#39;">mysql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '16, 05:13</strong></p><img src="https://secure.gravatar.com/avatar/798704f9604f874b2c042102e6e204ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anas%20Haxer&#39;s gravatar image" /><p><span>Anas Haxer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anas Haxer has no accepted answers">0%</span></p></div></div><div id="comments-container-50136" class="comments-container"></div><div id="comment-tools-50136" class="comment-tools"></div><div class="clear"></div><div id="comment-50136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50137"></span>

<div id="answer-container-50137" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50137-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50137-score" class="post-score" title="current number of votes">0</div><span id="post-50137-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unless you have access to the the other player's LAN (which may be the same as yours but may be completely different) or to the server's own LAN, and unless you have enough rights and knowledge to capture on that LAN, you cannot capture packets between the server and the other player.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Feb '16, 05:42</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-50137" class="comments-container"></div><div id="comment-tools-50137" class="comment-tools"></div><div class="clear"></div><div id="comment-50137-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

