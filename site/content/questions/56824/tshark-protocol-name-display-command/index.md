+++
type = "question"
title = "tshark protocol name display command"
description = '''Hey guys I&#x27;m trying to convert a pcap file to a csv one. I want to display the protocol name I tried this command: -e_ws.col.protocol but didn&#x27;t display any results. Can someone help me please? Thanks'''
date = "2016-10-29T15:58:00Z"
lastmod = "2016-10-29T15:58:00Z"
weight = 56824
keywords = [ "protocol", "tshark", "name" ]
aliases = [ "/questions/56824" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tshark protocol name display command](/questions/56824/tshark-protocol-name-display-command)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56824-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56824-score" class="post-score" title="current number of votes">0</div><span id="post-56824-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey guys I'm trying to convert a pcap file to a csv one. I want to display the protocol name I tried this command: -e_ws.col.protocol but didn't display any results. Can someone help me please? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '16, 15:58</strong></p><img src="https://secure.gravatar.com/avatar/de42d7bdfa41aaf2afd2b71c0f71fbfa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Seymouch&#39;s gravatar image" /><p><span>Seymouch</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Seymouch has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Oct '16, 16:05</strong> </span></p></div></div><div id="comments-container-56824" class="comments-container"></div><div id="comment-tools-56824" class="comment-tools"></div><div class="clear"></div><div id="comment-56824-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

