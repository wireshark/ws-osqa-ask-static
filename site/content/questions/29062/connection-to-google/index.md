+++
type = "question"
title = "connection to google"
description = '''hi  i use Firefox chrome never been installed on machine once windows starts i opened wireshark and saw tcp connection to 173.194.70.99 syn syn ack ack fin ack fin ack also in cmd ipconfig /displaydns there is already www.google.com Record Name . . . . . : www.google.com Record Type . . . . . : 1 Ti...'''
date = "2014-01-21T10:01:00Z"
lastmod = "2014-01-21T11:35:00Z"
weight = 29062
keywords = [ "windows7", "google", "syn+ack" ]
aliases = [ "/questions/29062" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [connection to google](/questions/29062/connection-to-google)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29062-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29062-score" class="post-score" title="current number of votes">0</div><span id="post-29062-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i use Firefox chrome never been installed on machine once windows starts i opened wireshark and saw tcp connection to 173.194.70.99 syn syn ack ack fin ack fin ack also in cmd ipconfig /displaydns there is already</p><h2 id="www.google.com">www.google.com</h2><p>Record Name . . . . . : www.google.com Record Type . . . . . : 1 Time To Live . . . . : 85 Data Length . . . . . : 4 Section . . . . . . . : Answer A (Host) Record . . . : 173.194.70.104</p><p>GOOGLE WHY</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-google" rel="tag" title="see questions tagged &#39;google&#39;">google</span> <span class="post-tag tag-link-syn+ack" rel="tag" title="see questions tagged &#39;syn+ack&#39;">syn+ack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '14, 10:01</strong></p><img src="https://secure.gravatar.com/avatar/9e0f2d5f6e71f802d940b052b2579ca6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="windoze&#39;s gravatar image" /><p><span>windoze</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="windoze has no accepted answers">0%</span></p></div></div><div id="comments-container-29062" class="comments-container"><span id="29063"></span><div id="comment-29063" class="comment"><div id="post-29063-score" class="comment-score"></div><div class="comment-text"><p>You do know that the default Firefox homepage is Google? And are you sure you don't use any other Google services?</p></div><div id="comment-29063-info" class="comment-info"><span class="comment-age">(21 Jan '14, 10:06)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="29067"></span><div id="comment-29067" class="comment"><div id="post-29067-score" class="comment-score"></div><div class="comment-text"><p>nope no google services at all usual stuff apple bonjour logmein Eset VMWARE</p></div><div id="comment-29067-info" class="comment-info"><span class="comment-age">(21 Jan '14, 11:19)</span> <span class="comment-user userinfo">windoze</span></div></div><span id="29069"></span><div id="comment-29069" class="comment"><div id="post-29069-score" class="comment-score"></div><div class="comment-text"><p>next i'll install new win7 on vmware and recheck it</p></div><div id="comment-29069-info" class="comment-info"><span class="comment-age">(21 Jan '14, 11:20)</span> <span class="comment-user userinfo">windoze</span></div></div><span id="29070"></span><div id="comment-29070" class="comment"><div id="post-29070-score" class="comment-score"></div><div class="comment-text"><p>some Firefox add-ins do connect to google as well.</p></div><div id="comment-29070-info" class="comment-info"><span class="comment-age">(21 Jan '14, 11:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29062" class="comment-tools"></div><div class="clear"></div><div id="comment-29062-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

