+++
type = "question"
title = "[closed] Where have you gone, Firewall ACL Rules?"
description = '''Hello all, Can you no longer generate Firewall Rules using the latest version of Wireshark 2.0.3? I have looked everywhere I can think of within Wireshark and can longer find this ability? If I remember correctly it use to be under tools within Wireshark. Thanks'''
date = "2016-05-23T19:45:00Z"
lastmod = "2016-05-24T03:53:00Z"
weight = 52841
keywords = [ "firewall" ]
aliases = [ "/questions/52841" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Where have you gone, Firewall ACL Rules?](/questions/52841/where-have-you-gone-firewall-acl-rules)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52841-score" class="post-score" title="current number of votes">0</div><span id="post-52841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all,</p><p>Can you no longer generate Firewall Rules using the latest version of Wireshark 2.0.3? I have looked everywhere I can think of within Wireshark and can longer find this ability? If I remember correctly it use to be under tools within Wireshark.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '16, 19:45</strong></p><img src="https://secure.gravatar.com/avatar/88de69ca1060ec4e29c141020d71efdf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PacketHunter56&#39;s gravatar image" /><p><span>PacketHunter56</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PacketHunter56 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 May '16, 03:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-52841" class="comments-container"><span id="52861"></span><div id="comment-52861" class="comment"><div id="post-52861-score" class="comment-score"></div><div class="comment-text"><p>Don't folks even bother searching these days, e.g <a href="https://ask.wireshark.org/questions/51711/not-able-to-see-firewall-acl-rules-option-in-tools-menu-in-wireshark-202">here</a>, <a href="https://ask.wireshark.org/questions/51701/windows-10-and-wireshark-202-firewall-rules-acl">here</a> and <a href="https://ask.wireshark.org/questions/50191/firewall-acl-rules-not-in-tools-dropdown-menu">here</a>.</p><p>As this is a common duplicate I'm closing it.</p></div><div id="comment-52861-info" class="comment-info"><span class="comment-age">(24 May '16, 03:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-52841" class="comment-tools"></div><div class="clear"></div><div id="comment-52841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 24 May '16, 03:53

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="52842"></span>

<div id="answer-container-52842" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52842-score" class="post-score" title="current number of votes">0</div><span id="post-52842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's not in the Qt version yet. Launch the Wireshark Legacy (GTK+ Version) and you will see it. Legacy is installed in the same directory as the Qt version.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 May '16, 20:50</strong></p><img src="https://secure.gravatar.com/avatar/bb79e0c62df46ecf47cc004a0a2d3cbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rooster_50&#39;s gravatar image" /><p><span>Rooster_50</span><br />
<span class="score" title="238 reputation points">238</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="18 badges"><span class="bronze">●</span><span class="badgecount">18</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rooster_50 has 5 accepted answers">15%</span></p></div></div><div id="comments-container-52842" class="comments-container"></div><div id="comment-tools-52842" class="comment-tools"></div><div class="clear"></div><div id="comment-52842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="52848"></span>

<div id="answer-container-52848" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52848-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52848-score" class="post-score" title="current number of votes">0</div><span id="post-52848-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a regression in the Qt interface, for which you could <a href="https://bugs.wireshark.org">file a bug report</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 May '16, 23:39</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52848" class="comments-container"></div><div id="comment-tools-52848" class="comment-tools"></div><div class="clear"></div><div id="comment-52848-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

