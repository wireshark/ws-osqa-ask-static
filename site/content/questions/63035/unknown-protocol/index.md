+++
type = "question"
title = "Unknown Protocol"
description = '''I was messing around with my settings when I realized all the things I was picking up are &quot;unknown.&quot; They can not be followed and I am only an amateur with this. Please help. Anything I will be grateful for'''
date = "2017-07-23T22:52:00Z"
lastmod = "2017-07-23T22:54:00Z"
weight = 63035
keywords = [ "nublet" ]
aliases = [ "/questions/63035" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unknown Protocol](/questions/63035/unknown-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63035-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63035-score" class="post-score" title="current number of votes">0</div><span id="post-63035-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was messing around with my settings when I realized all the things I was picking up are "unknown." They can not be followed and I am only an amateur with this. Please help. Anything I will be grateful for</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nublet" rel="tag" title="see questions tagged &#39;nublet&#39;">nublet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '17, 22:52</strong></p><img src="https://secure.gravatar.com/avatar/2b231b407f912bbfc3f380ece1820420?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nublet&#39;s gravatar image" /><p><span>Nublet</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nublet has no accepted answers">0%</span></p></div></div><div id="comments-container-63035" class="comments-container"></div><div id="comment-tools-63035" class="comment-tools"></div><div class="clear"></div><div id="comment-63035-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63036"></span>

<div id="answer-container-63036" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63036-score" class="post-score" title="current number of votes">0</div><span id="post-63036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Fixed it..</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jul '17, 22:54</strong></p><img src="https://secure.gravatar.com/avatar/2b231b407f912bbfc3f380ece1820420?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nublet&#39;s gravatar image" /><p><span>Nublet</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nublet has no accepted answers">0%</span></p></div></div><div id="comments-container-63036" class="comments-container"></div><div id="comment-tools-63036" class="comment-tools"></div><div class="clear"></div><div id="comment-63036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

