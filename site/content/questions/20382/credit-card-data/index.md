+++
type = "question"
title = "Credit card data"
description = '''Hi can Wireshark detect packets containing credit card information and block if necessary? thanks.'''
date = "2013-04-12T07:45:00Z"
lastmod = "2013-04-12T08:14:00Z"
weight = 20382
keywords = [ "credit", "credit-card" ]
aliases = [ "/questions/20382" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Credit card data](/questions/20382/credit-card-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20382-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20382-score" class="post-score" title="current number of votes">0</div><span id="post-20382-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi can Wireshark detect packets containing credit card information and block if necessary? thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-credit" rel="tag" title="see questions tagged &#39;credit&#39;">credit</span> <span class="post-tag tag-link-credit-card" rel="tag" title="see questions tagged &#39;credit-card&#39;">credit-card</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '13, 07:45</strong></p><img src="https://secure.gravatar.com/avatar/7b6808b05de828f598234c6568807be6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sxhutch&#39;s gravatar image" /><p><span>sxhutch</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sxhutch has no accepted answers">0%</span></p></div></div><div id="comments-container-20382" class="comments-container"></div><div id="comment-tools-20382" class="comment-tools"></div><div class="clear"></div><div id="comment-20382-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20383"></span>

<div id="answer-container-20383" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20383-score" class="post-score" title="current number of votes">3</div><span id="post-20383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could probably create a regular expression filter that matches data that looks like a credit card string, but you'd need to build that expression first (or maybe find it somewhere). Blocking isn't possible; for that you need a data leakage prevention device that is put inline into the connection.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Apr '13, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-20383" class="comments-container"></div><div id="comment-tools-20383" class="comment-tools"></div><div class="clear"></div><div id="comment-20383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

