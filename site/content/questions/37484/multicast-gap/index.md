+++
type = "question"
title = "Multicast Gap"
description = '''Hi I need a help, in my network some devlopers complain they are receiving data lose on multicast groups.I captured the data but no idea how to analyze lost packets on UDP.Please help me out. '''
date = "2014-10-30T22:38:00Z"
lastmod = "2014-10-31T01:31:00Z"
weight = 37484
keywords = [ "multicast" ]
aliases = [ "/questions/37484" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Multicast Gap](/questions/37484/multicast-gap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37484-score" class="post-score" title="current number of votes">0</div><span id="post-37484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I need a help, in my network some devlopers complain they are receiving data lose on multicast groups.I captured the data but no idea how to analyze lost packets on UDP.Please help me out.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multicast" rel="tag" title="see questions tagged &#39;multicast&#39;">multicast</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '14, 22:38</strong></p><img src="https://secure.gravatar.com/avatar/059c1a3fe7cccfdf16beccc14bb41407?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sunilngsoc&#39;s gravatar image" /><p><span>sunilngsoc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sunilngsoc has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-37484" class="comments-container"><span id="37491"></span><div id="comment-37491" class="comment"><div id="post-37491-score" class="comment-score"></div><div class="comment-text"><p>please upload the capture file somewhere (google drive, dropbox, cloudshark.org) and post the link here.</p></div><div id="comment-37491-info" class="comment-info"><span class="comment-age">(31 Oct '14, 01:31)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37484" class="comment-tools"></div><div class="clear"></div><div id="comment-37484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

