+++
type = "question"
title = "sniffing whatsapp chat"
description = '''is it possible sniff whatsapp voice chat in wireshark??'''
date = "2014-01-07T08:05:00Z"
lastmod = "2014-01-08T08:49:00Z"
weight = 28636
keywords = [ "whatsapp" ]
aliases = [ "/questions/28636" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [sniffing whatsapp chat](/questions/28636/sniffing-whatsapp-chat)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28636-score" class="post-score" title="current number of votes">0</div><span id="post-28636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it possible sniff whatsapp voice chat in wireshark??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '14, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Jan '14, 12:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28636" class="comments-container"><span id="28638"></span><div id="comment-28638" class="comment"><div id="post-28638-score" class="comment-score">1</div><div class="comment-text"><p>When your were busy decorating your question title to make it stand out did you even think to try typing whatsapp into the search box?</p></div><div id="comment-28638-info" class="comment-info"><span class="comment-age">(07 Jan '14, 08:38)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="28646"></span><div id="comment-28646" class="comment"><div id="post-28646-score" class="comment-score"></div><div class="comment-text"><p>Or simply click on the tag you added yourself ('whatsapp')...</p></div><div id="comment-28646-info" class="comment-info"><span class="comment-age">(07 Jan '14, 12:02)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28636" class="comment-tools"></div><div class="clear"></div><div id="comment-28636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28668"></span>

<div id="answer-container-28668" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28668-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28668-score" class="post-score" title="current number of votes">0</div><span id="post-28668-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>O.K. to add an answer to this:</p><p>Please see here</p><blockquote><p><a href="http://ask.wireshark.org/questions/13317/whatsapp-is-now-encrypted">http://ask.wireshark.org/questions/13317/whatsapp-is-now-encrypted</a><br />
</p></blockquote><p>and here</p><blockquote><p><a href="http://ask.wireshark.org/questions/15793/capturing-whatsapp-chat-messages">http://ask.wireshark.org/questions/15793/capturing-whatsapp-chat-messages</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '14, 08:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-28668" class="comments-container"></div><div id="comment-tools-28668" class="comment-tools"></div><div class="clear"></div><div id="comment-28668-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

