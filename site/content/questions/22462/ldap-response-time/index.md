+++
type = "question"
title = "ldap response time"
description = '''Please let me know if ldap.time is working on current version of wireshark. I am using Version 1.8.6 (SVN Rev 48142 from /trunk-1.8). Not sure if this is computed correctly in this version.  If not, is there a way to calculate the ldap (req&amp;lt;-&amp;gt;response) time using wireshark and how.  Thanks Rav...'''
date = "2013-06-28T14:28:00Z"
lastmod = "2013-06-28T15:09:00Z"
weight = 22462
keywords = [ "ldap" ]
aliases = [ "/questions/22462" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ldap response time](/questions/22462/ldap-response-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22462-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22462-score" class="post-score" title="current number of votes">0</div><span id="post-22462-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please let me know if ldap.time is working on current version of wireshark. I am using Version 1.8.6 (SVN Rev 48142 from /trunk-1.8). Not sure if this is computed correctly in this version.</p><p>If not, is there a way to calculate the ldap (req&lt;-&gt;response) time using wireshark and how.</p><p>Thanks Ravi</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ldap" rel="tag" title="see questions tagged &#39;ldap&#39;">ldap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '13, 14:28</strong></p><img src="https://secure.gravatar.com/avatar/83c75c650d34b2f4010037cb70882390?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ravi_NS&#39;s gravatar image" /><p><span>Ravi_NS</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ravi_NS has no accepted answers">0%</span></p></div></div><div id="comments-container-22462" class="comments-container"></div><div id="comment-tools-22462" class="comment-tools"></div><div class="clear"></div><div id="comment-22462-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22463"></span>

<div id="answer-container-22463" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22463-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22463-score" class="post-score" title="current number of votes">1</div><span id="post-22463-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I tested the value of ldap.time with <a href="http://wiki.wireshark.org/SampleCaptures">some capture files</a>. The calculation of Wireshark (1.10.0) is correct.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jun '13, 15:09</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Jun '13, 15:11</strong> </span></p></div></div><div id="comments-container-22463" class="comments-container"></div><div id="comment-tools-22463" class="comment-tools"></div><div class="clear"></div><div id="comment-22463-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

