+++
type = "question"
title = "Captured data into MySQL"
description = '''Hello, I am building HTTP analyzer (based on captured data) and I need to get access to HTTP fields. How to put this data to MySQL? I can parse XML, so XML is also fine. Wireshark has an excellent ability to divide raw data into HTTP headers. Is there any kind of API to perform same but with functio...'''
date = "2013-12-10T22:38:00Z"
lastmod = "2013-12-11T05:46:00Z"
weight = 27991
keywords = [ "xml", "capture-filter", "database", "http", "mysql" ]
aliases = [ "/questions/27991" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Captured data into MySQL](/questions/27991/captured-data-into-mysql)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27991-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27991-score" class="post-score" title="current number of votes">0</div><span id="post-27991-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am building HTTP analyzer (based on captured data) and I need to get access to HTTP fields. How to put this data to MySQL? I can parse XML, so XML is also fine.</p><p>Wireshark has an excellent ability to divide raw data into HTTP headers. Is there any kind of API to perform same but with function calls.</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xml" rel="tag" title="see questions tagged &#39;xml&#39;">xml</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-database" rel="tag" title="see questions tagged &#39;database&#39;">database</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-mysql" rel="tag" title="see questions tagged &#39;mysql&#39;">mysql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '13, 22:38</strong></p><img src="https://secure.gravatar.com/avatar/f0b69c89ed6649f4169f16aae3c4a411?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TigranTsat&#39;s gravatar image" /><p><span>TigranTsat</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TigranTsat has no accepted answers">0%</span></p></div></div><div id="comments-container-27991" class="comments-container"></div><div id="comment-tools-27991" class="comment-tools"></div><div class="clear"></div><div id="comment-27991-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28000"></span>

<div id="answer-container-28000" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28000-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28000-score" class="post-score" title="current number of votes">0</div><span id="post-28000-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please click on the tag 'database' in your question and then read the answers to the other questions that show up. Most of them are similar to your question.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Dec '13, 05:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28000" class="comments-container"></div><div id="comment-tools-28000" class="comment-tools"></div><div class="clear"></div><div id="comment-28000-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

