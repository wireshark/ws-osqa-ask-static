+++
type = "question"
title = "Capturing WhatsApp chat messages"
description = '''Any more help here? I am particularly interested in looking at WhatsApp chats on Blackberry. I know there are certain whatsapp chats being deleted and I won wondering:  Is there a way to auto backup or auto save WhatApp chats on their device which can be retrieved after they delete the chats? I have...'''
date = "2012-11-11T00:48:00Z"
lastmod = "2012-11-11T09:10:00Z"
weight = 15793
keywords = [ "whatsapp", "blackberry" ]
aliases = [ "/questions/15793" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing WhatsApp chat messages](/questions/15793/capturing-whatsapp-chat-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15793-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15793-score" class="post-score" title="current number of votes">0</div><span id="post-15793-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Any more help here? I am particularly interested in looking at WhatsApp chats on Blackberry. I know there are certain whatsapp chats being deleted and I won wondering:</p><ul><li>Is there a way to auto backup or auto save WhatApp chats on their device which can be retrieved after they delete the chats? I have access the the phone and can install whatever app I need</li></ul><p>-How can intercep or sniffer Whatsapp on Blackberry using Wifi?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span> <span class="post-tag tag-link-blackberry" rel="tag" title="see questions tagged &#39;blackberry&#39;">blackberry</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '12, 00:48</strong></p><img src="https://secure.gravatar.com/avatar/9d79d51d41864fae0200109e57807c31?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="torontoguy&#39;s gravatar image" /><p><span>torontoguy</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="torontoguy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>11 Nov '12, 09:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-15793" class="comments-container"><span id="15801"></span><div id="comment-15801" class="comment"><div id="post-15801-score" class="comment-score"></div><div class="comment-text"><p>I converted your "answer" to a new question as that's how this site works. Please read the FAQ for more details.</p></div><div id="comment-15801-info" class="comment-info"><span class="comment-age">(11 Nov '12, 09:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-15793" class="comment-tools"></div><div class="clear"></div><div id="comment-15793-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15796"></span>

<div id="answer-container-15796" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15796-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15796-score" class="post-score" title="current number of votes">0</div><span id="post-15796-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Regarding your Questions about the internals of WhatsApp:</p><ul><li><p>As we don't have insight into the application, it is impossible to answer the question about their ability to delete chats. Looking at it from a pure technical standpoint: The application runs on your Phone and manages the whole WhatsApp communication and data storage. So, yes technically it would be possible to delete single chats. But why should they do that? I suggest to ask this question in a forum related to WhatsApp.</p></li><li><p>I don't know whether it's possible to backup any WhatsApp data and I suggest to ask this question in a forum related to WhatsApp.</p></li></ul><blockquote><p>-How can intercep or sniffer Whatsapp on Blackberry using Wifi?</p></blockquote><p>WhatsApp communication is now encrypted, so you won't have any chance to decrypt that communication with Wireshark. See also the following question:</p><blockquote><p><code>http://ask.wireshark.org/questions/13317/whatsapp-is-now-encrypted</code><br />
</p></blockquote><p>So again: If you want to capture WhatsApp traffic, I suggest to ask this question in a forum related to WhatsApp. Maybe there is a way to do that on the Phone itself. However that is totally unrelated to Wireshark.</p><p><strong>UPDATE:</strong> According to the WhatsApp FAQ, it's possible to save the chat history to a "media card" (flash drive).</p><blockquote><p><code>https://whatsapp.zendesk.com/entries/21040067-my-chats-are-gone-how-do-i-save-chat-history-on-my-blackberry</code><br />
</p></blockquote><p>So, if you think you lost any chats, it cloud be</p><ul><li>due to write errors on the "media card" (disk full, card broken, etc.)</li><li><strong>OR</strong> due to some secret chat delete feature in the application</li><li><strong>OR</strong> due to bug in the application</li><li><strong>OR</strong> due to a classical layer 8 problem, namely: the user simply deleted the chat</li></ul><p>Guess what I believe is the most probable reason for the missing chats? ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '12, 01:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Nov '12, 00:53</strong> </span></p></div></div><div id="comments-container-15796" class="comments-container"></div><div id="comment-tools-15796" class="comment-tools"></div><div class="clear"></div><div id="comment-15796-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

