+++
type = "question"
title = "Cannot access Edit Interface Settings dialog box in Windows"
description = '''The manual says to double-click an interface in the Capture Options box to bring up the Edit Interface Settings box, but when I do that nothing happens. I am running WireShark 2.2.5 on Windows 10 Home Edition. (On a side note, it&#x27;s pretty lame that the only way to access it is through double-clickin...'''
date = "2017-03-04T16:47:00Z"
lastmod = "2017-03-05T06:21:00Z"
weight = 59856
keywords = [ "windows", "gui", "windows10", "networkinterfaces" ]
aliases = [ "/questions/59856" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot access Edit Interface Settings dialog box in Windows](/questions/59856/cannot-access-edit-interface-settings-dialog-box-in-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59856-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59856-score" class="post-score" title="current number of votes">0</div><span id="post-59856-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The manual says to double-click an interface in the Capture Options box to bring up the Edit Interface Settings box, but when I do that nothing happens. I am running WireShark 2.2.5 on Windows 10 Home Edition. (On a side note, it's pretty lame that the only way to access it is through double-clicking, without any fallbacks like right-clicking or a keyboard shortcut.) I am trying to turn on monitor mode so that I can monitor other machines on my wireless network (I have installed Npcap for that purpose), so I really need to access that dialog box or the whole effort to setup these programs has been pointless.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span> <span class="post-tag tag-link-networkinterfaces" rel="tag" title="see questions tagged &#39;networkinterfaces&#39;">networkinterfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '17, 16:47</strong></p><img src="https://secure.gravatar.com/avatar/e851a628ba9afc7af9fa5c0998dbe889?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="WillKommen&#39;s gravatar image" /><p><span>WillKommen</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="WillKommen has no accepted answers">0%</span></p></div></div><div id="comments-container-59856" class="comments-container"></div><div id="comment-tools-59856" class="comment-tools"></div><div class="clear"></div><div id="comment-59856-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59860"></span>

<div id="answer-container-59860" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59860-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59860-score" class="post-score" title="current number of votes">0</div><span id="post-59860-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Click on the gear icon in the toolbar, or the Capture -&gt; Options menu entry (keyboard shortcut is CTRL+K).</p><p>The double click is only applicable for the old GTK based GUI (Wireshark Legacy).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '17, 06:21</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-59860" class="comments-container"></div><div id="comment-tools-59860" class="comment-tools"></div><div class="clear"></div><div id="comment-59860-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

