+++
type = "question"
title = "sniff ip messanger chat in LAN connection"
description = '''i tried t sniff ip messanger chat using &quot;msnms&quot; command in wireshark....but noone packets shown...plz give me some proper way to sniff chat of ip messanger in LAN connection...i have submit this project in 2 days... if any technique for this project then mail me on &quot;johnghost6@gmail.com&quot;... ::::::th...'''
date = "2013-09-14T22:21:00Z"
lastmod = "2013-09-15T11:39:00Z"
weight = 24703
keywords = [ "sniffing", "chat" ]
aliases = [ "/questions/24703" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [sniff ip messanger chat in LAN connection](/questions/24703/sniff-ip-messanger-chat-in-lan-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24703-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24703-score" class="post-score" title="current number of votes">-3</div><span id="post-24703-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i tried t sniff ip messanger chat using "msnms" command in wireshark....but noone packets shown...plz give me some proper way to sniff chat of ip messanger in LAN connection...i have submit this project in 2 days... if any technique for this project then mail me on "<span class="__cf_email__" data-cfemail="214b4e494f46494e52551761464c40484d0f424e4c">[email protected]</span>"...</p><p>::::::thankyou:::::::</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-chat" rel="tag" title="see questions tagged &#39;chat&#39;">chat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '13, 22:21</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '13, 01:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-24703" class="comments-container"><span id="24709"></span><div id="comment-24709" class="comment"><div id="post-24709-score" class="comment-score"></div><div class="comment-text"><p>Please don't ask duplicate questions and please stop decorating the question title.</p></div><div id="comment-24709-info" class="comment-info"><span class="comment-age">(15 Sep '13, 01:09)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-24703" class="comment-tools"></div><div class="clear"></div><div id="comment-24703-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="24706"></span>

<div id="answer-container-24706" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24706-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24706-score" class="post-score" title="current number of votes">1</div><span id="post-24706-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>i have submit this project in 2 days.</p></blockquote><p>sounds like homework to me.</p><p>Furthermore, you already asked the same question.</p><blockquote><p><a href="http://ask.wireshark.org/questions/24464/chat-sniffing">http://ask.wireshark.org/questions/24464/chat-sniffing</a></p></blockquote><p>Have you ever read the <a href="http://www.wireshark.org/docs/">docs of Wireshark</a>? Have you ever watched one of the tutorial videos? If you have done that, please tell us what you don't understand and we will help you.</p><p>However, if you expect us to do your homework, by giving you details steps (as already asked in the question above), this site might be the wrong place.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '13, 01:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-24706" class="comments-container"><span id="24711"></span><div id="comment-24711" class="comment"><div id="post-24711-score" class="comment-score"></div><div class="comment-text"><p>i show 3-4 video..in those videos "msnms" command use for sniff msn packets but i couldn't get "msnms" packets..so any other technique???</p></div><div id="comment-24711-info" class="comment-info"><span class="comment-age">(15 Sep '13, 04:12)</span> <span class="comment-user userinfo">john6</span></div></div><span id="24718"></span><div id="comment-24718" class="comment"><div id="post-24718-score" class="comment-score"></div><div class="comment-text"><blockquote><p>so any other technique???</p></blockquote><p>hm.. did you tell us how your network setup looks like? I can't find any...</p><p>So, do you try to capture</p><ul><li>Ethernet traffic</li><li>wifi/wlan traffic</li><li>traffic of your own PC</li><li>traffic of other PCs in your network</li><li>do you use a switch</li><li>is it a managed switch</li><li>What is your current capture</li></ul></div><div id="comment-24718-info" class="comment-info"><span class="comment-age">(15 Sep '13, 11:39)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24706" class="comment-tools"></div><div class="clear"></div><div id="comment-24706-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="24713"></span>

<div id="answer-container-24713" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24713-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24713-score" class="post-score" title="current number of votes">0</div><span id="post-24713-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>So, 2 days to go - you better hurry up then! If this is your homework you got to 'work' this out by yourself though. Here some questions you need to answer yourself.</p><p>You think you've captured the msnms traffic:</p><ul><li>Do you see <code>tcp.port==1863</code> traffic as per <a href="http://wiki.wireshark.org/MSNMS">http://wiki.wireshark.org/MSNMS</a>?</li><li>If not, which port are communicating on to your msnms server? <code>"frame contains 4d53:4720"</code></li><li>Are you familiar with the "Decode As" function?</li><li>Is the session SSL/TLS encrypted? <code>"tcp contains 17:03:00 or  tcp contains 17:03:01"</code></li><li>Does your wireshark recognize msnms in this <a href="http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=msnms.pcap">sample capture</a>?</li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '13, 05:50</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div></div><div id="comments-container-24713" class="comments-container"></div><div id="comment-tools-24713" class="comment-tools"></div><div class="clear"></div><div id="comment-24713-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

