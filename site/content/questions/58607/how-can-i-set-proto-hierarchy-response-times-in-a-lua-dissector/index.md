+++
type = "question"
title = "How can I set proto hierarchy &amp; response times in a lua dissector?"
description = '''Hello there, I am writing a lua dissector for a custom protocol.  To make it more intuitive, I would like to add information to &quot;Statistics-&amp;gt;Protocol Hierarchy&quot; and &quot;Statistics-&amp;gt;Service Response Time&quot;.  Can somebody tell me, what datastructures Wireshark uses to generate these statistics and h...'''
date = "2017-01-09T00:59:00Z"
lastmod = "2017-01-09T00:59:00Z"
weight = 58607
keywords = [ "lua", "statistics", "dissector" ]
aliases = [ "/questions/58607" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I set proto hierarchy & response times in a lua dissector?](/questions/58607/how-can-i-set-proto-hierarchy-response-times-in-a-lua-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58607-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58607-score" class="post-score" title="current number of votes">0</div><span id="post-58607-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello there,</p><p>I am writing a lua dissector for a custom protocol.</p><p>To make it more intuitive, I would like to add information to "Statistics-&gt;Protocol Hierarchy" and "Statistics-&gt;Service Response Time".</p><p>Can somebody tell me, what datastructures Wireshark uses to generate these statistics and how/whether I can set them via a lua dissector?</p><p>Cheers, Niklas</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jan '17, 00:59</strong></p><img src="https://secure.gravatar.com/avatar/05a89c5522316ab18baf9be9fba5b40d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="niklas&#39;s gravatar image" /><p><span>niklas</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="niklas has no accepted answers">0%</span></p></div></div><div id="comments-container-58607" class="comments-container"></div><div id="comment-tools-58607" class="comment-tools"></div><div class="clear"></div><div id="comment-58607-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

