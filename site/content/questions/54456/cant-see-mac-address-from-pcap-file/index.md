+++
type = "question"
title = "Can&#x27;t see MAC address from .PCAP file"
description = '''Hi, I am new to wireshark.I am using mikrotik router and will be needing to see mac addresses and ssid from .pcap file. I can see the mac addresses in winbox but not in wireshark. What should i do?'''
date = "2016-07-30T07:32:00Z"
lastmod = "2016-07-30T09:44:00Z"
weight = 54456
keywords = [ "mikrotik", "wireshark" ]
aliases = [ "/questions/54456" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see MAC address from .PCAP file](/questions/54456/cant-see-mac-address-from-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54456-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54456-score" class="post-score" title="current number of votes">0</div><span id="post-54456-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am new to wireshark.I am using mikrotik router and will be needing to see mac addresses and ssid from .pcap file. I can see the mac addresses in winbox but not in wireshark. What should i do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mikrotik" rel="tag" title="see questions tagged &#39;mikrotik&#39;">mikrotik</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '16, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/557d426153aa6950b4ae3541a97ab03d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tatsugot&#39;s gravatar image" /><p><span>tatsugot</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tatsugot has no accepted answers">0%</span></p></div></div><div id="comments-container-54456" class="comments-container"><span id="54458"></span><div id="comment-54458" class="comment"><div id="post-54458-score" class="comment-score"></div><div class="comment-text"><p>Can you publish an example of such a problematic .pcap file from Mikrotik? As you mention SSID, I guess you talk about the wireless sniffer, and I've just tried and I can see MAC addresses normally.</p><p>This site doesn't allow direct upload of capture files, you have to post it, login-free, at the Cloudshark service or any file-sharing service like Google Drive, Microsoft onedrive etc.</p></div><div id="comment-54458-info" class="comment-info"><span class="comment-age">(30 Jul '16, 09:39)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-54456" class="comment-tools"></div><div class="clear"></div><div id="comment-54456-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54459"></span>

<div id="answer-container-54459" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54459-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54459-score" class="post-score" title="current number of votes">0</div><span id="post-54459-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hey, sorry. I just needed to disable resolution.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '16, 09:44</strong></p><img src="https://secure.gravatar.com/avatar/557d426153aa6950b4ae3541a97ab03d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tatsugot&#39;s gravatar image" /><p><span>tatsugot</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tatsugot has no accepted answers">0%</span></p></div></div><div id="comments-container-54459" class="comments-container"></div><div id="comment-tools-54459" class="comment-tools"></div><div class="clear"></div><div id="comment-54459-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

