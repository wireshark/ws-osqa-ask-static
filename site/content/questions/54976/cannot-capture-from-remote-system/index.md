+++
type = "question"
title = "cannot capture from remote system"
description = '''when I try to capture a remote system, I got the message: Error reading from pipe, error 1460. What is going wrong.  I use wireshark 2.0.5'''
date = "2016-08-19T07:44:00Z"
lastmod = "2016-09-02T04:15:00Z"
weight = 54976
keywords = [ "remote-capture", "rpcap" ]
aliases = [ "/questions/54976" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [cannot capture from remote system](/questions/54976/cannot-capture-from-remote-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54976-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54976-score" class="post-score" title="current number of votes">0</div><span id="post-54976-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when I try to capture a remote system, I got the message:</p><p>Error reading from pipe, error 1460.</p><p>What is going wrong.</p><p>I use wireshark 2.0.5</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-rpcap" rel="tag" title="see questions tagged &#39;rpcap&#39;">rpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Aug '16, 07:44</strong></p><img src="https://secure.gravatar.com/avatar/f5d34ac98d38af88c235843fadf49dc6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Johan%20Hendrikx&#39;s gravatar image" /><p><span>Johan Hendrikx</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Johan Hendrikx has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Aug '16, 08:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-54976" class="comments-container"><span id="55280"></span><div id="comment-55280" class="comment"><div id="post-55280-score" class="comment-score"></div><div class="comment-text"><p>Hi, i also have this error.</p><p>Any idea ?</p><p>Thanks.</p></div><div id="comment-55280-info" class="comment-info"><span class="comment-age">(02 Sep '16, 03:55)</span> <span class="comment-user userinfo">Peli</span></div></div><span id="55281"></span><div id="comment-55281" class="comment"><div id="post-55281-score" class="comment-score"></div><div class="comment-text"><p>Local OS, remote OS, connection details, anything else that might be useful to help diagnose the issue?</p></div><div id="comment-55281-info" class="comment-info"><span class="comment-age">(02 Sep '16, 04:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="55283"></span><div id="comment-55283" class="comment"><div id="post-55283-score" class="comment-score"></div><div class="comment-text"><p>local os windows 10 remote device is a wireless accesspoint.</p><p>Yesterday I used the portal version of whireshark. And then no problems</p></div><div id="comment-55283-info" class="comment-info"><span class="comment-age">(02 Sep '16, 04:15)</span> <span class="comment-user userinfo">Johan Hendrikx</span></div></div></div><div id="comment-tools-54976" class="comment-tools"></div><div class="clear"></div><div id="comment-54976-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

