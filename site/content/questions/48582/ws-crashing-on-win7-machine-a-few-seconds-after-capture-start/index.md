+++
type = "question"
title = "WS crashing on win7 machine a few seconds after capture start"
description = '''We are trying to run Wireshark on a win7 machine to capture traffic between two VM&#x27;s on a host. When I launch wireshark, with out fail after about 5 seconds the app crashes and I see an error in the logs saying the faulting module name: libwireshark.dll. I&#x27;ve tried setting it to create multiple file...'''
date = "2015-12-16T11:50:00Z"
lastmod = "2015-12-16T14:53:00Z"
weight = 48582
keywords = [ "libwireshark.dll" ]
aliases = [ "/questions/48582" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WS crashing on win7 machine a few seconds after capture start](/questions/48582/ws-crashing-on-win7-machine-a-few-seconds-after-capture-start)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48582-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48582-score" class="post-score" title="current number of votes">0</div><span id="post-48582-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are trying to run Wireshark on a win7 machine to capture traffic between two VM's on a host. When I launch wireshark, with out fail after about 5 seconds the app crashes and I see an error in the logs saying the faulting module name: libwireshark.dll. I've tried setting it to create multiple files with different size, but that doesn't seem to matter. I don't see any memory or CPU spikes. Anyone have any ideas? I've got a 4 core and 8gigs of ram on this machine.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libwireshark.dll" rel="tag" title="see questions tagged &#39;libwireshark.dll&#39;">libwireshark.dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '15, 11:50</strong></p><img src="https://secure.gravatar.com/avatar/3662749e2cfd048a9726ba54384135ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Robin%20Goins&#39;s gravatar image" /><p><span>Robin Goins</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Robin Goins has no accepted answers">0%</span></p></div></div><div id="comments-container-48582" class="comments-container"></div><div id="comment-tools-48582" class="comment-tools"></div><div class="clear"></div><div id="comment-48582-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48587"></span>

<div id="answer-container-48587" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48587-score" class="post-score" title="current number of votes">0</div><span id="post-48587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What version of Wireshark?</p><p>Try uninstalling, ensuring all traces have been removed, and reinstalling.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Dec '15, 14:53</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-48587" class="comments-container"></div><div id="comment-tools-48587" class="comment-tools"></div><div class="clear"></div><div id="comment-48587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

