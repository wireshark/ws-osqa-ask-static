+++
type = "question"
title = "Availability Date of Wireshark for Ubuntu 16.04 LTS?"
description = '''Hi, yesterday was officially released the new LTS, is there already a planned date to get a version on launchpad ? What generally was the time lapse to get the version available for the new release? Thanks Fabio'''
date = "2016-04-22T08:09:00Z"
lastmod = "2016-04-22T10:35:00Z"
weight = 51872
keywords = [ "16.04", "ubuntu" ]
aliases = [ "/questions/51872" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Availability Date of Wireshark for Ubuntu 16.04 LTS?](/questions/51872/availability-date-of-wireshark-for-ubuntu-1604-lts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51872-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51872-score" class="post-score" title="current number of votes">0</div><span id="post-51872-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, yesterday was officially released the new LTS, is there already a planned date to get a version on launchpad ?</p><p>What generally was the time lapse to get the version available for the new release?</p><p>Thanks</p><p>Fabio</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-16.04" rel="tag" title="see questions tagged &#39;16.04&#39;">16.04</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '16, 08:09</strong></p><img src="https://secure.gravatar.com/avatar/e5463d5ff7c14dbe04082962df0b04bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fabio_dalfonso&#39;s gravatar image" /><p><span>fabio_dalfonso</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fabio_dalfonso has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Apr '16, 10:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51872" class="comments-container"></div><div id="comment-tools-51872" class="comment-tools"></div><div class="clear"></div><div id="comment-51872-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51874"></span>

<div id="answer-container-51874" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51874-score" class="post-score" title="current number of votes">0</div><span id="post-51874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Xenial appears to already have the latest stable 2.0.2 build, as seen <a href="http://packages.ubuntu.com/xenial/wireshark">here</a>. What are you after exactly?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Apr '16, 08:23</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-51874" class="comments-container"><span id="51876"></span><div id="comment-51876" class="comment"><div id="post-51876-score" class="comment-score"></div><div class="comment-text"><p>Hi, thanks. Up to the 15.10 , there was a launchpad repository to get wireshark, so I supposed it was still to appear there.</p><p>I did not yet try to install based on this premise.</p><p>So now in 16.04 wireshark is in the mainstream channel?</p><p>Thanks Fabio</p></div><div id="comment-51876-info" class="comment-info"><span class="comment-age">(22 Apr '16, 08:29)</span> <span class="comment-user userinfo">fabio_dalfonso</span></div></div><span id="51877"></span><div id="comment-51877" class="comment"><div id="post-51877-score" class="comment-score"></div><div class="comment-text"><p>It's the usual thing, when a new version of a distribution is released they have a chance to catch up to newer versions of software, which they appear to have done in this case.</p><p>When 2.0.3 comes out it's likely to require an install from the <a href="https://launchpad.net/~wireshark-dev/+archive/ubuntu/stable">Wireshark ppa</a> as the distribution (16.04) is "fixed" at 2.0.2. The Wireshark ppa is maintained by a Wireshark core developer <span>@rbalint</span> who doesn't frequent this site.</p><p>I don't know the Canonical policy on package updates during the life of a distribution version, but it's up to their maintainers to pick up a new package and include it whenever they want.</p></div><div id="comment-51877-info" class="comment-info"><span class="comment-age">(22 Apr '16, 09:06)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="51882"></span><div id="comment-51882" class="comment"><div id="post-51882-score" class="comment-score"></div><div class="comment-text"><p>IOW this is really a Canonical/Ubuntu question, not a Wireshark question. They're the ones responsible for getting and packaging Wireshark for Ubuntu, not us.</p></div><div id="comment-51882-info" class="comment-info"><span class="comment-age">(22 Apr '16, 10:35)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-51874" class="comment-tools"></div><div class="clear"></div><div id="comment-51874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

