+++
type = "question"
title = "Time stamp format"
description = '''Hello I have captured a file using wireshark on windows. I have run a listener on that captured file and i use pinfo.abs_ts the time stamp i got is  1367758370.5337 1367758370.5337  1367758370.5344  1367758370.5344  1367758370.5347  1367758370.5347  1367758370.5416  1367758370.5417  1367758370.542  ...'''
date = "2013-05-06T09:40:00Z"
lastmod = "2013-05-06T09:54:00Z"
weight = 20987
keywords = [ "stamp", "format", "time" ]
aliases = [ "/questions/20987" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Time stamp format](/questions/20987/time-stamp-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20987-score" class="post-score" title="current number of votes">0</div><span id="post-20987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I have captured a file using wireshark on windows. I have run a listener on that captured file and i use pinfo.abs_ts the time stamp i got is 1367758370.5337 1367758370.5337<br />
1367758370.5344<br />
1367758370.5344<br />
1367758370.5347<br />
1367758370.5347<br />
1367758370.5416<br />
1367758370.5417<br />
1367758370.542<br />
1367758370.542<br />
1367758370.5429<br />
1367758370.543<br />
if any one could tell the meaning of those time stamps forma and how to convert it, note that i care to know the milliseconds and seconds Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-stamp" rel="tag" title="see questions tagged &#39;stamp&#39;">stamp</span> <span class="post-tag tag-link-format" rel="tag" title="see questions tagged &#39;format&#39;">format</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '13, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/583b809745fa45690fa8b950c5d28714?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ashraf&#39;s gravatar image" /><p><span>Ashraf</span><br />
<span class="score" title="16 reputation points">16</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ashraf has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-20987" class="comments-container"></div><div id="comment-tools-20987" class="comment-tools"></div><div class="clear"></div><div id="comment-20987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20988"></span>

<div id="answer-container-20988" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20988-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20988-score" class="post-score" title="current number of votes">2</div><span id="post-20988-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The absolute timestamp is a <a href="http://en.wikipedia.org/wiki/Unix_time">unix time stamp</a>, which counts the seconds since Jan 1st 1970 0:00. So 1367758370.5344 <a href="http://www.unixtimestamp.com/index.php">corresponds</a> to:</p><pre><code>05 / 05 / 13 @ 7:52:50.5344 am EST</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 May '13, 09:52</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span> </br></br></p></div></div><div id="comments-container-20988" class="comments-container"><span id="20989"></span><div id="comment-20989" class="comment"><div id="post-20989-score" class="comment-score"></div><div class="comment-text"><p>thank you a lot</p></div><div id="comment-20989-info" class="comment-info"><span class="comment-age">(06 May '13, 09:54)</span> <span class="comment-user userinfo">Ashraf</span></div></div></div><div id="comment-tools-20988" class="comment-tools"></div><div class="clear"></div><div id="comment-20988-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

