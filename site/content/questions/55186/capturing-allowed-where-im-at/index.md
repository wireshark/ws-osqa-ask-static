+++
type = "question"
title = "Capturing allowed where I&#x27;m at?"
description = '''I am currently in UAE (Abu Dhabi) and when I start the capture I get nothing in return. I get no list of anything per my assignment utilizing this app and CMD Prompt. When I go to your troubleshooter, it asks to ensure I have privilege and access where I&#x27;m at. Is there anyway to see if my country is...'''
date = "2016-08-29T19:52:00Z"
lastmod = "2016-08-30T02:55:00Z"
weight = 55186
keywords = [ "capture" ]
aliases = [ "/questions/55186" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing allowed where I'm at?](/questions/55186/capturing-allowed-where-im-at)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55186-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55186-score" class="post-score" title="current number of votes">0</div><span id="post-55186-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am currently in UAE (Abu Dhabi) and when I start the capture I get nothing in return. I get no list of anything per my assignment utilizing this app and CMD Prompt. When I go to your troubleshooter, it asks to ensure I have privilege and access where I'm at. Is there anyway to see if my country is blocking Wireshark's command?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '16, 19:52</strong></p><img src="https://secure.gravatar.com/avatar/4aa13fee530de56aef91b4b28f776888?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mhb1638&#39;s gravatar image" /><p><span>mhb1638</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mhb1638 has no accepted answers">0%</span></p></div></div><div id="comments-container-55186" class="comments-container"></div><div id="comment-tools-55186" class="comment-tools"></div><div class="clear"></div><div id="comment-55186-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55193"></span>

<div id="answer-container-55193" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55193-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55193-score" class="post-score" title="current number of votes">0</div><span id="post-55193-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From a technical point of view, go visit <a href="https://wiki.wireshark.org/">the Wireshark Wiki</a> where capture setup of various topologies is discussed. Whether of not you are able to capture anything very much depends on the way your capture setup is constructed. There could be traffic limiting features which can limit your access. The operation of Wireshark itself cannot be influenced, the amount of access to the network can.</p><p>From a legal point of view, you should have proper clearance from the authoritative party involved, either the network manager, services manager, teacher or someone else in charge. It varies per situation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '16, 02:55</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-55193" class="comments-container"></div><div id="comment-tools-55193" class="comment-tools"></div><div class="clear"></div><div id="comment-55193-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

