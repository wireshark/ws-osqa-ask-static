+++
type = "question"
title = "Including custom dissector conversation as part of Wireshark Conversations window"
description = '''Hello, I am currently developing a plugin that dissects a protocol which is on top of TCP. I would like to know whether it is possible to track the conversations specific to this protocol in a separate tab in Wireshark conversations window. If so, it would be really helpful to get some idea on how t...'''
date = "2016-09-14T04:10:00Z"
lastmod = "2016-09-14T04:10:00Z"
weight = 55552
keywords = [ "conversation", "dissector" ]
aliases = [ "/questions/55552" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Including custom dissector conversation as part of Wireshark Conversations window](/questions/55552/including-custom-dissector-conversation-as-part-of-wireshark-conversations-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55552-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55552-score" class="post-score" title="current number of votes">0</div><span id="post-55552-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am currently developing a plugin that dissects a protocol which is on top of TCP. I would like to know whether it is possible to track the conversations specific to this protocol in a separate tab in Wireshark conversations window. If so, it would be really helpful to get some idea on how to implement such a feature.</p><p>Thanks in advance. [PS: I noticed that there is a function called "register_conversation_table()". Is that for this specific purpose?]</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversation" rel="tag" title="see questions tagged &#39;conversation&#39;">conversation</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '16, 04:10</strong></p><img src="https://secure.gravatar.com/avatar/70baba446202981a08e25a49438b4161?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sherlock_000&#39;s gravatar image" /><p><span>sherlock_000</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sherlock_000 has no accepted answers">0%</span></p></div></div><div id="comments-container-55552" class="comments-container"></div><div id="comment-tools-55552" class="comment-tools"></div><div class="clear"></div><div id="comment-55552-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

