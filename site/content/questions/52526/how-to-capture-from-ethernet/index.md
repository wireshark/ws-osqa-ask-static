+++
type = "question"
title = "how to capture from ethernet?"
description = '''Can someone explain how it work?'''
date = "2016-05-13T11:07:00Z"
lastmod = "2016-05-13T14:09:00Z"
weight = 52526
keywords = [ "me", "help" ]
aliases = [ "/questions/52526" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to capture from ethernet?](/questions/52526/how-to-capture-from-ethernet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52526-score" class="post-score" title="current number of votes">0</div><span id="post-52526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone explain how it work?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-me" rel="tag" title="see questions tagged &#39;me&#39;">me</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '16, 11:07</strong></p><img src="https://secure.gravatar.com/avatar/874a669bb18d085ea60144c24014a398?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adihann6969&#39;s gravatar image" /><p><span>adihann6969</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adihann6969 has no accepted answers">0%</span></p></div></div><div id="comments-container-52526" class="comments-container"></div><div id="comment-tools-52526" class="comment-tools"></div><div class="clear"></div><div id="comment-52526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52544"></span>

<div id="answer-container-52544" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52544-score" class="post-score" title="current number of votes">0</div><span id="post-52544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">this wiki article</a> insufficient? Or is your question too brief and you actually don't ask how to set it up but how it works internally?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 May '16, 14:09</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-52544" class="comments-container"></div><div id="comment-tools-52544" class="comment-tools"></div><div class="clear"></div><div id="comment-52544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

