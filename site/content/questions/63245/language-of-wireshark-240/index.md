+++
type = "question"
title = "Language of Wireshark 2.4.0"
description = '''Hello, Wireshark opens in French language on my computer. Its difficult to use it, because documentation on Internet is all in English. How can I change the language in English? Thanks in advance.'''
date = "2017-07-30T10:10:00Z"
lastmod = "2017-07-30T10:29:00Z"
weight = 63245
keywords = [ "french", "language", "english" ]
aliases = [ "/questions/63245" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Language of Wireshark 2.4.0](/questions/63245/language-of-wireshark-240)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63245-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63245-score" class="post-score" title="current number of votes">0</div><span id="post-63245-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Wireshark opens in French language on my computer. Its difficult to use it, because documentation on Internet is all in English. How can I change the language in English? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-french" rel="tag" title="see questions tagged &#39;french&#39;">french</span> <span class="post-tag tag-link-language" rel="tag" title="see questions tagged &#39;language&#39;">language</span> <span class="post-tag tag-link-english" rel="tag" title="see questions tagged &#39;english&#39;">english</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '17, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/7cc132380fb2c46bbcdca0df6ec61f8f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rohegolu&#39;s gravatar image" /><p><span>rohegolu</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rohegolu has no accepted answers">0%</span></p></div></div><div id="comments-container-63245" class="comments-container"></div><div id="comment-tools-63245" class="comment-tools"></div><div class="clear"></div><div id="comment-63245-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63246"></span>

<div id="answer-container-63246" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63246-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63246-score" class="post-score" title="current number of votes">0</div><span id="post-63246-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="rohegolu has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go <code>Editer -&gt; Preferences</code> The bottom-most drop-down menu on the popup window which opens is <code>Langue</code>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '17, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-63246" class="comments-container"></div><div id="comment-tools-63246" class="comment-tools"></div><div class="clear"></div><div id="comment-63246-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

