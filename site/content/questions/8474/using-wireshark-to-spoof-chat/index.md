+++
type = "question"
title = "Using wireshark to spoof chat"
description = '''I know that wireshark can monitor plaintext chats for IRC, facebook, gmail, etc etc, but has anyone tried to inject data packets to fake a conversation with someone? Meaning, the victim thinks they are talking to someone else..thanks!'''
date = "2012-01-19T08:30:00Z"
lastmod = "2012-01-19T09:32:00Z"
weight = 8474
keywords = [ "spoof", "irc", "inject", "chat" ]
aliases = [ "/questions/8474" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Using wireshark to spoof chat](/questions/8474/using-wireshark-to-spoof-chat)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8474-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8474-score" class="post-score" title="current number of votes">0</div><span id="post-8474-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know that wireshark can monitor plaintext chats for IRC, facebook, gmail, etc etc, but has anyone tried to inject data packets to fake a conversation with someone? Meaning, the victim thinks they are talking to someone else..thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spoof" rel="tag" title="see questions tagged &#39;spoof&#39;">spoof</span> <span class="post-tag tag-link-irc" rel="tag" title="see questions tagged &#39;irc&#39;">irc</span> <span class="post-tag tag-link-inject" rel="tag" title="see questions tagged &#39;inject&#39;">inject</span> <span class="post-tag tag-link-chat" rel="tag" title="see questions tagged &#39;chat&#39;">chat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '12, 08:30</strong></p><img src="https://secure.gravatar.com/avatar/76e9dedc4f298d534850e9c7ee117240?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xenos&#39;s gravatar image" /><p><span>xenos</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xenos has no accepted answers">0%</span></p></div></div><div id="comments-container-8474" class="comments-container"></div><div id="comment-tools-8474" class="comment-tools"></div><div class="clear"></div><div id="comment-8474-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8476"></span>

<div id="answer-container-8476" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8476-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8476-score" class="post-score" title="current number of votes">0</div><span id="post-8476-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jaap has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a packet analyzer not a packet generator.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '12, 09:05</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-8476" class="comments-container"><span id="8479"></span><div id="comment-8479" class="comment"><div id="post-8479-score" class="comment-score"></div><div class="comment-text"><p>ah, thank you. I feel dumb now lol. I will try to edit the packets I capture in wireshark and inject them with something like Nemesis.</p></div><div id="comment-8479-info" class="comment-info"><span class="comment-age">(19 Jan '12, 09:32)</span> <span class="comment-user userinfo">xenos</span></div></div></div><div id="comment-tools-8476" class="comment-tools"></div><div class="clear"></div><div id="comment-8476-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

