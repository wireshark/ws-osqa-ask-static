+++
type = "question"
title = "SSDP - Denial of service?"
description = '''I am doing some training in examining PCAP files for attacks. I am particularly suspicious of an SSDP protocol NOTIFY HTTP/1.1 which over a 10 minute period has appeared 1710 times from the same local IP address. Could this be seen as a denial of service attack?'''
date = "2015-03-03T04:03:00Z"
lastmod = "2015-03-03T04:20:00Z"
weight = 40205
keywords = [ "ddos", "attack", "ssdp", "hack" ]
aliases = [ "/questions/40205" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SSDP - Denial of service?](/questions/40205/ssdp-denial-of-service)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40205-score" class="post-score" title="current number of votes">0</div><span id="post-40205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am doing some training in examining PCAP files for attacks. I am particularly suspicious of an SSDP protocol NOTIFY HTTP/1.1 which over a 10 minute period has appeared 1710 times from the same local IP address. Could this be seen as a denial of service attack?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span> <span class="post-tag tag-link-attack" rel="tag" title="see questions tagged &#39;attack&#39;">attack</span> <span class="post-tag tag-link-ssdp" rel="tag" title="see questions tagged &#39;ssdp&#39;">ssdp</span> <span class="post-tag tag-link-hack" rel="tag" title="see questions tagged &#39;hack&#39;">hack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '15, 04:03</strong></p><img src="https://secure.gravatar.com/avatar/377c496a3b4c2be6a666feee7d42f34e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="r0ssan0&#39;s gravatar image" /><p><span>r0ssan0</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="r0ssan0 has no accepted answers">0%</span></p></div></div><div id="comments-container-40205" class="comments-container"></div><div id="comment-tools-40205" class="comment-tools"></div><div class="clear"></div><div id="comment-40205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40206"></span>

<div id="answer-container-40206" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40206-score" class="post-score" title="current number of votes">0</div><span id="post-40206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>SSDP is the Simple Service Discovery Protocol, which is really chatty. If it shows up it means that a node is trying to discover services, e.g. printers, shared folders, iTunes Libraries, etc. I doubt that you're seeing a DoS attack, because it's not flooding enough packets for that. DoS attacks have thousands of packets per second, not minutes ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Mar '15, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40206" class="comments-container"><span id="40207"></span><div id="comment-40207" class="comment"><div id="post-40207-score" class="comment-score"></div><div class="comment-text"><p>Ah no problem, i will count that one out then. Thanks for your quick response.</p></div><div id="comment-40207-info" class="comment-info"><span class="comment-age">(03 Mar '15, 04:20)</span> <span class="comment-user userinfo">r0ssan0</span></div></div></div><div id="comment-tools-40206" class="comment-tools"></div><div class="clear"></div><div id="comment-40206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

