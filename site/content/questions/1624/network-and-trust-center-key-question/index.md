+++
type = "question"
title = "Network and Trust Center Key Question"
description = '''Hello, I am trying to read the data sent from a zigbee enabled smart meter but the packets keep saying &quot;Encrypted Payload&quot; I have inserted both Trust Center and Network Key under Edit-&amp;gt;Preferences-&amp;gt;Protocols-&amp;gt;ZigBee NWK, but I still get the same message. Here is the format of the Network an...'''
date = "2011-01-04T14:18:00Z"
lastmod = "2014-11-24T07:18:00Z"
weight = 1624
keywords = [ "trust", "center", "key", "network" ]
aliases = [ "/questions/1624" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Network and Trust Center Key Question](/questions/1624/network-and-trust-center-key-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1624-score" class="post-score" title="current number of votes">0</div><span id="post-1624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am trying to read the data sent from a zigbee enabled smart meter but the packets keep saying "Encrypted Payload"</p><p>I have inserted both Trust Center and Network Key under Edit-&gt;Preferences-&gt;Protocols-&gt;ZigBee NWK, but I still get the same message.</p><p>Here is the format of the Network and Trust Center keys: xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx</p><p>I have the security level set as AES 128 Bit Encryption and 32 bit Interity Protection.</p><p>Am I doing something wrong?</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trust" rel="tag" title="see questions tagged &#39;trust&#39;">trust</span> <span class="post-tag tag-link-center" rel="tag" title="see questions tagged &#39;center&#39;">center</span> <span class="post-tag tag-link-key" rel="tag" title="see questions tagged &#39;key&#39;">key</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '11, 14:18</strong></p><img src="https://secure.gravatar.com/avatar/30249592feb07f6b1bf11009e57d520f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ChooseScreenName&#39;s gravatar image" /><p><span>ChooseScreen...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ChooseScreenName has no accepted answers">0%</span></p></div></div><div id="comments-container-1624" class="comments-container"><span id="1630"></span><div id="comment-1630" class="comment"><div id="post-1630-score" class="comment-score"></div><div class="comment-text"><p>You don't say which OS and which version of Wireshark you're using. One simple possibility is that you don't have a version compiled with libcrypt.</p></div><div id="comment-1630-info" class="comment-info"><span class="comment-age">(04 Jan '11, 18:07)</span> <span class="comment-user userinfo">beroset</span></div></div></div><div id="comment-tools-1624" class="comment-tools"></div><div class="clear"></div><div id="comment-1624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38103"></span>

<div id="answer-container-38103" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38103-score" class="post-score" title="current number of votes">0</div><span id="post-38103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go back to "Edit-&gt;Preferences-&gt;Protocols-&gt;ZigBee NWK" and edit the network key you entered. For byte order, select "Reverse."<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Nov '14, 07:18</strong></p><img src="https://secure.gravatar.com/avatar/70b1cb4a51f3b96d75c9d1d63fe386fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ltello1529&#39;s gravatar image" /><p><span>ltello1529</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ltello1529 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-38103" class="comments-container"></div><div id="comment-tools-38103" class="comment-tools"></div><div class="clear"></div><div id="comment-38103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

