+++
type = "question"
title = "Where can I download version 2.0 beta?"
description = '''Hi, I got the option to install Wireshark 2.0 beta after I installed the regular one a few versions ago. I think I must have inadvertently removed it after a recent upgrade. Now I can&#x27;t find the link to it anywhere. I have tried uninstalling and reinstalling the regular version in hope that it would...'''
date = "2015-06-03T12:46:00Z"
lastmod = "2015-06-03T12:54:00Z"
weight = 42856
keywords = [ "beta", "version", "2.0" ]
aliases = [ "/questions/42856" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Where can I download version 2.0 beta?](/questions/42856/where-can-i-download-version-20-beta)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42856-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42856-score" class="post-score" title="current number of votes">0</div><span id="post-42856-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I got the option to install Wireshark 2.0 beta after I installed the regular one a few versions ago.</p><p>I think I must have inadvertently removed it after a recent upgrade. Now I can't find the link to it anywhere.</p><p>I have tried uninstalling and reinstalling the regular version in hope that it would give me the option to install the beta again but that did not work.</p><p>Could anyone provide me with a direct link to it?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-beta" rel="tag" title="see questions tagged &#39;beta&#39;">beta</span> <span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span> <span class="post-tag tag-link-2.0" rel="tag" title="see questions tagged &#39;2.0&#39;">2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '15, 12:46</strong></p><img src="https://secure.gravatar.com/avatar/90f78f2902c6f3d883c57273cd92ef67?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LennyS&#39;s gravatar image" /><p><span>LennyS</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LennyS has no accepted answers">0%</span></p></div></div><div id="comments-container-42856" class="comments-container"></div><div id="comment-tools-42856" class="comment-tools"></div><div class="clear"></div><div id="comment-42856-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42857"></span>

<div id="answer-container-42857" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42857-score" class="post-score" title="current number of votes">2</div><span id="post-42857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="LennyS has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's called 1.99, and you can get it on the normal Wireshark download page: <a href="https://www.wireshark.org/download.html">https://www.wireshark.org/download.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '15, 12:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-42857" class="comments-container"></div><div id="comment-tools-42857" class="comment-tools"></div><div class="clear"></div><div id="comment-42857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

