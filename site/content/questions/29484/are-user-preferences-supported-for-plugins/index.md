+++
type = "question"
title = "Are user preferences supported for plugins?"
description = '''I have written a simple dissector plugin and now I would like to make port ranges configurable for the user. I have not found how to do this. Is it possible to have user preferences for a dissector plugin? /Michael'''
date = "2014-02-06T03:29:00Z"
lastmod = "2014-02-06T05:11:00Z"
weight = 29484
keywords = [ "preferences", "plugin" ]
aliases = [ "/questions/29484" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Are user preferences supported for plugins?](/questions/29484/are-user-preferences-supported-for-plugins)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29484-score" class="post-score" title="current number of votes">0</div><span id="post-29484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have written a simple dissector plugin and now I would like to make port ranges configurable for the user. I have not found how to do this.</p><p>Is it possible to have user preferences for a dissector plugin?</p><p>/Michael</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '14, 03:29</strong></p><img src="https://secure.gravatar.com/avatar/46d5991705edf34099201995c5ea5ed3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="michla&#39;s gravatar image" /><p><span>michla</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="michla has no accepted answers">0%</span></p></div></div><div id="comments-container-29484" class="comments-container"></div><div id="comment-tools-29484" class="comment-tools"></div><div class="clear"></div><div id="comment-29484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29485"></span>

<div id="answer-container-29485" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29485-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29485-score" class="post-score" title="current number of votes">2</div><span id="post-29485-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, a number of the "standard" plugins do this, e.g. gryphon, mate, opcua, unistim, so you can look at what they do. Opcua is probably closest to what you want. I don't think preferences are any different for plugins as opposed to built-in dissectors.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '14, 03:42</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Feb '14, 03:43</strong> </span></p></div></div><div id="comments-container-29485" class="comments-container"><span id="29486"></span><div id="comment-29486" class="comment"><div id="post-29486-score" class="comment-score"></div><div class="comment-text"><p>Thanx, it's exactly what I need</p></div><div id="comment-29486-info" class="comment-info"><span class="comment-age">(06 Feb '14, 04:08)</span> <span class="comment-user userinfo">michla</span></div></div><span id="29489"></span><div id="comment-29489" class="comment"><div id="post-29489-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-29489-info" class="comment-info"><span class="comment-age">(06 Feb '14, 05:11)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-29485" class="comment-tools"></div><div class="clear"></div><div id="comment-29485-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

