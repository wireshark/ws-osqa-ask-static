+++
type = "question"
title = "About Chinese Language Installation"
description = '''I hope to have Chinese installation package'''
date = "2013-09-10T00:53:00Z"
lastmod = "2013-09-10T06:35:00Z"
weight = 24511
keywords = [ "languages", "chinese" ]
aliases = [ "/questions/24511" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [About Chinese Language Installation](/questions/24511/about-chinese-language-installation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24511-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24511-score" class="post-score" title="current number of votes">0</div><span id="post-24511-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I hope to have Chinese installation package</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-languages" rel="tag" title="see questions tagged &#39;languages&#39;">languages</span> <span class="post-tag tag-link-chinese" rel="tag" title="see questions tagged &#39;chinese&#39;">chinese</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '13, 00:53</strong></p><img src="https://secure.gravatar.com/avatar/dfa4d8f5192a1dd54ea8ff752f40e00a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sniperhalo&#39;s gravatar image" /><p><span>sniperhalo</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sniperhalo has no accepted answers">0%</span></p></div></div><div id="comments-container-24511" class="comments-container"></div><div id="comment-tools-24511" class="comment-tools"></div><div class="clear"></div><div id="comment-24511-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24523"></span>

<div id="answer-container-24523" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24523-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24523-score" class="post-score" title="current number of votes">1</div><span id="post-24523-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is only an english version of Wireshark. Please read the following article for an explanation.</p><blockquote><p><a href="http://wiki.wireshark.org/Development/Translations">http://wiki.wireshark.org/Development/Translations</a><br />
</p></blockquote><p>or in Chinese:</p><blockquote><p><a href="http://goo.gl/cBA7Ft">http://goo.gl/cBA7Ft</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Sep '13, 06:35</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Sep '13, 07:08</strong> </span></p></div></div><div id="comments-container-24523" class="comments-container"></div><div id="comment-tools-24523" class="comment-tools"></div><div class="clear"></div><div id="comment-24523-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

