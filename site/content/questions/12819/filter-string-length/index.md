+++
type = "question"
title = "Filter string length"
description = '''Hello, anyone knows if there&#x27;s a maximum length for a display filter string? Thanks Lucio'''
date = "2012-07-18T01:41:00Z"
lastmod = "2012-07-18T05:26:00Z"
weight = 12819
keywords = [ "filtering" ]
aliases = [ "/questions/12819" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter string length](/questions/12819/filter-string-length)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12819-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12819-score" class="post-score" title="current number of votes">0</div><span id="post-12819-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>anyone knows if there's a maximum length for a display filter string?</p><p>Thanks Lucio</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filtering" rel="tag" title="see questions tagged &#39;filtering&#39;">filtering</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '12, 01:41</strong></p><img src="https://secure.gravatar.com/avatar/3badef3f7a64dd34ca4824360fdd27bf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="luxxx&#39;s gravatar image" /><p><span>luxxx</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="luxxx has no accepted answers">0%</span></p></div></div><div id="comments-container-12819" class="comments-container"></div><div id="comment-tools-12819" class="comment-tools"></div><div class="clear"></div><div id="comment-12819-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12820"></span>

<div id="answer-container-12820" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12820-score" class="post-score" title="current number of votes">1</div><span id="post-12820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Without looking at the code, I just tried to fill the filter field and was able to enter a maximum of 65535 characters.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '12, 01:51</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-12820" class="comments-container"><span id="12823"></span><div id="comment-12823" class="comment"><div id="post-12823-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot</p></div><div id="comment-12823-info" class="comment-info"><span class="comment-age">(18 Jul '12, 05:26)</span> <span class="comment-user userinfo">luxxx</span></div></div></div><div id="comment-tools-12820" class="comment-tools"></div><div class="clear"></div><div id="comment-12820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

