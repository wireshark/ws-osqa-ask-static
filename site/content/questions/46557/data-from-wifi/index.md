+++
type = "question"
title = "Data from wifi"
description = '''Hi There  Could wireshark help me get information from my home WIFI with my devices such as Iphone, Ipad and so on ?  I need to track some data running from an app on an iphone. Thanks for a quick response..  /Claus'''
date = "2015-10-15T02:47:00Z"
lastmod = "2015-10-15T05:38:00Z"
weight = 46557
keywords = [ "app", "data", "iphone" ]
aliases = [ "/questions/46557" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Data from wifi](/questions/46557/data-from-wifi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46557-score" class="post-score" title="current number of votes">0</div><span id="post-46557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi There</p><p>Could wireshark help me get information from my home WIFI with my devices such as Iphone, Ipad and so on ?</p><p>I need to track some data running from an app on an iphone.</p><p>Thanks for a quick response..</p><p>/Claus</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-app" rel="tag" title="see questions tagged &#39;app&#39;">app</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '15, 02:47</strong></p><img src="https://secure.gravatar.com/avatar/418b24424a6e042a486b6bcfdbcb218f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Claus&#39;s gravatar image" /><p><span>Claus</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Claus has no accepted answers">0%</span></p></div></div><div id="comments-container-46557" class="comments-container"></div><div id="comment-tools-46557" class="comment-tools"></div><div class="clear"></div><div id="comment-46557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46559"></span>

<div id="answer-container-46559" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46559-score" class="post-score" title="current number of votes">0</div><span id="post-46559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes. See the Wireshark wiki:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '15, 03:19</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-46559" class="comments-container"><span id="46560"></span><div id="comment-46560" class="comment"><div id="post-46560-score" class="comment-score"></div><div class="comment-text"><p>Thanks Amato_C I´m a beginner in this. Will it still work with the free licensed version ?</p></div><div id="comment-46560-info" class="comment-info"><span class="comment-age">(15 Oct '15, 03:29)</span> <span class="comment-user userinfo">Claus</span></div></div><span id="46563"></span><div id="comment-46563" class="comment"><div id="post-46563-score" class="comment-score"></div><div class="comment-text"><p>There is no other license than the "free" one, as Wireshark is an open source project.</p></div><div id="comment-46563-info" class="comment-info"><span class="comment-age">(15 Oct '15, 05:38)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-46559" class="comment-tools"></div><div class="clear"></div><div id="comment-46559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

