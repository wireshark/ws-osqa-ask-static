+++
type = "question"
title = "HASH from EAPOL"
description = '''I have captured all 4 pieces of the WPA handshake ... How do I extract the HASH from here. I can see all 4 pieces when I search EAPOL in the dump file.. Were do I go for here. Thanks'''
date = "2011-01-04T23:51:00Z"
lastmod = "2011-01-04T23:51:00Z"
weight = 1631
keywords = [ "eapol" ]
aliases = [ "/questions/1631" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HASH from EAPOL](/questions/1631/hash-from-eapol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1631-score" class="post-score" title="current number of votes">0</div><span id="post-1631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured all 4 pieces of the WPA handshake ... How do I extract the HASH from here. I can see all 4 pieces when I search EAPOL in the dump file.. Were do I go for here. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '11, 23:51</strong></p><img src="https://secure.gravatar.com/avatar/8dff4a693f3eac5f735b0e41095d2b2b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mbfd560&#39;s gravatar image" /><p><span>mbfd560</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mbfd560 has no accepted answers">0%</span></p></div></div><div id="comments-container-1631" class="comments-container"></div><div id="comment-tools-1631" class="comment-tools"></div><div class="clear"></div><div id="comment-1631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

