+++
type = "question"
title = "Developers Guide for 1.12.x"
description = '''Hi, is it still possible to get the developers guide for the 1.12.x versions? I need to compile the SAP Dissector plugin for them that doesn&#x27;t seem to work under 2.x. (to be honest I am not entirely 100% convinced it will run under 1.12, but...) If not online then a simple text file walkthough will ...'''
date = "2016-02-18T01:20:00Z"
lastmod = "2016-02-18T02:07:00Z"
weight = 50298
keywords = [ "development", "sap" ]
aliases = [ "/questions/50298" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Developers Guide for 1.12.x](/questions/50298/developers-guide-for-112x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50298-score" class="post-score" title="current number of votes">0</div><span id="post-50298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>is it still possible to get the developers guide for the 1.12.x versions? I need to compile the SAP Dissector plugin for them that doesn't seem to work under 2.x. (to be honest I am not entirely 100% convinced it will run under 1.12, but...) If not online then a simple text file walkthough will be fine.</p><p>Windows 64 - Wireshark 1.12.x - Source code 1.12.9 is downloaded</p><p>I dont really care about 64 or 32 bit Wireshark , in fact I think 32bit would probably be the safer bet...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-sap" rel="tag" title="see questions tagged &#39;sap&#39;">sap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '16, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/05ba95262a3352e3af4ba69c0ec0dff2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DarrenWright&#39;s gravatar image" /><p><span>DarrenWright</span><br />
<span class="score" title="216 reputation points">216</span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DarrenWright has 5 accepted answers">26%</span></p></div></div><div id="comments-container-50298" class="comments-container"></div><div id="comment-tools-50298" class="comment-tools"></div><div class="clear"></div><div id="comment-50298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50300"></span>

<div id="answer-container-50300" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50300-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50300-score" class="post-score" title="current number of votes">1</div><span id="post-50300-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="DarrenWright has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I do not know whether the developer's guide for 1.12 is still available somewhere online, but if your downloaded Wireshark 1.12.9 source code you will find the ASCIIDOC source here: docbook/wsdg_src</p><p>You are probably interested by WSDG_chapter_quick_setup.asciidoc content</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Feb '16, 01:56</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-50300" class="comments-container"><span id="50301"></span><div id="comment-50301" class="comment"><div id="post-50301-score" class="comment-score"></div><div class="comment-text"><p>you the man.</p></div><div id="comment-50301-info" class="comment-info"><span class="comment-age">(18 Feb '16, 02:07)</span> <span class="comment-user userinfo">DarrenWright</span></div></div></div><div id="comment-tools-50300" class="comment-tools"></div><div class="clear"></div><div id="comment-50300-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

