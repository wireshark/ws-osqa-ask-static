+++
type = "question"
title = "How give the csv file to tshark?"
description = '''I have a csv file. I&#x27;m going to give it to tshark. How can i do it?'''
date = "2014-11-05T17:56:00Z"
lastmod = "2014-11-06T05:50:00Z"
weight = 37598
keywords = [ "csv" ]
aliases = [ "/questions/37598" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How give the csv file to tshark?](/questions/37598/how-give-the-csv-file-to-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37598-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37598-score" class="post-score" title="current number of votes">0</div><span id="post-37598-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a csv file. I'm going to give it to tshark. How can i do it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-csv" rel="tag" title="see questions tagged &#39;csv&#39;">csv</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 17:56</strong></p><img src="https://secure.gravatar.com/avatar/6bb9a7105c59980d5b8a4945ccec4b7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zohre&#39;s gravatar image" /><p><span>zohre</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zohre has no accepted answers">0%</span></p></div></div><div id="comments-container-37598" class="comments-container"><span id="37613"></span><div id="comment-37613" class="comment"><div id="post-37613-score" class="comment-score"></div><div class="comment-text"><p>What is the content of that CSV file and what do you expect tshark to do with that CSV file?</p></div><div id="comment-37613-info" class="comment-info"><span class="comment-age">(06 Nov '14, 04:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37598" class="comment-tools"></div><div class="clear"></div><div id="comment-37598-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37617"></span>

<div id="answer-container-37617" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37617-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37617-score" class="post-score" title="current number of votes">0</div><span id="post-37617-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Short answer: you don't.</p><p>Slightly longer answer: first you do like you suggested in your <a href="https://ask.wireshark.org/questions/37578/convert-csv-to-pcap">previous question</a> and find a (magic?) way to convert a CSV file into a PCAP file. Then you feed the PCAP file into tshark.</p><p>In other words, CSV just isn't a format Wireshark or any of its utilities will understand.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Nov '14, 05:50</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37617" class="comments-container"></div><div id="comment-tools-37617" class="comment-tools"></div><div class="clear"></div><div id="comment-37617-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

