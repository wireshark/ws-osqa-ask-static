+++
type = "question"
title = "BICC and H.248 filter for a single call"
description = '''Hi, I have a .pcap files with BSSAP, H.248 and BICC calls. Now I can filter out single BICC call by CIC number , but wondering if there is any way to see the corresponding H.248 messages. Please advice. Best Regards Suman'''
date = "2013-01-18T23:13:00Z"
lastmod = "2013-01-19T03:09:00Z"
weight = 17787
keywords = [ "filter", "a", "single", "call" ]
aliases = [ "/questions/17787" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [BICC and H.248 filter for a single call](/questions/17787/bicc-and-h248-filter-for-a-single-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17787-score" class="post-score" title="current number of votes">0</div><span id="post-17787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have a .pcap files with BSSAP, H.248 and BICC calls. Now I can filter out single BICC call by CIC number , but wondering if there is any way to see the corresponding H.248 messages.</p><p>Please advice.</p><p>Best Regards Suman</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-a" rel="tag" title="see questions tagged &#39;a&#39;">a</span> <span class="post-tag tag-link-single" rel="tag" title="see questions tagged &#39;single&#39;">single</span> <span class="post-tag tag-link-call" rel="tag" title="see questions tagged &#39;call&#39;">call</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '13, 23:13</strong></p><img src="https://secure.gravatar.com/avatar/321e5b55d5224c756dc7278df171e149?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="suman&#39;s gravatar image" /><p><span>suman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="suman has no accepted answers">0%</span></p></div></div><div id="comments-container-17787" class="comments-container"></div><div id="comment-tools-17787" class="comment-tools"></div><div class="clear"></div><div id="comment-17787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17788"></span>

<div id="answer-container-17788" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17788-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17788-score" class="post-score" title="current number of votes">0</div><span id="post-17788-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, Not directly, if you know the relation CIC - Context Id you can use that for H.248.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '13, 03:09</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-17788" class="comments-container"></div><div id="comment-tools-17788" class="comment-tools"></div><div class="clear"></div><div id="comment-17788-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

