+++
type = "question"
title = "wireshark says &#x27;there are no interfaces on which a capture can be done&#x27;"
description = '''When I choose Capture..Interfaces, I get this message - &#x27;there are no interfaces on which a capture can be done&#x27; [more text below this pic]  I am on knoppix usb, using WLAN so I have an interface I see it with an ip when I do ifconfig wlan0 I then plug my LAN cable in, and I get an IP address for et...'''
date = "2014-05-29T00:00:00Z"
lastmod = "2014-05-29T00:21:00Z"
weight = 33149
keywords = [ "error" ]
aliases = [ "/questions/33149" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark says 'there are no interfaces on which a capture can be done'](/questions/33149/wireshark-says-there-are-no-interfaces-on-which-a-capture-can-be-done)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33149-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33149-score" class="post-score" title="current number of votes">0</div><span id="post-33149-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I choose Capture..Interfaces, I get this message - 'there are no interfaces on which a capture can be done'</p><p>[more text below this pic] <img src="https://osqa-ask.wireshark.org/upfiles/Screenshot_from_2014-05-29_03:21:07.png" alt="alt text" /></p><p>I am on knoppix usb, using WLAN so I have an interface I see it with an ip when I do ifconfig wlan0 I then plug my LAN cable in, and I get an IP address for eth0 which I see with ifconfig</p><p>but I still get that error message.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '14, 00:00</strong></p><img src="https://secure.gravatar.com/avatar/4ca1e8011745413142c9056875b8d39c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="barlop&#39;s gravatar image" /><p><span>barlop</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="barlop has no accepted answers">0%</span></p></img></div></div><div id="comments-container-33149" class="comments-container"></div><div id="comment-tools-33149" class="comment-tools"></div><div class="clear"></div><div id="comment-33149-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33150"></span>

<div id="answer-container-33150" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33150-score" class="post-score" title="current number of votes">0</div><span id="post-33150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi this will be a linux permission thing. Have a read of <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">http://wiki.wireshark.org/CaptureSetup/CapturePrivileges</a> looking at the Linux section. The quick-and-dirty was is to just run "sudo wireshark" which is reasonably safe from a Knoppix type of distro.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '14, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-33150" class="comments-container"></div><div id="comment-tools-33150" class="comment-tools"></div><div class="clear"></div><div id="comment-33150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

