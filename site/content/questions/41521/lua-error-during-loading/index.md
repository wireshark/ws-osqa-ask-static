+++
type = "question"
title = "lua error during loading"
description = '''what does this error mean? Lua: Error during loading:  [string &quot;C:&#92;Program Files&#92;Wireshark1&#92;init.lua&quot;]:314: attempt to call global &#x27;datafile_path&#x27; (a nil value) I copied one .lua file into my wireshark folder.when i tried to run wireshark i got this error.'''
date = "2015-04-16T22:07:00Z"
lastmod = "2015-04-16T22:07:00Z"
weight = 41521
keywords = [ "lua", "error" ]
aliases = [ "/questions/41521" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [lua error during loading](/questions/41521/lua-error-during-loading)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41521-score" class="post-score" title="current number of votes">0</div><span id="post-41521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what does this error mean?</p><p>Lua: Error during loading: [string "C:\Program Files\Wireshark1\init.lua"]:314: attempt to call global 'datafile_path' (a nil value)</p><p>I copied one .lua file into my wireshark folder.when i tried to run wireshark i got this error.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '15, 22:07</strong></p><img src="https://secure.gravatar.com/avatar/a2e29df6af5eb33f09d1ed5321ea6586?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lakshmi&#39;s gravatar image" /><p><span>lakshmi</span><br />
<span class="score" title="16 reputation points">16</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lakshmi has no accepted answers">0%</span></p></div></div><div id="comments-container-41521" class="comments-container"></div><div id="comment-tools-41521" class="comment-tools"></div><div class="clear"></div><div id="comment-41521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

