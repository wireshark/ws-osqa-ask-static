+++
type = "question"
title = "Missing Field Names when capturing GSM CCCH"
description = '''Hello, when i am capturing a .pdml file i see a few field names are empty.  For example in the &quot;System Information Type 1&quot; &amp;gt; Cell Channel Description &amp;gt;&quot;List of ARFCNs&quot;  It shows all values of the neighbouring cells but there field name is &quot;&quot; empty  I need the name for the tshark capture filter...'''
date = "2015-11-10T10:57:00Z"
lastmod = "2015-11-10T10:57:00Z"
weight = 47477
keywords = [ "gsm_a.bcch_arfcn", "field", "gsm", "names", "arfcn" ]
aliases = [ "/questions/47477" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Missing Field Names when capturing GSM CCCH](/questions/47477/missing-field-names-when-capturing-gsm-ccch)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47477-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47477-score" class="post-score" title="current number of votes">0</div><span id="post-47477-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, when i am capturing a .pdml file i see a few field names are empty. For example in the "System Information Type 1" &gt; Cell Channel Description &gt;"List of ARFCNs" It shows all values of the neighbouring cells but there field name is "" empty</p><p>I need the name for the tshark capture filter. (to generate a .csv file) So i tried field names that i found in the wireshark documentation and on other webpages: like... gsm_a.rr.arfcn_list, gsm_a.rr.arfcn, gsm_a.bcch_arfcn, gsmtap.arfcn</p><p>But none of them are working. I hope somebody knows where the problem is.</p><p>Alternatively i tried to use a offset filter. But i had no luck with it. Maybe somebody can help me with this offset filter if that is the way to go in this case.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm_a.bcch_arfcn" rel="tag" title="see questions tagged &#39;gsm_a.bcch_arfcn&#39;">gsm_a.bcch_arfcn</span> <span class="post-tag tag-link-field" rel="tag" title="see questions tagged &#39;field&#39;">field</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-names" rel="tag" title="see questions tagged &#39;names&#39;">names</span> <span class="post-tag tag-link-arfcn" rel="tag" title="see questions tagged &#39;arfcn&#39;">arfcn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '15, 10:57</strong></p><img src="https://secure.gravatar.com/avatar/ad607f73f65457ba3dddb9244cb575d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mpr&#39;s gravatar image" /><p><span>mpr</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mpr has no accepted answers">0%</span></p></div></div><div id="comments-container-47477" class="comments-container"></div><div id="comment-tools-47477" class="comment-tools"></div><div class="clear"></div><div id="comment-47477-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

