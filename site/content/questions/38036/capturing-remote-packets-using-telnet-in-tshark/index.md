+++
type = "question"
title = "Capturing remote packets using telnet in tshark."
description = '''Hi I am new to wireshark and to this community, using wireshark (V 1.12.2) in Windows 7. How can I capture packets from a any particular remote host (particular IP) in my network using telnet in tshark? I will specifically mention the remote IP and tshark should capture traffic to and from that host...'''
date = "2014-11-21T02:21:00Z"
lastmod = "2014-11-21T02:21:00Z"
weight = 38036
keywords = [ "remote-capture", "packets", "tshark", "telnet" ]
aliases = [ "/questions/38036" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing remote packets using telnet in tshark.](/questions/38036/capturing-remote-packets-using-telnet-in-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38036-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38036-score" class="post-score" title="current number of votes">0</div><span id="post-38036-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am new to wireshark and to this community, using wireshark (V 1.12.2) in Windows 7. How can I capture packets from a any particular remote host (particular IP) in my network using telnet in tshark? I will specifically mention the remote IP and tshark should capture traffic to and from that host. Kindly help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-telnet" rel="tag" title="see questions tagged &#39;telnet&#39;">telnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '14, 02:21</strong></p><img src="https://secure.gravatar.com/avatar/c3a35ecbeed3bedf75564149337b1802?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deb_26&#39;s gravatar image" /><p><span>Deb_26</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deb_26 has no accepted answers">0%</span></p></div></div><div id="comments-container-38036" class="comments-container"></div><div id="comment-tools-38036" class="comment-tools"></div><div class="clear"></div><div id="comment-38036-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

