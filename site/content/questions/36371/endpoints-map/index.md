+++
type = "question"
title = "Endpoints map"
description = '''In my Statistics | Endpoints | IP tab, I have two columns for my geoip info. One set is filled in and one is not. When I click the map button, I get the circles for where my traffic is based but the pop-ups are not filled in. The geoip works perfect in my decode where I need it the most so this is o...'''
date = "2014-09-16T11:07:00Z"
lastmod = "2014-09-17T08:46:00Z"
weight = 36371
keywords = [ "geoip", "endpoints", "map" ]
aliases = [ "/questions/36371" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Endpoints map](/questions/36371/endpoints-map)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36371-score" class="post-score" title="current number of votes">0</div><span id="post-36371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In my Statistics | Endpoints | IP tab, I have two columns for my geoip info. One set is filled in and one is not. When I click the map button, I get the circles for where my traffic is based but the pop-ups are not filled in. The geoip works perfect in my decode where I need it the most so this is only an annoyance, but it bugs me when I demo in class.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span> <span class="post-tag tag-link-endpoints" rel="tag" title="see questions tagged &#39;endpoints&#39;">endpoints</span> <span class="post-tag tag-link-map" rel="tag" title="see questions tagged &#39;map&#39;">map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '14, 11:07</strong></p><img src="https://secure.gravatar.com/avatar/1521e6e24704b2876c893b4721e1f19d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bettyland&#39;s gravatar image" /><p><span>bettyland</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bettyland has no accepted answers">0%</span></p></div></div><div id="comments-container-36371" class="comments-container"></div><div id="comment-tools-36371" class="comment-tools"></div><div class="clear"></div><div id="comment-36371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36380"></span>

<div id="answer-container-36380" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36380-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36380-score" class="post-score" title="current number of votes">0</div><span id="post-36380-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Right, I have two AS number columns (Wireshark 1.12.1), which seems kind odd. But the dots in the map do show ballon popups when clicked - whenever I had trouble with the GeoIP map in my browser it was because I blocked some scripts (NoScript) or ads. Turning the blockers off usually did the trick.</p><p>What Wireshark Version do you use, and which browser on what OS? Maybe someone with the same settings can verify...</p><p>P.S: good to have you here, Betty! ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '14, 15:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-36380" class="comments-container"><span id="36399"></span><div id="comment-36399" class="comment"><div id="post-36399-score" class="comment-score"></div><div class="comment-text"><p>My Wireshark version is 1.10.9, and it happens in chrome, IE and firefox. The balloons show up when I click on the dots, they are just not populated with data. Very annoying considering my endpoints IPv4 tab is populated with city, country, latitude and longitude - everything it needs.</p><p>I've been a user here since the beginning. I've just never felt smart enough to answer a question :)</p></div><div id="comment-36399-info" class="comment-info"><span class="comment-age">(17 Sep '14, 05:03)</span> <span class="comment-user userinfo">bettyland</span></div></div><span id="36410"></span><div id="comment-36410" class="comment"><div id="post-36410-score" class="comment-score"></div><div class="comment-text"><p>I'm using Wireshark 1.10.9 with Chrome. When I click the dots, the balloon shows the IP address, number of packets, and number of bytes. AS, Country, and City are blank.</p></div><div id="comment-36410-info" class="comment-info"><span class="comment-age">(17 Sep '14, 08:46)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div></div><div id="comment-tools-36380" class="comment-tools"></div><div class="clear"></div><div id="comment-36380-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

