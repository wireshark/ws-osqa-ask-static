+++
type = "question"
title = "Wireshark 1.8.2 won&#x27;t start on Mac OS 10.6.8"
description = '''I just downloaded Wireshark 1.8.2. When I try to run it, it quits right after starting. The following is in the crash log: Dyld Error Message:  Library not loaded: /usr/X11/lib/libpng12.0.dylib  Referenced from: /Applications/Wireshark.app/Contents/Resources/bin/wireshark-bin  Reason: Incompatible l...'''
date = "2012-09-12T20:33:00Z"
lastmod = "2012-09-30T02:43:00Z"
weight = 14224
keywords = [ "mac", "10.6.8" ]
aliases = [ "/questions/14224" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.8.2 won't start on Mac OS 10.6.8](/questions/14224/wireshark-182-wont-start-on-mac-os-1068)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14224-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14224-score" class="post-score" title="current number of votes">0</div><span id="post-14224-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just downloaded Wireshark 1.8.2. When I try to run it, it quits right after starting. The following is in the crash log:</p><p>Dyld Error Message: Library not loaded: /usr/X11/lib/libpng12.0.dylib Referenced from: /Applications/Wireshark.app/Contents/Resources/bin/wireshark-bin Reason: Incompatible library version: wireshark-bin requires version 47.0.0 or later, but libpng12.0.dylib provides version 45.0.0</p><p>I appear to have the latest X11 (2.7.3, also just downloaded), so I'm not sure what the best thing to do about this is.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-10.6.8" rel="tag" title="see questions tagged &#39;10.6.8&#39;">10.6.8</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '12, 20:33</strong></p><img src="https://secure.gravatar.com/avatar/54b16a9fbfef951281d3fb08391c7921?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Doug%20DeMoss&#39;s gravatar image" /><p><span>Doug DeMoss</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Doug DeMoss has no accepted answers">0%</span></p></div></div><div id="comments-container-14224" class="comments-container"><span id="14613"></span><div id="comment-14613" class="comment"><div id="post-14613-score" class="comment-score"></div><div class="comment-text"><p>Hi, i have the same problem on a 10.6.1, same error message...have you found a solution yet, am still trying to find a way to make it work.</p></div><div id="comment-14613-info" class="comment-info"><span class="comment-age">(30 Sep '12, 02:43)</span> <span class="comment-user userinfo">Squid</span></div></div></div><div id="comment-tools-14224" class="comment-tools"></div><div class="clear"></div><div id="comment-14224-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

