+++
type = "question"
title = "Packet capture samples repository or cataloger"
description = '''Topic says it...I&#x27;m wanting to find an open source packet capture sample repository or storage. One that I can use locally to store things like how machine a talk to machine b would be awesome. Thanks for any advice.'''
date = "2015-03-13T12:24:00Z"
lastmod = "2015-03-13T12:24:00Z"
weight = 40541
keywords = [ "catalog" ]
aliases = [ "/questions/40541" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Packet capture samples repository or cataloger](/questions/40541/packet-capture-samples-repository-or-cataloger)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40541-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40541-score" class="post-score" title="current number of votes">0</div><span id="post-40541-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Topic says it...I'm wanting to find an open source packet capture sample repository or storage. One that I can use locally to store things like how machine a talk to machine b would be awesome. Thanks for any advice.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-catalog" rel="tag" title="see questions tagged &#39;catalog&#39;">catalog</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '15, 12:24</strong></p><img src="https://secure.gravatar.com/avatar/feeceb13b3a434a147fa2c173ad18db8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DigiAngelXX&#39;s gravatar image" /><p><span>DigiAngelXX</span><br />
<span class="score" title="21 reputation points">21</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DigiAngelXX has no accepted answers">0%</span></p></div></div><div id="comments-container-40541" class="comments-container"></div><div id="comment-tools-40541" class="comment-tools"></div><div class="clear"></div><div id="comment-40541-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

