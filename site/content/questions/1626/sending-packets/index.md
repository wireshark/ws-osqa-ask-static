+++
type = "question"
title = "Sending packets"
description = '''Is it possible to send packets into network by using wireshark?  I want to send packet files(ex: .pcap) from one user to another within a network. '''
date = "2011-01-04T15:09:00Z"
lastmod = "2011-01-04T15:37:00Z"
weight = 1626
keywords = [ "sending", "packet" ]
aliases = [ "/questions/1626" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sending packets](/questions/1626/sending-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1626-score" class="post-score" title="current number of votes">0</div><span id="post-1626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to send packets into network by using wireshark?</p><ul><li>I want to send packet files(ex: .pcap) from one user to another within a network.</li></ul></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sending" rel="tag" title="see questions tagged &#39;sending&#39;">sending</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '11, 15:09</strong></p><img src="https://secure.gravatar.com/avatar/c9ca3397c3560a1abd48351e776697c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rocky&#39;s gravatar image" /><p><span>rocky</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rocky has no accepted answers">0%</span></p></div></div><div id="comments-container-1626" class="comments-container"></div><div id="comment-tools-1626" class="comment-tools"></div><div class="clear"></div><div id="comment-1626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1627"></span>

<div id="answer-container-1627" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1627-score" class="post-score" title="current number of votes">0</div><span id="post-1627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Simple answer, no.</p><p>You might want to look at programs like bit-twist or tcpreplay to do what you want.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '11, 15:37</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-1627" class="comments-container"></div><div id="comment-tools-1627" class="comment-tools"></div><div class="clear"></div><div id="comment-1627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

