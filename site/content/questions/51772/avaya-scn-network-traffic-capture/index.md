+++
type = "question"
title = "[closed] Avaya SCN network traffic capture"
description = '''I am new to WS. I am having difficulty with connecting 2 sites over the network. I am told by the IT staff that the VPN is not blocking any port and that the firewall is not either. The two systems will not form a connection. I need to find out if port 70595 is passing traffic, as this is the keep a...'''
date = "2016-04-18T20:48:00Z"
lastmod = "2016-04-19T05:08:00Z"
weight = 51772
keywords = [ "scn", "avaya" ]
aliases = [ "/questions/51772" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Avaya SCN network traffic capture](/questions/51772/avaya-scn-network-traffic-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51772-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51772-score" class="post-score" title="current number of votes">0</div><span id="post-51772-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to WS. I am having difficulty with connecting 2 sites over the network. I am told by the IT staff that the VPN is not blocking any port and that the firewall is not either. The two systems will not form a connection. I need to find out if port 70595 is passing traffic, as this is the keep alive message from system to system.Any guidance to accomplish this would be great. Again, I have only captured test packets and not much else.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-scn" rel="tag" title="see questions tagged &#39;scn&#39;">scn</span> <span class="post-tag tag-link-avaya" rel="tag" title="see questions tagged &#39;avaya&#39;">avaya</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '16, 20:48</strong></p><img src="https://secure.gravatar.com/avatar/df69b5d64357447524bf749433971e3c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PhoneGuy27&#39;s gravatar image" /><p><span>PhoneGuy27</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PhoneGuy27 has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Apr '16, 05:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-51772" class="comments-container"></div><div id="comment-tools-51772" class="comment-tools"></div><div class="clear"></div><div id="comment-51772-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 19 Apr '16, 05:08

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51777"></span>

<div id="answer-container-51777" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51777-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51777-score" class="post-score" title="current number of votes">0</div><span id="post-51777-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds more like a <a href="http://networkengineering.stackexchange.com/">Network Engineering</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '16, 05:08</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-51777" class="comments-container"></div><div id="comment-tools-51777" class="comment-tools"></div><div class="clear"></div><div id="comment-51777-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

