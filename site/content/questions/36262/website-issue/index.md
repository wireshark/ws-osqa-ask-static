+++
type = "question"
title = "website issue."
description = '''users at one of our office were not able to open particular url and they receive error &quot;website not found&quot;. i have attached caputure to this question.'''
date = "2014-09-12T05:02:00Z"
lastmod = "2014-10-23T02:44:00Z"
weight = 36262
keywords = [ "webpage" ]
aliases = [ "/questions/36262" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [website issue.](/questions/36262/website-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36262-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36262-score" class="post-score" title="current number of votes">0</div><span id="post-36262-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/ecf_cap.JPG" alt="alt text" />users at one of our office were not able to open particular url and they receive error "website not found".</p><p>i have attached caputure to this question.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-webpage" rel="tag" title="see questions tagged &#39;webpage&#39;">webpage</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '14, 05:02</strong></p><img src="https://secure.gravatar.com/avatar/8bbc1ee2b230a283a07ed85c0a7f8819?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kalyan&#39;s gravatar image" /><p><span>kalyan</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kalyan has no accepted answers">0%</span></p></img></div></div><div id="comments-container-36262" class="comments-container"></div><div id="comment-tools-36262" class="comment-tools"></div><div class="clear"></div><div id="comment-36262-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37305"></span>

<div id="answer-container-37305" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37305-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37305-score" class="post-score" title="current number of votes">0</div><span id="post-37305-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The client (PC) is trying to access an encrypted (https) web site. The client then starts the encrypted session, the server responds, and the client immediately terminates the session.</p><p>Clearly there's something in the server's reply that the client does not like. It seems strange that the response is "website not found." You might try different browsers to see if you can get a more precise error message which would help explain the problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '14, 02:44</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37305" class="comments-container"></div><div id="comment-tools-37305" class="comment-tools"></div><div class="clear"></div><div id="comment-37305-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

