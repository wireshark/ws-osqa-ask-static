+++
type = "question"
title = "Could you give me a *.pcap that have protocols(dct2000:atm:fp:MAC:RLC:RRC.)？"
description = '''Could you give me a *.pcap that have these protocols(dct2000:atm:fp:MAC:RLC:RRC) in？ Protocols in frame: dct2000:atm:fp:MAC:RLC:RRC.'''
date = "2012-07-03T01:08:00Z"
lastmod = "2012-07-03T01:08:00Z"
weight = 12387
keywords = [ "fp", "dct2000", "rlc" ]
aliases = [ "/questions/12387" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Could you give me a \*.pcap that have protocols(dct2000:atm:fp:MAC:RLC:RRC.)？](/questions/12387/could-you-give-me-a-pcap-that-have-protocolsdct2000atmfpmacrlcrrc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12387-score" class="post-score" title="current number of votes">0</div><span id="post-12387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Could you give me a *.pcap that have these protocols(dct2000:atm:fp:MAC:RLC:RRC) in？ Protocols in frame: dct2000:atm:fp:MAC:RLC:RRC.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span> <span class="post-tag tag-link-dct2000" rel="tag" title="see questions tagged &#39;dct2000&#39;">dct2000</span> <span class="post-tag tag-link-rlc" rel="tag" title="see questions tagged &#39;rlc&#39;">rlc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '12, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-12387" class="comments-container"></div><div id="comment-tools-12387" class="comment-tools"></div><div class="clear"></div><div id="comment-12387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

