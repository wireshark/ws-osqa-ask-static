+++
type = "question"
title = "Opening Wireshark - sizing"
description = '''Hi, When first initializing WS gui, is there a way to modify how large/small the &quot;box&quot; by which Wireshark opens? I&#x27;m currently using WS-1.10.1. I&#x27;ve checked preferences, but didn&#x27;t see a sizing option. thanks, J'''
date = "2013-08-05T05:59:00Z"
lastmod = "2013-08-05T07:43:00Z"
weight = 23554
keywords = [ "sizing" ]
aliases = [ "/questions/23554" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Opening Wireshark - sizing](/questions/23554/opening-wireshark-sizing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23554-score" class="post-score" title="current number of votes">0</div><span id="post-23554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When first initializing WS gui, is there a way to modify how large/small the "box" by which Wireshark opens? I'm currently using WS-1.10.1. I've checked preferences, but didn't see a sizing option.</p><p>thanks,</p><p>J</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sizing" rel="tag" title="see questions tagged &#39;sizing&#39;">sizing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '13, 05:59</strong></p><img src="https://secure.gravatar.com/avatar/791c3a844bb1629d3a685adab364e2d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JTech_17&#39;s gravatar image" /><p><span>JTech_17</span><br />
<span class="score" title="41 reputation points">41</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JTech_17 has no accepted answers">0%</span></p></div></div><div id="comments-container-23554" class="comments-container"><span id="23556"></span><div id="comment-23556" class="comment"><div id="post-23556-score" class="comment-score"></div><div class="comment-text"><p>What platform / window manager do you run?</p></div><div id="comment-23556-info" class="comment-info"><span class="comment-age">(05 Aug '13, 06:11)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="23557"></span><div id="comment-23557" class="comment"><div id="post-23557-score" class="comment-score"></div><div class="comment-text"><h1 id="is-this-the-info-you-need">Is this the info you need?</h1><p>Compiled (64-bit) with GTK+ 2.24.14, with Cairo 1.10.2, with Pango 1.30.1, with GLib 2.34.1, with WinPcap (4_1_3), with libz 1.2.5, without POSIX capabilities, without libnl, with SMI 0.4.8, with c-ares 1.9.1, with Lua 5.1, without Python, with GnuTLS 2.12.18, with Gcrypt 1.4.6, without Kerberos, with GeoIP, with PortAudio V19-devel (built Jul 26 2013), with AirPcap.</p><p>Running on 64-bit Windows 7 Service Pack 1, build 7601, with WinPcap version 4.1.3 (packet.dll version 4.1.0.2980), based on libpcap version 1.0 branch 1_0_rel0b (20091008), GnuTLS 2.12.18, Gcrypt 1.4.6, without AirPcap. Intel(R) Core(TM) i5 CPU M 540 @ 2.53GHz, with 7989MB of physical memory.</p><h1 id="built-using-microsoft-visual-c-10.0-build-4021">Built using Microsoft Visual C++ 10.0 build 4021</h1></div><div id="comment-23557-info" class="comment-info"><span class="comment-age">(05 Aug '13, 06:15)</span> <span class="comment-user userinfo">JTech_17</span></div></div></div><div id="comment-tools-23554" class="comment-tools"></div><div class="clear"></div><div id="comment-23554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23560"></span>

<div id="answer-container-23560" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23560-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23560-score" class="post-score" title="current number of votes">2</div><span id="post-23560-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>After opening wireshark and resizing the window to your liking, choose <code>Edit -&gt; Preferences -&gt; Save window size</code>.</p><p>If you'd like to also save the window position or maximized state, there are separate preferences for those.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '13, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Aug '13, 07:40</strong> </span></p></div></div><div id="comments-container-23560" class="comments-container"><span id="23561"></span><div id="comment-23561" class="comment"><div id="post-23561-score" class="comment-score"></div><div class="comment-text"><p>That did it, thanks, J</p></div><div id="comment-23561-info" class="comment-info"><span class="comment-age">(05 Aug '13, 07:43)</span> <span class="comment-user userinfo">JTech_17</span></div></div></div><div id="comment-tools-23560" class="comment-tools"></div><div class="clear"></div><div id="comment-23560-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

