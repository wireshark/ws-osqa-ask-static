+++
type = "question"
title = "Try to obtain packet&#x27;s median value per the client ip"
description = '''Hi, For my study i have to do network analysis. With omnipeek i cannot obtain the median value (i could do the average with excel if i have it). I tried with Experts -&amp;gt; clients/servers -&amp;gt; i need a packet&#x27;s median value per client addr but i can&#x27;t collect them automatically.  on this picture, i...'''
date = "2015-02-15T20:30:00Z"
lastmod = "2015-02-16T10:14:00Z"
weight = 39876
keywords = [ "median", "value", "omnipeek" ]
aliases = [ "/questions/39876" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Try to obtain packet's median value per the client ip](/questions/39876/try-to-obtain-packets-median-value-per-the-client-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39876-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39876-score" class="post-score" title="current number of votes">0</div><span id="post-39876-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>For my study i have to do network analysis.</p><p>With omnipeek i cannot obtain the median value (i could do the average with excel if i have it).</p><p>I tried with Experts -&gt; clients/servers -&gt; i need a packet's median value per client addr but i can't collect them automatically.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Q_A_ofesYMd.png" alt="alt text" /></p><p>on this picture, i expect packet's median value is '1' for this client ip, but i should obtain the median value automatically. because there are many client ID.</p><p>Can someone help me?</p><p>PS: I use the Wildpacket Omnipeek 8.1.1</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-median" rel="tag" title="see questions tagged &#39;median&#39;">median</span> <span class="post-tag tag-link-value" rel="tag" title="see questions tagged &#39;value&#39;">value</span> <span class="post-tag tag-link-omnipeek" rel="tag" title="see questions tagged &#39;omnipeek&#39;">omnipeek</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '15, 20:30</strong></p><img src="https://secure.gravatar.com/avatar/89da163c13027a6536ccdc883adead9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Soong&#39;s gravatar image" /><p><span>Soong</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Soong has no accepted answers">0%</span></p></img></div></div><div id="comments-container-39876" class="comments-container"><span id="39894"></span><div id="comment-39894" class="comment"><div id="post-39894-score" class="comment-score"></div><div class="comment-text"><p>Are you asking how to calculate that median value in Omnipeek? You know this is the <strong>Wireshark</strong> Q&amp;A site, right?</p></div><div id="comment-39894-info" class="comment-info"><span class="comment-age">(16 Feb '15, 10:14)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-39876" class="comment-tools"></div><div class="clear"></div><div id="comment-39876-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

