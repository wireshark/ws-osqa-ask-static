+++
type = "question"
title = "How to get fields from another dissector?"
description = '''Hello, I am writing a dissector to decode COTP payload data and I would like to get data from other dissectors for my decoding purposes. Specifically I would like to get clnp.type, clnp.dsap, clnp.ssap values for use in my dissector.'''
date = "2013-03-04T10:06:00Z"
lastmod = "2013-03-04T10:06:00Z"
weight = 19131
keywords = [ "dissectortable", "dissector", "parent" ]
aliases = [ "/questions/19131" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get fields from another dissector?](/questions/19131/how-to-get-fields-from-another-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19131-score" class="post-score" title="current number of votes">0</div><span id="post-19131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am writing a dissector to decode COTP payload data and I would like to get data from other dissectors for my decoding purposes. Specifically I would like to get clnp.type, clnp.dsap, clnp.ssap values for use in my dissector.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissectortable" rel="tag" title="see questions tagged &#39;dissectortable&#39;">dissectortable</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-parent" rel="tag" title="see questions tagged &#39;parent&#39;">parent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '13, 10:06</strong></p><img src="https://secure.gravatar.com/avatar/0cad3bf5213700a2712a1a782dc46970?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="atmatn&#39;s gravatar image" /><p><span>atmatn</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="atmatn has no accepted answers">0%</span></p></div></div><div id="comments-container-19131" class="comments-container"></div><div id="comment-tools-19131" class="comment-tools"></div><div class="clear"></div><div id="comment-19131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

