+++
type = "question"
title = "Can&#x27;t find an interface in order to capture..."
description = '''I am having trouble getting started on my packet capture. When I go to Capture-&amp;gt;Interfaces I get a message saying &quot;There are no interfaces on which a capture can be done.&quot; I am using Mac OS 10.6.3 and have an en1 interface. When I went to Edit-&amp;gt; Preferences-&amp;gt; Capture I typed in en1 in the &quot;...'''
date = "2010-12-02T18:50:00Z"
lastmod = "2010-12-04T14:15:00Z"
weight = 1218
keywords = [ "capture", "interfaces" ]
aliases = [ "/questions/1218" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't find an interface in order to capture...](/questions/1218/cant-find-an-interface-in-order-to-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1218-score" class="post-score" title="current number of votes">0</div><span id="post-1218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am having trouble getting started on my packet capture. When I go to Capture-&gt;Interfaces I get a message saying "There are no interfaces on which a capture can be done." I am using Mac OS 10.6.3 and have an en1 interface. When I went to Edit-&gt; Preferences-&gt; Capture I typed in en1 in the "Default interface" box and hit OK. I continue to get the same message telling me "there are no interfaces on which a capture can be done."</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '10, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/d53d185b3779a53399e4bfd21bd546ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="danefishbaugh&#39;s gravatar image" /><p><span>danefishbaugh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="danefishbaugh has no accepted answers">0%</span></p></div></div><div id="comments-container-1218" class="comments-container"></div><div id="comment-tools-1218" class="comment-tools"></div><div class="clear"></div><div id="comment-1218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1247"></span>

<div id="answer-container-1247" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1247-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1247-score" class="post-score" title="current number of votes">0</div><span id="post-1247-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look at this thread:</p><p><a href="http://ask.wireshark.org/questions/578/mac-os-can-detect-any-interface">ask.wireshark.org/questions/578/mac-os-can-detect-any-interface</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Dec '10, 14:15</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-1247" class="comments-container"></div><div id="comment-tools-1247" class="comment-tools"></div><div class="clear"></div><div id="comment-1247-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

