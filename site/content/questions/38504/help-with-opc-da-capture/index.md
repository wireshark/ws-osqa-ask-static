+++
type = "question"
title = "Help with OPC-DA capture"
description = '''I&#x27;m using OPC(DCOM) to transfer data between two software products. The source ports are 55555 and 55556. I&#x27;m unable to see this data in Wireshark. I tried filtering out dcerpc data, but to no avail.'''
date = "2014-12-09T16:16:00Z"
lastmod = "2014-12-09T16:16:00Z"
weight = 38504
keywords = [ "opc" ]
aliases = [ "/questions/38504" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help with OPC-DA capture](/questions/38504/help-with-opc-da-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38504-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38504-score" class="post-score" title="current number of votes">0</div><span id="post-38504-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using OPC(DCOM) to transfer data between two software products. The source ports are 55555 and 55556. I'm unable to see this data in Wireshark. I tried filtering out dcerpc data, but to no avail.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-opc" rel="tag" title="see questions tagged &#39;opc&#39;">opc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '14, 16:16</strong></p><img src="https://secure.gravatar.com/avatar/e9307bbad655d99e1490c59e9a296338?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mc1234&#39;s gravatar image" /><p><span>mc1234</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mc1234 has no accepted answers">0%</span></p></div></div><div id="comments-container-38504" class="comments-container"></div><div id="comment-tools-38504" class="comment-tools"></div><div class="clear"></div><div id="comment-38504-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

