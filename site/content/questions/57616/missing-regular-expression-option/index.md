+++
type = "question"
title = "Missing regular expression option"
description = '''In version 2.2.2, there is no regular expression option inside Edit...Find in Packet. Is there a way to add this option back in? The find in packet is also floating now, is there a way to add it back below the menu bar? Regards, Bob.'''
date = "2016-11-24T11:51:00Z"
lastmod = "2016-11-25T04:39:00Z"
weight = 57616
keywords = [ "regex", "find" ]
aliases = [ "/questions/57616" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Missing regular expression option](/questions/57616/missing-regular-expression-option)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57616-score" class="post-score" title="current number of votes">0</div><span id="post-57616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In version 2.2.2, there is no regular expression option inside Edit...Find in Packet. Is there a way to add this option back in? The find in packet is also floating now, is there a way to add it back below the menu bar?</p><p>Regards, Bob.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-regex" rel="tag" title="see questions tagged &#39;regex&#39;">regex</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '16, 11:51</strong></p><img src="https://secure.gravatar.com/avatar/64d849d59fc5c2f8a2ec2930853f340e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BobC&#39;s gravatar image" /><p><span>BobC</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BobC has no accepted answers">0%</span></p></div></div><div id="comments-container-57616" class="comments-container"></div><div id="comment-tools-57616" class="comment-tools"></div><div class="clear"></div><div id="comment-57616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57623"></span>

<div id="answer-container-57623" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57623-score" class="post-score" title="current number of votes">1</div><span id="post-57623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="BobC has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It sounds like you're using the old GTK+ UI. If you're running Windows did you open "Wireshark" or "Wireshark Legacy"?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Nov '16, 19:00</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-57623" class="comments-container"><span id="57633"></span><div id="comment-57633" class="comment"><div id="post-57633-score" class="comment-score"></div><div class="comment-text"><p>You hit the nail on the head, after the last update I was starting the wrong app. Yes...I am new. Thanks for the help.</p></div><div id="comment-57633-info" class="comment-info"><span class="comment-age">(25 Nov '16, 04:39)</span> <span class="comment-user userinfo">BobC</span></div></div></div><div id="comment-tools-57623" class="comment-tools"></div><div class="clear"></div><div id="comment-57623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

