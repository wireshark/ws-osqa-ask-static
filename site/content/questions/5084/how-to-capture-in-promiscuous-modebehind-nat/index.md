+++
type = "question"
title = "How to capture in promiscuous mode(behind NAT)?"
description = '''I would like to capture packets that are sent to a someone behind the same router(there is just two of ous). I checked Capture packets in promiscuous mode in Capture options, but only thing I capture is some ARPs. What more do I need do to sniff from someone behind same router.'''
date = "2011-07-17T13:53:00Z"
lastmod = "2011-07-17T16:57:00Z"
weight = 5084
keywords = [ "capture", "promiscuous", "nat" ]
aliases = [ "/questions/5084" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture in promiscuous mode(behind NAT)?](/questions/5084/how-to-capture-in-promiscuous-modebehind-nat)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5084-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5084-score" class="post-score" title="current number of votes">0</div><span id="post-5084-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to capture packets that are sent to a someone behind the same router(there is just two of ous). I checked Capture packets in promiscuous mode in Capture options, but only thing I capture is some ARPs. What more do I need do to sniff from someone behind same router.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-nat" rel="tag" title="see questions tagged &#39;nat&#39;">nat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jul '11, 13:53</strong></p><img src="https://secure.gravatar.com/avatar/85d2e9101f043d1517f96394327ba0c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LivinOnAThinLine&#39;s gravatar image" /><p><span>LivinOnAThin...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LivinOnAThinLine has no accepted answers">0%</span></p></div></div><div id="comments-container-5084" class="comments-container"></div><div id="comment-tools-5084" class="comment-tools"></div><div class="clear"></div><div id="comment-5084-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5085"></span>

<div id="answer-container-5085" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5085-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5085-score" class="post-score" title="current number of votes">2</div><span id="post-5085-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Your router is probably also a switch, so you will normally only see broadcast traffic and traffic to and from your PC. See <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Ethernet capture setup</a> on the Wireshark wiki for a more detailed explanation and for suggestions on how to get around this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jul '11, 16:57</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-5085" class="comments-container"></div><div id="comment-tools-5085" class="comment-tools"></div><div class="clear"></div><div id="comment-5085-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

