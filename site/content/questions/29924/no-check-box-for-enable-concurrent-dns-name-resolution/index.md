+++
type = "question"
title = "No Check Box For &quot;Enable Concurrent DNS Name Resolution&quot;"
description = '''When I try to click the check box for &quot;Enable Concurrent DNS Name Resolution&quot;, there is no box, it says N/A. How would I go about fixing this?  Enable Concurrent DNS Name Resolution: N/A That is all I get when attempting to check it.'''
date = "2014-02-16T23:54:00Z"
lastmod = "2014-02-17T02:48:00Z"
weight = 29924
keywords = [ "concurrent", "enable", "dns", "error" ]
aliases = [ "/questions/29924" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No Check Box For "Enable Concurrent DNS Name Resolution"](/questions/29924/no-check-box-for-enable-concurrent-dns-name-resolution)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29924-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29924-score" class="post-score" title="current number of votes">0</div><span id="post-29924-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I try to click the check box for "Enable Concurrent DNS Name Resolution", there is no box, it says N/A. How would I go about fixing this?<br />
</p><p>Enable Concurrent DNS Name Resolution: N/A</p><p>That is all I get when attempting to check it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-concurrent" rel="tag" title="see questions tagged &#39;concurrent&#39;">concurrent</span> <span class="post-tag tag-link-enable" rel="tag" title="see questions tagged &#39;enable&#39;">enable</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Feb '14, 23:54</strong></p><img src="https://secure.gravatar.com/avatar/8bd9d47061d200ebe3cbffd24482ec72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cenbhildress&#39;s gravatar image" /><p><span>cenbhildress</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cenbhildress has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-29924" class="comments-container"></div><div id="comment-tools-29924" class="comment-tools"></div><div class="clear"></div><div id="comment-29924-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29929"></span>

<div id="answer-container-29929" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29929-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29929-score" class="post-score" title="current number of votes">2</div><span id="post-29929-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>By building your own version of Wireshark, linked with the C-ARES or ADNS library, or by getting whoever made the version of Wireshark you're running to do so.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '14, 02:48</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-29929" class="comments-container"></div><div id="comment-tools-29929" class="comment-tools"></div><div class="clear"></div><div id="comment-29929-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

