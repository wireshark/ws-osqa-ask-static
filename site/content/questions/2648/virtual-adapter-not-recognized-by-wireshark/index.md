+++
type = "question"
title = "Virtual adapter not recognized by wireshark"
description = '''Hi, I built a NDIS virtual miniport driver. The virtual adapter shows up in the network connections and IPConfig result. But it is not recognized by wireshark. Any ideas? Thx in advance.'''
date = "2011-03-03T07:02:00Z"
lastmod = "2011-03-21T20:01:00Z"
weight = 2648
keywords = [ "recognized", "adapter", "virtual", "ndis", "wireshark" ]
aliases = [ "/questions/2648" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Virtual adapter not recognized by wireshark](/questions/2648/virtual-adapter-not-recognized-by-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2648-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2648-score" class="post-score" title="current number of votes">0</div><span id="post-2648-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I built a NDIS virtual miniport driver. The virtual adapter shows up in the network connections and IPConfig result. But it is not recognized by wireshark. Any ideas? Thx in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-recognized" rel="tag" title="see questions tagged &#39;recognized&#39;">recognized</span> <span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-virtual" rel="tag" title="see questions tagged &#39;virtual&#39;">virtual</span> <span class="post-tag tag-link-ndis" rel="tag" title="see questions tagged &#39;ndis&#39;">ndis</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '11, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/c32df628df5ce7e3ddf8d92e4c0b3a13?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yijun&#39;s gravatar image" /><p><span>Yijun</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yijun has no accepted answers">0%</span></p></div></div><div id="comments-container-2648" class="comments-container"></div><div id="comment-tools-2648" class="comment-tools"></div><div class="clear"></div><div id="comment-2648-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2996"></span>

<div id="answer-container-2996" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2996-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2996-score" class="post-score" title="current number of votes">0</div><span id="post-2996-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is it recognized by <a href="http://www.winpcap.org/windump/default.htm">WinDump</a>? If not, you might try contacting the folks at <a href="http://www.winpcap.org/default.htm">WinPcap</a> first.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '11, 20:01</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-2996" class="comments-container"></div><div id="comment-tools-2996" class="comment-tools"></div><div class="clear"></div><div id="comment-2996-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

