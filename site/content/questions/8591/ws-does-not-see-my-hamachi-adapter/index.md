+++
type = "question"
title = "WS does not see my Hamachi adapter?"
description = '''Hi, can anyone advise? WS sees all my NIs/adapters (including my Realtek gigabit NIC, my VMware virtual adapter, OpenVPN, Tunngle) but it does not see the Logmein Hamachi one? The Hamachi network is connected and clearly visible in Windows Network Connections. Any ideas? '''
date = "2012-01-24T14:57:00Z"
lastmod = "2012-01-24T15:09:00Z"
weight = 8591
keywords = [ "capture", "issue", "hamachi" ]
aliases = [ "/questions/8591" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WS does not see my Hamachi adapter?](/questions/8591/ws-does-not-see-my-hamachi-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8591-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8591-score" class="post-score" title="current number of votes">0</div><span id="post-8591-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, can anyone advise? WS sees all my NIs/adapters (including my Realtek gigabit NIC, my VMware virtual adapter, OpenVPN, Tunngle) but it does not see the Logmein Hamachi one?</p><p>The Hamachi network is connected and clearly visible in Windows Network Connections.</p><p>Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-issue" rel="tag" title="see questions tagged &#39;issue&#39;">issue</span> <span class="post-tag tag-link-hamachi" rel="tag" title="see questions tagged &#39;hamachi&#39;">hamachi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '12, 14:57</strong></p><img src="https://secure.gravatar.com/avatar/66c5f39147cfc89b2185e8a51b02b911?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="glon&#39;s gravatar image" /><p><span>glon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="glon has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Jan '12, 14:59</strong> </span></p></div></div><div id="comments-container-8591" class="comments-container"></div><div id="comment-tools-8591" class="comment-tools"></div><div class="clear"></div><div id="comment-8591-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8592"></span>

<div id="answer-container-8592" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8592-score" class="post-score" title="current number of votes">0</div><span id="post-8592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>OK got it solved suprisingly fast and easy. After intalling Hamachi I did not reboot the PC seeing as it didn't prompt me.</p><p>Now that I did reboot, the Hamachi adapter is finally showing up in Wireshark!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '12, 15:09</strong></p><img src="https://secure.gravatar.com/avatar/66c5f39147cfc89b2185e8a51b02b911?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="glon&#39;s gravatar image" /><p><span>glon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="glon has no accepted answers">0%</span></p></div></div><div id="comments-container-8592" class="comments-container"></div><div id="comment-tools-8592" class="comment-tools"></div><div class="clear"></div><div id="comment-8592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

