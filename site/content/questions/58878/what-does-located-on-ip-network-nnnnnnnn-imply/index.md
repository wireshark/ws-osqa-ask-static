+++
type = "question"
title = "What does &#x27;Located on IP Network nn.nn.nn.nn&#x27; imply?"
description = '''I have a Netgear C6300 modem/router with my main W7 system connected directly to one port of the router. The router is at its default address of 192.168.0.1, and the W7 system is at 192.168.0.9. I also have a Netgear GS108E switch connected to the router, and XP and W2K systems connected to another ...'''
date = "2017-01-18T22:48:00Z"
lastmod = "2017-01-18T22:48:00Z"
weight = 58878
keywords = [ "network" ]
aliases = [ "/questions/58878" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What does 'Located on IP Network nn.nn.nn.nn' imply?](/questions/58878/what-does-located-on-ip-network-nnnnnnnn-imply)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58878-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58878-score" class="post-score" title="current number of votes">0</div><span id="post-58878-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a Netgear C6300 modem/router with my main W7 system connected directly to one port of the router. The router is at its default address of 192.168.0.1, and the W7 system is at 192.168.0.9. I also have a Netgear GS108E switch connected to the router, and XP and W2K systems connected to another two ports of the switch. When I run the Netgear ProSafe_Plus Configuration Utility (on the W7 system) it finds the switch and reports it at IP address 198.168.0.239 (the default address, as expected), and says that it is 'Located on IP Network' 192.168.0.9.</p><p>My questions then, are: 1. What is it that causes the switch to be associated with the W7 system? 2. What, exactly, is the entity ' IP Network 192.168.0.9 ' ? 3. What might be the case if there was a second system also connected directly to the router?</p><p>Apologies if I haven't expressed myself clearly. It's quite possible that I don't even know the correct question to ask!</p><p>Thanks, Roger</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '17, 22:48</strong></p><img src="https://secure.gravatar.com/avatar/f53fafa68a3705ccbd7e31cb1a5a494b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RLH&#39;s gravatar image" /><p><span>RLH</span><br />
<span class="score" title="8 reputation points">8</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RLH has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Jan '17, 01:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-58878" class="comments-container"></div><div id="comment-tools-58878" class="comment-tools"></div><div class="clear"></div><div id="comment-58878-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

