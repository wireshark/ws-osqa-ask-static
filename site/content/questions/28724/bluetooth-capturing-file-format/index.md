+++
type = "question"
title = "Bluetooth capturing file format"
description = '''i want to capture a bluetooth HCI layer log in my platfrom（not linux）.  In order to read the log in wireshark,I want the log file have the same format as blueZ hcidump log. so, What&#x27;s the wireshark bluetooth capture file format rules?'''
date = "2014-01-09T05:10:00Z"
lastmod = "2014-01-09T05:49:00Z"
weight = 28724
keywords = [ "bluetooth" ]
aliases = [ "/questions/28724" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bluetooth capturing file format](/questions/28724/bluetooth-capturing-file-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28724-score" class="post-score" title="current number of votes">0</div><span id="post-28724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i want to capture a bluetooth HCI layer log in my platfrom（not linux）. In order to read the log in wireshark,I want the log file have the same format as blueZ hcidump log. so, What's the wireshark bluetooth capture file format rules?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jan '14, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/6b3d475b96cf64dc971640da570c6bd8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yuan&#39;s gravatar image" /><p><span>Yuan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yuan has no accepted answers">0%</span></p></div></div><div id="comments-container-28724" class="comments-container"></div><div id="comment-tools-28724" class="comment-tools"></div><div class="clear"></div><div id="comment-28724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28725"></span>

<div id="answer-container-28725" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28725-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28725-score" class="post-score" title="current number of votes">0</div><span id="post-28725-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I want the log file have the <strong>same format as</strong> blueZ <strong>hcidump log</strong>.<br />
What's the wireshark bluetooth capture file format</p></blockquote><p>Wireshark supports the hcidump format, see the following sample capture</p><blockquote><p><a href="http://wiki.wireshark.org/SampleCaptures#Bluetooth">http://wiki.wireshark.org/SampleCaptures#Bluetooth</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jan '14, 05:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-28725" class="comments-container"></div><div id="comment-tools-28725" class="comment-tools"></div><div class="clear"></div><div id="comment-28725-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

