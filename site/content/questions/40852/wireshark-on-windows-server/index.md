+++
type = "question"
title = "WireShark on windows server"
description = '''I have a windows server that has a 10g fiber card on it. I would like to install wireshark on the server and use it for captures. Do I need to be running a particular windows server version?'''
date = "2015-03-25T10:27:00Z"
lastmod = "2015-03-25T10:35:00Z"
weight = 40852
keywords = [ "wireshark" ]
aliases = [ "/questions/40852" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark on windows server](/questions/40852/wireshark-on-windows-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40852-score" class="post-score" title="current number of votes">0</div><span id="post-40852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a windows server that has a 10g fiber card on it. I would like to install wireshark on the server and use it for captures. Do I need to be running a particular windows server version?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '15, 10:27</strong></p><img src="https://secure.gravatar.com/avatar/ddfacca5ce607d7266e2851541fbb2f8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="edhill&#39;s gravatar image" /><p><span>edhill</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="edhill has no accepted answers">0%</span></p></div></div><div id="comments-container-40852" class="comments-container"></div><div id="comment-tools-40852" class="comment-tools"></div><div class="clear"></div><div id="comment-40852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40853"></span>

<div id="answer-container-40853" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40853-score" class="post-score" title="current number of votes">0</div><span id="post-40853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As long as it's 2k8 or later it should be fine. 2k3 might work.</p><p>I suspect you might drop packets though if the traffic is running near the full line rate. You should maybe capture with dumpcap, and then examine the captures later.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Mar '15, 10:35</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-40853" class="comments-container"></div><div id="comment-tools-40853" class="comment-tools"></div><div class="clear"></div><div id="comment-40853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

