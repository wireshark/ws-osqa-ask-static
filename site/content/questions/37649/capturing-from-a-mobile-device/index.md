+++
type = "question"
title = "Capturing from a mobile device?"
description = '''I&#x27;m using an application on my Iphone that allows me to use it as an extension on a phone system. This is done through the VoIP card. I&#x27;m having an issue with the app and I need to get a capture while mirroring the VoIP port for incoming and outgoing traffic to my PC. I have no idea how to accomplis...'''
date = "2014-11-07T05:50:00Z"
lastmod = "2014-11-07T05:50:00Z"
weight = 37649
keywords = [ "mobile" ]
aliases = [ "/questions/37649" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing from a mobile device?](/questions/37649/capturing-from-a-mobile-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37649-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37649-score" class="post-score" title="current number of votes">0</div><span id="post-37649-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using an application on my Iphone that allows me to use it as an extension on a phone system. This is done through the VoIP card. I'm having an issue with the app and I need to get a capture while mirroring the VoIP port for incoming and outgoing traffic to my PC. I have no idea how to accomplish this and I was hoping for some help. Thanks in advance guys!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '14, 05:50</strong></p><img src="https://secure.gravatar.com/avatar/9a73d09202c0494730bfd120c790f10d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="borderxline53&#39;s gravatar image" /><p><span>borderxline53</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="borderxline53 has no accepted answers">0%</span></p></div></div><div id="comments-container-37649" class="comments-container"></div><div id="comment-tools-37649" class="comment-tools"></div><div class="clear"></div><div id="comment-37649-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

