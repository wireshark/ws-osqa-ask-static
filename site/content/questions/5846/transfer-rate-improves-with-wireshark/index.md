+++
type = "question"
title = "Transfer rate improves with Wireshark"
description = '''I am trying to measure the read/write performance of a networked attached storage appliance using a Dell Vostro laptop/ Windows XP. Nominally, the read transfer rate is poor (ie read is worse than write). However, when I monitor the ethernet connection using WireShark (installed on the laptop), the ...'''
date = "2011-08-24T11:06:00Z"
lastmod = "2011-08-24T11:06:00Z"
weight = 5846
keywords = [ "transfer", "rate" ]
aliases = [ "/questions/5846" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Transfer rate improves with Wireshark](/questions/5846/transfer-rate-improves-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5846-score" class="post-score" title="current number of votes">0</div><span id="post-5846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to measure the read/write performance of a networked attached storage appliance using a Dell Vostro laptop/ Windows XP. Nominally, the read transfer rate is poor (ie read is worse than write). However, when I monitor the ethernet connection using WireShark (installed on the laptop), the read performance significantly improves.</p><p>Does Wireshark buffer data, or modify the transmission in any way that may explain this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-transfer" rel="tag" title="see questions tagged &#39;transfer&#39;">transfer</span> <span class="post-tag tag-link-rate" rel="tag" title="see questions tagged &#39;rate&#39;">rate</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Aug '11, 11:06</strong></p><img src="https://secure.gravatar.com/avatar/a1c0aa2bac597420a999ee79f15d87b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ironman&#39;s gravatar image" /><p><span>Ironman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ironman has no accepted answers">0%</span></p></div></div><div id="comments-container-5846" class="comments-container"></div><div id="comment-tools-5846" class="comment-tools"></div><div class="clear"></div><div id="comment-5846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

