+++
type = "question"
title = "Wireshark and WinPcap"
description = '''Starting with Wireshark 1.10.0 When I try to update it tells me that there is a previous version of WinPcap - there isn&#x27;t (I have searched my computer high and low) and the installation aborts. Any ideas?'''
date = "2013-07-30T15:13:00Z"
lastmod = "2013-07-30T15:13:00Z"
weight = 23455
keywords = [ "winpcap", "abort", "installation" ]
aliases = [ "/questions/23455" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and WinPcap](/questions/23455/wireshark-and-winpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23455-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23455-score" class="post-score" title="current number of votes">0</div><span id="post-23455-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Starting with Wireshark 1.10.0 When I try to update it tells me that there is a previous version of WinPcap - there isn't (I have searched my computer high and low) and the installation aborts. Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-abort" rel="tag" title="see questions tagged &#39;abort&#39;">abort</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '13, 15:13</strong></p><img src="https://secure.gravatar.com/avatar/0fc4daa658f27cee442eb087e5fb2c36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="UriB&#39;s gravatar image" /><p><span>UriB</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="UriB has no accepted answers">0%</span></p></div></div><div id="comments-container-23455" class="comments-container"></div><div id="comment-tools-23455" class="comment-tools"></div><div class="clear"></div><div id="comment-23455-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

