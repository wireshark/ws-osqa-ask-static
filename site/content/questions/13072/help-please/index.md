+++
type = "question"
title = "help please!!"
description = '''I used wireshark.... with no other programs running, in the capture window when i start.. my system is peppered with files... some of them say malformed packet.. On top of this, there is no way i can target a specific http ip to investigate with all of this other interference. Please help'''
date = "2012-07-27T05:31:00Z"
lastmod = "2012-07-27T05:36:00Z"
weight = 13072
keywords = [ "problems", "help" ]
aliases = [ "/questions/13072" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [help please!!](/questions/13072/help-please)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13072-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13072-score" class="post-score" title="current number of votes">0</div><span id="post-13072-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I used wireshark.... with no other programs running, in the capture window when i start.. my system is peppered with files... some of them say malformed packet.. On top of this, there is no way i can target a specific http ip to investigate with all of this other interference. Please help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-problems" rel="tag" title="see questions tagged &#39;problems&#39;">problems</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '12, 05:31</strong></p><img src="https://secure.gravatar.com/avatar/4b2214946811404f151af484b1000cf3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abert&#39;s gravatar image" /><p><span>abert</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abert has no accepted answers">0%</span></p></div></div><div id="comments-container-13072" class="comments-container"><span id="13073"></span><div id="comment-13073" class="comment"><div id="post-13073-score" class="comment-score"></div><div class="comment-text"><p>This is a Q&amp;A site (see FAQ: <a href="http://ask.wireshark.org/faq/)">http://ask.wireshark.org/faq/)</a></p><p>So, what is your question?</p></div><div id="comment-13073-info" class="comment-info"><span class="comment-age">(27 Jul '12, 05:36)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-13072" class="comment-tools"></div><div class="clear"></div><div id="comment-13072-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

