+++
type = "question"
title = "RTMPT plugin shows RTMPT chunk data not RTMPT data? Is de-chunking implemented in RTMPT dissector ??"
description = '''RTMPT plugin only shows chuncked data , it doesn&#x27;t show un-chuncked data. ALL headers which it shows are chuncked data header not RTMPT headers. Is de-chuncking implemented in dissector or is their any way i can see RTMPT de-chuncked packed in dissector ? '''
date = "2012-11-19T02:23:00Z"
lastmod = "2012-11-19T02:23:00Z"
weight = 16045
keywords = [ "rtmpt" ]
aliases = [ "/questions/16045" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTMPT plugin shows RTMPT chunk data not RTMPT data? Is de-chunking implemented in RTMPT dissector ??](/questions/16045/rtmpt-plugin-shows-rtmpt-chunk-data-not-rtmpt-data-is-de-chunking-implemented-in-rtmpt-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16045-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16045-score" class="post-score" title="current number of votes">0</div><span id="post-16045-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>RTMPT plugin only shows chuncked data , it doesn't show un-chuncked data. ALL headers which it shows are chuncked data header not RTMPT headers. Is de-chuncking implemented in dissector or is their any way i can see RTMPT de-chuncked packed in dissector ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtmpt" rel="tag" title="see questions tagged &#39;rtmpt&#39;">rtmpt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '12, 02:23</strong></p><img src="https://secure.gravatar.com/avatar/9350908b20cf6e624a23a2db7b0fba7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ishan&#39;s gravatar image" /><p><span>ishan</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ishan has no accepted answers">0%</span></p></div></div><div id="comments-container-16045" class="comments-container"></div><div id="comment-tools-16045" class="comment-tools"></div><div class="clear"></div><div id="comment-16045-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

