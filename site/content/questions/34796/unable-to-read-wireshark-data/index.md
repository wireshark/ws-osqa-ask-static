+++
type = "question"
title = "Unable to read wireshark data"
description = '''Hi Team, I have a application hosted on my machine and it is not at all using any SSL certificates. i have installed a APM tool to monitor my application. this APM tool will gather some data from my application and saves it in it&#x27;s database. i want to see what the data this APM tool is gathering. so...'''
date = "2014-07-21T01:52:00Z"
lastmod = "2014-07-21T01:52:00Z"
weight = 34796
keywords = [ "data", "decrypt", "wireshark" ]
aliases = [ "/questions/34796" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to read wireshark data](/questions/34796/unable-to-read-wireshark-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34796-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34796-score" class="post-score" title="current number of votes">0</div><span id="post-34796-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Team,</p><p>I have a application hosted on my machine and it is not at all using any SSL certificates. i have installed a APM tool to monitor my application. this APM tool will gather some data from my application and saves it in it's database.</p><p>i want to see what the data this APM tool is gathering. so that i can be confident that it is not gathering any secure data.</p><p>so, i installed wire shark on the application sever and applied a filter for the IP addresses using which the APM tool gathers the data. i was able to see some entries in the log bu tif i see the data, all the data showing in wireshark is in encrypted format.</p><p>As i am not using any SSL,the data is showing up in the encrypted format.</p><p>please suggest how ca i view that data in a readable format.</p><p>thanks, vamsi k</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '14, 01:52</strong></p><img src="https://secure.gravatar.com/avatar/09556915d33f9202e28cbb2502388157?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vkethineni&#39;s gravatar image" /><p><span>vkethineni</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vkethineni has no accepted answers">0%</span></p></div></div><div id="comments-container-34796" class="comments-container"></div><div id="comment-tools-34796" class="comment-tools"></div><div class="clear"></div><div id="comment-34796-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

