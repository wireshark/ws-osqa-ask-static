+++
type = "question"
title = "How can I retrieve video encoding information from a capture?"
description = '''Hello, I am a Wireshark newbie. I am writing a project for computer networks. I used VLC to stream on other computer over LAN via HTTP protocol  The profile used is h.264+acc (TS) to stream and capture it at destination with Wireshark and stream on vlc on that computer. It look like this  Is there a...'''
date = "2011-12-27T09:42:00Z"
lastmod = "2011-12-27T09:42:00Z"
weight = 8142
keywords = [ "video", "h.264", "http" ]
aliases = [ "/questions/8142" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I retrieve video encoding information from a capture?](/questions/8142/how-can-i-retrieve-video-encoding-information-from-a-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8142-score" class="post-score" title="current number of votes">0</div><span id="post-8142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am a Wireshark newbie. I am writing a project for computer networks. I used VLC to stream on other computer over LAN via HTTP protocol The profile used is h.264+acc (TS) to stream and capture it at destination with Wireshark and stream on vlc on that computer. It look like this <img src="https://lh3.googleusercontent.com/-t4F09WjbGPQ/TvoBKnLOljI/AAAAAAAAALQ/zX9fq8LKX98/s1024/http.tcp.hand1.png" alt="screenshot" /></p><p>Is there any way to obtain further information about video codec or the packetized elementary stream (pes) of mp4 file that was streamed from HTTP?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span> <span class="post-tag tag-link-h.264" rel="tag" title="see questions tagged &#39;h.264&#39;">h.264</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Dec '11, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/ae0b3ca7e9ff9b6d61d68948e640e20b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nkv1&#39;s gravatar image" /><p><span>nkv1</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nkv1 has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Dec '11, 09:48</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-8142" class="comments-container"></div><div id="comment-tools-8142" class="comment-tools"></div><div class="clear"></div><div id="comment-8142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

