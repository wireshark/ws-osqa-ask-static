+++
type = "question"
title = "Permission to use the icon"
description = '''Hello We work on Windows Package Manager (http://code.google.com/p/windows-package-manager/) released under GNU GPL license. It helps to find and install software, keep a system up-to-date and uninstall it if no longer necessary. I&#x27;d like to ask for permission to copy the &#x27;Wireshark&#x27; icon (wireshark...'''
date = "2013-01-29T22:54:00Z"
lastmod = "2013-01-30T23:49:00Z"
weight = 18075
keywords = [ "icon" ]
aliases = [ "/questions/18075" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Permission to use the icon](/questions/18075/permission-to-use-the-icon)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18075-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18075-score" class="post-score" title="current number of votes">0</div><span id="post-18075-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>We work on Windows Package Manager (<a href="http://code.google.com/p/windows-package-manager/)">http://code.google.com/p/windows-package-manager/)</a> released under GNU GPL license. It helps to find and install software, keep a system up-to-date and uninstall it if no longer necessary.</p><p>I'd like to ask for permission to copy the 'Wireshark' icon (wireshark-1.8.5/image/wsicon32.png) to Npackd web server and use it for 'Wireshark' package identifying icon.</p><p>Thank You</p><p>Evgeny</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-icon" rel="tag" title="see questions tagged &#39;icon&#39;">icon</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jan '13, 22:54</strong></p><img src="https://secure.gravatar.com/avatar/50bb610f03e77720c3b986ebcb783139?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nockdown&#39;s gravatar image" /><p><span>nockdown</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nockdown has no accepted answers">0%</span></p></div></div><div id="comments-container-18075" class="comments-container"></div><div id="comment-tools-18075" class="comment-tools"></div><div class="clear"></div><div id="comment-18075-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="18078"></span>

<div id="answer-container-18078" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18078-score" class="post-score" title="current number of votes">1</div><span id="post-18078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not really a question for Ask Wireshark, please use the <a href="https://www.wireshark.org/mailman/listinfo/wireshark-dev">dev mail list</a> or contact <span>@Gerald</span> directly.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '13, 00:06</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-18078" class="comments-container"></div><div id="comment-tools-18078" class="comment-tools"></div><div class="clear"></div><div id="comment-18078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18136"></span>

<div id="answer-container-18136" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18136-score" class="post-score" title="current number of votes">0</div><span id="post-18136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I replied to Evgeny offline.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '13, 14:35</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-18136" class="comments-container"><span id="18147"></span><div id="comment-18147" class="comment"><div id="post-18147-score" class="comment-score"></div><div class="comment-text"><p>What is this "offline" communication? Phone or snail mail :-)</p></div><div id="comment-18147-info" class="comment-info"><span class="comment-age">(30 Jan '13, 23:49)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18136" class="comment-tools"></div><div class="clear"></div><div id="comment-18136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18140"></span>

<div id="answer-container-18140" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18140-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18140-score" class="post-score" title="current number of votes">0</div><span id="post-18140-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use it, but you must pay me one meeeeeeeelion dollars! :) j/k</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '13, 16:13</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span></p></div></div><div id="comments-container-18140" class="comments-container"></div><div id="comment-tools-18140" class="comment-tools"></div><div class="clear"></div><div id="comment-18140-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

