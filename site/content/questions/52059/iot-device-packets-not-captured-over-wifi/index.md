+++
type = "question"
title = "iot device packets not captured over wifi"
description = '''hi,  I am student using iot device to send packet over ethernet on my laptop using network sharin on my laptop.for tcp protocol the first syn syn ack mssgs are getting captured over wireshark when i am opening two windows for wireshark to capture..one for the ethernet to my device and one over my wi...'''
date = "2016-04-28T10:10:00Z"
lastmod = "2016-04-29T00:35:00Z"
weight = 52059
keywords = [ "rohit" ]
aliases = [ "/questions/52059" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [iot device packets not captured over wifi](/questions/52059/iot-device-packets-not-captured-over-wifi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52059-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52059-score" class="post-score" title="current number of votes">0</div><span id="post-52059-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, I am student using iot device to send packet over ethernet on my laptop using network sharin on my laptop.for tcp protocol the first syn syn ack mssgs are getting captured over wireshark when i am opening two windows for wireshark to capture..one for the ethernet to my device and one over my wifi.the first three way handshaking is seen on my wifi capture t a external site and when my sending the tcp traffic like get and post packets ,they are getting capture over the ethernet but not getting send or captured on the wifi.why is the laptop not routing those packets?can anyone help me wid this help is appreciated. thak you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rohit" rel="tag" title="see questions tagged &#39;rohit&#39;">rohit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '16, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/b41f9190c7b2282e8e02ae492175f74d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RoHit%20PaTil&#39;s gravatar image" /><p><span>RoHit PaTil</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RoHit PaTil has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>29 Apr '16, 00:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-52059" class="comments-container"><span id="52067"></span><div id="comment-52067" class="comment"><div id="post-52067-score" class="comment-score"></div><div class="comment-text"><p>That's a network sharin question, where a operating system related forum is more appropriate.</p></div><div id="comment-52067-info" class="comment-info"><span class="comment-age">(29 Apr '16, 00:35)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-52059" class="comment-tools"></div><div class="clear"></div><div id="comment-52059-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

