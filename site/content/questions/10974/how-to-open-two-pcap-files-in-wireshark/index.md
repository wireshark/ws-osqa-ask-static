+++
type = "question"
title = "How to open two PCAP Files in Wireshark"
description = '''Is it possible to open and to view two (or more) different PCAP files in Wireshark? In other words, is a &quot;multi document interface&quot; possible in Wireshark?'''
date = "2012-05-15T00:45:00Z"
lastmod = "2012-05-15T01:22:00Z"
weight = 10974
keywords = [ "mdi" ]
aliases = [ "/questions/10974" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to open two PCAP Files in Wireshark](/questions/10974/how-to-open-two-pcap-files-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10974-score" class="post-score" title="current number of votes">0</div><span id="post-10974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to open and to view two (or more) different PCAP files in Wireshark? In other words, is a "multi document interface" possible in Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mdi" rel="tag" title="see questions tagged &#39;mdi&#39;">mdi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '12, 00:45</strong></p><img src="https://secure.gravatar.com/avatar/f45848e2181a57e335916a9bc57aa0c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ZvDj&#39;s gravatar image" /><p><span>ZvDj</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ZvDj has no accepted answers">0%</span></p></div></div><div id="comments-container-10974" class="comments-container"></div><div id="comment-tools-10974" class="comment-tools"></div><div class="clear"></div><div id="comment-10974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10976"></span>

<div id="answer-container-10976" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10976-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10976-score" class="post-score" title="current number of votes">0</div><span id="post-10976-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you use File -&gt; Open, Wireshark will replace the currently viewed pcap file with the new one.</p><p><strong>If you want to view two pcap files at once, you must open two instances of Wireshark</strong> (double click on the pcap files, or just start two instances manually).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '12, 00:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 May '12, 01:28</strong> </span></p></div></div><div id="comments-container-10976" class="comments-container"><span id="10978"></span><div id="comment-10978" class="comment"><div id="post-10978-score" class="comment-score"></div><div class="comment-text"><p>That sounds fine. Thanks.</p></div><div id="comment-10978-info" class="comment-info"><span class="comment-age">(15 May '12, 01:22)</span> <span class="comment-user userinfo">ZvDj</span></div></div></div><div id="comment-tools-10976" class="comment-tools"></div><div class="clear"></div><div id="comment-10976-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

