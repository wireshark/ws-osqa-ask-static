+++
type = "question"
title = "How can we set up wireshark to display all Devices connected to our Netork (Router)"
description = '''Wondering a Couple of things 1) How do we set up Wireshark to display all devices connected to Router, directly or By wi-fi 2) Once the devices are all displayed, can we set it up so that not only are the Packets captured but the Destination IP is displayed? I have looked around viewed Videos but ha...'''
date = "2010-11-19T21:54:00Z"
lastmod = "2010-11-22T10:43:00Z"
weight = 1030
keywords = [ "ips", "devices", "wireshark" ]
aliases = [ "/questions/1030" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [How can we set up wireshark to display all Devices connected to our Netork (Router)](/questions/1030/how-can-we-set-up-wireshark-to-display-all-devices-connected-to-our-netork-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1030-score" class="post-score" title="current number of votes">0</div><span id="post-1030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wondering a Couple of things</p><p>1) How do we set up Wireshark to display all devices connected to Router, directly or By wi-fi</p><p>2) Once the devices are all displayed, can we set it up so that not only are the Packets captured but the Destination IP is displayed?</p><p>I have looked around viewed Videos but have found no info on that yet</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ips" rel="tag" title="see questions tagged &#39;ips&#39;">ips</span> <span class="post-tag tag-link-devices" rel="tag" title="see questions tagged &#39;devices&#39;">devices</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '10, 21:54</strong></p><img src="https://secure.gravatar.com/avatar/e2f6579a2a2973e0c7f92540dbf68459?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tech07&#39;s gravatar image" /><p><span>tech07</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tech07 has no accepted answers">0%</span></p></div></div><div id="comments-container-1030" class="comments-container"></div><div id="comment-tools-1030" class="comment-tools"></div><div class="clear"></div><div id="comment-1030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="1037"></span>

<div id="answer-container-1037" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1037-score" class="post-score" title="current number of votes">0</div><span id="post-1037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Look into 'monitor port', or 'SPAN' for instance.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '10, 06:03</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1037" class="comments-container"><span id="1043"></span><div id="comment-1043" class="comment"><div id="post-1043-score" class="comment-score"></div><div class="comment-text"><p>Thanks for reply but can you be a bit more specific please?</p><p>Where would I find these Options. I am new to wireshark</p></div><div id="comment-1043-info" class="comment-info"><span class="comment-age">(20 Nov '10, 10:59)</span> <span class="comment-user userinfo">tech07</span></div></div></div><div id="comment-tools-1037" class="comment-tools"></div><div class="clear"></div><div id="comment-1037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1044"></span>

<div id="answer-container-1044" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1044-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1044-score" class="post-score" title="current number of votes">0</div><span id="post-1044-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is no network mapping tool in itself, but you can use it to capture network packets and analyze them or create statistics on what devices talk to what other devices. So if you're looking for some kind of software that will scan you network and tell you about all the devices it has found Wireshark is not that much for you.</p><p>What you can do is hook up Wireshark to you router and have it "copy" all network packets (inbound and outbound) from all other ports to the port Wireshark is connected at. That is what Jaap meant when he said you might have a look into monitor ports A.K.A SPAN ports. This kind of functionality depends on the abilities of your router to do such a thing; not all routers have it or can provide monitor sessions for all ports.</p><p>All in all I would advise you to give us more details of your situation/requirements and you might get a much better answer ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '10, 11:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-1044" class="comments-container"></div><div id="comment-tools-1044" class="comment-tools"></div><div class="clear"></div><div id="comment-1044-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1067"></span>

<div id="answer-container-1067" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1067-score" class="post-score" title="current number of votes">0</div><span id="post-1067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you want to do this without modifying your network devices (e.g. switches), nmap might be a better tool for this project. It will scan entire subnets and present a list of "discovered" hosts and (if open) the service ports open on each host.</p><p>You can find both command-line and GUI versions of nmap at <a href="http://nmap.org/">http://nmap.org/</a>.</p><p>WARNING: This is an ACTIVE port scanner, and it WILL trigger many (if not most) intrusion detection systems. Be sure you have permission to run it against the networks in question before doing so.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Nov '10, 10:43</strong></p><img src="https://secure.gravatar.com/avatar/11ea89c2fd5a5830c69d0574a51b8142?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wesmorgan1&#39;s gravatar image" /><p><span>wesmorgan1</span><br />
<span class="score" title="411 reputation points">411</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wesmorgan1 has 2 accepted answers">4%</span></p></div></div><div id="comments-container-1067" class="comments-container"></div><div id="comment-tools-1067" class="comment-tools"></div><div class="clear"></div><div id="comment-1067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

