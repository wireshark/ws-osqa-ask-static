+++
type = "question"
title = "How to capture packets and then search for specific string and export"
description = '''Hi, I wish to capture automatically specific lines from a packet. I know what the packet header is. How do I do this (with Tshark I assume). I could simply settle for just exporting then entire packet to a text file and then using an independent parser. Thank you for any help!'''
date = "2013-05-05T10:13:00Z"
lastmod = "2013-05-05T10:13:00Z"
weight = 20961
keywords = [ "specific", "automatically" ]
aliases = [ "/questions/20961" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture packets and then search for specific string and export](/questions/20961/how-to-capture-packets-and-then-search-for-specific-string-and-export)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20961-score" class="post-score" title="current number of votes">0</div><span id="post-20961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I wish to capture automatically specific lines from a packet. I know what the packet header is. How do I do this (with Tshark I assume). I could simply settle for just exporting then entire packet to a text file and then using an independent parser.</p><p>Thank you for any help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-specific" rel="tag" title="see questions tagged &#39;specific&#39;">specific</span> <span class="post-tag tag-link-automatically" rel="tag" title="see questions tagged &#39;automatically&#39;">automatically</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '13, 10:13</strong></p><img src="https://secure.gravatar.com/avatar/6bf1a2b8ad51ce1f4f3c9a3a1d911584?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="D%20N&#39;s gravatar image" /><p><span>D N</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="D N has no accepted answers">0%</span></p></div></div><div id="comments-container-20961" class="comments-container"></div><div id="comment-tools-20961" class="comment-tools"></div><div class="clear"></div><div id="comment-20961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

