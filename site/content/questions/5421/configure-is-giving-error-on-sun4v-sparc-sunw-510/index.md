+++
type = "question"
title = "./configure is giving error on sun4v sparc SUNW 5.10"
description = '''i am getting below error which doing ./configure on my Solaris server. configure: error: change your path to search /usr/xpg4/bin or directory containing GNU sed before /usr/bin (and /bin and /usr/ucb)  Please help as soon as possible.'''
date = "2011-08-03T02:46:00Z"
lastmod = "2011-08-04T23:02:00Z"
weight = 5421
keywords = [ "development", "solaris" ]
aliases = [ "/questions/5421" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [./configure is giving error on sun4v sparc SUNW 5.10](/questions/5421/configure-is-giving-error-on-sun4v-sparc-sunw-510)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5421-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5421-score" class="post-score" title="current number of votes">0</div><span id="post-5421-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am getting below error which doing ./configure on my Solaris server.</p><pre><code>configure: error: change your path to search /usr/xpg4/bin or directory containing GNU sed before /usr/bin (and /bin and /usr/ucb)</code></pre><p>Please help as soon as possible.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-solaris" rel="tag" title="see questions tagged &#39;solaris&#39;">solaris</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '11, 02:46</strong></p><img src="https://secure.gravatar.com/avatar/46f1d92dbdd0596560b0f0534d1085b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="harshpandya88&#39;s gravatar image" /><p><span>harshpandya88</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="harshpandya88 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Aug '11, 23:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5421" class="comments-container"><span id="5424"></span><div id="comment-5424" class="comment"><div id="post-5424-score" class="comment-score"></div><div class="comment-text"><p>wireshark FAQ has solved my error.</p><p>On Solaris, changing your command search path to search /usr/xpg4/bin before /usr/bin should make the problem go away;</p><p>i changed my path in above way ,and it worked</p></div><div id="comment-5424-info" class="comment-info"><span class="comment-age">(03 Aug '11, 03:46)</span> <span class="comment-user userinfo">harshpandya88</span></div></div><span id="5468"></span><div id="comment-5468" class="comment"><div id="post-5468-score" class="comment-score">1</div><div class="comment-text"><p>It's perfectly OK to answer your own question (as opposed to simply commenting on it, which you have done) and then accept it. That way the question won't show up as unanswered.</p></div><div id="comment-5468-info" class="comment-info"><span class="comment-age">(03 Aug '11, 18:15)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-5421" class="comment-tools"></div><div class="clear"></div><div id="comment-5421-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5478"></span>

<div id="answer-container-5478" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5478-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5478-score" class="post-score" title="current number of votes">0</div><span id="post-5478-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>wireshark FAQ has solved my error.</p><p>On Solaris, changing your command search path to search /usr/xpg4/bin before /usr/bin should make the problem go away;</p><p>i changed my path in above way ,and it worked</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '11, 22:00</strong></p><img src="https://secure.gravatar.com/avatar/46f1d92dbdd0596560b0f0534d1085b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="harshpandya88&#39;s gravatar image" /><p><span>harshpandya88</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="harshpandya88 has no accepted answers">0%</span></p></div></div><div id="comments-container-5478" class="comments-container"><span id="5511"></span><div id="comment-5511" class="comment"><div id="post-5511-score" class="comment-score"></div><div class="comment-text"><p>And now you just need to accept it.</p></div><div id="comment-5511-info" class="comment-info"><span class="comment-age">(04 Aug '11, 18:32)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="5520"></span><div id="comment-5520" class="comment"><div id="post-5520-score" class="comment-score">1</div><div class="comment-text"><p>"you can bring a horse to water..."</p></div><div id="comment-5520-info" class="comment-info"><span class="comment-age">(04 Aug '11, 23:02)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-5478" class="comment-tools"></div><div class="clear"></div><div id="comment-5478-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

