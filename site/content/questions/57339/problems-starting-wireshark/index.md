+++
type = "question"
title = "Problems starting Wireshark"
description = '''I am running Kali Linux in a VirtualBox on a OS X host. When in my virtual machine (using kali as my OS), I try to open up Wireshark and get the following message: Lua: Error during loading:  [string &quot;/usr/share/wireshark/init.lua&quot;]:44: dofile has been disabled due to running Wireshark as superuser....'''
date = "2016-11-12T12:09:00Z"
lastmod = "2016-11-12T13:41:00Z"
weight = 57339
keywords = [ "starting" ]
aliases = [ "/questions/57339" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Problems starting Wireshark](/questions/57339/problems-starting-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57339-score" class="post-score" title="current number of votes">0</div><span id="post-57339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am running Kali Linux in a VirtualBox on a OS X host. When in my virtual machine (using kali as my OS), I try to open up Wireshark and get the following message:</p><p><strong>Lua: Error during loading: [string "/usr/share/wireshark/init.lua"]:44: dofile has been disabled due to running Wireshark as superuser. See <a href="https://wiki.wireshark.org/CaptureSetup/CapturePrivileges">https://wiki.wireshark.org/CaptureSetup/CapturePrivileges</a> for help in running Wireshark as an unprivileged user.</strong></p><p>How can I get around this problem? What do I need to do (other than what the error message clearly states. I just do not know how to do it).. and what are the steps EXACTLY to correct this error.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-starting" rel="tag" title="see questions tagged &#39;starting&#39;">starting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '16, 12:09</strong></p><img src="https://secure.gravatar.com/avatar/0e5449ea0b59fc5df5a4395008e3bbc6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="phantomlick&#39;s gravatar image" /><p><span>phantomlick</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="phantomlick has no accepted answers">0%</span></p></div></div><div id="comments-container-57339" class="comments-container"></div><div id="comment-tools-57339" class="comment-tools"></div><div class="clear"></div><div id="comment-57339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57341"></span>

<div id="answer-container-57341" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57341-score" class="post-score" title="current number of votes">0</div><span id="post-57341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Refer to <a href="https://ask.wireshark.org/questions/50546/couldnt-run-usrbindumpcap-in-child-process/50747">this older Answer regarding the same topic</a>. The English part of it should be sufficient.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Nov '16, 13:41</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-57341" class="comments-container"></div><div id="comment-tools-57341" class="comment-tools"></div><div class="clear"></div><div id="comment-57341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

