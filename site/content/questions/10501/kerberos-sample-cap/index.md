+++
type = "question"
title = "Kerberos sample cap"
description = '''Hi, I read this info http://wiki.wireshark.org/Kerberos Do you know if the capture files samples are working ? Because I downloaded the files and open them with -K but I cannot see the content inside the ticket Thanks'''
date = "2012-04-28T13:37:00Z"
lastmod = "2012-04-30T06:26:00Z"
weight = 10501
keywords = [ "kerberos" ]
aliases = [ "/questions/10501" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Kerberos sample cap](/questions/10501/kerberos-sample-cap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10501-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10501-score" class="post-score" title="current number of votes">0</div><span id="post-10501-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I read this info <a href="http://wiki.wireshark.org/Kerberos">http://wiki.wireshark.org/Kerberos</a> Do you know if the capture files samples are working ? Because I downloaded the files and open them with -K but I cannot see the content inside the ticket Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kerberos" rel="tag" title="see questions tagged &#39;kerberos&#39;">kerberos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '12, 13:37</strong></p><img src="https://secure.gravatar.com/avatar/f28c9bb02a9a8be03475c4583446c9e4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KerbTest&#39;s gravatar image" /><p><span>KerbTest</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KerbTest has no accepted answers">0%</span></p></div></div><div id="comments-container-10501" class="comments-container"><span id="10503"></span><div id="comment-10503" class="comment"><div id="post-10503-score" class="comment-score"></div><div class="comment-text"><p>BTW, why I cannot find this option in the new wireshark version ? Preferences -&gt; Protocols -&gt; KRB5 -&gt; keytab path</p></div><div id="comment-10503-info" class="comment-info"><span class="comment-age">(28 Apr '12, 20:42)</span> <span class="comment-user userinfo">KerbTest</span></div></div><span id="10517"></span><div id="comment-10517" class="comment"><div id="post-10517-score" class="comment-score">1</div><div class="comment-text"><p>(I converted your Answer to a Comment--please see the FAQ)</p><p>You won't see the preference if your version of Wireshark wasn't compiled against one of the kerberos (MIT or Heimdal) libraries Wireshark uses to decode kerberos.</p></div><div id="comment-10517-info" class="comment-info"><span class="comment-age">(30 Apr '12, 06:26)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-10501" class="comment-tools"></div><div class="clear"></div><div id="comment-10501-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

