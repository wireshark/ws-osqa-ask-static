+++
type = "question"
title = "Detect missing SYN ACK or missing HTTP response"
description = '''Hi,  I&#x27;m basically trying to detect &quot;incomplete&quot; TCP or HTTP communication in a capture file. Is it possible to display only TCP streams that consist of a SYN without a corresponding SYN ACK?  Or streams with no FINs? Or to display HTTP requests that do not receive an HTTP response? Thanks :)'''
date = "2011-07-08T11:27:00Z"
lastmod = "2011-07-08T11:27:00Z"
weight = 4962
keywords = [ "http", "tcp" ]
aliases = [ "/questions/4962" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Detect missing SYN ACK or missing HTTP response](/questions/4962/detect-missing-syn-ack-or-missing-http-response)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4962-score" class="post-score" title="current number of votes">0</div><span id="post-4962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm basically trying to detect "incomplete" TCP or HTTP communication in a capture file.</p><p>Is it possible to display only TCP streams that consist of a SYN without a corresponding SYN ACK? Or streams with no FINs? Or to display HTTP requests that do not receive an HTTP response?</p><p>Thanks :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jul '11, 11:27</strong></p><img src="https://secure.gravatar.com/avatar/102ac441341af0e7772eb274954d1237?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aatoga&#39;s gravatar image" /><p><span>aatoga</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aatoga has no accepted answers">0%</span></p></div></div><div id="comments-container-4962" class="comments-container"></div><div id="comment-tools-4962" class="comment-tools"></div><div class="clear"></div><div id="comment-4962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

