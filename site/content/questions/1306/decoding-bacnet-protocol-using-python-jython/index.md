+++
type = "question"
title = "Decoding BACnet protocol using Python / Jython"
description = '''I am using Wireshark for BACnet protocol. Since wireshahrk has dissector of BACnet, I would like to get the value of the packet using python / jython or any other programming language for verification. Similarly I would like to Construct BACnet data packets and send it to the network. Kindly help. T...'''
date = "2010-12-09T18:35:00Z"
lastmod = "2010-12-09T18:35:00Z"
weight = 1306
keywords = [ "bacnet", "protocol" ]
aliases = [ "/questions/1306" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding BACnet protocol using Python / Jython](/questions/1306/decoding-bacnet-protocol-using-python-jython)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1306-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1306-score" class="post-score" title="current number of votes">0</div><span id="post-1306-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Wireshark for BACnet protocol. Since wireshahrk has dissector of BACnet, I would like to get the value of the packet using python / jython or any other programming language for verification.</p><p>Similarly I would like to Construct BACnet data packets and send it to the network.</p><p>Kindly help.</p><p>Thanks in advance.</p><p>Kishore</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bacnet" rel="tag" title="see questions tagged &#39;bacnet&#39;">bacnet</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '10, 18:35</strong></p><img src="https://secure.gravatar.com/avatar/126375e4bf4696c88a79637ea722a4b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kishorevenki&#39;s gravatar image" /><p><span>kishorevenki</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kishorevenki has no accepted answers">0%</span></p></div></div><div id="comments-container-1306" class="comments-container"></div><div id="comment-tools-1306" class="comment-tools"></div><div class="clear"></div><div id="comment-1306-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

