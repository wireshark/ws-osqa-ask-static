+++
type = "question"
title = "It&#x27;s really important to leave the administrator user account on Windows 7 for using Wireshark (for security reasons)?"
description = '''Well, if it isn&#x27;t probable that remote the servers detect Wiseshark the last thing I have to do before using WireShark is swithing to non-administrator user account. After account change I should be ready to start WiseShark. Or are there other security concerns which I should consider?'''
date = "2014-11-14T07:39:00Z"
lastmod = "2014-11-14T08:02:00Z"
weight = 37861
keywords = [ "windows", "account", "security" ]
aliases = [ "/questions/37861" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [It's really important to leave the administrator user account on Windows 7 for using Wireshark (for security reasons)?](/questions/37861/its-really-important-to-leave-the-administrator-user-account-on-windows-7-for-using-wireshark-for-security-reasons)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37861-score" class="post-score" title="current number of votes">0</div><span id="post-37861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Well, if it isn't probable that remote the servers detect Wiseshark the last thing I have to do before using WireShark is swithing to non-administrator user account. After account change I should be ready to start WiseShark. Or are there other security concerns which I should consider?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-account" rel="tag" title="see questions tagged &#39;account&#39;">account</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '14, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/61f9c66b9d40becc69e0d8a24fe095e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fshark1&#39;s gravatar image" /><p><span>Fshark1</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fshark1 has no accepted answers">0%</span></p></div></div><div id="comments-container-37861" class="comments-container"><span id="37863"></span><div id="comment-37863" class="comment"><div id="post-37863-score" class="comment-score"></div><div class="comment-text"><p>your questions (see the other one) leave me behind with some big questionmarks in front of my face ;-)</p><p>You are asking if <strong>remote</strong> servers can detect if you are using Wireshark, you are talking about windows admin accounts, security <strong>concerns</strong>.</p><p>What exactly are you trying to do? Wireshark is a tool to analyze local network/communication problems. No more, no less.</p></div><div id="comment-37863-info" class="comment-info"><span class="comment-age">(14 Nov '14, 08:02)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37861" class="comment-tools"></div><div class="clear"></div><div id="comment-37861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

