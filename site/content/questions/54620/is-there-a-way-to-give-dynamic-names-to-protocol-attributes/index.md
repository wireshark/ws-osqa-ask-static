+++
type = "question"
title = "Is there a way to give dynamic names to protocol attributes?"
description = '''Hi, I am trying to create a custom lua dissector for the Modbus protocol. This protocol can have any number of identically structured attributes depending upon the packet contents. For example: Register 1 :some value Register 2 :some value Register 3 :some value Register 4 :some value and so on. I n...'''
date = "2016-08-05T12:56:00Z"
lastmod = "2016-08-15T08:12:00Z"
weight = 54620
keywords = [ "modbus", "lua", "dissector", "wireshark" ]
aliases = [ "/questions/54620" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a way to give dynamic names to protocol attributes?](/questions/54620/is-there-a-way-to-give-dynamic-names-to-protocol-attributes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54620-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54620-score" class="post-score" title="current number of votes">0</div><span id="post-54620-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to create a custom lua dissector for the Modbus protocol. This protocol can have any number of identically structured attributes depending upon the packet contents. For example: Register 1 :some value Register 2 :some value Register 3 :some value Register 4 :some value and so on. I need to come up with a way to create protocol fields dynamically and give them dynamic names. In case of the above example since I do not know before hand how many registers are going to be present, I need to count the data during run time and accordingly number the registers (eg. Register 1, Register 2 and so on). So I wanted to know if there is any way to achieve this.</p><p>Also I wanted to know if there is any way to optimize the Lua dissectors as I am dealing with large chunks of wireshark data and it takes a lot of time to apply a filter due to the dissector. I was wondering is there a way to precompile Lua dissectors so that they are not compiled every time a filter is applied.</p><p>Thank you in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-modbus" rel="tag" title="see questions tagged &#39;modbus&#39;">modbus</span> <span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '16, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/3aaad26a48e6f507d8f9137404269a46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shobhit_garg91&#39;s gravatar image" /><p><span>shobhit_garg91</span><br />
<span class="score" title="16 reputation points">16</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shobhit_garg91 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Aug '16, 04:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-54620" class="comments-container"><span id="54623"></span><div id="comment-54623" class="comment"><div id="post-54623-score" class="comment-score"></div><div class="comment-text"><p>I'm not posting this as an Answer as I haven't tested my suggestion of a workaround, it is just a rough idea.</p><p>Your question, although it is not obvious at first glance, is actually an extension of <a href="https://ask.wireshark.org/questions/50444/braces-inside-abbreviated-name-fieldabbrev-of-header_field_info">this one</a>, but apparently <span>@_michel</span> hasn't filed an enhancement request as <span>@Guy Harris</span> suggested back then.</p><p>There is currently no way to dynamically assign field names like <code>input_temperature</code>, <code>max_pressure</code> etc. to protocol fields because you have to register all potential field descriptions in advance. Nor there is currently a way to use indexed protocol fields like <code>register[3]</code>.</p><p>On the other hand, it is possible to register a field type once but hook to the dissection tree several instances of such field. All of them have the same name but their values differ.</p><p>So you may register a field type like <code>modbus.registerAVP</code> and let your dissector generate multiple instances of this field per PDU. The values of these fields would be composed of the dynamic name, a separator and the actual value, like <code>input_temperature=100</code>. This would allow you to use display filters like <code>modbus.registerAVP matches "^input_temperature"</code> but not to compare the actual values to constants (or to each other) for anything else than equality or non-equality.</p><p>To allow other types of value comparison, like <code>input_temperature &gt;= 50</code>, you would have to register pairs of <code>modbus.registerAVP.N.name</code>, <code>modbus.registerAVP.N.value</code> for N from, say, 1 to 10 (or 100 or maybe 1000), and then use complex display filters like <code>(modbus.registerAVP.1.name = "input_temperature" and modbus.registerAVP.1.value &gt;= 50) or (modbus.registerAVP.2.name = "input_temperature" and modbus.registerAVP.2.value &gt;= 50) or ...</code> So for more than 5 register values per PDU, already this (comparing the value of a field of a known name to a constant) would be a nightmare, and comparing values of two such fields to each other would be nearly impossible due to the exponential growth of the number of necessary filter primitives.</p><p>As your Question deals with modbus, you may also be interested in <a href="https://ask.wireshark.org/questions/50063/getting-register-values-from-modbustcp-response">this question</a>, but it seems <span>@Evan</span> hasn't filed an enhancement request either.</p><p>So maybe <strong>you</strong> will <a href="https://bugs.wireshark.org/bugzilla/enter_bug.cgi?product=Wireshark">file</a> them?</p></div><div id="comment-54623-info" class="comment-info"><span class="comment-age">(06 Aug '16, 03:39)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="54624"></span><div id="comment-54624" class="comment"><div id="post-54624-score" class="comment-score"></div><div class="comment-text"><p>But on the other hand, if you know in advance <strong>all</strong> the individual names you would use as aliases to the individual registers, you can register all of them and then choose always the appropriate one to hook it to the dissection tree.</p></div><div id="comment-54624-info" class="comment-info"><span class="comment-age">(06 Aug '16, 03:52)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="54830"></span><div id="comment-54830" class="comment"><div id="post-54830-score" class="comment-score"></div><div class="comment-text"><p>Hi Sindy, Thank you for your inputs. I have a question though. In your suggestion you mentioned that the field I create will contain a dynamic name, a separator and an actual value. I am not able to understand how to provide a dynamic name to the field. I am aware that the same field can be added to the dissector tree multiple times, but the name of the field remains the same in this case (the name of the field before the separator ":"). For now I am not keen on using the fields for filtering purposes, I just want to display them as having dynamic names (eg. Register 1: xxx, Register 2: xxx and so on. Here the numbers 1 and 2 are dynamic depending upon the number of register values present.) Please let me know what could be done in this regards.</p><p>Thanks, Shobhit Garg.</p></div><div id="comment-54830-info" class="comment-info"><span class="comment-age">(15 Aug '16, 07:55)</span> <span class="comment-user userinfo">shobhit_garg91</span></div></div><span id="54832"></span><div id="comment-54832" class="comment"><div id="post-54832-score" class="comment-score"></div><div class="comment-text"><p>What I had in mind is that a normal protocol field has a pre-declared name and (where required) a value. In the suggested case, the pre-declared name would be static, and the value would contain the dynamic name and the actual value in a single string, inside which the dynamic name part and actual value part would be distinguished by the means of a separator.</p><p>Example of the result in the dissection pane:</p><pre><code>modbus.registerAVP: input_temperature=100
modbus.registerAVP: max_pressure=20</code></pre><p>So here:</p><ul><li><p><code>modbus.registerAVP</code> is the static, "Wireshark", field name,</p></li><li><p><code>input_temperature=100</code> and <code>max_pressure=20</code> are the "Wireshark" values,</p></li><li><p><code>input_temperature</code> and <code>max_pressure</code> are the dynamic names,</p></li><li><p><code>=</code> is the separator,</p></li><li><p><code>100</code> and <code>20</code> are the actual values.</p></li></ul><p>Or, if we use the second way suggested:</p><pre><code>modbus.registerAVP.1.name: input_temperature
modbus.registerAVP.1.value: 100
modbus.registerAVP.2.name: max_pressure
modbus.registerAVP.2.value: 20</code></pre><ul><li><p><code>modbus.registerAVP.1.name</code>, <code>modbus.registerAVP.1.value</code>, <code>modbus.registerAVP.2.name</code>, and <code>modbus.registerAVP.2.value</code> are "Wireshark" names,</p></li><li><p>values of <code>modbus.registerAVP.X.name</code> are the dynamic names,</p></li><li>values of <code>modbus.registerAVP.X.value</code> are the actual values.</li></ul></div><div id="comment-54832-info" class="comment-info"><span class="comment-age">(15 Aug '16, 08:12)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-54620" class="comment-tools"></div><div class="clear"></div><div id="comment-54620-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

