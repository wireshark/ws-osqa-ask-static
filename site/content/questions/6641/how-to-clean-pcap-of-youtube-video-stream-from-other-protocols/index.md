+++
type = "question"
title = "How to clean pcap of Youtube video stream from other protocols."
description = '''I would like to filter only video stream from captured pcap of the youtube client/server comunication. The problem is there are also other HTTP requestes (images,..).'''
date = "2011-09-29T10:59:00Z"
lastmod = "2011-09-29T11:20:00Z"
weight = 6641
keywords = [ "filter", "capture", "youtube" ]
aliases = [ "/questions/6641" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to clean pcap of Youtube video stream from other protocols.](/questions/6641/how-to-clean-pcap-of-youtube-video-stream-from-other-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6641-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6641-score" class="post-score" title="current number of votes">0</div><span id="post-6641-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to filter only video stream from captured pcap of the youtube client/server comunication. The problem is there are also other HTTP requestes (images,..).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-youtube" rel="tag" title="see questions tagged &#39;youtube&#39;">youtube</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '11, 10:59</strong></p><img src="https://secure.gravatar.com/avatar/447b27791b5d25a8bb0c866e8cde3cc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bananajoe&#39;s gravatar image" /><p><span>bananajoe</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bananajoe has no accepted answers">0%</span></p></div></div><div id="comments-container-6641" class="comments-container"><span id="6642"></span><div id="comment-6642" class="comment"><div id="post-6642-score" class="comment-score"></div><div class="comment-text"><p>Take a look at this question:<br />
<a href="http://ask.wireshark.org/questions/5325/how-to-extract-flv-video-from-capture-pakets?page=1#5353">How to extract flv video from capture pakets.</a></p></div><div id="comment-6642-info" class="comment-info"><span class="comment-age">(29 Sep '11, 11:20)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-6641" class="comment-tools"></div><div class="clear"></div><div id="comment-6641-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

