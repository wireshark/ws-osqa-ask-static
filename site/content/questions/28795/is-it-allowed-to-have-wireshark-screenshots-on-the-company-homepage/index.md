+++
type = "question"
title = "Is it allowed to have wireshark screenshots on the company homepage?"
description = '''Hello Sirs, is it allowed to make screenshots from traces captured and displayed with wireshark and put them on a company&#x27;s homepage? And is it okay to use screenshots from wireshark logs/traces in some training material? Please advise. kind regards, Stefan B.'''
date = "2014-01-11T07:20:00Z"
lastmod = "2014-01-12T02:50:00Z"
weight = 28795
keywords = [ "published", "screeenshots", "wireshark" ]
aliases = [ "/questions/28795" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Is it allowed to have wireshark screenshots on the company homepage?](/questions/28795/is-it-allowed-to-have-wireshark-screenshots-on-the-company-homepage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28795-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28795-score" class="post-score" title="current number of votes">0</div><span id="post-28795-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Sirs,</p><p>is it allowed to make screenshots from traces captured and displayed with wireshark and put them on a company's homepage? And is it okay to use screenshots from wireshark logs/traces in some training material?</p><p>Please advise.</p><p>kind regards,</p><p>Stefan B.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-published" rel="tag" title="see questions tagged &#39;published&#39;">published</span> <span class="post-tag tag-link-screeenshots" rel="tag" title="see questions tagged &#39;screeenshots&#39;">screeenshots</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '14, 07:20</strong></p><img src="https://secure.gravatar.com/avatar/2664f53cd0708e5b121ed29c8a854086?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stefanblomeier&#39;s gravatar image" /><p><span>stefanblomeier</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stefanblomeier has no accepted answers">0%</span></p></div></div><div id="comments-container-28795" class="comments-container"></div><div id="comment-tools-28795" class="comment-tools"></div><div class="clear"></div><div id="comment-28795-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="28798"></span>

<div id="answer-container-28798" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28798-score" class="post-score" title="current number of votes">1</div><span id="post-28798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="stefanblomeier has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I can't see why not. I'm not a lawyer (thank god :-)), but I've seen Wireshark screenshots used in a lot of training materials (including my own classes I wrote), and nobody ever had a problem with it. The logs (captured packets) are yours anyway if they were taken on your network. So don't worry, just go ahead.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '14, 08:01</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28798" class="comments-container"><span id="28807"></span><div id="comment-28807" class="comment"><div id="post-28807-score" class="comment-score"></div><div class="comment-text"><p>thanks Jasper. I made tracing in differnet networks - not my private network at home. So I grey-out and cover all IP-addresses or Names which could give hint to a certain network. Wireshark Logo I do not use in the screenshots. I just say the trace has been recorded and displayed with Wireshark tool.</p></div><div id="comment-28807-info" class="comment-info"><span class="comment-age">(12 Jan '14, 01:29)</span> <span class="comment-user userinfo">stefanblomeier</span></div></div><span id="28810"></span><div id="comment-28810" class="comment"><div id="post-28810-score" class="comment-score"></div><div class="comment-text"><p>Greying out adddresses for screenshots is one option, but have you thought about anonymizing them instead? You could use TraceWrangler for that: <a href="http://www.tracewrangler.com">http://www.tracewrangler.com</a>.</p></div><div id="comment-28810-info" class="comment-info"><span class="comment-age">(12 Jan '14, 02:50)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-28798" class="comment-tools"></div><div class="clear"></div><div id="comment-28798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="28800"></span>

<div id="answer-container-28800" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28800-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28800-score" class="post-score" title="current number of votes">0</div><span id="post-28800-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wiresharks license is the GPL. There is nothing in the GPL that disallows the use of a screenshot of a GPL licensed product, even in a commercial context.</p><p><strong>HOWEVER</strong> the Wireshark logo (the 'fin' symbol) and the name <strong>Wireshark</strong> are registered trademarks of the Wireshark foundation.</p><p>See <strong>Trademarks</strong> here: <a href="http://www.wireshark.org/about.html">http://www.wireshark.org/about.html</a></p><p>So, if your screenshot shows either of those (logo or name) in a commercial context, you should have the permission of the Wireshark foundation for that (just in case ...)! Please contact <span>@Gerald Combs</span> and discuss the issue with him, or wait if he jumps into the discussion here.</p><p>See: <a href="http://www.wireshark.org/about.html">http://www.wireshark.org/about.html</a> --&gt; Authors --&gt; Original Author</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '14, 11:09</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28800" class="comments-container"><span id="28802"></span><div id="comment-28802" class="comment"><div id="post-28802-score" class="comment-score"></div><div class="comment-text"><p><span>@Gerald Combs</span>: Are there any written 'rules' regarding the use of the logo and the name in</p><ul><li>open source context</li><li>private / public context</li><li>commercial context</li></ul></div><div id="comment-28802-info" class="comment-info"><span class="comment-age">(11 Jan '14, 11:14)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28808"></span><div id="comment-28808" class="comment"><div id="post-28808-score" class="comment-score"></div><div class="comment-text"><p>I do not intend to use the fin-symbol of wireshark. I just provide a reference for the trace and say under the screenshot: "recorded and displayed by wireshark" so the folks know that the wireshark tool was used to capture trace and show it before I took a screenshot and put it on my presentation. Important IP-addresses or Network Names etc. which would reveal the Network I am going to grey out (cover them) to guarantee anonymity.<br />
</p><p>kind regards, Stefan</p></div><div id="comment-28808-info" class="comment-info"><span class="comment-age">(12 Jan '14, 01:37)</span> <span class="comment-user userinfo">stefanblomeier</span></div></div><span id="28809"></span><div id="comment-28809" class="comment"><div id="post-28809-score" class="comment-score"></div><div class="comment-text"><p>Well, then I see no restrictions.</p></div><div id="comment-28809-info" class="comment-info"><span class="comment-age">(12 Jan '14, 02:05)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28800" class="comment-tools"></div><div class="clear"></div><div id="comment-28800-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

