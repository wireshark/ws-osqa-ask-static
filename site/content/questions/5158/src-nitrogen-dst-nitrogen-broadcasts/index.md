+++
type = "question"
title = "Src =Nitrogen DST =nitrogen broadcasts"
description = '''Doing a sniff of only broadcasts during a network slowdown, About 20% of my broadcasts are src=Nitrogen DST =nitrogen. An hour on Google produced nothing any ideas? '''
date = "2011-07-21T18:46:00Z"
lastmod = "2011-07-21T19:16:00Z"
weight = 5158
keywords = [ "src", "nitrogen" ]
aliases = [ "/questions/5158" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Src =Nitrogen DST =nitrogen broadcasts](/questions/5158/src-nitrogen-dst-nitrogen-broadcasts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5158-score" class="post-score" title="current number of votes">0</div><span id="post-5158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Doing a sniff of only broadcasts during a network slowdown, About 20% of my broadcasts are src=Nitrogen DST =nitrogen. An hour on Google produced nothing any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-src" rel="tag" title="see questions tagged &#39;src&#39;">src</span> <span class="post-tag tag-link-nitrogen" rel="tag" title="see questions tagged &#39;nitrogen&#39;">nitrogen</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '11, 18:46</strong></p><img src="https://secure.gravatar.com/avatar/6858c9d19c751e96ba85008687e6c63e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mstrfl2000&#39;s gravatar image" /><p><span>mstrfl2000</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mstrfl2000 has no accepted answers">0%</span></p></div></div><div id="comments-container-5158" class="comments-container"><span id="5159"></span><div id="comment-5159" class="comment"><div id="post-5159-score" class="comment-score"></div><div class="comment-text"><p>Please provide a more info:</p><p>Is that SRC/DST <em>address</em> or SRC/DST <em>port</em> ?</p><p>What protocols are seen ?</p><p>Ethernet Broadcast ? IP Broadcast ?</p><p>etc</p></div><div id="comment-5159-info" class="comment-info"><span class="comment-age">(21 Jul '11, 19:16)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-5158" class="comment-tools"></div><div class="clear"></div><div id="comment-5158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

