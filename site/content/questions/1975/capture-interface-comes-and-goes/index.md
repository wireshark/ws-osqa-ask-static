+++
type = "question"
title = "Capture interface &quot;comes and goes&quot;"
description = '''My HP Netbook has begun not displaying the capture interface intermittently. (I love &quot;intermittents&quot;.) I have tried restarting, reinstalling Wireshark, reinstalling WinPCap, back-reving Wireshark and WinPCap. I&#x27;ve tried to &quot;browse&quot; to an Internet site before opening Wireshark, but so far, nothing wo...'''
date = "2011-01-27T10:19:00Z"
lastmod = "2011-01-27T10:19:00Z"
weight = 1975
keywords = [ "interface", "capture", "display", "intermittent" ]
aliases = [ "/questions/1975" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture interface "comes and goes"](/questions/1975/capture-interface-comes-and-goes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1975-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1975-score" class="post-score" title="current number of votes">0</div><span id="post-1975-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My HP Netbook has begun not displaying the capture interface intermittently. (I love "intermittents".) I have tried restarting, reinstalling Wireshark, reinstalling WinPCap, back-reving Wireshark and WinPCap. I've tried to "browse" to an Internet site before opening Wireshark, but so far, nothing works consistently. I have browsed with this Netbook on this wireless interface for over a year just fine with no problems.</p><p>There is nothing wrong with my browsing sessions...I can go wherever I want. The problem is only getting Wireshark to see my wireless interface every time I open up Wireshark. I have a wireless USB adapter which I installed, and Wireshark saw that interface fine, as well as my wired interface.</p><p>Help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-intermittent" rel="tag" title="see questions tagged &#39;intermittent&#39;">intermittent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '11, 10:19</strong></p><img src="https://secure.gravatar.com/avatar/77775825024c2262319e5bc6a1072710?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LAllen&#39;s gravatar image" /><p><span>LAllen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LAllen has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jan '11, 10:23</strong> </span></p></div></div><div id="comments-container-1975" class="comments-container"></div><div id="comment-tools-1975" class="comment-tools"></div><div class="clear"></div><div id="comment-1975-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

