+++
type = "question"
title = "More columns in endpoint statistics"
description = '''Hi, Is it possible to add more or edit/remove columns in Statistic-&amp;gt;Endpoints-&amp;gt;UDP? Are there column preferences somewhere for these columns? Thanks.'''
date = "2013-05-29T10:07:00Z"
lastmod = "2013-05-29T10:11:00Z"
weight = 21577
keywords = [ "udp", "endpoints", "columns", "statisticss" ]
aliases = [ "/questions/21577" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [More columns in endpoint statistics](/questions/21577/more-columns-in-endpoint-statistics)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21577-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21577-score" class="post-score" title="current number of votes">0</div><span id="post-21577-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is it possible to add more or edit/remove columns in Statistic-&gt;Endpoints-&gt;UDP? Are there column preferences somewhere for these columns? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-endpoints" rel="tag" title="see questions tagged &#39;endpoints&#39;">endpoints</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span> <span class="post-tag tag-link-statisticss" rel="tag" title="see questions tagged &#39;statisticss&#39;">statisticss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '13, 10:07</strong></p><img src="https://secure.gravatar.com/avatar/ad04d74b8276831dece62b84eb7cfa84?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nseq&#39;s gravatar image" /><p><span>nseq</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nseq has no accepted answers">0%</span></p></div></div><div id="comments-container-21577" class="comments-container"></div><div id="comment-tools-21577" class="comment-tools"></div><div class="clear"></div><div id="comment-21577-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21578"></span>

<div id="answer-container-21578" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21578-score" class="post-score" title="current number of votes">0</div><span id="post-21578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, it isn't possible through any column preference, but other columns could be added if they are useful and someone is willing to make the necessary code changes. What columns are you looking for exactly?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '13, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-21578" class="comments-container"></div><div id="comment-tools-21578" class="comment-tools"></div><div class="clear"></div><div id="comment-21578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

