+++
type = "question"
title = "Clear packet list on a live capture?"
description = '''Pretty simple question. I&#x27;m running 1.12.5 on Windows. Is there a way to clear the packet list on a live capture? I often find myself having to stop/start the trace in order to clear the packet list, and I have optimized this process down to a single click per operation. But I also run the risk of m...'''
date = "2015-06-18T08:17:00Z"
lastmod = "2015-06-18T09:00:00Z"
weight = 43331
keywords = [ "list", "feature-request", "packet" ]
aliases = [ "/questions/43331" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Clear packet list on a live capture?](/questions/43331/clear-packet-list-on-a-live-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43331-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43331-score" class="post-score" title="current number of votes">0</div><span id="post-43331-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Pretty simple question. I'm running 1.12.5 on Windows. Is there a way to clear the packet list on a live capture? I often find myself having to stop/start the trace in order to clear the packet list, and I have optimized this process down to a single click per operation. But I also run the risk of missing interesting packets if the timing is unfortunate. Also, this is an annoying number of clicks when I have the "Confirm unsaved capture files feature enabled". I want to keep this enabled for my own protection, but it would be nice to have a simple button (or key combination) to perform this action in one step so I don't risk missing any packets.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-list" rel="tag" title="see questions tagged &#39;list&#39;">list</span> <span class="post-tag tag-link-feature-request" rel="tag" title="see questions tagged &#39;feature-request&#39;">feature-request</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '15, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/32272e9efae0156b7a71e9b634428d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smp&#39;s gravatar image" /><p><span>smp</span><br />
<span class="score" title="39 reputation points">39</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jun '15, 08:20</strong> </span></p></div></div><div id="comments-container-43331" class="comments-container"></div><div id="comment-tools-43331" class="comment-tools"></div><div class="clear"></div><div id="comment-43331-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43332"></span>

<div id="answer-container-43332" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43332-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43332-score" class="post-score" title="current number of votes">2</div><span id="post-43332-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="smp has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you tried the "Restart running capture" button next to the Stop button? Clears the display and restarts the capture with no "save" prompt. Might still miss a packet though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '15, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-43332" class="comments-container"><span id="43333"></span><div id="comment-43333" class="comment"><div id="post-43333-score" class="comment-score"></div><div class="comment-text"><p>Um, no, embarrassed to say I haven't noticed that button before. That certainly does make it more efficient, and I will use it. I would like to see this feature, but I think the restart button is good enough for the majority of cases. Thanks!</p></div><div id="comment-43333-info" class="comment-info"><span class="comment-age">(18 Jun '15, 08:32)</span> <span class="comment-user userinfo">smp</span></div></div><span id="43334"></span><div id="comment-43334" class="comment"><div id="post-43334-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p><p>To make a feature request, head over to the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a>, check in case a similar request already exists, and if not add a new one, marking it as an enhancement.</p></div><div id="comment-43334-info" class="comment-info"><span class="comment-age">(18 Jun '15, 09:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-43332" class="comment-tools"></div><div class="clear"></div><div id="comment-43332-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

