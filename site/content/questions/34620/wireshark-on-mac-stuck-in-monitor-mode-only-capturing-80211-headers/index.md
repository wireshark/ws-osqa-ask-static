+++
type = "question"
title = "Wireshark (on Mac) stuck in monitor mode only capturing 802.11 headers"
description = '''My wireshark ( Version 1.10.8 (v1.10.8-2-g52a5244 from master-1.10) on my macbook (OSX 10.7.5) seems to be permanently stuck on monitor mode for my En1 (wifi) interface. Wireshark insists on capturing &#x27;802.11 plus radiotap header&#x27; link layer headers. This happens regardless of me setting the interfa...'''
date = "2014-07-13T04:46:00Z"
lastmod = "2014-07-13T04:46:00Z"
weight = 34620
keywords = [ "mac", "802.11", "monitor-mode" ]
aliases = [ "/questions/34620" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark (on Mac) stuck in monitor mode only capturing 802.11 headers](/questions/34620/wireshark-on-mac-stuck-in-monitor-mode-only-capturing-80211-headers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34620-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34620-score" class="post-score" title="current number of votes">0</div><span id="post-34620-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My wireshark ( Version 1.10.8 (v1.10.8-2-g52a5244 from master-1.10) on my macbook (OSX 10.7.5) seems to be permanently stuck on monitor mode for my En1 (wifi) interface.</p><p>Wireshark insists on capturing '802.11 plus radiotap header' link layer headers. This happens regardless of me setting the interface prefs to un-check monitor mode and capture ethernet link layers.</p><p>When I run the capture, it sets the wifi to monitor mode and only captures the 802.11 protocol headers. Wireshark seems to be stuck in this mode.</p><p>This seems like a bug?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '14, 04:46</strong></p><img src="https://secure.gravatar.com/avatar/f4a85918a51c0033b1c579f4dd207f87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Specialist&#39;s gravatar image" /><p><span>Specialist</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Specialist has no accepted answers">0%</span></p></div></div><div id="comments-container-34620" class="comments-container"></div><div id="comment-tools-34620" class="comment-tools"></div><div class="clear"></div><div id="comment-34620-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

