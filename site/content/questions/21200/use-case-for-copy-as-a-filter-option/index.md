+++
type = "question"
title = "use case for (copy | as a filter )option"
description = '''Going through Laura chappel&#x27;s WCNA guide and came across copy | as filter option. Any use case for this feature is appreciated.When do we need to buffer the filter?'''
date = "2013-05-16T22:04:00Z"
lastmod = "2013-05-17T01:40:00Z"
weight = 21200
keywords = [ "display", "filters" ]
aliases = [ "/questions/21200" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [use case for (copy | as a filter )option](/questions/21200/use-case-for-copy-as-a-filter-option)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21200-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21200-score" class="post-score" title="current number of votes">0</div><span id="post-21200-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Going through Laura chappel's WCNA guide and came across copy | as filter option. Any use case for this feature is appreciated.When do we need to buffer the filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '13, 22:04</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-21200" class="comments-container"></div><div id="comment-tools-21200" class="comment-tools"></div><div class="clear"></div><div id="comment-21200-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21204"></span>

<div id="answer-container-21204" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21204-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21204-score" class="post-score" title="current number of votes">1</div><span id="post-21204-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="krishnayeddula has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I can think of many use cases, but the one I had that made me develop this functionality was this:</p><p>I was analyzing client side and server side traces of a session over an Alteon loadbalancer. When matching a session on the client side to the session on the server side, I had to use the tcp sequence number. So what I did was prepare a filter on the sequence number, and then copied it from the filter box to put it in the search filter of the other wireshark session. Having "Copy as filter" made it a lot easier :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 May '13, 00:00</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-21204" class="comments-container"><span id="21210"></span><div id="comment-21210" class="comment"><div id="post-21210-score" class="comment-score"></div><div class="comment-text"><p>Thats why I use it too, it helps searching with the find dialog when you dont want to replace the display filter.</p></div><div id="comment-21210-info" class="comment-info"><span class="comment-age">(17 May '13, 01:40)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-21204" class="comment-tools"></div><div class="clear"></div><div id="comment-21204-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

