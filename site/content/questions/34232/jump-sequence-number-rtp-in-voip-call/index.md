+++
type = "question"
title = "Jump Sequence Number Rtp in Voip Call"
description = '''Observing a communication protocol rtp (wireshark) on a VoIP connection, I realized that during the data transmission sequence number value jumped from 292 to 14237 after mark. Could this have happened because there was a change in the transmission route! Or what would be the cause of this? '''
date = "2014-06-26T11:33:00Z"
lastmod = "2014-06-27T06:58:00Z"
weight = 34232
keywords = [ "rtp", "voip" ]
aliases = [ "/questions/34232" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Jump Sequence Number Rtp in Voip Call](/questions/34232/jump-sequence-number-rtp-in-voip-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34232-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34232-score" class="post-score" title="current number of votes">0</div><span id="post-34232-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Observing a communication protocol rtp (wireshark) on a VoIP connection, I realized that during the data transmission sequence number value jumped from 292 to 14237 after mark. Could this have happened because there was a change in the transmission route! Or what would be the cause of this?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/jump_seq.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '14, 11:33</strong></p><img src="https://secure.gravatar.com/avatar/a79d778ac3ee711846e97e458ffa6964?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="julius&#39;s gravatar image" /><p><span>julius</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="julius has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jun '14, 04:16</strong> </span></p></div></div><div id="comments-container-34232" class="comments-container"></div><div id="comment-tools-34232" class="comment-tools"></div><div class="clear"></div><div id="comment-34232-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34241"></span>

<div id="answer-container-34241" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34241-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34241-score" class="post-score" title="current number of votes">1</div><span id="post-34241-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>They have different SSRC so it shouldn't be the same stream.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '14, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-34241" class="comments-container"></div><div id="comment-tools-34241" class="comment-tools"></div><div class="clear"></div><div id="comment-34241-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

