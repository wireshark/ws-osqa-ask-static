+++
type = "question"
title = "Can I change the default font color?"
description = '''Hello, Does anyone know if I can change the font color of a selected packet in Wireshark? I am on a Mac, and when I select a packet the font turns white, and the background turns a light blue, which makes it difficult to see packet data, or description. I&#x27;d like keep the font color black, or somethi...'''
date = "2013-01-15T11:58:00Z"
lastmod = "2013-01-15T11:58:00Z"
weight = 17705
keywords = [ "wireshark" ]
aliases = [ "/questions/17705" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I change the default font color?](/questions/17705/can-i-change-the-default-font-color)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17705-score" class="post-score" title="current number of votes">1</div><span id="post-17705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Does anyone know if I can change the font color of a selected packet in Wireshark? I am on a Mac, and when I select a packet the font turns white, and the background turns a light blue, which makes it difficult to see packet data, or description.</p><p>I'd like keep the font color black, or something more contrasting in the packet list, packet details, and packet byte windows.</p><p>Sorry should have added some info... Plateform: MBP OS: OS X Mountain Lion ver 10.8.2 X11: XQuartz</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '13, 11:58</strong></p><img src="https://secure.gravatar.com/avatar/f0788bfe4816cb9dad99b27f23860acf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="newman71&#39;s gravatar image" /><p><span>newman71</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="newman71 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '13, 12:03</strong> </span></p></div></div><div id="comments-container-17705" class="comments-container"></div><div id="comment-tools-17705" class="comment-tools"></div><div class="clear"></div><div id="comment-17705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

