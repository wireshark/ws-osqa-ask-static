+++
type = "question"
title = "What would be some good examles for filtering those watching movies/videos on a network?"
description = '''Would appreciate some examples for filters that show video/movie watching traffic. '''
date = "2016-02-01T20:54:00Z"
lastmod = "2016-02-01T20:54:00Z"
weight = 49707
keywords = [ "filter", "movie", "video" ]
aliases = [ "/questions/49707" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What would be some good examles for filtering those watching movies/videos on a network?](/questions/49707/what-would-be-some-good-examles-for-filtering-those-watching-moviesvideos-on-a-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49707-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49707-score" class="post-score" title="current number of votes">0</div><span id="post-49707-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Would appreciate some examples for filters that show video/movie watching traffic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-movie" rel="tag" title="see questions tagged &#39;movie&#39;">movie</span> <span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '16, 20:54</strong></p><img src="https://secure.gravatar.com/avatar/a2549461515cdf36c6b8acb74277580e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NetSpeedz&#39;s gravatar image" /><p><span>NetSpeedz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NetSpeedz has no accepted answers">0%</span></p></div></div><div id="comments-container-49707" class="comments-container"></div><div id="comment-tools-49707" class="comment-tools"></div><div class="clear"></div><div id="comment-49707-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

