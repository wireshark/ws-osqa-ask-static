+++
type = "question"
title = "[closed] Which is the best protocol to test retransmissions over a wan link?"
description = '''Hi, which is the best protocolo for test the number of retransmissions over a wan link? I did a test copying a file vía shared folders between a windows server 2008 and a windows seven client, but the first day the 2 mb file got transfered in 4 seconds without retransmissions and the second day ther...'''
date = "2015-01-18T09:15:00Z"
lastmod = "2015-01-18T09:15:00Z"
weight = 39248
keywords = [ "wan", "retransmissions" ]
aliases = [ "/questions/39248" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Which is the best protocol to test retransmissions over a wan link?](/questions/39248/which-is-the-best-protocol-to-test-retransmissions-over-a-wan-link)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39248-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39248-score" class="post-score" title="current number of votes">0</div><span id="post-39248-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, which is the best protocolo for test the number of retransmissions over a wan link? I did a test copying a file vía shared folders between a windows server 2008 and a windows seven client, but the first day the 2 mb file got transfered in 4 seconds without retransmissions and the second day there where like 25% number of packets utilized for retransmissions mechanisms (dup acks, retranmsmissions, fast retransmissions), it was not a bandwith problem. I didn't know that SMB is not the best protocol for WAN transfers, but my cuestion is that if I want to check the posible number of retransmissions over a WAN, wan protocol I can use to take those captures?</p><p>Regards!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wan" rel="tag" title="see questions tagged &#39;wan&#39;">wan</span> <span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '15, 09:15</strong></p><img src="https://secure.gravatar.com/avatar/c350038a7dd33938cf13107a22cfb311?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ogoname&#39;s gravatar image" /><p><span>ogoname</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ogoname has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Jan '15, 07:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-39248" class="comments-container"></div><div id="comment-tools-39248" class="comment-tools"></div><div class="clear"></div><div id="comment-39248-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 19 Jan '15, 07:35

</div>

</div>

</div>

