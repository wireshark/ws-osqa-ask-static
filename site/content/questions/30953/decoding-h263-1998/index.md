+++
type = "question"
title = "Decoding H263-1998"
description = '''Hi all!  I am trying to decode and save video stream that I have captured with wireshark. Video is coded with H.263-1998, and when I go to Voip call-&amp;gt;player-&amp;gt;decode, I can play only audio. I have tried also &quot;decode as..&quot; RTP, and doesnt work ether... Is this possible? and How?'''
date = "2014-03-19T08:32:00Z"
lastmod = "2014-03-19T08:32:00Z"
weight = 30953
keywords = [ "decode", "videostream", "rtp", "voip" ]
aliases = [ "/questions/30953" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding H263-1998](/questions/30953/decoding-h263-1998)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30953-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30953-score" class="post-score" title="current number of votes">0</div><span id="post-30953-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all!</p><p>I am trying to decode and save video stream that I have captured with wireshark. Video is coded with H.263-1998, and when I go to Voip call-&gt;player-&gt;decode, I can play only audio. I have tried also "decode as.." RTP, and doesnt work ether... Is this possible? and How?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '14, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/9ffb99501dbc9321e81f85e46274095e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stevie&#39;s gravatar image" /><p><span>stevie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stevie has no accepted answers">0%</span></p></div></div><div id="comments-container-30953" class="comments-container"></div><div id="comment-tools-30953" class="comment-tools"></div><div class="clear"></div><div id="comment-30953-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

