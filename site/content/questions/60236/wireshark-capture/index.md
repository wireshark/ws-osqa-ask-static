+++
type = "question"
title = "Wireshark capture"
description = '''Hi, I have been given a wireshark capture to find illegal communications the packet count is 348,464 and most of the protocols are http,dns and tcp what is the best way to filter through the capture to look for messages and communication?  Thank you'''
date = "2017-03-21T13:02:00Z"
lastmod = "2017-03-21T13:16:00Z"
weight = 60236
keywords = [ "wireshark" ]
aliases = [ "/questions/60236" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark capture](/questions/60236/wireshark-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60236-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60236-score" class="post-score" title="current number of votes">0</div><span id="post-60236-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have been given a wireshark capture to find illegal communications the packet count is 348,464 and most of the protocols are http,dns and tcp what is the best way to filter through the capture to look for messages and communication?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '17, 13:02</strong></p><img src="https://secure.gravatar.com/avatar/9331fbc9e5ad1aec6c88229c257e2565?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emma123&#39;s gravatar image" /><p><span>emma123</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emma123 has no accepted answers">0%</span></p></div></div><div id="comments-container-60236" class="comments-container"><span id="60237"></span><div id="comment-60237" class="comment"><div id="post-60237-score" class="comment-score"></div><div class="comment-text"><p>In this case try to use networkminer.</p></div><div id="comment-60237-info" class="comment-info"><span class="comment-age">(21 Mar '17, 13:08)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="60238"></span><div id="comment-60238" class="comment"><div id="post-60238-score" class="comment-score"></div><div class="comment-text"><p>Hi, it says error opening PCAP file. The stream is not a PCAP file.</p></div><div id="comment-60238-info" class="comment-info"><span class="comment-age">(21 Mar '17, 13:16)</span> <span class="comment-user userinfo">emma123</span></div></div></div><div id="comment-tools-60236" class="comment-tools"></div><div class="clear"></div><div id="comment-60236-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

