+++
type = "question"
title = "How to check WeChat messages?"
description = '''what filter will enable checking wechat messages?'''
date = "2015-01-16T10:41:00Z"
lastmod = "2015-01-16T10:41:00Z"
weight = 39217
keywords = [ "messages", "message", "wechat", "instant-messaging" ]
aliases = [ "/questions/39217" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to check WeChat messages?](/questions/39217/how-to-check-wechat-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39217-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39217-score" class="post-score" title="current number of votes">0</div><span id="post-39217-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what filter will enable checking wechat messages?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-messages" rel="tag" title="see questions tagged &#39;messages&#39;">messages</span> <span class="post-tag tag-link-message" rel="tag" title="see questions tagged &#39;message&#39;">message</span> <span class="post-tag tag-link-wechat" rel="tag" title="see questions tagged &#39;wechat&#39;">wechat</span> <span class="post-tag tag-link-instant-messaging" rel="tag" title="see questions tagged &#39;instant-messaging&#39;">instant-messaging</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '15, 10:41</strong></p><img src="https://secure.gravatar.com/avatar/7240aa18eca54ad97ed0ebaa18f7cabe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gulbalee&#39;s gravatar image" /><p><span>gulbalee</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gulbalee has no accepted answers">0%</span></p></div></div><div id="comments-container-39217" class="comments-container"></div><div id="comment-tools-39217" class="comment-tools"></div><div class="clear"></div><div id="comment-39217-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

