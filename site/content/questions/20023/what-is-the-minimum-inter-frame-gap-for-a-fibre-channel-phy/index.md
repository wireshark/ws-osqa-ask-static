+++
type = "question"
title = "What is the minimum inter frame gap for a fibre channel phy?"
description = '''I saw some document saying that the minimum inter frame gap is 20 bytes but some say it is 24 bytes (I assume it include the SOF). Just wonder what is the standard minimum inter frame gap for a fibre channel phy? Thanks.'''
date = "2013-04-02T09:59:00Z"
lastmod = "2013-04-29T10:54:00Z"
weight = 20023
keywords = [ "fibre-channel" ]
aliases = [ "/questions/20023" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What is the minimum inter frame gap for a fibre channel phy?](/questions/20023/what-is-the-minimum-inter-frame-gap-for-a-fibre-channel-phy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20023-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20023-score" class="post-score" title="current number of votes">0</div><span id="post-20023-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I saw some document saying that the minimum inter frame gap is 20 bytes but some say it is 24 bytes (I assume it include the SOF). Just wonder what is the standard minimum inter frame gap for a fibre channel phy?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fibre-channel" rel="tag" title="see questions tagged &#39;fibre-channel&#39;">fibre-channel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '13, 09:59</strong></p><img src="https://secure.gravatar.com/avatar/d92822259e255fc18dcce53ae5403331?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kintaro&#39;s gravatar image" /><p><span>kintaro</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kintaro has no accepted answers">0%</span></p></div></div><div id="comments-container-20023" class="comments-container"></div><div id="comment-tools-20023" class="comment-tools"></div><div class="clear"></div><div id="comment-20023-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20842"></span>

<div id="answer-container-20842" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20842-score" class="post-score" title="current number of votes">0</div><span id="post-20842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The minimum inter-frame gap, 9.6 microsecond (µS)) and a 8 byte "preamble + SOF However for <strong>Gigabit ethernet</strong> the gap is .096 usec. At 10 Mbps this corresponds to about the time it takes for 12 bytes to be transmitted.</p><p><a href="http://www.infocellar.com/networks/ethernet/frame.htm">http://www.infocellar.com/networks/ethernet/frame.htm</a></p><p>Hope this is helpful, John</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Apr '13, 10:54</strong></p><img src="https://secure.gravatar.com/avatar/1f3966b6e9de3a63326e2d3fd51c8c04?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John_Modlin&#39;s gravatar image" /><p><span>John_Modlin</span><br />
<span class="score" title="120 reputation points">120</span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John_Modlin has no accepted answers">0%</span></p></div></div><div id="comments-container-20842" class="comments-container"></div><div id="comment-tools-20842" class="comment-tools"></div><div class="clear"></div><div id="comment-20842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

