+++
type = "question"
title = "Is there a basic guide on how to monitor an application?"
description = '''Hi. I have installed wireshack and would like to find a basic guide which will tell me how I can monitor specific applications such as Skype, Pidgin etc. The goal is to see if these applications are leaking DNS or my IP address, even with a VPN being used. Is there a basic guide on how I can achieve...'''
date = "2017-03-07T10:00:00Z"
lastmod = "2017-03-07T14:23:00Z"
weight = 59895
keywords = [ "monitor" ]
aliases = [ "/questions/59895" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a basic guide on how to monitor an application?](/questions/59895/is-there-a-basic-guide-on-how-to-monitor-an-application)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59895-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59895-score" class="post-score" title="current number of votes">0</div><span id="post-59895-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. I have installed wireshack and would like to find a basic guide which will tell me how I can monitor specific applications such as Skype, Pidgin etc. The goal is to see if these applications are leaking DNS or my IP address, even with a VPN being used. Is there a basic guide on how I can achieve this in Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Mar '17, 10:00</strong></p><img src="https://secure.gravatar.com/avatar/d69e6b687757a878d7ed7860a0c0eed9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Soldier&#39;s gravatar image" /><p><span>Soldier</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Soldier has no accepted answers">0%</span></p></div></div><div id="comments-container-59895" class="comments-container"></div><div id="comment-tools-59895" class="comment-tools"></div><div class="clear"></div><div id="comment-59895-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59903"></span>

<div id="answer-container-59903" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59903-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59903-score" class="post-score" title="current number of votes">0</div><span id="post-59903-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't believe wireshark can filter based on the process, since it only looks at what happens on the network. You might be able to use <a href="https://technet.microsoft.com/en-us/sysinternals/bb896653">process explorer</a> or <a href="https://blogs.technet.microsoft.com/netmon/p/downloads/">network monitor</a> if you're on windows</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Mar '17, 14:23</strong></p><img src="https://secure.gravatar.com/avatar/e499b21439c9e939e139b329ffffeb20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rf5566&#39;s gravatar image" /><p><span>rf5566</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rf5566 has no accepted answers">0%</span></p></div></div><div id="comments-container-59903" class="comments-container"></div><div id="comment-tools-59903" class="comment-tools"></div><div class="clear"></div><div id="comment-59903-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

