+++
type = "question"
title = "Get the information about which program or application create the traffic"
description = '''I would like to know if there is a way to capture which application or program create the traffic?'''
date = "2015-11-04T09:56:00Z"
lastmod = "2015-11-04T10:37:00Z"
weight = 47253
keywords = [ "program" ]
aliases = [ "/questions/47253" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Get the information about which program or application create the traffic](/questions/47253/get-the-information-about-which-program-or-application-create-the-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47253-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47253-score" class="post-score" title="current number of votes">0</div><span id="post-47253-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if there is a way to capture which application or program create the traffic?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-program" rel="tag" title="see questions tagged &#39;program&#39;">program</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '15, 09:56</strong></p><img src="https://secure.gravatar.com/avatar/7a1af24c5c977cf5fa7107eb490017b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John%20Ma&#39;s gravatar image" /><p><span>John Ma</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John Ma has no accepted answers">0%</span></p></div></div><div id="comments-container-47253" class="comments-container"></div><div id="comment-tools-47253" class="comment-tools"></div><div class="clear"></div><div id="comment-47253-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47256"></span>

<div id="answer-container-47256" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47256-score" class="post-score" title="current number of votes">0</div><span id="post-47256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Note with Wireshark. If you're on Windows, Microsoft's Message Analyzer can do that though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Nov '15, 10:21</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-47256" class="comments-container"><span id="47257"></span><div id="comment-47257" class="comment"><div id="post-47257-score" class="comment-score"></div><div class="comment-text"><p>Here is a link to a question which covers the same topic, more or less. <a href="https://ask.wireshark.org/questions/45952/correlating-a-netstat-connection-with-a-service?page=1&amp;focusedAnswerId=45955#45955">https://ask.wireshark.org/questions/45952/correlating-a-netstat-connection-with-a-service?page=1&amp;focusedAnswerId=45955#45955</a></p></div><div id="comment-47257-info" class="comment-info"><span class="comment-age">(04 Nov '15, 10:37)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-47256" class="comment-tools"></div><div class="clear"></div><div id="comment-47256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

