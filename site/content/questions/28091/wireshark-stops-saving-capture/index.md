+++
type = "question"
title = "Wireshark stops saving capture"
description = '''I have a dedicated VM on ESXi running Wireshark capturing one interface to multiple files. Looking at the data directory it will go along for a while and just quit after about 15 files, saving a new file every 1-2 minutes. It looks like it is capturing on the screen, in fact when I look at it and ha...'''
date = "2013-12-13T15:48:00Z"
lastmod = "2013-12-13T15:48:00Z"
weight = 28091
keywords = [ "stopping", "capture", "vmware", "error" ]
aliases = [ "/questions/28091" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark stops saving capture](/questions/28091/wireshark-stops-saving-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28091-score" class="post-score" title="current number of votes">0</div><span id="post-28091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a dedicated VM on ESXi running Wireshark capturing one interface to multiple files. Looking at the data directory it will go along for a while and just quit after about 15 files, saving a new file every 1-2 minutes. It looks like it is capturing on the screen, in fact when I look at it and have it move to the bottom of the captures it seems to restart saving. I am running it on a clean install of Win Server 2012 R2. There is nothing else running on the server and it has no roles assigned.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-stopping" rel="tag" title="see questions tagged &#39;stopping&#39;">stopping</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-vmware" rel="tag" title="see questions tagged &#39;vmware&#39;">vmware</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '13, 15:48</strong></p><img src="https://secure.gravatar.com/avatar/8897f54e76f5392f570b3305044f36ef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="electricranch&#39;s gravatar image" /><p><span>electricranch</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="electricranch has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Dec '13, 15:58</strong> </span></p></div></div><div id="comments-container-28091" class="comments-container"></div><div id="comment-tools-28091" class="comment-tools"></div><div class="clear"></div><div id="comment-28091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

