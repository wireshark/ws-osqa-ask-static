+++
type = "question"
title = "inter-packet delay"
description = '''Hi, I would like to create a graph of the inter-packet delays and the sequence numbers of an RTP steam. I don&#x27;t now which data I should use for it, the delta?  thank you in advance'''
date = "2011-06-10T08:22:00Z"
lastmod = "2011-06-10T08:22:00Z"
weight = 4506
keywords = [ "delay", "delta", "rtp", "inter-packet" ]
aliases = [ "/questions/4506" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [inter-packet delay](/questions/4506/inter-packet-delay)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4506-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4506-score" class="post-score" title="current number of votes">0</div><span id="post-4506-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to create a graph of the inter-packet delays and the sequence numbers of an RTP steam. I don't now which data I should use for it, the delta?</p><p>thank you in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-delta" rel="tag" title="see questions tagged &#39;delta&#39;">delta</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-inter-packet" rel="tag" title="see questions tagged &#39;inter-packet&#39;">inter-packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jun '11, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/510e2d19e5cf4596d2afb59467c68c6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lefkothea%20Vaitsi&#39;s gravatar image" /><p><span>Lefkothea Va...</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lefkothea Vaitsi has no accepted answers">0%</span></p></div></div><div id="comments-container-4506" class="comments-container"></div><div id="comment-tools-4506" class="comment-tools"></div><div class="clear"></div><div id="comment-4506-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

