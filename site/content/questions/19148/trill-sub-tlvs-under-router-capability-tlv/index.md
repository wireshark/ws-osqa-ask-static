+++
type = "question"
title = "TRILL Sub-TLVs under Router Capability TLV"
description = '''Hi~ The TRILL Sub-TLVs under Router Capability TLV As per RFC 6326 section 2.3 The TRILL_VERSION is 13 The VLAN_GROUP is 14 I can&#x27;t display the packet in wireshark'''
date = "2013-03-05T05:07:00Z"
lastmod = "2013-03-05T05:52:00Z"
weight = 19148
keywords = [ "trill", "wireshark" ]
aliases = [ "/questions/19148" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TRILL Sub-TLVs under Router Capability TLV](/questions/19148/trill-sub-tlvs-under-router-capability-tlv)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19148-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19148-score" class="post-score" title="current number of votes">0</div><span id="post-19148-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi~ The TRILL Sub-TLVs under Router Capability TLV As per RFC 6326 section 2.3</p><p>The TRILL_VERSION is 13</p><p>The VLAN_GROUP is 14</p><p>I can't display the packet in wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trill" rel="tag" title="see questions tagged &#39;trill&#39;">trill</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '13, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/b98d98103df5c78bb1fb1fd488293da2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aries_fang&#39;s gravatar image" /><p><span>aries_fang</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aries_fang has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Mar '13, 05:15</strong> </span></p></div></div><div id="comments-container-19148" class="comments-container"></div><div id="comment-tools-19148" class="comment-tools"></div><div class="clear"></div><div id="comment-19148-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19150"></span>

<div id="answer-container-19150" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19150-score" class="post-score" title="current number of votes">0</div><span id="post-19150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>These seem to be not-yet-implemented. You're welcome to provide, through <a href="https://bugs.wireshark.org">bugs.wireshark.org</a>, an enhancement request with a relevant capture file, or even a patch implementing the dissection.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '13, 05:52</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-19150" class="comments-container"></div><div id="comment-tools-19150" class="comment-tools"></div><div class="clear"></div><div id="comment-19150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

