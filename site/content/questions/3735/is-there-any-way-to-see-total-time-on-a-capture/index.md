+++
type = "question"
title = "Is there any way to see total time on a capture?"
description = '''What im aiming to do is transfer a file from host 1 to host 2 and see total time it takes to do it, im sure that wireshark has this function i just cant seem to find it at all. Am i missing something or isnt this implmented?  Thanks, steven'''
date = "2011-04-26T16:57:00Z"
lastmod = "2011-04-27T08:54:00Z"
weight = 3735
keywords = [ "timestamp", "help", "time" ]
aliases = [ "/questions/3735" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there any way to see total time on a capture?](/questions/3735/is-there-any-way-to-see-total-time-on-a-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3735-score" class="post-score" title="current number of votes">0</div><span id="post-3735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What im aiming to do is transfer a file from host 1 to host 2 and see total time it takes to do it, im sure that wireshark has this function i just cant seem to find it at all. Am i missing something or isnt this implmented?</p><p>Thanks, steven</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '11, 16:57</strong></p><img src="https://secure.gravatar.com/avatar/5d08f8311c636d7c00c08a14477f7265?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stevenc152&#39;s gravatar image" /><p><span>stevenc152</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stevenc152 has no accepted answers">0%</span></p></div></div><div id="comments-container-3735" class="comments-container"></div><div id="comment-tools-3735" class="comment-tools"></div><div class="clear"></div><div id="comment-3735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3736"></span>

<div id="answer-container-3736" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3736-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3736-score" class="post-score" title="current number of votes">0</div><span id="post-3736-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>nvm found it under summary, /faceplam</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '11, 17:22</strong></p><img src="https://secure.gravatar.com/avatar/5d08f8311c636d7c00c08a14477f7265?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stevenc152&#39;s gravatar image" /><p><span>stevenc152</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stevenc152 has no accepted answers">0%</span></p></div></div><div id="comments-container-3736" class="comments-container"><span id="3749"></span><div id="comment-3749" class="comment"><div id="post-3749-score" class="comment-score"></div><div class="comment-text"><p>Could you please "accept" your own answer (by clicking on the check-mark left of it). That way your question will not show up anymore in the unanswered questions list.</p></div><div id="comment-3749-info" class="comment-info"><span class="comment-age">(27 Apr '11, 08:54)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-3736" class="comment-tools"></div><div class="clear"></div><div id="comment-3736-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

