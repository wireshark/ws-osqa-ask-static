+++
type = "question"
title = "Checksum validation rejecting my security code"
description = '''I am trying to log into an internet site in London and when I put in the code for the security, the checksum validation rejects it before I complete the code.'''
date = "2011-11-08T13:32:00Z"
lastmod = "2011-11-09T15:11:00Z"
weight = 7290
keywords = [ "checksum", "security", "validation", "reject" ]
aliases = [ "/questions/7290" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Checksum validation rejecting my security code](/questions/7290/checksum-validation-rejecting-my-security-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7290-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7290-score" class="post-score" title="current number of votes">0</div><span id="post-7290-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to log into an internet site in London and when I put in the code for the security, the checksum validation rejects it before I complete the code.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-checksum" rel="tag" title="see questions tagged &#39;checksum&#39;">checksum</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-validation" rel="tag" title="see questions tagged &#39;validation&#39;">validation</span> <span class="post-tag tag-link-reject" rel="tag" title="see questions tagged &#39;reject&#39;">reject</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '11, 13:32</strong></p><img src="https://secure.gravatar.com/avatar/31a2ea95cd0d9848444c1622aa4a174b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tonya%20DeLozier&#39;s gravatar image" /><p><span>Tonya DeLozier</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tonya DeLozier has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 21:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7290" class="comments-container"><span id="7291"></span><div id="comment-7291" class="comment"><div id="post-7291-score" class="comment-score"></div><div class="comment-text"><p>ok, it is not on my mac - it is on the dell...</p></div><div id="comment-7291-info" class="comment-info"><span class="comment-age">(08 Nov '11, 13:37)</span> <span class="comment-user userinfo">Tonya DeLozier</span></div></div><span id="7294"></span><div id="comment-7294" class="comment"><div id="post-7294-score" class="comment-score"></div><div class="comment-text"><p>And the (wireshark-related) question is???</p></div><div id="comment-7294-info" class="comment-info"><span class="comment-age">(08 Nov '11, 14:05)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="7311"></span><div id="comment-7311" class="comment"><div id="post-7311-score" class="comment-score"></div><div class="comment-text"><p>wait, SYNbit, you have telepathy module disabled?</p></div><div id="comment-7311-info" class="comment-info"><span class="comment-age">(08 Nov '11, 21:14)</span> <span class="comment-user userinfo">ShomeaX</span></div></div></div><div id="comment-tools-7290" class="comment-tools"></div><div class="clear"></div><div id="comment-7290-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7314"></span>

<div id="answer-container-7314" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7314-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7314-score" class="post-score" title="current number of votes">-3</div><span id="post-7314-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Tonya, you should address those kind of questions to your Internet provider. If you aren't sure what it is, ask your neighbourhood.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 21:25</strong></p><img src="https://secure.gravatar.com/avatar/35d96b8e73e6deb4e332d076fd3269b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShomeaX&#39;s gravatar image" /><p><span>ShomeaX</span><br />
<span class="score" title="73 reputation points">73</span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShomeaX has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Nov '11, 15:14</strong> </span></p></div></div><div id="comments-container-7314" class="comments-container"><span id="7331"></span><div id="comment-7331" class="comment"><div id="post-7331-score" class="comment-score"></div><div class="comment-text"><p>Some of the most talented people in the protocol analysis industry are women. You might want to reconsider the "boyfriend" comment. :)</p></div><div id="comment-7331-info" class="comment-info"><span class="comment-age">(09 Nov '11, 11:55)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="7340"></span><div id="comment-7340" class="comment"><div id="post-7340-score" class="comment-score"></div><div class="comment-text"><p>Sure they are! And I'm aware of <a href="http://theinvisiblethings.blogspot.com/">smartest kernel hacker in the world</a> is woman as well, but ... this specific question just made me lol, sorry.</p></div><div id="comment-7340-info" class="comment-info"><span class="comment-age">(09 Nov '11, 15:11)</span> <span class="comment-user userinfo">ShomeaX</span></div></div></div><div id="comment-tools-7314" class="comment-tools"></div><div class="clear"></div><div id="comment-7314-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

