+++
type = "question"
title = "server disconnecting from printer intermittently"
description = '''Dear all, I have a problem with my server and it will be disconnected to my printer intermittently. Here I attached my packet capture with server iP: 10.110.117.10 and printer IP: 10.110.114.23 The Packet can be seen here, https://drive.google.com/file/d/0B-n7X77Fqu_RaGFlcjYwTDRFQXc/view?usp=sharing...'''
date = "2014-11-12T19:22:00Z"
lastmod = "2014-11-27T04:29:00Z"
weight = 37802
keywords = [ "printer", "disconnect", "server" ]
aliases = [ "/questions/37802" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [server disconnecting from printer intermittently](/questions/37802/server-disconnecting-from-printer-intermittently)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37802-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37802-score" class="post-score" title="current number of votes">0</div><span id="post-37802-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all,</p><p>I have a problem with my server and it will be disconnected to my printer intermittently. Here I attached my packet capture with server iP: 10.110.117.10 and printer IP: 10.110.114.23 The Packet can be seen here,</p><p><a href="https://drive.google.com/file/d/0B-n7X77Fqu_RaGFlcjYwTDRFQXc/view?usp=sharing">https://drive.google.com/file/d/0B-n7X77Fqu_RaGFlcjYwTDRFQXc/view?usp=sharing</a></p><p>Hopefully can someone tell me what’s wrong with my server. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-printer" rel="tag" title="see questions tagged &#39;printer&#39;">printer</span> <span class="post-tag tag-link-disconnect" rel="tag" title="see questions tagged &#39;disconnect&#39;">disconnect</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '14, 19:22</strong></p><img src="https://secure.gravatar.com/avatar/dacf8f83a9c585ca2775f22992332737?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="forest79&#39;s gravatar image" /><p><span>forest79</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="forest79 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Nov '14, 19:54</strong> </span></p></div></div><div id="comments-container-37802" class="comments-container"><span id="37806"></span><div id="comment-37806" class="comment"><div id="post-37806-score" class="comment-score"></div><div class="comment-text"><p>Your link seems to be incorrect.</p></div><div id="comment-37806-info" class="comment-info"><span class="comment-age">(13 Nov '14, 01:35)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="38191"></span><div id="comment-38191" class="comment"><div id="post-38191-score" class="comment-score"></div><div class="comment-text"><p>Dear graham,</p><p>Sorry for the link. Can you try the link below?</p><p><a href="https://drive.google.com/file/d/0B-n7X77Fqu_RaGFlcjYwTDRFQXc/view?usp=sharing">https://drive.google.com/file/d/0B-n7X77Fqu_RaGFlcjYwTDRFQXc/view?usp=sharing</a></p></div><div id="comment-38191-info" class="comment-info"><span class="comment-age">(26 Nov '14, 19:53)</span> <span class="comment-user userinfo">forest79</span></div></div><span id="38205"></span><div id="comment-38205" class="comment"><div id="post-38205-score" class="comment-score"></div><div class="comment-text"><p>Could not see any disconnection except some zero windows packets by printer.for more explanation on zero windows please see this "https://ask.wireshark.org/questions/28478/tcp-zerowindow-when-printing"</p></div><div id="comment-38205-info" class="comment-info"><span class="comment-age">(27 Nov '14, 04:29)</span> <span class="comment-user userinfo">kishan pandey</span></div></div></div><div id="comment-tools-37802" class="comment-tools"></div><div class="clear"></div><div id="comment-37802-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

