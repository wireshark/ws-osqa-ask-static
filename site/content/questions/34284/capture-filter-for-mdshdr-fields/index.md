+++
type = "question"
title = "capture-filter for mdshdr fields"
description = '''Hi All, I was using the display filter to filter my FC traffic. My display filter is something like this: -R mdshdr.srcidx==0x2f||mdshdr.dstidx==0x2f As write and display filter don&#x27;t work together, I wanted to know the equivalent of it in capture filter. How do I access mdshdr fiels in capture filt...'''
date = "2014-06-30T02:00:00Z"
lastmod = "2014-06-30T02:00:00Z"
weight = 34284
keywords = [ "capture-filter", "pcap", "tshark" ]
aliases = [ "/questions/34284" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [capture-filter for mdshdr fields](/questions/34284/capture-filter-for-mdshdr-fields)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34284-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34284-score" class="post-score" title="current number of votes">0</div><span id="post-34284-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, I was using the display filter to filter my FC traffic. My display filter is something like this: -R mdshdr.srcidx==0x2f||mdshdr.dstidx==0x2f</p><p>As write and display filter don't work together, I wanted to know the equivalent of it in capture filter. How do I access mdshdr fiels in capture filter?</p><p>Thanks and Regards,</p><p>Aparna N</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '14, 02:00</strong></p><img src="https://secure.gravatar.com/avatar/b605d47d2e423a49d4a281eb597b9fba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aparna&#39;s gravatar image" /><p><span>Aparna</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aparna has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jun '14, 02:14</strong> </span></p></div></div><div id="comments-container-34284" class="comments-container"></div><div id="comment-tools-34284" class="comment-tools"></div><div class="clear"></div><div id="comment-34284-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

