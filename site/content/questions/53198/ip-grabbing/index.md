+++
type = "question"
title = "Ip grabbing"
description = '''Does anyone know the commands on grabbing someones skype ip, I havent used wireshark in a long long time i am using ubuntu if you didnt know, If u know any ubuntu wireshark commands for grabbing someones ip thanks.'''
date = "2016-06-04T03:12:00Z"
lastmod = "2016-06-05T06:30:00Z"
weight = 53198
keywords = [ "ip", "help" ]
aliases = [ "/questions/53198" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ip grabbing](/questions/53198/ip-grabbing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53198-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53198-score" class="post-score" title="current number of votes">0</div><span id="post-53198-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know the commands on grabbing someones skype ip, I havent used wireshark in a long long time</p><p>i am using ubuntu if you didnt know, If u know any ubuntu wireshark commands for grabbing someones ip thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '16, 03:12</strong></p><img src="https://secure.gravatar.com/avatar/095fa549e39f7c68681e6ed944205646?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="asdadads&#39;s gravatar image" /><p><span>asdadads</span><br />
<span class="score" title="4 reputation points">4</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="asdadads has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jun '16, 03:14</strong> </span></p></div></div><div id="comments-container-53198" class="comments-container"></div><div id="comment-tools-53198" class="comment-tools"></div><div class="clear"></div><div id="comment-53198-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53204"></span>

<div id="answer-container-53204" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53204-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53204-score" class="post-score" title="current number of votes">0</div><span id="post-53204-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Haven't tried it myself, but a quick google query for "wireshark skype ip" returns tons of results, e.g.</p><p><a href="http://newhax.com/forum/index.php?threads/find-user-ip-on-skype-wireshark.3070/">http://newhax.com/forum/index.php?threads/find-user-ip-on-skype-wireshark.3070/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jun '16, 04:13</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-53204" class="comments-container"><span id="53206"></span><div id="comment-53206" class="comment"><div id="post-53206-score" class="comment-score"></div><div class="comment-text"><p>Yes, except that all those tutorials can only work if Skype on either end is not forced to use its firewall-cheating techniques. The more restrictive firewalls at one of the ends, the higher the probability that you'll be receiving the Skype packets from an IP address totally unrelated to the real one.</p></div><div id="comment-53206-info" class="comment-info"><span class="comment-age">(05 Jun '16, 06:03)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="53207"></span><div id="comment-53207" class="comment"><div id="post-53207-score" class="comment-score"></div><div class="comment-text"><p>Right, but in those cases Wireshark can't help at all :-)</p></div><div id="comment-53207-info" class="comment-info"><span class="comment-age">(05 Jun '16, 06:20)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="53208"></span><div id="comment-53208" class="comment"><div id="post-53208-score" class="comment-score"></div><div class="comment-text"><p>Well, something about the wording of the Question made me think that it is worth emphasizing that the result may be a false positive, i.e. that the method will always yield some address but it is more or less impossible to find out whether the address is actually linked to the Skype user or not.</p><p>A more real-life example than paranoid firewalls would be simply a Skype application on mobile phone.</p></div><div id="comment-53208-info" class="comment-info"><span class="comment-age">(05 Jun '16, 06:30)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-53204" class="comment-tools"></div><div class="clear"></div><div id="comment-53204-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

