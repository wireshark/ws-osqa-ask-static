+++
type = "question"
title = "Wireshark dissector for a Google Protocol Buffer defined protocol"
description = '''Hi, I would like to know if anyone has an example of a wireshark dissector for a protocol defined in a .proto file of Google Protocol Buffer. It would help me a lot! Many Thanks!'''
date = "2013-08-15T15:02:00Z"
lastmod = "2013-08-15T20:01:00Z"
weight = 23808
keywords = [ "dissector", "wireshark" ]
aliases = [ "/questions/23808" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark dissector for a Google Protocol Buffer defined protocol](/questions/23808/wireshark-dissector-for-a-google-protocol-buffer-defined-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23808-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23808-score" class="post-score" title="current number of votes">0</div><span id="post-23808-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I would like to know if anyone has an example of a wireshark dissector for a protocol defined in a .proto file of Google Protocol Buffer. It would help me a lot!</p><p>Many Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Aug '13, 15:02</strong></p><img src="https://secure.gravatar.com/avatar/fdbc91810fafdddfb7b893d55830e03d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Andr%C3%A9%20de%20Melo&#39;s gravatar image" /><p><span>André de Melo</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="André de Melo has no accepted answers">0%</span></p></div></div><div id="comments-container-23808" class="comments-container"></div><div id="comment-tools-23808" class="comment-tools"></div><div class="clear"></div><div id="comment-23808-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23809"></span>

<div id="answer-container-23809" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23809-score" class="post-score" title="current number of votes">2</div><span id="post-23809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here we go ...</p><blockquote><p><a href="http://code.google.com/p/protobuf-wireshark/">http://code.google.com/p/protobuf-wireshark/</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Aug '13, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-23809" class="comments-container"><span id="23815"></span><div id="comment-23815" class="comment"><div id="post-23815-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much!</p></div><div id="comment-23815-info" class="comment-info"><span class="comment-age">(15 Aug '13, 20:01)</span> <span class="comment-user userinfo">André de Melo</span></div></div></div><div id="comment-tools-23809" class="comment-tools"></div><div class="clear"></div><div id="comment-23809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

