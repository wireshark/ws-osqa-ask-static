+++
type = "question"
title = "bits of SIN"
description = '''What should I do to understand which bit of SIN is 1?I mean for example first bit or second bit or third...'''
date = "2011-02-22T09:47:00Z"
lastmod = "2011-02-23T23:49:00Z"
weight = 2485
keywords = [ "syn" ]
aliases = [ "/questions/2485" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [bits of SIN](/questions/2485/bits-of-sin)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2485-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2485-score" class="post-score" title="current number of votes">0</div><span id="post-2485-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What should I do to understand which bit of SIN is 1?I mean for example first bit or second bit or third...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-syn" rel="tag" title="see questions tagged &#39;syn&#39;">syn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '11, 09:47</strong></p><img src="https://secure.gravatar.com/avatar/96e902e433f9ca63286cc774486728f8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="baran&#39;s gravatar image" /><p><span>baran</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="baran has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Feb '12, 19:18</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-2485" class="comments-container"></div><div id="comment-tools-2485" class="comment-tools"></div><div class="clear"></div><div id="comment-2485-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2489"></span>

<div id="answer-container-2489" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2489-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2489-score" class="post-score" title="current number of votes">1</div><span id="post-2489-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The SYN bit is always the second bit of the TCP flags field. So the second bit of the 14th byte of the TCP header is the SYN bit.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '11, 10:08</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-2489" class="comments-container"><span id="2491"></span><div id="comment-2491" class="comment"><div id="post-2491-score" class="comment-score"></div><div class="comment-text"><p>thanks for answering me but I don't want to know which bit of TCP is SYN bit in fact I want to know which bit of SYN is one(1).</p></div><div id="comment-2491-info" class="comment-info"><span class="comment-age">(22 Feb '11, 11:15)</span> <span class="comment-user userinfo">baran</span></div></div><span id="2496"></span><div id="comment-2496" class="comment"><div id="post-2496-score" class="comment-score"></div><div class="comment-text"><p>Then I don't really understand your question... could you explain it a little more? What is the background of your question?</p></div><div id="comment-2496-info" class="comment-info"><span class="comment-age">(22 Feb '11, 11:39)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="2498"></span><div id="comment-2498" class="comment"><div id="post-2498-score" class="comment-score"></div><div class="comment-text"><p>Maybe my question is wrong,it is one of my university assignments.If I understand more about it I will write it in a better way,anyway thanks</p></div><div id="comment-2498-info" class="comment-info"><span class="comment-age">(22 Feb '11, 11:56)</span> <span class="comment-user userinfo">baran</span></div></div><span id="2499"></span><div id="comment-2499" class="comment not_top_scorer"><div id="post-2499-score" class="comment-score"></div><div class="comment-text"><p>Maybe this is simply a question of how to filter all packets that have the SYN bit set (a.k.a SYN bit being 1)? In that case the display filter would be "tcp.flags.syn==1", or, if you don't want the SYN/ACK packets you'd filter for "tcp.flags==0x02".</p></div><div id="comment-2499-info" class="comment-info"><span class="comment-age">(22 Feb '11, 12:06)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="2544"></span><div id="comment-2544" class="comment not_top_scorer"><div id="post-2544-score" class="comment-score"></div><div class="comment-text"><p>I got a better understanding about this question,I realized that SYN includes four bits and if the first bit is 1 it means that it is FIN and if the second bit is 1 it means that it is SYN and something like that about the last two bits but I don't know if they are one what it means.Now how can we recognize which bit of SYN is 1?(that it can have different meanings which I explained)</p></div><div id="comment-2544-info" class="comment-info"><span class="comment-age">(23 Feb '11, 20:22)</span> <span class="comment-user userinfo">baran</span></div></div><span id="2545"></span><div id="comment-2545" class="comment"><div id="post-2545-score" class="comment-score">1</div><div class="comment-text"><p>What you call "SYN" or "SIN" is the TCP flags field. In the TCP flags field, one of the bits is called SYN and as you already discovered, this is the second bit. If you expand the TCP flags field in the packet detail (second) pane of Wireshark, you see what the other bits are. In short, they are FIN, SYN, RST, PSH, ACK, URG, ECE, CWR and NS. Have a look at <a href="http://tools.ietf.org/search/rfc793">RFC 793</a> for more info on the function of the TCP flags.</p></div><div id="comment-2545-info" class="comment-info"><span class="comment-age">(23 Feb '11, 23:41)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="2546"></span><div id="comment-2546" class="comment"><div id="post-2546-score" class="comment-score">1</div><div class="comment-text"><p>No, sorry, SYN doesn't include four bits, it is still just one bit.</p><p>The TCP header has one byte (8 bits) dedicated to be used as "flags". One of those can be used to indicate things like "I want to establish a connection", and is called the "SYN flag" (or "SYN bit"). It is just one single bit (not four), and it is always the same bit. This bit is either 0 (usually telling you that the connection is already established) or 1 (meaning that the connection setup is going one just now).</p><p>You really should look up the "TCP Three Way Handshake" ;-)</p></div><div id="comment-2546-info" class="comment-info"><span class="comment-age">(23 Feb '11, 23:49)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2489" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-2489-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

