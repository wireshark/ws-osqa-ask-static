+++
type = "question"
title = "Multiport Adapter is not detecting"
description = '''Multiport Adapter is not detecting I have intel pro/1000 PT Dual Port network card but its is not detecting by wireshark. I am using winpcap4.1.1.is winpcap4.1.1 support Dual Port network card(like 2 ports in one adapter). I also have created java application using winpcap4.1.1 amd jpcap0.7 to captu...'''
date = "2012-03-16T05:34:00Z"
lastmod = "2012-03-16T06:00:00Z"
weight = 9581
keywords = [ "-adapter", "multiport" ]
aliases = [ "/questions/9581" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Multiport Adapter is not detecting](/questions/9581/multiport-adapter-is-not-detecting)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9581-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9581-score" class="post-score" title="current number of votes">0</div><span id="post-9581-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Multiport Adapter is not detecting</p><p>I have intel pro/1000 PT Dual Port network card but its is not detecting by wireshark. I am using <a href="http://winpcap4.1.1.is">winpcap4.1.1.is</a> winpcap4.1.1 support Dual Port network card(like 2 ports in one adapter). I also have created java application using winpcap4.1.1 amd jpcap0.7 to capture network trafic. that application also not detected that lan card</p><p>.What should I need to do?i need solution for that? It’s very urgent</p><p>i also try to send the email to winpcap on this adress but Delivery to the following recipients failed. <span class="__cf_email__" data-cfemail="285f4146584b4958054a5d4f5b68">[email protected]</span><a href="http://winpcap.org">winpcap.org</a></p><p>Regards<img src="http://upload.ecvv.com/upload/Product/201011/China_Intel_EXPI9402PTBLK_PRO_1000_PT_Dual_Port_Server201011161044350.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link--adapter" rel="tag" title="see questions tagged &#39;-adapter&#39;">-adapter</span> <span class="post-tag tag-link-multiport" rel="tag" title="see questions tagged &#39;multiport&#39;">multiport</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '12, 05:34</strong></p><img src="https://secure.gravatar.com/avatar/606252e8dfea9cac0294fa56234519a1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="togreatmind&#39;s gravatar image" /><p><span>togreatmind</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="togreatmind has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Mar '12, 06:54</strong> </span></p></div></div><div id="comments-container-9581" class="comments-container"><span id="9583"></span><div id="comment-9583" class="comment"><div id="post-9583-score" class="comment-score"></div><div class="comment-text"><p>Have you tried the latest version of WinPCap (<a href="http://www.winpcap.org/install/default.htm">4.1.2</a>)?</p><p>Maybe you should go and ask the WinPCap folks over at their <a href="http://www.winpcap.org/contact.htm">mail lists</a>. You might also try Network Monitor from Microsoft as that uses a different capture mechanism than WinPCap.</p></div><div id="comment-9583-info" class="comment-info"><span class="comment-age">(16 Mar '12, 06:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-9581" class="comment-tools"></div><div class="clear"></div><div id="comment-9581-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

