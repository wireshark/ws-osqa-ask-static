+++
type = "question"
title = "END OF DATA ITEM:"
description = '''In my dissector, add_item for my protocol data starting @ offset += 17;. The next function code after starts 3 bytes from the end. Example below: hf_funky_data STARTS @ offset += 17 :START DATA: 38 43 31 39 33 44 30 33 46 30 30 30 30 :STOP DATA: hf_funky_id always STARTS 3 bytes from the END: :START...'''
date = "2013-01-11T16:40:00Z"
lastmod = "2013-01-11T16:40:00Z"
weight = 17632
keywords = [ "end", "data" ]
aliases = [ "/questions/17632" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [END OF DATA ITEM:](/questions/17632/end-of-data-item)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17632-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17632-score" class="post-score" title="current number of votes">0</div><span id="post-17632-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In my dissector, add_item for my protocol data starting @ offset += 17;. The next function code after starts 3 bytes from the end. Example below:</p><p>hf_funky_data STARTS @ offset += 17 :START DATA: 38 43 31 39 33 44 30 33 46 30 30 30 30 :STOP DATA:</p><p>hf_funky_id always STARTS 3 bytes from the END: :START ID: 39 31 34 :END:</p><p>How do I setup my dissector so it stops 3 Bytes short, it always starts at the correct byte but always runs to the last byte in the data packet.</p><p>Thoughts?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-end" rel="tag" title="see questions tagged &#39;end&#39;">end</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '13, 16:40</strong></p><img src="https://secure.gravatar.com/avatar/1f51b148d3f1f063aa95114ceea3b70f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jballard1979&#39;s gravatar image" /><p><span>jballard1979</span><br />
<span class="score" title="20 reputation points">20</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jballard1979 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jan '13, 16:41</strong> </span></p></div></div><div id="comments-container-17632" class="comments-container"></div><div id="comment-tools-17632" class="comment-tools"></div><div class="clear"></div><div id="comment-17632-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

