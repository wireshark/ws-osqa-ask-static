+++
type = "question"
title = "MacBook Pro Mid 2014 cannot select monitor mode"
description = '''Hello, I am trying to enable monitor mode but it is greyed out with &quot;N/A&quot; displayed. Do you know how I can get this working on a mid 2014 MacBook pro? Thank you'''
date = "2015-07-06T13:21:00Z"
lastmod = "2015-07-06T16:28:00Z"
weight = 43901
keywords = [ "macbook" ]
aliases = [ "/questions/43901" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [MacBook Pro Mid 2014 cannot select monitor mode](/questions/43901/macbook-pro-mid-2014-cannot-select-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43901-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43901-score" class="post-score" title="current number of votes">0</div><span id="post-43901-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am trying to enable monitor mode but it is greyed out with "N/A" displayed. Do you know how I can get this working on a mid 2014 MacBook pro?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macbook" rel="tag" title="see questions tagged &#39;macbook&#39;">macbook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '15, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/ad1c454adb9e670aa54878f7fc102641?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tom%20Bruce&#39;s gravatar image" /><p><span>Tom Bruce</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tom Bruce has no accepted answers">0%</span></p></div></div><div id="comments-container-43901" class="comments-container"><span id="43909"></span><div id="comment-43909" class="comment"><div id="post-43909-score" class="comment-score"></div><div class="comment-text"><p>What is printed if you type <code>tcpdump -i en0 -L</code> and <code>tcpdump -i en0 -I -L</code> at the command line?</p></div><div id="comment-43909-info" class="comment-info"><span class="comment-age">(06 Jul '15, 16:28)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-43901" class="comment-tools"></div><div class="clear"></div><div id="comment-43901-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

