+++
type = "question"
title = "Can I recover a discarded capture?"
description = '''I was using Wireshark a few hours ago, and started a capture. A couple of hours later, it closed unexpectedly. I was not able to save the capture that I started, but I really need the VOIP call that I recorded earlier. Will I be able to recover it?'''
date = "2012-02-24T08:48:00Z"
lastmod = "2012-02-24T13:53:00Z"
weight = 9194
keywords = [ "capture", "crash", "recovery" ]
aliases = [ "/questions/9194" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Can I recover a discarded capture?](/questions/9194/can-i-recover-a-discarded-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9194-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9194-score" class="post-score" title="current number of votes">0</div><span id="post-9194-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was using Wireshark a few hours ago, and started a capture. A couple of hours later, it closed unexpectedly. I was not able to save the capture that I started, but I really need the VOIP call that I recorded earlier. Will I be able to recover it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span> <span class="post-tag tag-link-recovery" rel="tag" title="see questions tagged &#39;recovery&#39;">recovery</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '12, 08:48</strong></p><img src="https://secure.gravatar.com/avatar/830a683ae2682d4442ebb27b7053b2bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ishella&#39;s gravatar image" /><p><span>ishella</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ishella has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Feb '12, 09:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-9194" class="comments-container"></div><div id="comment-tools-9194" class="comment-tools"></div><div class="clear"></div><div id="comment-9194-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="9196"></span>

<div id="answer-container-9196" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9196-score" class="post-score" title="current number of votes">2</div><span id="post-9196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Actually the temporary file may very well be there if Wireshark crashed. See the FAQ question <a href="http://www.wireshark.org/faq.html#q7.12">7.12</a>.</p><p>[Update] Don't forget to drop by and Accept this answer if it answered your question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '12, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Mar '12, 07:06</strong> </span></p></div></div><div id="comments-container-9196" class="comments-container"><span id="9197"></span><div id="comment-9197" class="comment"><div id="post-9197-score" class="comment-score"></div><div class="comment-text"><p>Neat. Didn't know that. Note also that for Windows 7 the temporary file will be in <code>\Users\&lt;your_user&gt;\AppData\Local\Temp</code> on your main drive (usually <code>C:</code>), which is not mentioned in the FAQ.</p></div><div id="comment-9197-info" class="comment-info"><span class="comment-age">(24 Feb '12, 13:33)</span> <span class="comment-user userinfo">multipleinte...</span></div></div><span id="9198"></span><div id="comment-9198" class="comment"><div id="post-9198-score" class="comment-score"></div><div class="comment-text"><p>I added that Windows-7 specific location to the FAQ in r41183. I suppose it'll take a few hours to show up on the web site. Thanks for the info!</p></div><div id="comment-9198-info" class="comment-info"><span class="comment-age">(24 Feb '12, 13:53)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-9196" class="comment-tools"></div><div class="clear"></div><div id="comment-9196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="9195"></span>

<div id="answer-container-9195" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9195-score" class="post-score" title="current number of votes">0</div><span id="post-9195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><del>You are very probably out of luck. The temporary file containing the capture data is probably not present any more.</del> As <a href="http://ask.wireshark.org/questions/9194/#9196">mentioned by @JeffMorriss</a>, the temporary file is probably still present, and you should be able to use it (althouhg you should probably start by copying it somewhere it will be safe to work with) by opeining it with Wireshark.<br />
For future reference, if Wireshark was capturing for the entire time, you probably ran into the <a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory" title="Out Of Memory">known issue</a> with Wireshark running out of memory. You can avoid this issue by using <a href="http://www.wireshark.org/docs/man-pages/dumpcap.html" title="dumpcap manual page">dumpcap</a> directly for long-running captures, and then processing it post-mortem using Wireshark, possibly after reducing the file size by splitting the capture using <a href="http://www.wireshark.org/docs/man-pages/editcap.html" title="editcap manual page">editcap</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '12, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Feb '12, 14:39</strong> </span></p></div></div><div id="comments-container-9195" class="comments-container"></div><div id="comment-tools-9195" class="comment-tools"></div><div class="clear"></div><div id="comment-9195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

