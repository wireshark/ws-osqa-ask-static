+++
type = "question"
title = "TUP dissector/plugin"
description = '''Hello, I´m trying to decode some TUP traffic, but wireshark cant decode it, because it lacks the proper dissector. As i dont have the skill to code a dissector myself, i would like to know if anybody has done so already, and if he/she could post the code here. Thank you very much.'''
date = "2013-05-14T07:12:00Z"
lastmod = "2013-05-14T07:12:00Z"
weight = 21134
keywords = [ "tup", "ss7", "dissector" ]
aliases = [ "/questions/21134" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TUP dissector/plugin](/questions/21134/tup-dissectorplugin)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21134-score" class="post-score" title="current number of votes">0</div><span id="post-21134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I´m trying to decode some TUP traffic, but wireshark cant decode it, because it lacks the proper dissector. As i dont have the skill to code a dissector myself, i would like to know if anybody has done so already, and if he/she could post the code here.</p><p>Thank you very much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tup" rel="tag" title="see questions tagged &#39;tup&#39;">tup</span> <span class="post-tag tag-link-ss7" rel="tag" title="see questions tagged &#39;ss7&#39;">ss7</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 May '13, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/41cae5c8111115b7c81a5d2f5a624c14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Renan&#39;s gravatar image" /><p><span>Renan</span><br />
<span class="score" title="26 reputation points">26</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Renan has no accepted answers">0%</span></p></div></div><div id="comments-container-21134" class="comments-container"></div><div id="comment-tools-21134" class="comment-tools"></div><div class="clear"></div><div id="comment-21134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

