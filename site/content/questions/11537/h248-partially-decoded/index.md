+++
type = "question"
title = "H.248 partially decoded"
description = '''In H.248 all values items inside descriptors are not decoded (e.g. parameter: BNC Characteristics, value item: 0a0102 or parameter: interface, value item: 0a0101 and so on).'''
date = "2012-06-01T10:13:00Z"
lastmod = "2012-06-01T14:10:00Z"
weight = 11537
keywords = [ "h.248" ]
aliases = [ "/questions/11537" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [H.248 partially decoded](/questions/11537/h248-partially-decoded)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11537-score" class="post-score" title="current number of votes">0</div><span id="post-11537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In H.248 all values items inside descriptors are not decoded (e.g. parameter: BNC Characteristics, value item: 0a0102 or parameter: interface, value item: 0a0101 and so on).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h.248" rel="tag" title="see questions tagged &#39;h.248&#39;">h.248</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '12, 10:13</strong></p><img src="https://secure.gravatar.com/avatar/5de44d986fa1b2d4851ed0234c16b78c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cle&#39;s gravatar image" /><p><span>cle</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cle has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Jun '12, 10:20</strong> </span></p></div></div><div id="comments-container-11537" class="comments-container"></div><div id="comment-tools-11537" class="comment-tools"></div><div class="clear"></div><div id="comment-11537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="11539"></span>

<div id="answer-container-11539" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11539-score" class="post-score" title="current number of votes">0</div><span id="post-11539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, You should state which version of Wireshark you are usina and if it's 1.6.x(1.6.8 is the latest) or 1.7.x raise a bug report including a sample trace showing the problem. If you can try it on 1.7 that's a bonus.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jun '12, 10:37</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-11539" class="comments-container"></div><div id="comment-tools-11539" class="comment-tools"></div><div class="clear"></div><div id="comment-11539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="11552"></span>

<div id="answer-container-11552" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11552-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11552-score" class="post-score" title="current number of votes">0</div><span id="post-11552-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What equipment is generating the H.248, it could be proprietary signalling. Quite a few vendors do this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jun '12, 14:10</strong></p><img src="https://secure.gravatar.com/avatar/030196d67dc4e2b8f4ecff65eefdb63e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KeithFrench&#39;s gravatar image" /><p><span>KeithFrench</span><br />
<span class="score" title="121 reputation points">121</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KeithFrench has no accepted answers">0%</span></p></div></div><div id="comments-container-11552" class="comments-container"></div><div id="comment-tools-11552" class="comment-tools"></div><div class="clear"></div><div id="comment-11552-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

