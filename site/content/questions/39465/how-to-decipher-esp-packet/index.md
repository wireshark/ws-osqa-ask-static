+++
type = "question"
title = "how to decipher ESP packet?"
description = '''Hi, My MacOS is on a 802.11n wireless network with WPA2 personal security protected. When I capture the traffic through wireshark, all the packets are seen as ESP packets. Does wireshark support deciphering on these packets and how to do that?'''
date = "2015-01-28T22:50:00Z"
lastmod = "2015-01-28T22:50:00Z"
weight = 39465
keywords = [ "esp" ]
aliases = [ "/questions/39465" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to decipher ESP packet?](/questions/39465/how-to-decipher-esp-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39465-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39465-score" class="post-score" title="current number of votes">0</div><span id="post-39465-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>My MacOS is on a 802.11n wireless network with WPA2 personal security protected. When I capture the traffic through wireshark, all the packets are seen as ESP packets. Does wireshark support deciphering on these packets and how to do that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esp" rel="tag" title="see questions tagged &#39;esp&#39;">esp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jan '15, 22:50</strong></p><img src="https://secure.gravatar.com/avatar/2d1a8885858c8435654658b25f489bd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveZhou&#39;s gravatar image" /><p><span>SteveZhou</span><br />
<span class="score" title="191 reputation points">191</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveZhou has no accepted answers">0%</span></p></div></div><div id="comments-container-39465" class="comments-container"></div><div id="comment-tools-39465" class="comment-tools"></div><div class="clear"></div><div id="comment-39465-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

