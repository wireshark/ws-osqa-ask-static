+++
type = "question"
title = "May be Wireshark not capturing total network ?"
description = '''Dear Geeks, I am in process to find out the spamming computer on the network. So I have captured network traffic by using Wireshark 1.10.8 on Windows 8 and N/W Card is Wi-fi ( Qualcomm Atheros QCA9565 ). I have sent an email while capturing the network traffic by Wireshark for test. When I filter th...'''
date = "2014-08-04T02:35:00Z"
lastmod = "2014-08-04T02:43:00Z"
weight = 35131
keywords = [ "spamissue" ]
aliases = [ "/questions/35131" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [May be Wireshark not capturing total network ?](/questions/35131/may-be-wireshark-not-capturing-total-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35131-score" class="post-score" title="current number of votes">0</div><span id="post-35131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Geeks,</p><p>I am in process to find out the spamming computer on the network.</p><p>So I have captured network traffic by using Wireshark 1.10.8 on Windows 8 and N/W Card is Wi-fi ( Qualcomm Atheros QCA9565 ).</p><p>I have sent an email while capturing the network traffic by Wireshark for test. When I filter the captured traffic by using the filter "tcp.port == 587" ,it showing only the mail sent by my (sent by me ) laptop (same computer on which Wireshark is running). Here port no. is 587 because the Outlook is configured with 587 port for SMTP for all computers in the network.</p><p>But in the capture I am seeing the different IPs and computer names which are on the network (not only mine),it seems it is capturing the packets in network but not sure what is happening in case of mail sent filter ( for sure that someone on the network have sent the mail while the network traffic captured ).</p><p>Please suggest me to resolve the issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spamissue" rel="tag" title="see questions tagged &#39;spamissue&#39;">spamissue</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Aug '14, 02:35</strong></p><img src="https://secure.gravatar.com/avatar/8007c8353acec42f560dd7fcee0c4532?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="OpenMind&#39;s gravatar image" /><p><span>OpenMind</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="OpenMind has no accepted answers">0%</span></p></div></div><div id="comments-container-35131" class="comments-container"></div><div id="comment-tools-35131" class="comment-tools"></div><div class="clear"></div><div id="comment-35131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35133"></span>

<div id="answer-container-35133" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35133-score" class="post-score" title="current number of votes">0</div><span id="post-35133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://wiki.wireshark.org/CaptureSetup">This</a> should be informative.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Aug '14, 02:43</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-35133" class="comments-container"></div><div id="comment-tools-35133" class="comment-tools"></div><div class="clear"></div><div id="comment-35133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

