+++
type = "question"
title = "wireshark exercise"
description = '''Hi I am a reader of the book &quot;Troubleshooting with Wireshark: Locate the Source of Performance Problem&quot;. This is a very good book and I learn many of skills about TCP/IP troubleshooting, but I still feel weakness when I start to do troubleshoot in my work. I think I need to do more exercises. So, Ar...'''
date = "2015-02-25T03:36:00Z"
lastmod = "2015-02-26T01:59:00Z"
weight = 40067
keywords = [ "exercise" ]
aliases = [ "/questions/40067" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark exercise](/questions/40067/wireshark-exercise)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40067-score" class="post-score" title="current number of votes">0</div><span id="post-40067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am a reader of the book <a href="http://www.wiresharkbook.com/troubleshooting.html">"Troubleshooting with Wireshark: Locate the Source of Performance Problem"</a>. This is a very good book and I learn many of skills about TCP/IP troubleshooting, but I still feel weakness when I start to do troubleshoot in my work. I think I need to do more exercises.</p><p>So, Are there any resources of wireshark on the internet to provide more scenario which can let me do more exercise to enhance network analysis? Or is it possible to publish a book which has many scenario and also provide the pcapng file so that I can try to find out what the problems are? Thanks in advance if there's any information about that.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-exercise" rel="tag" title="see questions tagged &#39;exercise&#39;">exercise</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '15, 03:36</strong></p><img src="https://secure.gravatar.com/avatar/2ac0ce4f9f250d6d4f7022bc331c749b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stevejaw&#39;s gravatar image" /><p><span>stevejaw</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stevejaw has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Feb '15, 04:55</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-40067" class="comments-container"></div><div id="comment-tools-40067" class="comment-tools"></div><div class="clear"></div><div id="comment-40067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40084"></span>

<div id="answer-container-40084" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40084-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40084-score" class="post-score" title="current number of votes">1</div><span id="post-40084-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>One source would be the sharkfest challenge(s)<br />
<a href="http://sharkfest.wireshark.org/sharkfest.14/index.html">http://sharkfest.wireshark.org/sharkfest.14/index.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Feb '15, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span> </br></p></div></div><div id="comments-container-40084" class="comments-container"></div><div id="comment-tools-40084" class="comment-tools"></div><div class="clear"></div><div id="comment-40084-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

