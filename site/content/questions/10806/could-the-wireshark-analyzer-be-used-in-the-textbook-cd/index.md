+++
type = "question"
title = "could the wireshark analyzer be used in the textbook cd"
description = '''hello，my tutor wrote a textbook about network protocol analysis, and the book had network packet capture experiments. The wireshark is free of charge and open-source, so we planned to include the software in the textbook CD, and the CD is a gift with the book. Is this legitimate?'''
date = "2012-05-08T23:51:00Z"
lastmod = "2012-05-09T00:08:00Z"
weight = 10806
keywords = [ "legitimate" ]
aliases = [ "/questions/10806" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [could the wireshark analyzer be used in the textbook cd](/questions/10806/could-the-wireshark-analyzer-be-used-in-the-textbook-cd)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10806-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10806-score" class="post-score" title="current number of votes">0</div><span id="post-10806-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello，my tutor wrote a textbook about network protocol analysis, and the book had network packet capture experiments. The wireshark is free of charge and open-source, so we planned to include the software in the textbook CD, and the CD is a gift with the book. Is this legitimate?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-legitimate" rel="tag" title="see questions tagged &#39;legitimate&#39;">legitimate</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '12, 23:51</strong></p><img src="https://secure.gravatar.com/avatar/ac9aa1cdba7908e826a6a1d995a69667?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wei167&#39;s gravatar image" /><p><span>wei167</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wei167 has no accepted answers">0%</span></p></div></div><div id="comments-container-10806" class="comments-container"></div><div id="comment-tools-10806" class="comment-tools"></div><div class="clear"></div><div id="comment-10806-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10808"></span>

<div id="answer-container-10808" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10808-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10808-score" class="post-score" title="current number of votes">2</div><span id="post-10808-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wei167 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The license of Wireshark is GPL V2 and partly GPL V3. As long as you adhere to these licenses you may modify, copy and distribute Wireshark, which includes your textbook CD. Please consult the GPL for detailed information.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '12, 00:08</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-10808" class="comments-container"></div><div id="comment-tools-10808" class="comment-tools"></div><div class="clear"></div><div id="comment-10808-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

