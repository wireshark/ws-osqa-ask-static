+++
type = "question"
title = "Excessive Retries on CCTV"
description = '''We work in a very CCTV driven enviroment. We have about 10 dedicated PCs to watching either cameras or DVRs over the LAN. The CCTV is on its own VLAN. I am currently spanning the switch port that the CCTV router is on. We are getting flooded with TCP retransmissions and TCP Duplicate packets on that...'''
date = "2013-09-18T13:55:00Z"
lastmod = "2013-09-18T13:55:00Z"
weight = 24927
keywords = [ "excessive", "retransmissions", "tcp" ]
aliases = [ "/questions/24927" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Excessive Retries on CCTV](/questions/24927/excessive-retries-on-cctv)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24927-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24927-score" class="post-score" title="current number of votes">0</div><span id="post-24927-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We work in a very CCTV driven enviroment. We have about 10 dedicated PCs to watching either cameras or DVRs over the LAN. The CCTV is on its own VLAN. I am currently spanning the switch port that the CCTV router is on. We are getting flooded with TCP retransmissions and TCP Duplicate packets on that port. It equates to about one retransmission per client per device per second. After doing much reading, I changed the speed and duplex settings to static entries and get the same results. Thoughts on what to look at?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-excessive" rel="tag" title="see questions tagged &#39;excessive&#39;">excessive</span> <span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '13, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/5e8086502b140f85c8223b4bc20f665e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rvdsabu4life&#39;s gravatar image" /><p><span>rvdsabu4life</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rvdsabu4life has no accepted answers">0%</span></p></div></div><div id="comments-container-24927" class="comments-container"></div><div id="comment-tools-24927" class="comment-tools"></div><div class="clear"></div><div id="comment-24927-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

