+++
type = "question"
title = "Multiple packet import from text"
description = '''Hi, i have a TCP stream with multiple request/response in a text file. When importing the text file, it seems that all the file is put into one packet. how do i distinguish between the different HTTP request/response while importing?'''
date = "2012-04-20T16:04:00Z"
lastmod = "2012-04-20T16:04:00Z"
weight = 10362
keywords = [ "import", "multiple", "packets" ]
aliases = [ "/questions/10362" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Multiple packet import from text](/questions/10362/multiple-packet-import-from-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10362-score" class="post-score" title="current number of votes">0</div><span id="post-10362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i have a TCP stream with multiple request/response in a text file. When importing the text file, it seems that all the file is put into one packet. how do i distinguish between the different HTTP request/response while importing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-import" rel="tag" title="see questions tagged &#39;import&#39;">import</span> <span class="post-tag tag-link-multiple" rel="tag" title="see questions tagged &#39;multiple&#39;">multiple</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '12, 16:04</strong></p><img src="https://secure.gravatar.com/avatar/ffeb42cf810a0d7e76cce4b9f7e77d6c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yuvalshu&#39;s gravatar image" /><p><span>yuvalshu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yuvalshu has no accepted answers">0%</span></p></div></div><div id="comments-container-10362" class="comments-container"></div><div id="comment-tools-10362" class="comment-tools"></div><div class="clear"></div><div id="comment-10362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

