+++
type = "question"
title = "how to get Header structure of these protocols?"
description = '''Hello everyone, I need header structure of these protocols please anyone help me ...,  X window system protocol Finger H.323 S 1 application protocol X 2 application protocol '''
date = "2016-07-09T04:27:00Z"
lastmod = "2016-07-12T07:43:00Z"
weight = 53953
keywords = [ "protocols" ]
aliases = [ "/questions/53953" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [how to get Header structure of these protocols?](/questions/53953/how-to-get-header-structure-of-these-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53953-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53953-score" class="post-score" title="current number of votes">0</div><span id="post-53953-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone, I need header structure of these protocols please anyone help me ...,</p><ol><li>X window system protocol</li><li>Finger</li><li>H.323</li><li>S 1 application protocol</li><li>X 2 application protocol</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '16, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/e24e64b978b2bf9d8208824c7b902cd5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Adarsha%20Verma&#39;s gravatar image" /><p><span>Adarsha Verma</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Adarsha Verma has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Jul '16, 07:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-53953" class="comments-container"></div><div id="comment-tools-53953" class="comment-tools"></div><div class="clear"></div><div id="comment-53953-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53956"></span>

<div id="answer-container-53956" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53956-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53956-score" class="post-score" title="current number of votes">4</div><span id="post-53956-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li><a href="https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html">X window protocol</a></li><li><a href="https://tools.ietf.org/html/rfc1288">Finger</a></li><li><a href="http://www.itu.int/rec/T-REC-H.323">H.323</a></li><li><a href="http://www.3gpp.org/DynaReport/36413.htm">S1 protocol</a></li><li><a href="http://www.3gpp.org/DynaReport/36423.htm">X2 protocol</a></li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jul '16, 07:13</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-53956" class="comments-container"><span id="53979"></span><div id="comment-53979" class="comment"><div id="post-53979-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your reply Jaap But in these files i am not getting proper information to write C header structure for the above protocols. Please help me more.</p></div><div id="comment-53979-info" class="comment-info"><span class="comment-age">(11 Jul '16, 06:01)</span> <span class="comment-user userinfo">Adarsha Verma</span></div></div><span id="53981"></span><div id="comment-53981" class="comment"><div id="post-53981-score" class="comment-score">1</div><div class="comment-text"><p>This the the most detailed information you are going to find, short of having the C code written for you. If you are looking for that, you'll have to seek published source code of programs implementing these protocol and extract the C code from there.</p></div><div id="comment-53981-info" class="comment-info"><span class="comment-age">(11 Jul '16, 07:38)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="54000"></span><div id="comment-54000" class="comment"><div id="post-54000-score" class="comment-score"></div><div class="comment-text"><p>Ok thank you jaap. Your response helped me lot.</p></div><div id="comment-54000-info" class="comment-info"><span class="comment-age">(12 Jul '16, 00:26)</span> <span class="comment-user userinfo">Adarsha Verma</span></div></div><span id="54001"></span><div id="comment-54001" class="comment"><div id="post-54001-score" class="comment-score"></div><div class="comment-text"><p>If you have any link with respect to this protocols please let me know. 1. Msrp 2. Smtp Thank you in advance.</p></div><div id="comment-54001-info" class="comment-info"><span class="comment-age">(12 Jul '16, 00:29)</span> <span class="comment-user userinfo">Adarsha Verma</span></div></div><span id="54002"></span><div id="comment-54002" class="comment not_top_scorer"><div id="post-54002-score" class="comment-score"></div><div class="comment-text"><p>Seriously, you should be able to google that stuff yourself - it's not that hard. Searching for "[protocol name] rfc" does help a lot, e.g. "msrp rfc" or "smtp rfc".</p></div><div id="comment-54002-info" class="comment-info"><span class="comment-age">(12 Jul '16, 01:08)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="54003"></span><div id="comment-54003" class="comment"><div id="post-54003-score" class="comment-score">1</div><div class="comment-text"><p>Jasper has a point:</p><ol><li><a href="https://tools.ietf.org/html/rfc4975">MSRP</a></li><li><a href="https://tools.ietf.org/html/rfc5321">SMTP</a></li></ol><p>Oh, and if you're looking for code, the Wireshark <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=tree;f=epan/dissectors;hb=refs/heads/master">dissector code</a> may be of use as well.</p></div><div id="comment-54003-info" class="comment-info"><span class="comment-age">(12 Jul '16, 02:16)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="54007"></span><div id="comment-54007" class="comment not_top_scorer"><div id="post-54007-score" class="comment-score"></div><div class="comment-text"><p>Thanks alot Jaap</p></div><div id="comment-54007-info" class="comment-info"><span class="comment-age">(12 Jul '16, 07:43)</span> <span class="comment-user userinfo">Adarsha Verma</span></div></div></div><div id="comment-tools-53956" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-53956-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

