+++
type = "question"
title = "TLS decryption not applied on full TCP stream"
description = '''Hi I&#x27;ve got a very annoying bug I can&#x27;t resolve I&#x27;m capturing TLS traffic with tcpdump on a remote host, and then decrypt it with Wireshark Decryption seems to work, but on one given TLS stream, I can see the first messages correctly decrypted, but after a few messages (which number may vary a lot),...'''
date = "2014-07-22T05:10:00Z"
lastmod = "2014-07-22T05:10:00Z"
weight = 34824
keywords = [ "tls" ]
aliases = [ "/questions/34824" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TLS decryption not applied on full TCP stream](/questions/34824/tls-decryption-not-applied-on-full-tcp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34824-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34824-score" class="post-score" title="current number of votes">0</div><span id="post-34824-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I've got a very annoying bug I can't resolve I'm capturing TLS traffic with tcpdump on a remote host, and then decrypt it with Wireshark Decryption seems to work, but on one given TLS stream, I can see the first messages correctly decrypted, but after a few messages (which number may vary a lot), Wireshark only displays encrypted data Note that in the SSL debug file, I can see some of those messages correctly decrypted Is this a known issue or a bad use on my side ?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tls" rel="tag" title="see questions tagged &#39;tls&#39;">tls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '14, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/4f9ad99f3f1ab64f93c48bfd4da3d8a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="biniou&#39;s gravatar image" /><p><span>biniou</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="biniou has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Jul '14, 05:10</strong> </span></p></div></div><div id="comments-container-34824" class="comments-container"></div><div id="comment-tools-34824" class="comment-tools"></div><div class="clear"></div><div id="comment-34824-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

