+++
type = "question"
title = "Unable to discover any interface on computer"
description = '''After intalling Wireshark Ver. 1.62, it can&#x27;t discover any interface to capture on my computer.  At each start-up it shows a warning that &#x27;NPF Driver isn&#x27;t Running&#x27; and that I won&#x27;t be able to capture. How do I get the &#x27;NPF Driver&#x27; to run? I&#x27;m running Wireshark on Windows 7 OS. Thanks, Kay.'''
date = "2014-02-08T19:42:00Z"
lastmod = "2014-02-09T02:59:00Z"
weight = 29560
keywords = [ "not", "running", "npf" ]
aliases = [ "/questions/29560" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to discover any interface on computer](/questions/29560/unable-to-discover-any-interface-on-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29560-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29560-score" class="post-score" title="current number of votes">0</div><span id="post-29560-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After intalling Wireshark Ver. 1.62, it can't discover any interface to capture on my computer. At each start-up it shows a warning that 'NPF Driver isn't Running' and that I won't be able to capture. How do I get the 'NPF Driver' to run? I'm running Wireshark on Windows 7 OS. Thanks, Kay.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-running" rel="tag" title="see questions tagged &#39;running&#39;">running</span> <span class="post-tag tag-link-npf" rel="tag" title="see questions tagged &#39;npf&#39;">npf</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '14, 19:42</strong></p><img src="https://secure.gravatar.com/avatar/8aae9dc07410183c5140c6a2f759066a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kay&#39;s gravatar image" /><p><span>Kay</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kay has no accepted answers">0%</span></p></div></div><div id="comments-container-29560" class="comments-container"><span id="29568"></span><div id="comment-29568" class="comment"><div id="post-29568-score" class="comment-score"></div><div class="comment-text"><p>Wireshark 1.62 doesn't exist, 1.6.2 does. Regardless, it is an old and unsupported version, you should upgrade to the latest from the Wireshark <a href="http://www.wireshark.org/download.html">Download</a> page.</p></div><div id="comment-29568-info" class="comment-info"><span class="comment-age">(09 Feb '14, 02:59)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-29560" class="comment-tools"></div><div class="clear"></div><div id="comment-29560-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29561"></span>

<div id="answer-container-29561" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29561-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29561-score" class="post-score" title="current number of votes">0</div><span id="post-29561-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I can now see available network interfaces on my computer and can capture data on the interfaces. I configured the npf driver to auto-start at computer/windows start-up.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Feb '14, 21:07</strong></p><img src="https://secure.gravatar.com/avatar/8aae9dc07410183c5140c6a2f759066a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kay&#39;s gravatar image" /><p><span>Kay</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kay has no accepted answers">0%</span></p></div></div><div id="comments-container-29561" class="comments-container"></div><div id="comment-tools-29561" class="comment-tools"></div><div class="clear"></div><div id="comment-29561-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

