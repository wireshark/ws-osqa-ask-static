+++
type = "question"
title = "devices stop communication over the LAN"
description = '''Dear Expert, I have an issue in my network that my random devices Ethernet are dropped communication over the network, by changing LAN card is fixed the issue but temporary. it is happened with random devices over the lan , such as TMG Server local lan, Database server lan, Mikrotik local lan . and ...'''
date = "2017-02-10T22:19:00Z"
lastmod = "2017-02-10T23:33:00Z"
weight = 59341
keywords = [ "network" ]
aliases = [ "/questions/59341" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [devices stop communication over the LAN](/questions/59341/devices-stop-communication-over-the-lan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59341-score" class="post-score" title="current number of votes">0</div><span id="post-59341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Expert,</p><p>I have an issue in my network that my random devices Ethernet are dropped communication over the network, by changing LAN card is fixed the issue but temporary.</p><p>it is happened with random devices over the lan , such as TMG Server local lan, Database server lan, Mikrotik local lan . and some hosts .</p><p>please help me .</p><p>Regards,</p><p>Safder Abbas</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '17, 22:19</strong></p><img src="https://secure.gravatar.com/avatar/282baba6d95d33aad015c6e98ff53cef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Safder%20Abbas&#39;s gravatar image" /><p><span>Safder Abbas</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Safder Abbas has no accepted answers">0%</span></p></div></div><div id="comments-container-59341" class="comments-container"><span id="59342"></span><div id="comment-59342" class="comment"><div id="post-59342-score" class="comment-score"></div><div class="comment-text"><p>A few questions:</p><p>When the problem happens, what is the status oof the Ethernet adapter? Does it still show Connected?</p><p>How do you recover from the problem? Do you have to power the computer off and on, do you just reboot, dooes it recover automatically, etc?</p></div><div id="comment-59342-info" class="comment-info"><span class="comment-age">(10 Feb '17, 23:33)</span> <span class="comment-user userinfo">PaulOfford</span></div></div></div><div id="comment-tools-59341" class="comment-tools"></div><div class="clear"></div><div id="comment-59341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

