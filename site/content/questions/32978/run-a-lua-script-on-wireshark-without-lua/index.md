+++
type = "question"
title = "Run a lua script on Wireshark without Lua"
description = '''Hi everyone, I am using a wireshark version (1.0.15) without Lua on a server without root privilege. I need to run a lua_script but this could not be loaded as the version is without Lua. I could not find init.lua in global configuration folder. I have copied init.lua file to personal configuration ...'''
date = "2014-05-22T02:45:00Z"
lastmod = "2014-05-22T06:47:00Z"
weight = 32978
keywords = [ "lua" ]
aliases = [ "/questions/32978" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Run a lua script on Wireshark without Lua](/questions/32978/run-a-lua-script-on-wireshark-without-lua)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32978-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32978-score" class="post-score" title="current number of votes">0</div><span id="post-32978-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone,</p><p>I am using a wireshark version (1.0.15) without Lua on a server without root privilege. I need to run a lua_script but this could not be loaded as the version is without Lua. I could not find init.lua in global configuration folder. I have copied init.lua file to personal configuration folder. However, it still doesn't work. Hence, I would like to ask whether there is a way to execute a lua_script on wireshark without Lua.</p><p>Thanks a lot, Andy<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '14, 02:45</strong></p><img src="https://secure.gravatar.com/avatar/73851f29d1d6c5f3f62ae4d225175626?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anh%20Nguyen&#39;s gravatar image" /><p><span>Anh Nguyen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anh Nguyen has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-32978" class="comments-container"></div><div id="comment-tools-32978" class="comment-tools"></div><div class="clear"></div><div id="comment-32978-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32995"></span>

<div id="answer-container-32995" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32995-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32995-score" class="post-score" title="current number of votes">0</div><span id="post-32995-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would think not. If Wireshark is compiled without LUA, code to handle LUA will be missing from the resulting binary.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '14, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-32995" class="comments-container"></div><div id="comment-tools-32995" class="comment-tools"></div><div class="clear"></div><div id="comment-32995-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

