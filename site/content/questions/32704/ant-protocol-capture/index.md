+++
type = "question"
title = "Ant protocol capture"
description = '''I am trying to understand ant protocol. Could anyone help me out with capturing ant packet?'''
date = "2014-05-10T08:46:00Z"
lastmod = "2014-05-10T08:46:00Z"
weight = 32704
keywords = [ "capture", "protocol" ]
aliases = [ "/questions/32704" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ant protocol capture](/questions/32704/ant-protocol-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32704-score" class="post-score" title="current number of votes">0</div><span id="post-32704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to understand <a href="http://en.wikipedia.org/wiki/ANT_(network)">ant protocol</a>. Could anyone help me out with capturing ant packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '14, 08:46</strong></p><img src="https://secure.gravatar.com/avatar/fca3b4b7cf1a25f4c12b504146a8e69e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Replin&#39;s gravatar image" /><p><span>Replin</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Replin has no accepted answers">0%</span></p></div></div><div id="comments-container-32704" class="comments-container"></div><div id="comment-tools-32704" class="comment-tools"></div><div class="clear"></div><div id="comment-32704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

