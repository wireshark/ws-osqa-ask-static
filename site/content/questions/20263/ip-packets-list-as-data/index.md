+++
type = "question"
title = "IP packets list as data"
description = '''What is the expression to find all of the IP packets listed as Data??'''
date = "2013-04-10T00:25:00Z"
lastmod = "2013-04-10T05:32:00Z"
weight = 20263
keywords = [ "ip", "data" ]
aliases = [ "/questions/20263" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IP packets list as data](/questions/20263/ip-packets-list-as-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20263-score" class="post-score" title="current number of votes">0</div><span id="post-20263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is the expression to find all of the IP packets listed as Data??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '13, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/df3c13117e66cbe6771a23abb883af9a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ramyarani&#39;s gravatar image" /><p><span>Ramyarani</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ramyarani has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Apr '13, 03:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-20263" class="comments-container"><span id="20274"></span><div id="comment-20274" class="comment"><div id="post-20274-score" class="comment-score"></div><div class="comment-text"><p>I don't understand. Can you please add more details? What are you interested? A list of IP addresses in a capture file?</p></div><div id="comment-20274-info" class="comment-info"><span class="comment-age">(10 Apr '13, 05:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20263" class="comment-tools"></div><div class="clear"></div><div id="comment-20263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

