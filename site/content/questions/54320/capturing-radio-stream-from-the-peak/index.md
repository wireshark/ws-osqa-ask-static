+++
type = "question"
title = "Capturing Radio Stream from The Peak"
description = '''So I&#x27;m trying to capture the radio stream from a radio station called &quot;The Peak&quot;. It&#x27;s a great station, but I&#x27;d love to be able to play it in my local music player instead of via their website popup. The problem is, as far as I can tell the http address of the actual stream seems to have some sort o...'''
date = "2016-07-25T18:01:00Z"
lastmod = "2016-07-25T22:23:00Z"
weight = 54320
keywords = [ "capture", "radio", "http", "stream" ]
aliases = [ "/questions/54320" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing Radio Stream from The Peak](/questions/54320/capturing-radio-stream-from-the-peak)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54320-score" class="post-score" title="current number of votes">0</div><span id="post-54320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So I'm trying to capture the radio stream from a radio station called "The Peak". It's a great station, but I'd love to be able to play it in my local music player instead of via their website popup.</p><p>The problem is, as far as I can tell the http address of the actual stream seems to have some sort of code on the end of it that changes every so often and not including it in the address does not seem to work. But this is the first time I've used Wireshark, so it's entirely possible I'm doing something wrong.</p><p>If anybody feels like taking a look at it and deciphering whether I'm just being dumb or not, it would be super appreciated, thank you!</p><p>This is the address of the stream's website: <a href="http://ckpk.streamon.fm/#">http://ckpk.streamon.fm/#</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-radio" rel="tag" title="see questions tagged &#39;radio&#39;">radio</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jul '16, 18:01</strong></p><img src="https://secure.gravatar.com/avatar/713afadb17f699e8903a2f715974064e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ibigfire&#39;s gravatar image" /><p><span>ibigfire</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ibigfire has no accepted answers">0%</span></p></div></div><div id="comments-container-54320" class="comments-container"></div><div id="comment-tools-54320" class="comment-tools"></div><div class="clear"></div><div id="comment-54320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54324"></span>

<div id="answer-container-54324" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54324-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54324-score" class="post-score" title="current number of votes">0</div><span id="post-54324-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Without having looked at it, it is likely they append some sort of session ID as a parameter to the URL. That's probably to do with the internal workings of the web app.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jul '16, 22:23</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-54324" class="comments-container"></div><div id="comment-tools-54324" class="comment-tools"></div><div class="clear"></div><div id="comment-54324-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

