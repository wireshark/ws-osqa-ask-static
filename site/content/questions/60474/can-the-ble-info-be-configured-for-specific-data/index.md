+++
type = "question"
title = "Can the BLE Info be configured for specific data?"
description = '''I am sending out a user defined data for advertise. WireShark indicates this a s Malformed Packet. Is it possible to define this data so it is handled correctly by WireShark?'''
date = "2017-03-31T04:17:00Z"
lastmod = "2017-03-31T04:17:00Z"
weight = 60474
keywords = [ "ble", "info", "malformedpacket" ]
aliases = [ "/questions/60474" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can the BLE Info be configured for specific data?](/questions/60474/can-the-ble-info-be-configured-for-specific-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60474-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60474-score" class="post-score" title="current number of votes">0</div><span id="post-60474-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am sending out a user defined data for advertise. WireShark indicates this a s Malformed Packet. Is it possible to define this data so it is handled correctly by WireShark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ble" rel="tag" title="see questions tagged &#39;ble&#39;">ble</span> <span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-malformedpacket" rel="tag" title="see questions tagged &#39;malformedpacket&#39;">malformedpacket</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '17, 04:17</strong></p><img src="https://secure.gravatar.com/avatar/7ed6c9315b75f9e0f177249aa81d0e5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rbmisc&#39;s gravatar image" /><p><span>rbmisc</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rbmisc has no accepted answers">0%</span></p></div></div><div id="comments-container-60474" class="comments-container"></div><div id="comment-tools-60474" class="comment-tools"></div><div class="clear"></div><div id="comment-60474-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

