+++
type = "question"
title = "Constant RTT in slow start"
description = '''Hello everybody, I&#x27;m analyzing a round trip time graph that was acquired by using wireshark. It&#x27;s a slow start algorithm, so I am looking at an increasing RTT at the beginning, but around sequence number 130000, RTT becomes constant, then starts increasing again. I&#x27;m trying to find what causes this....'''
date = "2013-04-03T19:44:00Z"
lastmod = "2013-04-08T15:36:00Z"
weight = 20071
keywords = [ "rtt", "graph", "wireshark" ]
aliases = [ "/questions/20071" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Constant RTT in slow start](/questions/20071/constant-rtt-in-slow-start)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20071-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20071-score" class="post-score" title="current number of votes">0</div><span id="post-20071-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everybody,</p><p>I'm analyzing a round trip time graph that was acquired by using wireshark. It's a slow start algorithm, so I am looking at an increasing RTT at the beginning, but around sequence number 130000, RTT becomes constant, then starts increasing again. I'm trying to find what causes this.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtt" rel="tag" title="see questions tagged &#39;rtt&#39;">rtt</span> <span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Apr '13, 19:44</strong></p><img src="https://secure.gravatar.com/avatar/fa695f7c1767bcea14674d8e11b737f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cemil%20Toygan%20Ozyalcin&#39;s gravatar image" /><p><span>Cemil Toygan...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cemil Toygan Ozyalcin has no accepted answers">0%</span></p></div></div><div id="comments-container-20071" class="comments-container"><span id="20211"></span><div id="comment-20211" class="comment"><div id="post-20211-score" class="comment-score"></div><div class="comment-text"><p>can you please add a screenshot of the RTT graph?</p></div><div id="comment-20211-info" class="comment-info"><span class="comment-age">(08 Apr '13, 15:36)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20071" class="comment-tools"></div><div class="clear"></div><div id="comment-20071-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

