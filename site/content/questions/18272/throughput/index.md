+++
type = "question"
title = "Throughput"
description = '''Hello,  I have captured the Wire-shark file on a microwave Link interface , Which is feeding a 3G Site, Having Low throughput Issue. Its using UDP Frames for sending the User Data towards Core on the MW link, though the Traffic generated from Subscriber is TCP. But frames are having Length of Below ...'''
date = "2013-02-03T23:01:00Z"
lastmod = "2013-02-05T13:38:00Z"
weight = 18272
keywords = [ "udp" ]
aliases = [ "/questions/18272" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Throughput](/questions/18272/throughput)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18272-score" class="post-score" title="current number of votes">0</div><span id="post-18272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have captured the Wire-shark file on a microwave Link interface , Which is feeding a 3G Site, Having Low throughput Issue. Its using UDP Frames for sending the User Data towards Core on the MW link, though the Traffic generated from Subscriber is TCP. But frames are having Length of Below Hundred Bytes.near 87 Bytes..Can i Conclude that throughput can be a Issue as Frames are getting fragmented for delivery..</p><p>BR// Dipankar</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '13, 23:01</strong></p><img src="https://secure.gravatar.com/avatar/d853d25b05d460fb0b1224dde30b4f9f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dipankar%20Shaw&#39;s gravatar image" /><p><span>Dipankar Shaw</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dipankar Shaw has no accepted answers">0%</span></p></div></div><div id="comments-container-18272" class="comments-container"></div><div id="comment-tools-18272" class="comment-tools"></div><div class="clear"></div><div id="comment-18272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18327"></span>

<div id="answer-container-18327" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18327-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18327-score" class="post-score" title="current number of votes">0</div><span id="post-18327-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Can i Conclude that throughput can be a Issue as Frames are getting fragmented for delivery..</p></blockquote><p>I personally would not be able to conclude that, at least not without more information about the actual setup ;-)</p><p>Can you please add some ASCII art (or a simple, scanned, hand painted network diagram) that shows your network setup and where the data is sent via TCP, where the transition to UDP is made and where you believe that IP fragmentation takes place (do you actually see that in the capture file?).</p><p>Do you have a capture of the TCP data stream <strong>and</strong> the UDP data stream in parallel?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '13, 13:38</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Feb '13, 13:39</strong> </span></p></div></div><div id="comments-container-18327" class="comments-container"></div><div id="comment-tools-18327" class="comment-tools"></div><div class="clear"></div><div id="comment-18327-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

