+++
type = "question"
title = "Encrypted Alert"
description = '''I logged into my laptop, started wireshark, there are no running apps except KIS. Now in the wireshark log I see the attached Encrypted Alert. why did this alert pop when where are not applications running ?'''
date = "2017-04-22T03:14:00Z"
lastmod = "2017-04-22T22:49:00Z"
weight = 60963
keywords = [ "encrypted", "alert" ]
aliases = [ "/questions/60963" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Encrypted Alert](/questions/60963/encrypted-alert)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60963-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60963-score" class="post-score" title="current number of votes">0</div><span id="post-60963-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I logged into my laptop, started wireshark, there are no running apps except KIS. Now in the wireshark log I see the attached Encrypted Alert. why did this alert pop when where are not applications running ?<img src="https://osqa-ask.wireshark.org/upfiles/enc_alert.PNG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encrypted" rel="tag" title="see questions tagged &#39;encrypted&#39;">encrypted</span> <span class="post-tag tag-link-alert" rel="tag" title="see questions tagged &#39;alert&#39;">alert</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '17, 03:14</strong></p><img src="https://secure.gravatar.com/avatar/4debf4d644c7320e639547bd1b13c1b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="w_keyboard&#39;s gravatar image" /><p><span>w_keyboard</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="w_keyboard has no accepted answers">0%</span></p></img></div></div><div id="comments-container-60963" class="comments-container"></div><div id="comment-tools-60963" class="comment-tools"></div><div class="clear"></div><div id="comment-60963-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60967"></span>

<div id="answer-container-60967" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60967-score" class="post-score" title="current number of votes">0</div><span id="post-60967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You say no applications are running. But services are also capable of connecting to the outside world. Never assume since you're not running an application that your computer is idle.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Apr '17, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60967" class="comments-container"><span id="60976"></span><div id="comment-60976" class="comment"><div id="post-60976-score" class="comment-score"></div><div class="comment-text"><p>ok, so how can I know which application is trying to connect to 52.222.136.108 ?</p></div><div id="comment-60976-info" class="comment-info"><span class="comment-age">(22 Apr '17, 22:49)</span> <span class="comment-user userinfo">w_keyboard</span></div></div></div><div id="comment-tools-60967" class="comment-tools"></div><div class="clear"></div><div id="comment-60967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

