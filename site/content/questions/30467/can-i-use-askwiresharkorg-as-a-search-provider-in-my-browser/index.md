+++
type = "question"
title = "Can i use &quot;ask.wireshark.org&quot; as a search provider in my browser?"
description = '''Hi all, i&#x27;d like to use &quot;ask.wireshark.org&quot; as a search provider (in IE) or search engine(in FF) in my browser. Now adding it works fine, but no results to my Q&#x27;s .. it seems to use a token? '''
date = "2014-03-06T01:20:00Z"
lastmod = "2014-03-06T13:51:00Z"
weight = 30467
keywords = [ "searchengine", "ask.wireshark.org", "searchprovider" ]
aliases = [ "/questions/30467" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Can i use "ask.wireshark.org" as a search provider in my browser?](/questions/30467/can-i-use-askwiresharkorg-as-a-search-provider-in-my-browser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30467-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30467-score" class="post-score" title="current number of votes">0</div><span id="post-30467-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>i'd like to use "ask.wireshark.org" as a search provider (in IE) or search engine(in FF) in my browser. Now adding it works fine, but no results to my Q's .. it seems to use a token?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-searchengine" rel="tag" title="see questions tagged &#39;searchengine&#39;">searchengine</span> <span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span> <span class="post-tag tag-link-searchprovider" rel="tag" title="see questions tagged &#39;searchprovider&#39;">searchprovider</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '14, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/69710b84acce4cdf0a0cbdcb5930fda1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marc&#39;s gravatar image" /><p><span>Marc</span><br />
<span class="score" title="147 reputation points">147</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marc has 3 accepted answers">27%</span></p></div></div><div id="comments-container-30467" class="comments-container"></div><div id="comment-tools-30467" class="comment-tools"></div><div class="clear"></div><div id="comment-30467-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30503"></span>

<div id="answer-container-30503" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30503-score" class="post-score" title="current number of votes">1</div><span id="post-30503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Marc has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://www.google.com/?q=firefox%20add%20custom%20search%20provider">Follow the instruction of your browser</a> to add a new search provider, then add the following 'search engine prefix'</p><blockquote><p><a href="http://ask.wireshark.org/search/?q=">http://ask.wireshark.org/search/?q=</a></p></blockquote><p>or</p><blockquote><p><a href="http://ask.wireshark.org/tags/?q=">http://ask.wireshark.org/tags/?q=</a></p></blockquote><p>for a Tag search.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Mar '14, 13:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Mar '14, 13:47</strong> </span></p></div></div><div id="comments-container-30503" class="comments-container"><span id="30504"></span><div id="comment-30504" class="comment"><div id="post-30504-score" class="comment-score"></div><div class="comment-text"><p>BTW: Is it possible to go through some of your old questions and mark those as <strong>answered</strong>, where you got an acceptable answer (see the FAQ of this site)?</p><blockquote><p><a href="http://ask.wireshark.org/faq/">http://ask.wireshark.org/faq/</a></p></blockquote><p>Thanks!</p></div><div id="comment-30504-info" class="comment-info"><span class="comment-age">(06 Mar '14, 13:51)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30503" class="comment-tools"></div><div class="clear"></div><div id="comment-30503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

