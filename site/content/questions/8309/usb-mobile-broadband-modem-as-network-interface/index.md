+++
type = "question"
title = "USB Mobile Broadband Modem as Network Interface"
description = '''In reading up on earlier posts, it does not seem like there is a way to recognize mobile broadband USB modems connected to Windows 7 as network interfaces in Wireshark. Is that still the case? I wanted get a more recent confirmation on this? In my case, I am trying to monitor the Sierra Wireless ATT...'''
date = "2012-01-10T13:23:00Z"
lastmod = "2012-07-16T04:39:00Z"
weight = 8309
keywords = [ "mobile", "sierrawireless", "usbmodem" ]
aliases = [ "/questions/8309" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [USB Mobile Broadband Modem as Network Interface](/questions/8309/usb-mobile-broadband-modem-as-network-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8309-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8309-score" class="post-score" title="current number of votes">0</div><span id="post-8309-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In reading up on earlier posts, it does not seem like there is a way to recognize mobile broadband USB modems connected to Windows 7 as network interfaces in Wireshark. Is that still the case? I wanted get a more recent confirmation on this?</p><p>In my case, I am trying to monitor the Sierra Wireless ATT 4G LTE USB Modem. http://www.sierrawireless.com/productsandservices/AirCard/USBModems/AirCard_313U.aspx</p><p>Thanks! Ray</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-sierrawireless" rel="tag" title="see questions tagged &#39;sierrawireless&#39;">sierrawireless</span> <span class="post-tag tag-link-usbmodem" rel="tag" title="see questions tagged &#39;usbmodem&#39;">usbmodem</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jan '12, 13:23</strong></p><img src="https://secure.gravatar.com/avatar/52b9f1fac0859c8a72ad3a2d49e7cf90?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rayusa123&#39;s gravatar image" /><p><span>rayusa123</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rayusa123 has no accepted answers">0%</span></p></div></div><div id="comments-container-8309" class="comments-container"></div><div id="comment-tools-8309" class="comment-tools"></div><div class="clear"></div><div id="comment-8309-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="8312"></span>

<div id="answer-container-8312" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8312-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8312-score" class="post-score" title="current number of votes">0</div><span id="post-8312-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, that's still the case.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '12, 18:05</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-8312" class="comments-container"></div><div id="comment-tools-8312" class="comment-tools"></div><div class="clear"></div><div id="comment-8312-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8327"></span>

<div id="answer-container-8327" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8327-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8327-score" class="post-score" title="current number of votes">0</div><span id="post-8327-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Depending on your requirements it may not be answer to your question</p><p>I had similar requirement for capturing traffic via 3G dongle and ended up running linux box in vmware with usb pass through for the dongle to linux box and using wvdial to make start the connection.</p><p>You can then reroute windows traffic via the vmware machine and capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '12, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/96df873546556d82f89c599816554877?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="izopizo&#39;s gravatar image" /><p><span>izopizo</span><br />
<span class="score" title="202 reputation points">202</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="izopizo has no accepted answers">0%</span></p></div></div><div id="comments-container-8327" class="comments-container"></div><div id="comment-tools-8327" class="comment-tools"></div><div class="clear"></div><div id="comment-8327-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="12745"></span>

<div id="answer-container-12745" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12745-score" class="post-score" title="current number of votes">0</div><span id="post-12745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Our company made new filter driver for mobile broadband network interface.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jul '12, 04:39</strong></p><img src="https://secure.gravatar.com/avatar/a2cfcbb9754e717cee243a66c37ab35d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Louis&#39;s gravatar image" /><p><span>Louis</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Louis has no accepted answers">0%</span></p></div></div><div id="comments-container-12745" class="comments-container"></div><div id="comment-tools-12745" class="comment-tools"></div><div class="clear"></div><div id="comment-12745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

