+++
type = "question"
title = "IE11 sent RST flag"
description = '''We noticed that IE11 will send a RST flag to server once in a which causes the connection to the server disconnected. We can&#x27;t figure out what is it in IE11 that will send RST flag and it happens intermittently. We tested with Chrome and this issue never happens at all.  Anyone can give some advise ...'''
date = "2017-05-15T19:00:00Z"
lastmod = "2017-05-15T19:00:00Z"
weight = 61417
keywords = [ "rst", "ie11", "tcp" ]
aliases = [ "/questions/61417" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IE11 sent RST flag](/questions/61417/ie11-sent-rst-flag)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61417-score" class="post-score" title="current number of votes">0</div><span id="post-61417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We noticed that IE11 will send a RST flag to server once in a which causes the connection to the server disconnected. We can't figure out what is it in IE11 that will send RST flag and it happens intermittently. We tested with Chrome and this issue never happens at all.</p><p>Anyone can give some advise what can be the cause of the problem? Is it some setting in IE11 that causes this? Seek advise from the experts here. <img src="https://osqa-ask.wireshark.org/upfiles/2017_05_16_09_51_12_Capturing_from_Wireless_Network_Connection_B2hXg5e.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rst" rel="tag" title="see questions tagged &#39;rst&#39;">rst</span> <span class="post-tag tag-link-ie11" rel="tag" title="see questions tagged &#39;ie11&#39;">ie11</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '17, 19:00</strong></p><img src="https://secure.gravatar.com/avatar/f0f5644b6f3f77a729eb3d51b8dec1a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lkhua79&#39;s gravatar image" /><p><span>lkhua79</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lkhua79 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61417" class="comments-container"></div><div id="comment-tools-61417" class="comment-tools"></div><div class="clear"></div><div id="comment-61417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

