+++
type = "question"
title = "Initial Speaker (SIP)"
description = '''Hi all,  I&#x27;m catching a flow of SIP traffic and would like to filter the calls captured by the initial speaker, but i havent fonund a way to do so, any suggestions?'''
date = "2011-02-23T10:53:00Z"
lastmod = "2011-02-23T11:22:00Z"
weight = 2524
keywords = [ "-", "initial", "speaker", "sip" ]
aliases = [ "/questions/2524" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Initial Speaker (SIP)](/questions/2524/initial-speaker-sip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2524-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2524-score" class="post-score" title="current number of votes">0</div><span id="post-2524-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I'm catching a flow of SIP traffic and would like to filter the calls captured by the initial speaker, but i havent fonund a way to do so, any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link--" rel="tag" title="see questions tagged &#39;-&#39;">-</span> <span class="post-tag tag-link-initial" rel="tag" title="see questions tagged &#39;initial&#39;">initial</span> <span class="post-tag tag-link-speaker" rel="tag" title="see questions tagged &#39;speaker&#39;">speaker</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '11, 10:53</strong></p><img src="https://secure.gravatar.com/avatar/dae5978aca8b788c2880ad1232727a09?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jsarante&#39;s gravatar image" /><p><span>jsarante</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jsarante has no accepted answers">0%</span></p></div></div><div id="comments-container-2524" class="comments-container"></div><div id="comment-tools-2524" class="comment-tools"></div><div class="clear"></div><div id="comment-2524-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2527"></span>

<div id="answer-container-2527" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2527-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2527-score" class="post-score" title="current number of votes">0</div><span id="post-2527-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What about something like this:-</p><p>sip.from.user == "1234567890"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '11, 11:22</strong></p><img src="https://secure.gravatar.com/avatar/030196d67dc4e2b8f4ecff65eefdb63e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KeithFrench&#39;s gravatar image" /><p><span>KeithFrench</span><br />
<span class="score" title="121 reputation points">121</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KeithFrench has no accepted answers">0%</span></p></div></div><div id="comments-container-2527" class="comments-container"></div><div id="comment-tools-2527" class="comment-tools"></div><div class="clear"></div><div id="comment-2527-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

