+++
type = "question"
title = "PacketreceivePacket failed"
description = '''I am using WinPcap version 4.1.1 and if I start my program, at some point I am seeing PacketReceivePacket failed error message. I am using windows2008. Please let me know why we are seeing this error and what is the workaround for it.'''
date = "2013-07-29T03:49:00Z"
lastmod = "2013-09-22T19:49:00Z"
weight = 23424
keywords = [ "packetreceivepacket", "winpcap" ]
aliases = [ "/questions/23424" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [PacketreceivePacket failed](/questions/23424/packetreceivepacket-failed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23424-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23424-score" class="post-score" title="current number of votes">0</div><span id="post-23424-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using WinPcap version 4.1.1 and if I start my program, at some point I am seeing PacketReceivePacket failed error message. I am using windows2008. Please let me know why we are seeing this error and what is the workaround for it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packetreceivepacket" rel="tag" title="see questions tagged &#39;packetreceivepacket&#39;">packetreceivepacket</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '13, 03:49</strong></p><img src="https://secure.gravatar.com/avatar/ddd41d33689eb4f161167f3ef89ad64d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sudhagar&#39;s gravatar image" /><p><span>Sudhagar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sudhagar has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jul '13, 09:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-23424" class="comments-container"><span id="23425"></span><div id="comment-23425" class="comment"><div id="post-23425-score" class="comment-score"></div><div class="comment-text"><p>Are you using WinPCap in conjunction with Wireshark, or in your own application? If the latter, then this isn't an Ask Wireshark question and you'll be better off looking for support from the WinPCap developers.</p><p>If the former, then can you describe what you are doing from a Wireshark point of view.</p></div><div id="comment-23425-info" class="comment-info"><span class="comment-age">(29 Jul '13, 03:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="23426"></span><div id="comment-23426" class="comment"><div id="post-23426-score" class="comment-score"></div><div class="comment-text"><blockquote><p>If the former, then can you describe what you are doing from a Wireshark point of view.</p></blockquote><p>.. and please post the wireshark version (wireshark -v).</p></div><div id="comment-23426-info" class="comment-info"><span class="comment-age">(29 Jul '13, 05:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="23443"></span><div id="comment-23443" class="comment"><div id="post-23443-score" class="comment-score"></div><div class="comment-text"><p>I am using winpcap binaires integrated with my application. I had posted the question to winpcap support as well. Not sure why it is throwing this error.</p></div><div id="comment-23443-info" class="comment-info"><span class="comment-age">(30 Jul '13, 02:07)</span> <span class="comment-user userinfo">Sudhagar</span></div></div><span id="25093"></span><div id="comment-25093" class="comment"><div id="post-25093-score" class="comment-score"></div><div class="comment-text"><p>I'm not sure if you received an answer from the WinPcap folks or not, but have you tried upgrading <a href="http://www.winpcap.org/install/default.htm">WinPcap</a> to the most recent version, currently 4.1.3?</p></div><div id="comment-25093-info" class="comment-info"><span class="comment-age">(22 Sep '13, 19:49)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-23424" class="comment-tools"></div><div class="clear"></div><div id="comment-23424-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

