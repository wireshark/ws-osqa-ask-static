+++
type = "question"
title = "COTP Traffic"
description = '''How can i generate COTP traffic , I need to analyze this protocols traffic for my job . can anyone please name any application that uses this protocol , it doesn&#x27;t matter what is the functionality of the application , i just need traffic .'''
date = "2014-08-03T06:09:00Z"
lastmod = "2014-08-03T11:53:00Z"
weight = 35101
keywords = [ "cotp", "clnp" ]
aliases = [ "/questions/35101" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [COTP Traffic](/questions/35101/cotp-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35101-score" class="post-score" title="current number of votes">0</div><span id="post-35101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i generate COTP traffic , I need to analyze this protocols traffic for my job .<br />
can anyone please name any application that uses this protocol , it doesn't matter what is the functionality of the application , i just need traffic .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cotp" rel="tag" title="see questions tagged &#39;cotp&#39;">cotp</span> <span class="post-tag tag-link-clnp" rel="tag" title="see questions tagged &#39;clnp&#39;">clnp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '14, 06:09</strong></p><img src="https://secure.gravatar.com/avatar/cce50cb41e08f84235b3bffa81b24e94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saeedh&#39;s gravatar image" /><p><span>saeedh</span><br />
<span class="score" title="26 reputation points">26</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saeedh has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-35101" class="comments-container"></div><div id="comment-tools-35101" class="comment-tools"></div><div class="clear"></div><div id="comment-35101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35103"></span>

<div id="answer-container-35103" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35103-score" class="post-score" title="current number of votes">0</div><span id="post-35103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="saeedh has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't know how to generate COTP traffic, but I can tell you where to find sample capture files.</p><blockquote><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1163">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1163</a><br />
<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7393">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7393</a><br />
<a href="http://www.pcapr.net/browse?q=cotp">http://www.pcapr.net/browse?q=cotp</a> (need an account to download files!)<br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '14, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-35103" class="comments-container"><span id="35119"></span><div id="comment-35119" class="comment"><div id="post-35119-score" class="comment-score"></div><div class="comment-text"><p>the pcaps from the 3rd link looks like what i want , i'll check them and reply , anyway thanks a lot :D , u gave me something to work with .</p></div><div id="comment-35119-info" class="comment-info"><span class="comment-age">(03 Aug '14, 11:30)</span> <span class="comment-user userinfo">saeedh</span></div></div><span id="35120"></span><div id="comment-35120" class="comment"><div id="post-35120-score" class="comment-score"></div><div class="comment-text"><p>good luck.</p></div><div id="comment-35120-info" class="comment-info"><span class="comment-age">(03 Aug '14, 11:53)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-35103" class="comment-tools"></div><div class="clear"></div><div id="comment-35103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

