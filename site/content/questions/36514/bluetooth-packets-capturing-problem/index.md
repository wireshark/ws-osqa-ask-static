+++
type = "question"
title = "Bluetooth packets capturing problem"
description = '''Hello All, I have an Android Phone here (4.4v). Both Bluetooth and WiFI is on. I am getting BT Address and WiFI Address. Now, I have connected the phone to my PC which is a able to recognize well. Now, I want all the packets flowing in and out through Bluetooth. How can I manage to get this in Wires...'''
date = "2014-09-22T23:38:00Z"
lastmod = "2014-10-20T06:08:00Z"
weight = 36514
keywords = [ "bt", "packets" ]
aliases = [ "/questions/36514" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bluetooth packets capturing problem](/questions/36514/bluetooth-packets-capturing-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36514-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36514-score" class="post-score" title="current number of votes">0</div><span id="post-36514-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>I have an Android Phone here (4.4v). Both Bluetooth and WiFI is on. I am getting BT Address and WiFI Address. Now, I have connected the phone to my PC which is a able to recognize well. Now, I want all the packets flowing in and out through Bluetooth. How can I manage to get this in Wireshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bt" rel="tag" title="see questions tagged &#39;bt&#39;">bt</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '14, 23:38</strong></p><img src="https://secure.gravatar.com/avatar/2ab817bd090d8c59f1ee584902066268?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bharath&#39;s gravatar image" /><p><span>bharath</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bharath has no accepted answers">0%</span></p></div></div><div id="comments-container-36514" class="comments-container"></div><div id="comment-tools-36514" class="comment-tools"></div><div class="clear"></div><div id="comment-36514-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37198"></span>

<div id="answer-container-37198" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37198-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37198-score" class="post-score" title="current number of votes">0</div><span id="post-37198-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the <a href="http://wiki.wireshark.org/CaptureSetup/Bluetooth">CaptureSetup/Bluetooth</a> page on the wiki for information. (Hopefully it's up to date.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '14, 06:08</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37198" class="comments-container"></div><div id="comment-tools-37198" class="comment-tools"></div><div class="clear"></div><div id="comment-37198-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

