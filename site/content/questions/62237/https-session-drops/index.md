+++
type = "question"
title = "HTTPS session drops"
description = '''I&#x27;d greatly appreciate any guidance on troubleshooting an issue with HTTPS sessions between Hyper-V hosts (hyper-v replication). I&#x27;m convinced network issues are the root cause, but I can&#x27;t put my finger on it. Capture at Gateway, LAN interface, client side'''
date = "2017-06-22T08:49:00Z"
lastmod = "2017-06-22T08:49:00Z"
weight = 62237
keywords = [ "analysis", "https" ]
aliases = [ "/questions/62237" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HTTPS session drops](/questions/62237/https-session-drops)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62237-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62237-score" class="post-score" title="current number of votes">0</div><span id="post-62237-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'd greatly appreciate any guidance on troubleshooting an issue with HTTPS sessions between Hyper-V hosts (hyper-v replication).</p><p>I'm convinced network issues are the root cause, but I can't put my finger on it.</p><p><a href="https://www.cloudshark.org/captures/f82f57724e4f">Capture at Gateway, LAN interface, client side</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jun '17, 08:49</strong></p><img src="https://secure.gravatar.com/avatar/1e2248ffe211b38de2fb9be2943c4308?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Velas&#39;s gravatar image" /><p><span>Velas</span><br />
<span class="score" title="2 reputation points">2</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Velas has no accepted answers">0%</span></p></div></div><div id="comments-container-62237" class="comments-container"></div><div id="comment-tools-62237" class="comment-tools"></div><div class="clear"></div><div id="comment-62237-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

