+++
type = "question"
title = "Wifi interface Trouble"
description = '''Guys .. i have some questions about the &quot;capture&quot; and &quot;interface&quot; part. First of all .. i can not see any kind of devices in the &quot;interface menu&quot; the only i see is my ethernet... (I use a stable pc without a wi-fi anteena... is that the reason ?) What i want to achieve is to detect my android phone ...'''
date = "2015-04-24T10:05:00Z"
lastmod = "2015-04-25T11:23:00Z"
weight = 41794
keywords = [ "interfaces", "wifidevices" ]
aliases = [ "/questions/41794" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wifi interface Trouble](/questions/41794/wifi-interface-trouble)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41794-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41794-score" class="post-score" title="current number of votes">0</div><span id="post-41794-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Guys .. i have some questions about the "capture" and "interface" part. First of all .. i can not see any kind of devices in the "interface menu" the only i see is my ethernet... (I use a stable pc without a wi-fi anteena... is that the reason ?) What i want to achieve is to detect my android phone or my laptop (...a wifi device) on my network and capture some packets ... how to find these devices ???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span> <span class="post-tag tag-link-wifidevices" rel="tag" title="see questions tagged &#39;wifidevices&#39;">wifidevices</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '15, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/e8dcb35d1f1aade2da7a6d3d0ed733ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aswepaok&#39;s gravatar image" /><p><span>aswepaok</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aswepaok has no accepted answers">0%</span></p></div></div><div id="comments-container-41794" class="comments-container"><span id="41806"></span><div id="comment-41806" class="comment"><div id="post-41806-score" class="comment-score"></div><div class="comment-text"><p>What OS is your laptop running?</p></div><div id="comment-41806-info" class="comment-info"><span class="comment-age">(24 Apr '15, 16:16)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="41832"></span><div id="comment-41832" class="comment"><div id="post-41832-score" class="comment-score"></div><div class="comment-text"><p>I dont use a laptop i use a stable PC ... is that matters ?</p></div><div id="comment-41832-info" class="comment-info"><span class="comment-age">(25 Apr '15, 10:15)</span> <span class="comment-user userinfo">aswepaok</span></div></div><span id="41833"></span><div id="comment-41833" class="comment"><div id="post-41833-score" class="comment-score"></div><div class="comment-text"><p>No, but my laptop is pretty stable when it's sitting on the floor or a table, so "stable PC" is not a very good way to say "not a laptop". "Desktop PC" would be better.</p><p>What do you mean by "i can not see any kind of devices in the "interface menu" the only i see is my ethernet"? Your Ethernet <em>is</em> a device. Do you mean that's the <em>only</em> device you see? Do you <em>have</em> any other devices? Does your desktop PC <em>have</em> a Wi-Fi adapter, either built-in (yes, there are desktop PCs with built-in Wi-Fi) or an add-on device (such as a USB adapter)?</p><p>Or do you mean "other computers" when you say "devices"?</p><p>What operating system is your desktop PC running?</p></div><div id="comment-41833-info" class="comment-info"><span class="comment-age">(25 Apr '15, 11:23)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-41794" class="comment-tools"></div><div class="clear"></div><div id="comment-41794-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

