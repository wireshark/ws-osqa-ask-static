+++
type = "question"
title = "WS 2 on Windows states Meta+ in Edit shortcuts"
description = '''Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0) Several of the options in the Edit menu show short cuts that include Meta+, but I&#x27;m one of the old-fashioned Windows guys - I never bought into that 1984 advert. I&#x27;ve checked on bugzilla and can&#x27;t find a registered bug. Is this a known problem? Shoul...'''
date = "2016-03-14T05:19:00Z"
lastmod = "2016-03-14T09:38:00Z"
weight = 50887
keywords = [ "qt" ]
aliases = [ "/questions/50887" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [WS 2 on Windows states Meta+ in Edit shortcuts](/questions/50887/ws-2-on-windows-states-meta-in-edit-shortcuts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50887-score" class="post-score" title="current number of votes">0</div><span id="post-50887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0)</p><p>Several of the options in the Edit menu show short cuts that include Meta+, but I'm one of the old-fashioned Windows guys - I never bought into that 1984 advert.</p><p>I've checked on bugzilla and can't find a registered bug. Is this a known problem? Should I register a bug?</p><p>Thanks and regards...Paul</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-qt" rel="tag" title="see questions tagged &#39;qt&#39;">qt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '16, 05:19</strong></p><img src="https://secure.gravatar.com/avatar/2e1b4057f2ff59fe059b23cc6571abaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PaulOfford&#39;s gravatar image" /><p><span>PaulOfford</span><br />
<span class="score" title="131 reputation points">131</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PaulOfford has 5 accepted answers">11%</span></p></div></div><div id="comments-container-50887" class="comments-container"></div><div id="comment-tools-50887" class="comment-tools"></div><div class="clear"></div><div id="comment-50887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50894"></span>

<div id="answer-container-50894" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50894-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50894-score" class="post-score" title="current number of votes">1</div><span id="post-50894-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="PaulOfford has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you try 2.0.2, I don't see any issue there?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Mar '16, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50894" class="comments-container"><span id="50902"></span><div id="comment-50902" class="comment"><div id="post-50902-score" class="comment-score"></div><div class="comment-text"><p>Hi Graham, Yes that fixed it. Thanks.</p></div><div id="comment-50902-info" class="comment-info"><span class="comment-age">(14 Mar '16, 09:38)</span> <span class="comment-user userinfo">PaulOfford</span></div></div></div><div id="comment-tools-50894" class="comment-tools"></div><div class="clear"></div><div id="comment-50894-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

