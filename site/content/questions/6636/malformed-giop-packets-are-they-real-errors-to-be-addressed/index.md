+++
type = "question"
title = "Malformed GIOP Packets. Are they real errors to be addressed"
description = '''I am seeing Malformed GIOP exceptions in my trace. Are they indications of a real problem or just the dissector reading it incorrectly? '''
date = "2011-09-29T09:01:00Z"
lastmod = "2011-09-29T12:44:00Z"
weight = 6636
keywords = [ "giop", "malformed" ]
aliases = [ "/questions/6636" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed GIOP Packets. Are they real errors to be addressed](/questions/6636/malformed-giop-packets-are-they-real-errors-to-be-addressed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6636-score" class="post-score" title="current number of votes">0</div><span id="post-6636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am seeing Malformed GIOP exceptions in my trace. Are they indications of a real problem or just the dissector reading it incorrectly?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-giop" rel="tag" title="see questions tagged &#39;giop&#39;">giop</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Sep '11, 09:01</strong></p><img src="https://secure.gravatar.com/avatar/bec6717ca85d488d9c5f657d85417a90?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LillyB2424&#39;s gravatar image" /><p><span>LillyB2424</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LillyB2424 has no accepted answers">0%</span></p></div></div><div id="comments-container-6636" class="comments-container"></div><div id="comment-tools-6636" class="comment-tools"></div><div class="clear"></div><div id="comment-6636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6643"></span>

<div id="answer-container-6643" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6643-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6643-score" class="post-score" title="current number of votes">0</div><span id="post-6643-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It could be a real network problem or it could be a bug in the GIOP dissector. We'd have to see the raw trace file in order to figure out which it is.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Sep '11, 12:44</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-6643" class="comments-container"></div><div id="comment-tools-6643" class="comment-tools"></div><div class="clear"></div><div id="comment-6643-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

