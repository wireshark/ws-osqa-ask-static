+++
type = "question"
title = "Bad Header Checksum"
description = '''Help !!! 3.4% of my traffic are Checksum Errors all eminating from a single IP address. How significant is this? What is checksum offload?  Header checksum: 0x0000 [incorrect, should be 0x7d7d (may be caused by &quot;IP checksum offload&quot;?)]'''
date = "2013-12-16T11:50:00Z"
lastmod = "2013-12-16T12:02:00Z"
weight = 28176
keywords = [ "badchecksum" ]
aliases = [ "/questions/28176" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bad Header Checksum](/questions/28176/bad-header-checksum)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28176-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28176-score" class="post-score" title="current number of votes">0</div><span id="post-28176-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Help !!! 3.4% of my traffic are Checksum Errors all eminating from a single IP address. How significant is this? What is checksum offload? Header checksum: 0x0000 [incorrect, should be 0x7d7d (may be caused by "IP checksum offload"?)]</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-badchecksum" rel="tag" title="see questions tagged &#39;badchecksum&#39;">badchecksum</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '13, 11:50</strong></p><img src="https://secure.gravatar.com/avatar/16c80ca493c77f3486cbb7ff38cc5d3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zoberist&#39;s gravatar image" /><p><span>Zoberist</span><br />
<span class="score" title="0 reputation points">0</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zoberist has no accepted answers">0%</span></p></div></div><div id="comments-container-28176" class="comments-container"></div><div id="comment-tools-28176" class="comment-tools"></div><div class="clear"></div><div id="comment-28176-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28177"></span>

<div id="answer-container-28177" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28177-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28177-score" class="post-score" title="current number of votes">0</div><span id="post-28177-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can most likely ignore it. Check the IP, it's your own. You are suffering the the famous TCP Checksum Offloading routine. See this <a href="http://ask.wireshark.org/questions/13577/header-checksum-error-incorrect-should-be">question</a>, or many others.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Dec '13, 12:02</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28177" class="comments-container"></div><div id="comment-tools-28177" class="comment-tools"></div><div class="clear"></div><div id="comment-28177-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

