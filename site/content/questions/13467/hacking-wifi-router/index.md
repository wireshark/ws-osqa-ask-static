+++
type = "question"
title = "Hacking wifi router"
description = '''Im just a tech guy wondering whether is it possible to get the wifi router password which is secured with wpa2-psk with wireshark or any such other software after capturing pcap file on windows?'''
date = "2012-08-08T05:54:00Z"
lastmod = "2012-08-08T07:06:00Z"
weight = 13467
keywords = [ "router", "hacking", "password", "wpa2" ]
aliases = [ "/questions/13467" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Hacking wifi router](/questions/13467/hacking-wifi-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13467-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13467-score" class="post-score" title="current number of votes">0</div><span id="post-13467-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im just a tech guy wondering whether is it possible to get the wifi router password which is secured with wpa2-psk with wireshark or any such other software after capturing pcap file on windows?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-hacking" rel="tag" title="see questions tagged &#39;hacking&#39;">hacking</span> <span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '12, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/84c32c7bf6963b289601055017fe3865?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rulerofthehell&#39;s gravatar image" /><p><span>rulerofthehell</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rulerofthehell has no accepted answers">0%</span></p></div></div><div id="comments-container-13467" class="comments-container"></div><div id="comment-tools-13467" class="comment-tools"></div><div class="clear"></div><div id="comment-13467-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13471"></span>

<div id="answer-container-13471" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13471-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13471-score" class="post-score" title="current number of votes">0</div><span id="post-13471-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you have the WPA2 key of the router AP you might be able to decode the password, if someone is accessing the router via telnet (or any other plain text protocol) over WIFI while you are capturing.</p><p>If you don't know any password of the router (including the WPA2 key) you're out of luck, and that's by design - because that's exactly what encryption is there for.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Aug '12, 07:06</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-13471" class="comments-container"></div><div id="comment-tools-13471" class="comment-tools"></div><div class="clear"></div><div id="comment-13471-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

