+++
type = "question"
title = "Windows: Wireshark source code folder"
description = '''Chapter 2.2 of the Wirehark Build Environment, https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html, suggests putting the source code (and other folders) in the Windows root folder, i.e., c:&#92;.  Adding folders and/or files to the root folder is an extraordinarily bad practice - can thes...'''
date = "2017-10-26T15:29:00Z"
lastmod = "2017-10-26T15:29:00Z"
weight = 64262
keywords = [ "windows", "environment", "developer" ]
aliases = [ "/questions/64262" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows: Wireshark source code folder](/questions/64262/windows-wireshark-source-code-folder)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64262-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64262-score" class="post-score" title="current number of votes">0</div><span id="post-64262-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Chapter 2.2 of the Wirehark Build Environment, <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html,">https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html,</a> suggests putting the source code (and other folders) in the Windows root folder, i.e., c:\.</p><p>Adding folders and/or files to the root folder is an extraordinarily bad practice - can these files be placed elsewhere, most likely in a folder which has spaces in its name, or will this cause the compile/installation process to fail?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-environment" rel="tag" title="see questions tagged &#39;environment&#39;">environment</span> <span class="post-tag tag-link-developer" rel="tag" title="see questions tagged &#39;developer&#39;">developer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '17, 15:29</strong></p><img src="https://secure.gravatar.com/avatar/efb7a1f078cc0924d400ddc530222272?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="groston&#39;s gravatar image" /><p><span>groston</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="groston has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Oct '17, 15:29</strong> </span></p></div></div><div id="comments-container-64262" class="comments-container"></div><div id="comment-tools-64262" class="comment-tools"></div><div class="clear"></div><div id="comment-64262-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

