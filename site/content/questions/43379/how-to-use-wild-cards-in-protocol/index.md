+++
type = "question"
title = "How to use wild cards in Protocol"
description = '''HI I want to know if i can search a string across all protocols Like * contains&quot; 12567&quot; Regards Vj'''
date = "2015-06-19T11:06:00Z"
lastmod = "2015-06-19T11:10:00Z"
weight = 43379
keywords = [ "wild", "card" ]
aliases = [ "/questions/43379" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to use wild cards in Protocol](/questions/43379/how-to-use-wild-cards-in-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43379-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43379-score" class="post-score" title="current number of votes">0</div><span id="post-43379-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI I want to know if i can search a string across all protocols</p><p>Like * contains" 12567" Regards Vj</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wild" rel="tag" title="see questions tagged &#39;wild&#39;">wild</span> <span class="post-tag tag-link-card" rel="tag" title="see questions tagged &#39;card&#39;">card</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '15, 11:06</strong></p><img src="https://secure.gravatar.com/avatar/f1c4ad8a11798f63ad06d58ac22b5630?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grvijay&#39;s gravatar image" /><p><span>grvijay</span><br />
<span class="score" title="36 reputation points">36</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grvijay has one accepted answer">100%</span></p></div></div><div id="comments-container-43379" class="comments-container"></div><div id="comment-tools-43379" class="comment-tools"></div><div class="clear"></div><div id="comment-43379-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43380"></span>

<div id="answer-container-43380" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43380-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43380-score" class="post-score" title="current number of votes">0</div><span id="post-43380-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="grvijay has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Every packet is dissected by the "Frame" dissector, so you can use <code>frame contains " 12567"</code>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jun '15, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-43380" class="comments-container"></div><div id="comment-tools-43380" class="comment-tools"></div><div class="clear"></div><div id="comment-43380-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

