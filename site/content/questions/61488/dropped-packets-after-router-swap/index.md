+++
type = "question"
title = "dropped packets after router swap"
description = '''We swapped a router recently at one of our locations and now our outlook to exchange clients are in a constant state of reconnection. We have multiple locations and this issue is very specific to this location. Since there were no other changes to our environment outside of this down router I ran wi...'''
date = "2017-05-18T08:22:00Z"
lastmod = "2017-05-18T13:14:00Z"
weight = 61488
keywords = [ "outlook", "wireshark", "exchange" ]
aliases = [ "/questions/61488" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [dropped packets after router swap](/questions/61488/dropped-packets-after-router-swap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61488-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61488-score" class="post-score" title="current number of votes">0</div><span id="post-61488-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We swapped a router recently at one of our locations and now our outlook to exchange clients are in a constant state of reconnection. We have multiple locations and this issue is very specific to this location.</p><p>Since there were no other changes to our environment outside of this down router I ran wireshark and just see large lumps of dropped packets and connection resets but I cannot decipher the actual issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outlook" rel="tag" title="see questions tagged &#39;outlook&#39;">outlook</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-exchange" rel="tag" title="see questions tagged &#39;exchange&#39;">exchange</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 May '17, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/98647f9a0889c5211480b2b03a728f02?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="greensystemsadmin&#39;s gravatar image" /><p><span>greensystems...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="greensystemsadmin has no accepted answers">0%</span></p></div></div><div id="comments-container-61488" class="comments-container"><span id="61491"></span><div id="comment-61491" class="comment"><div id="post-61491-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-61491-info" class="comment-info"><span class="comment-age">(18 May '17, 11:19)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61493"></span><div id="comment-61493" class="comment"><div id="post-61493-score" class="comment-score"></div><div class="comment-text"><p>Have you already checked the port stats at the layer2 and layer3 devices, to find out where the loss occur?</p></div><div id="comment-61493-info" class="comment-info"><span class="comment-age">(18 May '17, 13:14)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-61488" class="comment-tools"></div><div class="clear"></div><div id="comment-61488-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

