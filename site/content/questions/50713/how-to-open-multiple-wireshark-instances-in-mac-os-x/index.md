+++
type = "question"
title = "how to open multiple wireshark instances in Mac OS X"
description = '''I have version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0) installed on my 10.11.3 Mac, but cannot find a way to open multiple instances of wireshark, start multiple captures or open several traces. How do I do that?'''
date = "2016-03-04T00:11:00Z"
lastmod = "2016-07-21T08:59:00Z"
weight = 50713
keywords = [ "guide" ]
aliases = [ "/questions/50713" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to open multiple wireshark instances in Mac OS X](/questions/50713/how-to-open-multiple-wireshark-instances-in-mac-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50713-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50713-score" class="post-score" title="current number of votes">0</div><span id="post-50713-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0) installed on my 10.11.3 Mac, but cannot find a way to open multiple instances of wireshark, start multiple captures or open several traces. How do I do that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-guide" rel="tag" title="see questions tagged &#39;guide&#39;">guide</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '16, 00:11</strong></p><img src="https://secure.gravatar.com/avatar/2d1a8885858c8435654658b25f489bd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveZhou&#39;s gravatar image" /><p><span>SteveZhou</span><br />
<span class="score" title="191 reputation points">191</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveZhou has no accepted answers">0%</span></p></div></div><div id="comments-container-50713" class="comments-container"></div><div id="comment-tools-50713" class="comment-tools"></div><div class="clear"></div><div id="comment-50713-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54224"></span>

<div id="answer-container-54224" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54224-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54224-score" class="post-score" title="current number of votes">0</div><span id="post-54224-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://ask.wireshark.org/users/1/gerald-combs">Gerald Combs</a> provided the answer in <a href="https://ask.wireshark.org/questions/54036/open-multiple-captures-on-mac">another question</a>, but in a nutshell:</p><pre><code>open -n /Applications/Wireshark.app</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '16, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54224" class="comments-container"></div><div id="comment-tools-54224" class="comment-tools"></div><div class="clear"></div><div id="comment-54224-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

