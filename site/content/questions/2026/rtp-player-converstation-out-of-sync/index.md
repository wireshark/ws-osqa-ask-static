+++
type = "question"
title = "RTP Player, Converstation Out of Sync"
description = '''Hi I capture the RTP stream to my VoIP providor (SIP Trunk) I then uses the Wireshart RTP player, to playback converstations when users complain about poor quality.  But when I play the converstations in the RTP player, the converstations is out of sync. The playback plays both voices at the same ti...'''
date = "2011-01-31T00:58:00Z"
lastmod = "2011-02-01T03:25:00Z"
weight = 2026
keywords = [ "player", "rtp", "voip" ]
aliases = [ "/questions/2026" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTP Player, Converstation Out of Sync](/questions/2026/rtp-player-converstation-out-of-sync)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2026-score" class="post-score" title="current number of votes">0</div><span id="post-2026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I capture the RTP stream to my VoIP providor (SIP Trunk) I then uses the Wireshart RTP player, to playback converstations when users complain about poor quality.</p><p>But when I play the converstations in the RTP player, the converstations is out of sync. The playback plays both voices at the same time.</p><p>Is it possible to setup the RTP player to properly play the converstation with only one speaker at one time?</p><p>Regards, Steffen.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '11, 00:58</strong></p><img src="https://secure.gravatar.com/avatar/1cb0b942752a7033ae6c4c9508ff632d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Steffen%20Frederiksen&#39;s gravatar image" /><p><span>Steffen Fred...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Steffen Frederiksen has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Jan '11, 00:59</strong> </span></p></div></div><div id="comments-container-2026" class="comments-container"><span id="2028"></span><div id="comment-2028" class="comment"><div id="post-2028-score" class="comment-score"></div><div class="comment-text"><p>What Wireshark version are you using?</p></div><div id="comment-2028-info" class="comment-info"><span class="comment-age">(31 Jan '11, 01:25)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="2060"></span><div id="comment-2060" class="comment"><div id="post-2060-score" class="comment-score"></div><div class="comment-text"><p>1.4.3 32bit on windows 7 32bit</p></div><div id="comment-2060-info" class="comment-info"><span class="comment-age">(01 Feb '11, 02:20)</span> <span class="comment-user userinfo">Steffen Fred...</span></div></div></div><div id="comment-tools-2026" class="comment-tools"></div><div class="clear"></div><div id="comment-2026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2061"></span>

<div id="answer-container-2061" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2061-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2061-score" class="post-score" title="current number of votes">0</div><span id="post-2061-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Just untick one of the streams in the main RTP player window and it will only play the one stream left ticked.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '11, 03:25</strong></p><img src="https://secure.gravatar.com/avatar/030196d67dc4e2b8f4ecff65eefdb63e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KeithFrench&#39;s gravatar image" /><p><span>KeithFrench</span><br />
<span class="score" title="121 reputation points">121</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KeithFrench has no accepted answers">0%</span></p></div></div><div id="comments-container-2061" class="comments-container"></div><div id="comment-tools-2061" class="comment-tools"></div><div class="clear"></div><div id="comment-2061-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

