+++
type = "question"
title = "[closed] Homeplug AV Unkown Types"
description = '''Is there anyway I can add names to these fields in the types or modify the info column object of the homeplug av protocol by accessing these fields?  I want to know if it is possible to write a post dissector which only modifies the unknown types and other required fields by keeping all other fields...'''
date = "2017-10-06T12:33:00Z"
lastmod = "2017-10-06T12:33:00Z"
weight = 63716
keywords = [ "homeplug-av", "unkown" ]
aliases = [ "/questions/63716" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Homeplug AV Unkown Types](/questions/63716/homeplug-av-unkown-types)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63716-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63716-score" class="post-score" title="current number of votes">0</div><span id="post-63716-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there anyway I can add names to these fields in the types or modify the info column object of the homeplug av protocol by accessing these fields?</p><p>I want to know if it is possible to write a post dissector which only modifies the unknown types and other required fields by keeping all other fields of homeplug AV.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-homeplug-av" rel="tag" title="see questions tagged &#39;homeplug-av&#39;">homeplug-av</span> <span class="post-tag tag-link-unkown" rel="tag" title="see questions tagged &#39;unkown&#39;">unkown</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '17, 12:33</strong></p><img src="https://secure.gravatar.com/avatar/bf8a8cb9da533bbc7b744c1ba0003458?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="golthitatun&#39;s gravatar image" /><p><span>golthitatun</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="golthitatun has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>07 Oct '17, 00:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-63716" class="comments-container"></div><div id="comment-tools-63716" class="comment-tools"></div><div class="clear"></div><div id="comment-63716-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Jaap 07 Oct '17, 00:35

</div>

</div>

</div>

