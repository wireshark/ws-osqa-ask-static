+++
type = "question"
title = "right click is not working when trying to copy cookie"
description = '''after you click the each packet.  I can not use the right click to copy some cookie information or other. is it common for wireshark? or do I have to change some setting?'''
date = "2016-10-29T04:40:00Z"
lastmod = "2016-10-29T05:54:00Z"
weight = 56811
keywords = [ "copy-paste", "cookie", "wireshark" ]
aliases = [ "/questions/56811" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [right click is not working when trying to copy cookie](/questions/56811/right-click-is-not-working-when-trying-to-copy-cookie)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56811-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56811-score" class="post-score" title="current number of votes">0</div><span id="post-56811-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>after you click the each packet. I can not use the right click to copy some cookie information or other. is it common for wireshark? or do I have to change some setting?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-copy-paste" rel="tag" title="see questions tagged &#39;copy-paste&#39;">copy-paste</span> <span class="post-tag tag-link-cookie" rel="tag" title="see questions tagged &#39;cookie&#39;">cookie</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '16, 04:40</strong></p><img src="https://secure.gravatar.com/avatar/e48d369116c859a09cce9c10bd78f525?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xdeimos&#39;s gravatar image" /><p><span>xdeimos</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xdeimos has no accepted answers">0%</span></p></div></div><div id="comments-container-56811" class="comments-container"></div><div id="comment-tools-56811" class="comment-tools"></div><div class="clear"></div><div id="comment-56811-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56814"></span>

<div id="answer-container-56814" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56814-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56814-score" class="post-score" title="current number of votes">0</div><span id="post-56814-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you clicking in the packet list pane, i.e. the list with the column "No." at the left hand side?</p><p>If so, you can't copy values from there, instead, select the packet you're interested in, probably an http GET or POST, then in the packet details pane, expand the tree until you see the field you're interested in, then right click that node in the tree and select Copy -&gt; Value.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '16, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-56814" class="comments-container"></div><div id="comment-tools-56814" class="comment-tools"></div><div class="clear"></div><div id="comment-56814-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

