+++
type = "question"
title = "Favorite Question"
description = '''I noticed that there is a &quot;Favorite Question&quot; badge. So ... how do you favorite a question?'''
date = "2011-04-28T21:17:00Z"
lastmod = "2011-04-29T10:32:00Z"
weight = 3801
keywords = [ "question", "favorite" ]
aliases = [ "/questions/3801" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Favorite Question](/questions/3801/favorite-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3801-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3801-score" class="post-score" title="current number of votes">1</div><span id="post-3801-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">2</div></div></td><td><div id="item-right"><div class="question-body"><p>I noticed that there is a "Favorite Question" badge. So ... how do you favorite a question?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-question" rel="tag" title="see questions tagged &#39;question&#39;">question</span> <span class="post-tag tag-link-favorite" rel="tag" title="see questions tagged &#39;favorite&#39;">favorite</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '11, 21:17</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3801" class="comments-container"><span id="3814"></span><div id="comment-3814" class="comment"><div id="post-3814-score" class="comment-score"></div><div class="comment-text"><p>There you go!</p></div><div id="comment-3814-info" class="comment-info"><span class="comment-age">(29 Apr '11, 10:21)</span> <span class="comment-user userinfo">packethunter</span></div></div><span id="3815"></span><div id="comment-3815" class="comment"><div id="post-3815-score" class="comment-score"></div><div class="comment-text"><p>OK, but I was wondering how to do it so I could favorite someone else's question, not for the sake of earning a badge.</p></div><div id="comment-3815-info" class="comment-info"><span class="comment-age">(29 Apr '11, 10:32)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-3801" class="comment-tools"></div><div class="clear"></div><div id="comment-3801-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3802"></span>

<div id="answer-container-3802" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3802-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3802-score" class="post-score" title="current number of votes">4</div><span id="post-3802-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Click on the star button below the down-vote button.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '11, 22:23</strong></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworld has 28 accepted answers">28%</span></p></div></div><div id="comments-container-3802" class="comments-container"><span id="3810"></span><div id="comment-3810" class="comment"><div id="post-3810-score" class="comment-score"></div><div class="comment-text"><p>Ah. Never noticed it before. Thanks.</p></div><div id="comment-3810-info" class="comment-info"><span class="comment-age">(29 Apr '11, 05:23)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-3802" class="comment-tools"></div><div class="clear"></div><div id="comment-3802-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

