+++
type = "question"
title = "Not capturing anything"
description = '''Hi I&#x27;ve installed wireshark on my PC but when I am connected to the Internet and have Wireshark running a capture, it is not capturing anything. I have no idea what I&#x27;m doing wrong - any suggestions? Thanks in advance'''
date = "2012-04-14T22:47:00Z"
lastmod = "2012-04-15T00:44:00Z"
weight = 10151
keywords = [ "capture" ]
aliases = [ "/questions/10151" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Not capturing anything](/questions/10151/not-capturing-anything)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10151-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10151-score" class="post-score" title="current number of votes">0</div><span id="post-10151-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I've installed wireshark on my PC but when I am connected to the Internet and have Wireshark running a capture, it is not capturing anything.</p><p>I have no idea what I'm doing wrong - any suggestions?</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Apr '12, 22:47</strong></p><img src="https://secure.gravatar.com/avatar/b33221c83f0b693d1703d7ce2f555bf6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jasial&#39;s gravatar image" /><p><span>jasial</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jasial has no accepted answers">0%</span></p></div></div><div id="comments-container-10151" class="comments-container"></div><div id="comment-tools-10151" class="comment-tools"></div><div class="clear"></div><div id="comment-10151-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10159"></span>

<div id="answer-container-10159" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10159-score" class="post-score" title="current number of votes">0</div><span id="post-10159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you make sure you are capturing on the right interface, you may be capturing on the PPP interface instead of the Ethernet interface. Click on Capture interfaces and select the interface where the packet counters increase when you browse the Internet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '12, 00:44</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-10159" class="comments-container"></div><div id="comment-tools-10159" class="comment-tools"></div><div class="clear"></div><div id="comment-10159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

