+++
type = "question"
title = "Wireshark slow performance in Citrix"
description = '''Has anybody ever run wireshark in Unix via Citrix displayback? After we upgraded the wireshark from V0.99.8 to V1.2.6 Wireshark displayed really slow and became unusable in Citrix. Does anybody know which Citrix version is compatible with wireshark V1.2.6+ version (built upon GTK2)? Or any possible ...'''
date = "2011-10-05T15:31:00Z"
lastmod = "2011-10-05T15:31:00Z"
weight = 6744
keywords = [ "performance", "gtk", "citrix" ]
aliases = [ "/questions/6744" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark slow performance in Citrix](/questions/6744/wireshark-slow-performance-in-citrix)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6744-score" class="post-score" title="current number of votes">0</div><span id="post-6744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anybody ever run wireshark in Unix via Citrix displayback? After we upgraded the wireshark from V0.99.8 to V1.2.6 Wireshark displayed really slow and became unusable in Citrix. Does anybody know which Citrix version is compatible with wireshark V1.2.6+ version (built upon GTK2)? Or any possible workaround for this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span> <span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span> <span class="post-tag tag-link-citrix" rel="tag" title="see questions tagged &#39;citrix&#39;">citrix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '11, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/3148db2a3ca8f909be97f9891fab6966?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dxl&#39;s gravatar image" /><p><span>dxl</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dxl has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Oct '11, 22:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-6744" class="comments-container"></div><div id="comment-tools-6744" class="comment-tools"></div><div class="clear"></div><div id="comment-6744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

