+++
type = "question"
title = "how does an emi differ from rfi?"
description = '''how does an emi differ from rfi filter? how much reliable they are?'''
date = "2016-06-09T06:05:00Z"
lastmod = "2016-06-10T12:16:00Z"
weight = 53330
keywords = [ "filter", "rfi", "emi" ]
aliases = [ "/questions/53330" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how does an emi differ from rfi?](/questions/53330/how-does-an-emi-differ-from-rfi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53330-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53330-score" class="post-score" title="current number of votes">0</div><span id="post-53330-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how does an emi differ from rfi filter? how much reliable they are?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-rfi" rel="tag" title="see questions tagged &#39;rfi&#39;">rfi</span> <span class="post-tag tag-link-emi" rel="tag" title="see questions tagged &#39;emi&#39;">emi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '16, 06:05</strong></p><img src="https://secure.gravatar.com/avatar/57a64b90f64b19849554e2cb6f95991e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="blaetech&#39;s gravatar image" /><p><span>blaetech</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="blaetech has no accepted answers">0%</span></p></div></div><div id="comments-container-53330" class="comments-container"><span id="53333"></span><div id="comment-53333" class="comment"><div id="post-53333-score" class="comment-score"></div><div class="comment-text"><p>You have to provide more context for this question. How does this relate to Wireshark?</p></div><div id="comment-53333-info" class="comment-info"><span class="comment-age">(09 Jun '16, 06:20)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="53350"></span><div id="comment-53350" class="comment"><div id="post-53350-score" class="comment-score"></div><div class="comment-text"><p>Probably best asked on <a href="http://electronics.stackexchange.com">Electrical Engineering Stack Exchange</a>, as it's not a particularly protocol-related question at the level at which Wireshark works (<em>maybe</em> relevant to protocols at the physical layer but that's about it).</p></div><div id="comment-53350-info" class="comment-info"><span class="comment-age">(10 Jun '16, 12:16)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-53330" class="comment-tools"></div><div class="clear"></div><div id="comment-53330-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

