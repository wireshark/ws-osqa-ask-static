+++
type = "question"
title = "Call Flow Graph incomplete"
description = '''Hi, I&#x27;m running 1.99.7 on OS X 10.10.4 and the VoIP Calls Flow Sequence only shows part of the call flow, specifically just the port numbers. i.e. no lines, no IP addresses, no division of hosts. Any ideas? Cheers, Craig'''
date = "2015-07-03T04:22:00Z"
lastmod = "2015-07-03T10:45:00Z"
weight = 43845
keywords = [ "voipcalls" ]
aliases = [ "/questions/43845" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Call Flow Graph incomplete](/questions/43845/call-flow-graph-incomplete)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43845-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43845-score" class="post-score" title="current number of votes">0</div><span id="post-43845-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm running 1.99.7 on OS X 10.10.4 and the VoIP Calls Flow Sequence only shows part of the call flow, specifically just the port numbers. i.e. no lines, no IP addresses, no division of hosts. Any ideas?</p><p>Cheers,</p><p>Craig</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '15, 04:22</strong></p><img src="https://secure.gravatar.com/avatar/efbf07dd50ba947e761705cf9b3de2a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nevermore&#39;s gravatar image" /><p><span>nevermore</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nevermore has no accepted answers">0%</span></p></div></div><div id="comments-container-43845" class="comments-container"></div><div id="comment-tools-43845" class="comment-tools"></div><div class="clear"></div><div id="comment-43845-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43853"></span>

<div id="answer-container-43853" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43853-score" class="post-score" title="current number of votes">0</div><span id="post-43853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's probably a bug in the Qt-based version of Wireshark. Please <a href="https://bugs.wireshark.org/bugzilla/">submit a bug</a>, along with an example capture file showing the problem. I've heard someone else had the same issue, but I've not been able to reproduce it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jul '15, 10:45</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div></div><div id="comments-container-43853" class="comments-container"></div><div id="comment-tools-43853" class="comment-tools"></div><div class="clear"></div><div id="comment-43853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

