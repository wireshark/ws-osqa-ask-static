+++
type = "question"
title = "Packet loss analysis through Wireshark ?"
description = '''I have a scenario where there is a slowness in accessing an applicationfrom user&#x27;s system after 7pm. So I just wanted to know what is happening that making the access to the application slow at that time. Is there any way that we can do it through wireshark'''
date = "2016-11-10T06:41:00Z"
lastmod = "2016-11-15T09:19:00Z"
weight = 57269
keywords = [ "2.2.1", "wireshark" ]
aliases = [ "/questions/57269" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet loss analysis through Wireshark ?](/questions/57269/packet-loss-analysis-through-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57269-score" class="post-score" title="current number of votes">0</div><span id="post-57269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a scenario where there is a slowness in accessing an applicationfrom user's system after 7pm. So I just wanted to know what is happening that making the access to the application slow at that time. Is there any way that we can do it through wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-2.2.1" rel="tag" title="see questions tagged &#39;2.2.1&#39;">2.2.1</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '16, 06:41</strong></p><img src="https://secure.gravatar.com/avatar/7ec71c5d609163ae8d5ba8028ff71b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sameetha&#39;s gravatar image" /><p><span>sameetha</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sameetha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>10 Nov '16, 06:42</strong> </span></p></div></div><div id="comments-container-57269" class="comments-container"></div><div id="comment-tools-57269" class="comment-tools"></div><div class="clear"></div><div id="comment-57269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57306"></span>

<div id="answer-container-57306" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57306-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57306-score" class="post-score" title="current number of votes">0</div><span id="post-57306-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, Wireshark can help you analyze this. Create a list of actions to perform in the application. Then during a time when performance is good perform these actions while making a trace with Wireshark. Now do the same after 7PM (performing exactly the same actions in the application).</p><p>Now you can look a the traffic in both traces and compare.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '16, 03:53</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-57306" class="comments-container"><span id="57393"></span><div id="comment-57393" class="comment"><div id="post-57393-score" class="comment-score"></div><div class="comment-text"><p>Thank you.</p></div><div id="comment-57393-info" class="comment-info"><span class="comment-age">(15 Nov '16, 09:19)</span> <span class="comment-user userinfo">sameetha</span></div></div></div><div id="comment-tools-57306" class="comment-tools"></div><div class="clear"></div><div id="comment-57306-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

