+++
type = "question"
title = "Telephony --&gt; VoIP Call Flow Sequence renders poorly on newer versions"
description = '''Windows 10, 64 bit Version 2.0.3 (v2.0.3-0-geed34f0 from master-2.0) When I do a trace on a VoIP Call, select Telephony and then select the trace and select Flow Sequence I get The following: Time and port (about 1.5&quot; Wide) Packet name and direction (about 1&quot; Wide) Port (the rest of the screen excep...'''
date = "2016-04-25T16:36:00Z"
lastmod = "2016-04-25T18:53:00Z"
weight = 51942
keywords = [ "telephony", "call", "flow", "voip" ]
aliases = [ "/questions/51942" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Telephony --&gt; VoIP Call Flow Sequence renders poorly on newer versions](/questions/51942/telephony-voip-call-flow-sequence-renders-poorly-on-newer-versions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51942-score" class="post-score" title="current number of votes">0</div><span id="post-51942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Windows 10, 64 bit Version 2.0.3 (v2.0.3-0-geed34f0 from master-2.0) When I do a trace on a VoIP Call, select Telephony and then select the trace and select Flow Sequence I get The following: Time and port (about 1.5" Wide) Packet name and direction (about 1" Wide) Port (the rest of the screen except 2" on the right) Actual description of the packet (that most of the time does not fit in the 2") I see no way to adjust the columns sizes and resizing the window does not change anything. Older versions I was able to real everything, not any more. Most lines end in a ... not very useful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telephony" rel="tag" title="see questions tagged &#39;telephony&#39;">telephony</span> <span class="post-tag tag-link-call" rel="tag" title="see questions tagged &#39;call&#39;">call</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '16, 16:36</strong></p><img src="https://secure.gravatar.com/avatar/56b4ec330b16d23f26ed2a10926c18ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tim%20Laren&#39;s gravatar image" /><p><span>Tim Laren</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tim Laren has no accepted answers">0%</span></p></div></div><div id="comments-container-51942" class="comments-container"><span id="51943"></span><div id="comment-51943" class="comment"><div id="post-51943-score" class="comment-score"></div><div class="comment-text"><p>"Older" as in "2.0, 2.0.1, and 2.0.2", or "older" as in "1.x"? If it's the latter, this is probably a problem with the new Qt version.</p></div><div id="comment-51943-info" class="comment-info"><span class="comment-age">(25 Apr '16, 18:53)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-51942" class="comment-tools"></div><div class="clear"></div><div id="comment-51942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

