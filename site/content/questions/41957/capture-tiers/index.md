+++
type = "question"
title = "capture tiers"
description = '''can wireshark identify devices in between?  Ex. I want to analyze the traffice from source=machine_A (PC) to destination=machine_B (server), why is it i cannot see the ip addresses of machines (e.g. FW, routers, etc.) in between Machine A &amp;amp; B? Wireshark is installed in source machine_A and initi...'''
date = "2015-04-29T20:17:00Z"
lastmod = "2015-04-30T02:36:00Z"
weight = 41957
keywords = [ "datatraffice" ]
aliases = [ "/questions/41957" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capture tiers](/questions/41957/capture-tiers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41957-score" class="post-score" title="current number of votes">0</div><span id="post-41957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can wireshark identify devices in between? Ex. I want to analyze the traffice from source=machine_A (PC) to destination=machine_B (server), why is it i cannot see the ip addresses of machines (e.g. FW, routers, etc.) in between Machine A &amp; B? Wireshark is installed in source machine_A and initiated the data capture.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-datatraffice" rel="tag" title="see questions tagged &#39;datatraffice&#39;">datatraffice</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Apr '15, 20:17</strong></p><img src="https://secure.gravatar.com/avatar/0b2eb33899f714343d611d24b7387e8c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="revilo2014&#39;s gravatar image" /><p><span>revilo2014</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="revilo2014 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Apr '15, 20:19</strong> </span></p></div></div><div id="comments-container-41957" class="comments-container"></div><div id="comment-tools-41957" class="comment-tools"></div><div class="clear"></div><div id="comment-41957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41963"></span>

<div id="answer-container-41963" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41963-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41963-score" class="post-score" title="current number of votes">1</div><span id="post-41963-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can only see the MAC address of the next hop device.</p><p>You can also identify how many layer 3 devices are in between by looking at the TTL.</p><p>To understand why you can't see the IPs of the devices that are in between, please search on youtube for "network fundamentals".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Apr '15, 02:36</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Apr '15, 02:37</strong> </span></p></div></div><div id="comments-container-41963" class="comments-container"></div><div id="comment-tools-41963" class="comment-tools"></div><div class="clear"></div><div id="comment-41963-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

