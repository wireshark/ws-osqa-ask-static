+++
type = "question"
title = "TCP Export"
description = '''How do I get an export of all (complete) TCP streams from a trace? (ie. TCP flow trace as bulk operation)'''
date = "2014-04-16T04:57:00Z"
lastmod = "2014-04-17T03:58:00Z"
weight = 31877
keywords = [ "export", "followtcpstream" ]
aliases = [ "/questions/31877" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TCP Export](/questions/31877/tcp-export)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31877-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31877-score" class="post-score" title="current number of votes">0</div><span id="post-31877-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I get an export of all (complete) TCP streams from a trace?</p><p>(ie. TCP flow trace as bulk operation)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-followtcpstream" rel="tag" title="see questions tagged &#39;followtcpstream&#39;">followtcpstream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '14, 04:57</strong></p><img src="https://secure.gravatar.com/avatar/6efb7f7a58f040077885d1fcae624151?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Keynan&#39;s gravatar image" /><p><span>Keynan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Keynan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Apr '14, 03:52</strong> </span></p></div></div><div id="comments-container-31877" class="comments-container"></div><div id="comment-tools-31877" class="comment-tools"></div><div class="clear"></div><div id="comment-31877-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31922"></span>

<div id="answer-container-31922" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31922-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31922-score" class="post-score" title="current number of votes">0</div><span id="post-31922-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://github.com/simsong/tcpflow">tcpflow</a> although that doesn't output pcap files, or look at <a href="http://ask.wireshark.org/questions/16690/split-pcap-file-into-smaller-pcap-file-according-to-tcp-flow">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '14, 03:58</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-31922" class="comments-container"></div><div id="comment-tools-31922" class="comment-tools"></div><div class="clear"></div><div id="comment-31922-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

