+++
type = "question"
title = "IDE with code completion - to write wireshark plugins"
description = '''Hi at all, i want to create some wireshark plugins with lua. Up to now i have written a dissector and have reimplemented some examples from the wiki - successfully. I used &quot;ZeroBrane Studio as IDE for development&quot;. It&#x27;s ok, but there is no code completion with possible values or functions... My ques...'''
date = "2016-05-10T08:27:00Z"
lastmod = "2016-05-10T11:11:00Z"
weight = 52395
keywords = [ "lua" ]
aliases = [ "/questions/52395" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IDE with code completion - to write wireshark plugins](/questions/52395/ide-with-code-completion-to-write-wireshark-plugins)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52395-score" class="post-score" title="current number of votes">0</div><span id="post-52395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi at all,</p><p>i want to create some wireshark plugins with lua. Up to now i have written a dissector and have reimplemented some examples from the wiki - successfully. I used "ZeroBrane Studio as IDE for development". It's ok, but there is no code completion with possible values or functions...</p><p><strong>My question:</strong> - Is there any IDE to develop lua plugins for wireshark with <strong>code completion</strong> ? If yes, which do you use ?</p><p>I'm not sure, because lua is a script language.</p><p>Hope for some answers. Thanks at all.</p><p>Wayne</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '16, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/3b9f83bc20f401eb918a06670d29854d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wayne&#39;s gravatar image" /><p><span>wayne</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wayne has no accepted answers">0%</span></p></div></div><div id="comments-container-52395" class="comments-container"></div><div id="comment-tools-52395" class="comment-tools"></div><div class="clear"></div><div id="comment-52395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52399"></span>

<div id="answer-container-52399" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52399-score" class="post-score" title="current number of votes">0</div><span id="post-52399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I haven't used it (I generally don't use IDEs) but I know that <a href="https://eclipse.org/">Eclipse</a> has a <a href="https://eclipse.org/ldt/">Lua plugin</a> that supports most of what you'd expect but, of course, it won't know about/support auto-completion for Wireshark's Lua APIs. (It's not clear to me, from your question, whether you're specifically looking for auto-completion for Wireshark's APIs or Lua in general.)</p><p>It's probably possible to extend that plugin to support Wireshark's APIs but I don't know how hard it would be.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '16, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-52399" class="comments-container"></div><div id="comment-tools-52399" class="comment-tools"></div><div class="clear"></div><div id="comment-52399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

