+++
type = "question"
title = "Who is monitoring my workstation at work"
description = '''What packets can I capture to see if my employer is using some tool to monitor my workstation at work while I am working, and can see my screen when I am not aware of it?'''
date = "2016-04-26T06:33:00Z"
lastmod = "2016-04-28T02:35:00Z"
weight = 51961
keywords = [ "workstation", "monitoring" ]
aliases = [ "/questions/51961" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Who is monitoring my workstation at work](/questions/51961/who-is-monitoring-my-workstation-at-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51961-score" class="post-score" title="current number of votes">0</div><span id="post-51961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What packets can I capture to see if my employer is using some tool to monitor my workstation at work while I am working, and can see my screen when I am not aware of it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-workstation" rel="tag" title="see questions tagged &#39;workstation&#39;">workstation</span> <span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '16, 06:33</strong></p><img src="https://secure.gravatar.com/avatar/f3e2558198392c5bce74f21f17170088?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RoadWarrior&#39;s gravatar image" /><p><span>RoadWarrior</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RoadWarrior has no accepted answers">0%</span></p></div></div><div id="comments-container-51961" class="comments-container"></div><div id="comment-tools-51961" class="comment-tools"></div><div class="clear"></div><div id="comment-51961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="51965"></span>

<div id="answer-container-51965" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51965-score" class="post-score" title="current number of votes">0</div><span id="post-51965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are many tools out there which are targeted to that need, and I assume proprietary in nature. Therefore it may no be easy to find. What could be an indication is a constants trickle or stream of packets to a certain destination, with little or no return traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '16, 07:45</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-51965" class="comments-container"></div><div id="comment-tools-51965" class="comment-tools"></div><div class="clear"></div><div id="comment-51965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="52038"></span>

<div id="answer-container-52038" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52038-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52038-score" class="post-score" title="current number of votes">0</div><span id="post-52038-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi RoadWarrior, could be an idea starting to take a capture on your PC setting a capture filter with a specific lan (employee) ?</p><p>Once done you should look for protocols used from this lan with your IP as destination.</p><p>Could it be useful ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '16, 02:35</strong></p><img src="https://secure.gravatar.com/avatar/bba638c3a54975c52c98530defa199af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ValerioItaly&#39;s gravatar image" /><p><span>ValerioItaly</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ValerioItaly has no accepted answers">0%</span></p></div></div><div id="comments-container-52038" class="comments-container"></div><div id="comment-tools-52038" class="comment-tools"></div><div class="clear"></div><div id="comment-52038-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

