+++
type = "question"
title = "Autoscroll Jumping"
description = '''Hello All, I am on Wireshark Version 2.2.0 (v2.2.0-0-g5368c50 from master-2.2) and while capturing live packets I have the auto scroll feature turned on. While it is capturing it jumps from the last packet all the way back to the first packet.  Now if I stop auto scroll and press the (go to last pac...'''
date = "2016-10-03T05:49:00Z"
lastmod = "2017-05-22T01:43:00Z"
weight = 56089
keywords = [ "autoscroll", "version2.2.0" ]
aliases = [ "/questions/56089" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Autoscroll Jumping](/questions/56089/autoscroll-jumping)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56089-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56089-score" class="post-score" title="current number of votes">0</div><span id="post-56089-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>I am on Wireshark Version 2.2.0 (v2.2.0-0-g5368c50 from master-2.2) and while capturing live packets I have the auto scroll feature turned on. While it is capturing it jumps from the last packet all the way back to the first packet.</p><p>Now if I stop auto scroll and press the (go to last packet button) next to auto scroll say it is at packet 600 and I turn on auto scroll again, it will then jump from the last packet it captures to packet 600 instead of the first packet.</p><p>Any one else notice this problem?</p><p>Thank you, Keybaord</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-autoscroll" rel="tag" title="see questions tagged &#39;autoscroll&#39;">autoscroll</span> <span class="post-tag tag-link-version2.2.0" rel="tag" title="see questions tagged &#39;version2.2.0&#39;">version2.2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '16, 05:49</strong></p><img src="https://secure.gravatar.com/avatar/7af69ca2debd99163d9c2112f10e60ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="keyboard&#39;s gravatar image" /><p><span>keyboard</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="keyboard has no accepted answers">0%</span></p></div></div><div id="comments-container-56089" class="comments-container"><span id="57537"></span><div id="comment-57537" class="comment"><div id="post-57537-score" class="comment-score"></div><div class="comment-text"><p>I'm having this problem as well. It has existed ever since v.2.0. It will not stay on the newest packet but instead jumps back and forth between the newest and some other ones further up the list. Very annoying.</p></div><div id="comment-57537-info" class="comment-info"><span class="comment-age">(21 Nov '16, 12:43)</span> <span class="comment-user userinfo">tyson3720</span></div></div></div><div id="comment-tools-56089" class="comment-tools"></div><div class="clear"></div><div id="comment-56089-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61534"></span>

<div id="answer-container-61534" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61534-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61534-score" class="post-score" title="current number of votes">0</div><span id="post-61534-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I also have this problem (Wireshark version 2.2.6). I have a workaround (albeit impractical): select a packet, then press and hold "end" on your keyboard.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '17, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/7c09e66819479ef9b4405f093fbabdb6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="luhunoxifa&#39;s gravatar image" /><p><span>luhunoxifa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="luhunoxifa has no accepted answers">0%</span></p></div></div><div id="comments-container-61534" class="comments-container"></div><div id="comment-tools-61534" class="comment-tools"></div><div class="clear"></div><div id="comment-61534-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

