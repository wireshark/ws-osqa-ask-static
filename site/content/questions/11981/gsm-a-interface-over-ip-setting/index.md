+++
type = "question"
title = "GSM A-Interface over IP setting ?"
description = '''Hi, Could someone help/show me the setting so I can see the GSM A-interface over IP message? Basically, I do have pcap trace captured BUT I could not tell what is the message because.... -Info Column shows &quot;SACK Unassigned: treated as an unknown Message Type&quot; -Protocol Colunm shows &quot;BSSAP+&quot; Therefor...'''
date = "2012-06-15T15:36:00Z"
lastmod = "2015-03-16T18:28:00Z"
weight = 11981
keywords = [ "aoip" ]
aliases = [ "/questions/11981" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [GSM A-Interface over IP setting ?](/questions/11981/gsm-a-interface-over-ip-setting)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11981-score" class="post-score" title="current number of votes">0</div><span id="post-11981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Could someone help/show me the setting so I can see the GSM A-interface over IP message? Basically, I do have pcap trace captured BUT I could not tell what is the message because.... -Info Column shows "SACK Unassigned: treated as an unknown Message Type" -Protocol Colunm shows "BSSAP+" Therefore, wireshark does not decode whatever after/under BSAP/BSSAP.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-aoip" rel="tag" title="see questions tagged &#39;aoip&#39;">aoip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jun '12, 15:36</strong></p><img src="https://secure.gravatar.com/avatar/0baf147128885cae12661f0172be8ce5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="qvuong&#39;s gravatar image" /><p><span>qvuong</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="qvuong has no accepted answers">0%</span></p></div></div><div id="comments-container-11981" class="comments-container"><span id="40602"></span><div id="comment-40602" class="comment"><div id="post-40602-score" class="comment-score"></div><div class="comment-text"><p>Hi qvuong, Kindly share these pcaps with me, my mail id is <span class="__cf_email__" data-cfemail="e496858e8590ca978c918f8885d1d7d6d5a48389858d88ca878b89">[email protected]</span> . I need these pcaps. Thanks :)</p></div><div id="comment-40602-info" class="comment-info"><span class="comment-age">(15 Mar '15, 23:34)</span> <span class="comment-user userinfo">Rajat12345</span></div></div><span id="40624"></span><div id="comment-40624" class="comment"><div id="post-40624-score" class="comment-score"></div><div class="comment-text"><p>If it isn't confidential data, you can upload here and then post the URL on the forum: <a href="https://appliance.cloudshark.org/upload/">https://appliance.cloudshark.org/upload/</a></p></div><div id="comment-40624-info" class="comment-info"><span class="comment-age">(16 Mar '15, 18:28)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-11981" class="comment-tools"></div><div class="clear"></div><div id="comment-11981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

