+++
type = "question"
title = "Does Wireshark support Wi-Fi Neighbor Awareness"
description = '''Does Wireshark support Wi-Fi Neighbor Awareness thanks'''
date = "2017-01-18T21:38:00Z"
lastmod = "2017-01-20T07:58:00Z"
weight = 58877
keywords = [ "awareness" ]
aliases = [ "/questions/58877" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does Wireshark support Wi-Fi Neighbor Awareness](/questions/58877/does-wireshark-support-wi-fi-neighbor-awareness)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58877-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58877-score" class="post-score" title="current number of votes">0</div><span id="post-58877-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does Wireshark support Wi-Fi Neighbor Awareness thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-awareness" rel="tag" title="see questions tagged &#39;awareness&#39;">awareness</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '17, 21:38</strong></p><img src="https://secure.gravatar.com/avatar/ff1513881ba5db305233522f6e459f5e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wsmswsms&#39;s gravatar image" /><p><span>wsmswsms</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wsmswsms has no accepted answers">0%</span></p></div></div><div id="comments-container-58877" class="comments-container"></div><div id="comment-tools-58877" class="comment-tools"></div><div class="clear"></div><div id="comment-58877-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58920"></span>

<div id="answer-container-58920" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58920-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58920-score" class="post-score" title="current number of votes">1</div><span id="post-58920-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To my knowledge, no, Wireshark does not yet support dissection of Wi-Fi <a href="https://www.wi-fi.org/file/neighbor-awareness-networking-technical-specification-v10-0">Neighbor Awareness Networking</a> (NAN) traffic. You can search the Wireshark source for yourself <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=epan/dissectors/packet-ieee80211.c;h=8116cba90e52106aefd70b8a946e7fbbfaa08273;hb=HEAD">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '17, 07:58</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-58920" class="comments-container"></div><div id="comment-tools-58920" class="comment-tools"></div><div class="clear"></div><div id="comment-58920-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

