+++
type = "question"
title = "Calculating Application Block Size"
description = '''using Wireshark how can we calculate Application Block size. Basically I am try to measure throughput for Particular Application like( CIFS ) so trying to understand how we can calculate Application Block Size. Thanks for help.'''
date = "2013-07-26T03:29:00Z"
lastmod = "2013-07-30T00:58:00Z"
weight = 23385
keywords = [ "application" ]
aliases = [ "/questions/23385" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Calculating Application Block Size](/questions/23385/calculating-application-block-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23385-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23385-score" class="post-score" title="current number of votes">0</div><span id="post-23385-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>using Wireshark how can we calculate Application Block size.</p><p>Basically I am try to measure throughput for Particular Application like( CIFS ) so trying to understand how we can calculate Application Block Size.</p><p>Thanks for help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-application" rel="tag" title="see questions tagged &#39;application&#39;">application</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '13, 03:29</strong></p><img src="https://secure.gravatar.com/avatar/6615a61d69b703d89076bb0f18342bbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m_1607&#39;s gravatar image" /><p><span>m_1607</span><br />
<span class="score" title="35 reputation points">35</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m_1607 has no accepted answers">0%</span></p></div></div><div id="comments-container-23385" class="comments-container"><span id="23386"></span><div id="comment-23386" class="comment"><div id="post-23386-score" class="comment-score"></div><div class="comment-text"><p>Can you please add some information regarding 'Application Block Size'. What do you mean by that in the context of what protocol?</p></div><div id="comment-23386-info" class="comment-info"><span class="comment-age">(26 Jul '13, 04:06)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="23441"></span><div id="comment-23441" class="comment"><div id="post-23441-score" class="comment-score"></div><div class="comment-text"><p>Ok Lets say I have Https Application server setup , and i want to measure how much data that server can provide to client , how could we calculate that.</p></div><div id="comment-23441-info" class="comment-info"><span class="comment-age">(30 Jul '13, 00:58)</span> <span class="comment-user userinfo">m_1607</span></div></div></div><div id="comment-tools-23385" class="comment-tools"></div><div class="clear"></div><div id="comment-23385-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

