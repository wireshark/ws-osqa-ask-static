+++
type = "question"
title = "Will the wireshark be extend to a monitor system"
description = '''Will the wireshark be extend to a monitor system'''
date = "2010-09-22T19:32:00Z"
lastmod = "2010-12-14T22:10:00Z"
weight = 282
keywords = [ "system" ]
aliases = [ "/questions/282" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Will the wireshark be extend to a monitor system](/questions/282/will-the-wireshark-be-extend-to-a-monitor-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-282-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-282-score" class="post-score" title="current number of votes">0</div><span id="post-282-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Will the wireshark be extend to a monitor system</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-system" rel="tag" title="see questions tagged &#39;system&#39;">system</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '10, 19:32</strong></p><img src="https://secure.gravatar.com/avatar/38f71b96e46009028d0049d2b50b52f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jianch&#39;s gravatar image" /><p><span>jianch</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jianch has no accepted answers">0%</span></p></div></div><div id="comments-container-282" class="comments-container"><span id="285"></span><div id="comment-285" class="comment"><div id="post-285-score" class="comment-score"></div><div class="comment-text"><p>Please define what you mean by monitor system</p></div><div id="comment-285-info" class="comment-info"><span class="comment-age">(22 Sep '10, 22:50)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="286"></span><div id="comment-286" class="comment"><div id="post-286-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "monitor system", features gets added by people contributing them; no formal plans to add features exists.</p></div><div id="comment-286-info" class="comment-info"><span class="comment-age">(23 Sep '10, 00:12)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-282" class="comment-tools"></div><div class="clear"></div><div id="comment-282-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1353"></span>

<div id="answer-container-1353" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1353-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1353-score" class="post-score" title="current number of votes">0</div><span id="post-1353-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>i think, a monitor system should have more capacity and high performance to high bandwidth traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Dec '10, 22:10</strong></p><img src="https://secure.gravatar.com/avatar/38f71b96e46009028d0049d2b50b52f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jianch&#39;s gravatar image" /><p><span>jianch</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jianch has no accepted answers">0%</span></p></div></div><div id="comments-container-1353" class="comments-container"></div><div id="comment-tools-1353" class="comment-tools"></div><div class="clear"></div><div id="comment-1353-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

