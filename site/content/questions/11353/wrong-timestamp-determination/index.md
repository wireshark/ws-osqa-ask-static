+++
type = "question"
title = "Wrong TimeStamp Determination"
description = '''what criteria is WS using to determine a &quot;wrong timestamp&quot; in an rtp playback? We see &quot;W&quot; &#x27;s all over the place in the beginning of the capture but nothing within the packets to tell us which packet it is flagging. Thanks Eric'''
date = "2012-05-25T13:35:00Z"
lastmod = "2012-05-25T13:35:00Z"
weight = 11353
keywords = [ "rtp", "voip", "wrongtimestamp" ]
aliases = [ "/questions/11353" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wrong TimeStamp Determination](/questions/11353/wrong-timestamp-determination)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11353-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11353-score" class="post-score" title="current number of votes">0</div><span id="post-11353-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what criteria is WS using to determine a "wrong timestamp" in an rtp playback? We see "W" 's all over the place in the beginning of the capture but nothing within the packets to tell us which packet it is flagging.</p><p>Thanks</p><p>Eric</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-wrongtimestamp" rel="tag" title="see questions tagged &#39;wrongtimestamp&#39;">wrongtimestamp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '12, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/f797bdc41d990dca073837114e048b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EricKnaus&#39;s gravatar image" /><p><span>EricKnaus</span><br />
<span class="score" title="46 reputation points">46</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EricKnaus has no accepted answers">0%</span></p></div></div><div id="comments-container-11353" class="comments-container"></div><div id="comment-tools-11353" class="comment-tools"></div><div class="clear"></div><div id="comment-11353-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

