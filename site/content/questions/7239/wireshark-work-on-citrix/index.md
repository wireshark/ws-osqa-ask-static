+++
type = "question"
title = "Wireshark work on Citrix"
description = '''Just wondering if wireshark works on Citrix? We are having some audio issues and were looking into this to be able to help us with dictation issues on a software our Dr.&#x27;s use, which is called EPIC.'''
date = "2011-11-04T09:10:00Z"
lastmod = "2011-11-04T09:10:00Z"
weight = 7239
keywords = [ "citrix" ]
aliases = [ "/questions/7239" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark work on Citrix](/questions/7239/wireshark-work-on-citrix)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7239-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7239-score" class="post-score" title="current number of votes">0</div><span id="post-7239-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Just wondering if wireshark works on Citrix? We are having some audio issues and were looking into this to be able to help us with dictation issues on a software our Dr.'s use, which is called EPIC.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-citrix" rel="tag" title="see questions tagged &#39;citrix&#39;">citrix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '11, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/84f35851afe6be1fb33c90f77ff652ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pb121970&#39;s gravatar image" /><p><span>pb121970</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pb121970 has no accepted answers">0%</span></p></div></div><div id="comments-container-7239" class="comments-container"></div><div id="comment-tools-7239" class="comment-tools"></div><div class="clear"></div><div id="comment-7239-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

