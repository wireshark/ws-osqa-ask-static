+++
type = "question"
title = "GPRS interface"
description = '''Hi experts, Can i use wireshark to capture GPRS trafic between my laptop Windows 7 OS using a Israeli Cellcom mobile phone connected to PC usb? I read this at archive by Guy Harris: &quot;As question 5 in the WinPcap FAQ indicates, PPP devices are not supported on Windows Vista and Windows 7. Mobile phon...'''
date = "2013-06-18T04:01:00Z"
lastmod = "2013-06-18T10:30:00Z"
weight = 22129
keywords = [ "gprs", "wireshark" ]
aliases = [ "/questions/22129" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GPRS interface](/questions/22129/gprs-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22129-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22129-score" class="post-score" title="current number of votes">0</div><span id="post-22129-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi experts,</p><p>Can i use wireshark to capture GPRS trafic between my laptop Windows 7 OS using a Israeli Cellcom mobile phone connected to PC usb?</p><p>I read this at archive by Guy Harris:</p><p>"As question 5 in the WinPcap FAQ indicates, PPP devices are not supported on Windows Vista and Windows 7. Mobile phone modems show up as PPP devices, so you cannot capture on them."</p><p>I wonder if it is still the same.</p><p>Thank you,</p><p>Ran.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gprs" rel="tag" title="see questions tagged &#39;gprs&#39;">gprs</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '13, 04:01</strong></p><img src="https://secure.gravatar.com/avatar/252c5a36879d2759a97ac6ff31f8cf38?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ranb323&#39;s gravatar image" /><p><span>ranb323</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ranb323 has no accepted answers">0%</span></p></div></div><div id="comments-container-22129" class="comments-container"></div><div id="comment-tools-22129" class="comment-tools"></div><div class="clear"></div><div id="comment-22129-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22139"></span>

<div id="answer-container-22139" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22139-score" class="post-score" title="current number of votes">1</div><span id="post-22139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I wonder if it is still the same.</p></blockquote><p>Yes.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '13, 10:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-22139" class="comments-container"></div><div id="comment-tools-22139" class="comment-tools"></div><div class="clear"></div><div id="comment-22139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

