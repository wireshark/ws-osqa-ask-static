+++
type = "question"
title = "tshark 1.5 version download location for Linux platform"
description = '''Hi, Can anyone point me to the location of Wireshark/tshark 1.5 version for Linux platform. I&#x27;m working on tshark commandline utility which require 1.5 version. Thanks Asif'''
date = "2011-06-09T23:42:00Z"
lastmod = "2011-06-10T16:33:00Z"
weight = 4489
keywords = [ "download", "tshark" ]
aliases = [ "/questions/4489" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tshark 1.5 version download location for Linux platform](/questions/4489/tshark-15-version-download-location-for-linux-platform)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4489-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4489-score" class="post-score" title="current number of votes">0</div><span id="post-4489-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Can anyone point me to the location of Wireshark/tshark 1.5 version for Linux platform. I'm working on tshark commandline utility which require 1.5 version.</p><p>Thanks Asif</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '11, 23:42</strong></p><img src="https://secure.gravatar.com/avatar/009622f35eab24cfbde3547b04a5bbea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="asif&#39;s gravatar image" /><p><span>asif</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="asif has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>10 Jun '11, 16:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-4489" class="comments-container"><span id="4517"></span><div id="comment-4517" class="comment"><div id="post-4517-score" class="comment-score"></div><div class="comment-text"><p>Just curious... What's so special about 1.5? (Why don't newer versions still work for you?)</p></div><div id="comment-4517-info" class="comment-info"><span class="comment-age">(10 Jun '11, 16:33)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-4489" class="comment-tools"></div><div class="clear"></div><div id="comment-4489-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4496"></span>

<div id="answer-container-4496" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4496-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4496-score" class="post-score" title="current number of votes">0</div><span id="post-4496-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.wireshark.org/download/src/all-versions/">http://www.wireshark.org/download/src/all-versions/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jun '11, 06:03</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-4496" class="comments-container"><span id="4500"></span><div id="comment-4500" class="comment"><div id="post-4500-score" class="comment-score"></div><div class="comment-text"><p>Ok, I've downloaded(http://www.wireshark.org/download/src/all-versions/wireshark-1.6.0rc2.tar.bz2) and performed the following steps, I still see version 1.2 instead of 1.6. Please advise if any step is missing to upgrade my wireshark to 1.6 version.</p><ol><li>./configure</li><li>make</li><li>make install</li></ol><p>I didn't find any issues running above three steps. I still see version as 1.2.</p><p>tshark -version TShark 1.2.10 (RVBD_208)</p><p>Copyright 1998-2010 Gerald Combs <span><span class="__cf_email__" data-cfemail="43242631222f2703342a3126302b2231286d2c3124">[email protected]</span></span> and contributors. This is free software; see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</p><p>Compiled (32-bit) with GLib 2.12.3, with libpcap 0.9.4, with libz 1.2.3, with POSIX capabilities (Linux), with libpcre 6.6, without SMI, without c-ares, without ADNS, without Lua, with GnuTLS 1.4.1, with Gcrypt 1.4.4, with MIT Kerberos, without GeoIP.</p><p>Running on Linux 2.6.18-194.el5, with libpcap version 0.9.4, GnuTLS 1.4.1, Gcrypt 1.4.4.</p><p>Built using gcc 4.1.2 20080704 (Red Hat 4.1.2-48).</p></div><div id="comment-4500-info" class="comment-info"><span class="comment-age">(10 Jun '11, 07:41)</span> <span class="comment-user userinfo">asif</span></div></div><span id="4512"></span><div id="comment-4512" class="comment"><div id="post-4512-score" class="comment-score"></div><div class="comment-text"><p>See <a href="http://ask.wireshark.org/questions/4502/unable-to-upgrade-to-wireshark-16">the answers given when you asked this as a separate question</a>.</p></div><div id="comment-4512-info" class="comment-info"><span class="comment-age">(10 Jun '11, 12:33)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4496" class="comment-tools"></div><div class="clear"></div><div id="comment-4496-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

