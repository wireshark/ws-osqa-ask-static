+++
type = "question"
title = "[closed] my atheros ar-9271 not support monitor mode in wireshark(linux)"
description = '''hi i am using back track 5,which wireless adapter support monitor mode in wireshark?,i am using tp-link tl-722n.this adapter not support monitor mode in wireshark.but monitor mode enabled for this command &quot;airmon-ng start wlan0&quot;.but not support monitor mode in wireshark.what is the reason?.which wir...'''
date = "2015-02-20T07:42:00Z"
lastmod = "2015-02-20T08:27:00Z"
weight = 39980
keywords = [ "wireless" ]
aliases = [ "/questions/39980" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] my atheros ar-9271 not support monitor mode in wireshark(linux)](/questions/39980/my-atheros-ar-9271-not-support-monitor-mode-in-wiresharklinux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39980-score" class="post-score" title="current number of votes">0</div><span id="post-39980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i am using back track 5,which wireless adapter support monitor mode in wireshark?,i am using tp-link tl-722n.this adapter not support monitor mode in wireshark.but monitor mode enabled for this command "airmon-ng start wlan0".but not support monitor mode in wireshark.what is the reason?.which wireless adapter i have to choose?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '15, 07:42</strong></p><img src="https://secure.gravatar.com/avatar/862faa5d26b66d1e5ade6679e966563c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hackerguru1989&#39;s gravatar image" /><p><span>hackerguru1989</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hackerguru1989 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>20 Feb '15, 08:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-39980" class="comments-container"><span id="39982"></span><div id="comment-39982" class="comment"><div id="post-39982-score" class="comment-score"></div><div class="comment-text"><p>This is basically a duplicate of your earlier question: <a href="https://ask.wireshark.org/questions/39944/tp-link-tl-wn-722n-support-monitor-mode-in-wireshark-or-not">https://ask.wireshark.org/questions/39944/tp-link-tl-wn-722n-support-monitor-mode-in-wireshark-or-not</a></p><p>Add comments to the questions asked by <span>@Guy Harris</span> for further help.</p></div><div id="comment-39982-info" class="comment-info"><span class="comment-age">(20 Feb '15, 08:27)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-39980" class="comment-tools"></div><div class="clear"></div><div id="comment-39980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 20 Feb '15, 08:27

</div>

</div>

</div>

