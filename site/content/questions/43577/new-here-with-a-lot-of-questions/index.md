+++
type = "question"
title = "New here with a lot of questions"
description = '''Greetings all, I have wireshark on a usb bootable &quot;wifislax&quot; flash drive that i have been struggling for over a month on and off to fully understand. First, none of my intended uses are malicious in nature. I have set up a fake/fictitious &quot;AP&quot; with &quot;linset&quot; (part of wifislax) which will deauthenicat...'''
date = "2015-06-25T18:00:00Z"
lastmod = "2015-06-25T18:00:00Z"
weight = 43577
keywords = [ "cookies", "wireshark" ]
aliases = [ "/questions/43577" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [New here with a lot of questions](/questions/43577/new-here-with-a-lot-of-questions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43577-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43577-score" class="post-score" title="current number of votes">0</div><span id="post-43577-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings all, I have wireshark on a usb bootable "wifislax" flash drive that i have been struggling for over a month on and off to fully understand. First, <strong>none of my intended uses are malicious in nature.</strong></p><p>I have set up a fake/fictitious "AP" with "linset" (part of wifislax) which will deauthenicate a user(s)on a wifi network <em>OTHER THAN MINE</em>. This practice will cause the "bounced" user to re-log back in to continue net use.</p><p>Using wireshark and selecting the proper interface as well as promiscuous mode, i am unable to load/display cookies even though i am using the correct "expression(s)". What am i doing incorrectly?</p><p>NEXT, would there be a difference if the "user" was using a cell/smart phone as opposed to a computer?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cookies" rel="tag" title="see questions tagged &#39;cookies&#39;">cookies</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jun '15, 18:00</strong></p><img src="https://secure.gravatar.com/avatar/cd5e59e2f060ea0e27f386765cc8f3cc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Reaver%20Grief&#39;s gravatar image" /><p><span>Reaver Grief</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Reaver Grief has no accepted answers">0%</span></p></div></div><div id="comments-container-43577" class="comments-container"></div><div id="comment-tools-43577" class="comment-tools"></div><div class="clear"></div><div id="comment-43577-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

