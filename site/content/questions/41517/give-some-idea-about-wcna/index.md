+++
type = "question"
title = "Give some idea about WCNA"
description = '''I want to give WCNA(Wireshark Certified Network Analyst).   Can anyone tell me how to prepare for that exam? What is the Course content? How is it helpful in future? Is there any other this kind of exams are there which wireshark provides? Which are the companies which validates this exam? '''
date = "2015-04-16T20:52:00Z"
lastmod = "2015-04-17T00:58:00Z"
weight = 41517
keywords = [ "exam", "wireshark" ]
aliases = [ "/questions/41517" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Give some idea about WCNA](/questions/41517/give-some-idea-about-wcna)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41517-score" class="post-score" title="current number of votes">0</div><span id="post-41517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to give WCNA(Wireshark Certified Network Analyst).</p><ol><li>Can anyone tell me how to prepare for that exam?</li><li>What is the Course content?</li><li>How is it helpful in future?</li><li>Is there any other this kind of exams are there which wireshark provides?</li><li>Which are the companies which validates this exam?</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-exam" rel="tag" title="see questions tagged &#39;exam&#39;">exam</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '15, 20:52</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Apr '15, 20:55</strong> </span></p></div></div><div id="comments-container-41517" class="comments-container"></div><div id="comment-tools-41517" class="comment-tools"></div><div class="clear"></div><div id="comment-41517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41518"></span>

<div id="answer-container-41518" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41518-score" class="post-score" title="current number of votes">1</div><span id="post-41518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ankit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>The official study guide book covers the content of the exam fairly well: <a href="http://www.wiresharkbook.com/studyguide.html">http://www.wiresharkbook.com/studyguide.html</a></li><li>I suggest reading through the info package materials here: <a href="http://wiresharktraining.com/certification.html">http://wiresharktraining.com/certification.html</a></li><li>The curriculum is useful/important knowledge for sure. In my opinion the certification itself isn't well-known enough in the industry to be of much value right now professionally, though. Get it if you have all your Cisco certs and have time to kill, but definitely don't prioritize it over CCxx if you're in network administration and this is to add to your professional worth in the industry.</li><li>I don't believe so, no.</li><li>By validates, you mean considers when hiring? Not many, at least in the sense that I've never seen it referenced in job ads.</li></ol><p>The WCNA kind of breaks my heart to be honest. The content speaks to exactly the kind of network analysis skills that are lacking in the industry, but it's been executed on such a small scale and in many ways poorly in my opinion. If it was done hands-on like the Redhat exams, through a resident/credible vendor like CompTIA or even Cisco, it could be something much greater than what it is.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '15, 21:26</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-41518" class="comments-container"><span id="41523"></span><div id="comment-41523" class="comment"><div id="post-41523-score" class="comment-score"></div><div class="comment-text"><p>Thanks Quadratic for sharing the information</p></div><div id="comment-41523-info" class="comment-info"><span class="comment-age">(17 Apr '15, 00:58)</span> <span class="comment-user userinfo">ankit</span></div></div></div><div id="comment-tools-41518" class="comment-tools"></div><div class="clear"></div><div id="comment-41518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

