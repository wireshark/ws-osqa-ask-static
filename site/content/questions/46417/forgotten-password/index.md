+++
type = "question"
title = "forgotten password"
description = '''Greetings! Here is a little story: my whife don&#x27;t remember she&#x27;s Hearthstone (this game works in mobile phone, tablet, and pc) password, and of course she was lazy to use a working e-mail, so we are unable to make the recover steps. This happened on she&#x27;s mobile phone, but it&#x27;s installed on a pc, an...'''
date = "2015-10-08T02:21:00Z"
lastmod = "2015-10-10T12:13:00Z"
weight = 46417
keywords = [ "password" ]
aliases = [ "/questions/46417" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [forgotten password](/questions/46417/forgotten-password)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46417-score" class="post-score" title="current number of votes">0</div><span id="post-46417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings!</p><p>Here is a little story: my whife don't remember she's Hearthstone (this game works in mobile phone, tablet, and pc) password, and of course she was lazy to use a working e-mail, so we are unable to make the recover steps. This happened on she's mobile phone, but it's installed on a pc, and in this moment she is logged in and it's working fine. She tryed to change on the games forum the e-mail adress to a correct one, but of coruse u need the password to do that. Is there a way to get that password with the help of wireshark?</p><p>Regards, Anthony</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '15, 02:21</strong></p><img src="https://secure.gravatar.com/avatar/2db46c9f1913a2d6ff2528261a3ca0b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yi57&#39;s gravatar image" /><p><span>Yi57</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yi57 has no accepted answers">0%</span></p></div></div><div id="comments-container-46417" class="comments-container"></div><div id="comment-tools-46417" class="comment-tools"></div><div class="clear"></div><div id="comment-46417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46445"></span>

<div id="answer-container-46445" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46445-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46445-score" class="post-score" title="current number of votes">2</div><span id="post-46445-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a network troubleshooting tool which helps to analyze problems on the network and/or with network applications. You can look at the content of packets, and if an application transmits a password without encryption, you will see it. <strong>However</strong> if you and/or your wife forgot the password, it won't be transmitted and thus you can't see it in Wireshark. So, the short answer to your question is: <strong>no</strong>.</p><p>I see two possible options for you:</p><ul><li>call the game provider support team and ask them for help</li><li>take your wife to a hypnotist. Maybe he is able to 'recover' the password ;-)</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '15, 12:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-46445" class="comments-container"></div><div id="comment-tools-46445" class="comment-tools"></div><div class="clear"></div><div id="comment-46445-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

