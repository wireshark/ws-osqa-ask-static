+++
type = "question"
title = "Wireshark decode Protocol SSHv1 and SSHv2"
description = '''Hi All, When I capture the packets for SFTP transfer, we notice in the packet from server &quot;server protocol: SSH-2.0-openssh_4.0&quot; but when we check under the protocol in the wireshark, it is showing as SSH and not sshv2.  Is it not Version2? (From the server Protocol?) How is Wireshark deoding it as ...'''
date = "2017-09-22T00:46:00Z"
lastmod = "2017-09-22T02:28:00Z"
weight = 63625
keywords = [ "sftp" ]
aliases = [ "/questions/63625" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark decode Protocol SSHv1 and SSHv2](/questions/63625/wireshark-decode-protocol-sshv1-and-sshv2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63625-score" class="post-score" title="current number of votes">0</div><span id="post-63625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>When I capture the packets for SFTP transfer, we notice in the packet from server "server protocol: SSH-2.0-openssh_4.0" but when we check under the protocol in the wireshark, it is showing as SSH and not sshv2.</p><p>Is it not Version2? (From the server Protocol?)</p><p>How is Wireshark deoding it as sshv1 or SSHv2? This is the packet after the 3 way handshake received from the server.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sftp" rel="tag" title="see questions tagged &#39;sftp&#39;">sftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '17, 00:46</strong></p><img src="https://secure.gravatar.com/avatar/6c685868d46cd97a6a734504d69f5373?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rakeshreddy&#39;s gravatar image" /><p><span>rakeshreddy</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rakeshreddy has no accepted answers">0%</span></p></div></div><div id="comments-container-63625" class="comments-container"><span id="63626"></span><div id="comment-63626" class="comment"><div id="post-63626-score" class="comment-score"></div><div class="comment-text"><p>What Wireshark version are you using? Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-63626-info" class="comment-info"><span class="comment-age">(22 Sep '17, 02:28)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-63625" class="comment-tools"></div><div class="clear"></div><div id="comment-63625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

