+++
type = "question"
title = "http-google101.pcapng"
description = '''I just purchased wirehark 101 essentials. I am trying to figure out where is http-google101.pcapng. Can someone help me out Thanks'''
date = "2013-04-08T12:25:00Z"
lastmod = "2013-04-08T12:31:00Z"
weight = 20195
keywords = [ "book", "supplements" ]
aliases = [ "/questions/20195" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [http-google101.pcapng](/questions/20195/http-google101pcapng)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20195-score" class="post-score" title="current number of votes">0</div><span id="post-20195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just purchased wirehark 101 essentials. I am trying to figure out where is http-google101.pcapng.</p><p>Can someone help me out</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-book" rel="tag" title="see questions tagged &#39;book&#39;">book</span> <span class="post-tag tag-link-supplements" rel="tag" title="see questions tagged &#39;supplements&#39;">supplements</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '13, 12:25</strong></p><img src="https://secure.gravatar.com/avatar/e4189f27221db61a00a77aba2233a6e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rikshaw&#39;s gravatar image" /><p><span>rikshaw</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rikshaw has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Apr '13, 13:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-20195" class="comments-container"></div><div id="comment-tools-20195" class="comment-tools"></div><div class="clear"></div><div id="comment-20195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20197"></span>

<div id="answer-container-20197" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20197-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20197-score" class="post-score" title="current number of votes">4</div><span id="post-20197-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to</p><p><a href="http://wiresharkbook.com/wireshark101.html">http://wiresharkbook.com/wireshark101.html</a></p><p>on left side of the page you see the book supplements.Please feel free to download them(includes lot of pcap traces associated to the book) and you can find your http-google101.pcapng too.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '13, 12:31</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Apr '13, 12:32</strong> </span></p></div></div><div id="comments-container-20197" class="comments-container"></div><div id="comment-tools-20197" class="comment-tools"></div><div class="clear"></div><div id="comment-20197-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

