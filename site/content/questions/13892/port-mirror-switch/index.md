+++
type = "question"
title = "Port mirror switch"
description = '''Does anybody know where I can find a cheap switch with port mirroring/monitoring ability. I have search google and other common sites but want to know if there is any specific one anybody would recommend. I was going to just get a tap, but they are kind of expensive.  Thanks! '''
date = "2012-08-25T23:19:00Z"
lastmod = "2015-01-19T04:35:00Z"
weight = 13892
keywords = [ "switch", "inexpensive", "port", "mirroring" ]
aliases = [ "/questions/13892" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Port mirror switch](/questions/13892/port-mirror-switch)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13892-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13892-score" class="post-score" title="current number of votes">0</div><span id="post-13892-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anybody know where I can find a cheap switch with port mirroring/monitoring ability. I have search google and other common sites but want to know if there is any specific one anybody would recommend. I was going to just get a tap, but they are kind of expensive.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-switch" rel="tag" title="see questions tagged &#39;switch&#39;">switch</span> <span class="post-tag tag-link-inexpensive" rel="tag" title="see questions tagged &#39;inexpensive&#39;">inexpensive</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '12, 23:19</strong></p><img src="https://secure.gravatar.com/avatar/52857dcff5e05dba5f87f9670ead91b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="I_GEEK_IT&#39;s gravatar image" /><p><span>I_GEEK_IT</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="I_GEEK_IT has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Aug '12, 23:21</strong> </span></p></div></div><div id="comments-container-13892" class="comments-container"><span id="13894"></span><div id="comment-13894" class="comment"><div id="post-13894-score" class="comment-score"></div><div class="comment-text"><p>Have a look here <a href="http://wiki.wireshark.org/SwitchReference">http://wiki.wireshark.org/SwitchReference</a></p></div><div id="comment-13894-info" class="comment-info"><span class="comment-age">(26 Aug '12, 00:27)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-13892" class="comment-tools"></div><div class="clear"></div><div id="comment-13892-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13895"></span>

<div id="answer-container-13895" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13895-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13895-score" class="post-score" title="current number of votes">0</div><span id="post-13895-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have good experience with these switches. They are not overly expensive, especially the HP switch. They may not work in every environment (high network load, &gt; 1 Gig), but for the "typical" application problems, they are good enough.</p><p><strong>Cisco 2960-8TC-L</strong></p><blockquote><p><code>http://www.cisco.com/web/DE/verticals/smb/products/routers_switches/catalyst_2960_series_switches/index.html#~models</code><br />
</p></blockquote><p><strong>HP 1810-8G</strong></p><blockquote><p><code>http://h10010.www1.hp.com/wwpc/uk/en/sm/WF06b/12883-12883-4172267-4172281-4172281-3963985-3963989.html?dnr=1</code><br />
</p></blockquote><p>Regarding a (cheap) tap, see the recommendations in this question:</p><blockquote><p><code>http://ask.wireshark.org/questions/13814/where-can-i-find-a-good-tap</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Aug '12, 01:18</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Aug '12, 01:19</strong> </span></p></div></div><div id="comments-container-13895" class="comments-container"></div><div id="comment-tools-13895" class="comment-tools"></div><div class="clear"></div><div id="comment-13895-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15526"></span>

<div id="answer-container-15526" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15526-score" class="post-score" title="current number of votes">0</div><span id="post-15526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's another great option around, the Netgear GS105E, a 5 port gigabit switch, where you can set the fifth port in monitoring mode in the web GUI.</p><p><a href="http://www.netgear.nl/business/products/switches/prosafe-plus-switches/gs105e.aspx#">http://www.netgear.nl/business/products/switches/prosafe-plus-switches/gs105e.aspx#</a></p><p>as Kurt said before, if you're not expecting 1 Gbit prolonged datastreams it'll do just fine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Nov '12, 10:54</strong></p><img src="https://secure.gravatar.com/avatar/69710b84acce4cdf0a0cbdcb5930fda1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marc&#39;s gravatar image" /><p><span>Marc</span><br />
<span class="score" title="147 reputation points">147</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marc has 3 accepted answers">27%</span> </br></br></p></div></div><div id="comments-container-15526" class="comments-container"><span id="15528"></span><div id="comment-15528" class="comment"><div id="post-15528-score" class="comment-score"></div><div class="comment-text"><p>Marc, do you have personal experience with that switch? Can you recommend it?</p></div><div id="comment-15528-info" class="comment-info"><span class="comment-age">(04 Nov '12, 12:02)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="35284"></span><div id="comment-35284" class="comment"><div id="post-35284-score" class="comment-score"></div><div class="comment-text"><p>It doesn't has a web GUI. You need the configuration software (provided) installed.</p></div><div id="comment-35284-info" class="comment-info"><span class="comment-age">(07 Aug '14, 01:10)</span> <span class="comment-user userinfo">cmheng</span></div></div><span id="39272"></span><div id="comment-39272" class="comment"><div id="post-39272-score" class="comment-score"></div><div class="comment-text"><p>GUI enabled on the GS105Ev2 (<a href="http://kb.netgear.com/app/answers/detail/a_id/25397/related/1">http://kb.netgear.com/app/answers/detail/a_id/25397/related/1</a>)</p></div><div id="comment-39272-info" class="comment-info"><span class="comment-age">(19 Jan '15, 04:35)</span> <span class="comment-user userinfo">castro145</span></div></div></div><div id="comment-tools-15526" class="comment-tools"></div><div class="clear"></div><div id="comment-15526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

