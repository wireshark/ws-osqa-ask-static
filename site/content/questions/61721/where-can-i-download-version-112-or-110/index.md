+++
type = "question"
title = "Where can I download version 1.12 or 1.10?"
description = '''Where can I download version 1.12 or 1.10? '''
date = "2017-05-31T14:09:00Z"
lastmod = "2017-05-31T20:35:00Z"
weight = 61721
keywords = [ "version" ]
aliases = [ "/questions/61721" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where can I download version 1.12 or 1.10?](/questions/61721/where-can-i-download-version-112-or-110)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61721-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61721-score" class="post-score" title="current number of votes">0</div><span id="post-61721-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can I download version 1.12 or 1.10?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 May '17, 14:09</strong></p><img src="https://secure.gravatar.com/avatar/a017d036b93c5ccd42e60c5a7fb3f582?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jasch&#39;s gravatar image" /><p><span>jasch</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jasch has no accepted answers">0%</span></p></div></div><div id="comments-container-61721" class="comments-container"></div><div id="comment-tools-61721" class="comment-tools"></div><div class="clear"></div><div id="comment-61721-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61722"></span>

<div id="answer-container-61722" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61722-score" class="post-score" title="current number of votes">1</div><span id="post-61722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://www.wireshark.org/download/">https://www.wireshark.org/download/</a></p><p>In there, you'll find "All versions" subdirectories for the various platforms. May I ask why you want to download an outdated version? If you want to use the GTK GUI, that's also available in the 2.x builds as "Legacy Wireshark".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 May '17, 14:11</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-61722" class="comments-container"><span id="61725"></span><div id="comment-61725" class="comment"><div id="post-61725-score" class="comment-score"></div><div class="comment-text"><p>Possibly to run on Windows XP...</p></div><div id="comment-61725-info" class="comment-info"><span class="comment-age">(31 May '17, 20:35)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-61722" class="comment-tools"></div><div class="clear"></div><div id="comment-61722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

