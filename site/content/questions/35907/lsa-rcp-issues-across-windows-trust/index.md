+++
type = "question"
title = "LSA RCP Issues Across Windows Trust"
description = '''Hi All, I have a 2000 &amp;gt; 2012 Windows External Trust and all works fine. Except for when I add nest a group across the trust I can only resolve the SID and not the actual group name. Looking at Wireshark I see the following: LSARPC - lsaqueryinfopolicy respons, status_invalid_handle , error status...'''
date = "2014-09-01T04:27:00Z"
lastmod = "2014-09-01T04:27:00Z"
weight = 35907
keywords = [ "windows", "2000", "trust" ]
aliases = [ "/questions/35907" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [LSA RCP Issues Across Windows Trust](/questions/35907/lsa-rcp-issues-across-windows-trust)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35907-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35907-score" class="post-score" title="current number of votes">0</div><span id="post-35907-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I have a 2000 &gt; 2012 Windows External Trust and all works fine. Except for when I add nest a group across the trust I can only resolve the SID and not the actual group name. Looking at Wireshark I see the following:</p><p>LSARPC - lsaqueryinfopolicy respons, status_invalid_handle , error status_invalid_Handle SMB - Close Request FID 0x4008 Name Query NB WPAD&lt;00&gt;</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-2000" rel="tag" title="see questions tagged &#39;2000&#39;">2000</span> <span class="post-tag tag-link-trust" rel="tag" title="see questions tagged &#39;trust&#39;">trust</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '14, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/e5c6f335dbf15223a4fdf91934d30e27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="clarkeyi&#39;s gravatar image" /><p><span>clarkeyi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="clarkeyi has no accepted answers">0%</span></p></div></div><div id="comments-container-35907" class="comments-container"></div><div id="comment-tools-35907" class="comment-tools"></div><div class="clear"></div><div id="comment-35907-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

