+++
type = "question"
title = "Identify data traffic requested by users"
description = '''Hi, how can i use wireshark to identify data traffic on a network that is requested by users'''
date = "2011-10-04T04:23:00Z"
lastmod = "2011-10-04T05:27:00Z"
weight = 6694
keywords = [ "traffic", "data" ]
aliases = [ "/questions/6694" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Identify data traffic requested by users](/questions/6694/identify-data-traffic-requested-by-users)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6694-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6694-score" class="post-score" title="current number of votes">0</div><span id="post-6694-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, how can i use wireshark to identify data traffic on a network that is requested by users</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '11, 04:23</strong></p><img src="https://secure.gravatar.com/avatar/af2b3a3c4791a109a1a6fc079d40ad5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samy&#39;s gravatar image" /><p><span>samy</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samy has no accepted answers">0%</span></p></div></div><div id="comments-container-6694" class="comments-container"><span id="6697"></span><div id="comment-6697" class="comment"><div id="post-6697-score" class="comment-score"></div><div class="comment-text"><p>Hi Samy, you have to be a little more specific then that...is it some sort of client-server model, is it Windows, is it webtraffic...many questions..</p></div><div id="comment-6697-info" class="comment-info"><span class="comment-age">(04 Oct '11, 05:27)</span> <span class="comment-user userinfo">Marc</span></div></div></div><div id="comment-tools-6694" class="comment-tools"></div><div class="clear"></div><div id="comment-6694-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

