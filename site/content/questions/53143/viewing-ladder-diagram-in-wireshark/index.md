+++
type = "question"
title = "Viewing ladder diagram in wireshark"
description = '''I&#x27;m using the free download of Wireshark, and I&#x27;m needing to know how to view the data in a Ladder view. How can I accomplish this? Thanks. Justin'''
date = "2016-06-02T07:16:00Z"
lastmod = "2016-06-02T08:30:00Z"
weight = 53143
keywords = [ "ladder" ]
aliases = [ "/questions/53143" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Viewing ladder diagram in wireshark](/questions/53143/viewing-ladder-diagram-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53143-score" class="post-score" title="current number of votes">0</div><span id="post-53143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using the free download of Wireshark, and I'm needing to know how to view the data in a Ladder view. How can I accomplish this?</p><p>Thanks.</p><p>Justin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ladder" rel="tag" title="see questions tagged &#39;ladder&#39;">ladder</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '16, 07:16</strong></p><img src="https://secure.gravatar.com/avatar/a32c9b2c6a6d396ad837e7ed7f6af340?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="justin&#39;s gravatar image" /><p><span>justin</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="justin has no accepted answers">0%</span></p></div></div><div id="comments-container-53143" class="comments-container"></div><div id="comment-tools-53143" class="comment-tools"></div><div class="clear"></div><div id="comment-53143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53144"></span>

<div id="answer-container-53144" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53144-score" class="post-score" title="current number of votes">0</div><span id="post-53144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In the new 2.x QT build, check the menu option "Statistics" -&gt; "FlowGraph"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '16, 07:18</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-53144" class="comments-container"><span id="53145"></span><div id="comment-53145" class="comment"><div id="post-53145-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper. Is that version (2.x QT build)the same as "Wireshark 2.0.2 (64-bit)"?? That's the version I have now, but I don't know if it's the same as the new build you mentioned. Please let me know.</p></div><div id="comment-53145-info" class="comment-info"><span class="comment-age">(02 Jun '16, 07:34)</span> <span class="comment-user userinfo">justin</span></div></div><span id="53146"></span><div id="comment-53146" class="comment"><div id="post-53146-score" class="comment-score"></div><div class="comment-text"><p>Yeah, it is if you're not using the "Legacy Wireshark" version installed along with it. Some are on older versions (1.12 and before) or use the "Legacy" build, which is slightly different.</p></div><div id="comment-53146-info" class="comment-info"><span class="comment-age">(02 Jun '16, 07:37)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="53147"></span><div id="comment-53147" class="comment"><div id="post-53147-score" class="comment-score"></div><div class="comment-text"><p>Thanks! Appreciate it.</p></div><div id="comment-53147-info" class="comment-info"><span class="comment-age">(02 Jun '16, 07:52)</span> <span class="comment-user userinfo">justin</span></div></div><span id="53148"></span><div id="comment-53148" class="comment"><div id="post-53148-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-53148-info" class="comment-info"><span class="comment-age">(02 Jun '16, 08:30)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-53144" class="comment-tools"></div><div class="clear"></div><div id="comment-53144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

