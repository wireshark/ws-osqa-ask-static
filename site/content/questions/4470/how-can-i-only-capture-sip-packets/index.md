+++
type = "question"
title = "How can I only capture SIP packets?"
description = '''Hello,  I only need to capture SIP packets. Would you please tell me how to configure Wireshark to accomplish this? It&#x27;s better to use pictures to describe to me. Thanks. '''
date = "2011-06-09T04:40:00Z"
lastmod = "2011-06-12T19:21:00Z"
weight = 4470
keywords = [ "capture", "sip" ]
aliases = [ "/questions/4470" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How can I only capture SIP packets?](/questions/4470/how-can-i-only-capture-sip-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4470-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4470-score" class="post-score" title="current number of votes">0</div><span id="post-4470-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I only need to capture SIP packets. Would you please tell me how to configure Wireshark to accomplish this? It's better to use pictures to describe to me. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '11, 04:40</strong></p><img src="https://secure.gravatar.com/avatar/fc08cce7831516cfed1a595ab643ed6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jacky%20Yeh&#39;s gravatar image" /><p><span>Jacky Yeh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jacky Yeh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Jun '11, 15:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4470" class="comments-container"><span id="4471"></span><div id="comment-4471" class="comment"><div id="post-4471-score" class="comment-score"></div><div class="comment-text"><p>I have tried the sip, udp, ip, port and ethernet as follows show. Only ethernet can capture SIP packets but this is not my wish. My wish is to use sip filter to capture all pure SIP packets. <embed src="http://www.facebook.com/home.php#!/photo.php?fbid=232523343430444&amp;set=a.232523340097111.78976.100000183401680&amp;type=1&amp;theater" /></p></div><div id="comment-4471-info" class="comment-info"><span class="comment-age">(09 Jun '11, 04:52)</span> <span class="comment-user userinfo">Jacky Yeh</span></div></div></div><div id="comment-tools-4470" class="comment-tools"></div><div class="clear"></div><div id="comment-4470-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4484"></span>

<div id="answer-container-4484" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4484-score" class="post-score" title="current number of votes">1</div><span id="post-4484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Refer to the <a href="http://wiki.wireshark.org/SIP">SIP</a> wiki page.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jun '11, 15:38</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></img></div></div><div id="comments-container-4484" class="comments-container"><span id="4488"></span><div id="comment-4488" class="comment"><div id="post-4488-score" class="comment-score"></div><div class="comment-text"><p>Dear Cmaynard, I mean how I can only capture SIP packets by wireshark? I have tried to configure the capture filter of the capture option but it's unavailable.</p><p>Jacky Yeh</p></div><div id="comment-4488-info" class="comment-info"><span class="comment-age">(09 Jun '11, 22:16)</span> <span class="comment-user userinfo">Jacky Yeh</span></div></div><span id="4514"></span><div id="comment-4514" class="comment"><div id="post-4514-score" class="comment-score">1</div><div class="comment-text"><p>I don't understand what you mean by, "<em>it's unavailable.</em>"</p><p>Maybe you're having trouble with capture filter syntax? Capture filter syntax is not the same as display filter syntax. Have a look at the <a href="http://wiki.wireshark.org/CaptureFilters">Capture Filters</a> wiki page; maybe that will help you.</p></div><div id="comment-4514-info" class="comment-info"><span class="comment-age">(10 Jun '11, 13:03)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="4518"></span><div id="comment-4518" class="comment"><div id="post-4518-score" class="comment-score"></div><div class="comment-text"><p>The <a href="http://wiki.wireshark.org/SIP#Capture_Filter">SIP</a> wiki link posted by @cmaynard states:</p><blockquote><p><strong>Capture Filter</strong></p><p>You cannot directly filter SIP protocols while capturing. However, if you know the UDP or TCP or port used (see above), you can filter on that one.</p></blockquote></div><div id="comment-4518-info" class="comment-info"><span class="comment-age">(10 Jun '11, 16:52)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-4484" class="comment-tools"></div><div class="clear"></div><div id="comment-4484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4526"></span>

<div id="answer-container-4526" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4526-score" class="post-score" title="current number of votes">0</div><span id="post-4526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Usually SIP is on UDP port 5060 (though sometime TCP port 5060 is also use)</p><p>So just use "port 5060" in your capture filter, and the use "sip" in the display filter to filter out any non-SIP traffic that might be on that port</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jun '11, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-4526" class="comments-container"><span id="4530"></span><div id="comment-4530" class="comment"><div id="post-4530-score" class="comment-score"></div><div class="comment-text"><p>Ok, I got it. Thanks so much.</p><p>Jacky Yeh</p></div><div id="comment-4530-info" class="comment-info"><span class="comment-age">(12 Jun '11, 19:21)</span> <span class="comment-user userinfo">Jacky Yeh</span></div></div></div><div id="comment-tools-4526" class="comment-tools"></div><div class="clear"></div><div id="comment-4526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

