+++
type = "question"
title = "Terminate in an unusual way, server 2008 R2 SP1"
description = '''I have two installs of Wireshark 1.0.5 running, 1 instance in on a Windows 7 32bit machine and runs without any issues for as long as I need, my 2nd instance is the problem: Running on a Windows Server 2008 R2 SP1 64-bit and crashes with Visual Studio error that caused the program to terminate in an...'''
date = "2014-01-02T16:52:00Z"
lastmod = "2014-01-02T17:19:00Z"
weight = 28538
keywords = [ "visual-studio" ]
aliases = [ "/questions/28538" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Terminate in an unusual way, server 2008 R2 SP1](/questions/28538/terminate-in-an-unusual-way-server-2008-r2-sp1)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28538-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28538-score" class="post-score" title="current number of votes">0</div><span id="post-28538-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have two installs of Wireshark 1.0.5 running, 1 instance in on a Windows 7 32bit machine and runs without any issues for as long as I need, my 2nd instance is the problem: Running on a Windows Server 2008 R2 SP1 64-bit and crashes with Visual Studio error that caused the program to terminate in an unusual way (every 30-40 minutes), this locks the software and forces you to close Wireshark. Both my machines are running WinPCap 4.1.3 both are fully up to date. Note: There does not seem to be any connection to filters, file sizes, or multiple files as to when it actually crashes. I have tried the troubleshooting steps provided by others by turning off Live data display. Please Help! Bottom Line: 2 installs, same version, one is breaking every 30-40 minutes, different OS's but basically the same.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-visual-studio" rel="tag" title="see questions tagged &#39;visual-studio&#39;">visual-studio</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jan '14, 16:52</strong></p><img src="https://secure.gravatar.com/avatar/327ef68299ada5a00677da39dcac21b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nexus7&#39;s gravatar image" /><p><span>Nexus7</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nexus7 has no accepted answers">0%</span></p></div></div><div id="comments-container-28538" class="comments-container"></div><div id="comment-tools-28538" class="comment-tools"></div><div class="clear"></div><div id="comment-28538-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28539"></span>

<div id="answer-container-28539" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28539-score" class="post-score" title="current number of votes">0</div><span id="post-28539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look at this: <a href="http://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/">http://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jan '14, 16:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28539" class="comments-container"><span id="28540"></span><div id="comment-28540" class="comment"><div id="post-28540-score" class="comment-score"></div><div class="comment-text"><p><span>@Jasper</span> Thanks soo much, I started to read it tonight and discovered I would not have enough time to give it all the attention it needs before I have to go. I will read more into this tomorrow morning and give an update.</p></div><div id="comment-28540-info" class="comment-info"><span class="comment-age">(02 Jan '14, 17:19)</span> <span class="comment-user userinfo">Nexus7</span></div></div></div><div id="comment-tools-28539" class="comment-tools"></div><div class="clear"></div><div id="comment-28539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

