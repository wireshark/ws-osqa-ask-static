+++
type = "question"
title = "Duplicate Packet is not showing in Latest version"
description = '''Hi,  I am trying to view a .pcap file in wireshark which has duplicate packets. Wireshark is not showing that packet only once. However when I read that packet via a JNetPcap library in Java or via notepad++ I see duplicate packet.  One of mu colleague&#x27;s wire shark has older version and his wireshar...'''
date = "2016-03-08T10:55:00Z"
lastmod = "2016-03-08T15:22:00Z"
weight = 50769
keywords = [ "duplicate", "packet" ]
aliases = [ "/questions/50769" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Duplicate Packet is not showing in Latest version](/questions/50769/duplicate-packet-is-not-showing-in-latest-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50769-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50769-score" class="post-score" title="current number of votes">0</div><span id="post-50769-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to view a .pcap file in wireshark which has duplicate packets. Wireshark is not showing that packet only once. However when I read that packet via a JNetPcap library in Java or via notepad++ I see duplicate packet.</p><p>One of mu colleague's wire shark has older version and his wireshark is showing the original and duplicate packets both. Seems like any setting in my wireshark is ignoring the duplicate packet.</p><p>Could any one help me view the duplicate packets also(including the original packet).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-duplicate" rel="tag" title="see questions tagged &#39;duplicate&#39;">duplicate</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '16, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/d20d7102fd9001359c35732770f6f143?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fixmessenger&#39;s gravatar image" /><p><span>fixmessenger</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fixmessenger has no accepted answers">0%</span></p></div></div><div id="comments-container-50769" class="comments-container"><span id="50771"></span><div id="comment-50771" class="comment"><div id="post-50771-score" class="comment-score"></div><div class="comment-text"><p>I think we'll only be able to help if you post the capture in a public space, e.g. Google Drive, Dropbox, etc. and edit your question with a link to it.</p></div><div id="comment-50771-info" class="comment-info"><span class="comment-age">(08 Mar '16, 15:22)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50769" class="comment-tools"></div><div class="clear"></div><div id="comment-50769-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

