+++
type = "question"
title = "Find a string by only knowing the number of characaters"
description = '''Let&#x27;s say I have a .pcap with several packets. how can find a version string by only knowing that is like 11 characters long and has this format x.x-xxxxxxx? Thanks. Regards '''
date = "2016-11-03T18:53:00Z"
lastmod = "2016-11-03T23:09:00Z"
weight = 56959
keywords = [ "filter", "version", "pcap", "string" ]
aliases = [ "/questions/56959" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Find a string by only knowing the number of characaters](/questions/56959/find-a-string-by-only-knowing-the-number-of-characaters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56959-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56959-score" class="post-score" title="current number of votes">0</div><span id="post-56959-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Let's say I have a .pcap with several packets. how can find a version string by only knowing that is like 11 characters long and has this format x.x-xxxxxxx? Thanks. Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-string" rel="tag" title="see questions tagged &#39;string&#39;">string</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '16, 18:53</strong></p><img src="https://secure.gravatar.com/avatar/db274fad338df8a85466f36b10b4bd13?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lcltornado&#39;s gravatar image" /><p><span>lcltornado</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lcltornado has no accepted answers">0%</span></p></div></div><div id="comments-container-56959" class="comments-container"></div><div id="comment-tools-56959" class="comment-tools"></div><div class="clear"></div><div id="comment-56959-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56961"></span>

<div id="answer-container-56961" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56961-score" class="post-score" title="current number of votes">0</div><span id="post-56961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This question is almost the same like <a href="https://ask.wireshark.org/questions/55970/how-to-search-for-a-string-with-the-format-xx-xxxxxxx">this one</a>, so the same answer applies. Regardless whether the older question was yours or not, I'd recommend to continue discussion there if that answer is not detailed enough.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '16, 23:09</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-56961" class="comments-container"></div><div id="comment-tools-56961" class="comment-tools"></div><div class="clear"></div><div id="comment-56961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

