+++
type = "question"
title = "Device to capture data for embedded system"
description = '''I am developing a embedded ethernet system, wireshark is installed in my PC. My ethernet system is required to connect to Internet, can someone inform me which particular hub/router can I use to capture my ethernet system traffic. Regards Sam'''
date = "2013-09-01T19:36:00Z"
lastmod = "2013-09-01T20:42:00Z"
weight = 24273
keywords = [ "router", "hub" ]
aliases = [ "/questions/24273" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Device to capture data for embedded system](/questions/24273/device-to-capture-data-for-embedded-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24273-score" class="post-score" title="current number of votes">0</div><span id="post-24273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am developing a embedded ethernet system, wireshark is installed in my PC.</p><p>My ethernet system is required to connect to Internet, can someone inform me which particular hub/router can I use to capture my ethernet system traffic.</p><p>Regards Sam</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-hub" rel="tag" title="see questions tagged &#39;hub&#39;">hub</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '13, 19:36</strong></p><img src="https://secure.gravatar.com/avatar/abbbd4f0eed2d71176528e800dd17d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rabbit&#39;s gravatar image" /><p><span>rabbit</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rabbit has no accepted answers">0%</span></p></div></div><div id="comments-container-24273" class="comments-container"></div><div id="comment-tools-24273" class="comment-tools"></div><div class="clear"></div><div id="comment-24273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24274"></span>

<div id="answer-container-24274" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24274-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24274-score" class="post-score" title="current number of votes">1</div><span id="post-24274-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Perhaps have a look at the Wireshark wiki page on <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Ethernet capture setup</a> first, as it may help you decide which type of device you're really interested in.</p><p>Also, if you're on <a href="http://www.linkedin.com/">LinkedIn</a>, Christian H. Carrasco started a discussion there entitled, <a href="http://www.linkedin.com/groups/I-am-looking-cheap-switch-2094006.S.215705197"><em>"I am looking for a cheap switch (under $100) that allows port mirroring or monitoring. Suggestions are very welcome."</em></a> The responses offer some possible devices that may suit you.</p><p>As you can see, there is no one particular device, so in the end you will simply have to do some research and then decide for yourself which device you like that meets your needs and is within your budget.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Sep '13, 20:42</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-24274" class="comments-container"></div><div id="comment-tools-24274" class="comment-tools"></div><div class="clear"></div><div id="comment-24274-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

