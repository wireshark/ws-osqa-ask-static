+++
type = "question"
title = "How do I see traffic to and from a machine?"
description = '''I had hoped I could enter the (local) ip of the machine in the &quot;filter:&quot; box, but i&#x27;m just told that isn&#x27;t valid.'''
date = "2012-10-19T08:05:00Z"
lastmod = "2012-10-21T10:52:00Z"
weight = 15106
keywords = [ "machine", "ip", "help" ]
aliases = [ "/questions/15106" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I see traffic to and from a machine?](/questions/15106/how-do-i-see-traffic-to-and-from-a-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15106-score" class="post-score" title="current number of votes">0</div><span id="post-15106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had hoped I could enter the (local) ip of the machine in the "filter:" box, but i'm just told that isn't valid.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-machine" rel="tag" title="see questions tagged &#39;machine&#39;">machine</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '12, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/7506bf73dbd930de4f1d4c6308b9462c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="territ&#39;s gravatar image" /><p><span>territ</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="territ has no accepted answers">0%</span></p></div></div><div id="comments-container-15106" class="comments-container"><span id="15134"></span><div id="comment-15134" class="comment"><div id="post-15134-score" class="comment-score"></div><div class="comment-text"><p>Thanks guys!</p></div><div id="comment-15134-info" class="comment-info"><span class="comment-age">(21 Oct '12, 10:52)</span> <span class="comment-user userinfo">territ</span></div></div></div><div id="comment-tools-15106" class="comment-tools"></div><div class="clear"></div><div id="comment-15106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15107"></span>

<div id="answer-container-15107" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15107-score" class="post-score" title="current number of votes">2</div><span id="post-15107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ip.addr== is the filter you are looking for</p><p>BTW: Google / Wireshark Wiki / several other resources are your friends...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '12, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div></div><div id="comments-container-15107" class="comments-container"><span id="15108"></span><div id="comment-15108" class="comment"><div id="post-15108-score" class="comment-score">1</div><div class="comment-text"><p>Or click the "Expression ..." button, choose your protocol (IP v4), then select the address field (ip.addr), then select the relation (==) then enter your value and click OK.</p></div><div id="comment-15108-info" class="comment-info"><span class="comment-age">(19 Oct '12, 09:03)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-15107" class="comment-tools"></div><div class="clear"></div><div id="comment-15107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

