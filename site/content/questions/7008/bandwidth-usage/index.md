+++
type = "question"
title = "bandwidth usage"
description = '''can wireshark be used to measure bandwidth usage?'''
date = "2011-10-20T06:50:00Z"
lastmod = "2011-10-20T07:46:00Z"
weight = 7008
keywords = [ "bandwidthutilization" ]
aliases = [ "/questions/7008" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [bandwidth usage](/questions/7008/bandwidth-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7008-score" class="post-score" title="current number of votes">0</div><span id="post-7008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can wireshark be used to measure bandwidth usage?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidthutilization" rel="tag" title="see questions tagged &#39;bandwidthutilization&#39;">bandwidthutilization</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Oct '11, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/a8faff166521adb1efb2e44573605fd2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dimmerrm&#39;s gravatar image" /><p><span>dimmerrm</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dimmerrm has no accepted answers">0%</span></p></div></div><div id="comments-container-7008" class="comments-container"></div><div id="comment-tools-7008" class="comment-tools"></div><div class="clear"></div><div id="comment-7008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7010"></span>

<div id="answer-container-7010" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7010-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7010-score" class="post-score" title="current number of votes">1</div><span id="post-7010-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is intended for detailed packet dissection. Of course it can give statistics, but it's not really tailored for it. Better go with native solutions, like netflow statistics from your network equipment, Pilot, ntop. It depends a bit on the context in which you need to monitor.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '11, 07:36</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-7010" class="comments-container"><span id="7011"></span><div id="comment-7011" class="comment"><div id="post-7011-score" class="comment-score"></div><div class="comment-text"><p>thanks for your insight</p></div><div id="comment-7011-info" class="comment-info"><span class="comment-age">(20 Oct '11, 07:46)</span> <span class="comment-user userinfo">dimmerrm</span></div></div></div><div id="comment-tools-7010" class="comment-tools"></div><div class="clear"></div><div id="comment-7010-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

