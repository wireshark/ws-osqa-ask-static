+++
type = "question"
title = "Identify Stretch Acks"
description = '''Hi. I have been searching for some way to definitively identify stretch acks within traces. I have access to ingress/egress and server traces. The stream is a http download from server. I see acks for up to multiples of six of mss in egress trace but unsure if the is segment offloading.  Is such a t...'''
date = "2017-05-31T16:18:00Z"
lastmod = "2017-05-31T16:18:00Z"
weight = 61724
keywords = [ "stretch", "acks" ]
aliases = [ "/questions/61724" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Identify Stretch Acks](/questions/61724/identify-stretch-acks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61724-score" class="post-score" title="current number of votes">0</div><span id="post-61724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi.</p><p>I have been searching for some way to definitively identify stretch acks within traces. I have access to ingress/egress and server traces. The stream is a http download from server. I see acks for up to multiples of six of mss in egress trace but unsure if the is segment offloading.</p><p>Is such a thing possible?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-stretch" rel="tag" title="see questions tagged &#39;stretch&#39;">stretch</span> <span class="post-tag tag-link-acks" rel="tag" title="see questions tagged &#39;acks&#39;">acks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 May '17, 16:18</strong></p><img src="https://secure.gravatar.com/avatar/de4f4e80cc043a42e11a5c437011816c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="petemcfoo&#39;s gravatar image" /><p><span>petemcfoo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="petemcfoo has no accepted answers">0%</span></p></div></div><div id="comments-container-61724" class="comments-container"></div><div id="comment-tools-61724" class="comment-tools"></div><div class="clear"></div><div id="comment-61724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

