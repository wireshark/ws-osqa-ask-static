+++
type = "question"
title = "WireShark support for BMC Remedy 7.6.x"
description = '''Hello, I am initiating search for monitoring tools for BMC Remedy 7.6.x system. I wanted to know if your tool is able to monitor Remedy and add value to our efforts to resolve ongoing issues, which we have yet to identify the source. I appreciate any assistance you may be able to provide.'''
date = "2011-09-06T11:05:00Z"
lastmod = "2011-09-06T11:05:00Z"
weight = 6131
keywords = [ "bmc", "remedy" ]
aliases = [ "/questions/6131" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark support for BMC Remedy 7.6.x](/questions/6131/wireshark-support-for-bmc-remedy-76x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6131-score" class="post-score" title="current number of votes">0</div><span id="post-6131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am initiating search for monitoring tools for BMC Remedy 7.6.x system. I wanted to know if your tool is able to monitor Remedy and add value to our efforts to resolve ongoing issues, which we have yet to identify the source.</p><p>I appreciate any assistance you may be able to provide.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bmc" rel="tag" title="see questions tagged &#39;bmc&#39;">bmc</span> <span class="post-tag tag-link-remedy" rel="tag" title="see questions tagged &#39;remedy&#39;">remedy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Sep '11, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/d2cc994b63ee309286a7e784fdc2a337?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="markl&#39;s gravatar image" /><p><span>markl</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="markl has no accepted answers">0%</span></p></div></div><div id="comments-container-6131" class="comments-container"></div><div id="comment-tools-6131" class="comment-tools"></div><div class="clear"></div><div id="comment-6131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

