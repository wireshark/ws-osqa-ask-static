+++
type = "question"
title = "Possible to disable the scan portion of wireshark"
description = '''Is it possible to install Wireshark only to read PCAP files. Our management doesn&#x27;t want any network scanner software available to anyone on the network. We get sent pcap files when there is an issue for our dns poduct users and can&#x27;t process them. I&#x27;m trying to justify having wireshark on my system...'''
date = "2013-03-25T05:24:00Z"
lastmod = "2013-03-25T05:45:00Z"
weight = 19803
keywords = [ "read", "pcap" ]
aliases = [ "/questions/19803" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Possible to disable the scan portion of wireshark](/questions/19803/possible-to-disable-the-scan-portion-of-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19803-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19803-score" class="post-score" title="current number of votes">0</div><span id="post-19803-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to install Wireshark only to read PCAP files. Our management doesn't want any network scanner software available to anyone on the network. We get sent pcap files when there is an issue for our dns poduct users and can't process them. I'm trying to justify having wireshark on my system, for local use only!!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-read" rel="tag" title="see questions tagged &#39;read&#39;">read</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '13, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/8c2886396c221bf00c1bed913ae33240?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rolsen&#39;s gravatar image" /><p><span>rolsen</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rolsen has no accepted answers">0%</span></p></div></div><div id="comments-container-19803" class="comments-container"></div><div id="comment-tools-19803" class="comment-tools"></div><div class="clear"></div><div id="comment-19803-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19804"></span>

<div id="answer-container-19804" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19804-score" class="post-score" title="current number of votes">2</div><span id="post-19804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="rolsen has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On Windows you could install Wireshark without running the WinPCAP installer. That way Wireshark would not get access to network cards for capturing data when started.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Mar '13, 05:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-19804" class="comments-container"><span id="19805"></span><div id="comment-19805" class="comment"><div id="post-19805-score" class="comment-score"></div><div class="comment-text"><p>This is exactly what i was believing would work. THANKS.</p></div><div id="comment-19805-info" class="comment-info"><span class="comment-age">(25 Mar '13, 05:45)</span> <span class="comment-user userinfo">rolsen</span></div></div></div><div id="comment-tools-19804" class="comment-tools"></div><div class="clear"></div><div id="comment-19804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

