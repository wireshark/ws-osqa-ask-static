+++
type = "question"
title = "Problem with capture in linux"
description = '''Hello! I&#x27;m having a problem when restarting a capture in linux. When I start wireshark it&#x27;s all good. The capture starts fine. If I hit restart/stop this happens: The capture stops, the packet table isn&#x27;t cleared up and I&#x27;m not able to start the packet capture again. I think the problem might be rel...'''
date = "2016-10-28T08:05:00Z"
lastmod = "2016-10-28T08:14:00Z"
weight = 56789
keywords = [ "capture", "problem", "error", "linux" ]
aliases = [ "/questions/56789" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problem with capture in linux](/questions/56789/problem-with-capture-in-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56789-score" class="post-score" title="current number of votes">0</div><span id="post-56789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello!</p><p>I'm having a problem when restarting a capture in linux. When I start wireshark it's all good. The capture starts fine. If I hit restart/stop this happens: The capture stops, the packet table isn't cleared up and I'm not able to start the packet capture again.</p><p>I think the problem might be related with libcap, but I don't know what to do..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '16, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/7095c0093dc6df76c42db43dac10d908?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Needhelp&#39;s gravatar image" /><p><span>Needhelp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Needhelp has no accepted answers">0%</span></p></div></div><div id="comments-container-56789" class="comments-container"><span id="56790"></span><div id="comment-56790" class="comment"><div id="post-56790-score" class="comment-score">1</div><div class="comment-text"><p>What version of Wireshark are you running? <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12715">Bug 12715</a> says that upgrading to 2.0.5 (from an earlier 2.0 release) fixes the problem.</p></div><div id="comment-56790-info" class="comment-info"><span class="comment-age">(28 Oct '16, 08:14)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-56789" class="comment-tools"></div><div class="clear"></div><div id="comment-56789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

