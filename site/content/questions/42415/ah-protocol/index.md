+++
type = "question"
title = "AH Protocol"
description = '''what makes AH to work at the transport layer ? though it does not have any port number but just protocol id .'''
date = "2015-05-15T07:23:00Z"
lastmod = "2015-05-15T09:47:00Z"
weight = 42415
keywords = [ "ah", "protocol" ]
aliases = [ "/questions/42415" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [AH Protocol](/questions/42415/ah-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42415-score" class="post-score" title="current number of votes">0</div><span id="post-42415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what makes AH to work at the transport layer ? though it does not have any port number but just protocol id .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ah" rel="tag" title="see questions tagged &#39;ah&#39;">ah</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '15, 07:23</strong></p><img src="https://secure.gravatar.com/avatar/962349492f305ec7bae240fb8c9996ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tech%20round&#39;s gravatar image" /><p><span>tech round</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tech round has no accepted answers">0%</span></p></div></div><div id="comments-container-42415" class="comments-container"></div><div id="comment-tools-42415" class="comment-tools"></div><div class="clear"></div><div id="comment-42415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42425"></span>

<div id="answer-container-42425" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42425-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42425-score" class="post-score" title="current number of votes">0</div><span id="post-42425-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>what makes AH to work at the transport layer ?</p></blockquote><p>What makes it work? Well, the implicit agreement of all IPSEC "vendors/implementations" to adhere to at least a common subset of the IPSEC standard plus other "things" in the IPSEC header, like the SPI, etc. (I guess you are asking how it works without using ports, right?). You'll find that information in any decent book about IPSEC.</p><p>I'm not sure if this Q&amp;A site is the right place for general questions like yours, as your question is neither related to <strong>Wireshark</strong> nor to <strong>network troubleshooting</strong>.</p><p>Unless you can add some details that link your question to Wireshark and/or network troubleshooting, this question is going to be closed as "off topic"!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '15, 09:47</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 May '15, 09:48</strong> </span></p></div></div><div id="comments-container-42425" class="comments-container"></div><div id="comment-tools-42425" class="comment-tools"></div><div class="clear"></div><div id="comment-42425-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

