+++
type = "question"
title = "How to capture data-link layer on ipad with wireshark"
description = '''Hello, i&#x27;ve managed to capture on a macbook traffic from an ipad (with a virtual interface mounted thanks to xcode) but it&#x27;s layer 4 (fake Ethernet capture). I&#x27;ve also managed to capture layer 2 from the macbook after editing preferences on the wifi interface. But i fail to modify preferences on the...'''
date = "2014-03-28T03:58:00Z"
lastmod = "2014-04-02T02:15:00Z"
weight = 31246
keywords = [ "ipad", "link", "layer", "data" ]
aliases = [ "/questions/31246" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture data-link layer on ipad with wireshark](/questions/31246/how-to-capture-data-link-layer-on-ipad-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31246-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31246-score" class="post-score" title="current number of votes">0</div><span id="post-31246-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>i've managed to capture on a macbook traffic from an ipad (with a virtual interface mounted thanks to xcode) but it's layer 4 (fake Ethernet capture). I've also managed to capture layer 2 from the macbook after editing preferences on the wifi interface.</p><p>But i fail to modify preferences on the rvi0 interface.</p><p>Is it possible?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipad" rel="tag" title="see questions tagged &#39;ipad&#39;">ipad</span> <span class="post-tag tag-link-link" rel="tag" title="see questions tagged &#39;link&#39;">link</span> <span class="post-tag tag-link-layer" rel="tag" title="see questions tagged &#39;layer&#39;">layer</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '14, 03:58</strong></p><img src="https://secure.gravatar.com/avatar/0d63a3fed0c60cece603474efb221744?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Itsma%20Live&#39;s gravatar image" /><p><span>Itsma Live</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Itsma Live has no accepted answers">0%</span></p></div></div><div id="comments-container-31246" class="comments-container"></div><div id="comment-tools-31246" class="comment-tools"></div><div class="clear"></div><div id="comment-31246-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31374"></span>

<div id="answer-container-31374" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31374-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31374-score" class="post-score" title="current number of votes">0</div><span id="post-31374-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>So it seems impossible but in monitor mode Wiresark is able to capture layer2. After filtering, it's OK.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '14, 02:15</strong></p><img src="https://secure.gravatar.com/avatar/0d63a3fed0c60cece603474efb221744?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Itsma%20Live&#39;s gravatar image" /><p><span>Itsma Live</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Itsma Live has no accepted answers">0%</span></p></div></div><div id="comments-container-31374" class="comments-container"></div><div id="comment-tools-31374" class="comment-tools"></div><div class="clear"></div><div id="comment-31374-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

