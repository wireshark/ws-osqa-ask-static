+++
type = "question"
title = "tcp dup ack and retransmission constant problem"
description = '''Getting constant retransmission and dup ack logs on several pc in lan network. How do i resolve such a problem? I can upload a wireshark scan if needed? Can anyone advise?'''
date = "2014-04-14T22:44:00Z"
lastmod = "2014-04-15T01:39:00Z"
weight = 31813
keywords = [ "lan", "dupack", "tcp" ]
aliases = [ "/questions/31813" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tcp dup ack and retransmission constant problem](/questions/31813/tcp-dup-ack-and-retransmission-constant-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31813-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31813-score" class="post-score" title="current number of votes">0</div><span id="post-31813-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Getting constant retransmission and dup ack logs on several pc in lan network. How do i resolve such a problem? I can upload a wireshark scan if needed? Can anyone advise?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-dupack" rel="tag" title="see questions tagged &#39;dupack&#39;">dupack</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Apr '14, 22:44</strong></p><img src="https://secure.gravatar.com/avatar/0f748e779284ae7360c5e78a3bd87b53?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Burundukas%20Lukas&#39;s gravatar image" /><p><span>Burundukas L...</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Burundukas Lukas has no accepted answers">0%</span></p></div></div><div id="comments-container-31813" class="comments-container"><span id="31828"></span><div id="comment-31828" class="comment"><div id="post-31828-score" class="comment-score"></div><div class="comment-text"><p>Please upload the capture file somewhere (google drive, dropbox, cloudshark.org).</p></div><div id="comment-31828-info" class="comment-info"><span class="comment-age">(15 Apr '14, 01:39)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-31813" class="comment-tools"></div><div class="clear"></div><div id="comment-31813-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

