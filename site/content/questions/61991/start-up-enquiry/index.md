+++
type = "question"
title = "start up enquiry"
description = '''Please does wire shark need to use my data for its work? .i e. Must I be online(using my own data) for it to start capturing packets? '''
date = "2017-06-13T13:43:00Z"
lastmod = "2017-06-14T00:51:00Z"
weight = 61991
keywords = [ "urgent" ]
aliases = [ "/questions/61991" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [start up enquiry](/questions/61991/start-up-enquiry)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61991-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61991-score" class="post-score" title="current number of votes">0</div><span id="post-61991-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please does wire shark need to use my data for its work? .i e. Must I be online(using my own data) for it to start capturing packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-urgent" rel="tag" title="see questions tagged &#39;urgent&#39;">urgent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '17, 13:43</strong></p><img src="https://secure.gravatar.com/avatar/5ccb451dee2c6853057786bc5b4f197f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ozor&#39;s gravatar image" /><p><span>Ozor</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ozor has no accepted answers">0%</span></p></div></div><div id="comments-container-61991" class="comments-container"></div><div id="comment-tools-61991" class="comment-tools"></div><div class="clear"></div><div id="comment-61991-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61993"></span>

<div id="answer-container-61993" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61993-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61993-score" class="post-score" title="current number of votes">1</div><span id="post-61993-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark can load any previously captured traffic from a file without being on-line. To resolve IP addresses to hostnames (if not saved in the file) will need access to an external name resolution system.</p><p>To capture traffic, the interface you wish to capture on must be enabled, however, Wireshark does not need access to any external resources to do so (apart from name resolution as noted above).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jun '17, 14:38</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61993" class="comments-container"><span id="61996"></span><div id="comment-61996" class="comment"><div id="post-61996-score" class="comment-score"></div><div class="comment-text"><p>Thanks. But for the initial capture do I need to b online?</p></div><div id="comment-61996-info" class="comment-info"><span class="comment-age">(13 Jun '17, 23:26)</span> <span class="comment-user userinfo">Ozor</span></div></div><span id="61997"></span><div id="comment-61997" class="comment"><div id="post-61997-score" class="comment-score"></div><div class="comment-text"><p>It depends... If you want to capture network traffic, then you'll need to have a network interface that is up, connected to a network. If you want to capture other types on traffic (eg. USB) then you don't have to.</p></div><div id="comment-61997-info" class="comment-info"><span class="comment-age">(14 Jun '17, 00:51)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-61993" class="comment-tools"></div><div class="clear"></div><div id="comment-61993-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

