+++
type = "question"
title = "Decoding .raw using ILBC ?"
description = '''I have captured RTP packets, and saved them as .raw file. It seems that the audio was encoded in ILBC, so I wonder if anybody has any clue about the tool to decode RTP (or .raw) ?? I have checked Cain &amp;amp; Abel, but it provides wav file for the captured packet, doesnt seem to have decoding option s...'''
date = "2015-05-03T09:29:00Z"
lastmod = "2015-05-03T09:29:00Z"
weight = 42031
keywords = [ "decode_rtp", "voip" ]
aliases = [ "/questions/42031" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding .raw using ILBC ?](/questions/42031/decoding-raw-using-ilbc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42031-score" class="post-score" title="current number of votes">0</div><span id="post-42031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured RTP packets, and saved them as .raw file. It seems that the audio was encoded in ILBC, so I wonder if anybody has any clue about the tool to decode RTP (or .raw) ?? I have checked Cain &amp; Abel, but it provides wav file for the captured packet, doesnt seem to have decoding option separately.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode_rtp" rel="tag" title="see questions tagged &#39;decode_rtp&#39;">decode_rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '15, 09:29</strong></p><img src="https://secure.gravatar.com/avatar/4ec917e3556fb6d9c03cc0e39ec7732a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shas&#39;s gravatar image" /><p><span>Shas</span><br />
<span class="score" title="1 reputation points">1</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shas has no accepted answers">0%</span></p></div></div><div id="comments-container-42031" class="comments-container"></div><div id="comment-tools-42031" class="comment-tools"></div><div class="clear"></div><div id="comment-42031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

