+++
type = "question"
title = "Capture my wireless (iPhone &amp; iPad) traffic"
description = '''I captured some iphone packets using shark for root on my android, is there a way to decrypt the messages using wireshark? Thanks.'''
date = "2014-02-05T03:10:00Z"
lastmod = "2014-02-05T06:11:00Z"
weight = 29453
keywords = [ "shark", "root", "iphone", "for" ]
aliases = [ "/questions/29453" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture my wireless (iPhone & iPad) traffic](/questions/29453/capture-my-wireless-iphone-ipad-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29453-score" class="post-score" title="current number of votes">0</div><span id="post-29453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured some iphone packets using shark for root on my android, is there a way to decrypt the messages using wireshark? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-shark" rel="tag" title="see questions tagged &#39;shark&#39;">shark</span> <span class="post-tag tag-link-root" rel="tag" title="see questions tagged &#39;root&#39;">root</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span> <span class="post-tag tag-link-for" rel="tag" title="see questions tagged &#39;for&#39;">for</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '14, 03:10</strong></p><img src="https://secure.gravatar.com/avatar/ae006da94212fc05710172903bbcc357?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hozz&#39;s gravatar image" /><p><span>Hozz</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hozz has no accepted answers">0%</span></p></div></div><div id="comments-container-29453" class="comments-container"></div><div id="comment-tools-29453" class="comment-tools"></div><div class="clear"></div><div id="comment-29453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29456"></span>

<div id="answer-container-29456" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29456-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29456-score" class="post-score" title="current number of votes">0</div><span id="post-29456-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>is there a way to <strong>decrypt the messages</strong> using wireshark?</p></blockquote><p>If you really mean to <strong>decrypt</strong> wifi traffic, please see the WLAN decryption wiki.</p><blockquote><p><a href="http://wiki.wireshark.org/HowToDecrypt802.11">http://wiki.wireshark.org/HowToDecrypt802.11</a></p></blockquote><p>If 'decrypt' just means 'decode/dissect', then simply open the capture file in Wireshark.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '14, 05:20</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Feb '14, 05:25</strong> </span></p></div></div><div id="comments-container-29456" class="comments-container"><span id="29458"></span><div id="comment-29458" class="comment"><div id="post-29458-score" class="comment-score"></div><div class="comment-text"><p>It's wi-fi traffic decryption. I used my android as a wi-fi modem, then connected the iphone in my android's wi-fi, and used the shark for root to get the data. I can use wlan decryption or there's another one?</p><p>Thanks, sorry about my poor english. =D</p></div><div id="comment-29458-info" class="comment-info"><span class="comment-age">(05 Feb '14, 05:40)</span> <span class="comment-user userinfo">Hozz</span></div></div><span id="29459"></span><div id="comment-29459" class="comment"><div id="post-29459-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I can use wlan decryption or there's another one?</p></blockquote><p>Yes, see my link above. You need the WPA password (or WEP key) and you must ensure that the four EAPOL frames (for WPA) are included in the capture file. However, that's all explained in the Wiki (link above).</p></div><div id="comment-29459-info" class="comment-info"><span class="comment-age">(05 Feb '14, 06:11)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29456" class="comment-tools"></div><div class="clear"></div><div id="comment-29456-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

