+++
type = "question"
title = "How to calculate latency for rtp packets using wireshark?"
description = '''How to calculate latency for rtp packets using wireshark?  is the maximum delta value is the latency for the RTP stream??'''
date = "2014-11-17T15:59:00Z"
lastmod = "2014-11-17T15:59:00Z"
weight = 37919
keywords = [ "latency" ]
aliases = [ "/questions/37919" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to calculate latency for rtp packets using wireshark?](/questions/37919/how-to-calculate-latency-for-rtp-packets-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37919-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37919-score" class="post-score" title="current number of votes">0</div><span id="post-37919-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to calculate latency for rtp packets using wireshark? is the maximum delta value is the latency for the RTP stream??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '14, 15:59</strong></p><img src="https://secure.gravatar.com/avatar/674cdec4c0e58b1d629afb8a6dfde3ed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SHailesh0805&#39;s gravatar image" /><p><span>SHailesh0805</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SHailesh0805 has no accepted answers">0%</span></p></div></div><div id="comments-container-37919" class="comments-container"></div><div id="comment-tools-37919" class="comment-tools"></div><div class="clear"></div><div id="comment-37919-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

