+++
type = "question"
title = "Version 1.12.0 - WLAN Adapter Now Not Listed"
description = '''I recently updated to WireShark Version 1.12.0 (v1.12.0-0-g4fab41a from master-1.12). Since the update, my wireless adapter is not listed under &#x27;Capture Interfaces&#x27;. No packet activity appears under any listed interface. (Not connected to Ethernet.) Windows 8.1 Intel Centrino Advanced-N 6250 AGN 2x2...'''
date = "2014-08-27T08:27:00Z"
lastmod = "2014-08-27T08:27:00Z"
weight = 35811
keywords = [ "wireless", "interface", "not", "listed" ]
aliases = [ "/questions/35811" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Version 1.12.0 - WLAN Adapter Now Not Listed](/questions/35811/version-1120-wlan-adapter-now-not-listed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35811-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35811-score" class="post-score" title="current number of votes">0</div><span id="post-35811-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I recently updated to WireShark Version 1.12.0 (v1.12.0-0-g4fab41a from master-1.12). Since the update, my wireless adapter is not listed under 'Capture Interfaces'. No packet activity appears under any listed interface. (Not connected to Ethernet.)</p><p>Windows 8.1 Intel Centrino Advanced-N 6250 AGN 2x2 HMC WiFi/WiMAX Adapter managed by Intel PROSet Utility.</p><p>Any assistance appreciated.</p><p>John</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-listed" rel="tag" title="see questions tagged &#39;listed&#39;">listed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '14, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/5823aea4be67e8b366a41b9bd6354c49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="configt&#39;s gravatar image" /><p><span>configt</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="configt has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Aug '14, 08:28</strong> </span></p></div></div><div id="comments-container-35811" class="comments-container"></div><div id="comment-tools-35811" class="comment-tools"></div><div class="clear"></div><div id="comment-35811-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

