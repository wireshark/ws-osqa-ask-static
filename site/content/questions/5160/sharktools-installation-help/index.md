+++
type = "question"
title = "Sharktools Installation help"
description = '''Looking for any sharktools user here.  In the README file, under &quot;= Building/Installation instructions =&quot;, no 6, this line:/path/to/sharktools$ ./configure --with-wireshark-src=/path/to/wireshark-x.y.z I don&#x27;t understand what the author means by &quot;--with-wireshark-src&quot;. Any help is appreciated. Thank...'''
date = "2011-07-21T19:27:00Z"
lastmod = "2011-07-21T23:47:00Z"
weight = 5160
keywords = [ "sharktools" ]
aliases = [ "/questions/5160" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sharktools Installation help](/questions/5160/sharktools-installation-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5160-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5160-score" class="post-score" title="current number of votes">0</div><span id="post-5160-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Looking for any sharktools user here. In the README file, under "= Building/Installation instructions =", no 6,</p><p>this line:/path/to/sharktools$ ./configure --with-wireshark-src=/path/to/wireshark-x.y.z</p><p>I don't understand what the author means by "--with-wireshark-src".</p><p>Any help is appreciated.</p><p>Thanks</p><p>Regards, Eddie Choo</p><p>NB: i only need to run the test program.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sharktools" rel="tag" title="see questions tagged &#39;sharktools&#39;">sharktools</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '11, 19:27</strong></p><img src="https://secure.gravatar.com/avatar/c1dac05d0e75992546b5da006c6b718e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eddie%20choo&#39;s gravatar image" /><p><span>eddie choo</span><br />
<span class="score" title="66 reputation points">66</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eddie choo has 2 accepted answers">66%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Jul '11, 20:08</strong> </span></p></div></div><div id="comments-container-5160" class="comments-container"><span id="5161"></span><div id="comment-5161" class="comment"><div id="post-5161-score" class="comment-score"></div><div class="comment-text"><p>Please let me know if there is a sharktools user group out there. Thanks</p></div><div id="comment-5161-info" class="comment-info"><span class="comment-age">(21 Jul '11, 19:28)</span> <span class="comment-user userinfo">eddie choo</span></div></div><span id="5164"></span><div id="comment-5164" class="comment"><div id="post-5164-score" class="comment-score"></div><div class="comment-text"><p>For those who are interested : <a href="http://www.mit.edu/~armenb/sharktools/">http://www.mit.edu/~armenb/sharktools/</a> and <a href="http://seclists.org/wireshark/2010/Nov/62">http://seclists.org/wireshark/2010/Nov/62</a></p></div><div id="comment-5164-info" class="comment-info"><span class="comment-age">(21 Jul '11, 19:59)</span> <span class="comment-user userinfo">eddie choo</span></div></div></div><div id="comment-tools-5160" class="comment-tools"></div><div class="clear"></div><div id="comment-5160-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5167"></span>

<div id="answer-container-5167" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5167-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5167-score" class="post-score" title="current number of votes">1</div><span id="post-5167-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="eddie choo has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Ok, its me again answering my own question.. type <code>./configure --help</code> and you will understand.</p><p>Thanks to Siti Nur Shuhada for her help</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '11, 23:47</strong></p><img src="https://secure.gravatar.com/avatar/c1dac05d0e75992546b5da006c6b718e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eddie%20choo&#39;s gravatar image" /><p><span>eddie choo</span><br />
<span class="score" title="66 reputation points">66</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eddie choo has 2 accepted answers">66%</span></p></div></div><div id="comments-container-5167" class="comments-container"></div><div id="comment-tools-5167" class="comment-tools"></div><div class="clear"></div><div id="comment-5167-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

