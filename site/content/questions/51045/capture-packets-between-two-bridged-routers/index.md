+++
type = "question"
title = "Capture Packets between two bridged routers"
description = '''Hi I&#x27;m having difficult time capturing packets from my home network. I&#x27;m connected to the router downstairs which is bridged by an ethernet cable. I&#x27;m trying to sniff another computer on the same router using my wireless nic card. I can see all of my traffic but not others. I can ping them and get a...'''
date = "2016-03-19T10:48:00Z"
lastmod = "2016-03-19T11:21:00Z"
weight = 51045
keywords = [ "router", "cant", "packets", "lost", "bridged" ]
aliases = [ "/questions/51045" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Packets between two bridged routers](/questions/51045/capture-packets-between-two-bridged-routers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51045-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51045-score" class="post-score" title="current number of votes">0</div><span id="post-51045-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I'm having difficult time capturing packets from my home network. I'm connected to the router downstairs which is bridged by an ethernet cable. I'm trying to sniff another computer on the same router using my wireless nic card. I can see all of my traffic but not others. I can ping them and get a reply but nothing other than that. I'm also under comcast and lots of it is showing up in ipv6.<br />
</p><p>Home Setup</p><p><strong>modem</strong>--<em>ethernet</em>--&gt;<strong>Xfinity Router</strong>--<em>ethernet</em>--&gt;<strong>Cisco Router</strong>-))<em>wifi</em> <strong>Com 1</strong> <strong>Com 2</strong> <strong>Com 3</strong> All the Com 1 2 3 are on the Cisco router</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-cant" rel="tag" title="see questions tagged &#39;cant&#39;">cant</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-lost" rel="tag" title="see questions tagged &#39;lost&#39;">lost</span> <span class="post-tag tag-link-bridged" rel="tag" title="see questions tagged &#39;bridged&#39;">bridged</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '16, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/896cae1e0f749467a3b7b5846a0fcbc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thanousay%20Khamphoune&#39;s gravatar image" /><p><span>Thanousay Kh...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thanousay Khamphoune has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-51045" class="comments-container"><span id="51046"></span><div id="comment-51046" class="comment"><div id="post-51046-score" class="comment-score"></div><div class="comment-text"><p>Please check <a href="https://ask.wireshark.org/questions/50844/cant-capture-all-packets-from-my-iphone">this older question</a> and this <a href="https://wiki.wireshark.org/CaptureSetup">manual page</a> first. If it doesn't help, write a comment with details regarding what is unclear there.</p></div><div id="comment-51046-info" class="comment-info"><span class="comment-age">(19 Mar '16, 11:21)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-51045" class="comment-tools"></div><div class="clear"></div><div id="comment-51045-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

