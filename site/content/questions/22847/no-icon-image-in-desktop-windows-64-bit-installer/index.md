+++
type = "question"
title = "No icon image in Desktop - windows 64 bit installer"
description = '''I just installed wireshark 1.10.0 stable using the 64 bit installer in my win 7 64 bit machine . Installation is proper and works fine but the Desktop shortcut seems to be missing image file.  Is it a problem with my machine or is the installer missing the icon file.'''
date = "2013-07-11T04:43:00Z"
lastmod = "2013-07-11T07:37:00Z"
weight = 22847
keywords = [ "installer" ]
aliases = [ "/questions/22847" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [No icon image in Desktop - windows 64 bit installer](/questions/22847/no-icon-image-in-desktop-windows-64-bit-installer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22847-score" class="post-score" title="current number of votes">0</div><span id="post-22847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed wireshark 1.10.0 stable using the 64 bit installer in my win 7 64 bit machine . Installation is proper and works fine but the Desktop shortcut seems to be missing image file.</p><p>Is it a problem with my machine or is the installer missing the icon file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '13, 04:43</strong></p><img src="https://secure.gravatar.com/avatar/dfb3264a70d2e679ba279f0a2a866fee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Arun%20Balaji&#39;s gravatar image" /><p><span>Arun Balaji</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Arun Balaji has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jul '13, 04:43</strong> </span></p></div></div><div id="comments-container-22847" class="comments-container"><span id="22852"></span><div id="comment-22852" class="comment"><div id="post-22852-score" class="comment-score">1</div><div class="comment-text"><p>Does the link work? I mean, if you double-click the icon on the Desktop, does Wireshark start?</p><p>If so, do you see an icon for wireshark.exe, if you open the Wireshark install directory?</p></div><div id="comment-22852-info" class="comment-info"><span class="comment-age">(11 Jul '13, 04:57)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="22857"></span><div id="comment-22857" class="comment"><div id="post-22857-score" class="comment-score"></div><div class="comment-text"><p>yes it starts and yes the wireshark.exe file has an icon.</p></div><div id="comment-22857-info" class="comment-info"><span class="comment-age">(11 Jul '13, 05:49)</span> <span class="comment-user userinfo">Arun Balaji</span></div></div></div><div id="comment-tools-22847" class="comment-tools"></div><div class="clear"></div><div id="comment-22847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="22849"></span>

<div id="answer-container-22849" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22849-score" class="post-score" title="current number of votes">1</div><span id="post-22849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Arun Balaji has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's probably a problem with your machine. Win7 often breaks the Icon cache and it then needs to be rebuilt to show the actual icon. There is a ton of how-to instructions when googling for "windows 7 rebuild icon cache". Or you can use a small tool that I usually use for this, which is the Icon Cache Rebuilder: <a href="http://www.thewindowsclub.com/rebuild-corrupt-icon-cache-with-icon-cache-rebuilder">http://www.thewindowsclub.com/rebuild-corrupt-icon-cache-with-icon-cache-rebuilder</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '13, 04:52</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-22849" class="comments-container"><span id="22859"></span><div id="comment-22859" class="comment"><div id="post-22859-score" class="comment-score">1</div><div class="comment-text"><p>there is also a Fix it from Microsoft</p><blockquote><p><a href="http://support.microsoft.com/kb/2396571/en-us">http://support.microsoft.com/kb/2396571/en-us</a></p></blockquote></div><div id="comment-22859-info" class="comment-info"><span class="comment-age">(11 Jul '13, 05:58)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="22860"></span><div id="comment-22860" class="comment"><div id="post-22860-score" class="comment-score"></div><div class="comment-text"><p>Thanks that did the trick</p></div><div id="comment-22860-info" class="comment-info"><span class="comment-age">(11 Jul '13, 05:58)</span> <span class="comment-user userinfo">Arun Balaji</span></div></div></div><div id="comment-tools-22849" class="comment-tools"></div><div class="clear"></div><div id="comment-22849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="22867"></span>

<div id="answer-container-22867" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22867-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22867-score" class="post-score" title="current number of votes">2</div><span id="post-22867-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This was fixed (in Wireshark) with <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8773">bug 8773</a>. The icon flushing stuff will automatically happen starting with version 1.10.1.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '13, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-22867" class="comments-container"></div><div id="comment-tools-22867" class="comment-tools"></div><div class="clear"></div><div id="comment-22867-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

