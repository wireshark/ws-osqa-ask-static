+++
type = "question"
title = "Ericsson CS1+ decoder"
description = '''I need to know if there is a plug-in, decoder for the CS1+ protocol that could be used with Wireshark? When opening an INAP traffic, some of them are successfully displayed while in others there are errors like &quot;wrong field in sequence&quot;....etc'''
date = "2011-03-06T00:31:00Z"
lastmod = "2011-03-21T19:55:00Z"
weight = 2680
keywords = [ "cs1+", "inap" ]
aliases = [ "/questions/2680" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ericsson CS1+ decoder](/questions/2680/ericsson-cs1-decoder)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2680-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2680-score" class="post-score" title="current number of votes">0</div><span id="post-2680-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to know if there is a plug-in, decoder for the CS1+ protocol that could be used with Wireshark?</p><p>When opening an INAP traffic, some of them are successfully displayed while in others there are errors like "wrong field in sequence"....etc</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cs1+" rel="tag" title="see questions tagged &#39;cs1+&#39;">cs1+</span> <span class="post-tag tag-link-inap" rel="tag" title="see questions tagged &#39;inap&#39;">inap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '11, 00:31</strong></p><img src="https://secure.gravatar.com/avatar/acb1f3dd396cb56edfe7c7027aa2f150?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ahmad_Zuhd&#39;s gravatar image" /><p><span>Ahmad_Zuhd</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ahmad_Zuhd has no accepted answers">0%</span></p></div></div><div id="comments-container-2680" class="comments-container"></div><div id="comment-tools-2680" class="comment-tools"></div><div class="clear"></div><div id="comment-2680-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2995"></span>

<div id="answer-container-2995" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2995-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2995-score" class="post-score" title="current number of votes">0</div><span id="post-2995-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To my knowledge, there is not. But you might want to try to contact some of the folks on <a href="http://seclists.org/wireshark/2009/Oct/141">this thread</a> to see if they made any progress with their CS1+ dissector.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '11, 19:55</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-2995" class="comments-container"></div><div id="comment-tools-2995" class="comment-tools"></div><div class="clear"></div><div id="comment-2995-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

