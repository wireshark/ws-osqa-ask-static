+++
type = "question"
title = "Monitor traffic of connected iphone to my wireless network"
description = '''Hi, I want to monitor traffic coming from iphone connected to my wireless network. My Modem+wireless router is Motorola SBG901 and I am on windows laptop. When I run wireshark, It just shows interfaces for my laptop, does not show iphone interface. Is my router capable of this type of sniffing? Can ...'''
date = "2012-05-09T20:12:00Z"
lastmod = "2012-05-09T21:07:00Z"
weight = 10861
keywords = [ "wireless", "wireshark" ]
aliases = [ "/questions/10861" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor traffic of connected iphone to my wireless network](/questions/10861/monitor-traffic-of-connected-iphone-to-my-wireless-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10861-score" class="post-score" title="current number of votes">0</div><span id="post-10861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I want to monitor traffic coming from iphone connected to my wireless network. My Modem+wireless router is Motorola SBG901 and I am on windows laptop. When I run wireshark, It just shows interfaces for my laptop, does not show iphone interface. Is my router capable of this type of sniffing? Can someone please tell me how to do this, without requiring any configuration on iphone?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '12, 20:12</strong></p><img src="https://secure.gravatar.com/avatar/9c58e736b6d1ae113daf4a1b6e350336?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lex&#39;s gravatar image" /><p><span>Lex</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lex has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 May '12, 20:15</strong> </span></p></div></div><div id="comments-container-10861" class="comments-container"></div><div id="comment-tools-10861" class="comment-tools"></div><div class="clear"></div><div id="comment-10861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10863"></span>

<div id="answer-container-10863" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10863-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10863-score" class="post-score" title="current number of votes">0</div><span id="post-10863-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you were trying to do this with just the wireless interface of your laptop you will have a problem. You will need an AirPcap adapter to pull that off. Otherwise You won't see the iphone traffic unless you are 'tapped' in to the AP or in between the handhoff from the router to the rest of your network with a mirrored switch (eg Dual-Comm) or hub. They are easy enough to spot via the MAC resolutions - I see them all the time.</p><p>Eric</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '12, 20:40</strong></p><img src="https://secure.gravatar.com/avatar/f797bdc41d990dca073837114e048b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EricKnaus&#39;s gravatar image" /><p><span>EricKnaus</span><br />
<span class="score" title="46 reputation points">46</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EricKnaus has no accepted answers">0%</span></p></div></div><div id="comments-container-10863" class="comments-container"><span id="10866"></span><div id="comment-10866" class="comment"><div id="post-10866-score" class="comment-score"></div><div class="comment-text"><p>Can I simply monitor traffic leaving my router? I have admin access to it.</p></div><div id="comment-10866-info" class="comment-info"><span class="comment-age">(09 May '12, 21:07)</span> <span class="comment-user userinfo">Lex</span></div></div></div><div id="comment-tools-10863" class="comment-tools"></div><div class="clear"></div><div id="comment-10863-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

