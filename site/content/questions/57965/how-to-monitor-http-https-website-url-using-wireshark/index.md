+++
type = "question"
title = "How to monitor HTTP HTTPS website URL using wireshark ?"
description = '''Hello, as an example, &quot;A&quot; is using a password encrypted enabled &quot;WIFI network A&quot; to surf the internet. I wish to find out where &quot;A&quot; is surfing to. This includes all HTTP and HTTPS traffic. specifically the website address as an example : www.oakley.com or www.gmail.com etc. Can this be done with wir...'''
date = "2016-12-08T11:25:00Z"
lastmod = "2016-12-08T11:43:00Z"
weight = 57965
keywords = [ "http", "https" ]
aliases = [ "/questions/57965" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to monitor HTTP HTTPS website URL using wireshark ?](/questions/57965/how-to-monitor-http-https-website-url-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57965-score" class="post-score" title="current number of votes">0</div><span id="post-57965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>as an example, "A" is using a password encrypted enabled "WIFI network A" to surf the internet.</p><p>I wish to find out where "A" is surfing to. This includes all HTTP and HTTPS traffic. specifically the website address as an example : www.oakley.com or www.gmail.com etc.</p><p>Can this be done with wireshark?</p><p>Can you please let me know how?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '16, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/dc0d842a48d224b8fbc7e28514a80909?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cat&#39;s gravatar image" /><p><span>Cat</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cat has no accepted answers">0%</span></p></div></div><div id="comments-container-57965" class="comments-container"></div><div id="comment-tools-57965" class="comment-tools"></div><div class="clear"></div><div id="comment-57965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57966"></span>

<div id="answer-container-57966" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57966-score" class="post-score" title="current number of votes">0</div><span id="post-57966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No it can't be done, unless you know the password. Because the wifi is encrypted.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '16, 11:43</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-57966" class="comments-container"></div><div id="comment-tools-57966" class="comment-tools"></div><div class="clear"></div><div id="comment-57966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

