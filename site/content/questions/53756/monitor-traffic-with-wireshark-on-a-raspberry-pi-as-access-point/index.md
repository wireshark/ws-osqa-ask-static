+++
type = "question"
title = "Monitor traffic with Wireshark on a Raspberry Pi as Access Point"
description = '''I have a Raspberry Pi working as a wireless access point for my own home network (not connected to the internet) running on my wlan0 interface. I have Wireshark running on my RP AP and i would like to use it to monitor all traffic going through it. If i use Wireshark to monitor the wlan0 interface i...'''
date = "2016-06-30T13:46:00Z"
lastmod = "2016-06-30T13:46:00Z"
weight = 53756
keywords = [ "capture", "traffic-analysis", "accesspoint" ]
aliases = [ "/questions/53756" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor traffic with Wireshark on a Raspberry Pi as Access Point](/questions/53756/monitor-traffic-with-wireshark-on-a-raspberry-pi-as-access-point)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53756-score" class="post-score" title="current number of votes">0</div><span id="post-53756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a Raspberry Pi working as a wireless access point for my own home network (not connected to the internet) running on my wlan0 interface. I have Wireshark running on my RP AP and i would like to use it to monitor all traffic going through it. If i use Wireshark to monitor the wlan0 interface i can see all traffic directed to the AP (e.g. DHCP requests). I cannot, however, see any of the packets being exchanged between the different clients connected to this AP. Note that i'm already in promiscuous mode.</p><p>Oddly enough (or not), as soon as i switch-off one of the clients connected to the AP i start seeing all the traffic directed to this client.</p><p>What am i missing? Is there any way to monitor the traffic that goes through the AP?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-accesspoint" rel="tag" title="see questions tagged &#39;accesspoint&#39;">accesspoint</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '16, 13:46</strong></p><img src="https://secure.gravatar.com/avatar/c7ca49c88eff92c708bc3487cd117e36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tns&#39;s gravatar image" /><p><span>tns</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tns has no accepted answers">0%</span></p></div></div><div id="comments-container-53756" class="comments-container"></div><div id="comment-tools-53756" class="comment-tools"></div><div class="clear"></div><div id="comment-53756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

