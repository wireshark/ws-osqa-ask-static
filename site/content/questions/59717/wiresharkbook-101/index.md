+++
type = "question"
title = "Wiresharkbook 101"
description = '''I recently bought &quot;Wireshark 101&quot; On page 38 it states that trace files for the book can be found in www.wiresharkbook.com  I search the site with no success Please help'''
date = "2017-02-27T13:52:00Z"
lastmod = "2017-02-27T14:17:00Z"
weight = 59717
keywords = [ "wireshar101trace" ]
aliases = [ "/questions/59717" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wiresharkbook 101](/questions/59717/wiresharkbook-101)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59717-score" class="post-score" title="current number of votes">0</div><span id="post-59717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently bought "Wireshark 101"</p><p>On page 38 it states that trace files for the book can be found in www.wiresharkbook.com</p><p>I search the site with no success</p><p>Please help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshar101trace" rel="tag" title="see questions tagged &#39;wireshar101trace&#39;">wireshar101trace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '17, 13:52</strong></p><img src="https://secure.gravatar.com/avatar/8597e1a15b5804270a0c1260c725951a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pr2jd2b&#39;s gravatar image" /><p><span>pr2jd2b</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pr2jd2b has no accepted answers">0%</span></p></div></div><div id="comments-container-59717" class="comments-container"></div><div id="comment-tools-59717" class="comment-tools"></div><div class="clear"></div><div id="comment-59717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59719"></span>

<div id="answer-container-59719" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59719-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59719-score" class="post-score" title="current number of votes">0</div><span id="post-59719-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just checked myself and I believe the files are in the "Book Supplements" section. Then choose "Wireshark 101: Essential Skills" and the download link is on the left... good luck!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '17, 14:17</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-59719" class="comments-container"></div><div id="comment-tools-59719" class="comment-tools"></div><div class="clear"></div><div id="comment-59719-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

