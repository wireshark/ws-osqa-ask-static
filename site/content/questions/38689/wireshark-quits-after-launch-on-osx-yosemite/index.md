+++
type = "question"
title = "WireShark quits after launch on OSX yosemite"
description = '''After installing Wireshark I got the dialog on where is X11. I clicked the wrong app and now it tries to start Xcode and then quits. Or it just starts and quits. What can I do to get it to re ask where X11 is. I have uninstalled and reinstalled using Zap. I have also tried that link to can&#x27;t find X1...'''
date = "2014-12-23T21:17:00Z"
lastmod = "2014-12-27T08:37:00Z"
weight = 38689
keywords = [ "yosemite" ]
aliases = [ "/questions/38689" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark quits after launch on OSX yosemite](/questions/38689/wireshark-quits-after-launch-on-osx-yosemite)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38689-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38689-score" class="post-score" title="current number of votes">0</div><span id="post-38689-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installing Wireshark I got the dialog on where is X11. I clicked the wrong app and now it tries to start Xcode and then quits. Or it just starts and quits. What can I do to get it to re ask where X11 is. I have uninstalled and reinstalled using Zap. I have also tried that link to can't find X11 and still no luck.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-yosemite" rel="tag" title="see questions tagged &#39;yosemite&#39;">yosemite</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Dec '14, 21:17</strong></p><img src="https://secure.gravatar.com/avatar/c5031e66d3c5ab1327068851957d6546?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="klaw&#39;s gravatar image" /><p><span>klaw</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="klaw has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Dec '14, 09:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-38689" class="comments-container"></div><div id="comment-tools-38689" class="comment-tools"></div><div class="clear"></div><div id="comment-38689-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38735"></span>

<div id="answer-container-38735" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38735-score" class="post-score" title="current number of votes">0</div><span id="post-38735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the answers to similar questions:</p><blockquote><p><a href="http://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx">http://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx</a> <a href="http://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion">http://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Dec '14, 08:37</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-38735" class="comments-container"></div><div id="comment-tools-38735" class="comment-tools"></div><div class="clear"></div><div id="comment-38735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

