+++
type = "question"
title = "nothing to capture"
description = '''why does it say nothing to capture? how do I turn on a driver?'''
date = "2012-02-18T09:02:00Z"
lastmod = "2012-02-20T13:16:00Z"
weight = 9129
keywords = [ "driver" ]
aliases = [ "/questions/9129" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [nothing to capture](/questions/9129/nothing-to-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9129-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9129-score" class="post-score" title="current number of votes">0</div><span id="post-9129-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>why does it say nothing to capture? how do I turn on a driver?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-driver" rel="tag" title="see questions tagged &#39;driver&#39;">driver</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '12, 09:02</strong></p><img src="https://secure.gravatar.com/avatar/43af32183ddf8f670c56faf9bd34aa12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Justme&#39;s gravatar image" /><p><span>Justme</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Justme has no accepted answers">0%</span></p></div></div><div id="comments-container-9129" class="comments-container"><span id="9148"></span><div id="comment-9148" class="comment"><div id="post-9148-score" class="comment-score"></div><div class="comment-text"><p>It might help if you add some information. What kind of OS are you using, which version of wireshark? Which steps have you taken when the error message occurred? And what is the <em>exact</em> error message?</p></div><div id="comment-9148-info" class="comment-info"><span class="comment-age">(20 Feb '12, 13:16)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-9129" class="comment-tools"></div><div class="clear"></div><div id="comment-9129-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

