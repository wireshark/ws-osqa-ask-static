+++
type = "question"
title = "Disabling generated field"
description = '''Hello,  is it possible to disable generated fields in packet details windows? E.g. disabling Response To, Response time, Timestamp relative in ICMP echo request. I would like to view only &quot;pure&quot; header without automatic generated fields. Thanks'''
date = "2013-06-05T07:18:00Z"
lastmod = "2013-06-06T02:26:00Z"
weight = 21764
keywords = [ "field", "generated" ]
aliases = [ "/questions/21764" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Disabling generated field](/questions/21764/disabling-generated-field)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21764-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21764-score" class="post-score" title="current number of votes">0</div><span id="post-21764-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, is it possible to disable generated fields in packet details windows? E.g. disabling Response To, Response time, Timestamp relative in ICMP echo request. I would like to view only "pure" header without automatic generated fields. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-field" rel="tag" title="see questions tagged &#39;field&#39;">field</span> <span class="post-tag tag-link-generated" rel="tag" title="see questions tagged &#39;generated&#39;">generated</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '13, 07:18</strong></p><img src="https://secure.gravatar.com/avatar/c27b47f694df2eeeb65845c9dbaa8920?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="couker&#39;s gravatar image" /><p><span>couker</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="couker has no accepted answers">0%</span></p></div></div><div id="comments-container-21764" class="comments-container"></div><div id="comment-tools-21764" class="comment-tools"></div><div class="clear"></div><div id="comment-21764-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21770"></span>

<div id="answer-container-21770" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21770-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21770-score" class="post-score" title="current number of votes">1</div><span id="post-21770-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="couker has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, it's not.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jun '13, 09:59</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-21770" class="comments-container"><span id="21772"></span><div id="comment-21772" class="comment"><div id="post-21772-score" class="comment-score">1</div><div class="comment-text"><p>But you can add fields of your choice to the packet list pane by adding icmp pure header fields as columns and deleting the default columns.In that way i guess you can achieve your purpose.</p></div><div id="comment-21772-info" class="comment-info"><span class="comment-age">(05 Jun '13, 10:35)</span> <span class="comment-user userinfo">krishnayeddula</span></div></div><span id="21773"></span><div id="comment-21773" class="comment"><div id="post-21773-score" class="comment-score"></div><div class="comment-text"><p>Not if his purpose is to change the packet details rather than the packet list.</p></div><div id="comment-21773-info" class="comment-info"><span class="comment-age">(05 Jun '13, 10:37)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="21790"></span><div id="comment-21790" class="comment"><div id="post-21790-score" class="comment-score"></div><div class="comment-text"><p>Thanks guys, apply as column is also ok. Thanks a lot.</p></div><div id="comment-21790-info" class="comment-info"><span class="comment-age">(06 Jun '13, 02:26)</span> <span class="comment-user userinfo">couker</span></div></div></div><div id="comment-tools-21770" class="comment-tools"></div><div class="clear"></div><div id="comment-21770-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

