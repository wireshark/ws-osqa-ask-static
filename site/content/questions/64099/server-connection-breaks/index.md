+++
type = "question"
title = "Server connection breaks"
description = '''Good morning everybody, We have a strange problem at server side , when we try to open our web application from Public IP ,connection break and web page is not completly loaded . '''
date = "2017-10-23T02:39:00Z"
lastmod = "2017-10-23T03:12:00Z"
weight = 64099
keywords = [ "connection" ]
aliases = [ "/questions/64099" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Server connection breaks](/questions/64099/server-connection-breaks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64099-score" class="post-score" title="current number of votes">0</div><span id="post-64099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good morning everybody, We have a strange problem at server side , when we try to open our web application from Public IP ,connection break and web page is not completly loaded . <img src="https://osqa-ask.wireshark.org/upfiles/satim.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection" rel="tag" title="see questions tagged &#39;connection&#39;">connection</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '17, 02:39</strong></p><img src="https://secure.gravatar.com/avatar/940cc74d60837baff7b661d28d67934c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gaddour87&#39;s gravatar image" /><p><span>gaddour87</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gaddour87 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-64099" class="comments-container"><span id="64100"></span><div id="comment-64100" class="comment"><div id="post-64100-score" class="comment-score"></div><div class="comment-text"><p>Sorry, but that capture quality is too bad to diagnose anything. If I see this correctly you're capturing this on WiFi and the machine you're using to test. This is not going to work at all, as you can easily see - you only have one side of the TCP conversation in your capture.</p><p>Please do a proper capture (using a dedicated capture machine, on a physical link). It would help if you can provide the PCAP of that proper capture, which is much better than a screenshot. Using this guide if you need to sanitize some details first:</p><p><a href="https://blog.packet-foo.com/2016/11/the-wireshark-qa-trace-file-sharing-tutorial/">https://blog.packet-foo.com/2016/11/the-wireshark-qa-trace-file-sharing-tutorial/</a></p></div><div id="comment-64100-info" class="comment-info"><span class="comment-age">(23 Oct '17, 03:12)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-64099" class="comment-tools"></div><div class="clear"></div><div id="comment-64099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

