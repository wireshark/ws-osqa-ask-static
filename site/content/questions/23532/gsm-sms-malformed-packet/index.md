+++
type = "question"
title = "GSM SMS [Malformed Packet]"
description = '''Using Wireshark 1.10.1, the GSM SMS dissector is reporting malformed packet  at [GSM Mobile Application] level. A few versions earlier, does not exhibit this issue. Any idea how to resolve it ? Thanks.'''
date = "2013-08-03T22:20:00Z"
lastmod = "2013-08-04T01:28:00Z"
weight = 23532
keywords = [ "sms" ]
aliases = [ "/questions/23532" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GSM SMS \[Malformed Packet\]](/questions/23532/gsm-sms-malformed-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23532-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23532-score" class="post-score" title="current number of votes">0</div><span id="post-23532-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using Wireshark 1.10.1, the GSM SMS dissector is reporting malformed packet at [GSM Mobile Application] level. A few versions earlier, does not exhibit this issue. Any idea how to resolve it ?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sms" rel="tag" title="see questions tagged &#39;sms&#39;">sms</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '13, 22:20</strong></p><img src="https://secure.gravatar.com/avatar/9cb2aa2add2f24bb776a181b51c9c1c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Knight&#39;s gravatar image" /><p><span>Knight</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Knight has no accepted answers">0%</span></p></div></div><div id="comments-container-23532" class="comments-container"></div><div id="comment-tools-23532" class="comment-tools"></div><div class="clear"></div><div id="comment-23532-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23533"></span>

<div id="answer-container-23533" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23533-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23533-score" class="post-score" title="current number of votes">0</div><span id="post-23533-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>Please fill a bug on <a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a> with the pcap file attached so that we can have a look at the GSM MAP packet and check what's wrong.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Aug '13, 01:28</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-23533" class="comments-container"></div><div id="comment-tools-23533" class="comment-tools"></div><div class="clear"></div><div id="comment-23533-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

