+++
type = "question"
title = "Export packet details pane data"
description = '''Hello, I noticed we can export packet list pane data to a csv file but not packet details pane data? Is there an application to do this?'''
date = "2017-10-12T13:11:00Z"
lastmod = "2017-10-16T10:42:00Z"
weight = 63852
keywords = [ "export" ]
aliases = [ "/questions/63852" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Export packet details pane data](/questions/63852/export-packet-details-pane-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63852-score" class="post-score" title="current number of votes">0</div><span id="post-63852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I noticed we can export packet list pane data to a csv file but not packet details pane data?</p><p>Is there an application to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '17, 13:11</strong></p><img src="https://secure.gravatar.com/avatar/cbec9feb93e0ca2fda38c9b7fee6254a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mjperry68&#39;s gravatar image" /><p><span>mjperry68</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mjperry68 has no accepted answers">0%</span></p></div></div><div id="comments-container-63852" class="comments-container"></div><div id="comment-tools-63852" class="comment-tools"></div><div class="clear"></div><div id="comment-63852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63853"></span>

<div id="answer-container-63853" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63853-score" class="post-score" title="current number of votes">0</div><span id="post-63853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can do that with Wireshark. Use File -&gt; Export Packet Dissections -&gt; As Plain Text</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '17, 13:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-63853" class="comments-container"><span id="63856"></span><div id="comment-63856" class="comment"><div id="post-63856-score" class="comment-score"></div><div class="comment-text"><p>I actually did that earlier but the file was too large for Notepad and unable to load in Wordpad.</p></div><div id="comment-63856-info" class="comment-info"><span class="comment-age">(12 Oct '17, 13:42)</span> <span class="comment-user userinfo">mjperry68</span></div></div><span id="63935"></span><div id="comment-63935" class="comment"><div id="post-63935-score" class="comment-score"></div><div class="comment-text"><p>Use a text editor that can handle big files, e.g. NotePad++ or UltraEdit.</p></div><div id="comment-63935-info" class="comment-info"><span class="comment-age">(16 Oct '17, 10:42)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-63853" class="comment-tools"></div><div class="clear"></div><div id="comment-63853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

