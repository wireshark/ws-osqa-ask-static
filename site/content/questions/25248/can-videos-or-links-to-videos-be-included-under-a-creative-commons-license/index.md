+++
type = "question"
title = "Can videos (or links to videos) be included under a Creative Commons license?"
description = '''We&#x27;re developing cybersecurity-related courseware that will be distributed under a Creative Commons license for non-commercial educational institutions. I&#x27;d like to include some of the videos (or links/references to those videos) but I wanted to make sure that would be clear to include the videos or...'''
date = "2013-09-25T16:17:00Z"
lastmod = "2013-09-26T06:29:00Z"
weight = 25248
keywords = [ "cc", "education" ]
aliases = [ "/questions/25248" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can videos (or links to videos) be included under a Creative Commons license?](/questions/25248/can-videos-or-links-to-videos-be-included-under-a-creative-commons-license)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25248-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25248-score" class="post-score" title="current number of votes">0</div><span id="post-25248-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We're developing cybersecurity-related courseware that will be distributed under a Creative Commons license for non-commercial educational institutions. I'd like to include some of the videos (or links/references to those videos) but I wanted to make sure that would be clear to include the videos or references to the videos under a Creative Commons license.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cc" rel="tag" title="see questions tagged &#39;cc&#39;">cc</span> <span class="post-tag tag-link-education" rel="tag" title="see questions tagged &#39;education&#39;">education</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '13, 16:17</strong></p><img src="https://secure.gravatar.com/avatar/3d568d903d205e5fdb7b14ae8a2d6f2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CyberTeach&#39;s gravatar image" /><p><span>CyberTeach</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CyberTeach has no accepted answers">0%</span></p></div></div><div id="comments-container-25248" class="comments-container"><span id="25275"></span><div id="comment-25275" class="comment"><div id="post-25275-score" class="comment-score"></div><div class="comment-text"><p>Sorry, what videos or links to videos are you talking about? Could you be more specific? (I'm guessing you're talking about the Sharkfest videos?)</p></div><div id="comment-25275-info" class="comment-info"><span class="comment-age">(26 Sep '13, 06:14)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="25276"></span><div id="comment-25276" class="comment"><div id="post-25276-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I should have been more specific - the Hands On with Wireshark, Intro to Wireshark, Solving Network Mysteries, etc.</p></div><div id="comment-25276-info" class="comment-info"><span class="comment-age">(26 Sep '13, 06:18)</span> <span class="comment-user userinfo">CyberTeach</span></div></div><span id="25278"></span><div id="comment-25278" class="comment"><div id="post-25278-score" class="comment-score"></div><div class="comment-text"><p>Hm.. That's a bunch of different people and companies. I guess you should contact the authors directly.</p><p>BTW: Can you please post a link to your courseware? Sounds interesting.</p></div><div id="comment-25278-info" class="comment-info"><span class="comment-age">(26 Sep '13, 06:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-25248" class="comment-tools"></div><div class="clear"></div><div id="comment-25248-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

