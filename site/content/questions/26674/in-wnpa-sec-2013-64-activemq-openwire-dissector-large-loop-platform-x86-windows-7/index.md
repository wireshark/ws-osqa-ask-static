+++
type = "question"
title = "In  wnpa-sec-2013-64 · ActiveMQ OpenWire dissector large loop :platform :x86 windows 7."
description = '''In wnpa-sec-2013-64 · ActiveMQ OpenWire dissector large loop :platform :x86 windows 7. So ,i would like to get information about is x86 related to 32-bit OS or 32 bit and 64 bit OS. -&amp;gt;does x86 relate to architecthure?'''
date = "2013-11-05T00:55:00Z"
lastmod = "2013-11-05T16:53:00Z"
weight = 26674
keywords = [ "wnpa-sec-2013-64" ]
aliases = [ "/questions/26674" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [In wnpa-sec-2013-64 · ActiveMQ OpenWire dissector large loop :platform :x86 windows 7.](/questions/26674/in-wnpa-sec-2013-64-activemq-openwire-dissector-large-loop-platform-x86-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26674-score" class="post-score" title="current number of votes">0</div><span id="post-26674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In wnpa-sec-2013-64 · ActiveMQ OpenWire dissector large loop :platform :x86 windows 7.</p><p>So ,i would like to get information about is x86 related to 32-bit OS or 32 bit and 64 bit OS. -&gt;does x86 relate to architecthure?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wnpa-sec-2013-64" rel="tag" title="see questions tagged &#39;wnpa-sec-2013-64&#39;">wnpa-sec-2013-64</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '13, 00:55</strong></p><img src="https://secure.gravatar.com/avatar/33a159105e6f0fd7fad2837c62f9fc32?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ganesh&#39;s gravatar image" /><p><span>ganesh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ganesh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Nov '13, 00:56</strong> </span></p></div></div><div id="comments-container-26674" class="comments-container"></div><div id="comment-tools-26674" class="comment-tools"></div><div class="clear"></div><div id="comment-26674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26690"></span>

<div id="answer-container-26690" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26690-score" class="post-score" title="current number of votes">2</div><span id="post-26690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>"x86" is a term that's used for a couple of purposes:</p><ol><li>It can refer to the 16-bit, 32-bit, and 64-bit instruction set architectures for Intel's main line of processors and the compatible processors from companies such as AMD, Via, etc..</li><li>It can refer to the 32-bit version, with "AMD64", "x86-64", or "x64" used to refer to the 64-bit version.</li></ol><p>In this case, it happens to refer to the second meaning. However, <a href="https://www.wireshark.org/security/wnpa-sec-2013-64.html">the Wireshark page about that vulnerability</a> doesn't mention the platform, because the vulnerability happens to apply to <strong><em>ALL</em></strong> platforms; that page links to <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=9248">Wireshark bug 9248</a>, which is the Wireshark bug database for that vulnerability, and the person who reported that bug <em>happened</em> to be running the 32-bit version of Wireshark on WIndows, so that bug gives the platform as x86 Windows 7, but it's also a bug on x64 (64-bit) Windows 7), and 32-bit and 64-bit Windows XP, and 32-bit and 64-bit Windows Vista, and 32-bit and 64-bit OS X, and Linux on 32-bit and 64-bit Intel and ARM and System/3x0 and PowerPC and SPARC and MIPS and....</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Nov '13, 16:53</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-26690" class="comments-container"></div><div id="comment-tools-26690" class="comment-tools"></div><div class="clear"></div><div id="comment-26690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

