+++
type = "question"
title = "Check print job size"
description = '''Hi, i am pretty new to using wireshark. The requirement is to check the compression of the print job size when sent from a Terminal Server to a client. I understand that the data can be filtered on basis of IP address and the port &amp;amp; analyze the data stream. Is there a way to get the exact print ...'''
date = "2016-09-07T04:09:00Z"
lastmod = "2016-09-07T04:09:00Z"
weight = 55368
keywords = [ "printer" ]
aliases = [ "/questions/55368" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Check print job size](/questions/55368/check-print-job-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55368-score" class="post-score" title="current number of votes">0</div><span id="post-55368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i am pretty new to using wireshark. The requirement is to check the compression of the print job size when sent from a Terminal Server to a client.</p><p>I understand that the data can be filtered on basis of IP address and the port &amp; analyze the data stream. Is there a way to get the exact print job size from the data collected using Wireshark?</p><p>Any help would be greatly appreciated!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-printer" rel="tag" title="see questions tagged &#39;printer&#39;">printer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '16, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/10f3b91bcfbd4e17dce084cd0248677a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DSingh13&#39;s gravatar image" /><p><span>DSingh13</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DSingh13 has no accepted answers">0%</span></p></div></div><div id="comments-container-55368" class="comments-container"></div><div id="comment-tools-55368" class="comment-tools"></div><div class="clear"></div><div id="comment-55368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

