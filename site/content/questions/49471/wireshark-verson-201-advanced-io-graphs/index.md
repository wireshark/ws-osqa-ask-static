+++
type = "question"
title = "Wireshark Verson 2.0.1 Advanced I/O Graphs"
description = '''What Happened to the I/O Graph Advanced Features?  Thanks in advance...Fritzbied'''
date = "2016-01-22T13:48:00Z"
lastmod = "2016-01-22T14:23:00Z"
weight = 49471
keywords = [ "graphs", "io", "advanced" ]
aliases = [ "/questions/49471" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Verson 2.0.1 Advanced I/O Graphs](/questions/49471/wireshark-verson-201-advanced-io-graphs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49471-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49471-score" class="post-score" title="current number of votes">0</div><span id="post-49471-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What Happened to the I/O Graph Advanced Features?</p><p>Thanks in advance...Fritzbied</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span> <span class="post-tag tag-link-io" rel="tag" title="see questions tagged &#39;io&#39;">io</span> <span class="post-tag tag-link-advanced" rel="tag" title="see questions tagged &#39;advanced&#39;">advanced</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '16, 13:48</strong></p><img src="https://secure.gravatar.com/avatar/6e0fda2a5c8d02515d88f004b33a9998?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fritzbied&#39;s gravatar image" /><p><span>fritzbied</span><br />
<span class="score" title="6 reputation points">6</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fritzbied has no accepted answers">0%</span></p></div></div><div id="comments-container-49471" class="comments-container"></div><div id="comment-tools-49471" class="comment-tools"></div><div class="clear"></div><div id="comment-49471-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49472"></span>

<div id="answer-container-49472" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49472-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49472-score" class="post-score" title="current number of votes">1</div><span id="post-49472-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>They got incorporated into the basic I/O graph. For example, if you double click on the item in the "Y Axis" column for one of the graph lines, you'll see all the calculations that you used to have to go to the Advanced I/O graph for. The field that you want to perform the calculation on goes in the "Y Field" column. There is now only one I/O Graph.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '16, 14:23</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-49472" class="comments-container"></div><div id="comment-tools-49472" class="comment-tools"></div><div class="clear"></div><div id="comment-49472-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

