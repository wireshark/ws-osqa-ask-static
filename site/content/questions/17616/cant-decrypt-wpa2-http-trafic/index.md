+++
type = "question"
title = "Can&#x27;t decrypt WPA2 http trafic"
description = '''Hello, I use a WPA2-PSK encryption, I have Wireshark 1.6.12 running on Fedora 17. The fact is that when I try to capture packets directly with wireshark in promiscuous mode, after obtaining the 4-way handshake (the 1/4 handshake is always a malformed packet): I have no decrypted HTTP trafic. Thank y...'''
date = "2013-01-11T11:37:00Z"
lastmod = "2013-01-11T11:37:00Z"
weight = 17616
keywords = [ "wireshark" ]
aliases = [ "/questions/17616" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't decrypt WPA2 http trafic](/questions/17616/cant-decrypt-wpa2-http-trafic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17616-score" class="post-score" title="current number of votes">0</div><span id="post-17616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I use a WPA2-PSK encryption, I have Wireshark 1.6.12 running on Fedora 17. The fact is that when I try to capture packets directly with wireshark in promiscuous mode, after obtaining the 4-way handshake (the 1/4 handshake is always a malformed packet): I have no decrypted HTTP trafic.</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '13, 11:37</strong></p><img src="https://secure.gravatar.com/avatar/5f0c1007bb9cf68c32a8c148fb250d7a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nole&#39;s gravatar image" /><p><span>nole</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nole has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jan '13, 05:08</strong> </span></p></div></div><div id="comments-container-17616" class="comments-container"></div><div id="comment-tools-17616" class="comment-tools"></div><div class="clear"></div><div id="comment-17616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

