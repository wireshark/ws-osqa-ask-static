+++
type = "question"
title = "Where is the link to Hansang&#x27;s troubleshooting book - presentation #2?"
description = '''I have the link to Hansang&#x27;s first webinar: https://www.youtube.com/watch?v=3aXSCIWty7o I was also able to attend and view the 3rd webinar online, but I can&#x27;t find a link to the 2nd one.  Does anyone else have a link to the video for the 2nd webinar? Thanks!'''
date = "2016-08-19T12:15:00Z"
lastmod = "2016-08-19T12:15:00Z"
weight = 54992
keywords = [ "book", "troubleshooting" ]
aliases = [ "/questions/54992" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Where is the link to Hansang's troubleshooting book - presentation \#2?](/questions/54992/where-is-the-link-to-hansangs-troubleshooting-book-presentation-2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54992-score" class="post-score" title="current number of votes">0</div><span id="post-54992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have the link to Hansang's first webinar: <a href="https://www.youtube.com/watch?v=3aXSCIWty7o">https://www.youtube.com/watch?v=3aXSCIWty7o</a> I was also able to attend and view the 3rd webinar online, but I can't find a link to the 2nd one.<br />
</p><p>Does anyone else have a link to the video for the 2nd webinar?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-book" rel="tag" title="see questions tagged &#39;book&#39;">book</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Aug '16, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/ff0a86a720311c5bec05905c6752c144?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="crrimson&#39;s gravatar image" /><p><span>crrimson</span><br />
<span class="score" title="15 reputation points">15</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="crrimson has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-54992" class="comments-container"></div><div id="comment-tools-54992" class="comment-tools"></div><div class="clear"></div><div id="comment-54992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

