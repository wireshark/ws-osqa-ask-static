+++
type = "question"
title = "Not able to capture Data Packet using AirPCap"
description = '''Hi, I am trying to capture data packet using AirPcap Nx dongle but no data packets are seen over wireshark. Control and Management frames are being captured but only data packets are not getting capture. Please Help. I searched many portals but didn&#x27;t find solution yet Setup Info: Wireshark v1.12.3 ...'''
date = "2015-02-06T04:02:00Z"
lastmod = "2015-02-19T10:52:00Z"
weight = 39680
keywords = [ "aircap", "troubleshooting" ]
aliases = [ "/questions/39680" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Not able to capture Data Packet using AirPCap](/questions/39680/not-able-to-capture-data-packet-using-airpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39680-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39680-score" class="post-score" title="current number of votes">0</div><span id="post-39680-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to capture data packet using AirPcap Nx dongle but no data packets are seen over wireshark. Control and Management frames are being captured but only data packets are not getting capture. Please Help. I searched many portals but didn't find solution yet</p><pre><code>Setup Info:
Wireshark v1.12.3 (64-bit, 
Wireless Dongle used : AirPcap Nx, 
Driver for AirPCap v4.1.1.1.1838, 
WinPcap version : v4.1.0.2980, 
Laptop OS: 64 bit Window 7 Enterprise.</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-aircap" rel="tag" title="see questions tagged &#39;aircap&#39;">aircap</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '15, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/e6c5980cea9ced057335502c78778aca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sumit7150&#39;s gravatar image" /><p><span>sumit7150</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sumit7150 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Feb '15, 06:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-39680" class="comments-container"></div><div id="comment-tools-39680" class="comment-tools"></div><div class="clear"></div><div id="comment-39680-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="39724"></span>

<div id="answer-container-39724" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39724-score" class="post-score" title="current number of votes">0</div><span id="post-39724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I searched many portals but didn't find solution yet</p></blockquote><p>Please contact the vendor support of that device. They should be able to help you, shouldn't they?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '15, 15:40</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Feb '15, 16:38</strong> </span></p></div></div><div id="comments-container-39724" class="comments-container"></div><div id="comment-tools-39724" class="comment-tools"></div><div class="clear"></div><div id="comment-39724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="39958"></span>

<div id="answer-container-39958" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39958-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39958-score" class="post-score" title="current number of votes">0</div><span id="post-39958-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>One reason why AirPcap Nx dongle <strong>MAY</strong> miss packets is if your network has Short Guard Interval enabled. Try with that disabled &amp; see if it helps?</p><p>If that doesn't help, you may want to disable 11n and see if it helps. Even after disabling 11n, if it doesn't display data packets, then it's definitely faulty!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '15, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/5c59321a66976ba615e1a50b46a4d209?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ramprasad&#39;s gravatar image" /><p><span>Ramprasad</span><br />
<span class="score" title="20 reputation points">20</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ramprasad has no accepted answers">0%</span></p></div></div><div id="comments-container-39958" class="comments-container"></div><div id="comment-tools-39958" class="comment-tools"></div><div class="clear"></div><div id="comment-39958-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

