+++
type = "question"
title = "RadioTap header in wireless packet capture"
description = '''Hi folks; I bought the AirPcap device and am sniffing away. I am curious about some of the fields in the Radiotap header. Can someone explain the SSI Signal/Noise values in the packet capture. Are those fields/values inserted by  AirPcap when upon packet capture ?? or are they already in the receive...'''
date = "2012-03-13T11:44:00Z"
lastmod = "2012-03-13T19:42:00Z"
weight = 9518
keywords = [ "wireless" ]
aliases = [ "/questions/9518" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [RadioTap header in wireless packet capture](/questions/9518/radiotap-header-in-wireless-packet-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9518-score" class="post-score" title="current number of votes">0</div><span id="post-9518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi folks;</p><p>I bought the AirPcap device and am sniffing away. I am curious about some of the fields in the Radiotap header. Can someone explain the SSI Signal/Noise values in the packet capture. Are those fields/values inserted by AirPcap when upon packet capture ?? or are they already in the received packet ??</p><p>I have found surprising little online documentation providing an explanation of the fields.</p><p>thanks, walter</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '12, 11:44</strong></p><img src="https://secure.gravatar.com/avatar/2b12f1f0687101a1dd8f75db884aed8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wakelt&#39;s gravatar image" /><p><span>wakelt</span><br />
<span class="score" title="13 reputation points">13</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wakelt has no accepted answers">0%</span></p></div></div><div id="comments-container-9518" class="comments-container"></div><div id="comment-tools-9518" class="comment-tools"></div><div class="clear"></div><div id="comment-9518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9528"></span>

<div id="answer-container-9528" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9528-score" class="post-score" title="current number of votes">0</div><span id="post-9528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wakelt has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To find the online documentation, you need to look at, for example, <a href="http://www.radiotap.org/defined-fields">the defined-fields page</a> at <a href="http://www.radiotap.org/">the Radiotap Website</a>.</p><p>Those fields are not in the received packets; they are inserted by the hardware and/or driver, as per the "Background" section of the Radiotap home page.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Mar '12, 19:42</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-9528" class="comments-container"></div><div id="comment-tools-9528" class="comment-tools"></div><div class="clear"></div><div id="comment-9528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

