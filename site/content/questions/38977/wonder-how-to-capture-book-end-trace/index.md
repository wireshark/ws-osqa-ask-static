+++
type = "question"
title = "Wonder how to capture book end trace"
description = '''Hello all, I am new to wireshark. I work for a telco company where we had call quality issues on one of our customer location. I have been asked to capture book end sniffer trace between two phones having the issue. So I am really confused on how to proceed that. basically my understanding is that w...'''
date = "2015-01-08T15:32:00Z"
lastmod = "2015-01-09T01:54:00Z"
weight = 38977
keywords = [ "sniffing" ]
aliases = [ "/questions/38977" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wonder how to capture book end trace](/questions/38977/wonder-how-to-capture-book-end-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38977-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38977-score" class="post-score" title="current number of votes">0</div><span id="post-38977-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all, I am new to wireshark. I work for a telco company where we had call quality issues on one of our customer location. I have been asked to capture book end sniffer trace between two phones having the issue. So I am really confused on how to proceed that. basically my understanding is that we need two mirrored ports for each of the phone and we need two laptops with wireshark and plug each laptop to the mirrored port and just run wireshark on two pc's when both ends of the phone started speaking?I really appreciate if somebody can help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '15, 15:32</strong></p><img src="https://secure.gravatar.com/avatar/935dd00ff77668aa0cadec7c4b04b115?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Keerthi%20kandaswamy&#39;s gravatar image" /><p><span>Keerthi kand...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Keerthi kandaswamy has no accepted answers">0%</span></p></div></div><div id="comments-container-38977" class="comments-container"><span id="38978"></span><div id="comment-38978" class="comment"><div id="post-38978-score" class="comment-score"></div><div class="comment-text"><p>both phones are on same location and connected to a single switch</p></div><div id="comment-38978-info" class="comment-info"><span class="comment-age">(08 Jan '15, 15:34)</span> <span class="comment-user userinfo">Keerthi kand...</span></div></div><span id="38988"></span><div id="comment-38988" class="comment"><div id="post-38988-score" class="comment-score"></div><div class="comment-text"><p>your plan sounds good. Go ahead and implement that!</p></div><div id="comment-38988-info" class="comment-info"><span class="comment-age">(09 Jan '15, 01:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-38977" class="comment-tools"></div><div class="clear"></div><div id="comment-38977-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

