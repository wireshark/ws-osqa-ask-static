+++
type = "question"
title = "[closed] How to get imformation used in dissector FP over udp/ip?"
description = '''I am using Wireshark version 1.6.4. I have some FP packets in pcap format.  Please tell me how can I decode packets as FP. Those sample fp packets are transported over UDP.  In the function &quot;dissector_fp()&quot;,it have some preference come from ATM/DCT2000,but in UDP/IP,there is no such information pass...'''
date = "2012-06-19T02:14:00Z"
lastmod = "2012-06-19T02:14:00Z"
weight = 12051
keywords = [ "fp", "attach_fp", "umts-fp" ]
aliases = [ "/questions/12051" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to get imformation used in dissector FP over udp/ip?](/questions/12051/how-to-get-imformation-used-in-dissector-fp-over-udpip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12051-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12051-score" class="post-score" title="current number of votes">0</div><span id="post-12051-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Wireshark version 1.6.4. I have some FP packets in pcap format. Please tell me how can I decode packets as FP. Those sample fp packets are transported over UDP. In the function "dissector_fp()",it have some preference come from ATM/DCT2000,but in UDP/IP,there is no such information pass to fp level. how can I get this information(used to decode the fp and above level,exmple:channel_type,is_uplink)?</p><p>Thanks and regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span> <span class="post-tag tag-link-attach_fp" rel="tag" title="see questions tagged &#39;attach_fp&#39;">attach_fp</span> <span class="post-tag tag-link-umts-fp" rel="tag" title="see questions tagged &#39;umts-fp&#39;">umts-fp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '12, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Jun '12, 02:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-12051" class="comments-container"></div><div id="comment-tools-12051" class="comment-tools"></div><div class="clear"></div><div id="comment-12051-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by helloworld 19 Jun '12, 02:29

</div>

</div>

</div>

