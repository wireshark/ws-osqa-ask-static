+++
type = "question"
title = "winPcap ECCN"
description = '''Hello,  As WinPcap is a separate piece of software that Wirshark depends on, does anyone know who I can contact concerning the Export Control Classification Number for WinPcap? Thank you, Ray'''
date = "2016-08-23T10:42:00Z"
lastmod = "2016-08-23T10:42:00Z"
weight = 55079
keywords = [ "control", "winpcap", "export", "eccn" ]
aliases = [ "/questions/55079" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [winPcap ECCN](/questions/55079/winpcap-eccn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55079-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55079-score" class="post-score" title="current number of votes">0</div><span id="post-55079-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>As WinPcap is a separate piece of software that Wirshark depends on, does anyone know who I can contact concerning the Export Control Classification Number for WinPcap?</p><p>Thank you, Ray</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-control" rel="tag" title="see questions tagged &#39;control&#39;">control</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-eccn" rel="tag" title="see questions tagged &#39;eccn&#39;">eccn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '16, 10:42</strong></p><img src="https://secure.gravatar.com/avatar/fb8b857ca6ec4081a34165848c33c588?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="regnaD%20kciN&#39;s gravatar image" /><p><span>regnaD kciN</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="regnaD kciN has no accepted answers">0%</span></p></div></div><div id="comments-container-55079" class="comments-container"></div><div id="comment-tools-55079" class="comment-tools"></div><div class="clear"></div><div id="comment-55079-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

