+++
type = "question"
title = "Wireshark based 3G Protocol Analyser"
description = '''Can anyone please post the link to download Wireshark based 3G Protocol Analyser referred to by this email message?'''
date = "2012-07-15T08:21:00Z"
lastmod = "2012-07-16T12:50:00Z"
weight = 12727
keywords = [ "3g", "analyser", "protocol" ]
aliases = [ "/questions/12727" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark based 3G Protocol Analyser](/questions/12727/wireshark-based-3g-protocol-analyser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12727-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12727-score" class="post-score" title="current number of votes">0</div><span id="post-12727-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone please post the link to download Wireshark based 3G Protocol Analyser referred to by <a href="http://www.wireshark.org/lists/wireshark-users/200912/msg00094.html">this email message</a>?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-3g" rel="tag" title="see questions tagged &#39;3g&#39;">3g</span> <span class="post-tag tag-link-analyser" rel="tag" title="see questions tagged &#39;analyser&#39;">analyser</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '12, 08:21</strong></p><img src="https://secure.gravatar.com/avatar/9eaa91ed317c2177c5bcbe55efa8b276?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vin_66_sag&#39;s gravatar image" /><p><span>vin_66_sag</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vin_66_sag has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Jul '12, 12:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-12727" class="comments-container"><span id="12735"></span><div id="comment-12735" class="comment"><div id="post-12735-score" class="comment-score"></div><div class="comment-text"><p>the link to the screenshot is no longer available. Please update!</p></div><div id="comment-12735-info" class="comment-info"><span class="comment-age">(16 Jul '12, 00:30)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12727" class="comment-tools"></div><div class="clear"></div><div id="comment-12727-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12779"></span>

<div id="answer-container-12779" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12779-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12779-score" class="post-score" title="current number of votes">0</div><span id="post-12779-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>He's referring to the analyzer mentioned in <a href="http://www.wireshark.org/lists/wireshark-users/200912/msg00094.html">the mail message I edited his question to refer to</a> - the mail message is a better reference than just the splash screen or whatever he was trying to show.</p><p>It's a version of Wireshark that somebody presumably modified; the person who sent the mail probably works for <a href="http://www.nokiasiemensnetworks.com/">Nokia Siemens Networks</a>, given the domain name in his e-mail address, and it might be a version that NSN has developed for internal use, in which case the only people who could post the link are people from NSN, and they might not be allowed to, so the only way to <em>get</em> that version would be to get a job at NSN and then ask around the company about it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jul '12, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-12779" class="comments-container"><span id="12782"></span><div id="comment-12782" class="comment"><div id="post-12782-score" class="comment-score"></div><div class="comment-text"><p>I sent him an e-mail. Let's see if he replies :-)</p></div><div id="comment-12782-info" class="comment-info"><span class="comment-age">(16 Jul '12, 12:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12779" class="comment-tools"></div><div class="clear"></div><div id="comment-12779-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

