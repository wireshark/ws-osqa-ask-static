+++
type = "question"
title = "Wireshark windows not always showing"
description = '''This happened to me the very first time I went to launch wireshark and associate with the xquartz X11. After I quit both and launched wireshark again, it started xquartz and I saw a progress bar appear and go away. If I do &quot;Show all windows&quot; on XQuartz, I can see the UI, but when I select it, it dis...'''
date = "2014-11-26T13:04:00Z"
lastmod = "2014-11-26T15:22:00Z"
weight = 38184
keywords = [ "osx" ]
aliases = [ "/questions/38184" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark windows not always showing](/questions/38184/wireshark-windows-not-always-showing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38184-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38184-score" class="post-score" title="current number of votes">0</div><span id="post-38184-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This happened to me the very first time I went to launch wireshark and associate with the xquartz X11. After I quit both and launched wireshark again, it started xquartz and I saw a progress bar appear and go away. If I do "Show all windows" on XQuartz, I can see the UI, but when I select it, it disappears again.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '14, 13:04</strong></p><img src="https://secure.gravatar.com/avatar/d6acd5bafb13813c16f2ad9b680b1208?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="James%20Hall&#39;s gravatar image" /><p><span>James Hall</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="James Hall has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>26 Nov '14, 15:20</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-38184" class="comments-container"><span id="38188"></span><div id="comment-38188" class="comment"><div id="post-38188-score" class="comment-score"></div><div class="comment-text"><p>I.e., if you do "Show all windows", you see the main Wireshark window, but if you click it, it disappears? What happens if you then do "Show all windows" in XQuartz after that?</p><p>Are there any crashes of "wireshark-bin" reported in Console?</p></div><div id="comment-38188-info" class="comment-info"><span class="comment-age">(26 Nov '14, 15:22)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-38184" class="comment-tools"></div><div class="clear"></div><div id="comment-38184-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

