+++
type = "question"
title = "No Voip calls in Telephony VoIP calls"
description = '''I just downloaded wireshark 1.6.1 and have a capture with RTP traffic. When I click on Telephony VoIP Calls there are no calls detected. But if I highlite a RTP stream and do Telephony RTP Stream Analysis the Player does decode the rtp to a VoIP call that can be listened to.  Only the Telephony VoIP...'''
date = "2012-01-24T07:13:00Z"
lastmod = "2012-01-25T02:18:00Z"
weight = 8583
keywords = [ "telephony", "voip" ]
aliases = [ "/questions/8583" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No Voip calls in Telephony VoIP calls](/questions/8583/no-voip-calls-in-telephony-voip-calls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8583-score" class="post-score" title="current number of votes">0</div><span id="post-8583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just downloaded wireshark 1.6.1 and have a capture with RTP traffic. When I click on Telephony VoIP Calls there are no calls detected. But if I highlite a RTP stream and do Telephony RTP Stream Analysis the Player does decode the rtp to a VoIP call that can be listened to. Only the Telephony VoIP Calls does not detect the RTP to VoIP. Is there a Preference setting?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telephony" rel="tag" title="see questions tagged &#39;telephony&#39;">telephony</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '12, 07:13</strong></p><img src="https://secure.gravatar.com/avatar/eac2080cc439971688061480de649f60?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rcamanj&#39;s gravatar image" /><p><span>rcamanj</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rcamanj has no accepted answers">0%</span></p></div></div><div id="comments-container-8583" class="comments-container"></div><div id="comment-tools-8583" class="comment-tools"></div><div class="clear"></div><div id="comment-8583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8600"></span>

<div id="answer-container-8600" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8600-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8600-score" class="post-score" title="current number of votes">0</div><span id="post-8600-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Telephony VoIP calls has to do with signaling (like SIP, H323, etc), not media (like RTP).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jan '12, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-8600" class="comments-container"></div><div id="comment-tools-8600" class="comment-tools"></div><div class="clear"></div><div id="comment-8600-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

