+++
type = "question"
title = "Can the portableApp work without manually installing winpcap?"
description = '''Contrary to what&#x27;s written in readme and doc, the portableapp doesn&#x27;t install/uninstall the required winpcap, at least on Windows 10. The ini file doesn&#x27;t match between the installer and the readme, so setting the path to the wpcap installer doesn&#x27;t do anything. Is the install/uninstall supposed to ...'''
date = "2017-05-15T03:00:00Z"
lastmod = "2017-05-15T03:00:00Z"
weight = 61402
keywords = [ "installer" ]
aliases = [ "/questions/61402" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can the portableApp work without manually installing winpcap?](/questions/61402/can-the-portableapp-work-without-manually-installing-winpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61402-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61402-score" class="post-score" title="current number of votes">0</div><span id="post-61402-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Contrary to what's written in readme and doc, the portableapp doesn't install/uninstall the required winpcap, at least on Windows 10. The ini file doesn't match between the installer and the readme, so setting the path to the wpcap installer doesn't do anything. Is the install/uninstall supposed to be automatic, and should the .ini and doc be updated?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '17, 03:00</strong></p><img src="https://secure.gravatar.com/avatar/febe69b72ce92e26cb8d097d57845879?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LDiCesare&#39;s gravatar image" /><p><span>LDiCesare</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LDiCesare has no accepted answers">0%</span></p></div></div><div id="comments-container-61402" class="comments-container"></div><div id="comment-tools-61402" class="comment-tools"></div><div class="clear"></div><div id="comment-61402-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

