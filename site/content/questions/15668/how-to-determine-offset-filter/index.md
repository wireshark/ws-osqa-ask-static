+++
type = "question"
title = "How to determine offset filter?"
description = '''I need to capture NLTMv1 and v2 traffic. Does this require an offset and if so how does one go about determining the offset value for a filter?'''
date = "2012-11-07T15:55:00Z"
lastmod = "2012-11-07T15:55:00Z"
weight = 15668
keywords = [ "ntlm", "offset" ]
aliases = [ "/questions/15668" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to determine offset filter?](/questions/15668/how-to-determine-offset-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15668-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15668-score" class="post-score" title="current number of votes">0</div><span id="post-15668-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to capture NLTMv1 and v2 traffic. Does this require an offset and if so how does one go about determining the offset value for a filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ntlm" rel="tag" title="see questions tagged &#39;ntlm&#39;">ntlm</span> <span class="post-tag tag-link-offset" rel="tag" title="see questions tagged &#39;offset&#39;">offset</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '12, 15:55</strong></p><img src="https://secure.gravatar.com/avatar/1c1917f4335764246c2c90d4d2e8f785?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ry6&#39;s gravatar image" /><p><span>ry6</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ry6 has no accepted answers">0%</span></p></div></div><div id="comments-container-15668" class="comments-container"></div><div id="comment-tools-15668" class="comment-tools"></div><div class="clear"></div><div id="comment-15668-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

