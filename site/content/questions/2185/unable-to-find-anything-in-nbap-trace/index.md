+++
type = "question"
title = "unable to find anything in NBAP trace"
description = '''Hi, I have captured a pcap on the IuB but when i filter for NBAP there is no message filtered. The filter field shows green but not a single message is filtered out. Is there anything special to be done while capturing. I can see in edit&amp;gt;&amp;gt; preference&amp;gt;&amp;gt; Protocol that NABP is present and a...'''
date = "2011-02-07T03:20:00Z"
lastmod = "2011-02-08T22:56:00Z"
weight = 2185
keywords = [ "nbap" ]
aliases = [ "/questions/2185" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [unable to find anything in NBAP trace](/questions/2185/unable-to-find-anything-in-nbap-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2185-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2185-score" class="post-score" title="current number of votes">0</div><span id="post-2185-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have captured a pcap on the IuB but when i filter for NBAP there is no message filtered. The filter field shows green but not a single message is filtered out. Is there anything special to be done while capturing.</p><p>I can see in edit&gt;&gt; preference&gt;&gt; Protocol that NABP is present and also the pcap shows correct IP in source and destination that of RNC and Node-B.</p><p>Pls suggest.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nbap" rel="tag" title="see questions tagged &#39;nbap&#39;">nbap</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '11, 03:20</strong></p><img src="https://secure.gravatar.com/avatar/9aba2429c4924d4ad4b9310df44f7d9b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rahul2909&#39;s gravatar image" /><p><span>Rahul2909</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rahul2909 has no accepted answers">0%</span></p></div></div><div id="comments-container-2185" class="comments-container"><span id="2217"></span><div id="comment-2217" class="comment"><div id="post-2217-score" class="comment-score">1</div><div class="comment-text"><p>What do you see? If it's NBAP over SCTP you may have to do "Decode as" "PPID" and select NBAP.</p></div><div id="comment-2217-info" class="comment-info"><span class="comment-age">(07 Feb '11, 23:18)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="2222"></span><div id="comment-2222" class="comment"><div id="post-2222-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot .. its working now.</p></div><div id="comment-2222-info" class="comment-info"><span class="comment-age">(08 Feb '11, 01:23)</span> <span class="comment-user userinfo">Rahul2909</span></div></div></div><div id="comment-tools-2185" class="comment-tools"></div><div class="clear"></div><div id="comment-2185-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2249"></span>

<div id="answer-container-2249" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2249-score" class="post-score" title="current number of votes">0</div><span id="post-2249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Move the comment to answer. If it's NBAP over SCTP you may have to do "Decode as" "PPID" and select NBAP.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Feb '11, 22:56</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-2249" class="comments-container"></div><div id="comment-tools-2249" class="comment-tools"></div><div class="clear"></div><div id="comment-2249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

