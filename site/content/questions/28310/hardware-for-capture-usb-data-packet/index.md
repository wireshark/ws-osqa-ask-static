+++
type = "question"
title = "Hardware for Capture USB data packet"
description = '''I would like to monitor the USB data traffic by using Wireshark,  I am seeking for hardware to capture the data signal and send to PC, then analyse by wireshark. What kinds of hardware are available?'''
date = "2013-12-20T23:58:00Z"
lastmod = "2013-12-24T07:38:00Z"
weight = 28310
keywords = [ "protocol", "usb" ]
aliases = [ "/questions/28310" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Hardware for Capture USB data packet](/questions/28310/hardware-for-capture-usb-data-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28310-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28310-score" class="post-score" title="current number of votes">0</div><span id="post-28310-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to monitor the USB data traffic by using Wireshark,</p><p>I am seeking for hardware to capture the data signal and send to PC, then analyse by wireshark. What kinds of hardware are available?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Dec '13, 23:58</strong></p><img src="https://secure.gravatar.com/avatar/abbbd4f0eed2d71176528e800dd17d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rabbit&#39;s gravatar image" /><p><span>rabbit</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rabbit has no accepted answers">0%</span></p></div></div><div id="comments-container-28310" class="comments-container"></div><div id="comment-tools-28310" class="comment-tools"></div><div class="clear"></div><div id="comment-28310-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="28313"></span>

<div id="answer-container-28313" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28313-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28313-score" class="post-score" title="current number of votes">0</div><span id="post-28313-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you really need special hardware? Take a look at <a href="http://wiki.wireshark.org/CaptureSetup/USB">http://wiki.wireshark.org/CaptureSetup/USB</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Dec '13, 07:56</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-28313" class="comments-container"><span id="28332"></span><div id="comment-28332" class="comment"><div id="post-28332-score" class="comment-score"></div><div class="comment-text"><p>Thanks. I download the USBPcap, it seems work, it can capture data for good board. I put it two PC, one is WinXP and another is in Win 7, both behavior differently.</p><p>I get a fault board, I can view the USB signal by scope, Win 7 shows some data are captured but WinXP no data is captured.</p><p>I am not sure the Win 7 captured data is wrong data or WinXP missing data packet?</p></div><div id="comment-28332-info" class="comment-info"><span class="comment-age">(22 Dec '13, 16:52)</span> <span class="comment-user userinfo">rabbit</span></div></div></div><div id="comment-tools-28313" class="comment-tools"></div><div class="clear"></div><div id="comment-28313-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="28370"></span>

<div id="answer-container-28370" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28370-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28370-score" class="post-score" title="current number of votes">0</div><span id="post-28370-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If USBPcap doesn't meet your needs and you're still looking for hardware for USB capturing, one of our engineers has the <a href="http://www.fte.com/products/FTS4USB.aspx">ComProbe USB 2.0 Protocol Analyzer</a> from <a href="http://www.fte.com">Frontline Test Equipment</a>. I don't know whether it would meet your needs or not.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Dec '13, 07:38</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-28370" class="comments-container"></div><div id="comment-tools-28370" class="comment-tools"></div><div class="clear"></div><div id="comment-28370-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

