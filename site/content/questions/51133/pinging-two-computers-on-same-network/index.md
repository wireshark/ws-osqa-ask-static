+++
type = "question"
title = "[closed] Pinging two computers on same network."
description = '''When i am pinging two computers using CMD one is giving success rate and the other is giving failure (like time out for all 4 packets). Anyone got any idea why this happening?? Moreover anyone have idea how to send ICMP Packets from hyeneaFE to another computer/machines? Thanks. Anique'''
date = "2016-03-23T12:40:00Z"
lastmod = "2016-03-24T02:56:00Z"
weight = 51133
keywords = [ "and", "icmp", "hyenaefe" ]
aliases = [ "/questions/51133" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Pinging two computers on same network.](/questions/51133/pinging-two-computers-on-same-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51133-score" class="post-score" title="current number of votes">0</div><span id="post-51133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When i am pinging two computers using CMD one is giving success rate and the other is giving failure (like time out for all 4 packets). Anyone got any idea why this happening?? Moreover anyone have idea how to send ICMP Packets from hyeneaFE to another computer/machines? Thanks. Anique</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-and" rel="tag" title="see questions tagged &#39;and&#39;">and</span> <span class="post-tag tag-link-icmp" rel="tag" title="see questions tagged &#39;icmp&#39;">icmp</span> <span class="post-tag tag-link-hyenaefe" rel="tag" title="see questions tagged &#39;hyenaefe&#39;">hyenaefe</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '16, 12:40</strong></p><img src="https://secure.gravatar.com/avatar/b60c46934f0b042f18f3f254951fef6d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anique&#39;s gravatar image" /><p><span>Anique</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anique has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Mar '16, 05:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-51133" class="comments-container"></div><div id="comment-tools-51133" class="comment-tools"></div><div class="clear"></div><div id="comment-51133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 24 Mar '16, 05:45

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51145"></span>

<div id="answer-container-51145" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51145-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51145-score" class="post-score" title="current number of votes">0</div><span id="post-51145-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Assuming this is a Windows system and you have no other problems like browsing the internet I would start with firewall blocking the connection, make sure you have not got the machine in the public network.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Mar '16, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/1d67795c1484b0652b735a3827dcb9b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m0rph&#39;s gravatar image" /><p><span>m0rph</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m0rph has no accepted answers">0%</span></p></div></div><div id="comments-container-51145" class="comments-container"></div><div id="comment-tools-51145" class="comment-tools"></div><div class="clear"></div><div id="comment-51145-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

