+++
type = "question"
title = "Unable to decrypt SSL traffic"
description = '''Hi there I am trying to decrypt SSL traffic on my wireshark. I know the private key of the server I am communicating with and I&#x27;ve installed it in my wireshark. But, for some reasons, the decrypt doesn&#x27;t work and I can&#x27;t figure out why. It looks like there are few error messages in the wireshark_ssl...'''
date = "2017-04-21T09:07:00Z"
lastmod = "2017-04-21T09:07:00Z"
weight = 60950
keywords = [ "ssl", "decrypt" ]
aliases = [ "/questions/60950" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to decrypt SSL traffic](/questions/60950/unable-to-decrypt-ssl-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60950-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60950-score" class="post-score" title="current number of votes">0</div><span id="post-60950-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there</p><p>I am trying to decrypt SSL traffic on my wireshark. I know the private key of the server I am communicating with and I've installed it in my wireshark. But, for some reasons, the decrypt doesn't work and I can't figure out why. It looks like there are few error messages in the <code>wireshark_ssl.log</code> but I can't understand which one is the root cause of the issue. Please find below the content of my <code>wireshark_ssl.log file.</code></p><p>Any help would be very appreciated.</p><p>Thanks.</p><p>Patrick</p><pre><code>Wireshark version: 2.2.6 (v2.2.6-0-g32dac6a)
GnuTLS version:    3.2.15
Libgcrypt version: 1.6.2

ssl_association_remove removing UDP 61614 - handle 0000000004F0D260
KeyID[20]:
| ab 25 27 83 0a bc 5c c2 8e 3d 45 11 9e 50 0b aa |.%&#39;...\..=E..P..|
| 3e 8c 6d 1f                                     |&gt;.m.            |
ssl_load_key: swapping p and q parameters and recomputing u
ssl_init private key file C:/private-key.pem successfully loaded.
ssl_init port &#39;61614&#39; filename &#39;C:/private-key.pem&#39; password(only for p12 file) &#39;&#39;
association_add ssl.port port 61614 handle 0000000004F0D260
KeyID[20]:
| ab 25 27 83 0a bc 5c c2 8e 3d 45 11 9e 50 0b aa |.%&#39;...\..=E..P..|
| 3e 8c 6d 1f                                     |&gt;.m.            |
ssl_load_key: swapping p and q parameters and recomputing u
ssl_init private key file C:/private-key.pem successfully loaded.
ssl_init port &#39;61614&#39; filename &#39;C:/private-key.pem&#39; password(only for p12 file) &#39;&#39;
association_add ssl.port port 61614 handle 0000000004F0D260

dissect_ssl enter frame #6 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 50
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 45
decrypt_ssl3_record: app_data len 45, ssl state 0x00
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 1 offset 5 length 41 bytes, remaining 50 
ssl_dissect_hnd_hello_common found CLIENT RANDOM -&gt; state 0x01

dissect_ssl enter frame #7 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 106

dissect_ssl enter frame #11 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 326
ssl_try_set_version found version 0x0301 -&gt; state 0x11
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 262
decrypt_ssl3_record: app_data len 262, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 16 offset 5 length 258 bytes, remaining 267 
ssl_load_keyfile dtls/ssl.keylog_file is not configured!
ssl_generate_pre_master_secret: found SSL_HND_CLIENT_KEY_EXCHG, state 11
ssl_generate_pre_master_secret: not enough data to generate key (required state 17)
dissect_ssl3_handshake can&#39;t generate pre master secret
  record: offset = 267, reported_length_remaining = 59
dissect_ssl3_record: content_type 20 Change Cipher Spec
ssl_load_keyfile dtls/ssl.keylog_file is not configured!
ssl_finalize_decryption state = 0x11
  Cipher suite (Server Hello) is missing!
packet_from_server: is from server - FALSE
ssl_change_cipher CLIENT
  record: offset = 273, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 278 48
decrypt_ssl3_record: app_data len 48, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 58 offset 278 length 551980 bytes, remaining 326

dissect_ssl enter frame #12 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 59
dissect_ssl3_record: content_type 20 Change Cipher Spec
ssl_dissect_change_cipher_spec No Session resumption, missing packets?
ssl_load_keyfile dtls/ssl.keylog_file is not configured!
ssl_finalize_decryption state = 0x11
  Cipher suite (Server Hello) is missing!
packet_from_server: is from server - TRUE
ssl_change_cipher SERVER
  record: offset = 6, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 11 48
decrypt_ssl3_record: app_data len 48, ssl state 0x11
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 1 offset 11 length 1206707 bytes, remaining 59

dissect_ssl enter frame #13 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 218
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 181
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 176, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #15 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 149
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 144, ssl state 0x11
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #16 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 117
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 112, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #18 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 405
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 400, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #20 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 656, ssl state 0x11
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #21 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 85
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 80, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #23 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #25 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x11
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #26 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000007FA24A0
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 21 Alert
decrypt_ssl3_record: app_data len 32, ssl state 0x11
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #35 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 50
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 45
decrypt_ssl3_record: app_data len 45, ssl state 0x00
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 1 offset 5 length 41 bytes, remaining 50 
ssl_dissect_hnd_hello_common found CLIENT RANDOM -&gt; state 0x01

dissect_ssl enter frame #37 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 79
ssl_try_set_version found version 0x0301 -&gt; state 0x11
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 74
decrypt_ssl3_record: app_data len 74, ssl state 0x11
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 2 offset 5 length 70 bytes, remaining 79 
ssl_try_set_version found version 0x0301 -&gt; state 0x11
ssl_dissect_hnd_hello_common found SERVER RANDOM -&gt; state 0x13
ssl_dissect_hnd_srv_hello found CIPHER 0x002F TLS_RSA_WITH_AES_128_CBC_SHA -&gt; state 0x17

dissect_ssl enter frame #38 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 28

dissect_ssl enter frame #40 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 9
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 4
decrypt_ssl3_record: app_data len 4, ssl state 0x17
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 14 offset 5 length 0 bytes, remaining 9

dissect_ssl enter frame #44 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 326
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 262
decrypt_ssl3_record: app_data len 262, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 16 offset 5 length 258 bytes, remaining 267 
ssl_load_keyfile dtls/ssl.keylog_file is not configured!
ssl_generate_pre_master_secret: found SSL_HND_CLIENT_KEY_EXCHG, state 217
ssl_restore_master_key can&#39;t find pre-master secret by Unencrypted pre-master secret
ssl_restore_master_key can&#39;t find pre-master secret by Encrypted pre-master secret
dissect_ssl3_handshake can&#39;t generate pre master secret
  record: offset = 267, reported_length_remaining = 59
dissect_ssl3_record: content_type 20 Change Cipher Spec
ssl_load_keyfile dtls/ssl.keylog_file is not configured!
ssl_finalize_decryption state = 0x217
ssl_restore_master_key can&#39;t find master secret by Session ID
ssl_restore_master_key can&#39;t find master secret by Client Random
  Cannot find master secret
packet_from_server: is from server - FALSE
ssl_change_cipher CLIENT
  record: offset = 273, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 278 48
decrypt_ssl3_record: app_data len 48, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 158 offset 278 length 2660428 bytes, remaining 326

dissect_ssl enter frame #45 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 6
dissect_ssl3_record: content_type 20 Change Cipher Spec
ssl_dissect_change_cipher_spec Not using Session resumption
ssl_load_keyfile dtls/ssl.keylog_file is not configured!
ssl_finalize_decryption state = 0x217
ssl_restore_master_key can&#39;t find master secret by Session ID
ssl_restore_master_key can&#39;t find master secret by Client Random
  Cannot find master secret
packet_from_server: is from server - TRUE
ssl_change_cipher SERVER

dissect_ssl enter frame #46 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 48
decrypt_ssl3_record: app_data len 48, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 149 offset 5 length 12596609 bytes, remaining 53

dissect_ssl enter frame #47 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 218
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 181
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 176, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #48 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
Calculating hash with offset 5 48
decrypt_ssl3_record: app_data len 48, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
dissect_ssl3_handshake iteration 1 type 149 offset 5 length 12596609 bytes, remaining 53

dissect_ssl enter frame #50 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 186
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 149
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 144, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #51 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 117
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 112, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #53 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 538
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 128, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available
  record: offset = 133, reported_length_remaining = 405
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 400, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #55 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #56 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 682
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 645
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 640, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #59 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 85
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 80, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #61 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 1428
  need_desegmentation: offset = 0, reported_length_remaining = 1428

dissect_ssl enter frame #62 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 1477
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 1472, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #62 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 1379
  need_desegmentation: offset = 0, reported_length_remaining = 1379

dissect_ssl enter frame #65 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 1477
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 1472, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #65 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 1237
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 1232, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #67 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #69 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 357
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 352, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #70 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #71 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 506
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 469
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 464, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #74 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 85
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 80, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #76 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 885
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 880, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #78 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #80 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #81 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #83 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #84 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #86 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #87 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #89 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #90 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #92 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #93 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #94 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #95 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #97 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #98 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #100 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 416, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #101 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #103 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 656, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #104 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #106 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #108 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #110 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 656, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #112 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #114 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #116 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #118 (first time)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 656, ssl state 0x217
packet_from_server: is from server - FALSE
decrypt_ssl3_record: using client decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #120 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 64, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #122 (first time)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000007FA6560
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data
decrypt_ssl3_record: app_data len 32, ssl state 0x217
packet_from_server: is from server - TRUE
decrypt_ssl3_record: using server decoder
decrypt_ssl3_record: no decoder available

dissect_ssl enter frame #6 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 50
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 1 offset 5 length 41 bytes, remaining 50

dissect_ssl enter frame #7 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106

dissect_ssl enter frame #11 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 326
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 16 offset 5 length 258 bytes, remaining 267 
  record: offset = 267, reported_length_remaining = 59
dissect_ssl3_record: content_type 20 Change Cipher Spec
  record: offset = 273, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 58 offset 278 length 551980 bytes, remaining 326

dissect_ssl enter frame #12 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 59
dissect_ssl3_record: content_type 20 Change Cipher Spec
  record: offset = 6, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 1 offset 11 length 1206707 bytes, remaining 59

dissect_ssl enter frame #13 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 218
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 181
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #15 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 149
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #16 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 117
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #18 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 405
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #20 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #21 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 85
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #23 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #25 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #26 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA1AD0, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 21 Alert

dissect_ssl enter frame #35 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 50
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 1 offset 5 length 41 bytes, remaining 50

dissect_ssl enter frame #37 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 79
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 2 offset 5 length 70 bytes, remaining 79

dissect_ssl enter frame #38 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 28

dissect_ssl enter frame #40 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 9
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 14 offset 5 length 0 bytes, remaining 9

dissect_ssl enter frame #44 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 326
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 16 offset 5 length 258 bytes, remaining 267 
  record: offset = 267, reported_length_remaining = 59
dissect_ssl3_record: content_type 20 Change Cipher Spec
  record: offset = 273, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 158 offset 278 length 2660428 bytes, remaining 326

dissect_ssl enter frame #45 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 6
dissect_ssl3_record: content_type 20 Change Cipher Spec

dissect_ssl enter frame #46 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 149 offset 5 length 12596609 bytes, remaining 53

dissect_ssl enter frame #47 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 218
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 181
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #48 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 53
dissect_ssl3_record: content_type 22 Handshake
dissect_ssl3_handshake iteration 1 type 149 offset 5 length 12596609 bytes, remaining 53

dissect_ssl enter frame #50 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 186
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 149
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #51 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 117
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #53 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 538
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 133, reported_length_remaining = 405
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #55 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #56 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 682
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 645
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #59 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 85
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #62 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 1477
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #65 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 1477
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #65 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 1237
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #67 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #69 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 357
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #70 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #71 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 506
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 469
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #74 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 85
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #76 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 885
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #78 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #80 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #81 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #83 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #84 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #86 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #87 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #89 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #90 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #92 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #93 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #94 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #95 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #97 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #98 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #100 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 421
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #101 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #103 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #104 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #106 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #108 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #110 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #112 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #114 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #116 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #118 (already visited)
packet_from_server: is from server - FALSE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 661
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #120 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 106
dissect_ssl3_record: content_type 23 Application Data
  record: offset = 37, reported_length_remaining = 69
dissect_ssl3_record: content_type 23 Application Data

dissect_ssl enter frame #122 (already visited)
packet_from_server: is from server - TRUE
  conversation = 0000000007FA5B90, ssl_session = 0000000000000000
  record: offset = 0, reported_length_remaining = 37
dissect_ssl3_record: content_type 23 Application Data</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Apr '17, 09:07</strong></p><img src="https://secure.gravatar.com/avatar/41168bd7c7b13f4d87539e84d0632849?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="patrickoli&#39;s gravatar image" /><p><span>patrickoli</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="patrickoli has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Apr '17, 13:08</strong> </span></p></div></div><div id="comments-container-60950" class="comments-container"></div><div id="comment-tools-60950" class="comment-tools"></div><div class="clear"></div><div id="comment-60950-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

