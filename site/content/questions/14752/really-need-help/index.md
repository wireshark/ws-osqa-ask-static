+++
type = "question"
title = "Really need help"
description = '''I am just trying to learn wireshark and have an assignment that I hope someone can point me in the right direction. I am suppose to analyze network traffic from google.ca. Q#1 is the query and response to determine th ip address of google.ca---done Q2 is three stages of three way handshake.---done Q...'''
date = "2012-10-07T06:39:00Z"
lastmod = "2012-10-07T14:09:00Z"
weight = 14752
keywords = [ "wireshark" ]
aliases = [ "/questions/14752" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Really need help](/questions/14752/really-need-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14752-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14752-score" class="post-score" title="current number of votes">0</div><span id="post-14752-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am just trying to learn wireshark and have an assignment that I hope someone can point me in the right direction. I am suppose to analyze network traffic from <a href="http://google.ca">google.ca</a>. Q#1 is the query and response to determine th ip address of google.ca---done Q2 is three stages of three way handshake.---done Q3 is the first http packet sent to <a href="http://google.ca">google.ca</a> after handshake. Don't know what to look for here?</p><p>Q4 is how many tcp packets were reassembled to display the <a href="http://google.ca">google.ca</a> page?---I have no idea what to look for?? Q5 is what transport layer was used to determine the ip address of <a href="http://google.ca">google.ca</a>.---I think its is DNS but not sure? Q5 is what transport layer is being used in sending html and associated files from <a href="http://google.ca">google.ca</a>.---I have no idea?</p><p>I just dont know what to look for on wireshark, there are so many lines in the capture from google.... I hope someone can point me in the right direction.</p><p>Thanks.</p><p>Rog</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '12, 06:39</strong></p><img src="https://secure.gravatar.com/avatar/ef1ba3e18370d8d25d2a0c8113e44c0c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rog3221&#39;s gravatar image" /><p><span>Rog3221</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rog3221 has no accepted answers">0%</span></p></div></div><div id="comments-container-14752" class="comments-container"></div><div id="comment-tools-14752" class="comment-tools"></div><div class="clear"></div><div id="comment-14752-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="14753"></span>

<div id="answer-container-14753" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14753-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14753-score" class="post-score" title="current number of votes">0</div><span id="post-14753-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Rog,</p><p>I've got some youtube vids and ppts going over some of this. <a href="http://thetechfirm.com/networking/networking.htm">http://thetechfirm.com/networking/networking.htm</a> and <a href="http://thetechfirm.com/wireshark/wireshark.html">http://thetechfirm.com/wireshark/wireshark.html</a></p><p>hope that helps</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '12, 07:33</strong></p><img src="https://secure.gravatar.com/avatar/dbc4d8cb6be85bd586ca4bf211e1337c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thetechfirm&#39;s gravatar image" /><p><span>thetechfirm</span><br />
<span class="score" title="64 reputation points">64</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thetechfirm has no accepted answers">0%</span></p></div></div><div id="comments-container-14753" class="comments-container"><span id="14754"></span><div id="comment-14754" class="comment"><div id="post-14754-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much, I will check them out.</p><p>Rog</p></div><div id="comment-14754-info" class="comment-info"><span class="comment-age">(07 Oct '12, 07:35)</span> <span class="comment-user userinfo">Rog3221</span></div></div></div><div id="comment-tools-14753" class="comment-tools"></div><div class="clear"></div><div id="comment-14753-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14755"></span>

<div id="answer-container-14755" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14755-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14755-score" class="post-score" title="current number of votes">0</div><span id="post-14755-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Let's see what we can help you with here - this is a refreshing case of someone not asking for all answers without looking by himself first, so I'll be happy to help.</p><p>Q3 - Usually the client wants something from the server. For that you need to establish a connection first, so that was what you answered in Q2. Next thing to look for is the actual request sent by the client. If you investigate how HTTP works you'll find that it uses GET/POST and some minor important request types, so you should look for those right after the handshake is complete. It will tell you the URL that is requested, too.</p><p>Q4 - Wireshark uses packet reassembly to find all packets that are part of a response, because in most cases the server response (in your case a web page) will not fit into the 1460 bytes a single TCP/IPv4 packet can hold. Once again, if you research how HTTP works you'll see it will use return codes to tell you if the request was answered successfully (Code 2xx/3xx) or not (4xx/5xx). So look into all packets coming from the server after the client has requested the content, and you'll find the return code at some point. Look into the packet and you'll see a section in square brackets where Wireshark will tell you what packets are part of the reassembled answer. BTW this only works in default configuration, because if someone turns of TCP Stream Reassembly it won't do that.</p><p>Q5 - DNS is correct, yes - DNS resolves fully qualified domain names (FQDNs) to IP addresses, and <a href="http://google.ca">google.ca</a> was the FQDN in your case.</p><p>Q6 (I guess, because you said Q5 twice) - HTML is transported via HTTP, but that is an application protocol, not a transport protocol. If you find a packet containing HTTP and look at layer 4 (which, if you look at the OSI modell, is the transport layer) - there should be ethernet (Layer 2), IP (Layer 3) and... your answer in Layer 4.</p><p>So, good luck in your quest for answers, and let us know if there's anything unclear you need help with.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '12, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14755" class="comments-container"><span id="14764"></span><div id="comment-14764" class="comment"><div id="post-14764-score" class="comment-score"></div><div class="comment-text"><p>Ah, I think its starting to make sense. That's a much better description than the classroom lesson I was given. Thank you very much</p></div><div id="comment-14764-info" class="comment-info"><span class="comment-age">(07 Oct '12, 14:09)</span> <span class="comment-user userinfo">Rog3221</span></div></div></div><div id="comment-tools-14755" class="comment-tools"></div><div class="clear"></div><div id="comment-14755-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

