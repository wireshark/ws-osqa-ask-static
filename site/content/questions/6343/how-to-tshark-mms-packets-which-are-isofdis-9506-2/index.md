+++
type = "question"
title = "how to tshark mms packets which are ISO/FDIS 9506-2"
description = '''some kind person tell me &quot;Edit-&amp;gt;preferences-&amp;gt;protocol-&amp;gt;PRES and edit the users context tale enter context = 3 and OID = 1.0.9506.2.3 and your trace will be dissected as MMS.&quot; i want to use &quot;tshak&quot; convert packets to text, such as &quot;tshak -r d:sg1.pcap -V -T text &amp;gt; d:sg1.txt&quot;. how to make ...'''
date = "2011-09-14T01:08:00Z"
lastmod = "2014-09-01T03:08:00Z"
weight = 6343
keywords = [ "tshark" ]
aliases = [ "/questions/6343" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to tshark mms packets which are ISO/FDIS 9506-2](/questions/6343/how-to-tshark-mms-packets-which-are-isofdis-9506-2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6343-score" class="post-score" title="current number of votes">1</div><span id="post-6343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>some kind person tell me "Edit-&gt;preferences-&gt;protocol-&gt;PRES and edit the users context tale enter context = 3 and OID = 1.0.9506.2.3 and your trace will be dissected as MMS."</p><p>i want to use "<strong>tshak</strong>" convert packets to text, such as "tshak -r d:sg1.pcap -V -T text &gt; d:sg1.txt".</p><p>how to make the contents in text to appear the result of "Edit-&gt;preferences-&gt;protocol-&gt;PRES and edit the users context tale， enter context = 3 and OID = 1.0.9506.2.3", not "single-ASN1-type: A029020201B5A423A121A01F301DA01BA1191A0944455035..." ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '11, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/a5a3214300b3b17fc46c3b656b7bed01?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ylda_ljm0620&#39;s gravatar image" /><p><span>ylda_ljm0620</span><br />
<span class="score" title="31 reputation points">31</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ylda_ljm0620 has no accepted answers">0%</span></p></div></div><div id="comments-container-6343" class="comments-container"><span id="35902"></span><div id="comment-35902" class="comment"><div id="post-35902-score" class="comment-score"></div><div class="comment-text"><p>You could add the mms tag</p></div><div id="comment-35902-info" class="comment-info"><span class="comment-age">(01 Sep '14, 02:56)</span> <span class="comment-user userinfo">mildred</span></div></div></div><div id="comment-tools-6343" class="comment-tools"></div><div class="clear"></div><div id="comment-6343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35903"></span>

<div id="answer-container-35903" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35903-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35903-score" class="post-score" title="current number of votes">0</div><span id="post-35903-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The solution is to use the preference files in ~/.wireshark</p><p>When you add a user context entry in the Wireshark GUI, it appears in the file <code>~/.wireshark/pres_context_list</code> (or if you use a profile, in <code>~/.wireshark/profiles/PROFILENAME/pres_context_list</code>). These are automatically picked up by tshark.</p><p>In tshark, you can use a profile by setting the <code>-C PROFILENAME</code> command line options.</p><p>The <code>pres_context_list</code> file contains the table in the following format:</p><pre><code># This file is automatically generated, DO NOT MODIFY.
&quot;3&quot;,&quot;1.0.9506.2.1&quot;</code></pre><p>You can use a script to populate this table, in a profile if you want to keep out from wireshark preferences:</p><pre><code>mkdir -p ~/.wireshark/profiles/tshark-mms
echo &quot;\&quot;3\&quot;,\&quot;1.0.9506.2.1\&quot;&quot; &gt;&gt; ~/.wireshark/profiles/tshark-mms/pres_context_list</code></pre><p>Then, run tshark with:</p><pre><code>tshak -C tshark-mms -r d:sg1.pcap -V -T text &gt; d:sg1.txt</code></pre><p>I waanted to do exactly the same thing, and it worked for me. Adjust the values in the table for your specific case. You can find the mapping between the identifier <code>3</code> and <code>1.0.9506.2.1</code> (that's what I have in my specific case) using the following steps:</p><ul><li>start the capture before the MMS session starts</li><li>Start the MMS session on the network</li><li>Stop the capture</li><li>Look at the very first MMS message captured (you can use the Filter TCP stream feature to locate it easily, the protocol column in wireshark must show "MMS")</li><li><p>The mapping can be found in:</p><ul><li>the <code>pres</code> protocol "ISO 8823 OSI Presentation Protocol"</li><li><code>pres.cptype</code>: "CP-type"</li><li><code>pres.normal_mode_parameters</code>: "normal-mode-parameters"</li><li><code>pres.presentation_context_definition_list</code>: "presentation-context-definition-list: 2 items"</li><li>one of <code>pres.Context_list_item</code>: "Context-list item"</li></ul></li></ul><p>Look at <code>pres.presentation_context_identifier</code> and <code>pres.abstract_syntax_name</code>. These are the two columns of the context list table.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Sep '14, 03:08</strong></p><img src="https://secure.gravatar.com/avatar/5211b7e23873f10bf6cb9777ac69bee1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mildred&#39;s gravatar image" /><p><span>mildred</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mildred has no accepted answers">0%</span></p></div></div><div id="comments-container-35903" class="comments-container"></div><div id="comment-tools-35903" class="comment-tools"></div><div class="clear"></div><div id="comment-35903-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

