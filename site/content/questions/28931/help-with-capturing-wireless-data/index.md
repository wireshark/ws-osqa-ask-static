+++
type = "question"
title = "Help with capturing wireless data"
description = '''Are you able to capture packets &#x27;in the air&#x27;? As in, not joined to the network or do you have to be joined to the network? I could have sworn there was a way in Linux to capture wireless packets without having to be joined on to the network.'''
date = "2014-01-15T11:40:00Z"
lastmod = "2014-01-15T13:27:00Z"
weight = 28931
keywords = [ "wireless" ]
aliases = [ "/questions/28931" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Help with capturing wireless data](/questions/28931/help-with-capturing-wireless-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28931-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28931-score" class="post-score" title="current number of votes">0</div><span id="post-28931-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are you able to capture packets 'in the air'? As in, not joined to the network or do you have to be joined to the network? I could have sworn there was a way in Linux to capture wireless packets without having to be joined on to the network.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '14, 11:40</strong></p><img src="https://secure.gravatar.com/avatar/bff9eda43cb311958922d6c4af0b6fc8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chris%20Hinton&#39;s gravatar image" /><p><span>Chris Hinton</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chris Hinton has no accepted answers">0%</span></p></div></div><div id="comments-container-28931" class="comments-container"></div><div id="comment-tools-28931" class="comment-tools"></div><div class="clear"></div><div id="comment-28931-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28933"></span>

<div id="answer-container-28933" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28933-score" class="post-score" title="current number of votes">0</div><span id="post-28933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you can do that, but you probably have to toggle monitor mode on your wireless card first. This is something Wireshark can't do for you. Take a look at the <a href="http://wiki.wireshark.org/CaptureSetup/WLAN#Turning_on_monitor_mode">Wiki</a> for more information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '14, 13:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28933" class="comments-container"></div><div id="comment-tools-28933" class="comment-tools"></div><div class="clear"></div><div id="comment-28933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

