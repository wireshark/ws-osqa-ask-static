+++
type = "question"
title = "Decoding CDRs"
description = '''Hello all, I&#x27;d like to know how I can decode GGSN CDRs in wireshark. I have the version 1.8 installed. I see that the packets are recognized as GTP Data record transfer request and response but when I checked the contents, it says that it&#x27;s malformed. Do I need to add something to wireshark to decod...'''
date = "2012-10-02T23:54:00Z"
lastmod = "2012-10-02T23:54:00Z"
weight = 14656
keywords = [ "gtp-prime", "gtp", "cdr" ]
aliases = [ "/questions/14656" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding CDRs](/questions/14656/decoding-cdrs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14656-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14656-score" class="post-score" title="current number of votes">0</div><span id="post-14656-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all,</p><p>I'd like to know how I can decode GGSN CDRs in wireshark. I have the version 1.8 installed. I see that the packets are recognized as GTP Data record transfer request and response but when I checked the contents, it says that it's malformed.</p><p>Do I need to add something to wireshark to decode it and how? I did not see much options for GTP in preferences</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtp-prime" rel="tag" title="see questions tagged &#39;gtp-prime&#39;">gtp-prime</span> <span class="post-tag tag-link-gtp" rel="tag" title="see questions tagged &#39;gtp&#39;">gtp</span> <span class="post-tag tag-link-cdr" rel="tag" title="see questions tagged &#39;cdr&#39;">cdr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Oct '12, 23:54</strong></p><img src="https://secure.gravatar.com/avatar/57c927aa92cc9484e4597aa74efc782b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bubade&#39;s gravatar image" /><p><span>Bubade</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bubade has no accepted answers">0%</span></p></div></div><div id="comments-container-14656" class="comments-container"></div><div id="comment-tools-14656" class="comment-tools"></div><div class="clear"></div><div id="comment-14656-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

