+++
type = "question"
title = "can i use dial up modem?"
description = '''can i use dial up modem in wireshark'''
date = "2013-01-12T08:27:00Z"
lastmod = "2013-01-12T09:57:00Z"
weight = 17640
keywords = [ "dial", "up" ]
aliases = [ "/questions/17640" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can i use dial up modem?](/questions/17640/can-i-use-dial-up-modem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17640-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17640-score" class="post-score" title="current number of votes">0</div><span id="post-17640-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can i use dial up modem in wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dial" rel="tag" title="see questions tagged &#39;dial&#39;">dial</span> <span class="post-tag tag-link-up" rel="tag" title="see questions tagged &#39;up&#39;">up</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '13, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/74c079fedff17ec09ee8f381960f8f42?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shivam%20Kulshrestha&#39;s gravatar image" /><p><span>Shivam Kulsh...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shivam Kulshrestha has no accepted answers">0%</span></p></div></div><div id="comments-container-17640" class="comments-container"></div><div id="comment-tools-17640" class="comment-tools"></div><div class="clear"></div><div id="comment-17640-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17641"></span>

<div id="answer-container-17641" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17641-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17641-score" class="post-score" title="current number of votes">0</div><span id="post-17641-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe.</p><p>See <a href="http://wiki.wireshark.org/CaptureSetup/PPP">PPP capture setup</a> for some (techy, maybe not up to date) info.</p><p>I'd have verify this, but ISTR that you might be better off using Microsoft Netmon to capture PPP data and then use Wireshark to dissect the captured data.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jan '13, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Jan '13, 08:52</strong> </span></p></div></div><div id="comments-container-17641" class="comments-container"><span id="17642"></span><div id="comment-17642" class="comment"><div id="post-17642-score" class="comment-score"></div><div class="comment-text"><p>i see..but doesnt help</p><p>(Converted to a comment following the style of ask.wireshark.org. Please see the FAQ).</p></div><div id="comment-17642-info" class="comment-info"><span class="comment-age">(12 Jan '13, 08:52)</span> <span class="comment-user userinfo">Shivam Kulsh...</span></div></div><span id="17644"></span><div id="comment-17644" class="comment"><div id="post-17644-score" class="comment-score"></div><div class="comment-text"><p>On Windows, WinPCap (the capture mechanism used by Wireshark) can't capture on PPP interfaces, but Network Monitor from Microsoft can. Wireshark can open the Network Monitor captures.</p></div><div id="comment-17644-info" class="comment-info"><span class="comment-age">(12 Jan '13, 09:57)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-17641" class="comment-tools"></div><div class="clear"></div><div id="comment-17641-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

