+++
type = "question"
title = "Write an ethernet-type lua script"
description = '''when i write like this: local wtap_encap_table = DissectorTable.get(&quot;wtap_encap&quot;) wtap_encap_table:add(wtap[&quot;ETHERNET&quot;], omciproto) All packets are parsed，How can the specified message be parsed？'''
date = "2016-12-01T18:48:00Z"
lastmod = "2016-12-01T20:09:00Z"
weight = 57772
keywords = [ "lua" ]
aliases = [ "/questions/57772" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Write an ethernet-type lua script](/questions/57772/write-an-ethernet-type-lua-script)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57772-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57772-score" class="post-score" title="current number of votes">0</div><span id="post-57772-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when i write like this: local wtap_encap_table = DissectorTable.get("wtap_encap") wtap_encap_table:add(wtap["ETHERNET"], omciproto)</p><p>All packets are parsed，How can the specified message be parsed？</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '16, 18:48</strong></p><img src="https://secure.gravatar.com/avatar/35d3f2ee8db58e4f71adf25791a297d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chenhao&#39;s gravatar image" /><p><span>chenhao</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chenhao has no accepted answers">0%</span></p></div></div><div id="comments-container-57772" class="comments-container"><span id="57773"></span><div id="comment-57773" class="comment"><div id="post-57773-score" class="comment-score"></div><div class="comment-text"><p>Does the specified message have an Ethernet type value? Is that what you mean by "an ethernet-type lua script"?</p></div><div id="comment-57773-info" class="comment-info"><span class="comment-age">(01 Dec '16, 20:09)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-57772" class="comment-tools"></div><div class="clear"></div><div id="comment-57772-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

