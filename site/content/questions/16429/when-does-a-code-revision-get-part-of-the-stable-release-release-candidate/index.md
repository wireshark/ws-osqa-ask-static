+++
type = "question"
title = "When does a code revision get part of the stable release / release candidate?"
description = '''Code fix for BUG 7713 was accepted in trunk as revision# 44895. I was interested to know, how and when does the fix get released in the stable release or get part of the release candidate.'''
date = "2012-11-29T07:51:00Z"
lastmod = "2012-11-29T19:00:00Z"
weight = 16429
keywords = [ "release" ]
aliases = [ "/questions/16429" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [When does a code revision get part of the stable release / release candidate?](/questions/16429/when-does-a-code-revision-get-part-of-the-stable-release-release-candidate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16429-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16429-score" class="post-score" title="current number of votes">0</div><span id="post-16429-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Code fix for BUG 7713 was accepted in trunk as revision# 44895. I was interested to know, how and when does the fix get released in the stable release or get part of the release candidate.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-release" rel="tag" title="see questions tagged &#39;release&#39;">release</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Nov '12, 07:51</strong></p><img src="https://secure.gravatar.com/avatar/d3235192ec192361d6021fe87c3a9bb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikhilkalu&#39;s gravatar image" /><p><span>nikhilkalu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikhilkalu has no accepted answers">0%</span></p></div></div><div id="comments-container-16429" class="comments-container"></div><div id="comment-tools-16429" class="comment-tools"></div><div class="clear"></div><div id="comment-16429-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16431"></span>

<div id="answer-container-16431" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16431-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16431-score" class="post-score" title="current number of votes">3</div><span id="post-16431-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://wiki.wireshark.org/Development/ReleasePolicy">http://wiki.wireshark.org/Development/ReleasePolicy</a> as this is new functionality it will be included in the next official release, no planned date yet but mid 2013 is a fair guess. No real plans for release candidates or development builds. Windows build bot builds are available.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Nov '12, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-16431" class="comments-container"><span id="16433"></span><div id="comment-16433" class="comment"><div id="post-16433-score" class="comment-score"></div><div class="comment-text"><p>But this is not a new functionality, but added support for the latest revision of Citrix NSTRACE. In lack of this code fix, wireshark shall be able dissect only older NSTRACE captures. Is there a way to bring it into notice of the right people and get it included in the next stable release?</p></div><div id="comment-16433-info" class="comment-info"><span class="comment-age">(29 Nov '12, 09:25)</span> <span class="comment-user userinfo">nikhilkalu</span></div></div><span id="16446"></span><div id="comment-16446" class="comment"><div id="post-16446-score" class="comment-score"></div><div class="comment-text"><p>Support for a new revision of a protocol <em>is</em> new functionality.</p></div><div id="comment-16446-info" class="comment-info"><span class="comment-age">(29 Nov '12, 19:00)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-16431" class="comment-tools"></div><div class="clear"></div><div id="comment-16431-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

