+++
type = "question"
title = "Baracuda Ethernet Tap BET10"
description = '''Greetings, would like to know if I can run multiple instances of Wireshark on win2k3ent edition with two intel 10/100 desktop cards plugged into a Baracuda Ethernet Tap model BET10 10/100 passive tap. two nics are required to do full duplex captures. but that is all I know. any info on this would be...'''
date = "2011-02-01T09:23:00Z"
lastmod = "2011-02-01T09:45:00Z"
weight = 2073
keywords = [ "tap" ]
aliases = [ "/questions/2073" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Baracuda Ethernet Tap BET10](/questions/2073/baracuda-ethernet-tap-bet10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2073-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2073-score" class="post-score" title="current number of votes">0</div><span id="post-2073-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings, would like to know if I can run multiple instances of Wireshark on win2k3ent edition with two intel 10/100 desktop cards plugged into a Baracuda Ethernet Tap model BET10 10/100 passive tap. two nics are required to do full duplex captures. but that is all I know. any info on this would be great. thanks in advance for your support and help.</p><p>Brent</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tap" rel="tag" title="see questions tagged &#39;tap&#39;">tap</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '11, 09:23</strong></p><img src="https://secure.gravatar.com/avatar/5754fe8dd1a1bf3e7a94356e8080b413?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="netsyseng69&#39;s gravatar image" /><p><span>netsyseng69</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="netsyseng69 has no accepted answers">0%</span></p></div></div><div id="comments-container-2073" class="comments-container"></div><div id="comment-tools-2073" class="comment-tools"></div><div class="clear"></div><div id="comment-2073-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2074"></span>

<div id="answer-container-2074" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2074-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2074-score" class="post-score" title="current number of votes">1</div><span id="post-2074-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you can run two instances of Wireshark to capture from the two NICs. After capturing the traffic, you'll probably want to save the captured data to two files and then merge the files into a single file to make the analysis easier.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '11, 09:45</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-2074" class="comments-container"></div><div id="comment-tools-2074" class="comment-tools"></div><div class="clear"></div><div id="comment-2074-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

