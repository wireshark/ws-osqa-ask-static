+++
type = "question"
title = "Install Wireshark Via Group Policy"
description = '''Is this possible? I have extracted the executable file, but I don&#x27;t see a .MSI file'''
date = "2011-05-04T08:56:00Z"
lastmod = "2011-05-04T23:24:00Z"
weight = 3924
keywords = [ "install" ]
aliases = [ "/questions/3924" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Install Wireshark Via Group Policy](/questions/3924/install-wireshark-via-group-policy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3924-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3924-score" class="post-score" title="current number of votes">0</div><span id="post-3924-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is this possible? I have extracted the executable file, but I don't see a .MSI file</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '11, 08:56</strong></p><img src="https://secure.gravatar.com/avatar/d489883b823e0cc47a60aa96514d950b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DelayedSynapsis&#39;s gravatar image" /><p><span>DelayedSynapsis</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DelayedSynapsis has no accepted answers">0%</span></p></div></div><div id="comments-container-3924" class="comments-container"></div><div id="comment-tools-3924" class="comment-tools"></div><div class="clear"></div><div id="comment-3924-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3933"></span>

<div id="answer-container-3933" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3933-score" class="post-score" title="current number of votes">0</div><span id="post-3933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's no MSI in there, so that's not possible. If you manage to wrap the EXE installer that would work, but I'm not even aware that can be done.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 May '11, 23:24</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3933" class="comments-container"></div><div id="comment-tools-3933" class="comment-tools"></div><div class="clear"></div><div id="comment-3933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

