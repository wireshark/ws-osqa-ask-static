+++
type = "question"
title = "Windows 7 64-bit Installation Problem"
description = '''I have run the 64-bit version on my Windows 7 64-bit desktop. However, I am now receiving a 0xc000007b error when trying to start Wireshark. I downloaded both the 64-bit and 32-bit installation packages. The error is received any time I try to run the 64-bit version, but the 32-bit version will inst...'''
date = "2013-11-10T13:03:00Z"
lastmod = "2013-11-12T06:50:00Z"
weight = 26820
keywords = [ "error", "0xc000007b", "install", "dll" ]
aliases = [ "/questions/26820" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 64-bit Installation Problem](/questions/26820/windows-7-64-bit-installation-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26820-score" class="post-score" title="current number of votes">0</div><span id="post-26820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have run the 64-bit version on my Windows 7 64-bit desktop. However, I am now receiving a 0xc000007b error when trying to start Wireshark. I downloaded both the 64-bit and 32-bit installation packages. The error is received any time I try to run the 64-bit version, but the 32-bit version will install and run. I wonder if this s a dll problem with the correct one not being installed. Additional troubleshooting and suggestions would be helpful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span> <span class="post-tag tag-link-0xc000007b" rel="tag" title="see questions tagged &#39;0xc000007b&#39;">0xc000007b</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-dll" rel="tag" title="see questions tagged &#39;dll&#39;">dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '13, 13:03</strong></p><img src="https://secure.gravatar.com/avatar/17086be3083fd77e6db6a39a306def00?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ff2emtvol&#39;s gravatar image" /><p><span>ff2emtvol</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ff2emtvol has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Nov '13, 15:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-26820" class="comments-container"></div><div id="comment-tools-26820" class="comment-tools"></div><div class="clear"></div><div id="comment-26820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26824"></span>

<div id="answer-container-26824" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26824-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26824-score" class="post-score" title="current number of votes">0</div><span id="post-26824-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the answers for a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/19977/how-can-i-fix-error-0xc000007b-on-startup">http://ask.wireshark.org/questions/19977/how-can-i-fix-error-0xc000007b-on-startup</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '13, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-26824" class="comments-container"><span id="26895"></span><div id="comment-26895" class="comment"><div id="post-26895-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@ff2emtvol</span>: You added a comment to the other question: <a href="http://ask.wireshark.org/questions/19977/how-can-i-fix-error-0xc000007b-on-startup">http://ask.wireshark.org/questions/19977/how-can-i-fix-error-0xc000007b-on-startup</a></p><p>It's hard to follow your own question (this one), if you spread your comments. Therefore I copied your comment here.</p><p>Cite:</p><blockquote><p>The install package was downloaded directly from Wireshark. All anti-malware software was disabled during the installation. The 64-bit program would still not start (gave error). However, if the 32-bit is installed it operates perfectly, even if the anti-malware software is active during the install. This appears to indicate a needed 64-bit dll is not being installed or referenced correctly.</p></blockquote></div><div id="comment-26895-info" class="comment-info"><span class="comment-age">(12 Nov '13, 06:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="26896"></span><div id="comment-26896" class="comment"><div id="post-26896-score" class="comment-score"></div><div class="comment-text"><p>Some questions:</p><ul><li>Which version of Wireshark did you download?</li><li>What is the SHA1 hash sum of your 64-bit installation package? Please google for a windows SHA1 hash calculation tool.</li></ul><p>If the SHA1 hash sum is not identical to the one posted on wireshark.org (<a href="http://www.wireshark.org/download/SIGNATURES-1.10.3.txt">version 1.10.3</a> or <a href="http://www.wireshark.org/download/SIGNATURES-1.11.0.txt">version 1.11.0</a>), something modified the binary during (or after) the download. If that is the case, please download it again, until the SHA1 hash sum is identical.</p></div><div id="comment-26896-info" class="comment-info"><span class="comment-age">(12 Nov '13, 06:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-26824" class="comment-tools"></div><div class="clear"></div><div id="comment-26824-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

