+++
type = "question"
title = "where can I find the file name specification that tells me what each field is and what they mean?"
description = '''where can I find the file name specification that tells me what each field is and what they mean? I am trying to better understand what each term means, but cannot find a manual or information explaining each category.'''
date = "2014-05-23T13:21:00Z"
lastmod = "2014-05-23T15:31:00Z"
weight = 33025
keywords = [ "file-format", "manual", "name-resolving" ]
aliases = [ "/questions/33025" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [where can I find the file name specification that tells me what each field is and what they mean?](/questions/33025/where-can-i-find-the-file-name-specification-that-tells-me-what-each-field-is-and-what-they-mean)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33025-score" class="post-score" title="current number of votes">0</div><span id="post-33025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>where can I find the file name specification that tells me what each field is and what they mean? I am trying to better understand what each term means, but cannot find a manual or information explaining each category.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-file-format" rel="tag" title="see questions tagged &#39;file-format&#39;">file-format</span> <span class="post-tag tag-link-manual" rel="tag" title="see questions tagged &#39;manual&#39;">manual</span> <span class="post-tag tag-link-name-resolving" rel="tag" title="see questions tagged &#39;name-resolving&#39;">name-resolving</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '14, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/85c96babcabb355595eee20f93071fec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jberger512&#39;s gravatar image" /><p><span>jberger512</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jberger512 has no accepted answers">0%</span></p></div></div><div id="comments-container-33025" class="comments-container"></div><div id="comment-tools-33025" class="comment-tools"></div><div class="clear"></div><div id="comment-33025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33027"></span>

<div id="answer-container-33027" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33027-score" class="post-score" title="current number of votes">0</div><span id="post-33027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If by "file name specification that tells me what each field is and what they mean" you mean "the name of the file that documents all of the named packet fields", then there's no file that's shipped with Wireshark, but there is <a href="http://www.wireshark.org/docs/dfref/">a reference for all the named fields</a>. (That page should call them "named fields", not "display filters", because they're useful for more than just display filtering.)</p><p>The description of the field is taken from the Wireshark code; it is not necessarily a detailed description of the field. For a detailed description of a field for a particular protocol, you'll probably need to read some form of specification for the protocol or tutorial for the protocol; we do not have a list of those.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 May '14, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 May '14, 15:34</strong> </span></p></div></div><div id="comments-container-33027" class="comments-container"></div><div id="comment-tools-33027" class="comment-tools"></div><div class="clear"></div><div id="comment-33027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

