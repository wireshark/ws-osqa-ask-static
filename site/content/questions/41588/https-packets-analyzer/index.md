+++
type = "question"
title = "Https Packets Analyzer"
description = '''I am looking for Live streaming Videos(https) detail information. Is there a plugin or other altenative in Wireshark.  Like we have mediainfo or exiftool shows the information of saved video or audio files similarly looking for Wireshark Plugin. '''
date = "2015-04-20T02:01:00Z"
lastmod = "2015-04-20T02:01:00Z"
weight = 41588
keywords = [ "videostream" ]
aliases = [ "/questions/41588" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Https Packets Analyzer](/questions/41588/https-packets-analyzer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41588-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41588-score" class="post-score" title="current number of votes">0</div><span id="post-41588-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>I am looking for Live streaming Videos(https) detail information. Is there a plugin or other altenative in Wireshark.</code></pre><p>Like we have mediainfo or exiftool shows the information of saved video or audio files similarly looking for Wireshark Plugin.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '15, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/2979cb2d6260821b39d210090553f342?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sunil%20M&#39;s gravatar image" /><p><span>Sunil M</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sunil M has no accepted answers">0%</span></p></div></div><div id="comments-container-41588" class="comments-container"></div><div id="comment-tools-41588" class="comment-tools"></div><div class="clear"></div><div id="comment-41588-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

