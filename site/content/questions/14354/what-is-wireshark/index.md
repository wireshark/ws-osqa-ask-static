+++
type = "question"
title = "what IS wireshark"
description = '''All, Just passing through, but there are so many things about this site which, on first glance, seem to be so very cool, that I thought I&#x27;d make a comment. I got here by Googling &#x27;wireshark&#x27;--I read a reference to it in a context that made me want to know more--but, getting here, I can&#x27;t find a sing...'''
date = "2012-09-18T11:47:00Z"
lastmod = "2012-09-18T16:55:00Z"
weight = 14354
keywords = [ "wireshark" ]
aliases = [ "/questions/14354" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [what IS wireshark](/questions/14354/what-is-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14354-score" class="post-score" title="current number of votes">0</div><span id="post-14354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>All,</p><p>Just passing through, but there are so many things about this site which, on first glance, seem to be so very cool, that I thought I'd make a comment. I got here by Googling 'wireshark'--I read a reference to it in a context that made me want to know more--but, getting here, I can't find a single thing that explains what wireshark <em>is</em>. The FAQs explain the rules of the forum very well, but there seems to be no FAQ about the software itself.<br />
</p><p>Now, I'll go back to Google and continue trying to find out what wireshark is.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '12, 11:47</strong></p><img src="https://secure.gravatar.com/avatar/253daf544821b140f4dfa14376547309?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rvja&#39;s gravatar image" /><p><span>rvja</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rvja has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-14354" class="comments-container"></div><div id="comment-tools-14354" class="comment-tools"></div><div class="clear"></div><div id="comment-14354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="14355"></span>

<div id="answer-container-14355" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14355-score" class="post-score" title="current number of votes">3</div><span id="post-14355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://wireshark.org">wireshark.org</a> ! Get Help ! FAQs</p><p><a href="https://www.wireshark.org/faq.html">Wireshark FAQ</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Sep '12, 11:51</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Sep '12, 11:52</strong> </span></p></div></div><div id="comments-container-14355" class="comments-container"></div><div id="comment-tools-14355" class="comment-tools"></div><div class="clear"></div><div id="comment-14355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14366"></span>

<div id="answer-container-14366" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14366-score" class="post-score" title="current number of votes">1</div><span id="post-14366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a <a href="http://en.wikipedia.org/wiki/Packet_analyzer">packet analyzer</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Sep '12, 16:55</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-14366" class="comments-container"></div><div id="comment-tools-14366" class="comment-tools"></div><div class="clear"></div><div id="comment-14366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

