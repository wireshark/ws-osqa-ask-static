+++
type = "question"
title = "Tcp retransmission for some packets within 40 ms"
description = '''Hi All, why are there so many retransmission with less than 40 millisecond.as per my understanding retransmission timer is generally 2*RTT so is it because of less RTT.I have collected this trace from hansang 2013 sharkfest collection on https://box.com/sharkfest2013 but i have uploaded it here, htt...'''
date = "2014-07-28T06:45:00Z"
lastmod = "2014-07-28T06:45:00Z"
weight = 34942
keywords = [ "tcp" ]
aliases = [ "/questions/34942" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Tcp retransmission for some packets within 40 ms](/questions/34942/tcp-retransmission-for-some-packets-within-40-ms)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34942-score" class="post-score" title="current number of votes">0</div><span id="post-34942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, why are there so many retransmission with less than 40 millisecond.as per my understanding retransmission timer is generally 2*RTT so is it because of less RTT.I have collected this trace from hansang 2013 sharkfest collection on <a href="https://box.com/sharkfest2013">https://box.com/sharkfest2013</a> but i have uploaded it here, <a href="https://www.cloudshark.org/captures/d777543cd470">https://www.cloudshark.org/captures/d777543cd470</a> .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '14, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/6f9cdab5081b4272d1abf703a2689372?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kishan%20pandey&#39;s gravatar image" /><p><span>kishan pandey</span><br />
<span class="score" title="221 reputation points">221</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="29 badges"><span class="silver">●</span><span class="badgecount">29</span></span><span title="36 badges"><span class="bronze">●</span><span class="badgecount">36</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kishan pandey has 2 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Jul '14, 13:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-34942" class="comments-container"></div><div id="comment-tools-34942" class="comment-tools"></div><div class="clear"></div><div id="comment-34942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

