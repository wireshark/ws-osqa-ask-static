+++
type = "question"
title = "Add new layout"
description = '''Hi everyone, I would like to add a fourth window in the central widget for my QT widget. I already added it in the master___split_ in the _main_windows and now I want to add it in the layout preferences to give it a disposition.  I have search in ui/qt/layout_preferences_frame and in epan/prefs how ...'''
date = "2016-06-30T05:45:00Z"
lastmod = "2016-06-30T05:55:00Z"
weight = 53749
keywords = [ "layout", "qt", "preferences" ]
aliases = [ "/questions/53749" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Add new layout](/questions/53749/add-new-layout)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53749-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53749-score" class="post-score" title="current number of votes">0</div><span id="post-53749-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone,</p><p>I would like to add a fourth window in the central widget for my QT widget. I already added it in the <strong>master___split_</strong> in the <strong>_main_windows</strong> and now I want to add it in the layout preferences to give it a disposition.</p><p>I have search in ui/qt/layout<strong><em>_</em></strong>preferences<strong><em>_</em></strong>frame and in epan/prefs how <strong>layout___type_5</strong> gives the description of windows module disposition but I didn't find the solution</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-layout" rel="tag" title="see questions tagged &#39;layout&#39;">layout</span> <span class="post-tag tag-link-qt" rel="tag" title="see questions tagged &#39;qt&#39;">qt</span> <span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '16, 05:45</strong></p><img src="https://secure.gravatar.com/avatar/1e089af1440cad240cf3e9651ec1b2fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stezerow&#39;s gravatar image" /><p><span>stezerow</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stezerow has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jun '16, 05:49</strong> </span></p></div></div><div id="comments-container-53749" class="comments-container"><span id="53751"></span><div id="comment-53751" class="comment"><div id="post-53751-score" class="comment-score"></div><div class="comment-text"><p>You should go to the <a href="https://www.wireshark.org/lists/">wireshark developer mailing list</a>.</p></div><div id="comment-53751-info" class="comment-info"><span class="comment-age">(30 Jun '16, 05:55)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-53749" class="comment-tools"></div><div class="clear"></div><div id="comment-53749-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

