+++
type = "question"
title = "patch file for 802.15.4"
description = '''hi currently i am doing my project in wireless sensor networks,and the simulator that i am working is NS2. i have decided of using 802.15.4 as MAC protocol. so can any one of you help me how to get the patch file for it. is there any website for downloading it. please help me. its urgent please'''
date = "2011-09-23T08:01:00Z"
lastmod = "2011-09-23T11:11:00Z"
weight = 6512
keywords = [ "file", "patch" ]
aliases = [ "/questions/6512" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [patch file for 802.15.4](/questions/6512/patch-file-for-802154)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6512-score" class="post-score" title="current number of votes">0</div><span id="post-6512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi currently i am doing my project in wireless sensor networks,and the simulator that i am working is NS2. i have decided of using 802.15.4 as MAC protocol. so can any one of you help me how to get the patch file for it. is there any website for downloading it. please help me. its urgent please</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-patch" rel="tag" title="see questions tagged &#39;patch&#39;">patch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '11, 08:01</strong></p><img src="https://secure.gravatar.com/avatar/113242d02d8f33a4a272ad00c36c07db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stefy89&#39;s gravatar image" /><p><span>stefy89</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stefy89 has no accepted answers">0%</span></p></div></div><div id="comments-container-6512" class="comments-container"></div><div id="comment-tools-6512" class="comment-tools"></div><div class="clear"></div><div id="comment-6512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6520"></span>

<div id="answer-container-6520" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6520-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6520-score" class="post-score" title="current number of votes">0</div><span id="post-6520-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p><em>How to get the patch file for it?</em></p></blockquote><p>I'm not exactly sure what you mean by that, but if you're looking for the 802.15.4 dissector source code, you can download it with the rest of the latest stable Wireshark sources from the Wireshark <a href="http://www.wireshark.org/download.html">download</a> page, extract the archive and then look in epan/dissectors/ for the packet-ieee802154.c file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '11, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-6520" class="comments-container"></div><div id="comment-tools-6520" class="comment-tools"></div><div class="clear"></div><div id="comment-6520-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

