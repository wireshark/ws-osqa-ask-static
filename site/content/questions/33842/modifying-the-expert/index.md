+++
type = "question"
title = "Modifying the Expert"
description = '''Sir, Would it be possible to add the capability for the user to modify the Expert? It would be nice to be able to modify which situations would fall under certain severity levels. If we would be able to save those settings to our profiles as well I feel this would greatly benefit many users of Wires...'''
date = "2014-06-15T13:40:00Z"
lastmod = "2014-06-15T14:35:00Z"
weight = 33842
keywords = [ "expert-info", "modify", "expert", "wireshark" ]
aliases = [ "/questions/33842" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Modifying the Expert](/questions/33842/modifying-the-expert)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33842-score" class="post-score" title="current number of votes">0</div><span id="post-33842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sir,</p><p>Would it be possible to add the capability for the user to modify the Expert? It would be nice to be able to modify which situations would fall under certain severity levels. If we would be able to save those settings to our profiles as well I feel this would greatly benefit many users of Wireshark. Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-expert-info" rel="tag" title="see questions tagged &#39;expert-info&#39;">expert-info</span> <span class="post-tag tag-link-modify" rel="tag" title="see questions tagged &#39;modify&#39;">modify</span> <span class="post-tag tag-link-expert" rel="tag" title="see questions tagged &#39;expert&#39;">expert</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jun '14, 13:40</strong></p><img src="https://secure.gravatar.com/avatar/8988cfa4b1abc8a6990290d638bb7d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zasher&#39;s gravatar image" /><p><span>zasher</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zasher has no accepted answers">0%</span></p></div></div><div id="comments-container-33842" class="comments-container"></div><div id="comment-tools-33842" class="comment-tools"></div><div class="clear"></div><div id="comment-33842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33844"></span>

<div id="answer-container-33844" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33844-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33844-score" class="post-score" title="current number of votes">1</div><span id="post-33844-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="zasher has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Would it be possible to add the capability for the user to modify the Expert?</p></blockquote><p>Yes, it would be possible. If you need that functionality, please file an enhancement request at <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a>.</p><p>See also my answer to a very similar question:</p><blockquote><p><a href="http://ask.wireshark.org/questions/33841/customizable-expert-window">http://ask.wireshark.org/questions/33841/customizable-expert-window</a></p></blockquote><p>BTW: Is that question from you - using a different account??</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jun '14, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-33844" class="comments-container"><span id="33850"></span><div id="comment-33850" class="comment"><div id="post-33850-score" class="comment-score"></div><div class="comment-text"><p>Kurt,</p><p>I am in the WCNA bootcamp right now and I brought that question up here. More than likely it was someone else from this class. Sorry about the redundancy.</p></div><div id="comment-33850-info" class="comment-info"><span class="comment-age">(15 Jun '14, 14:27)</span> <span class="comment-user userinfo">zasher</span></div></div><span id="33851"></span><div id="comment-33851" class="comment"><div id="post-33851-score" class="comment-score"></div><div class="comment-text"><blockquote><p>More than likely it was someone else from this class.</p></blockquote><p>:-)) Funny. Where is that bootcamp and who is the trainer?</p></div><div id="comment-33851-info" class="comment-info"><span class="comment-age">(15 Jun '14, 14:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="33852"></span><div id="comment-33852" class="comment"><div id="post-33852-score" class="comment-score"></div><div class="comment-text"><p>The WCNA Bootcamp is at Dominican University in San Rafael, CA and the trainer is Laura Chappell.</p></div><div id="comment-33852-info" class="comment-info"><span class="comment-age">(15 Jun '14, 14:32)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div><span id="33853"></span><div id="comment-33853" class="comment"><div id="post-33853-score" class="comment-score"></div><div class="comment-text"><p>Congrats, she is one of the best. Greetings to her from the ask wireshark 'folks'.</p></div><div id="comment-33853-info" class="comment-info"><span class="comment-age">(15 Jun '14, 14:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33844" class="comment-tools"></div><div class="clear"></div><div id="comment-33844-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

