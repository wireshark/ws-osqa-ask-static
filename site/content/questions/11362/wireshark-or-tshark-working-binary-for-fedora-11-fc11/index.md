+++
type = "question"
title = "Wireshark or TShark Working Binary for Fedora 11 (FC11)?"
description = '''Is there a working binary for Fedora 11 (FC11)? We are struggling to get it to work. Any suggestions?'''
date = "2012-05-25T15:49:00Z"
lastmod = "2012-05-25T15:57:00Z"
weight = 11362
keywords = [ "linux-support", "fedora" ]
aliases = [ "/questions/11362" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark or TShark Working Binary for Fedora 11 (FC11)?](/questions/11362/wireshark-or-tshark-working-binary-for-fedora-11-fc11)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11362-score" class="post-score" title="current number of votes">0</div><span id="post-11362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a working binary for Fedora 11 (FC11)? We are struggling to get it to work. Any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux-support" rel="tag" title="see questions tagged &#39;linux-support&#39;">linux-support</span> <span class="post-tag tag-link-fedora" rel="tag" title="see questions tagged &#39;fedora&#39;">fedora</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '12, 15:49</strong></p><img src="https://secure.gravatar.com/avatar/a9a455ea7eafb101b8e7ed9d0c6caf8f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="duquet&#39;s gravatar image" /><p><span>duquet</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="duquet has no accepted answers">0%</span></p></div></div><div id="comments-container-11362" class="comments-container"></div><div id="comment-tools-11362" class="comment-tools"></div><div class="clear"></div><div id="comment-11362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11363"></span>

<div id="answer-container-11363" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11363-score" class="post-score" title="current number of votes">0</div><span id="post-11363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>what happens if you run this command:</p><blockquote><p><code>yum install wireshark wireshark-gui</code></p></blockquote><p>BTW: What are your problems with the binary?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 May '12, 15:57</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-11363" class="comments-container"></div><div id="comment-tools-11363" class="comment-tools"></div><div class="clear"></div><div id="comment-11363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

