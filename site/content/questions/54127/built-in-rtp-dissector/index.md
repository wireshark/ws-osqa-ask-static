+++
type = "question"
title = "built in rtp dissector"
description = '''hi all, i have built my own rtp dissector adding some other info comments in debug log and built it.it is working fine.if i have to share to my friend how can i do that.? because there is no rtp dll in wireshark binary if we download. so to share is it possible.?'''
date = "2016-07-18T08:17:00Z"
lastmod = "2016-07-18T09:03:00Z"
weight = 54127
keywords = [ "rtp" ]
aliases = [ "/questions/54127" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [built in rtp dissector](/questions/54127/built-in-rtp-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54127-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54127-score" class="post-score" title="current number of votes">0</div><span id="post-54127-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi all,</p><p>i have built my own rtp dissector adding some other info comments in debug log and built it.it is working fine.if i have to share to my friend how can i do that.? because there is no rtp dll in wireshark binary if we download. so to share is it possible.?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '16, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/ac2e596b118c8fe00a8ffab91d2f650f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rose_us&#39;s gravatar image" /><p><span>rose_us</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rose_us has no accepted answers">0%</span></p></div></div><div id="comments-container-54127" class="comments-container"></div><div id="comment-tools-54127" class="comment-tools"></div><div class="clear"></div><div id="comment-54127-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54128"></span>

<div id="answer-container-54128" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54128-score" class="post-score" title="current number of votes">1</div><span id="post-54128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As you mention a DLL, I'm assuming you're using Windows. If so, then you can <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html#_optional_create_a_wireshark_installer">create</a> your own installer and distribute that to your friend.</p><p>Note that as Wireshark is licenced under the GPL, then when you distribute the installer you must make an offer for the source code for your changes as well.</p><p>If you feel your changes are useful, why not <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSrcContribute.html">submit</a> them back to the Wireshark project?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '16, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-54128" class="comments-container"></div><div id="comment-tools-54128" class="comment-tools"></div><div class="clear"></div><div id="comment-54128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

