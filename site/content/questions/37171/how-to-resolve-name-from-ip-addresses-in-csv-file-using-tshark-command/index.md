+++
type = "question"
title = "how to resolve name from ip addresses in .csv file using tshark command"
description = '''please mention full command not syntax'''
date = "2014-10-19T23:36:00Z"
lastmod = "2014-10-20T01:25:00Z"
weight = 37171
keywords = [ "using", "resolve", "name", "tshark" ]
aliases = [ "/questions/37171" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to resolve name from ip addresses in .csv file using tshark command](/questions/37171/how-to-resolve-name-from-ip-addresses-in-csv-file-using-tshark-command)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37171-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37171-score" class="post-score" title="current number of votes">0</div><span id="post-37171-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>please mention full command not syntax</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-using" rel="tag" title="see questions tagged &#39;using&#39;">using</span> <span class="post-tag tag-link-resolve" rel="tag" title="see questions tagged &#39;resolve&#39;">resolve</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '14, 23:36</strong></p><img src="https://secure.gravatar.com/avatar/7947f4a57793d0452f09b2227900f4ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Manpreet%20Singh&#39;s gravatar image" /><p><span>Manpreet Singh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Manpreet Singh has no accepted answers">0%</span></p></div></div><div id="comments-container-37171" class="comments-container"></div><div id="comment-tools-37171" class="comment-tools"></div><div class="clear"></div><div id="comment-37171-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37174"></span>

<div id="answer-container-37174" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37174-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37174-score" class="post-score" title="current number of votes">0</div><span id="post-37174-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you mean you want to give tshark a CSV file that contains hostname&lt;-&gt;ip address map and have tshark use it to resolve host names from the IP addresses in the capture file?</p><p>If so, you can't do that. tshark will read files in <code>hosts</code> file format but not CSV.</p></div><div class="answer-controls post-controls"><div class="community-wiki">This answer is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '14, 01:25</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37174" class="comments-container"></div><div id="comment-tools-37174" class="comment-tools"></div><div class="clear"></div><div id="comment-37174-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

