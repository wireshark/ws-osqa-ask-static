+++
type = "question"
title = "Please tell me if there is any malicious activities going on in this pcap file image?"
description = ''''''
date = "2017-10-21T19:51:00Z"
lastmod = "2017-10-22T01:53:00Z"
weight = 64075
keywords = [ "pcap" ]
aliases = [ "/questions/64075" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Please tell me if there is any malicious activities going on in this pcap file image?](/questions/64075/please-tell-me-if-there-is-any-malicious-activities-going-on-in-this-pcap-file-image)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64075-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64075-score" class="post-score" title="current number of votes">0</div><span id="post-64075-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2017-10-22_at_09.31.23_irLGHCi.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '17, 19:51</strong></p><img src="https://secure.gravatar.com/avatar/affade37d25ba3518a824b836007bb8f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ravindu%20Dilshan&#39;s gravatar image" /><p><span>Ravindu Dilshan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ravindu Dilshan has no accepted answers">0%</span></p></img></div></div><div id="comments-container-64075" class="comments-container"><span id="64080"></span><div id="comment-64080" class="comment"><div id="post-64080-score" class="comment-score"></div><div class="comment-text"><p>What is 10.1.1.1?</p></div><div id="comment-64080-info" class="comment-info"><span class="comment-age">(22 Oct '17, 01:53)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-64075" class="comment-tools"></div><div class="clear"></div><div id="comment-64075-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

