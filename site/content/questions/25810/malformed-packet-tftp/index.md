+++
type = "question"
title = "Malformed Packet: TFTP"
description = '''Hi,  I try to send 0x80200000 data over udp socket. I got this error. when i change data to 0x80200001 or 0x10100000 i can send it without problem. Didnt understand why i got this error... Internet Protocol Version 4, Src: 192.168.0.202 (192.168.0.202), Dst: 192.168.0.80 (192.168.0.80) User Datagram...'''
date = "2013-10-09T06:29:00Z"
lastmod = "2013-10-09T07:16:00Z"
weight = 25810
keywords = [ "tftp", "hex", "socket", "malformed" ]
aliases = [ "/questions/25810" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed Packet: TFTP](/questions/25810/malformed-packet-tftp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25810-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25810-score" class="post-score" title="current number of votes">0</div><span id="post-25810-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I try to send 0x80200000 data over udp socket. I got this error. when i change data to 0x80200001 or 0x10100000 i can send it without problem. Didnt understand why i got this error...</p><pre><code>Internet Protocol Version 4, Src:
192.168.0.202 (192.168.0.202), Dst: 192.168.0.80 (192.168.0.80) User Datagram Protocol, Src Port: hpvmmagent (1125), Dst Port: danf-ak2 (1041)

[Malformed Packet: TFTP] Expert Info (Error/Malformed): Malformed Packet (Exception occurred) Message: Malformed Packet (Exception occurred) Severity level: Error Group: Malformed</code></pre><p>I need your suggestions. Thank you, Can</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tftp" rel="tag" title="see questions tagged &#39;tftp&#39;">tftp</span> <span class="post-tag tag-link-hex" rel="tag" title="see questions tagged &#39;hex&#39;">hex</span> <span class="post-tag tag-link-socket" rel="tag" title="see questions tagged &#39;socket&#39;">socket</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '13, 06:29</strong></p><img src="https://secure.gravatar.com/avatar/a28a522e4bc5245b48ddf889597027d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mcan&#39;s gravatar image" /><p><span>mcan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mcan has no accepted answers">0%</span></p></div></div><div id="comments-container-25810" class="comments-container"><span id="25815"></span><div id="comment-25815" class="comment"><div id="post-25815-score" class="comment-score"></div><div class="comment-text"><p>If you can provide that one frame of capture, such as a hex dump or k12text export or putting it on pastebin or clouldshark it would enable a useful response. Right now, you haven't yet provided quite enough information to be able to reproduce or diagnose the problem.</p></div><div id="comment-25815-info" class="comment-info"><span class="comment-age">(09 Oct '13, 07:16)</span> <span class="comment-user userinfo">beroset</span></div></div></div><div id="comment-tools-25810" class="comment-tools"></div><div class="clear"></div><div id="comment-25810-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

