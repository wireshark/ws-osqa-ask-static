+++
type = "question"
title = "LTE interfaces support in wireshark"
description = '''wireshark will support all LTE interfaces (as per 3GPP rel.9) packet capturing?'''
date = "2015-05-18T20:48:00Z"
lastmod = "2015-05-18T21:00:00Z"
weight = 42522
keywords = [ "capture", "packet", "lte" ]
aliases = [ "/questions/42522" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [LTE interfaces support in wireshark](/questions/42522/lte-interfaces-support-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42522-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42522-score" class="post-score" title="current number of votes">0</div><span id="post-42522-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>wireshark will support all LTE interfaces (as per 3GPP rel.9) packet capturing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-lte" rel="tag" title="see questions tagged &#39;lte&#39;">lte</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 May '15, 20:48</strong></p><img src="https://secure.gravatar.com/avatar/687886a68d9ff0e86d09292a6c24f6bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ttsl&#39;s gravatar image" /><p><span>ttsl</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ttsl has no accepted answers">0%</span></p></div></div><div id="comments-container-42522" class="comments-container"><span id="42523"></span><div id="comment-42523" class="comment"><div id="post-42523-score" class="comment-score"></div><div class="comment-text"><p>Depends on how broadly you define "LTE interfaces". Are there specific protocols you have in mind?</p><p>Also not sure about your use of the word "will". Wireshark already supports plenty of LTE-related protocols, but understand that the industry's effective use of that acronym is incredibly broad so it's hard to answer. LTE was literally the name of 3GPP's Release 8 radio project, but it's evolved to be used to refer to many things including even IMS so you need to be more specific here about what you're asking.</p></div><div id="comment-42523-info" class="comment-info"><span class="comment-age">(18 May '15, 21:00)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-42522" class="comment-tools"></div><div class="clear"></div><div id="comment-42522-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

