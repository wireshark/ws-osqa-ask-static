+++
type = "question"
title = "TCP DUP ACK/TCP Retransmission"
description = '''Dear Guys, I have been met one issue recently. I do VPN-site-to-site with my partner. Long time ago is no issue. just a few week issue happen with &quot;TCP DUP ACK/TCP Retransmission&quot;. I don&#x27;t know this issue on Network or Server part. Please advice me how to troubleshooting this issue. Best Regards, Si...'''
date = "2017-10-24T08:18:00Z"
lastmod = "2017-10-24T08:18:00Z"
weight = 64156
keywords = [ "tcp_retransmission" ]
aliases = [ "/questions/64156" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP DUP ACK/TCP Retransmission](/questions/64156/tcp-dup-acktcp-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64156-score" class="post-score" title="current number of votes">0</div><span id="post-64156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Guys,</p><p>I have been met one issue recently. I do VPN-site-to-site with my partner. Long time ago is no issue. just a few week issue happen with "TCP DUP ACK/TCP Retransmission". I don't know this issue on Network or Server part.</p><p>Please advice me how to troubleshooting this issue.</p><p>Best Regards, Simon</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp_retransmission" rel="tag" title="see questions tagged &#39;tcp_retransmission&#39;">tcp_retransmission</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '17, 08:18</strong></p><img src="https://secure.gravatar.com/avatar/da7a56e3422729701f2e10a05166ef16?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Simon123&#39;s gravatar image" /><p><span>Simon123</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Simon123 has no accepted answers">0%</span></p></div></div><div id="comments-container-64156" class="comments-container"></div><div id="comment-tools-64156" class="comment-tools"></div><div class="clear"></div><div id="comment-64156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

