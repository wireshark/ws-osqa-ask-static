+++
type = "question"
title = "The capture session could not be initiated (just on windows) ?"
description = '''Hello. When i try to capture on windows seven i get this error : &quot;The capture session could not be initiated (failed to set hardware filter to promiscuous mode). Please check that &quot;DeviceNPF_{2879FC56-FA35-48DF-A0E7-6A2532417BFF}&quot; is the proper interface.&quot; I made i search about that and i found that...'''
date = "2011-09-17T10:10:00Z"
lastmod = "2011-09-17T23:51:00Z"
weight = 6433
keywords = [ "windows", "promiscuous", "mode", "linux" ]
aliases = [ "/questions/6433" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [The capture session could not be initiated (just on windows) ?](/questions/6433/the-capture-session-could-not-be-initiated-just-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6433-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6433-score" class="post-score" title="current number of votes">0</div><span id="post-6433-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello.</p><p>When i try to capture on windows seven i get this error :</p><p>"The capture session could not be initiated (failed to set hardware filter to promiscuous mode). Please check that "DeviceNPF_{2879FC56-FA35-48DF-A0E7-6A2532417BFF}" is the proper interface."</p><p>I made i search about that and i found that it was impossible de do that on windows without deactivating the promiscuous mode. So is that possible on Linux Ubuntu with the same wire adapter ?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Sep '11, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/3f15bdcbd6762973095be20deb3d1d43?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="firephoenix&#39;s gravatar image" /><p><span>firephoenix</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="firephoenix has no accepted answers">0%</span></p></div></div><div id="comments-container-6433" class="comments-container"><span id="6435"></span><div id="comment-6435" class="comment"><div id="post-6435-score" class="comment-score"></div><div class="comment-text"><p>Is the adapter in question an Ethernet adapter, a Wi-Fi adapter, or some other type of adapter?</p></div><div id="comment-6435-info" class="comment-info"><span class="comment-age">(17 Sep '11, 23:51)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-6433" class="comment-tools"></div><div class="clear"></div><div id="comment-6433-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

