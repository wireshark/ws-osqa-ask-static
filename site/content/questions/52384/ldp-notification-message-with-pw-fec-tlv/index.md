+++
type = "question"
title = "LDP notification message with PW-FEC-TLV"
description = '''In a particular scenario there was requirement for LDP notification message to have PW-FEC-TLV with only group ID present in it along with PW-info-length of FEC-TLV set to 0. In a normal scenario the packet is supposed to have non-zero PW-info-length with PW-ID field filled. WHen I modified the pack...'''
date = "2016-05-09T23:59:00Z"
lastmod = "2016-05-11T14:35:00Z"
weight = 52384
keywords = [ "malformed-packet", "pw-fec-tlv", "ldp", "status-tlv" ]
aliases = [ "/questions/52384" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [LDP notification message with PW-FEC-TLV](/questions/52384/ldp-notification-message-with-pw-fec-tlv)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52384-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52384-score" class="post-score" title="current number of votes">0</div><span id="post-52384-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In a particular scenario there was requirement for LDP notification message to have PW-FEC-TLV with only group ID present in it along with PW-info-length of FEC-TLV set to 0. In a normal scenario the packet is supposed to have non-zero PW-info-length with PW-ID field filled.</p><p>WHen I modified the packet according to the requirements i am getting an error saying it is a malformed packet. Can somebody help me what might be wrong in the packet. Have attached the packet in one of my comment below</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malformed-packet" rel="tag" title="see questions tagged &#39;malformed-packet&#39;">malformed-packet</span> <span class="post-tag tag-link-pw-fec-tlv" rel="tag" title="see questions tagged &#39;pw-fec-tlv&#39;">pw-fec-tlv</span> <span class="post-tag tag-link-ldp" rel="tag" title="see questions tagged &#39;ldp&#39;">ldp</span> <span class="post-tag tag-link-status-tlv" rel="tag" title="see questions tagged &#39;status-tlv&#39;">status-tlv</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '16, 23:59</strong></p><img src="https://secure.gravatar.com/avatar/de4a93c9d5404f6e7a1515eafdd43a3a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="muttanna2972&#39;s gravatar image" /><p><span>muttanna2972</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="muttanna2972 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 May '16, 22:58</strong> </span></p></div></div><div id="comments-container-52384" class="comments-container"><span id="52388"></span><div id="comment-52388" class="comment"><div id="post-52388-score" class="comment-score">1</div><div class="comment-text"><p>Show us a capture file (not text dump!) of such packet.</p></div><div id="comment-52388-info" class="comment-info"><span class="comment-age">(10 May '16, 01:30)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="52420"></span><div id="comment-52420" class="comment"><div id="post-52420-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jaap for the response. Please find the packet attached <img src="https://osqa-ask.wireshark.org/upfiles/malformed_packet.jpg.png" alt="alt text" /></p></div><div id="comment-52420-info" class="comment-info"><span class="comment-age">(10 May '16, 22:57)</span> <span class="comment-user userinfo">muttanna2972</span></div></div><span id="52451"></span><div id="comment-52451" class="comment"><div id="post-52451-score" class="comment-score"></div><div class="comment-text"><p>That's a screenshot of text, same thing, not useful.</p></div><div id="comment-52451-info" class="comment-info"><span class="comment-age">(11 May '16, 14:35)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-52384" class="comment-tools"></div><div class="clear"></div><div id="comment-52384-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

