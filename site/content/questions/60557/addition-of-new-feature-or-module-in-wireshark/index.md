+++
type = "question"
title = "Addition of new feature or module in WireShark"
description = '''Can we add any new feature into wireshark? like; 1- Live graphical representation on run time,  2- To block any node after capturing malicious activity packets, IF yes, then can we use any language to add fearure, like python, lua, c# etc...'''
date = "2017-04-03T22:59:00Z"
lastmod = "2017-04-04T00:46:00Z"
weight = 60557
keywords = [ "feature-request" ]
aliases = [ "/questions/60557" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Addition of new feature or module in WireShark](/questions/60557/addition-of-new-feature-or-module-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60557-score" class="post-score" title="current number of votes">0</div><span id="post-60557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can we add any new feature into wireshark? like; 1- Live graphical representation on run time, 2- To block any node after capturing malicious activity packets, IF yes, then can we use any language to add fearure, like python, lua, c# etc...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-feature-request" rel="tag" title="see questions tagged &#39;feature-request&#39;">feature-request</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Apr '17, 22:59</strong></p><img src="https://secure.gravatar.com/avatar/7dad232771e1aebbe460d0b329485ee1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alihassanws&#39;s gravatar image" /><p><span>alihassanws</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alihassanws has no accepted answers">0%</span></p></div></div><div id="comments-container-60557" class="comments-container"></div><div id="comment-tools-60557" class="comment-tools"></div><div class="clear"></div><div id="comment-60557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="60558"></span>

<div id="answer-container-60558" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60558-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60558-score" class="post-score" title="current number of votes">0</div><span id="post-60558-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can develop any feature you like, it's FOSS for a reason. If you want to distribute Wireshark with your new features make sure to understand the GPL, you have to allow others to have the source code of your new features as well. If you want to have your features included into mainstream Wireshark, then your options are limited to what's described in the README files found in the source code repository and what the core developers decide they are willing to support. So in that case it would be wise to put forward your plans on the wireshark-dev mailing list first to discuss and get feedback on your plans.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Apr '17, 23:42</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60558" class="comments-container"></div><div id="comment-tools-60558" class="comment-tools"></div><div class="clear"></div><div id="comment-60558-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="60560"></span>

<div id="answer-container-60560" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60560-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60560-score" class="post-score" title="current number of votes">0</div><span id="post-60560-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>then can we use any language to add fearure, like python, lua, c# etc..</p></blockquote><p>Wireshark is written mostly in C, with the Qt GUI code written in C++. It can be built with an embedded Lua interpreter; there are no other embedded languages.</p><p>So you can use C++ for the GUI code for features in the Qt UI, and C (or C++) for features in TShark, the GTK+ UI (now deprecated), or the Wireshark core (for use in both Wireshark and TShark). You can also use Lua; most versions of Wireshark are built with the embedded Lua interpreter (Lua 5.2 - we don't support Lua 5.3), so the feature should work in most versions of Wireshark on most platforms.</p><p>To use Python or C#, you would have to write your own code to allow that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '17, 00:46</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-60560" class="comments-container"></div><div id="comment-tools-60560" class="comment-tools"></div><div class="clear"></div><div id="comment-60560-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

