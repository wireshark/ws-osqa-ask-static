+++
type = "question"
title = "http.request.method == &quot;PASS&quot;"
description = '''I run Wireshark on my laptop and try to log into a website with another laptop that is connected to the same wifi but this filter doesn&#x27;t show any result on my screen. I see a bunch of ARP protocol showing that the computer I tried to log in is visible I&#x27;ve also tried this filter http.request.method...'''
date = "2017-06-03T18:19:00Z"
lastmod = "2017-06-04T02:48:00Z"
weight = 61762
keywords = [ "sniffing.", "password", "wireshark" ]
aliases = [ "/questions/61762" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [http.request.method == "PASS"](/questions/61762/httprequestmethod-pass)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61762-score" class="post-score" title="current number of votes">0</div><span id="post-61762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I run Wireshark on my laptop and try to log into a website with another laptop that is connected to the same wifi but this filter doesn't show any result on my screen. I see a bunch of ARP protocol showing that the computer I tried to log in is visible I've also tried this filter http.request.method == "GET".</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing." rel="tag" title="see questions tagged &#39;sniffing.&#39;">sniffing.</span> <span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '17, 18:19</strong></p><img src="https://secure.gravatar.com/avatar/31cfbb9be64efd03e9c9f21e3940df87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ecakir&#39;s gravatar image" /><p><span>ecakir</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ecakir has no accepted answers">0%</span></p></div></div><div id="comments-container-61762" class="comments-container"></div><div id="comment-tools-61762" class="comment-tools"></div><div class="clear"></div><div id="comment-61762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61764"></span>

<div id="answer-container-61764" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61764-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61764-score" class="post-score" title="current number of votes">0</div><span id="post-61764-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll need to read the Wiki page on <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">Wireless Capture</a>, you can't simply capture the wireless traffic of another device.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '17, 02:48</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61764" class="comments-container"></div><div id="comment-tools-61764" class="comment-tools"></div><div class="clear"></div><div id="comment-61764-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

