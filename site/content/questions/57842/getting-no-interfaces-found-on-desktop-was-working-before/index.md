+++
type = "question"
title = "getting no interfaces found on desktop, was working before."
description = '''used wireshark maybe few months ago interfaces showed up fine opened it recently and for some reason they are not showing up. wireshark is up to date so it winpcap. running on windows 7 64 bit. Tried run as admin that was first thing i tried,  tried these commands  Ipconfig /all Ping 127.0.0.1 Ipcon...'''
date = "2016-12-04T10:47:00Z"
lastmod = "2016-12-08T09:40:00Z"
weight = 57842
keywords = [ "interface", "no" ]
aliases = [ "/questions/57842" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [getting no interfaces found on desktop, was working before.](/questions/57842/getting-no-interfaces-found-on-desktop-was-working-before)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57842-score" class="post-score" title="current number of votes">0</div><span id="post-57842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>used wireshark maybe few months ago interfaces showed up fine opened it recently and for some reason they are not showing up. wireshark is up to date so it winpcap. running on windows 7 64 bit. Tried run as admin that was first thing i tried, tried these commands Ipconfig /all</p><p>Ping 127.0.0.1</p><p>Ipconfig /release</p><p>Ipconfig /renew</p><p>Ipconfig /flushdns</p><p>Netsh winsock reset</p><p>Netsh int ip reset resetlog.txt</p><p>NBTSTAT -R</p><p>NBTSTAT -RR</p><p>Run Prompt</p><p>Regsvr32 netshell.dll</p><p>Regsvr32 netvfgx.dll</p><p>Regsvr32 netman.dll</p><p>None of these seemed to work. also uninstalled wireshark and re installed it and seen on here someone said try to install winpcap from the direct site that didn't work as well.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-no" rel="tag" title="see questions tagged &#39;no&#39;">no</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '16, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/f012fbc28f787dccdfdc17fca91c85b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NickB1989&#39;s gravatar image" /><p><span>NickB1989</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NickB1989 has no accepted answers">0%</span></p></div></div><div id="comments-container-57842" class="comments-container"></div><div id="comment-tools-57842" class="comment-tools"></div><div class="clear"></div><div id="comment-57842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57963"></span>

<div id="answer-container-57963" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57963-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57963-score" class="post-score" title="current number of votes">0</div><span id="post-57963-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Never mind. Turns out it was npcap adapter. I had it disabled but guess that wasn't enough. uninstalled it. now wireshark works.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '16, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/f012fbc28f787dccdfdc17fca91c85b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NickB1989&#39;s gravatar image" /><p><span>NickB1989</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NickB1989 has no accepted answers">0%</span></p></div></div><div id="comments-container-57963" class="comments-container"></div><div id="comment-tools-57963" class="comment-tools"></div><div class="clear"></div><div id="comment-57963-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

