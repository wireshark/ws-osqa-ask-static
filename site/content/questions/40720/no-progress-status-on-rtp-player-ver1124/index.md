+++
type = "question"
title = "No Progress status on RTP Player ver.1.12.4"
description = '''Hi, There is no progress status on the RTP Player under: Telephony &amp;gt;&amp;gt; VoIP Calls &amp;gt;&amp;gt; Player &amp;gt;&amp;gt; Decode &amp;gt;&amp;gt; RTP Player. once either RTP stream is played on other earlyer Wiresahrk version there was Call Progress bar on the RTP player. has this been removed or is the a bug ? Regar...'''
date = "2015-03-20T04:36:00Z"
lastmod = "2015-03-23T01:23:00Z"
weight = 40720
keywords = [ "voipcalls", "player", "status", "rtp" ]
aliases = [ "/questions/40720" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No Progress status on RTP Player ver.1.12.4](/questions/40720/no-progress-status-on-rtp-player-ver1124)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40720-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40720-score" class="post-score" title="current number of votes">0</div><span id="post-40720-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>There is no progress status on the RTP Player under:</p><p>Telephony &gt;&gt; VoIP Calls &gt;&gt; Player &gt;&gt; Decode &gt;&gt; RTP Player.</p><p>once either RTP stream is played on other earlyer Wiresahrk version there was Call Progress bar on the RTP player.</p><p>has this been removed or is the a bug ?</p><p>Regards</p><p>GTull</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span> <span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-status" rel="tag" title="see questions tagged &#39;status&#39;">status</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '15, 04:36</strong></p><img src="https://secure.gravatar.com/avatar/b5fe15b4e28edae2b0a63b6937421b7a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gtull&#39;s gravatar image" /><p><span>Gtull</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gtull has no accepted answers">0%</span></p></div></div><div id="comments-container-40720" class="comments-container"><span id="40721"></span><div id="comment-40721" class="comment"><div id="post-40721-score" class="comment-score"></div><div class="comment-text"><p>What is your OS and which version and what is your Wireshark version?</p></div><div id="comment-40721-info" class="comment-info"><span class="comment-age">(20 Mar '15, 05:20)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="40723"></span><div id="comment-40723" class="comment"><div id="post-40723-score" class="comment-score"></div><div class="comment-text"><p>Hi Jaap.</p><p>I am using windows 7 o64 bit, with Wireshark version 1.12.4.<br />
</p><p>we have tested on another older version 1.10.8 of wireshark and we can see the RTP player call status progress bar.</p></div><div id="comment-40723-info" class="comment-info"><span class="comment-age">(20 Mar '15, 05:45)</span> <span class="comment-user userinfo">Gtull</span></div></div><span id="40775"></span><div id="comment-40775" class="comment"><div id="post-40775-score" class="comment-score"></div><div class="comment-text"><p>Hi Please can you confirm if there any issue ons version 1.12.4 and the RTP Player Status bar missing ?</p><p>Regards</p><p>G Tull</p></div><div id="comment-40775-info" class="comment-info"><span class="comment-age">(23 Mar '15, 01:23)</span> <span class="comment-user userinfo">Gtull</span></div></div></div><div id="comment-tools-40720" class="comment-tools"></div><div class="clear"></div><div id="comment-40720-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

