+++
type = "question"
title = "unable to capture sip packets"
description = '''Hi I have a windows 10 laptop with a Realtek USB GbE Family Controller, I am unable to capture any sip / rtp packets, I can see other packets and using wireshark on a MacBook works ok Thanks Mark'''
date = "2016-10-26T08:20:00Z"
lastmod = "2016-10-26T08:51:00Z"
weight = 56701
keywords = [ "capture", "sip" ]
aliases = [ "/questions/56701" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [unable to capture sip packets](/questions/56701/unable-to-capture-sip-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56701-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56701-score" class="post-score" title="current number of votes">0</div><span id="post-56701-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I have a windows 10 laptop with a Realtek USB GbE Family Controller, I am unable to capture any sip / rtp packets, I can see other packets and using wireshark on a MacBook works ok</p><p>Thanks</p><p>Mark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '16, 08:20</strong></p><img src="https://secure.gravatar.com/avatar/bc58b616fb492d4cf82ef587e60373b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="markuk99&#39;s gravatar image" /><p><span>markuk99</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="markuk99 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Oct '16, 08:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-56701" class="comments-container"><span id="56702"></span><div id="comment-56702" class="comment"><div id="post-56702-score" class="comment-score"></div><div class="comment-text"><p>Can you describe your setup in more detail, i.e. what type of interface you are capturing on (wired or wireless), and the relationship between the laptop and the sip endpoints?</p></div><div id="comment-56702-info" class="comment-info"><span class="comment-age">(26 Oct '16, 08:31)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="56703"></span><div id="comment-56703" class="comment"><div id="post-56703-score" class="comment-score"></div><div class="comment-text"><p>it looks like I have solved my problem, I had Symantec endpoint protection disabled but the firewall was still on ?? thanks</p></div><div id="comment-56703-info" class="comment-info"><span class="comment-age">(26 Oct '16, 08:51)</span> <span class="comment-user userinfo">markuk99</span></div></div></div><div id="comment-tools-56701" class="comment-tools"></div><div class="clear"></div><div id="comment-56701-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

