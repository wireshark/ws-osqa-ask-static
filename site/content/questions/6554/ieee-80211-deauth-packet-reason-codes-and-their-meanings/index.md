+++
type = "question"
title = "IEEE 802.11 deauth packet reason codes and their meanings"
description = '''I&#x27;m trying to understand the different meanings of the 802.11 deauth packets I&#x27;m receiving: I couldn&#x27;t find a document that describes what each reason code means i.e why the deauth packet was sent.'''
date = "2011-09-26T02:31:00Z"
lastmod = "2011-09-27T07:57:00Z"
weight = 6554
keywords = [ "802.11", "deauthentication" ]
aliases = [ "/questions/6554" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IEEE 802.11 deauth packet reason codes and their meanings](/questions/6554/ieee-80211-deauth-packet-reason-codes-and-their-meanings)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6554-score" class="post-score" title="current number of votes">0</div><span id="post-6554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to understand the different meanings of the 802.11 deauth packets I'm receiving: I couldn't find a document that describes what each reason code means i.e why the deauth packet was sent.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-deauthentication" rel="tag" title="see questions tagged &#39;deauthentication&#39;">deauthentication</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '11, 02:31</strong></p><img src="https://secure.gravatar.com/avatar/5d64d21de6598960bf2db61f1ca705cc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ddayan&#39;s gravatar image" /><p><span>ddayan</span><br />
<span class="score" title="41 reputation points">41</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ddayan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Sep '11, 16:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-6554" class="comments-container"></div><div id="comment-tools-6554" class="comment-tools"></div><div class="clear"></div><div id="comment-6554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6569"></span>

<div id="answer-container-6569" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6569-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6569-score" class="post-score" title="current number of votes">0</div><span id="post-6569-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The document in question is <a href="http://standards.ieee.org/getieee802/download/802.11-2007.pdf">IEEE Std 802.11-2007</a>, and you'll find the reason code values in section 7.3.1.7 "Reason Code field".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Sep '11, 16:52</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-6569" class="comments-container"><span id="6594"></span><div id="comment-6594" class="comment"><div id="post-6594-score" class="comment-score"></div><div class="comment-text"><p>already looked there, it gives you a small description about the reason codes but nothing about what triggered them. e.g. I got error code 3 which means "Deauthenticated because sending STA is leaving (or has left) IBSS or ESS". I was connected to an AP, no way that the AP was leaving the IBSS, so I guess it couldn't hear the AP anymore. but then how come it didn't use reason code 4 "Disassociated due to inactivity". the document doesn't specify much.</p></div><div id="comment-6594-info" class="comment-info"><span class="comment-age">(27 Sep '11, 06:48)</span> <span class="comment-user userinfo">ddayan</span></div></div><span id="6596"></span><div id="comment-6596" class="comment"><div id="post-6596-score" class="comment-score"></div><div class="comment-text"><p>Search the standards for the term "Deauthenticate". You may find what is applicable for your situation.</p></div><div id="comment-6596-info" class="comment-info"><span class="comment-age">(27 Sep '11, 07:57)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-6569" class="comment-tools"></div><div class="clear"></div><div id="comment-6569-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

