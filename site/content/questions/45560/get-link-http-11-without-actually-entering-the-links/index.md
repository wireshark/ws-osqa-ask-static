+++
type = "question"
title = "&quot;GET link HTTP /1.1&quot; without actually entering the links"
description = '''So, I&#x27;m suspecting from some malicious activiy on my PC so I perform a WireShark scan once in a while. When I open my Steam account in another PC, the activity on WireShark looks pretty normal. But when I open Steam on my main PC, once in a while this shows on WireShark: &quot;GET /my/tradeoffers/ HTTP/1...'''
date = "2015-08-31T23:23:00Z"
lastmod = "2015-08-31T23:23:00Z"
weight = 45560
keywords = [ "steam", "tcp", "get" ]
aliases = [ "/questions/45560" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# ["GET link HTTP /1.1" without actually entering the links](/questions/45560/get-link-http-11-without-actually-entering-the-links)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45560-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45560-score" class="post-score" title="current number of votes">0</div><span id="post-45560-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So, I'm suspecting from some malicious activiy on my PC so I perform a WireShark scan once in a while.</p><p>When I open my Steam account in another PC, the activity on WireShark looks pretty normal.</p><p>But when I open Steam on my main PC, once in a while this shows on WireShark:</p><p>"GET /my/tradeoffers/ HTTP/1.1" "GET /id/mysteamnickname/tradeoffers HTTP/1.1"</p><p>Protocol: TCP</p><p>For those who not know, those links are required to accept trade offers (sending or receiving Steam itens to others). But I never check those links when I log in, and specially that day I did not.</p><p>On my other PC, a notebook, when I open Steam these lines does not appear at WireShark.</p><p>Does anyone can help me on what this might mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-steam" rel="tag" title="see questions tagged &#39;steam&#39;">steam</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-get" rel="tag" title="see questions tagged &#39;get&#39;">get</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '15, 23:23</strong></p><img src="https://secure.gravatar.com/avatar/492ee34041dcde7e40e82dfd3120a1cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Andr%C3%A9%20Guimar%C3%A3es&#39;s gravatar image" /><p><span>André Guimarães</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="André Guimarães has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Aug '15, 23:24</strong> </span></p></div></div><div id="comments-container-45560" class="comments-container"></div><div id="comment-tools-45560" class="comment-tools"></div><div class="clear"></div><div id="comment-45560-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

