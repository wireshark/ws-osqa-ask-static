+++
type = "question"
title = "HTTP filter &quot;Content-Encoding: gzip&quot;"
description = '''Can you please put together a short tutorial showing how this is used on a Windows O/S. &quot;Uncompress entity bodies&quot; does not appear to do anything'''
date = "2010-11-24T17:06:00Z"
lastmod = "2010-11-24T17:06:00Z"
weight = 1116
keywords = [ "gzip", "entity", "bodies", "uncompress" ]
aliases = [ "/questions/1116" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HTTP filter "Content-Encoding: gzip"](/questions/1116/http-filter-content-encoding-gzip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1116-score" class="post-score" title="current number of votes">1</div><span id="post-1116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can you please put together a short tutorial showing how this is used on a Windows O/S. "Uncompress entity bodies" does not appear to do anything</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gzip" rel="tag" title="see questions tagged &#39;gzip&#39;">gzip</span> <span class="post-tag tag-link-entity" rel="tag" title="see questions tagged &#39;entity&#39;">entity</span> <span class="post-tag tag-link-bodies" rel="tag" title="see questions tagged &#39;bodies&#39;">bodies</span> <span class="post-tag tag-link-uncompress" rel="tag" title="see questions tagged &#39;uncompress&#39;">uncompress</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '10, 17:06</strong></p><img src="https://secure.gravatar.com/avatar/c4e1f94b30b1666e6e52e857c9d130ef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tinywizard&#39;s gravatar image" /><p><span>tinywizard</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tinywizard has no accepted answers">0%</span></p></div></div><div id="comments-container-1116" class="comments-container"></div><div id="comment-tools-1116" class="comment-tools"></div><div class="clear"></div><div id="comment-1116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

