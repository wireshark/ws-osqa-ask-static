+++
type = "question"
title = "-z voip,calls"
description = '''I have some scripts which starts wireshark and does the flow analysis wireshark -z voip,calls $dump But voip,calls is gone? Is there a replacement or is is simply just gone Kjeld'''
date = "2017-02-22T01:27:00Z"
lastmod = "2017-02-22T06:02:00Z"
weight = 59598
keywords = [ "invocation" ]
aliases = [ "/questions/59598" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [-z voip,calls](/questions/59598/-z-voipcalls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59598-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59598-score" class="post-score" title="current number of votes">0</div><span id="post-59598-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have some scripts which starts wireshark and does the flow analysis wireshark -z voip,calls $dump</p><p>But voip,calls is gone? Is there a replacement or is is simply just gone</p><p>Kjeld</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-invocation" rel="tag" title="see questions tagged &#39;invocation&#39;">invocation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '17, 01:27</strong></p><img src="https://secure.gravatar.com/avatar/b0ac00407121781dba912c3cd3ede4c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kjeld%20Flarup&#39;s gravatar image" /><p><span>Kjeld Flarup</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kjeld Flarup has no accepted answers">0%</span></p></div></div><div id="comments-container-59598" class="comments-container"><span id="59601"></span><div id="comment-59601" class="comment"><div id="post-59601-score" class="comment-score"></div><div class="comment-text"><p>Wireshark version?</p></div><div id="comment-59601-info" class="comment-info"><span class="comment-age">(22 Feb '17, 04:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="59605"></span><div id="comment-59605" class="comment"><div id="post-59605-score" class="comment-score"></div><div class="comment-text"><p>Version 2.0.2</p></div><div id="comment-59605-info" class="comment-info"><span class="comment-age">(22 Feb '17, 06:02)</span> <span class="comment-user userinfo">Kjeld Flarup</span></div></div></div><div id="comment-tools-59598" class="comment-tools"></div><div class="clear"></div><div id="comment-59598-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

