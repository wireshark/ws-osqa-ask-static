+++
type = "question"
title = "how to indentify a perticular application layer protocols  like skype/fbook/torrent"
description = '''Hi,  I am just writing the my own customize tshark which give all info about layer l2, l3,l4 .But when i have started for Application layer then i got the problem about the header file info of http(other application protocols header) . I saw the pcaket-http.h and .c file but i can&#x27;t use all these fi...'''
date = "2014-06-30T06:44:00Z"
lastmod = "2014-06-30T06:44:00Z"
weight = 34287
keywords = [ "tshark" ]
aliases = [ "/questions/34287" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to indentify a perticular application layer protocols like skype/fbook/torrent](/questions/34287/how-to-indentify-a-perticular-application-layer-protocols-like-skypefbooktorrent)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34287-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34287-score" class="post-score" title="current number of votes">0</div><span id="post-34287-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am just writing the my own customize tshark which give all info about layer l2, l3,l4 .But when i have started for Application layer then i got the problem about the header file info of http(other application protocols header) .</p><p>I saw the pcaket-http.h and .c file but i can't use all these file for my small function, it will like a tshark itself . For l2 we have ether_header that way we can take mac and type ,but how i can get the host address of the http packet .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '14, 06:44</strong></p><img src="https://secure.gravatar.com/avatar/2efb52ebce8c778f71e8970779679aab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deepak660d&#39;s gravatar image" /><p><span>deepak660d</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deepak660d has no accepted answers">0%</span></p></div></div><div id="comments-container-34287" class="comments-container"></div><div id="comment-tools-34287" class="comment-tools"></div><div class="clear"></div><div id="comment-34287-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

