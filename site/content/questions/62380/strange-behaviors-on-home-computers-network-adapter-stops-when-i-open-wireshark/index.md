+++
type = "question"
title = "Strange behaviors on home computers. Network adapter stops when I open wireshark."
description = '''Im hoping someone can review my logs. I have had a few occasions where after I open wireshark my network adapter stops. I am able to save the log still. Someone has been trolling me on my cell phone and on my computer. Its getting to me and making me really paranoid and nervous. Is anyone able to he...'''
date = "2017-06-28T16:50:00Z"
lastmod = "2017-06-28T16:50:00Z"
weight = 62380
keywords = [ "help" ]
aliases = [ "/questions/62380" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Strange behaviors on home computers. Network adapter stops when I open wireshark.](/questions/62380/strange-behaviors-on-home-computers-network-adapter-stops-when-i-open-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62380-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62380-score" class="post-score" title="current number of votes">0</div><span id="post-62380-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im hoping someone can review my logs. I have had a few occasions where after I open wireshark my network adapter stops. I am able to save the log still.</p><p>Someone has been trolling me on my cell phone and on my computer. Its getting to me and making me really paranoid and nervous.</p><p>Is anyone able to help me look at these logs and figure out who is doing this? or at least let me know how?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '17, 16:50</strong></p><img src="https://secure.gravatar.com/avatar/24b083e05b6e9e3847736fffc8988bae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cgrients22&#39;s gravatar image" /><p><span>cgrients22</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cgrients22 has no accepted answers">0%</span></p></div></div><div id="comments-container-62380" class="comments-container"></div><div id="comment-tools-62380" class="comment-tools"></div><div class="clear"></div><div id="comment-62380-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

