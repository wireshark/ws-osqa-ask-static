+++
type = "question"
title = "Exchange Connectivity possible network delay?"
description = '''Hi all, I have a client 10.230.190.20 and an Exchange Server 10.2.1.3. I would like someone to review the attached wireshark captures as to me it looks as though that there is a delay in the client recieving SYN,ACK packets from the Exchange server so the Client sends a FIN and then the whole proces...'''
date = "2014-06-02T06:39:00Z"
lastmod = "2014-06-02T12:18:00Z"
weight = 33273
keywords = [ "delay", "exchange" ]
aliases = [ "/questions/33273" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Exchange Connectivity possible network delay?](/questions/33273/exchange-connectivity-possible-network-delay)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33273-score" class="post-score" title="current number of votes">0</div><span id="post-33273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I have a client 10.230.190.20 and an Exchange Server 10.2.1.3.</p><p>I would like someone to review the attached wireshark captures as to me it looks as though that there is a delay in the client recieving SYN,ACK packets from the Exchange server so the Client sends a FIN and then the whole process repeats itself.</p><p>My question is that if this were network delay, why is the first part of the MAPI process no affected at all and those packets are larger in size?</p><p>I am no exchange expert so I am unsure if this is an Exchange or a network issue.</p><p>One capture is from the client and the other capture is from the egress interface of our Cisco router.</p><p>Thanks!</p><p>Mario</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-exchange" rel="tag" title="see questions tagged &#39;exchange&#39;">exchange</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '14, 06:39</strong></p><img src="https://secure.gravatar.com/avatar/5595deac00632ff38856a332dd70a60d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mario%20De%20Rosa&#39;s gravatar image" /><p><span>Mario De Rosa</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mario De Rosa has no accepted answers">0%</span></p></div></div><div id="comments-container-33273" class="comments-container"><span id="33274"></span><div id="comment-33274" class="comment"><div id="post-33274-score" class="comment-score"></div><div class="comment-text"><p>Looks like I cannot attach the wireshark captures. Is this normal?</p><p>Apologies... I can send them to anyone that requests them</p><p>thanks</p><p>Mario</p></div><div id="comment-33274-info" class="comment-info"><span class="comment-age">(02 Jun '14, 06:40)</span> <span class="comment-user userinfo">Mario De Rosa</span></div></div><span id="33296"></span><div id="comment-33296" class="comment"><div id="post-33296-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Looks like I cannot attach the wireshark captures. Is this normal?</p></blockquote><p>yes.</p><p>Please post the capture file on google drive, dropbox or cloudshark.org.</p></div><div id="comment-33296-info" class="comment-info"><span class="comment-age">(02 Jun '14, 12:18)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33273" class="comment-tools"></div><div class="clear"></div><div id="comment-33273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

