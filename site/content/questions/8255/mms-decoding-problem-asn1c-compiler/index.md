+++
type = "question"
title = "MMS decoding problem (asn1c Compiler)"
description = '''Hi all, I know this is not directly related to wireshark but I expect that most of you will be able to help me on this one. I am using asn1c Compiler to decode MMS protocol (transported over TPKT, COTP,Session, Presentation, ACSE layers). The problem that I am having is the asn1c compiler complains ...'''
date = "2012-01-06T07:28:00Z"
lastmod = "2012-01-06T07:28:00Z"
weight = 8255
keywords = [ "asn1" ]
aliases = [ "/questions/8255" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [MMS decoding problem (asn1c Compiler)](/questions/8255/mms-decoding-problem-asn1c-compiler)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8255-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8255-score" class="post-score" title="current number of votes">0</div><span id="post-8255-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I know this is not directly related to wireshark but I expect that most of you will be able to help me on this one. I am using asn1c Compiler to decode MMS protocol (transported over TPKT, COTP,Session, Presentation, ACSE layers). The problem that I am having is the asn1c compiler complains of unexpected tag when I decode the MMS layer packets. I suspect that this is related to context that needs to be passed from presentation layer onwards. Can someone kindly tell me what is a context (I understand that it is used for translating abstract syntax to transfer syntax i.e actual bits, I hope I am right in my understanding) , where should I look for it in an MMS trace ? And how is it transfered (between layers) and used by them ?</p><p>Any pointers for resolving this problem are much appreciated. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asn1" rel="tag" title="see questions tagged &#39;asn1&#39;">asn1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '12, 07:28</strong></p><img src="https://secure.gravatar.com/avatar/d698b590fa03c946629a0ac3689494b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mmalik10&#39;s gravatar image" /><p><span>mmalik10</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mmalik10 has no accepted answers">0%</span></p></div></div><div id="comments-container-8255" class="comments-container"></div><div id="comment-tools-8255" class="comment-tools"></div><div class="clear"></div><div id="comment-8255-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

