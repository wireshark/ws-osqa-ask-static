+++
type = "question"
title = "Does TCP stream graph show some data points incorrectly?"
description = '''Using version 1.6.4, I want to rely on TCP stream graph to chart and summarize RTT for ACKs. I capture data because I&#x27;m suspecting some network delay is impacting a unicast HD Video on Demand application I&#x27;m testing. This traffic is delivered via HTTP over TCP. Most data points plot in the graph aro...'''
date = "2011-12-01T10:59:00Z"
lastmod = "2011-12-01T10:59:00Z"
weight = 7731
keywords = [ "graph" ]
aliases = [ "/questions/7731" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Does TCP stream graph show some data points incorrectly?](/questions/7731/does-tcp-stream-graph-show-some-data-points-incorrectly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7731-score" class="post-score" title="current number of votes">0</div><span id="post-7731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using version 1.6.4, I want to rely on TCP stream graph to chart and summarize RTT for ACKs. I capture data because I'm suspecting some network delay is impacting a unicast HD Video on Demand application I'm testing. This traffic is delivered via HTTP over TCP. Most data points plot in the graph around 30ms value which is okay for my application and normal for my network environment.<br />
</p><p>A few plotted values on the graph are at high (problematic) values between 55ms and 65ms. Yet when I click on any of these values in the graph, and then go to the selected frame, and the RTT value shown within the packet details, is low/normal value. When I take the difference between the time stamps of the ACK and the packet the ACK was for, the answer does correlate with the timee stamps.</p><p>Is there a known bug with the TCP stream graphing so RTT values do not always display correctly????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '11, 10:59</strong></p><img src="https://secure.gravatar.com/avatar/4a453f3dd28d87db5bbd6bb6fd79be79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dja0301&#39;s gravatar image" /><p><span>dja0301</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dja0301 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-7731" class="comments-container"></div><div id="comment-tools-7731" class="comment-tools"></div><div class="clear"></div><div id="comment-7731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

