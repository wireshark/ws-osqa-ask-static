+++
type = "question"
title = "Learning to understand the data captured by Wireshark?"
description = '''I want to be able to use and understand the data captured by Wireshark. To achieve this goal, can you please advise me if using the user manual and hands-on use will be enough? Or would you advise me to also purchase the Wireshark Network Analysis book written by Laura Chappell along with the Exam P...'''
date = "2011-12-29T05:05:00Z"
lastmod = "2011-12-30T09:23:00Z"
weight = 8164
keywords = [ "beginner" ]
aliases = [ "/questions/8164" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Learning to understand the data captured by Wireshark?](/questions/8164/learning-to-understand-the-data-captured-by-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8164-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8164-score" class="post-score" title="current number of votes">0</div><span id="post-8164-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to be able to use and understand the data captured by Wireshark.</p><p>To achieve this goal, can you please advise me if using the user manual and hands-on use will be enough? Or would you advise me to also purchase the Wireshark Network Analysis book written by Laura Chappell along with the Exam Prep Guide?</p><p>Additional to the books, are there any CBT videos that you may recommend not shown on the Wireshark websites? http://cbtvideos.ecrater.com/p/10225638/wireshark-video-training-cbt-4-dvd</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Dec '11, 05:05</strong></p><img src="https://secure.gravatar.com/avatar/51cb10f2e5a07d001040cd1bb67165ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DC4Networks&#39;s gravatar image" /><p><span>DC4Networks</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DC4Networks has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Dec '11, 14:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-8164" class="comments-container"></div><div id="comment-tools-8164" class="comment-tools"></div><div class="clear"></div><div id="comment-8164-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8168"></span>

<div id="answer-container-8168" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8168-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8168-score" class="post-score" title="current number of votes">0</div><span id="post-8168-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You cant go wrong with getting any of Laura's books or CBT's.</p><p>I also have some material on my website, if it helps you. www.thetechfirm.com</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Dec '11, 09:23</strong></p><img src="https://secure.gravatar.com/avatar/dbc4d8cb6be85bd586ca4bf211e1337c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thetechfirm&#39;s gravatar image" /><p><span>thetechfirm</span><br />
<span class="score" title="64 reputation points">64</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thetechfirm has no accepted answers">0%</span></p></div></div><div id="comments-container-8168" class="comments-container"></div><div id="comment-tools-8168" class="comment-tools"></div><div class="clear"></div><div id="comment-8168-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

