+++
type = "question"
title = "the tcp stream analyzing"
description = '''this is a tcp stream for a client(app,not brower),the content is about SSL handshake,but this is no client publickey,why?and what is the &quot;AuthnToken&quot; meaning?is it the client publickey or signatrue?how can i find the clientpravitekey on my computer? Any help or pointers would be greatly appreciated!...'''
date = "2014-02-23T02:48:00Z"
lastmod = "2014-02-24T10:17:00Z"
weight = 30104
keywords = [ "the", "analyzing", "stream", "tcp" ]
aliases = [ "/questions/30104" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [the tcp stream analyzing](/questions/30104/the-tcp-stream-analyzing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30104-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30104-score" class="post-score" title="current number of votes">0</div><span id="post-30104-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>this is a tcp stream for a client(app,not brower),the content is about SSL handshake,but this is no client publickey,why?and what is the "AuthnToken" meaning?is it the client publickey or signatrue?how can i find the clientpravitekey on my computer?</p><p>Any help or pointers would be greatly appreciated!</p><p><strong>ClientRand</strong>:WN39UqcGva4NNtZh0IVBGbIIe4ebdOuWLQqpI6IZyaM=</p><p><strong>ServerRand</strong>:ZN39Ut8wEybFu7Vsl2RuT6BHaKugXxfxZQmeonItxyA <strong>ServerPublicKey</strong>:BAAAAAEAAQCAAAAAEUYJdXyMoc6BsY+yp9Sjv3Tx9HIQEOblctoXhbYUrGSosYrTP/RmdwJgzBzX6bT0bNoA9m1A1GNkFoSrqOnRHMN2hPlGTpMhvCvJ4H4GVtf5HW1mlSM2OeqgGSPle4O2aNYDNPR8ejziCizaD1sr7rfbUDr+cTAny0lz/I0b2aA= <strong>ServerSignature</strong>:eV8xRM7NpjAjzwn4QslI07azcz0gd0QltYvZrXKp3DxxTvvjdpqZ+krjjkTkdCEuFdgI5KPVoKuo4CnS+03r/RaIDiJKvWBfFQwuM2Fevtn7v6F1MoLSmm7bKJTwDakr8qWWhQJIVpT7g0Uuc6IWCPItS+CIoBkkSisoVGY8m9TUQR3WRQt4mxbna0zz7Tl7EgEISpvT5iY677IPoXvk9UV5nXG8sP9mr848s4Lei2ZJbZQl6qkxHkFnfBgc8d0d7qf6bxLOwzu79Tfajkk6HqEV0adGmmw8aQyT2b/+RJtGAyLUNIsXIg1mEc8hepC80Xzx81BT45TNkFNbmPkGLw==</p><p><strong>PremasterSecret</strong>:zcmUJjgjwnxPSZ3En8EvT4CPB6+g1wGR/05pDGbZDMnSjibiQ8RsCG7LQOnmTsbMo/skZScWxcXpRvsXC/YPH+nD1cZMvTc6Z2r+lv42jYHc0ZK2GYs9IB5uzoVgSdQLHxRdZ3wHHkiIuTivdBII3ZW5J9w8+T7r/c/cXA6nwlY= <strong>AuthnToken</strong>:Gk8BVW04iJKg/74Awq8LCwiVA0ije/ySVdifqq2zdyIClxwlAXqzkOPrbX6czbOL78IMtlGGWkd/pxVlcEQtEU8C3MECTI2ljMlhkEzklhr9FKDy9vKfaDs15bO3RmT1YRFxgYwY1524uoxXX3Dt1ICePzn1WbUOdj6sIGcoUq375gaobLJTFvH5Re0we+mRM7DYq+cl8Md5u226xGawWarfEl3y9cx9lTrNuOSWJ8h/klkU/lQ7/wApi4Tqxx7HLaDsFXnmgqRJmusQ3lJL+MjUNZ16W1dgRy6fBJTMwSBHtx1qwlcPC4/uOjyL16ctugoEh6WqSzaQLqsqLOx7WnermgmxJ97BfMd1uK58MhlT0/3fGevptRMOlfSdjkkmIcRAxwfk8BY3mP8HWTru++5DZJHTQAS+CbS6L5BSsfNfkVTPKWfDIbK/AfkYvAUTxma0kSHf7RrX2YDXx+cqDDu0pDMUD4dNMpoMcSkZmngocVurbek2nvH99/WJXeEj <strong>AppId</strong>:b0d42105-0cb6-bc9f-3cb2-be28a0662340</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-the" rel="tag" title="see questions tagged &#39;the&#39;">the</span> <span class="post-tag tag-link-analyzing" rel="tag" title="see questions tagged &#39;analyzing&#39;">analyzing</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '14, 02:48</strong></p><img src="https://secure.gravatar.com/avatar/dfc516c0375528eee73b5d984b64c5c3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="barrney&#39;s gravatar image" /><p><span>barrney</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="barrney has no accepted answers">0%</span></p></div></div><div id="comments-container-30104" class="comments-container"><span id="30138"></span><div id="comment-30138" class="comment"><div id="post-30138-score" class="comment-score"></div><div class="comment-text"><blockquote><p><strong>tcp stream analyzing</strong> and <strong>the content is about SSL handshake</strong></p></blockquote><p>Where did you get that data from? It looks like a dump of your client software !?!</p></div><div id="comment-30138-info" class="comment-info"><span class="comment-age">(24 Feb '14, 10:17)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30104" class="comment-tools"></div><div class="clear"></div><div id="comment-30104-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

