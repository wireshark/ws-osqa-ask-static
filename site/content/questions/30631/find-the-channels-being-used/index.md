+++
type = "question"
title = "Find the channels being used"
description = '''How do I use the Wire Shark UI to determine what channels surrounding WiFi networks are using? I&#x27;m trying to pick one channel to use (to increase wifi stability) but I want to choose a channel that is not being used.'''
date = "2014-03-09T23:03:00Z"
lastmod = "2014-03-10T02:32:00Z"
weight = 30631
keywords = [ "wifi", "channel" ]
aliases = [ "/questions/30631" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Find the channels being used](/questions/30631/find-the-channels-being-used)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30631-score" class="post-score" title="current number of votes">0</div><span id="post-30631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I use the Wire Shark UI to determine what channels surrounding WiFi networks are using? I'm trying to pick one channel to use (to increase wifi stability) but I want to choose a channel that is not being used.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-channel" rel="tag" title="see questions tagged &#39;channel&#39;">channel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '14, 23:03</strong></p><img src="https://secure.gravatar.com/avatar/294b2fb6127dd914c0af8d19fbd30b6a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shakana&#39;s gravatar image" /><p><span>shakana</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shakana has no accepted answers">0%</span></p></div></div><div id="comments-container-30631" class="comments-container"></div><div id="comment-tools-30631" class="comment-tools"></div><div class="clear"></div><div id="comment-30631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30635"></span>

<div id="answer-container-30635" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30635-score" class="post-score" title="current number of votes">0</div><span id="post-30635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>How do I use the Wire Shark UI to <strong>determine</strong> what <strong>channels surrounding WiFi</strong> networks are using?</p></blockquote><p>you cant't, because that functionality is not built into Wireshark. Please use a different tool, like <a href="http://www.metageek.net/products/inssider/">inSSIDer</a> or similar tools.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '14, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Mar '14, 00:22</strong> </span></p></div></div><div id="comments-container-30635" class="comments-container"><span id="30638"></span><div id="comment-30638" class="comment"><div id="post-30638-score" class="comment-score"></div><div class="comment-text"><p>Well, you can, but it is not exactly a smart thing to do, because you'd have to switch through channels using the Wireless tool bar (I guess that only works with AirPCAP adapters though) and check what shows up and what signal quality it is. It's basically a slow pen-and-paper version of what inSSIDer etc do.</p></div><div id="comment-30638-info" class="comment-info"><span class="comment-age">(10 Mar '14, 02:32)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-30635" class="comment-tools"></div><div class="clear"></div><div id="comment-30635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

