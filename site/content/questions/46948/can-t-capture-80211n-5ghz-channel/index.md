+++
type = "question"
title = "Can´t capture 802.11n 5GHz channel"
description = '''Hi, I´m trying to capture 802.11n traffic with a aipcap nx, but I cannot select 802.11n 5Ghz channels on the capture menu in wireshark. Can anyone help? thanks.'''
date = "2015-10-26T10:57:00Z"
lastmod = "2015-10-26T10:57:00Z"
weight = 46948
keywords = [ "wifi", "802.11n" ]
aliases = [ "/questions/46948" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can´t capture 802.11n 5GHz channel](/questions/46948/can-t-capture-80211n-5ghz-channel)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46948-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46948-score" class="post-score" title="current number of votes">0</div><span id="post-46948-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I´m trying to capture 802.11n traffic with a aipcap nx, but I cannot select 802.11n 5Ghz channels on the capture menu in wireshark. Can anyone help?</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-802.11n" rel="tag" title="see questions tagged &#39;802.11n&#39;">802.11n</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '15, 10:57</strong></p><img src="https://secure.gravatar.com/avatar/c92508dc9301a7e15c0a7069dda76433?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ricci&#39;s gravatar image" /><p><span>ricci</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ricci has no accepted answers">0%</span></p></div></div><div id="comments-container-46948" class="comments-container"></div><div id="comment-tools-46948" class="comment-tools"></div><div class="clear"></div><div id="comment-46948-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

