+++
type = "question"
title = "[closed] Cannot capture any output packet"
description = '''My PC is Windows 10, the Wireshark version is 2.0.3 and it is installed in this PC. My PC is connected by ethernet to my home router. I use a TCP client tool on an Android phone to connect to a TCP sever tool on my PC. And then send and receive TCP packets between them. The communication has no prob...'''
date = "2016-06-12T05:23:00Z"
lastmod = "2016-06-12T05:50:00Z"
weight = 53366
keywords = [ "tcp" ]
aliases = [ "/questions/53366" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Cannot capture any output packet](/questions/53366/cannot-capture-any-output-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53366-score" class="post-score" title="current number of votes">0</div><span id="post-53366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My PC is Windows 10, the Wireshark version is 2.0.3 and it is installed in this PC. My PC is connected by ethernet to my home router.</p><p>I use a TCP client tool on an Android phone to connect to a TCP sever tool on my PC. And then send and receive TCP packets between them. The communication has no problem because I can see the message on both tools. However I cannot see any packet sent out from my PC. I mean other than the packets of the client/server tool, I even cannot see any packet sent out from my PC's ip address.</p><p>I have switched off the my PC's anti-virus and firewall. Also I have switched on the promiscuous mode. Why is that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '16, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/d075420fd2ed7d78364856553fcbe705?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="garypty&#39;s gravatar image" /><p><span>garypty</span><br />
<span class="score" title="31 reputation points">31</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="garypty has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>12 Jun '16, 05:50</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-53366" class="comments-container"><span id="53368"></span><div id="comment-53368" class="comment"><div id="post-53368-score" class="comment-score"></div><div class="comment-text"><p>See <a href="https://ask.wireshark.org/questions/27296">this question</a></p></div><div id="comment-53368-info" class="comment-info"><span class="comment-age">(12 Jun '16, 05:50)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-53366" class="comment-tools"></div><div class="clear"></div><div id="comment-53366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Jaap 12 Jun '16, 05:50

</div>

</div>

</div>

