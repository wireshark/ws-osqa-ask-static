+++
type = "question"
title = "Decrypt SSL using wildcard IP address"
description = '''Is it possible to consistently use a wildcard IP address 0.0.0.0 to decrypt SSL traffic for a specific RSA key. I have wildcard SSL certs applied to multiple VIPs in our test environment and I&#x27;m tired of adding each VIP&#x27;s IP address every time I need to decrypt that conversation. I had some luck ini...'''
date = "2013-12-31T08:26:00Z"
lastmod = "2013-12-31T08:26:00Z"
weight = 28503
keywords = [ "ssl", "decryption" ]
aliases = [ "/questions/28503" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt SSL using wildcard IP address](/questions/28503/decrypt-ssl-using-wildcard-ip-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28503-score" class="post-score" title="current number of votes">0</div><span id="post-28503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to consistently use a wildcard IP address 0.0.0.0 to decrypt SSL traffic for a specific RSA key. I have wildcard SSL certs applied to multiple VIPs in our test environment and I'm tired of adding each VIP's IP address every time I need to decrypt that conversation.</p><p>I had some luck initially in version in 1.10.1 but for reasons unknown it stopped working. I've since upgraded to ver 1.10.4 and the wildcard IP address has never worked.</p><p>Any help is much appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '13, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/e90748ff37a82003ccce10387231846f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ilcuse&#39;s gravatar image" /><p><span>ilcuse</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ilcuse has no accepted answers">0%</span></p></div></div><div id="comments-container-28503" class="comments-container"></div><div id="comment-tools-28503" class="comment-tools"></div><div class="clear"></div><div id="comment-28503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

