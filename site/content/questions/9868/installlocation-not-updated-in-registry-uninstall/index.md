+++
type = "question"
title = "InstallLocation not updated in Registry Uninstall"
description = '''Hi, I&#x27;ve see that Registry is not updated correctly during program install : HKEY_LOCAL_MACHINE&#92;SOFTWARE&#92;Wow6432Node&#92;Microsoft&#92;Windows&#92;CurrentVersion&#92;Uninstall&#92;Wireshark the field &#x27;InstallLocation&#x27; is not present. ++ Vincent'''
date = "2012-03-30T09:47:00Z"
lastmod = "2012-03-31T19:14:00Z"
weight = 9868
keywords = [ "installation", "registry", "wireshark" ]
aliases = [ "/questions/9868" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [InstallLocation not updated in Registry Uninstall](/questions/9868/installlocation-not-updated-in-registry-uninstall)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9868-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9868-score" class="post-score" title="current number of votes">-1</div><span id="post-9868-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I've see that Registry is not updated correctly during program install : HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Wireshark the field 'InstallLocation' is not present.</p><p>++ Vincent</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span> <span class="post-tag tag-link-registry" rel="tag" title="see questions tagged &#39;registry&#39;">registry</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Mar '12, 09:47</strong></p><img src="https://secure.gravatar.com/avatar/397c039d3d7656fd2859ee3459373934?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vincent%20Duvernet%20_%20Nolm%C3%AB%20Info&#39;s gravatar image" /><p><span>Vincent Duve...</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vincent Duvernet _ Nolmë Info has no accepted answers">0%</span></p></div></div><div id="comments-container-9868" class="comments-container"></div><div id="comment-tools-9868" class="comment-tools"></div><div class="clear"></div><div id="comment-9868-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9869"></span>

<div id="answer-container-9869" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9869-score" class="post-score" title="current number of votes">0</div><span id="post-9869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This isn't really a question for Ask Wireshark. Please report your issue on the developers mailing list, details <a href="https://www.wireshark.org/mailman/listinfo/wireshark-dev">HERE</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '12, 12:36</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-9869" class="comments-container"><span id="9881"></span><div id="comment-9881" class="comment"><div id="post-9881-score" class="comment-score"></div><div class="comment-text"><p>Or report it as a bug on <a href="http://bugs.wireshark.org">the Wireshark bugzilla</a>.</p></div><div id="comment-9881-info" class="comment-info"><span class="comment-age">(31 Mar '12, 19:14)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-9869" class="comment-tools"></div><div class="clear"></div><div id="comment-9869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

