+++
type = "question"
title = "How to determine packets of an uploaded image uploaded to browser?"
description = '''Hi, I uploaded a jpg to Google, but when I checked the packets using the &#x27;image-jfif&#x27; filter I only see those that I&#x27;ve downloaded, that&#x27;s obviously not what I want. So how to I determine packets of an uploaded image uploaded to browser?'''
date = "2016-10-01T00:14:00Z"
lastmod = "2016-10-01T00:14:00Z"
weight = 56037
keywords = [ "filter", "jfif", "packets", "upload" ]
aliases = [ "/questions/56037" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to determine packets of an uploaded image uploaded to browser?](/questions/56037/how-to-determine-packets-of-an-uploaded-image-uploaded-to-browser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56037-score" class="post-score" title="current number of votes">0</div><span id="post-56037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I uploaded a jpg to Google, but when I checked the packets using the 'image-jfif' filter I only see those that I've <strong>downloaded</strong>, that's obviously not what I want. So how to I determine packets of an uploaded image uploaded to browser?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-jfif" rel="tag" title="see questions tagged &#39;jfif&#39;">jfif</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-upload" rel="tag" title="see questions tagged &#39;upload&#39;">upload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '16, 00:14</strong></p><img src="https://secure.gravatar.com/avatar/87ad56f6de6d2b2df61e575eef287b85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wstd1234&#39;s gravatar image" /><p><span>wstd1234</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wstd1234 has no accepted answers">0%</span></p></div></div><div id="comments-container-56037" class="comments-container"></div><div id="comment-tools-56037" class="comment-tools"></div><div class="clear"></div><div id="comment-56037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

