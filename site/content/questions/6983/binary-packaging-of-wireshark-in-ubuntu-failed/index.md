+++
type = "question"
title = "[closed] Binary packaging of wireshark in ubuntu failed"
description = '''make debian-package Results in errors ... dpkg-checkbuilddeps: Unmet build dependencies: automake1.9 libc-ares-dev docbook-xsl (&amp;gt;= 1.64.1.0-0) libcap-dev bison libgnutls-dev portaudio19-dev libkrb5-dev liblua5.1-0-dev libsmi2-dev dpkg-buildpackage: warning: Build dependencies/conflicts unsatisfie...'''
date = "2011-10-19T00:16:00Z"
lastmod = "2011-10-19T04:34:00Z"
weight = 6983
keywords = [ "packaging" ]
aliases = [ "/questions/6983" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [\[closed\] Binary packaging of wireshark in ubuntu failed](/questions/6983/binary-packaging-of-wireshark-in-ubuntu-failed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6983-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6983-score" class="post-score" title="current number of votes">0</div><span id="post-6983-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">2</div></div></td><td><div id="item-right"><div class="question-body"><p>make debian-package Results in errors ...</p><p>dpkg-checkbuilddeps: Unmet build dependencies: automake1.9 libc-ares-dev docbook-xsl (&gt;= 1.64.1.0-0) libcap-dev bison libgnutls-dev portaudio19-dev libkrb5-dev liblua5.1-0-dev libsmi2-dev dpkg-buildpackage: warning: Build dependencies/conflicts unsatisfied; aborting. dpkg-buildpackage: warning: (Use -d flag to override.) make: *** [debian-package] Error 3</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packaging" rel="tag" title="see questions tagged &#39;packaging&#39;">packaging</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '11, 00:16</strong></p><img src="https://secure.gravatar.com/avatar/264adc05b644c1ab2d670b4773a12392?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flashkicker&#39;s gravatar image" /><p><span>flashkicker</span><br />
<span class="score" title="109 reputation points">109</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="19 badges"><span class="silver">●</span><span class="badgecount">19</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flashkicker has 5 accepted answers">41%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>20 Oct '11, 02:17</strong> </span></p></div></div><div id="comments-container-6983" class="comments-container"></div><div id="comment-tools-6983" class="comment-tools"></div><div class="clear"></div><div id="comment-6983-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "inappropriate question" by flashkicker 20 Oct '11, 02:17

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6986"></span>

<div id="answer-container-6986" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6986-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6986-score" class="post-score" title="current number of votes">1</div><span id="post-6986-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="flashkicker has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>And the question is????</p><p>Probably: why doesn't it build?</p><p>Have you read the output? <em>Build dependencies/conflicts unsatisfied.</em> So go satisfy the dependencies. Install the listed development packages.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '11, 04:34</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6986" class="comments-container"></div><div id="comment-tools-6986" class="comment-tools"></div><div class="clear"></div><div id="comment-6986-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

