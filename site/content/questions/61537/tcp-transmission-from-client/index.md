+++
type = "question"
title = "TCP Transmission from Client"
description = '''I wanted to establish TCP connection with my server. For this I have created a client on my Win7Laptop,  But What I observe, Server responds to client&#x27;s connection request, But still Client re-transmit request  Additionally, In Server&#x27;s response I could see :  Can anyone help in understanding the is...'''
date = "2017-05-22T04:40:00Z"
lastmod = "2017-05-22T04:40:00Z"
weight = 61537
keywords = [ "tcp_retransmission" ]
aliases = [ "/questions/61537" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP Transmission from Client](/questions/61537/tcp-transmission-from-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61537-score" class="post-score" title="current number of votes">0</div><span id="post-61537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I wanted to establish TCP connection with my server. For this I have created a client on my Win7Laptop, But What I observe, Server responds to client's connection request, But still Client re-transmit request <img src="https://osqa-ask.wireshark.org/upfiles/WiresharkSnapshot_001_DoOTiI1.png" alt="alt text" /></p><p>Additionally, In Server's response I could see : <img src="https://osqa-ask.wireshark.org/upfiles/WiresharkSnapshot_002_ytsAl5d.png" alt="alt text" /></p><p>Can anyone help in understanding the issue, what i should do to resolve this issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp_retransmission" rel="tag" title="see questions tagged &#39;tcp_retransmission&#39;">tcp_retransmission</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '17, 04:40</strong></p><img src="https://secure.gravatar.com/avatar/45ee83366f9466a704c3c39ad5b3032b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="avi2936&#39;s gravatar image" /><p><span>avi2936</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="avi2936 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61537" class="comments-container"></div><div id="comment-tools-61537" class="comment-tools"></div><div class="clear"></div><div id="comment-61537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

