+++
type = "question"
title = "wireshark won&#x27;t start on mountain lion"
description = '''hi, i have installed wireshark and Xquartz for x11. and when i load wireshark, all i get is a xterm window on the upper left corner. I followed all the instructions that were provided here: http://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallBeforeBuild.html and the referenced links to gtk...'''
date = "2013-02-19T10:11:00Z"
lastmod = "2013-02-19T10:17:00Z"
weight = 18743
keywords = [ "mountain-lion" ]
aliases = [ "/questions/18743" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark won't start on mountain lion](/questions/18743/wireshark-wont-start-on-mountain-lion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18743-score" class="post-score" title="current number of votes">0</div><span id="post-18743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i have installed wireshark and Xquartz for x11. and when i load wireshark, all i get is a xterm window on the upper left corner. I followed all the instructions that were provided here: <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallBeforeBuild.html">http://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallBeforeBuild.html</a> and the referenced links to gtk. Does anyone know what' i'm missing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mountain-lion" rel="tag" title="see questions tagged &#39;mountain-lion&#39;">mountain-lion</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '13, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/a8752beaad4e7700fd4102f33cbaea1c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bennett&#39;s gravatar image" /><p><span>bennett</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bennett has no accepted answers">0%</span></p></div></div><div id="comments-container-18743" class="comments-container"></div><div id="comment-tools-18743" class="comment-tools"></div><div class="clear"></div><div id="comment-18743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18744"></span>

<div id="answer-container-18744" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18744-score" class="post-score" title="current number of votes">0</div><span id="post-18744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>nevermind i figured it out by going here: <a href="http://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion">http://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion</a></p><p>thanks!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '13, 10:17</strong></p><img src="https://secure.gravatar.com/avatar/a8752beaad4e7700fd4102f33cbaea1c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bennett&#39;s gravatar image" /><p><span>bennett</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bennett has no accepted answers">0%</span></p></div></div><div id="comments-container-18744" class="comments-container"></div><div id="comment-tools-18744" class="comment-tools"></div><div class="clear"></div><div id="comment-18744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

