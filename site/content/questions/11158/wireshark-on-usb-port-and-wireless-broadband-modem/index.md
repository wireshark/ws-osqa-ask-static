+++
type = "question"
title = "Wireshark on USB port and Wireless Broadband Modem"
description = '''Two issues:  Wireshark doesn&#x27;t seem to recognize a wireless broadband modem (WBM) connected via the USB port. Using netstat, this particular WBM -- apparetly was being routed through a DOD network, out of Maryland as I recall. Isn&#x27;t that interesting.. So, thinking it was a one-time/temporary thing, ...'''
date = "2012-05-20T10:13:00Z"
lastmod = "2012-05-24T11:04:00Z"
weight = 11158
keywords = [ "wireless", "broadband", "modem", "usb", "network" ]
aliases = [ "/questions/11158" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on USB port and Wireless Broadband Modem](/questions/11158/wireshark-on-usb-port-and-wireless-broadband-modem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11158-score" class="post-score" title="current number of votes">0</div><span id="post-11158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Two issues:</p><ol><li>Wireshark doesn't seem to recognize a wireless broadband modem (WBM) connected via the USB port.</li><li>Using netstat, this particular WBM -- apparetly was being routed through a DOD network, out of Maryland as I recall. Isn't that interesting.. So, thinking it was a one-time/temporary thing, I acquired another separate unit (though similar/identical manufacturer and model) and -- same thing. Routing through a DOD network. I'm on now yet another separate unit, and the last time I checked it was not routing through this DOD network.</li></ol><p>Assistance with both of these issues would be greatly appreciated. I wonder how many customers realize this was even happening (or still is).</p><p>The following information is from the Virgin Mobile online account (www..<a href="http://virginmobileusa.com">virginmobileusa.com</a>):</p><p>//partial information</p><p>What is Broadband2Go?</p><p>Broadband2Go is Virgin Mobile's 3G nationwide wireless internet service. With Broadband2Go, you can connect to the internet from almost anywhere in the US - without having to search for WiFi or hotspots. Just buy a USB device, plug it into your laptop, choose a plan, and you're good to go. Pay with cash (Top-Up), credit, or debit. There are no long-term contracts or activation fees and you can change plans as your needs change. Use your Broadband2Go service to work, shop, chat, and more - all at lightning-fast speed.</p><p><a href="http://virginmobileusa.com">virginmobileusa.com</a></p><p>Which network does Broadband2Go use?</p><p>Your Broadband2Go device connects to Sprint's EVDO Rev A data network. Under Virgin Mobile USA's arrangement with Sprint, Virgin Mobile USA customers have access to broadband service with average downlink data speeds between 600 and 1.4 Mbps and average uplink speeds between 350 and 500 Kbps.</p><p>Where can I buy?</p><p><a href="http://www.virginmobileusa.com/mobile-broadband/">http://www.virginmobileusa.com/mobile-broadband/</a></p><p>//Hi Laura :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-broadband" rel="tag" title="see questions tagged &#39;broadband&#39;">broadband</span> <span class="post-tag tag-link-modem" rel="tag" title="see questions tagged &#39;modem&#39;">modem</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '12, 10:13</strong></p><img src="https://secure.gravatar.com/avatar/2deaced8f7894c6971f8ae606b35315a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="riderr&#39;s gravatar image" /><p><span>riderr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="riderr has no accepted answers">0%</span></p></div></div><div id="comments-container-11158" class="comments-container"><span id="11159"></span><div id="comment-11159" class="comment"><div id="post-11159-score" class="comment-score"></div><div class="comment-text"><p>By the way, I do still have the IP address(es) resolving to this DOD network, along with the screen caps.</p></div><div id="comment-11159-info" class="comment-info"><span class="comment-age">(20 May '12, 10:15)</span> <span class="comment-user userinfo">riderr</span></div></div></div><div id="comment-tools-11158" class="comment-tools"></div><div class="clear"></div><div id="comment-11158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11162"></span>

<div id="answer-container-11162" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11162-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11162-score" class="post-score" title="current number of votes">0</div><span id="post-11162-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>Try running dumpcap -D to see if the USB device is listed. If not, Wireshark will not be able to work with it.</li><li>It shouldn't matter with what modem you connect - as long as it is the same provider on the other end the routing will be pretty much the same in most cases. And coming across a network that looks like its DoD doesn't necessarily mean it is - and even if it is, it could just be a public transport network run by them. As long as you don't have direct access to IP addresses that should not be accessable I don't see why anyone should bother.</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '12, 11:29</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-11162" class="comments-container"><span id="11315"></span><div id="comment-11315" class="comment"><div id="post-11315-score" class="comment-score"></div><div class="comment-text"><p>I'm probably reading/interpreting netstat incorrectly. It's the local address (which appears to be different than the 'what's my ip" address resolving to Sprint PCS out of Wichita Kansas) that resolved to a DoD network (in Ohio) not the foreign address. Probably my mistake.</p></div><div id="comment-11315-info" class="comment-info"><span class="comment-age">(24 May '12, 11:04)</span> <span class="comment-user userinfo">riderr</span></div></div></div><div id="comment-tools-11162" class="comment-tools"></div><div class="clear"></div><div id="comment-11162-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

