+++
type = "question"
title = "How to use Wireshark to monitor Wifi traffic from phones?"
description = '''Is there a way for Wireshark to capture packets from mobile phones connected to my Wifi network? If so, can anyone tell me how or why it can&#x27;t do this? Thank you.'''
date = "2014-09-10T22:27:00Z"
lastmod = "2014-09-11T11:35:00Z"
weight = 36189
keywords = [ "wireless", "mobile", "wifi", "phone", "wireshark" ]
aliases = [ "/questions/36189" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to use Wireshark to monitor Wifi traffic from phones?](/questions/36189/how-to-use-wireshark-to-monitor-wifi-traffic-from-phones)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36189-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36189-score" class="post-score" title="current number of votes">0</div><span id="post-36189-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way for Wireshark to capture packets from mobile phones connected to my Wifi network? If so, can anyone tell me how or why it can't do this? Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-phone" rel="tag" title="see questions tagged &#39;phone&#39;">phone</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '14, 22:27</strong></p><img src="https://secure.gravatar.com/avatar/681c4fe6026d0469594a30209befddbb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ssizz&#39;s gravatar image" /><p><span>ssizz</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ssizz has no accepted answers">0%</span></p></div></div><div id="comments-container-36189" class="comments-container"><span id="36207"></span><div id="comment-36207" class="comment"><div id="post-36207-score" class="comment-score"></div><div class="comment-text"><p>Insert the 7th and 8th word from your question ("WiFi traffic") into the awesome Search field right on top of this page and you'll be amazed of the multiple good answers to your exact question.</p><p>Good luck!</p></div><div id="comment-36207-info" class="comment-info"><span class="comment-age">(11 Sep '14, 11:35)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-36189" class="comment-tools"></div><div class="clear"></div><div id="comment-36189-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

