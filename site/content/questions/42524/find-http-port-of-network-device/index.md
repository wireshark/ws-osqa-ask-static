+++
type = "question"
title = "[closed] Find HTTP port of network device"
description = '''I have to logon to reconfigure a network device. I know the IP address (192.168.2.4) However the default port (80) and the documented port(1024) don&#x27;t work. It pings away merrily - rebooting makes no difference. How do I find the HTTP port? Regards Tango'''
date = "2015-05-18T21:53:00Z"
lastmod = "2015-05-18T21:53:00Z"
weight = 42524
keywords = [ "http", "port" ]
aliases = [ "/questions/42524" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Find HTTP port of network device](/questions/42524/find-http-port-of-network-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42524-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42524-score" class="post-score" title="current number of votes">0</div><span id="post-42524-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have to logon to reconfigure a network device. I know the IP address (192.168.2.4) However the default port (80) and the documented port(1024) don't work. It pings away merrily - rebooting makes no difference.</p><p>How do I find the HTTP port?</p><p>Regards</p><p>Tango</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 May '15, 21:53</strong></p><img src="https://secure.gravatar.com/avatar/3974dbc1e378b90e2ab8776b9db30ec7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tango%20Charlie&#39;s gravatar image" /><p><span>Tango Charlie</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tango Charlie has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>18 May '15, 21:58</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-42524" class="comments-container"></div><div id="comment-tools-42524" class="comment-tools"></div><div class="clear"></div><div id="comment-42524-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 18 May '15, 21:58

</div>

</div>

</div>

