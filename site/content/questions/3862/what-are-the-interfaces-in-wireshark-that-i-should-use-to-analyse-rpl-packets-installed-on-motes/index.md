+++
type = "question"
title = "what are the interfaces (in wireshark) that I should use to analyse RPL packets installed on motes?"
description = '''Hi! I need to analyse RPL packets. For that I use an application that sniff all packets in the network. I install this application on mote and I use it USB port for sniffing. But I use wireshark I don&#x27;t find this USB interface to analyse packets. What interface should I use to capture RPL packets ex...'''
date = "2011-05-01T11:08:00Z"
lastmod = "2011-05-02T21:33:00Z"
weight = 3862
keywords = [ "interface", "packets", "rpl", "wireshark" ]
aliases = [ "/questions/3862" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [what are the interfaces (in wireshark) that I should use to analyse RPL packets installed on motes?](/questions/3862/what-are-the-interfaces-in-wireshark-that-i-should-use-to-analyse-rpl-packets-installed-on-motes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3862-score" class="post-score" title="current number of votes">0</div><span id="post-3862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! I need to analyse RPL packets. For that I use an application that sniff all packets in the network. I install this application on mote and I use it USB port for sniffing. But I use wireshark I don't find this USB interface to analyse packets. What interface should I use to capture RPL packets exchanged between motes? Is there any configuration that I have to do to be able to capture from the sniffer mote? Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-rpl" rel="tag" title="see questions tagged &#39;rpl&#39;">rpl</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '11, 11:08</strong></p><img src="https://secure.gravatar.com/avatar/0335030e1ae576653b4d6a4c811f2276?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RCH&#39;s gravatar image" /><p><span>RCH</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RCH has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>02 May '11, 15:52</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-3862" class="comments-container"><span id="3889"></span><div id="comment-3889" class="comment"><div id="post-3889-score" class="comment-score"></div><div class="comment-text"><p>See http://wiki.wireshark.org/CaptureSetup/USB</p></div><div id="comment-3889-info" class="comment-info"><span class="comment-age">(02 May '11, 21:33)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-3862" class="comment-tools"></div><div class="clear"></div><div id="comment-3862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

