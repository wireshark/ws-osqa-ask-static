+++
type = "question"
title = "Voip Capture"
description = '''Caller(outside) &amp;lt;---&amp;gt; Incoming Server 192.168.60.9 &amp;lt;---&amp;gt; WAN Local Loop Link &amp;lt;----&amp;gt; Receiver (Softphone) 192.168.60.208 Caller is can&#x27;t hear from receiver but receiver hear from caller.  I seem many times this problems how to find solution for fix it.  Thank you.'''
date = "2014-09-15T03:26:00Z"
lastmod = "2014-09-15T06:13:00Z"
weight = 36321
keywords = [ "voip" ]
aliases = [ "/questions/36321" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Voip Capture](/questions/36321/voip-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36321-score" class="post-score" title="current number of votes">0</div><span id="post-36321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Caller(outside) &lt;---&gt; Incoming Server 192.168.60.9 &lt;---&gt; WAN Local Loop Link &lt;----&gt; Receiver (Softphone) 192.168.60.208</p><p>Caller is can't hear from receiver but receiver hear from caller. I seem many times this problems how to find solution for fix it.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/111.png" alt="VOIP" /></p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '14, 03:26</strong></p><img src="https://secure.gravatar.com/avatar/d2c4f7650eb43a6010e1bf3fa7636422?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="peachband&#39;s gravatar image" /><p><span>peachband</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="peachband has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '14, 04:13</strong> </span></p></div></div><div id="comments-container-36321" class="comments-container"><span id="36322"></span><div id="comment-36322" class="comment"><div id="post-36322-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-36322-info" class="comment-info"><span class="comment-age">(15 Sep '14, 06:13)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-36321" class="comment-tools"></div><div class="clear"></div><div id="comment-36321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

