+++
type = "question"
title = "[closed] Newbie trying to track down cause of &quot;Unidentified Network,&quot;"
description = '''We&#x27;ve got an office with a DHCP server and domain that will randomly give machines &quot;Unidentified network,&quot; it can occur over wifi/wired and from any point on the network. Not just one switch/connection. The IP auto-assigned outside of the range. Manually assigning inside the range does not resolve t...'''
date = "2014-07-28T06:25:00Z"
lastmod = "2014-07-28T06:25:00Z"
weight = 34940
keywords = [ "dhcp" ]
aliases = [ "/questions/34940" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Newbie trying to track down cause of "Unidentified Network,"](/questions/34940/newbie-trying-to-track-down-cause-of-unidentified-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34940-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34940-score" class="post-score" title="current number of votes">0</div><span id="post-34940-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We've got an office with a DHCP server and domain that will randomly give machines "Unidentified network," it can occur over wifi/wired and from any point on the network. Not just one switch/connection. The IP auto-assigned outside of the range. Manually assigning inside the range does not resolve the issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dhcp" rel="tag" title="see questions tagged &#39;dhcp&#39;">dhcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '14, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/323d5eef32635cbdff3af4932e96dd97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Solsolis&#39;s gravatar image" /><p><span>Solsolis</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Solsolis has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>29 Jul '14, 02:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-34940" class="comments-container"></div><div id="comment-tools-34940" class="comment-tools"></div><div class="clear"></div><div id="comment-34940-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 29 Jul '14, 02:10

</div>

</div>

</div>

