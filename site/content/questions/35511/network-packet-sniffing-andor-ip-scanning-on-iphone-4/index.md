+++
type = "question"
title = "Network packet sniffing and/or IP scanning on iPhone 4?"
description = '''As a network administrator, I often need to connect into any given point of a wired ethernet network to perform an IP scan, sniff network packets between other devices and/or check the traffic types and flow rates, to troubleshoot the networks. So far, I&#x27;ve been using a laptop for this, but that is ...'''
date = "2014-08-16T11:38:00Z"
lastmod = "2014-08-16T11:38:00Z"
weight = 35511
keywords = [ "kit", "iphone", "network", "troubleshooting" ]
aliases = [ "/questions/35511" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Network packet sniffing and/or IP scanning on iPhone 4?](/questions/35511/network-packet-sniffing-andor-ip-scanning-on-iphone-4)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35511-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35511-score" class="post-score" title="current number of votes">0</div><span id="post-35511-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>As a network administrator, I often need to connect into any given point of a wired ethernet network to perform an IP scan, sniff network packets between other devices and/or check the traffic types and flow rates, to troubleshoot the networks.</p><p>So far, I've been using a laptop for this, but that is highly inconvenient in most cases, as a lot of the network equipment is in tight or elevated locations, where a laptop is just too big and cumbersome.</p><p>It would be preferable to have a way to do the same using my iPhone 4 or a newer model iPhone. Is there any iPhone 4 dock-to-gigabit-ethernet adapter and iOS driver set that would allow my iPhone 4 to do this? Is there any such set that would work with an iPhone 5? I want to do this without jail-breaking the iPhone.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kit" rel="tag" title="see questions tagged &#39;kit&#39;">kit</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '14, 11:38</strong></p><img src="https://secure.gravatar.com/avatar/ba963d84ad8dbfe8344f88f36855590a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zenmaster43&#39;s gravatar image" /><p><span>zenmaster43</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zenmaster43 has no accepted answers">0%</span></p></div></div><div id="comments-container-35511" class="comments-container"></div><div id="comment-tools-35511" class="comment-tools"></div><div class="clear"></div><div id="comment-35511-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

