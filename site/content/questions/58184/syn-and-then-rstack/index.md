+++
type = "question"
title = "syn and then rst&amp;ack"
description = '''What does it mean?  Client - Server (syn)  server - client (rst, ack)  and then its finished it has only 2 packets. '''
date = "2016-12-17T06:52:00Z"
lastmod = "2016-12-17T07:09:00Z"
weight = 58184
keywords = [ "syn", "rst+ack" ]
aliases = [ "/questions/58184" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [syn and then rst&ack](/questions/58184/syn-and-then-rstack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58184-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58184-score" class="post-score" title="current number of votes">0</div><span id="post-58184-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What does it mean?</p><p>Client - Server (syn) server - client (rst, ack)</p><p>and then its finished it has only 2 packets.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-syn" rel="tag" title="see questions tagged &#39;syn&#39;">syn</span> <span class="post-tag tag-link-rst+ack" rel="tag" title="see questions tagged &#39;rst+ack&#39;">rst+ack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '16, 06:52</strong></p><img src="https://secure.gravatar.com/avatar/b44dffcbc9d550d562a112e2b83e786c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="luna&#39;s gravatar image" /><p><span>luna</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="luna has no accepted answers">0%</span></p></div></div><div id="comments-container-58184" class="comments-container"></div><div id="comment-tools-58184" class="comment-tools"></div><div class="clear"></div><div id="comment-58184-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58185"></span>

<div id="answer-container-58185" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58185-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58185-score" class="post-score" title="current number of votes">1</div><span id="post-58185-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="luna has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The client has initiated a TCP connection with a SYN packet which the server has rejected with a RST (along with an ack).</p><p>There could be a few reasons for this, a firewall (on the server or an intermediate device), or no application listening on the port on the server, or an application rejecting the connection.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Dec '16, 07:09</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-58185" class="comments-container"></div><div id="comment-tools-58185" class="comment-tools"></div><div class="clear"></div><div id="comment-58185-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

