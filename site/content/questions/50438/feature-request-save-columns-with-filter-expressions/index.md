+++
type = "question"
title = "Feature Request - Save columns with Filter Expressions"
description = '''It would be a very handy feature to have the Column Selection and Layout saved with the Filter Expressions.'''
date = "2016-02-23T07:22:00Z"
lastmod = "2016-02-23T07:41:00Z"
weight = 50438
keywords = [ "filter", "expressions", "feature-request", "columns" ]
aliases = [ "/questions/50438" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Feature Request - Save columns with Filter Expressions](/questions/50438/feature-request-save-columns-with-filter-expressions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50438-score" class="post-score" title="current number of votes">0</div><span id="post-50438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>It would be a very handy feature to have the Column Selection and Layout saved with the Filter Expressions.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-expressions" rel="tag" title="see questions tagged &#39;expressions&#39;">expressions</span> <span class="post-tag tag-link-feature-request" rel="tag" title="see questions tagged &#39;feature-request&#39;">feature-request</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '16, 07:22</strong></p><img src="https://secure.gravatar.com/avatar/0e6e255ae38f37bedc12f09f67be0aa2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="e2p2&#39;s gravatar image" /><p><span>e2p2</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="e2p2 has no accepted answers">0%</span></p></div></div><div id="comments-container-50438" class="comments-container"><span id="50441"></span><div id="comment-50441" class="comment"><div id="post-50441-score" class="comment-score"></div><div class="comment-text"><p>Could you elaborate what you mean by this? You can already save columns and the layout today. You can even have many different sets of columns and layouts using <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChCustConfigProfilesSection.html">Profiles</a>.</p></div><div id="comment-50441-info" class="comment-info"><span class="comment-age">(23 Feb '16, 07:41)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-50438" class="comment-tools"></div><div class="clear"></div><div id="comment-50438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50439"></span>

<div id="answer-container-50439" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50439-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50439-score" class="post-score" title="current number of votes">1</div><span id="post-50439-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a> is the place for enhancements requests.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '16, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50439" class="comments-container"></div><div id="comment-tools-50439" class="comment-tools"></div><div class="clear"></div><div id="comment-50439-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

