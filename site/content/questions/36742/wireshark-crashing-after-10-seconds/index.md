+++
type = "question"
title = "Wireshark crashing after 10 seconds"
description = '''Hi all, I&#x27;m not sure if I&#x27;m just being dumb (I&#x27;m very new to this programme) but I have a .pcap file I need to open but every time I double click on it, select &#x27;open with&amp;gt; wireshark, or simply open the application by itself, it crashes. Or rather, the icon will bounce on my dock for a couple of s...'''
date = "2014-09-30T21:30:00Z"
lastmod = "2014-10-01T01:58:00Z"
weight = 36742
keywords = [ "macbook", "crashing" ]
aliases = [ "/questions/36742" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark crashing after 10 seconds](/questions/36742/wireshark-crashing-after-10-seconds)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36742-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36742-score" class="post-score" title="current number of votes">0</div><span id="post-36742-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I'm not sure if I'm just being dumb (I'm very new to this programme) but I have a .pcap file I need to open but every time I double click on it, select 'open with&gt; wireshark, or simply open the application by itself, it crashes. Or rather, the icon will bounce on my dock for a couple of seconds and then it will close again. It's getting rather annoying now... I have a MacBook Pro thats running on Mavericks, the version of Wireshark I have is 1.8.15 and the version of XQuartz is 2.7.7. If anyone could help me that would be great!</p><p>B x</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macbook" rel="tag" title="see questions tagged &#39;macbook&#39;">macbook</span> <span class="post-tag tag-link-crashing" rel="tag" title="see questions tagged &#39;crashing&#39;">crashing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '14, 21:30</strong></p><img src="https://secure.gravatar.com/avatar/2a766304750d475428fe06735223b996?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="beffern&#39;s gravatar image" /><p><span>beffern</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="beffern has no accepted answers">0%</span></p></div></div><div id="comments-container-36742" class="comments-container"><span id="36749"></span><div id="comment-36749" class="comment"><div id="post-36749-score" class="comment-score"></div><div class="comment-text"><p>It sounds as though Wireshark isn't even starting, there are a lot of similar questions on this site, mostly around issues with X11. Have you tried looking at other questions tagged OSX, and\or installing the latest stable release (1.12.1).</p></div><div id="comment-36749-info" class="comment-info"><span class="comment-age">(01 Oct '14, 01:58)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-36742" class="comment-tools"></div><div class="clear"></div><div id="comment-36742-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

