+++
type = "question"
title = "mistake by starting of Wireshark - selected wrong application of x11"
description = '''Hello Wireshark-Team, may you help me? I did a mistake in the beginning to start Wireshark.  On the first time if I start Wireshark I should select the x11 software, but I selected a wrong application by mistake.  Now I can not change it afterwards and wireshark does not start correctly.  I already ...'''
date = "2014-11-26T05:35:00Z"
lastmod = "2014-12-01T02:08:00Z"
weight = 38167
keywords = [ "x11", "issue", "beginning", "wireshark" ]
aliases = [ "/questions/38167" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [mistake by starting of Wireshark - selected wrong application of x11](/questions/38167/mistake-by-starting-of-wireshark-selected-wrong-application-of-x11)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38167-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38167-score" class="post-score" title="current number of votes">0</div><span id="post-38167-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Wireshark-Team,</p><p>may you help me? I did a mistake in the beginning to start Wireshark.</p><p>On the first time if I start Wireshark I should select the x11 software, but I selected a wrong application by mistake.</p><p>Now I can not change it afterwards and wireshark does not start correctly.</p><p>I already deinstalled it and installed it new. But I could not do the selection new.</p><p>What can I do? Could you please help me?</p><p>Kind regards Jane</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x11" rel="tag" title="see questions tagged &#39;x11&#39;">x11</span> <span class="post-tag tag-link-issue" rel="tag" title="see questions tagged &#39;issue&#39;">issue</span> <span class="post-tag tag-link-beginning" rel="tag" title="see questions tagged &#39;beginning&#39;">beginning</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '14, 05:35</strong></p><img src="https://secure.gravatar.com/avatar/c20aa940be16a39283bc659bb03d1acc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jane&#39;s gravatar image" /><p><span>jane</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jane has no accepted answers">0%</span></p></div></div><div id="comments-container-38167" class="comments-container"><span id="38246"></span><div id="comment-38246" class="comment"><div id="post-38246-score" class="comment-score"></div><div class="comment-text"><p>Hello everyone :) ,</p><p>does anybody have an idea what I could do?</p><p>Kind regards,</p><p>Jane</p></div><div id="comment-38246-info" class="comment-info"><span class="comment-age">(01 Dec '14, 01:06)</span> <span class="comment-user userinfo">jane</span></div></div></div><div id="comment-tools-38167" class="comment-tools"></div><div class="clear"></div><div id="comment-38167-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38247"></span>

<div id="answer-container-38247" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38247-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38247-score" class="post-score" title="current number of votes">0</div><span id="post-38247-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe search the site and find similar questions and answers, e.g.:</p><p><a href="https://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx">https://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Dec '14, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-38247" class="comments-container"><span id="38249"></span><div id="comment-38249" class="comment"><div id="post-38249-score" class="comment-score"></div><div class="comment-text"><p>Hello Grahamb,</p><p>thanks for your answer.</p><p>This does not work on my side. Did you do anything else? Did you delete and logs or something like that, before you installed it new?</p><p>Kind regards, Jane</p></div><div id="comment-38249-info" class="comment-info"><span class="comment-age">(01 Dec '14, 01:50)</span> <span class="comment-user userinfo">jane</span></div></div><span id="38250"></span><div id="comment-38250" class="comment"><div id="post-38250-score" class="comment-score"></div><div class="comment-text"><p>I don't use OSX, so can't help personally, I thought this question had come up more often than I could actually find with a search. You should edit your question and include the OSX and Wireshark version numbers to allow others to help.</p></div><div id="comment-38250-info" class="comment-info"><span class="comment-age">(01 Dec '14, 02:08)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-38247" class="comment-tools"></div><div class="clear"></div><div id="comment-38247-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

