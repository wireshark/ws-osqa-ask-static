+++
type = "question"
title = "How it works Wireshark with usb antenna tp-link TL-WN8200ND"
description = '''Good morning I have a PC with Windows XP and an antenna usb TL-WN8200ND, please help me I would like it to work with Wireshark. From already thank you very much and I look forward to a prompt and favorable response.'''
date = "2016-06-08T10:51:00Z"
lastmod = "2016-06-08T11:52:00Z"
weight = 53321
keywords = [ "wn8200nd" ]
aliases = [ "/questions/53321" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How it works Wireshark with usb antenna tp-link TL-WN8200ND](/questions/53321/how-it-works-wireshark-with-usb-antenna-tp-link-tl-wn8200nd)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53321-score" class="post-score" title="current number of votes">0</div><span id="post-53321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good morning I have a PC with Windows XP and an antenna usb TL-WN8200ND, please help me I would like it to work with Wireshark. From already thank you very much and I look forward to a prompt and favorable response.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wn8200nd" rel="tag" title="see questions tagged &#39;wn8200nd&#39;">wn8200nd</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '16, 10:51</strong></p><img src="https://secure.gravatar.com/avatar/c4e7a687651747c425982b18029169c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ricardo%20Iribarren&#39;s gravatar image" /><p><span>Ricardo Irib...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ricardo Iribarren has no accepted answers">0%</span></p></div></div><div id="comments-container-53321" class="comments-container"></div><div id="comment-tools-53321" class="comment-tools"></div><div class="clear"></div><div id="comment-53321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53322"></span>

<div id="answer-container-53322" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53322-score" class="post-score" title="current number of votes">0</div><span id="post-53322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark supports monitor mode only if the adapter, driver, and libpcap/WinPcap all support it; WinPcap doesn't support monitor mode, and Npcap doesn't support monitor mode on Windows XP, so you cannot capture in monitor mode on Windows XP.</p><p>All you will be able to do with Wireshark is capture traffic to and from your machine, and broadcast/multicast packets on the network with which you're associated.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jun '16, 11:52</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-53322" class="comments-container"></div><div id="comment-tools-53322" class="comment-tools"></div><div class="clear"></div><div id="comment-53322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

