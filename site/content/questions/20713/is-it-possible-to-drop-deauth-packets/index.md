+++
type = "question"
title = "Is it possible to drop DeAuth Packets?"
description = '''Is it possible to drop / reject deAuthentication packets ??? If yes how??  Beacause I want to prevent DeAuthentication attack performed on my wireless LAN.. Can scapy tool with python can help to solve this problem?'''
date = "2013-04-22T09:41:00Z"
lastmod = "2013-04-22T12:29:00Z"
weight = 20713
keywords = [ "python", "scapy", "deauthentication" ]
aliases = [ "/questions/20713" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to drop DeAuth Packets?](/questions/20713/is-it-possible-to-drop-deauth-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20713-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20713-score" class="post-score" title="current number of votes">0</div><span id="post-20713-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to drop / reject deAuthentication packets ??? If yes how?? Beacause I want to prevent DeAuthentication attack performed on my wireless LAN.. Can scapy tool with python can help to solve this problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-python" rel="tag" title="see questions tagged &#39;python&#39;">python</span> <span class="post-tag tag-link-scapy" rel="tag" title="see questions tagged &#39;scapy&#39;">scapy</span> <span class="post-tag tag-link-deauthentication" rel="tag" title="see questions tagged &#39;deauthentication&#39;">deauthentication</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '13, 09:41</strong></p><img src="https://secure.gravatar.com/avatar/aac4ec05fa1d431151018bc544c6ca3a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aki4891&#39;s gravatar image" /><p><span>aki4891</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aki4891 has no accepted answers">0%</span></p></div></div><div id="comments-container-20713" class="comments-container"></div><div id="comment-tools-20713" class="comment-tools"></div><div class="clear"></div><div id="comment-20713-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20715"></span>

<div id="answer-container-20715" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20715-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20715-score" class="post-score" title="current number of votes">0</div><span id="post-20715-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is it possible to drop / reject deAuthentication packets ???</p></blockquote><p>Wireshark is a passive network troubleshooting tool and thus it will not send any data into the network (except maybe DNS requests, if that feature is enabled). Thus, you cannot drop/reject any Wifi/Wlan packets with Wireshark.</p><blockquote><p>Can scapy tool with python can help to solve this problem?</p></blockquote><p>maybe, but that is a question you better ask in a scapy related forum.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Apr '13, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-20715" class="comments-container"></div><div id="comment-tools-20715" class="comment-tools"></div><div class="clear"></div><div id="comment-20715-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

