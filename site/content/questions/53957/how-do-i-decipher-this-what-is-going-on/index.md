+++
type = "question"
title = "how do i decipher this? what is going on?"
description = '''Hello,  I am trying to figure out why I&#x27;m having some communications issues with a device and I started doing a capture which is in this screen shot. Can someone help me understand what I&#x27;m seeing? Thanks! '''
date = "2016-07-09T13:31:00Z"
lastmod = "2016-07-09T13:31:00Z"
weight = 53957
keywords = [ "segment_not_captured" ]
aliases = [ "/questions/53957" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how do i decipher this? what is going on?](/questions/53957/how-do-i-decipher-this-what-is-going-on)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53957-score" class="post-score" title="current number of votes">0</div><span id="post-53957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am trying to figure out why I'm having some communications issues with a device and I started doing a capture which is in this screen shot. Can someone help me understand what I'm seeing?</p><p>Thanks! <img src="https://osqa-ask.wireshark.org/upfiles/Capture_Tlt9N38.JPG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-segment_not_captured" rel="tag" title="see questions tagged &#39;segment_not_captured&#39;">segment_not_captured</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '16, 13:31</strong></p><img src="https://secure.gravatar.com/avatar/fcc345313d9fb73e097a0aa9a1299813?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dunkin4gzus&#39;s gravatar image" /><p><span>dunkin4gzus</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dunkin4gzus has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jul '16, 08:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-53957" class="comments-container"></div><div id="comment-tools-53957" class="comment-tools"></div><div class="clear"></div><div id="comment-53957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

