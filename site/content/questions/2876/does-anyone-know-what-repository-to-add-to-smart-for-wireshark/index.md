+++
type = "question"
title = "Does anyone know what repository to add to smart for wireshark?"
description = '''I&#x27;m in a situation where I can only use smart to install packages. I&#x27;m trying to add one of the repositories that apt uses to install wireshark which is: http://us.archive.ubuntu.com/ubuntu/  So I used this command: smart channel --add Ubuntu-Universe type=apt-deb name=&quot;Universe&quot; baseurl=http:// us....'''
date = "2011-03-16T12:03:00Z"
lastmod = "2011-03-16T23:55:00Z"
weight = 2876
keywords = [ "install", "smart", "wireshark" ]
aliases = [ "/questions/2876" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Does anyone know what repository to add to smart for wireshark?](/questions/2876/does-anyone-know-what-repository-to-add-to-smart-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2876-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2876-score" class="post-score" title="current number of votes">0</div><span id="post-2876-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm in a situation where I can only use smart to install packages. I'm trying to add one of the repositories that apt uses to install wireshark which is:</p><pre><code>http://us.archive.ubuntu.com/ubuntu/</code></pre><p>So I used this command:</p><pre><code>smart channel --add Ubuntu-Universe type=apt-deb name=&quot;Universe&quot; baseurl=http://    us.archive.ubuntu.com/ubuntu/ distribution=maverick</code></pre><p>But unfortunately that doesn't work. Does anyone know how to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-smart" rel="tag" title="see questions tagged &#39;smart&#39;">smart</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '11, 12:03</strong></p><img src="https://secure.gravatar.com/avatar/3d3535b19a6debac9e2b855465a2027b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rodayo&#39;s gravatar image" /><p><span>Rodayo</span><br />
<span class="score" title="61 reputation points">61</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rodayo has no accepted answers">0%</span></p></div></div><div id="comments-container-2876" class="comments-container"><span id="2886"></span><div id="comment-2886" class="comment"><div id="post-2886-score" class="comment-score">1</div><div class="comment-text"><p>You may want to ask the Smart mailing list.</p></div><div id="comment-2886-info" class="comment-info"><span class="comment-age">(16 Mar '11, 23:55)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-2876" class="comment-tools"></div><div class="clear"></div><div id="comment-2876-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

