+++
type = "question"
title = "Windows Server 2012"
description = '''When a new OS comes out like Release Candidate Windows Server 2012, do you actively test with it?'''
date = "2012-08-22T03:06:00Z"
lastmod = "2012-08-22T03:19:00Z"
weight = 13812
keywords = [ "2012" ]
aliases = [ "/questions/13812" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows Server 2012](/questions/13812/windows-server-2012)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13812-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13812-score" class="post-score" title="current number of votes">0</div><span id="post-13812-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When a new OS comes out like Release Candidate Windows Server 2012, do you actively test with it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-2012" rel="tag" title="see questions tagged &#39;2012&#39;">2012</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '12, 03:06</strong></p><img src="https://secure.gravatar.com/avatar/18b90188e619589890256bce8bec51b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GaryChaulklin&#39;s gravatar image" /><p><span>GaryChaulklin</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GaryChaulklin has no accepted answers">0%</span></p></div></div><div id="comments-container-13812" class="comments-container"></div><div id="comment-tools-13812" class="comment-tools"></div><div class="clear"></div><div id="comment-13812-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13813"></span>

<div id="answer-container-13813" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13813-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13813-score" class="post-score" title="current number of votes">0</div><span id="post-13813-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Someone might on their own behalf, but there is no <a href="http://buildbot.wireshark.org/trunk/builders">buildbot</a> for Windows 8 or Server 2012 yet, so no "official" tests. I would suggest that you check on the <a href="http://www.wireshark.org/lists/wireshark-dev">dev</a> mailing list for info as I'm sure it will come up there at some point.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Aug '12, 03:19</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-13813" class="comments-container"></div><div id="comment-tools-13813" class="comment-tools"></div><div class="clear"></div><div id="comment-13813-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

