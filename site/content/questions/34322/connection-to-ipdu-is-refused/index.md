+++
type = "question"
title = "[closed] Connection to IPDU is refused"
description = '''Hello, I tried to connect to the IPDU-0 of my MME Server via port=5999, but Ii get the following error: &quot;Can&#x27;t get list of interfaces. Error code 10061&quot;.  The fact is that the Unit is ok. Thank you in advance for your help.'''
date = "2014-07-01T07:00:00Z"
lastmod = "2014-07-01T07:00:00Z"
weight = 34322
keywords = [ "ipdu" ]
aliases = [ "/questions/34322" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Connection to IPDU is refused](/questions/34322/connection-to-ipdu-is-refused)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34322-score" class="post-score" title="current number of votes">0</div><span id="post-34322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I tried to connect to the IPDU-0 of my MME Server via port=5999, but Ii get the following error:</p><p>"Can't get list of interfaces. Error code 10061".</p><p>The fact is that the Unit is ok.</p><p>Thank you in advance for your help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipdu" rel="tag" title="see questions tagged &#39;ipdu&#39;">ipdu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jul '14, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/7afafde071af2b1f8ecd11f80bffd076?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oxibilko&#39;s gravatar image" /><p><span>oxibilko</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oxibilko has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>08 Jul '14, 03:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-34322" class="comments-container"></div><div id="comment-tools-34322" class="comment-tools"></div><div class="clear"></div><div id="comment-34322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 08 Jul '14, 03:38

</div>

</div>

</div>

