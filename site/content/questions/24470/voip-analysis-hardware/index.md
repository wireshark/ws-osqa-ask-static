+++
type = "question"
title = "VoIP analysis hardware"
description = '''my othet project in wire shark is VoIP analysis...so plz tell me which hardware use for the VoIP anlaysis..give me particular hardware name radio analysis... :::::::::::::thankyou:::::::::::::'''
date = "2013-09-09T04:28:00Z"
lastmod = "2013-09-09T06:30:00Z"
weight = 24470
keywords = [ "hardware", "voip", "analysis" ]
aliases = [ "/questions/24470" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP analysis hardware](/questions/24470/voip-analysis-hardware)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24470-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24470-score" class="post-score" title="current number of votes">-1</div><span id="post-24470-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>my othet project in wire shark is VoIP analysis...so plz tell me which hardware use for the VoIP anlaysis..give me particular hardware name radio analysis...</p><p>:::::::::::::thankyou:::::::::::::</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hardware" rel="tag" title="see questions tagged &#39;hardware&#39;">hardware</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '13, 04:28</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Sep '13, 05:11</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-24470" class="comments-container"><span id="24473"></span><div id="comment-24473" class="comment"><div id="post-24473-score" class="comment-score"></div><div class="comment-text"><p>What exactly is your question ?</p></div><div id="comment-24473-info" class="comment-info"><span class="comment-age">(09 Sep '13, 05:00)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="24478"></span><div id="comment-24478" class="comment"><div id="post-24478-score" class="comment-score">1</div><div class="comment-text"><blockquote><p><strong>which hardware</strong> use <strong>for VoIP anlaysis</strong></p></blockquote><p>I usually use a laptop.</p></div><div id="comment-24478-info" class="comment-info"><span class="comment-age">(09 Sep '13, 06:30)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24470" class="comment-tools"></div><div class="clear"></div><div id="comment-24470-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

