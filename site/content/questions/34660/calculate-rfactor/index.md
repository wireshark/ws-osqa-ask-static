+++
type = "question"
title = "Calculate RFactor"
description = '''Hi, I have to calculate rfactor with the itu-t&#x27;s e-mocdel http://www.itu.int/ITU-T/studygroups/com12/emodelv1/calcul.php so i have to insert the value of rtp analysis in this web page....can anyone explain me how to use these values??because these are for a single packet how I can use this in genera...'''
date = "2014-07-15T06:20:00Z"
lastmod = "2014-07-15T06:43:00Z"
weight = 34660
keywords = [ "e-model", "rfactor", "itu-t", "voip" ]
aliases = [ "/questions/34660" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Calculate RFactor](/questions/34660/calculate-rfactor)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34660-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34660-score" class="post-score" title="current number of votes">0</div><span id="post-34660-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have to calculate rfactor with the itu-t's e-mocdel <a href="http://www.itu.int/ITU-T/studygroups/com12/emodelv1/calcul.php">http://www.itu.int/ITU-T/studygroups/com12/emodelv1/calcul.php</a> so i have to insert the value of rtp analysis in this web page....can anyone explain me how to use these values??because these are for a single packet how I can use this in general?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-e-model" rel="tag" title="see questions tagged &#39;e-model&#39;">e-model</span> <span class="post-tag tag-link-rfactor" rel="tag" title="see questions tagged &#39;rfactor&#39;">rfactor</span> <span class="post-tag tag-link-itu-t" rel="tag" title="see questions tagged &#39;itu-t&#39;">itu-t</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '14, 06:20</strong></p><img src="https://secure.gravatar.com/avatar/e778b32221e09e52585a198f1fc32592?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Francesco%20Rovito&#39;s gravatar image" /><p><span>Francesco Ro...</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Francesco Rovito has no accepted answers">0%</span></p></div></div><div id="comments-container-34660" class="comments-container"></div><div id="comment-tools-34660" class="comment-tools"></div><div class="clear"></div><div id="comment-34660-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34662"></span>

<div id="answer-container-34662" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34662-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34662-score" class="post-score" title="current number of votes">0</div><span id="post-34662-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You already <a href="http://ask.wireshark.org/questions/34537/how-to-measure-mos-parameter">asked a very similar question</a>, and to be honest I don't understand what you are trying to do.</p><p>Wireshark neither calculates the R-factor nor the MOS value, so <strong>what exactly is your question</strong>?</p><p>If your question is: How can I show those values within Wireshark, then the answer is: You can't, as Wireshark does not calculate those values.</p><p>If your question is: How can I calculate those values within your own program, then the answer would be: Find the algorithm of those values with google, and translate that into your programming language.</p><p>If neither of the above is your question, then please be more precise regarding the nature of your question, meaning please add some more details!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '14, 06:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-34662" class="comments-container"></div><div id="comment-tools-34662" class="comment-tools"></div><div class="clear"></div><div id="comment-34662-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

