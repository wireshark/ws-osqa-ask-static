+++
type = "question"
title = "Capturing packets on WAN link"
description = '''How do I capture packets on WAN link and then figure out Latency?'''
date = "2013-09-09T12:26:00Z"
lastmod = "2013-09-10T15:11:00Z"
weight = 24491
keywords = [ "latency" ]
aliases = [ "/questions/24491" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing packets on WAN link](/questions/24491/capturing-packets-on-wan-link)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24491-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24491-score" class="post-score" title="current number of votes">0</div><span id="post-24491-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I capture packets on WAN link and then figure out Latency?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '13, 12:26</strong></p><img src="https://secure.gravatar.com/avatar/c205ce05701a5ba45f2e343d7133bba8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nube67&#39;s gravatar image" /><p><span>Nube67</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nube67 has no accepted answers">0%</span></p></div></div><div id="comments-container-24491" class="comments-container"><span id="24539"></span><div id="comment-24539" class="comment"><div id="post-24539-score" class="comment-score"></div><div class="comment-text"><p>two questions:</p><ol><li>on the <strong>WAN</strong> link of what kind of device?</li><li>Latency of the <strong>WAN</strong> link?</li></ol></div><div id="comment-24539-info" class="comment-info"><span class="comment-age">(10 Sep '13, 15:11)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24491" class="comment-tools"></div><div class="clear"></div><div id="comment-24491-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

