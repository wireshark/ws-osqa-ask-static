+++
type = "question"
title = "Does wireshark sort unsorted packets"
description = '''Hi all,  Does wireshark sort unsorted packets before showing them ? Thanks for your help'''
date = "2014-05-05T08:26:00Z"
lastmod = "2014-05-05T08:31:00Z"
weight = 32538
keywords = [ "packets", "unsorted" ]
aliases = [ "/questions/32538" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does wireshark sort unsorted packets](/questions/32538/does-wireshark-sort-unsorted-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32538-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32538-score" class="post-score" title="current number of votes">0</div><span id="post-32538-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Does wireshark sort unsorted packets before showing them ?</p><p>Thanks for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-unsorted" rel="tag" title="see questions tagged &#39;unsorted&#39;">unsorted</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '14, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/31856543dad1a12f24073c17126cb1e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ikuzar&#39;s gravatar image" /><p><span>ikuzar</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ikuzar has no accepted answers">0%</span></p></div></div><div id="comments-container-32538" class="comments-container"></div><div id="comment-tools-32538" class="comment-tools"></div><div class="clear"></div><div id="comment-32538-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32539"></span>

<div id="answer-container-32539" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32539-score" class="post-score" title="current number of votes">0</div><span id="post-32539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark does not sort packets. When a capture file is loaded the packets are displayed in the exact order as found in the file. You can sort the columns by clicking on the column headers, but that won't change the sequence in which the packets are processed. Which means that e.g. the TCP expert won't change it's findings.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '14, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32539" class="comments-container"></div><div id="comment-tools-32539" class="comment-tools"></div><div class="clear"></div><div id="comment-32539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

