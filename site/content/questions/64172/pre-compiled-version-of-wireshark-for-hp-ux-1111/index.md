+++
type = "question"
title = "Pre-Compiled version of wireshark for Hp-Ux 11.11"
description = '''I would like to know if there is a pre-compiled version of wireshark for HP-UX 11.11. If so where can I download such a copy.'''
date = "2017-10-24T14:46:00Z"
lastmod = "2017-10-24T23:37:00Z"
weight = 64172
keywords = [ "hp-ux", "11.11" ]
aliases = [ "/questions/64172" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Pre-Compiled version of wireshark for Hp-Ux 11.11](/questions/64172/pre-compiled-version-of-wireshark-for-hp-ux-1111)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64172-score" class="post-score" title="current number of votes">0</div><span id="post-64172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if there is a pre-compiled version of wireshark for HP-UX 11.11. If so where can I download such a copy.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hp-ux" rel="tag" title="see questions tagged &#39;hp-ux&#39;">hp-ux</span> <span class="post-tag tag-link-11.11" rel="tag" title="see questions tagged &#39;11.11&#39;">11.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '17, 14:46</strong></p><img src="https://secure.gravatar.com/avatar/71808ad36dd4665d0f4157e051413a0b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sinestro0097&#39;s gravatar image" /><p><span>sinestro0097</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sinestro0097 has no accepted answers">0%</span></p></div></div><div id="comments-container-64172" class="comments-container"></div><div id="comment-tools-64172" class="comment-tools"></div><div class="clear"></div><div id="comment-64172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64176"></span>

<div id="answer-container-64176" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64176-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64176-score" class="post-score" title="current number of votes">0</div><span id="post-64176-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://hpux.connect.org.uk/hppd/hpux/Gtk/Applications/wireshark-1.10.5/">Found</a> on the <a href="https://www.wireshark.org/download.html">download page</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Oct '17, 23:37</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-64176" class="comments-container"></div><div id="comment-tools-64176" class="comment-tools"></div><div class="clear"></div><div id="comment-64176-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

