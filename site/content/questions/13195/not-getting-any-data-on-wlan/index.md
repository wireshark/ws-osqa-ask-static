+++
type = "question"
title = "Not getting any data on WLAN"
description = '''UPDATE: I&#x27;m getting my Q51 ZigBee sniffer to show up, but not getting any packets. Need to know what I need to do to get the packets.... I am in need of desperate help. Trying to sniff out zigbee signals on the WLAN and can&#x27;t pull it up on the interface. Also, nothing is showing up in 802.11. Please...'''
date = "2012-07-31T13:11:00Z"
lastmod = "2012-08-01T16:45:00Z"
weight = 13195
keywords = [ "zigbee" ]
aliases = [ "/questions/13195" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not getting any data on WLAN](/questions/13195/not-getting-any-data-on-wlan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13195-score" class="post-score" title="current number of votes">0</div><span id="post-13195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>UPDATE: I'm getting my Q51 ZigBee sniffer to show up, but not getting any packets. Need to know what I need to do to get the packets....</p><p>I am in need of desperate help. Trying to sniff out zigbee signals on the WLAN and can't pull it up on the interface. Also, nothing is showing up in 802.11. Please help!</p><p><img src="http://" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zigbee" rel="tag" title="see questions tagged &#39;zigbee&#39;">zigbee</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '12, 13:11</strong></p><img src="https://secure.gravatar.com/avatar/0d605f5de9b18aa71cf55a76d14fc896?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="J-Bird&#39;s gravatar image" /><p><span>J-Bird</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="J-Bird has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Jul '12, 15:34</strong> </span></p></div></div><div id="comments-container-13195" class="comments-container"><span id="13255"></span><div id="comment-13255" class="comment"><div id="post-13255-score" class="comment-score"></div><div class="comment-text"><p>what is your</p><ul><li>OS / OS version of the machine running Wireshark</li><li>Wireshark version (wireshark -v)</li><li>capture setup (who is sending, where is the WLAN AP, etc.)</li></ul><p>Are there</p><ul><li>any error messages</li><li>any packets at all (broadcast, etc.)</li></ul><p>Regards<br />
Kurt</p></div><div id="comment-13255-info" class="comment-info"><span class="comment-age">(01 Aug '12, 08:14)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="13292"></span><div id="comment-13292" class="comment"><div id="post-13292-score" class="comment-score"></div><div class="comment-text"><p>I'm running vista 32bit Version 1.2.2-Exegin-1.2 on Wireshark Digi XBee Range Extender</p><p>No error messages and no packets at all. I have tried running different versions of wireshark (1.8.1 and 1.6.9), those I wasn't able to pull up sniffer on the Ethernet port.</p><p>Thanks for your continuous help, Jathan</p></div><div id="comment-13292-info" class="comment-info"><span class="comment-age">(01 Aug '12, 11:49)</span> <span class="comment-user userinfo">J-Bird</span></div></div><span id="13302"></span><div id="comment-13302" class="comment"><div id="post-13302-score" class="comment-score"></div><div class="comment-text"><p>Let me clarify this a bit:</p><p>You do have a Q51 Panalyzer (ZigBee sniffer) in place and you configured it to send data to a system where Wireshark is installed and started. Right?</p><p>Now, the problem is, that you don't see any ZigBee traffic in Wireshark.</p><p>If so, please</p><ul><li>double check your configuration of the Q51 Panalyzer. Maybe it's not sending the data to the right box.</li><li>disable the windows firewall on your Wireshark system. Maybe the firewall blocks the traffic from the Q51 Panalyzer</li></ul><blockquote><p>those I wasn't able to pull up sniffer on the Ethernet port.</p></blockquote><p>I'm not sure what that means.</p></div><div id="comment-13302-info" class="comment-info"><span class="comment-age">(01 Aug '12, 16:45)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-13195" class="comment-tools"></div><div class="clear"></div><div id="comment-13195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

