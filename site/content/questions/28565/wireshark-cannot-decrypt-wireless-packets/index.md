+++
type = "question"
title = "Wireshark cannot decrypt wireless packets"
description = '''Wireshark cannot decrypt WEP packets with 64-bit (10 digits) WEP key given. What it shows is just a bunch of 802.11 packets and LLC packets, and IP addresses are even not visible. Is it because Wireshark detects my key as invalid, or is it because the captured packets are corrupted?'''
date = "2014-01-04T00:04:00Z"
lastmod = "2014-01-12T15:03:00Z"
weight = 28565
keywords = [ "linux", "decryption", "wep", "802.11", "ubuntu" ]
aliases = [ "/questions/28565" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark cannot decrypt wireless packets](/questions/28565/wireshark-cannot-decrypt-wireless-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28565-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28565-score" class="post-score" title="current number of votes">0</div><span id="post-28565-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark cannot decrypt WEP packets with 64-bit (10 digits) WEP key given.</p><p>What it shows is just a bunch of 802.11 packets and LLC packets, and IP addresses are even not visible.</p><p>Is it because Wireshark detects my key as invalid, or is it because the captured packets are corrupted?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wep" rel="tag" title="see questions tagged &#39;wep&#39;">wep</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '14, 00:04</strong></p><img src="https://secure.gravatar.com/avatar/de6d260a6af9d4676547ee293e677a8d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jinoh67&#39;s gravatar image" /><p><span>jinoh67</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jinoh67 has no accepted answers">0%</span></p></div></div><div id="comments-container-28565" class="comments-container"><span id="28832"></span><div id="comment-28832" class="comment"><div id="post-28832-score" class="comment-score"></div><div class="comment-text"><p>can you post a screenshot of the</p><ul><li>frames <strong>without</strong> decryption</li><li>the <strong>same</strong> frames <strong>decrypted</strong></li></ul><p>Please add the packet details pane of one frame</p></div><div id="comment-28832-info" class="comment-info"><span class="comment-age">(12 Jan '14, 15:03)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28565" class="comment-tools"></div><div class="clear"></div><div id="comment-28565-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

