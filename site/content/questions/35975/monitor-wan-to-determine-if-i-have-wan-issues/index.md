+++
type = "question"
title = "monitor WAN to determine if I have WAN issues"
description = '''Hello, I want to use wireshark to monitor my internet connection. My provider tells me they have no problems and it is all on &quot;my side&quot;. What would be the best way to monitor if I am having problems with my provider. Perhaps the issue is on my side but I don&#x27;t really know and would like to check thi...'''
date = "2014-09-03T15:11:00Z"
lastmod = "2014-09-03T15:11:00Z"
weight = 35975
keywords = [ "wan" ]
aliases = [ "/questions/35975" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [monitor WAN to determine if I have WAN issues](/questions/35975/monitor-wan-to-determine-if-i-have-wan-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35975-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35975-score" class="post-score" title="current number of votes">0</div><span id="post-35975-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I want to use wireshark to monitor my internet connection. My provider tells me they have no problems and it is all on "my side". What would be the best way to monitor if I am having problems with my provider.</p><p>Perhaps the issue is on my side but I don't really know and would like to check this out.</p><p>I use Comcast as a provider.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wan" rel="tag" title="see questions tagged &#39;wan&#39;">wan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '14, 15:11</strong></p><img src="https://secure.gravatar.com/avatar/f632b5c8ae227e2c79b4f4116fef01ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tucktech&#39;s gravatar image" /><p><span>tucktech</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tucktech has no accepted answers">0%</span></p></div></div><div id="comments-container-35975" class="comments-container"></div><div id="comment-tools-35975" class="comment-tools"></div><div class="clear"></div><div id="comment-35975-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

