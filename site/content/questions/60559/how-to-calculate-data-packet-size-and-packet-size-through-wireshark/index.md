+++
type = "question"
title = "How to calculate Data Packet Size and Packet Size through wireshark ?"
description = '''I&#x27;m working on ML for network classification and I need help for setting attributes . I need to find how to calculate Data Packet Size and Packet size. Thank you'''
date = "2017-04-04T00:30:00Z"
lastmod = "2017-04-04T00:30:00Z"
weight = 60559
keywords = [ "data", "packet", "size" ]
aliases = [ "/questions/60559" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to calculate Data Packet Size and Packet Size through wireshark ?](/questions/60559/how-to-calculate-data-packet-size-and-packet-size-through-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60559-score" class="post-score" title="current number of votes">0</div><span id="post-60559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm working on ML for network classification and I need help for setting attributes . I need to find how to calculate Data Packet Size and Packet size. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '17, 00:30</strong></p><img src="https://secure.gravatar.com/avatar/a6fcb1d4415eed6a4522aae01c4ababf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kartik&#39;s gravatar image" /><p><span>kartik</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kartik has no accepted answers">0%</span></p></div></div><div id="comments-container-60559" class="comments-container"></div><div id="comment-tools-60559" class="comment-tools"></div><div class="clear"></div><div id="comment-60559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

