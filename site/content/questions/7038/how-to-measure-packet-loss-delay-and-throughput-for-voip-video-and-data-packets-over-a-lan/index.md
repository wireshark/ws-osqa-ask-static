+++
type = "question"
title = "How to measure packet loss, delay and throughput for VoIP, Video and Data packets over a LAN?"
description = '''hii can any1 help me with finding throughput, delay and packet loss in a LAN for voip, video and data packets transmitted over the network..its for my dissertation and i dont hv much time left..so damn tensed here! the LAN setup is this: 3 wired connections and 1 one wireless connection. one of the ...'''
date = "2011-10-22T00:10:00Z"
lastmod = "2011-10-22T00:10:00Z"
weight = 7038
keywords = [ "delay", "loss", "lan", "throughput", "qos" ]
aliases = [ "/questions/7038" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to measure packet loss, delay and throughput for VoIP, Video and Data packets over a LAN?](/questions/7038/how-to-measure-packet-loss-delay-and-throughput-for-voip-video-and-data-packets-over-a-lan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7038-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7038-score" class="post-score" title="current number of votes">0</div><span id="post-7038-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hii can any1 help me with finding throughput, delay and packet loss in a LAN for voip, video and data packets transmitted over the network..its for my dissertation and i dont hv much time left..so damn tensed here! the LAN setup is this: 3 wired connections and 1 one wireless connection. one of the wired systems act as the server (just where the video files are hosted, and where the PBX server (3CX Phone system) for VoIP is installed). the video i tried to play over the network was a blue ray movie and there are VoIP clients in all the other systems in the LAN (3CX Phone). i jus tried to play the video in the client systems through the network from a shared folder in the server. is this the right thing to do? (because the data and video packets are both TCP) and wot im supposed to do is video streaming.. found the option of tcp stream graph in the statistics menu from which we can get graphs for throughput, RTT etc. is RTT and delay the same thing? for measuring the throughput of a connection (A to B or B to A) how exactly shud we proceed? like, wen to start and stop the capture or which packets to chose? im getting graphs for throughput, RTT and all but jus not sure if those are the ones i want.. hv to measure values of those qos parameters for different cases like video + data packets, data + voip, video + voip and all of them together..should we take the readings from the wireshark in the server or from the clients in the LAN.. hv got wireshark installed in all the systems..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-loss" rel="tag" title="see questions tagged &#39;loss&#39;">loss</span> <span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span> <span class="post-tag tag-link-qos" rel="tag" title="see questions tagged &#39;qos&#39;">qos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '11, 00:10</strong></p><img src="https://secure.gravatar.com/avatar/45166a5df521f83b8cdc4fcb4342f98b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Arun%20Kripal&#39;s gravatar image" /><p><span>Arun Kripal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Arun Kripal has no accepted answers">0%</span></p></div></div><div id="comments-container-7038" class="comments-container"></div><div id="comment-tools-7038" class="comment-tools"></div><div class="clear"></div><div id="comment-7038-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

