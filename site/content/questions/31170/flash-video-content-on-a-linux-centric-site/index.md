+++
type = "question"
title = "Flash video content on a Linux-centric site"
description = '''Isn&#x27;t the use of Flash on this site ill-advised? There are more open video formats, that I am sure will be favored more in the open source community. PS. I am on OSX (the latest), with Flash Player (temporary unblocked for this site) at the very latest version, but no cigar.'''
date = "2014-03-26T05:29:00Z"
lastmod = "2014-03-26T08:30:00Z"
weight = 31170
keywords = [ "flash", "video" ]
aliases = [ "/questions/31170" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Flash video content on a Linux-centric site](/questions/31170/flash-video-content-on-a-linux-centric-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31170-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31170-score" class="post-score" title="current number of votes">0</div><span id="post-31170-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Isn't the use of Flash on this site ill-advised? There are more open video formats, that I am sure will be favored more in the open source community. PS. I am on OSX (the latest), with Flash Player (temporary unblocked for this site) at the very latest version, but no cigar.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flash" rel="tag" title="see questions tagged &#39;flash&#39;">flash</span> <span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '14, 05:29</strong></p><img src="https://secure.gravatar.com/avatar/9ec258cf9d95d249c280dd0f56753a1a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fvimac21&#39;s gravatar image" /><p><span>fvimac21</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fvimac21 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Mar '14, 07:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-31170" class="comments-container"><span id="31177"></span><div id="comment-31177" class="comment"><div id="post-31177-score" class="comment-score"></div><div class="comment-text"><p>Sorry for reporting this user and deleting the post. At the time I couldn't see the relevance of the question. No offence intended.</p></div><div id="comment-31177-info" class="comment-info"><span class="comment-age">(26 Mar '14, 08:30)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-31170" class="comment-tools"></div><div class="clear"></div><div id="comment-31170-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31172"></span>

<div id="answer-container-31172" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31172-score" class="post-score" title="current number of votes">0</div><span id="post-31172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Where is the site trying to play video, exactly? As far as I can tell from grepping the OSQA sources the only place it uses flash is to automatically copy text using <a href="https://github.com/dzone/osqa/tree/master/forum/skins/default/media/js">ZeroClipboard</a>. There isn't a good cross-platform way to copy text using pure JavaScript so unfortunately Flash is the best option at the present time.</p><p>(BTW, what leads you to think this site is Linux-centric? The majority of our users are on Windows but a significant portion use Linux and OS X.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '14, 07:51</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-31172" class="comments-container"></div><div id="comment-tools-31172" class="comment-tools"></div><div class="clear"></div><div id="comment-31172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

