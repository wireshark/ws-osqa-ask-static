+++
type = "question"
title = "Casandra CQL Dissector"
description = '''Hi All, Could some one help me Casandra CQL Dissector in wireshark? Thanks Pradeep'''
date = "2016-04-05T21:25:00Z"
lastmod = "2016-04-05T21:25:00Z"
weight = 51431
keywords = [ "cql" ]
aliases = [ "/questions/51431" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Casandra CQL Dissector](/questions/51431/casandra-cql-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51431-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51431-score" class="post-score" title="current number of votes">0</div><span id="post-51431-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>Could some one help me Casandra CQL Dissector in wireshark?</p><p>Thanks Pradeep</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cql" rel="tag" title="see questions tagged &#39;cql&#39;">cql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '16, 21:25</strong></p><img src="https://secure.gravatar.com/avatar/1bbd308a0295aaa63d9e14625c137afb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pradeep%20Mishra&#39;s gravatar image" /><p><span>Pradeep Mishra</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pradeep Mishra has no accepted answers">0%</span></p></div></div><div id="comments-container-51431" class="comments-container"></div><div id="comment-tools-51431" class="comment-tools"></div><div class="clear"></div><div id="comment-51431-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

