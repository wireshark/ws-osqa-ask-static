+++
type = "question"
title = "Wireless toolbar options greyed out"
description = '''I used airmon-ng to create a monitor interface on my wireless card, and airodump-ng to capture some packets. However, when I load the capture file into wireshark to decrypt it, I am not able to do so. have enabled wireless decryption in the preferences, and added wpa keys. However, the wireless tool...'''
date = "2013-12-09T10:56:00Z"
lastmod = "2013-12-09T10:56:00Z"
weight = 27958
keywords = [ "wireless_toolbar" ]
aliases = [ "/questions/27958" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless toolbar options greyed out](/questions/27958/wireless-toolbar-options-greyed-out)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27958-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27958-score" class="post-score" title="current number of votes">0</div><span id="post-27958-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I used airmon-ng to create a monitor interface on my wireless card, and airodump-ng to capture some packets. However, when I load the capture file into wireshark to decrypt it, I am not able to do so. have enabled wireless decryption in the preferences, and added wpa keys. However, the wireless toolbar options are all greyed out except for the button that switches between "wireshark" and none, and the decryption keys button is completely missing. I have tried toggling the "Assume Packets Have FCS" and the "Ignore the Protection bit" options, all to no avail. What am I doing wrong? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless_toolbar" rel="tag" title="see questions tagged &#39;wireless_toolbar&#39;">wireless_toolbar</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '13, 10:56</strong></p><img src="https://secure.gravatar.com/avatar/a0f3c3d17546daf333e21371fd870bd6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nomadpenguin&#39;s gravatar image" /><p><span>nomadpenguin</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nomadpenguin has no accepted answers">0%</span></p></div></div><div id="comments-container-27958" class="comments-container"></div><div id="comment-tools-27958" class="comment-tools"></div><div class="clear"></div><div id="comment-27958-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

