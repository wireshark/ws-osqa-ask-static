+++
type = "question"
title = "New Office Environment (NOE) RFC"
description = '''I&#x27;m looking for any kind of documentation regarding the structure of NOE protocol communications. Wireshark supports the NOE protocol, so I&#x27;m wondering if you guys can help point me in the right direction. I&#x27;ve looked around for RFCs and whatnot, but apparently I&#x27;m not using the right search criteri...'''
date = "2012-12-06T12:22:00Z"
lastmod = "2012-12-06T14:16:00Z"
weight = 16653
keywords = [ "environment", "noe", "rfc", "office", "new" ]
aliases = [ "/questions/16653" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [New Office Environment (NOE) RFC](/questions/16653/new-office-environment-noe-rfc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16653-score" class="post-score" title="current number of votes">0</div><span id="post-16653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm looking for any kind of documentation regarding the structure of NOE protocol communications. Wireshark supports the NOE protocol, so I'm wondering if you guys can help point me in the right direction. I've looked around for RFCs and whatnot, but apparently I'm not using the right search criteria. Any help would be extremely appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-environment" rel="tag" title="see questions tagged &#39;environment&#39;">environment</span> <span class="post-tag tag-link-noe" rel="tag" title="see questions tagged &#39;noe&#39;">noe</span> <span class="post-tag tag-link-rfc" rel="tag" title="see questions tagged &#39;rfc&#39;">rfc</span> <span class="post-tag tag-link-office" rel="tag" title="see questions tagged &#39;office&#39;">office</span> <span class="post-tag tag-link-new" rel="tag" title="see questions tagged &#39;new&#39;">new</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '12, 12:22</strong></p><img src="https://secure.gravatar.com/avatar/9c9b4a62eb85558bcf0ab16ea806f0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nullifi3d&#39;s gravatar image" /><p><span>nullifi3d</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nullifi3d has no accepted answers">0%</span></p></div></div><div id="comments-container-16653" class="comments-container"></div><div id="comment-tools-16653" class="comment-tools"></div><div class="clear"></div><div id="comment-16653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16658"></span>

<div id="answer-container-16658" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16658-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16658-score" class="post-score" title="current number of votes">0</div><span id="post-16658-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>maybe the specs are not publicly available. I suggest to contact the author of the NOE dissector. You'll find his e-mail address at the beginning of the code.</p><blockquote><p><code>http://anonsvn.wireshark.org/wireshark/trunk/epan/dissectors/packet-noe.c</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Dec '12, 14:16</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-16658" class="comments-container"></div><div id="comment-tools-16658" class="comment-tools"></div><div class="clear"></div><div id="comment-16658-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

