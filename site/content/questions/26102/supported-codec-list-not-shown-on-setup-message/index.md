+++
type = "question"
title = "Supported Codec List not shown on setup message"
description = '''hi,  Normally supported codec list is displayed on the setup message (A interface). But on some phone models (eg : NOKIA E72 )supported codec list is not displayed on the wire shark trace. Can you please explain the reason for this ?'''
date = "2013-10-17T00:10:00Z"
lastmod = "2013-10-17T07:29:00Z"
weight = 26102
keywords = [ "codec" ]
aliases = [ "/questions/26102" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Supported Codec List not shown on setup message](/questions/26102/supported-codec-list-not-shown-on-setup-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26102-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26102-score" class="post-score" title="current number of votes">0</div><span id="post-26102-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, Normally <strong>supported codec list</strong> is displayed on the setup message (A interface). But on some phone models (eg : NOKIA E72 )<strong>supported codec list</strong> is not displayed on the wire shark trace. Can you please explain the reason for this ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-codec" rel="tag" title="see questions tagged &#39;codec&#39;">codec</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '13, 00:10</strong></p><img src="https://secure.gravatar.com/avatar/0657fe7bfd8f4381b67b796327da5406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kotagodahetti&#39;s gravatar image" /><p><span>kotagodahetti</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kotagodahetti has no accepted answers">0%</span></p></div></div><div id="comments-container-26102" class="comments-container"></div><div id="comment-tools-26102" class="comment-tools"></div><div class="clear"></div><div id="comment-26102-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26132"></span>

<div id="answer-container-26132" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26132-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26132-score" class="post-score" title="current number of votes">0</div><span id="post-26132-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If everything is dissected OK in the Wireshark trace I suppose the phone isn't sending it. It might be an optional IE in the message you are looking at.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '13, 07:29</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-26132" class="comments-container"></div><div id="comment-tools-26132" class="comment-tools"></div><div class="clear"></div><div id="comment-26132-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

