+++
type = "question"
title = "Can one start and terminate WinPCap manually under Window 7 64bit OS"
description = '''For some reason, selecting the default Wireshark installation option of starting WinPCap automatically at boot-time, messes up the desktop icons in my Windows 7 64bit machine (the shortcut square in each icon is missing). Another reason for my avoiding automatic startup, is that I use the computer f...'''
date = "2017-03-02T14:36:00Z"
lastmod = "2017-03-02T14:51:00Z"
weight = 59813
keywords = [ "winpcap", "manually" ]
aliases = [ "/questions/59813" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can one start and terminate WinPCap manually under Window 7 64bit OS](/questions/59813/can-one-start-and-terminate-winpcap-manually-under-window-7-64bit-os)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59813-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59813-score" class="post-score" title="current number of votes">0</div><span id="post-59813-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For some reason, selecting the default Wireshark installation option of starting WinPCap automatically at boot-time, messes up the desktop icons in my Windows 7 64bit machine (the shortcut square in each icon is missing). Another reason for my avoiding automatic startup, is that I use the computer for coding and other non-(packet stream analysis) processor intensive activities. Is it possible to choose the setup option of not starting WinPCap at boot time and then being able to start (and terminate) it manually either just before starting (and just after terminating) Wireshark, or concurrently with Wireshark?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-manually" rel="tag" title="see questions tagged &#39;manually&#39;">manually</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Mar '17, 14:36</strong></p><img src="https://secure.gravatar.com/avatar/469e7fba59f37c655afa16ea559bee97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Utnapishtim&#39;s gravatar image" /><p><span>Utnapishtim</span><br />
<span class="score" title="31 reputation points">31</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Utnapishtim has one accepted answer">100%</span> </br></p></div></div><div id="comments-container-59813" class="comments-container"></div><div id="comment-tools-59813" class="comment-tools"></div><div class="clear"></div><div id="comment-59813-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59814"></span>

<div id="answer-container-59814" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59814-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59814-score" class="post-score" title="current number of votes">0</div><span id="post-59814-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Utnapishtim has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have found the answer to this question here: <a href="http://www.lovemytool.com/blog/2009/08/joke_snelders4.html">http://www.lovemytool.com/blog/2009/08/joke_snelders4.html</a></p><p>Just in case that link dies in future: To run Wireshark AND WinPCap simultaneously under Windows 7, right-click on the Wireshark shortcut (on the Desktop or Start menu) and choose Run as Administrator. That will run (and presumably terminate) WinPCap concurrenntly with Wireshark.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Mar '17, 14:51</strong></p><img src="https://secure.gravatar.com/avatar/469e7fba59f37c655afa16ea559bee97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Utnapishtim&#39;s gravatar image" /><p><span>Utnapishtim</span><br />
<span class="score" title="31 reputation points">31</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Utnapishtim has one accepted answer">100%</span> </br></p></div></div><div id="comments-container-59814" class="comments-container"></div><div id="comment-tools-59814" class="comment-tools"></div><div class="clear"></div><div id="comment-59814-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

