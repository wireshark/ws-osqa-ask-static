+++
type = "question"
title = "Tracking Incognito Use"
description = '''I would like to know how to track the incognito browser use of my family using wireshark ]'''
date = "2014-06-14T14:00:00Z"
lastmod = "2014-06-15T20:11:00Z"
weight = 33805
keywords = [ "wireless", "incognito", "wireshark" ]
aliases = [ "/questions/33805" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Tracking Incognito Use](/questions/33805/tracking-incognito-use)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33805-score" class="post-score" title="current number of votes">0</div><span id="post-33805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know how to track the incognito browser use of my family using wireshark ]</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-incognito" rel="tag" title="see questions tagged &#39;incognito&#39;">incognito</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '14, 14:00</strong></p><img src="https://secure.gravatar.com/avatar/e6c607c6aa62572ae069de9e6f4b64c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ashley%20Lynne%20Torgerson&#39;s gravatar image" /><p><span>Ashley Lynne...</span><br />
<span class="score" title="5 reputation points">5</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ashley Lynne Torgerson has no accepted answers">0%</span></p></div></div><div id="comments-container-33805" class="comments-container"><span id="33858"></span><div id="comment-33858" class="comment"><div id="post-33858-score" class="comment-score"></div><div class="comment-text"><p>If the question is "how can I track <em>only</em> the incognito browser use", Kurt's answer is correct, in that there's nothing <em>on the network</em> that's different about incognito use.</p><p>If the question is "how can I track the browser use of my family, even if it's incognito and isn't getting logged in the browser history", Quadratic's answer is correct.</p></div><div id="comment-33858-info" class="comment-info"><span class="comment-age">(15 Jun '14, 20:11)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-33805" class="comment-tools"></div><div class="clear"></div><div id="comment-33805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="33825"></span>

<div id="answer-container-33825" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33825-score" class="post-score" title="current number of votes">1</div><span id="post-33825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>how to track the incognito browser use of my family using wireshark</p></blockquote><p>You can't!</p><p>Reason: traffic in incognito (private) mode looks the same as in 'normal' mode in Wireshark.</p><p>All that incognito mode does is this: it deletes traces of the activities on the PC.</p><blockquote><p><a href="https://support.google.com/chrome/answer/95464?hl=en">https://support.google.com/chrome/answer/95464?hl=en</a></p></blockquote><p>So, if you wan't to know what your family is trying to hide from you, here is what you can do:</p><ul><li><strong>ask them directly and try to discuss the problem</strong></li><li>install spyware on every PC</li><li>install hidden surveillance cameras in every room</li><li>If nothing else works: give them a <a href="http://en.wikipedia.org/wiki/Truth_serum">truth serum</a> and then start some insistent questioning ;-))</li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jun '14, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jun '14, 08:56</strong> </span></p></div></div><div id="comments-container-33825" class="comments-container"><span id="33838"></span><div id="comment-33838" class="comment"><div id="post-33838-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your overly sarcastic answer :)</p></div><div id="comment-33838-info" class="comment-info"><span class="comment-age">(15 Jun '14, 10:25)</span> <span class="comment-user userinfo">Ashley Lynne...</span></div></div><span id="33839"></span><div id="comment-33839" class="comment"><div id="post-33839-score" class="comment-score"></div><div class="comment-text"><p>overly sarcastic answer ?? No way.... ;-))</p><p>All I was trying to say: If your family denies to be cooperative and talk openly about their failures, you need to protect the family by doing the inevitable: show them who's the boss!</p><p>I'm not suggesting you should apply water boarding (although that might teach them some respect), I'm just saying that some soft pressure will help your family to learn the borders of their freedom and to respect the laws within your family, defined by whom? Of course <strong>you</strong> (who else!)</p><p>Good luck with those little stubborn bast&lt;beep&gt;. You should &lt;beep&gt; the cr&lt;beep&gt; out of them. &lt;beep&gt;, &lt;beep&gt;, &lt;beep&gt;</p><p>;-))</p></div><div id="comment-33839-info" class="comment-info"><span class="comment-age">(15 Jun '14, 13:13)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33825" class="comment-tools"></div><div class="clear"></div><div id="comment-33825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="33846"></span>

<div id="answer-container-33846" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33846-score" class="post-score" title="current number of votes">0</div><span id="post-33846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are a few things you can do.</p><p>Assumptions:</p><ol><li>There are multiple devices running in your (home?) network, and you want to track the web usage of all of them.</li><li>Your network consists of end user devices like computers and tablets, along with a 'typical' home router and modem.</li><li>You want to track usage but you don't know exactly when the suspect browser usage is taking place, nor by which device.</li></ol><p>If that's all true, this might actually be a use-case for a program like "Snort", where you set it up to log all the URLs that it sees on the network, with a timestamp and source IP info for the device in the network that browsed there.</p><p>Whether by something like Snort or Wireshark, the critical piece here is that you need a system that is in-between all your users and the Internet, to see all the packets. If your home router supports a feature called "port mirroring", that could be the solution, but that's not a common feature on a home router. Installing a 'hub' between the home router and the modem (if that is your setup) could degrade performance, but that would be another method (where your "spy" workstation could connect along with the router and modem in a three-way network and see all the traffic between them).</p><p>And for a Wireshark method, I'm not sure what technical expertise you have with UNIX or Wireshark but if you're familiar with scripting and the use of scheduled 'cron' jobs, that's another way. A statement like "/usr/bin/dumpcap -a duration:3600 -i {interface name} -f tcp port 80 -w {filename}" would capture all visible HTTP traffic using TCP port 80 for one hour. Write that into a script that runs hourly, and set that filename to something with a timestamp in it, and you've got yourself an hourly packet capture database that will capture all HTTP traffic on port 80 continuously.</p><p>There are a few reasons why I think Snort would be much better for this (detecting and logging traffic patterns is the bread-and-butter of what IDS systems like Snort does), but either way there's very little you're going to be able to do about parsing HTTPS traffic, and in the case of a network hub you wouldn't be getting the traffic before NATing happens so it would be more difficult to tell which device originated the traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jun '14, 14:01</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jun '14, 19:27</strong> </span></p></div></div><div id="comments-container-33846" class="comments-container"></div><div id="comment-tools-33846" class="comment-tools"></div><div class="clear"></div><div id="comment-33846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

