+++
type = "question"
title = "How do I check the synchronization of audo/video RTP streams?"
description = '''I am currently analyzing the RTP packets captured in a video call. I see that there are two RTP streams: one for audio and one for video. Is there a way to check how the synchronization is happening between audio and video? I have looked at the timestamp field in the RTP packet, but it was of no use...'''
date = "2012-04-11T22:06:00Z"
lastmod = "2012-04-11T22:06:00Z"
weight = 10080
keywords = [ "rtp", "analysis" ]
aliases = [ "/questions/10080" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I check the synchronization of audo/video RTP streams?](/questions/10080/how-do-i-check-the-synchronization-of-audovideo-rtp-streams)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10080-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10080-score" class="post-score" title="current number of votes">0</div><span id="post-10080-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I am currently analyzing the RTP packets captured in a video call. I see that there are two RTP streams: one for audio and one for video. Is there a way to check how the synchronization is happening between audio and video?</p><p>I have looked at the timestamp field in the RTP packet, but it was of no use.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '12, 22:06</strong></p><img src="https://secure.gravatar.com/avatar/9d93461073829dbd310f6237a5e1c719?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jack%20Sparrow&#39;s gravatar image" /><p><span>Jack Sparrow</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jack Sparrow has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Apr '12, 11:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-10080" class="comments-container"></div><div id="comment-tools-10080" class="comment-tools"></div><div class="clear"></div><div id="comment-10080-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

