+++
type = "question"
title = "2000 Packets on NIC but only 20 or so on capture."
description = '''Sorry I dont have a screen dump. But there was over 2000 packets on the NIC but wireshark was not detecting it in realtime. I have since upgraded and havent seen this error since. XP SP3 with updates. Intel desktop board. I have being trying to track down the cause/source of my ghost radio station s...'''
date = "2013-08-02T22:19:00Z"
lastmod = "2013-08-08T02:29:00Z"
weight = 23521
keywords = [ "ghost", "pcacket" ]
aliases = [ "/questions/23521" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [2000 Packets on NIC but only 20 or so on capture.](/questions/23521/2000-packets-on-nic-but-only-20-or-so-on-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23521-score" class="post-score" title="current number of votes">-1</div><span id="post-23521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sorry I dont have a screen dump. But there was over 2000 packets on the NIC but wireshark was not detecting it in realtime. I have since upgraded and havent seen this error since.</p><p>XP SP3 with updates. Intel desktop board.</p><p>I have being trying to track down the cause/source of my ghost radio station stream. When I am transmitting a stream of shoutcast music to the net to a public server I seen to get a loop of my various musics mixing back into my stream, I have only tested on internal LAN for some time now.</p><p>Anyway I did a few things and had a 60 second ghost stream loop then removed some programs and got rid of the problem but then installed a certain music player updated of course and now have a 4 min 4 sec music loop on my public radio station when I connect to the web page even if the shoutcast servers are shutdown.</p><p>I have also had an IP conflict on my OpenVPN server with a certain music player installed on a reboot. I have since removed the certain music player IP conflict gone but music loop remains.</p><p>Ask me to explain something if you dont understand my english please.</p><p>I don't really expect an answer.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ghost" rel="tag" title="see questions tagged &#39;ghost&#39;">ghost</span> <span class="post-tag tag-link-pcacket" rel="tag" title="see questions tagged &#39;pcacket&#39;">pcacket</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Aug '13, 22:19</strong></p><img src="https://secure.gravatar.com/avatar/fda026b086bb4cc04d5b0f407b40d3dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mp3streetparty&#39;s gravatar image" /><p><span>Mp3streetparty</span><br />
<span class="score" title="10 reputation points">10</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mp3streetparty has no accepted answers">0%</span></p></div></div><div id="comments-container-23521" class="comments-container"><span id="23630"></span><div id="comment-23630" class="comment"><div id="post-23630-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I have since upgraded and <strong>havent seen this error since</strong>.</p></blockquote><p>O.K. so what is your question then?</p></div><div id="comment-23630-info" class="comment-info"><span class="comment-age">(08 Aug '13, 02:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23521" class="comment-tools"></div><div class="clear"></div><div id="comment-23521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

