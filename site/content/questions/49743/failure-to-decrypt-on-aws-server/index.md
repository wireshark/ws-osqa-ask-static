+++
type = "question"
title = "Failure to decrypt on AWS server"
description = '''Unable to decrypt specific captures using command line tshark on AWS instance. Able to decrypt same capture on local machine. Verified using same version. It is also seemingly random. Happens on the same trace but in a group of seemingly 5 identical traces. Had the same issue on 1.27. Upgraded to se...'''
date = "2016-02-02T14:52:00Z"
lastmod = "2016-02-02T22:42:00Z"
weight = 49743
keywords = [ "decryption", "amazon" ]
aliases = [ "/questions/49743" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Failure to decrypt on AWS server](/questions/49743/failure-to-decrypt-on-aws-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49743-score" class="post-score" title="current number of votes">0</div><span id="post-49743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Unable to decrypt specific captures using command line tshark on AWS instance. Able to decrypt same capture on local machine. Verified using same version. It is also seemingly random. Happens on the same trace but in a group of seemingly 5 identical traces.</p><p>Had the same issue on 1.27. Upgraded to see if it would fix the issue</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-amazon" rel="tag" title="see questions tagged &#39;amazon&#39;">amazon</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '16, 14:52</strong></p><img src="https://secure.gravatar.com/avatar/e52ed6268b16b75639b452318b810190?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adamshickAllion&#39;s gravatar image" /><p><span>adamshickAllion</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adamshickAllion has no accepted answers">0%</span></p></div></div><div id="comments-container-49743" class="comments-container"><span id="49755"></span><div id="comment-49755" class="comment"><div id="post-49755-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question. Please rephrase the text as a proper question.</p></div><div id="comment-49755-info" class="comment-info"><span class="comment-age">(02 Feb '16, 22:42)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-49743" class="comment-tools"></div><div class="clear"></div><div id="comment-49743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

