+++
type = "question"
title = "[closed] coloring rules cannot be edited, deleted or added"
description = '''Hello, in my wireshark on windows 7 (Version 2.2.4 (v2.2.4-0-gcc3dc1b))I can no longer change the coloring rules. If I open the configuration dialog I can add, change and delete rules but after clicking OK there is a message: &quot;wireshark doesn&#x27;t recognize one or more of your coloring rules They have ...'''
date = "2017-01-24T08:46:00Z"
lastmod = "2017-01-25T04:38:00Z"
weight = 59012
keywords = [ "rules", "coloring", "bug" ]
aliases = [ "/questions/59012" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] coloring rules cannot be edited, deleted or added](/questions/59012/coloring-rules-cannot-be-edited-deleted-or-added)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59012-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59012-score" class="post-score" title="current number of votes">0</div><span id="post-59012-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>in my wireshark on windows 7 (Version 2.2.4 (v2.2.4-0-gcc3dc1b))I can no longer change the coloring rules. If I open the configuration dialog I can add, change and delete rules but after clicking OK there is a message: "wireshark doesn't recognize one or more of your coloring rules They have been disabled" Is there any way to remove all coloring rules (e.g. by deleting a file). Reinstalling wireshark doesn't help because coloring rules stay the same.</p><p>Thanks Ben</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rules" rel="tag" title="see questions tagged &#39;rules&#39;">rules</span> <span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '17, 08:46</strong></p><img src="https://secure.gravatar.com/avatar/9b73b276ed414e27020b3fcf1f1af02e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ben_texxxas&#39;s gravatar image" /><p><span>Ben_texxxas</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ben_texxxas has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Jan '17, 09:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-59012" class="comments-container"><span id="59013"></span><div id="comment-59013" class="comment"><div id="post-59013-score" class="comment-score"></div><div class="comment-text"><p>There are a number of previous questions on this subject:</p><ul><li><a href="https://ask.wireshark.org/questions/58300/your-coloring-rules-files-contains-unknown-rules">here</a></li><li><a href="https://ask.wireshark.org/questions/55840/coloring-rules-ok-button-grayed-out-after-update-to-wireshark-220">here</a></li><li><a href="https://ask.wireshark.org/questions/55504/getting-colorfilter-error-after-upgrading-to-220">here</a></li></ul></div><div id="comment-59013-info" class="comment-info"><span class="comment-age">(24 Jan '17, 09:04)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="59042"></span><div id="comment-59042" class="comment"><div id="post-59042-score" class="comment-score"></div><div class="comment-text"><p>All of the previous questions are answered with "just make the rules valid". BUT I am not able to edit my rules. I can not delete them or anything. So I can not make them valid. I am totally stuck on the rules I have configured years ago...</p></div><div id="comment-59042-info" class="comment-info"><span class="comment-age">(25 Jan '17, 03:44)</span> <span class="comment-user userinfo">Ben_texxxas</span></div></div><span id="59044"></span><div id="comment-59044" class="comment"><div id="post-59044-score" class="comment-score"></div><div class="comment-text"><p>The answer, and the comment by <span>@sindy</span> in the third question I linked to contains the info you require. Use a text editor, such as notepad if you have nothing else available, to manually edit the cfilters file in the profile directory.</p></div><div id="comment-59044-info" class="comment-info"><span class="comment-age">(25 Jan '17, 04:38)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-59012" class="comment-tools"></div><div class="clear"></div><div id="comment-59012-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 24 Jan '17, 09:04

</div>

</div>

</div>

