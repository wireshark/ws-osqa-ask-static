+++
type = "question"
title = "Offensive polls on twitter"
description = '''Hello, today I bring forth a question that must be answered. Can I analyze an IP through a twitter DM? I need to know because there is a large cyber bullying issue at my school and I want to fix it.'''
date = "2015-12-11T07:48:00Z"
lastmod = "2015-12-11T08:11:00Z"
weight = 48452
keywords = [ "bullying", "problems" ]
aliases = [ "/questions/48452" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Offensive polls on twitter](/questions/48452/offensive-polls-on-twitter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48452-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48452-score" class="post-score" title="current number of votes">0</div><span id="post-48452-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, today I bring forth a question that must be answered. Can I analyze an IP through a twitter DM? I need to know because there is a large cyber bullying issue at my school and I want to fix it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bullying" rel="tag" title="see questions tagged &#39;bullying&#39;">bullying</span> <span class="post-tag tag-link-problems" rel="tag" title="see questions tagged &#39;problems&#39;">problems</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Dec '15, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/7107d62539ebd24e91b3b5e1204fe774?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Reido&#39;s gravatar image" /><p><span>Reido</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Reido has no accepted answers">0%</span></p></div></div><div id="comments-container-48452" class="comments-container"></div><div id="comment-tools-48452" class="comment-tools"></div><div class="clear"></div><div id="comment-48452-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48454"></span>

<div id="answer-container-48454" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48454-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48454-score" class="post-score" title="current number of votes">1</div><span id="post-48454-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm afraid that the only way to do something about it is to involve Twitter Inc. into the solution.</p><p>From the received message you cannot identify anything but the sender's account.</p><p>Twitter uses https so you'd need to use some privacy attackers' tools to get to the DM content and spy on every message anyone in school is sending via Twitter. And even if you would assume such a Big Brother role, it would work only if you could capture the sending of the message, so if the bad guy is sending it from home or from a cellular network, you cannot capture that communication.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Dec '15, 08:11</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-48454" class="comments-container"></div><div id="comment-tools-48454" class="comment-tools"></div><div class="clear"></div><div id="comment-48454-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

