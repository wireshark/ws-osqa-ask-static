+++
type = "question"
title = "Extracting specific bytes from raw udp data"
description = '''I&#x27;ve a list of udp data and I wanted to extract specific bytes from Data in order to create a new column and then export that info in a CSV file. For example I need to get 4 bytes that belong to 003a - 003b - 003c - 003d in hex, unfortunately the slice operator [] only work by comparison. Then put t...'''
date = "2014-05-13T18:34:00Z"
lastmod = "2014-05-13T18:34:00Z"
weight = 32772
keywords = [ "filter", "operators", "tshark", "wireshark" ]
aliases = [ "/questions/32772" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extracting specific bytes from raw udp data](/questions/32772/extracting-specific-bytes-from-raw-udp-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32772-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32772-score" class="post-score" title="current number of votes">0</div><span id="post-32772-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've a list of udp data and I wanted to extract specific bytes from Data in order to create a new column and then export that info in a CSV file. For example I need to get 4 bytes that belong to 003a - 003b - 003c - 003d in hex, unfortunately the slice operator [] only work by comparison. Then put that value in a column (I already know how to accomplish this). I've been trying with tshark but I can't get it! Is possible to do this withouth building a dissector?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-operators" rel="tag" title="see questions tagged &#39;operators&#39;">operators</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '14, 18:34</strong></p><img src="https://secure.gravatar.com/avatar/9862a57360b433bd5b0b1fbcb47a425d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="progloverfan&#39;s gravatar image" /><p><span>progloverfan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="progloverfan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 May '14, 16:09</strong> </span></p></div></div><div id="comments-container-32772" class="comments-container"></div><div id="comment-tools-32772" class="comment-tools"></div><div class="clear"></div><div id="comment-32772-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

