+++
type = "question"
title = "gtpp prime data record problem"
description = '''Hello, Im trying to dissect contents of gtp prime packet in wireshark 1.12 with following versions: http://tinypic.com/r/2ur4i87/8 but I cant see details in details in data part. Do you have any suggestions what should I do? Or which part of wireshark should I modify? Thanks'''
date = "2014-12-05T04:04:00Z"
lastmod = "2014-12-08T04:02:00Z"
weight = 38357
keywords = [ "gtpp", "cdr" ]
aliases = [ "/questions/38357" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [gtpp prime data record problem](/questions/38357/gtpp-prime-data-record-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38357-score" class="post-score" title="current number of votes">0</div><span id="post-38357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Im trying to dissect contents of gtp prime packet in wireshark 1.12 with following versions: <a href="http://tinypic.com/r/2ur4i87/8">http://tinypic.com/r/2ur4i87/8</a> but I cant see details in details in data part.</p><p>Do you have any suggestions what should I do? Or which part of wireshark should I modify?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtpp" rel="tag" title="see questions tagged &#39;gtpp&#39;">gtpp</span> <span class="post-tag tag-link-cdr" rel="tag" title="see questions tagged &#39;cdr&#39;">cdr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '14, 04:04</strong></p><img src="https://secure.gravatar.com/avatar/4ab8dcc37a2ff129967a4faea0dd3857?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrotter23&#39;s gravatar image" /><p><span>mrotter23</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrotter23 has no accepted answers">0%</span></p></div></div><div id="comments-container-38357" class="comments-container"></div><div id="comment-tools-38357" class="comment-tools"></div><div class="clear"></div><div id="comment-38357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38427"></span>

<div id="answer-container-38427" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38427-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38427-score" class="post-score" title="current number of votes">1</div><span id="post-38427-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mrotter23 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you could open a bug and attach the sample trace it might be possible to extend the dissection to Release 11, currently only 6,8 and 9 are supported.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '14, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-38427" class="comments-container"></div><div id="comment-tools-38427" class="comment-tools"></div><div class="clear"></div><div id="comment-38427-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

