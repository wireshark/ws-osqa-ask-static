+++
type = "question"
title = "wireshark not capturing http requests from the browser"
description = '''I just recently started learning to use wirehsark, I&#x27;ve managed to capture ping requests amd information being sent between sockets but whenever I use a browser, the http requests do not show up on wireshark. I&#x27;ve tried doing the same thing on a different computer on the same wifi network and the ht...'''
date = "2016-09-26T15:29:00Z"
lastmod = "2016-09-26T15:29:00Z"
weight = 55882
keywords = [ "http", "browser" ]
aliases = [ "/questions/55882" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark not capturing http requests from the browser](/questions/55882/wireshark-not-capturing-http-requests-from-the-browser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55882-score" class="post-score" title="current number of votes">0</div><span id="post-55882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just recently started learning to use wirehsark, I've managed to capture ping requests amd information being sent between sockets but whenever I use a browser, the http requests do not show up on wireshark. I've tried doing the same thing on a different computer on the same wifi network and the http requests showed up perfectly. I've tried multiple browsers on my computer and the only interface which captures packets on it is called wifi. any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-browser" rel="tag" title="see questions tagged &#39;browser&#39;">browser</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '16, 15:29</strong></p><img src="https://secure.gravatar.com/avatar/ad989cfc609ef79bb84e3ff83112ec18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="randomscreenname&#39;s gravatar image" /><p><span>randomscreen...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="randomscreenname has no accepted answers">0%</span></p></div></div><div id="comments-container-55882" class="comments-container"></div><div id="comment-tools-55882" class="comment-tools"></div><div class="clear"></div><div id="comment-55882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

