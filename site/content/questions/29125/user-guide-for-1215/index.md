+++
type = "question"
title = "User Guide for 1.2.15"
description = '''Where can I find the user guide (preferably pdf) for version 1.2.15, which is the version installed by yum for CentOS 6.'''
date = "2014-01-23T11:45:00Z"
lastmod = "2014-01-24T07:24:00Z"
weight = 29125
keywords = [ "manual", "guide" ]
aliases = [ "/questions/29125" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [User Guide for 1.2.15](/questions/29125/user-guide-for-1215)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29125-score" class="post-score" title="current number of votes">0</div><span id="post-29125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can I find the user guide (preferably pdf) for version 1.2.15, which is the version installed by yum for CentOS 6.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-manual" rel="tag" title="see questions tagged &#39;manual&#39;">manual</span> <span class="post-tag tag-link-guide" rel="tag" title="see questions tagged &#39;guide&#39;">guide</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '14, 11:45</strong></p><img src="https://secure.gravatar.com/avatar/619e02e8fd8427fc76b06ce7a59ef155?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="khoranyi&#39;s gravatar image" /><p><span>khoranyi</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="khoranyi has no accepted answers">0%</span></p></div></div><div id="comments-container-29125" class="comments-container"></div><div id="comment-tools-29125" class="comment-tools"></div><div class="clear"></div><div id="comment-29125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="29126"></span>

<div id="answer-container-29126" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29126-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29126-score" class="post-score" title="current number of votes">0</div><span id="post-29126-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are the man pages, e.g. <code>man wireshark</code>, the html version of the man page, <code>/usr/share/wireshark/wireshark.html</code> and the text help files in <code>/usr/share/wireshark/help</code>.</p><p>Note that this info was extracted from looking at the contents of the <a href="http://rpm.pbone.net/index.php3/stat/6/idpl/20342464/dir/centos_6/com/wireshark-1.2.15-2.el6_2.1.i686.rpm">wireshark 1.2.15 rpms</a> for Centos 6 found at rpm.pbone.net</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '14, 13:10</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Jan '14, 02:12</strong> </span></p></div></div><div id="comments-container-29126" class="comments-container"></div><div id="comment-tools-29126" class="comment-tools"></div><div class="clear"></div><div id="comment-29126-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="29131"></span>

<div id="answer-container-29131" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29131-score" class="post-score" title="current number of votes">0</div><span id="post-29131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not from the Wireshark Web site. See if there are RPMs for CentOS 6 that include Wireshark documentation, and, if there are, see if any of them include the user manual. If not, you'd have to build it from source yourself.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '14, 15:41</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-29131" class="comments-container"></div><div id="comment-tools-29131" class="comment-tools"></div><div class="clear"></div><div id="comment-29131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="29139"></span>

<div id="answer-container-29139" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29139-score" class="post-score" title="current number of votes">0</div><span id="post-29139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The CentOS RPM packages <strong>don't contain the User Guide</strong> (PDF file). Unfortunately, <strong>you cannot even build</strong> that PDF yourself for 1.2.x, as the <code>docbook</code> directory (contains the files to create the User Guide) is <strong>empty</strong> in the <strong>source code tar packages</strong> up until 1.4.0. I don't know how the User Guide has been created before 1.4.0 !?! Maybe it was just not part of the source code tar package.</p><p>So, here are your options:</p><ul><li><strong>Best solution:</strong>: Checkout the sources from the SVN, instead of using the tar files and build the docs from the code:<br />
1.) svn co <a href="http://anonsvn.wireshark.org/wireshark/trunk-1.2/">http://anonsvn.wireshark.org/wireshark/trunk-1.2/</a> wireshark-1.2<br />
2.) cd wireshark-1.2/docbook<br />
3.) read README.txt<br />
4.) build the User Guide according to the instructions in README.txt<br />
</li><li><strong>2nd best solution:</strong>: google it: <a href="https://www.google.com/?q=+Wireshark+User+Guide+%22for+Wireshark+1.2%22#q=filetype%3Apdf+Wireshark+User+Guide+%22for+Wireshark+1.2%22">https://www.google.com/?q=+Wireshark+User+Guide+%22for+Wireshark+1.2%22#q=filetype%3Apdf+Wireshark+User+Guide+%22for+Wireshark+1.2%22</a> (2nd link)</li><li><strong>3rd best solution:</strong> Download the Windows portable Version of Wireshark 1.2.15. You'll find the User Guide as a <strong>CHM file</strong> in the package (WiresharkPortable\1.2.15\App\Wireshark\user-guide.chm): <a href="http://wireshark.askapache.com/download/win32/all-versions/WiresharkPortable-1.2.15.paf.exe">http://wireshark.askapache.com/download/win32/all-versions/WiresharkPortable-1.2.15.paf.exe</a> . Then use a (online) CHM to PDF converter to get a PDF file, like this one: <a href="http://www.zamzar.com/convert/chm-to-pdf/">http://www.zamzar.com/convert/chm-to-pdf/</a> . It's not perfect, but the best you can get if you need the User Guide for 1.2.x, without building it yourself (see above).</li><li><del>Upgrade your CentOS to 6.4 or later. You will then have Wireshark 1.8.x. There is still no User Guide PDF in the RPM package, but you will be able to build it yourself from source now (the <code>docbook</code> directory contains what you need)</del></li><li><del>Use the User Guide of 1.4.0 (you'll have to build it from source). The difference to 1.2.x should be 'acceptable'</del></li><li><del>Don't use the User Guide at all ;-)</del></li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '14, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Jan '14, 07:27</strong> </span></p></div></div><div id="comments-container-29139" class="comments-container"></div><div id="comment-tools-29139" class="comment-tools"></div><div class="clear"></div><div id="comment-29139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

