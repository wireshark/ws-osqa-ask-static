+++
type = "question"
title = "Distributing Wireshark"
description = '''I have a product that is tied to a new protocol. The release date is a ways away (1-2 years) and development is being done in various places. I intend to contribute everything back to Wireshark, but I was wondering in the meantime what my options are for distributing my version. The development is b...'''
date = "2012-10-22T06:04:00Z"
lastmod = "2012-10-22T06:14:00Z"
weight = 15154
keywords = [ "distributing", "wireshark" ]
aliases = [ "/questions/15154" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Distributing Wireshark](/questions/15154/distributing-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15154-score" class="post-score" title="current number of votes">0</div><span id="post-15154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a product that is tied to a new protocol. The release date is a ways away (1-2 years) and development is being done in various places. I intend to contribute everything back to Wireshark, but I was wondering in the meantime what my options are for distributing my version. The development is being done internally, and also with 3rd parties, which is where I have my concern.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-distributing" rel="tag" title="see questions tagged &#39;distributing&#39;">distributing</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '12, 06:04</strong></p><img src="https://secure.gravatar.com/avatar/1a2b00290d1bc9cd0babb3591ca0a010?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bmwiese&#39;s gravatar image" /><p><span>bmwiese</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bmwiese has no accepted answers">0%</span></p></div></div><div id="comments-container-15154" class="comments-container"></div><div id="comment-tools-15154" class="comment-tools"></div><div class="clear"></div><div id="comment-15154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15155"></span>

<div id="answer-container-15155" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15155-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15155-score" class="post-score" title="current number of votes">0</div><span id="post-15155-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>please check the answers to these questions.</p><blockquote><p><code>http://ask.wireshark.org/questions/10806/could-the-wireshark-analyzer-be-used-in-the-textbook-cd</code><br />
<code>http://ask.wireshark.org/questions/11553/company-usage-confirmation</code><br />
<code>http://ask.wireshark.org/questions/12371/wireshark-plugin-and-gpl-license</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '12, 06:14</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-15155" class="comments-container"></div><div id="comment-tools-15155" class="comment-tools"></div><div class="clear"></div><div id="comment-15155-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

