+++
type = "question"
title = "Windows 7 64 bit MSS issue with Wireshark Ver 1.8.1"
description = '''I have a MSS question with Wireshark Ver 1.8.1. When I check TCP syn packet,I can&#x27;t see MSS value but Malformed Packet. When I install it back to Wiresahrk Ver 1.6.2,I can see the MSS value without problem... The MSS value in my test environment is 1460 bytes. I have enabled and disable large offloa...'''
date = "2012-08-03T01:57:00Z"
lastmod = "2012-08-03T02:40:00Z"
weight = 13341
keywords = [ "mss" ]
aliases = [ "/questions/13341" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 64 bit MSS issue with Wireshark Ver 1.8.1](/questions/13341/windows-7-64-bit-mss-issue-with-wireshark-ver-181)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13341-score" class="post-score" title="current number of votes">0</div><span id="post-13341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a MSS question with Wireshark Ver 1.8.1. When I check TCP syn packet,I can't see MSS value but Malformed Packet. When I install it back to Wiresahrk Ver 1.6.2,I can see the MSS value without problem... The MSS value in my test environment is 1460 bytes. I have enabled and disable large offload send but both not work. The problem I found is from the Wireshark Ver 1.6.3 Could someone help me to find out the root cause? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mss" rel="tag" title="see questions tagged &#39;mss&#39;">mss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '12, 01:57</strong></p><img src="https://secure.gravatar.com/avatar/b7df32a9b84082e0fc10c3e58f800d97?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mp3hunter&#39;s gravatar image" /><p><span>mp3hunter</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mp3hunter has no accepted answers">0%</span></p></div></div><div id="comments-container-13341" class="comments-container"></div><div id="comment-tools-13341" class="comment-tools"></div><div class="clear"></div><div id="comment-13341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13343"></span>

<div id="answer-container-13343" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13343-score" class="post-score" title="current number of votes">0</div><span id="post-13343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If there is a packet that is not correctly dissected in v1.8.1 but it is correctly dissected in v1.6.2, then please file a bug report on <a href="https://bugs.wireshark.org"></a><a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> and attach the capture file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '12, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-13343" class="comments-container"></div><div id="comment-tools-13343" class="comment-tools"></div><div class="clear"></div><div id="comment-13343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

