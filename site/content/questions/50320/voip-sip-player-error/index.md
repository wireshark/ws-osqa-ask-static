+++
type = "question"
title = "[closed] voIP &amp; SIP player Error"
description = '''Telephony -&amp;gt; VoIP calls -&amp;gt; Player  Exception: Exception thrown at 0x00007FF80C69775A (winmmbase.dll) in Wireshark-gtk.exe: 0xC0000005: Access violation writing location 0x0000000029F0EA74. break: PaError PaWinMme_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex hostApiIndex ) ...'''
date = "2016-02-18T19:33:00Z"
lastmod = "2016-02-19T02:36:00Z"
weight = 50320
keywords = [ "player", "voip" ]
aliases = [ "/questions/50320" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] voIP & SIP player Error](/questions/50320/voip-sip-player-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50320-score" class="post-score" title="current number of votes">0</div><span id="post-50320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Telephony -&gt; VoIP calls -&gt; Player</p><p>Exception: Exception thrown at 0x00007FF80C69775A (winmmbase.dll) in Wireshark-gtk.exe: 0xC0000005: Access violation writing location 0x0000000029F0EA74.</p><p>break: PaError PaWinMme_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex hostApiIndex ) { waveOutMessage( (HWAVEOUT)WAVE_MAPPER, DRVM_MAPPER_PREFERRED_GET, (DWORD)&amp;waveOutPreferredDevice, (DWORD)&amp;preferredDeviceStatusFlags );</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '16, 19:33</strong></p><img src="https://secure.gravatar.com/avatar/052d94b4468066f2cdccf6effb8fa925?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="weihp&#39;s gravatar image" /><p><span>weihp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="weihp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Feb '16, 02:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50320" class="comments-container"><span id="50329"></span><div id="comment-50329" class="comment"><div id="post-50329-score" class="comment-score"></div><div class="comment-text"><p>So what question are you asking on this Q&amp;A site? A bug report should go to the <a href="https://bugs/wireshark.org">Wireshark Bugzilla</a>, the OS and Wireshark version should be identified and the capture causing the issue really should be attached.</p></div><div id="comment-50329-info" class="comment-info"><span class="comment-age">(19 Feb '16, 02:36)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50320" class="comment-tools"></div><div class="clear"></div><div id="comment-50320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 19 Feb '16, 02:37

</div>

</div>

</div>

