+++
type = "question"
title = "Wireless Command Line Options"
description = '''I am needing to run Wireshark from a Windows Command Prompt but I don&#x27;t see any options to specify the wireless settings such as the channel or decryption keys. Is this possible?'''
date = "2011-11-11T12:58:00Z"
lastmod = "2011-11-13T03:38:00Z"
weight = 7384
keywords = [ "wireless" ]
aliases = [ "/questions/7384" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless Command Line Options](/questions/7384/wireless-command-line-options)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7384-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7384-score" class="post-score" title="current number of votes">0</div><span id="post-7384-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am needing to run Wireshark from a Windows Command Prompt but I don't see any options to specify the wireless settings such as the channel or decryption keys. Is this possible?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '11, 12:58</strong></p><img src="https://secure.gravatar.com/avatar/01e681d40410ad56b094e86f75afd18e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eagle3089&#39;s gravatar image" /><p><span>eagle3089</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eagle3089 has no accepted answers">0%</span></p></div></div><div id="comments-container-7384" class="comments-container"></div><div id="comment-tools-7384" class="comment-tools"></div><div class="clear"></div><div id="comment-7384-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7401"></span>

<div id="answer-container-7401" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7401-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7401-score" class="post-score" title="current number of votes">0</div><span id="post-7401-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>create a configuration profile and use -C option</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '11, 03:38</strong></p><img src="https://secure.gravatar.com/avatar/35d96b8e73e6deb4e332d076fd3269b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShomeaX&#39;s gravatar image" /><p><span>ShomeaX</span><br />
<span class="score" title="73 reputation points">73</span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShomeaX has no accepted answers">0%</span></p></div></div><div id="comments-container-7401" class="comments-container"></div><div id="comment-tools-7401" class="comment-tools"></div><div class="clear"></div><div id="comment-7401-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

