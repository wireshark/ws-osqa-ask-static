+++
type = "question"
title = "WS stop running at file openning"
description = '''Hello, I just met a problem with WS Version 2.0.5 (v2.0.5-0-ga3be9c6 from master-2.0), on win 7 (6.1 numéro 7601: Service Pack 1). It stopped running (closed the WS window without any message) while I asked to open a capture file. Same problem at hexdump file opening. What hapenned ? Why ? Is there ...'''
date = "2016-08-16T05:54:00Z"
lastmod = "2016-08-16T07:57:00Z"
weight = 54861
keywords = [ "open", "running", "stop", "file" ]
aliases = [ "/questions/54861" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WS stop running at file openning](/questions/54861/ws-stop-running-at-file-openning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54861-score" class="post-score" title="current number of votes">1</div><span id="post-54861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I just met a problem with WS Version 2.0.5 (v2.0.5-0-ga3be9c6 from master-2.0), on win 7 (6.1 numéro 7601: Service Pack 1). It stopped running (closed the WS window without any message) while I asked to open a capture file. Same problem at hexdump file opening. What hapenned ? Why ? Is there any solution ? Best regards,</p><p>H. Rogues</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-open" rel="tag" title="see questions tagged &#39;open&#39;">open</span> <span class="post-tag tag-link-running" rel="tag" title="see questions tagged &#39;running&#39;">running</span> <span class="post-tag tag-link-stop" rel="tag" title="see questions tagged &#39;stop&#39;">stop</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '16, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/17a79f3803b9c8b3c5ae9df918563875?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hrogues&#39;s gravatar image" /><p><span>hrogues</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hrogues has no accepted answers">0%</span></p></div></div><div id="comments-container-54861" class="comments-container"></div><div id="comment-tools-54861" class="comment-tools"></div><div class="clear"></div><div id="comment-54861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54864"></span>

<div id="answer-container-54864" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54864-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54864-score" class="post-score" title="current number of votes">1</div><span id="post-54864-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Possibly <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12036">this</a> bug. Do you have Dell Backup and Recovery installed?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Aug '16, 06:36</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-54864" class="comments-container"><span id="54871"></span><div id="comment-54871" class="comment"><div id="post-54871-score" class="comment-score"></div><div class="comment-text"><p>Thank you graham. No more problem since I uninstalled Dell Recovery and Backup.</p></div><div id="comment-54871-info" class="comment-info"><span class="comment-age">(16 Aug '16, 07:23)</span> <span class="comment-user userinfo">hrogues</span></div></div><span id="54874"></span><div id="comment-54874" class="comment"><div id="post-54874-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-54874-info" class="comment-info"><span class="comment-age">(16 Aug '16, 07:57)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-54864" class="comment-tools"></div><div class="clear"></div><div id="comment-54864-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

