+++
type = "question"
title = "how to capture output"
description = '''Hi all. I&#x27;m very noob and first time open wireshark. I would like to capture an output string from my pc to a remote server. Precisely I would like to grab the command that connect to an rtmp server and capture the option used in the command. Is this possible with wireshark?'''
date = "2016-07-13T03:48:00Z"
lastmod = "2016-07-13T04:00:00Z"
weight = 54027
keywords = [ "rtmp" ]
aliases = [ "/questions/54027" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [how to capture output](/questions/54027/how-to-capture-output)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54027-score" class="post-score" title="current number of votes">0</div><span id="post-54027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all. I'm very noob and first time open wireshark.</p><p>I would like to capture an output string from my pc to a remote server.</p><p>Precisely I would like to grab the command that connect to an rtmp server and capture the option used in the command.</p><p>Is this possible with wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtmp" rel="tag" title="see questions tagged &#39;rtmp&#39;">rtmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '16, 03:48</strong></p><img src="https://secure.gravatar.com/avatar/362c2796414a442c2467344a669b4a5a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="albert56&#39;s gravatar image" /><p><span>albert56</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="albert56 has no accepted answers">0%</span></p></div></div><div id="comments-container-54027" class="comments-container"></div><div id="comment-tools-54027" class="comment-tools"></div><div class="clear"></div><div id="comment-54027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54030"></span>

<div id="answer-container-54030" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54030-score" class="post-score" title="current number of votes">0</div><span id="post-54030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="albert56 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Start reading the <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">Wiki</a> and <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChapterCapture.html">User Documentation</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jul '16, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-54030" class="comments-container"></div><div id="comment-tools-54030" class="comment-tools"></div><div class="clear"></div><div id="comment-54030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

