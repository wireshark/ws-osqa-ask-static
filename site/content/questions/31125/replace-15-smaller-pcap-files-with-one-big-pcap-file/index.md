+++
type = "question"
title = "[closed] replace 15 smaller pcap files with one big pcap file"
description = '''Hi I would like to make a script in linux to do merge or connect 15 smaller files into one big pcap file. I have created the linux script to find the required files and store them in a matrix oldest file in the beginning and newest file at the end of the matrix. Will mergecap command without &quot;-a&quot; op...'''
date = "2014-03-24T10:46:00Z"
lastmod = "2014-03-24T11:32:00Z"
weight = 31125
keywords = [ "mergecap", "wireshark" ]
aliases = [ "/questions/31125" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] replace 15 smaller pcap files with one big pcap file](/questions/31125/replace-15-smaller-pcap-files-with-one-big-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31125-score" class="post-score" title="current number of votes">0</div><span id="post-31125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I would like to make a script in linux to do merge or connect 15 smaller files into one big pcap file. I have created the linux script to find the required files and store them in a matrix oldest file in the beginning and newest file at the end of the matrix. Will mergecap command without "-a" option solve this?? The program that reads the final pcap file should be able to read it and parse it just like if it were the same 15 files.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mergecap" rel="tag" title="see questions tagged &#39;mergecap&#39;">mergecap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '14, 10:46</strong></p><img src="https://secure.gravatar.com/avatar/5bf5e940f9cb50a96c3ee06e808e5eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jichu&#39;s gravatar image" /><p><span>jichu</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jichu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Mar '14, 11:32</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-31125" class="comments-container"><span id="31128"></span><div id="comment-31128" class="comment"><div id="post-31128-score" class="comment-score"></div><div class="comment-text"><p>I closed your new question, as it is exactly the same as the other one (see link in the close message),</p></div><div id="comment-31128-info" class="comment-info"><span class="comment-age">(24 Mar '14, 11:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-31125" class="comment-tools"></div><div class="clear"></div><div id="comment-31125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question: http://ask.wireshark.org/questions/31113/wireshark-merging-pcap-files" by Kurt Knochner 24 Mar '14, 11:32

</div>

</div>

</div>

