+++
type = "question"
title = "Error in reaching Wireshark.org"
description = '''I tried going to the website home several times today and got this message: (Error code: sec_error_ocsp_old_response) '''
date = "2014-11-02T16:28:00Z"
lastmod = "2014-11-02T16:28:00Z"
weight = 37549
keywords = [ "website-error" ]
aliases = [ "/questions/37549" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Error in reaching Wireshark.org](/questions/37549/error-in-reaching-wiresharkorg)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37549-score" class="post-score" title="current number of votes">0</div><span id="post-37549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I tried going to the website home several times today and got this message: (Error code: sec_error_ocsp_old_response)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website-error" rel="tag" title="see questions tagged &#39;website-error&#39;">website-error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '14, 16:28</strong></p><img src="https://secure.gravatar.com/avatar/e33271a8b2d49dd3370cf160b207955b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Leighbe72&#39;s gravatar image" /><p><span>Leighbe72</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Leighbe72 has no accepted answers">0%</span></p></div></div><div id="comments-container-37549" class="comments-container"></div><div id="comment-tools-37549" class="comment-tools"></div><div class="clear"></div><div id="comment-37549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

