+++
type = "question"
title = "filter on tshark"
description = '''hi  i have a folder full of pcap files i want that each pcapfile extract information with filter Tcp.flags.syn==1 &amp;amp;&amp;amp; tcp.flags.ack==1 tcp.flags.reset==1 icmp udp and !icmp and then find no of packets average per second   and store in a file like that eg:  file Rst syn-ack icmp udp! icmp tota...'''
date = "2012-07-02T06:39:00Z"
lastmod = "2012-07-02T06:39:00Z"
weight = 12370
keywords = [ "plshelp" ]
aliases = [ "/questions/12370" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [filter on tshark](/questions/12370/filter-on-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12370-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12370-score" class="post-score" title="current number of votes">0</div><span id="post-12370-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i have a folder full of pcap files i want that each pcapfile extract information with filter Tcp.flags.syn==1 &amp;&amp; tcp.flags.ack==1 tcp.flags.reset==1 icmp udp and !icmp and then find no of packets average per second and store in a file like that eg:</p><pre><code>  file            Rst        syn-ack     icmp            udp! icmp         total</code></pre><ol><li>pcap file 1 1309.08 107.683 19.7167 .14444 1436.62</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plshelp" rel="tag" title="see questions tagged &#39;plshelp&#39;">plshelp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jul '12, 06:39</strong></p><img src="https://secure.gravatar.com/avatar/985e5271f9d7d2cffbb226e3d82e5cc3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="honey&#39;s gravatar image" /><p><span>honey</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="honey has no accepted answers">0%</span></p></div></div><div id="comments-container-12370" class="comments-container"></div><div id="comment-tools-12370" class="comment-tools"></div><div class="clear"></div><div id="comment-12370-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

