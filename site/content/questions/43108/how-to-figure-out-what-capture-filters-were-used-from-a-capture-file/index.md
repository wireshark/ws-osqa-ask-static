+++
type = "question"
title = "How to figure out what capture filters were used from a capture file"
description = '''Hi...How can I find out what filters were used when a capture took place and saved in a file? If you get a capture file from someone, can you find out what capture filters were used?...Thanks'''
date = "2015-06-12T14:37:00Z"
lastmod = "2015-06-12T15:40:00Z"
weight = 43108
keywords = [ "capture-filter" ]
aliases = [ "/questions/43108" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to figure out what capture filters were used from a capture file](/questions/43108/how-to-figure-out-what-capture-filters-were-used-from-a-capture-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43108-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43108-score" class="post-score" title="current number of votes">0</div><span id="post-43108-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi...How can I find out what filters were used when a capture took place and saved in a file? If you get a capture file from someone, can you find out what capture filters were used?...Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '15, 14:37</strong></p><img src="https://secure.gravatar.com/avatar/e6076057904575714a9e76205e6f735b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zerowire&#39;s gravatar image" /><p><span>zerowire</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zerowire has no accepted answers">0%</span></p></div></div><div id="comments-container-43108" class="comments-container"></div><div id="comment-tools-43108" class="comment-tools"></div><div class="clear"></div><div id="comment-43108-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43109"></span>

<div id="answer-container-43109" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43109-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43109-score" class="post-score" title="current number of votes">1</div><span id="post-43109-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, if the file format for the capture was pcapng, and if the capture tool wrote the capture filter string to the file. You can check by looking at the summary statistic. There's a list of capture interfaces, and one column will tell you the capture filter, if any.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jun '15, 15:11</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-43109" class="comments-container"><span id="43111"></span><div id="comment-43111" class="comment"><div id="post-43111-score" class="comment-score"></div><div class="comment-text"><p>Found it. Thank you Jasper so much.</p></div><div id="comment-43111-info" class="comment-info"><span class="comment-age">(12 Jun '15, 15:40)</span> <span class="comment-user userinfo">zerowire</span></div></div></div><div id="comment-tools-43109" class="comment-tools"></div><div class="clear"></div><div id="comment-43109-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

