+++
type = "question"
title = "How to create a pipe interface for ethernet ?"
description = '''I just installed WS 1.10.6 (v1.10.6 from master-1.10) on Ubuntu 14.04 LTS. But in &quot;Capture options&quot; I see only Bluetooth0. When I try to create a pipe in &quot;Interface Management&quot;, it ask me an inferface in the system. I try some ones in &quot;proc/sys/net&quot; and elsewhere but without success. What is the goo...'''
date = "2015-06-08T01:29:00Z"
lastmod = "2015-06-08T01:43:00Z"
weight = 42965
keywords = [ "pipe", "ethernet" ]
aliases = [ "/questions/42965" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to create a pipe interface for ethernet ?](/questions/42965/how-to-create-a-pipe-interface-for-ethernet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42965-score" class="post-score" title="current number of votes">0</div><span id="post-42965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed WS 1.10.6 (v1.10.6 from master-1.10) on Ubuntu 14.04 LTS. But in "Capture options" I see only Bluetooth0.</p><p>When I try to create a pipe in "Interface Management", it ask me an inferface in the system. I try some ones in "proc/sys/net" and elsewhere but without success.</p><p>What is the good one for ethernet eth0 and similar ? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '15, 01:29</strong></p><img src="https://secure.gravatar.com/avatar/2f1f8111e9e6a0d905c02a294fd4315f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rical&#39;s gravatar image" /><p><span>Rical</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rical has no accepted answers">0%</span></p></div></div><div id="comments-container-42965" class="comments-container"></div><div id="comment-tools-42965" class="comment-tools"></div><div class="clear"></div><div id="comment-42965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42966"></span>

<div id="answer-container-42966" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42966-score" class="post-score" title="current number of votes">0</div><span id="post-42966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you made the necessary changes for capture permissions on Ubuntu? See the Wiki page on <a href="https://wiki.wireshark.org/CaptureSetup/CapturePrivileges">Capture Privileges</a> for more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jun '15, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-42966" class="comments-container"></div><div id="comment-tools-42966" class="comment-tools"></div><div class="clear"></div><div id="comment-42966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

