+++
type = "question"
title = "how to connect my GSM phone to wireshark"
description = '''Hi, How can I connect my GSM phone to wireshark, I had installed the modem driver of the phone but still not appearing in interface list, thanks Solarisan'''
date = "2011-05-24T12:00:00Z"
lastmod = "2011-05-25T12:18:00Z"
weight = 4202
keywords = [ "interface", "phone", "handset", "modem", "gsm" ]
aliases = [ "/questions/4202" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to connect my GSM phone to wireshark](/questions/4202/how-to-connect-my-gsm-phone-to-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4202-score" class="post-score" title="current number of votes">0</div><span id="post-4202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, How can I connect my GSM phone to wireshark, I had installed the modem driver of the phone but still not appearing in interface list,</p><p>thanks Solarisan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-phone" rel="tag" title="see questions tagged &#39;phone&#39;">phone</span> <span class="post-tag tag-link-handset" rel="tag" title="see questions tagged &#39;handset&#39;">handset</span> <span class="post-tag tag-link-modem" rel="tag" title="see questions tagged &#39;modem&#39;">modem</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '11, 12:00</strong></p><img src="https://secure.gravatar.com/avatar/7cd42793b9dc6b3ce6985cffbcaa3485?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="solarisan&#39;s gravatar image" /><p><span>solarisan</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="solarisan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jun '11, 22:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-4202" class="comments-container"><span id="4207"></span><div id="comment-4207" class="comment"><div id="post-4207-score" class="comment-score">1</div><div class="comment-text"><p>What operating system are you using, and what version of that operating system?</p></div><div id="comment-4207-info" class="comment-info"><span class="comment-age">(24 May '11, 15:57)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="4233"></span><div id="comment-4233" class="comment"><div id="post-4233-score" class="comment-score"></div><div class="comment-text"><p>I am using Win XP SP3</p></div><div id="comment-4233-info" class="comment-info"><span class="comment-age">(25 May '11, 08:23)</span> <span class="comment-user userinfo">solarisan</span></div></div></div><div id="comment-tools-4202" class="comment-tools"></div><div class="clear"></div><div id="comment-4202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4235"></span>

<div id="answer-container-4235" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4235-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4235-score" class="post-score" title="current number of votes">1</div><span id="post-4235-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Your GSM phone probably appears as a PPP device; support for those in WinPcap is limited. See <a href="http://www.winpcap.org/misc/faq.htm#Q-5">the item about PPP devices in the WinPcap FAQ</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 May '11, 12:18</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-4235" class="comments-container"></div><div id="comment-tools-4235" class="comment-tools"></div><div class="clear"></div><div id="comment-4235-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

