+++
type = "question"
title = "Why did Voip Calls flow diagram lose the colors?"
description = '''Version 2.0.2 presents the SIP flows on a white background. Previous versions had each call color coded. The new version is unusable when analyzing many calls between two devices. Please add the colors back.'''
date = "2016-03-18T09:53:00Z"
lastmod = "2016-03-19T18:06:00Z"
weight = 51039
keywords = [ "voipcalls" ]
aliases = [ "/questions/51039" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why did Voip Calls flow diagram lose the colors?](/questions/51039/why-did-voip-calls-flow-diagram-lose-the-colors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51039-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51039-score" class="post-score" title="current number of votes">0</div><span id="post-51039-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Version 2.0.2 presents the SIP flows on a white background. Previous versions had each call color coded. The new version is unusable when analyzing many calls between two devices. Please add the colors back.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '16, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/c71f7c08c8812f7ddd6a848743539461?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jsonr&#39;s gravatar image" /><p><span>jsonr</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jsonr has no accepted answers">0%</span></p></div></div><div id="comments-container-51039" class="comments-container"></div><div id="comment-tools-51039" class="comment-tools"></div><div class="clear"></div><div id="comment-51039-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51047"></span>

<div id="answer-container-51047" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51047-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51047-score" class="post-score" title="current number of votes">0</div><span id="post-51047-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The colors were probably not implemented in the Qt version, so were lost in the conversion from GTK+ to Qt. Please file a bug on <a href="http://bugs.wireshark.org">the Wireshark Bugzilla</a> for this, so that it can be tracked.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Mar '16, 18:06</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-51047" class="comments-container"></div><div id="comment-tools-51047" class="comment-tools"></div><div class="clear"></div><div id="comment-51047-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

