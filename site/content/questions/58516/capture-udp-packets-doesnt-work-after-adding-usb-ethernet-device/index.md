+++
type = "question"
title = "Capture UDP packets doesn&#x27;t work after adding USB ethernet device"
description = '''I&#x27;m capturing UDP network traffic using a switch with port forwarding connected to a Realtek PCIe GBE Family Controller. I needed a network connection so I added a USB device (ASIX AX88179 USB 3.0 to Gigabit Ehternet Adapter) After installing the driver, Wireshark does not capture UDP packets anymor...'''
date = "2017-01-04T13:10:00Z"
lastmod = "2017-01-04T14:01:00Z"
weight = 58516
keywords = [ "capture" ]
aliases = [ "/questions/58516" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture UDP packets doesn't work after adding USB ethernet device](/questions/58516/capture-udp-packets-doesnt-work-after-adding-usb-ethernet-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58516-score" class="post-score" title="current number of votes">0</div><span id="post-58516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm capturing UDP network traffic using a switch with port forwarding connected to a Realtek PCIe GBE Family Controller.</p><p>I needed a network connection so I added a USB device (ASIX AX88179 USB 3.0 to Gigabit Ehternet Adapter) After installing the driver, Wireshark does not capture UDP packets anymore.</p><p>I'm using Wireshark 64bits 2.2.3 on Windows 7 64bits</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '17, 13:10</strong></p><img src="https://secure.gravatar.com/avatar/81fca3f1c7e413b7f9c4810f5a2fd5e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DoogQc&#39;s gravatar image" /><p><span>DoogQc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DoogQc has one accepted answer">100%</span></p></div></div><div id="comments-container-58516" class="comments-container"></div><div id="comment-tools-58516" class="comment-tools"></div><div class="clear"></div><div id="comment-58516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58517"></span>

<div id="answer-container-58517" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58517-score" class="post-score" title="current number of votes">0</div><span id="post-58517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="DoogQc has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Fixed: Seem to be my antivirus</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '17, 14:01</strong></p><img src="https://secure.gravatar.com/avatar/81fca3f1c7e413b7f9c4810f5a2fd5e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DoogQc&#39;s gravatar image" /><p><span>DoogQc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DoogQc has one accepted answer">100%</span></p></div></div><div id="comments-container-58517" class="comments-container"></div><div id="comment-tools-58517" class="comment-tools"></div><div class="clear"></div><div id="comment-58517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

