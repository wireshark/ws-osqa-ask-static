+++
type = "question"
title = "How to check if there was a successful connection to another computer?"
description = '''Hello, I&#x27;m finishing up a lab for a networking security class, and was asked if in the packet capture included, where any connections made to any other computer. I&#x27;m not exactly sure how to go about that and I would greatly appreciate some help. Thanks!!!'''
date = "2017-05-02T17:54:00Z"
lastmod = "2017-05-02T17:54:00Z"
weight = 61177
keywords = [ "connection" ]
aliases = [ "/questions/61177" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to check if there was a successful connection to another computer?](/questions/61177/how-to-check-if-there-was-a-successful-connection-to-another-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61177-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61177-score" class="post-score" title="current number of votes">0</div><span id="post-61177-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I'm finishing up a lab for a networking security class, and was asked if in the packet capture included, where any connections made to any other computer. I'm not exactly sure how to go about that and I would greatly appreciate some help. Thanks!!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection" rel="tag" title="see questions tagged &#39;connection&#39;">connection</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '17, 17:54</strong></p><img src="https://secure.gravatar.com/avatar/d0a89d2a3e57c354973843cabdd28fcb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TheOnlyBirdy&#39;s gravatar image" /><p><span>TheOnlyBirdy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TheOnlyBirdy has no accepted answers">0%</span></p></div></div><div id="comments-container-61177" class="comments-container"></div><div id="comment-tools-61177" class="comment-tools"></div><div class="clear"></div><div id="comment-61177-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

