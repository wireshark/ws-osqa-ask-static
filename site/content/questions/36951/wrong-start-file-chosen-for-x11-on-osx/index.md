+++
type = "question"
title = "wrong start file chosen for X11 on osx"
description = '''my system version is OS X 10.9.5. The first time i ran wireshark, it asked for X11. it should be XQuarts which i had installed but i can not find in applications. So i just chose safari. And now i can run wireshark by using the terminal in XQuarts. But everytime wireshark runs, safari runs. it&#x27;s kin...'''
date = "2014-10-09T19:06:00Z"
lastmod = "2014-10-09T19:06:00Z"
weight = 36951
keywords = [ "wireshark" ]
aliases = [ "/questions/36951" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wrong start file chosen for X11 on osx](/questions/36951/wrong-start-file-chosen-for-x11-on-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36951-score" class="post-score" title="current number of votes">0</div><span id="post-36951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>my system version is OS X 10.9.5. The first time i ran wireshark, it asked for X11. it should be XQuarts which i had installed but i can not find in applications. So i just chose safari. And now i can run wireshark by using the terminal in XQuarts. But everytime wireshark runs, safari runs. it's kind of annoying. what can i do to disconnect wireshark and safari? all advises are welcomed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '14, 19:06</strong></p><img src="https://secure.gravatar.com/avatar/1ba8cf2fa4b700e86a8166caa16c1d68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Leon&#39;s gravatar image" /><p><span>Leon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Leon has no accepted answers">0%</span></p></div></div><div id="comments-container-36951" class="comments-container"></div><div id="comment-tools-36951" class="comment-tools"></div><div class="clear"></div><div id="comment-36951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

