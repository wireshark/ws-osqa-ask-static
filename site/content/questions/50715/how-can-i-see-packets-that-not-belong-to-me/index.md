+++
type = "question"
title = "how can i see packets that not belong to me?"
description = '''I connect with wifi and i want see on wireshark all the packets, even that not belong to my computer how can i do this? i have windows 7 64  thanks'''
date = "2016-03-04T02:10:00Z"
lastmod = "2016-03-04T03:11:00Z"
weight = 50715
keywords = [ "wireshark" ]
aliases = [ "/questions/50715" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how can i see packets that not belong to me?](/questions/50715/how-can-i-see-packets-that-not-belong-to-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50715-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50715-score" class="post-score" title="current number of votes">0</div><span id="post-50715-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I connect with wifi and i want see on wireshark all the packets, even that not belong to my computer how can i do this?</p><p>i have windows 7 64 thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '16, 02:10</strong></p><img src="https://secure.gravatar.com/avatar/8abfdc858fd344186559d20972a1dee2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abcdefg&#39;s gravatar image" /><p><span>abcdefg</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abcdefg has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Mar '16, 02:30</strong> </span></p></div></div><div id="comments-container-50715" class="comments-container"></div><div id="comment-tools-50715" class="comment-tools"></div><div class="clear"></div><div id="comment-50715-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50717"></span>

<div id="answer-container-50717" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50717-score" class="post-score" title="current number of votes">0</div><span id="post-50717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Start by having a look at the <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">Wireless LAN capture</a> page on the Wiki.</p><p>Note that using Windows with standard wifi NICs and drivers you'll be unlikely to get into monitor mode to capture traffic to other devices.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Mar '16, 03:11</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50717" class="comments-container"></div><div id="comment-tools-50717" class="comment-tools"></div><div class="clear"></div><div id="comment-50717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

