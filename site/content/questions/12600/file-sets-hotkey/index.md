+++
type = "question"
title = "File Sets Hotkey"
description = '''Hi, Is there a hotkey for scrolling trough filesets avoiding to got to File-Filesets evertytime? Thanks'''
date = "2012-07-11T03:29:00Z"
lastmod = "2012-07-11T06:17:00Z"
weight = 12600
keywords = [ "hotkey" ]
aliases = [ "/questions/12600" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [File Sets Hotkey](/questions/12600/file-sets-hotkey)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12600-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12600-score" class="post-score" title="current number of votes">0</div><span id="post-12600-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is there a hotkey for scrolling trough filesets avoiding to got to File-Filesets evertytime?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hotkey" rel="tag" title="see questions tagged &#39;hotkey&#39;">hotkey</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '12, 03:29</strong></p><img src="https://secure.gravatar.com/avatar/b307aedf12ca75ee1da5ec114fa2028e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ivan&#39;s gravatar image" /><p><span>Ivan</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ivan has no accepted answers">0%</span></p></div></div><div id="comments-container-12600" class="comments-container"></div><div id="comment-tools-12600" class="comment-tools"></div><div class="clear"></div><div id="comment-12600-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12629"></span>

<div id="answer-container-12629" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12629-score" class="post-score" title="current number of votes">0</div><span id="post-12629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no such hotkey. Please file an enhancement request at <a href="http://bugs.wireshark.org">bugs.wireshark.org</a>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '12, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-12629" class="comments-container"></div><div id="comment-tools-12629" class="comment-tools"></div><div class="clear"></div><div id="comment-12629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

