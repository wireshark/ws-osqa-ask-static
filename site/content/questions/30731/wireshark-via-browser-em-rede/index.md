+++
type = "question"
title = "Wireshark via Browser em rede"
description = '''Good day, Please I use wireshark on Linux CentOS 6 and I need to use the tool via browser wireshark on my network is possible?  att, Janduy Euclides'''
date = "2014-03-12T07:44:00Z"
lastmod = "2014-03-12T11:03:00Z"
weight = 30731
keywords = [ "wireshark" ]
aliases = [ "/questions/30731" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark via Browser em rede](/questions/30731/wireshark-via-browser-em-rede)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30731-score" class="post-score" title="current number of votes">0</div><span id="post-30731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good day, Please I use wireshark on Linux CentOS 6 and I need to use the tool via browser wireshark on my network is possible?</p><p>att, Janduy Euclides</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '14, 07:44</strong></p><img src="https://secure.gravatar.com/avatar/17f457b9df5f021ff91af3972efa35f8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Janduy&#39;s gravatar image" /><p><span>Janduy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Janduy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Mar '14, 07:50</strong> </span></p></div></div><div id="comments-container-30731" class="comments-container"><span id="30742"></span><div id="comment-30742" class="comment"><div id="post-30742-score" class="comment-score"></div><div class="comment-text"><p>any plausible reason why you need that?</p></div><div id="comment-30742-info" class="comment-info"><span class="comment-age">(12 Mar '14, 11:03)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30731" class="comment-tools"></div><div class="clear"></div><div id="comment-30731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30739"></span>

<div id="answer-container-30739" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30739-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30739-score" class="post-score" title="current number of votes">0</div><span id="post-30739-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, wireshark is a standalone application.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '14, 10:25</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-30739" class="comments-container"><span id="30740"></span><div id="comment-30740" class="comment"><div id="post-30740-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I have the need to do this know any alternatives?</p></div><div id="comment-30740-info" class="comment-info"><span class="comment-age">(12 Mar '14, 10:27)</span> <span class="comment-user userinfo">Janduy</span></div></div><span id="30741"></span><div id="comment-30741" class="comment"><div id="post-30741-score" class="comment-score"></div><div class="comment-text"><p><a href="http://www.cloudshark.org">Cloudshark</a>?</p></div><div id="comment-30741-info" class="comment-info"><span class="comment-age">(12 Mar '14, 10:48)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-30739" class="comment-tools"></div><div class="clear"></div><div id="comment-30739-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

