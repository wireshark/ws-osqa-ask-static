+++
type = "question"
title = "Disable Warning message in tshark"
description = '''How do I disable the warning message in tshark? I&#x27;m running tshark from a python script and I get the following messages: (tshark.exe:1144): WARNING **: Dissector bug, protocol MP2T, in packet ####: tv_buffcomposite.c:235: failed assertion &quot;member-&amp;gt;length&quot; tshark spits these errors out and never ...'''
date = "2014-12-22T08:27:00Z"
lastmod = "2014-12-22T10:04:00Z"
weight = 38660
keywords = [ "tshark", "mp2t" ]
aliases = [ "/questions/38660" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Disable Warning message in tshark](/questions/38660/disable-warning-message-in-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38660-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38660-score" class="post-score" title="current number of votes">0</div><span id="post-38660-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I disable the warning message in tshark?</p><p>I'm running tshark from a python script and I get the following messages: (tshark.exe:1144): WARNING **: Dissector bug, protocol MP2T, in packet ####: tv_buffcomposite.c:235: failed assertion "member-&gt;length"</p><p>tshark spits these errors out and never seems to finish.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-mp2t" rel="tag" title="see questions tagged &#39;mp2t&#39;">mp2t</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Dec '14, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/fb0162650b58fb32e58ed171922829b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mark%20Lambie&#39;s gravatar image" /><p><span>Mark Lambie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mark Lambie has no accepted answers">0%</span></p></div></div><div id="comments-container-38660" class="comments-container"><span id="38662"></span><div id="comment-38662" class="comment"><div id="post-38662-score" class="comment-score"></div><div class="comment-text"><p>What version of tshark?</p></div><div id="comment-38662-info" class="comment-info"><span class="comment-age">(22 Dec '14, 10:04)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-38660" class="comment-tools"></div><div class="clear"></div><div id="comment-38660-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

