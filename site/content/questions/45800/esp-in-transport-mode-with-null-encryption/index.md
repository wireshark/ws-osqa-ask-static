+++
type = "question"
title = "ESP in transport mode with null encryption"
description = '''Is it possible to correctly display the transport layer protocol, such as tcp/udp? and all those operations for tcp/udp still works.'''
date = "2015-09-11T16:49:00Z"
lastmod = "2015-09-12T12:56:00Z"
weight = 45800
keywords = [ "esp" ]
aliases = [ "/questions/45800" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ESP in transport mode with null encryption](/questions/45800/esp-in-transport-mode-with-null-encryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45800-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45800-score" class="post-score" title="current number of votes">0</div><span id="post-45800-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to correctly display the transport layer protocol, such as tcp/udp? and all those operations for tcp/udp still works.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esp" rel="tag" title="see questions tagged &#39;esp&#39;">esp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '15, 16:49</strong></p><img src="https://secure.gravatar.com/avatar/7acfbffb4d99881acb8a75f001849574?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xwz7611&#39;s gravatar image" /><p><span>xwz7611</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xwz7611 has no accepted answers">0%</span></p></div></div><div id="comments-container-45800" class="comments-container"></div><div id="comment-tools-45800" class="comment-tools"></div><div class="clear"></div><div id="comment-45800-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45817"></span>

<div id="answer-container-45817" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45817-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45817-score" class="post-score" title="current number of votes">0</div><span id="post-45817-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>According to the Wiki, yes. I have never tried it...</p><p>Please read "Attempt to detect/decode NULL encrypted ESP payloads" in the Wiki and try the ESP Option "Attempt to detect/decode NULL encrypted ESP payloads"</p><blockquote><p><a href="https://wiki.wireshark.org/ESP_Preferences">https://wiki.wireshark.org/ESP_Preferences</a></p></blockquote><p><strong>++ UPDATE ++</strong></p><p>It works. I've just tested it with a pcap (ESP null encryption) that is posted here:</p><blockquote><p><a href="https://supportforums.cisco.com/discussion/11175751/ipsec-vpn-between-two-routers-esp-transport-mode-and-tunnel-mode">https://supportforums.cisco.com/discussion/11175751/ipsec-vpn-between-two-routers-esp-transport-mode-and-tunnel-mode</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Sep '15, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Sep '15, 12:59</strong> </span></p></div></div><div id="comments-container-45817" class="comments-container"></div><div id="comment-tools-45817" class="comment-tools"></div><div class="clear"></div><div id="comment-45817-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

