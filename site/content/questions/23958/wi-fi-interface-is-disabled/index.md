+++
type = "question"
title = "Wi-Fi Interface is &quot;disabled&quot;"
description = '''I can&#x27;t run a packet capture on my Wi-Fi interface because it says its disabled. How do I enable it?'''
date = "2013-08-22T06:23:00Z"
lastmod = "2013-08-24T14:51:00Z"
weight = 23958
keywords = [ "packet-capture" ]
aliases = [ "/questions/23958" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wi-Fi Interface is "disabled"](/questions/23958/wi-fi-interface-is-disabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23958-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23958-score" class="post-score" title="current number of votes">0</div><span id="post-23958-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't run a packet capture on my Wi-Fi interface because it says its disabled. How do I enable it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '13, 06:23</strong></p><img src="https://secure.gravatar.com/avatar/df0eb5fd48600ec9d0edf449c2a3bb07?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="michaelthb&#39;s gravatar image" /><p><span>michaelthb</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="michaelthb has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Aug '13, 06:23</strong> </span></p></div></div><div id="comments-container-23958" class="comments-container"><span id="23960"></span><div id="comment-23960" class="comment"><div id="post-23960-score" class="comment-score"></div><div class="comment-text"><p>Which OS are you using? If you saerch this site the question may have been asked before.</p></div><div id="comment-23960-info" class="comment-info"><span class="comment-age">(22 Aug '13, 06:59)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="24013"></span><div id="comment-24013" class="comment"><div id="post-24013-score" class="comment-score"></div><div class="comment-text"><p>Where does it say that the interface is "disabled"?</p><p>In the Capture Options dialog (Capture -&gt; Options menu item), there are columns that can say "disabled", but that refers not to the interface but to promiscuous or monitor mode, and they reflect what the current or default capture settings are in Wireshark, not a persistent state for the interface, and you change them by double-clicking the line for the interface and setting the checkbox for the mode in question.</p></div><div id="comment-24013-info" class="comment-info"><span class="comment-age">(24 Aug '13, 14:51)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-23958" class="comment-tools"></div><div class="clear"></div><div id="comment-23958-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

