+++
type = "question"
title = "When will 11ac sniffer be available?"
description = '''When will 11ac sniffer be available?'''
date = "2012-08-28T16:36:00Z"
lastmod = "2012-08-28T16:36:00Z"
weight = 13947
keywords = [ "11ac" ]
aliases = [ "/questions/13947" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [When will 11ac sniffer be available?](/questions/13947/when-will-11ac-sniffer-be-available)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13947-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13947-score" class="post-score" title="current number of votes">0</div><span id="post-13947-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When will 11ac sniffer be available?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-11ac" rel="tag" title="see questions tagged &#39;11ac&#39;">11ac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '12, 16:36</strong></p><img src="https://secure.gravatar.com/avatar/ead0dba268fbb0136e174ca464601f78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AAM&#39;s gravatar image" /><p><span>AAM</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AAM has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Sep '12, 08:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-13947" class="comments-container"></div><div id="comment-tools-13947" class="comment-tools"></div><div class="clear"></div><div id="comment-13947-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

