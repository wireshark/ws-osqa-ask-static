+++
type = "question"
title = "Usbcap bug, Asking 4 help"
description = '''Im new to wireshark, Trying to learn how it works and etc. I deleted somethings in wireshark so i decided to uninstall and reinstall it back to get the defaults back in order. As i was reinstalling wireshark usbcap had and error downloading i chose to ignore it.I rebooted my desktop and i noticed th...'''
date = "2017-07-05T20:15:00Z"
lastmod = "2017-07-06T00:49:00Z"
weight = 62550
keywords = [ "usbcap", "help", "mouseandkeyboard." ]
aliases = [ "/questions/62550" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Usbcap bug, Asking 4 help](/questions/62550/usbcap-bug-asking-4-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62550-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62550-score" class="post-score" title="current number of votes">0</div><span id="post-62550-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im new to wireshark, Trying to learn how it works and etc. I deleted somethings in wireshark so i decided to uninstall and reinstall it back to get the defaults back in order. As i was reinstalling wireshark usbcap had and error downloading i chose to ignore it.I rebooted my desktop and i noticed that my keyboard and mouse no longer work.Im on windows 10 and i cannot find a fix.I also have 1 device which is my phone so i could could not file share to remove usbcap</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usbcap" rel="tag" title="see questions tagged &#39;usbcap&#39;">usbcap</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-mouseandkeyboard." rel="tag" title="see questions tagged &#39;mouseandkeyboard.&#39;">mouseandkeyboard.</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jul '17, 20:15</strong></p><img src="https://secure.gravatar.com/avatar/2232f4f90ca6fe2cef35871bce77cfdc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dogeus115&#39;s gravatar image" /><p><span>Dogeus115</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dogeus115 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Jul '17, 20:18</strong> </span></p></div></div><div id="comments-container-62550" class="comments-container"></div><div id="comment-tools-62550" class="comment-tools"></div><div class="clear"></div><div id="comment-62550-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62554"></span>

<div id="answer-container-62554" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62554-score" class="post-score" title="current number of votes">0</div><span id="post-62554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you seen the answer to <a href="https://ask.wireshark.org/questions/50248/usb-ports-not-functioning">this question</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jul '17, 00:49</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-62554" class="comments-container"></div><div id="comment-tools-62554" class="comment-tools"></div><div class="clear"></div><div id="comment-62554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

