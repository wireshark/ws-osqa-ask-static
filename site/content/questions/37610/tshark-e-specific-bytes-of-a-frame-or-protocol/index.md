+++
type = "question"
title = "Tshark -e specific bytes of a frame or protocol"
description = '''Hi, The gsm_a.rp dissector does not implement filter on rp_cause (22,&quot;Memory capacity exceeded&quot; eg) which I beleive the most important element of this protocol in case of an RP-ERROR. In wshark can filter by frame[x:y] so would it be possible to use tshark -e frame[x:y] in any way? Or alternatively ...'''
date = "2014-11-06T02:32:00Z"
lastmod = "2014-11-06T02:32:00Z"
weight = 37610
keywords = [ "frame", "bytes", "tshark", "rp_cause" ]
aliases = [ "/questions/37610" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Tshark -e specific bytes of a frame or protocol](/questions/37610/tshark-e-specific-bytes-of-a-frame-or-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37610-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37610-score" class="post-score" title="current number of votes">0</div><span id="post-37610-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>The gsm_a.rp dissector does not implement filter on rp_cause (22,"Memory capacity exceeded" eg) which I beleive the most important element of this protocol in case of an RP-ERROR. In wshark can filter by frame[x:y] so would it be possible to use tshark -e frame[x:y] in any way? Or alternatively is it possible to modify the gsm_a.rp dissector to include rp_cause?</p><p>Thanks, PeterK</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-bytes" rel="tag" title="see questions tagged &#39;bytes&#39;">bytes</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-rp_cause" rel="tag" title="see questions tagged &#39;rp_cause&#39;">rp_cause</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '14, 02:32</strong></p><img src="https://secure.gravatar.com/avatar/d7a0b0243086b78ddd5ff6626e729976?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PeterK&#39;s gravatar image" /><p><span>PeterK</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PeterK has no accepted answers">0%</span></p></div></div><div id="comments-container-37610" class="comments-container"></div><div id="comment-tools-37610" class="comment-tools"></div><div class="clear"></div><div id="comment-37610-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

