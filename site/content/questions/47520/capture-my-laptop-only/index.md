+++
type = "question"
title = "Capture my laptop only"
description = '''I would like to know if there is a way that I could capture only my laptop thats connected to the WIFI. Thank you'''
date = "2015-11-11T09:58:00Z"
lastmod = "2015-11-13T08:20:00Z"
weight = 47520
keywords = [ "capture", "laptop", "my", "help" ]
aliases = [ "/questions/47520" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Capture my laptop only](/questions/47520/capture-my-laptop-only)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47520-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47520-score" class="post-score" title="current number of votes">1</div><span id="post-47520-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if there is a way that I could capture only my laptop thats connected to the WIFI.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-laptop" rel="tag" title="see questions tagged &#39;laptop&#39;">laptop</span> <span class="post-tag tag-link-my" rel="tag" title="see questions tagged &#39;my&#39;">my</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Nov '15, 09:58</strong></p><img src="https://secure.gravatar.com/avatar/330ebe0e35013e8479790466d416ff90?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BigBoost&#39;s gravatar image" /><p><span>BigBoost</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BigBoost has no accepted answers">0%</span></p></div></div><div id="comments-container-47520" class="comments-container"></div><div id="comment-tools-47520" class="comment-tools"></div><div class="clear"></div><div id="comment-47520-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="47521"></span>

<div id="answer-container-47521" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47521-score" class="post-score" title="current number of votes">2</div><span id="post-47521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, there is, untick "use promiscuous mode on all interfaces" in the Capture options window, then double-click the wireless interface in the list to open its "Edit interface settings" window and verify that the "Capture packets in promiscuous mode" is also unticked there (should be by default).</p><p>This way, you'll capture only the packets your laptop is sending, packets which others send solely to it, and broadcast/multicast packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '15, 10:23</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-47521" class="comments-container"><span id="47522"></span><div id="comment-47522" class="comment"><div id="post-47522-score" class="comment-score"></div><div class="comment-text"><p>Thank you!</p></div><div id="comment-47522-info" class="comment-info"><span class="comment-age">(11 Nov '15, 10:31)</span> <span class="comment-user userinfo">BigBoost</span></div></div><span id="47527"></span><div id="comment-47527" class="comment"><div id="post-47527-score" class="comment-score"></div><div class="comment-text"><p>But we may get Broad cast traffic from other terminals also ... AM i right????</p></div><div id="comment-47527-info" class="comment-info"><span class="comment-age">(11 Nov '15, 19:29)</span> <span class="comment-user userinfo">srinu_bel</span></div></div><span id="47530"></span><div id="comment-47530" class="comment"><div id="post-47530-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@srinu_bel</span>, please convert what you wrote into a comment, either to BigBoost's question or to my answer.</p><p><span></span><span>@BigBoost</span>, thank you for voting my answer up, but could you please also accept it? It makes a difference, when you look to a list of questions, those with accepted answers have nicer colour ;-) Yes, I agree that if the questioner upvotes an answer, it should automatically mark it also as "accepted", but it is not the case yet.</p><p><strong>To the subject:</strong> yes, of course, the broadcast traffic will be captured, as I wrote in my answer. You can use capture filter to further limit what will be captured, and display filter to further limit what will be shown to you, but using them requires some knowledge of protocols and various types of messages and their purpose. For example, an arp request (who has IP x? Tell IP y) must be a broadcast one by principle, but it is definitely related to "your laptop" if your laptop has IP x. So if you filter out broadcast traffic completely, your picture of communication related to "your laptop" is not complete.</p><p>BigBoost's question suggests that he is not a power user yet, so I've adjusted my answer to that. Any kind of more detailed limitation (like "use capture filter <code>host 192.168.1.7</code> where 192.168.1.7 is the IPv4 address of your laptop") could make his capture lack something he was really after.</p><p>Pavel</p></div><div id="comment-47530-info" class="comment-info"><span class="comment-age">(11 Nov '15, 22:10)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="47531"></span><div id="comment-47531" class="comment"><div id="post-47531-score" class="comment-score"></div><div class="comment-text"><p>Sorry... i am not frequent visitor to this portal... I kept query and looking for reply for it &amp; just seen this.... Thank u &amp; i will update myself...</p><p>Regards</p></div><div id="comment-47531-info" class="comment-info"><span class="comment-age">(11 Nov '15, 22:40)</span> <span class="comment-user userinfo">srinu_bel</span></div></div></div><div id="comment-tools-47521" class="comment-tools"></div><div class="clear"></div><div id="comment-47521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="47578"></span>

<div id="answer-container-47578" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47578-score" class="post-score" title="current number of votes">0</div><span id="post-47578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Let's be careful of the definition of "Broadcast". If you follow the directions provided by <span>@sindy</span>, you will be able to see Ethernet Broadcasts. However, you will not be able to view any WiFi management or control frames. So if your problem is on the WiFi connection, then allowing a capture in non-promiscuous mode may not be the best solution.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '15, 08:20</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-47578" class="comments-container"></div><div id="comment-tools-47578" class="comment-tools"></div><div class="clear"></div><div id="comment-47578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

