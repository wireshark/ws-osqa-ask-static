+++
type = "question"
title = "can&#x27;t see &quot;Save Capture File As&quot; dialog box in wireshark"
description = '''Hi,  I have installed 64 bit 1.10, i see &quot;Save Capture File As&quot; dialog box in wireshark, but without the possibility to display packets etc, Does someone have any idea? Yariv'''
date = "2013-07-11T02:19:00Z"
lastmod = "2013-07-11T02:24:00Z"
weight = 22838
keywords = [ "filesave" ]
aliases = [ "/questions/22838" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can't see "Save Capture File As" dialog box in wireshark](/questions/22838/cant-see-save-capture-file-as-dialog-box-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22838-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22838-score" class="post-score" title="current number of votes">0</div><span id="post-22838-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have installed 64 bit 1.10, i see "Save Capture File As" dialog box in wireshark, but without the possibility to display packets etc, Does someone have any idea? Yariv</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filesave" rel="tag" title="see questions tagged &#39;filesave&#39;">filesave</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '13, 02:19</strong></p><img src="https://secure.gravatar.com/avatar/18c8c9d674f39d643fdbe15175362d73?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ygolan99&#39;s gravatar image" /><p><span>ygolan99</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ygolan99 has no accepted answers">0%</span></p></div></div><div id="comments-container-22838" class="comments-container"></div><div id="comment-tools-22838" class="comment-tools"></div><div class="clear"></div><div id="comment-22838-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22840"></span>

<div id="answer-container-22840" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22840-score" class="post-score" title="current number of votes">2</div><span id="post-22840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Saving displayed packets can now be done with "File -&gt; Export Specified Packets...".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '13, 02:24</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-22840" class="comments-container"></div><div id="comment-tools-22840" class="comment-tools"></div><div class="clear"></div><div id="comment-22840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

