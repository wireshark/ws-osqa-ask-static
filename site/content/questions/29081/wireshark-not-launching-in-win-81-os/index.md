+++
type = "question"
title = "Wireshark not launching in Win 8.1 OS"
description = '''Wireshark (Wireshark-win64-1.10.1) &amp;amp; Wincap 4.1.3 installed succesfully, but not getting launched. Getting hanged while loading configuration files for wireshark at 100%.'''
date = "2014-01-22T00:30:00Z"
lastmod = "2014-01-22T02:40:00Z"
weight = 29081
keywords = [ "wireshark" ]
aliases = [ "/questions/29081" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not launching in Win 8.1 OS](/questions/29081/wireshark-not-launching-in-win-81-os)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29081-score" class="post-score" title="current number of votes">0</div><span id="post-29081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark (Wireshark-win64-1.10.1) &amp; Wincap 4.1.3 installed succesfully, but not getting launched. Getting hanged while loading configuration files for wireshark at 100%.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '14, 00:30</strong></p><img src="https://secure.gravatar.com/avatar/3150aff7392d7002c7010182b4e08712?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DIGSI&#39;s gravatar image" /><p><span>DIGSI</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DIGSI has no accepted answers">0%</span></p></div></div><div id="comments-container-29081" class="comments-container"><span id="29088"></span><div id="comment-29088" class="comment"><div id="post-29088-score" class="comment-score"></div><div class="comment-text"><p>AV, VPN software?</p></div><div id="comment-29088-info" class="comment-info"><span class="comment-age">(22 Jan '14, 02:40)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-29081" class="comment-tools"></div><div class="clear"></div><div id="comment-29081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29083"></span>

<div id="answer-container-29083" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29083-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29083-score" class="post-score" title="current number of votes">0</div><span id="post-29083-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That seems to be a yet unresolved problem.</p><p>See here: <a href="http://ask.wireshark.org/questions/26361/loading-configuration-files">http://ask.wireshark.org/questions/26361/loading-configuration-files</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '14, 01:17</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-29083" class="comments-container"></div><div id="comment-tools-29083" class="comment-tools"></div><div class="clear"></div><div id="comment-29083-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

