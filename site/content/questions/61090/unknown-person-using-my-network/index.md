+++
type = "question"
title = "unknown person using my network"
description = '''I log into my netgear router to see if there are any intruders and at the same time I run wireshark. when I checked my router log there were 3 devices logged in and they belong to me. the 3rd device has an IP address ending with ..**.04, but when I see wireshark capture I see another device with .05...'''
date = "2017-04-28T01:45:00Z"
lastmod = "2017-04-28T04:10:00Z"
weight = 61090
keywords = [ "intruders" ]
aliases = [ "/questions/61090" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [unknown person using my network](/questions/61090/unknown-person-using-my-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61090-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61090-score" class="post-score" title="current number of votes">0</div><span id="post-61090-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I log into my netgear router to see if there are any intruders and at the same time I run wireshark. when I checked my router log there were 3 devices logged in and they belong to me. the 3rd device has an IP address ending with <strong>.</strong>.**.04, but when I see wireshark capture I see another device with .05 ending IP I have attached the screen shot pls help. <img src="https://osqa-ask.wireshark.org/upfiles/c3_9Y1I67f.PNG" alt="alt text" /> I want know to which device is using ip 10.99.1.5.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-intruders" rel="tag" title="see questions tagged &#39;intruders&#39;">intruders</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '17, 01:45</strong></p><img src="https://secure.gravatar.com/avatar/4debf4d644c7320e639547bd1b13c1b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="w_keyboard&#39;s gravatar image" /><p><span>w_keyboard</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="w_keyboard has no accepted answers">0%</span></p></img></div></div><div id="comments-container-61090" class="comments-container"></div><div id="comment-tools-61090" class="comment-tools"></div><div class="clear"></div><div id="comment-61090-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61093"></span>

<div id="answer-container-61093" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61093-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61093-score" class="post-score" title="current number of votes">0</div><span id="post-61093-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Either look into your Netgear router logs, or look at this frame details for the MAC address OUI which may give away the manufacturer of the network interface. This may get you started on your search.</p><p>PS: Never attach screen shots! Provide a link to a capture file instead.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '17, 02:22</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-61093" class="comments-container"><span id="61096"></span><div id="comment-61096" class="comment"><div id="post-61096-score" class="comment-score"></div><div class="comment-text"><p>Is it safe to attach the capture file here in the forums ? and how to attach it ?</p></div><div id="comment-61096-info" class="comment-info"><span class="comment-age">(28 Apr '17, 04:02)</span> <span class="comment-user userinfo">w_keyboard</span></div></div><span id="61097"></span><div id="comment-61097" class="comment"><div id="post-61097-score" class="comment-score"></div><div class="comment-text"><p>The capture file will be as accessible as this Q&amp;A, so unlimited.</p></div><div id="comment-61097-info" class="comment-info"><span class="comment-age">(28 Apr '17, 04:04)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61099"></span><div id="comment-61099" class="comment"><div id="post-61099-score" class="comment-score"></div><div class="comment-text"><p>Captures should be shared in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>, Google Drive, Dropbox, etc.</p></div><div id="comment-61099-info" class="comment-info"><span class="comment-age">(28 Apr '17, 04:10)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61093" class="comment-tools"></div><div class="clear"></div><div id="comment-61093-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

