+++
type = "question"
title = "laptop&#x27;s NICs (wired) that support promiscuous mode natively"
description = '''I need to purchase a laptop that&#x27;s NIC supports promiscuous mode. I can&#x27;t find a list of wired NICS. There are plenty of lists that show wireless. Any suggestions on the right laptop or NIC manafactuers? Thanks Adam'''
date = "2013-03-05T18:24:00Z"
lastmod = "2013-03-05T23:20:00Z"
weight = 19195
keywords = [ "laptop", "promiscuous-mode" ]
aliases = [ "/questions/19195" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [laptop's NICs (wired) that support promiscuous mode natively](/questions/19195/laptops-nics-wired-that-support-promiscuous-mode-natively)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19195-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19195-score" class="post-score" title="current number of votes">0</div><span id="post-19195-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to purchase a laptop that's NIC supports promiscuous mode. I can't find a list of wired NICS. There are plenty of lists that show wireless. Any suggestions on the right laptop or NIC manafactuers?</p><p>Thanks</p><p>Adam</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-laptop" rel="tag" title="see questions tagged &#39;laptop&#39;">laptop</span> <span class="post-tag tag-link-promiscuous-mode" rel="tag" title="see questions tagged &#39;promiscuous-mode&#39;">promiscuous-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '13, 18:24</strong></p><img src="https://secure.gravatar.com/avatar/43a4cbf9b3a9f58a3f073b49df133de5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xenon&#39;s gravatar image" /><p><span>xenon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xenon has no accepted answers">0%</span></p></div></div><div id="comments-container-19195" class="comments-container"></div><div id="comment-tools-19195" class="comment-tools"></div><div class="clear"></div><div id="comment-19195-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19203"></span>

<div id="answer-container-19203" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19203-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19203-score" class="post-score" title="current number of votes">1</div><span id="post-19203-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Every ethernet (wired) NIC supports promiscuous mode, at least I never had any problems with ethernet NICs.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '13, 23:20</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-19203" class="comments-container"></div><div id="comment-tools-19203" class="comment-tools"></div><div class="clear"></div><div id="comment-19203-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

