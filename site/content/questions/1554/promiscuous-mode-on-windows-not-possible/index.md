+++
type = "question"
title = "Promiscuous mode on Windows - not possible?"
description = '''I am studying some network security and have two questions:   The WinPCap library that Wireshark (for Windows) is using requires that the network card can be set into promiscuous mode to be able to capture all packets &quot;in the air&quot;. I have understood that not many network cards can be set into that m...'''
date = "2010-12-31T05:40:00Z"
lastmod = "2010-12-31T07:54:00Z"
weight = 1554
keywords = [ "windows", "promiscuous", "mode" ]
aliases = [ "/questions/1554" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Promiscuous mode on Windows - not possible?](/questions/1554/promiscuous-mode-on-windows-not-possible)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1554-score" class="post-score" title="current number of votes">1</div><span id="post-1554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am studying some network security and have two questions:</p><ol><li><p>The WinPCap library that Wireshark (for Windows) is using requires that the network card can be set into promiscuous mode to be able to capture all packets "in the air". I have understood that not many network cards can be set into that mode in Windows. So is it uncommon to use Wireshark for Windows to capture wifi packets?</p></li><li><p>So to the next question: The famous tool Firesheep also uses WinPCap. When Firesheep was released, all newspapers worldwide said it was so easy to hijack HTTP sessions, but you still need a network card that supports promiscuous mode in Windows, which is uncommon, right?</p></li><li><p>My wifi adapter is an Atheros AR5007, can it be set in promiscuous mode in Windows? How about Linux?</p></li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Dec '10, 05:40</strong></p><img src="https://secure.gravatar.com/avatar/b40d415d5a5ed5142e38ad841b2e176a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rox&#39;s gravatar image" /><p><span>Rox</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rox has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Dec '10, 06:17</strong> </span></p></div></div><div id="comments-container-1554" class="comments-container"></div><div id="comment-tools-1554" class="comment-tools"></div><div class="clear"></div><div id="comment-1554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1557"></span>

<div id="answer-container-1557" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1557-score" class="post-score" title="current number of votes">1</div><span id="post-1557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Most windows wifi NICs cannot be used to capture in promiscuous mode. Most Windows wired NICs can be set to promiscuous mode. No clue on your Atheros chipset - sorry.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Dec '10, 07:54</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span></p></div></div><div id="comments-container-1557" class="comments-container"></div><div id="comment-tools-1557" class="comment-tools"></div><div class="clear"></div><div id="comment-1557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

