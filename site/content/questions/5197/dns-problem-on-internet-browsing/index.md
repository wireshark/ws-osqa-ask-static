+++
type = "question"
title = "DNS Problem on internet browsing."
description = '''Is there a way to reset or flush registry DNS of windows XP service pack 3? Each time when i typed in an internet address, browser respond an error unable to locate/match website address to I.P. address, that something is wrong with the DNS.'''
date = "2011-07-24T19:14:00Z"
lastmod = "2011-07-25T02:22:00Z"
weight = 5197
keywords = [ "dns" ]
aliases = [ "/questions/5197" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [DNS Problem on internet browsing.](/questions/5197/dns-problem-on-internet-browsing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5197-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5197-score" class="post-score" title="current number of votes">0</div><span id="post-5197-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to reset or flush registry DNS of windows XP service pack 3? Each time when i typed in an internet address, browser respond an error unable to locate/match website address to I.P. address, that something is wrong with the DNS.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '11, 19:14</strong></p><img src="https://secure.gravatar.com/avatar/c3d8290c6fcfb178b932433e9760e864?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kane&#39;s gravatar image" /><p><span>Kane</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kane has no accepted answers">0%</span></p></div></div><div id="comments-container-5197" class="comments-container"></div><div id="comment-tools-5197" class="comment-tools"></div><div class="clear"></div><div id="comment-5197-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5202"></span>

<div id="answer-container-5202" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5202-score" class="post-score" title="current number of votes">1</div><span id="post-5202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ipconfig /flushdns</p><p>See ipconfig /? for more info</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jul '11, 23:43</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-5202" class="comments-container"><span id="5206"></span><div id="comment-5206" class="comment"><div id="post-5206-score" class="comment-score"></div><div class="comment-text"><p>Just a note that if you think this is an answer to the question the submitter of the question should mark it as such by clicking the "tick" for the answer.</p><p>This helps others find answers to their similar questions.</p></div><div id="comment-5206-info" class="comment-info"><span class="comment-age">(25 Jul '11, 02:22)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-5202" class="comment-tools"></div><div class="clear"></div><div id="comment-5202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

