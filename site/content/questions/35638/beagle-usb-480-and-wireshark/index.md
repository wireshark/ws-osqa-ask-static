+++
type = "question"
title = "Beagle USB 480 and Wireshark"
description = '''I am trying to convert the traffic captured via Beagle USB 480 and feed into Wireshark for further processing. Does anyone has a solution for it? '''
date = "2014-08-20T11:41:00Z"
lastmod = "2014-08-25T15:49:00Z"
weight = 35638
keywords = [ "beagle", "usb", "480" ]
aliases = [ "/questions/35638" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Beagle USB 480 and Wireshark](/questions/35638/beagle-usb-480-and-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35638-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35638-score" class="post-score" title="current number of votes">0</div><span id="post-35638-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to convert the traffic captured via Beagle USB 480 and feed into Wireshark for further processing. Does anyone has a solution for it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-beagle" rel="tag" title="see questions tagged &#39;beagle&#39;">beagle</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-480" rel="tag" title="see questions tagged &#39;480&#39;">480</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '14, 11:41</strong></p><img src="https://secure.gravatar.com/avatar/5304435ff877fc95e4f7c05bf2d459a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rad&#39;s gravatar image" /><p><span>Rad</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rad has no accepted answers">0%</span></p></div></div><div id="comments-container-35638" class="comments-container"></div><div id="comment-tools-35638" class="comment-tools"></div><div class="clear"></div><div id="comment-35638-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35732"></span>

<div id="answer-container-35732" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35732-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35732-score" class="post-score" title="current number of votes">0</div><span id="post-35732-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>According to the following KB article</p><blockquote><p><a href="http://www.totalphase.com/blog/2013/03/support-question-of-the-week-monitoring-ethernet-frames-using-the-beagle-usb-480-analyzer/">http://www.totalphase.com/blog/2013/03/support-question-of-the-week-monitoring-ethernet-frames-using-the-beagle-usb-480-analyzer/</a></p></blockquote><p>Cite: "<strong>Several of our customers</strong> monitor data using the Data Center software, <strong>export the capture as a CSV file, and re-encode the data for Wireshark</strong> using a custom application they wrote."</p><p>Unfortunately they don't tell you how their customers did the re-encoding. Probably you should contact the vendors support and ask for help. Maybe they have access to one of those custom applications and are willing to share it with you...</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Aug '14, 15:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-35732" class="comments-container"></div><div id="comment-tools-35732" class="comment-tools"></div><div class="clear"></div><div id="comment-35732-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

