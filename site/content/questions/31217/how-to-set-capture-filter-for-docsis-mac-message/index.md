+++
type = "question"
title = "How to set capture filter for DOCSIS MAC message?"
description = '''I need to capture DOCSIS map message encapsulated in DEPI (L2TP over IP) packet. Anyone know if any legacy capture filter I can use for such case? thanks in advance!'''
date = "2014-03-27T07:30:00Z"
lastmod = "2014-04-02T18:16:00Z"
weight = 31217
keywords = [ "capture", "capture-filter", "docsis" ]
aliases = [ "/questions/31217" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to set capture filter for DOCSIS MAC message?](/questions/31217/how-to-set-capture-filter-for-docsis-mac-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31217-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31217-score" class="post-score" title="current number of votes">0</div><span id="post-31217-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to capture DOCSIS map message encapsulated in DEPI (L2TP over IP) packet. Anyone know if any legacy capture filter I can use for such case? thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-docsis" rel="tag" title="see questions tagged &#39;docsis&#39;">docsis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '14, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/3540a8ef2d9764d4e0ad88b01abed52c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="guyin&#39;s gravatar image" /><p><span>guyin</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="guyin has no accepted answers">0%</span></p></div></div><div id="comments-container-31217" class="comments-container"><span id="31223"></span><div id="comment-31223" class="comment"><div id="post-31223-score" class="comment-score"></div><div class="comment-text"><p>Can you post some sample frames of such traffic on cloudshark.org?</p></div><div id="comment-31223-info" class="comment-info"><span class="comment-age">(27 Mar '14, 10:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="31241"></span><div id="comment-31241" class="comment"><div id="post-31241-score" class="comment-score"></div><div class="comment-text"><p>Hi, Kurt</p><p>Thanks for giving hand again. I upload a capture file example in dropbox, pls download it in following link: <a href="https://www.dropbox.com/sh/rd2nuojuj0ctc16/qEfz4DwhMo/0905_8*1_map.pcapng">https://www.dropbox.com/sh/rd2nuojuj0ctc16/qEfz4DwhMo/0905_8*1_map.pcapng</a></p><p>Do you know if we have any legacy support of DOCSIS in capture-filter? or any documents I can refer to? thanks in advance!</p></div><div id="comment-31241-info" class="comment-info"><span class="comment-age">(27 Mar '14, 22:11)</span> <span class="comment-user userinfo">guyin</span></div></div><span id="31447"></span><div id="comment-31447" class="comment"><div id="post-31447-score" class="comment-score"></div><div class="comment-text"><p>Hi, Anyone know about the capture filter support for DOCSIS? up.</p></div><div id="comment-31447-info" class="comment-info"><span class="comment-age">(02 Apr '14, 18:16)</span> <span class="comment-user userinfo">guyin</span></div></div></div><div id="comment-tools-31217" class="comment-tools"></div><div class="clear"></div><div id="comment-31217-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

