+++
type = "question"
title = "Can Wireshark be installed on multiple computers with the license it comes with"
description = '''I&#x27;m testing Wireshark and before I buy it I want to make sure I can use it at our multiple locations. Does the license allow this?'''
date = "2013-11-27T07:57:00Z"
lastmod = "2013-11-28T12:33:00Z"
weight = 27500
keywords = [ "license" ]
aliases = [ "/questions/27500" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark be installed on multiple computers with the license it comes with](/questions/27500/can-wireshark-be-installed-on-multiple-computers-with-the-license-it-comes-with)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27500-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27500-score" class="post-score" title="current number of votes">0</div><span id="post-27500-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm testing Wireshark and before I buy it I want to make sure I can use it at our multiple locations. Does the license allow this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-license" rel="tag" title="see questions tagged &#39;license&#39;">license</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '13, 07:57</strong></p><img src="https://secure.gravatar.com/avatar/dc64ba98cbcc0d138e2c7ad2778ca9d5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jrsitman&#39;s gravatar image" /><p><span>jrsitman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jrsitman has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Nov '13, 08:18</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-27500" class="comments-container"></div><div id="comment-tools-27500" class="comment-tools"></div><div class="clear"></div><div id="comment-27500-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="27502"></span>

<div id="answer-container-27502" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27502-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27502-score" class="post-score" title="current number of votes">3</div><span id="post-27502-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Install it as many times and in as many places as you'd like. Wireshark is distributed under the <a href="https://www.gnu.org/licenses/gpl-2.0.html">GNU GPLv2</a>. We don't place any restrictions on its use.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '13, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Nov '13, 08:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-27502" class="comments-container"></div><div id="comment-tools-27502" class="comment-tools"></div><div class="clear"></div><div id="comment-27502-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="27540"></span>

<div id="answer-container-27540" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27540-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27540-score" class="post-score" title="current number of votes">0</div><span id="post-27540-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I'm testing Wireshark and before I buy it</p></blockquote><p>You know that you can get Wireshark without buying it, right? You can <a href="http://www.wireshark.org/download.html">download it for free</a>; you can get binaries for Windows and OS X (32-bit and 64-bit), and can get the source code. Many Linux distributions provide binary packages in their own packaging systems, as do some *BSDs and Solaris 11.</p><p>And, as Gerald notes, there are no restrictions on how many machines you can put it on. It's both <a href="https://en.wikipedia.org/wiki/Freeware">freeware</a> ("free as in beer") and <a href="http://www.gnu.org/philosophy/free-sw.html">free software</a> ("free as in speech").</p><p>(If somebody's "selling" Wireshark, note that it is <em>illegal</em> for them to restrict the number of machines they can put it on, as per the GNU GPL. If they're just selling standard Wireshark, you might as well just download it. If they've made enhancements, the GPL may require them to make the source code for those enhancements available, and not to restrict whether, for example, the Wireshark developers can incorporate those enhancements into the standard version of Wireshark and give the resulting version away for free. They might also be selling <em>support</em> for Wireshark, but that's a different matter.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Nov '13, 12:33</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-27540" class="comments-container"></div><div id="comment-tools-27540" class="comment-tools"></div><div class="clear"></div><div id="comment-27540-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

