+++
type = "question"
title = "Wireshark for linux"
description = '''whats the newest version available for Linux.. i can&#x27;t seem to find a newer version for Linux.'''
date = "2011-02-08T08:39:00Z"
lastmod = "2011-12-03T02:45:00Z"
weight = 2231
keywords = [ "linux" ]
aliases = [ "/questions/2231" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark for linux](/questions/2231/wireshark-for-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2231-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2231-score" class="post-score" title="current number of votes">1</div><span id="post-2231-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>whats the newest version available for Linux.. i can't seem to find a newer version for Linux.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '11, 08:39</strong></p><img src="https://secure.gravatar.com/avatar/b55c18d0696d3f0302ce84166242d655?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Curtis%20Gregory&#39;s gravatar image" /><p><span>Curtis Gregory</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Curtis Gregory has no accepted answers">0%</span></p></div></div><div id="comments-container-2231" class="comments-container"></div><div id="comment-tools-2231" class="comment-tools"></div><div class="clear"></div><div id="comment-2231-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="2236"></span>

<div id="answer-container-2236" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2236-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2236-score" class="post-score" title="current number of votes">2</div><span id="post-2236-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Both questions and answers here tend to live for a long time, so rather than giving you an answer that will get quickly out of date, let me instead direct you to the main <a href="http://www.wireshark.org/">Wireshark web site</a> and ask if you are able to find your answer there under "latest release."<br />
</p><p>Releases for <a href="http://www.wireshark.org/download.html">Windows and OSX</a> are available directly from the wireshark.org web site and third-party packages (including ones for many Linux distros) are available through <a href="http://www.wireshark.org/download.html#thirdparty">this list</a>. Additionally, if you have a relatively current Linux distribution, you can probably just use yum, or apt or the package manager of your choice to very simply install Wireshark.</p><p>If all else fails, it's not hard to download the source code and build it yourself.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Feb '11, 10:54</strong></p><img src="https://secure.gravatar.com/avatar/6f579677517345ebea1cfef9e9e88f0c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="beroset&#39;s gravatar image" /><p><span>beroset</span><br />
<span class="score" title="226 reputation points">226</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="beroset has 4 accepted answers">33%</span> </br></p></div></div><div id="comments-container-2236" class="comments-container"></div><div id="comment-tools-2236" class="comment-tools"></div><div class="clear"></div><div id="comment-2236-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="7753"></span>

<div id="answer-container-7753" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7753-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7753-score" class="post-score" title="current number of votes">1</div><span id="post-7753-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>if u r using ubuntu or your linux distro hav apt-get then just enter this command in termunal:</p><blockquote><p>sudo apt-get install wireshark</p></blockquote><p>that's all</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Dec '11, 02:45</strong></p><img src="https://secure.gravatar.com/avatar/2a1d9dcf1bed0566568415137cdcfc79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="XKumarX&#39;s gravatar image" /><p><span>XKumarX</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="XKumarX has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Dec '11, 02:46</strong> </span></p></div></div><div id="comments-container-7753" class="comments-container"></div><div id="comment-tools-7753" class="comment-tools"></div><div class="clear"></div><div id="comment-7753-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

