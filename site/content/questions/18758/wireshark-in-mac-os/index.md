+++
type = "question"
title = "Wireshark in MAC OS?"
description = '''Hello, I have a MAC OS X Version 10.7. I cannot function with Wireshark. I cannot even initiate the program. How can solve this any suggestion. I try to open , the wireshark icon pops at the bottom of the screen and then it dissapears.  Any one can you help. Thanks Bharat C P'''
date = "2013-02-19T16:35:00Z"
lastmod = "2014-04-14T14:30:00Z"
weight = 18758
keywords = [ "development", "mac", "crash" ]
aliases = [ "/questions/18758" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark in MAC OS?](/questions/18758/wireshark-in-mac-os)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18758-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18758-score" class="post-score" title="current number of votes">0</div><span id="post-18758-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have a MAC OS X Version 10.7. I cannot function with Wireshark. I cannot even initiate the program. How can solve this any suggestion.</p><p>I try to open , the wireshark icon pops at the bottom of the screen and then it dissapears.</p><p>Any one can you help.</p><p>Thanks Bharat C P</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '13, 16:35</strong></p><img src="https://secure.gravatar.com/avatar/e689f2131ff3f3113d0b2cee2f420ebf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BharatNT2IE&#39;s gravatar image" /><p><span>BharatNT2IE</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BharatNT2IE has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '14, 22:32</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-18758" class="comments-container"></div><div id="comment-tools-18758" class="comment-tools"></div><div class="clear"></div><div id="comment-18758-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31805"></span>

<div id="answer-container-31805" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31805-score" class="post-score" title="current number of votes">0</div><span id="post-31805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You must have X11 installed in order for Wireshark to display anything, have you made sure to install the developer tools from Apple? You may be able to install this update:</p><p><a href="http://support.apple.com/downloads/X11_for_Mac_OS_X_1_0"></a><a href="http://support.apple.com/downloads/X11_for_Mac_OS_X_1_0">http://support.apple.com/downloads/X11_for_Mac_OS_X_1_0</a></p><p>Once you update to Mountain Lion (10.8) or Mavericks (10.9) you will have to install XQuartz in order to use X11.</p><p>XQuartz is available here: <a href="http://support.apple.com/kb/ht5293"></a><a href="http://support.apple.com/kb/ht5293">http://support.apple.com/kb/ht5293</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Apr '14, 14:30</strong></p><img src="https://secure.gravatar.com/avatar/5b11899f6ef8d3994b8bcc4e5c27609f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mire3212&#39;s gravatar image" /><p><span>mire3212</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mire3212 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Apr '14, 09:38</strong> </span></p></div></div><div id="comments-container-31805" class="comments-container"></div><div id="comment-tools-31805" class="comment-tools"></div><div class="clear"></div><div id="comment-31805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

