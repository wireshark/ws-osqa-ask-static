+++
type = "question"
title = "tshark on solaris"
description = '''Is there tshark on solaris and if does how to install it? I need to conver pcap to text from shell. Thanks'''
date = "2010-12-16T06:36:00Z"
lastmod = "2010-12-19T12:38:00Z"
weight = 1376
keywords = [ "to", "solaris", "picap", "text" ]
aliases = [ "/questions/1376" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tshark on solaris](/questions/1376/tshark-on-solaris)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1376-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1376-score" class="post-score" title="current number of votes">0</div><span id="post-1376-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there tshark on solaris and if does how to install it?</p><p>I need to conver pcap to text from shell.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-solaris" rel="tag" title="see questions tagged &#39;solaris&#39;">solaris</span> <span class="post-tag tag-link-picap" rel="tag" title="see questions tagged &#39;picap&#39;">picap</span> <span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '10, 06:36</strong></p><img src="https://secure.gravatar.com/avatar/a6177ee35aeb33bea7993b8b362e3517?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adostic&#39;s gravatar image" /><p><span>adostic</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adostic has no accepted answers">0%</span></p></div></div><div id="comments-container-1376" class="comments-container"></div><div id="comment-tools-1376" class="comment-tools"></div><div class="clear"></div><div id="comment-1376-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1396"></span>

<div id="answer-container-1396" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1396-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1396-score" class="post-score" title="current number of votes">1</div><span id="post-1396-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is TShark shipped as part of Solaris? No.</p><p>If you install a third-party Wireshark package on Solaris, or build and install Wireshark from source code on Solaris, do you get TShark? Yes.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '10, 18:37</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-1396" class="comments-container"><span id="1399"></span><div id="comment-1399" class="comment"><div id="post-1399-score" class="comment-score"></div><div class="comment-text"><p>Thanks,this is the answer I wanted.</p></div><div id="comment-1399-info" class="comment-info"><span class="comment-age">(19 Dec '10, 12:38)</span> <span class="comment-user userinfo">adostic</span></div></div></div><div id="comment-tools-1396" class="comment-tools"></div><div class="clear"></div><div id="comment-1396-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

