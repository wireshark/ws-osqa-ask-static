+++
type = "question"
title = "Unable to see decrypted client SSL requests"
description = '''Hi folks; I am using IE and|or Firefox to do some browsing to pages protected with SSL. I have the server private key, and have configured Wireshark to use it to decrypt the server responses. This works great. My problem is that the browser requests are still encrypted within wireshark so I am not s...'''
date = "2012-04-06T05:26:00Z"
lastmod = "2012-04-06T06:02:00Z"
weight = 9980
keywords = [ "ssl", "client" ]
aliases = [ "/questions/9980" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to see decrypted client SSL requests](/questions/9980/unable-to-see-decrypted-client-ssl-requests)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9980-score" class="post-score" title="current number of votes">0</div><span id="post-9980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi folks;</p><p>I am using IE and|or Firefox to do some browsing to pages protected with SSL. I have the server private key, and have configured Wireshark to use it to decrypt the server responses. This works great. My problem is that the browser requests are still encrypted within wireshark so I am not seeing the entire ssl transaction in the clear.</p><p>Do I need to provide wireshark the private key being used my the browser for encryption ?? If so, where would I find the private key being used by the browser ??</p><p>Any help/guidance would be appreciated (yes, I have read the various wiki's but haven't seen this thoroughly addressed).</p><p>thanks, wk</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-client" rel="tag" title="see questions tagged &#39;client&#39;">client</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '12, 05:26</strong></p><img src="https://secure.gravatar.com/avatar/2b12f1f0687101a1dd8f75db884aed8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wakelt&#39;s gravatar image" /><p><span>wakelt</span><br />
<span class="score" title="13 reputation points">13</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wakelt has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>06 Apr '12, 06:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-9980" class="comments-container"><span id="9981"></span><div id="comment-9981" class="comment"><div id="post-9981-score" class="comment-score"></div><div class="comment-text"><p>I converted your "answer" to a question in its own right. This is a Q&amp;A site not a forum. Please see the <a href="http://ask.wireshark.org/faq/">FAQ</a> for more details.</p></div><div id="comment-9981-info" class="comment-info"><span class="comment-age">(06 Apr '12, 06:02)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-9980" class="comment-tools"></div><div class="clear"></div><div id="comment-9980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

