+++
type = "question"
title = "how to  Capture packet ??"
description = '''i am connected with my own wifi &amp;amp; its open wifi and many other people also use it  so is it possible to capture others data who are connected with same wireless ??'''
date = "2014-05-27T11:52:00Z"
lastmod = "2014-05-27T13:00:00Z"
weight = 33112
keywords = [ "wireshark" ]
aliases = [ "/questions/33112" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to Capture packet ??](/questions/33112/how-to-capture-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33112-score" class="post-score" title="current number of votes">0</div><span id="post-33112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am connected with my own wifi &amp; its open wifi and many other people also use it so is it possible to capture others data who are connected with same wireless ??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '14, 11:52</strong></p><img src="https://secure.gravatar.com/avatar/bd79e364d20ad0ddca34d88bb145a0f7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="r4nd0m&#39;s gravatar image" /><p><span>r4nd0m</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="r4nd0m has no accepted answers">0%</span></p></div></div><div id="comments-container-33112" class="comments-container"></div><div id="comment-tools-33112" class="comment-tools"></div><div class="clear"></div><div id="comment-33112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33117"></span>

<div id="answer-container-33117" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33117-score" class="post-score" title="current number of votes">1</div><span id="post-33117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, just read the following Wikis</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a><br />
<a href="http://wiki.wireshark.org/HowToDecrypt802.11">http://wiki.wireshark.org/HowToDecrypt802.11</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '14, 12:50</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-33117" class="comments-container"><span id="33118"></span><div id="comment-33118" class="comment"><div id="post-33118-score" class="comment-score"></div><div class="comment-text"><p>any video or something like that ??</p></div><div id="comment-33118-info" class="comment-info"><span class="comment-age">(27 May '14, 12:53)</span> <span class="comment-user userinfo">r4nd0m</span></div></div><span id="33119"></span><div id="comment-33119" class="comment"><div id="post-33119-score" class="comment-score"></div><div class="comment-text"><p><a href="http://www.youtube.com/results?search_query=wireshark+wifi+sniffing">http://www.youtube.com/results?search_query=wireshark+wifi+sniffing</a></p></div><div id="comment-33119-info" class="comment-info"><span class="comment-age">(27 May '14, 13:00)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33117" class="comment-tools"></div><div class="clear"></div><div id="comment-33117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

