+++
type = "question"
title = "megaco H.248 decoding only partially (1.6.5)"
description = '''Trying to decode h.248 megaco packets, only getting a partial decode of the packet. majority of the packet is shown as additional text and can only be viewed in binary. is there a fix for this?'''
date = "2012-02-27T14:13:00Z"
lastmod = "2012-02-28T08:20:00Z"
weight = 9263
keywords = [ "h.248", "megaco", "media" ]
aliases = [ "/questions/9263" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [megaco H.248 decoding only partially (1.6.5)](/questions/9263/megaco-h248-decoding-only-partially-165)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9263-score" class="post-score" title="current number of votes">0</div><span id="post-9263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trying to decode h.248 megaco packets, only getting a partial decode of the packet. majority of the packet is shown as additional text and can only be viewed in binary. is there a fix for this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h.248" rel="tag" title="see questions tagged &#39;h.248&#39;">h.248</span> <span class="post-tag tag-link-megaco" rel="tag" title="see questions tagged &#39;megaco&#39;">megaco</span> <span class="post-tag tag-link-media" rel="tag" title="see questions tagged &#39;media&#39;">media</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '12, 14:13</strong></p><img src="https://secure.gravatar.com/avatar/c7d4f074e4fab588bd5033488bb84192?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ddd&#39;s gravatar image" /><p><span>ddd</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ddd has no accepted answers">0%</span></p></div></div><div id="comments-container-9263" class="comments-container"><span id="9268"></span><div id="comment-9268" class="comment"><div id="post-9268-score" class="comment-score"></div><div class="comment-text"><p>Could you please add an example packet file to see what you are referring to?</p><p>(Converted from an answer to a comment to reflecty <a href="http://ask.wireshark.org">ask.wireshark.org</a> convention. Please see the FAQ).</p></div><div id="comment-9268-info" class="comment-info"><span class="comment-age">(28 Feb '12, 08:20)</span> <span class="comment-user userinfo">Hiftu</span></div></div></div><div id="comment-tools-9263" class="comment-tools"></div><div class="clear"></div><div id="comment-9263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

