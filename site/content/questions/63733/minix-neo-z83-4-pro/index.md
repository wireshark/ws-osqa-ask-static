+++
type = "question"
title = "minix neo z83-4 pro"
description = '''hello  the softare is compatible with this system minix neo z83-4 pro?'''
date = "2017-10-07T15:52:00Z"
lastmod = "2017-10-24T04:34:00Z"
weight = 63733
keywords = [ "minix" ]
aliases = [ "/questions/63733" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [minix neo z83-4 pro](/questions/63733/minix-neo-z83-4-pro)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63733-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63733-score" class="post-score" title="current number of votes">0</div><span id="post-63733-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello</p><p>the softare is compatible with this system minix neo z83-4 pro?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-minix" rel="tag" title="see questions tagged &#39;minix&#39;">minix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '17, 15:52</strong></p><img src="https://secure.gravatar.com/avatar/cfbaa9b6a683a5635d9225852395b656?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scanman&#39;s gravatar image" /><p><span>scanman</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scanman has no accepted answers">0%</span></p></div></div><div id="comments-container-63733" class="comments-container"><span id="63742"></span><div id="comment-63742" class="comment"><div id="post-63742-score" class="comment-score"></div><div class="comment-text"><p>Generally, the hardware doesn't matter, it's the OS running on the hardware. What is that?</p></div><div id="comment-63742-info" class="comment-info"><span class="comment-age">(08 Oct '17, 14:15)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="63787"></span><div id="comment-63787" class="comment"><div id="post-63787-score" class="comment-score"></div><div class="comment-text"><p>windows 10</p></div><div id="comment-63787-info" class="comment-info"><span class="comment-age">(10 Oct '17, 02:20)</span> <span class="comment-user userinfo">scanman</span></div></div></div><div id="comment-tools-63733" class="comment-tools"></div><div class="clear"></div><div id="comment-63733-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63789"></span>

<div id="answer-container-63789" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63789-score" class="post-score" title="current number of votes">0</div><span id="post-63789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes Wireshark runs on Windows 10.</p><p>As noted on the download page the list of supported platforms can be found in the <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChIntroPlatforms.html">User's Guide</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '17, 02:31</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63789" class="comments-container"><span id="64142"></span><div id="comment-64142" class="comment"><div id="post-64142-score" class="comment-score"></div><div class="comment-text"><p>anyway i can confirm now works</p></div><div id="comment-64142-info" class="comment-info"><span class="comment-age">(24 Oct '17, 04:16)</span> <span class="comment-user userinfo">scanman</span></div></div><span id="64145"></span><div id="comment-64145" class="comment"><div id="post-64145-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-64145-info" class="comment-info"><span class="comment-age">(24 Oct '17, 04:34)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-63789" class="comment-tools"></div><div class="clear"></div><div id="comment-63789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

