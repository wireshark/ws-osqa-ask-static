+++
type = "question"
title = "ACS and tacacs+"
description = '''Hi I am trying to decode a tacacs+ session between cisco routers and cisco acs. in regular (telnet )configuration everything works. when i try to decode session comes from a router that using PPP &quot;aaa authentication ppp default group tacacs+&quot; the wireshark see the packets as malformed packet. Does a...'''
date = "2011-07-06T05:35:00Z"
lastmod = "2011-07-06T05:35:00Z"
weight = 4925
keywords = [ "cisco" ]
aliases = [ "/questions/4925" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ACS and tacacs+](/questions/4925/acs-and-tacacs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4925-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4925-score" class="post-score" title="current number of votes">0</div><span id="post-4925-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am trying to decode a tacacs+ session between cisco routers and cisco acs. in regular (telnet )configuration everything works. when i try to decode session comes from a router that using PPP "aaa authentication ppp default group tacacs+" the wireshark see the packets as malformed packet. Does anybody know how to solve this ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '11, 05:35</strong></p><img src="https://secure.gravatar.com/avatar/66855c8333a4d3496c45dbc1c80bbdad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yarivco&#39;s gravatar image" /><p><span>yarivco</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yarivco has no accepted answers">0%</span></p></div></div><div id="comments-container-4925" class="comments-container"></div><div id="comment-tools-4925" class="comment-tools"></div><div class="clear"></div><div id="comment-4925-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

