+++
type = "question"
title = "can wireshark evaluate the performance of different versions of TCP"
description = '''sir how to find different vesions of TCP using Wireshark and how to compare the diffrent versions of TCP performance using wireshark pls help '''
date = "2013-09-13T03:06:00Z"
lastmod = "2013-09-13T03:32:00Z"
weight = 24638
keywords = [ "tcp" ]
aliases = [ "/questions/24638" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can wireshark evaluate the performance of different versions of TCP](/questions/24638/can-wireshark-evaluate-the-performance-of-different-versions-of-tcp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24638-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24638-score" class="post-score" title="current number of votes">0</div><span id="post-24638-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>sir how to find different vesions of TCP using Wireshark and how to compare the diffrent versions of TCP performance using wireshark pls help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '13, 03:06</strong></p><img src="https://secure.gravatar.com/avatar/d316fdeb8063b3195b7cc70909561df0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shaziya%20islam&#39;s gravatar image" /><p><span>shaziya islam</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shaziya islam has no accepted answers">0%</span></p></div></div><div id="comments-container-24638" class="comments-container"></div><div id="comment-tools-24638" class="comment-tools"></div><div class="clear"></div><div id="comment-24638-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24639"></span>

<div id="answer-container-24639" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24639-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24639-score" class="post-score" title="current number of votes">3</div><span id="post-24639-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you can compare different TCP stack types (I guess that's what you mean with "versions") with Wireshark if you know how the stack types behave. So first you need to find out what the differences between the stack types are, and then look for telltale signs in the decoded packets. It is quite a challenging task, and I know of at least one person who has written a thesis about this (with months of preparation). So good luck! :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '13, 03:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-24639" class="comments-container"></div><div id="comment-tools-24639" class="comment-tools"></div><div class="clear"></div><div id="comment-24639-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

