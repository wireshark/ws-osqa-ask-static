+++
type = "question"
title = "Wifi monitoring mode Windows XP"
description = '''Good day! I downloaded your program and install it on Windows XP. Connect WiFi modules TP-Link TL-WN821N TP-Link TL-WN822N TP-Link TL-WN721N TP-Link TL-WN722N D-Link DWA-160 v.B2 No device has been detected by the program. Prompt, please, if you have the drivers for these devices? What device can wo...'''
date = "2014-11-06T05:07:00Z"
lastmod = "2016-06-07T02:08:00Z"
weight = 37614
keywords = [ "xp", "monitoring", "mode" ]
aliases = [ "/questions/37614" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wifi monitoring mode Windows XP](/questions/37614/wifi-monitoring-mode-windows-xp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37614-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37614-score" class="post-score" title="current number of votes">0</div><span id="post-37614-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good day!</p><p>I downloaded your program and install it on Windows XP.</p><p>Connect WiFi modules TP-Link TL-WN821N TP-Link TL-WN822N TP-Link TL-WN721N TP-Link TL-WN722N D-Link DWA-160 v.B2</p><p>No device has been detected by the program.</p><p>Prompt, please, if you have the drivers for these devices? What device can work with your program in monitor mode under Windows XP</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xp" rel="tag" title="see questions tagged &#39;xp&#39;">xp</span> <span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '14, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/f37b65d8b75fb3de205a3dc562578e12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="%D0%9B%D0%B5%D0%B2%20%D0%91%D0%B5%D1%80%D0%B5%D0%B6%D0%BD%D0%BE%D0%B9&#39;s gravatar image" /><p><span>Лев Бережной</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Лев Бережной has no accepted answers">0%</span></p></div></div><div id="comments-container-37614" class="comments-container"></div><div id="comment-tools-37614" class="comment-tools"></div><div class="clear"></div><div id="comment-37614-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="37615"></span>

<div id="answer-container-37615" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37615-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37615-score" class="post-score" title="current number of votes">0</div><span id="post-37615-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on WLAN capturing, in particular the notes about <a href="http://wiki.wireshark.org/CaptureSetup/WLAN#Windows">monitor mode on Windows</a> not working.</p><p>This is why such things as <a href="http://www.riverbed.com/us/products/cascade/airpcap.php">AirPCap</a> were created.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Nov '14, 05:29</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-37615" class="comments-container"></div><div id="comment-tools-37615" class="comment-tools"></div><div class="clear"></div><div id="comment-37615-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="53256"></span>

<div id="answer-container-53256" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53256-score" class="post-score" title="current number of votes">0</div><span id="post-53256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Buenas tardes, descargué el programa Wireshark e intente hacer que me reconozca la antena usb TP-LINK TL-WN8200ND en Windows XP pero no logro hacerla funcionar. Podrían Uds. por favor indicarme como puedo utilizar Wireshark en Windows XP con antena TP-LINK TL-WN8200ND. Desde ya muchas gracias y quedo a la espera una pronta y favorable respuesta.</p><p>Atte.</p><p>Ricardo Iribarren</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jun '16, 17:22</strong></p><img src="https://secure.gravatar.com/avatar/c4e7a687651747c425982b18029169c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ricardo%20Iribarren&#39;s gravatar image" /><p><span>Ricardo Irib...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ricardo Iribarren has no accepted answers">0%</span></p></div></div><div id="comments-container-53256" class="comments-container"><span id="53266"></span><div id="comment-53266" class="comment"><div id="post-53266-score" class="comment-score"></div><div class="comment-text"><p><span>@Ricardo</span> Irib...</p><p>Please don't ask a (slightly) different question by posting an "answer" to a similar question. This is a Q&amp;A site, not a forum, please read the FAQ for more info.</p><p>The usual language used here is English, can you please try to translate your question. You should convert your "answer" to a question in its own right with a suitable subject line.</p></div><div id="comment-53266-info" class="comment-info"><span class="comment-age">(07 Jun '16, 02:08)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-53256" class="comment-tools"></div><div class="clear"></div><div id="comment-53256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

