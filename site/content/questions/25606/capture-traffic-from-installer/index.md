+++
type = "question"
title = "Capture Traffic from Installer"
description = '''I have an installer that I know downloads other packages from the &#x27;net. I&#x27;m trying to use WS to discover what those requests are for, but so far I&#x27;m not having much luck. I&#x27;ve tried to find or filter for requests that I already know are being made, and I can&#x27;t even find those. Does anyone know what ...'''
date = "2013-10-03T10:47:00Z"
lastmod = "2013-10-03T11:24:00Z"
weight = 25606
keywords = [ "wireshark" ]
aliases = [ "/questions/25606" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Traffic from Installer](/questions/25606/capture-traffic-from-installer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25606-score" class="post-score" title="current number of votes">0</div><span id="post-25606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have an installer that I know downloads other packages from the 'net. I'm trying to use WS to discover what those requests are for, but so far I'm not having much luck. I've tried to find or filter for requests that I already know are being made, and I can't even find those. Does anyone know what I might be doing wrong, or what I could do to troubleshoot?</p><p>If it matters, I'm running the installer behind a proxy at work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '13, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/50fd5511f035cc670d35e748b39b95f6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SusanHPayne&#39;s gravatar image" /><p><span>SusanHPayne</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SusanHPayne has no accepted answers">0%</span></p></div></div><div id="comments-container-25606" class="comments-container"><span id="25607"></span><div id="comment-25607" class="comment"><div id="post-25607-score" class="comment-score"></div><div class="comment-text"><p>Do you see ANY traffic? Are you using a display or capture filter? I know in my environment, I don't talk to our proxy server on a standard HTTP port 80.</p></div><div id="comment-25607-info" class="comment-info"><span class="comment-age">(03 Oct '13, 11:24)</span> <span class="comment-user userinfo">smp</span></div></div></div><div id="comment-tools-25606" class="comment-tools"></div><div class="clear"></div><div id="comment-25606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

