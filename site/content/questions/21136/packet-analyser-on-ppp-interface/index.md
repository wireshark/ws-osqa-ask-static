+++
type = "question"
title = "Packet analyser on PPP interface"
description = '''Is there any packet analyser on PPP interface?'''
date = "2013-05-14T09:13:00Z"
lastmod = "2013-05-16T04:14:00Z"
weight = 21136
keywords = [ "analyser", "packet" ]
aliases = [ "/questions/21136" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Packet analyser on PPP interface](/questions/21136/packet-analyser-on-ppp-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21136-score" class="post-score" title="current number of votes">0</div><span id="post-21136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any packet analyser on PPP interface?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analyser" rel="tag" title="see questions tagged &#39;analyser&#39;">analyser</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 May '13, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/f411ef7fc1f833adad171b7d2eb550a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dhira&#39;s gravatar image" /><p><span>Dhira</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dhira has no accepted answers">0%</span></p></div></div><div id="comments-container-21136" class="comments-container"><span id="21138"></span><div id="comment-21138" class="comment"><div id="post-21138-score" class="comment-score"></div><div class="comment-text"><p>what is your OS?</p></div><div id="comment-21138-info" class="comment-info"><span class="comment-age">(14 May '13, 09:39)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="21170"></span><div id="comment-21170" class="comment"><div id="post-21170-score" class="comment-score"></div><div class="comment-text"><p>OS: Windows 8</p></div><div id="comment-21170-info" class="comment-info"><span class="comment-age">(16 May '13, 01:13)</span> <span class="comment-user userinfo">Dhira</span></div></div></div><div id="comment-tools-21136" class="comment-tools"></div><div class="clear"></div><div id="comment-21136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="21160"></span>

<div id="answer-container-21160" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21160-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21160-score" class="post-score" title="current number of votes">0</div><span id="post-21160-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Kinda confused by the question. However you can analyze a PPP session off a client if you can get between the Client and Server to sniff the traffic. Wireshark has the dissector.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '13, 12:52</strong></p><img src="https://secure.gravatar.com/avatar/b9a4be286b5fb288b053e3bf1ad16710?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samiam&#39;s gravatar image" /><p><span>samiam</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samiam has no accepted answers">0%</span></p></div></div><div id="comments-container-21160" class="comments-container"></div><div id="comment-tools-21160" class="comment-tools"></div><div class="clear"></div><div id="comment-21160-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21180"></span>

<div id="answer-container-21180" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21180-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21180-score" class="post-score" title="current number of votes">0</div><span id="post-21180-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>OS: Windows 8</p></blockquote><p><a href="http://www.microsoft.com/en-us/download/details.aspx?id=4865">Microsoft Network Monitor</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 May '13, 04:14</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-21180" class="comments-container"></div><div id="comment-tools-21180" class="comment-tools"></div><div class="clear"></div><div id="comment-21180-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

