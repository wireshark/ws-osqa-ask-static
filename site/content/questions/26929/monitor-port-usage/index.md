+++
type = "question"
title = "Monitor port usage"
description = '''Hi guys, i need you help. I would like to monitor three tcp ports and receive an email alert if there is no traffic on these ports. It&#x27;s possibile with wireshark? Thank you very much. Regards Stefano'''
date = "2013-11-13T02:17:00Z"
lastmod = "2013-11-13T02:36:00Z"
weight = 26929
keywords = [ "alert" ]
aliases = [ "/questions/26929" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor port usage](/questions/26929/monitor-port-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26929-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26929-score" class="post-score" title="current number of votes">0</div><span id="post-26929-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys, i need you help. I would like to monitor three tcp ports and receive an email alert if there is no traffic on these ports. It's possibile with wireshark?</p><p>Thank you very much. Regards</p><p>Stefano</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-alert" rel="tag" title="see questions tagged &#39;alert&#39;">alert</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '13, 02:17</strong></p><img src="https://secure.gravatar.com/avatar/6c8bab7da1a2289421c86fd2eb8c5579?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Stefo%20G&#39;s gravatar image" /><p><span>Stefo G</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Stefo G has no accepted answers">0%</span></p></div></div><div id="comments-container-26929" class="comments-container"></div><div id="comment-tools-26929" class="comment-tools"></div><div class="clear"></div><div id="comment-26929-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26930"></span>

<div id="answer-container-26930" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26930-score" class="post-score" title="current number of votes">1</div><span id="post-26930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark isn't really the tool for this, it's a packet analysis tool, not a Network Monitoring tool.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '13, 02:36</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-26930" class="comments-container"></div><div id="comment-tools-26930" class="comment-tools"></div><div class="clear"></div><div id="comment-26930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

