+++
type = "question"
title = "I can&#x27;t see any ping replies"
description = '''Hi, When capturing a ping between a host (with a RTL8192DU WIFI adaptor) and the access point with an external WIFI adaptor in monitor mode I can&#x27;t see the reply packets in Wireshark even though ping doesn&#x27;t complain about packet loss. This is not always the case. By pinging the same access point fr...'''
date = "2015-03-10T13:15:00Z"
lastmod = "2015-03-11T08:57:00Z"
weight = 40448
keywords = [ "wireless", "reply", "wifi", "ping", "monitor-mode" ]
aliases = [ "/questions/40448" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I can't see any ping replies](/questions/40448/i-cant-see-any-ping-replies)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40448-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40448-score" class="post-score" title="current number of votes">0</div><span id="post-40448-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When capturing a ping between a host (with a RTL8192DU WIFI adaptor) and the access point with an external WIFI adaptor in monitor mode I can't see the reply packets in Wireshark even though ping doesn't complain about packet loss. This is not always the case. By pinging the same access point from another host (with a WL1271 WIFI adaptor) I can see the reply packets properly.</p><p>Encryption is disabled on the AP. Both hosts are running Linux.</p><p>I already checked all available channels. Hence, channel hoping seems not to be the problem. Further, I assume that the reply packet isn't send come on another channel as the request.</p><p>Can anyone help?</p><p>-Carsten</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-reply" rel="tag" title="see questions tagged &#39;reply&#39;">reply</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-ping" rel="tag" title="see questions tagged &#39;ping&#39;">ping</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '15, 13:15</strong></p><img src="https://secure.gravatar.com/avatar/fa9cc1986dc2cd5d366183e5c881eb24?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cbehling&#39;s gravatar image" /><p><span>cbehling</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cbehling has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Mar '15, 13:22</strong> </span></p></div></div><div id="comments-container-40448" class="comments-container"><span id="40474"></span><div id="comment-40474" class="comment"><div id="post-40474-score" class="comment-score"></div><div class="comment-text"><p>Update: I found out that some packages are captured if the RTL8192DU and the capture adaptor (Qualcomm Atheros QCA9565 / AR9565) are on the same host and I enable capturing on all interfaces.</p></div><div id="comment-40474-info" class="comment-info"><span class="comment-age">(11 Mar '15, 08:57)</span> <span class="comment-user userinfo">cbehling</span></div></div></div><div id="comment-tools-40448" class="comment-tools"></div><div class="clear"></div><div id="comment-40448-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

