+++
type = "question"
title = "Should beginner start off using Wireshark 2.0, and if so, where to find a &quot;get started&quot; tutorial?"
description = '''I&#x27;ve never used Wireshark before, and for that matter, any network protocol analyzer. I now need to do so to troubleshoot an IP protocol called BACnet. I just installed Wireshark 2.0 on my Windows PC, but didn&#x27;t know what to do next. Search for tutorials, and found a bunch, but they looked different...'''
date = "2015-12-15T16:59:00Z"
lastmod = "2015-12-28T04:35:00Z"
weight = 48554
keywords = [ "wireshark-2.0", "wireshark" ]
aliases = [ "/questions/48554" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Should beginner start off using Wireshark 2.0, and if so, where to find a "get started" tutorial?](/questions/48554/should-beginner-start-off-using-wireshark-20-and-if-so-where-to-find-a-get-started-tutorial)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48554-score" class="post-score" title="current number of votes">0</div><span id="post-48554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've never used Wireshark before, and for that matter, any network protocol analyzer. I now need to do so to troubleshoot an IP protocol called BACnet.</p><p>I just installed Wireshark 2.0 on my Windows PC, but didn't know what to do next. Search for tutorials, and found a bunch, but they looked different that what I was seeing.</p><p>Then, I found out that I was using the new (and presumably improved) 2.0 version. I see that I could also launch "Wireshark Legacy".</p><p>Questions...</p><ol><li>If just starting off, should I jump right into using 2.0?</li><li>If so, any recommended quick get started documents?</li></ol><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Dec '15, 16:59</strong></p><img src="https://secure.gravatar.com/avatar/938ce772de83dc07272b66f4b12a5453?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NotionCommotion&#39;s gravatar image" /><p><span>NotionCommotion</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NotionCommotion has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Dec '15, 17:26</strong> </span></p></div></div><div id="comments-container-48554" class="comments-container"></div><div id="comment-tools-48554" class="comment-tools"></div><div class="clear"></div><div id="comment-48554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="48566"></span>

<div id="answer-container-48566" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48566-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48566-score" class="post-score" title="current number of votes">0</div><span id="post-48566-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The functionality is pretty much the same, it's the UI that has changed mostly, although some things haven't yet been implemented in the new UI. The legacy link offers the same functionality using the old UI, which is likely to be dropped for 2.2, maybe.</p><p>If you're happy to work things out yourself when the UI differs from all the old tutorials I think you'll actually learn more using the new UI. If you're getting stuck, then by all means use the legacy UI but remember it's likely to go away at some time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Dec '15, 04:21</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-48566" class="comments-container"><span id="48568"></span><div id="comment-48568" class="comment"><div id="post-48568-score" class="comment-score"></div><div class="comment-text"><p>Isn't the "legacy link" and the "old UI" the same? Recommendations on any "get started" documentation for the new 2.0 UI? Thanks</p></div><div id="comment-48568-info" class="comment-info"><span class="comment-age">(16 Dec '15, 04:31)</span> <span class="comment-user userinfo">NotionCommotion</span></div></div><span id="48572"></span><div id="comment-48572" class="comment"><div id="post-48572-score" class="comment-score"></div><div class="comment-text"><p>Yes, legacy is the old UI (or GTK). The new UI is also know as the Qt UI.</p><p>Wireshark University has a Webinar on Wireshark 2.0 listed on their front <a href="http://www.wiresharktraining.com/">page</a>.</p></div><div id="comment-48572-info" class="comment-info"><span class="comment-age">(16 Dec '15, 05:55)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-48566" class="comment-tools"></div><div class="clear"></div><div id="comment-48566-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="48730"></span>

<div id="answer-container-48730" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48730-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48730-score" class="post-score" title="current number of votes">0</div><span id="post-48730-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If it helps I have a lot of Wireshark training videos on my website and starting posting Wireshark 2.0 stuff.</p><p>No registration, all free, no gimmicks, just enjoy.</p><p><a href="http://thetechfirm.com/wireshark/wireshark.html">http://thetechfirm.com/wireshark/wireshark.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Dec '15, 04:35</strong></p><img src="https://secure.gravatar.com/avatar/dbc4d8cb6be85bd586ca4bf211e1337c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thetechfirm&#39;s gravatar image" /><p><span>thetechfirm</span><br />
<span class="score" title="64 reputation points">64</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thetechfirm has no accepted answers">0%</span></p></div></div><div id="comments-container-48730" class="comments-container"></div><div id="comment-tools-48730" class="comment-tools"></div><div class="clear"></div><div id="comment-48730-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

