+++
type = "question"
title = "can wireshark capture traffic in a loop of 10 pc?"
description = '''how to enable the wireshark to capture the traffic from 10 pc and all of them being sent back the network to the host? do I need to add interfaces in the remote network by entering the other devices interfaces?'''
date = "2016-06-20T19:40:00Z"
lastmod = "2016-06-20T23:35:00Z"
weight = 53578
keywords = [ "token-ring", "loop" ]
aliases = [ "/questions/53578" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can wireshark capture traffic in a loop of 10 pc?](/questions/53578/can-wireshark-capture-traffic-in-a-loop-of-10-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53578-score" class="post-score" title="current number of votes">0</div><span id="post-53578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to enable the wireshark to capture the traffic from 10 pc and all of them being sent back the network to the host? do I need to add interfaces in the remote network by entering the other devices interfaces?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-token-ring" rel="tag" title="see questions tagged &#39;token-ring&#39;">token-ring</span> <span class="post-tag tag-link-loop" rel="tag" title="see questions tagged &#39;loop&#39;">loop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '16, 19:40</strong></p><img src="https://secure.gravatar.com/avatar/0f027e8694583c69b34592a1f3328ab6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anuar&#39;s gravatar image" /><p><span>anuar</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anuar has no accepted answers">0%</span></p></div></div><div id="comments-container-53578" class="comments-container"><span id="53579"></span><div id="comment-53579" class="comment"><div id="post-53579-score" class="comment-score"></div><div class="comment-text"><p>token-ring ?</p></div><div id="comment-53579-info" class="comment-info"><span class="comment-age">(20 Jun '16, 23:35)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-53578" class="comment-tools"></div><div class="clear"></div><div id="comment-53578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

