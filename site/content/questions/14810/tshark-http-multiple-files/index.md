+++
type = "question"
title = "Tshark HTTP multiple files"
description = '''Hi, How can i get all the http.requests and http.response packets from 10 pcap files. I know how to do it from a single file. But i was wondering how to do it on 10 files, cause I&#x27;m thinking that http packets may be truncated between one file and the other. Am i right ? There is capture options to n...'''
date = "2012-10-09T02:27:00Z"
lastmod = "2012-10-09T02:27:00Z"
weight = 14810
keywords = [ "files", "multiple", "pcap", "tshark" ]
aliases = [ "/questions/14810" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Tshark HTTP multiple files](/questions/14810/tshark-http-multiple-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14810-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14810-score" class="post-score" title="current number of votes">0</div><span id="post-14810-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>How can i get all the http.requests and http.response packets from 10 pcap files. I know how to do it from a single file. But i was wondering how to do it on 10 files, cause I'm thinking that http packets may be truncated between one file and the other. Am i right ?</p><p>There is capture options to not split http packets between pcap files ? Or even better there is a way to recover truncated packets between pcap files ?</p><p>I've wrote a brief listener in lua... my experience is not so advanced.</p><p>I'm using tshark cause i need this operation as a batch process to run on very big pcap files.</p><p>Thanks very much</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span> <span class="post-tag tag-link-multiple" rel="tag" title="see questions tagged &#39;multiple&#39;">multiple</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '12, 02:27</strong></p><img src="https://secure.gravatar.com/avatar/071cc8bdde247f6fc8798768afbf5c76?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marcs&#39;s gravatar image" /><p><span>Marcs</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marcs has no accepted answers">0%</span></p></div></div><div id="comments-container-14810" class="comments-container"></div><div id="comment-tools-14810" class="comment-tools"></div><div class="clear"></div><div id="comment-14810-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

