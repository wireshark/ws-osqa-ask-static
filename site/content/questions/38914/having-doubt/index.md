+++
type = "question"
title = "Having doubt"
description = '''I have a wireless router from which few people use internet.When I use wireshark and select wireless connection network as interface I see that packets only from my ip address to router and so on is shown but not the others who connected to same router. How can I achieve this?'''
date = "2015-01-06T18:49:00Z"
lastmod = "2015-01-07T01:49:00Z"
weight = 38914
keywords = [ "interfaces" ]
aliases = [ "/questions/38914" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Having doubt](/questions/38914/having-doubt)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38914-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38914-score" class="post-score" title="current number of votes">0</div><span id="post-38914-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a wireless router from which few people use internet.When I use wireshark and select wireless connection network as interface I see that packets only from my ip address to router and so on is shown but not the others who connected to same router. How can I achieve this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '15, 18:49</strong></p><img src="https://secure.gravatar.com/avatar/f5536d4a1eeaee2f51c69cd77e011ac3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="manoj%20reddy&#39;s gravatar image" /><p><span>manoj reddy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="manoj reddy has no accepted answers">0%</span></p></div></div><div id="comments-container-38914" class="comments-container"></div><div id="comment-tools-38914" class="comment-tools"></div><div class="clear"></div><div id="comment-38914-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38921"></span>

<div id="answer-container-38921" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38921-score" class="post-score" title="current number of votes">0</div><span id="post-38921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please read the WLAN/Wireless Capture Wiki, especially the part about "monitor mode"</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p></blockquote><p>Then search this site for "monitor mode" to see other answers for similar questions.</p><blockquote><p><a href="https://ask.wireshark.org/search/?q=monitor+mode&amp;Submit=Search&amp;t=question">https://ask.wireshark.org/search/?q=monitor+mode&amp;Submit=Search&amp;t=question</a><br />
</p></blockquote><p>Then search google for "Wireshark wireless sniffing",</p><blockquote><p><a href="https://www.google.com/?q=wireshark+wireless+sniffing">https://www.google.com/?q=wireshark+wireless+sniffing</a></p></blockquote><p>OR read the following article.</p><blockquote><p><a href="http://searchnetworking.techtarget.com/feature/Wireless-sniffing-best-practices-using-Wireshark">http://searchnetworking.techtarget.com/feature/Wireless-sniffing-best-practices-using-Wireshark</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jan '15, 01:49</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-38921" class="comments-container"></div><div id="comment-tools-38921" class="comment-tools"></div><div class="clear"></div><div id="comment-38921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

