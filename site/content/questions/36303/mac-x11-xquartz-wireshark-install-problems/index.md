+++
type = "question"
title = "Mac X11-XQuartz Wireshark Install Problems"
description = '''I have to get Wireshark to run on my MacBook laptop for school, and I have not for the life of me been able to do it. Is there anyone who could help troubleshoot this with me? I have downloaded and installed XQuartz for the purposes of having X11, which Wireshark said it required. That didn&#x27;t work. ...'''
date = "2014-09-13T19:02:00Z"
lastmod = "2014-09-13T19:02:00Z"
weight = 36303
keywords = [ "x11", "xquartz", "mac", "install", "apple" ]
aliases = [ "/questions/36303" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac X11-XQuartz Wireshark Install Problems](/questions/36303/mac-x11-xquartz-wireshark-install-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36303-score" class="post-score" title="current number of votes">0</div><span id="post-36303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have to get Wireshark to run on my MacBook laptop for school, and I have not for the life of me been able to do it. Is there anyone who could help troubleshoot this with me? I have downloaded and installed XQuartz for the purposes of having X11, which Wireshark said it required. That didn't work. I uninstalled and reinstalled both XQuartz and Wireshark. That didn't work. I don't have much programming experience, so some of the scripts I've seen may work, but perhaps I'm running them incorrectly? Basically I'm at the end of my rope, and I really need to get this done by tomorrow (Sunday, the 14th of September, 2014). Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x11" rel="tag" title="see questions tagged &#39;x11&#39;">x11</span> <span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-apple" rel="tag" title="see questions tagged &#39;apple&#39;">apple</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '14, 19:02</strong></p><img src="https://secure.gravatar.com/avatar/94fdad831ce7eb0e5788af30642a47c1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="harshmp&#39;s gravatar image" /><p><span>harshmp</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="harshmp has no accepted answers">0%</span></p></div></div><div id="comments-container-36303" class="comments-container"></div><div id="comment-tools-36303" class="comment-tools"></div><div class="clear"></div><div id="comment-36303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

