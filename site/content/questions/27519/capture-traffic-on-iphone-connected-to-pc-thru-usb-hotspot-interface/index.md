+++
type = "question"
title = "Capture traffic on iPhone connected to PC thru USB hotspot interface"
description = '''I want to monitor network traffic on a laptop connected to an iPhone via iPhone&#x27;s hotspot USB interface. Is Wireshark up to the job and in that case: how do I configure the interface in the Wireshark application?'''
date = "2013-11-27T21:39:00Z"
lastmod = "2013-11-27T21:39:00Z"
weight = 27519
keywords = [ "usb", "traffic", "network", "hotspot", "iphone" ]
aliases = [ "/questions/27519" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture traffic on iPhone connected to PC thru USB hotspot interface](/questions/27519/capture-traffic-on-iphone-connected-to-pc-thru-usb-hotspot-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27519-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27519-score" class="post-score" title="current number of votes">0</div><span id="post-27519-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to monitor network traffic on a laptop connected to an iPhone via iPhone's hotspot USB interface. Is Wireshark up to the job and in that case: how do I configure the interface in the Wireshark application?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-hotspot" rel="tag" title="see questions tagged &#39;hotspot&#39;">hotspot</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '13, 21:39</strong></p><img src="https://secure.gravatar.com/avatar/02575600ff1cd948368be6b8c37fb140?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Caragoli&#39;s gravatar image" /><p><span>Caragoli</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Caragoli has no accepted answers">0%</span></p></div></div><div id="comments-container-27519" class="comments-container"></div><div id="comment-tools-27519" class="comment-tools"></div><div class="clear"></div><div id="comment-27519-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

