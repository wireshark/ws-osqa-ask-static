+++
type = "question"
title = "[closed] time interval of each packets during re transmission"
description = '''I have an application which runs on master PC and receives data from number of slave controllers in certain time interval. I have an issue with re transmission time interval for the lost packets from Slave to Master. When the Packets are lost while they are transmitted from Slave to Master, The lost...'''
date = "2016-01-05T06:50:00Z"
lastmod = "2016-01-05T07:55:00Z"
weight = 48869
keywords = [ "interval", "retransmission", "time" ]
aliases = [ "/questions/48869" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] time interval of each packets during re transmission](/questions/48869/time-interval-of-each-packets-during-re-transmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48869-score" class="post-score" title="current number of votes">0</div><span id="post-48869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have an application which runs on master PC and receives data from number of slave controllers in certain time interval.</p><p>I have an issue with re transmission time interval for the lost packets from Slave to Master.</p><p>When the Packets are lost while they are transmitted from Slave to Master, The lost packets are re transmitted for every one sec according to the wireshark log.</p><p>But during the normal transmission, something around 10-15 packets are transmitted with in fraction amount of time.</p><p>Actually the first lost packet gets re transmitted after one sec which is fine but subsequent packets get retransmitted in every one sec time interval which is not acceptable in my case.</p><p>So please advise me if I can configure the time interval of each packets during re-transmission .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interval" rel="tag" title="see questions tagged &#39;interval&#39;">interval</span> <span class="post-tag tag-link-retransmission" rel="tag" title="see questions tagged &#39;retransmission&#39;">retransmission</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '16, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/5f95711321f840922720016670d7d3b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tony_2013&#39;s gravatar image" /><p><span>Tony_2013</span><br />
<span class="score" title="11 reputation points">11</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tony_2013 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>05 Jan '16, 07:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-48869" class="comments-container"><span id="48872"></span><div id="comment-48872" class="comment"><div id="post-48872-score" class="comment-score"></div><div class="comment-text"><p>This is not a wireshark-related question;</p><p>You will need to contact whomever provides the software for the "slave controller".</p></div><div id="comment-48872-info" class="comment-info"><span class="comment-age">(05 Jan '16, 07:55)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-48869" class="comment-tools"></div><div class="clear"></div><div id="comment-48869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Not wirehark-related" by Bill Meier 05 Jan '16, 07:56

</div>

</div>

</div>

