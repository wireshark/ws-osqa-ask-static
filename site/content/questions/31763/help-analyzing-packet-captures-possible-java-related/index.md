+++
type = "question"
title = "Help analyzing Packet Captures (possible Java related)"
description = '''We have an internal business website. I have 2 packet captures, one 417 pkts, the other 581 pkts. They recorded packets for about 1-2 minutes of traffic, to 1 specific IP/Server only. I am trying to see why the one with 417 packets FAILS to initiate http/java/page/forms And why the 581 packets WORKS...'''
date = "2014-04-11T17:29:00Z"
lastmod = "2014-04-11T17:29:00Z"
weight = 31763
keywords = [ "troubleshooting" ]
aliases = [ "/questions/31763" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help analyzing Packet Captures (possible Java related)](/questions/31763/help-analyzing-packet-captures-possible-java-related)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31763-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31763-score" class="post-score" title="current number of votes">0</div><span id="post-31763-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have an internal business website. I have 2 packet captures, one 417 pkts, the other 581 pkts. They recorded packets for about 1-2 minutes of traffic, to 1 specific IP/Server only.</p><p>I am trying to see why the one with 417 packets FAILS to initiate http/java/page/forms And why the 581 packets WORKS to initiate the http/java/page/forms</p><p>Trying to ID the differences, learn why and how to resolve...TROUBLESHOOT the problem =)</p><p>Sorry, had to run...back now, more info follows here:</p><p>2 servers, 1 works fine, other not</p><p>The one that fails, about 70-80% of the time, user clicks on desktop shortcut to URL Its internal link, goes to this server which fails</p><p>They receive the initial page, provide user credentials and login - All is fine - this works all the time. Login is succesful.</p><p>NOW: Problem is when the next link is clicked, this is what fails about 70-80% of the time to load properly. It wont load, citing "forms" and failures most of the time.</p><p>The packet captures are both, 1 session that I captured that worked - another that failed.</p><p>Both I immediately logged out/off of and stopped recording.</p><p>Therefore, trying to learn how to distinguish the differences between the 2 captures. ID what is in one, thats not in the other. Why one capture, the session worked - the other, it failed.</p><p>Any helpers available? Any recommendations? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '14, 17:29</strong></p><img src="https://secure.gravatar.com/avatar/489f548088d407561aaf04255f18e08c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JFL&#39;s gravatar image" /><p><span>JFL</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JFL has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Apr '14, 17:46</strong> </span></p></div></div><div id="comments-container-31763" class="comments-container"></div><div id="comment-tools-31763" class="comment-tools"></div><div class="clear"></div><div id="comment-31763-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

