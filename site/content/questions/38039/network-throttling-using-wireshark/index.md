+++
type = "question"
title = "Network throttling using wireshark"
description = '''I&#x27;m researching on Wireshark &amp;amp; new to this, Can someone suggest me whether we can do network throttling ? If so, please let me know the way of doing it !'''
date = "2014-11-21T04:53:00Z"
lastmod = "2014-11-22T03:54:00Z"
weight = 38039
keywords = [ "throttling" ]
aliases = [ "/questions/38039" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Network throttling using wireshark](/questions/38039/network-throttling-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38039-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38039-score" class="post-score" title="current number of votes">0</div><span id="post-38039-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm researching on Wireshark &amp; new to this, Can someone suggest me whether we can do network throttling ? If so, please let me know the way of doing it !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-throttling" rel="tag" title="see questions tagged &#39;throttling&#39;">throttling</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '14, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/5da36ed019ea1702fa86f32866e1757e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kalsfru&#39;s gravatar image" /><p><span>kalsfru</span><br />
<span class="score" title="21 reputation points">21</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kalsfru has no accepted answers">0%</span></p></div></div><div id="comments-container-38039" class="comments-container"></div><div id="comment-tools-38039" class="comment-tools"></div><div class="clear"></div><div id="comment-38039-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38043"></span>

<div id="answer-container-38043" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38043-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38043-score" class="post-score" title="current number of votes">1</div><span id="post-38043-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="kalsfru has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark can't do this. It's a packet analyser, not a network router.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '14, 05:12</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-38043" class="comments-container"><span id="38061"></span><div id="comment-38061" class="comment"><div id="post-38061-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the reply !</p></div><div id="comment-38061-info" class="comment-info"><span class="comment-age">(21 Nov '14, 16:19)</span> <span class="comment-user userinfo">kalsfru</span></div></div><span id="38067"></span><div id="comment-38067" class="comment"><div id="post-38067-score" class="comment-score"></div><div class="comment-text"><p>if you need to throttle or test packet loss behavior, built a linux router and use <a href="http://www.linuxfoundation.org/collaborate/workgroups/networking/netem">netem</a>.</p></div><div id="comment-38067-info" class="comment-info"><span class="comment-age">(22 Nov '14, 03:54)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-38043" class="comment-tools"></div><div class="clear"></div><div id="comment-38043-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

