+++
type = "question"
title = "SIP-I Decode:- National Specific ISUP parameter decode"
description = '''How can I add National Specific ISUP parameter&#x27;s into the Wireshark decode for SIP-I ? Once I&#x27;ve done that can I save the SIP-I variant as a specific version of SIP-I decode, eg SIP-I (UK ISUP) etc.'''
date = "2012-05-01T01:14:00Z"
lastmod = "2012-05-01T14:26:00Z"
weight = 10544
keywords = [ "wireshark" ]
aliases = [ "/questions/10544" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SIP-I Decode:- National Specific ISUP parameter decode](/questions/10544/sip-i-decode-national-specific-isup-parameter-decode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10544-score" class="post-score" title="current number of votes">0</div><span id="post-10544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I add National Specific ISUP parameter's into the Wireshark decode for SIP-I ?</p><p>Once I've done that can I save the SIP-I variant as a specific version of SIP-I decode, eg SIP-I (UK ISUP) etc.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '12, 01:14</strong></p><img src="https://secure.gravatar.com/avatar/2ff9913e360d948274b1c8ee541beabe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="IAMoramnot&#39;s gravatar image" /><p><span>IAMoramnot</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="IAMoramnot has no accepted answers">0%</span></p></div></div><div id="comments-container-10544" class="comments-container"></div><div id="comment-tools-10544" class="comment-tools"></div><div class="clear"></div><div id="comment-10544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10553"></span>

<div id="answer-container-10553" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10553-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10553-score" class="post-score" title="current number of votes">0</div><span id="post-10553-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The ISUP content of SIP-I is dissected by packet-isup.c You need to add code to dissect the UK variant, preferebly with a preference.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 May '12, 14:26</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-10553" class="comments-container"></div><div id="comment-tools-10553" class="comment-tools"></div><div class="clear"></div><div id="comment-10553-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

