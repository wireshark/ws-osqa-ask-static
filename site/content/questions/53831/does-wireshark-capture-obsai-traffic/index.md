+++
type = "question"
title = "Does wireshark capture OBSAI traffic?"
description = '''I&#x27;ll be thankful to know,if the wireshark supports capture &amp;amp; analysis of OBSAI traffic. Thanks, Abhieshek Deshpande'''
date = "2016-07-05T04:54:00Z"
lastmod = "2016-07-06T00:43:00Z"
weight = 53831
keywords = [ "obsai" ]
aliases = [ "/questions/53831" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does wireshark capture OBSAI traffic?](/questions/53831/does-wireshark-capture-obsai-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53831-score" class="post-score" title="current number of votes">0</div><span id="post-53831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'll be thankful to know,if the wireshark supports capture &amp; analysis of OBSAI traffic.</p><p>Thanks, Abhieshek Deshpande</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-obsai" rel="tag" title="see questions tagged &#39;obsai&#39;">obsai</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jul '16, 04:54</strong></p><img src="https://secure.gravatar.com/avatar/f5f5e3055a264c5ac0be65a9c6511c69?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Abhieshek&#39;s gravatar image" /><p><span>Abhieshek</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Abhieshek has no accepted answers">0%</span></p></div></div><div id="comments-container-53831" class="comments-container"></div><div id="comment-tools-53831" class="comment-tools"></div><div class="clear"></div><div id="comment-53831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53849"></span>

<div id="answer-container-53849" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53849-score" class="post-score" title="current number of votes">1</div><span id="post-53849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As far as I know there are no dissector for OBSAI interfaces. Also capture depends more on the capture hardware available vs. Wireshark being able to capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jul '16, 00:43</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-53849" class="comments-container"></div><div id="comment-tools-53849" class="comment-tools"></div><div class="clear"></div><div id="comment-53849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

