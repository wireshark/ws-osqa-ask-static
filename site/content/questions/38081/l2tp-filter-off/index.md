+++
type = "question"
title = "l2tp filter off"
description = '''Hi! How to filter off all l2tp layer to se ONLY what goes INSIDE l2tp, w/o all service traffic?'''
date = "2014-11-23T12:16:00Z"
lastmod = "2014-11-23T12:16:00Z"
weight = 38081
keywords = [ "l2tp" ]
aliases = [ "/questions/38081" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [l2tp filter off](/questions/38081/l2tp-filter-off)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38081-score" class="post-score" title="current number of votes">0</div><span id="post-38081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! How to filter off all l2tp layer to se ONLY what goes INSIDE l2tp, w/o all service traffic?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-l2tp" rel="tag" title="see questions tagged &#39;l2tp&#39;">l2tp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '14, 12:16</strong></p><img src="https://secure.gravatar.com/avatar/c0f015d5f809137266e741018df27920?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pavel%20Fomin&#39;s gravatar image" /><p><span>Pavel Fomin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pavel Fomin has no accepted answers">0%</span></p></div></div><div id="comments-container-38081" class="comments-container"></div><div id="comment-tools-38081" class="comment-tools"></div><div class="clear"></div><div id="comment-38081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

