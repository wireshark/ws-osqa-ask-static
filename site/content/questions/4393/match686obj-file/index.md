+++
type = "question"
title = "match686.obj file"
description = '''hi, during compilation of wireshark source i am getting error as &quot;match686.obj&quot; is not found, i tried to compile match686.asm n getting error as undefined symbol esp, if anybody has the object file of match686 then please send to sagu072@gmail.com. thank you.'''
date = "2011-06-05T23:29:00Z"
lastmod = "2011-06-06T04:01:00Z"
weight = 4393
keywords = [ "match686" ]
aliases = [ "/questions/4393" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [match686.obj file](/questions/4393/match686obj-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4393-score" class="post-score" title="current number of votes">0</div><span id="post-4393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, during compilation of wireshark source i am getting error as "match686.obj" is not found, i tried to compile match686.asm n getting error as undefined symbol esp, if anybody has the object file of match686 then please send to <span class="__cf_email__" data-cfemail="3e4d5f594b0e090c7e59535f5752105d515310">[email protected]</span> thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-match686" rel="tag" title="see questions tagged &#39;match686&#39;">match686</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '11, 23:29</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div></div><div id="comments-container-4393" class="comments-container"><span id="4395"></span><div id="comment-4395" class="comment"><div id="post-4395-score" class="comment-score"></div><div class="comment-text"><p>Hi, I would think that's when building zlib which Visual studio version are you using? have you tried a distclean before building?</p></div><div id="comment-4395-info" class="comment-info"><span class="comment-age">(06 Jun '11, 03:38)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="4396"></span><div id="comment-4396" class="comment"><div id="post-4396-score" class="comment-score"></div><div class="comment-text"><p>yes, distclean works fine, but wen i run nmake -f makefile.nmake i am getting error as <strong>"can not open input file match686.obj"</strong></p></div><div id="comment-4396-info" class="comment-info"><span class="comment-age">(06 Jun '11, 04:01)</span> <span class="comment-user userinfo">sagu072</span></div></div></div><div id="comment-tools-4393" class="comment-tools"></div><div class="clear"></div><div id="comment-4393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

