+++
type = "question"
title = "Encrypted POST data not decrypted"
description = '''To demonstrate SSL decryption, I created a dummy website running on a local webserver which prompts the user to login using HTTPS. I analyzed the traffic using WireShark and have successfully decrypted the SSL traffic. The cipher used is TLS_RSA_WITH_AES_256_CBC_SHA. However, the POST data (Line-bas...'''
date = "2011-10-30T06:19:00Z"
lastmod = "2011-10-30T06:19:00Z"
weight = 7159
keywords = [ "decryption", "post", "decrypt", "ssl" ]
aliases = [ "/questions/7159" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Encrypted POST data not decrypted](/questions/7159/encrypted-post-data-not-decrypted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7159-score" class="post-score" title="current number of votes">0</div><span id="post-7159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>To demonstrate SSL decryption, I created a dummy website running on a local webserver which prompts the user to login using HTTPS. I analyzed the traffic using WireShark and have successfully decrypted the SSL traffic. The cipher used is TLS_RSA_WITH_AES_256_CBC_SHA. However, the POST data (Line-based text data: application/x-www-form-urlencoded) containing the username and password remains encrypted. Any suggestions how to decrypt this data?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-post" rel="tag" title="see questions tagged &#39;post&#39;">post</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span> <span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '11, 06:19</strong></p><img src="https://secure.gravatar.com/avatar/3e9a0bc1ef1a8fb36c0d006a71179ad6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Caveman&#39;s gravatar image" /><p><span>Caveman</span><br />
<span class="score" title="26 reputation points">26</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Caveman has one accepted answer">50%</span></p></div></div><div id="comments-container-7159" class="comments-container"></div><div id="comment-tools-7159" class="comment-tools"></div><div class="clear"></div><div id="comment-7159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

