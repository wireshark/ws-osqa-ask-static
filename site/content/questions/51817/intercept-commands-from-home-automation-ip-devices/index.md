+++
type = "question"
title = "Intercept commands from Home Automation IP devices"
description = '''Is there anyway to capture and playback the commands from an automation controller that uses IP based technology? I would like to create an alternative way to activate the commands using a computer instead of the ip based touchpanel.  Thank you '''
date = "2016-04-20T05:20:00Z"
lastmod = "2016-04-20T06:59:00Z"
weight = 51817
keywords = [ "automation" ]
aliases = [ "/questions/51817" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Intercept commands from Home Automation IP devices](/questions/51817/intercept-commands-from-home-automation-ip-devices)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51817-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51817-score" class="post-score" title="current number of votes">0</div><span id="post-51817-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there anyway to capture and playback the commands from an automation controller that uses IP based technology? I would like to create an alternative way to activate the commands using a computer instead of the ip based touchpanel. Thank you<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-automation" rel="tag" title="see questions tagged &#39;automation&#39;">automation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '16, 05:20</strong></p><img src="https://secure.gravatar.com/avatar/8984897b43d7081c20162bb416501c6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gunnarvingren&#39;s gravatar image" /><p><span>gunnarvingren</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gunnarvingren has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '16, 05:20</strong> </span></p></div></div><div id="comments-container-51817" class="comments-container"></div><div id="comment-tools-51817" class="comment-tools"></div><div class="clear"></div><div id="comment-51817-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51820"></span>

<div id="answer-container-51820" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51820-score" class="post-score" title="current number of votes">0</div><span id="post-51820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a tool for capturing and analysing communication, so it should be possible for you to capture the communication. The complexity of this task depends on the physical layer of the network used between the touchpanel and the executive elements and sensors.</p><p>For playback you would need another tool than Wireshark. But mere playback may not be sufficient if the original communication is encrypted. To learn this it is necessary to take the capture and analyse it.</p><p>Look <a href="https://wiki.wireshark.org/CaptureSetup">here</a> for instructions how to set up a capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '16, 06:59</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-51820" class="comments-container"></div><div id="comment-tools-51820" class="comment-tools"></div><div class="clear"></div><div id="comment-51820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

