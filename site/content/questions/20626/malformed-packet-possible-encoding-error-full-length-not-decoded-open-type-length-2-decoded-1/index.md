+++
type = "question"
title = "Malformed packet: Possible encoding error full length not decoded, open type length 2, decoded 1"
description = '''Hi, I am seeing following error line in H323 setup message, this issue came after added H245SecurityCapability header. &quot;Possible encoding error full length not decoded, open type length 2, decoded 1&quot; Please any one tell me know what is this error and solution. I am not able to upload the image file....'''
date = "2013-04-19T06:52:00Z"
lastmod = "2013-04-19T07:12:00Z"
weight = 20626
keywords = [ "h225", "h323", "h245" ]
aliases = [ "/questions/20626" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed packet: Possible encoding error full length not decoded, open type length 2, decoded 1](/questions/20626/malformed-packet-possible-encoding-error-full-length-not-decoded-open-type-length-2-decoded-1)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20626-score" class="post-score" title="current number of votes">0</div><span id="post-20626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am seeing following error line in H323 setup message, this issue came after added H245SecurityCapability header.</p><p>"Possible encoding error full length not decoded, open type length 2, decoded 1"</p><p>Please any one tell me know what is this error and solution.</p><p>I am not able to upload the image file.</p><p>Thanks Vinayraj</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h225" rel="tag" title="see questions tagged &#39;h225&#39;">h225</span> <span class="post-tag tag-link-h323" rel="tag" title="see questions tagged &#39;h323&#39;">h323</span> <span class="post-tag tag-link-h245" rel="tag" title="see questions tagged &#39;h245&#39;">h245</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '13, 06:52</strong></p><img src="https://secure.gravatar.com/avatar/8ac19e9257675246f7c08277488e09a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vinayrajalur&#39;s gravatar image" /><p><span>vinayrajalur</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vinayrajalur has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Apr '13, 06:59</strong> </span></p></div></div><div id="comments-container-20626" class="comments-container"></div><div id="comment-tools-20626" class="comment-tools"></div><div class="clear"></div><div id="comment-20626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20628"></span>

<div id="answer-container-20628" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20628-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20628-score" class="post-score" title="current number of votes">0</div><span id="post-20628-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Probably an encoding or Wireshark bug. Open a bug report at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a> attaching a small trace file showing the problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '13, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-20628" class="comments-container"></div><div id="comment-tools-20628" class="comment-tools"></div><div class="clear"></div><div id="comment-20628-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

