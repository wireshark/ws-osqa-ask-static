+++
type = "question"
title = "Why cant I capture packets even after I turn off promiscuous mode?"
description = '''When capturing packets without promiscuous mode I get nothing. How do I set WireShark to work on my Wireless N router?'''
date = "2012-02-22T21:25:00Z"
lastmod = "2012-02-22T23:18:00Z"
weight = 9177
keywords = [ "wireless" ]
aliases = [ "/questions/9177" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why cant I capture packets even after I turn off promiscuous mode?](/questions/9177/why-cant-i-capture-packets-even-after-i-turn-off-promiscuous-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9177-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9177-score" class="post-score" title="current number of votes">0</div><span id="post-9177-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When capturing packets without promiscuous mode I get nothing. How do I set WireShark to work on my Wireless N router?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '12, 21:25</strong></p><img src="https://secure.gravatar.com/avatar/c2a461b70fd0fb3816c0cba94450fcaa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bburke89&#39;s gravatar image" /><p><span>bburke89</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bburke89 has no accepted answers">0%</span></p></div></div><div id="comments-container-9177" class="comments-container"></div><div id="comment-tools-9177" class="comment-tools"></div><div class="clear"></div><div id="comment-9177-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9179"></span>

<div id="answer-container-9179" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9179-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9179-score" class="post-score" title="current number of votes">0</div><span id="post-9179-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If this is on Windows, the answer may be "by installing Linux or *BSD on your machine". The support for Wi-Fi on Windows with WinPcap is limited, due to a combination of driver problems and WinPcap not supporting NDIS 6 (so that even with drivers that support Native Wi-Fi, WinPcap can't use it).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '12, 23:18</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-9179" class="comments-container"></div><div id="comment-tools-9179" class="comment-tools"></div><div class="clear"></div><div id="comment-9179-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

