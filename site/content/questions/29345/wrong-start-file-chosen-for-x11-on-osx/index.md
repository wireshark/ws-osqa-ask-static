+++
type = "question"
title = "Wrong start file chosen for X11 on OSX"
description = '''Hi i use Mac os X mavericks and when i originally installed wireshark i had to tell the program were X11 was i chose the wrong programe the mac thinks Apple script editor is X11 so wireshark doesn&#x27;t run does start up X11 but doesn&#x27;t function ive tryd to reinstall but that doesn&#x27;t work and Apple scri...'''
date = "2014-01-31T05:06:00Z"
lastmod = "2014-11-02T07:10:00Z"
weight = 29345
keywords = [ "mac", "stupid", "mavericks", "wireshark" ]
aliases = [ "/questions/29345" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wrong start file chosen for X11 on OSX](/questions/29345/wrong-start-file-chosen-for-x11-on-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29345-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29345-score" class="post-score" title="current number of votes">0</div><span id="post-29345-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i use Mac os X mavericks and when i originally installed wireshark i had to tell the program were X11 was i chose the wrong programe the mac thinks Apple script editor is X11 so wireshark doesn't run does start up X11 but doesn't function ive tryd to reinstall but that doesn't work and Apple script editor can't be removed...</p><p>any help is welcome</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-stupid" rel="tag" title="see questions tagged &#39;stupid&#39;">stupid</span> <span class="post-tag tag-link-mavericks" rel="tag" title="see questions tagged &#39;mavericks&#39;">mavericks</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '14, 05:06</strong></p><img src="https://secure.gravatar.com/avatar/76dd95a9cda6db6b08301998b928b792?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dutch&#39;s gravatar image" /><p><span>Dutch</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dutch has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Jan '14, 05:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-29345" class="comments-container"><span id="29480"></span><div id="comment-29480" class="comment"><div id="post-29480-score" class="comment-score"></div><div class="comment-text"><p>Same questions from my side! To uninstall Wireshark didn't help (also rm -r ~/.wireshark*) By accident I have chosen Xcode at first start of Wireshark. Now I can't find the link between Wireshark and Xcode in any of the configuration files (incl. plist) to get rid of the wrong entry. Any help is more than welcome!</p><p>Dirk</p></div><div id="comment-29480-info" class="comment-info"><span class="comment-age">(05 Feb '14, 23:44)</span> <span class="comment-user userinfo">dsi</span></div></div></div><div id="comment-tools-29345" class="comment-tools"></div><div class="clear"></div><div id="comment-29345-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29518"></span>

<div id="answer-container-29518" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29518-score" class="post-score" title="current number of votes">1</div><span id="post-29518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I could solve the problem (more or less) with the following workaround: I moved the wrong program/application into the trash and started Wireshark. Voila - I was asked again for the location of X11. After choosing the right application I moved the wrong one back from trash.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '14, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/8ae1ed74f27ae3b1e83eee4114fb6ad2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dsi&#39;s gravatar image" /><p><span>dsi</span><br />
<span class="score" title="26 reputation points">26</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dsi has no accepted answers">0%</span></p></div></div><div id="comments-container-29518" class="comments-container"><span id="29527"></span><div id="comment-29527" class="comment"><div id="post-29527-score" class="comment-score"></div><div class="comment-text"><p>+1 for lateral thinking!</p></div><div id="comment-29527-info" class="comment-info"><span class="comment-age">(07 Feb '14, 05:19)</span> <span class="comment-user userinfo">orionrush</span></div></div><span id="37541"></span><div id="comment-37541" class="comment"><div id="post-37541-score" class="comment-score"></div><div class="comment-text"><p>I tried it before but failed because OSX not allow me move systeminformation to trash. please share me. I'm newbie on OSX.</p></div><div id="comment-37541-info" class="comment-info"><span class="comment-age">(02 Nov '14, 07:10)</span> <span class="comment-user userinfo">deknoy</span></div></div></div><div id="comment-tools-29518" class="comment-tools"></div><div class="clear"></div><div id="comment-29518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

